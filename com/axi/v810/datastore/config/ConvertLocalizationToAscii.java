package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class will convert Localization.properties files from any Java supported encoding format to ascii.
 *
 * How it works:
 * It looks in the config/properties/nonAscii directory for any files that look like this:
 *   localization_xx.properties.yy
 *     where xx specifies the language, for example _ch_ZN
 *     where yy specifies the character encoding the file is in, for example UTF-16
 *
 * Any filenames in that directory that match the pattern above will be converted.
 * The original file will be left unmodified.  The newly converted file will be put in
 * localization_xx.properties in the config/properties directory
 *
 *
 * @author Bill Darbie
 */
public class ConvertLocalizationToAscii
{
  private static ConvertLocalizationToAscii _instance;
  private CharacterEncodingConverterAxi _characterEncodingConverter;

  /**
   * @author Bill Darbie
   */
  public static synchronized ConvertLocalizationToAscii getInstance()
  {
    if (_instance == null)
      _instance = new ConvertLocalizationToAscii();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ConvertLocalizationToAscii()
  {
    _characterEncodingConverter = new CharacterEncodingConverterAxi();
  }

  /**
   * Convert the inFile from UTF16 to ascii with escaped unicode characters
   * This is the format required by the Java properties files.
   *
   * @author Bill Darbie
   */
  public void convert() throws DatastoreException
  {
    String outputFileName;

    String propExtension = FileName.getPropertiesExtension().toLowerCase() + ".";
    String nonAsciiDir = Directory.getPropertiesNonAsciiDir();
    for (String inputFileName : FileUtilAxi.listAllFilesFullPathInDirectory(nonAsciiDir))
    {
      String inFileNameLower = inputFileName.toLowerCase();
      int index = inFileNameLower.indexOf(propExtension);
      int size = inFileNameLower.length();
      if ((index != -1) && (size > index))
      {
        // get the decoding string
        int index2 = inputFileName.lastIndexOf(".");
        Assert.expect(index2 != -1);
        Assert.expect(index + 1 < size);
        String decodingString = inputFileName.substring(index2 + 1, size);

        // get the file name only (no path)
        index2 = inputFileName.lastIndexOf(File.separator);
        Assert.expect(index2 != -1);
        Assert.expect(index + 1 < size);
        String fileNameOnly = inputFileName.substring(index2 + 1, size);
        index2 = fileNameOnly.lastIndexOf(".");
        Assert.expect(index2 != -1);
        fileNameOnly = fileNameOnly.substring(0, index2);

        // get the output file name
        outputFileName = Directory.getPropertiesDir() + File.separator + fileNameOnly;

        if (FileUtilAxi.exists(outputFileName))
        {
          File inFile = new File(inputFileName);
          long inputFileTime = inFile.lastModified();
          File outputFile = new File(outputFileName);
          long outputFileTime = outputFile.lastModified();
          if (inputFileTime < outputFileTime)
          {
            // the file has already been converted, don't reconvert it
            continue;
          }
        }

        // now do the conversion
        try
        {
          _characterEncodingConverter.convert(inputFileName, outputFileName, decodingString, "US-ASCII");
        }
        catch (CharacterEncodingNotSupportedDatastoreException ex)
        {
          DatastoreException dex = new CannotConvertLocalizationFileDatastoreException(inputFileName, decodingString);
          dex.initCause(ex);
          throw dex;
        }
      }
    }
  }
}
