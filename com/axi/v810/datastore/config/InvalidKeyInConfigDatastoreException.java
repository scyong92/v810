package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This exception is thrown when a config file has a key in it that it shoule not.
 * @author Bill Darbie
 */
class InvalidKeyInConfigDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  InvalidKeyInConfigDatastoreException(String key, String value, String configFile)
  {
    super(new LocalizedString("DS_ERROR_INVALID_KEY_IN_CONFIG_KEY", new Object[]{key, value, configFile}));

    Assert.expect(key != null);
    Assert.expect(value != null);
    Assert.expect(configFile != null);
  }
}