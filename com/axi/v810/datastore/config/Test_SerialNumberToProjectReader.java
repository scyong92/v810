package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class Test_SerialNumberToProjectReader extends UnitTest
{
  private SerialNumberToProjectReader _reader;

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SerialNumberToProjectReader());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _reader = SerialNumberToProjectReader.getInstance();

      try
      {
        // try opening a non-existant file
        String fileName = getTestDataDir() + File.separator + "nonExistantSerialNumToProject.config";
        _reader.setConfigFileNameForUnitTesting(fileName);
        Collection<SerialNumberRegularExpressionData> data =
          _reader.getSerialNumberRegularExpressionData();
        Assert.expect(false);
      }
      catch(CannotOpenFileDatastoreException de)
      {
        // do nothing
      }

      for(int i = 1; i < 3; ++i)
      {
        try
        {
          // try opening an invalid file
          String fileName = getTestDataDir() + File.separator + "invalidSerialNumToProject" + i + ".config";
          _reader.setConfigFileNameForUnitTesting(fileName);
          Collection<SerialNumberRegularExpressionData> data = _reader.getSerialNumberRegularExpressionData();
          Assert.expect(false);
        }
        catch(FileCorruptDatastoreException de)
        {
          // do nothing
        }
      }


      // try a good file
      String fileName = getTestDataDir() + File.separator + "serialNumToProject1.config";
      _reader.setConfigFileNameForUnitTesting(fileName);

      Collection<SerialNumberRegularExpressionData> data =
          _reader.getSerialNumberRegularExpressionData();
      Iterator it = data.iterator();
      while(it.hasNext())
      {
        SerialNumberRegularExpressionData serialNumData = (SerialNumberRegularExpressionData)it.next();
        String origEx = serialNumData.getOriginalExpression();
        String regEx = serialNumData.getRegularExpression();
        String projName = serialNumData.getProjectName();
        boolean isRegEx = serialNumData.isInterpretedAsRegularExpression();

        if (isRegEx)
          System.out.println("Regular expression: " + origEx + " -> " + regEx + " -> " + projName);
        else
          System.out.println("Simple wild cards: " + origEx + " -> " + regEx + " -> " + projName);
      }

      // try opening a empty file
      fileName = getTestDataDir() + File.separator + "emptySerialNumToProject.config";
      _reader.setConfigFileNameForUnitTesting(fileName);
      data = _reader.getSerialNumberRegularExpressionData();
      Assert.expect(data.size() == 0);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
