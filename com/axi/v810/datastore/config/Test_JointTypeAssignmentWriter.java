package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import java.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class Test_JointTypeAssignmentWriter extends UnitTest
{
  private JointTypeAssignmentWriter _writer;
  private JointTypeAssignmentReader _reader;

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_JointTypeAssignmentWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String fileName = "jointTypeAssignmentWriterRegressionTest.config";
    String fullPathFileName = FileName.getJointTypeAssignmentFullPath(fileName);

    try
    {
      if (FileUtilAxi.exists(fullPathFileName))
        FileUtilAxi.delete(fullPathFileName);

      JointTypeEnumAssignment jointTypeEnumAssignment = JointTypeEnumAssignment.getInstance();
      List<JointTypeEnumRule> rules = jointTypeEnumAssignment.getJointTypeEnumRules();
      _writer = JointTypeAssignmentWriter.getInstance();
      _writer.write("jointTypeAssignmentWriterRegressionTest.config", rules);

      _reader = JointTypeAssignmentReader.getInstance();
      List<JointTypeEnumRule> rulesReadIn = _reader.read(fileName);
      for (JointTypeEnumRule rule : rulesReadIn)
      {
        String refDes = "";
        if (rule.isRefDesPatternSet())
          refDes = rule.getRefDesPattern();
        String numPads = "";
        if (rule.isNumPadsSet())
          numPads = rule.getNumPadsPattern();
        String numRows = "";
        if (rule.isNumRowsSet())
          numRows = rule.getNumRowsPattern();
        String numCols = "";
        if (rule.isNumColsSet())
          numCols = rule.getNumColsPattern();
        String landPatternName = "";
        if (rule.isLandPatternNamePatternSet())
          landPatternName = rule.getLandPatternNamePattern();
        String isPadInCenterOfRowOrColumn = "";
        if (rule.isPadOneInCenterOfRowSet())
          isPadInCenterOfRowOrColumn = Boolean.toString(rule.isPadOneInCenterOfRow());
        String jointType = rule.getJointTypeEnum().getName();

        os.println("JointTypeEnum: " + jointType);
        os.println("  refDes: " + refDes);
        os.println("  numPads: " + numPads);
        os.println("  numRows: " + numRows);
        os.println("  numCols: " + numCols);
        os.println("  landPatternName: " + landPatternName);
        os.println("  isPadInCenter: " + isPadInCenterOfRowOrColumn);
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (FileUtilAxi.exists(fullPathFileName))
          FileUtilAxi.delete(fullPathFileName);
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }

    try
    {
      // read an empty file
      fileName = "emptyJointTypeAssignment.config";
      String dataDir = getTestDataDir();
      String dataFullPath = dataDir + File.separator + fileName;
      fullPathFileName = FileName.getJointTypeAssignmentFullPath(fileName);

      FileUtilAxi.copy(dataFullPath, fullPathFileName);
      _reader.read(fileName);
      FileUtilAxi.delete(fullPathFileName);

      // read a file with some syntax errors
      fileName = "badJointTypeAssignment.config";
      dataDir = getTestDataDir();
      dataFullPath = dataDir + File.separator + fileName;
      fullPathFileName = FileName.getJointTypeAssignmentFullPath(fileName);

      FileUtilAxi.copy(dataFullPath, fullPathFileName);
      try
      {
        _reader.read(fileName);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException ex)
      {
        // do nothing
      }
      FileUtilAxi.delete(fullPathFileName);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (FileUtilAxi.exists(fullPathFileName))
          FileUtilAxi.delete(fullPathFileName);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }
}
