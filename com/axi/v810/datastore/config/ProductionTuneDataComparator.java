package com.axi.v810.datastore.config;

import com.axi.util.AlphaNumericComparator;
import com.axi.util.Assert;
import com.axi.v810.util.ComponentTypeComparatorEnum;
import com.axi.v810.business.panelDesc.ComponentType;
import java.util.*;
import com.axi.util.CaseInsensitiveStringComparator;

/**
 * @author Andy Mechtenberg
 */
public class ProductionTuneDataComparator implements Comparator<ProductionTuneData>
{
  private CaseInsensitiveStringComparator _caseInsensitiveStringComparator = null;
  private boolean _ascending;
  private ProductionTuneDataComparatorEnum _comparingAttribute;

  /**
   * @author George Booth
   */
  public ProductionTuneDataComparator(boolean ascending, ProductionTuneDataComparatorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _caseInsensitiveStringComparator = new CaseInsensitiveStringComparator();
  }

  /**
   * @author George Booth
   */
  public int compare(ProductionTuneData lhs, ProductionTuneData rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);
    String lhsString;
    String rhsString;
    Integer lhsInteger;
    Integer rhsInteger;


    if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.PROJECT_NAME))
    {
      lhsString = lhs.getProjectName();
      rhsString = rhs.getProjectName();
      if (_ascending)
        return _caseInsensitiveStringComparator.compare(lhsString, rhsString);
      else
        return _caseInsensitiveStringComparator.compare(rhsString, lhsString);
    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.IMAGE_DESTINATION))
    {
      lhsString = lhs.getImageDestination();
      rhsString = rhs.getImageDestination();
      if (_ascending)
        return _caseInsensitiveStringComparator.compare(lhsString, rhsString);
      else
        return _caseInsensitiveStringComparator.compare(rhsString, lhsString);

    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.RESULTS_DESTINATION))
    {
      lhsString = lhs.getResultsDestination();
      rhsString = rhs.getResultsDestination();
      if (_ascending)
        return _caseInsensitiveStringComparator.compare(lhsString, rhsString);
      else
        return _caseInsensitiveStringComparator.compare(rhsString, lhsString);
    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.CURRENT_COUNT))
    {
      lhsInteger = lhs.getCurrentCount();
      rhsInteger = rhs.getCurrentCount();
      if (_ascending)
          return lhsInteger.compareTo(rhsInteger);
        else
          return rhsInteger.compareTo(lhsInteger);

    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.RE_ENABLE_THRESHOLD))
    {
      lhsInteger = lhs.getReEnableThreshold();
      rhsInteger = rhs.getReEnableThreshold();
      if (_ascending)
          return lhsInteger.compareTo(rhsInteger);
        else
          return rhsInteger.compareTo(lhsInteger);

    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.RE_ENABLE_COUNT))
    {
      lhsInteger = lhs.getReEnableCount();
      rhsInteger = rhs.getReEnableCount();
      if (_ascending)
          return lhsInteger.compareTo(rhsInteger);
        else
          return rhsInteger.compareTo(lhsInteger);

    }
    else if (_comparingAttribute.equals(ProductionTuneDataComparatorEnum.NOTIFICATION_EMAIL))
    {
      lhsString = lhs.getNotificationEmailAddress();
      rhsString = rhs.getNotificationEmailAddress();
      if (_ascending)
        return _caseInsensitiveStringComparator.compare(lhsString, rhsString);
      else
        return _caseInsensitiveStringComparator.compare(rhsString, lhsString);

    }
    else
      return 0;
  }
}
