package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;

/**
 * The class reads in a List of SingleInitialThreshold for the InitialThreshold definitions file.
 *
 * An example of the file syntax is:
 *
 *# The first line specifies the units used
 *#
 *# [mils][millimeters]
 *#
 *# Each line specified a value different from the default
 *#
 *# SYNTAX for each line:
 *# jointTypeEnum algorithmSettingName tunedValueType tunedValue
 *
 * @author Andy Mechtenberg
 */
public class InitialThresholdsReader
{
  private static InitialThresholdsReader _instance;
  private MathUtilEnum _units = null;
  private List<SingleInitialThreshold> _initialThresholds;
  private EnumStringLookup _enumStringLookup;
  private Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");

  // pattern jointTypeEnum algorithm algorithmSettingName tunedValueType tunedValue
  private Pattern _pattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([a-zA-Z]+)\\s+\"(.+)\"$");

  /**
   * @author Bill Darbie
   */
  public static synchronized InitialThresholdsReader getInstance()
  {
    if (_instance == null)
      _instance = new InitialThresholdsReader();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private InitialThresholdsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * Parse in the files contents for later use.
   * @author Andy Mechtenberg
   */
  public List<SingleInitialThreshold> read(String fileNameFullPath) throws DatastoreException
  {
    Assert.expect(fileNameFullPath != null);

    _initialThresholds = new ArrayList<SingleInitialThreshold>();

    boolean unitsRead = false;
    _units = null;

//    if (UnitTest.unitTesting())
//      fileNameFullPath = fileName;
//    else
//      fileNameFullPath = FileName.getInitialThresholdsFullPath(fileName);

    FileReader fr = null;
    try
    {
      fr = new FileReader(fileNameFullPath);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(fileNameFullPath);
      ex.initCause(fnfe);
      throw ex;
    }

    LineNumberReader is = new LineNumberReader(fr);

    try
    {
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(fileNameFullPath);
          ex.initCause(ioe);
          throw ex;
        }

        if (line == null)
          continue;

        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
          continue;
        if (unitsRead == false)
        {
          line = line.replaceAll("\\\\#", "#");
          String units = line;
          // supported units are ONLY "mils" and "millimeters"
          if (units.equalsIgnoreCase("mils") || (units.equalsIgnoreCase("millimeters")))
          {
            _units = _enumStringLookup.getMathUtilEnum(units);
            unitsRead = true;
            continue;
          }
          else
            throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());
        }
        // pattern jointTypeEnum algorithmSettingName tunedValueType tunedValue
        matcher = _pattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());

        String jointTypeEnumStr = matcher.group(1);
        String algorithmName = matcher.group(2);
        String algorithmSettingName = matcher.group(3);
        String tunedValueType = matcher.group(4);
        String tunedValueStr = matcher.group(5);

        JointTypeEnum jointTypeEnum = _enumStringLookup.getJointTypeEnum(jointTypeEnumStr);
        AlgorithmEnum algorithmEnum = _enumStringLookup.getAlgorithmEnum(algorithmName);
        AlgorithmSettingEnum algorithmSettingEnum = _enumStringLookup.getAlgorithmSettingEnum(algorithmSettingName);
        Serializable value = null;
        try
        {
          value = getSerializedValue(tunedValueType, tunedValueStr);
        }
        catch (BadFormatException bfe)
        {
          throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());
        }

//        System.out.println("JointTypeEnum: " + jointTypeEnumStr);
//        System.out.println("Algorithm    : " + algorithmName);
//        System.out.println("Threshold    : " + algorithmSettingName);
//        System.out.println("Type         : " + tunedValueType);
//        System.out.println("Value        : " + tunedValueStr);
//        System.out.println("JointTypeEnum: " + jointTypeEnum);
//        System.out.println("Algorithm    : " + algorithmEnum);
//        System.out.println("Threshold    : " + algorithmSettingEnum);
//        System.out.println("Value        : " + value);

        InspectionFamily family = InspectionFamily.getInspectionFamily(jointTypeEnum);
        Algorithm algorithm = family.getAlgorithm(family.getInspectionFamilyEnum(), algorithmEnum);
        AlgorithmSetting algorithmSetting = algorithm.getAlgorithmSetting(algorithmSettingEnum);

        SingleInitialThreshold sit = new SingleInitialThreshold(jointTypeEnum, algorithm, algorithmSetting, value);
        _initialThresholds.add(sit);
      }
      while (line != null);

      if (unitsRead == false)
        throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());
    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
    Assert.expect(_initialThresholds != null);
    return _initialThresholds;
  }

  /**
   * @author Andy Mechtenberg
   */
   public MathUtilEnum getUnits()
   {
     Assert.expect(_units != null);
     return _units;
   }

  /**
   * @author Andy Mechtenberg
   */
  private Serializable getSerializedValue(String tunedValueType, String tunedValueStr) throws BadFormatException, DatastoreException
  {
    Assert.expect(tunedValueType != null);
    Assert.expect(tunedValueStr != null);

    Serializable value = null;

    if (tunedValueType.equals("int"))
      value = StringUtil.convertStringToInt(tunedValueStr);
    else if (tunedValueType.equals("string"))
      value = (String)tunedValueStr;
    else if (tunedValueType.equals("float"))
      value = StringUtil.convertStringToFloat(tunedValueStr);
    else
      throw new FileCorruptDatastoreException("", 1);

    return value;
  }
}
