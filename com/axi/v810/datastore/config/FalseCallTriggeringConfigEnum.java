package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.v810.datastore.*;
import java.util.*;
import com.axi.util.*;

/**
 * This class contains all the barcode reader settings.
 * @author Bob Balliew
 */
public class FalseCallTriggeringConfigEnum extends ConfigEnum implements Serializable
{
  private static int _index = -1;
  private static final String _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG = FileName.getFalseCallTriggeringConfigFullPath();

  private static List<ConfigEnum> _falseCallTriggeringConfigEnums = new ArrayList<ConfigEnum>();
  
  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  private static final int _DEFECT_PIN_NUMBER_TYPE = 0;
  private static final int _DEFECT_PPM_PIN_TYPE = 1;

  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the barcode reader config file
   * @param filename String which holds the barcode reader configuration filename.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Bob Balliew
   */
  private FalseCallTriggeringConfigEnum(int id,
                                    String key,
                                    String filename,
                                    TypeEnum type,
                                    Object[] valid)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _falseCallTriggeringConfigEnums.add(this);
  }

   public static final FalseCallTriggeringConfigEnum CONSECUTIVE_COUNT =
     new FalseCallTriggeringConfigEnum(++_index,
                             "consecutiveCount",
                             _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                             TypeEnum.INTEGER,
                             new Object[]{1, Integer.MAX_VALUE});
  
  public static final FalseCallTriggeringConfigEnum TOTAL_INSPECTION =
     new FalseCallTriggeringConfigEnum(++_index,
                             "totalInspection",
                             _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                             TypeEnum.INTEGER,
                             new Object[]{1, Integer.MAX_VALUE});
  
  public static final FalseCallTriggeringConfigEnum TOTAL_SAMPLE_TRIGGERING =
     new FalseCallTriggeringConfigEnum(++_index,
                             "totalSampleTriggering",
                             _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                             TypeEnum.INTEGER,
                             new Object[]{1, Integer.MAX_VALUE});
  
  public static final FalseCallTriggeringConfigEnum MAXIMUM_ERROR_TRIGGER =
     new FalseCallTriggeringConfigEnum(++_index,
                             "maximumErrorTrigger",
                             _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                             TypeEnum.INTEGER,
                             new Object[]{1, Integer.MAX_VALUE});

  public static final FalseCallTriggeringConfigEnum DYNAMIC_CALL_NUMBER =
      new FalseCallTriggeringConfigEnum(++_index,
                                  "dynamicCallNumber",
                                  _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                                  TypeEnum.LIST_OF_STRINGS,
                                  null
                                  );

  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  public static final FalseCallTriggeringConfigEnum DEFECT_THRESHOLD_TYPE =
      new FalseCallTriggeringConfigEnum(++_index,
                                  "defectThresholdType",
                                  _DEFAULT_FALSE_CALL_TRIGGERING_CONFIG,
                                  TypeEnum.INTEGER,
                                  null
                                  );
  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getBarCodeReaderConfigEnums()
  {
    Assert.expect(_falseCallTriggeringConfigEnums != null);

    return _falseCallTriggeringConfigEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_falseCallTriggeringConfigEnums != null);

    return _falseCallTriggeringConfigEnums;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getDefectivePinsNumberThresholdType()
  {
    return _DEFECT_PIN_NUMBER_TYPE;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getDefectivePPMPinThresholdType()
  {
    return _DEFECT_PPM_PIN_TYPE;
  }
}

