package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.business.panelSettings.SingleInitialThreshold;

/**
 * @author Andy Mechtenberg
 */
public class Test_InitialThresholdsWriter extends UnitTest
{
  private InitialThresholdsWriter _writer;
  private InitialThresholdsReader _reader;

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_InitialThresholdsWriter());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _writer = InitialThresholdsWriter.getInstance();

      // try a good file
      String fileName = getTestDataDir() + File.separator + "initialThresholdsWriter.config";      

      // create some data for the writer to write
      InspectionFamily inspectionFamily = InspectionFamily.getInstance(JointTypeEnum.CAPACITOR);

      Algorithm measAlgo = inspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
      AlgorithmSetting setting = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);

      SingleInitialThreshold sit = new SingleInitialThreshold(JointTypeEnum.CAPACITOR,
              measAlgo,
              setting,
              "5.6");
      
      // create a new one to test the compare method
      AlgorithmSetting setting2 = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
      SingleInitialThreshold sit2 = new SingleInitialThreshold(JointTypeEnum.CAPACITOR,
              measAlgo,
              setting2,
              "5.7");
      
      List<SingleInitialThreshold> dataCollection = new ArrayList<SingleInitialThreshold>();
      dataCollection.add(sit);
      dataCollection.add(sit2);

      _writer.write(fileName, MathUtilEnum.MILS, dataCollection);

      // now check that it was written out correctly by reading it back in
      _reader = InitialThresholdsReader.getInstance();      
      Collection readInDataCollection = _reader.read(fileName);

      Expect.expect(_reader.getUnits().equals(MathUtilEnum.MILS));
      Expect.expect(readInDataCollection.size() == dataCollection.size());

      Iterator origIt = dataCollection.iterator();
      Iterator readIt = readInDataCollection.iterator();
      while (readIt.hasNext())
      {
        SingleInitialThreshold origData = (SingleInitialThreshold)origIt.next();
        SingleInitialThreshold readInData = (SingleInitialThreshold)readIt.next();

        //System.out.println(origData.getOriginalExpression());
        //System.out.println(readInData.getOriginalExpression());
        Expect.expect(origData.getJointTypeEnum().equals(readInData.getJointTypeEnum()));
        Expect.expect(origData.getAlgorithm().equals(readInData.getAlgorithm()));
        Expect.expect(origData.getAlgorithmSetting().equals(readInData.getAlgorithmSetting()));
        Expect.expect(origData.getValue().equals(readInData.getValue()));
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
