package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ConfigFileBinaryReader
{
  private Map<Integer, Object> _keyToValueMap = new HashMap<Integer, Object>();
  private String _configFile = null;

  /**
  * Given a configuration file name, return a Map of the key value pairs
  * in that file.
  *
  * @author Bill Darbie
  * @author Ying-Huan.Chu
  */
  public void read(String configFile, List<ConfigEnum> configEnums) throws DatastoreException
  {
    Assert.expect(configFile != null);
    Assert.expect(configEnums != null);

    // clear the current map
    _keyToValueMap.clear();

    // if the configFile has a : in it then it has the entire path - this is used for selftest only
    if (configFile.indexOf(":") != -1)
      _configFile = configFile;
    else
      _configFile = Directory.getConfigDir() + File.separator + configFile;

    _configFile = _configFile + FileName.getBinaryExtension();

    _keyToValueMap = (HashMap<Integer, Object>)FileUtilAxi.loadObjectFromSerializedFile(_configFile);
    
    Map<Integer, ConfigEnum> configEnumIdToConfigEnumMap = new HashMap<Integer, ConfigEnum>();
    for (ConfigEnum configEnum : configEnums)
    {
      configEnumIdToConfigEnumMap.put(configEnum.getId(), configEnum);
    }
    // Ying-Huan.Chu [XCR1661]
    // throws Datastore Exception when the number of ConfigEnum found is different from expected.
    if (configEnumIdToConfigEnumMap.size() != _keyToValueMap.size())
    {
      throw new DatastoreException(_configFile + " has incorrect number of ConfigEnum." + 
                                  " Expected number of ConfigEnum = " + configEnumIdToConfigEnumMap.size() + 
                                  " Number of ConfigEnum found = " + _keyToValueMap.size());
    }
    for (Map.Entry<Integer, Object> entry : _keyToValueMap.entrySet())
    {
      int enumId = entry.getKey();
      Object value = entry.getValue();
      ConfigEnum configEnum = configEnumIdToConfigEnumMap.get(enumId);
      // Ying-Huan.Chu [XCR1661]
      //Assert.expect(configEnum != null);
      if (configEnum != null)
      {
        configEnum.setValue(value);
      }
    }
  }
  
    /**
  * Given a configuration file name, return a Map of the key value pairs
  * in that file.
  *
  * @author Jack Hwee
  */
  public Map<ConfigEnum, Integer> readPreviousConfig(String calibFile, List<ConfigEnum> configEnums) throws DatastoreException
  {
    Assert.expect(calibFile != null);
    Assert.expect(configEnums != null);

    // clear the current map
    _keyToValueMap.clear();

    _keyToValueMap = (HashMap<Integer, Object>)FileUtilAxi.loadObjectFromSerializedFile(calibFile);

    Map<Integer, ConfigEnum> configEnumIdToConfigEnumMap = new HashMap<Integer, ConfigEnum>();
    Map<ConfigEnum, Integer> keyValueMap = new HashMap<ConfigEnum, Integer>();
    
    for (ConfigEnum configEnum : configEnums)
    {
      configEnumIdToConfigEnumMap.put(configEnum.getId(), configEnum);
    }
    
    for (Map.Entry<Integer, ConfigEnum> entry : configEnumIdToConfigEnumMap.entrySet())
    {
      int enumId = entry.getKey();
      Object value = _keyToValueMap.get(enumId);

      if (value != null)
      {
		// XCR-2577 Software Assert when upgrade from 5.3a to 5.6 - Cheah Lee Herng 02 Mar 2015
        if (value instanceof Long)
        {
          value = entry.getValue().getDefaultValue();
        }
        keyValueMap.put(entry.getValue(), (int) value);
      }
    }
 
    return keyValueMap;
  }
}


