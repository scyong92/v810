package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.v810.datastore.*;

/**
 * Given the name of a configuration file this class will return a mapping of key value pairs
 * from that file.  Note that this class knows how to read old config files.  It can read .cfg
 * files that have headers of the format [HEADING] or no headers.  If the file does have
 * headers all keys under that heading will begin with the header plus and underscore.
 * The supported formats are:
 *
 * <pre>
 * Preferred file format (all new config files should be in this format):
 * key = value
 *
 * Legacy format 1 (that should go away over time):
 * "@key value"  (ignore the quotes - they are here because javadoc treats "@" as a special character"
 *
 * Legacy format 2 (that should go away over time):
 * [header]
 * key = value
 * </pre>
 *
 * @author Bill Darbie
 */
public class ConfigFileTextReader
{
  private Map<String, String> _keyValueMapping = new HashMap<String, String>();
  private String _configFile = null;
  // key = value (# is a comment)
  private Pattern _keyValuePattern = Pattern.compile("^\\s*([^#=\\s]+)\\s*=\\s*((\\\\#|[^#=])*)([#].*)?$");
  // comment line
  private Pattern _commentPattern = Pattern.compile("^\\s*[#]");
  // empty line
  private Pattern _blankPattern = Pattern.compile("^\\s*$");

  /**
  * Given a configuration file name, return a Map of the key value pairs
  * in that file.
  *
  * @param configFile is the file name of the config file.  It is assumed to exist
  *         in the AT5DX_HOME/calib directory.
  * @return a Map of the key value pairs from the configFile specified.
  * @author Bill Darbie
  */
  public Map<String, String> getKeyValueMapping(String configFile) throws DatastoreException
  {
    // clear the current map
    _keyValueMapping.clear();

    // if the configFile has a : in it then it has the entire path - this is used for selftest only
    if (configFile.indexOf(":") != -1)
      _configFile = configFile;
    else
      _configFile = Directory.getConfigDir() + File.separator + configFile;

    parseFile();

    // return a copy of the map
    return new HashMap<String, String>(_keyValueMapping);
  }

  /**
  * Parse the file.
  * @author Bill Darbie
  */
  private void parseFile() throws DatastoreException
  {
    String keyPrefix = "";
    String line = null;
    int lineNum = 0;

    FileReader fr = null;
    try
    {
      fr = new FileReader(_configFile);
    }
    catch(FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_configFile);
      dex.initCause(fnfe);
      throw dex;
    }
    BufferedReader is = new BufferedReader(fr);

    try
    {
      do
      {
        ++lineNum;
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotReadDatastoreException(_configFile);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        // check to see if this line is blank
        Matcher blankMatcher = _blankPattern.matcher(line);
        if (blankMatcher.find())
          continue;

        // check to see if this is a comment line first
        Matcher commentMatcher = _commentPattern.matcher(line);
        if (commentMatcher.find())
          continue;

        // it was not a comment, check to see if it is a key=value line
        Matcher keyValueMatcher = _keyValuePattern.matcher(line);
        if (keyValueMatcher.matches())
        {
          String keyStr = keyValueMatcher.group(1);
          String valueStr = keyValueMatcher.group(2);
          // get rid of any leading or trailing spaces
          valueStr = valueStr.trim();
          // change any \# to #
          valueStr = valueStr.replaceAll("\\\\#", "#");

          if (keyPrefix.equals("") == false)
            keyStr = keyPrefix + "_" + keyStr;

          // build up the key value mapping
          if (_keyValueMapping.put(keyStr, valueStr) != null)
          {
            throw new ConfigFileDuplicateKeyDatastoreException(_configFile, lineNum);
          }
        }
        else
        {
          // it was not a comment, or a header, or a key=value line, so give an error
          throw new ConfigFileCorruptDatastoreException(_configFile, lineNum, line);
        }
      }
      while (line != null);
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(Exception ex)
        {
          // do nothing
        }
      }
    }
  }
}


