package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;

/**
 * This class contains all the software settings for the test system.
 * Do NOT put any hardware configuration or calibration settings here!
 *
 * @author Bill Darbie
 */
public class SoftwareConfigEnum extends ConfigEnum implements Serializable
{
  private static int _index = -1;
  private static final String _SOFTWARE_CONFIG = FileName.getSoftwareConfigFullPath();

  private static List<ConfigEnum> _softwareConfigEnums = new ArrayList<ConfigEnum>();

  // Note: this value should never be changed because it is read and written out to disk.
  private static final String _PANEL_HANDLER_PANEL_LOADING_PASS_BACK_MODE_NAME = "passBack";
  private static final String _PANEL_HANDLER_PANEL_FLOW_FLOW_THROUGH_MODE_NAME = "flowThrough";
  private static final String _PANEL_HANDLER_PANEL_FLOW_DIRECTION_LEFT_TO_RIGHT_MODE_NAME = "leftToRight";
  private static final String _PANEL_HANDLER_PANEL_FLOW_DIRECTION_RIGHT_TO_LEFT_MODE_NAME = "rightToLeft";
  private static final String _PANEL_SAMPLING_MODE_NO_SAMPLING = "noSampling";
  private static final String _PANEL_SAMPLING_MODE_TEST_EVERY_NTH_PANEL = "testEveryNthPanel";
  private static final String _PANEL_SAMPLING_MODE_SKIP_EVERY_NTH_PANEL = "skipEveryNthPanel";

  private static final int _IMAGE_TYPE_PNG = 0;
  private static final int _IMAGE_TYPE_TIFF = 1;
  private static final int _IMAGE_TYPE_JPG = 2;
  
  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the config file
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Bill Darbie
   */
  protected SoftwareConfigEnum(int id,
                               String key,
                               String filename,
                               TypeEnum type,
                               Object[] valid)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _softwareConfigEnums.add(this);
  }

  public static final SoftwareConfigEnum LANGUAGE =
      new SoftwareConfigEnum(++_index,
                             "language",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum ONLINE_WORKSTATION =
      new SoftwareConfigEnum(++_index,
                             "onlineWorkstation",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum PROJECT_DIR =
      new SoftwareConfigEnum(++_index,
                             "projectsDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //Kee Chin Seong - Verification Images directory setup
  public static final SoftwareConfigEnum VERIFICATION_DIR = 
      new SoftwareConfigEnum(++_index,
                             "verificationImagesDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
          
  public static final SoftwareConfigEnum NDF_LEGACY_DIR =
      new SoftwareConfigEnum(++_index,
                             "ndfDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  public static final SoftwareConfigEnum CAD_DIR =
      new SoftwareConfigEnum(++_index,
                             "cadDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  public static final SoftwareConfigEnum RESULTS_DIR =
      new SoftwareConfigEnum(++_index,
                             "resultsDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum OFFLINE_IMAGE_SETS_DIRS =
      new SoftwareConfigEnum(++_index,
                             "offlineImageSetsDirs",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LIST_OF_STRINGS,
                             null);

  public static final SoftwareConfigEnum TEMP_DIR =
      new SoftwareConfigEnum(++_index,
                             "tempDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum RMI_PORT_NUMBER =
      new SoftwareConfigEnum(++_index,
                             "rmiPort",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MESSAGE_SOCKET_PORT_NUMBER =
      new SoftwareConfigEnum(++_index,
                             "messageSocketPort",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CUSTOMER_COMPANY_NAME =
      new SoftwareConfigEnum(++_index,
                             "customerCompanyName",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum CUSTOMER_NAME =
      new SoftwareConfigEnum(++_index,
                             "customerName",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum CUSTOMER_EMAIL_ADDRESS =
      new SoftwareConfigEnum(++_index,
                             "customerEmailAddress",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum CUSTOMER_PHONE_NUMBER =
      new SoftwareConfigEnum(++_index,
                             "customerPhoneNumber",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum SMTP_SERVER =
      new SoftwareConfigEnum(++_index,
                             "smtpServer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum SPT_EMAIL_ADDRESS =
      new SoftwareConfigEnum(++_index,
                             "sptEmailAddress",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum HARDWARE_SIMULATION =
      new SoftwareConfigEnum(++_index,
                             "hardwareSimulation",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum INSTALLATION_DATE =
      new SoftwareConfigEnum(++_index,
                             "installationDate",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum SHOW_STAGE_MONITOR_GUI =
      new SoftwareConfigEnum(++_index,
                             "showStageMonitorGUI",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum DEVELOPER_DEBUG_MODE =
      new SoftwareConfigEnum(++_index,
                             "developerDebugMode",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum PROJECT_DATABASE_PATH =
      new SoftwareConfigEnum(++_index,
                             "projectDatabasePath",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum OFFLINE_WORKSTATION_PATH =
      new SoftwareConfigEnum(++_index,
                             "offlineWorkstationPath",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum ONLINE_WORKSTATION_PATH =
      new SoftwareConfigEnum(++_index,
                             "onlineWorkstationPath",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum AUTOMATICALLY_PULL_LATEST_PROJECT_VERSION_ON_LOAD =
      new SoftwareConfigEnum(++_index,
                             "automaticallyPullLastestProjectVersionOnLoad",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum PANEL_HANDLER_LOADING_MODE =
      new SoftwareConfigEnum(++_index,
                             "panelHandlerLoadingMode",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[]
                             {getPanelHandlerPanelLoadingFlowThroughModeName(), getPanelHandlerPanelLoadingPassBackModeName()});

  public static final SoftwareConfigEnum PANEL_HANDLER_PANEL_FLOW_DIRECTION =
      new SoftwareConfigEnum(++_index,
                             "panelHandlerPanelFlowDirection",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[]
                             {getPanelHandlerPanelFlowDirectionLeftToRightModeName(), getPanelHandlerPanelFlowDirectionRightToLeftModeName()});

  public static final SoftwareConfigEnum PANEL_HANDLER_SMEMA_ENABLE =
      new SoftwareConfigEnum(++_index,
                             "panelHandlerSmemaEnable",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum PANEL_HANDLER_IGNORE_PIP_ENABLE =
      new SoftwareConfigEnum(++_index,
                             "panelHandlerIgnorePIPEnable",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE =
      new SoftwareConfigEnum(++_index,
                             "panelHandlerCheckSmemaSensorSignalDuringUnloadingEnable",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum LIGHT_STACK_BUZZER_ENABLE =
    new SoftwareConfigEnum(++_index,
                           "lightStackBuzzerEnable",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum XOUT_DETECTION_ENABLE =
      new SoftwareConfigEnum(++_index,
                             "xoutDetectionEnable",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

    public static final SoftwareConfigEnum XOUT_DETECTION_METHOD =
      new SoftwareConfigEnum(++_index,
                             "xoutDetectionMethod",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[]
                             {XoutDetectionMethodEnum.ALIGNMENT_FAILURE.toString(), XoutDetectionMethodEnum.DEFECT_THRESHOLD.toString()});

  public static final SoftwareConfigEnum XOUT_DETECTION_THRESHOLD =
    new SoftwareConfigEnum(++_index,
                           "xoutDetectionFailureThreshold",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           new Object[]{1, 100});
  
  public static final SoftwareConfigEnum CONFIRMATION_AND_ADJUSTMENT_MODE =
    new SoftwareConfigEnum(++_index,
                           "confirmationAndAdjustmentMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[]{ConfirmationAndAdjustmentModeEnum.STANDARD_MODE.toString(), ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE.toString()});
  
  public static final SoftwareConfigEnum PROMPT_SAVING_RECIPE_AFTER_LEARNING =
      new SoftwareConfigEnum(++_index,
                             "promptSavingRecipeAfterLearning",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  
  //Prompt warning message if recipe contains high mag CR-hsia-fen.tan
    public static final SoftwareConfigEnum PROMPT_WARNING_MESSAGE_IF_RECIPE_CONTAIN__HIGH =
      new SoftwareConfigEnum(++_index,
                             "promptWarningMsgIfRecipeContainHighMag",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
    
  //Project's history log CR - hsia-fen.tan
    public static final SoftwareConfigEnum CREATE_HISTORY_LOG =
      new SoftwareConfigEnum(++_index,
                             "createRecipeHistoryLog",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
      
      
  public static final SoftwareConfigEnum OFFLINE_RECONSTRUCTION =
      new SoftwareConfigEnum(++_index,
                             "offlineReconstruction",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum MAX_FOCUS_REGION_SIZE_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maxFocusRegionSizeInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum FOCUS_REGION_GRANULARITY_IN_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "focusRegionGranularityInPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED =
      new SoftwareConfigEnum(++_index,
                             "twoPassVerificationSurfaceModelingEnabled",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             booleanRange());

  public static final SoftwareConfigEnum TWO_PASS_VERIFICATION_SURFACE_MODELING_DEBUG_ENABLED =
      new SoftwareConfigEnum(++_index,
                             "twoPassVerificationSurfaceModelingDebugEnabled",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             booleanRange());
  
  public static final SoftwareConfigEnum INITIAL_RECIPE_SETTING_FILEDIR=
      new SoftwareConfigEnum(++_index,
                             "initialRecipeSettingFileDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  public static final SoftwareConfigEnum INITIAL_RECIPE_SETTING_FILENAME=
      new SoftwareConfigEnum(++_index,
                             "initialRecipeSettingFileName",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  public static final SoftwareConfigEnum INITIAL_THRESHOLDS_FILEDIR =
      new SoftwareConfigEnum(++_index,
                             "initialThresholdsFileDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum INITIAL_THRESHOLDS_FILENAME =
      new SoftwareConfigEnum(++_index,
                             "initialThresholdsFileName",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum JOINT_TYPE_ASSIGNMENT_FILENAME =
      new SoftwareConfigEnum(++_index,
                             "jointTypeAssignmentFileName",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum NUM_IMAGE_ANALYSIS_THREADS =
      new SoftwareConfigEnum(++_index,
                             "numImageAnalysisThreads",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum USE_SERIAL_NUMBER_TO_FIND_PROJECT =
      new SoftwareConfigEnum(++_index,
                             "useSerialNumberToFindProject",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum USE_FIRST_SERIAL_NUMBER_FOR_PROJECT_LOOKUP_ONLY =
      new SoftwareConfigEnum(++_index,
                             "useSerialNumberForProjectLookupOnly",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum USE_UNIQUE_SERIAL_NUMBER_PER_BOARD =
      new SoftwareConfigEnum(++_index,
                             "useUniqueSerialNumberPerBoard",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum CHECK_FOR_DUPLICATE_SERIAL_NUMBERS =
      new SoftwareConfigEnum(++_index,
                             "checkForDuplicateSerialNumbers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum VALIDATE_BOARD_SERIAL_NUMBERS =
      new SoftwareConfigEnum(++_index,
                             "validateBoardSerialNumbers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[] {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum SKIP_SERIAL_NUMBERS =
    new SoftwareConfigEnum(++_index,
                           "skipSerialNumbers",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

public static final SoftwareConfigEnum TEST_EXEC_PRE_SELECT_PROJECT =
    new SoftwareConfigEnum(++_index,
                           "testExecPreSelectProject",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                             new Object[] {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum ALLOW_ONLY_NEW_SERIAL_NUMBERS_WHEN_CAD_MATCH_FAILS =
    new SoftwareConfigEnum(++_index,
                           "allowOnlyNewSerialNumbersWhenCADMatchFails",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

public static final SoftwareConfigEnum TEST_EXEC_DISPLAY_LAST_CAD_IMAGE_IN_QUEUE =
    new SoftwareConfigEnum(++_index,
                           "testExecDisplayLastCADImageInQueue",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                             new Object[] {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum BARCODE_SCANNER_READ_ERROR_CHECK =
    new SoftwareConfigEnum(++_index,
                           "barcodeScannerReadErrorCheck",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

  //Serial Number Mapping
  public static final SoftwareConfigEnum ENABLE_SERIAL_NUMBER_MAPPING =
    new SoftwareConfigEnum(++_index,
                           "enableSerialNumberMapping",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum PANEL_SAMPLING_MODE =
      new SoftwareConfigEnum(++_index,
                             "panelSamplingMode",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[] {getPanelSamplingModeNoSampling(),
                                           getPanelSamplingModeSkipEveryNthPanel(),
                                           getPanelSamplingModeTestEveryNthPanel()});

  public static final SoftwareConfigEnum SKIP_EVERY_NTH_PANEL_FOR_SAMPLING =
      new SoftwareConfigEnum(++_index,
                             "skipEveryNthPanelForSampling",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             new Object[]{1, Integer.MAX_VALUE});

  public static final SoftwareConfigEnum TEST_EVERY_NTH_PANEL_FOR_SAMPLING =
    new SoftwareConfigEnum(++_index,
                           "testEveryNthPanelForSampling",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           new Object[]{1, Integer.MAX_VALUE});

  public static final SoftwareConfigEnum ENABLE_PRE_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "enablePreInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum PRE_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "preInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final SoftwareConfigEnum ENABLE_POST_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "enablePostInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum POST_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "postInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.STRING,
                            null);

  public static final SoftwareConfigEnum ENABLE_PRE_PANEL_SERIAL_NUMBER_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "enablePrePanelSerialNumberInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum ENABLE_PRE_BOARD_SERIAL_NUMBER_INSPECTION_SCRIPT =
    new SoftwareConfigEnum(++_index,
                           "enablePreBoardSerialNumberInspectionScript",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});

  
  public static final SoftwareConfigEnum BARCODE_READER_AUTOMATIC_READER_ENABLED =
    new SoftwareConfigEnum(++_index,
                           "barcodeReaderAutomaticReaderEnabled",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum BARCODE_READER_CONFIGURATION_NAME_TO_USE =
      new SoftwareConfigEnum(++_index,
                             "barcodeReaderConfigurationNameToUse",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum BARCODE_READER_CONFIGURATION_NAME_TO_EDIT =
      new SoftwareConfigEnum(++_index,
                             "barcodeReaderConfigurationNameToEdit",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final SoftwareConfigEnum BARCODE_READER_PROTECTED_CONFIGURATION_NAMES =
      new SoftwareConfigEnum(++_index,
                             "barcodeReaderProtectedConfigurationNames",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //False Call Monitoring
  public static final SoftwareConfigEnum FALSE_CALL_MONITORING_ENABLED =
    new SoftwareConfigEnum(++_index,
                           "falseCallMonitoringEnabled",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum ENFORCE_ADMINISTRATOR_LOGIN =
    new SoftwareConfigEnum(++_index,
                           "enforceAdministratorLogin",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum AUTO_DELETE_PRODUCTION_RESULTS =
    new SoftwareConfigEnum(++_index,
                           "autoDeleteProductionResults",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum EJECT_PANEL_IF_ALIGNMENT_FAILS =
      new SoftwareConfigEnum(++_index,
                             "ejectPanelIfAlignmentFails",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum TEST_EXEC_DEMO_MODE =
      new SoftwareConfigEnum(++_index,
                             "testExecDemoMode",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum TEST_EXEC_BYPASS_MODE =
    new SoftwareConfigEnum(++_index,
                           "testExecBypassMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  //sheng chuan
  public static final SoftwareConfigEnum TEST_EXEC_BYPASS_WITH_SERIAL_MODE =
    new SoftwareConfigEnum(++_index,
                           "testExecBypassWithSerialMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum TEST_EXEC_BYPASS_WITH_RESULTS =
    new SoftwareConfigEnum(++_index,
                           "testExecBypassWithResults",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME =
    new SoftwareConfigEnum(++_index,
                           "alwaysDisplayVariationName",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum TEST_EXEC_NO_TEST_AS_HIGH_PRIORITY =
    new SoftwareConfigEnum(++_index,
                           "enableNoTestAsHighPriorityInVariation",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum SEMI_AUTOMATED_MODE =
    new SoftwareConfigEnum(++_index,
                           "semiAutomatedMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum AUTOMATICALLY_DELETE_RESULTS =
      new SoftwareConfigEnum(++_index,
                             "automaticallyDeleteResultFiles",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  
  //Project's history log CR-hsia-fen.tan
  public static final SoftwareConfigEnum AUTOMATICALLY_DELETE_HISTORY_FILES =
      new SoftwareConfigEnum(++_index,
                             "automaticallyDeleteHistoryFiles",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum KEEP_N_MOST_RECENT_HISTORY_FILE =
      new SoftwareConfigEnum(++_index,
                             "keepNMostRecentHistoryFile",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(0));

  public static final SoftwareConfigEnum KEEP_N_MOST_RECENT_REPAIR_FILE_RESULTS =
      new SoftwareConfigEnum(++_index,
                             "keepNMostRecentRepairFileResults",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(0));

  public static final SoftwareConfigEnum KEEP_N_MOST_RECENT_RESULT_FILES =
      new SoftwareConfigEnum(++_index,
                             "keepNMostRecentResults",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(0));

  public static final SoftwareConfigEnum GENERATE_AND_PROCESS_TEST_DEVELOPMENT_RESULTS =
      new SoftwareConfigEnum(++_index,
                             "generateAndProcessTestDevelopmentResults",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum ENABLE_RESULTS_PROCESSING =
      new SoftwareConfigEnum(++_index,
                             "enableResultsProcessing",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER =
      new SoftwareConfigEnum(++_index,
                             "enableCreateTimeStampFolderForFileTransfer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum ALIGN_ON_DARK_PADS_WITH_LIGHT_BACKGROUNDS =
      new SoftwareConfigEnum(++_index,
                             "alignOnDarkPadsWithLightBackgrounds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum VIRTUAL_LIVE_MODE =
      new SoftwareConfigEnum(++_index,
                             "virtualLiveMode",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_IMAGE_RECEIVER_BUFFER_SIZE_MEGEBYTES =
      new SoftwareConfigEnum(++_index,
                             "imageReconstructionProcessorImageReceiverBufferSizeMegaBytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(1));

  public static final SoftwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE =
      new SoftwareConfigEnum(++_index,
                             "imageReconstructionProcessorCommunicationsHandshake",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_PRIORITY_IMAGE_RECEIVER_BUFFER_SIZE_MEGEBYTES =
      new SoftwareConfigEnum(++_index,
                             "imageReconstructionProcessorPriorityImageReceiverBufferSizeMegaBytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(1));

  public static final SoftwareConfigEnum MAXIMUM_SPACE_FOR_IMAGES_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "maximumSpaceForImagesInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             lowerBound((long)100));

  public static final SoftwareConfigEnum IMAGE_CACHE_TO_DISK_THRESHOLD_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "imageCacheToDiskThresholdInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                            lowerBound((long)100));

  public static final SoftwareConfigEnum IMAGE_INITIAL_TUNING_LOAD_LIMIT_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "imageInitialTuningLoadLimitInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                            lowerBound(100));

  public static final SoftwareConfigEnum IMAGE_OFFLINE_INSPECTION_LOAD_LIMIT_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "imageOfflineInspectionLoadLimitInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                            lowerBound(100));

  public static final SoftwareConfigEnum IMAGE_PAUSE_IRP_THRESHOLD_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "imagePauseIRPThresholdInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(10));

  public static final SoftwareConfigEnum IMAGE_RESUME_IRP_THRESHOLD_IN_MEGABYTES =
      new SoftwareConfigEnum(++_index,
                             "imageResumeIRPThresholdInMegabytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(10));

  public static final SoftwareConfigEnum PANEL_ALIGNMENT_UNCERTAINTY_BORDER_IN_NANOMETERS =
    new SoftwareConfigEnum(++_index,
                           "panelAlignmentUncertaintyBorderInNanometers",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final SoftwareConfigEnum PANEL_ALIGNMENT_UNCERTAINTY_BORDER_AFTER_AUTOMATIC_ALIGNMENT_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "panelAlignmentUncertaintyBorderAfterAutomaticAlignmentInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MINIMUM_PERCENT_COVERAGE_OF_IMAGEABLE_AREA_FOR_ALIGNMENT_GROUPS =
      new SoftwareConfigEnum(++_index,
                             "minimumPercentCoverageOfImageableAreaForAlignmentGroups",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_BORDER_FOR_SINGLE_SIDED_ALIGNMENT_REGION_TEST_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumBorderForSingleSidedAlignmentRegionTestInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MINIMUM_ALIGNMENT_REGION_DENSITY_PERCENTAGE =
      new SoftwareConfigEnum(++_index,
                             "minimumAlignmentRegionDensityPercentage",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_ALIGNMENT_REGION_PERCENTAGE_OCCUPIED_BY_BGAS =
      new SoftwareConfigEnum(++_index,
                             "maximumAlignmentRegionPercentageOccupiedByBGAs",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "alignmentRegionAreaArrayProximityMarginInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_ALIGNMENT_FEATURE_BOUNDING_BOX_PERCENTAGE_OCCUPIED_BY_PROBLEMATIC_LEAD_FRAMES =
      new SoftwareConfigEnum(++_index,
                             "maximumAlignmentFeatureBoundingBoxPercentageOccupiedByProblematicLeadFrames",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_ALIGNMENT_BACKGROUND_GRAY_LEVEL_ERROR =
      new SoftwareConfigEnum(++_index,
                             "maximumAlignmentBackgroundGrayLevelError",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_ALIGNMENT_DELTA_GRAY_LEVEL_ERROR_FACTOR =
      new SoftwareConfigEnum(++_index,
                             "maximumAlignmentDeltaGrayLevelErrorFactor",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum LOG_DETAILED_ALIGNMENT_FAILURE_DATA =
      new SoftwareConfigEnum(++_index,
                             "logDetailedAlignmentFailureData",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum LIMIT_NUM_REPAIR_IMAGES_TO_SAVE =
      new SoftwareConfigEnum(++_index,
                             "limitNumRepairImagesToSave",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum NUM_REPAIR_IMAGES_TO_SAVE =
      new SoftwareConfigEnum(++_index,
                             "numRepairImagesToSave",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(0));

  public static final SoftwareConfigEnum ENABLE_LIGHTEST_IMAGE_TEST_FOR_SHORT_DEFECTS =
      new SoftwareConfigEnum(++_index,
                             "enableLightestImageTestForShortDefects",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum DISABLE_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP =
      new SoftwareConfigEnum(++_index,
                             "disableImageReconstructionProcessorsStartup",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
  public static final SoftwareConfigEnum TERMINATE_IMAGE_RECONSTRUCTION_PROCESSORS_WHEN_EXIT =
      new SoftwareConfigEnum(++_index,
                             "terminateImageReconstructionProcessorsWhenExit",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum FORCE_UPLOAD_FOR_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP =
    new SoftwareConfigEnum(++_index,
                           "forceUploadForImageReconstructionProcessorsStartup",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});

  /** @todo mdw - this is TEMP CODE to help Vyn with auto-focus debugging. */
  public static final SoftwareConfigEnum LOG_ALGORITHM_PROFILE_DATA =
      new SoftwareConfigEnum(++_index,
                             "logAlgorithmProfileData",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum USED_SAVED_IMAGES_FOR_AUTOMATIC_ALIGNMENT =
      new SoftwareConfigEnum(++_index,
                             "useSavedImagesForAutomaticAlignment",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "focusConfirmationStepSizeInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum FOCUS_CONFIRMATION_NUM_SLICES_ABOVE =
      new SoftwareConfigEnum(++_index,
                             "focusConfirmationNumSlicesAbove",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum FOCUS_CONFIRMATION_NUM_SLICES_BELOW =
      new SoftwareConfigEnum(++_index,
                             "focusConfirmationNumSlicesBelow",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum LOG_FOCUS_CONFIRMATION_DATA =
      new SoftwareConfigEnum(++_index,
                             "logFocusConfirmationData",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES =
      new SoftwareConfigEnum(++_index,
                             "saveFocusConfirmationRepairImages",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum SAVE_ALL_CONFIRMATION_IMAGES =
      new SoftwareConfigEnum(++_index,
                             "saveAllConfirmationImages",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});


  public static final SoftwareConfigEnum OVERRIDE_PANEL_EJECT_FOR_CALIBRATIONS =
      new SoftwareConfigEnum(++_index,
                             "overridePanelEjectForCalibrations",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC =
      new SoftwareConfigEnum(++_index,
                             "overridePanelEjectForTestExec",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum SIMULATE_PANEL_EJECT_FOR_TEST_EXEC =
      new SoftwareConfigEnum(++_index,
                             "simulatePanelEjectForTestExec",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum RUN_ADJUSTMENTS_AND_CONFIRM_WITH_PANEL_IN_SYSTEM =
      new SoftwareConfigEnum(++_index,
                             "runAdjustmentsAndConfirmationsWithPanelInSystem",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum ENABLE_FOCUS_CONFIRMATION =
      new SoftwareConfigEnum(++_index,
                             "enableFocusConfirmation",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum IMAGE_ACQUISITION_PROGRESS_MONITOR_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "imageAcquisitionProgressMonitorTimeOutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             lowerBound(0L));
  
  public static final SoftwareConfigEnum OPTICAL_IMAGE_ACQUISITION_PROGRESS_MONITOR_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalImageAcquisitionProgressMonitorTimeOutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             lowerBound(0L));

  public static final SoftwareConfigEnum GENERATE_MEASUREMENTS_XML_FILE =
      new SoftwareConfigEnum(++_index,
                             "generateMeasurementsXMLFile",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
                             
  public static final SoftwareConfigEnum DUMP_ZHEIGHT_IN_MEASUREMENTS_XML_FILE =
      new SoftwareConfigEnum(++_index,
                             "dumpZHeightInMeasurementsXMLFile",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum NUMBER_OF_SCAN_PASSES_IRPS_CAN_GET_AHEAD_BECAUSE_IMAGES_STORED_IN_CAMERA_BUFFERS =
      new SoftwareConfigEnum(++_index,
                             "numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum DROP_INSPECTION_IMAGES =
      new SoftwareConfigEnum(++_index,
                             "dropInspectionImages",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum FLAG_CALL_RATE_SPIKES =
      new SoftwareConfigEnum(++_index,
                             "flagCallRateSpikes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum MAX_ALLOWABLE_CALL_RATE_DEVIATION_FRACTION_FROM_BASELINE =
      new SoftwareConfigEnum(++_index,
                             "maxAllowableCallRateDeviationFractionFromBaseline",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum USE_SCAN_PASS_INTEGRATION =
      new SoftwareConfigEnum(++_index,
                             "useScanPassIntegration",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum ENABLE_STEP_SIZE_OPTIMIZATION =
    new SoftwareConfigEnum(++_index,
                           "enableStepSizeOptimization",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});

  //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
  public static final SoftwareConfigEnum ENABLE_FOCUS_OPTIMIZATION =
    new SoftwareConfigEnum(++_index,
                           "enableFocusOptimization",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});

  // Added by Lee Herng to control whether user wants large image view in inspection
  public static final SoftwareConfigEnum ENABLE_LARGE_IMAGE_VIEW =
    new SoftwareConfigEnum(++_index,
                           "enableLargeImageView",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});

  /* -------------------- Calibration Enums that need to be configurable -------------- */

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraGrayscaleCompleteAdjustmentTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_PIXEL_LIGHT_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraGrayscalePixelLightTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Time extensions for various Grayscale triggers.
  // Unload is assumed to be the best time to run grayscale, so no time extension
  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_UNLOAD_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForPartialCalAtUnloadInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_UNLOAD_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForCompleteCalAtUnloadInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Running Grayscale during a load may slow production, so add a delay of
  //5 minutes is added here (5 minutes * 60 seconds) = 300 seconds
  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_LOAD_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForPartialCalAtLoadInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_LOAD_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForCompleteCalAtLoadInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Running Grayscale here will force a panel to be ejected, so the timeout
  // for this case is the longest. (60 minutes * 60 seconds) = 3600 seconds
  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_IMAGE_ACQUISITION_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForPartialCalAtImageAcquisitionInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_IMAGE_ACQUISITION_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForCompleteCalAtImageAcquisitionInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_INSPECTION_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "timeExtensionForCompleteCalAtInspectionInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);


  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_PIXEL_DARK_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraGrayscalePixelDarkTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraGrayscaleLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  //Khaw Chek Hau - XCR2343: Grayscale error happened after Grayscale timeout and run Auto Start up at Fine Tuning
  public static final SoftwareConfigEnum CALIB_DELAY_IN_MILLISSECONDS =
      new SoftwareConfigEnum(++_index,
                             "calibDelayInMilliseconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_GRAYSCALE_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "cameraGrayscaleMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum XRAY_SPOT_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "xraySpotExecuteEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum XRAY_SPOT_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "xraySpotLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum XRAY_SPOT_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "xraySpotMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
//CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
  public static final SoftwareConfigEnum XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED =
      new SoftwareConfigEnum(++_index,
                             "xrayAutoShortTermShutdownEnabled",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

  public static final SoftwareConfigEnum XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES =
      new SoftwareConfigEnum(++_index,
                             "xrayAutoShortTermShutdownTimeoutMinutes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum V810_AUTO_START_UP_ENABLED =
      new SoftwareConfigEnum(++_index,
                             "machineAutoStartupEnabled",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

  public static final SoftwareConfigEnum V810_AUTO_START_UP_HOUR =
      new SoftwareConfigEnum(++_index,
                             "machineAutoStartupHour",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum V810_AUTO_START_UP_MINUTES =
      new SoftwareConfigEnum(++_index,
                             "machineAutoStartupMinute",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  

  public static final SoftwareConfigEnum HYSTERESIS_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "hysteresisExecuteEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum HYSTERESIS_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "hysteresisLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum HYSTERESIS_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "hysteresisMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum SYSTEM_OFFSETS_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "systemOffsetsExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum SYSTEM_OFFSETS_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "systemOffsetsLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum SYSTEM_OFFSETS_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "systemOffsetsMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MAGNIFICATION_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "referencePlaneMagnificationExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);


  public static final SoftwareConfigEnum MAGNIFICATION_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "referencePlaneMagnificationLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MAGNIFICATION_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "referencePlaneMagnificationMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  //Xray Spot Deflection Adjustment values

  // how many times to repeat this task, should be combined with looping in Service UI
  // to create snapshots for long enough to do this adjustment
  public static final SoftwareConfigEnum XRAY_SPOT_DEFLECTION_ADJUSTMENT_REPEAT_COUNT =
      new SoftwareConfigEnum(++_index,
                             "xraySpotDeflectionAdjustmentRepeatCount",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // How long to sleep between capturing pictured
  public static final SoftwareConfigEnum XRAY_SPOT_DEFLECTION_ADJUSTMENT_SLEEP_INTERVAL_IN_MILLIS =
      new SoftwareConfigEnum(++_index,
                             "xraySpotDeflectionAdjustmentSleepIntervalInMilliSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical System Fiducial values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_FIDUCIAL_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemFiducialExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_FIDUCIAL_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemFiducialMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical Reference Plane values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_REFERENCE_PLANE_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalReferencePlaneExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_REFERENCE_PLANE_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalReferencePlaneMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical System Fiducial Rotation values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_FIDUCIAL_ROTATION_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemFiducialRotationExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_FIDUCIAL_ROTATION_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemFiducialRotationMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical Magnification values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_MAGNIFICATION_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalMagnificationExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_MAGNIFICATION_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalMagnificationMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical Fringe Pattern Uniformity values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_FRINGE_PATTERN_UNIFORMITY_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalFringePatternUniformityMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical Height values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_HEIGHT_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalHeightExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_HEIGHT_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalHeightMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  /* --------------- Optical System Fiducial Offset values -------------- */
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_OFFSET_EXECUTE_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemOffsetExecuteTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_OFFSET_LOG_RESULTS_EVERY_TIMEOUT_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemOffsetLogResultsEveryTimeoutSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_SYSTEM_OFFSET_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "opticalSystemOffsetMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  /* --------------- Confirmation values that need to be configurable -------------- */

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum AREA_MODE_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "areaModeImageConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // how large to let log file grow before deleting bytes
  public static final SoftwareConfigEnum AREA_MODE_IMAGE_CONFIRM_MAX_NUMBER_OF_LOG_FILES =
      new SoftwareConfigEnum(++_index,
                             "areaModeImageConfirmMaxNumberOfLogFiles",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);


  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum CAMERA_ADJUSTMENT_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraAdjustmentConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_ADJUSTMENT_CONFIRM_LOGGING_TIMEOUT =
      new SoftwareConfigEnum(++_index,
                             "cameraAdjustmentConfirmLoggingTimeout",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // how large to let log file grow before deleting bytes
  public static final SoftwareConfigEnum CAMERA_ADJUSTMENT_CONFIRM_MAX_LOG_FILES_IN_DIRECTORY =
      new SoftwareConfigEnum(++_index,
                             "cameraAdjustmentConfirmMaxLogFilesInDirectory",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  //Shared Image Confirmation values
  public static final SoftwareConfigEnum IMAGE_CONFIRM_MAX_LOG_FILE_NUMBER =
      new SoftwareConfigEnum(++_index,
                             "imageConfirmMaxNumberOfLogFiles",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CAMERA_DARK_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraDarkImageConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);
  public static final SoftwareConfigEnum CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraLightImageAllGroupsConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);

  public static final SoftwareConfigEnum CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraLightImageConfirmGroupOneTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);

  public static final SoftwareConfigEnum CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraLightImageConfirmGroupTwoTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);

  public static final SoftwareConfigEnum CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "cameraLightImageConfirmGroupThreeTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum XRAY_SOURCE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "xraySourceConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // how large to let log file grow before deleting bytes
  public static final SoftwareConfigEnum XRAY_SOURCE_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES =
      new SoftwareConfigEnum(++_index,
                             "xraySourceConfirmMaxLogFilesizeInBytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // time interval to have elapsed before running the confirmation after being triggered
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // how large to let log file grow before deleting bytes
  public static final SoftwareConfigEnum SHARPNESS_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES =
      new SoftwareConfigEnum(++_index,
                             "sharpnessConfirmMaxLogFilesizeInBytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  ///////////////////// Enum values for magnification confirmation
  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_REGION_TO_IMAGE_WIDTH_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponRegionToImageWidthNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponRegionToImageHeightNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

// George A. David moved to SystemConfigEnum
//
//  // Margins to use for requesting images:
//  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
//      new SoftwareConfigEnum(++_index,
//                             "confirmMagCouponMarginInXFromQuadrilateralNanometers",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//
//  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS =
//      new SoftwareConfigEnum(++_index,
//                             "confirmMagCouponMarginForFocusRegionNanometers",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);

  // Dimensions to crop out edges for measurement
  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponCroppedHorizontalEdgeWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponCroppedHorizontalEdgeHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponCroppedVerticalEdgeLeftWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmMagCouponCroppedVerticalEdgeLeftHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

// George A. David moved to SystemConfigEnum
//
//  // Remove quadrilateral so it won't affect template match
//  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
//      new SoftwareConfigEnum(++_index,
//                             "confirmMagCouponKeepYNanometersBelowSystemFiducialForTemplateMatch",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//
//  // special value because all of coupon isn't visible in mag pocket
//  public static final SoftwareConfigEnum CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
//      new SoftwareConfigEnum(++_index,
//                             "confirmMagCouponSyscoupLowerRightVisibleDotYNanometers",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);

  ////////////// Enum values for system fiducial confirmation

  //How big of a region to image.
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_REGION_TO_IMAGE_WIDTH_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialRegionToImageWidthNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialRegionToImageHeightNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Margins to use for requesting images:
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialMarginInXFromQuadrilateralNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

// George A. David moved to SystemConfigEnum
//
//  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS =
//      new SoftwareConfigEnum(++_index,
//                             "confirmSystemFiducialMarginForFocusRegionNanometers",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//
//  // Remove quadrilateral so it won't affect template match
//  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
//      new SoftwareConfigEnum(++_index,
//                             "confirmSystemFiducialKeepYNanometersBelowSystemFiducialForTemplateMatch",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);

  // Dimensions to crop out edges for measurement
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialCroppedHorizontalEdgeWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialCroppedHorizontalEdgeHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialCroppedVerticalEdgeRightWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialCroppedVerticalEdgeRightHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

// George A. David moved to SystemConfigEnum
//
//  // Special value because bottom of quadrilateral isn't visible in system fiducial pocket (i.e. fixed rail,
//  // right side) on all machines.  This is 2 mm less than lowest visible dot on coupon according to CAD
//  // drawing (19.85 mm - 2 mm or 19850000 nm - 2000000 nm = 17850000 nm).
//  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
//      new SoftwareConfigEnum(++_index,
//                             "confirmSystemFiducialSyscoupLowerRightVisibleDotYNanometers",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);

  ///////////// Enum values for system fiducial B confirmation
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_REGION_TO_IMAGE_WIDTH_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBRegionToImageWidthNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBRegionToImageHeightNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Margins to use for requesting images:
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBMarginInXFromQuadrilateralNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_MARGIN_FOR_FOCUS_REGION_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBMarginForFocusRegionNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Remove quadrilateral so it won't affect template match
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_KEEP_Y_NANOMETERS_ABOVE_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBKeepYNanometersAboveSystemFiducialForTemplateMatch",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedHorizontalEdgeWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedHorizontalEdgeHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedVerticalEdgeLeftWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedVerticalEdgeLeftHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedVerticalEdgeRightWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBCroppedVerticalEdgeRightHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Special value because lowest visible dot isn't enough margin to make sure that the lower horizontal
  // edge of quadrilateral is safely visible.  Cases were observed where the lower edge was closer to
  // the image edge than expected.  Increasing this value provides some margin.  The value is 1 mm greater
  // than lowest visible dot on coupon according to CAD drawing (19.85 mm + 1 mm or
  // 19850000 nm + 1000000 nm = 20850000 nm).
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_FIDUCIAL_B_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemFiducialBSyscoupLowerRightVisibleDotYNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);


  /////////////////////////// Enum values for systemMTF confirmation
  /** @todo Bob B. - Investigate the possibility of changing this to TypeEnum.LIST_OF_STRINGS */
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_LIST_OF_CAMERAS_TO_RUN_ON =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFListOfCamerasToRunOn",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  // How big of a region to image.
  // Width will be 1 camera array width minus , 1084 pixels = 17344000 nm (at 16000 nm / pixel)
  // Height will be 1588 pixels = 25408000 nm
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_REGION_TO_IMAGE_WIDTH_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFRegionToImageWidthNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFRegionToImageHeightNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Margins to use for requesting projections:
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_MARGIN_IN_X_FROM_QUADRILATERAL_UPPER_LEFT_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFMarginInXFromQuadrilateralUpperLeftNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  // Dimensions to crop out edges for measurement
  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFCroppedHorizontalEdgeWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFCroppedHorizontalEdgeHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFCroppedVerticalEdgeRightWidthPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
      new SoftwareConfigEnum(++_index,
                             "confirmSystemMTFCroppedVerticalEdgeRightHeightPixels",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION =
      new SoftwareConfigEnum(++_index,
                             "useSurfaceModelDuringFocusConfirmation",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final SoftwareConfigEnum RETEST_PASSING_OUTLIERS_DURING_FOCUS_CONFIRMATION =
      new SoftwareConfigEnum(++_index,
                             "retestPassingOutliersDuringFocusConfirmation",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
  //Siew Yeng - XCR-3781 - PSH info log
  public static final SoftwareConfigEnum ENABLE_SURFACE_MODEL_INFO_LOG = 
      new SoftwareConfigEnum(++_index,
                             "enableSurfaceModelInfoLog",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             booleanRange());


  public static final SoftwareConfigEnum REQUIRED_DISTANCE_FROM_INTERFERENCE_AFFECTED_REGION_IN_NANO_METERS_TYPE_FULL =
      new SoftwareConfigEnum(++_index,
                             "requiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum REQUIRED_DISTANCE_FROM_INTERFERENCE_AFFECTED_REGION_IN_NANO_METERS_TYPE_HALF =
      new SoftwareConfigEnum(++_index,
                             "requiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MAXIMUM_JOINT_HEIGHT_ALLOWED_FROM_USER_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumJointHeightAllowedFromUserInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum TALLEST_INSPECTABLE_JOINT_HEIGHT_IN_NANO_METERS =
      new SoftwareConfigEnum(++_index,
                             "tallestInspectableJointHeightInNanoMeters",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

// George A. David moved to SystemConfigEnum
//
//  public static final SoftwareConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION =
//      new SoftwareConfigEnum(++_index,
//                             "maxStepSizeInNanoMetersForDefaultLowSignalCompensation",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//
//  public static final SoftwareConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION =
//      new SoftwareConfigEnum(++_index,
//                             "maxStepSizeInNanoMetersForMediumSignalCompensation",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//  public static final SoftwareConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION =
//      new SoftwareConfigEnum(++_index,
//                             "maxStepSizeInNanoMetersForMediumHighSignalCompensation",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//  public static final SoftwareConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION =
//      new SoftwareConfigEnum(++_index,
//                             "maxStepSizeInNanoMetersForHighSignalCompensation",
//                             _SOFTWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);

  public static final SoftwareConfigEnum DEVELOPER_PERFORMANCE_MODE =
    new SoftwareConfigEnum(++_index,
                           "developerPerformanceMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum ENABLE_XOUT_FEATURE =
    new SoftwareConfigEnum(++_index,
                           "enableXoutFeature",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);
  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_FEATURE =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringFeature",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA0_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera0QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA1_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera1QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA2_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera2QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA3_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera3QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA4_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera4QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA5_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera5QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA6_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera6QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA7_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera7QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA8_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera8QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA9_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera9QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA10_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera10QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA11_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera11QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA12_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera12QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum DISABLE_XRAY_CAMERA13_QUALITY_MONITORING_TRIGGER =
    new SoftwareConfigEnum(++_index,
                           "disableXrayCamera13QualityMonitoringTrigger",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringGradientMean",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STDDEV =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringGradientStdDev",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringGradientAboveMean",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringGradientMax",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA =
    new SoftwareConfigEnum(++_index,
                           "enableXrayCameraQualityMonitoringGradientBeta",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_DATA_SIZE =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_DataSize",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_CHART_WIDTH =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_ChartWidth",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_CHART_HEIGHT =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_ChartHeight",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_GRADIENT_MEAN =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_GradientMean",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_GRADIENT_STDDEV =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_GradientStdDev",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_GRADIENT_ABOVE_MEAN =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_GradientAboveMean",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_GRADIENT_MAX =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_GradientMax",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_GRADIENT_BETA =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_GradientBeta",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);
   public static final SoftwareConfigEnum XRAY_CAMERA_QUALITY_SIGMA_VALUE =
      new SoftwareConfigEnum(++_index,
                             "xrayCameraQuality_SigmaValue",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

    public static final SoftwareConfigEnum DATABASE =
    new SoftwareConfigEnum(++_index,
                           "database",
                           _SOFTWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

    public static final SoftwareConfigEnum USE_IMAGE_TYPE_FORMAT =
    new SoftwareConfigEnum(++_index,
                           "useImageTypeFormat",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final SoftwareConfigEnum MICROCONTROLLER_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES =
      new SoftwareConfigEnum(++_index,
                             "microControllerConfirmMaxLogFilesizeInBytes",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum MICROCONTROLLER_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "microControllerConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final SoftwareConfigEnum ENABLE_PSP =
      new SoftwareConfigEnum(++_index,
                             "enablePsp",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);
  
  public static final SoftwareConfigEnum ENABLE_SURFACE_MAP_INFO_LOG =
      new SoftwareConfigEnum(++_index,
                             "enableSurfaceMapInfoLog",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_CAMERA_DARK_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalCameraDarkImageConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);
  
  public static final SoftwareConfigEnum OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS =
      new SoftwareConfigEnum(++_index,
                             "opticalCameraLightImageConfirmTimeoutInSeconds",
                             _SOFTWARE_CONFIG,
                             TypeEnum.LONG,
                             null);
  
  public static final SoftwareConfigEnum SAVE_PSP_INSPECTION_IMAGES =
      new SoftwareConfigEnum(++_index,
                             "savePspInspectionImages",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

  // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
  public static final SoftwareConfigEnum GENERATE_MULTI_ANGLE_IMAGES =
    new SoftwareConfigEnum(++_index,
                           "generateMultiAngleImages",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});
  
  // Kee Chin Seong, Added this key for user decide whether they want extra info on slicename
  public static final SoftwareConfigEnum ADD_EXTRA_INFO_ON_SLICE_NAME_IN_OUTPUT_FILE =
    new SoftwareConfigEnum(++_index,
                           "addExtraInfoOnSliceNameInOutputFile",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});
  
  // Chnee Khang Wah, to include sharpness profile info in image header or not
  public static final SoftwareConfigEnum GENERATE_IMAGE_WITH_SHARPNESS_INFO =
    new SoftwareConfigEnum(++_index,
                           "generateImageWithSharpnessInfo",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});
  
  // Chong Wei Chin - Adding more autofocus parameter for IRP
  public static final SoftwareConfigEnum MAXIMUM_PANEL_UP_WARP_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumPanelUpWarpInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MAXIMUM_PANEL_DOWN_WARP_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumPanelDownWarpInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MAXIMUM_CARRIER_UP_WARP_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumCarrierUpWarpInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MAXIMUM_CARRIER_DOWN_WARP_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumCarrierDownWarpInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum UPWARD_BOARD_BOTTOM_UNCERTAINTY_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "upwardBoardBottomUncertaintyInNanometer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum DOWNWARD_BOARD_BOTTOM_UNCERTAINTY_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "downwardBoardBottomUncertaintyInNanometer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MAXIMUM_JOINT_HEIGHT_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumJointHeightInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MAXIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "maximumSliceHeightFromNominalSliceInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum MINIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "minimumSliceHeightFromNominalSliceInNanometers",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum BOTTOM_OF_BOARD_OFFSET_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "bottomOfBoardOffsetInNanometer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum TOP_OF_BOARD_OFFSET_IN_NANOMETERS =
      new SoftwareConfigEnum(++_index,
                             "topOfBoardOffsetInNanometer",
                             _SOFTWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final SoftwareConfigEnum BOARD_THICKNESS_VARIATION_RATIO =
      new SoftwareConfigEnum(++_index,
                             "boardThicknessVariationRatio",
                             _SOFTWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             null);

  // Xray tube crash preventive action -Anthony Fong
  public static final SoftwareConfigEnum BYPASS_PANEL_CLEARED_CHECKING_AFTER_PANEL_UNLOADED_BEFORE_CDA =
      new SoftwareConfigEnum(++_index,
                             "bypassPanelClearedCheckingAfterPanelUnloadedBeforeCDA",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});

  public static final SoftwareConfigEnum PANEL_CLEARED_SCAN_PERIOD_AFTER_PANEL_UNLOADED_BEFORE_CDA_IN_MILLISECONDS =
    new SoftwareConfigEnum(++_index,
                           "panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
                           //range(3000,20000));
  
  // Enable Real Time PSH
  public static final SoftwareConfigEnum ENABLE_REAL_TIME_PSH =
      new SoftwareConfigEnum(++_index,
                             "enableRealTimePSH",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  //Ngie Xing, XCR-2027 
  public static final SoftwareConfigEnum ENABLE_INTELLIGENT_LEARNING =
    new SoftwareConfigEnum(++_index,
                           "enableIntelligentLearning",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  //Ngie Xing
  public static final SoftwareConfigEnum ENABLE_DELETE_SHORTS_LEARNING_DURING_PROJECT_LOAD =
    new SoftwareConfigEnum(++_index,
                           "enableDeleteShortsLearningDuringProjectLoad",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
    public static final SoftwareConfigEnum ENABLE_INFO_HANDLER =
    new SoftwareConfigEnum(++_index,
                           "enableInfoHandler",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[] {new Boolean(true), new Boolean(false)});
    
    //Siew Yeng
    public static final SoftwareConfigEnum MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL =
    new SoftwareConfigEnum(++_index,
                           "maxDynamicRangeOptimizationLevel",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(1,10));

   // Lee Herng, 2015-02-25
   public static final SoftwareConfigEnum ENABLE_COMPONENT_LEVEL_CLASSIFICATION =
    new SoftwareConfigEnum(++_index,
                           "enableComponentLevelClassification",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
   
   // XCR-2465 Ability to transfer PNG image to VVTS - Cheah Lee Herng
   public static final SoftwareConfigEnum REPAIR_IMAGE_TYPE_FORMAT =
    new SoftwareConfigEnum(++_index,
                           "repairImageTypeFormat",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  //Khaw Chek Hau - XCR2654: CAMX Integration
  public static final SoftwareConfigEnum ENABLE_SHOP_FLOOR_SYSTEM = 
    new SoftwareConfigEnum(++_index,
                           "enableShopFloorSystem",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});

  //Khaw Chek Hau - XCR2654: CAMX Integration
  public static final SoftwareConfigEnum SHOP_FLOOR_SYSTEM_TYPE = 
    new SoftwareConfigEnum(++_index,
                           "shopFloorSystemType",
                           _SOFTWARE_CONFIG,
                           TypeEnum.STRING,
                           null);
  //Anthony Fong - XCR2657 Performance benchmark on CDNA
  public static final SoftwareConfigEnum ENABLE_CDNA_BENCHMARK = 
    new SoftwareConfigEnum(++_index,
                           "enableCDNABenchmark",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
     
  public static final SoftwareConfigEnum MINIMUM_CDNA_BENCHMARK_LOOP_COUNT =
    new SoftwareConfigEnum(++_index,
                           "minimumCDNABenchmarkLoopCount",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  //V810 Kee Chin Seong - XCR-2828 Handle SMEMA Panel Handling Sequenc
  public static final SoftwareConfigEnum ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL =
    new SoftwareConfigEnum(++_index,
                           "enableSetPanelAvailableAfterDownStramAcceptPanel",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

   // Swee Yee Wong -  Enable PSFC 
   public static final SoftwareConfigEnum ENABLE_MULTI_ANGLE_POST_PROCESSING =
    new SoftwareConfigEnum(++_index,
                           "enableMultiAnglePostProcessing",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
   
   // Configurable thickness table version - Swee Yee Wong
   public static final SoftwareConfigEnum THICKNESS_TABLE_LATEST_VERSION =
    new SoftwareConfigEnum(++_index,
                           "thicknessTableLatestVersion",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
           
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  public static final SoftwareConfigEnum GENERATE_MACHINE_UTILIZATION_REPORT = 
    new SoftwareConfigEnum(++_index,
                           "generateMachineUtilizationReport",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
  // Swee yee Wong, to enable camera debugging log
  // XCR-2630 Support New X-Ray Camera
  public static final SoftwareConfigEnum CAMERA_DEBUG_MODE =
    new SoftwareConfigEnum(++_index,
                           "xrayCameraDebugMode",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});
  
  public static final SoftwareConfigEnum CAMERA_DEBUG_COMMAND_LIST_OF_CAMERAS_TO_SHOW =
      new SoftwareConfigEnum(++_index,
                             "cameraDebugCommandListOfCamerasToRun",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  public static final SoftwareConfigEnum GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT = 
    new SoftwareConfigEnum(++_index,
                           "generateVOneMachineStatusMonitoringReport",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  public static final SoftwareConfigEnum VONE_STATUS_MONITORING_REPORT_DIR = 
      new SoftwareConfigEnum(++_index,
                             "vOneStatusMonitoringReportDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  public static final SoftwareConfigEnum MACHINE_UTILIZATION_REPORT_DIR = 
      new SoftwareConfigEnum(++_index,
                             "machineUtilizationReportDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
  public static final SoftwareConfigEnum SPECIFIC_HISTORY_LOG_DIR =
      new SoftwareConfigEnum(++_index,
                             "specificHistoryLogDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
  public static final SoftwareConfigEnum USE_SPECIFIC_HISTORY_LOG_DIR =
    new SoftwareConfigEnum(++_index,
                           "useSpecificHistoryLogDir",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
  //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
  public static final SoftwareConfigEnum PROMPT_WARNING_MESSAGE_IF_MODIFY_DRO_LEVEL =
    new SoftwareConfigEnum(++_index,
                           "promptWarningMsgIfModifyDROLevel",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE =
      new SoftwareConfigEnum(++_index,
                             "customizeSettingConfigurationNameToUse",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);  

  //Khaw Chek Hau - XCR3534: v810 SECS/GEM protocol implementation
  public static final SoftwareConfigEnum SECS_GEM_LOG_DIR = 
      new SoftwareConfigEnum(++_index,
                             "secsgemLogDir",
                             _SOFTWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
  
  public static final SoftwareConfigEnum ENABLE_CAD_CREATOR = 
      new SoftwareConfigEnum(++_index,
                           "enableCadCreator",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});    
  
  // Kok Chun, Tan - enable velocity mapping for MVEDR
  public static final SoftwareConfigEnum USE_VELOCITY_MAPPING =
      new SoftwareConfigEnum(++_index,
                             "useVelocityMapping",
                             _SOFTWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]
                             {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum MASKING_THRESHOLD =
    new SoftwareConfigEnum(++_index,
                           "maskingThreshold",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final SoftwareConfigEnum MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION =
    new SoftwareConfigEnum(++_index,
                           "minCameraUsedForReconstruction",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(1,14));
  public static final SoftwareConfigEnum MAXIMUM_SPEED_SELECTION =
    new SoftwareConfigEnum(++_index,
                           "maximumSpeedSelection",
                           _SOFTWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(1,17));

  //Khaw Chek Hau - XCR3774: Display load & unload time during production
  public static final SoftwareConfigEnum TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME = 
      new SoftwareConfigEnum(++_index,
                           "displayProductionPanelLoadingAndUnloadingTime",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});
  
  public static final SoftwareConfigEnum KEEP_OUTER_BARRIER_OPEN_AFTER_SCANNING_COMPLETE_S2EX = 
      new SoftwareConfigEnum(++_index,
                           "panelHandlerKeepOuterBarrierOpenAfterScanningComplete",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)});    
  
  public static final SoftwareConfigEnum KEEP_OUTER_BARRIER_OPEN_FOR_LOADING_UNLOADING_PANEL = 
      new SoftwareConfigEnum(++_index,
                           "panelHandlerKeepOuterBarrierOpenForLoadingAndUnloadingPanel",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]
                           {new Boolean(true), new Boolean(false)}); 
  
  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  public static final SoftwareConfigEnum ENFORCE_WRITE_REMARK =
    new SoftwareConfigEnum(++_index,
                           "enforceWriteRemark",
                           _SOFTWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{new Boolean(true), new Boolean(false)});
  
 /**
   * @author Rex Shang
   */
  public static String getPanelHandlerPanelLoadingPassBackModeName()
  {
    return _PANEL_HANDLER_PANEL_LOADING_PASS_BACK_MODE_NAME;
  }

  /**
   * @author Rex Shang
   */
  public static String getPanelHandlerPanelLoadingFlowThroughModeName()
  {
    return _PANEL_HANDLER_PANEL_FLOW_FLOW_THROUGH_MODE_NAME;
  }

  /**
   * @author Rex Shang
   */
  public static String getPanelHandlerPanelFlowDirectionLeftToRightModeName()
  {
    return _PANEL_HANDLER_PANEL_FLOW_DIRECTION_LEFT_TO_RIGHT_MODE_NAME;
  }

  /**
   * @author Rex Shang
   */
  public static String getPanelHandlerPanelFlowDirectionRightToLeftModeName()
  {
    return _PANEL_HANDLER_PANEL_FLOW_DIRECTION_RIGHT_TO_LEFT_MODE_NAME;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPanelSamplingModeNoSampling()
  {
    return _PANEL_SAMPLING_MODE_NO_SAMPLING;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPanelSamplingModeTestEveryNthPanel()
  {
    return _PANEL_SAMPLING_MODE_TEST_EVERY_NTH_PANEL;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPanelSamplingModeSkipEveryNthPanel()
  {
    return _PANEL_SAMPLING_MODE_SKIP_EVERY_NTH_PANEL;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public static int getPngImageType()
  {
    return _IMAGE_TYPE_PNG;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public static int getTiffImageType()
  {
    return _IMAGE_TYPE_TIFF;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static int getJpegImageType()
  {
    return _IMAGE_TYPE_JPG;
  }

  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getSoftwareConfigEnums()
  {
    Assert.expect(_softwareConfigEnums != null);

    return _softwareConfigEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_softwareConfigEnums != null);

    return _softwareConfigEnums;
  }
}
