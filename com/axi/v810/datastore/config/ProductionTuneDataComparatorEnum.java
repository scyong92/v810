package com.axi.v810.datastore.config;

public class ProductionTuneDataComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ProductionTuneDataComparatorEnum PROJECT_NAME = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum IMAGE_DESTINATION = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum RESULTS_DESTINATION = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum CURRENT_COUNT = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum RE_ENABLE_THRESHOLD = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum RE_ENABLE_COUNT = new ProductionTuneDataComparatorEnum(++_index);
  public static ProductionTuneDataComparatorEnum NOTIFICATION_EMAIL = new ProductionTuneDataComparatorEnum(++_index);

  /**
   * @author George Booth
   */
  private ProductionTuneDataComparatorEnum(int id)
  {
    super(id);
  }
}
