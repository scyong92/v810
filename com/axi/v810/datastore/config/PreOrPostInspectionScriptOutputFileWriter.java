package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.business.BusinessException;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kee chin Seong
 */
public class PreOrPostInspectionScriptOutputFileWriter
{
    private String _fileName = "";  
    /**
     * Author Kee chin Seong 
     * constructor
     */
    public PreOrPostInspectionScriptOutputFileWriter()
    {
        
    }
    
    /**
     * @author Kee Chin Seong
     * @param inputFileName 
     */
    public void setFilePath(String inputFileName)
    {
        _fileName = inputFileName;
    }
    
    /**
     * @author Kee Chin Seong
     * @param fileName
     * @param data
     * @throws DatastoreException
     * @throws BusinessException 
     */
    public void saveSettings(List<String> data) throws DatastoreException, BusinessException
    {
      Assert.expect(data != null);  
      
      writeSettingsToFile(data);
    }
    
    /**
     * @author Kee Chin Seong
     * @param inputFileName
     * @throws DatastoreException 
     */
    private void writeSettingsToFile(List<String> data) throws DatastoreException, BusinessException
    {

      Assert.expect(data != null);

      FileWriterUtil fileWriterUtil = new FileWriterUtil(_fileName, false);
      try
      {
        fileWriterUtil.open();
        for(String parameter : data)
            fileWriterUtil.writeln(parameter);
      }
      catch (CouldNotCreateFileException ex)
      {
        CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        fileWriterUtil.close();
      }
    }
}
