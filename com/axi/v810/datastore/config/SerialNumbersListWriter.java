package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class SerialNumbersListWriter
{
  private static Map _fileNameToSerialNumbersListWriter = new HashMap();
  private String _fileName;

  /**
   * @author Michael Tutkowski
   */
  public static synchronized SerialNumbersListWriter getInstance(String fileName)
  {
    Assert.expect(fileName != null);

    SerialNumbersListWriter serialNumbersListWriter = (SerialNumbersListWriter)_fileNameToSerialNumbersListWriter.get(fileName);
    if (serialNumbersListWriter == null)
    {
      serialNumbersListWriter = new SerialNumbersListWriter(fileName);
      _fileNameToSerialNumbersListWriter.put(fileName, serialNumbersListWriter);
    }

    Assert.expect(serialNumbersListWriter != null);
    return serialNumbersListWriter;
  }

  /**
   * @author Michael Tutkowski
   */
  private SerialNumbersListWriter(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }

  public void write(Collection collectionOfSerialNumberRegularExpressionData) throws DatastoreException
  {
    Assert.expect(collectionOfSerialNumberRegularExpressionData != null);
    Assert.expect(_fileName != null);

    // don't forget to call isExpressionValid on each item before writing anything
    // if they are not all valid, do not write anything

    Iterator it = collectionOfSerialNumberRegularExpressionData.iterator();

    while (it.hasNext())
    {
      SerialNumberRegularExpressionData data = (SerialNumberRegularExpressionData)it.next();
    }

    FileWriter fw = null;

    try
    {
      fw = new FileWriter(_fileName);
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(_fileName);

      ex.initCause(ioe);

      throw ex;
    }

    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: serialNumToProject.config");
    os.println("# PURPOSE: To store a list of serial number expressions.");
    os.println("#");
    os.println("# Each entry consists of two lines.");
    os.println("# line1: simple|regEx");
    os.println("# line2: serial number string to match");
    os.println("#");
    os.println("# If it is a simple line, then the serial number a project are a case insensitive match");
    os.println("# simple matching accepts the following wild cards:");
    os.println("#");
    os.println("#   ? - matches one of any character");
    os.println("#   * - matches one or more of any character");
    os.println("#");
    os.println("# Here are some examples:");
    os.println("# simple");
    os.println("# 1234");
    os.println("#");
    os.println("# simple");
    os.println("# 1234?");
    os.println("#");
    os.println("# simple");
    os.println("# 1234*");
    os.println("#");
    os.println("# If it is regEx then it does full regular expression matching");
    os.println("#");
    os.println("# regEx");
    os.println("# 12\\d\\w");
    os.println("#");
    os.println("# regEx");
    os.println("# 12[a|b][a-z]*");
    os.println("#");
    os.println("#");
    os.println("########################################################################");

    it = collectionOfSerialNumberRegularExpressionData.iterator();

    while (it.hasNext())
    {
      SerialNumberRegularExpressionData data = (SerialNumberRegularExpressionData)it.next();

      if (data.isInterpretedAsRegularExpression())
        os.println("regEx");
      else
        os.println("simple");

      String origExpr = data.getOriginalExpression();
      origExpr = origExpr.replaceAll("#", "\\\\#");
      os.println(origExpr);
      os.println("");
    }

    os.close();
  }
}
