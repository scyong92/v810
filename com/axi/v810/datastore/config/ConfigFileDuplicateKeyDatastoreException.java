package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class gets thrown when a config file has the same key listed more than once.
 * @author Bill Darbie
 */
public class ConfigFileDuplicateKeyDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ConfigFileDuplicateKeyDatastoreException(String fileName,
                                                  int lineNumber)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_FILE_DUPLICATE_KEY_KEY", new Object[]{fileName,
                                                                                     new Integer(lineNumber)}));
    Assert.expect(fileName != null);
  }
}
