package com.axi.v810.datastore.config;

import com.axi.util.*;

import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.io.*;
import java.util.*;

/**
 * @author wei-chin.chong
 */
public class ProductionTuneSettings implements Serializable
{
  private boolean _saveAllImages = false;
  private boolean _saveOnlyDefectImages = true;
  private boolean _saveOnlySelectedImages = false;
  private String _directoryPath;
  
  // this cannot be changes due to it will save into binary format, hardcode needed for the key below
  transient public static final String _JOINTTYPE = "Joint Type";
  transient public static final String _SUBTYPE = "Subtype";
  transient public static final String _REFDES = "References Designator";
  transient public static final String _LANDPATTERN = "Land Pattern";
  transient private static List<String> _listOfTypes = null;
  
  transient public static final int _EXACT_MATCH = 1;  
  
  private Map<String, Map<String, Integer>> _typeToNamesMap = new HashMap();
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static ProductionTuneSettings readSettings()
  {
    ProductionTuneSettings instance = null;
    try
    {
      int version = FileName.getProductionTuneSettingsLatestFileVersion();
      if (FileUtilAxi.exists(FileName.getProductionTuneSettingsFullPath(version)))
      {
        instance = (ProductionTuneSettings) FileUtilAxi.loadObjectFromSerializedFile(FileName.getProductionTuneSettingsFullPath(version));
      }
      else
        instance = new ProductionTuneSettings();
    }
    catch(DatastoreException de)
    {
      // do nothing..
      instance = new ProductionTuneSettings();
    }

      return instance;
    }

    /**
    * @author Wei Chin
    */
    public static void writeSettings(ProductionTuneSettings instance) throws DatastoreException
    {
      int version = FileName.getProductionTuneSettingsLatestFileVersion();
      FileUtilAxi.saveObjectToSerializedFile(instance, FileName.getProductionTuneSettingsFullPath(version));
    }
    
    /**
     * @return
     * @author Wei Chin
     */
    static public List<String> getListOfTypes()
    {
      if(_listOfTypes == null)
      {
        _listOfTypes = new ArrayList();
        _listOfTypes.add(_JOINTTYPE);
        _listOfTypes.add(_SUBTYPE);
        _listOfTypes.add(_LANDPATTERN);
        _listOfTypes.add(_REFDES);
      }
      
      return _listOfTypes;
    }

  /**
   * @return the _saveAllImages
   * @author Wei Chin
   */
  public boolean isSaveAllImages()
  {
    return _saveAllImages;
  }

  /**
   * @param saveAllImages the _saveAllImages to set
   * @author Wei Chin
   */
  public void setSaveAllImages()
  {
    _saveAllImages = true;
    _saveOnlyDefectImages = false;
    _saveOnlySelectedImages = false;
  }

  /**
   * @return the _saveOnlyDefectImages
   * @author Wei Chin
   */
  public boolean isSaveOnlyDefectImages()
  {
    return _saveOnlyDefectImages;
  }

  /**
   * @param saveOnlyDefectImages the _saveOnlyDefectImages to set
   * @author Wei Chin
   */
  public void setSaveOnlyDefectImages()
  {
    _saveAllImages = false;
    _saveOnlyDefectImages = true;
    _saveOnlySelectedImages = false;
  }

  /**
   * @return the _saveOnlySelectedImages
   * @author Wei Chin
   */
  public boolean isSaveOnlySelectedImages()
  {
    return _saveOnlySelectedImages;
  }

  /**
   * @param saveOnlySelectedImages the _saveOnlySelectedImages to set
   * @author Wei Chin
   */
  public void setSaveOnlySelectedImages()
  {
    _saveAllImages = false;
    _saveOnlyDefectImages = false;
    _saveOnlySelectedImages = true;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasDirectoryPath()
  {
    if (_directoryPath != null && _directoryPath.length() > 0)
      return true;
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getDirectoryPath()
  {
    return _directoryPath;
  }
  
  /**
   * @author CHeah Lee Herng 
   */
  public void setDirectoryPath(String directoryPath)
  {
    _directoryPath = directoryPath;
  }

  /**
   * @return the _typeToNamesMap
   * @author Wei Chin
   */
  public Map<String, Map<String, Integer>> getTypeToNamesMap()
  {
    return _typeToNamesMap;
  }
  
  /**
   * @return the _typeToNamesMap
   * @author Wei Chin
   */
  public Set<String> getTypes()
  {
    return _typeToNamesMap.keySet();
  }

  /**
   * @return the _typeToNamesMap
   * @author Wei Chin
   */
  public Map<String, Integer> getSelectedType(String type)
  {
    Assert.expect(type != null);
    
    return _typeToNamesMap.get(type);
  }  
  
  
  /**
   * @param ListOfTypeToNames the _ListOfTypeToNames to set
   * @author Wei Chin
   */
  public void addTypeToNameMap(String type, String name)
  {
    Assert.expect(type != null);
    Assert.expect(name != null);
    Assert.expect(name.length() > 0);
    Assert.expect(_listOfTypes.contains(type));
    
    Map <String, Integer> names =  _typeToNamesMap.get(type);
    if(names != null)
    {
      names.put(name.toLowerCase(), _EXACT_MATCH);
    }
    else
    {
      names = new HashMap();
      names.put(name.toLowerCase(), _EXACT_MATCH);
      _typeToNamesMap.put(type, names);
    }
  }
  
  /**
   * @param ListOfTypeToNames the _ListOfTypeToNames to set
   * @author Wei Chin
   */
  public void removeTypeToNameMap(String type, String name)
  {
    Assert.expect(type != null);
    Assert.expect(name != null);
    Assert.expect(name.length() > 0);
    Assert.expect(_listOfTypes.contains(type));
    
    Map <String, Integer> names =  _typeToNamesMap.get(type);
    if(names != null)
    {
      names.remove(name.toLowerCase());
    }
  }

  /**
   * @author Wei Chin
   */
  public boolean isSelectedSaveImages(ReconstructionRegion region)
  {
    String jointTypeName = region.getJointTypeEnum().getName();
    String refDesName = region.getComponent().getReferenceDesignator();
    String landPatternName = region.getComponent().getLandPattern().getName();
    String subtypeName = region.getSubtypes().iterator().next().getShortName();
    
    Map<String, Integer> jointTypeNames = getSelectedType(_JOINTTYPE);
    Map<String, Integer> landPatternNames = getSelectedType(_LANDPATTERN);
    Map<String, Integer> subtypeNames = getSelectedType(_SUBTYPE);
    Map<String, Integer> refDesNames = getSelectedType(_REFDES);    
    
    // order for checking is follow by: jointType, LandPattern, Subtype, and Refdes (from largest group to smallest group)   
    // check joint type 
    if(jointTypeNames != null && jointTypeNames.isEmpty() == false)
    {     
      Integer matchValue = jointTypeNames.get(jointTypeName.toLowerCase());
      if(matchValue != null)
      {
        if(matchValue.intValue() == _EXACT_MATCH)
          return true;
      }
    }
    
    // check landPatternNames
    if(landPatternNames != null && landPatternNames.isEmpty() == false)
    {
      Integer matchValue = landPatternNames.get(landPatternName.toLowerCase());
      if(matchValue != null)
      {
        if(matchValue.intValue() == _EXACT_MATCH)
          return true;
      }
    }
    
    // check subtypeNames
    if(subtypeNames != null && subtypeNames.isEmpty() == false)
    {
      Integer matchValue = subtypeNames.get(subtypeName.toLowerCase());
      if(matchValue != null)
      {
        if(matchValue.intValue() == _EXACT_MATCH)
          return true;
      }
    }
    
    // check refDesNames
    if(refDesNames != null && refDesNames.isEmpty() == false)
    {
      Integer matchValue = refDesNames.get(refDesName.toLowerCase());
      if(matchValue != null)
      {
        if(matchValue.intValue() == _EXACT_MATCH)
          return true;
      }
    }
    
    return false;
  } 
}
