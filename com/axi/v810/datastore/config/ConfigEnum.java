package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * This is the base enumeration class for any config files.  There should
 * be one Enumeration per config file.  Each of those enumeration classes
 * need to implement this interface.
 *
 * @author Bill Darbie
 */
public abstract class ConfigEnum extends com.axi.util.Enum
{
  private String _key = null;
  private String _fileName = null;
  private TypeEnum _type = null;
  private Object [] _valid = null;
  private Object _value = null;
  private Map<SystemTypeEnum, String> _systemTypeToFilePathMap = new HashMap<SystemTypeEnum, String>();
  private transient static List<ConfigEnum> _configEnumList = new ArrayList<ConfigEnum>();
  private transient static Map<String, Set<String>> _classNameToKeyStringSet = new HashMap<String, Set<String>>();
  private transient static Map<ConfigEnum, Object> _lastValuesCleared = new HashMap<ConfigEnum, Object>();
  private static int _index = -1;

  /**
   * @param id int value of the enumeration.
   * @param key String "key" name for the configuration in the file.
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Bill Darbie
   */
  protected ConfigEnum(int id,
                       String key,
                       String filename,
                       TypeEnum type,
                       Object[] valid)
  {
    super(id);
    Assert.expect(key != null);
    _key = key;
    Assert.expect(filename != null);
    _fileName = filename;
    Assert.expect(type != null);
    _type = type;
    // _valid can be null
    _valid = valid;

    _configEnumList.add(this);
    String className = getClass().getName();

    Set<String> keyStringSet = _classNameToKeyStringSet.get(className);
    if (keyStringSet == null)
    {
      keyStringSet = new HashSet<String>();
      keyStringSet.add(_key);
      _classNameToKeyStringSet.put(className, keyStringSet);
    }
    else
    {
      boolean added = keyStringSet.add(_key);
      Assert.expect(added, "ERROR: The key string " + _key +
                           " was found being used by more than one enum in class " + className);
    }
  }

  /**
   * @return the number of elements in this enumeration.
   * @author Bill Darbie
   */
  static int size()
  {
    return _index + 1;
  }

  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getAllConfigEnums()
  {
    Assert.expect(_configEnumList != null);

    return _configEnumList;
  }

  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public abstract List<ConfigEnum> getConfigEnums();

  /**
   * @return the configuration key.
   *
   * @author Steve Anonson
   */
  public String getKey()
  {
    return _key;
  }

  /**
   * @return the configuration file that holds the key/value.
   *
   * @author Steve Anonson
   */
  public synchronized String getFileName()
  {
    // check to see if we used a system specific file to load this value
    String fileName = null;

    // must do this check first because if it is empty, then that means
    // we haven't yet loaded the files and we are probably in the process
    // of doing so. XrayTest.getSystemType() will trigger another
    // Config.loadIfNecessary and we will get caught in an infinite loop.
    if(_systemTypeToFilePathMap.isEmpty() == false)
      fileName = _systemTypeToFilePathMap.get(XrayTester.getSystemType());

    if(fileName == null)
      fileName = _fileName; // no system specific file, just use the default file

    Assert.expect(fileName != null);
    return fileName;
  }

  /**
   * @param fileName to become the (new) configuration file that holds the key/value.
   * @author Bob Balliew
   */
  public synchronized void setFileName(String fileName)
  {
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author George A. David
   */
  public void setFilePath(SystemTypeEnum systemType, String filePath)
  {
    Assert.expect(systemType != null);
    Assert.expect(filePath != null);

    _systemTypeToFilePathMap.put(systemType, filePath);
  }

  /**
   * @return the type of the configuration value.
   *
   * @author Steve Anonson
   */
  public TypeEnum getType()
  {
    return _type;
  }

  /**
   * @return an array of valid configuration values.
   *
   * @author Steve Anonson
   */
  public Object [] getValidValues()
  {
    return _valid;
  }

  /**
   * Set the configuration value.
   *
   * @author Steve Anonson
   */
  synchronized void setValue(Object value) throws InvalidTypeDatastoreException
  {
    if (value == null)
      _value = null;
    else
    {
      if (_type.equals(TypeEnum.BOOLEAN))
      {
        if ((value.toString().equalsIgnoreCase("true")) || (value.toString().equalsIgnoreCase("false")))
          _value = new Boolean(value.toString());
        else
          throw new InvalidTypeDatastoreException(_key, _fileName, _type, value.toString());
      }
      else if (_type.equals(TypeEnum.STRING))
        _value = new String(value.toString());
      else if (_type.equals(TypeEnum.BINARY))
        _value = new String(value.toString());
      else if (_type.equals(TypeEnum.LIST_OF_STRINGS))
      {
        List<String> stringList = new ArrayList<String>();

        if (value instanceof List)
          stringList = new ArrayList<String>((List<String>)value);
        else if (value instanceof String)
        {
          // now lets create the individual strings - each string is deliminated by ','
          String entireString = value.toString();
          java.util.StringTokenizer stringTokenizer = new java.util.StringTokenizer(entireString, ",", false);

          while (stringTokenizer.hasMoreElements())
            stringList.add(stringTokenizer.nextToken());
        }

        _value = stringList;
      }
      else
      {
        try
        {
          if (_type.equals(TypeEnum.INTEGER))
            _value = new Integer(value.toString());
          else if (_type.equals(TypeEnum.LONG))
            _value = new Long(value.toString());
          else if (_type.equals(TypeEnum.DOUBLE))
            _value = new Double(value.toString());
          else
            Assert.expect(false);
        }
        catch (NumberFormatException nfe)
        {
          InvalidTypeDatastoreException dex = new InvalidTypeDatastoreException(_key, _fileName, _type, value.toString());
          dex.initCause(nfe);
          throw dex;
        }
      }
    }
  }

  /**
   * @return the configuration value.
   *
   * @author Steve Anonson
   */
  public synchronized Object getValue()
  {
    return _value;
  }

  /**
   * @author George A. David
   */
  public void clearSystemSpecificFilePaths()
  {
    Assert.expect(_systemTypeToFilePathMap != null);

    _systemTypeToFilePathMap.clear();
  }

  /**
   * @return the enumeration whose configuration key matches the passed in String.
   *
   * @param key String to match to the configuration key.
   * @author Steve Anonson
   */
  public static ConfigEnum getConfigEnum(String key)
  {

    for (ConfigEnum current : _configEnumList)
    {
      if (key.equals(current.getKey()))
        return current;
    }

    return null;
  }

  /**
   * Clear out all the entries read in previously
   * @author Bill Darbie
   */
  static void clear() throws InvalidTypeDatastoreException
  {
    _lastValuesCleared.clear();
    for (ConfigEnum enumeration : _configEnumList)
    {
      _lastValuesCleared.put(enumeration, enumeration.getValue());
      enumeration.setValue(null);
      enumeration.clearSystemSpecificFilePaths();
    }
  }

  /**
   * Clear out all the entries read in previously from the specified file.
   * @author Bob Balliew
   */
  static List<ConfigEnum> clear(String fileName) throws InvalidTypeDatastoreException
  {
    _lastValuesCleared.clear();
    File file = new File(fileName);
    List<ConfigEnum> clearedEnums = new ArrayList<ConfigEnum>();
    for (ConfigEnum enumeration : _configEnumList)
    {
      if (file.equals(new File(enumeration.getFileName())))
      {
        _lastValuesCleared.put(enumeration, enumeration.getValue());
        enumeration.setValue(null);
        clearedEnums.add(enumeration);
      }
    }

    return clearedEnums;
  }

  /**
   * Undo the last clear operation.
   * @author Bob Balliew
   */
  static void undoLastClear() throws InvalidTypeDatastoreException
  {
    for (ConfigEnum enumeration : _lastValuesCleared.keySet())
      enumeration.setValue(_lastValuesCleared.get(enumeration));

    _lastValuesCleared.clear();
  }

  /**
   * Change the fileName of every ConfigEnum that matches the oldFileName
   * to the newFileName.
   * @author Bob Balliew
   */
  static void changeFileName(String oldFileName, String newFileName)
  {
    File oldFile = new File(oldFileName);

    for (ConfigEnum enumeration : _configEnumList)
    {
      if (oldFile.equals(new File(enumeration.getFileName())))
      {
        enumeration.setFileName(newFileName);
      }
    }
  }

  /**
   * @return all the config file names
   * @author Bill Darbie
   */
  static List<String> getConfigFileNames()
  {
    Set<String> fileNames = new HashSet<String>();
    for (ConfigEnum enumeration : _configEnumList)
    {
      String name = enumeration.getFileName();
      fileNames.add(name);
    }

    List<String> list = new ArrayList<String>(fileNames);
    return list;
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] range(double min, double max)
  {
    Assert.expect(min <= max);
    return new Object[] { min, max }; // java1.5 autoboxing
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] lowerBound(double min)
  {
    return range(min, Double.MAX_VALUE);
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] upperBound(double max)
  {
    return range(Double.MIN_VALUE, max);
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] range(int min, int max)
  {
    Assert.expect(min <= max);
    return new Object[] { min, max }; // java1.5 autoboxing
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] lowerBound(int min)
  {
    return range(min, Integer.MAX_VALUE);
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Greg Loring
   */
  protected static Object[] upperBound(int max)
  {
    return range(Integer.MIN_VALUE, max);
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Reid Hayhow
   */
  protected static Object[] range(long min, long max)
  {
    Assert.expect(min <= max);
    return new Object[] { min, max };
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Reid Hayhow
   */
  protected static Object[] lowerBound(long min)
  {
    return range(min, Long.MAX_VALUE);
  }

  /**
   * create an Object[] to pass as "valid" which will cause a range check when config file is parsed
   *
   * @author Reid Hayhow
   */
  protected static Object[] upperBound(long max)
  {
    return range(Long.MIN_VALUE, max);
  }

  /**
   * create an Object[] to pass all boolean values as "valid"
   *
   * @author Bob Balliew
   */
  protected static Object[] booleanRange()
  {
    return new Object[] { false, true }; // java1.5 autoboxing
  }

  /**
   * @author Bill Darbie
   */
  protected Object getDefaultValue()
  {
    Assert.expect(false);
    return null;
  }

  /**
   * Get the corresponde value object type.
   *
   * @author Poh Kheng
   */
  synchronized Object getValueObjectType(Object value) throws InvalidTypeDatastoreException
  {
    Assert.expect(value != null);
    Assert.expect(_type != null);
    Assert.expect(_key != null);
    Assert.expect(_fileName != null);
    Object returnObject = null;

      if (_type.equals(TypeEnum.BOOLEAN))
      {
        if ((value.toString().equalsIgnoreCase("true")) || (value.toString().equalsIgnoreCase("false")))
          returnObject = new Boolean(value.toString());
        else
          throw new InvalidTypeDatastoreException(_key, _fileName, _type, value.toString());
      }
      else if (_type.equals(TypeEnum.STRING))
        returnObject = new String(value.toString());
      else if (_type.equals(TypeEnum.BINARY))
        returnObject = new String(value.toString());
      else if (_type.equals(TypeEnum.LIST_OF_STRINGS))
      {
        List<String> stringList = new ArrayList<String>();

        if (value instanceof List)
          returnObject = new ArrayList<String>((List<String>)value);
        else if (value instanceof String)
        {
          // now lets create the individual strings - each string is deliminated by ','
          String entireString = value.toString();
          java.util.StringTokenizer stringTokenizer = new java.util.StringTokenizer(entireString, ",", false);

          while (stringTokenizer.hasMoreElements())
            stringList.add(stringTokenizer.nextToken());
        }
        returnObject = stringList;
      }
      else
      {
        try
        {
          if (_type.equals(TypeEnum.INTEGER))
            returnObject = new Integer(value.toString());
          else if (_type.equals(TypeEnum.LONG))
            returnObject = new Long(value.toString());
          else if (_type.equals(TypeEnum.DOUBLE))
            returnObject = new Double(value.toString());
          else
            Assert.expect(false);
        }
        catch (NumberFormatException nfe)
        {
          InvalidTypeDatastoreException dex = new InvalidTypeDatastoreException(_key, _fileName, _type, value.toString());
          dex.initCause(nfe);
          throw dex;
        }
      }
    Assert.expect(returnObject != null);
    return returnObject;
  }  
}
