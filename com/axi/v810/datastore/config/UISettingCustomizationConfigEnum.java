/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class UISettingCustomizationConfigEnum extends ConfigEnum implements Serializable
{

  private static int _index = -1;
  private static final String _DEFAULT_CUSTOMIZE_CONFIG = FileName.getCustomizeConfigurationFullPath(Config.getInstance().getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE));

  private static List<ConfigEnum> _customizeSettingConfigEnum = new ArrayList<ConfigEnum>();

  private Object _defaultValue;

  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the customize setting
   * config file
   * @param filename String which holds the customize setting configuration
   * filename.
   * @param type TypeEnum which determines the type of value associated with
   * this key.
   * @param valid array of Objects which holds what values are valid. (can be
   * null)
   * @author weng-jian.eoh
   */
  protected UISettingCustomizationConfigEnum(int id,
    String key,
    String filename,
    TypeEnum type,
    Object[] valid,
    Object defaultValue)
  {
    super(id,
      key,
      filename,
      type,
      valid);
    _defaultValue = defaultValue;
    _customizeSettingConfigEnum.add(this);
  }

  public static final UISettingCustomizationConfigEnum CUSTOMIZE_MAIN_UI
    = new UISettingCustomizationConfigEnum(++_index,
      "useCustomizeMainUISetting",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMIZE_UI_SHOW_NUMBERING
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeUISettingShowNumbering",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMIZE_UI_SHOW_VARIATION
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeUISettingShowVariation",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMISE_UI_SHOW_CUSTOMER_NAME
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeUiSettingShowCustomerName",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMIZE_UI_SHOW_VERSION
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeUISettingShowVersion",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMISE_UI_SHOW_MODIFIED_DATE
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeUiSettingShowLastModifiedDate",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  public static final UISettingCustomizationConfigEnum CUSTOMISE_UI_SHOW_SYSTEMTYPE
    = new UISettingCustomizationConfigEnum(++_index,
      "customizeSettingShowSystemType",
      _DEFAULT_CUSTOMIZE_CONFIG,
      TypeEnum.BOOLEAN,
      new Object[]
      {
        new Boolean(true), new Boolean(false)
      },
      true);

  /**
   * @return a List of all ConfigEnums available.
   * @author weng-jian.eoh
   */
  public static List<ConfigEnum> getCustomizeSettingConfigEnums()
  {
    Assert.expect(_customizeSettingConfigEnum != null);

    return _customizeSettingConfigEnum;
  }

  /**
   * @author weng-jian.eoh
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_customizeSettingConfigEnum != null);

    return _customizeSettingConfigEnum;
  }

  /*
   * @author weng-jian.eoh
   */
  protected Object getDefaultValue()
  {
    return _defaultValue;
  }
}
