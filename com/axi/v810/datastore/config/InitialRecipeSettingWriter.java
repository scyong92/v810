package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.Map.Entry;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 * @return
 */
public class InitialRecipeSettingWriter
{
  private static InitialRecipeSettingWriter _instance;

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static synchronized InitialRecipeSettingWriter getInstance()
  {
    if (_instance == null)
      _instance = new InitialRecipeSettingWriter();

    return _instance;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private InitialRecipeSettingWriter()
  {
    
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void write(String fullPathAndName,Map<RecipeAdvanceSettingEnum,Object> initialRecipeSetting) throws DatastoreException
  {
    Assert.expect(fullPathAndName != null);
    Assert.expect(initialRecipeSetting != null);

//    String fullPathAndName = null;
//    if (UnitTest.unitTesting())
//      fullPathAndName = fileName;
//    else
//      fullPathAndName = FileName.getInitialThresholdsFullPath(fileName);

    // make sure the initialThresholds directory exists here
    if (FileUtilAxi.existsDirectory(Directory.getInitialRecipeSettingDir()) == false)
    {
      FileUtilAxi.createDirectory(Directory.getInitialRecipeSettingDir());
    }

    FileWriter fw = null;
    try
    {
      fw = new FileWriter(fullPathAndName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(fullPathAndName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: " + fullPathAndName);
    os.println("# PURPOSE: To store the initial recipe setting used when importing new project.");
    os.println("#");
    os.println("#");
    os.println("# Each line specifies recipe setting");
    os.println("#");
    os.println("# SYNTAX:");
    os.println("# RecipeSetting Value");
    os.println("#");
    os.println("########################################################################");
    os.println("# RecipeSetting Value");

    for ( Entry<RecipeAdvanceSettingEnum,Object> entry : initialRecipeSetting.entrySet())
    {
      RecipeAdvanceSettingEnum recipeSetting = entry.getKey();
      Object value = entry.getValue();
      

      os.println(recipeSetting.getName() + "=" + value);
    }

    os.close();
  }

}
