package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class CannotConvertLocalizationFileDatastoreException extends DatastoreException
{
  public CannotConvertLocalizationFileDatastoreException(String fileName, String invalidEncoding)
  {
    super(new LocalizedString("DS_CANNOT_CONVERT_LOCALIZATION_FILE_KEY", new Object[]{fileName, invalidEncoding}));
    Assert.expect(fileName != null);
    Assert.expect(invalidEncoding != null);
  }
}
