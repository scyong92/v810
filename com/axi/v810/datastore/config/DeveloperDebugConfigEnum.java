package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author chin-seong.kee
 * description: this class is for developer debug purpose only,
 *              wont expose to N user
 */
public class DeveloperDebugConfigEnum extends ConfigEnum implements Serializable
{
    
  private static int _index = -1;
  private static final String _DEVELOPER_DEBUG_CONFIG = FileName.getDeveloperDebugConfigFullPath();

  private static List<ConfigEnum> _developerDebugConfigEnums = new ArrayList<ConfigEnum>();

  protected DeveloperDebugConfigEnum(int id,
                               String key,
                               String filename,
                               TypeEnum type,
                               Object[] valid)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _developerDebugConfigEnums.add(this);
  }

  public static final DeveloperDebugConfigEnum IMAGING_CHAIN_PROGRAM_GENERATOR =
      new DeveloperDebugConfigEnum(++_index,
                             "imagingChainProgramGenerator",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final DeveloperDebugConfigEnum IMAGING_CHAIN_PROGRAM_GENERATOR_SCANPATH_ONLY =
      new DeveloperDebugConfigEnum(++_index,
                             "imagingChainProgramGeneratorScanPathOnly",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final DeveloperDebugConfigEnum IMAGING_CHAIN_PROGRAM_GENERATOR_SUMMARY_ONLY =
      new DeveloperDebugConfigEnum(++_index,
                             "imagingChainProgramGeneratorSummaryOnly",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});

  public static final DeveloperDebugConfigEnum HOMOGENOUSIMAGEGROUP_STEPSIZE =
      new DeveloperDebugConfigEnum(++_index,
                             "homogenousImageGroupStepSize",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
    public static final DeveloperDebugConfigEnum DITHER_AMPLITUDE =
      new DeveloperDebugConfigEnum(++_index,
                             "ditherAmplitude",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(0));

  public static final DeveloperDebugConfigEnum ENABLE_TESTPROGRAM_SCANPATH_REVIEWED =
       new DeveloperDebugConfigEnum(++_index,
                             "enableTestProgramScanPathReviewed",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});   

  public static final DeveloperDebugConfigEnum ENABLE_PSH_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "enablePSHDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});   

  public static final DeveloperDebugConfigEnum GENERATE_TESTPROGRAM_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "generateTestProgramDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});   

  public static final DeveloperDebugConfigEnum DEBUG_BOARD_Z_OFFSET =
       new DeveloperDebugConfigEnum(++_index,
                             "debugBoardZOffset",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
  public static final DeveloperDebugConfigEnum ENABLE_PSP_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "enableDebugPsp",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    public static final DeveloperDebugConfigEnum OPTICAL_IMAGES_PRODUCER_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "OpticalImagesProducerDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
    public static final DeveloperDebugConfigEnum PROJECTION_PRODUCER_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ProjectionProducerDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
    
    public static final DeveloperDebugConfigEnum SAVE_IMAGE_TO_DISK_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "SaveImageToDiskDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});    
    
    public static final DeveloperDebugConfigEnum RECONSTRUCTION_REGIONS_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ReconstructionRegionsDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});   
    
    public static final DeveloperDebugConfigEnum ALGORITHM_FEATURE_UTIL_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "AlgorithmFeatureUtil",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});      
    
    public static final DeveloperDebugConfigEnum EXPOSED_PAD_MEASUREMENT_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ExposedPadMeasurementDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false}); 
    
    public static final DeveloperDebugConfigEnum SURFACE_MODEL_BREAKPOINT_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "SurfaceModelBreakPointDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});        
    
    public static final DeveloperDebugConfigEnum REVIEW_MEASUREMENT_PANEL_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ReviewMeasurementPanelDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
    
    public static final DeveloperDebugConfigEnum XRAY_SAFETY_TEST_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "XraySafetyTestDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
    
    public static final DeveloperDebugConfigEnum REMOTE_MOTION_SERVER_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "RemoteDigitalIODebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});    
    
    public static final DeveloperDebugConfigEnum IMAGE_RENDERER_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ImageRendererDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});   

    public static final DeveloperDebugConfigEnum MEMORY_UTIL_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "MemoryUtilDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false}); 
    
     public static final DeveloperDebugConfigEnum THIN_PLATE_SPLINE_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "ThinPlateSplineDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});    

     public static final DeveloperDebugConfigEnum LEARNING_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "LearningDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});    
     
     public static final DeveloperDebugConfigEnum INSPECTION_ENGINE_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "InspectionEngineDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});         
     
     public static final DeveloperDebugConfigEnum CHECKSUM_TIMING_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "CheckSumTimingDebug",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});        

  public static final DeveloperDebugConfigEnum ENABLE_ZHEIGHT_ESTIMATOR_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "enableDebugZHeightEstimator",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
 
  public static final DeveloperDebugConfigEnum ENABLE_CDNA_BENCHMARK_DEBUG =
       new DeveloperDebugConfigEnum(++_index,
                             "enableDebugCDNABenchmark",
                             _DEVELOPER_DEBUG_CONFIG,
                             TypeEnum.BOOLEAN,
                             new Object[]{true, false});
  
  // Below are samples only
    // As of 19-Dec-2011, only boolean are supported in UI. All default values in this class will be false.
//  public static final DeveloperDebugConfigEnum PROGRAM_GENERATION =
//      new DeveloperDebugConfigEnum(++_index,
//                             "Program Generation",
//                             _DEVELOPER_DEBUG_CONFIG,
//                             TypeEnum.BOOLEAN,
//                             new Object[]{true, false});
//  
//  
//    public static final DeveloperDebugConfigEnum MODIFY_SUBTYPE =
//      new DeveloperDebugConfigEnum(++_index,
//                             "Modify Subtype",
//                             _DEVELOPER_DEBUG_CONFIG,
//                             TypeEnum.BOOLEAN,
//                             new Object[]{true, false});
//    
//    public static final DeveloperDebugConfigEnum TEST_EXECUTION =
//      new DeveloperDebugConfigEnum(++_index,
//                             "Test Execution",
//                             _DEVELOPER_DEBUG_CONFIG,
//                             TypeEnum.BOOLEAN,
//                             new Object[]{true, false});
//    
//    public static final DeveloperDebugConfigEnum TEST_EXECUTION_2 =
//      new DeveloperDebugConfigEnum(++_index,
//                             "Test Execution 2",
//                             _DEVELOPER_DEBUG_CONFIG,
//                             TypeEnum.BOOLEAN,
//                             new Object[]{true, false});
//    
//    public static final DeveloperDebugConfigEnum TEST_DEVELOPMENT =
//      new DeveloperDebugConfigEnum(++_index,
//                             "Test Development",
//                             _DEVELOPER_DEBUG_CONFIG,
//                             TypeEnum.BOOLEAN,
//                             new Object[]{true, false});
    
  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getDeveloperDebugConfigEnum()
  {
    Assert.expect(_developerDebugConfigEnums != null);

    return _developerDebugConfigEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_developerDebugConfigEnums != null);

    return _developerDebugConfigEnums;
  }
    
  /**
   * @author Bill Darbie
   */
  public static boolean isConfigEnumExists(ConfigEnum currentConfigEnum)
  {
    Assert.expect(currentConfigEnum != null);

    for (ConfigEnum configEnum : _developerDebugConfigEnums)
      if (configEnum.equals(currentConfigEnum))
        return true;
    return false;
  }
  
  /**
   * @author Wei Chin
   */
  protected Object getDefaultValue()
  {
    if(this == DITHER_AMPLITUDE)
    {
      return new Integer(53);
    }
    else
    {
      return new Boolean(false);
    }
  }

}
