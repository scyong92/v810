package com.axi.v810.datastore.config;

import com.axi.util.*;

/**
 * This class handles the configuration of the production tuning.  Which projects, their counts, etc.
 * @author Andy Mechtenberg
 */
public class ProductionTuneData implements Comparable
{
  private String _projectName;
  private String _imageDestination;
  private String _resultsDestination;
  private int _currentCount;
  private int _reEnableThreshold;
  private int _reEnableCount;
  private String _notificationEmailAddress;

  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  public ProductionTuneData(String projectName,
                            String imageDestination,
                            String resultsDestination,
                            int currentCount,
                            int reEnableThreshold,
                            int reEnableCount,
                            String notificationEmailAddress)
  {
    // asserts are all done in the set methods
    _configObservable.setEnabled(false);
    setProjectName(projectName);
    setImageDestination(imageDestination);
    setResultsDestination(resultsDestination);
    setCurrentCount(currentCount);
    setReEnableThreshold(reEnableThreshold);
    setReEnableCount(reEnableCount);
    setNotificationEmailAddress(notificationEmailAddress);
    _configObservable.setEnabled(true);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);

    return _projectName;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setProjectName(String projectName)
  {
    Assert.expect(projectName != null);
    _projectName = projectName;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getImageDestination()
  {
    Assert.expect(_imageDestination != null);

    return _imageDestination;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setImageDestination(String imageDestination)
  {
    Assert.expect(imageDestination != null);
    _imageDestination = imageDestination;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getResultsDestination()
  {
    Assert.expect(_resultsDestination != null);

    return _resultsDestination;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setResultsDestination(String resultsDestination)
  {
    Assert.expect(resultsDestination != null);
    _resultsDestination = resultsDestination;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getCurrentCount()
  {
    return _currentCount;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCurrentCount(int currentCount)
  {
    _currentCount = currentCount;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getReEnableThreshold()
  {
    return _reEnableThreshold;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setReEnableThreshold(int reEnableThreshold)
  {
    _reEnableThreshold = reEnableThreshold;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getReEnableCount()
  {
    return _reEnableCount;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setReEnableCount(int reEnableCount)
  {
    _reEnableCount = reEnableCount;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getNotificationEmailAddress()
  {
    Assert.expect(_notificationEmailAddress != null);

    return _notificationEmailAddress;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setNotificationEmailAddress(String notificationEmailAddress)
  {
    Assert.expect(notificationEmailAddress != null);
    _notificationEmailAddress = notificationEmailAddress;
    _configObservable.stateChanged(this);
  }

  /**
   * XCR-3007 Assert when press undo after add production tuning recipe
   * 
   * We implement Comparable so that we can perform comparison in ProductionTuneManager
   * to identify required object in _productionTuneData list.
   * 
   * @author Cheah Lee Herng
   */
  public int compareTo(Object object)
  {
    if (!(object instanceof ProductionTuneData))
    {
      throw new ClassCastException("Wrong class used in ProductionTuneData.compareTo()");
    }
    
    if (this == object)
    {
      return 0;
    }
    
    ProductionTuneData productionTuneData = (ProductionTuneData) object;
    
    if (_projectName.equalsIgnoreCase(productionTuneData.getProjectName()) == false)
    {
      return _projectName.compareTo(productionTuneData.getProjectName());
    }
    
    if (_imageDestination.equalsIgnoreCase(productionTuneData.getImageDestination()) == false)
    {
      return _imageDestination.compareTo(productionTuneData.getImageDestination());
    }
    
    if (_resultsDestination.equalsIgnoreCase(productionTuneData.getResultsDestination()) == false)
    {
      return _resultsDestination.compareTo(productionTuneData.getResultsDestination());
    }
    
    if ((_currentCount == productionTuneData.getCurrentCount()) == false)
    {
      if (_currentCount < productionTuneData.getCurrentCount())
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
    
    if ((_reEnableThreshold == productionTuneData.getReEnableThreshold()) == false)
    {
      if (_reEnableThreshold < productionTuneData.getReEnableThreshold())
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
    
    if ((_reEnableCount == productionTuneData.getReEnableCount()) == false)
    {
      if (_reEnableCount < productionTuneData.getReEnableCount())
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
    
    if (_notificationEmailAddress.equalsIgnoreCase(productionTuneData.getNotificationEmailAddress()) == false)
    {
      return _notificationEmailAddress.compareTo(productionTuneData.getNotificationEmailAddress());
    }
    
    return 1;
  }
}
