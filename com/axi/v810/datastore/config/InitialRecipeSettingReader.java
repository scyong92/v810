package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 * @return
 */
public class InitialRecipeSettingReader
{
  private static InitialRecipeSettingReader _instance;
  private Map<RecipeAdvanceSettingEnum,Object> _recipeSettingEnumToValueMap;
  private Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");

  private Pattern _pattern = Pattern.compile("^(.+)=(.+)$");
  private Pattern _arrayPattern = Pattern.compile("^\\[(.+)\\]");

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static synchronized InitialRecipeSettingReader getInstance()
  {
    if (_instance == null)
      _instance = new InitialRecipeSettingReader();

    return _instance;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private InitialRecipeSettingReader()
  {
    
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public Map<RecipeAdvanceSettingEnum,Object> read(String fileNameFullPath) throws DatastoreException
  {
    Assert.expect(fileNameFullPath != null);

    _recipeSettingEnumToValueMap = new HashMap<RecipeAdvanceSettingEnum,Object>();

    FileReader fr = null;
    try
    {
      fr = new FileReader(fileNameFullPath);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(fileNameFullPath);
      ex.initCause(fnfe);
      throw ex;
    }

    LineNumberReader is = new LineNumberReader(fr);

    try
    {
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(fileNameFullPath);
          ex.initCause(ioe);
          throw ex;
        }

        if (line == null)
          continue;

        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
          continue;

        // pattern jointTypeEnum algorithmSettingName tunedValueType tunedValue
        matcher = _pattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());

        String recipeSettingString = matcher.group(1);
        String valueStr = matcher.group(2);
                        
        RecipeAdvanceSettingEnum recipeSetting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(recipeSettingString);
                       
        Matcher arrayMatcher = _arrayPattern.matcher(valueStr);
        
        if (arrayMatcher.find())
        {
          List<String> valueList = new ArrayList<String>();
          for (String value : arrayMatcher.group(1).split(","))
          {
            value = value.trim();
            valueList.add(value);
          }
          _recipeSettingEnumToValueMap.put(recipeSetting, valueList);
        }
        else
          _recipeSettingEnumToValueMap.put(recipeSetting, valueStr);
      }
      while (line != null);

    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
    Assert.expect(_recipeSettingEnumToValueMap != null);
    return _recipeSettingEnumToValueMap;
  }
}
