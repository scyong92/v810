package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.projWriters.*;


/**
 * @author Bill Darbie
 */
public class JointTypeAssignmentWriter
{
  private static JointTypeAssignmentWriter _instance;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill Darbie
   */
  public static synchronized JointTypeAssignmentWriter getInstance()
  {
    if (_instance == null)
      _instance = new JointTypeAssignmentWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private JointTypeAssignmentWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public void write(String fileName, Collection<JointTypeEnumRule> jointTypeEnumRules) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(jointTypeEnumRules != null);

    String fullPathAndName = FileName.getJointTypeAssignmentFullPath(fileName);
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(fullPathAndName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(fullPathAndName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: " + fileName);
    os.println("# PURPOSE: To store the rules used for assigning JointTypes to each LandPatternPad.");
    os.println("#");
    os.println("#");
    os.println("# Each line is a rule that matches to the joint type specified at the end of the line");
    os.println("# Each rule is looked at from the top of the list to the end, the first line to");
    os.println("# match determines the joint type assignment");
    os.println("#");
    os.println("# SYNTAX for each rule:");
    os.println("# enabled|disabled \"reference designator\" \"total # pads\" \"number of rows\" \"number of cols\" \"land pattern name\" \"pad in center of row or column\" \"jointType\"");
    os.println("#");
    os.println("# All entires that are not numbers use regular expressions.");
    os.println("#");
    os.println("########################################################################");
    os.println("# enabled|disabled \"reference designator\" \"total # pads\" \"number of rows\" \"number of cols\" \"land pattern name\" \"pad in center of row or column\" \"jointType\"");

    for (JointTypeEnumRule rule : jointTypeEnumRules)
    {
      String enabled = "enabled";
      if (rule.isEnabled() == false)
        enabled = "disabled";
      String refDes = "";
      if (rule.isRefDesPatternSet())
        refDes = rule.getRefDesPattern();
      String numPads = "";
      if (rule.isNumPadsSet())
        numPads = rule.getNumPadsPattern();
      String numRows = "";
      if (rule.isNumRowsSet())
        numRows = rule.getNumRowsPattern();
      String numCols = "";
      if (rule.isNumColsSet())
        numCols = rule.getNumColsPattern();
      String landPatternName = "";
      if (rule.isLandPatternNamePatternSet())
        landPatternName = rule.getLandPatternNamePattern();
      String isPadInCenterOfRowOrColumn = "";
      if (rule.isPadOneInCenterOfRowSet())
        isPadInCenterOfRowOrColumn = Boolean.toString(rule.isPadOneInCenterOfRow());
      String jointTypeEnumString = _enumStringLookup.getJointTypeString(rule.getJointTypeEnum());

      os.println(enabled + " \"" + refDes + "\" \"" + numPads + "\" \"" + numRows + "\" \"" + numCols + "\" \"" + landPatternName + "\" \"" + isPadInCenterOfRowOrColumn + "\" \"" + jointTypeEnumString + "\"");
    }

    os.close();
  }
  
}
