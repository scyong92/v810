package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 *
 * An example of the file syntax is:
 *
 * ###########################################################################
 * # Serial Numbers to be skipped
 * #
 * # If the line is simple then it does case insensitive
 * # matching and accepts the following wild cards:
 * #
 * #   ? - matches one of any character
 * #   * - matches one or more of any character
 * #
 * # simple
 * # 1234
 * #
 * # simple
 * # 1234?
 * #
 * # simple
 * # 1234*
 * #
 * # If a line begines with "regEx" then it does full regular expression matching
 * #
 * # regEx
 * # 12\d\w
 * #
 * # regEx
 * # 12[a|b][a-z]*
 * #
 * ###########################################################################
 *
 * @author Bill Darbie
 * @author Michael Tutkowski
 */
public class SerialNumbersListReader
{
  private static Map<String, SerialNumbersListReader> _fileNameToSerialNumbersListReader = new HashMap<String, SerialNumbersListReader>();

  private List<SerialNumberRegularExpressionData> _serialNumbersToSkipRegularExpressionData = new ArrayList<SerialNumberRegularExpressionData>();
  private String _fileName;

  Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");


  /**
   * @author Michael Tutkowski
   */
  public static synchronized SerialNumbersListReader getInstance(String fileName)
  {
    Assert.expect(fileName != null);

    SerialNumbersListReader serialNumbersListReader = (SerialNumbersListReader)_fileNameToSerialNumbersListReader.get(fileName);
    if (serialNumbersListReader == null)
    {
      serialNumbersListReader = new SerialNumbersListReader(fileName);
      _fileNameToSerialNumbersListReader.put(fileName, serialNumbersListReader);
    }

    Assert.expect(serialNumbersListReader != null);
    return serialNumbersListReader;
  }

  /**
   * @author Michael Tutkowski
   */
  private SerialNumbersListReader(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }

  /**
   * Parse in the files contents for later use.
   *
   * @author Bill Darbie
   * @author Michael Tutkowski
   */
  private void readSerialNumbersListFile() throws DatastoreException
  {
    Assert.expect(_fileName != null);

    FileReader fr = null;

    try
    {
      fr = new FileReader(_fileName);
    }
    catch (FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(_fileName);

      ex.initCause(fnfe);

      throw ex;
    }

    _serialNumbersToSkipRegularExpressionData.clear();

    LineNumberReader is = new LineNumberReader(fr);

    try
    {
      String line = null;
      int i = 0;
      String simpleOrRegEx = "";
      String serialNum = "";

      do
      {
        try
        {
          line = is.readLine();
        }
        catch (IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(_fileName);

          ex.initCause(ioe);

          throw ex;
        }

        if (line == null)
          continue;

        // if the line is empty or a comment continue to the next line
        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
          continue;

        line = line.replaceAll("\\\\#", "#");

        if (i == 0)
        {
          simpleOrRegEx = "";
          serialNum = "";
        }

        ++i;
        if (i == 1)
        {
          simpleOrRegEx = line;
        }
        else if (i == 2)
        {
          serialNum = line;
          i = 0;
        }
        else
          Assert.expect(false);

        if (i > 0)
          continue;

        boolean interpretAsRegEx = false;
        simpleOrRegEx = simpleOrRegEx.trim();

        if (simpleOrRegEx.equalsIgnoreCase("regEx"))
        {
          // make sure the regular expression is valid
          try
          {
            Pattern.compile(serialNum);
          }
          catch (PatternSyntaxException pse)
          {
            FileCorruptDatastoreException de = new FileCorruptDatastoreException(_fileName, is.getLineNumber());
            de.initCause(pse);
            throw de;
          }
          interpretAsRegEx = true;
        }
        else if (simpleOrRegEx.equalsIgnoreCase("simple"))
        {
          interpretAsRegEx = false;
        }
        else
        {
          FileCorruptDatastoreException de = new FileCorruptDatastoreException(_fileName, is.getLineNumber() - 1);
          throw de;
        }


        SerialNumberRegularExpressionData data = new SerialNumberRegularExpressionData(serialNum,
                                                                                         interpretAsRegEx);

        _serialNumbersToSkipRegularExpressionData.add(data);
      }
      while (line != null);
    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
  }

  /**
   * @return a Collection of all the serial number regular expressions found in the config file
   * along with how the expression should be interpreted and which project name it maps to
   *
   * @author Bill Darbie
   */
  public Collection<SerialNumberRegularExpressionData> getCollectionOfSerialNumberExpressionData() throws DatastoreException
  {
    readSerialNumbersListFile();

    return _serialNumbersToSkipRegularExpressionData;
  }
}
