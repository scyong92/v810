package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;

/**
 * This file format defines the rules used by the results processor. Each Rule contains Conditions that define
 * when the rest of the rule is used. The rest of the rule defines which file is copied to which location.
 *
 * A Condition defines a condition key and an expression to match. If multiple conditions with the same key exist, the
 * condition is matched if ANY of the conditions are met. For simple expressions, a comma (,) can be used to the same effect.
 *
 * Comment lines begin with a '#'.
 *
 * This defines version 1.0
 * A reader of version x.y can read all versions x.z where z<=y.
 *
 * {Header}
 * Version = <number>
 * RepairToolFRootDirectory = <path>
 * RepairToolEnabled = (true|false)
 *
 * {Rule}
 * Rule <name>
 * Enabled = (true|false)
 * [Condition <keyEnumName> = [regex] <expression>]*
 * [Copy <sourceEnumName> to <directory path>]*
 *
 * @author Patrick Lacz
 */
public class ResultsProcessorWriter
{

  private static final String _VERSION = "1.0";
  private static final String _VERSION_ID = "Version";

  private static final String _REPAIR_TOOL_DIRECTORY = "RepairToolRootDirectory";
  private static final String _REPAIR_TOOL_ENABLED = "RepairToolEnabled";

  private static final String _IS_ENABLED = "Enabled";


  private static EnumStringLookup _enumStringLookup = EnumStringLookup.getInstance();

  /**
   * @author Patrick Lacz
   */
  public ResultsProcessorWriter()
  {
  }

  private static ResultsProcessorWriter _instance = null;

  /**
   * @author Patrick Lacz
   */
  public static synchronized ResultsProcessorWriter getInstance()
  {
    if (_instance == null)
      _instance = new ResultsProcessorWriter();

    return _instance;
  }

  /**
   * @author Patrick Lacz
   */
  public static void writeConfigurationFile() throws DatastoreException
  {
    ResultsProcessor resultsProcessor = ResultsProcessor.getInstance();
    Assert.expect(resultsProcessor != null);

    String configurationFileName = FileName.getResultsProcessingConfigFullPath();

    FileWriter fileWriter = null;
    try
    {
      fileWriter = new FileWriter(configurationFileName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(configurationFileName);
      ex.initCause(ioe);
      throw ex;
    }

    PrintWriter out = new PrintWriter(fileWriter);

    out.println("#######################################################################");
    out.println("# FILE: " + FileName.getResultsProcessingConfigFile());
    out.println("# PURPOSE: To store all the rules for copying results files to");
    out.println("#          other directories after an inspection. The parameters for");
    out.println("#          deleting old results are also specified here.");
    out.println("#");
    out.println("#");
    out.println("# Comment lines begin with a '#'.");
    out.println("#");
    out.println("# This defines version 1.0");
    out.println("#");
    out.println("# {Header}");
    out.println("# "+ _VERSION_ID + " = <number>");
    out.println("# " + _REPAIR_TOOL_DIRECTORY + " = <path>");
    out.println("# " + _REPAIR_TOOL_ENABLED + " = (true|false)");
    out.println("#");
    out.println("# {Rule}");
    out.println("# Rule <name>");
    out.println("# " + _IS_ENABLED + " = (true|false)");
    out.println("# [Condition <keyEnumName> (regex|simple) <expression>]*");
    out.println("# [Copy <sourceEnumName> <directory path>]*");
    out.println("#");
    out.println("########################################################################");

    out.println("");
    out.println(_VERSION_ID + " = " + _VERSION );

    out.println(_REPAIR_TOOL_DIRECTORY + " = " + resultsProcessor.getRepairToolDirectory());
    out.println(_REPAIR_TOOL_ENABLED + " = " + String.valueOf(resultsProcessor.getIsRepairToolRuleEnabled()));
    out.println("");

    for (Rule rule : resultsProcessor.getRuleList())
    {
      out.println("");
      writeRuleConfiguration(rule, out);
    }

    try
    {
      fileWriter.flush();
      fileWriter.close();
    }
    catch(IOException ioe)
    {
      CannotCloseFileDatastoreException ex = new CannotCloseFileDatastoreException(configurationFileName);
      ex.initCause(ioe);
      throw ex;
    }
  }

  /**
   * @author Patrick Lacz
   */
  public static void writeRuleConfiguration(Rule rule, PrintWriter out)
  {
    Assert.expect(rule != null);
    Assert.expect(out != null);

    out.println("Rule " + rule.getName());
    out.println(_IS_ENABLED + " = " + rule.getIsEnabled());
    for (Condition condition : rule.getConditions())
    {
      String conditionPrefix = "\tCondition " + _enumStringLookup.getConditionKeyString(condition.getConditionKeyEnum())
                               + " " + condition.getExpressionType() + " ";
      for (String pattern : condition.getDefintionExpressionList())
        out.println(conditionPrefix + pattern);
    }
    for (Target target : rule.getTargets())
    {
      String targetPrefix = "\tCopy " + _enumStringLookup.getResultFileTypeString(target.getResultFileTypeEnum()) + " ";
      for (String destination : target.getDestinationList())
        out.println(targetPrefix + destination);
    }
  }
}
