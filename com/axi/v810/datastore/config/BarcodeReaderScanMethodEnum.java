package com.axi.v810.datastore.config;

import com.axi.util.*;
import java.io.*;

/**
 *
 * @author siew-yeng.phang
 */
public class BarcodeReaderScanMethodEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private String _name;

  public static final BarcodeReaderScanMethodEnum SCAN_BEFORE_BOARD_LOAD = new BarcodeReaderScanMethodEnum(++_index, "scanBeforeLoadBoard");
  public static final BarcodeReaderScanMethodEnum SCAN_DURING_BOARD_LOADING = new BarcodeReaderScanMethodEnum(++_index, "scanDuringBoardLoading");

  /**
   * @author Phang Siew Yeng
   */
  private BarcodeReaderScanMethodEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name =  name.intern();
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public String toString()
  {
    return _name;
  }
}
