package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;


/**
 * @author Bill Darbie
 */
public class SerialNumberToProjectWriter
{
  private static SerialNumberToProjectWriter _instance;
  private String _fileName = FileName.getSerialNumberToProjectConfigFullPath();

  /**
   * @author Bill Darbie
   */
  public static synchronized SerialNumberToProjectWriter getInstance()
  {
    if (_instance == null)
      _instance = new SerialNumberToProjectWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SerialNumberToProjectWriter()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public void write(Collection<SerialNumberRegularExpressionData> serialNumberRegularExpressionDataCollection) throws DatastoreException
  {
    Assert.expect(serialNumberRegularExpressionDataCollection != null);

    // dont forget to call isExpressionValid on each item before writing anything
    // if they are not all valid do not write anything
    Iterator<SerialNumberRegularExpressionData> it = serialNumberRegularExpressionDataCollection.iterator();
    while (it.hasNext())
    {
      SerialNumberRegularExpressionData data = (SerialNumberRegularExpressionData)it.next();
    }

    FileWriter fw = null;
    try
    {
      fw = new FileWriter(_fileName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(_fileName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: serialNumToProject.config");
    os.println("# PURPOSE: To store all serial number regular expression to");
    os.println("#          project name data.  This information is used to look");
    os.println("#          up which project should be used based on the serial number");
    os.println("#          entered by hand or with a bar code reader.");
    os.println("#");
    os.println("#");
    os.println("# Each entry consists of three lines.");
    os.println("# line1: simple|regEx");
    os.println("# line2: serial number string to match");
    os.println("# line3: the project name");
    os.println("# line4: the variation name");
    os.println("#");
    os.println("# If it is a simple line, then the serial number a project are a case insensitive match");
    os.println("# simple matching accepts the following wild cards:");
    os.println("#");
    os.println("#   ? - matches one of any character");
    os.println("#   * - matches one or more of any character");
    os.println("#");
    os.println("# Here are some examples:");
    os.println("# simple");
    os.println("# 1234");
    os.println("# projectName1");
    os.println("# variationName1");
    os.println("#");
    os.println("# simple");
    os.println("# 1234?");
    os.println("# projectName2");
    os.println("# variationName2");
    os.println("#");
    os.println("# simple");
    os.println("# 1234*");
    os.println("# projectName3");
    os.println("# variationName3");
    os.println("#");
    os.println("# If it is regEx then it does full regular expression matching");
    os.println("#");
    os.println("# regEx");
    os.println("# 12\\d\\w");
    os.println("# projectName4");
    os.println("# variationName4");
    os.println("#");
    os.println("# regEx");
    os.println("# 12[a|b][a-z]*");
    os.println("# projectName5");
    os.println("# variationName5");
    os.println("#");
    os.println("########################################################################");


    it = serialNumberRegularExpressionDataCollection.iterator();
    while (it.hasNext())
    {
      SerialNumberRegularExpressionData data = (SerialNumberRegularExpressionData)it.next();
      if (data.isInterpretedAsRegularExpression())
        os.println("regEx ");
      else
        os.println("simple ");

      String origExpr = data.getOriginalExpression();
      origExpr = origExpr.replaceAll("#", "\\\\#");
      String projName = data.getProjectName();
      projName = projName.replaceAll("#", "\\\\#");
      String variationName = data.getVariationName();
      variationName = variationName.replaceAll("#", "\\\\#");
      os.println(origExpr);
      os.println(projName);
      os.println(variationName);
      
      os.println("");
    }

    os.close();
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   * @author Bill Darbie
   */
  public void setConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }
}
