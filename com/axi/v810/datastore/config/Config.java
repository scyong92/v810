package com.axi.v810.datastore.config;

import com.axi.guiUtil.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.ImagingChainProgramGenerator;
import com.axi.v810.business.license.*;
import com.axi.v810.datastore.*;
import static com.axi.v810.gui.mainMenu.MainMenuGui.finishLicenseMonitor;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for reading and writing all configuration
 * information to the disk.
 *
 * @author Bill Darbie
 */
public class Config
{
  // change this to true to write out hardware.calib as a text file in addition to the binary version
  // when this is true both the binary and the text file will be written out.
  // no matter what this variable is set to, on reads, if the text file exists, it will be read instead of the binary file
  private static boolean _writeTextFileInAdditionToBinary = false;

  private static Config _config = null;
  private static boolean _selftest = false;
  /** @todo rfh - remove this when LP2 goes away*/
  private static boolean _earlyPrototypeHardware = false;
  private static ConfigObservable _configObservable;
  private static Map<String, List<ConfigEnum>> _binaryFileToConfigEnumsMap = new HashMap<String, List<ConfigEnum>>();
  private static Map<SystemTypeEnum, List<String>> _systemTypeToConfigFilePathsMap = new HashMap<SystemTypeEnum, List<String>>();
  private static Map<Integer, Pair<Double, String>> _nominalSteSizeToPassCodeMap = new HashMap<Integer, Pair<Double, String>>(); // XCR1529, New Scan Route, khang-wah.chnee
  private static Map<Integer, Pair<Integer, String>> _xLocationToPassCodeMap = new HashMap<Integer, Pair<Integer, String>>(); // XCR1529, New Scan Route, khang-wah.chnee

  private boolean _checkedForEarlyPrototype = false;
  private boolean _loaded = false;
  private CSVFileReaderAxi _csvFileReaderAxi = new CSVFileReaderAxi(); // XCR1529, New Scan Route, khang-wah.chnee
  private ConfigFileTextReader _configFileTextReader = new ConfigFileTextReader();
  private ConfigFileTextWriter _configFileTextWriter = new ConfigFileTextWriter();
  private ConfigFileBinaryReader _configFileBinaryReader = new ConfigFileBinaryReader();
  private ConfigFileBinaryWriter _configFileBinaryWriter = new ConfigFileBinaryWriter();
  private Map<ConfigEnum, String> _configEnumToPreMacroExpansionValueMap = new HashMap<ConfigEnum, String>();
  private SystemTypeEnum _loadedSystemTypeEnumSetting;
  private boolean _areSystemSpecificConfigFilesInitialized = false;

  // Single Server and 64-bits IRP checking flags
  private static boolean _is64bitsIRP = false;
  private static boolean _isRealTimePshEnabled = false;
  private static boolean _isMultiAnglePostProcessing = false;
  private static final String _IRP_32_BITS_IN_HEXA = "4C01"; // Machine Type of 32bits
  private static final String _IRP_64_BITS_IN_HEXA = "6486"; // Machine Type of 64bits
  private static final String _PORTABLE_EXECUTABLE_IN_HEXA = "50450000"; // The Portable Executable "PE" in hexadecimal
  
  private Object _configKeyLock = new Object();
  
  private static SystemTypeEnum _newSystemType = null;
  private String _previousPath = null;
  private String _previousNDFPath = null;
  private String _previousProjetDatabasePath = null;
  private boolean _hardwareSimulation = false;
  
  // match $(MACRO)
  //                                                      $ (                  )
  private Pattern _macroPattern = Pattern.compile("^(.*)\\$\\(([^$\\s\\(\\)]+)\\)(.*)$");
  
  // Ying-Huan.Chu
  private java.util.List<String> _corruptedBinaryConfigFileNameList = new ArrayList<>();
  /**
   * @author Bill Darbie
   */
  static
  {
    if (UnitTest.unitTesting())
      _selftest = true;

    // force the enumeration classes to be loaded here so the
    // static methods will have data
    try
    {
      Class.forName("com.axi.v810.datastore.config.HardwareCalibEnum");
      Class.forName("com.axi.v810.datastore.config.HardwareConfigEnum");
      Class.forName("com.axi.v810.datastore.config.SoftwareConfigEnum");
      Class.forName("com.axi.v810.datastore.config.DeveloperDebugConfigEnum");
      Class.forName("com.axi.v810.datastore.config.PspCalibEnum");
    }
    catch(ClassNotFoundException cnfe)
    {
      cnfe.printStackTrace();
      Assert.expect(false);
    }

    _binaryFileToConfigEnumsMap.put(FileName.getHardwareCalibFullPath(), HardwareCalibEnum.getHardwareCalibEnums());
    
    //chin-seong,kee - add in for binary file to config map.
    _binaryFileToConfigEnumsMap.put(FileName.getDeveloperDebugConfigFullPath(), DeveloperDebugConfigEnum.getDeveloperDebugConfigEnum());
    
    // Kok Chun, Tan - XCR3062 - psp calibration binary file
    _binaryFileToConfigEnumsMap.put(FileName.getPspCalibFullPath(), PspCalibEnum.getPspCalibEnums());

    _configObservable = ConfigObservable.getInstance();

    // initialize the system specific configuration files
    for(SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
    {
      List<String> filePaths = new LinkedList<String>();
      String systemConfigFilePath = FileName.getSystemConfigFullPath(systemType);
      filePaths.add(systemConfigFilePath);

      Object previous = _systemTypeToConfigFilePathsMap.put(systemType, filePaths);
      Assert.expect(previous == null);
    }
  }

  /**
  * @author Bill Darbie
  */
  private Config()
  {
    // do nothing - make this private so getInstance() must be used
  }

  /**
  * @author Bill Darbie
  */
  public synchronized static Config getInstance()
  {
    if (_config == null)
      _config = new Config();

    return _config;
  }

  /**
  * @author Bill Darbie
  */
  private void checkThatKeyIsInCorrectFile(ConfigEnum configEnum, String key, String configFile) throws KeyInWrongFileDatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(key != null);
    Assert.expect(configFile != null);

    // now check that the key was found in the correct file
    String expectedFile = configEnum.getFileName();
    if ((expectedFile == null) || (expectedFile.equals(configFile) == false))
      throw new KeyInWrongFileDatastoreException(key,expectedFile,configFile);
  }

  /**
  * @author Bill Darbie
  * @author Reid Hayhow
  */
  private boolean checkValidValue(ConfigEnum configEnum, String value)
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    // now check that the key has a valid value
    Object[] validValues = configEnum.getValidValues();
    boolean valid = false;
    boolean doubleRange = false;
    boolean integerRange = false;
    boolean longRange = false;
    boolean booleanRange = false;

    if ((validValues == null) || (validValues.length == 0))
    {
      // we do not have a list of valid values so return now and call it good
      return true;
    }
    else if (validValues.length > 0)
    {
      Object object = validValues[0];
      if (object instanceof Double)
      {
        doubleRange = true;
        Assert.expect(validValues.length == 2);
        Assert.expect(configEnum.getType().equals(TypeEnum.DOUBLE));
      }
      else if (object instanceof Integer)
      {
        integerRange = true;
        Assert.expect(validValues.length == 2);
        Assert.expect(configEnum.getType().equals(TypeEnum.INTEGER));
      }
      else if (object instanceof Long)
      {
        longRange = true;
        Assert.expect(validValues.length == 2);
        Assert.expect(configEnum.getType().equals(TypeEnum.LONG));
      }
      else if (object instanceof Boolean)
      {
        booleanRange = true;
        Assert.expect(validValues.length == 2);
        Assert.expect(configEnum.getType().equals(TypeEnum.BOOLEAN));
      }
      else
      {
        Assert.expect(configEnum.getType().equals(TypeEnum.STRING) || configEnum.getType().equals(TypeEnum.BINARY));
      }
    }
    if (doubleRange)
    {
      try
      {
        double val = Double.parseDouble(value);
        double low = ((Double)validValues[0]).doubleValue();
        double high = ((Double)validValues[1]).doubleValue();
        if ((val >= low) && (val <= high))
          valid = true;
      }
      catch(NumberFormatException ne)
      {
        // do nothing - leave valid to false
      }
    }
    else if (integerRange)
    {
      try
      {
        int val = Integer.parseInt(value);
        int low = ((Integer)validValues[0]).intValue();
        int high = ((Integer)validValues[1]).intValue();
        if ((val >= low) && (val <= high))
          valid = true;
      }
      catch(NumberFormatException ne)
      {
        // do nothing - leave valid to false
      }
    }
    else if (longRange)
    {
      try
      {
        long val = Long.parseLong(value);
        long low = ((Long)validValues[0]).longValue();
        long high = ((Long)validValues[1]).longValue();
        if ((val >= low) && (val <= high))
          valid = true;
      }
      catch(NumberFormatException ne)
      {
        // do nothing - leave valid to false
      }
    }
    else if (booleanRange)
    {
      try
      {
        (Boolean.valueOf(value)).booleanValue();
        valid = true;
      }
      catch(NumberFormatException ne)
      {
        // do nothing - leave valid to false
      }
    }
    else if (configEnum.getType().equals(TypeEnum.BINARY))
    {
      for(int i = 0; i < validValues.length; ++i)
      {
        String validValue = (String)validValues[i];
        if (validValue.equals(value))
          valid = true;
      }
    }
    else
    {
      for(int i = 0; i < validValues.length; ++i)
      {
        String validValue = (String)validValues[i];
        if (validValue.equals(value))
          valid = true;
      }
    }

    return valid;
  }

  /**
  * @author Bill Darbie
  */
  private String checkThatKeyHasAValidValue(ConfigEnum configEnum, String key, String value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(key != null);
    Assert.expect(value != null);

    String newValue = value;
    // now check that the key has a valid value
    boolean valid = checkValidValue(configEnum, value);

    if (valid == false)
      throw new InvalidValueDatastoreException(key, value, configEnum.getFileName());

    return newValue;
  }

  /**
  * @author Bill Darbie
  */
  private void addKeyValuePairToMap(ConfigEnum configEnum, String key, String value) throws InvalidTypeDatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(key != null);
    Assert.expect(value != null);

    // enter the key value pair
    Assert.expect(configEnum.getValue() == null, "Duplicate keys are not allowed. " + key + " was duplicated in file " + configEnum.getFileName());

    Object prev = _configEnumToPreMacroExpansionValueMap.put(configEnum, value);
    Assert.expect(prev == null);
  }

  /**
  * @author Bill Darbie
  */
  private void addKeyValuePairsToMap(String configFile) throws DatastoreException
  {
    Assert.expect(configFile != null);

    Map<String, String> keyValueMap = null;
    boolean extraKey = false;
    
    // if we are writing out both a text and binary version, read the text file since it may have been modified by hand
    if ((_binaryFileToConfigEnumsMap.containsKey(configFile)) && (FileUtilAxi.exists(configFile) == false))
    {
      boolean binaryFileRead = false;
      // check for the existance of the binary file and read it if it exists
      if (FileUtilAxi.exists(configFile + FileName.getBinaryExtension()))
      {
        // read the binary file
        try
        {
          if (UnitTest.unitTesting())
          {
            _configFileBinaryReader.read(configFile, _binaryFileToConfigEnumsMap.get(configFile));
            binaryFileRead = true;
          }
          else
          {
            //previous systemtype config might contain wrong setting, force delete and reload default value
            if (configFile.equals(FileName.getSystemConfigFullPath(XrayTester.getSystemType())) == false || (XrayTester.isOfflineProgramming()) == false)
            {
              _configFileBinaryReader.read(configFile, _binaryFileToConfigEnumsMap.get(configFile));
              binaryFileRead = true;
            }
            else
              FileUtilAxi.delete(configFile + FileName.getBinaryExtension());
          }
        }
        catch (DatastoreException dex)
        {
          // Ying-Huan.Chu [XCR1661]
          //dex.printStackTrace();
          System.out.println(dex.getLocalizedMessage());
          backupOldBinaryConfigFile(configFile);
        }      
      }

      if (binaryFileRead == false)
      {
        // we could not load the serialized file - populate the keyValueMap with default values
        for (ConfigEnum configEnum : _binaryFileToConfigEnumsMap.get(configFile))
        {
          if (_newSystemType == null)
          {
            Object value =configEnum.getDefaultValue();
            Assert.expect(value != null);
            configEnum.setValue(value);
          }
          else
          {
            //load default value of systemconfig based on selected systemtype
            if (configEnum instanceof SystemConfigEnum)
            {
              SystemConfigEnum systemConfigEnum = (SystemConfigEnum)configEnum;
              Object value = systemConfigEnum.getDefaultValue(_newSystemType);
              configEnum.setValue(value);
            }
            else
            {
              Object value = configEnum.getDefaultValue();
              Assert.expect(value != null);
              configEnum.setValue(value);
            }
          }
        }

        // now write out a known good file on top of the bad one and load that
        _configFileBinaryWriter.saveValuesToDisk(_binaryFileToConfigEnumsMap.get(configFile));

        // read in the binary file
        _configFileBinaryReader.read(configFile, _binaryFileToConfigEnumsMap.get(configFile));
      }

      if(_writeTextFileInAdditionToBinary)
      {
        try
        {
          //let write out the text file
          if (FileUtilAxi.exists(configFile))
          {
            _configFileTextWriter.saveValuesToDisk(_binaryFileToConfigEnumsMap.get(configFile));
          }
          else if (isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
          {
            _configFileTextWriter.saveValuesToDiskAsNewFile(configFile, _binaryFileToConfigEnumsMap.get(configFile));
          }
        }
        catch (BusinessException ex)
        {
          // this can happen when trying to determine if we are on a developer system.
          // it does break architecture, but this peice of code is only for development
          // and will not be used by the customer.
          ex.printStackTrace();
        }
      }
    }
    else
    {
      // read the text file
      keyValueMap = _configFileTextReader.getKeyValueMapping(configFile);

      for (Map.Entry<String, String> entry : keyValueMap.entrySet())
      {
        String key = entry.getKey();
        String value = entry.getValue();
        ConfigEnum configEnum = ConfigEnum.getConfigEnum(key);

        // if object is null then the key found in the config file is not available through Config
        if (configEnum == null)
        {
          extraKey = true;
          System.out.println(StringLocalizer.keyToString(new LocalizedString("DS_ERROR_INVALID_KEY_IN_CONFIG_KEY", new Object[]{key, value, configFile})));
        }
        
        //  throw new InvalidKeyInConfigDatastoreException(key, value, configFile);
        if (extraKey == false)
        {
          if (configEnum.getType().equals(TypeEnum.BINARY))
          {
            try
            {
              // convert list of hexidecimal codes into binary (one byte per character)
              value = StringUtil.convertHexCodedStringIntoUnicodeString(value);
            }
            catch (CharConversionException cce)
            {
              throw new InvalidValueDatastoreException(key, value, configEnum.getFileName());
            }
            catch (NumberFormatException nfe)
            {
              throw new InvalidValueDatastoreException(key, value, configEnum.getFileName());
            }
          }

          checkThatKeyIsInCorrectFile(configEnum, key, configFile);
          value = checkThatKeyHasAValidValue(configEnum, key, value);
          addKeyValuePairToMap(configEnum, key, value);
        }
        else
        {
          extraKey = false;
        }
      }
    }
  }

  /**
   * Read info from scanroute.csvconfig, the file should be a csv format file
   * and only consist step size and pass code. 
   * example: 0.00022222, A
   * 
   * function will get the data as a list of String array
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void addScanRouteInfoToMap(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    
    List<String []> dataList = null;    
    try
    {
      dataList = _csvFileReaderAxi.readFile(fileName);
      
      // XCR1533 - Memory leak fix by Lee Herng
      _nominalSteSizeToPassCodeMap.clear();
      
      // put the data into map with the following format: 
      // <id, Pair<StepSize, PassCode>>
      int i = 0;
      for(String [] d: dataList)
      {
        double nominalStepSize = Double.parseDouble(d[0]);
        String passCode = d[1];
        
        Pair<Double, String> pair = new Pair<Double, String>(nominalStepSize,passCode);
        _nominalSteSizeToPassCodeMap.put(i, pair);
        i++;
      }
    }
    catch(Exception ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally   // Bee Hoon - Close scanroute.csvconfig file after read
    {
      try 
      {
        _csvFileReaderAxi.closeFile();
      } 
      catch (IOException ex) 
      {
        System.out.println("Warning: " + ex);
      }
    }
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void calculateXLocationToPassCode()
  {
    // put the data into map with the following format:
    // <id, Pair<XLocation, PassCode>>
    int preFixStepNum = _nominalSteSizeToPassCodeMap.size();
    Pair<Double, String> lastPair = _nominalSteSizeToPassCodeMap.get(preFixStepNum-1);
    double maxLenthInNanometers = XrayTester.getInstance().getMaxImageableAreaLengthInNanoMeters() * 3.0; // just to enlarge the scan pass number.
    int maxAllowableStepSize = ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers();
    double currentXLocationInNanometers = 0;
    int j=0;
    
    // XCR1533 - Memory leak fix by Lee Herng
    _xLocationToPassCodeMap.clear();
    
    // Update map size before proceed
    preFixStepNum = preFixStepNum-1;

    // is ok if we have one more scan pass which is out of max length, no big deal
    while(currentXLocationInNanometers < maxLenthInNanometers)
    {
      currentXLocationInNanometers =  (_nominalSteSizeToPassCodeMap.get(j%preFixStepNum).getFirst()*maxAllowableStepSize);
      currentXLocationInNanometers += ((int)(j/preFixStepNum)*maxAllowableStepSize*lastPair.getFirst());
      String passCode = _nominalSteSizeToPassCodeMap.get(j%preFixStepNum).getSecond();
      Pair<Integer, String> pair = new Pair<Integer, String>((int)currentXLocationInNanometers,passCode);
      _xLocationToPassCodeMap.put(j, pair);
      j++;
    }
  }
  
  /**
   * @author Bill Darbie
   * @author Chin-Seong, Kee
   */
  private void checkForMissingKeys() throws KeyMissingDatastoreException, InvalidTypeDatastoreException
  {
    boolean isDeveloperDebugHasMissingKey = false;
    boolean isBarcodeReaderConfigHasMissingKey = false;
    boolean isCustomiseSettingConfigHasMissingKey = false;
    boolean isCustomMeasurementFileConfigEnumHasMissingKey = false;       //Janan XCR-3551
    // ConfigEnum has all the keys
    for (ConfigEnum configEnum : ConfigEnum.getAllConfigEnums())
    {
      if (configEnum.getValue() == null)
      {
        //if Developer config, just get the default value, for missing key
        if(configEnum instanceof DeveloperDebugConfigEnum)
        {
          String key = configEnum.getKey();
          configEnum.setValue(configEnum.getDefaultValue());
          isDeveloperDebugHasMissingKey = true;
        }
        else if(configEnum instanceof BarcodeReaderConfigEnum)
        {
          //Siew Yeng - get default value for missing key
          configEnum.setValue(configEnum.getDefaultValue());
          isBarcodeReaderConfigHasMissingKey = true;
        }
        else if (configEnum instanceof UISettingCustomizationConfigEnum)
        {
          configEnum.setValue(configEnum.getDefaultValue());
          isCustomiseSettingConfigHasMissingKey = true;
        }
        else if(configEnum instanceof CustomMeasurementFileConfigEnum)
        {
          configEnum.setValue(configEnum.getDefaultValue());
          isCustomMeasurementFileConfigEnumHasMissingKey = true;          
        }
        else
        {
          String configFile = configEnum.getFileName();
          throw new KeyMissingDatastoreException(configEnum.getKey(), configFile);
        }
      }
    }
    
    if(isDeveloperDebugHasMissingKey == true)
    {
      try
      {
      // now write out a known good file on top of the bad one and load that
      _configFileBinaryWriter.saveValuesToDisk(_binaryFileToConfigEnumsMap.get(FileName.getDeveloperDebugConfigFullPath()));

      // read in the binary file
      _configFileBinaryReader.read(FileName.getDeveloperDebugConfigFullPath(), _binaryFileToConfigEnumsMap.get(FileName.getDeveloperDebugConfigFullPath()));
      }
      catch(Exception ex)
      {
        //do Not
      }
    }
    
    if(isBarcodeReaderConfigHasMissingKey)
    {
      try
      {
        String fileName = BarcodeReaderConfigEnum.getBarCodeReaderConfigEnums().get(0).getFileName();
        _configFileTextWriter.updateValueToDiskWithMissingKeys(fileName, BarcodeReaderConfigEnum.getBarCodeReaderConfigEnums());
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
    
    if(isCustomiseSettingConfigHasMissingKey)
    {
      try
      {
        String fileName = UISettingCustomizationConfigEnum.getCustomizeSettingConfigEnums().get(0).getFileName();
        _configFileTextWriter.updateValueToDiskWithMissingKeys(fileName, UISettingCustomizationConfigEnum.getCustomizeSettingConfigEnums());
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
    
    //Janan XCR-3551 
    if(isCustomMeasurementFileConfigEnumHasMissingKey)
    {
      try
      {
        String fileName = CustomMeasurementFileConfigEnum.getCustomMeasurementConfigEnums().get(0).getFileName();
        _configFileTextWriter.updateValueToDiskWithMissingKeys(fileName, CustomMeasurementFileConfigEnum.getCustomMeasurementConfigEnums());
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Force all config files to be loaded off disk again.  In general, this should not be used.
   * @author Bill Darbie
   */
  public synchronized void forceLoad() throws DatastoreException
  {
    _loaded = false;
    loadIfNecessary();
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   *
   *
   */
  public synchronized void forceLoad(SystemTypeEnum Systemtype) throws DatastoreException
  {
    _newSystemType = Systemtype;
    _previousPath = getStringValue(SoftwareConfigEnum.PROJECT_DIR);
    _previousProjetDatabasePath = getStringValue(SoftwareConfigEnum.PROJECT_DATABASE_PATH);
    _previousNDFPath = getStringValue(SoftwareConfigEnum.NDF_LEGACY_DIR);
    _hardwareSimulation = getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    forceLoad();
    
    // for S2EX, default value is set based on m19 if systemType XXL is supported oso
    // hardcoded set the value of specific configenum to ensure M23 setting is loaded.
    if (XrayTester.getSystemType().equals(SystemTypeEnum.S2EX))
    {
      if (Config.getSystemCurrentLowMagnification().equals("M23"))
      {
        SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setValue(323818000);
        SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setValue(4.34782608695652);
        SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setValue(4.34782608695652);
      }
      else if (Config.getSystemCurrentLowMagnification().equals("M19"))
      {
        SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setValue(307827000);
        SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setValue(5.26315789473684);
        SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setValue(5.2631695932);
      }
      
      if (Config.getSystemCurrentHighMagnification().equals("M11"))
      {
        SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setValue(280157000);
        SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setValue(9.09090909090909);
        SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setValue(9.09090909090909);
      }
    }   
  }


  /**
  * Load all the key value pairs from the config files and check that they are all valid.
  * If any keys are missing or any values are invalid, and the key in question
  * does not have a default value the DatastoreException will be thrown.  If any keys
  * are missing or any values are invalid, and a default is available a
  * warning message will be added to the List returned so it can be reported.
  *
  * @author Bill Darbie
  */
  public synchronized void loadIfNecessary() throws DatastoreException
  {
    try
    {
      if (_loaded)
        return;

      ConfigEnum.clear();
      _configEnumToPreMacroExpansionValueMap.clear();

      if (_newSystemType != null)
      {   
        List<String> oldConfigFile = ConfigEnum.getConfigFileNames();
        changeConfigFileDueToDifferentSystemType(oldConfigFile,XrayTester.getSystemType(), _newSystemType);
      }

      List<String> configFiles = ConfigEnum.getConfigFileNames();
      loadConfigFiles(configFiles);

      expandMacrosAndDoSomeChecks();
      _loaded = true;

      if (_newSystemType == null)
      {
        _loadedSystemTypeEnumSetting = getSystemType();
        loadSystemSpecificConfigFiles(_loadedSystemTypeEnumSetting);
      }
      else 
      {
        _loadedSystemTypeEnumSetting = _newSystemType;
        loadSystemSpecificConfigFiles(_loadedSystemTypeEnumSetting);

      }

  //     Must wait until after FALSE_CALL_MONITORING config name has a value defined (loaded)
  //     before the initial loading the barcode reader configuration.
      Assert.expect(getTheValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED) != null);
      try
      {
        Class.forName("com.axi.v810.datastore.config.FalseCallTriggeringConfigEnum");
      }
      catch (ClassNotFoundException cnfe)
      {
        Assert.logException(cnfe);
      }

      if (_newSystemType == null)
      {
        String fileName = FileName.getFalseCallTriggeringConfigFullPath();
        if (configFiles.contains(fileName) == false)
        {
          load(fileName);
        }

        // XCR1529, New Scan Route, khang-wah.chnee
        fileName = FileName.getScanRouteConfigFullPath();
        if (configFiles.contains(fileName) == false)
        {
          load(fileName);
        }

        // Must wait until after barcode reader config name has a value defined (loaded)
        // before the initial loading the barcode reader configuration.
        Assert.expect(getTheValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE) != null);
        try
        {
          Class.forName("com.axi.v810.datastore.config.BarcodeReaderConfigEnum");
        }
        catch (ClassNotFoundException cnfe)
        {
          Assert.logException(cnfe);
        }

        String configName = getStringValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE);
        fileName = FileName.getBarcodeReaderConfigurationFullPath(configName);
        if (configFiles.contains(fileName) == false)
        {
    //      load(fileName);
          //Siew Yeng - XCR-3251 - Barcode keys are not carry foward after install 5.8 patch
          BarcodeReaderManager.getInstance().load(fileName);
          setValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT, configName);
        }

        // set up the AssertUtil information now that the config files have been loaded
        AssertUtil.updateConfigData();

        Assert.expect(getTheValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE) != null);
        try
        {
          Class.forName("com.axi.v810.datastore.config.UISettingCustomizationConfigEnum");
        }
        catch (ClassNotFoundException cnfe)
        {
          Assert.logException(cnfe);
        }

        String customizeConfigName = getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE);
        fileName = FileName.getCustomizeConfigurationFullPath(customizeConfigName);
        if (configFiles.contains(fileName) == false)
        {
          CustomizationSettingReader.getInstance().load(customizeConfigName);
        }

        //Janan XCR-3551
        Assert.expect(getTheValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE) != null);
        try
        {
          Class.forName("com.axi.v810.datastore.config.CustomMeasurementFileConfigEnum");
        }
        catch (ClassNotFoundException cnfe)
        {
          Assert.logException(cnfe);
        }

        fileName = FileName.getCustomMeasurementConfigurationFullPath();
        if (configFiles.contains(fileName) == false)
        {
          CustomMeasurementXMLConfigReader.getInstance().load();
        }        
      }   
      else
      {
        _newSystemType = null;
        this.setValue(SoftwareConfigEnum.PROJECT_DIR, _previousPath);
        this.setValue(SoftwareConfigEnum.NDF_LEGACY_DIR, _previousNDFPath);
        this.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false);
        this.setValue(SoftwareConfigEnum.PROJECT_DATABASE_PATH, _previousProjetDatabasePath);
        this.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, _hardwareSimulation);
        this.setValue(SoftwareConfigEnum.VERIFICATION_DIR, _previousPath);
        this.save(SoftwareConfigEnum.ONLINE_WORKSTATION.getFileName());
      }
    }
    finally
    {
      _newSystemType = null;
    }
    // set up the AssertUtil information now that the config files have been loaded
    AssertUtil.updateConfigData();
  }
  /**
  * Load all the key value pairs from the specified config file and check that they
  * are all valid.  If any keys are missing or any values are invalid, and the key
  * in question does not have a default value the DatastoreException will be thrown.
  * If any keys are missing or any values are invalid, and a default is available a
  * warning message will be added to the List returned so it can be reported.
  * @param pathName is the path to the config file
  * @author Bob Balliew
  */
  public synchronized void load(String pathName) throws DatastoreException
  {
    Assert.expect(pathName != null);

    Assert.expect(_loaded, "the Config.load() method must be called before Config.load(pathName) is called");

    synchronized (_configKeyLock)
    {
      ConfigEnum.clear(pathName);
      _configEnumToPreMacroExpansionValueMap.clear();

      List<String> fn = new ArrayList<String>();
      fn.add(pathName);

      try
      {
        loadConfigFiles(fn);
      }
      catch (DatastoreException e)
      {
        ConfigEnum.undoLastClear();
        throw e;
      }

      expandMacrosAndDoSomeChecks();

      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      isDeveloperDebugOnCheck();

      // set up the AssertUtil information now that the config files have been loaded
      AssertUtil.updateConfigData();
      _configKeyLock.notifyAll();
    }
  }

  /**
  * Load all the key value pairs from the specified (old) config file with key value
  * pairs from the spicified (new) config file and check that they are all valid.
  * If any keys are missing or any values are invalid, and the key in question does
  * not have a default value the DatastoreException will be thrown.  If any keys are
  * missing or any values are invalid, and a default is available a warning message
  * will be added to the List returned so it can be reported.
  * @param oldPathName is the path to the old config file
  * @param newPathName is the path to the new config file
  * @author Bob Balliew
  */
  public synchronized void load(String oldPathName, String newPathName) throws DatastoreException
  {
    Assert.expect(oldPathName != null);
    Assert.expect(newPathName != null);

    Assert.expect(_loaded, "the Config.load() method must be called before Config.load(oldPathName, newPathName) is called");

    ConfigEnum.clear(oldPathName);
    _configEnumToPreMacroExpansionValueMap.clear();

    ConfigEnum.changeFileName(oldPathName, newPathName);

    List<String> fn = new ArrayList<String>();
    fn.add(newPathName);

    try
    {
      loadConfigFiles(fn);
    }
    catch (DatastoreException e)
    {
      ConfigEnum.undoLastClear();
      ConfigEnum.changeFileName(newPathName, oldPathName);
      throw e;
    }

    expandMacrosAndDoSomeChecks();

    // set up the AssertUtil information now that the config files have been loaded
    AssertUtil.updateConfigData();

    /** @todo wpd remove this when LP2 is gone */
    if (_checkedForEarlyPrototype == false)
    {
      assignEarlyProtoHardware();
      if (_earlyPrototypeHardware)
        _writeTextFileInAdditionToBinary = true;
      _checkedForEarlyPrototype = true;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void assignEarlyProtoHardware()
  {
    /** @todo rfh - remove this when LP2 goes away*/
    // Unit tests fail on the build system when calling the LicenseManager. Not sure why
    // but we will bypass it for now.
    if(UnitTest.unitTesting())
    {
      _earlyPrototypeHardware = false;
    }
    // Code is running in deployment mode, check the license to see if it is LP2
    else
    {
      try
      {
        _earlyPrototypeHardware = LicenseManager.isEarlyPrototypeHardwareEnabled();
      }
      catch (BusinessException ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
  * Save all the key value pairs for the specified config file.
  * @param pathName is the path to the config file
  * @author Bob Balliew
  */
  public synchronized void save(String pathName) throws DatastoreException
  {
    Assert.expect(pathName != null);

    Assert.expect(_loaded, "the Config.load() method must be called before Config.save(pathName) is called");

    Map<ConfigEnum, Object> keyToValueMap = new HashMap<ConfigEnum,Object>();
    File file = new File(pathName);

    for (ConfigEnum configEnum : ConfigEnum.getAllConfigEnums())
    {
      if (file.equals(new File(configEnum.getFileName())))
      {
        keyToValueMap.put(configEnum, configEnum.getValue());
      }
    }

    setValues(keyToValueMap);
  }

  /**
  * Save all the key value pairs from the specified (old) config file into
  * the specified (new) config file.
  * @param oldPathName is the path to the old config file
  * @param newPathName is the path to the new config file
  * @author Bob Balliew
  */
  public synchronized void save(String oldPathName, String newPathName) throws DatastoreException
  {
    Assert.expect(oldPathName != null);
    Assert.expect(newPathName != null);

    Assert.expect(_loaded, "the Config.load() method must be called before Config.save(oldPathName, newPathName) is called");

    if ((new File(oldPathName)).equals(new File(newPathName)) == false)
    {
      // ConfigFileWriter requires a complete config exists before it can process
      // modifications.
      FileUtilAxi.copy(oldPathName, newPathName);
      ConfigEnum.changeFileName(oldPathName, newPathName);
    }

    Map<ConfigEnum, Object> keyToValueMap = new HashMap<ConfigEnum,Object>();
    File newFile = new File(newPathName);

    for (ConfigEnum configEnum : ConfigEnum.getAllConfigEnums())
    {
      if (newFile.equals(new File(configEnum.getFileName())))
      {
        keyToValueMap.put(configEnum, configEnum.getValue());
      }
    }

    Assert.expect(keyToValueMap.size() > 0);
    setValues(keyToValueMap);
  }

  /**
   * This method will find any $(MACRO) and replace it with the appropriate text.
   * A macro can be an enviromment variable or another KEY in the config file.
   * Nested macros are supported.
   *
   * @author George David
   */
  private void expandMacros() throws DatastoreException
  {
    Set<String> keyNameSet = new HashSet<String>();

    for (Map.Entry<ConfigEnum, String> entry : _configEnumToPreMacroExpansionValueMap.entrySet())
    {
      ConfigEnum configEnum = entry.getKey();
      // check the the same key is not used twice since this is not allowed
      Assert.expect(keyNameSet.add(configEnum.getKey()), "The same key " + configEnum.getKey() + " exists in more than one config file");
    }

    // do the macros expansion here
    int numChanges = 0;
    do
    {
      numChanges = 0;
      for (Map.Entry<ConfigEnum, String> entry : _configEnumToPreMacroExpansionValueMap.entrySet())
      {
        ConfigEnum configEnum = entry.getKey();
        String value = entry.getValue();
        String newValue = substituteMacrosInValue(configEnum, value);
        if (value.equals(newValue) == false)
        {
          ++numChanges;
          _configEnumToPreMacroExpansionValueMap.put(configEnum, newValue);
        }
      }
    } while (numChanges > 0);

    // now set the values on ConfigEnums here
    for (Map.Entry<ConfigEnum, String> entry : _configEnumToPreMacroExpansionValueMap.entrySet())
    {
      ConfigEnum configEnum = entry.getKey();
      String value = entry.getValue();
      configEnum.setValue(value);
    }
  }

  /**
   * This method will find any $(MACRO) and replace it with the appropriate text.
   * A macro can be an enviromment variable or another KEY in the config file.
   * Nested macros are supported.
   *
   * @author Bill Darbie
   */
  private void expandMacrosAndDoSomeChecks() throws DatastoreException
  {
    expandMacros();
    checkForMissingKeys();
  }

  /**
   * @author Bill Darbie
   */
  private boolean expandSingleMacroIfNecessary(ConfigEnum configEnum, Object value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    // do the macros expansion here
    String valueStr = value.toString();
    if (valueStr.contains("$(") == false)
      return false;

    String newValue = substituteMacrosInValue(configEnum, valueStr);
    if (newValue == null)
    {
      String configFile = configEnum.getFileName();
      throw new KeyMissingDatastoreException(configEnum.getKey(), configFile);
    }

    if (valueStr.equals(newValue) == false)
    {
      _configEnumToPreMacroExpansionValueMap.put(configEnum, newValue);
      configEnum.setValue(newValue);
    }

    return true;
  }

  /**
   * @author Bill Darbie
   */
  private String substituteMacrosInValue(ConfigEnum configEnum, String value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    // replace any $(MACRO) with the correct environment variable setting
    Matcher matcher = _macroPattern.matcher(value);
    while (matcher.matches())
    {
      String prefix = matcher.group(1);
      String macro = matcher.group(2);
      String postfix = matcher.group(3);
      // see if the macro matches a key
      String macroValue = null;
      ConfigEnum enumeration = ConfigEnum.getConfigEnum(macro);
      if (enumeration != null)
        macroValue = _configEnumToPreMacroExpansionValueMap.get(enumeration);
      else
      {
        // it does not match a key, see if it matches an environment variable
        if (UnitTest.unitTesting() && macro.equals("AXI_XRAY_HOME"))
          macroValue = Directory.getXrayTesterReleaseDir();
        else
          macroValue = System.getenv(macro);
      }

      if (macroValue == null)
      {
        throw new InvalidValueDatastoreException(configEnum.getKey(), value, configEnum.getFileName());
      }

      value = prefix + macroValue + postfix;
      matcher = _macroPattern.matcher(value);
    }
    return value;
  }

  /**
   * @author Bill Darbie
   */
  private void loadFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    if (fileName.endsWith(".cfg") || fileName.endsWith(".config")|| fileName.endsWith(".calib"))
    {
      addKeyValuePairsToMap(fileName);
    }
    else if (fileName.endsWith(".properties"))
    {
      int index = fileName.lastIndexOf(".properties");

      StringTokenizer st = new StringTokenizer(fileName.substring(0,index),File.separator);
      String name = "";
      while (st.hasMoreTokens())
        name = st.nextToken();

      ResourceBundle rb = ResourceHandler.getPropertyBundle(Directory.getPropertiesPackage() + "." + name);
      Assert.expect(rb != null);
      Enumeration enumeration = rb.getKeys();
      while (enumeration.hasMoreElements())
      {
        String key = (String)enumeration.nextElement();
        String value = (String)rb.getString(key);
        // now check that the key was found in the correct file
        ConfigEnum keyIndex = ConfigEnum.getConfigEnum(key);

        if (keyIndex == null)
          throw new InvalidKeyInConfigDatastoreException(key, value, fileName);

        checkThatKeyIsInCorrectFile(keyIndex, key, fileName);
        value = checkThatKeyHasAValidValue(keyIndex, key, value);
        addKeyValuePairToMap(keyIndex, key, value);
      }
    }
    // XCR1529, New Scan Route, khang-wah.chnee
    else if (fileName.endsWith(".csvconfig"))  
    {
      addScanRouteInfoToMap(fileName);
    }
    else
      Assert.expect(false);
  }

  /**
   * this function is only to be used when accessing config values during load.
   * in this case, we are in the middle of loading and we do not want to trigger
   * anther load, otherwise we will be caught in an endless loop.
   * @author George A. David
   */
  private Object getTheValueWithoutLoading(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    // first get the default
    String keyString = configEnum.getKey();
    Object value = configEnum.getValue();

    Assert.expect(value != null, "The value for key " + keyString + " is null");
    return value;

  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bill Darbie
  */
  private Object getTheValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    synchronized (_configKeyLock)
    {
      if ((_selftest) && (_loaded == false))
      {
        try
        {
          loadIfNecessary();
        }
        catch (DatastoreException e)
        {
          System.out.println("ERROR - configuration files had errors during parsing while in selftest mode");
          e.printStackTrace();
        }
      }
      else if (_loaded == false)
      {
        Assert.expect(_loaded, "the Config.load() method must be called before Config.getValue() is called");
      }

      _configKeyLock.notifyAll();
      return getTheValueWithoutLoading(configEnum);
    }
 }

  /**
   * Given a key return its corresponding value in the correct type.
   * The key passed in should be one of the public static variables in the
   * ConfigEnum class.
   *
   * @return the correctly typed value corresponding to the key passed in.
   * @author Bill Darbie
   */
  public Object getValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    TypeEnum type = configEnum.getType();
    Object value = configEnum.getValue();
    Assert.expect(value != null);

    Object ret = null;
    if (type.equals(TypeEnum.BOOLEAN))
      ret = Boolean.valueOf(value.toString());
    else if (type.equals(TypeEnum.DOUBLE))
      ret = Double.valueOf(value.toString());
    else if (type.equals(TypeEnum.INTEGER))
      ret = Integer.valueOf(value.toString());
    else if (type.equals(TypeEnum.LONG))
      ret = Long.valueOf(value.toString());
    else if (type.equals(TypeEnum.STRING))
      ret = value;
    else if (type.equals(TypeEnum.BINARY))
      ret = value;
    else
      Assert.expect(false);

    return ret;
  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bill Darbie
  */
  public String getStringValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

   return (String)getTheValue(configEnum);
  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bob Balliew
  */
  public String getBinaryValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

   return (String)getTheValue(configEnum);
  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bill Darbie
  */
  public boolean getBooleanValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object value = getTheValue(configEnum);

    return ((Boolean)value).booleanValue();
  }

  /**
   * Given a key return its corresponding value.  The key passed in should be one of the
   * public static variables in this class.
   *
   * @return the value corresponding to the key passed in
   * @author Erica Wheatcroft
   */
  public List<String> getStringListValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object value = getTheValue(configEnum);
    return (List<String>)value;
  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bill Darbie
  */
  public int getIntValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object value = getTheValue(configEnum);

    return ((Integer)value).intValue();
  }

  /**
   * Given an enum key, return the long value from that key.
   *
   * @author Reid Hayhow
   */
  public long getLongValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object value = getTheValue(configEnum);

    return ((Long)value).longValue();
  }

  /**
  * Given a key return its corresponding value.  The key passed in should be one of the
  * public static variables in this class.
  *
  * @return the value corresponding to the key passed in.
  * @author Bill Darbie
  */
  public double getDoubleValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object value = getTheValue(configEnum);

    return ((Double)value).doubleValue();
  }

  /**
   * @return the key string given the int value.
   * @author Bill Darbie
   */
  public String getKey(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    return configEnum.getKey();
  }

  /**
   * If the value contains a macro with this syntax: $(SOME_MACRO), it will be expanded.
   * @author Bill Darbie
   */
  public synchronized void setValueInMemoryOnlyExpandingMacros(ConfigEnum configEnum, Object value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    setValueInMemoryOnly(configEnum, value, true);
  }

  /**
  * Set the value of the key passed in memory only.
  *
  * @author Bill Darbie
  * @author Bob Balliew
  */
  public synchronized void setValueInMemoryOnly(ConfigEnum configEnum, Object value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    setValueInMemoryOnly(configEnum, value, false);
  }

  /**
   * Set the value of the key passed in memory only.
   *
   * @author Bill Darbie
   */
  private synchronized void setValueInMemoryOnly(ConfigEnum configEnum, Object value, boolean expandMacro) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    // just in case the new value is a macro, make this call to expand it
    boolean macroExpanded = false;
    if (expandMacro)
      macroExpanded = expandSingleMacroIfNecessary(configEnum, value);
    if (macroExpanded == false)
    {
      // check that the type is correct
      TypeEnum type = configEnum.getType();
      if (type.equals(TypeEnum.BOOLEAN))
        Assert.expect(value instanceof Boolean);
      else if (type.equals(TypeEnum.INTEGER))
        Assert.expect(value instanceof Integer);
      else if (type.equals(TypeEnum.LONG))
        Assert.expect(value instanceof Long);
      else if (type.equals(TypeEnum.DOUBLE))
        Assert.expect(value instanceof Double);
      else if (type.equals(TypeEnum.STRING))
        Assert.expect(value instanceof String);
      else if (type.equals(TypeEnum.LIST_OF_STRINGS))
        Assert.expect(value instanceof List<?>);
      else if (type.equals(TypeEnum.BINARY))
        Assert.expect(value instanceof String);
      else
        Assert.expect(false);

      // check that the value is valid
      boolean valid = checkValidValue(configEnum, value.toString());
      Assert.expect(valid);

      configEnum.setValue(value);
    }

    _configObservable.stateChanged(configEnum);

  }

  /**
   * @author Bill Darbie
   */
  public synchronized void setValueAndExpandMacro(ConfigEnum configEnum, Object value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    setValue(configEnum, value, true);
  }

  /**
  * Set the value of the key passed in to disk.  Note that the entire file will be written for
  * each call to this method.  If you have multiple values to write use setValues(Map<ConfigEnum, Object>) instead.
  *
  * @author Bill Darbie
  * @author Bob Balliew
  */
  public synchronized void setValue(ConfigEnum configEnum, Object value) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    setValue(configEnum, value, false);
  }

  /**
  * Set the value of the key passed in to disk.  Note that the entire file will be written for
  * each call to this method.  If you have multiple values to write use setValues(Map<ConfigEnum, Object>) instead.
  *
  * @author Bill Darbie
  * @author Bob Balliew
  */
  private synchronized void setValue(ConfigEnum configEnum, Object value, boolean expandMacros) throws DatastoreException
  {
    Assert.expect(configEnum != null);
    Assert.expect(value != null);

    setValueInMemoryOnly(configEnum, value, expandMacros);

    TypeEnum type = configEnum.getType();

    if (type.equals(TypeEnum.LIST_OF_STRINGS))
    {
      // convert the list of strings back to one string so that we can save the value.
      value = StringUtil.join((List<String>)value, ',');
    }
    else if (type.equals(TypeEnum.BINARY))
    {
      // convert binary (one byte per character) into a list of hexidecimal codes
      value = StringUtil.convertUnicodeStringIntoHexCodedString(value.toString());
    }

    /** @todo wpd remove this when LP2 is gone */
    if (_checkedForEarlyPrototype == false)
    {
      assignEarlyProtoHardware();
      if (_earlyPrototypeHardware)
        _writeTextFileInAdditionToBinary = true;
      _checkedForEarlyPrototype = true;
    }

    if (_binaryFileToConfigEnumsMap.containsKey(configEnum.getFileName()))
    {
      _configFileBinaryWriter.saveValueToDisk(configEnum);

      if (_writeTextFileInAdditionToBinary)
      {
        // now save the new value into the file
        _configFileTextWriter.saveValueToDisk(configEnum, value);
      }
    }
    else
      _configFileTextWriter.saveValueToDisk(configEnum, value);
  }

  /**
   * Set a bunch of values at once.
   * @author Bill Darbie
   * @author Bob Balliew
   */
  public synchronized void setValuesInMemoryOnly(Map<ConfigEnum, Object> keyToValueMap) throws DatastoreException
  {
    Assert.expect(keyToValueMap != null);

    for (Map.Entry<ConfigEnum, Object> entry : keyToValueMap.entrySet())
    {
      setValueInMemoryOnly(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Set a bunch of values at once.
   * @author Bill Darbie
   * @author Bob Balliew
   */
  public synchronized void setValues(Map<ConfigEnum, Object> keyToValueMap) throws DatastoreException
  {
    Assert.expect(keyToValueMap != null);

    /** @todo wpd remove this when LP2 is gone */
    if (_checkedForEarlyPrototype == false)
    {
      assignEarlyProtoHardware();
      if (_earlyPrototypeHardware)
        _writeTextFileInAdditionToBinary = true;
      _checkedForEarlyPrototype = true;
    }

    setValuesInMemoryOnly(keyToValueMap);

    // use a copy of the map passed in because it may be modified by this method
    Map<ConfigEnum, Object> configEnumToValueMap = new HashMap<ConfigEnum, Object>(keyToValueMap);

    for (Map.Entry<ConfigEnum, Object> entry : configEnumToValueMap.entrySet())
    {
      ConfigEnum key = entry.getKey();
      Object value = entry.getValue();

      // check that the type is correct
      TypeEnum type = key.getType();

      if (type.equals(TypeEnum.LIST_OF_STRINGS))
      {
        // convert the list of strings back to one string so that we can save the value.
        value = StringUtil.join((List<String>)value, ',');
        configEnumToValueMap.put(key, value);
      }
      else if (type.equals(TypeEnum.BINARY))
      {
        // convert binary (one byte per character) into a list of hexidecimal codes
        value = StringUtil.convertUnicodeStringIntoHexCodedString(value.toString());
        configEnumToValueMap.put(key, value);
      }
    }

    for (Map.Entry<ConfigEnum, Object> entry : configEnumToValueMap.entrySet())
    {
      ConfigEnum configEnum = entry.getKey();
      if (_binaryFileToConfigEnumsMap.containsKey(configEnum.getFileName()))
      {
        Map<ConfigEnum, Object> configEnumToValueMap2 = new HashMap<ConfigEnum, Object>();
        configEnumToValueMap2.put(configEnum, configEnumToValueMap.get(configEnum));

        _configFileBinaryWriter.saveValueToDisk(configEnum);

        // if this is true then write out the text file in addition to the binary file
        // this is meant for lab use only
        if (_writeTextFileInAdditionToBinary)
        {
          _configFileTextWriter.saveValuesToDisk(configEnumToValueMap2);
        }
      }
      else
      {
        Map<ConfigEnum, Object> configEnumToValueMap2 = new HashMap<ConfigEnum, Object>();
        configEnumToValueMap2.put(configEnum, configEnumToValueMap.get(configEnum));

        // now save the new value into the file
        _configFileTextWriter.saveValuesToDisk(configEnumToValueMap2);
      }
    }
  }

  /**
   * @return the type of the value for the given key.
   * @author Bill Darbie
   */
  public TypeEnum getValueType(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    return configEnum.getType();
  }

  /**
   * @return the default value for the given key.  If no default exists a null will be returned.
   * @author Bill Darbie
   */
  public Object getDefaultValue(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    TypeEnum type = configEnum.getType();

    // make the return Object be the correct type of object (Integer or Double or String or Boolean)
    Object defaultValue = null;
    if (type.equals(TypeEnum.BOOLEAN))
      defaultValue = new Boolean(getBooleanValue(configEnum));
    else if (type.equals(TypeEnum.INTEGER))
      defaultValue = new Integer(getIntValue(configEnum));
    else if (type.equals(TypeEnum.LONG))
      defaultValue = new Long(getLongValue(configEnum));
    else if (type.equals(TypeEnum.DOUBLE))
      defaultValue = new Double(getDoubleValue(configEnum));
    else if (type.equals(TypeEnum.STRING))
      defaultValue = new String(getStringValue(configEnum));
    else if (type.equals(TypeEnum.LIST_OF_STRINGS))
      defaultValue = getStringListValue(configEnum);
    else if (type.equals(TypeEnum.BINARY))
      defaultValue = new String(getStringValue(configEnum));
    else
      Assert.expect(false);

    return defaultValue;
  }

  /**
   * @return the list of valid values for the given key.  If any value is allowed the
   *         list returned will be empty.
   * @author Bill Darbie
   */
  public List<Object> getValidValues(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);
    Object[] validValues = configEnum.getValidValues();

    List<Object> values = new ArrayList<Object>();
    if (validValues != null)
    {
      for(int i = 0; i < validValues.length; ++i)
        values.add(validValues[i]);
    }

    return values;
  }
  
  /**
   * @return the map of scan route config
   *         Map(Sequence, Pair<StepSizeInNanometers, Pass Code>)
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public Map<Integer, Pair<Double, String>> getNominalStepSizeToPassCodeInfo()
  {
    return _nominalSteSizeToPassCodeMap;
  }
  
  /**
   * @return the map of scan route config
   *         Map(Sequence, Pair<xLocationInNanometers, Pass Code>)
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public Map<Integer, Pair<Integer, String>> getXLocationToPassCodeInfo()
  {
    return _xLocationToPassCodeMap;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void loadConfigFiles(List<String> configFiles) throws DatastoreException
  {
    Assert.expect(configFiles != null);

    for (String fileName : configFiles)
    {
      loadFile(fileName);
    }
  }

  /**
   *
   * @author Erica Wheatcroft
   */
  public boolean isLoaded()
  {
    return _loaded;
  }

  /**
   * @author George A. David
   */
  public static boolean isDeveloperDebugModeOn()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
  }

  /**
   * This would check if the developer performance mode is set to ON/OFF
   *
   * @author Poh Kheng
   */
  public static boolean isDeveloperPerformanceModeOn()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_PERFORMANCE_MODE);
  }

  /**
   * This would check if the developer license is enabled.
   *
   * @author Lim, Seng Yew
   */
  public static boolean isDeveloperSystemEnabled()
  {
    try
    {
      if (isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
        return true;
    }
    catch (Exception ex)
    {
      // Do nothing
    }
    return false;
  }

  /**
   * @author George David
   */
  private void initializeSystemSpecificConfigFiles()
  {
    if(_areSystemSpecificConfigFilesInitialized == false)
    {
      try
      {
        Class.forName("com.axi.v810.datastore.config.SystemConfigEnum");
      }
      catch (ClassNotFoundException cnfe)
      {
        Assert.logException(cnfe);
      }

      // initialize the system specific configuration files
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        if (_newSystemType == null)
        {
          String systemConfigFilePath = FileName.getSystemConfigFullPath(systemType);

          // also now add it to the system config files to the binary map
          _binaryFileToConfigEnumsMap.put(systemConfigFilePath, SystemConfigEnum.getSystemConfigEnums());
        }
        else
        {
          String systemConfigFilePath = FileName.getSystemConfigFullPathForOLP(_newSystemType);

          // also now add it to the system config files to the binary map
          _binaryFileToConfigEnumsMap.put(systemConfigFilePath, SystemConfigEnum.getSystemConfigEnums());
          break;
        }
      }

      _areSystemSpecificConfigFilesInitialized = true;
    }
  }

  /**
   * @author George A. David
   */
  public void loadSystemSpecificConfigFiles(SystemTypeEnum systemTypeEnum) throws DatastoreException
  {
    initializeSystemSpecificConfigFiles();

    SystemConfigEnum.updateSystemType(systemTypeEnum);
    _loadedSystemTypeEnumSetting = systemTypeEnum;

    for (String filePath : _systemTypeToConfigFilePathsMap.get(systemTypeEnum))
    {
      loadFile(filePath);
    }

    expandMacrosAndDoSomeChecks();
  }

  /**
   * use XrayTester.getSystemType(). This is only for loading the config files.
   * This is necessary for avoing a circular dependency.
   * @author George A. David
   * @author Wei Chin
   */
  private SystemTypeEnum getSystemType()
  {
//    Assert.expect(_configEnumToPreMacroExpansionValueMap != null);
//    // no macro expansions have occured yet, so check the proper map
//    Object value = _configEnumToPreMacroExpansionValueMap.get(HardwareConfigEnum.SYSTEM_TYPE);
////    Object value = getValue(HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE);
//
//    return SystemTypeEnum.getSystemType((String)value);
    return XrayTester.getSystemType();
  }


  /**
   * @author Poh Kheng
   */
  public SystemTypeEnum getSystemTypeSettingLoaded()
  {
    Assert.expect(_loadedSystemTypeEnumSetting != null);
    return _loadedSystemTypeEnumSetting;
  }

  /**
   * @author Poh Kheng
   */
  public void resetToMachineSystemTypeSetting() throws DatastoreException
  {
    if(XrayTester.getSystemType().equals(_loadedSystemTypeEnumSetting) == false)
    {
      _loadedSystemTypeEnumSetting = XrayTester.getSystemType();
      loadSystemSpecificConfigFiles(_loadedSystemTypeEnumSetting);
    }
  }
  
  //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
  public void isDeveloperDebugOnCheck() throws KeyMissingDatastoreException, InvalidTypeDatastoreException
  {
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) == false) 
    {
      for (ConfigEnum configEnum : ConfigEnum.getAllConfigEnums())
      {
        //if Developer debug mode is off, set all key value to false
        if(configEnum instanceof DeveloperDebugConfigEnum)
        {
          String key = configEnum.getKey();
          configEnum.setValue(configEnum.getDefaultValue());
        } 
      }
    }
  }
  
  /**
   * @author Bee Hoon
   */
  public static void checkIs64BitsIRP()
  {
    File irpExeFile = new File(Directory.getLocalImageReconstructionProcessorBinDir() + File.separator + "IRP.exe");
    Assert.expect(irpExeFile.exists());
    
    FileInputStream fin;
    int fileSize = 0;
    int fileLength = (int) irpExeFile.length();
    byte data[] = new byte[fileLength];
    String strPE = null;
    String strMachineType = null;
    
    try 
    {
      fin = new FileInputStream(irpExeFile);
      fileSize = fin.read(data, 0, fileLength);
      
      for (int i = 0; i < fileSize; i++)
      {
        strPE = String.format("%02x", data[i]) + 
                String.format("%02x", data[i+1]) + 
                String.format("%02x", data[i+2]) + 
                String.format("%02x", data[i+3]) ;
        
        if (strPE.equalsIgnoreCase(_PORTABLE_EXECUTABLE_IN_HEXA))
        {
          strMachineType = String.format("%02x", data[i+4]) + String.format("%02x", data[i+5]);
          if (strMachineType.equalsIgnoreCase(_IRP_32_BITS_IN_HEXA))
          {
            _is64bitsIRP = false;
            break;
          }
          else if (strMachineType.equalsIgnoreCase(_IRP_64_BITS_IN_HEXA))
          {
            _is64bitsIRP = true;
            break;
          }
        }
      }
    } 
    catch (FileNotFoundException ex) 
    {
      System.out.println(ex);
    } 
    catch (IOException ex) 
    {
      System.out.println(ex);
    }
  }
  
  /**
   * author Khang Wah, 2013-11-05
   */
  public static boolean is64bitIrp()
  {
    return _is64bitsIRP;
  }

  /**
   * @author Bee Hoon
   */
  public static boolean isRealTimePshEnabled()
  {
    _isRealTimePshEnabled = getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_REAL_TIME_PSH);
    
    return is64bitIrp() && _isRealTimePshEnabled;
  }
  
  /**
   * @author Ngie Xing
   */
  public static boolean isIntelligentLearning()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_INTELLIGENT_LEARNING);
  }
  
  /**
   * @author Ngie Xing
   */
  public static String getSystemCurrentLowMagnification()
  {
    if (XrayTester.isS2EXEnabled())
    {
      return getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION);
    }
    else
    {
      return "M19";
    }
  }
  
  /**
   * @author Ngie Xing
   * @author Chnee Khang Wah, 2016-03-17, to log magnification info into log file during CD&A
   */
  public static String getSystemCurrentHighMagnification()
  {
    if (XrayTester.isS2EXEnabled())
    {
      return getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION);
    }
    else
    {
      return "M11";
    }
  }
  
  /**
   * @author Chnee Khang Wah, 2016-03-17, to log magnification info into log file during CD&A
   */
  public static String getSystemCurrentLowMagnificationForLogging()
  {
    return getSystemCurrentLowMagnification();
  }
  
  /**
   * @author Chnee Khang Wah, 2016-03-17, to log magnification info into log file during CD&A
   */
  public static String getSystemCurrentHighMagnificationForLogging()
  {
    if(XrayTester.isXXLBased())
    {
      return "M13";
    }
    else
    {
      return getSystemCurrentHighMagnification();
    }
  }
  
  
  /**
   * @author Cheah Lee Herng 
   */
  public static boolean isComponentLevelClassificationEnabled()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_COMPONENT_LEVEL_CLASSIFICATION);
  }
  
  /**
   * Backup the binary config file if it is corrupted.
   * @author Ying-Huan.Chu
   */
  private void backupOldBinaryConfigFile(String configFile)
  {
    String configFileInBinaryExtension = configFile + FileName.getBinaryExtension();
    File binaryConfigFile = new File (configFileInBinaryExtension); 
    String binaryConfigFileName = binaryConfigFile.getName(); // get binary config file name from its file path
    String backUpBinaryConfigFileName = configFileInBinaryExtension + FileName.getBackupExtension();
    _corruptedBinaryConfigFileNameList.add(binaryConfigFileName);
    try
    {
      FileUtilAxi.copy(configFileInBinaryExtension, backUpBinaryConfigFileName); // backup a copy of the old (corrupted) binary file 
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
    }
  }
  
  /**
   * Get the list of corrupted Binary Config file names
   * @author Ying-Huan.Chu
   */
  public List<String> getCorruptedBinaryConfigFileNameList()
  {
    return _corruptedBinaryConfigFileNameList;
  }
  
  /**
   * @author Ngie Xing
   */
  public synchronized void deleteSystemTypeConfigBinaryFileIfExist()
  {
    try
    {
      if (FileUtilAxi.exists(FileName.getConfigBinaryFileFullPath()))
      {
        FileUtilAxi.delete(FileName.getConfigBinaryFileFullPath());
      }
    }
    catch (DatastoreException ex)
    {
      //do nothing
    }
  }
  
  /**
   * @author Swee Yee
   */
  public static boolean isInfoHandlerEnabled()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_INFO_HANDLER);
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getMaximumDynamicRangeOptimizationLevel()
  {
    return getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL);
  }

  /**
   * @author Swee Yee
   */
  public static boolean isMultiAnglePostProcessing()
  {
    _isMultiAnglePostProcessing = getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_MULTI_ANGLE_POST_PROCESSING);
    
    return is64bitIrp() && _isMultiAnglePostProcessing;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee
   */
  public static boolean isCameraDebugModeOn()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.CAMERA_DEBUG_MODE);
  }
  
  /**
   * Enabled Cad Creator
   * @return 
   */
  public static boolean isCadCreatorEnabled()
  {
    return getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CAD_CREATOR);
  }
  
  /**
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author Swee yee
   */
  public static boolean isMotionControlLogEnabled()
  {
    return getInstance().getBooleanValue(HardwareConfigEnum.ENABLE_MOTION_CONTROL_LOG);
  }

  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   *
   *
   * set all configenum file to new config file based on system type pass in.
   */
  public void changeConfigFileDueToDifferentSystemType(List<String> configFile,SystemTypeEnum oldsystemType,SystemTypeEnum newsystemType) throws DatastoreException
  {
    Assert.expect(configFile != null);
    Assert.expect(oldsystemType != null);
    Assert.expect(newsystemType != null);
    
    Map<String,String> newConfigFileToOldFileMap = new HashMap<String,String>();
    try
    {
      XrayTester.changeSystemType(_newSystemType);
      for (String oldfile : configFile)
      {
        if (oldfile.equals(Directory.getConfigDirWithSystemType(oldsystemType)+ File.separator+FileName.getSoftwareConfigFile()))
        {
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+ File.separator+FileName.getSoftwareConfigFile();
          if (FileUtilAxi.exists(newfile))
          {
            ConfigEnum.changeFileName(oldfile, newfile);
            newConfigFileToOldFileMap.put(newfile, oldfile);
          }
          else
          {
            throw new FileNotFoundDatastoreException(newfile);
          }
        }
        else if (oldfile.equals(FileName.getPspCalibFullPath(oldsystemType)))
        {
          String newfile = FileName.getPspCalibFullPath(newsystemType);
          _binaryFileToConfigEnumsMap.put(newfile, PspCalibEnum.getPspCalibEnums());
          ConfigEnum.changeFileName(oldfile, newfile);
          newConfigFileToOldFileMap.put(newfile, oldfile);
        }
        else if (oldfile.equals(FileName.getHardwareCalibFullPath(oldsystemType)))
        {
          String newfile = FileName.getHardwareCalibFullPath(newsystemType);
          ConfigEnum.changeFileName(oldfile, newfile);
          newConfigFileToOldFileMap.put(newfile, oldfile);
          _binaryFileToConfigEnumsMap.put(newfile, HardwareCalibEnum.getHardwareCalibEnums());
        }
        else if (oldfile.equals(Directory.getConfigDirWithSystemType(oldsystemType)+File.separator+FileName.getHardwareConfigFile()))
        {
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+File.separator+FileName.getHardwareConfigFile();
          if (FileUtilAxi.exists(newfile))
          {
            ConfigEnum.changeFileName(oldfile, newfile);
            newConfigFileToOldFileMap.put(newfile, oldfile);
          }
          else
          {
            throw new FileNotFoundDatastoreException(newfile);
          }
        }
        else if (oldfile.equals(Directory.getConfigDirWithSystemType(oldsystemType)+File.separator+FileName.getDeveloperDebugConfigFile()))
        {
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+File.separator+FileName.getDeveloperDebugConfigFile();
          ConfigEnum.changeFileName(oldfile, newfile);
          _binaryFileToConfigEnumsMap.put(newfile, DeveloperDebugConfigEnum.getDeveloperDebugConfigEnum());
          newConfigFileToOldFileMap.put(newfile, oldfile);     
        }
        else if (oldfile.equals(Directory.getConfigDirWithSystemType(oldsystemType)+File.separator+FileName.getFalseCallTriggeringConfigFile()))
        {
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+File.separator+FileName.getFalseCallTriggeringConfigFile();
          if (FileUtilAxi.exists(newfile))
          {
            ConfigEnum.changeFileName(oldfile, newfile);
            newConfigFileToOldFileMap.put(newfile, oldfile);
          }
          else
          {
            throw new FileNotFoundDatastoreException(newfile);
          }
        }
        else if (oldfile.equals(FileName.getSystemConfigFullPath(oldsystemType)))
        {
          _areSystemSpecificConfigFilesInitialized = false;
          initializeSystemSpecificConfigFiles();
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+File.separator+FileName.getSystemConfigFile(newsystemType);
          ConfigEnum.changeFileName(oldfile, newfile);
          newConfigFileToOldFileMap.put(newfile, oldfile);
        }
        else if (oldfile.equals(FileName.getFalseCallTriggeringConfigFullPath()))
        {
          String newfile = Directory.getConfigDirWithSystemType(newsystemType)+File.separator+FileName.getFalseCallTriggeringConfigFile();
          if (FileUtilAxi.exists(newfile))
          {
            ConfigEnum.changeFileName(oldfile, newfile);
            newConfigFileToOldFileMap.put(newfile, oldfile);
          }
          else
          {
            throw new FileNotFoundDatastoreException(newfile);
          }
        }
      }
    }
    catch (DatastoreException e)
    {
      XrayTester.changeSystemType(oldsystemType);
      if (newConfigFileToOldFileMap.isEmpty() == false)
      {
        for (Map.Entry<String, String> entry : newConfigFileToOldFileMap.entrySet()) 
        {
          String newFile = entry.getKey();
          String oldFile = entry.getValue();
          ConfigEnum.changeFileName(newFile, oldFile);
        }
      }
      throw e;
    }
  }
  
  /**
   * XCR-3822 System crash when switch to xray safety test tab
   * @author Cheah Lee Herng
   */
  public void checkXrayActuatorSetting()
  {
    boolean isXrayCylinderInstalled = getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED);
    boolean isXrayZAxisMotorInstalled = getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED);
    
    if (XrayTester.isOfflineProgramming() == false)
    {
      if (XrayTester.isS2EXEnabled())
      {
        Assert.expect(isXrayCylinderInstalled == false && isXrayZAxisMotorInstalled == true, 
          "Xray magnification actuator setting is not setup correctly in hardware.config for S2EX system");
      }
      else if (XrayTester.isXXLBased())
      {
        Assert.expect(isXrayZAxisMotorInstalled == false, 
          "Xray magnification actuator setting is not setup correctly in hardware.config for XXL system");
      }
      else
      {
        Assert.expect(isXrayZAxisMotorInstalled == false, 
          "Xray magnification actuator setting is not setup correctly in hardware.config for Standard system");
      }
    }
  }
}