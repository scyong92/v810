package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_ConfigFileTextWriter extends UnitTest
{

  /**
   * @author Bill Darbie
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_ConfigFileTextWriter());
  }

  /**
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String origFileName = getTestDataDir() + File.separator + "usedByTestConfigWriter.config";
    String fileName = origFileName + ".temp";
    try
    {
      FileUtilAxi.copy(origFileName, fileName);

      // first read in the config file and make sure that the enabled key is set to false
      ConfigFileTextReader configFileReader = new ConfigFileTextReader();
      Map<String, String> keyValueMap = configFileReader.getKeyValueMapping(fileName);
      String value = keyValueMap.get(Test_ConfigEnum.TEST_ENABLED.getKey());
      Expect.expect(value.equalsIgnoreCase("false"));

      // now modify the key to say true instead
      Test_ConfigEnum.setFileDir(getTestDataDir());
      ConfigFileTextWriter configFileWriter = new ConfigFileTextWriter();
      configFileWriter.saveValueToDisk(Test_ConfigEnum.TEST_ENABLED, "true");
      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.TEST_ENABLED.getKey());
      Expect.expect(value.equalsIgnoreCase("true"));

      // return the file back to false
      configFileWriter.saveValueToDisk(Test_ConfigEnum.TEST_ENABLED, "false");
      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.TEST_ENABLED.getKey());
      Expect.expect(value.equalsIgnoreCase("false"));

      //Test read of long, added by Reid
      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.TEST_LONG.getKey());
      Expect.expect(value.equalsIgnoreCase("5551212"));

      //Test long read-write, added by Reid
      configFileWriter.saveValueToDisk(Test_ConfigEnum.TEST_LONG, "100000000000");
      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.TEST_LONG.getKey());
      Expect.expect(value.equalsIgnoreCase("100000000000"));

      // now test that perl metacharacters are handled properly
      value = "some name";// *;-$ goes here";
      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.NAME.getKey());
      Expect.expect(value.equalsIgnoreCase(value));

      String newValue = "some strange text ; . * \\ was here";
      configFileWriter.saveValueToDisk(Test_ConfigEnum.NAME, newValue);

      keyValueMap = configFileReader.getKeyValueMapping(fileName);
      value = keyValueMap.get(Test_ConfigEnum.NAME.getKey());
      Expect.expect(value.equalsIgnoreCase(newValue));

    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      if (FileUtilAxi.exists(fileName))
      {
        try
        {
          FileUtilAxi.delete(fileName);
        }
        catch(DatastoreException de)
        {
          // do nothing
        }
      }
    }

  }
}
