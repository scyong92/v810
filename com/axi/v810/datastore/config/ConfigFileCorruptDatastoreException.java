package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class gets thrown when a config file is corrupt in some way.
 * @author Bill Darbie
 */
public class ConfigFileCorruptDatastoreException extends DatastoreException
{
  /**
   * Create an exception.  Make sure that the localizedString you pass
   * in constains the file name, line number, and an explanation of what
   * was expected compared to what was actually parsed in.
   * @param fileName is the name of the file that is corrupt
   * @param lineNumber is the line number where the problem is
   * @param actual is the string of what was actually found
   * @author Bill Darbie
   */
  public ConfigFileCorruptDatastoreException(String fileName,
                                             int lineNumber,
                                             String actual)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_FILE_CORRUPT_KEY", new Object[]{fileName, new Integer(lineNumber), actual}));
    Assert.expect(fileName != null);
    Assert.expect(actual != null);
  }
}
