package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Bob Balliew
 */
public class Test_BarcodeReaderConfigEnum extends UnitTest
{
  private static final String _DEFAULT_BARCODE_READER_CONFIGURATION_NAME = "_ms820_p2p";
  private static final String NO_SUCH_BARCODE_READER_KEY = "BARF";

  /**
   * @author Bob Balliew
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_BarcodeReaderConfigEnum());
  }

  /**
   * @author Bill Darbie
   */
  private static String stripOffView(String fullPath)
  {
    // view names might begin with axi, so we'll look for the
    // vob name all by itself with the slashes at both ends
    int index = fullPath.indexOf("\\axi\\");
    if (index != -1)
      index = index + "\\axi".length();
    if (index == -1)
    {
      index = fullPath.lastIndexOf("/axi/");
      if (index != -1)
        index = index + "/axi".length();
    }

    String strippedPath = fullPath;
    if (index != -1)
      strippedPath = fullPath.substring(index, fullPath.length());

    return strippedPath;
  }

  /**
   * Must be called before config is loaded.
   * @author Bob Balliew
   */
  private void test_configNeverLoadedException(BufferedReader is, PrintWriter os)
  {
    Config config = Config.getInstance();
    Expect.expect(! config.isLoaded());
    try
    {
      config.load("barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      Expect.expect(ae.getMessage().equals("the Config.load() method must be called before Config.load(pathName) is called"));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(! config.isLoaded());
    try
    {
      config.load("barf", "barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      Expect.expect(ae.getMessage().equals("the Config.load() method must be called before Config.load(oldPathName, newPathName) is called"));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(! config.isLoaded());
    try
    {
      config.save("barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      Expect.expect(ae.getMessage().equals("the Config.load() method must be called before Config.save(pathName) is called"));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(! config.isLoaded());
    try
    {
      config.save("barf", "barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      Expect.expect(ae.getMessage().equals("the Config.load() method must be called before Config.save(oldPathName, newPathName) is called"));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(! config.isLoaded());
  }

  /**
   * Must be called before config is loaded.
   * Causes the config to be loaded.
   * @author Bob Balliew
   */
  private void test_isLoaded(BufferedReader is, PrintWriter os)
  {
    Config config = Config.getInstance();
    Expect.expect(! config.isLoaded());
    try
    {
      config.loadIfNecessary();
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(config.isLoaded());
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_AUTOMATIC_READER_ENABLED(BufferedReader is, PrintWriter os)
  {
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getKey().equals("barcodeReaderAutomaticReaderEnabled"));
    Expect.expect(SoftwareConfigEnum.getConfigEnum("barcodeReaderAutomaticReaderEnabled") == SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED);
    Expect.expect(SoftwareConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getSoftwareConfigFullPath())));
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue();
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
//    catch (DatastoreException e)
//    {
//      Expect.expect(false, e.getMessage());
//    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue() == null);
    try
    {
      SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_CONFIGURATION_NAME_TO_USE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getKey().equals("barcodeReaderConfigurationNameToUse"));
    Expect.expect(SoftwareConfigEnum.getConfigEnum("barcodeReaderConfigurationNameToUse") == SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE);
    Expect.expect(SoftwareConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getSoftwareConfigFullPath())));
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue();
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyConfiguration");
    TEST_VALUES.add("MyCOnfig");
    TEST_VALUES.add("Guess Who");
    TEST_VALUES.add("xyzzy");
    for (String value : TEST_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue() == null);
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_CONFIGURATION_NAME_TO_EDIT(BufferedReader is, PrintWriter os)
  {
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getKey().equals("barcodeReaderConfigurationNameToEdit"));
    Expect.expect(SoftwareConfigEnum.getConfigEnum("barcodeReaderConfigurationNameToEdit") == SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT);
    Expect.expect(SoftwareConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getSoftwareConfigFullPath())));
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue();
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyConfiguration");
    TEST_VALUES.add("MyCOnfig");
    TEST_VALUES.add("Guess Who");
    TEST_VALUES.add("xyzzy");
    for (String value : TEST_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue() == null);
    try
    {
      SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PROTECTED_CONFIGURATION_NAMES(BufferedReader is, PrintWriter os)
  {
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getKey().equals("barcodeReaderProtectedConfigurationNames"));
    Expect.expect(SoftwareConfigEnum.getConfigEnum("barcodeReaderProtectedConfigurationNames") == SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES);
    Expect.expect(SoftwareConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getSoftwareConfigFullPath())));
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getType() == TypeEnum.STRING);
    final List<List<String>> LEGAL_VALUES = new LinkedList<List<String>>();
    Object[] validValues = SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue();
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() == null);
    for (List<String> value : LEGAL_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("");
    TEST_VALUES.add("_*");
    TEST_VALUES.add("protected*");
    for (String value : TEST_VALUES)
    {
      try
      {
        SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue().equals(value));
    }
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("0.0"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("x"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("false"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() instanceof String);
    Expect.expect(((String)SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue()).equals("true"));
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue() == null);
    try
    {
      SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_NUMBER_OF_SCANNERS(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getKey().equals("barcodeReaderNumberOfScanners"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderNumberOfScanners") == BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValidValues()[0] == 0);
    if (BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValidValues()[1] == 16);
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == null);
    for (int i = 0; i <= 16; ++i)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_OUTPUT_BUFFER_SIZE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getKey().equals("barcodeReaderOutputBufferSize"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderOutputBufferSize") == BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValidValues()[0] == 16);
    if (BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValidValues()[1] == Integer.MAX_VALUE);
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == null);
    for (int i = 16; i >= 16; i *= 2)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }    
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_INPUT_BUFFER_SIZE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getKey().equals("barcodeReaderInputBufferSize"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderInputBufferSize") == BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValidValues()[0] == 16);
    if (BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValidValues()[1] == Integer.MAX_VALUE);
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == null);
    for (int i = 16; i >= 16; i *= 2)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_CHARSET_NAME(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getKey().equals("barcodeReaderCharsetName"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderCharsetName") == BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("US-ASCII");
    LEGAL_VALUES.add("ISO-8859-1");
    LEGAL_VALUES.add("UTF-8");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_SERIAL_PORT_NAME(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getKey().equals("barcodeReaderSerialPortName"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderSerialPortName") == BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("COM1");
    LEGAL_VALUES.add("COM2");
    LEGAL_VALUES.add("COM3");
    LEGAL_VALUES.add("COM4");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_DATA_BITS(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getKey().equals("barcodeReaderRs232DataBits"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232DataBits") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("5");
    LEGAL_VALUES.add("6");
    LEGAL_VALUES.add("7");
    LEGAL_VALUES.add("8");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getKey().equals("barcodeReaderRs232RtsCtsFlowControlIn"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232RtsCtsFlowControlIn") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getKey().equals("barcodeReaderRs232RtsCtsFlowControlOut"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232RtsCtsFlowControlOut") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getKey().equals("barcodeReaderRs232XonXoffFlowControlIn"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232XonXoffFlowControlIn") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
   
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }    
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getKey().equals("barcodeReaderRs232XonXoffFlowControlOut"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232XonXoffFlowControlOut") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_PARITY(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getKey().equals("barcodeReaderRs232Parity"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232Parity") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("EVEN");
    LEGAL_VALUES.add("MARK");
    LEGAL_VALUES.add("NONE");
    LEGAL_VALUES.add("ODD");
    LEGAL_VALUES.add("SPACE");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_STOP_BITS(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getKey().equals("barcodeReaderRs232StopBits"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232StopBits") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("1");
    LEGAL_VALUES.add("2");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_BITS_PER_SECOND(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getKey().equals("barcodeReaderRs232BitsPerSecond"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232BitsPerSecond") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    LEGAL_VALUES.add("75");
    LEGAL_VALUES.add("110");
    LEGAL_VALUES.add("134");
    LEGAL_VALUES.add("150");
    LEGAL_VALUES.add("300");
    LEGAL_VALUES.add("600");
    LEGAL_VALUES.add("1200");
    LEGAL_VALUES.add("1800");
    LEGAL_VALUES.add("2400");
    LEGAL_VALUES.add("4800");
    LEGAL_VALUES.add("7200");
    LEGAL_VALUES.add("9600");
    LEGAL_VALUES.add("14400");
    LEGAL_VALUES.add("19200");
    LEGAL_VALUES.add("38400");
    LEGAL_VALUES.add("57600");
    LEGAL_VALUES.add("115200");
    LEGAL_VALUES.add("128000");
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_APPLICATION_NAME(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getKey().equals("barcodeReaderRs232ApplicationName"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232ApplicationName") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getType() == TypeEnum.STRING);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyApplication");
    TEST_VALUES.add("MyName");
    TEST_VALUES.add("Guess Who");
    TEST_VALUES.add("xyzzy");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getKey().equals("barcodeReaderRs232OpenTimeoutInMilliseconds"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderRs232OpenTimeoutInMilliseconds") == BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValidValues()[0] == 0);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValidValues()[1] == 65000);
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == null);
    for (int i = 0; i <= 65000; ++i)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_RECEIVE_FRAMING(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getKey().equals("barcodeReaderEnableReceiveFraming"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableReceiveFraming") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RECEIVE_FRAMING_BYTE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getKey().equals("barcodeReaderReceiveFramingByte"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveFramingByte") == BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValidValues()[0] == 0x00);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValidValues()[1] == 0xFF);
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == null);
    for (int i = 0; i < 256; ++i)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_RECEIVE_THRESHOLD(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getKey().equals("barcodeReaderEnableReceiveThreshold"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableReceiveThreshold") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RECEIVE_THRESHOLD_COUNT(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getKey().equals("barcodeReaderReceiveThresholdCount"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveThresholdCount") == BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValidValues()[0] == 0);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValidValues()[1] == (Integer.MAX_VALUE - 1));
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == null);
    for (int i = 0; i < 256; ++i)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_RECEIVE_TIMEOUT(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getKey().equals("barcodeReaderEnableReceiveTimeout"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableReceiveTimeout") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getKey().equals("barcodeReaderReceiveTimeoutInMilliseconds"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveTimeoutInMilliseconds") == BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getType() == TypeEnum.INTEGER);
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValidValues().length == 2);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValidValues().length > 0)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValidValues()[0] == 0);
    if (BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValidValues().length > 1)
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValidValues()[1] == (Integer.MAX_VALUE - 1));
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == null);
    for (int i = 0; i < 60000; i += 10)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((long)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(Long.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((Integer)i);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      if ((i >= Short.MIN_VALUE) && (i <= Short.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((Short)(short)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      if ((i >= Byte.MIN_VALUE) && (i <= Byte.MAX_VALUE))
      {
        try
        {
          BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((Byte)(byte)i);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
        Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
      }
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(Integer.toString(i));
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == i);
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(false);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(true);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(new Boolean(false));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(new Boolean(true));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_NO_READ_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getKey().equals("barcodeReaderEnableNoReadMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableNoReadMessage") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_NO_READ_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getKey().equals("barcodeReaderReceiveNoReadMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveNoReadMessage") == BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("READ?");
    TEST_VALUES.add("?");
    TEST_VALUES.add("NOREAD");
    TEST_VALUES.add("No Read");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getKey().equals("barcodeReaderEnableBadSymbolMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableBadSymbolMessage") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_BAD_SYMBOL_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getKey().equals("barcodeReaderReceiveBadSymbolMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveBadSymbolMessage") == BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("BAD");
    TEST_VALUES.add("?!?");
    TEST_VALUES.add("BADSYMBOL");
    TEST_VALUES.add("Bad Symbol");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_ENABLE_NO_LABEL_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getKey().equals("barcodeReaderEnableNoLabelMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderEnableNoLabelMessage") == BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getType() == TypeEnum.BOOLEAN);
    final List<Boolean> LEGAL_VALUES = new LinkedList<Boolean>();
    LEGAL_VALUES.add(false);
    LEGAL_VALUES.add(true);
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue() == null);
    for (Boolean value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("false");
    TEST_VALUES.add("true");
    TEST_VALUES.add("False");
    TEST_VALUES.add("True");
    TEST_VALUES.add("FALSE");
    TEST_VALUES.add("TRUE");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((long)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((int)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((short)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((byte)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((double)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue((float)0);
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue('x');
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Long(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Integer(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Short((short)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Byte((byte)0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Double(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Float(0));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(new Character('x'));
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue("Barf");
      Expect.expect(false, "Expected exception to be thrown.");
    }
    catch (InvalidTypeDatastoreException e)
    {
      // Do nothing.
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_NO_LABEL_MESSAGE(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getKey().equals("barcodeReaderReceiveNoLabelMessage"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderReceiveNoLabelMessage") == BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("LABEL?");
    TEST_VALUES.add("??");
    TEST_VALUES.add("NOLABEL");
    TEST_VALUES.add("No Label");
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_1(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getKey().equals("barcodeReaderTriggerScanner_1"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_1") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_1(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getKey().equals("barcodeReaderPreambleScanner_1"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_1") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_1(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getKey().equals("barcodeReaderPostambleScanner_1"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_1") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_2(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getKey().equals("barcodeReaderTriggerScanner_2"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_2") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_2(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getKey().equals("barcodeReaderPreambleScanner_2"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_2") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_2(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getKey().equals("barcodeReaderPostambleScanner_2"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_2") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_3(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getKey().equals("barcodeReaderTriggerScanner_3"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_3") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_3(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getKey().equals("barcodeReaderPreambleScanner_3"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_3") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_3(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getKey().equals("barcodeReaderPostambleScanner_3"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_3") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_4(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getKey().equals("barcodeReaderTriggerScanner_4"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_4") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_4(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getKey().equals("barcodeReaderPreambleScanner_4"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_4") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_4(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getKey().equals("barcodeReaderPostambleScanner_4"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_4") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_5(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getKey().equals("barcodeReaderTriggerScanner_5"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_5") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_5(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getKey().equals("barcodeReaderPreambleScanner_5"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_5") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_5(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getKey().equals("barcodeReaderPostambleScanner_5"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_5") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_6(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getKey().equals("barcodeReaderTriggerScanner_6"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_6") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_6(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getKey().equals("barcodeReaderPreambleScanner_6"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_6") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_6(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getKey().equals("barcodeReaderPostambleScanner_6"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_6") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_7(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getKey().equals("barcodeReaderTriggerScanner_7"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_7") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_7(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getKey().equals("barcodeReaderPreambleScanner_7"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_7") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_7(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getKey().equals("barcodeReaderPostambleScanner_7"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_7") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_8(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getKey().equals("barcodeReaderTriggerScanner_8"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_8") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_8(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getKey().equals("barcodeReaderPreambleScanner_8"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_8") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_8(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getKey().equals("barcodeReaderPostambleScanner_8"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_8") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_9(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getKey().equals("barcodeReaderTriggerScanner_9"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_9") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_9(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getKey().equals("barcodeReaderPreambleScanner_9"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_9") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_9(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getKey().equals("barcodeReaderPostambleScanner_9"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_9") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_10(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getKey().equals("barcodeReaderTriggerScanner_10"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_10") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_10(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getKey().equals("barcodeReaderPreambleScanner_10"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_10") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_10(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getKey().equals("barcodeReaderPostambleScanner_10"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_10") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_11(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getKey().equals("barcodeReaderTriggerScanner_11"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_11") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_11(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getKey().equals("barcodeReaderPreambleScanner_11"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_11") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_11(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getKey().equals("barcodeReaderPostambleScanner_11"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_11") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_12(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getKey().equals("barcodeReaderTriggerScanner_12"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_12") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_12(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getKey().equals("barcodeReaderPreambleScanner_12"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_12") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_12(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getKey().equals("barcodeReaderPostambleScanner_12"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_12") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_13(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getKey().equals("barcodeReaderTriggerScanner_13"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_13") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_13(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getKey().equals("barcodeReaderPreambleScanner_13"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_13") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_13(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getKey().equals("barcodeReaderPostambleScanner_13"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_13") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_14(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getKey().equals("barcodeReaderTriggerScanner_14"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_14") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_14(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getKey().equals("barcodeReaderPreambleScanner_14"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_14") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_14(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getKey().equals("barcodeReaderPostambleScanner_14"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_14") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_15(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getKey().equals("barcodeReaderTriggerScanner_15"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_15") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_15(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getKey().equals("barcodeReaderPreambleScanner_15"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_15") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_15(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getKey().equals("barcodeReaderPostambleScanner_15"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_15") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_TRIGGER_SCANNER_16(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getKey().equals("barcodeReaderTriggerScanner_16"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderTriggerScanner_16") == BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_PREAMBLE_SCANNER_16(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getKey().equals("barcodeReaderPreambleScanner_16"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPreambleScanner_16") == BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void test_BARCODE_READER_POSTAMBLE_SCANNER_16(BufferedReader is, PrintWriter os)
  {
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getKey().equals("barcodeReaderPostambleScanner_16"));
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum("barcodeReaderPostambleScanner_16") == BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16);
    Expect.expect(BarcodeReaderConfigEnum.getConfigEnum(NO_SUCH_BARCODE_READER_KEY) == null);
    String path = stripOffView(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getFileName());
    Expect.expect(path.equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))));
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getType() == TypeEnum.BINARY);
    final List<String> LEGAL_VALUES = new LinkedList<String>();
    Object[] validValues = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValidValues();
    int validValuesSize = (validValues != null) ? validValues.length : 0;
    Expect.expect(validValuesSize <= LEGAL_VALUES.size());
    for (int i = 0; i < validValuesSize; ++i)
    {
      Expect.expect(LEGAL_VALUES.contains(validValues[i]));
    }
    Object originalValue = BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue();
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue() == null);
    for (String value : LEGAL_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect(value.equals((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()));
    }
    final List<String> TEST_VALUES = new LinkedList<String>();
    TEST_VALUES.add("MyCode");
    StringBuffer code = new StringBuffer(256);
    for (int i = 0; i < 256; ++i)
      code.append((char)i);
    TEST_VALUES.add(code.toString());
    for (String value : TEST_VALUES)
    {
      try
      {
        BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(value);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
      Expect.expect((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue().toString().equalsIgnoreCase(value));
    }
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((long)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((int)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((short)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((byte)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((double)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue((float)0);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue('x');
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(false);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(true);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Long(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Integer(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Short((short)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Byte((byte)0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Double(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Float(0));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("0.0"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Character('x'));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("x"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("false"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("true"));
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(null);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue() == null);
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue(originalValue);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    Expect.expect(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue().equals(originalValue));
  }

  /**
   * @author Bob Balliew
   */
  private void testStaticFunctions(BufferedReader is, PrintWriter os)
  {
    Map<String, Object> cacheConfig = new TreeMap<String, Object>();

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
      cacheConfig.put(configEnum.getKey(), configEnum.getValue());

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    try
    {
      BarcodeReaderConfigEnum.clear();
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
      Expect.expect(configEnum.getValue() == null);

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      try
      {
        configEnum.setValue(cacheConfig.get(configEnum.getKey()));
      }
      catch (InvalidTypeDatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      if (configEnum.getFileName().equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))))
      {
        try
        {
          if (configEnum.getType().equals(TypeEnum.BOOLEAN))
            configEnum.setValue("false");
          else if (configEnum.getType().equals(TypeEnum.INTEGER))
            configEnum.setValue("0");
          else if (configEnum.getType().equals(TypeEnum.DOUBLE))
            configEnum.setValue("0");
          else
            configEnum.setValue("JUNK");
        }
        catch (InvalidTypeDatastoreException e)
        {
          Expect.expect(false, e.getMessage());
        }
      }
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      if (configEnum.getFileName().equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))))
      {
        if (configEnum.getType().equals(TypeEnum.BOOLEAN))
          Expect.expect((Boolean)configEnum.getValue() == false);
        else if (configEnum.getType().equals(TypeEnum.INTEGER))
          Expect.expect((Integer)configEnum.getValue() == 0);
        else if (configEnum.getType().equals(TypeEnum.DOUBLE))
          Expect.expect((Double)configEnum.getValue() == 0.0);
        else
          Expect.expect(((String)configEnum.getValue()).equals("JUNK"));
      }
      else
      {
        String key = configEnum.getKey();
        Expect.expect(key != null, "key is null.");

        if (key == null)  continue;

        Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
        Object cachedValue = cacheConfig.get(key);
        Object configValue = configEnum.getValue();

        if ((cachedValue != null) && (configValue != null))
          Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
        else
          Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
      }
    }

    List<ConfigEnum> clearedEnums = null;

    try
    {
      String fileName = FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME);
      clearedEnums = BarcodeReaderConfigEnum.clear(fileName);
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      if (stripOffView(configEnum.getFileName()).equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))))
      {
        Expect.expect(clearedEnums.contains(configEnum));
        Expect.expect(configEnum.getValue() == null);
      }
      else
      {
        Expect.expect(! clearedEnums.contains(configEnum));
        String key = configEnum.getKey();
        Expect.expect(key != null, "key is null.");

        if (key == null)  continue;

        Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
        Object cachedValue = cacheConfig.get(key);
        Object configValue = configEnum.getValue();

        if ((cachedValue != null) && (configValue != null))
          Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
        else
          Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
      }
    }

    for (ConfigEnum configEnum : clearedEnums)
    {
      try
      {
        configEnum.setValue(cacheConfig.get(configEnum.getKey()));
      }
      catch (InvalidTypeDatastoreException e)
      {
        Expect.expect(false, e.getMessage());
      }
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    try
    {
      BarcodeReaderConfigEnum.clear();
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
      Expect.expect(configEnum.getValue() == null);

    try
    {
      BarcodeReaderConfigEnum.undoLastClear();
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    clearedEnums = null;

    try
    {
      String fileName = FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME);
      clearedEnums = BarcodeReaderConfigEnum.clear(fileName);
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      if (stripOffView(configEnum.getFileName()).equals(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME))))
      {
        Expect.expect(clearedEnums.contains(configEnum));
        Expect.expect(configEnum.getValue() == null);
      }
      else
      {
        Expect.expect(! clearedEnums.contains(configEnum));
        String key = configEnum.getKey();
        Expect.expect(key != null, "key is null.");

        if (key == null)  continue;

        Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
        Object cachedValue = cacheConfig.get(key);
        Object configValue = configEnum.getValue();

        if ((cachedValue != null) && (configValue != null))
          Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
        else
          Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
      }
    }

    try
    {
      BarcodeReaderConfigEnum.undoLastClear();
    }
    catch (InvalidTypeDatastoreException e)
    {
      Expect.expect(false);
    }

    for (ConfigEnum configEnum : BarcodeReaderConfigEnum.getAllConfigEnums())
    {
      String key = configEnum.getKey();
      Expect.expect(key != null, "key is null.");

      if (key == null)  continue;

      Expect.expect(cacheConfig.containsKey(configEnum.getKey()), "key: " + configEnum.getKey() + " from config not found in cache.");
      Object cachedValue = cacheConfig.get(key);
      Object configValue = configEnum.getValue();

      if ((cachedValue != null) && (configValue != null))
        Expect.expect(cacheConfig.get(configEnum.getKey()).equals(configEnum.getValue()), "key: " + configEnum.getKey() + " cache value: " + cacheConfig.get(configEnum.getKey()) + "config: " + configEnum.getValue());
      else
        Expect.expect(cachedValue == configValue, "key: " + configEnum.getKey() + " both values not null.");
    }

    Set<String> origConfigurationFilesExpected = new TreeSet<String>();
    origConfigurationFilesExpected.add(stripOffView(FileName.getSoftwareConfigFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getHardwareCalibFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getHardwareConfigFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveXaxisConfigFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveYaxisConfigFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveRailWidthAxisConfigFullPath()));
    origConfigurationFilesExpected.add(stripOffView(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME)));
//    for(SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      origConfigurationFilesExpected.add(stripOffView(FileName.getSystemConfigFullPath(XrayTester.getSystemType())));
    Set<String> relConfigFiles = new TreeSet<String>();

    for (String name : BarcodeReaderConfigEnum.getConfigFileNames())
    {
      final String relName = stripOffView(name);
      Expect.expect(! relConfigFiles.contains(relName));
      relConfigFiles.add(relName);
    }

    Expect.expect(origConfigurationFilesExpected.equals(relConfigFiles));

    Set<String> newConfigurationFilesExpected = new TreeSet<String>();
    newConfigurationFilesExpected.add(stripOffView(FileName.getSoftwareConfigFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getHardwareCalibFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getHardwareConfigFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveXaxisConfigFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveYaxisConfigFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getKollmorgenCdMotionDriveRailWidthAxisConfigFullPath()));
    newConfigurationFilesExpected.add(stripOffView(FileName.getBarcodeReaderConfigurationFullPath("GARBAGE")));
    newConfigurationFilesExpected.add(stripOffView(FileName.getSystemConfigFullPath(XrayTester.getSystemType())));
    relConfigFiles.clear();
    BarcodeReaderConfigEnum.changeFileName(FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME), FileName.getBarcodeReaderConfigurationFullPath("GARBAGE"));

    for (String name : BarcodeReaderConfigEnum.getConfigFileNames())
    {
      final String relName = stripOffView(name);
      Expect.expect(! relConfigFiles.contains(relName));
      relConfigFiles.add(relName);
    }

    Expect.expect(newConfigurationFilesExpected.equals(relConfigFiles));

    relConfigFiles.clear();
    BarcodeReaderConfigEnum.changeFileName(FileName.getBarcodeReaderConfigurationFullPath("GARBAGE"), FileName.getBarcodeReaderConfigurationFullPath(_DEFAULT_BARCODE_READER_CONFIGURATION_NAME));

    for (String name : BarcodeReaderConfigEnum.getConfigFileNames())
    {
      final String relName = stripOffView(name);
      Expect.expect(! relConfigFiles.contains(relName));
      relConfigFiles.add(relName);
    }
  }

  /**
   * @author Bob Balliew
   */
  private void writeBarcodeReaderConfig(final String pathName, final List<String> lines)
  {
    FileWriter fw = null;

    try
    {
      fw = new FileWriter(pathName);
    }
    catch (IOException ioe)
    {
      Expect.expect(false, ioe.getMessage());
    }

    PrintWriter os = new PrintWriter(new BufferedWriter(fw));

    for (String line : lines)
      os.println(line);

    if (os != null)
      os.close();
  }

  /**
   * @author Bob Balliew
   */
  private List<String> readBarcodeReaderConfig(final String pathName)
  {
    List<String> lines = new ArrayList<String>(250);
    FileReader fr = null;

    try
    {
      fr = new FileReader(pathName);
    }
    catch(FileNotFoundException fnfe)
    {
      Expect.expect(false, fnfe.getMessage());
    }

    BufferedReader is = new BufferedReader(fr);

    try
    {
      String line = null;

      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          Expect.expect(false, ioe.getMessage());
        }

        if (line != null)
          lines.add(line);

      } while (line != null);
    }
    finally
    {
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          Expect.expect(false, ex.getMessage());
        }
      }
    }

    return lines;
  }

  /**
   * @author Bob Balliew
   */
  private void checkBarcodeValueTypes()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).equals(TypeEnum.STRING));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS).equals(TypeEnum.INTEGER));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE).equals(TypeEnum.BOOLEAN));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).equals(TypeEnum.BINARY));
    Expect.expect(config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).equals(TypeEnum.BINARY));
  }

  /**
   * @author Bob Balliew
   */
  private void checkValidValuesTypes()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).size() == 3);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).size() == 4);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).size() == 4);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).size() == 3);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).size() == 14);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS).size() == 2);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).size() == 0);
    Expect.expect(config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).size() == 0);
  }

  /**
   * @author Bob Balliew
   */
  private List<String> createBarcodeConfiguration_1()
  {
    final int NUMBER_OF_LINES = 72;
    List<String> lines = new ArrayList<String>(NUMBER_OF_LINES);
    lines.add("barcodeReaderNumberOfScanners = 2");
    lines.add("barcodeReaderOutputBufferSize = 16");
    lines.add("barcodeReaderInputBufferSize = 32");
    lines.add("barcodeReaderCharsetName = UTF-8");
    lines.add("barcodeReaderSerialPortName = COM3");
    lines.add("barcodeReaderRs232DataBits = 6");
    lines.add("barcodeReaderRs232RtsCtsFlowControlIn = false");
    lines.add("barcodeReaderRs232RtsCtsFlowControlOut = false");
    lines.add("barcodeReaderRs232XonXoffFlowControlIn = false");
    lines.add("barcodeReaderRs232XonXoffFlowControlOut = false");
    lines.add("barcodeReaderRs232Parity = ODD");
    lines.add("barcodeReaderRs232StopBits = 1");
    lines.add("barcodeReaderRs232BitsPerSecond = 2400");
    lines.add("barcodeReaderRs232ApplicationName = barcode_reader");
    lines.add("barcodeReaderRs232OpenTimeoutInMilliseconds = 20");
    lines.add("barcodeReaderEnableReceiveFraming = false");
    lines.add("barcodeReaderReceiveFramingByte = 63");
    lines.add("barcodeReaderEnableReceiveThreshold = false");
    lines.add("barcodeReaderReceiveThresholdCount = 12");
    lines.add("barcodeReaderEnableReceiveTimeout = false");
    lines.add("barcodeReaderReceiveTimeoutInMilliseconds = 5000");
    lines.add("barcodeReaderEnableNoReadMessage = false");
    lines.add("barcodeReaderReceiveNoReadMessage = 4e,6f,20,52,65,61,64");
    lines.add("barcodeReaderEnableBadSymbolMessage = false");
    lines.add("barcodeReaderReceiveBadSymbolMessage = 42,61,64,20,43,6f,64,65");
    lines.add("barcodeReaderEnableNoLabelMessage = false");
    lines.add("barcodeReaderReceiveNoLabelMessage = 4e,6f,20,4c,61,62,65,6c");
    lines.add("barcodeReaderTriggerScanner_1 = 3c,11,3e");
    lines.add("barcodeReaderPreambleScanner_1 = 41");
    lines.add("barcodeReaderPostambleScanner_1 = 42");
    lines.add("barcodeReaderTriggerScanner_2 = 3c,12,3e");
    lines.add("barcodeReaderPreambleScanner_2 = 43");
    lines.add("barcodeReaderPostambleScanner_2 = 44");
    lines.add("barcodeReaderTriggerScanner_3 = 3c,13,3e");
    lines.add("barcodeReaderPreambleScanner_3 = 45");
    lines.add("barcodeReaderPostambleScanner_3 = 46");
    lines.add("barcodeReaderTriggerScanner_4 = 3c,14,3e");
    lines.add("barcodeReaderPreambleScanner_4 = 47");
    lines.add("barcodeReaderPostambleScanner_4 = 48");
    lines.add("barcodeReaderTriggerScanner_5 = 3c,15,3e");
    lines.add("barcodeReaderPreambleScanner_5 = 49");
    lines.add("barcodeReaderPostambleScanner_5 = 4a");
    lines.add("barcodeReaderTriggerScanner_6 = 3c,16,3e");
    lines.add("barcodeReaderPreambleScanner_6 = 4b");
    lines.add("barcodeReaderPostambleScanner_6 = 4c");
    lines.add("barcodeReaderTriggerScanner_7 = 3c,17,3e");
    lines.add("barcodeReaderPreambleScanner_7 = 4d");
    lines.add("barcodeReaderPostambleScanner_7 = 4e");
    lines.add("barcodeReaderTriggerScanner_8 = 3c,18,3e");
    lines.add("barcodeReaderPreambleScanner_8 = 4f");
    lines.add("barcodeReaderPostambleScanner_8 = 50");
    lines.add("barcodeReaderTriggerScanner_9 = 3c,19,3e");
    lines.add("barcodeReaderPreambleScanner_9 = 51");
    lines.add("barcodeReaderPostambleScanner_9 = 52");
    lines.add("barcodeReaderTriggerScanner_10 = 3c,1a,3e");
    lines.add("barcodeReaderPreambleScanner_10 = 53");
    lines.add("barcodeReaderPostambleScanner_10 = 54");
    lines.add("barcodeReaderTriggerScanner_11 = 3c,1b,3e");
    lines.add("barcodeReaderPreambleScanner_11 = 55");
    lines.add("barcodeReaderPostambleScanner_11 = 56");
    lines.add("barcodeReaderTriggerScanner_12 = 3c,1c,3e");
    lines.add("barcodeReaderPreambleScanner_12 = 57");
    lines.add("barcodeReaderPostambleScanner_12 = 58");
    lines.add("barcodeReaderTriggerScanner_13 = 3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_13 = 59");
    lines.add("barcodeReaderPostambleScanner_13 = 5a");
    lines.add("barcodeReaderTriggerScanner_14 = 3c,1e,3e");
    lines.add("barcodeReaderPreambleScanner_14 = 61");
    lines.add("barcodeReaderPostambleScanner_14 = 62");
    lines.add("barcodeReaderTriggerScanner_15 = 3c,1f,3e");
    lines.add("barcodeReaderPreambleScanner_15 = 63");
    lines.add("barcodeReaderPostambleScanner_15 = 64");
    lines.add("barcodeReaderTriggerScanner_16 = 3c,20,3e");
    lines.add("barcodeReaderPreambleScanner_16 = 65");
    lines.add("barcodeReaderPostambleScanner_16 = 66");
    return lines;
  }

  /**
   * @author Bob Balliew
   */
  private boolean isBarcodeConfiguration_1(final List<String> lines)
  {
    final List<String> expected = createBarcodeConfiguration_1();
    return lines.containsAll(expected);
  }

  /**
   * @author Bob Balliew
   */
  private void checkBarcodeConfiguration_1()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS) == 2);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE) == 16);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE) == 32);
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).equals("UTF-8"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).equals("COM3"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).equals("6"));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).equals("ODD"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).equals("1"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).equals("2400"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).equals("barcode_reader"));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS) == 20);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE) == 63);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT) == 12);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS) == 5000);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).equals("No Read"));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).equals("Bad Code"));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).equals("No Label"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).equals("<\u0011>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).equals("A"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).equals("B"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).equals("<\u0012>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).equals("C"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).equals("D"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).equals("<\u0013>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).equals("E"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).equals("F"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).equals("<\u0014>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).equals("G"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).equals("H"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).equals("<\u0015>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).equals("I"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).equals("J"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).equals("<\u0016>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).equals("K"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).equals("L"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).equals("<\u0017>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).equals("M"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).equals("N"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).equals("<\u0018>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).equals("O"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).equals("P"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).equals("<\u0019>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).equals("Q"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).equals("R"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).equals("<\u001a>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).equals("S"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).equals("T"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).equals("<\u001b>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).equals("U"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).equals("V"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).equals("<\u001c>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).equals("W"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).equals("X"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).equals("<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).equals("Y"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).equals("Z"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).equals("<\u001e>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).equals("a"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).equals("b"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).equals("<\u001f>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).equals("c"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).equals("d"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).equals("< >"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).equals("e"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).equals("f"));

    checkBarcodeValueTypes();
    checkValidValuesTypes();
  }

  /**
   * @author Bob Balliew
   */
  private void checkBarcodeEnumeration_1()
  {
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == 2);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == 16);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == 32);
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("UTF-8"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("COM3"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("6"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue()).equals(false));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue()).equals(false));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue()).equals(false));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue()).equals(false));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("ODD"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("1"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("2400"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("barcode_reader"));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == 20);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == 63);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == 12);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == 5000);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue()).equals(false));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("No Read"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue()).equals(false));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("Bad Code"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue()).equals(false));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("No Label"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("<\u0011>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("A"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("B"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("<\u0012>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("C"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("D"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("<\u0013>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("E"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("F"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("<\u0014>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("G"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("H"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("<\u0015>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("I"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("J"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("<\u0016>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("K"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("L"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("<\u0017>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("M"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("N"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("<\u0018>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("O"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("P"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("<\u0019>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("Q"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("R"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("<\u001a>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("S"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("T"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("<\u001b>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("U"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("V"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("<\u001c>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("W"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("X"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("Y"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("Z"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("<\u001e>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("a"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("b"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("<\u001f>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("c"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("d"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("< >"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("e"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("f"));
  }

  /**
   * @author Bob Balliew
   */
  private void checkNotBarcodeConfiguration_1()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS) != 2);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE) != 16);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE) != 32);
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).equals("UTF-8"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).equals("COM3"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).equals("6"));
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN));
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT));
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN));
//    Expect.expect( config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).equals("ODD"));
//    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).equals("1"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).equals("2400"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).equals("barcode_reader"));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS) != 20);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE) != 63);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT) != 12);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS) != 5000);
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).equals("No Read"));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).equals("Bad Code"));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).equals("No Label"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).equals("<\u0011>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).equals("A"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).equals("B"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).equals("<\u0012>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).equals("C"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).equals("D"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).equals("<\u0013>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).equals("E"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).equals("F"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).equals("<\u0014>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).equals("G"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).equals("H"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).equals("<\u0015>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).equals("I"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).equals("J"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).equals("<\u0016>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).equals("K"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).equals("L"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).equals("<\u0017>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).equals("M"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).equals("N"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).equals("<\u0018>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).equals("O"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).equals("P"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).equals("<\u0019>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).equals("Q"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).equals("R"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).equals("<\u001a>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).equals("S"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).equals("T"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).equals("<\u001b>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).equals("U"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).equals("V"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).equals("<\u001c>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).equals("W"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).equals("X"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).equals("<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).equals("Y"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).equals("Z"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).equals("<\u001e>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).equals("a"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).equals("b"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).equals("<\u001f>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).equals("c"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).equals("d"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).equals("< >"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).equals("e"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).equals("f"));
  }

  /**
   * @author Bob Balliew
   */
  private void checkNotBarcodeEnumeration_1()
  {
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() != 2);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() != 16);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() != 32);
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("UTF-8"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("COM3"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("6"));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue()).equals(false));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue()).equals(true));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue()).equals(false));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue()).equals(true));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("ODD"));
//    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("1"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("2400"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("barcode_reader"));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() != 20);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue()).equals(true));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() != 63);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue()).equals(true));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() != 12);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue()).equals(true));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() != 5000);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue()).equals(false));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("No Read"));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue()).equals(false));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("Bad Code"));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue()).equals(false));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("No Label"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("<\u0011>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("A"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("B"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("<\u0012>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("C"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("D"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("<\u0013>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("E"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("F"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("<\u0014>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("G"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("H"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("<\u0015>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("I"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("J"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("<\u0016>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("K"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("L"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("<\u0017>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("M"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("N"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("<\u0018>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("O"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("P"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("<\u0019>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("Q"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("R"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("<\u001a>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("S"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("T"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("<\u001b>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("U"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("V"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("<\u001c>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("W"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("X"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("Y"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("Z"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("<\u001e>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("a"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("b"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("<\u001f>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("c"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("d"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("< >"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("e"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("f"));
  }

  /**
   * @author Bob Balliew
   */
  private void putBarcodeEnumeration_1()
  {
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(2);
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(16);
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(32);
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue("UTF-8");
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue("COM3");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue("6");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue("ODD");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue("1");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue("2400");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue("barcode_reader");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(20);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(63);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(12);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(5000);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue("No Read");
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue("Bad Code");
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue("No Label");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue("<\u0011>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue("A");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue("B");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue("<\u0012>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue("C");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue("D");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue("<\u0013>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue("E");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue("F");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue("<\u0014>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue("G");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue("H");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue("<\u0015>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue("I");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue("J");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue("<\u0016>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue("K");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue("L");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue("<\u0017>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue("M");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue("N");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue("<\u0018>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue("O");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue("P");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue("<\u0019>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue("Q");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue("R");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue("<\u001a>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue("S");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue("T");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue("<\u001b>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue("U");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue("V");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue("<\u001c>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue("W");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue("X");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue("<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue("Y");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue("Z");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue("<\u001e>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue("a");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue("b");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue("<\u001f>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue("c");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue("d");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue("< >");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue("e");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue("f");
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }

  /**
   * @author Bob Balliew
   */
  private List<String> createBarcodeConfiguration_2()
  {
    final int NUMBER_OF_LINES = 72;
    List<String> lines = new ArrayList<String>(NUMBER_OF_LINES);
    lines.add("barcodeReaderNumberOfScanners = 16");
    lines.add("barcodeReaderOutputBufferSize = 4096");
    lines.add("barcodeReaderInputBufferSize = 256");
    lines.add("barcodeReaderCharsetName = ISO-8859-1");
    lines.add("barcodeReaderSerialPortName = COM2");
    lines.add("barcodeReaderRs232DataBits = 8");
    lines.add("barcodeReaderRs232RtsCtsFlowControlIn = true");
    lines.add("barcodeReaderRs232RtsCtsFlowControlOut = false");
    lines.add("barcodeReaderRs232XonXoffFlowControlIn = true");
    lines.add("barcodeReaderRs232XonXoffFlowControlOut = false");
    lines.add("barcodeReaderRs232Parity = NONE");
    lines.add("barcodeReaderRs232StopBits = 2");
    lines.add("barcodeReaderRs232BitsPerSecond = 38400");
    lines.add("barcodeReaderRs232ApplicationName = x6000_bcr");
    lines.add("barcodeReaderRs232OpenTimeoutInMilliseconds = 100");
    lines.add("barcodeReaderEnableReceiveFraming = false");
    lines.add("barcodeReaderReceiveFramingByte = 10");
    lines.add("barcodeReaderEnableReceiveThreshold = false");
    lines.add("barcodeReaderReceiveThresholdCount = 50");
    lines.add("barcodeReaderEnableReceiveTimeout = false");
    lines.add("barcodeReaderReceiveTimeoutInMilliseconds = 0");
    lines.add("barcodeReaderEnableNoReadMessage = true");
    lines.add("barcodeReaderReceiveNoReadMessage = 3f");
    lines.add("barcodeReaderEnableBadSymbolMessage = true");
    lines.add("barcodeReaderReceiveBadSymbolMessage = 3f,21,3f");
    lines.add("barcodeReaderEnableNoLabelMessage = true");
    lines.add("barcodeReaderReceiveNoLabelMessage = 3f,3f");
    lines.add("barcodeReaderTriggerScanner_1 = 30,31,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_1 = 30,31");
    lines.add("barcodeReaderPostambleScanner_1 = 30,31,d,a");
    lines.add("barcodeReaderTriggerScanner_2 = 30,32,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_2 = 30,32");
    lines.add("barcodeReaderPostambleScanner_2 = 30,32,d,a");
    lines.add("barcodeReaderTriggerScanner_3 = 30,33,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_3 = 30,33");
    lines.add("barcodeReaderPostambleScanner_3 = 30,33,d,a");
    lines.add("barcodeReaderTriggerScanner_4 = 30,34,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_4 = 30,34");
    lines.add("barcodeReaderPostambleScanner_4 = 30,34,d,a");
    lines.add("barcodeReaderTriggerScanner_5 = 30,35,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_5 = 30,35");
    lines.add("barcodeReaderPostambleScanner_5 = 30,35,d,a");
    lines.add("barcodeReaderTriggerScanner_6 = 30,36,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_6 = 30,36");
    lines.add("barcodeReaderPostambleScanner_6 = 30,36,d,a");
    lines.add("barcodeReaderTriggerScanner_7 = 30,37,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_7 = 30,37");
    lines.add("barcodeReaderPostambleScanner_7 = 30,37,d,a");
    lines.add("barcodeReaderTriggerScanner_8 = 30,38,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_8 = 30,38");
    lines.add("barcodeReaderPostambleScanner_8 = 30,38,d,a");
    lines.add("barcodeReaderTriggerScanner_9 = 30,39,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_9 = 30,39");
    lines.add("barcodeReaderPostambleScanner_9 = 30,39,d,a");
    lines.add("barcodeReaderTriggerScanner_10 = 31,30,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_10 = 31,30");
    lines.add("barcodeReaderPostambleScanner_10 = 31,30,d,a");
    lines.add("barcodeReaderTriggerScanner_11 = 31,31,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_11 = 31,31");
    lines.add("barcodeReaderPostambleScanner_11 = 31,31,d,a");
    lines.add("barcodeReaderTriggerScanner_12 = 31,32,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_12 = 31,32");
    lines.add("barcodeReaderPostambleScanner_12 = 31,32,d,a");
    lines.add("barcodeReaderTriggerScanner_13 = 31,33,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_13 = 31,33");
    lines.add("barcodeReaderPostambleScanner_13 = 31,33,d,a");
    lines.add("barcodeReaderTriggerScanner_14 = 31,34,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_14 = 31,34");
    lines.add("barcodeReaderPostambleScanner_14 = 31,34,d,a");
    lines.add("barcodeReaderTriggerScanner_15 = 31,35,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_15 = 31,35");
    lines.add("barcodeReaderPostambleScanner_15 = 31,35,d,a");
    lines.add("barcodeReaderTriggerScanner_16 = 31,36,3c,1d,3e");
    lines.add("barcodeReaderPreambleScanner_16 = 31,36");
    lines.add("barcodeReaderPostambleScanner_16 = 31,36,d,a");
    return lines;
  }

  /**
   * @author Bob Balliew
   */
  private boolean isBarcodeConfiguration_2(final List<String> lines)
  {
    final List<String> expected = createBarcodeConfiguration_2();
    return lines.containsAll(expected);
  }

  /**
   * @author Bob Balliew
   */
  private void checkBarcodeConfiguration_2()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS) == 16);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE) == 4096);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE) == 256);
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).equals("ISO-8859-1"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).equals("COM2"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).equals("8"));
    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN));
    Expect.expect(!config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT));
    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN));
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).equals("NONE"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).equals("2"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).equals("38400"));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).equals("x6000_bcr"));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS) == 100);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE) == 10);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT) == 50);
    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS) == 0);
    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).equals("?"));
    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).equals("?!?"));
    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE));
    Expect.expect(config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).equals("??"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).equals("01<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).equals("01"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).equals("01\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).equals("02<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).equals("02"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).equals("02\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).equals("03<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).equals("03"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).equals("03\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).equals("04<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).equals("04"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).equals("04\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).equals("05<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).equals("05"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).equals("05\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).equals("06<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).equals("06"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).equals("06\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).equals("07<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).equals("07"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).equals("07\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).equals("08<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).equals("08"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).equals("08\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).equals("09<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).equals("09"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).equals("09\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).equals("10<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).equals("10"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).equals("10\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).equals("11<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).equals("11"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).equals("11\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).equals("12<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).equals("12"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).equals("12\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).equals("13<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).equals("13"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).equals("13\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).equals("14<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).equals("14"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).equals("14\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).equals("15<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).equals("15"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).equals("15\r\n"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).equals("16<\u001d>"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).equals("16"));
    Expect.expect(config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).equals("16\r\n"));

    checkBarcodeValueTypes();
    checkValidValuesTypes();
  }

  /**
   * @author Bob Balliew
   */
  private void checkBarcodeEnumeration_2()
  {
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() == 16);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() == 4096);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() == 256);
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("ISO-8859-1"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("COM2"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("8"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue()).equals(true));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue()).equals(false));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue()).equals(true));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue()).equals(false));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("NONE"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("2"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("38400"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("x6000_bcr"));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() == 100);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() == 10);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() == 50);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() == 0);
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.getValue()).equals(true));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("?"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.getValue()).equals(true));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("?!?"));
    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.getValue()).equals(true));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("??"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("01<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("01"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("01\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("02<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("02"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("02\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("03<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("03"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("03\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("04<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("04"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("04\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("05<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("05"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("05\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("06<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("06"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("06\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("07<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("07"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("07\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("08<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("08"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("08\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("09<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("09"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("09\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("10<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("10"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("10\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("11<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("11"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("11\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("12<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("12"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("12\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("13<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("13"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("13\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("14<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("14"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("14\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("15<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("15"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("15\r\n"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("16<\u001d>"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("16"));
    Expect.expect(((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("16\r\n"));
  }

  /**
   * @author Bob Balliew
   */
  private void checkNotBarcodeConfiguration_2()
  {
    Config config = Config.getInstance();
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS) != 16);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE) != 4096);
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE) != 256);
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME).equals("ISO-8859-1"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME).equals("COM2"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS).equals("8"));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN));
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN));
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY).equals("NONE"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS).equals("2"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND).equals("38400"));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME).equals("x6000_bcr"));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS) != 100);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE) != 10);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT) != 50);
//    Expect.expect(config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT));
    Expect.expect(config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS) != 0);
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE).equals("?"));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE).equals("?!?"));
//    Expect.expect(! config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE));
    Expect.expect(! config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE).equals("??"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1).equals("01<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1).equals("01"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1).equals("01\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2).equals("02<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2).equals("02"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2).equals("02\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3).equals("03<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3).equals("03"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3).equals("03\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4).equals("04<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4).equals("04"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4).equals("04\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5).equals("05<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5).equals("05"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5).equals("05\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6).equals("06<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6).equals("06"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6).equals("06\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7).equals("07<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7).equals("07"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7).equals("07\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8).equals("08<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8).equals("08"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8).equals("08\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9).equals("09<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9).equals("09"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9).equals("09\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10).equals("10<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10).equals("10"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10).equals("10\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11).equals("11<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11).equals("11"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11).equals("11\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12).equals("12<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12).equals("12"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12).equals("12\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13).equals("13<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13).equals("13"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13).equals("13\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14).equals("14<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14).equals("14"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14).equals("14\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15).equals("15<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15).equals("15"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15).equals("15\r\n"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16).equals("16<\u001d>"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16).equals("16"));
    Expect.expect(! config.getBinaryValue(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16).equals("16\r\n"));
  }

  /**
   * @author Bob Balliew
   */
  private void checkNotBarcodeEnumeration_2()
  {
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.getValue() != 16);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.getValue() != 4096);
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.getValue() != 256);
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.getValue()).equals("ISO-8859-1"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getValue()).equals("COM2"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.getValue()).equals("8"));//
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.getValue()).equals(true));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.getValue()).equals(false));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.getValue()).equals(true));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.getValue()).equals(false));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.getValue()).equals("NONE"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.getValue()).equals("2"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.getValue()).equals("38400"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.getValue()).equals("x6000_bcr"));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.getValue() != 100);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.getValue() != 10);
//    Expect.expect(((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.getValue() != 50);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.getValue()).equals(false));
    Expect.expect((Integer)BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.getValue() != 0);
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals(true));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.getValue()).equals("?"));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals(true));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.getValue()).equals("?!?"));
//    Expect.expect(! ((Boolean)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals(true));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.getValue()).equals("??"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.getValue()).equals("01<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.getValue()).equals("01"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.getValue()).equals("01\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.getValue()).equals("02<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.getValue()).equals("02"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.getValue()).equals("02\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.getValue()).equals("03<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.getValue()).equals("03"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.getValue()).equals("03\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.getValue()).equals("04<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.getValue()).equals("04"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.getValue()).equals("04\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.getValue()).equals("05<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.getValue()).equals("05"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.getValue()).equals("05\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.getValue()).equals("06<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.getValue()).equals("06"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.getValue()).equals("06\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.getValue()).equals("07<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.getValue()).equals("07"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.getValue()).equals("07\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.getValue()).equals("08<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.getValue()).equals("08"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.getValue()).equals("08\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.getValue()).equals("09<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.getValue()).equals("09"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.getValue()).equals("09\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.getValue()).equals("10<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.getValue()).equals("10"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.getValue()).equals("10\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.getValue()).equals("11<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.getValue()).equals("11"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.getValue()).equals("11\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.getValue()).equals("12<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.getValue()).equals("12"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.getValue()).equals("12\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.getValue()).equals("13<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.getValue()).equals("13"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.getValue()).equals("13\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.getValue()).equals("14<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.getValue()).equals("14"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.getValue()).equals("14\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.getValue()).equals("15<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.getValue()).equals("15"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.getValue()).equals("15\r\n"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.getValue()).equals("16<\u001d>"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.getValue()).equals("16"));
    Expect.expect(! ((String)BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.getValue()).equals("16\r\n"));
  }

  /**
   * @author Bob Balliew
   */
  private void putBarcodeEnumeration_2()
  {
    try
    {
      BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS.setValue(16);
      BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE.setValue(4096);
      BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE.setValue(256);
      BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME.setValue("ISO-8859-1");
      BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.setValue("COM2");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS.setValue("8");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN.setValue(true);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN.setValue(true);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY.setValue("NONE");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS.setValue("2");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND.setValue("38400");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME.setValue("x6000_bcr");
      BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS.setValue(100);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE.setValue(10);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT.setValue(50);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT.setValue(false);
      BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS.setValue(0);
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE.setValue(true);
      BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE.setValue("?");
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE.setValue(true);
      BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE.setValue("?!?");
      BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE.setValue(true);
      BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE.setValue("??");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1.setValue("01<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1.setValue("01");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1.setValue("01\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2.setValue("02<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2.setValue("02");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2.setValue("02\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3.setValue("03<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3.setValue("03");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3.setValue("03\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4.setValue("04<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4.setValue("04");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4.setValue("04\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5.setValue("05<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5.setValue("05");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5.setValue("05\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6.setValue("06<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6.setValue("06");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6.setValue("06\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7.setValue("07<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7.setValue("07");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7.setValue("07\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8.setValue("08<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8.setValue("08");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8.setValue("08\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9.setValue("09<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9.setValue("09");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9.setValue("09\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10.setValue("10<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10.setValue("10");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10.setValue("10\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11.setValue("11<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11.setValue("11");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11.setValue("11\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12.setValue("12<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12.setValue("12");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12.setValue("12\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13.setValue("13<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13.setValue("13");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13.setValue("13\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14.setValue("14<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14.setValue("14");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14.setValue("14\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15.setValue("15<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15.setValue("15");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15.setValue("15\r\n");
      BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16.setValue("16<\u001d>");
      BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16.setValue("16");
      BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16.setValue("16\r\n");
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }

  /**
   * @author Bob Balliew
   */
  private void testConfigLoadSave(BufferedReader is, PrintWriter os)
  {
    Config config = Config.getInstance();
    final String DEFAULT_BCR_CONFIG = config.getStringValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE);
    final String TEST_BCR_CONFIG_NAME_1 = "TEST_1";
    final String TEST_BCR_CONFIG_NAME_2 = "TEST_2";
    final String DEFAULT_BCR_CONFIG_FULL_PATH = FileName.getBarcodeReaderConfigurationFullPath(DEFAULT_BCR_CONFIG);
    final String TEST_BCR_CONFIG_FULL_PATH_1 = FileName.getBarcodeReaderConfigurationFullPath(TEST_BCR_CONFIG_NAME_1);
    final String TEST_BCR_CONFIG_FULL_PATH_2 = FileName.getBarcodeReaderConfigurationFullPath(TEST_BCR_CONFIG_NAME_2);
    final List<String> BCR_CONFIG_LINES_1 = createBarcodeConfiguration_1();
    final List<String> BCR_CONFIG_LINES_2 = createBarcodeConfiguration_2();
    final List<String> BCR_CONFIG_LINES_ORIG = readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH);

    checkNotBarcodeConfiguration_1();
    checkNotBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));

    writeBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1, BCR_CONFIG_LINES_1);

    checkNotBarcodeConfiguration_1();
    checkNotBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));

    try
    {
      config.load(DEFAULT_BCR_CONFIG_FULL_PATH, TEST_BCR_CONFIG_FULL_PATH_1);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
      return;
    }

    checkBarcodeConfiguration_1();
    checkBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));

    writeBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2, BCR_CONFIG_LINES_2);

    checkBarcodeConfiguration_1();
    checkBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

    try
     {
       config.load(TEST_BCR_CONFIG_FULL_PATH_1, TEST_BCR_CONFIG_FULL_PATH_2);
     }
     catch (DatastoreException e)
     {
       Expect.expect(false, e.getMessage());
       return;
     }

     checkNotBarcodeConfiguration_1();
     checkNotBarcodeEnumeration_1();
     checkBarcodeConfiguration_2();
     checkBarcodeEnumeration_2();
     Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
     Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
     Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

     writeBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2, BCR_CONFIG_LINES_1);

     checkNotBarcodeConfiguration_1();
     checkNotBarcodeEnumeration_1();
     checkBarcodeConfiguration_2();
     checkBarcodeEnumeration_2();
     Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
     Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
     Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
     Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
     Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

     try
      {
        config.load(TEST_BCR_CONFIG_FULL_PATH_2);
      }
      catch (DatastoreException e)
      {
        Expect.expect(false, e.getMessage());
        return;
      }

      checkBarcodeConfiguration_1();
      checkBarcodeEnumeration_1();
      checkNotBarcodeConfiguration_2();
      checkNotBarcodeEnumeration_2();
      Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

      putBarcodeEnumeration_2();

      checkNotBarcodeConfiguration_1();
      checkNotBarcodeEnumeration_1();
      checkBarcodeConfiguration_2();
      checkBarcodeEnumeration_2();
      Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

      try
       {
         config.load(TEST_BCR_CONFIG_FULL_PATH_2);
       }
       catch (DatastoreException e)
       {
         Expect.expect(false, e.getMessage());
         return;
       }

       checkBarcodeConfiguration_1();
       checkBarcodeEnumeration_1();
       checkNotBarcodeConfiguration_2();
       checkNotBarcodeEnumeration_2();
       Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

      putBarcodeEnumeration_1();

      checkBarcodeConfiguration_1();
      checkBarcodeEnumeration_1();
      checkNotBarcodeConfiguration_2();
      checkNotBarcodeEnumeration_2();
      Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

      putBarcodeEnumeration_2();

      checkNotBarcodeConfiguration_1();
      checkNotBarcodeEnumeration_1();
      checkBarcodeConfiguration_2();
      checkBarcodeEnumeration_2();
      Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
      Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
      Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

      try
       {
         config.save(TEST_BCR_CONFIG_FULL_PATH_2);
       }
       catch (DatastoreException e)
       {
         Expect.expect(false, e.getMessage());
         return;
       }

       checkNotBarcodeConfiguration_1();
       checkNotBarcodeEnumeration_1();
       checkBarcodeConfiguration_2();
       checkBarcodeEnumeration_2();
       Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
       Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

       try
       {
         FileUtilAxi.delete(TEST_BCR_CONFIG_FULL_PATH_1);
       }
       catch (DatastoreException e)
       {
         Expect.expect(false, e.getMessage());
       }

       checkNotBarcodeConfiguration_1();
       checkNotBarcodeEnumeration_1();
       checkBarcodeConfiguration_2();
       checkBarcodeEnumeration_2();
       Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
       Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
       Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

       try
        {
          config.save(TEST_BCR_CONFIG_FULL_PATH_2, TEST_BCR_CONFIG_FULL_PATH_1);
        }
        catch (DatastoreException e)
        {
          Expect.expect(false, e.getMessage());
          return;
        }

        checkNotBarcodeConfiguration_1();
        checkNotBarcodeEnumeration_1();
        checkBarcodeConfiguration_2();
        checkBarcodeEnumeration_2();
        Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
        Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
        Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

        writeBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1, BCR_CONFIG_LINES_1);

        checkNotBarcodeConfiguration_1();
        checkNotBarcodeEnumeration_1();
        checkBarcodeConfiguration_2();
        checkBarcodeEnumeration_2();
        Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
        Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(!  isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
        Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
        Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
        Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

        try
         {
           config.save(TEST_BCR_CONFIG_FULL_PATH_1, TEST_BCR_CONFIG_FULL_PATH_1);
         }
         catch (DatastoreException e)
         {
           Expect.expect(false, e.getMessage());
           return;
         }

         checkNotBarcodeConfiguration_1();
         checkNotBarcodeEnumeration_1();
         checkBarcodeConfiguration_2();
         checkBarcodeEnumeration_2();
         Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
         Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

         putBarcodeEnumeration_1();

         checkBarcodeConfiguration_1();
         checkBarcodeEnumeration_1();
         checkNotBarcodeConfiguration_2();
         checkNotBarcodeEnumeration_2();
         Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
         Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
         Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
         Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
         Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

         try
          {
            config.save(TEST_BCR_CONFIG_FULL_PATH_1, TEST_BCR_CONFIG_FULL_PATH_2);
          }
          catch (DatastoreException e)
          {
            Expect.expect(false, e.getMessage());
            return;
          }

          checkBarcodeConfiguration_1();
          checkBarcodeEnumeration_1();
          checkNotBarcodeConfiguration_2();
          checkNotBarcodeEnumeration_2();
          Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
          Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
          Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
          Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
          Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
          Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
          Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
          Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
          Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

    try
    {
      config.load(TEST_BCR_CONFIG_FULL_PATH_2, DEFAULT_BCR_CONFIG_FULL_PATH);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    checkNotBarcodeConfiguration_1();
    checkNotBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_1)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

    try
    {
      FileUtilAxi.delete(TEST_BCR_CONFIG_FULL_PATH_1);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    checkNotBarcodeConfiguration_1();
    checkNotBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(isBarcodeConfiguration_1(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(TEST_BCR_CONFIG_FULL_PATH_2)));

    try
    {
      FileUtilAxi.delete(TEST_BCR_CONFIG_FULL_PATH_2);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    checkNotBarcodeConfiguration_1();
    checkNotBarcodeEnumeration_1();
    checkNotBarcodeConfiguration_2();
    checkNotBarcodeEnumeration_2();
    Expect.expect(BCR_CONFIG_LINES_ORIG.containsAll(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_1(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
    Expect.expect(! isBarcodeConfiguration_2(readBarcodeReaderConfig(DEFAULT_BCR_CONFIG_FULL_PATH)));
  }

  /**
   * @author Bob Balliew
   */
  private void testConfigLoadSaveExceptions(BufferedReader is, PrintWriter os)
  {
    Config config = Config.getInstance();
    try
    {
      config.load(null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.load(null, "barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.load("barf", null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.load(null, null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.save(null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.save(null, "barf");
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.save("barf", null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
    try
    {
      config.save(null, null);
      Expect.expect(false, "Should have thrown an exception.");
    }
    catch (AssertException ae)
    {
      // Expected Exception.
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }

  /**
   * @author Bob Balliew
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Must be called before the config is loaded.
    test_configNeverLoadedException(is, os);

    // Must be called before the config is loaded, causes the config to be loaded.
    test_isLoaded(is, os);

    test_BARCODE_READER_AUTOMATIC_READER_ENABLED(is, os);
    test_BARCODE_READER_CONFIGURATION_NAME_TO_USE(is, os);
    test_BARCODE_READER_CONFIGURATION_NAME_TO_EDIT(is, os);
    test_BARCODE_READER_PROTECTED_CONFIGURATION_NAMES(is, os);

    test_BARCODE_READER_NUMBER_OF_SCANNERS(is, os);
    test_BARCODE_READER_OUTPUT_BUFFER_SIZE(is, os);
    test_BARCODE_READER_INPUT_BUFFER_SIZE(is, os);
    test_BARCODE_READER_CHARSET_NAME(is, os);
    test_BARCODE_READER_SERIAL_PORT_NAME(is, os);
    test_BARCODE_READER_RS232_DATA_BITS(is, os);
    test_BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN(is, os);
    test_BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT(is, os);
    test_BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN(is, os);
    test_BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT(is, os);
    test_BARCODE_READER_RS232_PARITY(is, os);
    test_BARCODE_READER_RS232_STOP_BITS(is, os);
    test_BARCODE_READER_RS232_BITS_PER_SECOND(is, os);
    test_BARCODE_READER_RS232_APPLICATION_NAME(is, os);
    test_BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS(is, os);
    test_BARCODE_READER_ENABLE_RECEIVE_FRAMING(is, os);
    test_BARCODE_READER_RECEIVE_FRAMING_BYTE(is, os);
    test_BARCODE_READER_ENABLE_RECEIVE_THRESHOLD(is, os);
    test_BARCODE_READER_RECEIVE_THRESHOLD_COUNT(is, os);
    test_BARCODE_READER_ENABLE_RECEIVE_TIMEOUT(is, os);
    test_BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS(is, os);
    test_BARCODE_READER_ENABLE_NO_READ_MESSAGE(is, os);
    test_BARCODE_READER_NO_READ_MESSAGE(is, os);
    test_BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE(is, os);
    test_BARCODE_READER_BAD_SYMBOL_MESSAGE(is, os);
    test_BARCODE_READER_ENABLE_NO_LABEL_MESSAGE(is, os);
    test_BARCODE_READER_NO_LABEL_MESSAGE(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_1(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_1(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_1(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_2(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_2(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_2(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_3(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_3(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_3(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_4(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_4(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_4(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_5(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_5(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_5(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_6(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_6(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_6(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_7(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_7(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_7(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_8(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_8(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_8(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_9(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_9(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_9(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_10(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_10(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_10(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_11(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_11(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_11(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_12(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_12(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_12(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_13(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_13(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_13(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_14(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_14(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_14(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_15(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_15(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_15(is, os);
    test_BARCODE_READER_TRIGGER_SCANNER_16(is, os);
    test_BARCODE_READER_PREAMBLE_SCANNER_16(is, os);
    test_BARCODE_READER_POSTAMBLE_SCANNER_16(is, os);

    testConfigLoadSave(is, os);
    testConfigLoadSaveExceptions(is, os);

    testStaticFunctions(is, os);
  }
}
