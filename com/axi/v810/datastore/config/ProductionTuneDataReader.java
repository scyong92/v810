package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * The file will read in the productionTune.config file to determine
 * the current production tuning setting configuration
 *
 *
 * An example of the file syntax is:
 *
 * #######################################################################
 * # FILE: productionTune.config
 * # PURPOSE: To store all the production tuning information
 * #
 * # projectName1
 * #
 * #  projectName1
 * #  imageDestination
 * #  resultsDestination
 * #  50
 * #  400
 * #  10
 * #  notifyEmail@domain.com
 * #
 * ########################################################################
 *
 * @author Andy Mechtenberg
 */
public class ProductionTuneDataReader
{
  private static ProductionTuneDataReader _instance;
  private String _defaultLocation = null;
  private List<ProductionTuneData> _productionTuneData = new ArrayList<ProductionTuneData>();
  private String _fileName = FileName.getProductionTuneConfigFullPath();
  Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");


  /**
   * @author Andy Mechtenberg
   */
  public static synchronized ProductionTuneDataReader getInstance()
  {
    if (_instance == null)
      _instance = new ProductionTuneDataReader();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private ProductionTuneDataReader()
  {
    // do nothing
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   * @author Andy Mechtenberg
   */
  public void setConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }

  /**
   * Parse in the files contents for later use.
   * @author Andy Mechtenberg
   */
  private void readProductionTuneFile() throws DatastoreException
  {
    _productionTuneData.clear();

    FileReader fr = null;
    try
    {
      fr = new FileReader(_fileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new FileNotFoundDatastoreException(_fileName);
      ex.initCause(fnfe);
      throw ex;
    }

    LineNumberReader is = new LineNumberReader(fr);
    try
    {
      String line = null;
      int i = -1;
      _defaultLocation = "";
      String projectName = "";
      String imageDestination = "";
      String resultsDestination = "";
      int currentCount = 0;
      int reEnableThreshold = 0;
      int reEnableCount = 0;
      String notifyEmailAddress = "";

      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(_fileName);
          ex.initCause(ioe);
          throw ex;
        }

        if (line == null)
          continue;

        // if the line is empty or a comment continue to the next line
        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
          continue;

        line = line.replaceAll("\\\\#", "#");

        if (i == -1)
        {
          _defaultLocation = line;
          if (_defaultLocation.equals("<blank>"))
            _defaultLocation = "";
          i++;
          continue;
        }

        if (i == 0)
        {
          projectName = "";
          imageDestination = "";
          resultsDestination = "";
          currentCount = 0;
          reEnableThreshold = 0;
          reEnableCount = 0;
          notifyEmailAddress = "";
        }

        ++i;
        if (i == 1)
        {
          projectName = line;
          if (projectName.equals("<blank>"))
            projectName = "";
        }
        else if (i == 2)
        {
          imageDestination = line;
        }
        else if (i == 3)
        {
          resultsDestination = line;
        }
        else if (i ==4)
        {
          currentCount = Integer.parseInt(line);
        }
        else if (i ==5)
        {
          reEnableThreshold = Integer.parseInt(line);
        }
        else if (i ==6)
        {
          reEnableCount = Integer.parseInt(line);
        }
        else if (i ==7)
        {
          notifyEmailAddress = line;
          if (notifyEmailAddress.equals("<blank>"))
            notifyEmailAddress = "";
          i = 0;
        }
        else
          Assert.expect(false);

        if (i > 0)
          continue;

        ProductionTuneData data = new ProductionTuneData(projectName,
                                                         imageDestination,
                                                         resultsDestination,
                                                         currentCount,
                                                         reEnableThreshold,
                                                         reEnableCount,
                                                         notifyEmailAddress);
        _productionTuneData.add(data);
      }
      while (line != null);
    }
    catch (Exception ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, is.getLineNumber());
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
  }

  /**
   * @return a Collection of all production tune data entries
   *
   * @author Andy Mechtenberg
   */
  public String getDefaultLocation() throws DatastoreException
  {
    readProductionTuneFile();

    return _defaultLocation;
  }

  /**
   * @return a Collection of all production tune data entries
   *
   * @author Andy Mechtenberg
   */
  public Collection<ProductionTuneData> getProductionTuneData() throws DatastoreException
  {
    readProductionTuneFile();

    return _productionTuneData;
  }
}
