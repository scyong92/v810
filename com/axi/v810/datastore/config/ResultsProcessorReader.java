package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;


/**
 * This file format defines the rules used by the results processor. Each Rule contains Conditions that define
 * when the rest of the rule is used. The rest of the rule defines which file is copied to which location.
 *
 * A Condition defines a condition key and an expression to match. If multiple conditions with the same key exist, the
 * condition is matched if ANY of the conditions are met. For simple expressions, a comma (,) can be used to the same effect.
 *
 * Comment lines begin with a '#'.
 *
 * This defines version 1.0
 * A reader of version x.y can read all versions x.z where z<=y.
 *
 * {Header}
 * Version = <number>
 * RepairToolRootDirectory = <directory>
 * RepairToolEnabled = (true|false)
 *
 * {Rule}
 * Rule <name>
 * Enabled = (true|false)
 * [Condition <keyEnumName> = [regex] <expression>]*
 * [Copy <sourceEnumName> to <directory path>]*
 *
 * @author Patrick Lacz
 */
public class ResultsProcessorReader
{
  // These keys should match the keys defined in the Writer. They have been separated to allow versioning with fewer dependencies.
  private static final String _VERSION_ID = "Version";

  private static final String _REPAIR_TOOL_DIRECTORY = "RepairToolRootDirectory";
  private static final String _REPAIR_TOOL_ENABLED = "RepairToolEnabled";

  private static final String _IS_ENABLED = "Enabled";

  private Map<ConditionKeyEnum, Pair<Boolean, List<String>>> _conditionToRegExFlagAndPatternListPairMap = null;
  private Map<ResultFileTypeEnum, List<String>> _resultFileTypeToDestinationListMap = null;
  private boolean _isRuleEnabled = false;
  private String _ruleName = "";

  private static ResultsProcessorReader _instance  = null;

  /**
   * @author Patrick Lacz
   */
  private ResultsProcessorReader()
  {
    // do nothing
  }

  public static synchronized ResultsProcessorReader getInstance()
  {
    if (_instance == null)
      _instance = new ResultsProcessorReader();
    return _instance;
  }

  /**
   * @author Patrick Lacz
   */
  public void readResultsProcessorConfigFile() throws DatastoreException
  {
    ResultsProcessor resultsProcessor = ResultsProcessor.getInstance();

    String fileName = FileName.getResultsProcessingConfigFullPath();

    FileReader fileReader = null;
    try
    {
      fileReader = new FileReader(fileName);
    }
    catch (FileNotFoundException fnfe)
    {
      // just start with the defaults.
      return;
    }

    LineNumberReader lineNumberReader = new LineNumberReader(fileReader);

    String sol = "^\\s*"; // start of line (with white space)
    String eol = "\\s*$"; // end of line (with white space)
    String ws = "\\s*"; // white space
    String mws = "\\s+"; // mandatory white space (required)

    // # is a comment
    Pattern commentPattern = Pattern.compile(sol + "(?:#" + ws + "(.*)?)?" + eol);
    Pattern keyPattern = Pattern.compile(sol + "(\\w+)" + ws + "=" + ws + "(.+)" + eol);
    Pattern startRulePattern = Pattern.compile(sol + "rule" + mws + "(.+)" + eol, Pattern.CASE_INSENSITIVE);
    Pattern conditionPattern = Pattern.compile(sol + "condition" + mws + "(\\w+)" + mws + "(regex|simple)" + mws + "(.+)" + eol, Pattern.CASE_INSENSITIVE);
    Pattern copyTargetPattern = Pattern.compile(sol + "copy" + mws + "(\\w+)" + mws + "(.+)" + eol, Pattern.CASE_INSENSITIVE);

    _ruleName = "";
    _conditionToRegExFlagAndPatternListPairMap = new HashMap<ConditionKeyEnum,Pair<Boolean,List<String>>>();
    _resultFileTypeToDestinationListMap = new HashMap<ResultFileTypeEnum, List<String>>();

    // don't try to save/load while the results processor is working.
    synchronized (resultsProcessor)
    {
      resultsProcessor.clear();
      try
      {
        String line = null;
        do
        {
          try
          {
            line = lineNumberReader.readLine();
          }
          catch (IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(fileName);
            ex.initCause(ioe);
            throw ex;
          }

          // if the line is null, we have reached the end of the file.
          if (line == null)
            break; // this is actually the only way this loop ends without throwing.

          Matcher commentMatcher = commentPattern.matcher(line);
          if (commentMatcher.matches())
            continue;

          Matcher keyMatcher = keyPattern.matcher(line);
          if (keyMatcher.matches())
          {
            parseKey(keyMatcher, resultsProcessor, lineNumberReader.getLineNumber(), fileName);
            continue;
          }

          Matcher startRuleMatcher = startRulePattern.matcher(line);
          if (startRuleMatcher.matches())
          {
            if (_ruleName.length() > 0)
              createRule(_ruleName, resultsProcessor);
            _ruleName = startRuleMatcher.group(1);
            continue;
          }

          Matcher conditionMatcher = conditionPattern.matcher(line);
          if (conditionMatcher.matches())
          {
            parseCondition(_ruleName, conditionMatcher, lineNumberReader.getLineNumber(), fileName);
            continue;
          }

          Matcher copyTargetMatcher = copyTargetPattern.matcher(line);
          if (copyTargetMatcher.matches())
          {
            parseTarget(copyTargetMatcher, lineNumberReader.getLineNumber(), fileName);
            continue;
          }

          // Syntax Error
          throw new ResultsProcessorConfigErrorDatastoreException(_ruleName, fileName, lineNumberReader.getLineNumber(), line);
        }
        while (line != null);

        if (_ruleName.length() > 0)
          createRule(_ruleName, resultsProcessor);
      }
      finally
      {
        // close the file
        if (lineNumberReader != null)
        {
          try
          {
            lineNumberReader.close();
          }
          catch (IOException ioe)
          {
            ioe.printStackTrace();
          }
        }
        _conditionToRegExFlagAndPatternListPairMap = null;
        _resultFileTypeToDestinationListMap = null;
      }
    }
  }

  /**
   * The line number and file are passed in for creating error messages.
   * @author Patrick Lacz
   */
  private void parseCondition(String ruleName, Matcher conditionMatcher, int lineNumber, String fileName) throws DatastoreException
  {
    Assert.expect(ruleName != null);
    Assert.expect(conditionMatcher != null);
    Assert.expect(fileName != null);
    Assert.expect(_conditionToRegExFlagAndPatternListPairMap != null);

    Assert.expect(conditionMatcher.groupCount() == 3);
    if (ruleName.length() == 0)
      // throw something?
      return;

    // could check here to see if the number of targets > 0.. but seems like an unneccesary requirement.

    String conditionKey = conditionMatcher.group(1);
    String isRegEx = conditionMatcher.group(2);
    String pattern = conditionMatcher.group(3);

    ConditionKeyEnum conditionKeyEnum = null;
    boolean isRegularExpression = isRegEx != null && isRegEx.equalsIgnoreCase("regex");

    conditionKeyEnum = EnumStringLookup.getInstance().getConditionKeyEnum(conditionKey);

    // append the expression to the list of expressions for that value.
    // this allows for multiple regular expressions and can be used for multiple simple expressions (although use of commas is preferred)
    Pair<Boolean,List<String>> regExFlagAndPatternListForConditionKey = _conditionToRegExFlagAndPatternListPairMap.get(conditionKeyEnum);
    if (regExFlagAndPatternListForConditionKey == null)
    {
      // create the list if it doesn't already exist.
      // intiialize the 'defined-using-regular-expressions' flag with the current setting.
      regExFlagAndPatternListForConditionKey = new Pair<Boolean,List<String>>(isRegularExpression, new ArrayList<String>());
      _conditionToRegExFlagAndPatternListPairMap.put(conditionKeyEnum, regExFlagAndPatternListForConditionKey);
    }
    Assert.expect(regExFlagAndPatternListForConditionKey.getFirst() == isRegularExpression);
    regExFlagAndPatternListForConditionKey.getSecond().add(pattern);
  }

  /**
   *
   * The line number and file are passed in for creating error messages.
   * @author Patrick Lacz
   */
  private void parseTarget(Matcher copyTargetMatcher, int lineNumber, String fileName) throws DatastoreException
  {
    Assert.expect(copyTargetMatcher != null);
    Assert.expect(fileName != null);
    Assert.expect(_resultFileTypeToDestinationListMap != null);
    Assert.expect(copyTargetMatcher.groupCount() == 2);

    String resultFileString = copyTargetMatcher.group(1);
    String destinationPath = copyTargetMatcher.group(2);

    ResultFileTypeEnum resultFileTypeEnum = EnumStringLookup.getInstance().getResultFileTypeEnum(resultFileString);

    List<String> destinationPathList = _resultFileTypeToDestinationListMap.get(resultFileTypeEnum);
    if (destinationPathList == null)
    {
      destinationPathList = new ArrayList<String>();
      _resultFileTypeToDestinationListMap.put(resultFileTypeEnum, destinationPathList);
    }
    destinationPathList.add(destinationPath);
  }

  /**
   * @author Patrick Lacz
   */
  private void parseKey(Matcher keyMatcher, ResultsProcessor resultsProcessor, int lineNumber, String fileName) throws DatastoreException
  {
    Assert.expect(keyMatcher != null);
    Assert.expect(resultsProcessor != null);
    Assert.expect(fileName != null);

    Assert.expect(keyMatcher.groupCount() == 2);
    String key = keyMatcher.group(1);
    String value = keyMatcher.group(2);

    if (key.equalsIgnoreCase(_VERSION_ID))
    {
      // only version 1.0 is supported by this reader
    }
    else if (key.equalsIgnoreCase(_IS_ENABLED))
    {
      _isRuleEnabled = Boolean.parseBoolean(value);
    }
    else if (key.equalsIgnoreCase(_REPAIR_TOOL_ENABLED))
    {
      resultsProcessor.setIsRepairToolRuleEnabled(Boolean.parseBoolean(value));
    }
    else if (key.equalsIgnoreCase(_REPAIR_TOOL_DIRECTORY))
    {
      String ITFPath = value;
      resultsProcessor.setRepairToolDirectory(ITFPath);
    }
    else
    {
      throw new ResultsProcessorConfigErrorDatastoreException(_ruleName, fileName, lineNumber, value);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private void createRule(String ruleName, ResultsProcessor resultsProcessor) throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(ruleName != null);
    Assert.expect(_conditionToRegExFlagAndPatternListPairMap != null);
    Assert.expect(_resultFileTypeToDestinationListMap != null);

    Rule newRule = new Rule();

    newRule.setName(ruleName);

    // populate the conditions
    for (Map.Entry<ConditionKeyEnum, Pair<Boolean, List<String>>> conditionEntry : _conditionToRegExFlagAndPatternListPairMap.entrySet())
    {
      ConditionKeyEnum conditionKeyEnum = conditionEntry.getKey();
      boolean isRegularExpression = conditionEntry.getValue().getFirst();
      List<String> patternList = conditionEntry.getValue().getSecond();

      Condition newCondition = null;
      if (isRegularExpression)
        newCondition = ResultsProcessor.createRegularExpressionCondition(conditionKeyEnum, patternList);
      else
        newCondition = ResultsProcessor.createSimpleExpressionCondition(conditionKeyEnum, patternList);

      newRule.addCondition(newCondition);
    }

    // populate the target list.
    // a target is a result file rule and the directories that it copies to.
    for (Map.Entry<ResultFileTypeEnum, List<String>> targetEntry  : _resultFileTypeToDestinationListMap.entrySet())
    {
      ResultFileTypeEnum resultFileTypeEnum = targetEntry.getKey();
      List<String> destinationPathList = targetEntry.getValue();

      Target newTarget = ResultsProcessor.createTarget(resultFileTypeEnum, destinationPathList);
      newRule.addTarget(newTarget);
    }

    if (_resultFileTypeToDestinationListMap.isEmpty())
    {
      // if the rule doesn't do anything, disable it
      newRule.setIsEnabled(false);
    }
    else
      newRule.setIsEnabled(_isRuleEnabled);

    _conditionToRegExFlagAndPatternListPairMap.clear();
    _resultFileTypeToDestinationListMap.clear();

    resultsProcessor.appendRule(newRule);
    return;
  }
}
