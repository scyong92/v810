package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * This enumeration contains all the hardware configuration settings for the test system.
 * ONLY hardware configuration information belongs here, NO calibration or non-hardware settings!
 *
 * To add a configuration value, create a new static final instance and pass this
 * information to the constructor:
 * <pre>
 *    name of key found in the configuration file
 *    name of the file that contains the key/value pair
 *    type of the configuration value
 *    array which holds the valid range of values (can be null)
 *    default value if no value is found (can be null)
 *    description key to use for localization
 *    type of machine the key would be found on
 * </pre>
 *
 * @author Bill Darbie
 * @author George Booth (added support for HTube, the Hamamatsu L9181-18 X-ray source)
 */
public class HardwareConfigEnum extends ConfigEnum implements Serializable
{
  // the config file names
  private static final String _HARDWARE_CONFIG = FileName.getHardwareConfigFullPath();
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG = FileName.getKollmorgenCdMotionDriveXaxisConfigFullPath();
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG = FileName.getKollmorgenCdMotionDriveYaxisConfigFullPath();
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG = FileName.getKollmorgenCdMotionDriveRailWidthAxisConfigFullPath();

  private static int _index = -1;

  // Note: this value should never be changed because it is read and written out to disk.
  private static final String _XMP_SYNQNET_PCI_MOTION_CONTROLLER_MODEL_NAME = "xmpSynqNetPci";
  // The real model name is "agilentTdi" but since this name is displayed to the user and we are
  // protecting our intellectual property information, we have to just call it "agilentType1"
  private static final String _AXI_TDI_XRAY_CAMERA_MODEL_NAME = "axiType1";
  private static final String _AXI_OPTICAL_CAMERA_MODEL_NAME = "axiOpticalCameraType1";
  private static final String _AXI_OPTICAL_CAMERA_MODEL_NAME_TWO = "axiOpticalCameraType2";
  private static final Integer _MOTION_CONTROLLER_ID_0 = new Integer(0);

  private static List<ConfigEnum> _hardwareConfigEnums = new ArrayList<ConfigEnum>();

  /**
   * @param id int value of the enumeration.
   * @param key String "key" name for the configuration in the file.
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @param descriptionKey String key to use to lookup the description in the localization file.
   * @author Bill Darbie
   */
  private HardwareConfigEnum(int id,
                             String key,
                             String filename,
                             TypeEnum type,
                             Object[] valid)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _hardwareConfigEnums.add(this);
  }

  // all the available keys from machine.cfg
  public static final HardwareConfigEnum XRAYTESTER_SERIAL_NUMBER =
      new HardwareConfigEnum(++_index,
                             "xrayTesterSerialNumber",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum SYSTEM_TYPE =
      new HardwareConfigEnum(++_index,
                             "xrayTesterSystemType",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             getSystemTypeValidValues());

  public static final HardwareConfigEnum WINDOWS_DRIVER_SYS_FILE_DIR =
    new HardwareConfigEnum(++_index,
                           "windowsDriverSysFileDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum WINDOWS_DRIVER_INF_FILE_DIR =
    new HardwareConfigEnum(++_index,
                           "windowsDriverInfFileDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum PANEL_POSITIONER_MOTION_CONTROLLER_ID =
    new HardwareConfigEnum(++_index,
                           "panelPositionerMotionControllerId",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(getMotionControllerId0(), getMotionControllerId0()));

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_CLEAR_SENSOR_ACTIVE_LOW_TYPE_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelClearSensorActiveLowTypeInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_CLEAR_SENSOR_RECESSED_MOUNT_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelClearSensorRecessedMountInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum PANEL_HANDLER_MOTION_CONTROLLER_ID =
    new HardwareConfigEnum(++_index,
                           "panelHandlerMotionControllerId",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(getMotionControllerId0(), getMotionControllerId0()));

  public static final HardwareConfigEnum PANEL_HANDLER_INNER_BARRIER_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "panelHandlerInnerBarrierInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum PANEL_HANDLER_LEFT_OUTER_BARRIER_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "panelHandlerLeftOuterBarrierInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum PANEL_HANDLER_RIGH_OUTER_BARRIER_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "panelHandlerRightOuterBarrierInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum PANEL_HANDLER_MAXIMUM_PANEL_WIDTH_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelMaximumWidthInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_MINIMUM_PANEL_WIDTH_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelMinimumWidthInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_WIDTH_CLEARANCE_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelWidthClearanceInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_MAXIMUM_PANEL_LENGTH_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerMaximumPanelLengthInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_MINIMUM_PANEL_LENGTH_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerMinimumPanelLengthInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_MAXIMUM_PANEL_THICKNESS_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelMaximumThicknessInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_MINIMUM_PANEL_THICKNESS_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelMinimumThicknessInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelPositionerXalignmentWithLeftOuterBarrierInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelPositionerYalignmentWithLeftOuterBarrierInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelPositionerXalignmentWithRightOuterBarrierInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "panelHandlerPanelPositionerYalignmentWithRightOuterBarrierInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum DIGITAL_IO_CONNECTION_TYPE =
    new HardwareConfigEnum(++_index,
                           "digitalIoConnectionType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                             new Object[] {"synqNet"});

  public static final HardwareConfigEnum DIGITAL_IO_CONNECTION_ID =
    new HardwareConfigEnum(++_index,
                           "digitalIoConnectionId",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0, 0));

  public static final HardwareConfigEnum REMOTE_MOTION_CONTROLLER_IP_ADDRESS =
    new HardwareConfigEnum(++_index,
                           "remoteMotionControllerIpAddress",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RMI_TIMEOUT_IN_SECONDS =
    new HardwareConfigEnum(++_index,
                           "RMITimeOutInSeconds",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum DIGITAL_IO_LOCATION_TYPE =
    new HardwareConfigEnum(++_index,
                           "digitalIoLocationType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                             new Object[] {"Node"});

  public static final HardwareConfigEnum DIGITAL_IO_LOCATION_ID =
    new HardwareConfigEnum(++_index,
                           "digitalIoLocationId",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum NUMBER_OF_MOTION_CONTROLLERS_INSTALLED =
    new HardwareConfigEnum(++_index,
                           "numberOfMotionControllersInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROL_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "motionControlHomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "motionController0HomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_DRIVE_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "motionDriveHomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_MOTION_DRIVE_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "synqNetMotionDriveHomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_DIGITAL_IO_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "synqNetDigitalIoHomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_MODEL =
    new HardwareConfigEnum(++_index,
                           "motionController0Model",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {getXmpSynqPciMotionControllerModelName()});

  public static final HardwareConfigEnum XMP_SYNQNET_PCI =
    new HardwareConfigEnum(++_index,
                           "xmpSynqNetPci",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"mei/xmpSynqNetPci"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"synqNetMotionDrive", "synqNetIo"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"synqNetMotionDrive", "synqNetIo"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"synqNetMotionDrive", "synqNetIo"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_MODEL =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0Model",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"kollmorgen_cd", "meiSliceIo_TSIO_1001"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_MODEL =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1Model",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"kollmorgen_cd", "meiSliceIo_TSIO_1001"});

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_MODEL =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2Model",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"kollmorgen_cd", "meiSliceIo_TSIO_1001"});

  public static final HardwareConfigEnum KOLLMORGEN_CD_MODEL_OPTION =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cdModelOption",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001_MODEL_OPTION =
    new HardwareConfigEnum(++_index,
                           "meiSliceIoModelOption",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_MOTOR_0_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor0Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"servo", "stepper"});

  public static final HardwareConfigEnum SYNQNET_0_MOTOR_1_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor1Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"servo", "stepper"});

  public static final HardwareConfigEnum SYNQNET_0_MOTOR_2_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor2Type",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"servo", "stepper"});

  public static final HardwareConfigEnum SYNQNET_0_X_AXIS_SETTINGS =
    new HardwareConfigEnum(++_index,
                           "synqNet0xAxisSettings",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_Y_AXIS_SETTINGS =
    new HardwareConfigEnum(++_index,
                           "synqNet0yAxisSettings",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_RAIL_WIDTH_AXIS_SETTINGS =
    new HardwareConfigEnum(++_index,
                           "synqNet0railWidthAxisSettings",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "motionController0VendorFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "motionController0NetworkFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_CONFIGURATION_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "motionController0ConfigurationFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MOTION_CONTROLLER_0_DRIVER_FILES =
    new HardwareConfigEnum(++_index,
                           "motionController0DriverFiles",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cd",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001 =
    new HardwareConfigEnum(++_index,
                           "meiSliceIo_TSIO_1001",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_VENDOR_FIRMWARE_FILE =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cdVendorFirmwareFile",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

    public static final HardwareConfigEnum KOLLMORGEN_CD_NETWORK_FIRMWARE_FILE =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cdNetworkFirmwareFile",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_NETWORK_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cdNetworkFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_VENDOR_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "kollmorgen_cdVendorFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_FILE =
    new HardwareConfigEnum(++_index,
                           "meiSliceIo_TSIO_1001VendorFirmwareFile",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_FILE =
    new HardwareConfigEnum(++_index,
                           "meiSliceIo_TSIO_1001NetworkFirmwareFile",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "meiSliceIo_TSIO_1001VendorFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "meiSliceIo_TSIO_1001NetworkFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // SynqNet node 0 settings for motion controller 0
  public static final HardwareConfigEnum SYNQNET_0_NODE_0_VENDOR_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0VendorFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_NETWORK_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0NetworkFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_CONFIGURATION_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0ConfigurationFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0VendorFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node0NetworkFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // SynqNet node 1 settings for motion controller 0
  public static final HardwareConfigEnum SYNQNET_0_NODE_1_VENDOR_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1VendorFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_NETWORK_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1NetworkFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_CONFIGURATION_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1ConfigurationFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1VendorFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_1_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node1NetworkFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // motion drive 2 settings
  public static final HardwareConfigEnum SYNQNET_0_NODE_2_VENDOR_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2VendorFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_NETWORK_FIRMWARE_VERSION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2NetworkFirmwareVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_CONFIGURATION_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2ConfigurationFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2VendorFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum SYNQNET_0_NODE_2_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH =
    new HardwareConfigEnum(++_index,
                           "synqNet0Node2NetworkFirmwareFileNameFullPath",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // all available keys from the motion drive "kollmorgen_cdRailWidthAxis.config"
  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPITCH =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mpitch",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MOTOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_motortype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MIPEAK =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mipeak",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MICONT =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_micont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MSPEED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mspeed",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MKT =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mkt",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCRES =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mencres",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCTYPE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_menctype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCOFF =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mencoff",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLMIN =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mlmin",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPHASE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mphase",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPOLES =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mpoles",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MBEMFCOMP =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mbemfcomp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINC =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mlgainc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINP =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mlgainp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLC =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mtanglc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLP =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mtanglp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLF =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mvanglf",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLH =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mvanglh",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVA =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mhinva",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVB =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mhinvb",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVC =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mhinvc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VBUS =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_vbus",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ILIM =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_ilim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ICONT =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_icont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VLIM =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_vlim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MFBDIR =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_mfbdir",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF1 =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_anoff1",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF2 =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_anoff2",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_INITGAIN =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_initgain",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IENCSTART =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_iencstart",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVMODE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_uvmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVTIME =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_uvtime",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVRECOVER =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_uvrecover",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMMODE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_thermmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMTYPE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_thermtype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IZERO =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_izero",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "railWidthAxis_maximumMotorVelocityLimitInNanometersPerSecond",
                           _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED =
      new HardwareConfigEnum(++_index,
                             "railWidthAxis_maximumMotorAccelerationLimitInNanometersPerSecondSquared",
                             _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG,
                             TypeEnum.STRING,
                             null);

  // all available keys from the motion drive "kollmorgen_cdXaxis.config"
  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MPITCH =
    new HardwareConfigEnum(++_index,
                           "xAxis_mpitch",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MOTOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "xAxis_motortype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MIPEAK =
    new HardwareConfigEnum(++_index,
                           "xAxis_mipeak",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MICONT =
    new HardwareConfigEnum(++_index,
                           "xAxis_micont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MSPEED =
    new HardwareConfigEnum(++_index,
                           "xAxis_mspeed",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MKT =
    new HardwareConfigEnum(++_index,
                           "xAxis_mkt",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MENCRES =
    new HardwareConfigEnum(++_index,
                           "xAxis_mencres",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MENCTYPE =
    new HardwareConfigEnum(++_index,
                           "xAxis_menctype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MENCOFF =
    new HardwareConfigEnum(++_index,
                           "xAxis_mencoff",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MLMIN =
    new HardwareConfigEnum(++_index,
                           "xAxis_mlmin",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MPHASE =
    new HardwareConfigEnum(++_index,
                           "xAxis_mphase",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MPOLES =
    new HardwareConfigEnum(++_index,
                           "xAxis_mpoles",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MBEMFCOMP =
    new HardwareConfigEnum(++_index,
                           "xAxis_mbemfcomp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MLGAINC =
    new HardwareConfigEnum(++_index,
                           "xAxis_mlgainc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MLGAINP =
    new HardwareConfigEnum(++_index,
                           "xAxis_mlgainp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MTANGLC =
    new HardwareConfigEnum(++_index,
                           "xAxis_mtanglc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MTANGLP =
    new HardwareConfigEnum(++_index,
                           "xAxis_mtanglp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MVANGLF =
    new HardwareConfigEnum(++_index,
                           "xAxis_mvanglf",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MVANGLH =
    new HardwareConfigEnum(++_index,
                           "xAxis_mvanglh",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MHINVA =
    new HardwareConfigEnum(++_index,
                           "xAxis_mhinva",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MHINVB =
    new HardwareConfigEnum(++_index,
                           "xAxis_mhinvb",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MHINVC =
    new HardwareConfigEnum(++_index,
                           "xAxis_mhinvc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_VBUS =
    new HardwareConfigEnum(++_index,
                           "xAxis_vbus",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_ILIM =
    new HardwareConfigEnum(++_index,
                           "xAxis_ilim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_ICONT =
    new HardwareConfigEnum(++_index,
                           "xAxis_icont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_VLIM =
    new HardwareConfigEnum(++_index,
                           "xAxis_vlim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MFBDIR =
    new HardwareConfigEnum(++_index,
                           "xAxis_mfbdir",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_ANOFF1 =
    new HardwareConfigEnum(++_index,
                           "xAxis_anoff1",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_ANOFF2 =
    new HardwareConfigEnum(++_index,
                           "xAxis_anoff2",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_INITGAIN =
    new HardwareConfigEnum(++_index,
                           "xAxis_initgain",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_IENCSTART =
    new HardwareConfigEnum(++_index,
                           "xAxis_iencstart",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_UVMODE =
    new HardwareConfigEnum(++_index,
                           "xAxis_uvmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_UVTIME =
    new HardwareConfigEnum(++_index,
                           "xAxis_uvtime",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_UVRECOVER =
    new HardwareConfigEnum(++_index,
                           "xAxis_uvrecover",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_THERMMODE =
    new HardwareConfigEnum(++_index,
                           "xAxis_thermmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_THERMTYPE =
    new HardwareConfigEnum(++_index,
                           "xAxis_thermtype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_IZERO =
    new HardwareConfigEnum(++_index,
                           "xAxis_izero",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxis_maximumMotorVelocityLimitInNanometersPerSecond",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxis_maximumMotorAccelerationLimitInNanometersPerSecondSquared",
                           _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  // all available keys from the motion drive "kollmorgen_cdYaxis.config"
  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MPITCH =
    new HardwareConfigEnum(++_index,
                           "yAxis_mpitch",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MOTOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "yAxis_motortype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MIPEAK =
    new HardwareConfigEnum(++_index,
                           "yAxis_mipeak",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MICONT =
    new HardwareConfigEnum(++_index,
                           "yAxis_micont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MSPEED =
    new HardwareConfigEnum(++_index,
                           "yAxis_mspeed",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MKT =
    new HardwareConfigEnum(++_index,
                           "yAxis_mkt",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MENCRES =
    new HardwareConfigEnum(++_index,
                           "yAxis_mencres",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MENCTYPE =
    new HardwareConfigEnum(++_index,
                           "yAxis_menctype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MENCOFF =
    new HardwareConfigEnum(++_index,
                           "yAxis_mencoff",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MLMIN =
    new HardwareConfigEnum(++_index,
                           "yAxis_mlmin",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MPHASE =
    new HardwareConfigEnum(++_index,
                           "yAxis_mphase",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MPOLES =
    new HardwareConfigEnum(++_index,
                           "yAxis_mpoles",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MBEMFCOMP =
    new HardwareConfigEnum(++_index,
                           "yAxis_mbemfcomp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MLGAINC =
    new HardwareConfigEnum(++_index,
                           "yAxis_mlgainc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MLGAINP =
    new HardwareConfigEnum(++_index,
                           "yAxis_mlgainp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MTANGLC =
    new HardwareConfigEnum(++_index,
                           "yAxis_mtanglc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MTANGLP =
    new HardwareConfigEnum(++_index,
                           "yAxis_mtanglp",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MVANGLF =
    new HardwareConfigEnum(++_index,
                           "yAxis_mvanglf",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MVANGLH =
    new HardwareConfigEnum(++_index,
                           "yAxis_mvanglh",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MHINVA =
  new HardwareConfigEnum(++_index,
                         "yAxis_mhinva",
                         _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                         TypeEnum.STRING,
                         null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MHINVB =
    new HardwareConfigEnum(++_index,
                           "yAxis_mhinvb",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MHINVC =
    new HardwareConfigEnum(++_index,
                           "yAxis_mhinvc",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_VBUS =
    new HardwareConfigEnum(++_index,
                           "yAxis_vbus",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_ILIM =
    new HardwareConfigEnum(++_index,
                           "yAxis_ilim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_ICONT =
    new HardwareConfigEnum(++_index,
                           "yAxis_icont",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_VLIM =
    new HardwareConfigEnum(++_index,
                           "yAxis_vlim",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MFBDIR =
    new HardwareConfigEnum(++_index,
                           "yAxis_mfbdir",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_ANOFF1 =
    new HardwareConfigEnum(++_index,
                           "yAxis_anoff1",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_ANOFF2 =
    new HardwareConfigEnum(++_index,
                           "yAxis_anoff2",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_INITGAIN =
    new HardwareConfigEnum(++_index,
                           "yAxis_initgain",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_IENCSTART =
    new HardwareConfigEnum(++_index,
                           "yAxis_iencstart",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_UVMODE =
    new HardwareConfigEnum(++_index,
                           "yAxis_uvmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_UVTIME =
    new HardwareConfigEnum(++_index,
                           "yAxis_uvtime",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_UVRECOVER =
    new HardwareConfigEnum(++_index,
                           "yAxis_uvrecover",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_THERMMODE =
    new HardwareConfigEnum(++_index,
                           "yAxis_thermmode",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_THERMTYPE =
    new HardwareConfigEnum(++_index,
                           "yAxis_thermtype",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_IZERO =
    new HardwareConfigEnum(++_index,
                           "yAxis_izero",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxis_maximumMotorVelocityLimitInNanometersPerSecond",
                           _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
                           TypeEnum.STRING,
                           null);
// George A. David moved to SystemConfigEnum
//  public static final HardwareConfigEnum KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED =
//      new HardwareConfigEnum(++_index,
//                             "yAxis_maximumMotorAccelerationLimitInNanometersPerSecondSquared",
//                             _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG,
//                             TypeEnum.STRING,
//                             null);

  // X-axis motion profile 0 parameters
  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile0AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile0DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile0VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile0ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis motion profile 0 parameters
  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile0AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile0DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile0VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile0ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // RailWidth-axis motion profile 0 parameters
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile0AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile0DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile0VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile0ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis motion profile 1 parameters
  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile1AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile1DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile1VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile1ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis motion profile 1 parameters
  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile1AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile1DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile1VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile1ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // RailWidth-axis motion profile 1 parameters
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile1AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile1DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile1VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile1ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis motion profile 2 parameters
  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile2AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile2DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile2VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile2ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis motion profile 2 parameters
  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile2AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile2DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile2VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile2ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // RailWidth-axis motion profile 2 parameters
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile2AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile2DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile2VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile2ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis motion profile 3 parameters
  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile3AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile3DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile3VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "xAxisPointToPointMotionProfile3ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis motion profile 3 parameters
  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile3AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile3DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile3VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "yAxisPointToPointMotionProfile3ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // RailWidth-axis motion profile 3 parameters
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile3AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile3DecelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile3VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisPointToPointMotionProfile3ScurveSmoothingPercentage",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // SynqNet 0 motor 0 encoder feedback type
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_0_ENCODER_FEEDBACK_TYPE =
      new HardwareConfigEnum(++_index,
                             "synqNet0Motor0EncoderFeedbackType",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[] {"primary", "secondary", "none"});

  // SynqNet 0 motor 1 encoder feedback type
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_1_ENCODER_FEEDBACK_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor1EncoderFeedbackType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"primary", "secondary", "none"});

  // SynqNet 0 motor 2 encoder feedback type
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_2_ENCODER_FEEDBACK_TYPE =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor2EncoderFeedbackType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"primary", "secondary", "none"});

  // SynqNet motor 0 counts per revolution
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_0_COUNTS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor0CountsPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // SynqNet motor 1 counts per revolution
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_1_COUNTS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor1CountsPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // SynqNet motor 2 counts per revolution
  public static final HardwareConfigEnum SYNQNET_0_MOTOR_2_COUNTS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "synqNet0Motor2CountsPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis home offset
  public static final HardwareConfigEnum X_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "xAxisOffsetFromHomeSensorInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // X-axis offset from home sensor to the hard stop
  public static final HardwareConfigEnum X_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "xAxisOffsetFromHomeSensorToHardStopInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // X-axis home sensor type
  public static final HardwareConfigEnum X_AXIS_HOME_SENSOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "xAxisHomeSensorType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"indexPulse", "optical"});

  // Y-axis home sensor type
  public static final HardwareConfigEnum Y_AXIS_HOME_SENSOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "yAxisHomeSensorType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"indexPulse", "optical"});

  // Rail Width-axis home sensor type
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_HOME_SENSOR_TYPE =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisHomeSensorType",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"indexPulse", "optical"});

  // Y-axis home offset
  public static final HardwareConfigEnum Y_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "yAxisOffsetFromHomeSensorInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // X-axis offset from home sensor to the hard stop
  public static final HardwareConfigEnum Y_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "yAxisOffsetFromHomeSensorToHardStopInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // Rail Width-axis home offset
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisOffsetFromHomeSensorInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // Rail Width-axis offset from home sensor to the hard stop
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisOffsetFromHomeSensorToHardStopInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // X-axis ball screw travel
  public static final HardwareConfigEnum X_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "xAxisBallScrewTravelInNanometersPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis ball screw travel
  public static final HardwareConfigEnum Y_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "yAxisBallScrewTravelInNanometersPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Rail Width axis ball screw travel
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisBallScrewTravelInNanometersPerRevolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis minimum position limit
  public static final HardwareConfigEnum X_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "xAxisMinimumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis maximum position limit
  public static final HardwareConfigEnum X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "xAxisMaximumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis minimum position limit
  public static final HardwareConfigEnum Y_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "yAxisMinimumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis maximum position limit
  public static final HardwareConfigEnum Y_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "yAxisMaximumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Rail Width axis minimum position limit
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisMinimumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // Rail Width axis maximum position limit
  public static final HardwareConfigEnum RAIL_WIDTH_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "railWidthAxisMaximumPositionLimitInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // X-axis scan motion profile 0 parameters
  // For Standard system, xAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared = 6400000000
  // For Thunderbolt system, xAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared = 8960000000
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // For Standard system, xAxisScanMotionProfile0VelocityInNanometersPerSecond = 670000000
  // For Thunderbolt system, xAxisScanMotionProfile0VelocityInNanometersPerSecond = 670000000
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile0VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis scan motion profile 0 parameters
  // For Standard system, yAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared = 6400000000
  // For Thunderbolt system, yAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared = 9000000000
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile0AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // For Standard system, yAxisScanMotionProfile0VelocityInNanometersPerSecond = 333333500
  // For Thunderbolt system, yAxisScanMotionProfile0VelocityInNanometersPerSecond = 395833000
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile0VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis scan motion profile 1 parameters (High Magnification)
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile1AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  //(High Magnification)
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile1VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis scan motion profile 1 parameters (High Magnification)
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile1AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // (High Magnification)
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile1VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);  
  
  // X-axis scan motion profile 2 parameters (Low Magnification, M23)
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile2AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // X-axis scan motion profile 2 parameters (Low Magnification, M23)
  public static final HardwareConfigEnum X_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "xAxisScanMotionProfile2VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis scan motion profile 2 parameters (Low Magnification, M23)
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile2AccelerationInNanometersPerSecondSquared",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  // Y-axis scan motion profile 2 parameters (Low Magnification, M23)
  public static final HardwareConfigEnum Y_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND =
    new HardwareConfigEnum(++_index,
                           "yAxisScanMotionProfile2VelocityInNanometersPerSecond",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);
  
  // Swee Yee Wong - Insufficient trigger fix
  // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
  public static final HardwareConfigEnum PANEL_MEAN_POSITION_ERROR_IN_ENCODER_COUNTS =
    new HardwareConfigEnum(++_index,
                           "panelMeanPositionErrorInEncoderCounts",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum PANEL_SIGMA_POSITION_ERROR_IN_ENCODER_COUNTS =
    new HardwareConfigEnum(++_index,
                           "panelSigmaPositionErrorInEncoderCounts",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum PANEL_MULTIPLIER_POSITION_ERROR_IN_ENCODER_COUNTS =
    new HardwareConfigEnum(++_index,
                           "panelMultiplierPositionErrorInEncoderCounts",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum PANEL_MINIMUM_SILENT_PERIOD_IN_MS =
    new HardwareConfigEnum(++_index,
                           "panelMinimumSilentPeriodInMilliseconds",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum PANEL_SIGMA_SILENT_PERIOD_IN_MS =
    new HardwareConfigEnum(++_index,
                           "panelSigmaSilentPeriodInMilliseconds",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum PANEL_MULTIPLIER_SILENT_PERIOD_IN_MS =
    new HardwareConfigEnum(++_index,
                           "panelMultiplierSilentPeriodInMilliseconds",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           null);
  
  public static final HardwareConfigEnum ENABLE_MOTION_CONTROL_LOG =
    new HardwareConfigEnum(++_index,
                           "enableMotionControlLog",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           new Object[]{true, false});
  
  public static final HardwareConfigEnum ENABLE_MOTION_CONTROL_PVT_LOG =
    new HardwareConfigEnum(++_index,
                           "enableMotionControlPvtLog",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"true", "false"});

  public static final HardwareConfigEnum ENABLE_MOTION_CONTROL_TIMING_LOG =
    new HardwareConfigEnum(++_index,
                           "enableMotionControlTimingLog",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"true", "false"});

  public static final HardwareConfigEnum DIGITAL_IO_HOME_DIR =
    new HardwareConfigEnum(++_index,
                           "digitalIoHomeDir",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS =
    new HardwareConfigEnum(++_index,
                           "cameraControllerNumberOfRequiredSampleTriggers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  //Ngie Xing, add a different sample trigger for 23um
  public static final HardwareConfigEnum CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS_FOR_M23 =
    new HardwareConfigEnum(++_index,
                           "cameraControllerNumberOfRequiredSampleTriggersForM23",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  // locations of PIP sensors with respect to the system fiducial
  public static final HardwareConfigEnum LEFT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "leftPIPSensorXLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum LEFT_PIP_SENSOR_Y_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "leftPIPSensorYLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum RIGHT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "rightPIPSensorXLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum RIGHT_PIP_SENSOR_Y_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "rightPIPSensorYLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum OPTICAL_FIDUCIAL_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "opticalFiducialXLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum OPTICAL_FIDUCIAL_Y_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "opticalFiducialYLocationInSystemFiducialCoordinatesNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum INTERLOCK_LOGIC_BOARD_REVISION_ID =
    new HardwareConfigEnum(++_index,
                          "interLockLogicBoardRevisionId",
                          _HARDWARE_CONFIG,
                          TypeEnum.INTEGER,
                          null);
  public static final HardwareConfigEnum INTERLOCK_LOGIC_BOARD_WITH_KEYLOCK =
    new HardwareConfigEnum(++_index,
                           "interLockLogicBoardWithKeylock",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

//
// x-ray camera properties
//

  public static final HardwareConfigEnum XRAY_CAMERA_LOGGING_ENABLED =
    new HardwareConfigEnum(++_index,
                           "xRayCameraLoggingEnabled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);

  public static final HardwareConfigEnum XRAY_CAMERA_MODEL =
    new HardwareConfigEnum(++_index,
                           "xRayCameraModel",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {getAxiTdiXrayCameraModelName()});

  public static final HardwareConfigEnum AXI_TDI_CAMERA_AUTOMATIC_RUNTIME_FILE_UPDATE =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraAutomaticRuntimeFileUpdateEnabled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
  
   public static final HardwareConfigEnum AXI_TDI_CAMERA_BYPASS_BOARD_VERSION_IN_HARDWARE_CONFIG_UPDATE =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraBypassBoardVersionInHardwareConfigEnabled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum AXI_TDI_CAMERA_0_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera0BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_1_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera1BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_2_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera2BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_3_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera3BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_4_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera4BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_5_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera5BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_6_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera6BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_7_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera7BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_8_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera8BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_9_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera9BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_10_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera10BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_11_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera11BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_12_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera12BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_13_BOARD_VERSION =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera13BoardVersion",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);
  public static final HardwareConfigEnum AXI_TDI_CAMERA_USER_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraUserGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           range(0.0, 32.0));

  public static final HardwareConfigEnum AXI_TDI_CAMERA_USER_OFFSET =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraUserOffset",
                           _HARDWARE_CONFIG,
                           TypeEnum.DOUBLE,
                           range(-255.0, 255.0));

  public static final HardwareConfigEnum AXI_TDI_CAMERA_0_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera0PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_1_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera1PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_2_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera2PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_3_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera3PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_4_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera4PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_5_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera5PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_6_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera6PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_7_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera7PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_8_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera8PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_9_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera9PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_10_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera10PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_11_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera11PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_12_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera12PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_13_PREAMP_GAIN =
    new HardwareConfigEnum(++_index,
                           "axiType1Camera13PreampGain",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MIN =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraCalPointSegmentMin",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,255));

  public static final HardwareConfigEnum AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MAX =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraCalPointSegmentMax",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,255));

  public static final HardwareConfigEnum AXI_TDI_CAMERA_CAL_POINT_PIXEL_MIN =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraCalPointPixelMin",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,255));

  public static final HardwareConfigEnum AXI_TDI_CAMERA_CAL_POINT_PIXEL_MAX =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraCalPointPixelMax",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,255));
  
  // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  public static final HardwareConfigEnum AXI_TDI_CAMERA_BACKWARD_COMPATIBLE_MODE =
      new HardwareConfigEnum(++_index,
                             "axiType1CameraBackwardCompatibleMode",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);
  
  public static final HardwareConfigEnum AXI_TDI_CAMERA_BINNING_MODE =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraBinningMode",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(1,4));
  
  public static final HardwareConfigEnum AXI_TDI_CAMERA_PIXEL_RESOLUTION =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraPixelresolution",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum AXI_TDI_CAMERA_CONVOLUTION_FILTER =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraConvolutionFilter",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,1));
  
  public static final HardwareConfigEnum AXI_TDI_CAMERA_MEDIAN_FILTER =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraMedianFilter",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,1));
  
  public static final HardwareConfigEnum AXI_TDI_CAMERA_FLAT_FIELDING =
    new HardwareConfigEnum(++_index,
                           "axiType1CameraFlatFielding",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0,1));
  
  public static final HardwareConfigEnum XRAY_CAMERA_JUMBO_PACKET_SIZE =
    new HardwareConfigEnum(++_index,
                           "xRayCameraJumboPacketSize",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSORS =
    new HardwareConfigEnum(++_index,
                           "numberOfImageReconstructionProcessors",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           lowerBound(1));

  public static final HardwareConfigEnum NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES =
      new HardwareConfigEnum(++_index,
                             "numberOfImageReconstructionEngines",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             lowerBound(1));

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_RESERVED_PHYSICAL_MEMORY_IN_MEGABYTES =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionProcessorReservedPhysicalMemoryInMegabytes",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0, 4096));

public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_EXTRA_VIRTUAL_MEMORY_IN_MEGABYTES =
  new HardwareConfigEnum(++_index,
                         "imageReconstructionProcessorProcessExtraVirtualMemoryInMegabytes",
                         _HARDWARE_CONFIG,
                         TypeEnum.INTEGER,
                         range(0, 4096));

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_EXTRA_WORKING_SET_MEMORY_IN_MEGABYTES =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionProcessorProcessExtraWorkingSetMemoryInMegabytes",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0, 2048));

public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_MINIMUM_WORKING_SET_MEMORY_IN_MEGABYTES =
  new HardwareConfigEnum(++_index,
                         "imageReconstructionProcessorProcessMinimumWorkingSetMemoryInMegabytes",
                         _HARDWARE_CONFIG,
                         TypeEnum.INTEGER,
                         range(0, 1024));

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_PROJECTION_MEMORY_IN_MEGABYTES =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionProcessorAllocatedProjectionMemoryInMegabytes",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0, 262144));

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_RECONSTRUCTION_MEMORY_IN_MEGABYTES =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionProcessorAllocatedReconstructionMemoryInMegabytes",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           range(0, 4096));

  public static final HardwareConfigEnum NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSOR_NETWORK_CARDS =
    new HardwareConfigEnum(++_index,
                           "numberOfImageReconstructionProcessorNetworkCards",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_SERVICE_PORT_NUMBER =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionServicePortNumber",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

//
//image reconstruction processor ip addresses
//note that each processor has two physical ports with different ip addresses
//
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_1 =
    new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor0IpAddress1",
                           _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_2 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor0IpAddress2",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_1 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor1IpAddress1",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_2 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor1IpAddress2",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_1 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor2IpAddress1",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_2 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor2IpAddress2",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_1 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor3IpAddress1",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_2 =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionProcessor3IpAddress2",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_0_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine0Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_1_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine1Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_2_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine2Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_3_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine3Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_4_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine4Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_5_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine5Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_6_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine6Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_7_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine7Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_8_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine8Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_9_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine9Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_10_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine10Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_11_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine11Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_12_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine12Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_13_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine13Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_14_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine14Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_15_PORT =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine15Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_0_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine0HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_1_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine1HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_2_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine2HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_3_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine3HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_4_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine4HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_5_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine5HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_6_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine6HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_7_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine7HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_8_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine8HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_9_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine9HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_10_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine10HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_11_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine11HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_12_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine12HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_13_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine13HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_14_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine14HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
  
  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_15_HOST_PROCESSOR =
      new HardwareConfigEnum(++_index,
                             "imageReconstructionEngine15HostProcessor",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum MAX_RECONSTRUCITON_REGION_SIZE_DUE_TO_WARP_LIMITS_IN_NANOMETERS =
    new HardwareConfigEnum(++_index,
                           "maxReconstructionRegionSizeDueToWarpLimitsInNanoMeters",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  public static final HardwareConfigEnum MAX_RECONSTRUCTION_REGION_SIZE_DUE_TO_CAMERA_LIMITS_IN_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxReconstructionRegionSizeDueToCameraLimitsInNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

  public static final HardwareConfigEnum XRAY_SOURCE_TYPE =
      new HardwareConfigEnum(++_index,
                             "xraySourceType",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[] {"legacy", "standard", "htube"});

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_SERIAL_PORT_NAME =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceSerialPortName",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             new Object[] {"COM1", "COM2", "COM3", "COM4"}); // want this restriction);

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_TURN_ON_FOCUS_VOLTS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceTurnOnFocusVolts",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 3000.0));  // 5DX code comment sez: MLS added 2/00, usable range is 600 to 2980, but use 0-3300v for x-ray lab experimentation.
                                                   // commandable range tops out at 3000, however

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_TURN_ON_FILAMENT_VOLTS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceTurnOnFilamentVolts",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 6.0)); // lower than commanding range 'cuz upper end not realizable

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_MAX_MINUTES_DOWN_FOR_WARM_START =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceMaxMinutesDownForWarmStart",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 1440)); // <= 1 day

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_LAST_SHUTDOWN_TIME_IN_MILLIS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceLastShutdownTimeInMillis",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             lowerBound(0.0));

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_COLD_START_ANODE_VOLTAGE_RAMP_TIME_IN_MINUTES =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceColdStartAnodeVoltageRampTimeInMinutes",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             lowerBound(0.0));

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_WARM_START_ANODE_VOLTAGE_RAMP_TIME_IN_MINUTES =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceWarmStartAnodeVoltageRampTimeInMinutes",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             lowerBound(0.0));

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_TURN_ON_ANODE_KILOVOLTS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceTurnOnAnodeKiloVolts",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 165.0)); // 5DX code comment about "safety regultations"
                                                // this is also the same as the commanding range

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_EXPECTED_ANODE_ON_BLEEDER_MICROAMPS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceExpectedAnodeOnBleederMicroAmps",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(46.0, 54.0)); // 48 - 52 according to Jim Behnke (deployment)

  public static final HardwareConfigEnum LEGACY_XRAY_SOURCE_TURN_ON_CATHODE_MICROAMPS =
      new HardwareConfigEnum(++_index,
                             "legacyXraySourceTurnOnCathodeMicroAmps",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 198.5)); // 5DX code comment about "safety regultations"
                                                // actually 5DX has 198.5 for production and 125. for testing;
                                                // however, that was almost certainly a bug; leave it loose
                                                // for now (keep probable bug) to match old system

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_SERIAL_PORT_NAME =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceSerialPortName",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null); // Need to remove this restriction in order to use virtual COM port
                             //new Object[] {"COM1", "COM2", "COM3", "COM4"}); // want this restriction);

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_IMAGING_VOLTAGE_KILOVOLTS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceImagingVoltageKiloVolts",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 130));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_IMAGING_CURRENT_MICROAMPS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceImagingCurrentMicroAmps",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 300));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_IMAGING_FOCAL_SPOT_MODE =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceImagingFocalSpotMode",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 2));
  
  //Variable Mag Anthony August 2011
  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_VOLTAGE_KILOVOLTS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceHighMagImagingVoltageKiloVolts",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 130));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_CURRENT_MICROAMPS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceHighMagImagingCurrentMicroAmps",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 300));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_FOCAL_SPOT_MODE =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceHighMagImagingFocalSpotMode",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 2));
  

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_TURN_ON_DELAY_SECONDS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceTurnONDelaySeconds",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 200.0));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_TURN_OFF_DELAY_SECONDS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceTurnOFFDelaySeconds",
                             _HARDWARE_CONFIG,
                             TypeEnum.DOUBLE,
                             range(0.0, 200.0));

  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_KEEPALIVE_TIMEOUT_SECONDS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceKeepAliveTimeoutSeconds",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             range(0, 60));
  
  public static final HardwareConfigEnum XRAY_FILTER_INSTALLED =
      new HardwareConfigEnum(++_index,
                             "xrayFilterInstalled",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

  public static final HardwareConfigEnum SWITCH_IP_ADDRESS =
      new HardwareConfigEnum(++_index,
                             "switchIPAddress",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);

    public static final HardwareConfigEnum STAGE_MOTION_RANGE_OK_FOR_SYS_MTF_CONFIRMATION =
      new HardwareConfigEnum(++_index,
                             "stageMotionRangeOkForSysMTFConfirmation",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

    public static final HardwareConfigEnum  STAGE_MOTION_RANGE_OK_FOR_MAG_COUPON_CONFIRMATION =
      new HardwareConfigEnum(++_index,
                             "stageMotionRangeOkForMagCouponConfirmation",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

    public static final HardwareConfigEnum  STAGE_MOTION_RANGE_OK_FOR_SYS_FID_CONFIRMATION =
      new HardwareConfigEnum(++_index,
                             "stageMotionRangeOkForSysFidConfirmation",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

    public static final HardwareConfigEnum  STAGE_MOTION_RANGE_OK_FOR_SYS_FID_B_CONFIRMATION =
      new HardwareConfigEnum(++_index,
                             "stageMotionRangeOkForSysFidBConfirmation",
                             _HARDWARE_CONFIG,
                             TypeEnum.BOOLEAN,
                             null);

    public static final HardwareConfigEnum NOMINAL_BELT_THICKNESS_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "nominalBeltThicknessNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

    public static final HardwareConfigEnum MAX_BELT_THICKNESS_TOLERANCE_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxBeltThicknessToleranceNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

    public static final HardwareConfigEnum MIN_BELT_THICKNESS_TOLERANCE_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "minBeltThicknessToleranceNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

    public static final HardwareConfigEnum MAX_BELT_THICKNESS_WEAR_AND_TEAR_LOSS_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxBeltThicknessWearAndTearLossNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

    public static final HardwareConfigEnum BELT_RESTING_SURFACE_DISTANCE_FROM_SYSTEM_COUPON_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "beltRestingSurfaceDistanceFromSystemCouponNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

//
// optical camera properties
//

    public static final HardwareConfigEnum OPTICAL_CAMERA_MODEL =
        new HardwareConfigEnum(++_index,
                           "opticalCameraModel",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           null);

    public static final HardwareConfigEnum OPTICAL_CAMERA_LOGGING_ENABLED =
        new HardwareConfigEnum(++_index,
                           "opticalCameraLoggingEnabled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           null);
    
    public static final HardwareConfigEnum STAGE_NAVIGATION_COARSE_MOVE_STEP_SIZE_IN_NANOMETERS =
        new HardwareConfigEnum(++_index,
                           "stageNavigationCoarseMoveStepSizeInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
    
    public static final HardwareConfigEnum STAGE_NAVIGATION_FINE_MOVE_STEP_SIZE_IN_NANOMETERS =
        new HardwareConfigEnum(++_index,
                           "stageNavigationFineMoveStepSizeInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
    
    public static final HardwareConfigEnum DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS =
        new HardwareConfigEnum(++_index,
                           "distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
    
    public static final HardwareConfigEnum DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_FRONT_MODULE =
      new HardwareConfigEnum(++_index,
                         "distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometersForFrontModule",
                         _HARDWARE_CONFIG,
                         TypeEnum.INTEGER,
                         null);
    
     public static final HardwareConfigEnum DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_REAR_MODULE =
        new HardwareConfigEnum(++_index,
                           "distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometersForRearModule",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

    public static final HardwareConfigEnum DISTANCE_BETWEEN_FIXED_RAIL_DOWN_SHIM_TOP_FIDUCIAL_AREA_TO_SYSTEM_COUPON_IN_NANOMETERS =
        new HardwareConfigEnum(++_index,
                           "distanceBetweenFixedRailDownShimTopFiducialAreaToSystemCouponInNanometers",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
    
  public static final HardwareConfigEnum FRONT_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL =
        new HardwareConfigEnum(++_index,
                           "frontModuleCropImageOffsetXInPixel",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum REAR_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL =
        new HardwareConfigEnum(++_index,
                           "rearModuleCropImageOffsetXInPixel",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
   
  //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
  /*public static final HardwareConfigEnum LARGER_FOV_FRONT_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL =
        new HardwareConfigEnum(++_index,
                           "largerFovFrontModuleCropImageOffsetXInPixel",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum LARGER_FOV_REAR_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL =
        new HardwareConfigEnum(++_index,
                           "largerFovRearModuleCropImageOffsetXInPixel",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);*/  

  //Variable Mag Anthony August 2011
  public static final HardwareConfigEnum XRAY_MAGNIFICATION_CYLINDER_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "xrayMagnificationCylinderInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
  
  //S2EX Anthony 2014
  public static final HardwareConfigEnum XRAY_Z_AXIS_MOTOR_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "xrayZAxisMotorInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
    public static final HardwareConfigEnum XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION =
      new HardwareConfigEnum(++_index,
                           "xrayZAxisMotorPositionLookupForLowMagnification",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"M23", "M19", "M11"});
    public static final HardwareConfigEnum XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION =
      new HardwareConfigEnum(++_index,
                           "xrayZAxisMotorPositionLookupForHighMagnification",
                           _HARDWARE_CONFIG,
                           TypeEnum.STRING,
                           new Object[] {"M23", "M19", "M11"});
    public static final HardwareConfigEnum XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "xrayMotorizedHeightLevelSafetySensorInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
    //Wei Chin
  public static final HardwareConfigEnum SYSTEM_INSPECTION_TYPE =
      new HardwareConfigEnum(++_index,
                           "systemInspectionType",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);

  //XXL Optical Stop Sensor - Anthony (May 2013)
  public static final HardwareConfigEnum OPTICAL_STOP_CONTROLLER_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "opticalStopControllerInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
 
  //XXL Optical Stop Sensor - Anthony (May 2013)
  public static final HardwareConfigEnum OPTICAL_STOP_CONTROLLER_END_STOPPER_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "opticalStopControllerEndStopperInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  //New Optical Stop Controller - Anthony (May 2015)
  public static final HardwareConfigEnum OPTICAL_STOP_CONTROLLER_END_STOPPER_RESUMED_AS_NORMAL_PIP =
      new HardwareConfigEnum(++_index,
                           "opticalStopControllerEndStopperResumedAsNormalPIP",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

// George A. David moved to SystemConfigEnum
//
//    public static final HardwareConfigEnum DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS =
//      new HardwareConfigEnum(++_index,
//                             "distanceFromCameraArrayToXraySourceInNanometers",
//                             _HARDWARE_CONFIG,
//                             TypeEnum.INTEGER,
//                             null);
//    // Nominal magnification
//    public static final HardwareConfigEnum REFERENCE_PLANE_MAGNIFICATION_NOMINAL =
//        new HardwareConfigEnum(++_index,
//                              "referencePlaneMagnificationNominal",
//                              _HARDWARE_CONFIG,
//                              TypeEnum.DOUBLE,
//                              null);
//      public static final HardwareConfigEnum REFERENCE_PLANE_MAGNIFICATION =
//          new HardwareConfigEnum(++_index,
//                                "referencePlaneMagnification",
//                                _HARDWARE_CONFIG,
//                                TypeEnum.DOUBLE,
//                                null);
  
    // PSP Module Version Control
    public static final HardwareConfigEnum PSP_MODULE_VERSION =
        new HardwareConfigEnum(++_index,
                            "pspModuleVersion",
                            _HARDWARE_CONFIG,
                            TypeEnum.INTEGER,
                            null);
	
	// PSP Calibration Version Control
    public static final HardwareConfigEnum PSP_CALIBRATION_VERSION =
        new HardwareConfigEnum(++_index,
                            "pspCalibrationVersion",
                            _HARDWARE_CONFIG,
                            TypeEnum.INTEGER,
                            null);
    
    // PSP Setting Control
    public static final HardwareConfigEnum PSP_SETTING =
        new HardwareConfigEnum(++_index,
                            "pspSetting",
                            _HARDWARE_CONFIG,
                            TypeEnum.INTEGER,
                            null);
    
    // PSP Calibration Jig Object Plane Height
    public static final HardwareConfigEnum PSP_ON_STAGE_JIG_OBJECT_PLANE_ZHEIGHT_IN_MICRON =
        new HardwareConfigEnum(++_index,
                            "pspOnStageJigObjectPlaneZHeightInMicron",
                            _HARDWARE_CONFIG,
                            TypeEnum.INTEGER,
                            null);
    
    // PSP Optical Fiducial Adjustment AWA Width (in nanometers)
    public static final HardwareConfigEnum PSP_OPTICAL_FIDUCIAL_ADJUSTMENT_AWA_WIDTH_IN_NANOMETERS =
        new HardwareConfigEnum(++_index,
                            "pspOpticalFiducialAdjustmentAWAWidthInNanometers",
                            _HARDWARE_CONFIG,
                            TypeEnum.INTEGER,
                            null);

    public static final HardwareConfigEnum PSP_MICROCONTROLLER_SERIAL_PORT_NAME =
      new HardwareConfigEnum(++_index,
                             "pspMicroControllerSerialPortName",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
    
    // left to right delay time 
    public static final HardwareConfigEnum LEFT_TO_RIGHT_DELAY_TIME_IN_MILISECONDS =
    new HardwareConfigEnum(++_index,
                           "leftToRightDelayTimeInMiliseconds",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_HOST_IP_ADDRESS =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngineHostIpAddress",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_CURRENT_SETTING_MICRO_AMPS =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngineCurrentSettingMicroAmps",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_PERIOD =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEnginePeriod",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_0_IP_ADDRESS =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngine0IpAddress",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_0_PORT =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngine0Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_1_IP_ADDRESS =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngine1IpAddress",
                             _HARDWARE_CONFIG,
                             TypeEnum.STRING,
                             null);
    
    public static final HardwareConfigEnum ETHERNET_BASED_LIGHT_ENGINE_1_PORT =
      new HardwareConfigEnum(++_index,
                             "ethernetBasedLightEngine1Port",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    /**
   * @author Swee-Yee.Wong
   * Added a output bit to optical PIP sensor for signal strength adjustment (better stability)
   */
    public static final HardwareConfigEnum OPTICAL_PIP_RESET_INSTALLED =
      new HardwareConfigEnum(++_index,
                           "opticalPIPResetInstalled",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());
    
    public static final HardwareConfigEnum OPTICAL_PIP_RESET_SIGNAL_DURATION =
      new HardwareConfigEnum(++_index,
                             "opticalPIPResetSignalDuration",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum OUTER_BATTIERS_CLOSE_DELAY_FOR_INTERLOCK_CONFIRMATION =
      new HardwareConfigEnum(++_index,
                             "outerBarriersCloseDelayForInterlockConfirmation",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

	public static final HardwareConfigEnum MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxInspectionRegionWidthInNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxInspectionRegionLengthInNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxHighMagInspectionRegionWidthInNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);
    
    public static final HardwareConfigEnum MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS =
      new HardwareConfigEnum(++_index,
                             "maxHighMagInspectionRegionLengthInNanoMeters",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);

    //swee yee wong
    public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_FLOATING_PORT_NUMBER =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionEngineFloatingPortNumber",
                           _HARDWARE_CONFIG,
                           TypeEnum.BOOLEAN,
                           booleanRange());

  public static final HardwareConfigEnum IMAGE_RECONSTRUCTION_ENGINE_STARTING_PORT_NUMBER =
    new HardwareConfigEnum(++_index,
                           "imageReconstructionEngineStartingPortNumber",
                           _HARDWARE_CONFIG,
                           TypeEnum.INTEGER,
                           null);
  
  public static final HardwareConfigEnum HTUBE_XRAY_SOURCE_MOMENT_DELAY_IN_MILLIS =
      new HardwareConfigEnum(++_index,
                             "htubeXraySourceMomentDelayInMillis",
                             _HARDWARE_CONFIG,
                             TypeEnum.INTEGER,
                             null);


  /**
   * @author Greg Esparza
   */
  public static String getXmpSynqPciMotionControllerModelName()
  {
    return _XMP_SYNQNET_PCI_MOTION_CONTROLLER_MODEL_NAME;
  }

  /**
   * @author Rex Shang
   */
  public static Integer getMotionControllerId0()
  {
    return _MOTION_CONTROLLER_ID_0;
  }

  /**
   * @author Greg Esparza
   */
  public static String getAxiTdiXrayCameraModelName()
  {
    return _AXI_TDI_XRAY_CAMERA_MODEL_NAME;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getAxiOpticalCameraModelName()
  {
    return _AXI_OPTICAL_CAMERA_MODEL_NAME;
  }
  
   /**
   * @author Jack Hwee
   */
  public static String getAxiOpticalCameraModelNameTwo()
  {
    return _AXI_OPTICAL_CAMERA_MODEL_NAME_TWO;
  }

  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getHardwareConfigEnums()
  {
    Assert.expect(_hardwareConfigEnums != null);

    return _hardwareConfigEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_hardwareConfigEnums != null);

    return _hardwareConfigEnums;
  }

  /**
   * creates an object array of valid values for system type names
   * @author George A. David
   */
  public static Object[] getSystemTypeValidValues()
  {
    List<SystemTypeEnum> systemTypes =  SystemTypeEnum.getAllSystemTypes();
    Object[] systemTypeNames = new Object[systemTypes.size()];
    for(int i = 0; i < systemTypes.size(); ++i)
    {
      systemTypeNames[i] = systemTypes.get(i).getName();
    }

    return systemTypeNames;
  }
}
