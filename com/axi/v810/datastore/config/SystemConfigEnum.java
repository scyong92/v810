package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * This class contains all settings that are different among the different
 * system types. Currently we have a standard system and a a throughput system.
 * There will be one of these files for each system type. It will also be binary.
 * standard.binary.config and throughput.binary.config.
 *
 * @author George A. David
 */
public class SystemConfigEnum extends ConfigEnum implements Serializable
{

  private static int _index = -1;
//  private static final String _SYSTEM_CONFIG = FileName.getSystemConfigFullPath(XrayTester.getSystemType());
  private static String _SYSTEM_CONFIG = FileName.getCurrentSystemConfigFullPath();
  private static List<ConfigEnum> _systemConfigEnums = new ArrayList<ConfigEnum>();
  private Map<String, Object> _systemConfigFileToDefaultValueMap = new HashMap<String, Object>();
  private static Config _config = Config.getInstance();
  private static final String _POSITION_NAME_23_MICRON = "M23";
  private static final String _POSITION_NAME_19_MICRON = "M19";
  private static final String _POSITION_NAME_11_MICRON = "M11";
  private static final String _xrayZAxisMotorPositionLookupForLowMagnification =
    Config.getSystemCurrentLowMagnification();
  private static final String _xrayZAxisMotorPositionLookupForHighMagnification =
    Config.getSystemCurrentHighMagnification();

  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the config file
   * @param type TypeEnum which determines the type of value associated with this key.
   *
   * @author George A. David
   */
  protected SystemConfigEnum(int id,
          String key,
          TypeEnum type)
  {
    super(id,
            key,
            _SYSTEM_CONFIG,
            type,
            null);

    _systemConfigEnums.add(this);
  }

  ////////////////////////////////////////
  // begin moved from software.config
  public static final SystemConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION =
          new SystemConfigEnum(++_index,
          "maxStepSizeInNanoMetersForDefaultLowSignalCompensation",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION =
          new SystemConfigEnum(++_index,
          "maxStepSizeInNanoMetersForMediumSignalCompensation",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION =
          new SystemConfigEnum(++_index,
          "maxStepSizeInNanoMetersForMediumHighSignalCompensation",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION =
          new SystemConfigEnum(++_index,
          "maxStepSizeInNanoMetersForHighSignalCompensation",
          TypeEnum.INTEGER);

  // Margins to use for requesting images:
  public static final SystemConfigEnum CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmMagCouponMarginInXFromQuadrilateralNanometers",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmMagCouponMarginForFocusRegionNanometers",
          TypeEnum.INTEGER);

  // special value because all of coupon isn't visible in mag pocket
  public static final SystemConfigEnum CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmMagCouponSyscoupLowerRightVisibleDotYNanometers",
          TypeEnum.INTEGER);
  
  public static final SystemConfigEnum CONFIRM_HIGH_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmHighMagCouponSyscoupLowerRightVisibleDotYNanometers",
          TypeEnum.INTEGER);

  public static final SystemConfigEnum CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
          new SystemConfigEnum(++_index,
          "confirmMagCouponKeepYNanometersBelowSystemFiducialForTemplateMatch",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmSystemFiducialMarginForFocusRegionNanometers",
          TypeEnum.INTEGER);

  // Remove quadrilateral so it won't affect template match
  public static final SystemConfigEnum CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
          new SystemConfigEnum(++_index,
          "confirmSystemFiducialKeepYNanometersBelowSystemFiducialForTemplateMatch",
          TypeEnum.INTEGER);

  // Special value because bottom of quadrilateral isn't visible in system fiducial pocket (i.e. fixed rail,
  // right side) on all machines.  This is 2 mm less than lowest visible dot on coupon according to CAD
  // drawing (19.85 mm - 2 mm or 19850000 nm - 2000000 nm = 17850000 nm).
  public static final SystemConfigEnum CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
          new SystemConfigEnum(++_index,
          "confirmSystemFiducialSyscoupLowerRightVisibleDotYNanometers",
          TypeEnum.INTEGER);
  // end moved from software.config
  ////////////////////////////////////////

  ////////////////////////////////////////
  // begin moved from hardware.config
  public static final SystemConfigEnum DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS =
          new SystemConfigEnum(++_index,
          "distanceFromCameraArrayToXraySourceInNanometers",
          TypeEnum.INTEGER);
  
  // Nominal magnification
  public static final SystemConfigEnum REFERENCE_PLANE_MAGNIFICATION_NOMINAL =
          new SystemConfigEnum(++_index,
          "referencePlaneMagnificationNominal",
          TypeEnum.DOUBLE);
  public static final SystemConfigEnum REFERENCE_PLANE_MAGNIFICATION =
          new SystemConfigEnum(++_index,
          "referencePlaneMagnification",
          TypeEnum.DOUBLE);
  
  // Nominal High Magnification
   public static final SystemConfigEnum DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS =
          new SystemConfigEnum(++_index,
          "distanceFromCameraArrayTo2ndXraySourceInNanometers",
          TypeEnum.INTEGER);
  public static final SystemConfigEnum REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL =
          new SystemConfigEnum(++_index,
          "referencePlaneHighMagnificationNominal",
          TypeEnum.DOUBLE);
  public static final SystemConfigEnum REFERENCE_PLANE_HIGH_MAGNIFICATION =
          new SystemConfigEnum(++_index,
          "referencePlaneHighMagnification",
          TypeEnum.DOUBLE);

  public static final SystemConfigEnum KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED =
          new SystemConfigEnum(++_index,
          "yAxis_maximumMotorAccelerationLimitInNanometersPerSecondSquared",
          TypeEnum.STRING);

  // end moved form hardware.config
  ////////////////////////////////////////

  /**
   * @author George A. David
   */
  static
  {
    // setup the default values

    ////////////////////////////////////////
    // begin moved from software.config
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.STANDARD, 15011400);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 18390000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.XXL, 18390000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.S2EX, 18390000);

    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.STANDARD, 6451600);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7903000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.XXL, 7903000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.S2EX, 7903000);

    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.STANDARD, 4318000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5290000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.XXL, 5290000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.S2EX, 5290000);

    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.STANDARD, 3225800);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 3952000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.XXL, 3952000);
    SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION.setDefaultValue(SystemTypeEnum.S2EX, 3952000);

    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 1000000);

    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 650000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 650000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 650000);

    SystemConfigEnum.CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 14550000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11750000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 11750000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 11750000);

    SystemConfigEnum.CONFIRM_HIGH_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 14550000);
    SystemConfigEnum.CONFIRM_HIGH_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 14550000);
    SystemConfigEnum.CONFIRM_HIGH_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 14550000);
    SystemConfigEnum.CONFIRM_HIGH_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 14550000);

    SystemConfigEnum.CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.STANDARD, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.XXL, 1000000);
    SystemConfigEnum.CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.S2EX, 1000000);

    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 1000000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 650000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 650000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 650000);

    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.STANDARD, 1000000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1000000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.XXL, 1000000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH.setDefaultValue(SystemTypeEnum.S2EX, 1000000);

    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17850000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11750000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 11750000);
    SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 11750000);
    // end moved from software.config
    ////////////////////////////////////////

    ////////////////////////////////////////
    // begin moved from hardware.config
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 241858800);
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 250760000);
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 282738500);
    //for 19 micron 
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 307827000);
    //for 23 micron 
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 323418000); //323018000); //323818000);

    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.STANDARD, 6.25);
    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5.26315789473684);
    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.XXL, 5.26315789473684);
    //S2EX - CRITICAL, 30mils above belt - nominal value
    //hardcode this for now, will move to hardware config
    //For now the magnification is fixed but Distance From Xray Source to Ref Plane is affected instead
    //19 micron
//    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 5.26315789473684);
    //23 micron
//    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 4.34782608695652);

    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 6.22);
    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5.24262031368997);
    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 5.24262031368997);
//    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 5.2631695932); ////Value for 19 micron
//    SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 4.34782608695652);  //Value for 23 micron

    SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED.setDefaultValue(SystemTypeEnum.STANDARD, 6400000000L);
    SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED.setDefaultValue(SystemTypeEnum.THROUGHPUT, 9600000000L);
    SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED.setDefaultValue(SystemTypeEnum.XXL, 9600000000L);
    SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED.setDefaultValue(SystemTypeEnum.S2EX, 9600000000L);

    // High Magnification (Wei Chin)
////    10 um
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 241858800);
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 225488900);
//
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.STANDARD, 9.9999);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.THROUGHPUT, 10.0);
//
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 9.9523);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 9.9946784922394);
  
    // 11um
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 241858800);
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 228025291);
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 257287600);
    //For future reference, the resolution might be 11 micron or 13 micron. Needs to take note
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 280157000);
//
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.STANDARD, 9.9999);
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.THROUGHPUT, 9.09090909090909);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 9.09090909090909);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.XXL, 9.09090909090909);
//
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 9.9999);
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 9.09090909090909);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 9.09090909090909);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 9.09090909090909);
    
    // 13um
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 241858800);
//    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 233264367);
    SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 263239282);

//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.STANDARD, 7.69230769230769);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.69230769230769);
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.XXL, 7.69230769230769);

//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 7.69230769230769);
//    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.69230769230769);
    SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 7.69230769230769);
    // end moved form hardware.config
    ////////////////////////////////////////

    //Ngie Xing
    setupLowAndHighMagAccordingToChosenMagInHardwareConfig();
    
    // a sanity check to enforce that all enums have a default value for all
    // system types  and initialize the correct file name. we couldn't do this
    // beforehand because of a circular dependency.
//    _SYSTEM_CONFIG = FileName.getSystemConfigFullPath(XrayTester.getSystemType());
    for (ConfigEnum configEnum : _systemConfigEnums)
    {
      SystemConfigEnum systemConfig = (SystemConfigEnum) configEnum;
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        systemConfig.getDefaultValue(systemType);
      }
//      ((ConfigEnum)systemConfig).setFileName(_SYSTEM_CONFIG);
    }
  }
  
  /**
   * This used for Systems with Various Magnifications (more than 2) and
   * implements the use of look up table in hardware config.
   * Make sure that hardware config is read before reading system config.
   * 
   * Separate Low and High Mag because Different Scan Motion Profiles are classified 
   * as either Nominal or High Nominal Magnification.
   *
   * @author Ngie Xing
   */
  private static void setupLowAndHighMagAccordingToChosenMagInHardwareConfig()
  {
    setupLowMagAccordingToChosenMagInHardwareConfig();
    setupHighMagAccordingToChosenMagInHardwareConfig();
  }
  
  /**
   * This is used for Systems with Various LOW Magnifications (more than 2) and
   * implements the use of look up table in hardware config.
   *
   * @author Ngie Xing
   */
  private static void setupLowMagAccordingToChosenMagInHardwareConfig()
  {
    if (_xrayZAxisMotorPositionLookupForLowMagnification.equalsIgnoreCase(_POSITION_NAME_23_MICRON))
    {
      SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 323818000);
      SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 4.34782608695652);
      SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 4.34782608695652);
    }
    else if (_xrayZAxisMotorPositionLookupForLowMagnification.equalsIgnoreCase(_POSITION_NAME_19_MICRON))
    {
      SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 307827000);
      SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 5.26315789473684);
      SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 5.2631695932);
    }
  }
  
  /**
   * This is used for Systems with Various HIGH Magnifications (more than 2) and
   * implements the use of look up table in hardware config.
   *
   * @author Ngie Xing
   */
  private static void setupHighMagAccordingToChosenMagInHardwareConfig()
  {
    if (_xrayZAxisMotorPositionLookupForHighMagnification.equalsIgnoreCase(_POSITION_NAME_11_MICRON))
    {
      SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 280157000);
      SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL.setDefaultValue(SystemTypeEnum.S2EX, 9.09090909090909);
      SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 9.09090909090909);
    }
  }

//  /**
//   * @return the configuration file that holds the key/value.
//   *
//   * @author George A. David
//   */
//  public synchronized String getFileName(SystemTypeEnum systemType)
//  {
//    return FileName.getSystemConfigFullPath(systemType);
//  }
//
//  /**
//   * @return the configuration file that holds the key/value.
//   *
//   * @author George A. David
//   */
//  public synchronized String getFileName()
//  {
//    if(_config.isLoaded())
//      return FileName.getSystemConfigFullPath(XrayTester.getSystemType());
//    else
//      return FileName.getSystemConfigFullPath(Config.getInstance().getSystemType());
////    String fileName = _systemTypeToFilePathMap.get(XrayTester.getSystemType());
////
////    Assert.expect(fileName != null, "No file for system type " + XrayTester.getSystemType().getName());
////    return fileName;
//  }

  /**
   * @author George A. David
   */
  protected Object getDefaultValue()
  {
    String fileName = getFileName();
    Object value = _systemConfigFileToDefaultValueMap.get(fileName);

    Assert.expect(value != null, "No default value for " + fileName);
    return value;
  }

  /**
   * @author George David
   */
  public Object getDefaultValue(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    Object defaultValue = _systemConfigFileToDefaultValueMap.get(FileName.getSystemConfigFullPath(systemType));

    Assert.expect(defaultValue != null, "No default value for system config " +
            getKey() + " and system type " + systemType.getName());
    return defaultValue;
  }

  /**
   * @author George A. David
   */
  private void setDefaultValue(SystemTypeEnum systemType, Object defaultValue)
  {
    Assert.expect(systemType != null);
    Assert.expect(defaultValue != null);

    Object previous = _systemConfigFileToDefaultValueMap.put(FileName.getSystemConfigFullPath(systemType), defaultValue);
    Assert.expect(previous == null); 
  }

  /**
   * @author George A. David
   */
  public static List<ConfigEnum> getSystemConfigEnums()
  {
    Assert.expect(_systemConfigEnums != null);

    return _systemConfigEnums;
  }

  /**
   * @author George A. David
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_systemConfigEnums != null);

    return _systemConfigEnums;
  }

  /**
   * @author George David
   */
  static void updateSystemType(SystemTypeEnum systemType)
  {
    _SYSTEM_CONFIG = FileName.getSystemConfigFullPath(systemType);
    for (ConfigEnum configEnum : _systemConfigEnums)
    {
      //SystemConfigEnum systemConfig = (SystemConfigEnum) configEnum;
      configEnum.setFileName(_SYSTEM_CONFIG);
    }
  }
  
  /**
   * @author Ngie Xing 
   */
  public static String[] getLowMagnificationNames()
  {
    return new String[]{_POSITION_NAME_23_MICRON, _POSITION_NAME_19_MICRON};
  }
  
  /**
   * @author Ngie Xing 
   */
  public static String[] getHighMagnificationNames()
  {
    return new String[]{_POSITION_NAME_11_MICRON};
  }
}
