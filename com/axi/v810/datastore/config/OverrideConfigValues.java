package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class OverrideConfigValues
{
  private static OverrideConfigValues _instance;

  private Pattern _keyValuePattern = Pattern.compile("(^\\s*([^#=\\s]+)(\\s*=\\s*)((\\\\#|[^#=])*))([#].*)?$");

  /**
   * @author Bill Darbie
   */
  public static synchronized OverrideConfigValues getInstance()
  {
    if (_instance == null)
      _instance = new OverrideConfigValues();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private OverrideConfigValues()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public void carryForward(String keysToCarryValuesForwardFileName, String fileWithValuesToOverride, String fileToBeOverriden) throws DatastoreException
  {
    Assert.expect(keysToCarryValuesForwardFileName != null);
    Assert.expect(fileWithValuesToOverride != null);
    Assert.expect(fileToBeOverriden != null);

    ConfigFileTextReader reader = new ConfigFileTextReader();

    // get the keys - there are no values defined in this file
    Map<String, String> keyValueMap = new HashMap<String, String>(reader.getKeyValueMapping(keysToCarryValuesForwardFileName));

    // find the values in the previous software.config file
    Map<String, String> keyValueMap2 = new HashMap<String, String>(reader.getKeyValueMapping(fileWithValuesToOverride));

    // take the keys from keyValueMap1 and fill in their values with those found in keyValueMap2
    for (String key : keyValueMap.keySet())
    {
      String value = keyValueMap2.get(key);
      if (value != null)
        keyValueMap.put(key, value);
    }

    override(keysToCarryValuesForwardFileName, fileToBeOverriden, keyValueMap);
  }
  
   /**
   * @author Jack Hwee
   */
  public void carryForwardBinaryConfig(String keysToCarryValuesForwardFileName, String fileWithValuesToOverride, String fileToBeOverriden) throws DatastoreException
  {
    Assert.expect(keysToCarryValuesForwardFileName != null);
    Assert.expect(fileWithValuesToOverride != null);
    Assert.expect(fileToBeOverriden != null);
    
    ConfigFileBinaryReader reader = new ConfigFileBinaryReader();
    List<ConfigEnum> configEnumList = new LinkedList<>();

    // open the hardware.calib.carryForward.config for reading
    String inputFileName = keysToCarryValuesForwardFileName;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    
    BufferedReader is = new BufferedReader(fr);

    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file

      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch (IOException ioe)
        {
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
        {
          continue;
        }

        Matcher keyValueMatcher = _keyValuePattern.matcher(line);

        if (keyValueMatcher.matches())
        {
          String keyValueStr = keyValueMatcher.group(1);
          String keyStr = keyValueMatcher.group(2);
          String equalsStr = keyValueMatcher.group(3);
     
          configEnumList.add(ConfigEnum.getConfigEnum(keyStr));
        }

      } while (line != null);
    }
    catch (DatastoreException ex)
    {
      throw ex;
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    }

    //Delete hardware.calib.binary 
    if (FileUtilAxi.exists(fileToBeOverriden))
    {
      FileUtilAxi.delete(fileToBeOverriden);
    }
     
    //Based on the list, read the value from previous hardware.calib.binary, then set the previous value for current hardware.calib.binary
    Map<ConfigEnum, Integer> keyValueMap = new HashMap<ConfigEnum, Integer>();
    
    keyValueMap = reader.readPreviousConfig(fileWithValuesToOverride, configEnumList);
    
    Config config = Config.getInstance();
    
    for (Map.Entry<ConfigEnum, Integer> entry : keyValueMap.entrySet())
    {
      int value = entry.getValue();
      ConfigEnum configEnum = entry.getKey();

      config.setValue(configEnum, value);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void override(String newValuesFileName, String fileToBeOverriden) throws DatastoreException
  {
    Assert.expect(newValuesFileName != null);
    Assert.expect(fileToBeOverriden != null);

    ConfigFileTextReader reader = new ConfigFileTextReader();
    Map<String, String> keyValueMap = new HashMap<String, String>(reader.getKeyValueMapping(newValuesFileName));

    override(newValuesFileName, fileToBeOverriden, keyValueMap);
  }
  
  /**
   * @author Jack Hwee
   */
  public void overrideOldOffsetToNewOffset(String newValuesFileName, String fileToBeOverriden, String oldHardwareConfigKeyString, List<String> newHardwareConfigKeyStringList) throws DatastoreException
  {
    Assert.expect(newValuesFileName != null);
    Assert.expect(fileToBeOverriden != null);

    ConfigFileTextReader reader = new ConfigFileTextReader();
    Map<String, String> keyValueMap = new HashMap<String, String>(reader.getKeyValueMapping(newValuesFileName));
    Assert.expect(keyValueMap != null);
    
    // open the file for reading
    String inputFileName = fileToBeOverriden;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    
    BufferedReader is = new BufferedReader(fr);

    // open a temporary file for writing
    String outputFileName = inputFileName + FileName.getTempFileExtension();
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(outputFileName);
    }
    catch(IOException ioe)
    {
      DatastoreException ex = new CannotCreateFileDatastoreException(outputFileName);
      ex.initCause(ioe);
      throw ex;
    }
    
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter os = new PrintWriter(bw);

    boolean exception = false;
    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file

      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          exception = true;
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        if (keyValueMap.isEmpty() == false)
        {
          Matcher keyValueMatcher = _keyValuePattern.matcher(line);

          if (keyValueMatcher.matches())
          {
            String keyValueStr = keyValueMatcher.group(1);
            String keyStr = keyValueMatcher.group(2);
            String equalsStr = keyValueMatcher.group(3);

            Object value = keyValueMap.get(keyStr);           
      
            if (value != null && value.toString().equalsIgnoreCase("")==false)
            {
              String prev = keyValueMap.remove(keyStr);
              Assert.expect(prev != null);
              String stringValue = value.toString();
              // change \ to \\
              stringValue = stringValue.replaceAll("\\\\", "\\\\\\\\");
              // change any # to \#
              stringValue = stringValue.replaceAll("#", "\\\\#");
              // change any $ to \$
              stringValue = stringValue.replaceAll("(\\$)", "\\\\$1");

              // change keyValueStr so it has \\ in front of all special characters so it will be interpreted
              // literally
              keyValueStr = keyValueStr.replaceAll("(\\W)","\\\\$1");
              // now replace the non comment part of the line with the new value
              line = line.replaceFirst(keyValueStr, keyStr + equalsStr + stringValue);
              
              if (keyStr.equals(oldHardwareConfigKeyString))
              {  
                // the value from old hardware config will be copy into new hardware config
                for (String newHardwareConfigKeyString: newHardwareConfigKeyStringList)
                {
                  keyValueMap.put(newHardwareConfigKeyString, stringValue);
                }
              }
            }
          }
        }
        os.println(line);
      } while(line != null);
    }
    catch (DatastoreException ex)
    {
      exception = true;
      throw ex;
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }

      if (os != null)
        os.close();

      if (exception)
      {
        try
        {
          if (FileUtilAxi.exists(outputFileName))
            FileUtilAxi.delete(outputFileName);
        }
        catch (DatastoreException dex)
        {
          dex.printStackTrace();
        }
      }
    }

    // now move the temporary file over to the original file
    FileUtilAxi.rename(outputFileName, inputFileName); 
  }

  /**
   * @author Bill Darbie
   */
  private void override(String newValuesFileName, String fileToBeOverriden, Map<String, String> keyValueMap) throws DatastoreException
  {
    Assert.expect(newValuesFileName != null);
    Assert.expect(fileToBeOverriden != null);
    Assert.expect(keyValueMap != null);

    // open the file for reading
    String inputFileName = fileToBeOverriden;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    // open a temporary file for writing
    String outputFileName = inputFileName + FileName.getTempFileExtension();
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(outputFileName);
    }
    catch(IOException ioe)
    {
      DatastoreException ex = new CannotCreateFileDatastoreException(outputFileName);
      ex.initCause(ioe);
      throw ex;
    }
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter os = new PrintWriter(bw);

    boolean exception = false;
    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file

      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          exception = true;
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        if (keyValueMap.isEmpty() == false)
        {
          Matcher keyValueMatcher = _keyValuePattern.matcher(line);

          if (keyValueMatcher.matches())
          {
            String keyValueStr = keyValueMatcher.group(1);
            String keyStr = keyValueMatcher.group(2);
            String equalsStr = keyValueMatcher.group(3);

            Object value = keyValueMap.get(keyStr);
            if (value != null && value.toString().equalsIgnoreCase("")==false)
            {
              String prev = keyValueMap.remove(keyStr);
              Assert.expect(prev != null);
              String stringValue = value.toString();
              // change \ to \\
              stringValue = stringValue.replaceAll("\\\\", "\\\\\\\\");
              // change any # to \#
              stringValue = stringValue.replaceAll("#", "\\\\#");
              // change any $ to \$
              stringValue = stringValue.replaceAll("(\\$)", "\\\\$1");

              // change keyValueStr so it has \\ in front of all special characters so it will be interpreted
              // literally
              keyValueStr = keyValueStr.replaceAll("(\\W)","\\\\$1");
              // now replace the non comment part of the line with the new value
              line = line.replaceFirst(keyValueStr, keyStr + equalsStr + stringValue);
            }
          }
        }
        os.println(line);
      } while(line != null);
    }
    catch (DatastoreException ex)
    {
      exception = true;
      throw ex;
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }

      if (os != null)
        os.close();

      if (exception)
      {
        try
        {
          if (FileUtilAxi.exists(outputFileName))
            FileUtilAxi.delete(outputFileName);
        }
        catch (DatastoreException dex)
        {
          dex.printStackTrace();
        }
      }

      // a line should have been modified for each key or there was a bug
//      if ((exception == false) && (keyValueMap.isEmpty() == false))
//      {
//        String key = keyValueMap.keySet().iterator().next();
//        String value = keyValueMap.get(key);
//        throw new InvalidKeyInConfigDatastoreException(key, value, newValuesFileName);
//      }
    }

    // now move the temporary file over to the original file
    FileUtilAxi.rename(outputFileName, inputFileName);
  }
}

