package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a key is found in the wrong file.
 * @author Bill Darbie
 */
public class KeyInWrongFileDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param key is the key that was found in the wrong config file
   * @param expectedConfigFileName is the config file that the key belongs in
   * @param actualConfigFileName is the config file that the key was found in
   * @author Bill Darbie
   */
  public KeyInWrongFileDatastoreException(String key, String expectedConfigFileName, String actualConfigFileName)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_KEY_IN_WRONG_FILE_KEY",
                              new Object[]{key,
                                           expectedConfigFileName,
                                           actualConfigFileName}));
    Assert.expect(key != null);
    Assert.expect(expectedConfigFileName != null);
    Assert.expect(actualConfigFileName != null);
  }
}
