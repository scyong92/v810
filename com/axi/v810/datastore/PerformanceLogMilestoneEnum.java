package com.axi.v810.datastore;

import java.util.*;

import com.axi.util.*;

/**
 * Enums used for Performance Log milestones
 *
 * @author George Booth
 */
public class PerformanceLogMilestoneEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static final PerformanceLogMilestoneEnum CYCLE_TIME_SEPARATOR = new PerformanceLogMilestoneEnum(++_index,
                                                 "#####################################################");
  public static final PerformanceLogMilestoneEnum START_LOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "START LOAD PANEL - stage in position to accept panel");
  public static final PerformanceLogMilestoneEnum CONTINUE_LOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "CONTINUE LOAD PANEL - panel place in rail");
  public static final PerformanceLogMilestoneEnum END_LOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "END LOAD PANEL - close outer barrier");
  public static final PerformanceLogMilestoneEnum START_FULL_INSPECTION = new PerformanceLogMilestoneEnum(++_index,
                                                 "START FULL INSPECTION");
  public static final PerformanceLogMilestoneEnum END_FULL_INSPECTION = new PerformanceLogMilestoneEnum(++_index,
                                                 "END FULL INSPECTION");
  public static final PerformanceLogMilestoneEnum PANEL_IN_LEFT_POSITION = new PerformanceLogMilestoneEnum(++_index,
                                                 "PANEL IN LEFT POSITION");
  public static final PerformanceLogMilestoneEnum PANEL_IN_RIGHT_POSITION = new PerformanceLogMilestoneEnum(++_index,
                                                 "PANEL IN RIGHT POSITION");
  public static final PerformanceLogMilestoneEnum START_INITIALIZE_TEST = new PerformanceLogMilestoneEnum(++_index,
                                                 "START InitializeTest");
  public static final PerformanceLogMilestoneEnum END_INITIALIZE_TEST = new PerformanceLogMilestoneEnum(++_index,
                                                 "END InitializeTest");
  public static final PerformanceLogMilestoneEnum SCAN_PATH_SIZE = new PerformanceLogMilestoneEnum(++_index,
                                                 "SCAN PATH SIZE");
  public static final PerformanceLogMilestoneEnum START_ALIGNMENT_ONLY_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ALIGNMENT-ONLY SCAN");
  public static final PerformanceLogMilestoneEnum END_ALIGNMENT_ONLY_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ALIGNMENT-ONLY SCAN");
  public static final PerformanceLogMilestoneEnum START_ALIGNMENT_INSPECTION_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ALIGNMENT+INSPECTION SCAN");
  public static final PerformanceLogMilestoneEnum END_ALIGNMENT_INSPECTION_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ALIGNMENT+INSPECTION SCAN");
  public static final PerformanceLogMilestoneEnum START_INSPECTION_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "START INSPECTION SCAN");
  public static final PerformanceLogMilestoneEnum END_INSPECTION_SCAN = new PerformanceLogMilestoneEnum(++_index,
                                                 "END INSPECTION SCAN");
  public static final PerformanceLogMilestoneEnum START_RECONSTRUCT = new PerformanceLogMilestoneEnum(++_index,
                                                 "START RECONSTRUCT");
  public static final PerformanceLogMilestoneEnum END_RECONSTRUCT = new PerformanceLogMilestoneEnum(++_index,
                                                 "END RECONSTRUCT");
  public static final PerformanceLogMilestoneEnum START_CLASSIFY = new PerformanceLogMilestoneEnum(++_index,
                                                 "START CLASSIFY");
  public static final PerformanceLogMilestoneEnum END_CLASSIFY = new PerformanceLogMilestoneEnum(++_index,
                                                 "END CLASSIFY");
  public static final PerformanceLogMilestoneEnum START_UNLOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "START UNLOAD - move stage to unload position");
  public static final PerformanceLogMilestoneEnum CONTINUE_UNLOAD_PANEL_1 = new PerformanceLogMilestoneEnum(++_index,
                                                 "CONTINUE UNLOAD - open outer barrier");
  public static final PerformanceLogMilestoneEnum CONTINUE_UNLOAD_PANEL_2 = new PerformanceLogMilestoneEnum(++_index,
                                                 "CONTINUE UNLOAD - now the panel sensor is clear");
  public static final PerformanceLogMilestoneEnum END_UNLOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "END UNLOAD - outer barrier closed");
  public static final PerformanceLogMilestoneEnum START_MOVE_STAGE_FOR_LOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "START MOVE STAGE FOR LOAD PANEL");
  public static final PerformanceLogMilestoneEnum END_MOVE_STAGE_FOR_LOAD_PANEL = new PerformanceLogMilestoneEnum(++_index,
                                                 "END MOVE STAGE FOR LOAD PANEL");
  // for Grayscale Cal
  public static final PerformanceLogMilestoneEnum GRAYSCALE_CAL_IGNORE_TRIGGERS = new PerformanceLogMilestoneEnum(++_index,
                                                 "GRAYSCALE CAL IGNORE TRIGGERS");
  public static final PerformanceLogMilestoneEnum GRAYSCALE_CAL_RESTORE_TRIGGERS = new PerformanceLogMilestoneEnum(++_index,
                                                 "GRAYSCALE CAL RESTORE TRIGGERS");
  public static final PerformanceLogMilestoneEnum GRAYSCALE_CAL_CALIB_STEP = new PerformanceLogMilestoneEnum(++_index,
                                                 "GRAYSCALE CAL STEP - "); // append XrayCameraEventEnum toString()
  // for IAE sequence
  public static final PerformanceLogMilestoneEnum START_ACQUIRE_IMAGES = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ACQUIRE IMAGES");
  public static final PerformanceLogMilestoneEnum END_ACQUIRE_IMAGES = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ACQUIRE IMAGES");
  public static final PerformanceLogMilestoneEnum GENERATE_IMAGE_CHAIN_PROGRAM = new PerformanceLogMilestoneEnum(++_index,
                                                 "GENERATE IMAGE CHAIN PROGRAM");
  public static final PerformanceLogMilestoneEnum START_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ACQUISITION SEQUENCE FOR ALL SUB PROGRAMS");
  public static final PerformanceLogMilestoneEnum END_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ACQUISITION SEQUENCE FOR ALL SUB PROGRAMS");
  public static final PerformanceLogMilestoneEnum TURN_OFF_TRIGGERS = new PerformanceLogMilestoneEnum(++_index,
                                                 "TURN OFF TRIGGERS");
  public static final PerformanceLogMilestoneEnum TURN_ON_TRIGGERS = new PerformanceLogMilestoneEnum(++_index,
                                                 "TURN ON TRIGGERS");
  public static final PerformanceLogMilestoneEnum DOWNLOAD_TEST_PROGRAMS = new PerformanceLogMilestoneEnum(++_index,
                                                 "DOWNLOAD TEST PROGRAMS");
  public static final PerformanceLogMilestoneEnum INITIALIZE_RECONSTRUCTED_IMAGES_MANAGER = new PerformanceLogMilestoneEnum(++_index,
                                                 "INITIALIZE RECONSTRUCTED IMAGES MANAGER");
  public static final PerformanceLogMilestoneEnum INITIALIZE_CAMERAS = new PerformanceLogMilestoneEnum(++_index,
                                                 "INITIALIZE CAMERAS");
  public static final PerformanceLogMilestoneEnum RUN_ACQUISITION_SEQUENCES = new PerformanceLogMilestoneEnum(++_index,
                                                 "RUN ACQUISITION SEQUENCES");
  public static final PerformanceLogMilestoneEnum OPTICAL_POINT_TO_POINT_SCAN_SIZE = new PerformanceLogMilestoneEnum(++_index,
                                                 "OPTICAL POINT TO POINT SCAN SIZE");
  public static final PerformanceLogMilestoneEnum START_ACQUIRE_OPTICAL_IMAGES = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ACQUIRE OPTICAL IMAGES");
  public static final PerformanceLogMilestoneEnum END_ACQUIRE_OPTICAL_IMAGES = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ACQUIRE OPTICAL IMAGES");
  public static final PerformanceLogMilestoneEnum START_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS = new PerformanceLogMilestoneEnum(++_index,
                                                 "START OPTICAL ACQUISITION SEQUENCE FOR ALL SUB PROGRAMS");
  public static final PerformanceLogMilestoneEnum END_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS = new PerformanceLogMilestoneEnum(++_index,
                                                 "END OPTICAL ACQUISITION SEQUENCE FOR ALL SUB PROGRAMS");
  public static final PerformanceLogMilestoneEnum START_POINT_TO_POINT_MOVE_STAGE = new PerformanceLogMilestoneEnum(++_index,
                                                 "START POINT-TO-POINT MOVE STAGE");
  public static final PerformanceLogMilestoneEnum END_POINT_TO_POINT_MOVE_STAGE = new PerformanceLogMilestoneEnum(++_index,
                                                 "END POINT-TO-POINT MOVE STAGE");
  public static final PerformanceLogMilestoneEnum START_OPTICAL_CAMERA_INITIALIZATION = new PerformanceLogMilestoneEnum(++_index,
                                                 "START OPTICAL CAMERA INITIALIZATION");
  public static final PerformanceLogMilestoneEnum END_OPTICAL_CAMERA_INITIALIZATION = new PerformanceLogMilestoneEnum(++_index,
                                                 "END OPTICAL CAMERA INITIALIZATION");
  public static final PerformanceLogMilestoneEnum START_ENABLE_OPTICAL_CAMERA_HARDWARE_TRIGGER = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ENABLE OPTICAL CAMERA HARDWARE TRIGGER");
  public static final PerformanceLogMilestoneEnum END_ENABLE_OPTICAL_CAMERA_HARDWARE_TRIGGER = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ENABLE OPTICAL CAMERA HARDWARE TRIGGER");
  public static final PerformanceLogMilestoneEnum START_ACQUIRE_OPTICAL_IMAGE = new PerformanceLogMilestoneEnum(++_index,
                                                 "START ACQUIRE OPTICAL IMAGE");
  public static final PerformanceLogMilestoneEnum END_ACQUIRE_OPTICAL_IMAGE = new PerformanceLogMilestoneEnum(++_index,
                                                 "END ACQUIRE OPTICAL IMAGE");

  
  private String _milestone;

  private static List<PerformanceLogMilestoneEnum> _allPerformanceLogMilestoneEnums;

  /**
   * @author Roy Williams
   * @author George Booth
   */
  static {
    Collection<com.axi.util.Enum> mapValues = getIdToEnumMap(PerformanceLogMilestoneEnum.class).values();
    _allPerformanceLogMilestoneEnums = new ArrayList<PerformanceLogMilestoneEnum>(mapValues.size());
    for (com.axi.util.Enum type : mapValues)
    {
      _allPerformanceLogMilestoneEnums.add((PerformanceLogMilestoneEnum)type);
    }
  }

   /**
    * @author George Booth
    */
   private PerformanceLogMilestoneEnum(int id, String milestone)
   {
     super(id);
     Assert.expect(milestone != null);

     _milestone = milestone;
   }

   /**
    * @author George Booth
    */
   public String toString()
   {
     return _milestone;
  }

  /**
   * @author George Booth
   */
  public static List<PerformanceLogMilestoneEnum> getAllTypes()
  {
    return _allPerformanceLogMilestoneEnums;
  }
}
