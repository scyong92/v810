package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is not found.
 * @author Bill Darbie
 */
public class FileNotFoundDatastoreException extends DatastoreException
{
  private String _fileName;

  /**
   * Construct the class.
   * @param fileName is the name of the file that is not found
   * @author Bill Darbie
   */
  public FileNotFoundDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_FILE_NOT_FOUND_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    Assert.expect(_fileName != null);
    return _fileName;
  }
}
