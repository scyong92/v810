package com.axi.v810.datastore;

import java.sql.*;

import com.axi.util.*;
import com.axi.v810.datastore.database.*;

/**
 * Contains some common relational database access helper methods.
 *
 * All code that interacts with a relational database should use this class and follow this basic paradigm:
 * <code>
 * try
 * {
 *   // some database access code here ...
 *   DatabaseUtil.checkForSQLWarnings(connection);
 * }
 * catch (SQLException sqlex)
 * {
 *   DatabaseUtil.handleSQLException(connection, sqlex);
 * }
 * </code>
 *
 * @author Matt Wharton
 */
public class RelationalDatabaseUtil
{
  private static final String SLASH = "/";
  private static final String BACKSLASH = "\\";

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private RelationalDatabaseUtil()
  {
    // Do nothing...
  }

  /**
   * Loads the specfied database driver class.
   *
   * @author Cheah Lee Herng
   * @author Matt Wharton
   */
  public static void loadDatabaseDriver(String databaseDriverClassName, DatabaseTypeEnum databaseTypeEnum)
      throws DatastoreException
  {
    Assert.expect(databaseDriverClassName != null);
    Assert.expect(databaseTypeEnum != null);

    try
    {
      Class.forName(databaseDriverClassName).newInstance();
    }
    catch (ClassNotFoundException cnfex)
    {
      DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, cnfex);
      dex.initCause(cnfex);
      throw dex;
    }
    catch (InstantiationException ie)
    {
      DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, ie);
      dex.initCause(ie);
      throw dex;
    }
    catch (IllegalAccessException iae)
    {
      DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, iae);
      dex.initCause(iae);
      throw dex;
    }
  }

  /**
   * Attempts to open a database connection using the specified connection string, userId and password.
   * Sets auto-commit to false.
   *
   * @param connectionString String
   * @param userId String
   * @param password String
   * @param databaseTypeEnum DatabaseTypeEnum
   * @return Connection
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static Connection openConnection(String connectionString,
                                         String userId,
                                         String password,
                                         DatabaseTypeEnum databaseTypeEnum) throws DatastoreException
  {
    Assert.expect(connectionString != null);
    Assert.expect(userId != null);
    Assert.expect(password != null);
    Assert.expect(databaseTypeEnum != null);

    Connection connection = null;
    try
    {
      connection = DriverManager.getConnection(connectionString, userId, password);
      if (connection != null)
      {
        connection.setAutoCommit(false);
      }
    }
    catch (SQLException sqlex)
    {
      DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, sqlex);
      dex.initCause(sqlex);
      throw dex;
    }

    Assert.expect(connection != null);

    return connection;
  }

  /**
   * Attempts the close the specified Connection.  Throws a DatastoreException if this fails.
   *
   * @author Matt Wharton
   */
  public static void closeConnection(Connection connection, DatabaseTypeEnum databaseTypeEnum) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(databaseTypeEnum != null);

    try
    {
      // Rollback any pending transactions.
      connection.rollback();

      connection.close();
    }
    catch (SQLException sqlex)
    {
      handleSQLException(connection, sqlex, databaseTypeEnum);
    }
  }

  /**
   * Rethrows the specified SQL Exception as a DatastoreException.  Also attempts to rollback any pending transactions.
   *
   * @author Matt Wharton
   */
  public static void handleSQLException(
      Connection connection,
      SQLException sqlException,
      DatabaseTypeEnum databaseTypeEnum) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(sqlException != null);
    Assert.expect(databaseTypeEnum != null);

    try
    {
      // Attempt to rollback any pending transactions.
      connection.rollback();
    }
    catch (SQLException sqlex)
    {
      // Do nothing...we're already throwing an exception.
    }

    DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, sqlException);
    dex.initCause(sqlException);
    throw dex;
  }

  /**
   * Checks to see if the specified Connection has any pending warnings.  If so, any pending transactions are
   * rolled back, and a DatastoreException is thrown.
   *
   * @author Matt Wharton
   */
  public static void checkForSQLWarnings(Connection connection, DatabaseTypeEnum databaseTypeEnum)
      throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(databaseTypeEnum != null);

    try
    {
      SQLWarning sqlWarning =  connection.getWarnings();
      if (sqlWarning != null)
      {
        handleSQLException(connection, sqlWarning, databaseTypeEnum);
      }
    }
    catch (SQLException sqlex)
    {
      handleSQLException(connection, sqlex, databaseTypeEnum);
    }
  }

  /**
   * Checks to see if the specified ResultSet has any pending warnings.  If so, any pending transactions are
   * rolled back, and a DatastoreException is thrown.
   *
   * @author Matt Wharton
   */
  public static void checkForSQLWarnings(Connection connection, ResultSet resultSet, DatabaseTypeEnum databaseTypeEnum)
      throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(resultSet != null);
    Assert.expect(databaseTypeEnum != null);

    try
    {
      // Check for warnings on the ResultSet.
      SQLWarning sqlWarning = resultSet.getWarnings();
      if (sqlWarning != null)
      {
        handleSQLException(connection, sqlWarning, databaseTypeEnum);
      }
      else
      {
        // Check for warnings on the underlying Connection.
        sqlWarning = connection.getWarnings();
        if (sqlWarning != null)
        {
          handleSQLException(connection, sqlWarning, databaseTypeEnum);
        }
      }
    }
    catch (SQLException sqlex)
    {
      handleSQLException(connection, sqlex, databaseTypeEnum);
    }
  }

  /**
   * Returns the number of rows in the specified result set.  Assumes the result set is free scrolling (i.e. not
   * "forward only").
   *
   * @author Matt Wharton
   */
  public static int getNumberOfRowsInResultSet(ResultSet resultSet, DatabaseTypeEnum databaseTypeEnum)
      throws DatastoreException
  {
    Assert.expect(resultSet != null);
    Assert.expect(databaseTypeEnum != null);

    int rowCount = 0;
    try
    {
      Assert.expect(resultSet.getType() != ResultSet.TYPE_FORWARD_ONLY);

      resultSet.last();
      rowCount = resultSet.getRow();
      resultSet.first();
    }
    catch (SQLException sqlex)
    {
      DatastoreException dex = new DatabaseDatastoreException(databaseTypeEnum, sqlex);
      dex.initCause(sqlex);
      throw dex;
    }

    return rowCount;
  }

  /**
   * This method gets database location and database name from library path.
   *
   * @param libraryPath String
   * @return Pair
   *
   * @author Cheah Lee Herng
   */
  public static Pair<String, String> getDatabasePathInfo(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    Pair<String, String> databasePathInfo = new Pair<String,String>();
    String databaseName = null;
    String databaseLocation = null;

    // Get database name and database location
    int indexOfSlash = libraryPath.lastIndexOf(SLASH);
    if (indexOfSlash == -1) indexOfSlash = libraryPath.lastIndexOf(BACKSLASH);

    if (indexOfSlash != -1)
    {
      databaseName = libraryPath.substring(indexOfSlash + 1, libraryPath.length());
      databaseLocation = libraryPath.substring(0, indexOfSlash);
    }

    Assert.expect(databaseName != null);
    Assert.expect(databaseLocation != null);

    databasePathInfo.setFirst(databaseName);
    databasePathInfo.setSecond(databaseLocation);
    return databasePathInfo;
  }

  /**
   *
   * @param statement Statement
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public static int getAutoGeneratedId(Statement statement) throws DatastoreException
  {
    Assert.expect(statement != null);

    int autoGeneratedKey = 0;

    try
    {
      ResultSet resultSet = statement.getGeneratedKeys();
      if (resultSet.next())
        autoGeneratedKey = resultSet.getInt(1);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return autoGeneratedKey;
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param index int
   * @param value String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void setString(PreparedStatement preparedStatement, int index, String value) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(index > 0);
    Assert.expect(value != null);

    try
    {
      preparedStatement.setString(index, value);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param index int
   * @param value long
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void setLong(PreparedStatement preparedStatement, int index, long value) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(index > 0);

    try
    {
      preparedStatement.setLong(index, value);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param index int
   * @param value int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void setInt(PreparedStatement preparedStatement, int index, int value) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(index > 0);

    try
    {
      preparedStatement.setInt(index, value);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param index int
   * @param value double
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void setDouble(PreparedStatement preparedStatement, int index, double value) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(index > 0);

    try
    {
      preparedStatement.setDouble(index, value);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param index int
   * @param value boolean
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void setBoolean(PreparedStatement preparedStatement, int index, boolean value) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(index > 0);

    try
    {
      preparedStatement.setBoolean(index, value);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void executeUpdate(PreparedStatement preparedStatement) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);

    try
    {
      preparedStatement.executeUpdate();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param statement Statement
   * @param sqlString String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void executeUpdate(Statement statement, String sqlString) throws DatastoreException
  {
    Assert.expect(statement != null);
    Assert.expect(sqlString != null);

    try
    {
      statement.executeUpdate(sqlString);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void addBatch(PreparedStatement preparedStatement) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);

    try
    {
      preparedStatement.addBatch();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void executeBatch(PreparedStatement preparedStatement) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);

    try
    {
      preparedStatement.executeBatch();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param preparedStatement PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static ResultSet executeQuery(PreparedStatement preparedStatement) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);

    ResultSet resultSet;
    try
    {
      resultSet = preparedStatement.executeQuery();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return resultSet;
  }

  /**
   * @param connection Connection
   * @param sqlString String
   * @return PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static PreparedStatement createPreparedStatement(Connection connection, String sqlString) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(sqlString != null);

    PreparedStatement preparedStatement = null;
    try
    {
      preparedStatement = connection.prepareStatement(sqlString);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return preparedStatement;
  }

  /**
   * @param connection Connection
   * @param sqlString String
   * @param statementProperty int
   * @return PreparedStatement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static PreparedStatement createPreparedStatement(Connection connection, String sqlString, int statementProperty) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(sqlString != null);

    PreparedStatement preparedStatement = null;
    try
    {
      preparedStatement = connection.prepareStatement(sqlString, statementProperty);
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return preparedStatement;
  }

  /**
   * @param connection Connection
   * @return Statement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static Statement createStatement(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);

    Statement statement = null;
    try
    {
      statement = connection.createStatement();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return statement;
  }

  /**
   * @param connection Connection
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void commit(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);

    try
    {
      connection.commit();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param connection Connection
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void rollBack(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);

    try
    {
      connection.rollback();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param statement Statement
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static void closeStatement(Statement statement) throws DatastoreException
  {
    Assert.expect(statement != null);

    try
    {
      statement.close();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param resultSet ResultSet
   * @param columnName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public static int getIntResultSetData(ResultSet resultSet, String columnName) throws DatastoreException
  {
    Assert.expect(resultSet != null);
    Assert.expect(columnName != null);

    int value = 0;
    try
    {
      if (resultSet.next())
      {
        value = resultSet.getInt(columnName);
      }
      resultSet.close();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return value;
  }
  
  /**
   * This function checks if a table exists in database.
   * @param connection
   * @param tableName
   * 
   * @author Cheah Lee Herng
   */
  public static boolean isTableExists(Connection connection, String tableName) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(tableName != null);
    
    boolean isTableExists = false;
    try
    {
      DatabaseMetaData databaseMetaData = connection.getMetaData();
      ResultSet tableResultSet = databaseMetaData.getTables(null, null, null, new String[] { "TABLE" });
      while (tableResultSet.next())
      {
        String currentTableName = tableResultSet.getString("TABLE_NAME");
        if (currentTableName.equalsIgnoreCase(tableName))
        {
          isTableExists = true;
          break;
        }
      }
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    
    return isTableExists;
  }
}
