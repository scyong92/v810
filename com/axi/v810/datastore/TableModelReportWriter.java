package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: TableModelReportWriter</p>
 *
 * <p>Description: Creates a report file from data in a table model. Various methods allow
 * different delimiters between data values.</p>
 * <p>The only type supported at this release is a
 * comma-separated-value (.csv) file. This is designed to be easily readable by Excel and
 * customer data manipulation programs.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class TableModelReportWriter
{
  String csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");


  /**
   * @author George Booth
   */
  public TableModelReportWriter()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public TableModelReportWriter(String csvDelimiter)
  {
    this.csvDelimiter = csvDelimiter;
  }
  
  /**
   * @author George Booth
   */
  public void writeCSVFile(String reportFileName, javax.swing.table.DefaultTableModel tableModel) throws IOException
  {
    Assert.expect(reportFileName != null);
    Assert.expect(tableModel != null);

    writeFile(csvDelimiter, reportFileName, null, tableModel);
  }

  /**
   * @author George Booth
   */
  public void writeCSVFile(String reportFileName, String title, javax.swing.table.DefaultTableModel tableModel) throws IOException
  {
    Assert.expect(reportFileName != null);
    Assert.expect(tableModel != null);
    Assert.expect(title != null);

    writeFile(csvDelimiter, reportFileName, title, tableModel);
  }

  /**
   * @author George Booth
   */
  void writeFile(String delimiter, String reportFileName, String title, javax.swing.table.DefaultTableModel tableModel) throws IOException
  {
    // do not append
    FileWriterUtil fileWriter = new FileWriterUtil(reportFileName, false);
    fileWriter.open();

    // report title and date
    Date date = new Date();
    if (title != null)
    {
      fileWriter.writeln(title + delimiter + date);
    }
    else
    {
      fileWriter.writeln(delimiter + delimiter + date);
    }
    fileWriter.writeln();

    // column header line
    StringBuffer headerLine = new StringBuffer();
    for (int col = 0; col < tableModel.getColumnCount(); col++)
    {
      if (col > 0)
      {
        headerLine.append(delimiter);
      }
      headerLine.append(tableModel.getColumnName(col));
    }
    fileWriter.writeln(headerLine.toString());

    // table data
    for (int row = 0; row < tableModel.getRowCount(); row++)
    {
      StringBuffer reportLine = new StringBuffer();
      for (int col = 0; col < tableModel.getColumnCount(); col++)
      {
        if (col > 0)
        {
          reportLine.append(delimiter);
        }
        reportLine.append(tableModel.getValueAt(row, col));
      }
      fileWriter.writeln(reportLine.toString());
    }

    fileWriter.close();
  }
}
