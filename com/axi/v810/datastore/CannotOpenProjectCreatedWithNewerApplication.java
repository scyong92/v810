package com.axi.v810.datastore;

import com.axi.util.*;

/**
 *
 * @author Rex Shang
 */
public class CannotOpenProjectCreatedWithNewerApplication extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public CannotOpenProjectCreatedWithNewerApplication(String projName, Double projCreationVersion, String currentVersion)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_OPEN_PROJECT_CREATED_WITH_NEWER_APPLICATION_KEY",
      new Object[]{projName, projCreationVersion.toString(), currentVersion}));
    Assert.expect(projName != null);
    Assert.expect(projCreationVersion != null);
    Assert.expect(currentVersion != null);
  }
}
