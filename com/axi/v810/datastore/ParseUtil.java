package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * Performs parsing and file reading functions for the datastore layer.  This class
 * is here mostly to convert non-datastore Exceptions to datastore Exceptions
 * for these commonly made calls.
 *
 * @author Keith Lee
 */
public class ParseUtil
{
  /**
   * Read the next line from the BufferedReader.
   *
   * @param br - BufferedReader to read from.
   * @param filename - the name of the file to read from.
   * @return String holding the line.  Null if no more lines to read.
   * @author Keith Lee
   */
  public static String readNextLine(BufferedReader br, String filename) throws DatastoreException
  {
    Assert.expect(br != null);
    Assert.expect(filename != null);

    String line = null;
    try
    {
      line = br.readLine();
    }
    catch(IOException e)
    {
      DatastoreException dex = new DatastoreException(filename);
      dex.initCause(e);
      throw dex;
    }
    return line;
  }

  /**
   * Return the next token from a line.  Return null if no more tokens.
   *
   * @return String containing token or null if no more tokens.
   * @author Keith Lee
   */
  public static String getNextToken(StringTokenizer st)
  {
    Assert.expect(st != null);

    String token = null;
    if (st.hasMoreTokens())
    {
      token = st.nextToken().intern();
    }
    return token;
  }

  /**
   * Change the delimeters and return a token based on the new delimters
   * @return String containing token or null if no more tokens.
   * @author George A. David
   */
   public static String getNextToken(StringTokenizer st, String newDelimiters)
   {
     Assert.expect(st != null);
     Assert.expect(newDelimiters != null);

     String token = null;
     if(st.hasMoreTokens())
     {
       token = st.nextToken(newDelimiters).intern();
     }
     return token;
   }

  /**
   * Opens a file and returns a LineNumberReader representing the file.
   *
   * @param filename the String holding the name of the file to open.
   * @return LineNumberReader initialize to the opened file.
   * @author Keith Lee
   */
  public static LineNumberReader openFile(String filename) throws FileNotFoundDatastoreException
  {
    Assert.expect(filename != null);

    LineNumberReader is;
    try
    {
      FileReader fr = new FileReader(filename);
      is = new LineNumberReader(fr);
    }
    catch(FileNotFoundException e)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(filename);
      dex.initCause(e);
      throw dex;
    }

    return is;
  }

  /**
   * Opens a file and returns a LineNumberReader representing the file with
   * the specifed buffer size
   *
   * @param filename the name of the file to open.
   * @param bufferSize the size of the buffer
   * @return LineNumberReader initialize to the opened file.
   * @author George A. David
   */
  public static LineNumberReader openFile(String filename, int bufferSize ) throws FileNotFoundDatastoreException
  {
    Assert.expect(filename != null);

    LineNumberReader is;
    try
    {
      FileReader fr = new FileReader(filename);
      is = new LineNumberReader(fr, bufferSize);
    }
    catch(FileNotFoundException e)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(filename);
      dex.initCause(e);
      throw dex;
    }

    return is;
  }

 /**
   * @return the next quote delimted token
   * @author George A. David
   */
  public static String getNextQuoteDelimitedToken(QuoteTokenizer qt)
  {
    Assert.expect(qt != null);

    String token = null;
    if(qt.hasMoreTokens())
    {
      token = qt.nextToken().intern();
    }
    return token;
  }
}
