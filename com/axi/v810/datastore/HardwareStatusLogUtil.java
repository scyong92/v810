package com.axi.v810.datastore;

import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is used to create log for command from iae to camera - setScan
 * @author swee-yee.wong
 */
public class HardwareStatusLogUtil 
{
  private static HardwareStatusLogUtil _instance;
  private FileLoggerAxi _logUtility;
  private String _logFileNameFullPath;
  private boolean _firstLog = true;
  private static final int _TWENTY_MEGABYTES = 20*1024*1024;
  
  /**
   * @author swee-yee.wong
   */
  private HardwareStatusLogUtil()
  {
    //do nothing
  }

   /**
   * @author swee-yee.wong
   */
  public static synchronized HardwareStatusLogUtil getInstance()
  {
    if (_instance == null)
      _instance = new HardwareStatusLogUtil();

    return _instance;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void log(String data) throws XrayTesterException
  {
    _logFileNameFullPath = FileName.getHardwareStatusLogFullPath();
    if (_logFileNameFullPath.isEmpty() == false)
    {
      _logUtility = new FileLoggerAxi(_logFileNameFullPath, _TWENTY_MEGABYTES);
      if(_firstLog == true)
      {
        _logUtility.append("Version: " + Version.getVersion() + " " + XrayTester.getInstance().getMachineDescription());
        _firstLog = false;
      }
      _logUtility.append(data);
    }
  }

}

