package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
public class OutOfMemoryDatastoreException extends DatastoreException
{
  public OutOfMemoryDatastoreException()
  {
    super(new LocalizedString("DS_ERROR_LOW_MEMORY_KEY", new Object[]{}));
  }
}
