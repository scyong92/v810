package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is cannot be read for some reason.
 * @author Bill Darbie
 */
public class CannotReadDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the name of the file that cannot be read to for some reason
   * @author Bill Darbie
   */
  public CannotReadDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_READ_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
