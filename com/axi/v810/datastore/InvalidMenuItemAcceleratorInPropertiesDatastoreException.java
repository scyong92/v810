package com.axi.v810.datastore;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * This exception is thrown when a properties file has a menu accelerator that is a duplicate
 * of another menu in the menubar.
 * @author George Booth
 */
public class InvalidMenuItemAcceleratorInPropertiesDatastoreException extends DatastoreException
{
  /**
   * @author George Booth
   */
  public InvalidMenuItemAcceleratorInPropertiesDatastoreException(String menu1, String shortcut)
  {
    super(new LocalizedString("GUI_ERROR_INVALID_ACCELERATOR_KEY",
                              new Object[]{menu1, shortcut,
                              FileName.getSoftwareConfigFullPath()}));

    Assert.expect(menu1 != null);
    Assert.expect(shortcut != null);
  }
}
