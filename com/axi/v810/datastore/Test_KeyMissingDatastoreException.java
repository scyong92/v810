package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_KeyMissingDatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_KeyMissingDatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    KeyMissingDatastoreException ex = new KeyMissingDatastoreException("key", "fileName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
