package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to backup a project.
 *
 * @author George A. David
 */
public class FailedBackingUpProjectDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while backing it up
   * @author George A. David
   */
  public FailedBackingUpProjectDatastoreException(String projectName, String projectDir)
  {
    super(new LocalizedString("DS_ERROR_FAILED_BACKING_UP_PROJECT_KEY", new Object[]{projectName, projectDir}));
    Assert.expect(projectName != null);
    Assert.expect(projectDir != null);
  }
}
