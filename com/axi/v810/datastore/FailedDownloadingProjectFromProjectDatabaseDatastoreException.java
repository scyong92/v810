package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to download a project from the repository.
 *
 * @author George A. David
 */
public class FailedDownloadingProjectFromProjectDatabaseDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while downloading it from the repository
   * @author George A. David
   */
  public FailedDownloadingProjectFromProjectDatabaseDatastoreException(String projectName, String repositoryPath)
  {
    super(new LocalizedString("DS_ERROR_FAILED_DOWNLOADING_PROJECT_FROM_PROJECT_DATABASE_KEY", new Object[]{projectName, repositoryPath}));
    Assert.expect(projectName != null);
    Assert.expect(repositoryPath != null);
  }
}
