package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * The file will read in the serialNumMapping.config file to determine how
 * to map serial numbers to projects.
 *
 *
 * An example of the file syntax is:
 *
 * #######################################################################
 * # FILE: serialNumMapping.config
 * # PURPOSE: To store all serial number sequence mapping with
 * #          barcode readers and board name.
 * #          To store barcode reader configuration used in production.
 * #
 * ########################################################################
 *
 * @author siew-yeng.phang
 */
public class SerialNumberMappingSettingsReader
{
  private static SerialNumberMappingSettingsReader _instance;
  
  private String _projectName = "";
  private List<SerialNumberMappingData> _serialNumberMappingData;
  private String _barcodeReaderConfigurationNameUsed;
  private String _fileName = "";
  Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");
  private Pattern _serialNumberMappingPattern = Pattern.compile("([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)$");
  
  /**
   * @author Phang Siew Yeng
   */
  public static synchronized SerialNumberMappingSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new SerialNumberMappingSettingsReader();

    return _instance;
  }

  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingSettingsReader()
  {
    _serialNumberMappingData = new ArrayList<SerialNumberMappingData>();
  }
    
  /*
   * @author Phang Siew Yeng
   */
  public void read(String projectName) throws DatastoreException
  {
    _serialNumberMappingData.clear();
    _projectName = projectName;
    _fileName = FileName.getSerialNumberMappingSettingsFullPath(projectName);

    FileReader fr = null;
    LineNumberReader is = null;
    try
    {
      fr = new FileReader(_fileName);

      boolean barcodeReaderConfigNameRead = false;
      String line = null;
      String barcodeReaderId = "";
      String boardName = "";
      String scanSequence = "";
      
      is = new LineNumberReader(fr);
      line = is.readLine();
      
      do
      {
        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
        {
          line = is.readLine();
          continue;
        }
        
        if(barcodeReaderConfigNameRead == false)
        {
          _barcodeReaderConfigurationNameUsed = line.trim();
          barcodeReaderConfigNameRead = true;
          continue;
        }
        
        matcher = _serialNumberMappingPattern.matcher(line);
        if(matcher.matches())
        {
          boardName = matcher.group(1);
          barcodeReaderId = matcher.group(2);
          scanSequence = matcher.group(3);
          
          SerialNumberMappingData data = new SerialNumberMappingData(projectName,
                                                                    boardName,
                                                                    Integer.parseInt(barcodeReaderId),
                                                                    Integer.parseInt(scanSequence));
          _serialNumberMappingData.add(data);
        }
        
        line = is.readLine();
      }
      while(line != null);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(_fileName);
      ex.initCause(fnfe);
      throw ex;
    }
    catch (Exception ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, is.getLineNumber());
      dex.initCause(ex);
      throw dex;
    }
    finally
    {      
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
  }
  
  /*
   * Get Serial Number Mapping Data from serialNumberMapping.settings file.
   * @author Phang Siew Yeng
   */
  public List<SerialNumberMappingData> getSerialNumberMappingData(String projectName) throws DatastoreException
  {    
    read(projectName);
    
    return new ArrayList(_serialNumberMappingData);
  }
  
  public String getBarcodeReaderConfigurationNameUsed() 
  {    
    return _barcodeReaderConfigurationNameUsed;
  }
}
