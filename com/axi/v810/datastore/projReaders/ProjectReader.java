package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class will read in the project cad and settings information from
 * text files.
 * @author Bill Darbie
 */
public class ProjectReader
{
  private static ProjectReader _instance;

  private PanelReader _panelReader;
  private BoardTypeReader _boardTypeReader;
  private LandPatternReader _landPatternReader;
  private CompPackageReader _compPackageReader;

  private ProjectSettingsReader _projectSettingsReader;
  private PanelSettingsReader _panelSettingsReader;
  private BoardSettingsReader _boardSettingsReader;
  private SubtypeSettingsReader _subtypeSettingsReader;
  private SubtypeAdvanceSettingsReader _subtypeAdvanceSettingsReader;
  private BoardTypeSettingsReader _boardTypeSettingsReader;
  private ComponentNoLoadSettingsReader _componentNoLoadSettingsReader;
  private AlignmentTransformSettingsReader _alignmentTransformSettingsReader;
  private CompPackageSettingsReader _compPackageSettingsReader;
  private PanelSurfaceMapSettingsReader _panelSurfaceMapSettingsReader;
  //private PanelAlignmentSurfaceMapSettingsReader _panelAlignmentSurfaceMapSettingsReader;
  private PanelMeshSettingsReader _panelMeshSettingsReader;
  private BoardSurfaceMapSettingsReader _boardSurfaceMapSettingsReader;
  //private BoardAlignmentSurfaceMapSettingsReader _boardAlignmentSurfaceMapSettingsReader;
  private BoardMeshSettingsReader _boardMeshSettingsReader;
  private PspSettingsReader _pspSettingsReader;
  private PshSettingsReader _pshSettingsReader;
  private AutoExposureSettingsReader _autoExposureSettingsReader;

  //Kee Chin Seong
  private PanelFocusRegionSettingsReader _panelFocusRegionSettingsReader;

  // group 1 - all leading spaces
  // group 2 - all characters before the first # with all leading and trailing spaces removed
  // group 3 - all trailing spaces before the first #
  // group 4 - all characters after the first # including the #
  // note that \# is not treated as a comment character
  private Pattern _commentPattern = Pattern.compile("^(\\s*)((\\\\#|[^#])*+)(\\s*)(#.*)*$");
  //                                                                      ^
  // Notice the + marked by the line above, this is there only to prevent stack overflow errors
  // it turns out that (x|y)* is very expensive and will cause stack overflow for fairly small strings (5000 characters)
  // adding the + is a way to get rid of the stack overflow - found on Sun's bug database, bug ID: 5050507


  private LineNumberReader _is = null;
  private String _fileName;
  private String _line;
  private boolean _hasNextLineWasCalled = false;
  private boolean _prevHasNextLineReturnValue = false;

  private boolean _fastLoadToggle = true;
  private boolean _loadWasFast = true;

  private ProgramGeneration _programGeneration;
  private boolean _displayTimes = false;

  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private PackageOnPackageReader _packageOnPackageReader;
  
  /**
   * @author Bill Darbie
   */
  public static synchronized ProjectReader getInstance()
  {
    if (_instance == null)
    {
      _instance = new ProjectReader();
      _instance._displayTimes = Config.getInstance().isDeveloperDebugModeOn();
    }
    
    return _instance;
  }
  
  /**
   * @author Anthony Fong
   */
  public BoardTypeReader  getBoardTypeReader()
  {
    return _boardTypeReader;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public PackageOnPackageReader getPackageOnPackageReader()
  {
    return _packageOnPackageReader;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectReader()
  {
    _panelReader = PanelReader.getInstance();
    _boardTypeReader = BoardTypeReader.getInstance();
    _landPatternReader = LandPatternReader.getInstance();
    _compPackageReader = CompPackageReader.getInstance();

    _projectSettingsReader = ProjectSettingsReader.getInstance();
    _boardSettingsReader = BoardSettingsReader.getInstance();
    _panelSettingsReader = PanelSettingsReader.getInstance();
    _subtypeSettingsReader = SubtypeSettingsReader.getInstance();
    _subtypeAdvanceSettingsReader = SubtypeAdvanceSettingsReader.getInstance();
    _boardTypeSettingsReader = BoardTypeSettingsReader.getInstance();
    _componentNoLoadSettingsReader = ComponentNoLoadSettingsReader.getInstance();
    _alignmentTransformSettingsReader = AlignmentTransformSettingsReader.getInstance();
    _compPackageSettingsReader = CompPackageSettingsReader.getInstance();
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
        _panelSurfaceMapSettingsReader = PanelSurfaceMapSettingsReader.getInstance();
        //_panelAlignmentSurfaceMapSettingsReader = PanelAlignmentSurfaceMapSettingsReader.getInstance();
        _panelMeshSettingsReader = PanelMeshSettingsReader.getInstance();
        _boardSurfaceMapSettingsReader = BoardSurfaceMapSettingsReader.getInstance();
        //_boardAlignmentSurfaceMapSettingsReader = BoardAlignmentSurfaceMapSettingsReader.getInstance();
        _boardMeshSettingsReader = BoardMeshSettingsReader.getInstance();
        _pspSettingsReader = PspSettingsReader.getInstance();
    }
    
    _autoExposureSettingsReader = AutoExposureSettingsReader.getInstance();
    //Kee Chin Seong
    _panelFocusRegionSettingsReader = PanelFocusRegionSettingsReader.getInstance();
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageReader = PackageOnPackageReader.getInstance();

	//Siew Yeng - XCR-3781
    _pshSettingsReader = PshSettingsReader.getInstance();

    _programGeneration = ProgramGeneration.getInstance();
  }

  /**
   * @author Roy Williams
   */
  private boolean shouldSerializedProjectFileBeLoaded(String projectName)
  {
    Assert.expect(projectName != null);

    boolean projectFileIsOk = true;
    File projectFile = new File(FileName.getProjectSerializedFullPath(projectName));
    if (projectFile.exists() == false)
      projectFileIsOk = false;

    // clean up any unused .project files.
    String projectDirectoryName = Directory.getProjectDir(projectName);
    String desiredProjectName = FileName.getProjectSerializedNameWithExtension(projectName);
    File projectDir = new File(projectDirectoryName);
    String[] projectFiles = projectDir.list(new FilenameFilter()
    {
      public boolean accept(File dir, String name)
      {
        Assert.expect(dir != null);
        Assert.expect(name != null);

        if (name.endsWith(FileName.getProjectFileExtension()))
          return true;

        return false;
      }
    });
    for (int i=0; i< projectFiles.length; i++)
    {
      String str = projectFiles[i];
      if (str.equals(desiredProjectName) == false)
      {
        File tempFile = new File(projectDirectoryName + File.separator + str);
        if (tempFile.exists())
          tempFile.delete();
      }
    }
    return projectFileIsOk;
  }

  /**
   * @author Bill Darbie
   */
  private boolean shouldTheSerializedFileBeLoaded(String projectName)
  {
    Assert.expect(projectName != null);

    // if the serialized .project file does not exist then do not try to load it
    boolean projectFileIsOk = shouldSerializedProjectFileBeLoaded(projectName);
    if (projectFileIsOk == false)
      return projectFileIsOk;

    // if the subtype settings file is not the current version then do not try to load the serialized file
    if (_subtypeSettingsReader.isCurrentVersion(projectName) == false)
      return false;

    if (_subtypeAdvanceSettingsReader.isCurrentVersion(projectName) == false)
      return false;

    // if the project settings file is not the current version then do not load the serialized file
    if (_projectSettingsReader.isCurrentVersion(projectName) == false)
      return false;
    
    //XCR-3333, Alignment point is not clear after recipe conversion if recipe is from same software version
    //XCR-3331, System crash when run alignment using converted recipe
    try
    {
      //if the recipe system type and machine system type is not the same, it should reloaded again from setting
      if(_projectSettingsReader.readSystemType(projectName).equals(com.axi.v810.hardware.XrayTester.getSystemType()) == false)
        return false;
    }
    catch (DatastoreException ex)
    {
      //unable to read some of the file, it might be corrupted or wrong format
      return false;
    }
    
    // figure out the most recent modification time for any .cad or .settings file
    String cadExt = FileName.getCadFileExtension().toLowerCase();
    String settingsExt = FileName.getSettingsFileExtension().toLowerCase();

    Set<String> alignmentTransformFileSet = new HashSet<String>();
    int version = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion();
    while (version > 0)
    {
      String fileName = FileName.getProjectAlignmentTransformSettingsFullPath(projectName, version);
      if (FileUtilAxi.exists(fileName))
      {
        alignmentTransformFileSet.add(fileName.toLowerCase());
        break;
      }
      --version;
    }

    List<String> files = new ArrayList<String>();
    String projectDir = Directory.getProjectDir(projectName);
    for (String file : FileUtilAxi.listAllFilesFullPathInDirectory(projectDir))
    {
      file = file.toLowerCase();
      if (file.endsWith(cadExt) || (file.endsWith(settingsExt)))
      {
        // the alignment transform file gets updated on every inspection, so do not look at its time stamp
        if (alignmentTransformFileSet.contains(file) == false)
          files.add(file);
      }
    }

    long mostRecentCadOrSettingsFileTime = 0;
    for (String file : files)
    {
      File theFile = new File(file);
      long modified = theFile.lastModified();
      if (modified > mostRecentCadOrSettingsFileTime)
        mostRecentCadOrSettingsFileTime = modified;
    }


    // if a .cad or .settings file was modified then do not load the serialized .project file
    File projectFile = new File(FileName.getProjectSerializedFullPath(projectName));
    long serializedFileModifyTime = projectFile.lastModified();
    if (serializedFileModifyTime < mostRecentCadOrSettingsFileTime)
      return false;

    // wei chin added (28/12/2009 Fix for CR1027)
    // if a software config file was modified then do not load the serialized .project file
//    File configFile = new File(FileName.getSoftwareConfigFullPath());
//    long configFileModifyTime = configFile.lastModified();
//    if (configFileModifyTime > serializedFileModifyTime)
//      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean wasFastLoad()
  {
    return _loadWasFast;
  }

  /**
   * Load the project from disk.
   * @author Bill Darbie
   * Edited By Kee Chin Seong - XCR-3158 Assert when set all component to no load and save the recipe as new recipe
   */
  public Project load(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    Project project = null;
    //XCR-2069 - Anthony Fong - Fiducial Setting Inconsistent
    BoardTypeReader.getInstance().resetFiducialSettingInconsistent();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("\n\nBEGIN LOAD of project: " + projectName);
    TimerUtil totalTimer = new TimerUtil();
    totalTimer.start();

    if (shouldTheSerializedFileBeLoaded(projectName))
    {
      if (UnitTest.unitTesting())
         _fastLoadToggle = true;

      // try to load from the serialized file first
      _loadWasFast = true;
// wpd - if you want to toggle between fast load (serialized file) and slow load (text files) remove
//       the comments on the 2 lines below.  This is useful for exercising both paths of the code
//      if (_fastLoadToggle)
//      {
        if ((_displayTimes) &&(UnitTest.unitTesting() == false))
        {
          System.out.println("\n#####################################################################################");
          System.out.println("wpd FAST serialized load this time.");
          System.out.println("\n");
        }
        TimerUtil timer = new TimerUtil();
        timer.start();
        project = loadSerializedFile(projectName);

        if (project != null)
        {
          project.setName(projectName);
          
          // XCR-3158 Assert when set all component to no load and save the recipe as new recipe
          project.setCurrentLoadedProject(project);
          // on a serialized load not all of the test program gets pulled in
          // because not all of it was save, so let's initialize it
          _programGeneration.intializeTestProgramAfterProjectLoad(project.getTestProgram());
          //project.verifyProjectSolderThicknessFilesCompatibility();
        }

        timer.stop();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          long totalTime = timer.getElapsedTimeInMillis();
          System.out.println("\nwpd Project.load() from serialized file time in millis: " + totalTime + "\n");
        }

//        _fastLoadToggle = false;
//      }
//      else
//      {
//        _fastLoadToggle = true;
//      }
    }

    if (project == null)
    {
      // the fast serialized file load did not work, do the text file load
      if ((_displayTimes) && (UnitTest.unitTesting() == false))
      {
        System.out.println("\n#####################################################################################");
        System.out.println("wpd SLOW text file load this time.");
        System.out.println("\n");
      }
      _loadWasFast = false;
      TimerUtil timer = new TimerUtil();
      timer.reset();
      timer.start();
      project = read(projectName);
      timer.stop();

      if ((_displayTimes) && (UnitTest.unitTesting() == false))
      {
        long totalTime = timer.getElapsedTimeInMillis();
        System.out.println("\nwpd Project.load() from text files time in millis: " + totalTime + "\n");
      }

      timer.reset();
      timer.start();
      // this must be done to make sure all variables are set during the load
      // that way no ProjectEvents will be sent up after the load due to lazy assignment calls
      assignPadOrientationsPitchAndInterPadDistancesAndPadOneAndLandPatternWidthLengthAndCoord(project);
      timer.stop();
      long time = timer.getElapsedTimeInMillis();
      if ((_displayTimes) && (UnitTest.unitTesting() == false))
        System.out.println("Time to assign pad orientations, pitch and interpad distances in millis: " + time);

      totalTimer.stop();
      if ((_displayTimes) && (UnitTest.unitTesting() == false))
      {
        long totalTime = totalTimer.getElapsedTimeInMillis();
        int numPins = project.getPanel().getNumJoints();
        System.out.println("wpd - number of pins: " + numPins);
        System.out.println("wpd - pins / sec: " + (double)numPins / totalTime * 1000.0);
        System.out.println("wpd - TOTAL LOAD TIME in milliseconds: " + totalTime);
      }
    }
    
	// XCR1529, New Scan Route, khang-wah.chnee
    checkAvailabilityOfScanRouteConfig(project);

    Assert.expect(project != null);

    MemoryUtil.garbageCollect();

    return project;
  }

  /**
   * @author Bill Darbie
   */
  private void assignPadOrientationsPitchAndInterPadDistancesAndPadOneAndLandPatternWidthLengthAndCoord(Project project)
  {
    Assert.expect(project != null);
    for (LandPattern landPattern : project.getPanel().getAllLandPatterns())
      landPattern.assignAll();
  }

  /**
   * @return the Project if it can be loaded from the serialized file.  If it cannot
   * because it does not exist or for any other reason, a null is returned.
   *
   * @author Bill Darbie
   */
  private Project loadSerializedFile(String projectName)
  {
    Assert.expect(projectName != null);

    Project project = null;
    //XCR-2069 - Anthony Fong - Fiducial Setting Inconsistent
    BoardTypeReader.getInstance().resetFiducialSettingInconsistent();
    // first try to load the serialized file if it exists
    if (FileUtil.exists(FileName.getProjectSerializedFullPath(projectName)))
    {
      // ok - load the java serialized project file
      ObjectInput is = null;
      String fileName = FileName.getProjectSerializedFullPath(projectName);
      try
      {
        FileInputStream fis = new FileInputStream(fileName);
        BufferedInputStream bis = new BufferedInputStream(fis);
        is = new ObjectInputStream(bis);

        TimerUtil timer = new TimerUtil();
        timer.start();
        project = (Project)is.readObject();

        if (UnitTest.unitTesting() == false)
        {
          ProjectObservable.getInstance().setProjectState(project.getProjectState());
        }

        // disable the project state tracking while things are assigned
        // this will prevent a bunch of state change events going up to the GUI during the load
        project.getProjectState().disable();
        assignComponentReferenceToPads(project);

        // Check to see if the alignment transform text file is missing.  Blow away the alignment transforms if it is.
        // This gives us a method of clearing out old alignment transforms.
//        int version = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion();
//        String alignmentTransformFileName = null;
//        for (int i = version; i > 0; --i)
//        {
//          String theFileName = FileName.getProjectAlignmentTransformSettingsFullPath(projectName, i);
//          if (FileUtilAxi.exists(theFileName))
//          {
//            alignmentTransformFileName = theFileName;
//            break;
//          }
//        }
//        PanelSettings panelSettings = project.getPanel().getPanelSettings();
//        if (alignmentTransformFileName == null)
//        {
//          panelSettings.clearAllAlignmentTransforms();
//        }
        timer.stop();

        if ((_displayTimes) && (UnitTest.unitTesting() == false))
          System.out.println("wpd ProjectReader.load() serialized file time in millis: " + timer.getElapsedTimeInMillis());
      }
      catch(InvalidClassException ice)
      {
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: InvalidClassException while loading Project serialized file for " + projectName);
        }
      }
      catch(FileNotFoundException fnfe)
      {
        // this should not happen since we checked if the file existed before
        // we get here
        Assert.logException(fnfe);
      }
      catch(StreamCorruptedException sce)
      {
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: StreamCorruptedException while loading Project serialized file for " + projectName);
        }
        sce.printStackTrace();
      }
      catch (StackOverflowError soe)
      {
        // for now ignore this error, java has a bug about this already
        // BugID: 4152790  It has been open since June 1998.
        // if we can figure this out in the future, we can stop catching this error.
        // There is similar code in ProjectWriter as well.
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: StackOverflowError while while loading Project serialized file for : " + projectName);
        }
      }
      catch(IOException ioe)
      {
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: IOException while while loading Project serialized file for : " + projectName);
        }
        ioe.printStackTrace();
      }
      catch(ClassNotFoundException cnfe)
      {
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: ClassNotFoundException while loading Project serialized file for : " + projectName);
        }
      }
      catch (Throwable thr)
      {
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: Throwable caught while loading Project serialized file for : " + projectName);
        }
      }
      finally
      {
        try
        {
          if (is != null)
            is.close();
        }
        catch(IOException ioe)
        {
          // do nothing
        }
      }
    }

    // a null is okay here - it indicates that the serialized file could not be read
    return project;
  }

  /**
   * Do this on a serialize load so we don't get a bunch of events later when this relationship is needed
   * @author Andy Mechtenberg
   */
  private void assignComponentReferenceToPads(Project project)
  {
    Assert.expect(project != null);
    for (Component component : project.getPanel().getComponents())
    {
      for (Pad pad : component.getPads())
      {
        pad.setComponent(component);
      }
    }
  }
  
  /**
   * to copy scanroute.config to within project directory from config directory
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private void checkAvailabilityOfScanRouteConfig(Project project) throws DatastoreException
  {
    if(FileUtil.exists(project.getScanRouteConfigFullPath())==false)
    {
      try
      {
        project.setScanRouteConfigFileName(FileName.getScanRouteConfigFile());
        FileUtil.copy(FileName.getScanRouteConfigFullPath(), project.getScanRouteConfigFullPath());
        // add the scan route info into map for current project
        Config.getInstance().addScanRouteInfoToMap(project.getScanRouteConfigFullPath());
      }
      catch (Exception e)
      {
        // doing nothing
      }
    }
    else
    {
      // add the scan route info into map for current project
      try
      {
        Config.getInstance().addScanRouteInfoToMap(project.getScanRouteConfigFullPath());
      }
      catch (Exception e)
      {
        // doing nothing
      }
    }
  }

  /**
   * Read in all the project files and return the resulting Project
   * @author Bill Darbie
   */
  private Project read(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    Project project = new Project(false);
    Panel panel = new Panel();
    project.setPanel(panel);
    project.setName(projectName);

    // parse in all CAD

    // Creates LandPattern, SurfaceMountLandPatternPad, ThroughHoleLandPatternPad
    Map<String, LandPattern> landPatternNameToLandPatternMap = _landPatternReader.read(project);
    // Creates BoardType, SideBoardType, ComponentType, CompPackage, FiducialType
    Map<Integer, BoardType> boardTypeNumberToBoardTypeMap =
      _boardTypeReader.read(project, landPatternNameToLandPatternMap);
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageReader.read(project);

    // create compPackageNameToCompPackageMap
    Collection<BoardType> boardTypes = boardTypeNumberToBoardTypeMap.values();
    Map<String, CompPackage> compPackageNameToCompPackageMap = new HashMap<String, CompPackage>();
    for (BoardType boardType : boardTypes)
    {
      List<CompPackage> compPackages = boardType.getCompPackages();
      for (CompPackage compPackage : compPackages)
      {
        compPackageNameToCompPackageMap.put(compPackage.getLongName(), compPackage);
      }
    }

    // Creates PackagePin, PadType
    _compPackageReader.read(project, compPackageNameToCompPackageMap);

    // Creates Panel, Board, SideBoard, Component, Pad, Fiducial, FiducialType
    Map<Board, Integer> boardToBoardInstanceNumberMap = _panelReader.read(project, boardTypeNumberToBoardTypeMap);

    // parse in all settings

    // creates nothing, just sets Project info
    _projectSettingsReader.read(project);
    // Creates PanelSettings, AlignmentGroup
    _panelSettingsReader.read(project);

    // Creates BoardSettings
    _boardSettingsReader.read(project, boardToBoardInstanceNumberMap);
    // creates Subtype
    _subtypeSettingsReader.read(project);
    // creates SubtypeAdvance
    _subtypeAdvanceSettingsReader.read(project);
    // Creates ComponentTypeSettings, PadTypeSettings
    _boardTypeSettingsReader.read(project, boardTypeNumberToBoardTypeMap);
    // Creates nothing, just sets ComponentTypeSettings to noLoad where appropriate.
    _componentNoLoadSettingsReader.read(project);

    // creates nothing, just sets PanelSettings alignment transforms
    _alignmentTransformSettingsReader.read(project);

    // creates nothing, just sets CompPackage library path
    _compPackageSettingsReader.read(project, compPackageNameToCompPackageMap);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
        // Create PanelSurfaceMap
        _panelSurfaceMapSettingsReader.read(project); 
        
        // Create PanelAlignmentSurfaceMap
        //_panelAlignmentSurfaceMapSettingsReader.read(project);
        
        // Create PanelMesh
        _panelMeshSettingsReader.read(project);

        // Create BoardSurfaceMap
        _boardSurfaceMapSettingsReader.read(project, boardToBoardInstanceNumberMap);
        
        // Create BoardAlignmentSurfaceMap
        //_boardAlignmentSurfaceMapSettingsReader.read(project, boardToBoardInstanceNumberMap);
        
        // Create BoardMesh
        _boardMeshSettingsReader.read(project, boardToBoardInstanceNumberMap);
              
        // Create PspSettings
        _pspSettingsReader.read(project);
    }
    _autoExposureSettingsReader.read(project);
    
    _panelFocusRegionSettingsReader.read(project);
    
    //Siew Yeng - XCR-3781
    _pshSettingsReader.read(project);
    
    return project;
  }

  /**
   * @author Bill Darbie
   */
  void openFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    _fileName = fileName;

    _hasNextLineWasCalled = false;
    try
    {
      _is = new LineNumberReader(new FileReader(_fileName));
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_fileName);
      de.initCause(ex);
      throw de;
    }
  }

  /**
   * @return true if there is a next line that can be parsed
   * @author Bill Darbie
   */
  boolean hasNextLine() throws DatastoreException
  {
    // if this method was called already, do not parse again until getNextLine() is called
    if (_hasNextLineWasCalled)
      return _prevHasNextLineReturnValue;

    try
    {
      do
      {
        _line = _is.readLine();
        if (_line == null)
        {
          _prevHasNextLineReturnValue = false;
          _hasNextLineWasCalled = false;
          return false;
        }

        Matcher commentMatcher = _commentPattern.matcher(_line);
        if (commentMatcher.matches() == false)
        {
          throw new FileCorruptDatastoreException(_fileName, _is.getLineNumber());
        }
        else
        {
          // get rid of any comments
          _line = commentMatcher.group(2);
          // get rid of leading and trailing spaces
          _line = _line.trim();
          _line = _line.replaceAll("\\\\#", "#");
        }
      }
      while (_line.equals(""));
    }
    catch (IOException ioe)
    {
      closeFileDuringExceptionHandling();

      DatastoreException de = new CannotReadDatastoreException(_fileName);
      de.initCause(ioe);
      throw de;
    }

    _hasNextLineWasCalled = true;
    Assert.expect(_line != null);
    _prevHasNextLineReturnValue = true;
    return true;
  }

  /**
   * @return the next valid line that contains something parsable.  All comments and leading
   * and trailing spaces will be removed.  This method will not return blank lines.
   * @author Bill Darbie
   */
  String getNextLine() throws DatastoreException
  {
    // check that hasNextLine() was already called.  If it was not then
    // call it automatically.
    if (_hasNextLineWasCalled == false)
    {
      if (hasNextLine() == false)
      {
        // there is no next line so throw an exception
        int lineNumber = getLineNumber();
        closeFileDuringExceptionHandling();
        throw new FileCorruptDatastoreException(_fileName, lineNumber + 1);
      }
    }
    _hasNextLineWasCalled = false;

    Assert.expect(_line != null);
    return _line;
  }

  /**
   * @author Bill Darbie
   */
  int getLineNumber()
  {
    Assert.expect(_is != null);

    int lineNumber = _is.getLineNumber();
    if (_hasNextLineWasCalled)
      --lineNumber;

    return lineNumber;
  }

  /**
   * @author Bill Darbie
   */
  void closeFile() throws DatastoreException
  {
    // test that there are no more lines to be read in the file
    if (hasNextLine())
      throw new FileCorruptDatastoreException(_fileName, getLineNumber());
    if (_is != null)
    {
      try
      {
        _is.close();
      }
      catch (IOException ioe)
      {
        DatastoreException de = new DatastoreException(_fileName);
        de.initCause(ioe);
        throw de;
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  void closeFileDuringExceptionHandling()
  {
    if (_is != null)
    {
      try
      {
        _is.close();
        _is = null;
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  String readMultipleLinesOfText(String header, String footer) throws DatastoreException, BadFormatException
  {
    Assert.expect(header != null);
    Assert.expect(footer != null);

    String line = getNextLine();
    if (line.equals(header) == false)
      throw new FileCorruptDatastoreException(_fileName, getLineNumber());

    String numLinesStr = getNextLine();
    int numLines = StringUtil.convertStringToInt(numLinesStr);
    StringBuffer textBuffer = new StringBuffer();
    if (numLines > 0)
    {
      line = getNextLine();
      // replace \# with #
      line = line.replaceAll("\\\\#", "#");
      for (int i = 0; i < numLines; ++i)
      {
        // remove the leading and trailing "
        if (line.length() <= 1)
          throw new FileCorruptDatastoreException(_fileName, getLineNumber());
        line = line.substring(1, line.length() - 1);
        textBuffer.append(line + "\n");
        line = getNextLine();
      }
      // remove the last \n
      if (textBuffer.length() <= 1)
          throw new FileCorruptDatastoreException(_fileName, getLineNumber());
      textBuffer.deleteCharAt(textBuffer.length() - 1);


    }
    String footerStr = null;
    if (numLines > 0)
      footerStr = line;
    else
      footerStr = getNextLine();
    if (footerStr.equals(footer) == false)
      throw new FileCorruptDatastoreException(_fileName, getLineNumber());

    return textBuffer.toString();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void goToLastLine()
  {
    try
    {
      _is.skip(Long.MAX_VALUE);
    }
    catch(IOException ioe)
    {
      if (UnitTest.unitTesting() == false)
      {
        System.out.println("Error : " + ioe.getMessage());
      }
      ioe.printStackTrace();
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * @author Kee chin Seong 
   */
  public Map<String, Pair<String, String>> readProjectVariation(String projectName) throws DatastoreException
  {
    Map<String, Pair<String, String>> variationNameToEnabledMap = new LinkedHashMap<>();
    variationNameToEnabledMap = _componentNoLoadSettingsReader.read(projectName);
    
    return variationNameToEnabledMap;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean readProjectPreSelectVariation(String projectName) throws DatastoreException
  {
    boolean isPreSelectVariation = false;
    isPreSelectVariation = _projectSettingsReader.isPreSelectVariation(projectName);
    
    return isPreSelectVariation;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public SystemTypeEnum readProjectSystemType(String projectName) throws DatastoreException
  {
    return _projectSettingsReader.readSystemType(projectName);
  }
  
   /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh Read Project Low Magnification Setting before project
   * loaded
   */
  public String readProjectLowMagnification(String projectName) throws DatastoreException
  {
    return _projectSettingsReader.readLowMagnification(projectName);
  }
  
   /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh XCR-3589 Combo STD and XXL software GUI Read Project
   * High Magnification Setting before project loaded
   */
  public String readProjectHighMagnification(String projectName) throws DatastoreException
  {
    return _projectSettingsReader.readHighMagnification(projectName);
  }
  
  public static void reintializeDueToSystemTypeChange()
  {
    _instance = null;
  }
}


