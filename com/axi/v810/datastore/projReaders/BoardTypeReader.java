package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Creates BoardType, SideBoardType, ComponentType, CompPackage, FiducialType
 * @author Bill Darbie
 */
public class BoardTypeReader
{
  private static BoardTypeReader _instance;
  private static boolean fiducialSettingInconsistent = false;

  private ProjectReader _projectReader;
  // referenceDesignator1 side1|side2 xLocation yLocation rotation landPattern packageLongName
  private Pattern _refDesPattern = Pattern.compile("^([^\\s]+)\\s+(side1|side2)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+(.\\d+)?)\\s+([^\\s]+)\\s+([^\\s]+)$");
  // fiducialName side1|side2 xLocation yLocation width length circle|rectangle
  private Pattern _fiducialPattern = Pattern.compile("^fiducial:\\s([^\\s]+)\\s+(side1|side2)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(circle|rectangle)$");

  /**
   * XCR-2069 - Fiducial Setting Inconsistent
   * @author Anthony Fong
   */
  public void  resetFiducialSettingInconsistent()
  {
    fiducialSettingInconsistent = false;
  }
  /**
   * XCR-2069 - Fiducial Setting Inconsistent
   * @author Anthony Fong
   */
  public boolean  getFiducialSettingInconsistent()
  {
    return fiducialSettingInconsistent;
  }  
  /**
   * @author Bill Darbie
   */
  static synchronized BoardTypeReader getInstance()
  {
    if (_instance == null)
      _instance = new BoardTypeReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardTypeReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @return a Map of boardTypeName to BoardType object.
   * @author Bill Darbie
   */
  Map<Integer, BoardType> read(Project project,
                              Map<String, LandPattern> landPatternNameToLandPatternMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(landPatternNameToLandPatternMap != null);

    fiducialSettingInconsistent =false;
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    Map<Integer, BoardType> boardTypeNumberToBoardTypeMap = new HashMap<Integer, BoardType>();
    Map<String, CompPackage> compPackageNameToCompPackageMap = new HashMap<String, CompPackage>();
    init();

    Panel panel = project.getPanel();
    String projName = project.getName();
    String directoryName = Directory.getProjectDir(projName);
    Collection<String> boardTypeFiles = new ArrayList<String>();
    Collection<String> files = FileUtilAxi.listAllFilesInDirectory(directoryName);
    for (String file : files)
    {
      if (file.startsWith("boardType_") && (file.endsWith(".cad")))
        boardTypeFiles.add(file);
    }

    Set<String> filesReadSet = new HashSet<String>();
    String fileName = null;
    try
    {
      for (String boardTypeFileName : boardTypeFiles)
      {
        int boardTypeNumber = 0;
        if (boardTypeFileName.endsWith(".1.cad"))
        {
          String boardTypeNumberStr = boardTypeFileName.substring("boardType_boardType".length(), boardTypeFileName.length());
          int index = boardTypeNumberStr.lastIndexOf('.');
          Assert.expect(index != -1);
          index = boardTypeNumberStr.lastIndexOf('.', index - 1);
          Assert.expect(index != -1);
          boardTypeNumberStr = boardTypeNumberStr.substring(0, index);
          boardTypeNumber = StringUtil.convertStringToInt(boardTypeNumberStr);
        }
        else
        {
          String boardTypeNumberStr = boardTypeFileName.substring("boardType_".length(), boardTypeFileName.length());
          int index = boardTypeNumberStr.lastIndexOf('.');
          Assert.expect(index != -1);
          index = boardTypeNumberStr.lastIndexOf('.', index - 1);
          Assert.expect(index != -1);
          boardTypeNumberStr = boardTypeNumberStr.substring(0, index);
          boardTypeNumber = StringUtil.convertStringToInt(boardTypeNumberStr);
        }

        int version = FileName.getProjectBoardTypeLatestFileVersion();
        fileName = null;
        while (version > 0)
        {
          fileName = FileName.getProjectBoardTypeFullPath(projName, boardTypeNumber, version);
          if (FileUtilAxi.exists(fileName))
            break;
          --version;
        }

        // check to see if we already parsed this file
        boolean added = filesReadSet.add(fileName);
        if (added == false)
          continue;

        _projectReader.openFile(fileName);

        String boardTypeName = _projectReader.getNextLine();
        BoardType boardType = new BoardType();
        boardType.setPanel(panel);
        boardType.setName(boardTypeName);
        boardTypeNumberToBoardTypeMap.put(boardTypeNumber, boardType);

        String boardWidthInNanos = _projectReader.getNextLine();
        int widthInNanos = StringUtil.convertStringToInt(boardWidthInNanos);
        String boardLengthInNanos = _projectReader.getNextLine();
        int lengthInNanos = StringUtil.convertStringToInt(boardLengthInNanos);
        boardType.setWidthInNanoMeters(widthInNanos);
        boardType.setLengthInNanoMeters(lengthInNanos);

        SideBoardType sideBoardType1 = null;
        SideBoardType sideBoardType2 = null;
        
        boolean sideBoardType1Exist = false;
        boolean sideBoardType2Exist = false;

        String line = null;
        boolean endOfFile = true;
        while (_projectReader.hasNextLine())
        {
          line = _projectReader.getNextLine();
          Matcher matcher = _refDesPattern.matcher(line);

          if (matcher.matches() == false)
          {
            // we must have hit the first fiducial line
            endOfFile = false;
            break;
          }

          // referenceDesignator side1|side2 xLocationInNanometers yLocationInNanoMeters rotationInDegrees landPattern longPackageName
          String refDes = matcher.group(1);
          String side1OrSide2 = matcher.group(2);
          String xInNanosStr = matcher.group(3);
          int xInNanos = StringUtil.convertStringToInt(xInNanosStr);
          String yInNanosStr = matcher.group(4);
          int yInNanos = StringUtil.convertStringToInt(yInNanosStr);
          String degreesRotationStr = matcher.group(5);
          double degreesRotation = StringUtil.convertStringToDouble(degreesRotationStr);
          String landPatternName = matcher.group(7);
          String compPackageLongName = matcher.group(8);

          LandPattern landPattern = landPatternNameToLandPatternMap.get(landPatternName);
          if (landPattern == null)
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          CompPackage compPackage = compPackageNameToCompPackageMap.get(compPackageLongName);
          if (compPackage == null)
          {
            compPackage = new CompPackage();
            compPackage.setPanel(panel);
            compPackage.setLongName(compPackageLongName);
            compPackageNameToCompPackageMap.put(compPackageLongName, compPackage);
          }

          ComponentType componentType = new ComponentType();
          componentType.setPanel(panel);
          componentType.setCoordinateInNanoMeters(new BoardCoordinate(xInNanos, yInNanos));
          componentType.setDegreesRotationRelativeToBoard(degreesRotation);
          componentType.setLandPattern(landPattern);
          componentType.setCompPackage(compPackage);
          compPackage.setLandPattern(landPattern);

          if (side1OrSide2.equals("side1"))
          {
            if (sideBoardType1 == null)
            {
              sideBoardType1 = new SideBoardType();
              boardType.setSideBoardType1(sideBoardType1);
              sideBoardType1.setBoardType(boardType);
            }
            componentType.setSideBoardType(sideBoardType1);
            sideBoardType1Exist=true;
          }
          else if (side1OrSide2.equals("side2"))
          {
            if (sideBoardType2 == null)
            {
              sideBoardType2 = new SideBoardType();
              boardType.setSideBoardType2(sideBoardType2);
              sideBoardType2.setBoardType(boardType);
            }
            componentType.setSideBoardType(sideBoardType2);
            sideBoardType2Exist=true;
          }
          else
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          componentType.setReferenceDesignator(refDes);
          landPattern.addComponentType(componentType);
          compPackage.addComponentType(componentType);
        }

        while (endOfFile == false)
        {
          Matcher matcher = _fiducialPattern.matcher(line);
          if (matcher.matches() == false)
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          // fiducialName side1|side2 xLocation yLocation width length circle|rectangle
          String fiducialName = matcher.group(1);
          String side1OrSide2 = matcher.group(2);
          String xInNanosStr = matcher.group(3);
          int xInNanos = StringUtil.convertStringToInt(xInNanosStr);
          String yInNanosStr = matcher.group(4);
          int yInNanos = StringUtil.convertStringToInt(yInNanosStr);
          String widthInNanosStr = matcher.group(5);
          widthInNanos = StringUtil.convertStringToInt(widthInNanosStr);
          String lengthInNanosStr = matcher.group(6);
          lengthInNanos = StringUtil.convertStringToInt(lengthInNanosStr);
          String circleOrRect = matcher.group(7);

          FiducialType fiducialType = new FiducialType();           
          if((side1OrSide2.equals("side1") && sideBoardType1Exist)||(side1OrSide2.equals("side2") && sideBoardType2Exist))
          {      
            SideBoardType sideBoardType = null;
            if (side1OrSide2.equals("side1"))
              sideBoardType = boardType.getSideBoardType1();
            else if (side1OrSide2.equals("side2"))
              sideBoardType = boardType.getSideBoardType2();
            else
              throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

            fiducialType.setName(fiducialName);
            fiducialType.setCoordinateInNanoMeters(new PanelCoordinate(xInNanos, yInNanos));
            fiducialType.setLengthInNanoMeters(lengthInNanos);
            fiducialType.setWidthInNanoMeters(widthInNanos);

            sideBoardType.addFiducialType(fiducialType);

            if (circleOrRect.equals("circle"))
              fiducialType.setShapeEnum(ShapeEnum.CIRCLE);
            else if (circleOrRect.equals("rectangle"))
              fiducialType.setShapeEnum(ShapeEnum.RECTANGLE);
            else
              throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
          }
          else
          {
            // XCR-2069 - Anthony Fong - Fiducial Setting Inconsistent
            fiducialSettingInconsistent =true;
          }
          if (_projectReader.hasNextLine() == false)
          {
            // we have hit the end of the file
            break;
          }

          line = _projectReader.getNextLine();
        }

        _projectReader.closeFile();
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }

    return boardTypeNumberToBoardTypeMap;
  }
}
