package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kee Chin Seong
 */
public class PanelFocusRegionSettingsReader 
{
    public static String _COMMA_DELIMITER = ","; 
    
    private static PanelFocusRegionSettingsReader _instance;
    
    private String _fileName;
    
    private ProjectReader _projectReader;
    //Point StageXCoordinateInNanometers StageYCoordinateInNanometers Yes|No
    private Pattern _regionWithPanelPositionCoordinatePattern = Pattern.compile("^Region\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+FocusRegion\\s+(\\d+)\\s+(\\d+)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)$");
    //Component refDes
    private Pattern _componentRefDesPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");
    // bee-hoon.goh - Log out any errors that the recipe contained
    private ProjectErrorLogUtil _projectErrorLogUtil;
    
    /**
     * @author Kee Chin Seong
     */
    static synchronized PanelFocusRegionSettingsReader getInstance()
    {
        if (_instance == null)
          _instance = new PanelFocusRegionSettingsReader();

        return _instance;
    }
    
    /**
     * @author Kee Chin Seong
    */
    private void init()
    {
        // Do nothing
    }
    
    /**
     * @author Kee Chin Seong
    */
    void read(Project project) throws DatastoreException
    {
      Assert.expect(project != null);
        
      if (_projectReader == null)
       _projectReader = ProjectReader.getInstance();

      init();

      String projName = project.getName();
        
      _fileName = "";
      int version = FileName.getProjectPanelFocusRegionSettingsLatestFileVersion();
      while (version > 0)
      {
        _fileName = FileName.getProjectPanelFocusRegionSettingsFullPath(projName, version);
        if (FileUtilAxi.exists(_fileName))
          break;
        --version;
      }
        
      // We need to handle backward-compatibility issue.
      if (version == 1)
      {
          try
          {
            _projectReader.openFile(_fileName);
              
            if (_projectReader.hasNextLine() == false)
            {
              _projectReader.closeFile();
              return;
            }
                

            String line = _projectReader.getNextLine();   
            boolean skipLoading = false;
            while (true)
            {
              // bee-hoon.goh - reset skipLoading when reach next component
              if (skipLoading)
              {               
                if (_projectReader.hasNextLine()==false)
                  break;
                line = _projectReader.getNextLine();
                Matcher matcher1 = _regionWithPanelPositionCoordinatePattern.matcher(line);

                if (matcher1.matches() == false)
                {
                  skipLoading = false;
                }
              }
              else
              {       
                if (line == null)
                    break;
      
                // figure out which type of line we have
                int lineType = 0;
                //Matcher matcher = _regionWithPanelPositionCoordinatePattern.matcher(line);
                Matcher matcher = _componentRefDesPattern.matcher(line);
                if (matcher.matches())
                {
                    lineType = 1;
                }
                if (lineType == 0) 
                {
                    throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                }
                  
                if (lineType == 1)
                {
                   if (project.getPanel().hasComponent(matcher.group(2), matcher.group(1)))
                   {
                      skipLoading = false;
                      Component comp = project.getPanel().getComponent(matcher.group(2), matcher.group(1));

                      comp.setUseCustomFocusRegion(true);

                      int subLineType = 0;
                      line = _projectReader.getNextLine();
                      matcher = _regionWithPanelPositionCoordinatePattern.matcher(line);
                      if (matcher.matches())
                      {
                          subLineType = 1;
                      }
                      if (subLineType == 0) 
                      {
                          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                      }

                      String regionKey = "";

                      while (matcher.matches())    // read Reconstruction region and the focus region
                      {
                          if (subLineType == 1)
                          {
                              double focusRegionX = Double.parseDouble(matcher.group(5));
                              double focusRegionY = Double.parseDouble(matcher.group(6));
                              double focusRegionWidth = Double.parseDouble(matcher.group(7));
                              double focusRegionHeight = Double.parseDouble(matcher.group(8));

                              regionKey = matcher.group(1) + "_" + matcher.group(2) + "_" + matcher.group(3) + "_" + matcher.group(4);

                              FocusRegion focusRegion = new FocusRegion();

                              focusRegion.setRectangleRelativeToPanelInNanoMeters(new PanelRectangle((int)focusRegionX, (int)focusRegionY, 
                                                                                                         (int)focusRegionWidth, (int)focusRegionHeight));


                              //just to make sure that we have something to read already then we need proceed with the saving 
                              //process
                              if(regionKey != "")
                              {
                                project.saveFocusRegionIntoMap(comp, regionKey, focusRegion);
                              }

                          }

                          if(_projectReader.hasNextLine())
                          {
                            line = _projectReader.getNextLine();
                            matcher = _regionWithPanelPositionCoordinatePattern.matcher(line);
                            if (matcher.matches())
                            {
                                subLineType = 1;
                            }
                            if (subLineType == 0) 
                            {
                                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                            }
                          }
                          else
                          {
                            line = null;
                            break;
                          }
                      }
                      //comp.setInspectionRegionList(rrList);
                      //project.saveFocusRegionIntoMap(comp, rrList);
                }
                else
                {
                  skipLoading = true;
                  _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
                  try 
                  {
                    _projectErrorLogUtil.log(projName, "Cannot find panel focus region for component: ", line);
                  } 
                  catch (XrayTesterException ex) 
                  {
                    System.out.println("Could not write into recipe error log");
                  }              
                }
               }
              }
             }          
              _projectReader.closeFile();
            }
            catch (DatastoreException dex)
            {
              _projectReader.closeFileDuringExceptionHandling();
              throw dex;
            }        
            finally
            {
              init();
            }
        }              
    }
}
