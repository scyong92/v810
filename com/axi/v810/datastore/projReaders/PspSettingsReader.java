package com.axi.v810.datastore.projReaders;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspSettingsReader 
{
  private static PspSettingsReader _instance;
  private ProjectReader _projectReader;
  private EnumStringLookup _enumStringLookup;
  
  /**
   * @author Cheah Lee Herng
   */
  static synchronized PspSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new PspSettingsReader();

    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private PspSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void init()
  {
    // do nothing
  }
  
  /**
   * @author Bill Darbie
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();

    String fileName = "";
    int version = FileName.getProjectPspSettingsLatestFileVersion();
    while (version > 0)
    {
      fileName = FileName.getProjectPspSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }
    
    // We need to handle backward-compatibility issue.
    if (version == 0)
    {
        Panel panel = project.getPanel();
        PspSettings pspSettings = new PspSettings();
        panel.setPspSettings(pspSettings);
        pspSettings.setPanel(panel);
    }
    else
    {
      try
      {
        _projectReader.openFile(fileName);

        Panel panel = project.getPanel();
        PspSettings pspSettings = new PspSettings();
        panel.setPspSettings(pspSettings);
        pspSettings.setPanel(panel);

//        String displayUnits = _projectReader.getNextLine();
//        MathUtilEnum mathUtilEnum = _enumStringLookup.getMathUtilEnum(displayUnits);
//        pspSettings.setAutoPopulateDisplayUnits(mathUtilEnum);
        
        String autoPopulateSpacingInNanometersStr = _projectReader.getNextLine();
        int autoPopulateSpacingInNanometers = StringUtil.convertStringToInt(autoPopulateSpacingInNanometersStr);
        pspSettings.setAutoPopulateSpacingInNanometers(autoPopulateSpacingInNanometers);
        
        String removeRectanglesOnTopOfComponentBoolString = _projectReader.getNextLine();
        boolean removeRectanglesOnTopOfComponentBool = getBooleanValue(removeRectanglesOnTopOfComponentBoolString);
        pspSettings.setIsRemoveRectanglesOnTopOfComponent(removeRectanglesOnTopOfComponentBool);
       
        String extendRectanglesToEdgeOfBoardBoolString = _projectReader.getNextLine();
        boolean extendRectanglesToEdgeOfBoardBool = getBooleanValue(extendRectanglesToEdgeOfBoardBoolString);
        pspSettings.setIsExtendRectanglesToEdgeOfBoard(extendRectanglesToEdgeOfBoardBool);
        
        String showMeshTriangleBoolString = _projectReader.getNextLine();
        boolean showMeshTriangleBool = getBooleanValue(showMeshTriangleBoolString);
        pspSettings.setIsShowMeshTriangle(showMeshTriangleBool);
        
        String applyToAllBoardBoolString = _projectReader.getNextLine();
        boolean applyToAllBoardBool = getBooleanValue(applyToAllBoardBoolString);
        pspSettings.setIsApplyToAllBoard(applyToAllBoardBool);
        
        String automaticallyIncludeAllComponentsBoolString = _projectReader.getNextLine();
        boolean automaticallyIncludeAllComponentsBool = getBooleanValue(automaticallyIncludeAllComponentsBoolString);
        pspSettings.setIsAutomaticallyIncludeAllComponents(automaticallyIncludeAllComponentsBool);

        if (version > 1)
        {
          String useMeshTriangleBoolString = _projectReader.getNextLine();
          boolean useMeshTriangleBool = getBooleanValue(useMeshTriangleBoolString);
          pspSettings.setIsUseMeshTriangle(useMeshTriangleBool);
        }
        if (version > 2)
        {
          String currentSettingIntegerString = _projectReader.getNextLine();
          int currentSetting = Integer.valueOf(currentSettingIntegerString);
          pspSettings.setCurrentSetting(currentSetting);
        }
      }
      catch (DatastoreException dex)
      {
        _projectReader.closeFileDuringExceptionHandling();
        _projectReader = null;
        throw dex;
      }
      catch (BadFormatException bex)
      {
        FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
        dex.initCause(bex);

        _projectReader.closeFileDuringExceptionHandling();
        _projectReader = null;
        throw dex;
      }
      finally
      {
        init();

        if (_projectReader != null)
        {
          _projectReader.closeFile();
          _projectReader = null;
        }
      }
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private boolean getBooleanValue(String boolString)
  {
    if (boolString.equals("true"))
    {
      return true;
    }
    else if (boolString.equals("false"))
    {
      return false;
    }
    Assert.expect(false);
    return false;
  }
}
