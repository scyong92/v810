package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Creates nothing.
 * @author Bill Darbie
 */
class ProjectSettingsReader
{
  private static ProjectSettingsReader _instance;

  private ProjectReader _projectReader;
  private EnumStringLookup _enumStringLookup;
  private String _fileName;

  // targetCustomerName: name
  private Pattern _targetCustomerNamePattern = Pattern.compile("^\\s*targetCustomerName:(.*)$");
  // programmerName: name
  private Pattern _programmerNamePattern = Pattern.compile("^\\s*programmerName:(.*)$");

  private BooleanLock _waitForUserResponseLock = new BooleanLock();
  private boolean _proceedWithLoad = false;
  // preSelectVariation:true/false
  private Pattern _preSelectVariationKeyPattern = Pattern.compile("^(\\s*preSelectVariation:)(.*)$");
  // system type
  private Pattern _systemTypePattern = Pattern.compile("^\\s*(standard|throughput|XXL|S2EX)\\s*$");
  
  private Pattern _lowMagPattern = Pattern.compile("^\\s*(M19|M23)\\s*$");
  private Pattern _highMagPattern = Pattern.compile("^\\s*(M11|M13)\\s*$");

  /**
   * @author Bill Darbie
   */
  static synchronized ProjectSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new ProjectSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author bee-hoon.goh
   */
  public boolean isCurrentVersion(String projectName)
  {
    int fileVersion = FileName.getProjectProjectSettingsLatestFileVersion();
    _fileName = FileName.getProjectProjectSettingsFullPath(projectName, fileVersion);
    return FileUtilAxi.exists(_fileName);
  }
  
  /**
   * @author Bill Darbie
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }

    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);

      String projectMajorVersionStr = _projectReader.getNextLine();
      int projectMajorVersion = StringUtil.convertStringToPositiveInt(projectMajorVersionStr);
      String projectMinorVersionStr = _projectReader.getNextLine();
      int projectMinorVersion = StringUtil.convertStringToPositiveInt(projectMinorVersionStr);

      if (version >= 2)
      {
        String programGenerationVersionStr = _projectReader.getNextLine();
        int programGenerationVersion = StringUtil.convertStringToPositiveInt(programGenerationVersionStr);
        project.setProgramGenerationVersion(programGenerationVersion);
      }

      String projectCadXmlChecksumStr = _projectReader.getNextLine();
      long projectCadXmlChecksum = StringUtil.convertStringToLong(projectCadXmlChecksumStr);

      String projectSettingsXmlChecksumStr = _projectReader.getNextLine();
      long projectSettingsXmlChecksum = StringUtil.convertStringToLong(projectSettingsXmlChecksumStr);

      String lastModificationTimeInMillisStr = _projectReader.getNextLine();
      long lastModificationTimeInMillis = StringUtil.convertStringToLong(lastModificationTimeInMillisStr);

      String line = _projectReader.getNextLine();
      Matcher matcher = _targetCustomerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      String targetCustomerName = matcher.group(1);

      line = _projectReader.getNextLine();
      matcher = _programmerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      String programmerName = matcher.group(1);

      String displayUnits = _projectReader.getNextLine();

      project.setInitialThresholdsSetName(FileName.getInitialThresholdsDefaultFileWithoutExtension());
      if (version >= 3)
      {
        String initialThresholdSetName = _projectReader.getNextLine();
        project.saveInitialThresholdsSetName(initialThresholdSetName);
      }
      
      project.setInitialRecipeSettingSetName(FileName.getInitialRecipeDefaultSettingFileWithoutExtension());
      if (version >= 20)
      {
        String initialRecipeSetting = _projectReader.getNextLine();
        project.saveInitialRecipeSettingSetName(initialRecipeSetting);
      }

      String softwareVersionUsedForProjectCreation = _projectReader.getNextLine();

      SystemTypeEnum systemType = null;
      if(version >= 4)
      {
        String systemTypeName = _projectReader.getNextLine();
        systemType = SystemTypeEnum.getSystemType(systemTypeName);
      }
      else // anything prior to version 4 will not have a system type, they were developed using a standard system
        systemType = SystemTypeEnum.STANDARD;
      
      // XCR-2674 Recipe is corrupted message is prompted when open a recipe converted in the previous version
      // We do not want to continue load recipe if the system type is invalid or unknown - Cheah Lee Herng
      if (systemType.getName().equals(SystemTypeEnum._UNKNONWN_TYPE))
        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

      project.setSystemType(systemType);
      if(version >= 6)
      {
        String scanPathMethod = _projectReader.getNextLine();
        ScanPathMethodEnum scanPathMethodEnum = ScanPathMethodEnum.getScanPathMethodToEnumMapValue(Integer.valueOf(scanPathMethod));
        project.setScanPathMethod(scanPathMethodEnum);
      }
      else
        // Set to original method for older recipe
        project.setScanPathMethod(ScanPathMethodEnum.METHOD_ORIGINAL);

      // XCR1529, New Scan Route, khang-wah.chnee
      if(version >= 7)
      {
        String generateByNewScanRoute = _projectReader.getNextLine();
        boolean isGenerateByNewScanRoute = Boolean.valueOf(generateByNewScanRoute);
        project.setGenerateByNewScanRoute(isGenerateByNewScanRoute);
        
        String scanRouteConfigFileName = _projectReader.getNextLine();
        project.setScanRouteConfigFileName(scanRouteConfigFileName);
      }
      else
        // Set to old type scan path generation method
        project.setGenerateByNewScanRoute(false);
      
      if(version >= 8)
      {
        String enableVarDivN = _projectReader.getNextLine();
        boolean isVarDivNEnable = Boolean.valueOf(enableVarDivN);
        project.setVarDivN(isVarDivNEnable);
      }
      else
        // Set enable variable divn to false by default
        project.setVarDivN(false);
      
      if(version >= 9)
      {
        String useLargeTomoAngle = _projectReader.getNextLine();
        boolean isUseLargeTomoAngle = Boolean.valueOf(useLargeTomoAngle);
        project.setUseLargeTomoAngle(isUseLargeTomoAngle);
      }
      else
        // Set use large tomographic angle to false by default
        project.setUseLargeTomoAngle(false);
      
	  // bee-hoon.goh 
      if(version >= 10)
      {
        String generateLargeImageView = _projectReader.getNextLine();
        boolean isGenerateLargeImageView = Boolean.valueOf(generateLargeImageView);
        project.setEnableLargeImageView(isGenerateLargeImageView);
        
        String generateMultiAngleImage = _projectReader.getNextLine();
        boolean isGenerateMultiAngleImage = Boolean.valueOf(generateMultiAngleImage);
        project.setGenerateMultiAngleImage(isGenerateMultiAngleImage);
        
        String generateComponentImage = _projectReader.getNextLine();
        boolean isGenerateComponentImage = Boolean.valueOf(generateComponentImage);
        project.setGenerateComponentImage(isGenerateComponentImage);
      }
      else
      {
        project.setEnableLargeImageView(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_LARGE_IMAGE_VIEW));
        project.setGenerateMultiAngleImage(Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MULTI_ANGLE_IMAGES));
        project.setGenerateComponentImage(false);
      }
      
      if(version >= 11)
      {
        String panelExtraClearDelay = _projectReader.getNextLine();
        int panelExtraClearDelayInMiliSeconds = Integer.valueOf(panelExtraClearDelay);
        project.setPanelExtraClearDelayInMiliSeconds(panelExtraClearDelayInMiliSeconds);
      }
      else
        project.setPanelExtraClearDelayInMiliSeconds(0);
	  
      if(version >= 12)
      {
        String enlargeReconstructionRegion = _projectReader.getNextLine();
        boolean isEnlargeReconstructionRegion = Boolean.valueOf(enlargeReconstructionRegion);
        project.setEnlargeReconstructionRegion(isEnlargeReconstructionRegion);
      }
      else
         project.setEnlargeReconstructionRegion(false);
  
      //Ngie Xing
      //add project last saved low magnification and high magnifications
      if(version >= 13)
      {
        String projectLowMagnification = _projectReader.getNextLine();
        project.setProjectLowMagnification(projectLowMagnification);
        String projectHighMagnification = _projectReader.getNextLine();
        project.setProjectHighMagnification(projectHighMagnification);
      }
      else
      {
        //older versions only support 19um and 11um
        project.setProjectLowMagnification("M19");
        project.setProjectHighMagnification("M11");
      }
      
      //Swee Yee
      //add project key to enable BGA joint based threshold inspection method
      if(version >= 14)
      {
        String enableBGAJointBasedThresholdInspection = _projectReader.getNextLine();
        boolean isBGAJointBasedThresholdInspectionEnabled = Boolean.valueOf(enableBGAJointBasedThresholdInspection);
        project.setEnableBGAJointBasedThresholdInspection(isBGAJointBasedThresholdInspectionEnabled);
        
        String setBGAInspectionRegionSetTo1X1 = _projectReader.getNextLine();
        boolean isBGAInspectionRegionSetTo1X1 = Boolean.valueOf(setBGAInspectionRegionSetTo1X1);
        project.setBGAInspectionRegionTo1X1(isBGAInspectionRegionSetTo1X1);
      }
      else
      {
         project.setEnableBGAJointBasedThresholdInspection(false);
         project.setBGAInspectionRegionTo1X1(false);
      }
      
      // Kok Chun, Tan - pre-select variation
      if(version >= 15)
      {
        boolean isPreSelectVariationEnabled = false;
        matcher = _preSelectVariationKeyPattern.matcher(_projectReader.getNextLine());
        if (matcher.matches())
        {
          isPreSelectVariationEnabled = Boolean.valueOf(matcher.group(2));
        }
        project.setEnablePreSelectVariation(isPreSelectVariationEnabled);
      }
      else
      {
        project.setEnablePreSelectVariation(true);
      }
      
      //Swee Yee
      //add project key to set thickness table version
      if(version >= 16)
      {
        String thicknessTableVersionString = _projectReader.getNextLine();
        int thicknessTableVersion = Integer.valueOf(thicknessTableVersionString);
        project.setThicknessTableVersion(thicknessTableVersion);
      }
      else
      {
        project.setThicknessTableVersion(0);
      }
      
      // Kok Chun, Tan - bypass mode
      if(version >= 17)
      {
        String bypassMode = _projectReader.getNextLine();
        boolean isBypassMode = Boolean.valueOf(bypassMode);
        project.setProjectBasedBypassMode(isBypassMode);
      }
      else
      {
        project.setProjectBasedBypassMode(false);
      }
      
      if(version >= 18)
      {
        String enlargeAlignmentRegion = _projectReader.getNextLine();
        boolean isEnlargeAlignmentRegion = Boolean.valueOf(enlargeAlignmentRegion);
        project.setEnlargeAlignmentRegion(isEnlargeAlignmentRegion);
      }
      else
         project.setEnlargeAlignmentRegion(false);
      
      // Kok Chun, Tan - DRO version
      if(version >= 19)
      {
        String dynamicRangeOptimizationVersionInString = _projectReader.getNextLine();
        int dynamicRangeOptimizationVersion = Integer.valueOf(dynamicRangeOptimizationVersionInString);
        project.setDynamicRangeOptimizationVersion(dynamicRangeOptimizationVersion);
      }
      else
      {
        project.setDynamicRangeOptimizationVersion(0);
      }
	  
      if (VersionUtil.isNewerVersion(softwareVersionUsedForProjectCreation))
      {
        // We are not suppose to open project that is created with newer software.
        throw new CannotOpenProjectCreatedWithNewerApplication(projName, Double.parseDouble(softwareVersionUsedForProjectCreation), Version.getVersionNumber());
      }

      project.setName(projName);
      project.setTestProgramVersion(projectMajorVersion);
      project.setMinorVersion(projectMinorVersion);
      project.setCadXmlChecksum(projectCadXmlChecksum);
      project.setSettingsXmlChecksum(projectSettingsXmlChecksum);
      project.setLastModificationTimeInMillis(lastModificationTimeInMillis);
      MathUtilEnum mathUtilEnum = _enumStringLookup.getMathUtilEnum(displayUnits);
      project.setDisplayUnits(mathUtilEnum);
      project.setSoftwareVersionOfProjectLastSave(softwareVersionUsedForProjectCreation);
      project.setTargetCustomerName(targetCustomerName);
      project.setProgrammerName(programmerName);

      String notes = _projectReader.readMultipleLinesOfText("notesBegin", "notesEnd");
      project.setNotes(notes);

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (ValueOutOfRangeException vex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(vex);
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (IllegalArgumentException iex)
    {
      //XCR-2522, Assert when open an empty system type recipe
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName);
      dex.initCause(iex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isPreSelectVariation(String projName) throws DatastoreException
  {
    Assert.expect(projName != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();
    
    // if current project directory do not have this project, return true.(to display N/A)
//    if (FileName.getProjectNames().contains(projName) == false)
//      return true;

    init();

    boolean isPreSelectVariationEnabled = true;
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    
    // if current project directory do not have this project, return true.(to display N/A)
    if (version == 0)
      return true;

    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);
      String line = _projectReader.getNextLine();
      
      // Kok Chun, Tan - pre-select variation
      if (version >= 15)
      {
        boolean match = false;
        do
        {
          Matcher matcher = _preSelectVariationKeyPattern.matcher(line);
          if (matcher.matches())
          {
            match = true;
            isPreSelectVariationEnabled = Boolean.valueOf(matcher.group(2));
          }
          if (_projectReader.hasNextLine())
          {
            line = _projectReader.getNextLine();
          }
          else
          {
            break;
          }
        }
        while(match == false);
      }
      else
      {
        isPreSelectVariationEnabled = true;
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
      // until finish
      _projectReader.goToLastLine();
      _projectReader.closeFile();
    }
    return isPreSelectVariationEnabled;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public SystemTypeEnum readSystemType(String projName) throws DatastoreException 
  {
    Assert.expect(projName != null);
    
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();
    
    SystemTypeEnum systemTypeEnum = SystemTypeEnum.UNKNOWN;
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    
    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);
      String line = _projectReader.getNextLine();
      
      if (version >= 4)
      {
        boolean match = false;
        do
        {
          Matcher matcher = _systemTypePattern.matcher(line);
          if (matcher.matches())
          {
            match = true;            
            systemTypeEnum = SystemTypeEnum.getSystemType(line);
          }
          if (_projectReader.hasNextLine())
          {
            line = _projectReader.getNextLine();
          }
          else
          {
            break;
          }
        }
        while(match == false);
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
      // until finish
      _projectReader.goToLastLine();
      _projectReader.closeFile();
    }
    return systemTypeEnum;
  }
  
   /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh XCR-3589 Combo STD and XXL software GUI
   */
  public String readLowMagnification(String projName) throws DatastoreException
  {
    Assert.expect(projName != null);
    
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();
    
    String lowMag = "";
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    
    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);
      String line = _projectReader.getNextLine();
      
      if (version >= 4)
      {
        boolean match = false;
        do
        {
          Matcher matcher = _lowMagPattern.matcher(line);
          if (matcher.matches())
          {
            match = true;            
            lowMag = line;
          }
          if (_projectReader.hasNextLine())
          {
            line = _projectReader.getNextLine();
          }
          else
          {
            break;
          }
        }
        while(match == false);
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
      // until finish
      _projectReader.goToLastLine();
      _projectReader.closeFile();
    }
    return lowMag;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   */
  public String readHighMagnification(String projName) throws DatastoreException
  {
    Assert.expect(projName != null);
    
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();
    
    String HighMag = "";
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    
    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);
      String line = _projectReader.getNextLine();
      
      if (version >= 4)
      {
        boolean match = false;
        do
        {
          Matcher matcher = _highMagPattern.matcher(line);
          if (matcher.matches())
          {
            match = true;            
            HighMag = line;
          }
          if (_projectReader.hasNextLine())
          {
            line = _projectReader.getNextLine();
          }
          else
          {
            break;
          }
        }
        while(match == false);
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
      // until finish
      _projectReader.goToLastLine();
      _projectReader.closeFile();
    }
    return HighMag;
  }
}
