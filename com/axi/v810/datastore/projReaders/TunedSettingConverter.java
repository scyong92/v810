package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class knows how to convert a single tuned algorithm setting.
 *
 * @author Peter Esbensen
 */
abstract class TunedSettingConverter
{
  // this is the EnumStringLookup string for the old setting that we want to convert
  private String _algorithmSettingEnumString;

  // This is used purely for doing sanity checks in EnumStringLookup to make sure that nobody is using an old obsolete string.
  // See TunedSettingConversionEngine for those sanity checks.
  // If you are removing an old setting and converting it to a new one, you should mark this true.
  // If you are converting a setting's value but keeping the setting itself around, then mark this false.
  private boolean _oldSettingNoLongerExists;

  /**
   * @author Peter Esbensen
   */
  TunedSettingConverter(String algorithmSettingEnumString,
                        boolean oldSettingNoLongerExists)
  {
    Assert.expect(algorithmSettingEnumString != null);
    _algorithmSettingEnumString = algorithmSettingEnumString;
    _oldSettingNoLongerExists = oldSettingNoLongerExists;
  }

  /**
   * @author Peter Esbensen
   */
  boolean oldSettingNoLongerExists()
  {
    return _oldSettingNoLongerExists;
  }

  /**
   * @author Peter Esbensen
   */
  String getAlgorithmSettingEnumString()
  {
    Assert.expect(_algorithmSettingEnumString != null);
    return _algorithmSettingEnumString;
  }

  /**
   * Return the new setting enum and value
   * @author Peter Esbensen
   */
  abstract Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(Serializable obsoleteSettingValue,
                                                                                 Subtype subtype);

  /**
   * Return a list of new settings that will use previous value
   * @author George Booth
   */
  abstract ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList();
}
