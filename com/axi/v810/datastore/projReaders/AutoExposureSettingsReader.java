package com.axi.v810.datastore.projReaders;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;
import java.util.*;
import java.util.regex.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class AutoExposureSettingsReader
{
  private static AutoExposureSettingsReader _instance;
  private ProjectReader _projectReader;
  private String _fileName;
  private EnumStringLookup _enumStringLookup;
  private Pattern _settingPattern1 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)$");
  
  /**
   * @author Bill Darbie
   */
  static synchronized AutoExposureSettingsReader getInstance()
  {
    if(_instance == null)
    {
      _instance = new AutoExposureSettingsReader();
    }

    return _instance;
  }

  /**
   * @author sham
   */
  private AutoExposureSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }
  
  /**
   * author sham
   * @param projectName
   * @return boolean
   */
  public boolean isCurrentVersion(String projectName)
  {
    Assert.expect(projectName != null);

    int fileVersion = FileName.getProjectAutoExposureSettingsLatestFileVersion();
    _fileName = FileName.getProjectAutoExposureSettingsFullPath(projectName,fileVersion);

    return FileUtilAxi.exists(_fileName);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if(_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    String projName = project.getName();
    int fileVersion = FileName.getProjectAutoExposureSettingsLatestFileVersion();
    while (fileVersion > 0)
    {
      _fileName = FileName.getProjectAutoExposureSettingsFullPath(projName,fileVersion);
      if(FileUtilAxi.exists(_fileName))
      {
        break;
      }
      --fileVersion;
    }
    
    if(fileVersion < 1)
      return;
    
    try
    {
      _projectReader.openFile(_fileName);
      String line = _projectReader.getNextLine();
      
      while(true)
      {
        String subtypeNameStr = null;
        String stageSpeedStr = null;
        String enabledCameras = null;
        
        Matcher matcher = null;
        
        matcher = _settingPattern1.matcher(line);
        matcher.matches();
        subtypeNameStr = matcher.group(1);
        stageSpeedStr = matcher.group(2);
        enabledCameras = matcher.group(3);
        
        Subtype subtype = project.getPanel().getSubtype(subtypeNameStr);
        SubtypeAdvanceSettings subtypeAdvanceSettings = subtype.getSubtypeAdvanceSettings();
        
        List<StageSpeedEnum> speedSettingList = new ArrayList<StageSpeedEnum>();
        List<Integer> enabledCamerasList = new ArrayList<Integer>();
        if (stageSpeedStr != null)
        {
          for (String speedString : stageSpeedStr.split(","))
            speedSettingList.add(_enumStringLookup.getstageSpeedEnum(speedString));
        }
        
        if(enabledCameras != null)
        {
          for (String enableCameraId : enabledCameras.split(","))
            enabledCamerasList.add(Integer.parseInt(enableCameraId));
        }
          
        subtypeAdvanceSettings.setStageSpeedEnum(speedSettingList);
        subtypeAdvanceSettings.setEnabledCamera(enabledCamerasList);
                
        if (_projectReader.hasNextLine())
          line = _projectReader.getNextLine();
        else
          break;
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
  }
}
