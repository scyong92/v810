package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Peter Esbensen
 */
class VariableHeightConnectorPadSliceSettingConverter extends TunedSettingConverter
{
  /**
   * @author Peter Esbensen
   */
  VariableHeightConnectorPadSliceSettingConverter()
  {
    // store the old EnumStringLookup string for the setting that we want to convert
    super( "gridArrayVariableHeightConnectorPadSliceOffset", true );
  }

  /**
   * @author Peter Esbensen
   */
  Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(Serializable settingValue,
                                                                        Subtype subtype)
  {
    Assert.expect(settingValue != null);
    Assert.expect(subtype != null);

    // both the old setting and the new one were in millimeters, so just copy the old value over
    AlgorithmSettingEnum newAlgorithmSettingEnum = AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT;
    Serializable newValue = settingValue;

    return new Pair<AlgorithmSettingEnum, Serializable>(newAlgorithmSettingEnum, newValue);
  }

  /**
   * Not used for this converter. Should not be called.
   * @author George Booth
   */
  ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList()
  {
    Assert.expect(false, "Bad call to VariableHeightConnectorPadSliceSettingConverter.getSplitAlgorithmSettingList()");
    return null;
  }
}
