package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * Creates PackagePin, PadType
 * @author Bill Darbie
 */
class CompPackageReader
{
  private static CompPackageReader _instance;

  private ProjectReader _projectReader;
  // padName jointTypeName padOrientation
  private Pattern _padNameAndJointTypeAndOrientationPattern1 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(north|south|east|west|doesNotApply|undefined)$");
  // padName jointTypeName padOrientation customJointHeightInNanos
  private Pattern _padNameAndJointTypeAndOrientationPattern2 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(north|south|east|west|doesNotApply|undefined)\\s+([^\\s]+)$");
  // packageLongName packageShortName
  private Pattern _packageNamePattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");
  private EnumStringLookup _enumStringLookup;
  // Log out any errors that the recipe contained
  private ProjectErrorLogUtil _projectErrorLogUtil;
  // Prevent repeatable log
  private String _prevCompPackageName = null;
  private String _currentCompPackageName = null;
  private List<String> _compPackageLongNameList = new ArrayList<String>();
  
  /**
   * @author Bill Darbie
   */
  static synchronized CompPackageReader getInstance()
  {
    if (_instance == null)
      _instance = new CompPackageReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private CompPackageReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project,
            Map<String, CompPackage> compPackageNameToCompPackageMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(compPackageNameToCompPackageMap != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    String fileName = null;
    int version = FileName.getProjectCompPackageLatestFileVersion();
    while (version > 0)
    {
      fileName = FileName.getProjectCompPackageFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }

    try
    {
      _projectReader.openFile(fileName);

      String line = "";
      if (_projectReader.hasNextLine())
        line = _projectReader.getNextLine();

      Map<String, PackagePin> landPatternPadNameToPackagePinMap = new HashMap<String, PackagePin>();
      while (true)
      {
        if (_projectReader.hasNextLine() == false)
          break;

        // packageName
        Matcher matcher = _packageNamePattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        String compPackageLongName = matcher.group(1);
        String compPackageShortName = matcher.group(2);

        CompPackage compPackage = compPackageNameToCompPackageMap.get(compPackageLongName);
        if (compPackage == null)
        {
          // we have an unused CompPackage
          compPackage = new CompPackage();
          compPackage.setPanel(project.getPanel());
          compPackage.setLongName(compPackageLongName);
        }
        compPackage.setShortName(compPackageShortName);

        // iterate over all Pads for the CompPackage
        landPatternPadNameToPackagePinMap.clear();
        while (true)
        {
          if (_projectReader.hasNextLine() == false)
            break;

          line = _projectReader.getNextLine();
          if(version == 1)
            matcher = _padNameAndJointTypeAndOrientationPattern1.matcher(line);
          else if (version == 2)
            matcher = _padNameAndJointTypeAndOrientationPattern2.matcher(line);
          else
            Assert.expect(false);

          if (matcher.matches() == false)
          {
            // if we are here then we did not match on the pad line, so we are at the
            // next compPackageName
            compPackageLongName = line;
            break;
          }

          // padName padOrientation jointTypeName [customJointHeightInNanos]
          String landPatternPadName = matcher.group(1);
          String jointTypeName = matcher.group(2);
          String pinOrientation = matcher.group(3);

          PackagePin packagePin = new PackagePin();
          PackagePin prev = landPatternPadNameToPackagePinMap.put(landPatternPadName, packagePin);
          Assert.expect(prev == null);

          if (pinOrientation.equals("undefined") == false)
            packagePin.setPinOrientationEnum(_enumStringLookup.getPadOrientationEnum(pinOrientation));

          packagePin.setJointTypeEnum(_enumStringLookup.getJointTypeEnum(jointTypeName));
          packagePin.setCompPackage(compPackage);

          // finally check if the custom joint height was specivied
          if(version == 2)
          {
            int customJointHeight = StringUtil.convertStringToInt(matcher.group(4));
            packagePin.setCustomJointHeightInNanoMeters(customJointHeight);
          }
        }

        // ok - now we have a CompPackage object and all of its PackagePin objects created
        //      for each ComponentType that uses this CompPackage we need to:
        //      - create a PadType object
        //      - assign a PackagePin to the PadType
        //      - assign a LandPatternPad to the PadType
        //      - assign LandPatternPad to the PackagePin
        //      - assign PadType to LandPatternPad
        for (ComponentType componentType : compPackage.getComponentTypes())
        {
          // create a Map from landPatternPadName to LandPatternPad
          Map<String, LandPatternPad> landPatternPadNameToLandPatternPadMap = new HashMap<String, LandPatternPad>();
          for (LandPatternPad landPatternPad : componentType.getLandPattern().getLandPatternPads())
          {
            LandPatternPad prevObject = landPatternPadNameToLandPatternPadMap.put(landPatternPad.getName(), landPatternPad);
            if (prevObject != null)
              throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
          }

          for (Map.Entry<String, PackagePin> mapEntry : landPatternPadNameToPackagePinMap.entrySet())
          {
            String landPatternPadName = mapEntry.getKey();
            PackagePin packagePin = mapEntry.getValue();

            LandPatternPad landPatternPad = landPatternPadNameToLandPatternPadMap.get(landPatternPadName);
            Assert.expect(landPatternPad != null);

            PadType padType = new PadType();
            padType.setComponentType(componentType);
            padType.setLandPatternPad(landPatternPad);
            packagePin.setLandPatternPad(landPatternPad);
            padType.setPackagePin(packagePin);
            componentType.addPadType(padType);
          }
        }

        // fixed by Wei Chin (17/12/2009)
        // manually assign landPattern for this compPackage if no compononetType assign into this CompPackage
        if(compPackage.getComponentTypes().size() == 0)
        {
          //Siew Yeng - XCR1777 - fix always return lanpattern not found error
          boolean lanPatternFound = false; 
          
          List<LandPattern> landPatterns = project.getPanel().getAllLandPatterns();
          for(LandPattern landPattern : landPatterns)
          {
            if( landPattern.getName().equals(compPackage.getShortName()) )
            {
              compPackage.setLandPattern(landPattern);

              for (Map.Entry<String, PackagePin> mapEntry : landPatternPadNameToPackagePinMap.entrySet())
              {
                String landPatternPadName = mapEntry.getKey();
                PackagePin packagePin = mapEntry.getValue();

                LandPatternPad landPatternPad = landPattern.getLandPatternPad(landPatternPadName);
                Assert.expect(landPatternPad != null);
                packagePin.setLandPatternPad(landPatternPad);
              }
              
              lanPatternFound = true; //Siew Yeng - XCR1777
              break;
            }
          }
            
          //Siew Yeng - XCR1777 - fix always return lanpattern not found error
          if(lanPatternFound == false) 
          {
            _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
            try 
            {
              _currentCompPackageName = compPackage.getLongName();   
              if(_currentCompPackageName.equalsIgnoreCase(_prevCompPackageName))
                  continue; 
              _projectErrorLogUtil.log(projName, "Cannot find land pattern pad name for component package: ", compPackage.getShortName());
              _projectErrorLogUtil.setIsLandPatternNameNotFoundError(true);
              _compPackageLongNameList.add(compPackage.getLongName());
              _projectErrorLogUtil.setUnusedCompPackage(_compPackageLongNameList);
              _prevCompPackageName = _currentCompPackageName;      
            } catch (XrayTesterException ex) {
              System.out.println("Could not write into recipe error log");
            } 
          }
        }
        
        // now that packagePin.setLandPatternPad() has been called, it is safe
        // to call compPackage.addPackagePin()
        for (PackagePin packagePin : landPatternPadNameToPackagePinMap.values())
        {
          // requires packagePin.setLandPatternPad()
          compPackage.addPackagePin(packagePin);
        }
      }
      _prevCompPackageName = null;
      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }
}
