package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Peter Esbensen
 */
class ThroughHoleExtrusionSliceSettingConverter extends TunedSettingConverter
{
  // Pin Side @ -10%
  private Pattern _extrusionSliceSettingPattern = Pattern.compile("Pin Side @ ([-+]?[0-9]+)%");

  /**
   * @author Peter Esbensen
   */
  ThroughHoleExtrusionSliceSettingConverter()
  {
    // store the old EnumStringLookup string for the setting that we want to convert
    super( "throughholeExtrusionSlice", false );
  }

  /**
   * @author Peter Esbensen
   */
  Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(Serializable settingValue,
                                                                        Subtype subtype)
  {
    Assert.expect(settingValue != null);
    Assert.expect(subtype != null);

    AlgorithmSettingEnum newAlgorithmSettingEnum = AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT;
    Serializable newValue = null;

    // the obsolete extrusion slice settings were None, Pin Side @ -30%, Pin Side @ -20%, Pin Side @ -10%
    String oldValue = (String)settingValue;

    if (oldValue.equals("None"))
    {
      newValue = subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT);
    }
    else
    {
      // if not None, get the previous setting
      float valueFromOldSettingAsPercentOfBarrel = getExtrusionHeightAsPercentBarrelFromOldThroughHoleSetting((String)settingValue);

      // convert to millimeters.  Also, flip the sign because positive values in the new setting are considered to be moving away
      // from the board, which is opposite of the old setting
      float boardThicknessInNanos = subtype.getPanel().getThicknessInNanometers();
      float newValueInMillimeters = MathUtil.convertNanometersToMillimeters( -1 * valueFromOldSettingAsPercentOfBarrel * 0.01f * boardThicknessInNanos );

      newValue = newValueInMillimeters;
    }

    return new Pair<AlgorithmSettingEnum, Serializable>(newAlgorithmSettingEnum, newValue);
  }

  /**
   * Get the extrusion height from the obsolete Through Hole barrel height setting.
   * This is based on the hardcoded name for the obsolete setting enums.
   *
   * @author Peter Esbensen
   */
  private float getExtrusionHeightAsPercentBarrelFromOldThroughHoleSetting(String settingValue)
  {
    // Pull out the number from the string, which will be of the form:
    //    Barrel @ 10%
    String sliceHeightPercentString = null;
    Matcher matcher = _extrusionSliceSettingPattern.matcher(settingValue);
    if (matcher.matches())
    {
      sliceHeightPercentString = matcher.group(1);
    }
    else
    {
      Assert.expect(false, "unexpected format for PTH extrusion slice setting in subtype.settings");
    }

    Assert.expect(sliceHeightPercentString != null);

    float sliceHeightPercent = Float.valueOf(sliceHeightPercentString);
    return sliceHeightPercent;
  }

  /**
   * Not used for this converter. Should not be called.
   * @author George Booth
   */
  ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList()
  {
    Assert.expect(false, "Bad call to ThroughHoleExtrusionSliceSettingConverter.getSplitAlgorithmSettingList()");
    return null;
  }

}
