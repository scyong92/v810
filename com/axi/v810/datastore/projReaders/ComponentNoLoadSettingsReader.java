package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Creates nothing, just sets ComponentTypeSettings to noLoad where appropriate.
 * @author Bill Darbie
 */
class ComponentNoLoadSettingsReader
{
  private static ComponentNoLoadSettingsReader _instance;

  private ProjectReader _projectReader;
  // Name: variation name
  private Pattern _variationNamePattern = Pattern.compile("^(variationName:)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)$");
  // isEnable: isEnable
  private Pattern _isEnablePattern = Pattern.compile("^(isEnable:)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)$");
  //Kee Chin Seong
  private Pattern _isViewableInProduction = Pattern.compile("^(isViewableInProduction:)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)$");
  
  // boardTypeName: boardTypeName
  private Pattern _boardTypeNamePattern = Pattern.compile("^(boardTypeName:)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)$");
  // refDes
  private Pattern _refDesPattern = Pattern.compile("^([^\\s]+)$");
  private String _fileName;
  private Pattern _fileNamePattern = Pattern.compile("^(componentNoLoad.)(\\d)((\\.\\d+)?)(.settings)$");
  private int _version = 1;

  /**
   * @author Bill Darbie
   */
  static synchronized ComponentNoLoadSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new ComponentNoLoadSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ComponentNoLoadSettingsReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   * @author Kok Chun, Tan
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();
    
    int latestVersion = FileName.getProjectComponentNoLoadSettingsLatestFileVersion();
    _version = 1;
    
    String projName = project.getName();
    String projDir = Directory.getProjectDir(projName);
    java.util.List<String> componentNoLoadFileList = getComponentNoLoadSettingsFileNames(projDir);
    
    Assert.expect(_version <= latestVersion);
    
    for (String file : componentNoLoadFileList)
    {
      try
      {
        _fileName = "";
        _fileName = FileName.getProjectComponentNoLoadSettingsFullPath(project.getName(), file);
        _projectReader.openFile(_fileName);

        Panel panel = project.getPanel();
        String boardTypeName = null;
        String variationName = null;
        String isEnable = null;
        String isEnableInProduction = null;
        boolean hasComponent = false;
        boolean isContinueNextLoop = false;
        Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<>();
        
        if (_projectReader.hasNextLine() == false)
        {
          break;
        }
        
        String line = _projectReader.getNextLine();
        if (_version > 1)
        {
          //match variation name
          Matcher matcher = _variationNamePattern.matcher(line);
          if (matcher.matches())
          {
            variationName = matcher.group(2);
            if (_projectReader.hasNextLine() == false)
            {
              continue;
            }
            line = _projectReader.getNextLine();
          }
          else
          {
            continue;
          }
          //match enable boolean
          matcher = _isEnablePattern.matcher(line);
          if (matcher.matches())
          {
            isEnable = matcher.group(2);
            if (_projectReader.hasNextLine() == false)
            {
              continue;
            }
            line = _projectReader.getNextLine();
          }
          else
          {
            continue;
          }
          if (_version > 2)
          {
            matcher = _isViewableInProduction.matcher(line);
            if (matcher.matches())
            {
              isEnableInProduction = matcher.group(2);
              if (_projectReader.hasNextLine() == false)
              {
                break;
              }
              line = _projectReader.getNextLine();
            }
            else
            {
              continue;
            }
          }
        }
        else
        {
          variationName = VariationSettingManager.getInstance().getDefaultVariationName();
          isEnable = "true";
        }
        
        while (true)
        {
          Matcher matcher = _refDesPattern.matcher(line);
          if (matcher.matches() == false)
          {
            matcher = _boardTypeNamePattern.matcher(line);
            if (matcher.matches() == false)
            {
              isContinueNextLoop = true;
              break;
            }

            // boardTypeName: boardTypeName
            boardTypeName = matcher.group(2);
            
            if (boardTypeToRefDesMap.containsKey(boardTypeName) == false)
            {
              boardTypeToRefDesMap.put(boardTypeName, new ArrayList<String> ());
            }
          }
          else
          {
            // refDes
            String refDes = matcher.group(1);

            if (boardTypeName == null)
            {
              isContinueNextLoop = true;
              break;
            }

            if (panel.hasComponentType(boardTypeName, refDes) == false)
            {
              isContinueNextLoop = true;
              break;
            }
            
            if (boardTypeToRefDesMap.containsKey(boardTypeName))
            {
              hasComponent = true;
              boardTypeToRefDesMap.get(boardTypeName).add(refDes);
            }
          }
          if (_projectReader.hasNextLine() == false)
          {
            break;
          }
          line = _projectReader.getNextLine();
        }
        
        if (isContinueNextLoop)
        {
          continue;
        }
        
        if (variationName != null && hasComponent)
        {
          boolean enable = Boolean.parseBoolean(isEnable);
          boolean isEnabledInProduction = Boolean.parseBoolean(isEnableInProduction);
          VariationSetting variation = new VariationSetting(project, variationName, enable, isEnabledInProduction, boardTypeToRefDesMap);
          project.getPanel().getPanelSettings().addVariationSetting(variation);
          
          if (variation.isEnable())
          {
            VariationSettingManager.getInstance().setupVariationSetting(true, project, boardTypeToRefDesMap);
            project.setEnabledVariationName(variation.getName());
            project.setSelectedVariationName(variation.getName());
          }
        }
      }
      catch (DatastoreException dex)
      {
        _projectReader.closeFileDuringExceptionHandling();
        throw dex;
      }
      finally
      {
        //until finish
        _projectReader.goToLastLine();
        _projectReader.closeFile();
        init();
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * @param projectName
   * @throws DatastoreException 
   */
  Map<String, Pair<String, String>> read(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();
    
    int latestVersion = FileName.getProjectComponentNoLoadSettingsLatestFileVersion();
    _version = 1;
    
    String projDir = Directory.getProjectDir(projectName);
    java.util.List<String> componentNoLoadFileList = getComponentNoLoadSettingsFileNames(projDir);
    Map<String, Pair<String, String>> variationNameToEnabledMap = new LinkedHashMap<>();
    
    Assert.expect(_version <= latestVersion);
    
    for (String file : componentNoLoadFileList)
    {
      try
      {
        String variationName = "";
        String isEnabled = "";
        String isEnableInProduction = "";
        _fileName = "";
        _fileName = FileName.getProjectComponentNoLoadSettingsFullPath(projectName, file);
        _projectReader.openFile(_fileName);
        
        if (_projectReader.hasNextLine() == false)
        {
          continue;
        }
        
        String line = _projectReader.getNextLine();
        if (_version > 1)
        {
          Matcher matcher = _variationNamePattern.matcher(line);
          if (matcher.matches())
          {
            variationName = matcher.group(2);
            if (_projectReader.hasNextLine() == false)
            {
              continue;
            }
          }
          else
          {
            continue;
          }
          line = _projectReader.getNextLine();
          matcher = _isEnablePattern.matcher(line);
          if (matcher.matches())
          {
            isEnabled = matcher.group(2);
            if (_projectReader.hasNextLine() == false)
            {
              continue;
            }
          }
          else
          {
            continue;
          }   
          if (_version == 3)
          {
            line = _projectReader.getNextLine();
            matcher = _isViewableInProduction.matcher(line);
            if (matcher.matches())
            {
              isEnableInProduction = matcher.group(2);
              if (_projectReader.hasNextLine() == false)
              {
                continue;
              }
            }
            else
            {
              continue;
            }
          }
          line = _projectReader.getNextLine();
          matcher = _boardTypeNamePattern.matcher(line);
          if (matcher.matches())
          {
            if (_projectReader.hasNextLine() == false)
            {
              continue;
            }
          }
          else
          {
            continue;
          }
          line = _projectReader.getNextLine();
          matcher = _refDesPattern.matcher(line);
          if (matcher.matches())
          {
            //nothing
          }
          else
          {
            continue;
          }
          //has component only add to map
          Pair<String, String> pairEnableAndViewableInProduction = new Pair<String, String>();
          //if(Boolean.parseBoolean(isEnableInProduction) == true)
          pairEnableAndViewableInProduction.setFirst(isEnabled);
          pairEnableAndViewableInProduction.setSecond(isEnableInProduction);
          
          variationNameToEnabledMap.put(variationName, pairEnableAndViewableInProduction);    
        }
      }
      catch (DatastoreException dex)
      {
        _projectReader.closeFileDuringExceptionHandling();
        throw dex;
      }
      finally
      {
        //until finish
        _projectReader.goToLastLine();
        _projectReader.closeFile();
        init();
      }
    }
    return variationNameToEnabledMap;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private List<String> getComponentNoLoadSettingsFileNames(String projDir)
  {
    List<String> componentNoLoadFileList = new ArrayList<String> ();
    _version = 1;
    
    for (String file : FileUtilAxi.listAllFilesInDirectory(projDir))
    {
      Matcher matcher = _fileNamePattern.matcher(file);
      if (matcher.matches())
      {
        if (Integer.parseInt(matcher.group(2)) > _version)
        {
          _version = Integer.parseInt(matcher.group(2));
          componentNoLoadFileList.clear();
          componentNoLoadFileList.add(file);
        }
        else if (Integer.parseInt(matcher.group(2)) == _version)
        {
          componentNoLoadFileList.add(file);
        }
      }
    }
    
    return componentNoLoadFileList;
  }
}
