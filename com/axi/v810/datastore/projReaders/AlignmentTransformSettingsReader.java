package com.axi.v810.datastore.projReaders;

import java.awt.geom.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Creates nothing.
 * @author Bill Darbie
 */
class AlignmentTransformSettingsReader
{
  private static AlignmentTransformSettingsReader _instance;

  private ProjectReader _projectReader;

  // alignmentGroupName
  private Pattern _alignmentGroupNameOnlyPattern = Pattern.compile("^(\\w+)$");
  // alignmentGroupName matchQuality foregroundGrayLevel backgroundGrayLevel
  private Pattern _alignmentGroupNameFullDefinitionPattern = Pattern.compile("^(\\w+)"+
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "$");
    // alignmentGroupName matchQuality foregroundGrayLevel backgroundGrayLevel foregroundGrayLevelInHighMag backgroundGrayLevelInHighMag
  private Pattern _alignmentGroupNameFullDefinitionPattern2 = Pattern.compile("^(\\w+)"+
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "\\s+" +
                                                                             "(" + RegularExpressionUtil.FLOATING_POINT_PATTERN_STRING + ")" +
                                                                             "$");
  // leftManualAlignmentTransformMatrix|rightManualAlignmentTransformMatrix|leftRuntimeAlignmentTransformMatrix|rightRuntimeAlignmentTransformMatrix
  private Pattern _alignmentMatrixPattern =
      Pattern.compile("^(leftManualAlignmentTransformMatrix|" +
                      "rightManualAlignmentTransformMatrix|" +
                      "leftRuntimeAlignmentTransformMatrix|" +
                      "rightRuntimeAlignmentTransformMatrix)$");
  
  private Pattern _alignmentMatrixPattern2 =
      Pattern.compile("^(leftManualAlignmentTransformMatrix|" +
                      "rightManualAlignmentTransformMatrix|" +
                      "leftRuntimeAlignmentTransformMatrix|" +
                      "rightRuntimeAlignmentTransformMatrix|" +
                      "leftManualAlignmentTransformMatrixForHighMag|" +
                      "rightManualAlignmentTransformMatrixForHighMag|" +
                      "leftRuntimeAlignmentTransformMatrixForHighMag|" +
                      "rightRuntimeAlignmentTransformMatrixForHighMag|" +
                      "leftLastSavedViewCadRuntimeAlignmentTransformMatrix|" +
                      "rightLastSavedViewCadRuntimeAlignmentTransformMatrix|" +
                      "leftLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag|" +
                      "rightLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag)$");
                              

  /**
   * @author Bill Darbie
   */
  static synchronized AlignmentTransformSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new AlignmentTransformSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private AlignmentTransformSettingsReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    int version = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion();
    Assert.expect(version >= 1);
    String fileName = FileName.getProjectAlignmentTransformSettingsFullPath(projName, version);
    
    //XCR1145, Chnee Khang Wah, 15-Dec-2010, Making older alignment matrix to be still valid when using 2.1 software to open 2.0 recipes
    while (version > 0)
    {
      String fileNameFound = FileName.getProjectAlignmentTransformSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(fileNameFound))
      {
        fileName = fileNameFound.toLowerCase();
        break;
      }
      --version;
    }
    //XCR1145 - end
    
    PanelSettings panelSettings = project.getPanel().getPanelSettings();

    // If the transform file doesn't exist, blow any existing transforms away and bail.
    // This gives us a method of clearing out old alignment transforms.
    if (FileUtil.exists(fileName) == false)
    {
      panelSettings.clearAllAlignmentTransforms();
      if(panelSettings.isPanelBasedAlignment() == false)
      {
        for(Board board: project.getPanel().getBoards())
        {
          board.getBoardSettings().clearAllAlignmentTransforms();
        }
      }
      return;
    }

    try
    {
      _projectReader.openFile(fileName);

      List<AlignmentGroup> alignmentGroups = null;

      if(version >= 2)
      {
        if(panelSettings.isPanelBasedAlignment() == false)
        {
          for(Board board: project.getPanel().getBoards())
          {
            if(alignmentGroups == null)
              alignmentGroups = board.getBoardSettings().getAllAlignmentGroups();
            else
              alignmentGroups.addAll(board.getBoardSettings().getAllAlignmentGroups());
          }
        }
        else
          alignmentGroups = panelSettings.getAllAlignmentGroups();
      }
      else if( version == 1)
      {
        alignmentGroups = panelSettings.getAllAlignmentGroups();
      }

      if(version == 1)
      {
        readAlignmentTransformSettings(version, fileName, alignmentGroups, panelSettings);
      }
      else if(version >= 2)
      {
        if(panelSettings.isPanelBasedAlignment() == false)
        {
          for(Board board: project.getPanel().getBoards())
          {
            readAlignmentTransformSettings(version, fileName, alignmentGroups, panelSettings, board.getBoardSettings());
          }
        }
        else
        {
          readAlignmentTransformSettings(version, fileName, alignmentGroups, panelSettings);
        }
      }
      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      // do nothing if XXL System
      if(XrayTester.isXXLBased() == false && XrayTester.isS2EXEnabled() == false)
        throw dex;
    } 
    finally
    {
      init();
    }
  }

  /**
   * @param version
   * @param fileName
   * @param alignmentGroups
   * @param panelSettings
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  void readAlignmentTransformSettings(int version, String fileName, List<AlignmentGroup> alignmentGroups ,
          PanelSettings panelSettings) throws DatastoreException
  {
    Assert.expect(version >= 1);
    Assert.expect(fileName != null);
    Assert.expect(alignmentGroups != null && alignmentGroups.size() > 0);
    Assert.expect(_projectReader != null);
    
    readAlignmentTransformSettings(version, fileName, alignmentGroups ,panelSettings, null);
  }

  /**
   * @param version
   * @param fileName
   * @param alignmentGroups
   * @param panelSettings
   * @param boardSettings
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  void readAlignmentTransformSettings(int version, String fileName, List<AlignmentGroup> alignmentGroups ,
          PanelSettings panelSettings, BoardSettings boardSettings) throws DatastoreException
  {
    Assert.expect(version >= 1);
    Assert.expect(fileName != null);
    Assert.expect(alignmentGroups != null && alignmentGroups.size() > 0);
    Assert.expect(_projectReader != null);

    // XCR-3186 Assert When Collect Fine Tuning Image Set
    // Due to XXL spliting panel way is different from standard
    // Assign empty alignment groups if different system type
    if (panelSettings.getPanel().getProject().getSystemType().equals(com.axi.v810.hardware.XrayTester.getSystemType()) == false)
    {
      _projectReader.goToLastLine();
      return;
    }
    
    try
    {
      int numberOfFullAlignmentGroupDefinitionsFound = 0;
      String line = null;
      while (_projectReader.hasNextLine())
      {
        line = _projectReader.getNextLine();

        if (line.equals("boardData"))
          continue;

        if (line.equals("matrixData"))
          break;
        
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        if(line.contains("alignmentMode"))
          break;

        Matcher matcher = _alignmentGroupNameOnlyPattern.matcher(line);
        String alignmentGroupName = null;
        String matchQualityStr = null;
        String foregroundGrayLevelStr = null;
        String backgroundGrayLevelStr = null;
        String foregroundGrayLevelInHighMagStr = null;
        String backgroundGrayLevelInHighMagStr = null;
        if (matcher.matches())
        {
          alignmentGroupName = matcher.group(1);
        }
        else
        {
          matcher = _alignmentGroupNameFullDefinitionPattern.matcher(line);
          if (matcher.matches())
          {
            alignmentGroupName = matcher.group(1);
            matchQualityStr = matcher.group(2);
            foregroundGrayLevelStr = matcher.group(3);
            backgroundGrayLevelStr = matcher.group(4);
            ++numberOfFullAlignmentGroupDefinitionsFound;
          }
          else
          {
            matcher = _alignmentGroupNameFullDefinitionPattern2.matcher(line);
            if (matcher.matches())
            {
              alignmentGroupName = matcher.group(1);
              matchQualityStr = matcher.group(2);
              foregroundGrayLevelStr = matcher.group(3);
              backgroundGrayLevelStr = matcher.group(4);
              foregroundGrayLevelInHighMagStr = matcher.group(5);
              backgroundGrayLevelInHighMagStr = matcher.group(6);
              ++numberOfFullAlignmentGroupDefinitionsFound;
            }            
          }
        }

        // find the alignmentGroup
        AlignmentGroup alignmentGroup = null;
        for (AlignmentGroup alignGroup : alignmentGroups)
        {
          if (alignGroup.getName().equals(alignmentGroupName))
          {
            alignmentGroup = alignGroup;
            break;
          }
        }

        if (alignmentGroup == null)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        if (matcher.matches())
        {
          if (matchQualityStr != null)
          {
            double matchQuality = StringUtil.convertStringToDouble(matchQualityStr);
            alignmentGroup.setMatchQuality(matchQuality);
          }
          if (foregroundGrayLevelStr != null)
          {
            double foregroundGrayLevel = StringUtil.convertStringToDouble(foregroundGrayLevelStr);
            if(foregroundGrayLevel != -1d )
              alignmentGroup.setForegroundGrayLevel(foregroundGrayLevel);
          }
          if (backgroundGrayLevelStr != null)
          {
            double backgroundGrayLevel = StringUtil.convertStringToDouble(backgroundGrayLevelStr);
            if(backgroundGrayLevel != -1d )
              alignmentGroup.setBackgroundGrayLevel(backgroundGrayLevel);
          }
          if (foregroundGrayLevelInHighMagStr != null)
          {
            double foregroundGrayLevel = StringUtil.convertStringToDouble(foregroundGrayLevelInHighMagStr);
            if(foregroundGrayLevel != -1d )
              alignmentGroup.setForegroundGrayLevelInHighMag(foregroundGrayLevel);
          }
          if (backgroundGrayLevelInHighMagStr != null)
          {
            double backgroundGrayLevel = StringUtil.convertStringToDouble(backgroundGrayLevelInHighMagStr);
            if(backgroundGrayLevel != -1d )
              alignmentGroup.setBackgroundGrayLevelInHighMag(backgroundGrayLevel);
          }
        }
      }

      boolean someTransformExists = false;

      while (_projectReader.hasNextLine())
      {
        if(line.contains("alignmentMode"))
          break;
        
        line = _projectReader.getNextLine();

        if(line.equals("boardData"))
          break;
        
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        if(line.contains("alignmentMode"))
          break;

        Matcher matcher = null;
        
        if(version < 3)
          matcher = _alignmentMatrixPattern.matcher(line);
        
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        if(version == 3 || version == 4 || version == 5)
          matcher = _alignmentMatrixPattern2.matcher(line);
        
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        someTransformExists = true;
        String matrixType = matcher.group(1);
        String value1Str = _projectReader.getNextLine();
        String value2Str = _projectReader.getNextLine();
        String value3Str = _projectReader.getNextLine();
        String value4Str = _projectReader.getNextLine();
        String value5Str = _projectReader.getNextLine();
        String value6Str = _projectReader.getNextLine();
        double[] matrix = new double[6];
        matrix[0] = StringUtil.convertStringToDouble(value1Str);
        matrix[1] = StringUtil.convertStringToDouble(value2Str);
        matrix[2] = StringUtil.convertStringToDouble(value3Str);
        matrix[3] = StringUtil.convertStringToDouble(value4Str);
        matrix[4] = StringUtil.convertStringToDouble(value5Str);
        matrix[5] = StringUtil.convertStringToDouble(value6Str);

        AffineTransform trans = new AffineTransform(matrix);
        if (matrixType.equals("rightManualAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setRightManualAlignmentTransform(trans);
          else
            panelSettings.setRightManualAlignmentTransform(trans);
        }
        else if (matrixType.equals("leftManualAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setLeftManualAlignmentTransform(trans);
          else
            panelSettings.setLeftManualAlignmentTransform(trans);
        }
        else if (matrixType.equals("rightRuntimeAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setRightRuntimeAlignmentTransform(trans);
          else
            panelSettings.setRightRuntimeAlignmentTransform(trans);
        }
        else if (matrixType.equals("leftRuntimeAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setLeftRuntimeAlignmentTransform(trans);
          else
            panelSettings.setLeftRuntimeAlignmentTransform(trans);
        }
        else if (matrixType.equals("rightRuntimeAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setRightLastSavedViewCadRuntimeAlignmentTransform(trans);
          else
            panelSettings.setRightLastSavedViewCadRuntimeAlignmentTransform(trans);
        }
        else if (matrixType.equals("leftLastSavedRuntimeAlignmentTransformMatrix"))
        {
          if(version > 1 && panelSettings.isPanelBasedAlignment() == false)
            boardSettings.setLeftLastSavedViewCadRuntimeAlignmentTransform(trans);
          else
            panelSettings.setLeftLastSavedViewCadRuntimeAlignmentTransform(trans);
        }
        // XCR-3707 Alignment OOF
        else if (matrixType.equals("rightManualAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if( panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setRightManualAlignmentTransformForHighMag(trans);
            else
              panelSettings.setRightManualAlignmentTransformForHighMag(trans);
          }
        }
        else if (matrixType.equals("leftManualAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if( panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setLeftManualAlignmentTransformForHighMag(trans);
            else
              panelSettings.setLeftManualAlignmentTransformForHighMag(trans);
          }
        }
        else if (matrixType.equals("rightRuntimeAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if( panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setRightRuntimeAlignmentTransformForHighMag(trans);
            else
              panelSettings.setRightRuntimeAlignmentTransformForHighMag(trans);
          }
        }
        else if (matrixType.equals("leftRuntimeAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if(panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setLeftRuntimeAlignmentTransformForHighMag(trans);
            else
              panelSettings.setLeftRuntimeAlignmentTransformForHighMag(trans);
          }
        }
        else if (matrixType.equals("rightLastSavedRuntimeAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if( panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(trans);
            else
              panelSettings.setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(trans);
          }
        }
        else if (matrixType.equals("leftLastSavedRuntimeAlignmentTransformMatrixForHighMag"))
        {
          if(version >= 4)
          {
            if(panelSettings.isPanelBasedAlignment() == false)
              boardSettings.setLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag(trans);
            else
              panelSettings.setLeftLastSavedRuntimeAlignmentTransformForHighMag(trans);
          }
        }
      }
      
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      while (_projectReader.hasNextLine())
      {
        String alignmentMethod = null;
        
        if (line.equals("alignmentMode") == false)
          break;
        
        line = _projectReader.getNextLine();
        
        if (line.equals("2D") || line.equals("3D"))
          alignmentMethod = line;
        else
          break;

        boolean isUsing2DAlignmentMethod = false;

        if (alignmentMethod.equals("2D"))  
        {
          isUsing2DAlignmentMethod = true;            
        }
          
        panelSettings.setUsing2DAlignmentMethod(isUsing2DAlignmentMethod);
      }

      //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
      if (someTransformExists)
        panelSettings.setIsManualAlignmentAllCompleted(true);
      
      /** @todo mdw - 2/9/2007 - change this to throw an exception before we ship.  This is just a temporary kludge to
       * help the lab.
       */
      if(version == 1)
      {
        if (someTransformExists && ((numberOfFullAlignmentGroupDefinitionsFound != 3) && (numberOfFullAlignmentGroupDefinitionsFound != 6)))
        {
          panelSettings.clearAllAlignmentTransforms();
        }
      }
      else if(version >= 2)
      {
        if (someTransformExists && ((numberOfFullAlignmentGroupDefinitionsFound != 3) && (numberOfFullAlignmentGroupDefinitionsFound != 6)))
        {
          if(panelSettings.isPanelBasedAlignment() == false)
          {
            boardSettings.clearAllAlignmentTransforms();
          }
          else
          {
            panelSettings.clearAllAlignmentTransforms();
          }
        }
      }
    }
    catch (DatastoreException dex)
    {
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);
      throw dex;
    }
  }
}
