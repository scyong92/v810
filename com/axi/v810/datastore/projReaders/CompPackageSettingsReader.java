package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is a reader for package setting file
 * Any modification on package setting file format, this file must be modifed to the new format.
 *
 * @author Poh Kheng
 */
class CompPackageSettingsReader
{
  private static CompPackageSettingsReader _instance;

  private ProjectReader _projectReader;
  // packageLongName packageShortName importedPackageLibraryPath
  private Pattern _packageNamePattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+\"(.*)\"$");
  // packageLongName packageShortName importedPackageLibraryPath importedCompakageCheckSum libraryLandPatternName
  private Pattern _packageNamePattern2 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+\"(.*)\"\\s+([^\\s]+)\\s+([^\\s]+)$");
  // packageLongName packageShortName importedPackageLibraryPath importedCompakageCheckSum libraryLandPatternName imported subtypeThresholdCheckSum
  private Pattern _packageNamePattern3 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+\"(.*)\"\\s+([^\\s]+)\\s+([^\\s]+)\\s+(true|false)\\s+([^\\s]+)$");

  /**
   * @author Poh Kheng
   */
  static synchronized CompPackageSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new CompPackageSettingsReader();

    return _instance;
  }

  /**
   * @author Poh Kheng
   */
  private CompPackageSettingsReader()
  {
    // do nothing
  }

  /**
   * Thsi would read the package setting file
   *
   * @author Poh Kheng
   */
  void read(Project project,
            Map<String, CompPackage> compPackageNameToCompPackageMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(compPackageNameToCompPackageMap != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    String projName = project.getName();
    String fileName = null;
    int version = FileName.getProjectCompPackageSettingLatestFileVersion();
    while (version > 0)
    {
      fileName = FileName.getProjectCompPackageSettingFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }

    try
    {
      if (FileUtil.exists(fileName) == false)
      {
        return;
      }

      _projectReader.openFile(fileName);

      String line = "";

      while (true)
      {
        if (_projectReader.hasNextLine() == false)
          break;
        else
          line = _projectReader.getNextLine();

        Matcher matcher = null;
        // packageName
        if(version == 1)
          matcher = _packageNamePattern.matcher(line);
        else if(version == 2)
          matcher = _packageNamePattern2.matcher(line);
        else if(version == 3)
          matcher = _packageNamePattern3.matcher(line);
        
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        String compPackageLongName = matcher.group(1);
        String compPackageLibraryPath = matcher.group(3);
        String compPackageCheckSum = null;
        String libraryLandPatternName = null;
        String imported = null;
        String subtypeThresholdsCheckSum = null;

        if(version == 2)
        {
          compPackageCheckSum = matcher.group(4);
          libraryLandPatternName = matcher.group(5);
        }
        if(version == 3)
        {
          compPackageCheckSum = matcher.group(4);
          libraryLandPatternName = matcher.group(5);
          imported = matcher.group(6);
          subtypeThresholdsCheckSum = matcher.group(7);
        }

        CompPackage compPackage = compPackageNameToCompPackageMap.get(compPackageLongName);
        if (compPackage == null)
        {
          // we have an unused CompPackage
          compPackage = new CompPackage();
          compPackage.setPanel(project.getPanel());
          compPackage.setLongName(compPackageLongName);
        }
        compPackage.setSourceLibrary(compPackageLibraryPath);

        if(version == 2)
        {
          compPackage.setLibraryCheckSum(Long.parseLong(compPackageCheckSum));
          if(libraryLandPatternName.equals("null") == false)
            compPackage.setLibraryLandpatternName(libraryLandPatternName);
        }
        if(version == 3)
        {
          compPackage.setLibraryCheckSum(Long.parseLong(compPackageCheckSum));
          if(libraryLandPatternName.equals("null") == false)
            compPackage.setLibraryLandpatternName(libraryLandPatternName);
          
          if(imported.equals("true"))
            compPackage.setImported(true);
          else if(imported.equals("false"))
            compPackage.setImported(false);
          
          if(subtypeThresholdsCheckSum.equals("0") == false)
            compPackage.setPreviousSubtypeThresholdsCheckSum(Long.parseLong(subtypeThresholdsCheckSum));

//          if (Config.isDeveloperDebugModeOn() == true)
//          {
//            System.out.println("package : " + compPackage.getShortName());
//            System.out.println("Read: " + subtypeThresholdsCheckSum);
//            System.out.println();
//          }
        }
      }
      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
  }
}
