package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR3554: Package on package (PoP) development
 */
public class PackageOnPackageReader
{
  private static PackageOnPackageReader _instance;
  private static boolean fiducialSettingInconsistent = false;

  private ProjectReader _projectReader;
  private Pattern _refDesPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(-?\\d+)\\s+(-?\\d+)$");  
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  static synchronized PackageOnPackageReader getInstance()
  {
    if (_instance == null)
      _instance = new PackageOnPackageReader();

    return _instance;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private PackageOnPackageReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    Map<String, CompPackageOnPackage> compPackageOnPackageNameToCompPackageOnPackageMap = new HashMap<String, CompPackageOnPackage>();
    init();

    Panel panel = project.getPanel();
    String projName = project.getName();

    String fileName = null;
    try
    {
      int version = FileName.getProjectPackageOnPackageLatestFileVersion();
      fileName = null;
      while (version > 0)
      {
        fileName = FileName.getProjectPackageOnPackageFullPath(projName, version);
        if (FileUtilAxi.exists(fileName))
          break;
        --version;
      }
      if(version < 1)
      {
        return;
      }

      _projectReader.openFile(fileName);

      String line = null;
      while (_projectReader.hasNextLine())
      {
        line = _projectReader.getNextLine();
        Matcher matcher = _refDesPattern.matcher(line);

        if (matcher.matches() == false)
        {
          break;
        }

        String refDes = matcher.group(1);
        String compPackageOnPackageName = matcher.group(2);
        POPLayerIdEnum layerId = _enumStringLookup.getLayerIdEnum(matcher.group(3));
        String zHeightPOPStr = matcher.group(4);
        int popZHeightInNanometers = StringUtil.convertStringToInt(zHeightPOPStr);

        CompPackageOnPackage compPackageOnPackage = compPackageOnPackageNameToCompPackageOnPackageMap.get(compPackageOnPackageName);
        if (compPackageOnPackage == null)
        {
          compPackageOnPackage = new CompPackageOnPackage();
          compPackageOnPackage.setPanel(panel);
          compPackageOnPackage.setName(compPackageOnPackageName);
          compPackageOnPackageNameToCompPackageOnPackageMap.put(compPackageOnPackageName, compPackageOnPackage);
        }

        ComponentType componentType = new ComponentType();
        for (BoardType currentBoardType : panel.getBoardTypes())
        {
          componentType = currentBoardType.getComponentType(refDes);
          componentType.setPanel(panel);
          componentType.setCompPackageOnPackage(compPackageOnPackage);
          componentType.setPOPLayerId(layerId);
          componentType.setPOPZHeightInNanometers(popZHeightInNanometers);
          
          if (compPackageOnPackage.getComponentTypes().contains(componentType) == false)
            compPackageOnPackage.addComponentType(componentType);
        }
      }

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }
}
