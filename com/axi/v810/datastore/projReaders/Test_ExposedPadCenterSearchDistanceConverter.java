package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import java.util.ArrayList;

class Test_ExposedPadCenterSearchDistanceConverter extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExposedPadCenterSearchDistanceConverter());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TunedSettingConversionEngine tunedSettingConversionEngine = new TunedSettingConversionEngine();

    Subtype subtype = new Subtype();

    // make sure conversionRequired() works

    String oldAlgorithmSettingEnumString = "exposedPadMeasurementCenterSearchDistance";
    boolean splitRequired = tunedSettingConversionEngine.splitIsRequired( oldAlgorithmSettingEnumString );

    Expect.expect(splitRequired);

    // get new settings
    ArrayList<AlgorithmSettingEnum> settingList = tunedSettingConversionEngine.getSplitAlgorithmSettingList(oldAlgorithmSettingEnumString);

    Expect.expect(settingList.get(0).equals(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS));
    Expect.expect(settingList.get(1).equals(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG));
  }
}
