package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class PshSettingsReader 
{
  private static PshSettingsReader _instance;

  private ProjectReader _projectReader;
  
  private Pattern _pshComponentPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");
  private Pattern _neighbourRefDesPattern = Pattern.compile("^([^\\s]+)$");
  
  private String _fileName;
  
  private int _version = 1;
  
  private ProjectErrorLogUtil _projectErrorLogUtil;

  /**
   * @author Siew yeng
   */
  public static synchronized PshSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new PshSettingsReader();

    return _instance;
  }

  /**
   * @author Siew Yeng
   */
  public PshSettingsReader()
  {
    // do nothing
  }

  /**
   * @author Siew Yeng
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Siew Yeng
   */
  public void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();
    
    int version = FileName.getProjectPshSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectPshSettingsFullPath(project.getName(), version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    
    //Siew Yeng - XCR-3855
    Panel panel = project.getPanel();
    PshSettings pshSettings = new PshSettings(panel);
    panel.setPshSettings(pshSettings);
    
    if (version == 0)
      return;
    
    try
    {
      _projectReader.openFile(_fileName);
      
      if (_projectReader.hasNextLine() == false)
        return;
        
      //selective board side
      String selectiveBoardSideString = _projectReader.getNextLine();
      boolean isEnabledSelectiveBoardSide = getBooleanValue(selectiveBoardSideString);
      pshSettings.setEnableSelectiveBoardSide(isEnabledSelectiveBoardSide);
      
      String pshSideBoardReference = _projectReader.getNextLine();
      PshSettingsBoardSideEnum sideBoardRefereneEnum = PshSettingsBoardSideEnum.getPshSettingsBoardSideEnum(pshSideBoardReference);
      pshSettings.setPshBoardSideReference(sideBoardRefereneEnum);
      
      //selective jointType
      String selectiveJointTypeString = _projectReader.getNextLine();
      boolean isEnabledSelectiveJointType = getBooleanValue(selectiveJointTypeString);
      pshSettings.setEnableSelectiveJointType(isEnabledSelectiveJointType);
      
      String line = _projectReader.getNextLine();
      
      Matcher matcher;
      while(true)
      {
        if(line == null ||
          (line.equals("true") || line.equals("false")))
          break;
        
        pshSettings.setEnabledJointType(JointTypeEnum.getJointTypeEnum(line), false);

        line = _projectReader.getNextLine();
      }
      
      //selective component
      boolean isEnabledSelectiveComponent = getBooleanValue(line);
      pshSettings.setEnableSelectiveComponent(isEnabledSelectiveComponent);
      
      //Siew Yeng - XCR-3834 - recipe unable to open when 2nd load
      if(_projectReader.hasNextLine())
        line = _projectReader.getNextLine();
      else
        line = null;
      
      while(true)
      {
        if(line == null)
          break;
        
        matcher = _pshComponentPattern.matcher(line);
        if(matcher.matches())
        {
          String boardName = matcher.group(1);
          String refDes = matcher.group(2);
          
          if(project.getPanel().hasComponent(boardName, refDes) == false)
          {
            _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
            try 
            {
              _projectErrorLogUtil.log(project.getName(), "Cannot find component: ", line);
            } 
            catch (XrayTesterException ex) 
            {
              System.out.println("Could not write into recipe error log");
            }        
            
            throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
          }
        
          if(_projectReader.hasNextLine())
            line = _projectReader.getNextLine();
          else
            break;
          
          matcher = _neighbourRefDesPattern.matcher(line);
          while(matcher.matches())
          {
            String neighbourRefDes = matcher.group(1);
            if(project.getPanel().hasComponent(boardName, refDes))
            {
              if(project.getPanel().hasComponent(boardName, neighbourRefDes))
              {
                pshSettings.addSelectiveNeighbor(project.getPanel().getComponent(boardName, refDes).getComponentType(), 
                                                 project.getPanel().getComponent(boardName, neighbourRefDes).getComponentType());
              }
              else
              {
                _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
                try 
                {
                  _projectErrorLogUtil.log(project.getName(), "Cannot find component: ", line);
                } 
                catch (XrayTesterException ex) 
                {
                  System.out.println("Could not write into recipe error log");
                }  
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
              }  
            }
            
            if(_projectReader.hasNextLine())
            {
              line = _projectReader.getNextLine();
              matcher = _neighbourRefDesPattern.matcher(line);
            }
            else
            {
              line = null;
              break;
            }
          }
        }
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      //until finish
      _projectReader.goToLastLine();
      _projectReader.closeFile();
      init();
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private boolean getBooleanValue(String boolString)
  {
    if (boolString.equals("true"))
    {
      return true;
    }
    else if (boolString.equals("false"))
    {
      return false;
    }
    Assert.expect(false);
    return false;
  }
}
