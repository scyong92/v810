package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class LibrariesReader
{
  private static LibrariesReader _instance;

  private String _fileName;
  private String _line;
  private LineNumberReader _is = null;
  boolean _hasNextLineWasCalled = false;
  boolean _prevHasNextLineReturnValue;

  private LibrariesReader()
  {
    //do nothing
  }

  /**
   * @return LibrariesReader
   *
   * @author Wei Chin, Chong
   */
  public static synchronized LibrariesReader getInstance()
  {
    if (_instance == null)
      _instance = new LibrariesReader();

    return _instance;
  }

  /**
   * @param librarylist LibraryManager
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  public void read(LibraryManager librarylist) throws DatastoreException
  {
    openFile(FileName.getLibraryListFullPath());

    String line = "";
    if (hasNextLine())
    {
      line = getNextLine();

      if (line.equals(LibraryManager.START_OF_SELECTED_LIBRARIES) == false)
        librarylist.add(line,false);
    }
    while (true)
    {
      if (hasNextLine() == false)
        break;
      line = getNextLine();

      if (line.equals(LibraryManager.START_OF_SELECTED_LIBRARIES))
        break;

      librarylist.add(line,false);
    }

    while (true)
    {
      if (hasNextLine() == false)
        break;
      line = getNextLine();

      librarylist.addSelectedLibrary(line, false);
    }
  }

  /**
   * @param fileName String
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  void openFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    _fileName = fileName;

    if(FileUtil.exists(fileName) == false)
    {
      try
      {
        FileUtil.createEmtpyFile(fileName);
      }
      catch(IOException ioe)
      {
        CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(fileName);
        dex.initCause(ioe);
        throw dex;
      }
    }

    _hasNextLineWasCalled = false;
    try
    {
      _is = new LineNumberReader(new FileReader(_fileName));
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_fileName);
      de.initCause(ex);
      throw de;
    }
  }

  /**
   * @return true if there is a next line that can be parsed
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  boolean hasNextLine() throws DatastoreException
  {
    // if this method was called already, do not parse again until getNextLine() is called
    if (_hasNextLineWasCalled)
      return _prevHasNextLineReturnValue;

    try
    {
      do
      {
        _line = _is.readLine();
        if (_line == null)
        {
          _prevHasNextLineReturnValue = false;
          _hasNextLineWasCalled = false;
          return false;
        }
      }
      while (_line.equals(""));
    }
    catch (IOException ioe)
    {
      closeFileDuringExceptionHandling();
      DatastoreException de = new CannotReadDatastoreException(_fileName);
      de.initCause(ioe);
      throw de;
    }
    _hasNextLineWasCalled = true;
    Assert.expect(_line != null);
    _prevHasNextLineReturnValue = true;
    return true;
  }

  /**
   * @return String, the next valid line that contains something parsable.
   * This method will not return blank lines.
   *
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  String getNextLine() throws DatastoreException
  {
    // check that hasNextLine() was already called.  If it was not then
    // call it automatically.
    if (_hasNextLineWasCalled == false)
    {
      if (hasNextLine() == false)
      {
        // there is no next line so throw an exception
        int lineNumber = getLineNumber();
        closeFileDuringExceptionHandling();
        throw new FileCorruptDatastoreException(_fileName, lineNumber + 1);
      }
    }
    _hasNextLineWasCalled = false;

    Assert.expect(_line != null);
    return _line;
  }

  /**
   * @return int
   *
   * @author Wei Chin, Chong
   */
  int getLineNumber()
  {
    Assert.expect(_is != null);

    int lineNumber = _is.getLineNumber();
    if (_hasNextLineWasCalled)
      --lineNumber;

    return lineNumber;
  }

  /**
   * @author Wei Chin, Chong
   */
  void closeFileDuringExceptionHandling()
  {
    if (_is != null)
    {
      try
      {
        _is.close();
        _is = null;
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }
  }
}
