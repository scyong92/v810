package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 *
 * @author sham
 */
public class SubtypeAdvanceSettingsReader
{

  private static SubtypeAdvanceSettingsReader _instance;
  private ProjectReader _projectReader;
  private String _fileName;
  private EnumStringLookup _enumStringLookup;
  // Version 1
  // subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType canUseVariableMagnification
  private Pattern _subtypePattern1 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)\\s+([^\\s]+)$");
  private Pattern _subtypePattern2 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)\\s+([^\\s]+)\\s+(true|false)$");
  private Pattern _subtypePattern3 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0|10.0)$");
  private Pattern _subtypePattern4 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)\\s+([^\\s]+)\\s+(true|false)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0|10.0)\\s+(1.0|1.5|2.0|2.5|3.0|3.5|4.0|4.5|5.0|5.5|6.0|6.5|7.0|7.5|8.0|8.5|9.0|136.0)$");
  
  /**
   * @author Bill Darbie
   */
  static synchronized SubtypeAdvanceSettingsReader getInstance()
  {
    if(_instance == null)
    {
      _instance = new SubtypeAdvanceSettingsReader();
    }

    return _instance;
  }

  /**
   * @author sham
   */
  private SubtypeAdvanceSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * author sham
   * @param projectName
   * @return boolean
   */
  public boolean isCurrentVersion(String projectName)
  {
    Assert.expect(projectName != null);

    int fileVersion = FileName.getProjectSubtypeAdvanceSettingsLatestFileVersion();
    _fileName = FileName.getProjectSubtypeAdvanceSettingsFullPath(projectName,fileVersion);

    return FileUtilAxi.exists(_fileName);
  }

  /**
   * @author Yong Sheng Chuan
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if(_projectReader == null)
      _projectReader = ProjectReader.getInstance();
    
    Panel panel = project.getPanel();
    String projName = project.getName();

    int fileVersion = FileName.getProjectSubtypeAdvanceSettingsLatestFileVersion();
    while (fileVersion > 0)
    {
      _fileName = FileName.getProjectSubtypeAdvanceSettingsFullPath(projName,fileVersion);
      if(FileUtilAxi.exists(_fileName))
      {
        break;
      }
      --fileVersion;
    }
    
    if(fileVersion < 1)
      return;
    
    try
    {
      _projectReader.openFile(_fileName);
      // subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType
      String line = _projectReader.getNextLine();
      
      while(true)
      {
        String subtypeNameStr = null;
        String signalCompensationStr = null;
        String artifactCompensationStr = null;
        String isInterferenceCompensatableStr = null;
        String userGainStr = null;
        String magnificationTypeStr = null;
        String canUseVariableMagnificationStr = null;
        String droLevelStr = null;
        String stageSpeedStr = null;
        
        Matcher matcher = null;
        if(_subtypePattern4.matcher(line).matches())
        {
          matcher = _subtypePattern4.matcher(line);
          matcher.matches();
          subtypeNameStr = matcher.group(1);
          signalCompensationStr = matcher.group(2);
          artifactCompensationStr = matcher.group(3);
          isInterferenceCompensatableStr = matcher.group(4);
          userGainStr = matcher.group(5);
          magnificationTypeStr = matcher.group(6);
          canUseVariableMagnificationStr = matcher.group(7);
          droLevelStr = matcher.group(8);
          stageSpeedStr = matcher.group(9);
        }
        else if(_subtypePattern3.matcher(line).matches())
        {
          matcher = _subtypePattern3.matcher(line);
          matcher.matches();
          subtypeNameStr = matcher.group(1);
          signalCompensationStr = matcher.group(2);
          artifactCompensationStr = matcher.group(3);
          isInterferenceCompensatableStr = matcher.group(4);
          userGainStr = matcher.group(5);
          magnificationTypeStr = matcher.group(6);
          canUseVariableMagnificationStr = matcher.group(7);
          droLevelStr = matcher.group(8);
        }
        else if(_subtypePattern2.matcher(line).matches())
        {
          matcher = _subtypePattern2.matcher(line);
          matcher.matches();
          subtypeNameStr = matcher.group(1);
          signalCompensationStr = matcher.group(2);
          artifactCompensationStr = matcher.group(3);
          isInterferenceCompensatableStr = matcher.group(4);
          userGainStr = matcher.group(5);
          magnificationTypeStr = matcher.group(6);
          canUseVariableMagnificationStr = matcher.group(7);
        }
        else
        {
          matcher = _subtypePattern1.matcher(line);
          matcher.matches();
          subtypeNameStr = matcher.group(1);
          signalCompensationStr = matcher.group(2);
          artifactCompensationStr = matcher.group(3);
          isInterferenceCompensatableStr = matcher.group(4);
          userGainStr = matcher.group(5);
          magnificationTypeStr = matcher.group(6);
        }
        
        //Khaw Chek Hau - XCR3355: Software assert when open certain recipe which has both subtype.4.settings and subtype.5.setting file
        if (panel.doesSubtypeExist(subtypeNameStr) == false)
        {
          String subtypeSettingVer5FileName = FileName.getProjectSubtypeSettingsFullPath(projName, 5);
          String subtypeSettingVer4FileName = FileName.getProjectSubtypeSettingsFullPath(projName, 4);
          
          if (FileUtilAxi.exists(subtypeSettingVer5FileName) && FileUtilAxi.exists(subtypeSettingVer4FileName))
          {
            SubtypeSettingFileVersionNotCompatibleDatastoreException ex = new SubtypeSettingFileVersionNotCompatibleDatastoreException(subtypeNameStr, 
                                                                                                                                       subtypeSettingVer5FileName, 
                                                                                                                                       subtypeSettingVer4FileName);
            throw ex;
          }          
        }
        
        Subtype subtype = panel.getSubtype(subtypeNameStr);
        SubtypeAdvanceSettings subtypeAdvanceSettings = new SubtypeAdvanceSettings();
        subtype.setSubtypeAdvanceSettings(subtypeAdvanceSettings);
        subtypeAdvanceSettings.setSubtype(subtype);
        if (signalCompensationStr != null)
          subtypeAdvanceSettings.setSignalCompensation(_enumStringLookup.getSignalCompensationEnum(signalCompensationStr));
        if (artifactCompensationStr != null)
          subtypeAdvanceSettings.setArtifactCompensationState(getArtifactCompensationState(artifactCompensationStr));
        if (isInterferenceCompensatableStr != null)
          subtypeAdvanceSettings.setIsInterferenceCompensatable(isTrueOrFalseString(isInterferenceCompensatableStr));
        if (userGainStr != null)
          subtypeAdvanceSettings.setUserGain(_enumStringLookup.getUserGainEnum(userGainStr));
        if (magnificationTypeStr != null)
          subtypeAdvanceSettings.setMagnificationType(_enumStringLookup.getMagnificationTypeEnum(magnificationTypeStr));
        if (canUseVariableMagnificationStr != null)
          subtypeAdvanceSettings.setCanUseVariableMagnification(isTrueOrFalseString(canUseVariableMagnificationStr));
        if (droLevelStr != null)
          subtypeAdvanceSettings.setDynamicRangeOptimizationLevel(_enumStringLookup.getDynamicRangeOptimizationLevelEnum(droLevelStr));
        
        List<StageSpeedEnum> speedSettingList = new ArrayList<StageSpeedEnum>();
        if (stageSpeedStr != null && fileVersion <= 4)
          speedSettingList.add(_enumStringLookup.getstageSpeedEnum(stageSpeedStr));
        else
          speedSettingList.add(StageSpeedEnum.ONE);
          
        subtypeAdvanceSettings.setStageSpeedEnum(speedSettingList);
                
        if (_projectReader.hasNextLine())
          line = _projectReader.getNextLine();
        else
          break;
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
  }

  /**
   * @author George A. David
   */
  private ArtifactCompensationStateEnum getArtifactCompensationState(String shadingCompensationString) throws DatastoreException
  {
    Assert.expect(shadingCompensationString!=null);

    ArtifactCompensationStateEnum allowCompensation = null;
    if(shadingCompensationString.equals("compensated"))
      allowCompensation = ArtifactCompensationStateEnum.COMPENSATED;
    else if(shadingCompensationString.equals("notCompensated"))
      allowCompensation = ArtifactCompensationStateEnum.NOT_COMPENSATED;
    else if(shadingCompensationString.equals("noneSpecified"))
      allowCompensation = ArtifactCompensationStateEnum.NOT_COMPENSATED;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return allowCompensation;
  }

  /**
   * author sham
   */
  private boolean isTrueOrFalseString(String trueOrFalseString)
  {
    Assert.expect(trueOrFalseString!=null);

    boolean trueOrFalse = false;
    if(trueOrFalseString.equals("true"))
    {
      trueOrFalse = true;
    }
    else if(trueOrFalseString.equals("false"))
    {
      trueOrFalse = false;
    }

    return trueOrFalse;
  }
}
