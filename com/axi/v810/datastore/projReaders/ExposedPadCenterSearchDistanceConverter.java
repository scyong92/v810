package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import java.util.List;
import java.util.ArrayList;

/**
 * The original version of the algorithm had a single Center Search Distance for Across and Along profiles but
 * separate values are desired. If the user set a non-default value, it will be copied to the across and along
 * settings defined here.
 *
 * @author George Booth
 */
class ExposedPadCenterSearchDistanceConverter extends TunedSettingConverter
{

  ArrayList<AlgorithmSettingEnum> _algorithmSettingEnumList;

  /**
   * @author George Booth
   */
  ExposedPadCenterSearchDistanceConverter()
  {
    // store the old EnumStringLookup string for the setting that we want to convert
    super( "exposedPadMeasurementCenterSearchDistance", true );
  }

  /**
   * Not used for this converter. Should not be called.
   * @author George Booth
   */
  Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(Serializable settingValue,
                                                                        Subtype subtype)
  {
    Assert.expect(false, "Bad call to ExposedPadCenterSearchDistanceConverter.getConvertedAlgorithmSetting()");
    return null;
  }

  /**
   * @author George Booth
   */
  ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList()
  {
    _algorithmSettingEnumList = new ArrayList<AlgorithmSettingEnum>();
    _algorithmSettingEnumList.add(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS);
    _algorithmSettingEnumList.add(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG);
    return _algorithmSettingEnumList;
  }
}
