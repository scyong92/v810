package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Peter Esbensen
 */
class ThroughHoleBarrelSliceSettingConverter extends TunedSettingConverter
{
  // the old settings are in this format:  "Barrel @ 10%"
  private Pattern _barrelSliceSettingPattern = Pattern.compile("Barrel @ ([-+]?[0-9]+)%");

  /**
   * @author Peter Esbensen
   */
  ThroughHoleBarrelSliceSettingConverter()
  {
    // store the old EnumStringLookup string for the setting that we want to convert
    super( "throughholeBarrelSlice", true );
  }

  /**
   * @author Peter Esbensen
   */
  Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(Serializable settingValue,
                                                                        Subtype subtype)
  {
    Assert.expect(settingValue != null);
    Assert.expect(subtype != null);

    Float valueFromObsoleteSetting = getBarrelHeightAsPercentBarrelFromOldThroughHoleSetting((String)settingValue);

    AlgorithmSettingEnum newAlgorithmSettingEnum = AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT;
    Serializable newValue = valueFromObsoleteSetting;

    return new Pair<AlgorithmSettingEnum, Serializable>(newAlgorithmSettingEnum, newValue);
  }

  /**
   * Get the barrel height percentage from the obsolete Through Hole barrel height setting.
   * This is based on the hardcoded name for the obsolete setting enums.
   *
   * @author Peter Esbensen
   */
  private float getBarrelHeightAsPercentBarrelFromOldThroughHoleSetting(String settingValue)
  {
    // Pull out the number from the string, which will be of the form:
    //    Barrel @ 10%
    String sliceHeightPercentString = null;
    Matcher matcher = _barrelSliceSettingPattern.matcher(settingValue);
    if (matcher.matches())
    {
      sliceHeightPercentString = matcher.group(1);
    }
    else
    {
      /** @todo PE should this be an assert or handled more gracefully? */
      Assert.expect(false, "unexpected format for PTH barrel slice setting in subtype.settings");
    }

    Assert.expect(sliceHeightPercentString != null);

    float sliceHeightPercent = Float.valueOf(sliceHeightPercentString);
    return sliceHeightPercent;
  }

  /**
   * Not used for this converter. Should not be called.
   * @author George Booth
   */
  ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList()
  {
    Assert.expect(false, "Bad call to ThroughHoleBarrelSliceSettingConverter.getSplitAlgorithmSettingList()");
    return null;
  }

}
