package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class ProjectSummaryReader
{
  private static ProjectSummaryReader _instance;

  private ProjectReader _projectReader;

  // targetCustomerName: name
  private Pattern _targetCustomerNamePattern = Pattern.compile("^\\s*targetCustomerName:(.*)$");
  // programmerName: name
  private Pattern _programmerNamePattern = Pattern.compile("^\\s*programmerName:(.*)$");
  private String _fileName;
  private int _version;

  /**
   * @author Bill Darbie
   */
  public static synchronized ProjectSummaryReader getInstance()
  {
    if (_instance == null)
      _instance = new ProjectSummaryReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectSummaryReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  private ProjectSummary readFile(String projectName, String fileName, boolean isDatabase) throws DatastoreException
  {
    ProjectSummary projectSummary = new ProjectSummary();
    projectSummary.setProjectName(projectName);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    try
    {
      //if version less than 3, read the system type from project setting first
      String systemType = "";
      if (_version < 3)
      {
        if (isDatabase)
        {
          systemType = SystemTypeEnum._UNKNONWN_TYPE;
        }
        else
        {
          systemType = ProjectSettingsReader.getInstance().readSystemType(projectName).getName();
        }
      }
      
      _projectReader.openFile(fileName);

      String versionStr = _projectReader.getNextLine();
      double version = StringUtil.convertStringToDouble(versionStr);
      projectSummary.setVersion(version);

      String lastModificationTimeInMillisStr = _projectReader.getNextLine();
      long lastModificationTimeInMillis = StringUtil.convertStringToLong(lastModificationTimeInMillisStr);
      projectSummary.setLastModificationTimeInMillis(lastModificationTimeInMillis);

      String line = _projectReader.getNextLine();
      Matcher matcher = _targetCustomerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      String targetCustomerName = matcher.group(1);
      projectSummary.setTargetCustomerName(targetCustomerName);

      line = _projectReader.getNextLine();
      matcher = _programmerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      String programmerName = matcher.group(1);
      projectSummary.setProgrammerName(programmerName);

      String notes = _projectReader.readMultipleLinesOfText("notesBegin", "notesEnd");
      projectSummary.setProjectNotes(notes);

      String numBoardTypesStr = _projectReader.getNextLine();
      int numBoardTypes = StringUtil.convertStringToInt(numBoardTypesStr);
      projectSummary.setNumBoardTypes(numBoardTypes);

      String numBoardsStr = _projectReader.getNextLine();
      int numBoards = StringUtil.convertStringToInt(numBoardsStr);
      projectSummary.setNumBoards(numBoards);

      for(int i = 0; i < numBoards; i++)
      {
        line = _projectReader.getNextLine();
        projectSummary.addBoardName(line);
      }

      String numComponentsStr = _projectReader.getNextLine();
      int numComponents = StringUtil.convertStringToInt(numComponentsStr);
      projectSummary.setNumComponents(numComponents);

      String numJointsStr = _projectReader.getNextLine();
      int numJoints = StringUtil.convertStringToInt(numJointsStr);
      projectSummary.setNumJoints(numJoints);

      String numInspectedJointsStr = _projectReader.getNextLine();
      int numInspectedJoints = StringUtil.convertStringToInt(numInspectedJointsStr);
      projectSummary.setNumInspectedJoints(numInspectedJoints);

      //XCR1374, KhangWah, 2011-09-12
      if(_version >= 2) //project.2.summary exist, read untestable joint count from the summary file
      {
          String numUnTestableJointsStr = _projectReader.getNextLine();
          int numUnTestableJoints = StringUtil.convertStringToInt(numUnTestableJointsStr);
          projectSummary.setNumUnTestableJoints(numUnTestableJoints);
      }
      else //project.2.summary did not exist, set the untestable joint count to 0
      {
          projectSummary.setNumUnTestableJoints(0);
      }
      
      String cadXmlCheckSumStr = _projectReader.getNextLine();
      long cadXmlCheckSum = StringUtil.convertStringToLong(cadXmlCheckSumStr);
      projectSummary.setCadXmlCheckSum(cadXmlCheckSum);

      String settingsXmlCheckSumStr = _projectReader.getNextLine();
      long settingsXmlCheckSum = StringUtil.convertStringToLong(settingsXmlCheckSumStr);
      projectSummary.setSettingsXmlCheckSum(settingsXmlCheckSum);
      
      if (_version >= 3)
      {
        systemType = _projectReader.getNextLine();
        projectSummary.setSystemType(systemType);
      }
      else
      {
        projectSummary.setSystemType(systemType);
      }
      

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }

    return projectSummary;

  }
  
  /**
   * @author George A. David
   * @author Chnee Khang Wah, XCR1374
   */
  public ProjectSummary read(String projectName, int databaseVersion) throws DatastoreException
  {
    //get project summary version
    int version = FileName.getProjectSummaryLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getProjectDatabaseSummaryFileFullPath(projectName, databaseVersion, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    _version = version;
        
    ProjectSummary summary = readFile(projectName, _fileName, true);
    summary.setDatabaseVersion(databaseVersion);

    return summary;
  }

  /**
   * @author Bill Darbie
   * @author Chnee Khang Wah, XCR1374
   */
  public ProjectSummary read(String projectName) throws DatastoreException
  {
    int version = FileName.getProjectSummaryLatestFileVersion();
    
    while (version > 0)
    {
      _fileName = FileName.getProjectSummaryFullPath(projectName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }
    _version = version;

    return readFile(projectName, _fileName, false);
  }
}
