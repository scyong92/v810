package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Creates LandPattern, SurfaceMountLandPatternPad, ThroughHoleLandPatternPad
 * @author Bill Darbie
 */
class LandPatternReader
{
  private static LandPatternReader _instance;

  private ProjectReader _projectReader;
  // padNameOrNumber1 xLocation yLocation width length rectangle|circle rotation surfaceMount|throughHole [xHoleLocation yHoleLocation holeDiameter]
  private Pattern _padPattern = Pattern.compile("^([^\\s]+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(rectangle|circle)\\s+(-?\\d+(\\.\\d+)?)\\s+(surfaceMount|throughHole)(\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+))?$");
  //Siew Yeng - XCR-3318 - Oval PTH
  // padNameOrNumber1 xLocation yLocation width length rectangle|circle rotation surfaceMount|throughHole [xHoleLocation yHoleLocation holeWidth holeLength]
  private Pattern _padPattern2 = Pattern.compile("^([^\\s]+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(rectangle|circle)\\s+(-?\\d+(\\.\\d+)?)\\s+(surfaceMount|throughHole)(\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+))?$");
  // landPatternName
  private Pattern _landPatternNameAndUserAssignedPadOnePattern = Pattern.compile("^([^\\s]+)(\\s+([^\\s]+))?$");

  /**
   * @author Bill Darbie
   */
  static synchronized LandPatternReader getInstance()
  {
    if (_instance == null)
      _instance = new LandPatternReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private LandPatternReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @return a Map of landPatternName to LandPattern objects.
   * @author Bill Darbie
   */
  Map<String, LandPattern> read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    Map<String, LandPattern> landPatternNameToLandPatternMap = new HashMap<String, LandPattern>();
    init();

    String projName = project.getName();
    String fileName = null;
    int version = FileName.getProjectLandPatternLatestFileVersion();
//    Assert.expect(version == 1);
//    String fileName = FileName.getProjectLandPatternFullPath(projName, version);
    //Siew Yeng - XCR-3318 - Oval PTH
    while (version > 0)
    {
      fileName = FileName.getProjectLandPatternFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }

    try
    {
      _projectReader.openFile(fileName);

      String line = null;
      if (_projectReader.hasNextLine())
        line = _projectReader.getNextLine();

      while (true)
      {
        if (_projectReader.hasNextLine() == false)
          break;

        String landPatternName;
        String userAssignedLandPatternPadName;
        Matcher matcher = _landPatternNameAndUserAssignedPadOnePattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
        else
        {
          landPatternName = matcher.group(1);
          userAssignedLandPatternPadName = matcher.group(3);
        }

        LandPattern landPattern = new LandPattern();
        landPattern.setPanel(project.getPanel());
        landPattern.setName(landPatternName);

        LandPattern prevValue = landPatternNameToLandPatternMap.put(landPatternName, landPattern);
        if (prevValue != null)
        {
          // the same landPatternName should not be in the file more than once
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
        }

        while (true)
        {
          if (_projectReader.hasNextLine() == false)
          {
            // we are at the end of the file
            break;
          }

          line = _projectReader.getNextLine();
          //matcher = _padPattern.matcher(line);
          //Siew Yeng - XCR-3318
          switch(version)
          {
            case 1: 
              matcher = _padPattern.matcher(line);
              break;
            case 2:
              matcher = _padPattern2.matcher(line);
              break;
          }


          if (matcher.matches() == false)
          {
            // if we are here then we did not match on the landPattern line, so we are at the
            // next landPatternName
            break;
          }

          // padNameOrNumber1 xLocation yLocation width length rectangle|circle rotation surfaceMount|throughHole [xHoleLocation yHoleLocation holeDiameter]
          String padNameOrNumber = matcher.group(1);
          String xInNanosStr = matcher.group(2);
          int xInNanos = StringUtil.convertStringToInt(xInNanosStr);
          String yInNanosStr = matcher.group(3);
          int yInNanos = StringUtil.convertStringToInt(yInNanosStr);
          String widthInNanosStr = matcher.group(4);
          int widthInNanos = StringUtil.convertStringToInt(widthInNanosStr);
          String lengthInNanosStr = matcher.group(5);
          int lengthInNanos = StringUtil.convertStringToInt(lengthInNanosStr);
          String rectangleOrCircle = matcher.group(6);
          String degreesRotationStr = matcher.group(7);
          double degreesRotation = StringUtil.convertStringToDouble(degreesRotationStr);
          String surfaceMountOrThroughHole = matcher.group(9);
          int xHoleInNanos = -1;
          int yHoleInNanos = -1;
//          int holeDiamInNanos = -1;
          //Siew Yeng - XCR-3318 - Oval PTH
          int holeWidthInNanos = -1;
          int holeLengthInNanos = -1;
          if (surfaceMountOrThroughHole.equals("throughHole"))
          {
            String xHoleInNanosStr = matcher.group(11);
            xHoleInNanos = StringUtil.convertStringToInt(xHoleInNanosStr);
            String yHoleInNanosStr = matcher.group(12);
            yHoleInNanos = StringUtil.convertStringToInt(yHoleInNanosStr);
            
            if(version == 1)
            {
              String holeDiamInNanosStr = matcher.group(13);
              holeWidthInNanos = holeLengthInNanos = StringUtil.convertStringToInt(holeDiamInNanosStr);
            }
            else if(version == 2)
            {
              String holeWidthInNanosStr = matcher.group(13);
              holeWidthInNanos = StringUtil.convertStringToInt(holeWidthInNanosStr);
              
              String holeLengthInNanosStr = matcher.group(14);
              holeLengthInNanos = StringUtil.convertStringToInt(holeLengthInNanosStr);
            }
          }

          LandPatternPad landPatternPad = null;
          if (surfaceMountOrThroughHole.equals("surfaceMount"))
            landPatternPad = new SurfaceMountLandPatternPad();
          else if (surfaceMountOrThroughHole.equals("throughHole"))
            landPatternPad = new ThroughHoleLandPatternPad();
          else
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          landPatternPad.setLandPattern(landPattern);
          landPatternPad.setName(padNameOrNumber);
          landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(xInNanos, yInNanos));
          landPatternPad.setWidthInNanoMeters(widthInNanos);
          landPatternPad.setLengthInNanoMeters(lengthInNanos);
          landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(degreesRotation);
          if (rectangleOrCircle.equals("circle"))
            landPatternPad.setShapeEnum(ShapeEnum.CIRCLE);
          else if (rectangleOrCircle.equals("rectangle"))
            landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
          else
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          if (landPatternPad instanceof ThroughHoleLandPatternPad)
          {
            ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
            thLandPatternPad.setHoleCoordinateInNanoMeters(new ComponentCoordinate(xHoleInNanos, yHoleInNanos));
//            thLandPatternPad.setHoleDiameterInNanoMeters(holeDiamInNanos);
            //Siew Yeng - XCR-3318 - Oval PTH
            thLandPatternPad.setHoleWidthInNanoMeters(holeWidthInNanos);
            thLandPatternPad.setHoleLengthInNanoMeters(holeLengthInNanos);
          }

          if ((userAssignedLandPatternPadName != null) && (userAssignedLandPatternPadName.equals(padNameOrNumber)))
          {
            landPattern.setUserAssignedPadOneForReaders(landPatternPad);
          }
        }
      }

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }

    return landPatternNameToLandPatternMap;
  }
}
