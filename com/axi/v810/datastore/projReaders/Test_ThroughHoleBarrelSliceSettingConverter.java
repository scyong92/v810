package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

class Test_ThroughHoleBarrelSliceSettingConverter extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ThroughHoleBarrelSliceSettingConverter());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TunedSettingConversionEngine tunedSettingConversionEngine = new TunedSettingConversionEngine();

    Subtype subtype = new Subtype();
    String oldAlgorithmSettingEnumString = "throughholeBarrelSlice";

    // make sure conversionRequired() works
    boolean conversionRequired = tunedSettingConversionEngine.conversionRequired( oldAlgorithmSettingEnumString );

    Expect.expect(conversionRequired);

    // try converting Barrel @ 10
    Serializable oldValue = "Barrel @ 10%";
    Pair< AlgorithmSettingEnum, Serializable > result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                        oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), 10.0f));

    // try converting Barrel @ 50
    oldValue = "Barrel @ 50%";
    result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                       oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), 50.0f));

  }
}
