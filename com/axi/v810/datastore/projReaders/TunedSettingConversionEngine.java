package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.projWriters.EnumStringLookup;

/**
 * This class handles all conversions of tuned algorithm settings in subtype.x.settings.
 *
 * @author Peter Esbensen
 */
class TunedSettingConversionEngine
{
  static private HashSet<TunedSettingConverter> _TUNED_SETTING_CONVERTERS = new HashSet<TunedSettingConverter>();
  // The Strings in these collections are from the EnumStringLookup utility class.  They are NOT the user visible setting names.
  static private HashMap<String, TunedSettingConverter> _ALGO_SETTING_UNIQUE_STRING_TO_CONVERTER_MAP = new HashMap<String,TunedSettingConverter>();
  static private HashSet<String> _OBSOLETE_ALGORITHM_SETTING_UNIQUE_STRINGS_TO_IGNORE = new HashSet<String>();
  static private HashSet<String> _ALGORITHM_SETTING_UNIQUE_STRINGS_TO_SPLIT = new HashSet<String>();

  static private EnumStringLookup _enumStringLookup = EnumStringLookup.getInstance();

  /**
   * @author Peter Esbensen
   */
  private static void addTunedSettingConverters()
  {
    _TUNED_SETTING_CONVERTERS.add( new ThroughHoleBarrelSliceSettingConverter() );
    _TUNED_SETTING_CONVERTERS.add( new ThroughHoleExtrusionSliceSettingConverter() );
    _TUNED_SETTING_CONVERTERS.add( new PressfitBarrelSliceSettingConverter() );
    _TUNED_SETTING_CONVERTERS.add( new VariableHeightConnectorPadSliceSettingConverter() );
    _TUNED_SETTING_CONVERTERS.add( new ExposedPadCenterSearchDistanceConverter() );
  }

  /**
   * @author George Booth
   */
  private static void addSettingsToSplit()
  {
    // add the settings we want to split here
    _ALGORITHM_SETTING_UNIQUE_STRINGS_TO_SPLIT.add("exposedPadMeasurementCenterSearchDistance");
  }

  /**
   * @author Peter Esbensen
   */
  private static void addObsoleteSettingsToIgnore()
  {
    // add the settings we want to ignore here
    // _OBSOLETE_ALGORITHM_SETTING_UNIQUE_STRINGS_TO_IGNORE.add();  nothing yet to obsolete
    }

  /**
   * ADD YOUR TUNED_SETTING_CONVERTERS AND YOUR OBSOLETE SETTINGS HERE!
   *
   * @author Peter Esbensen
   */
  static
  {
    // add the TunedSettingConverters
    addTunedSettingConverters();

    // add the settings we want to ignore
    addSettingsToSplit();

    // add the settings we want to ignore
    addObsoleteSettingsToIgnore();

    // this is just for convenience, we'll create a map of the TunedSettingConverters keyed by their associated AlgorithmSettingEnum
    // when adding new TunedSettingConverters, don't need to modify this
    for (TunedSettingConverter tunedSettingConverter : _TUNED_SETTING_CONVERTERS)
    {
      EnumStringLookup enumStringLookup = EnumStringLookup.getInstance();
      _ALGO_SETTING_UNIQUE_STRING_TO_CONVERTER_MAP.put( tunedSettingConverter.getAlgorithmSettingEnumString(),
                                                        tunedSettingConverter );
    }

    // now do a sanity check:  all obsolete settings should be in the EnumStringLookup class' list of obsoleted settings
    // so that nobody can use them again in the future
    EnumStringLookup enumStringLookup = EnumStringLookup.getInstance();
    for (String obsoleteString : _OBSOLETE_ALGORITHM_SETTING_UNIQUE_STRINGS_TO_IGNORE)
    {
      Assert.expect( enumStringLookup.algorithmSettingEnumStringIsObsolete(obsoleteString) );
    }
    for (TunedSettingConverter tunedSettingConverter : _TUNED_SETTING_CONVERTERS)
    {
      if (tunedSettingConverter.oldSettingNoLongerExists())
      {
        Assert.expect( enumStringLookup.algorithmSettingEnumStringIsObsolete(tunedSettingConverter.getAlgorithmSettingEnumString()) );
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  boolean conversionRequired(String algorithmSettingEnumString)
  {
    Assert.expect(algorithmSettingEnumString != null);
    return _ALGO_SETTING_UNIQUE_STRING_TO_CONVERTER_MAP.containsKey(algorithmSettingEnumString);
  }

  /**
   * @author George Booth
   */
  boolean splitIsRequired(String algorithmSettingEnumString)
  {
    Assert.expect(algorithmSettingEnumString != null);
    return _ALGORITHM_SETTING_UNIQUE_STRINGS_TO_SPLIT.contains(algorithmSettingEnumString);
  }

  /**
   * @author Peter Esbensen
   */
  boolean settingIsObsoleteDoNotConvert(String algorithmSettingEnumString)
  {
    Assert.expect(algorithmSettingEnumString != null);
    return _OBSOLETE_ALGORITHM_SETTING_UNIQUE_STRINGS_TO_IGNORE.contains(algorithmSettingEnumString);
  }

  /**
   * Take the given algorithm setting enum and its value and convert it.  Returns a pair consisting of the new algorithm
   * setting enum and the new converted value.
   *
   * @author Peter Esbensen
   */
  Pair<AlgorithmSettingEnum, Serializable> getConvertedAlgorithmSetting(String algorithmSettingEnumString,
                                                                        Serializable settingValue,
                                                                        Subtype subtype)
  {
    Assert.expect(algorithmSettingEnumString != null);
    Assert.expect(settingValue != null);
    Assert.expect(subtype != null);

    // get the appropriate tuned setting converter
    TunedSettingConverter tunedSettingConverter = _ALGO_SETTING_UNIQUE_STRING_TO_CONVERTER_MAP.get(algorithmSettingEnumString);
    Assert.expect(tunedSettingConverter != null); // you should only call getConvertedAlgorithmSetting() if conversionRequired() returns true AND settingIsObsoleteDoNotConvert() returns false

    // do the conversion
    Pair<AlgorithmSettingEnum, Serializable> result = tunedSettingConverter.getConvertedAlgorithmSetting(settingValue,
                                                                                                         subtype);

    return result;
  }

  /**
   * Return a list of AlgorithmSettingEnum that will have the same value as algorithmSettingEnumString.
   *
   * @author George Booth
   */
  ArrayList<AlgorithmSettingEnum> getSplitAlgorithmSettingList(String algorithmSettingEnumString)
  {
    Assert.expect(algorithmSettingEnumString != null);

    // get the appropriate tuned setting converter
    TunedSettingConverter tunedSettingConverter = _ALGO_SETTING_UNIQUE_STRING_TO_CONVERTER_MAP.get(algorithmSettingEnumString);
    Assert.expect(tunedSettingConverter != null);

    // do the conversion
    ArrayList<AlgorithmSettingEnum> result = tunedSettingConverter.getSplitAlgorithmSettingList();

    return result;
  }

}
