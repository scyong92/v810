/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.projReaders;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
 *
 * @author weng-jian.eoh
 */
public class MeasurementJointHighlightSettingsReader
{

  private String _filePath;
  private List<String> _defectiveComponent = new ArrayList<String>();

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public MeasurementJointHighlightSettingsReader(String projectName) throws DatastoreException
  {
    _filePath = FileName.getProjectMeasurementJointHighlightSettingFullPath(projectName);
    readFile();
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void readFile() throws DatastoreException
  {
    _defectiveComponent.clear();

    if (FileUtilAxi.exists(_filePath))
    {
      String inputFileText = FileReaderUtilAxi.readFileContents(_filePath);
      String[] lines = inputFileText.split("\\n");
      int lineNumber = 0;

      for (String line : lines)
      {
        ++lineNumber;

        // # is a comment
        if (line.startsWith("#"))
        {
          continue;
        }

        if (line.isEmpty() == false)
        {
          String[] data = line.split(",");
          if (data.length == 3)
          {
            _defectiveComponent.add(line);
          }
          else
          {
            _defectiveComponent.clear();
            return;
          }
        }
      }
    }
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public List<String> getDefectiveSetting()
  {
    return _defectiveComponent;
  }
}
