package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelAlignmentSurfaceMapSettingsReader 
{
    public static String _COMMA_DELIMITER = ",";
    
    private static PanelAlignmentSurfaceMapSettingsReader _instance;
    
    private String _fileName;
    
    private ProjectReader _projectReader;
    // alignmentGroupName
    private Pattern _alignmentGroupNamePattern = Pattern.compile("^([^\\s]+)\\s+(yes|no)$");
    //Point StageXCoordinateInNanometers StageYCoordinateInNanometers Yes|No
    private Pattern _regionWithPanelPositionCoordinatePattern = Pattern.compile("^Region\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+([^\\s]+)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)$");
    //private Pattern _regionWithPanelPositionCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)$");
     //CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel
    private Pattern _cameraRectWithPanelPositionCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)$");
    //CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel
    private Pattern _cameraRectWithPanelPositionCoordinatePatternAndStatusPattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(true|false)\\s+(\\d+\\.\\d*)$");
    //private Pattern _pointWithStageCoordinatePattern = Pattern.compile("^\\s+Point\\s+(\\d*)\\s+(\\d*)\\s+(yes|no)$");
    private Pattern _pointWithPanelPositionCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(yes|no)$");
     
    /**
     * @author Cheah Lee Herng
     */
    static synchronized PanelAlignmentSurfaceMapSettingsReader getInstance()
    {
        if (_instance == null)
          _instance = new PanelAlignmentSurfaceMapSettingsReader();

        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void init()
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
    */
    void read(Project project) throws DatastoreException
    {
      Assert.expect(project != null);
        
      if (_projectReader == null)
       _projectReader = ProjectReader.getInstance();

      init();

      String projName = project.getName();
        
      _fileName = "";
      int version = FileName.getProjectPanelAlignmentSurfaceMapSettingsLatestFileVersion();
      while (version > 0)
      {
        _fileName = FileName.getProjectPanelAlignmentSurfaceMapSettingsFullPath(projName, version);
        if (FileUtilAxi.exists(_fileName))
          break;
        --version;
      }
        
      // We need to handle backward-compatibility issue.
      if (version == 0)
      {
          Panel panel = project.getPanel();
          PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = new PanelAlignmentSurfaceMapSettings();
          panel.setPanelAlignmentSurfaceMapSettings(panelAlignmentSurfaceMapSettings);            
      }
      else
      {
          try
          {
            _projectReader.openFile(_fileName);

            Panel panel = project.getPanel();
            PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = new PanelAlignmentSurfaceMapSettings();
            panel.setPanelAlignmentSurfaceMapSettings(panelAlignmentSurfaceMapSettings);
              
            if (_projectReader.hasNextLine() == false)
                return;

            String line = _projectReader.getNextLine();          
            while (true)
            {
                if (line == null)
                    break;
                
                Matcher matcher = _alignmentGroupNamePattern.matcher(line);
                String alignmentGroupName = null;
                String setToUsePspString = null;
                boolean setToUsePsp = false;
                if (matcher.matches())
                {
                    alignmentGroupName = matcher.group(1);
                    setToUsePspString = matcher.group(2);

                    if (setToUsePspString.equals("yes"))
                        setToUsePsp = true;
                    else if (setToUsePspString.equals("no"))
                        setToUsePsp = false;
                    else
                        Assert.expect(false);
                }
                else
                    throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                
                if (matcher.matches())
                {
                    if (_projectReader.hasNextLine())
                      line = _projectReader.getNextLine();

                    if (line == null)
                      break;
                    
                    // figure out which type of line we have
                    int lineType = 0;
                    matcher = _regionWithPanelPositionCoordinatePattern.matcher(line);
                    if (matcher.matches())
                    {
                        lineType = 1;
                    }
                    if (lineType == 0) 
                    {
                        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                    }

                    if (lineType == 1)
                    {
                       OpticalRegion opticalRegion = new OpticalRegion();

                      //Region PanelPositionXCoordinateInNanometers PanelPositionYCoordinateInNanometers width height boardName
                       String regionPanelPositionCoordinateXInNanometersString = matcher.group(1);
                       String regionPanelPositionCoordinateYInNanometersString = matcher.group(2);
                       String regionPanelPositionWidthInNanometersString = matcher.group(3);
                       String regionPanelPositionHeightInNanometersString = matcher.group(4);
                       String boardName = matcher.group(5);
                       String scaleX = matcher.group(6);
                       String shearY = matcher.group(7);
                       String shearX = matcher.group(8);
                       String scaleY = matcher.group(9);
                       String translateX = matcher.group(10);
                       String translateY = matcher.group(11);

                       opticalRegion.setRegion(Integer.parseInt(regionPanelPositionCoordinateXInNanometersString), 
                                               Integer.parseInt(regionPanelPositionCoordinateYInNanometersString), 
                                               Integer.parseInt(regionPanelPositionWidthInNanometersString), 
                                               Integer.parseInt(regionPanelPositionHeightInNanometersString));

                       java.awt.geom.AffineTransform transformFromRendererCoords = new java.awt.geom.AffineTransform();

                       transformFromRendererCoords.setTransform(Double.parseDouble(scaleX), 
                                                                Double.parseDouble(shearY), 
                                                                Double.parseDouble(shearX), 
                                                                Double.parseDouble(scaleY), 
                                                                Double.parseDouble(translateX), 
                                                                Double.parseDouble(translateY));

                       opticalRegion.setAffineTransform(transformFromRendererCoords);
                       
                       // We might have more than 1 board. Split it here
                       String[] boardNames = boardName.split(_COMMA_DELIMITER);
                       int boardNameLength = boardNames.length;
                       Board currentBoard = panel.getBoards().get(0);
                       for(int i = 0; i < boardNameLength; ++i)
                       {
                        String splitBoardName = boardNames[i];
                        opticalRegion.setBoard(panel.getBoard(splitBoardName));
                        currentBoard = panel.getBoard(splitBoardName);
                       }
                       
                       opticalRegion.setAlignmentGroupName(alignmentGroupName);
                       opticalRegion.setToUsePspForAlignment(setToUsePsp);

                       panelAlignmentSurfaceMapSettings.addOpticalRegion(alignmentGroupName, opticalRegion);

                       int subLineType = 0;
                       line = _projectReader.getNextLine();
                       if (version == 1)
                         matcher = _cameraRectWithPanelPositionCoordinatePattern.matcher(line);
                       else if (version == 2)
                         matcher = _cameraRectWithPanelPositionCoordinatePatternAndStatusPattern.matcher(line);
                     
                       if (matcher.matches())
                       {
                           subLineType = 1;
                       }
                       if (subLineType == 0) 
                       {
                           throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                       }

                       while (matcher.matches())    // read OpticalCameraRect
                       {
                           if (subLineType == 1)
                           {
                               OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle();

                               //CameraRect PanelPositionXCoordinateInNanometers PanelPositionYCoordinateInNanometers widthInNanometers heightInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel widthInPixel heightInPixel
                               String cameraRectPanelPositionCoordinateXInNanometersString = matcher.group(2);
                               String cameraRectPanelPositionCoordinateYInNanometersString = matcher.group(3);
                               String cameraRectPanelPositionWidthInNanometersString = matcher.group(4);
                               String cameraRectPanelPositionHeightInNanometersString = matcher.group(5);
                               String cameraRectPanelPositionCoordinateXInPixelString = matcher.group(6);
                               String cameraRectPanelPositionCoordinateYInPixelString = matcher.group(7);
                               String cameraRectPanelPositionWidthInPixelString = matcher.group(8);
                               String cameraRectPanelPositionHeightInPixelString = matcher.group(9);

                               opticalCameraRectangle.setRegion(Integer.parseInt(cameraRectPanelPositionCoordinateXInNanometersString), 
                                                                Integer.parseInt(cameraRectPanelPositionCoordinateYInNanometersString), 
                                                                Integer.parseInt(cameraRectPanelPositionWidthInNanometersString), 
                                                                Integer.parseInt(cameraRectPanelPositionHeightInNanometersString));
                               
                               //Jack Hwee - to handle previous setting compatibility, set all previous rectangle to "True"
                               if (version == 1)
                               {
                                 opticalCameraRectangle.setInspectableBool(true);
                                 opticalCameraRectangle.setZHeightInNanometer(0);
                               }
                              
                              if (version == 2)
                              {
                                String inspectableBoolString = matcher.group(10);
                                boolean inspectableBool = false;
                                if (inspectableBoolString.equals("true"))
                                  inspectableBool = true;
                                else if (inspectableBoolString.equals("false"))
                                  inspectableBool = false;
                                else
                                  Assert.expect(false);
                             
                                String zHeightString = matcher.group(11);
                                float zHeightValue = Float.parseFloat(zHeightString);
                                
                                opticalCameraRectangle.setInspectableBool(inspectableBool);
                                opticalCameraRectangle.setZHeightInNanometer(zHeightValue);
                              }
                               opticalRegion.addOpticalCameraRectangle(opticalCameraRectangle);
                           
                               int subSubLineType = 0;
                               line = _projectReader.getNextLine();
                               if (version >= 1)
                               {
                                   matcher = _pointWithPanelPositionCoordinatePattern.matcher(line);
                                   if (matcher.matches())
                                       subSubLineType = 1;
                               }
                               else
                                   Assert.expect(false);

                               while (matcher.matches())
                               {
                                   if (subSubLineType == 1)
                                   {
                                        //Point PanelPositionXCoordinateInNanometers PanelPositionYCoordinateInNanometers pointPanelXCoordinateInPixel pointPanelYCoordinateInPixel Yes|No
                                        String pointPanelPositionCoordinateXInNanometersString = matcher.group(2);
                                        String pointPanelPositionCoordinateYInNanometersString = matcher.group(3);
                                        String pointPanelPositionCoordinateXInPixelString = matcher.group(4);
                                        String pointPanelPositionCoordinateYInPixelString = matcher.group(5);
                                        String isVerifiedString = matcher.group(6);

                                        boolean isVerified = false;
                                        if (isVerifiedString.equals("yes"))
                                            isVerified = true;
                                        else if (isVerifiedString.equals("no"))
                                            isVerified = false;
                                        else
                                            Assert.expect(false);

                                        opticalRegion.addSurfaceMapPoints(Integer.parseInt(pointPanelPositionCoordinateXInNanometersString), Integer.parseInt(pointPanelPositionCoordinateYInNanometersString), isVerified);
                                        opticalCameraRectangle.addPointPanelCoordinate(Integer.parseInt(pointPanelPositionCoordinateXInNanometersString), 
                                                                                       Integer.parseInt(pointPanelPositionCoordinateYInNanometersString));
                                   }
                                   else if (subSubLineType == 2)
                                   {
                                       //Component refDes
                                       String refDes = matcher.group(2);

                                       for(Board board : opticalRegion.getBoards())
                                       {
                                           if (panel.hasComponent(board.getName(), refDes))
                                           {
                                               Component component = panel.getComponent(board.getName(), refDes);
                                               opticalRegion.addComponent(component);
                                               
                                               opticalCameraRectangle.addToComponentList(component);
                                           }
                                       }
                                   }

                                   if (_projectReader.hasNextLine() == false)
                                   {
                                       line = null;
                                       break;
                                   }
                                   subSubLineType = 0;
                                   line = _projectReader.getNextLine();
                                   if (version >= 1)
                                   {
                                       matcher = _pointWithPanelPositionCoordinatePattern.matcher(line);
                                       if (matcher.matches())
                                           subSubLineType = 1;
                                   }
                                   else
                                       Assert.expect(false);
                              }
                           }

                           if (line == null)
                               break;

                           subLineType = 0;
                           if (version == 1)
                             matcher = _cameraRectWithPanelPositionCoordinatePattern.matcher(line);
                           else if (version == 2)
                             matcher = _cameraRectWithPanelPositionCoordinatePatternAndStatusPattern.matcher(line);                
                           if (matcher.matches())
                           {
                               subLineType = 1;
                           }
                       }                 
                    }
                }
              }          
            }
            catch (DatastoreException dex)
            {
              _projectReader.closeFileDuringExceptionHandling();
              _projectReader = null;
              throw dex;
            }        
            finally
            {
              init();
              
              if (_projectReader != null)
              {
                _projectReader.closeFile();
                _projectReader = null;
              }
            }
        }              
    }
}
