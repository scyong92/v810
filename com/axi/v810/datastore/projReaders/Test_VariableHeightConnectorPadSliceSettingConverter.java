package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

class Test_VariableHeightConnectorPadSliceSettingConverter extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_VariableHeightConnectorPadSliceSettingConverter());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TunedSettingConversionEngine tunedSettingConversionEngine = new TunedSettingConversionEngine();

    Subtype subtype = new Subtype();

    // make sure conversionRequired() works

    String oldAlgorithmSettingEnumString = "gridArrayVariableHeightConnectorPadSliceOffset";
    boolean conversionRequired = tunedSettingConversionEngine.conversionRequired( oldAlgorithmSettingEnumString );

    Expect.expect(conversionRequired);

    // try converting 10.0f mils
    float oldValueMillimeters = MathUtil.convertMilsToMillimeters( 10.0f );
    Serializable oldValue = (Float)oldValueMillimeters;
    Pair< AlgorithmSettingEnum, Serializable > result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                                                                  oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), oldValueMillimeters));

    // try converting -5.0f mils
    oldValueMillimeters = MathUtil.convertMilsToMillimeters( -5.0f );
    oldValue = (Float)oldValueMillimeters;
    result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                       oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), oldValueMillimeters));

  }
}
