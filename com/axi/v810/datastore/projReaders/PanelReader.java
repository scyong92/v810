package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.v810.business.*;

/**
 * Creates Board, SideBoard, Component, Pad, Fiducial, FiducialType
 * @author Bill Darbie
 */
class PanelReader
{
  private static PanelReader _instance;

  private ProjectReader _projectReader;
  // boardInstanceName1 boardType1 flippedLeftToRight|notFlippedLeftToRight flippedTopToBottom|notFlippedTopToBottom xLocation yLocation xShift yShift rotation
  private Pattern _boardInstancePattern1 = Pattern.compile("^([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)\\s+(flippedLeftToRight|notFlippedLeftToRight)\\s+(flippedTopToBottom|notFlippedTopToBottom)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(0|90|180|270)$");
  // boardInstanceNumber boardInstanceName1 boardType1 flippedLeftToRight|notFlippedLeftToRight flippedTopToBottom|notFlippedTopToBottom xLocation yLocation xShift yShift rotation
  private Pattern _boardInstancePattern2 = Pattern.compile("^(\\d+)\\s+([^\\s]+)\\s+([^\\s\\:\\*\\?\"\\<\\>\\|\\\\]+)\\s+(flippedLeftToRight|notFlippedLeftToRight)\\s+(flippedTopToBottom|notFlippedTopToBottom)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(0|90|180|270)$");
  // fiducialName side1|side2 xLocation yLocation width length circle|rectangle
  private Pattern _fiducialPattern = Pattern.compile("^([^\\s]+)\\s+(side1|side2)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(circle|rectangle)$");
  private Set<BoardType> _boardTypeSet = new HashSet<BoardType>();

  /**
   * @author Bill Darbie
   */
  static synchronized PanelReader getInstance()
  {
    if (_instance == null)
      _instance = new PanelReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _boardTypeSet.clear();
  }

  /**
   * @author Bill Darbie
   */
  Map<Board, Integer> read(Project project, Map<Integer, BoardType> boardTypeNumberToBoardTypeMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardTypeNumberToBoardTypeMap != null);

    // create boardTypeNameToBoardTypeMap and boardTypeNameToBoardTypeNumberMap
    Map<String, BoardType> boardTypeNameToBoardTypeMap = new HashMap<String, BoardType>();
    Map<String, Integer> boardTypeNameToBoardTypeNumberMap = new HashMap<String, Integer>();
    for (Map.Entry<Integer, BoardType> entry : boardTypeNumberToBoardTypeMap.entrySet())
    {
      Integer number = entry.getKey();
      BoardType boardType = entry.getValue();
      String boardTypeName = boardType.getName();
      boardTypeNameToBoardTypeMap.put(boardTypeName, boardType);
      boardTypeNameToBoardTypeNumberMap.put(boardTypeName, number);
    }

    Map<Board, Integer> boardToBoardInstanceNumberMap = new HashMap<Board, Integer>();
    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    String fileName = null;
    int version = FileName.getProjectPanelLatestFileVersion();
    while (version > 0)
    {
      fileName = FileName.getProjectPanelFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }
    Assert.expect(fileName != null);

    Panel panel = project.getPanel();
    project.setPanel(panel);
    try
    {
      _projectReader.openFile(fileName);

      String widthStr = _projectReader.getNextLine();
      int width = StringUtil.convertStringToInt(widthStr);
      String lengthStr = _projectReader.getNextLine();
      int length = StringUtil.convertStringToInt(lengthStr);
      String thicknessStr = _projectReader.getNextLine();
      int thickness = StringUtil.convertStringToInt(thicknessStr);

      if (width <= 0)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      panel.setWidthInNanoMeters(width);
      if (length <= 0)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      panel.setLengthInNanoMeters(length);
      if (thickness < 0)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      panel.setThicknessInNanoMeters(thickness);

      String line = "";
      boolean endOfFile = true;
      while (_projectReader.hasNextLine())
      {
        line = _projectReader.getNextLine();

        // boardInstanceName1 boardType1 flipped|notFlipped xLocation yLocation rotation
        int boardInstanceNumber = 0;
        String boardName = null;
        String boardTypeName = null;
        String flippedOrNotFlippedLeftToRight = null;
        String flippedOrNotFlippedTopToBottom = null;
        int xInNanos = 0;
        int yInNanos = 0;
        int degreesRotation = 0;

        if (version == 1)
        {
          ++boardInstanceNumber;
          Matcher matcher = _boardInstancePattern1.matcher(line);
          if (matcher.matches() == false)
          {
            // we are done reading in boardType lines, this must be a fiducial line
            endOfFile = false;
            break;
          }

          // boardInstanceName1 boardType1 flipped|notFlipped xLocation yLocation rotation
          boardName = matcher.group(1);
          boardInstanceNumber = StringUtil.convertStringToInt(boardName);
          boardTypeName = matcher.group(2);
          flippedOrNotFlippedLeftToRight = matcher.group(3);
          flippedOrNotFlippedTopToBottom = matcher.group(4);
          String xInNanosStr = matcher.group(5);
          xInNanos = StringUtil.convertStringToInt(xInNanosStr);
          String yInNanosStr = matcher.group(6);
          yInNanos = StringUtil.convertStringToInt(yInNanosStr);
          String degreesRotationStr = matcher.group(7);
          degreesRotation = StringUtil.convertStringToInt(degreesRotationStr);
        }
        else if (version >= 2)
        {
          Matcher matcher = _boardInstancePattern2.matcher(line);
          if (matcher.matches() == false)
          {
            // we are done reading in boardType lines, this must be a fiducial line
            endOfFile = false;
            break;
          }

          // boardInstanceNumber1 boardInstanceName1 boardType1 flipped|notFlipped xLocation yLocation rotation
          String boardInstanceNumberStr = matcher.group(1);
          boardInstanceNumber = StringUtil.convertStringToInt(boardInstanceNumberStr);
          boardName = matcher.group(2);
          boardTypeName = matcher.group(3);
          flippedOrNotFlippedLeftToRight = matcher.group(4);
          flippedOrNotFlippedTopToBottom = matcher.group(5);
          String xInNanosStr = matcher.group(6);
          xInNanos = StringUtil.convertStringToInt(xInNanosStr);
          String yInNanosStr = matcher.group(7);
          yInNanos = StringUtil.convertStringToInt(yInNanosStr);
          String degreesRotationStr = matcher.group(8);
          degreesRotation = StringUtil.convertStringToInt(degreesRotationStr);
        }

        Board board = new Board();
        boardToBoardInstanceNumberMap.put(board, boardInstanceNumber);

        BoardType boardType = boardTypeNameToBoardTypeMap.get(boardTypeName);
        boardType.addBoard(board);
        board.setName(boardName);

        // if this is null then the boardType_boardTypeName.1.cad file is missing
        if (boardType == null)
        {
          int boardTypeNumber = boardTypeNameToBoardTypeNumberMap.get(boardTypeName);
          String boardTypeFileName = FileName.getProjectBoardTypeFullPath(project.getName(), boardTypeNumber, FileName.getProjectBoardTypeLatestFileVersion());
          throw new FileNotFoundDatastoreException(boardTypeFileName);
        }

        if (_boardTypeSet.contains(boardType) == false)
        {
          panel.addBoardType(boardType);
          board.setBoardType(boardType);
          _boardTypeSet.add(boardType);
        }

        board.setCoordinateInNanoMeters(new PanelCoordinate(xInNanos, yInNanos));
        for (int i = 0; i < 2; ++i)
        {
          SideBoard sideBoard = new SideBoard();
          SideBoardType sideBoardType = null;
          if (i == 0)
          {
            if (boardType.sideBoardType1Exists())
            {
              sideBoardType = boardType.getSideBoardType1();
              board.setSideBoard1(sideBoard);
            }
          }
          else if (i == 1)
          {
            if (boardType.sideBoardType2Exists())
            {
              sideBoardType = boardType.getSideBoardType2();
              board.setSideBoard2(sideBoard);
            }
          }
          else
            Assert.expect(false);

          if (sideBoardType == null)
            continue;

          sideBoard.setSideBoardType(sideBoardType);
          sideBoardType.addSideBoard(sideBoard);

          List<FiducialType> fiducialTypes = sideBoardType.getFiducialTypes();
          for (FiducialType fiducialType : fiducialTypes)
          {
            Fiducial fiducial = new Fiducial();
            fiducial.setFiducialType(fiducialType);
            fiducial.setSideBoard(sideBoard);
            board.setNewFiducialName(fiducial, sideBoard, null, fiducialType.getName());
          }

          List<ComponentType> componentTypes = sideBoardType.getComponentTypes();
          for (ComponentType componentType : componentTypes)
          {
            Component component = new Component();
            component.setComponentType(componentType);
            sideBoard.addComponent(component);
            component.setSideBoard(sideBoard);

            List<PadType> padTypes = componentType.getPadTypes();
            for (PadType padType : padTypes)
            {
              Pad pad = new Pad();
              pad.setNodeName("");
              PadSettings padSettings = new PadSettings();
              pad.setPadSettings(padSettings);
              padSettings.setPad(pad);
              pad.setComponent(component);
              pad.setPadType(padType);
              padType.addPad(pad);
              component.addPad(pad);
            }
          }
        }

        if (flippedOrNotFlippedLeftToRight.equals("notFlippedLeftToRight"))
          board.setLeftToRightFlip(false);
        else if (flippedOrNotFlippedLeftToRight.equals("flippedLeftToRight"))
          board.setLeftToRightFlip(true);
        else
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        if (flippedOrNotFlippedTopToBottom.equals("notFlippedTopToBottom"))
          board.setTopToBottomFlip(false);
        else if (flippedOrNotFlippedTopToBottom.equals("flippedTopToBottom"))
          board.setTopToBottomFlip(true);
        else
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        board.setDegreesRotationRelativeToPanel(degreesRotation);

      }

      while (endOfFile == false)
      {
        Matcher matcher = _fiducialPattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        // fiducialName side1|side2 xLocation yLocation width length circle|rectangle
        String fiducialName = matcher.group(1);
        String side1OrSide2 = matcher.group(2);
        String xInNanosStr = matcher.group(3);
        int xInNanos = StringUtil.convertStringToInt(xInNanosStr);
        String yInNanosStr = matcher.group(4);
        int yInNanos = StringUtil.convertStringToInt(yInNanosStr);
        String widthInNanosStr = matcher.group(5);
        int widthInNanos = StringUtil.convertStringToInt(widthInNanosStr);
        String lengthInNanosStr = matcher.group(6);
        int lengthInNanos = StringUtil.convertStringToInt(lengthInNanosStr);
        String circleOrRect = matcher.group(7);

        Fiducial fiducial = new Fiducial();
        FiducialType fiducialType = new FiducialType();
        fiducial.setFiducialType(fiducialType);
        if (side1OrSide2.equals("side1"))
          fiducial.setPanel(panel, true);
        else if (side1OrSide2.equals("side2"))
          fiducial.setPanel(panel, false);
        else
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        fiducialType.setName(fiducialName);
        fiducialType.setCoordinateInNanoMeters(new PanelCoordinate(xInNanos, yInNanos));
        fiducialType.setLengthInNanoMeters(lengthInNanos);
        fiducialType.setWidthInNanoMeters(widthInNanos);

        if (circleOrRect.equals("circle"))
          fiducialType.setShapeEnum(ShapeEnum.CIRCLE);
        else if (circleOrRect.equals("rectangle"))
          fiducialType.setShapeEnum(ShapeEnum.RECTANGLE);
        else
          throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

        if (_projectReader.hasNextLine() == false)
        {
          // we have reached the end of the file
          break;
        }

        line = _projectReader.getNextLine();
      }
      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }

    return boardToBoardInstanceNumberMap;
  }
}
