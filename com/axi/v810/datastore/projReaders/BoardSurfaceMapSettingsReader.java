package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardSurfaceMapSettingsReader
{

  private static BoardSurfaceMapSettingsReader _instance;

  private String _fileName;

  private ProjectReader _projectReader;
  //OpticalRegion panelXCoordinateInNanometers panelYCoordinateInNanometers panelWidthInNanometer panelHeightInNanometer scaleX shearY shearX scaleY translateX translateY
  private Pattern _regionWithPanelPositionCoordinateAndPixelCoordinateWithAffineTransformPattern = Pattern.compile("^Region\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)\\s+([-+]?\\d+\\.\\d+[eE]?[-+]?\\d?)$");
  //OpticalRegion pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers widthInNanometer heightInNanometer
  private Pattern _regionWithPanelPositionCoordinatePattern = Pattern.compile("^Region\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)");
  //OpticalCameraRectangle cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel
  private Pattern _cameraRectWithPanelPositionCoordinateAndPixelCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)$");
  //OpticalCameraRectangle cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectPanelWidthInNanometers cameraRectPanelHeightInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel cameraRectWidthInPixel cameraRectHeightInPixel true|false z-height
  private Pattern _cameraRectWithPanelPositionCoordinateAndPixelCoordinateAndStatusPattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(true|false)\\s+([-+]?\\d+\\.\\d*)$");
  //OpticalCameraRectangle cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectPanelWidthInNanometers cameraRectPanelHeightInNanometers true|false z-height
  private Pattern _cameraRectWithPanelPositionCoordinateAndStatusPattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(true|false)\\s+([-+]?\\d+\\.\\d*)$");
  //Point pointPanelXCoordinateInNanometer pointPanelYCoordinateInNanometer pointPanelXCoordinateInPixel pointPanelYCoordinateInPixel yes|no
  private Pattern _pointWithPanelPositionCoordinateAndPixelCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(yes|no)$");
  //Point pointPanelXCoordinateInNanometer pointPanelYCoordinateInNanometer yes|no
  private Pattern _pointWithPanelPositionCoordinatePattern = Pattern.compile("^([^\\s]+)\\s+(\\d*)\\s+(\\d*)\\s+(yes|no)$");
  //Component refDes
  private Pattern _componentRefDesPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");

  /**
   * @author Cheah Lee Herng
   */
  static synchronized BoardSurfaceMapSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new BoardSurfaceMapSettingsReader();

    return _instance;
  }

  /**
   * @author Cheah Lee Herng
   */
  private void init()
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng
   */
  void read(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardToBoardInstanceNumberMap != null);

    init();

    String projName = project.getName();
    _fileName = "";

    for (Board board : project.getPanel().getBoards())
    {
      if (_projectReader == null)
        _projectReader = ProjectReader.getInstance();

      int version = FileName.getProjectBoardSurfaceMapSettingsLatestFileVersion();
      int boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);

      while (version > 0)
      {
        _fileName = FileName.getProjectBoardSurfaceMapSettingsFullPath(projName, boardInstanceNumber, version);
        if (FileUtilAxi.exists(_fileName))
          break;
        --version;
      }

      // We need to handle backward-compatibility issue.
      if (version == 0 || version == 1) // we don't support PSP 1 anymore.
      {
        BoardSurfaceMapSettings boardSurfaceMapSettings = new BoardSurfaceMapSettings(board);
        board.setBoardSurfaceMapSettings(boardSurfaceMapSettings);
      }
      else
      {
        try
        {
          _projectReader.openFile(_fileName);

          BoardSurfaceMapSettings boardSurfaceMapSettings = new BoardSurfaceMapSettings(board);
          board.setBoardSurfaceMapSettings(boardSurfaceMapSettings);

          if (_projectReader.hasNextLine() == false)
            continue;

          String line = _projectReader.getNextLine();
          while (true)
          {
            if (line == null)
              break;

            // figure out which type of line we have
            Matcher matcher = version < 4 ? _regionWithPanelPositionCoordinateAndPixelCoordinateWithAffineTransformPattern.matcher(line) : _regionWithPanelPositionCoordinatePattern.matcher(line);
            if (matcher.matches() == false)
            {
              throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            }

            OpticalRegion opticalRegion = new OpticalRegion();

            //OpticalRegion panelXCoordinateInNanometers panelYCoordinateInNanometers panelCoordinateWidthInNanometers panelCoordinateHeightInNanometers
            String regionStageCoordinateXInNanometersString = matcher.group(1);
            String regionStageCoordinateYInNanometersString = matcher.group(2);
            String regionStageWidthInNanometersString = matcher.group(3);
            String regionStageHeightInNanometersString = matcher.group(4);

            opticalRegion.setRegion(Integer.parseInt(regionStageCoordinateXInNanometersString),
              Integer.parseInt(regionStageCoordinateYInNanometersString),
              Integer.parseInt(regionStageWidthInNanometersString),
              Integer.parseInt(regionStageHeightInNanometersString));

            opticalRegion.setBoard(board);

            boardSurfaceMapSettings.addOpticalRegion(opticalRegion);

            line = _projectReader.getNextLine();

            if (version == 1)
              matcher = _cameraRectWithPanelPositionCoordinateAndPixelCoordinatePattern.matcher(line);
            else if (version == 2 || version == 3)
              matcher = _cameraRectWithPanelPositionCoordinateAndPixelCoordinateAndStatusPattern.matcher(line);
            else if (version == 4)
              matcher = _cameraRectWithPanelPositionCoordinateAndStatusPattern.matcher(line);

            if (matcher.matches() == false)
            {
              throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            }

            while (matcher.matches())    // read OpticalCameraRect
            {
              OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle();

            //OpticalCameraRectangle PanelPositionXCoordinateInNanometers PanelPositionYCoordinateInNanometers widthInNanometers heightInNanometers
              String cameraRectPanelPositionCoordinateXInNanometersString = matcher.group(2);
              String cameraRectPanelPositionCoordinateYInNanometersString = matcher.group(3);
              String cameraRectPanelPositionWidthInNanometersString = matcher.group(4);
              String cameraRectPanelPositionHeightInNanometersString = matcher.group(5);

              opticalCameraRectangle.setRegion(Integer.parseInt(cameraRectPanelPositionCoordinateXInNanometersString),
                Integer.parseInt(cameraRectPanelPositionCoordinateYInNanometersString),
                Integer.parseInt(cameraRectPanelPositionWidthInNanometersString),
                Integer.parseInt(cameraRectPanelPositionHeightInNanometersString));
              //Jack Hwee - to handle previous setting compatibility, set all previous rectangle to "True"
              if (version == 1)
              {
                opticalCameraRectangle.setInspectableBool(true);
                opticalCameraRectangle.setZHeightInNanometer(0);
              }
              else if (version == 2 || version == 3)
              {
                String inspectableBoolString = matcher.group(10);
                boolean inspectableBool = false;
                if (inspectableBoolString.equals("true"))
                  inspectableBool = true;
                else if (inspectableBoolString.equals("false"))
                  inspectableBool = false;
                else
                  Assert.expect(false);

                String zHeightString = matcher.group(11);
                float zHeightValue = Float.parseFloat(zHeightString);
                
                // XCR-3089 Recipe compatibility: Surface Map point z-height is different after upgrade recipe version
                float zHeightInNanometers = MathUtil.convertUnits(zHeightValue, MathUtilEnum.MILLIMETERS, MathUtilEnum.NANOMETERS);

                opticalCameraRectangle.setInspectableBool(inspectableBool);
                opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometers);
              }
              else if (version == 4)
              {
                String inspectableBoolString = matcher.group(6);
                boolean inspectableBool = false;
                if (inspectableBoolString.equals("true"))
                  inspectableBool = true;
                else if (inspectableBoolString.equals("false"))
                  inspectableBool = false;
                else
                  Assert.expect(false);

                String zHeightString = matcher.group(7);
                float zHeightValue = Float.parseFloat(zHeightString);

                opticalCameraRectangle.setInspectableBool(inspectableBool);
                opticalCameraRectangle.setZHeightInNanometer(zHeightValue);
              }
              else
                Assert.expect(false);
              
              opticalRegion.addBoardOpticalCameraRectangle(board, opticalCameraRectangle);
              int subSubLineType = 0;
              line = _projectReader.getNextLine();
              if (version == 1 || version == 2 || version == 3)
              {
                matcher = _pointWithPanelPositionCoordinateAndPixelCoordinatePattern.matcher(line);
                if (matcher.matches())
                  subSubLineType = 1;
                if (subSubLineType == 0)
                {
                  matcher = _componentRefDesPattern.matcher(line);
                  if (matcher.matches())
                    subSubLineType = 2;
                }
              }
              else if (version == 4)
              {
                matcher = _pointWithPanelPositionCoordinatePattern.matcher(line);
                if (matcher.matches())
                  subSubLineType = 1;
                if (subSubLineType == 0)
                {
                  matcher = _componentRefDesPattern.matcher(line);
                  if (matcher.matches())
                    subSubLineType = 2;
                }
              }
              else
                Assert.expect(false);

              if (version < 4) // need to convert here first to prevent the case of a rectangle not having any points (disabled rectangle).
                opticalCameraRectangle = OpticalMechanicalConversions.convertOldOpticalCameraRectangleToNewOpticalCameraRectangle(opticalCameraRectangle);
              
              while (matcher.matches())
              {
                if (subSubLineType == 1)
                {
                  //Point PanelPositionXCoordinateInNanometers PanelPositionYCoordinateInNanometers Yes|No
                  String pointPanelPositionCoordinateXInNanometersString = matcher.group(2);
                  String pointPanelPositionCoordinateYInNanometersString = matcher.group(3);
                  java.awt.Point coordinateInNanometer = new java.awt.Point(Integer.parseInt(pointPanelPositionCoordinateXInNanometersString), Integer.parseInt(pointPanelPositionCoordinateYInNanometersString));
                  // Convert the SurfaceMapPoints if they are incorrect.
                  if (version < 4)
                  {
                    String pointPanelPositionCoordinateXInPixelString = matcher.group(4);
                    String pointPanelPositionCoordinateYInPixelString = matcher.group(5);
                    java.awt.Point pixelCoordinate = new java.awt.Point(Integer.parseInt(pointPanelPositionCoordinateXInPixelString),
                                                      Integer.parseInt(pointPanelPositionCoordinateYInPixelString));
                    java.awt.Point convertedCoordinateInNanometer = OpticalMechanicalConversions.convertPixelCoordinateToPanelCoordinateInNanometer(opticalCameraRectangle, pixelCoordinate);
                    coordinateInNanometer.setLocation(convertedCoordinateInNanometer.getX(), convertedCoordinateInNanometer.getY());
                  }
                  String isVerifiedString = version == 4 ? matcher.group(4) : matcher.group(6);
                  boolean isVerified = false;
                  if (isVerifiedString.equals("yes"))
                    isVerified = true;
                  else if (isVerifiedString.equals("no"))
                    isVerified = false;
                  else
                    Assert.expect(false);

                  opticalRegion.addSurfaceMapPoints(board, (int)coordinateInNanometer.getX(), (int)coordinateInNanometer.getY());
                  opticalCameraRectangle.addPointPanelCoordinate((int)coordinateInNanometer.getX(), (int)coordinateInNanometer.getY());
                }
                else if (subSubLineType == 2)
                {
                  //Component refDes
                  String refDes = matcher.group(2);

                  Component component = board.getComponent(refDes);
                  opticalRegion.addComponent(board, component);

                  opticalCameraRectangle.addToComponentList(component);
                }

                if (_projectReader.hasNextLine() == false)
                {
                  line = null;
                  break;
                }
                subSubLineType = 0;
                line = _projectReader.getNextLine();
                if (version == 1 || version == 2 || version == 3)
                {
                  matcher = _pointWithPanelPositionCoordinateAndPixelCoordinatePattern.matcher(line);
                  if (matcher.matches())
                    subSubLineType = 1;
                  if (subSubLineType == 0)
                  {
                    matcher = _componentRefDesPattern.matcher(line);
                    if (matcher.matches())
                      subSubLineType = 2;
                  }
                }
                else if (version == 4)
                {
                  matcher = _pointWithPanelPositionCoordinatePattern.matcher(line);
                  if (matcher.matches())
                    subSubLineType = 1;
                  if (subSubLineType == 0)
                  {
                    matcher = _componentRefDesPattern.matcher(line);
                    if (matcher.matches())
                      subSubLineType = 2;
                  }
                }
                else
                  Assert.expect(false);
              }
              
              if (project.getPanel().getShapeInNanoMeters().contains(opticalCameraRectangle.getRegion().getRectangle2D().getBounds()) == false)
              {
                // remove a OpticalCameraRectangle and its relative points if it's not within the panel boundaries.
				for (PanelCoordinate point : opticalCameraRectangle.getPanelCoordinateList())
                {
                  opticalRegion.removeSurfaceMapPoint(opticalCameraRectangle.getBoard(), point.getX(), point.getY());
                }
                opticalRegion.removeOpticalCameraRectangle(opticalCameraRectangle.getBoard(), opticalCameraRectangle);
              }
            
              if (line == null)
                break;

              if (version == 1)
                matcher = _cameraRectWithPanelPositionCoordinateAndPixelCoordinatePattern.matcher(line);
              else if (version == 2 || version == 3)
                matcher = _cameraRectWithPanelPositionCoordinateAndPixelCoordinateAndStatusPattern.matcher(line);
              else if (version == 4)
                matcher = _cameraRectWithPanelPositionCoordinateAndStatusPattern.matcher(line);
            }
          }
        }
        catch (DatastoreException dex)
        {
          _projectReader.closeFileDuringExceptionHandling();
          _projectReader = null;
          throw dex;
        }
        finally
        {
          init();
          if (_projectReader != null)
          {
            _projectReader.closeFile();
            _projectReader = null;
          }
        }
      }
    }
  }
}
