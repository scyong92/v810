package com.axi.v810.datastore.projReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.projWriters.*;

class Test_ThroughHoleExtrusionSliceSettingConverter extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ThroughHoleExtrusionSliceSettingConverter());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TunedSettingConversionEngine tunedSettingConversionEngine = new TunedSettingConversionEngine();

    Subtype subtype = new Subtype();
    subtype.setInspectionFamilyEnum(InspectionFamilyEnum.THROUGHHOLE);
    Panel panel = new Panel();
    subtype.setPanel(panel);
    float BOARD_THICKNESS_IN_MILS = 100.0f;
    panel.setThicknessInNanoMeters((int)MathUtil.convertMilsToNanoMeters( BOARD_THICKNESS_IN_MILS ));

    AlgorithmSettingEnum oldAlgorithmSettingEnum = AlgorithmSettingEnum.THROUGHHOLE_EXTRUSION_SLICE_OBSOLETE;

    EnumStringLookup enumStringLookup = EnumStringLookup.getInstance();
    String oldAlgorithmSettingEnumString = enumStringLookup.getAlgorithmSettingString( oldAlgorithmSettingEnum );

    // make sure conversionRequired() works
    boolean conversionRequired = tunedSettingConversionEngine.conversionRequired( oldAlgorithmSettingEnumString );
    Expect.expect(conversionRequired);

    // the obsolete extrusion slice settings were None, Pin Side @ -30%, Pin Side @ -20%, Pin Side @ -10%

    // try converting None
    Serializable oldValue = "None";
    Pair< AlgorithmSettingEnum, Serializable > result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                        oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT)));

    // try converting Pin Side @ -30%
    oldValue = "Pin Side @ -30%";
    result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                       oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), MathUtil.convertMilsToMillimeters(BOARD_THICKNESS_IN_MILS * 0.30f)));

    // try converting Pin Side @ -20%
    oldValue = "Pin Side @ -20%";
    result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                       oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), MathUtil.convertMilsToMillimeters(BOARD_THICKNESS_IN_MILS * 0.20f)));

    // try converting Pin Side @ -10%
    oldValue = "Pin Side @ -10%";
    result = tunedSettingConversionEngine.getConvertedAlgorithmSetting(oldAlgorithmSettingEnumString,
                                                                       oldValue, subtype);

    Expect.expect(result.getFirst().equals(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT));
    Expect.expect(MathUtil.fuzzyEquals((Float)result.getSecond(), MathUtil.convertMilsToMillimeters(BOARD_THICKNESS_IN_MILS * 0.10f)));



  }
}
