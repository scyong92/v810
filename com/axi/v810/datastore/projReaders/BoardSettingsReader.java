package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * Creates BoardSettings
 * @author Bill Darbie
 */
class BoardSettingsReader
{
  private static BoardSettingsReader _instance;

  // single|left|right alignmentGroupName numPadsOrFiducials
  private Pattern _alignmentGroupNameNoQualityPattern = Pattern.compile("^(single|left|right)\\s+([^\\s]+)\\s+(\\d+)$");
  // single|left|right alignmentGroupName numPadsOrFiducials useZheight zHeightInNanometer  
  private Pattern _alignmentGroupNameNoQualityPattern2 = Pattern.compile("^(single|left|right)\\s+([^\\s]+)\\s+(\\d+)\\s+(\\w+)\\s+(-?\\d+)$");//XCR-3649
  // single|left|right alignmentGroupName numPadsOrFiducials useZHeight zHeightInNanometers useCustomizeAlignmentSetting userGain signalCompensation
  private Pattern _alignmentGroupNameNoQualityPattern3 = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)\\s+(\\w+)\\s+(-\\w+|\\w+)\\s+(\\w+)\\s+(\\d+)\\s+(\\d+[\\.\\d+]?)$");
  // single|left|right alignmentGroupName numPadsOrFiducials useZHeight zHeightInNanometers useCustomizeAlignmentSetting userGain signalCompensation StageSpeed DRO Magnification
  private Pattern _alignmentGroupNameNoQualityPattern4 = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)\\s+(\\w+)\\s+(-\\w+|\\w+)\\s+(\\w+)\\s+(\\d+)\\s+(\\d+\\.?\\d?)\\s+(\\d+\\.?\\d?)\\s+(\\d+)$");
  // boardName refDes padName
  private Pattern _padPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)$");
  // fiducialName
  private Pattern _fiducialPanelPattern = Pattern.compile("^([^\\s]+)$");
  // boardName fiducialName
  private Pattern _fiducialBoardPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");

  private ProjectReader _projectReader;
  private String _fileName;

  /**
   * @author Bill Darbie
   */
  static synchronized BoardSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new BoardSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardSettingsReader()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardToBoardInstanceNumberMap != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    _fileName = "";
    try
    {
      for (Board board : project.getPanel().getBoards())
      {
        int version = FileName.getProjectBoardSettingsLatestFileVersion();
        int boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);

        while (version > 0)
        {
          _fileName = FileName.getProjectBoardSettingsFullPath(projName, boardInstanceNumber, version);
          if (FileUtilAxi.exists(_fileName))
            break;
          --version;
        }

        _projectReader.openFile(_fileName);

        String boardName = _projectReader.getNextLine();
        String inspectedOrNotStr = _projectReader.getNextLine();
        boolean inspected = isInspected(inspectedOrNotStr);
        
        BoardSettings boardSettings = new BoardSettings(board);
        boardSettings.setInspected(inspected);
        board.setBoardSettings(boardSettings);
        
        String strZOffset = null;
        if (version > 2)
        {
          strZOffset = _projectReader.getNextLine();
          int zOffset = StringUtil.legacyConvertStringToInt(strZOffset);
          boardSettings.getBoard().getBoardSettings().setBoardZOffsetInNanometers(zOffset);
        }
        
        BoardSurfaceMapSettings boardSurfaceMapSettings = new BoardSurfaceMapSettings(board);
        board.setBoardSurfaceMapSettings(boardSurfaceMapSettings);
        
        BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = new BoardAlignmentSurfaceMapSettings(board);
        board.setBoardAlignmentSurfaceMapSettings(boardAlignmentSurfaceMapSettings);
        
        // Wei Chin: implement later
        if(version == 1)
          boardSettings.assignAlignmentGroups();
        if(version > 1)
        {
          String line = null;
//          boolean single = false;
          while (true)
          {
            // Due to XXL spliting panel way is different from standard
            // Assign empty alignment groups if different system type
            if(project.getSystemType().equals(com.axi.v810.hardware.XrayTester.getSystemType()) == false)
            {
              boardSettings.assignAlignmentGroups();
              _projectReader.goToLastLine();
              break;
            }
        
            if (line == null)
              line = _projectReader.getNextLine();

            Matcher matcher = _alignmentGroupNameNoQualityPattern.matcher(line);
            Matcher matcher2 = _alignmentGroupNameNoQualityPattern2.matcher(line);
            Matcher matcher3 = _alignmentGroupNameNoQualityPattern3.matcher(line);
            Matcher matcher4 = _alignmentGroupNameNoQualityPattern4.matcher(line);
        
            String singleOrLeftOrRight = null;
            String alignmentGroupName = null;
            String numPadsAndFiducialsStr = null;
            String matchQualityStr = null;            
            String useZHeightStr = null;
            String zHeightInNanometerStr = null;
            String useCustomizeAlignmentSettingStr = null;
            String userGainStr = null;
            String signalCompensationStr = null;
            String stageSpeedStr = null;
            String droStr = null;
            if (matcher.matches())
            {
              singleOrLeftOrRight = matcher.group(1);
              alignmentGroupName = matcher.group(2);
              numPadsAndFiducialsStr = matcher.group(3);
            }
            else if(matcher2.matches())
            {
              singleOrLeftOrRight = matcher2.group(1);
              alignmentGroupName = matcher2.group(2);
              numPadsAndFiducialsStr = matcher2.group(3);
              useZHeightStr = matcher2.group(4);
              zHeightInNanometerStr = matcher2.group(5);
            }
            else if(matcher3.matches())
            {
              singleOrLeftOrRight = matcher3.group(1);
              alignmentGroupName = matcher3.group(2);
              numPadsAndFiducialsStr = matcher3.group(3);
              useZHeightStr = matcher3.group(4);
              zHeightInNanometerStr = matcher3.group(5);
              useCustomizeAlignmentSettingStr = matcher3.group(6);
              userGainStr = matcher3.group(7);
              signalCompensationStr = matcher3.group(8);
            }
            else if(matcher4.matches())
            {
              singleOrLeftOrRight = matcher4.group(1);
              alignmentGroupName = matcher4.group(2);
              numPadsAndFiducialsStr = matcher4.group(3);
              useZHeightStr = matcher4.group(4);
              zHeightInNanometerStr = matcher4.group(5);
              useCustomizeAlignmentSettingStr = matcher4.group(6);
              userGainStr = matcher4.group(7);
              signalCompensationStr = matcher4.group(8);
              stageSpeedStr = matcher4.group(9);
              droStr = matcher4.group(10);
            }
            else //Kee Chin Seong - throw the exception if the matcher is not fullfill any single pattern
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            
            //Swee yee - panel settings shown left and right alignment group for short panel, it is corrupted and will erase all the alignment group
            //XCR-3324, 5.6 software version recipe is corrupted when load on 5.8 sw
            if (project.getPanel().getPanelSettings().isLongPanel() == false && singleOrLeftOrRight.equals("right") == false)
            {
              boardSettings.assignAlignmentGroups();
              _projectReader.goToLastLine();
              break;
            }

            if (matcher.matches() || matcher2.matches() || matcher3.matches() || matcher4.matches())
            {
              int numPadsAndFiducials = StringUtil.convertStringToInt(numPadsAndFiducialsStr);
              double matchQuality;
              boolean useZheight = false;
              int zHeightInNanometer = 0;
              boolean useCustomizeAlignmentSetting = false;

              if(useZHeightStr != null)
                useZheight = StringUtil.convertStringToBoolean(useZHeightStr);
              if(zHeightInNanometerStr != null)
                zHeightInNanometer = StringUtil.convertStringToInt(zHeightInNanometerStr);
              if(useCustomizeAlignmentSettingStr != null)
                useCustomizeAlignmentSetting = StringUtil.convertStringToBoolean(useCustomizeAlignmentSettingStr);

              AlignmentGroup alignmentGroup = new AlignmentGroup(boardSettings);
              if (singleOrLeftOrRight.equals("single"))
                boardSettings.addAlignmentGroup(alignmentGroup);
              else if (singleOrLeftOrRight.equals("left"))
                boardSettings.addLeftAlignmentGroup(alignmentGroup);
              else if (singleOrLeftOrRight.equals("right"))
                boardSettings.addRightAlignmentGroup(alignmentGroup);
              else
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

              alignmentGroup.setName(alignmentGroupName);
              if (matchQualityStr != null)
              {
                matchQuality = StringUtil.convertStringToDouble(matchQualityStr);
                alignmentGroup.setMatchQuality(matchQuality);
              }
              alignmentGroup.setUseZHeight(useZheight);
              alignmentGroup.setZHeightInNanometer(zHeightInNanometer);
              alignmentGroup.setUseCustomizeAlignmentSetting(useCustomizeAlignmentSetting);
              if(userGainStr != null)
                alignmentGroup.setUserGainEnum(UserGainEnum.getUserGainToEnumMapValue(StringUtil.convertStringToInt(userGainStr)));             
              if (signalCompensationStr != null)
                alignmentGroup.setSignalCompensationEnum(SignalCompensationEnum.getSignalCompensationEnum(signalCompensationStr));
              if (stageSpeedStr != null)
                alignmentGroup.setStageSpeedEnum(StageSpeedEnum.getStageSpeedToEnumMapValue(StringUtil.convertStringToDouble(stageSpeedStr)));
              if (droStr != null)
                alignmentGroup.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(StringUtil.convertStringToInt(droStr)));
              line = null;

              for (int i = 0; i < numPadsAndFiducials; ++i)
              {
                line = _projectReader.getNextLine();
                matcher = _padPattern.matcher(line);
                if (matcher.matches())
                {
                  boardName = matcher.group(1);
                  String refDes = matcher.group(2);
                  String padName = matcher.group(3);

                  // find the correct pad
                  if (project.getPanel().hasPad(boardName, refDes, padName) == false)
                    throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                  Pad pad = project.getPanel().getPad(boardName, refDes, padName);
                  alignmentGroup.addPad(pad);
                  line = null;
                }
                else
                {
                  matcher = _fiducialBoardPattern.matcher(line);
                  if (matcher.matches())
                  {
                    boardName = matcher.group(1);
                    String fiducialName = matcher.group(2);

                    // find the correct board fiducial
                    if (project.getPanel().hasFiducial(boardName, fiducialName) == false)
                      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                    Fiducial fiducial = project.getPanel().getFiducial(boardName, fiducialName);
                  alignmentGroup.addFiducial(fiducial);
                  line = null;
                }
                else
                {
                  matcher = _fiducialPanelPattern.matcher(line);
                  if (matcher.matches() == false)
                    throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

                  String fiducialName = matcher.group(1);

                  // find the correct panel fiducial
                  if (project.getPanel().hasFiducial(fiducialName) == false)
                    throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
                  Fiducial fiducial = project.getPanel().getFiducial(fiducialName);
                  alignmentGroup.addFiducial(fiducial);
                  line = null;
                  }
                }
              }
            }
            if (_projectReader.hasNextLine())
              line = _projectReader.getNextLine();
            else
              line = null; //Kee Chin Seong - Make it null once the line finish

            if (line == null)
              break;
          }
        }

        // assign empty aligment group
        if( boardSettings.hasLeftAlignmentGroups() == false )
        {
          boardSettings.assignEmptyLeftAlignmentGroups();
        }
        if( boardSettings.hasRightAlignmentGroups() == false )
        {
          boardSettings.assignEmptyRightAlignmentGroups();
        }

        _projectReader.closeFile();
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean isInspected(String inspectedOrNotStr) throws DatastoreException
  {
    Assert.expect(inspectedOrNotStr != null);

    boolean inspected = false;
    if (inspectedOrNotStr.equals("inspected"))
      inspected = true;
    else if (inspectedOrNotStr.equals("notInspected"))
      inspected = true;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return inspected;
  }
}
