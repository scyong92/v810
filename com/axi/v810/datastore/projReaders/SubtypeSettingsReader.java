package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * Creates Subtype.
 * @author Bill Darbie
 */
class SubtypeSettingsReader
{
  private static SubtypeSettingsReader _instance;

  private ProjectReader _projectReader;
  private String _fileName;
  private EnumStringLookup _enumStringLookup;

  // Version 1
  // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned userComment importedPackageLibraryPath");
  private Pattern _subtypePattern1 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"(.*)\"$");
  // Version 2
  // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned ExpectedImageAlgorithmLearned userComment");
  private Pattern _subtypePattern2 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"(.*)\"$");
  // Version 3
  // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned ExpectedImageAlgorithmLearned userComment");
  private Pattern _subtypePattern3 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"{1}(.*)\"{1}\\s+\"(.*)\"$");
  // Version 4 - ShengChuan - Chip Tombstone
  // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned ExpectedImageAlgorithmLearned ExpectedImageTemplateAlgorithmLearned userComment");
  private Pattern _subtypePattern4 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"{1}(.*)\"{1}\\s+\"(.*)\"$");
  // algoritmName algorithmVersion on|off
  private Pattern _algorithmNamePattern = Pattern.compile("^([^\\s]+)\\s+(\\d+)\\s+(on|off)$");
  // Version 1
  // algorithmSettingName tunedValueType tunedValue
  private Pattern _tunedAlgorithmSettingPattern1 = Pattern.compile("^([^\\s]+)\\s+([a-zA-Z]+)\\s+\"(.+)\"$");
//  // Version 2
//  // algorithmSettingName tunedValueType tunedValue importedFromLibrary modifiedByUser
//  private Pattern _tunedAlgorithmSettingPattern2 = Pattern.compile("^([^\\s]+)\\s+([a-zA-Z]+)\\s+\"(.+)\"\\s+([a-zA-Z]+)\\s+([a-zA-Z]+)$");
  // Version 2
  // algorithmSettingName tunedValueType tunedValue importedFromLibrary
  private Pattern _tunedAlgorithmSettingPattern2 = Pattern.compile("^([^\\s]+)\\s+([a-zA-Z]+)\\s+\"(.+)\"\\s+([a-zA-Z]+)$");
  // algorithmSettingName userComment
  private Pattern _commentedAlgorithmSettingPattern = Pattern.compile("^([^\\s]+)\\s+\"(.*)\"$");
  
  //Siew Yeng - sub subtype pattern
  // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned expectedImageAlgorithmLearned userComment importedPackageLibraryPath parentSubtype");
  private Pattern _subSubtypePattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"{1}(.*)\"{1}\\s+\"(.*)\"\\s+([^\\s]+)$");

  //ShengChuan - Clear Tombstone
  private Pattern _subSubtypePatternPattern2 = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)\\s+\"{1}(.*)\"{1}\\s+\"(.*)\"\\s+([^\\s]+)$");

  /**
   * @author Bill Darbie
   */
  static synchronized SubtypeSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new SubtypeSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SubtypeSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  public boolean isCurrentVersion(String projectName)
  {
    int fileVersion = FileName.getProjectSubtypeSettingsLatestFileVersion();
    _fileName = FileName.getProjectSubtypeSettingsFullPath(projectName, fileVersion);
    return FileUtilAxi.exists(_fileName);
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    int fileVersion = FileName.getProjectSubtypeSettingsLatestFileVersion();
    while (fileVersion > 0)
    {
      _fileName = FileName.getProjectSubtypeSettingsFullPath(projName, fileVersion);
      if (FileUtilAxi.exists(_fileName))
        break;
      --fileVersion;
    }
    Assert.expect(fileVersion > 0);

    // added by Wei Chin for Fix CR 1047
    // will removed it once the Merge with latest X6000 Code
//    boolean isGreaterThan116 = false;
    try
    {
      _projectReader.openFile(_fileName);

      Panel panel = project.getPanel();
      // origSubtypeName subtypeName inspectionFamilyName userComment
      String line = _projectReader.getNextLine();
      Matcher matcher = _subtypePattern3.matcher(line);
      if (fileVersion == 2)
      {
        matcher = _subtypePattern2.matcher(line);
      }
      else if (fileVersion == 1)
      {
        matcher = _subtypePattern1.matcher(line);
      }
      else if (fileVersion == 6)//ShengChuan - Chip Tombstone
      {
        matcher = _subtypePattern4.matcher(line);
      }
      while (matcher.matches())
      {
        String longSubtypeName = matcher.group(1);
        String shortSubtypeName = matcher.group(2);
        String inspectionFamilyName = matcher.group(3);
        String jointTypeEnumStr = matcher.group(4);
        String allAlgorithmExceptShortsLearnedStr = matcher.group(5);
        String shortsAlgorithmLearnedStr = matcher.group(6);
        String expectedImageAlgorithmLearnedStr;
        String expectedImageTemplateAlgorithmLearnedStr = "false";
        String userComment;
        String importedPackageLibraryPath = "";
        boolean isSubSubtype = false;
        String parentSubtype = "";
        if (fileVersion >= 2)
        {
          expectedImageAlgorithmLearnedStr = matcher.group(7);
          userComment = matcher.group(8);
        }
        else
        {
          expectedImageAlgorithmLearnedStr = "false";
          userComment = matcher.group(7);
        }
        if (fileVersion >= 3)
        {
          importedPackageLibraryPath = matcher.group(9);
        }
        
        //Siew Yeng - XCR-2764 - Sub-subtype feature
        if(fileVersion >=5)
        {
          if(fileVersion == 5)//ShengChuan - Clear Tombstone
          {
            if(matcher.groupCount() > 9)
            {
              parentSubtype = matcher.group(10);
              isSubSubtype = true;
            }
          }
          else if (fileVersion == 6)//ShengChuan - Clear Tombstone
          {
            if (matcher.groupCount() > 10)
            {
              parentSubtype = matcher.group(11);
              isSubSubtype = true;
            }
            expectedImageTemplateAlgorithmLearnedStr = matcher.group(8);
            userComment = matcher.group(9);
            importedPackageLibraryPath = matcher.group(10);
          }
        }

        JointTypeEnum jointTypeEnum = _enumStringLookup.getJointTypeEnum(jointTypeEnumStr);
        boolean allAlgorithmExceptShortsLearned;
        if (allAlgorithmExceptShortsLearnedStr.equals("false"))
          allAlgorithmExceptShortsLearned = false;
        else if (allAlgorithmExceptShortsLearnedStr.equals("true"))
          allAlgorithmExceptShortsLearned = true;
        else
          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
        boolean shortsAlgorithmLearned;
        if (shortsAlgorithmLearnedStr.equals("false"))
          shortsAlgorithmLearned = false;
        else if (shortsAlgorithmLearnedStr.equals("true"))
          shortsAlgorithmLearned = true;
        else
          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
        boolean expectedImageAlgorithmLearned;
        if (expectedImageAlgorithmLearnedStr.equals("false"))
          expectedImageAlgorithmLearned = false;
        else if (expectedImageAlgorithmLearnedStr.equals("true"))
          expectedImageAlgorithmLearned = true;
        else
          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
        //ShengChuan - Clear tombstone
        boolean expectedImageTemplateAlgorithmLearned;
        if (expectedImageTemplateAlgorithmLearnedStr.equals("false"))
          expectedImageTemplateAlgorithmLearned = false;
        else if (expectedImageTemplateAlgorithmLearnedStr.equals("true"))
          expectedImageTemplateAlgorithmLearned = true;
        else
          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

        InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum(inspectionFamilyName);

        // Early versions of the software used the Gullwing inspection family to test GFN joints.
        // Automatically convert any QFN joint using the Gullwing family to the QuadFlatNoLead family.
        boolean convertingToQuadFlatNoLead = false;
        if (jointTypeEnum.equals(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD) &&
            inspectionFamilyEnum.equals(InspectionFamilyEnum.GULLWING))
        {
          inspectionFamilyEnum = InspectionFamilyEnum.QUAD_FLAT_NO_LEAD;
          convertingToQuadFlatNoLead = true;
//          System.out.println("Converting " + longSubtypeName + ", " + shortSubtypeName + " to QFN inspection family");
        }

        Subtype subtype = new Subtype();
        subtype.setPanel(panel);
        subtype.setInspectionFamilyEnum(inspectionFamilyEnum);
        subtype.setJointTypeEnum(jointTypeEnum);
        subtype.setUserComment(userComment);
        subtype.setLongName(longSubtypeName, isSubSubtype);
        subtype.setShortName(shortSubtypeName);
        subtype.setAlgorithmSettingsLearned(allAlgorithmExceptShortsLearned);
        subtype.setJointSpecificDataLearned(shortsAlgorithmLearned);
        subtype.setExpectedImageDataLearned(expectedImageAlgorithmLearned);
        subtype.setExpectedImageTemplateLearned(expectedImageTemplateAlgorithmLearned);//ShengChuan - Clear Tombstone
        subtype.setImportedPackageLibraryPath(importedPackageLibraryPath);

        // If converting to QuadFlatNoLead family, any tuned settings are deleted and the "misalignment" algorithm is changed to "voiding".
        if (convertingToQuadFlatNoLead)
        {
          // PE:  I have added a new TunedSettingConverter utility that should be used for future setting deletions or conversions.
          // I am going to leave this gullwing-to-qfn conversion code alone, mainly because I don't want to break it
          // but also because my new TunedSettingConverter utility classes do not handle the Misalignment to Voiding
          // conversion.
          line = convertGullwingSettingsToQuadFlatNoLeadSettings(matcher, subtype, fileVersion);
        }
        else
        {
          // algorithmName algorithmVersion on|off
          line = _projectReader.getNextLine();
          matcher = _algorithmNamePattern.matcher(line);
          while (matcher.matches())
          {
            String algorithmName = matcher.group(1);
            String versionStr = matcher.group(2);
            int version = StringUtil.convertStringToInt(versionStr);
            String onOrOff = matcher.group(3);
            AlgorithmEnum algorithmEnum = getAlgorithmEnum(algorithmName);
            subtype.addAlgorithmEnum(algorithmEnum, version);
            if (onOrOff.equals("on"))
              subtype.setAlgorithmEnabled(algorithmEnum, true);

            if (_projectReader.hasNextLine() == false)
              break;
            line = _projectReader.getNextLine();

            // algorithmSettingName tunedValueType tunedValue importedFromLibrary
            matcher = _tunedAlgorithmSettingPattern2.matcher(line);
            if (fileVersion < 3)
            {
              matcher = _tunedAlgorithmSettingPattern1.matcher(line);
            }
            while (matcher.matches())
            {
              String algorithmSettingName = matcher.group(1);
              String tunedValueType = matcher.group(2);
              String tunedValueStr = matcher.group(3);
              String isImportedFromLibraryStr = null;
              boolean isImportedFromLibrary = false;
              boolean isModifiedByUser = false;
//              if (fileVersion == 3)
//              {
//                isImportedFromLibraryStr = matcher.group(4);
//                isModifiedByUserStr = matcher.group(5);
//                Assert.expect(isImportedFromLibraryStr != null);
//                Assert.expect(isModifiedByUserStr != null);
//
//                if (isImportedFromLibraryStr.equals("true"))
//                  isImportedFromLibrary = true;
//                else if (isImportedFromLibraryStr.equals("false"))
//                  isImportedFromLibrary = false;
//                else
//                  Assert.expect(false);
//
//                if (isModifiedByUserStr.equals("true"))
//                  isModifiedByUser = true;
//                else if (isModifiedByUserStr.equals("false"))
//                  isModifiedByUser = false;
//                else
//                  Assert.expect(false);
//              }
              //if(fileVersion == 3 || fileVersion == 4 || fileVersion == 5)
              //Siew Yeng
              if(fileVersion >= 3)
              {
                isImportedFromLibraryStr = matcher.group(4);
                Assert.expect(isImportedFromLibraryStr != null);

                if (isImportedFromLibraryStr.equals("true"))
                  isImportedFromLibrary = true;
                else if (isImportedFromLibraryStr.equals("false"))
                  isImportedFromLibrary = false;
                else
                  Assert.expect(false);
              }

              Serializable value = null;
              try
              {
                value = getSerializedValue(tunedValueType, tunedValueStr);
              }
              catch (BadFormatException bfe)
              {
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
              }

              if(fileVersion < 3 )
                setTunedValue(subtype, algorithmSettingName, value, fileVersion);

              //else if(fileVersion == 3 || fileVersion == 4 || fileVersion == 5)
              //Siew Yeng
              else if(fileVersion >= 3)
              {
                if(isImportedFromLibrary)
                {
                  if (EnumStringLookup.getInstance().isAlgorithmSettingEnumExist(algorithmSettingName)==false)
                    System.out.println("Warning: "+algorithmSettingName+" algorithm setting in "+subtype.getShortName()+" subtype will be ignored because it does not exist in this version.");
                  else
                  {
                    AlgorithmSettingEnum algorithmSettingEnum = null;
                    algorithmSettingEnum = getAlgorithmSettingEnum(algorithmSettingName);
                    Assert.expect(algorithmSettingEnum != null);
                    subtype.addAlgorithmsSettingEnumToImportedValueMap(algorithmSettingEnum, value);
                  }
                }
                else if (EnumStringLookup.getInstance().isAlgorithmSettingEnumExist(algorithmSettingName)==false)
                {
                  System.out.println("Warning: "+algorithmSettingName+" algorithm setting in "+subtype.getShortName()+" subtype will be ignored because it does not exist in this version.");
                }
                // added by Wei Chin for Fix CR 1047
                // will removed it once the Merge with latest X6000 Code
//                else if("chipMeasurementUpdateNorminalFilletThicknessMethod".equals(algorithmSettingName) ||
//                        "chipMeasurementNominalShiftRotation".equals(algorithmSettingName) ||
//                        "chipMeasurementNominalPad1ShiftAlong".equals(algorithmSettingName) ||
//                        "chipMeasurementNominalPad2ShiftAlong".equals(algorithmSettingName) ||
//                        "chipMisalignmentMaxComponentShiftAlong".equals(algorithmSettingName) ||
//                        "advancedGullwingNominalHeelToPadCenter".equals(algorithmSettingName) ||
//                        "advancedGullwingPadCenterReference".equals(algorithmSettingName) ||
//                        "advancedGullwingMisalignmentHeelToPadCenterDifferenceFromNominal".equals(algorithmSettingName) ||
//                        "advancedGullwingMisalignmentMaximumMassShiftAcrossPercent".equals(algorithmSettingName) )
//                {
//                    // do nothing
//                    isGreaterThan116 = true;
//                    System.out.println("Warning: This Project is create by x6000 ver 1.16 or greater!");
//                    System.out.println("         New algorithm setting added from 1.16 or greater will be removed.");
//                }
                else
                  setTunedValue(subtype, algorithmSettingName, value, fileVersion);
              }

              if (_projectReader.hasNextLine() == false)
                break;
              line = _projectReader.getNextLine();
              matcher = _tunedAlgorithmSettingPattern2.matcher(line);
              if (fileVersion < 3)
              {
                matcher = _tunedAlgorithmSettingPattern1.matcher(line);
              }
            }

            // algorithmSettingName userComment
            matcher = _commentedAlgorithmSettingPattern.matcher(line);
            while (matcher.matches())
            {
              String algorithmSettingName = matcher.group(1);
              String comment = matcher.group(2);

              AlgorithmSettingEnum algorithmSettingEnum = getAlgorithmSettingEnum(algorithmSettingName);
              subtype.setAlgorithmSettingUserComment(algorithmSettingEnum, comment);

              if (_projectReader.hasNextLine() == false)
                break;
              line = _projectReader.getNextLine();
              matcher = _commentedAlgorithmSettingPattern.matcher(line);
            }
            matcher = _algorithmNamePattern.matcher(line);
          }
          
          //Siew Yeng - XCR-2764 - Sub-subtype feature
          if(isSubSubtype)
          {
            subtype.setIsSubSubtype(isSubSubtype);
            Subtype parent = panel.getSubtype(parentSubtype);
            parent.addSubSubtype(subtype);
          }
        }
        if (_projectReader.hasNextLine() == false)
          break;
        
        if(fileVersion == 5)
        {
          matcher = _subSubtypePattern.matcher(line);
          if(matcher.matches() == false)
            matcher = _subtypePattern3.matcher(line);
        }
        else if(fileVersion == 6)//ShengChuan - Clear Tombstone
        {
          matcher = _subSubtypePatternPattern2.matcher(line);
          if(matcher.matches() == false)
          {
            matcher = _subtypePattern4.matcher(line);
          }
        }
        else
          matcher = _subtypePattern3.matcher(line);
        
        if (fileVersion == 2)
        {
          matcher = _subtypePattern2.matcher(line);
        }
        else if (fileVersion == 1)
        {
          matcher = _subtypePattern1.matcher(line);
        }
      }

      // added by Wei Chin for Fix CR 1047 
      // will removed it once the Merge with latest X6000 Code
//      if(isGreaterThan116)
//      {
//          System.out.println("Warning: This Project is create by x6000 ver 1.16 or greater!");
//          System.out.println("         New algorithm setting added from 1.16 or greater will be removed.");
//      }

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bfe)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(bfe);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }

  /**
   * Set the tuned value for the given algorithm setting.  This will sometimes do some conversions to handle
   * obsolete or changed settings.
   *
   * @todo use the new TunedSettingConversionEngine
   *
   * @author Peter Esbensen
   * @author Geprge Booth
   * @Edited By Kee Chin Seong - When the old recipes which contains the Short algorithm broken pin inspected
   *                             2.5D slice, the tunedvalue automatic set to "Both" as default
   */
  private void setTunedValue(Subtype subtype,
                             String algorithmSettingName,
                             Serializable value,
                             int fileVersion) throws DatastoreException, BadFormatException
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingName != null);
    Assert.expect(algorithmSettingName.length() > 0);
    Assert.expect(value != null);

    AlgorithmSettingEnum algorithmSettingEnum = null;

    TunedSettingConversionEngine tunedSettingConversionEngine = new TunedSettingConversionEngine();

    // Sometimes we want to completely obsolete an old setting and pretend that it never existed.
    if (tunedSettingConversionEngine.settingIsObsoleteDoNotConvert( algorithmSettingName ))
      return;
    
    //Khaw Chek Hau - XCR2514 : Threshold value changed by itself after using software 5.6
    if (fileVersion < 5 && getAlgorithmSettingEnum(algorithmSettingName).equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS))
    {
      //Use 0.635mm as compare threshold as it is the high limit of mm working unit for minimum thickness
      if ((Float)value > 0.635)
        value = MathUtil.convertMilsToMillimeters((Float)value);
    }

    //Check the tune value if is inspected slice contain 2.5 is not allowed!
    //Lim, Lay Ngor - remove this checking for BrokenPin branch since it is allow 2.5D combo selection.
//    if (subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) && 
//        getAlgorithmSettingEnum(algorithmSettingName).equals(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE))
//    {
//        String strValue = (String)value;
//        
//        if(strValue.contains("2.5D"))
//           value = "Both";
//    }
   
    // Sometimes we want to split an old obsolete setting into new settings with the same value.  Do that here.
    if (tunedSettingConversionEngine.splitIsRequired( algorithmSettingName ))
    {
      ArrayList<AlgorithmSettingEnum> newSettingsList = tunedSettingConversionEngine.getSplitAlgorithmSettingList( algorithmSettingName );
      for (AlgorithmSettingEnum settingEnum : newSettingsList)
      {
        subtype.setTunedValue(settingEnum, value);
      }
      return;
    }

    // Sometimes we want to convert an old obsolete setting by converting it to a new setting.  Do that here.
    if (tunedSettingConversionEngine.conversionRequired( algorithmSettingName ))
    {
      Pair<AlgorithmSettingEnum, Serializable> conversionResult = tunedSettingConversionEngine.getConvertedAlgorithmSetting( algorithmSettingName,
                                                                                                                             value, subtype );
      algorithmSettingEnum = conversionResult.getFirst();
      value = conversionResult.getSecond();
    }
    else // this must be a regular algorithm setting . . . just lookup the associated enum
    {
      algorithmSettingEnum = getAlgorithmSettingEnum(algorithmSettingName);
    }

    Assert.expect(algorithmSettingEnum != null);
    subtype.setTunedValue(algorithmSettingEnum, value);
  }

  /**
   * PE:  I have added a new TunedSettingConverter utility that should be used for future setting deletions or conversions.
   * I am going to leave this gullwing-to-qfn conversion code alone, mainly because I don't want to break it
   * but also because my new TunedSettingConverter utility classes do not handle the Misalignment to Voiding
   * conversion.

   * @author George Booth
   */
  private String convertGullwingSettingsToQuadFlatNoLeadSettings(Matcher matcher, Subtype subtype, int fileVersion) throws DatastoreException, BadFormatException
  {
    Assert.expect(matcher != null);
    Assert.expect(subtype != null);

    // indicate settings need to be learned
    subtype.setAlgorithmSettingsLearned(false);
    subtype.setJointSpecificDataLearned(false);
    subtype.setExpectedImageDataLearned(false);
    subtype.setExpectedImageTemplateLearned(false);//ShengChuan Clear Tombstone

    // algorithName algorithmVersion on|off
    String line = _projectReader.getNextLine();
    matcher = _algorithmNamePattern.matcher(line);
    while (matcher.matches())
    {
      String algorithmName = matcher.group(1);
      String versionStr = matcher.group(2);
      int version = StringUtil.convertStringToInt(versionStr);
      String onOrOff = matcher.group(3);
      if (algorithmName.equals(_enumStringLookup.getAlgorithmString(AlgorithmEnum.MISALIGNMENT)))
      {
        // QFN does not have misalignment but has voiding - change it
        AlgorithmEnum algorithmEnum = AlgorithmEnum.VOIDING;
        subtype.addAlgorithmEnum(algorithmEnum, 1);
        subtype.setAlgorithmEnabled(algorithmEnum, true);
      }
      else
      {
        AlgorithmEnum algorithmEnum = getAlgorithmEnum(algorithmName);
        subtype.addAlgorithmEnum(algorithmEnum, version);
        if (onOrOff.equals("on"))
          subtype.setAlgorithmEnabled(algorithmEnum, true);
      }

      if (_projectReader.hasNextLine() == false)
        return line;
      line = _projectReader.getNextLine();

      // algorithmSettingName tunedValueType tunedValue
      // check for valid entries then ignore them
      matcher = _tunedAlgorithmSettingPattern2.matcher(line);
      if (fileVersion < 3)
      {
        matcher = _tunedAlgorithmSettingPattern1.matcher(line);
      }
      while (matcher.matches())
      {
        String algorithmSettingName = matcher.group(1);
        String tunedValueType = matcher.group(2);
        String tunedValueStr = matcher.group(3);
        Serializable value = null;
        try
        {
          value = getSerializedValue(tunedValueType, tunedValueStr);
        }
        catch (BadFormatException bfe)
        {
          throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
        }

        // ignore the tuned value
        // setTunedValue(subtype, algorithmSettingName, value);

        if (_projectReader.hasNextLine() == false)
          return line;
        line = _projectReader.getNextLine();
        matcher = _tunedAlgorithmSettingPattern2.matcher(line);
        if (fileVersion < 3)
        {
          matcher = _tunedAlgorithmSettingPattern1.matcher(line);
        }
      }

      // algorithmSettingName userComment
      matcher = _commentedAlgorithmSettingPattern.matcher(line);
      while (matcher.matches())
      {
        String algorithmSettingName = matcher.group(1);
        String comment = matcher.group(2);

        // ignore the comment
        // AlgorithmSettingEnum algorithmSettingEnum = getAlgorithmSettingEnum(algorithmSettingName);
        // subtype.setAlgorithmSettingUserComment(algorithmSettingEnum, comment);

        if (_projectReader.hasNextLine() == false)
         return line;
        line = _projectReader.getNextLine();
        matcher = _commentedAlgorithmSettingPattern.matcher(line);
      }
      matcher = _algorithmNamePattern.matcher(line);
    }
    return line;
  }

  /**
   * @author Bill Darbie
   */
  private InspectionFamilyEnum getInspectionFamilyEnum(String inspectionFamilyName)
  {
    Assert.expect(inspectionFamilyName != null);

    InspectionFamilyEnum inspectionFamilyEnum = _enumStringLookup.getInspectionFamilyEnum(inspectionFamilyName);
    return inspectionFamilyEnum;
  }

  /**
   * @author Bill Darbie
   */
  private AlgorithmEnum getAlgorithmEnum(String algorithmName)
  {
    Assert.expect(algorithmName != null);

    AlgorithmEnum algorithmEnum = _enumStringLookup.getAlgorithmEnum(algorithmName);
    return algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  private AlgorithmSettingEnum getAlgorithmSettingEnum(String algorithmSettingName)
  {
    Assert.expect(algorithmSettingName != null);

    AlgorithmSettingEnum algorithmSettingEnum = _enumStringLookup.getAlgorithmSettingEnum(algorithmSettingName);
    return algorithmSettingEnum;
  }

  /**
   * @author Bill Darbie
   */
  private Serializable getSerializedValue(String tunedValueType, String tunedValueStr) throws BadFormatException, DatastoreException
  {
    Assert.expect(tunedValueType != null);
    Assert.expect(tunedValueStr != null);

    Serializable value = null;

    if (tunedValueType.equals("int"))
      value = StringUtil.convertStringToInt(tunedValueStr);
    else if (tunedValueType.equals("string"))
      value = (String)tunedValueStr;
    else if (tunedValueType.equals("float"))
      value = StringUtil.convertStringToFloat(tunedValueStr);
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return value;
  }
}
