package com.axi.v810.datastore.projReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * Creates ComponentTypeSettings, PadTypeSettings
 * @author Bill Darbie
 */
class BoardTypeSettingsReader
{
  private static BoardTypeSettingsReader _instance;

  private EnumStringLookup _enumStringLookup;
  private String _fileName;

  private ProjectReader _projectReader;
  //refDes1 inspected|notInspected singleSubtype subtypeName
  private Pattern _refDesWithInspectionAndSubtypePattern = Pattern.compile("^([^\\s]+)\\s+(inspected|notInspected)\\s+(singleSubtype)\\s+([^\\s]+)$");
  //refDes2 inspected|notInspected multipleSubtypes
  private Pattern _refDesWithInspectionPattern = Pattern.compile("^([^\\s]+)\\s+(inspected|notInspected)\\s+(multipleSubtypes)$");
  //refDes3
  private Pattern _refDesPattern = Pattern.compile("^([^\\s]+)$");
  //  padName1 subTypeName
  private Pattern _padAndSubtypePattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");
  //  padName1 testable|notTestable inspected|notInspected subTypeName
  private Pattern _padAndInspectedAndSubtypePattern1 = Pattern.compile("^([^\\s]+)\\s+(testable|notTestable)\\s+(inspected|notInspected)\\s+([^\\s]+)$");
  //  padName1 subTypeName signalCompensation allowShadingCompensation
  private Pattern _padAndSubtypeAndSignalCompensationAndArtifactCompensationPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high)\\s+(compensated|notCompensated|noneSpecified)$");
  //  padName1 testable|notTestable inspected|notInspected subTypeName signalCompensation allowShadingCompensation
  private Pattern _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationPattern = Pattern.compile("^([^\\s]+)\\s+(testable|notTestable)\\s+(inspected|notInspected)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high)\\s+(compensated|notCompensated|noneSpecified)$");
  //  padName1 subTypeName signalCompensation allowShadingCompensation globalSurfaceModel
  private Pattern _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high)\\s+(compensated|notCompensated|noneSpecified)\\s+(autoFocus|GSM|autoSelect|RFB)$");
  //  padName1 subTypeName signalCompensation allowShadingCompensation globalSurfaceModel userGain
  private Pattern _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high|superHigh)\\s+(compensated|notCompensated|noneSpecified)\\s+(autoFocus|GSM|autoSelect|RFB|surfaceMap)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)$");
  //  padName1 testable|notTestable inspected|notInspected subTypeName signalCompensation allowShadingCompensation globalSurfaceModel
  private Pattern _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern = Pattern.compile("^([^\\s]+)\\s+(testable|notTestable)\\s+(inspected|notInspected)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high)\\s+(compensated|notCompensated|noneSpecified)\\s+(autoFocus|GSM|autoSelect|RFB)$");
  //  padName1 testable|notTestable inspected|notInspected subTypeName signalCompensation allowShadingCompensation globalSurfaceModel userGain
  private Pattern _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern = Pattern.compile("^([^\\s]+)\\s+(testable|notTestable)\\s+(inspected|notInspected)\\s+([^\\s]+)\\s+(defaultLow|medium|mediumHigh|high|superHigh)\\s+(compensated|notCompensated|noneSpecified)\\s+(autoFocus|GSM|autoSelect|RFB)\\s+(1.0|2.0|3.0|4.0|5.0|6.0|7.0|8.0|9.0)$");

  //  padName1 subTypeName globalSurfaceModel
  private Pattern _padAndSubtypeAndGlobalSurfaceModelPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+(autoFocus|GSM|autoSelect|RFB)$");
  //  padName1 testable|notTestable inspected|notInspected subTypeName globalSurfaceModel
  private Pattern _padAndInspectedAndSubtypeAndGlobalSurfaceModelPattern = Pattern.compile("^([^\\s]+)\\s+(testable|notTestable)\\s+(inspected|notInspected)\\s+([^\\s]+)\\s+(autoFocus|GSM|autoSelect|RFB)$");

  // new subtypeNamePattern
  private Pattern _newSubtypeNamePattern = Pattern.compile("^(.*_IL[\\d]_IC[\\d]_G[\\d]_M[\\d])$");
  private final String IL = "IL";
  private final String IC = "IC";
  private final String MAGNIFICATION = "M";
  private final String GAIN = "G";
  private final String UNDERSCORE = "_";
  
  private File _subtypeConversionFile = null;
  private PrintWriter _subtypeConversionFileWriter = null;
  
  private Set<Subtype> _subTypeSet = new HashSet<Subtype>();

  /**
   * @author Bill Darbie
   */
  static synchronized BoardTypeSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new BoardTypeSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardTypeSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _subTypeSet.clear();
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project, Map<Integer, BoardType> boardTypeNumberToBoardTypeMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardTypeNumberToBoardTypeMap != null);

    // create boardTypeNameToBoardTypeNumberMap
    Map<String, Integer> boardTypeNameToBoardTypeNumberMap = new HashMap<String, Integer>();
    for (Map.Entry<Integer, BoardType> entry : boardTypeNumberToBoardTypeMap.entrySet())
    {
      Integer number = entry.getKey();
      BoardType boardType = entry.getValue();
      String boardTypeName = boardType.getName();
      boardTypeNameToBoardTypeNumberMap.put(boardTypeName, number);
    }

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    _fileName = "";
    try
    {
      for (BoardType boardType : project.getPanel().getBoardTypes())
      {
        String boardTypeName = boardType.getName();
        int boardTypeNumber = boardTypeNameToBoardTypeNumberMap.get(boardTypeName);

        int version = FileName.getProjectBoardTypeSettingsLatestFileVersion();
        while (version > 0)
        {
          _fileName = FileName.getProjectBoardTypeSettingsFullPath(projName, boardTypeNumber, version);
          if (FileUtilAxi.exists(_fileName))
            break;
          --version;
        }

        // conversion csv
        if(version < 8)
        {
          _subtypeConversionFile = new File(_fileName + "-subtypeConversion.csv");
          if (_subtypeConversionFile.exists())
          {
            _subtypeConversionFile.delete();
          }
          try
          {
            _subtypeConversionFileWriter = new PrintWriter(_subtypeConversionFile);
          }
          catch(FileNotFoundException fnfe)
          {
            FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_subtypeConversionFile.getName());
            de.initCause(fnfe);
            throw de;
          }          
          _subtypeConversionFileWriter.println("Subtypes size before convert :  "  + project.getPanel().getSubtypes().size());
        }

        _projectReader.openFile(_fileName);

        Panel panel = project.getPanel();
        if (_projectReader.hasNextLine() == false)
          return;

        String line = _projectReader.getNextLine();

        while (true)
        {
          if (line == null)
            break;
          // figure out which type of line we have
          int lineType = 0;
          Matcher matcher = _refDesWithInspectionAndSubtypePattern.matcher(line);
          if (matcher.matches())
            lineType = 1;
          if (lineType == 0)
          {
            matcher = _refDesWithInspectionPattern.matcher(line);
            if (matcher.matches())
              lineType = 2;
          }
          if (lineType == 0)
          {
            matcher = _refDesPattern.matcher(line);
            if (matcher.matches())
              lineType = 3;
          }

          if (lineType == 0)
            throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

          if (lineType == 1)
          {
            //refDes1 inspected|notInspected singleSubtype subtypeName
            String refDes = matcher.group(1);
            String inspectedOrNotStr = matcher.group(2);
            String subtypeOrigName = matcher.group(4);

            if (panel.hasComponentType(boardTypeName, refDes) == false)
              throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            ComponentType componentType = panel.getComponentType(boardTypeName, refDes);
            boolean inspected = isInspected(inspectedOrNotStr);
            createComponentTypeSettings(componentType);

            inspected = isInspected(inspectedOrNotStr);

            Subtype subtype = getSubtype(panel, subtypeOrigName, _fileName);

            String newSubtypeName = "";
            if( version < 8 )
            {
              newSubtypeName = getSubtypeName(subtype, SignalCompensationEnum.DEFAULT_LOW, ArtifactCompensationStateEnum.NOT_APPLICABLE, UserGainEnum.ONE, MagnificationTypeEnum.LOW);
            }

            // in this case all Pads have the same subtype
            for (PadType padType : componentType.getPadTypes())
            {
              if( version < 8 )
              {
                String newSubtypeLongName = newSubtypeName + UNDERSCORE + padType.getJointTypeEnum().getNameWithoutSpaces();
                subtype = getSubtypeOrCreateNewSubtype(panel, newSubtypeLongName, newSubtypeName, subtype, padType.getLandPatternPad().getLandPattern(), padType.getJointTypeEnum());
                subtype.getSubtypeAdvanceSettings().setSignalCompensation(SignalCompensationEnum.DEFAULT_LOW);
                subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(false);
                subtype.getSubtypeAdvanceSettings().setUserGain(UserGainEnum.ONE);
                createPadTypeSettings(_fileName, padType, subtype, true, inspected);
              }
              else
                createPadTypeSettings(_fileName, padType, subtype, true, inspected);
              
              for (Pad pad : padType.getPads())
              {
                createPadSettings(pad, true);
              }
            }

            if (_projectReader.hasNextLine() == false)
            {
              line = null;
              break;
            }
            line = _projectReader.getNextLine();
          }
          else if (lineType == 2)
          {
            //refDes2 inspected|notInspected multipleSubtypes
            String refDes = matcher.group(1);
            String inspectedOrNotStr = matcher.group(2);

            if (panel.hasComponentType(boardTypeName, refDes) == false)
              throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            ComponentType componentType = panel.getComponentType(boardTypeName, refDes);
            boolean inspected = isInspected(inspectedOrNotStr);
            createComponentTypeSettings(componentType);

            line = _projectReader.getNextLine();
            if (version == 1 || version == 2)
              matcher = _padAndSubtypePattern.matcher(line);
            else if (version == 3 || version == 4)
              matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationPattern.matcher(line);
            else if (version == 5)
              matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern.matcher(line);
             else if(version == 6 || version == 7)
              matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern.matcher(line);
            else if(version==8)
              matcher = _padAndSubtypeAndGlobalSurfaceModelPattern.matcher(line);
            else
              Assert.expect(false);

            while (matcher.matches())
            {
              //  padName1 subTypeName
              String padName = matcher.group(1);
              String subtypeOrigName = matcher.group(2);

              if (componentType.hasPadType(padName) == false)
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

              PadType padType = componentType.getPadType(padName);
              Subtype subtype = getSubtype(panel, subtypeOrigName, _fileName);

              // now check to see if a signal compensation was specified
              if(version == 3 || version == 4 || version == 5 || version == 6 || version == 7)
              {
                SignalCompensationEnum signalComp = _enumStringLookup.getSignalCompensationEnum(matcher.group(3));
                ArtifactCompensationStateEnum artifactCompensationState = getArtifactCompensationState(matcher.group(4), version);                
                
                UserGainEnum userGain = UserGainEnum.ONE;
                if(version == 6 || version == 7)
                {
                  userGain = _enumStringLookup.getUserGainEnum(matcher.group(6));
                }
                String newSubtypeName = getSubtypeName(subtype, signalComp, artifactCompensationState, userGain, MagnificationTypeEnum.LOW);
                String newSubtypeLongName = newSubtypeName + UNDERSCORE + padType.getJointTypeEnum().getNameWithoutSpaces();
                subtype = getSubtypeOrCreateNewSubtype(panel, newSubtypeLongName, newSubtypeName, subtype, padType.getLandPatternPad().getLandPattern(), padType.getJointTypeEnum());
                subtype.getSubtypeAdvanceSettings().setSignalCompensation(signalComp);
                if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
                  subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(false);
                else
                  subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(artifactCompensationState);
                subtype.getSubtypeAdvanceSettings().setUserGain(userGain);
              }

              createPadTypeSettings(_fileName, padType, subtype, true, inspected);
              
              for (Pad pad : padType.getPads())
              {
                createPadSettings(pad, true);
              }

              //*Jack Hwee - automatically change to AutoFocus
              // now check to see if a global surface model was specified
              if (version == 5 || version == 6 || version == 7)
              {
                if (matcher.group(5).equalsIgnoreCase("surfaceMap"))
                {
                  padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.AUTOFOCUS);             
                }
                else
                {
                  GlobalSurfaceModelEnum globalSurfaceModel = _enumStringLookup.getGlobalSurfaceModelEnum(matcher.group(5));
                  padType.getPadTypeSettings().setGlobalSurfaceModel(globalSurfaceModel);
                }
              }
              if(version == 8)
              {
                GlobalSurfaceModelEnum globalSurfaceModel = _enumStringLookup.getGlobalSurfaceModelEnum(matcher.group(3));
                padType.getPadTypeSettings().setGlobalSurfaceModel(globalSurfaceModel);
              }

              if (_projectReader.hasNextLine() == false)
              {
                line = null;
                break;
              }
              line = _projectReader.getNextLine();
              if (version == 1 || version == 2)
                matcher = _padAndSubtypePattern.matcher(line);
              else if (version == 3 || version == 4)
                matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationPattern.matcher(line);
              else if (version == 5)
                matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern.matcher(line);
              else if(version == 6 || version == 7)
                matcher = _padAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern.matcher(line);
              else if(version==8)
                matcher = _padAndSubtypeAndGlobalSurfaceModelPattern.matcher(line);
              else
                Assert.expect(false);
            }
          }
          else if (lineType == 3)
          {
            //refDes2
            String refDes = matcher.group(1);

            if (panel.hasComponentType(boardTypeName, refDes) == false)
              throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
            ComponentType componentType = panel.getComponentType(boardTypeName, refDes);
            createComponentTypeSettings(componentType);

            line = _projectReader.getNextLine();
            if (version == 1 || version == 2)
              matcher = _padAndInspectedAndSubtypePattern1.matcher(line);
            else if (version == 3 || version == 4)
              matcher = _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationPattern.matcher(line);
            else if (version == 5)
              matcher = _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern.matcher(line);
            else if(version == 6 || version == 7)
              matcher= _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern.matcher(line);
            else if(version==8)
              matcher= _padAndInspectedAndSubtypeAndGlobalSurfaceModelPattern.matcher(line);
            else
              Assert.expect(false);

            while (matcher.matches())
            {
              //  padName1 testable|notTestable inspected|notInspected subTypeName
              String padName = matcher.group(1);
              String testableOrNotStr = matcher.group(2);
              String inspectedOrNotStr = matcher.group(3);
              String subtypeOrigName = matcher.group(4);

              boolean testable = isTestable(testableOrNotStr);
              boolean inspected = isInspected(inspectedOrNotStr);
              if (componentType.hasPadType(padName) == false)
                throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
              PadType padType = componentType.getPadType(padName);

              Subtype subtype = getSubtype(panel, subtypeOrigName, _fileName);

              // now check to see if a signal compensation was specified
              if(version == 3 || version == 4 || version == 5 || version == 6 || version == 7)
              {
                SignalCompensationEnum signalComp = _enumStringLookup.getSignalCompensationEnum(matcher.group(5));
                ArtifactCompensationStateEnum artifactCompensationState = getArtifactCompensationState(matcher.group(6) , version);
                
                UserGainEnum userGain = UserGainEnum.ONE;
                // now check to see if user gain was specified
                if (version == 6 || version == 7)
                {
                  userGain = _enumStringLookup.getUserGainEnum(matcher.group(8));
                }
                
                String newSubtypeName = getSubtypeName(subtype, signalComp, artifactCompensationState, userGain, MagnificationTypeEnum.LOW);
                String newSubtypeLongName = newSubtypeName + UNDERSCORE + padType.getJointTypeEnum().getNameWithoutSpaces();
                subtype = getSubtypeOrCreateNewSubtype(panel, newSubtypeLongName, newSubtypeName, subtype, padType.getLandPatternPad().getLandPattern(), padType.getJointTypeEnum());
                subtype.getSubtypeAdvanceSettings().setSignalCompensation(signalComp);
                if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
                  subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(false);
                else
                  subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(artifactCompensationState);
                subtype.getSubtypeAdvanceSettings().setUserGain(userGain);
              }

              createPadTypeSettings(_fileName, padType, subtype, testable, inspected);
              
              for (Pad pad : padType.getPads())
              {
                createPadSettings(pad, true);
              }

              // now check to see if a global surface model was specified
              if (version == 5|| version == 6 || version == 7)
              {
                GlobalSurfaceModelEnum globalSurfaceModel = _enumStringLookup.getGlobalSurfaceModelEnum(matcher.group(7));
                padType.getPadTypeSettings().setGlobalSurfaceModel(globalSurfaceModel);
              }
              if(version == 8)
              {
                GlobalSurfaceModelEnum globalSurfaceModel = _enumStringLookup.getGlobalSurfaceModelEnum(matcher.group(5));
                padType.getPadTypeSettings().setGlobalSurfaceModel(globalSurfaceModel);
              }
              if (_projectReader.hasNextLine() == false)
              {
                line = null;
                break;
              }
              line = _projectReader.getNextLine();
              if (version == 1 || version == 2)
                matcher = _padAndInspectedAndSubtypePattern1.matcher(line);
              else if (version == 3 || version == 4)
                matcher = _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationPattern.matcher(line);
              else if (version == 5)
                matcher = _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelPattern.matcher(line);
              else if (version == 6 || version == 7)
                matcher = _padAndInspectedAndSubtypeAndSignalCompensationAndArtifactCompensationAndGlobalSurfaceModelAndUserGainPattern.matcher(line);
              else if (version == 8)
                matcher = _padAndInspectedAndSubtypeAndGlobalSurfaceModelPattern.matcher(line);
              else
                Assert.expect(false);
            }
          }
          else
            Assert.expect(false);
        }

        // conversion csv
        if(version < 8)
        {
          _subtypeConversionFileWriter.println();
          _subtypeConversionFileWriter.flush();
          _subtypeConversionFileWriter.close();
        }
        _projectReader.closeFile();
      }
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean isTestable(String testableOrNotStr) throws DatastoreException
  {
    Assert.expect(testableOrNotStr != null);

    boolean testable = false;
    if (testableOrNotStr.equals("testable"))
      testable = true;
    else if (testableOrNotStr.equals("notTestable"))
      testable = false;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return testable;
  }


  /**
   * @author Bill Darbie
   */
  private boolean isInspected(String inspectedOrNotStr) throws DatastoreException
  {
    Assert.expect(inspectedOrNotStr != null);

    boolean inspected = false;
    if (inspectedOrNotStr.equals("inspected"))
      inspected = true;
    else if (inspectedOrNotStr.equals("notInspected"))
      inspected = false;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return inspected;
  }

  /**
   * @author George A. David
   */
  private ArtifactCompensationStateEnum getArtifactCompensationState(String shadingCompensationString, int version) throws DatastoreException
  {
    ArtifactCompensationStateEnum allowCompensation = null;
    if(shadingCompensationString.equals("compensated"))
      allowCompensation = ArtifactCompensationStateEnum.COMPENSATED;
    else if(shadingCompensationString.equals("notCompensated"))
      allowCompensation = ArtifactCompensationStateEnum.NOT_COMPENSATED;
    else if(shadingCompensationString.equals("noneSpecified") && version == 3)
      allowCompensation = ArtifactCompensationStateEnum.NOT_COMPENSATED;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return allowCompensation;
  }

  /**
   * @author Bill Darbie
   */
  private boolean isLoaded(String loadedOrNotStr) throws DatastoreException
  {
    Assert.expect(loadedOrNotStr != null);

    boolean loaded = false;
    if (loadedOrNotStr.equals("loaded"))
      loaded = true;
    else if (loadedOrNotStr.equals("notLoaded"))
      loaded = true;
    else
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());

    return loaded;
  }

  /**
   * @author Bill Darbie
   */
  private void createComponentTypeSettings(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
    componentType.setComponentTypeSettings(componentTypeSettings);
    componentTypeSettings.setLoaded(true);
  }

  /**
   * @author Bill Darbie
   */
  private void createPadTypeSettings(String fileName, PadType padType, Subtype subtype, boolean testable, boolean inspected) throws FileCorruptDatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(padType != null);
    Assert.expect(subtype != null);

    PadTypeSettings padTypeSettings = new PadTypeSettings();
    //padTypeSettings.setTestable(testable);
    padTypeSettings.setInspected(inspected);
    padType.setPadTypeSettings(padTypeSettings);
    padTypeSettings.setPadType(padType);
    padTypeSettings.setSubtype(subtype);
    
    // set this subtypes jointTypeEnum if it has not already been set
    JointTypeEnum jointTypeEnum = padType.getPackagePin().getJointTypeEnum();
    if (_subTypeSet.add(subtype))
    {
      subtype.setJointTypeEnum(jointTypeEnum);
    }
    else
    {
      if (subtype.getJointTypeEnum().equals(jointTypeEnum) == false)
        throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void createPadSettings(Pad pad, boolean testable) throws FileCorruptDatastoreException
  {
    Assert.expect(pad != null);
 
    PadSettings padSettings = new PadSettings();
    padSettings.setTestable(testable);
    pad.setPadSettings(padSettings);
    padSettings.setPad(pad);
  }

  /**
   * @author Bill Darbie
   */
  private InspectionFamilyEnum getInspectionFamilyEnum(String inspectionFamilyName)
  {
    Assert.expect(inspectionFamilyName != null);

    InspectionFamilyEnum inspectionFamilyEnum = _enumStringLookup.getInspectionFamilyEnum(inspectionFamilyName);
    return inspectionFamilyEnum;
  }

  /**
   * @author Bill Darbie
   */
  private Subtype getSubtype(Panel panel, String subtypeOrigName, String fileName) throws FileCorruptDatastoreException
  {
    Assert.expect(panel != null);
    Assert.expect(subtypeOrigName != null);
    Assert.expect(fileName != null);

    if (panel.doesSubtypeExist(subtypeOrigName) == false)
      throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
    Subtype subtype = panel.getSubtype(subtypeOrigName);

    return subtype;
  }
  
  /**
   * @param oldSubtype
   * @param signalComp
   * @param artifactCompensationState
   * @param userGain
   * @param magnification
   * @return 
   * @authoer Wei Chin
   */  
  private String getSubtypeName(Subtype oldSubtype,
    SignalCompensationEnum signalComp,
    ArtifactCompensationStateEnum artifactCompensationState,
    UserGainEnum userGain, MagnificationTypeEnum magnification)
  {
    String newSubtypeName = oldSubtype.getShortName();
    
    Matcher matcher = _newSubtypeNamePattern.matcher(oldSubtype.getShortName());
    if(matcher.matches())
    {
      _subtypeConversionFileWriter.println("Match before convert :  " + oldSubtype.getShortName() );
      newSubtypeName += "_CONV";
    }
    
    // IL
    newSubtypeName += (UNDERSCORE + IL + signalComp.toString());
    
    // IC
    newSubtypeName += (UNDERSCORE + IC + artifactCompensationState.getId());
    
    // Gain
    newSubtypeName += (UNDERSCORE + GAIN + userGain.toString());
    
    // Magnification
    newSubtypeName += (UNDERSCORE + MAGNIFICATION + magnification.getId() );
    
    // WC: for debug purpose 
    if(false)
    {
      matcher = _newSubtypeNamePattern.matcher(newSubtypeName);
      if(matcher.matches())
      {
        System.out.println("Match  : " + newSubtypeName);
      }
    }
    
    return newSubtypeName;
  }
  
  /**
   * @author Wei Chin
   */
  private Subtype getSubtypeOrCreateNewSubtype(Panel panel, String subtypeNewName, String newShortName, Subtype oldSubtype, LandPattern landPattern, JointTypeEnum jointType)
  {
    Assert.expect(panel != null);
    Assert.expect(subtypeNewName != null);

    Subtype subtype = null;
    if (panel.doesSubtypeExist(subtypeNewName) == false)
    {
      _subtypeConversionFileWriter.println(oldSubtype.getShortName() + "," + newShortName);
      subtype = Subtype.createNewSubtype(panel, subtypeNewName, newShortName, jointType);
      subtype.setLandPattern(landPattern);
      for(AlgorithmSettingEnum algorithmSettingEnum : oldSubtype.getTunedAlgorithmSettingEnums())
      {
        Subtype.copyThreshold(oldSubtype, subtype, oldSubtype.getAlgorithmSetting(algorithmSettingEnum));
      }
//      String oriMaskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + oldSubtype.getShortName() + ".png";
//      String newMaskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + newShortName + ".png";
      //Siew Yeng - XCR-2843 - masking rotation
      java.util.List<String> files = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                        FileName.getMaskImagePatternString(oldSubtype.getShortName()));
      if(files.isEmpty() == false)
      {
        String oriMaskImagePath = files.get(0);
        String rotationString = oriMaskImagePath.substring(oriMaskImagePath.lastIndexOf(FileName.getMaskImageRotationSeparator()) + 1, oriMaskImagePath.length() - FileName.getMaskImageExtension().length());
        try
        {       
          double rotation = Double.parseDouble(rotationString);
          String newMaskImagePath = FileName.getMaskImageWithRotationFullPath(subtype.getPanel().getProject().getName(), newShortName, rotation);

          FileUtil.copy(oriMaskImagePath, newMaskImagePath);
          _subtypeConversionFileWriter.println(oriMaskImagePath + "," + newMaskImagePath);
        }
        catch(NumberFormatException nfe)
        {
          NumberFormatException numFormatException = new NumberFormatException(rotationString);
          numFormatException.initCause(nfe);
          _subtypeConversionFileWriter.println("NumberFormatException: " + numFormatException.getMessage());
        }
        catch(IOException io)
        {
          _subtypeConversionFileWriter.println("IOException : " + io.getMessage());
        }
      }
    }
    else
    {
      subtype = panel.getSubtype(subtypeNewName);
    }

    return subtype;
  }
}
