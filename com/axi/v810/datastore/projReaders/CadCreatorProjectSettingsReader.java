package com.axi.v810.datastore.projReaders;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Creates nothing.
 * @author Kee Chin Seong
 */
class CadCreatorProjectSettingsReader
{
  private static CadCreatorProjectSettingsReader _instance;

  private ProjectReader _projectReader;
  private EnumStringLookup _enumStringLookup;
  private String _fileName;

  // targetCustomerName: name
  private Pattern _targetCustomerNamePattern = Pattern.compile("^\\s*targetCustomerName:(.*)$");
  // programmerName: name
  private Pattern _programmerNamePattern = Pattern.compile("^\\s*programmerName:(.*)$");

  /**
   * @author Kee Chin Seong
   */
  static synchronized CadCreatorProjectSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new CadCreatorProjectSettingsReader();

    return _instance;
  }

  /**
   * @author Kee Chin Seong
   */
  private CadCreatorProjectSettingsReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Kee Chin Seong
   */
  private void init()
  {
    // do nothing
  }
  
  /**
   * @author Kee Chin Seong
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();
    int version = FileName.getCadCreatorProjectSettingsLatestFileVersion();
    while (version > 0)
    {
      _fileName = FileName.getCadCreatorProjectSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(_fileName))
        break;
      --version;
    }

    try
    {
      Assert.expect(version > 0);
      _projectReader.openFile(_fileName);

      String projectMajorVersionStr = _projectReader.getNextLine();
      int projectMajorVersion = StringUtil.convertStringToPositiveInt(projectMajorVersionStr);
      String projectMinorVersionStr = _projectReader.getNextLine();
      int projectMinorVersion = StringUtil.convertStringToPositiveInt(projectMinorVersionStr);

      String programGenerationVersionStr = _projectReader.getNextLine();
      int programGenerationVersion = StringUtil.convertStringToPositiveInt(programGenerationVersionStr);
      project.setProgramGenerationVersion(programGenerationVersion);

      String projectCadXmlChecksumStr = _projectReader.getNextLine();
      long projectCadXmlChecksum = StringUtil.convertStringToLong(projectCadXmlChecksumStr);

      String projectSettingsXmlChecksumStr = _projectReader.getNextLine();
      long projectSettingsXmlChecksum = StringUtil.convertStringToLong(projectSettingsXmlChecksumStr);

      String lastModificationTimeInMillisStr = _projectReader.getNextLine();
      long lastModificationTimeInMillis = StringUtil.convertStringToLong(lastModificationTimeInMillisStr);

      String line = _projectReader.getNextLine();
      Matcher matcher = _targetCustomerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      String targetCustomerName = matcher.group(1);

      line = _projectReader.getNextLine();
      matcher = _programmerNamePattern.matcher(line);
      if (matcher.matches() == false)
        throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      String programmerName = matcher.group(1);

      String displayUnits = _projectReader.getNextLine();

      project.setInitialThresholdsSetName(FileName.getInitialThresholdsDefaultFileWithoutExtension());
      
      project.setInitialRecipeSettingSetName(FileName.getInitialRecipeDefaultSettingFileWithoutExtension());
      
      String initialThresholdSetName = _projectReader.getNextLine();
      project.saveInitialThresholdsSetName(initialThresholdSetName);

      String softwareVersionUsedForProjectCreation = _projectReader.getNextLine();
      double softwareVersion = StringUtil.convertStringToDouble(softwareVersionUsedForProjectCreation);

      SystemTypeEnum systemType = null;

      String systemTypeName = _projectReader.getNextLine();
      systemType = SystemTypeEnum.getSystemType(systemTypeName);
      project.setSystemType(systemType);

      String scanPathMethod = _projectReader.getNextLine();
      ScanPathMethodEnum scanPathMethodEnum = ScanPathMethodEnum.getScanPathMethodToEnumMapValue(Integer.valueOf(scanPathMethod));
      project.setScanPathMethod(scanPathMethodEnum);

      String generateByNewScanRoute = _projectReader.getNextLine();
      boolean isGenerateByNewScanRoute = Boolean.valueOf(generateByNewScanRoute);
      project.setGenerateByNewScanRoute(isGenerateByNewScanRoute);

      String scanRouteConfigFileName = _projectReader.getNextLine();
      project.setScanRouteConfigFileName(scanRouteConfigFileName);
    
      String enableVarDivN = _projectReader.getNextLine();
      boolean isVarDivNEnable = Boolean.valueOf(enableVarDivN);
      project.setVarDivN(isVarDivNEnable);
     
      String useLargeTomoAngle = _projectReader.getNextLine();
      boolean isUseLargeTomoAngle = Boolean.valueOf(useLargeTomoAngle);
      project.setUseLargeTomoAngle(isUseLargeTomoAngle);
     
      String generateLargeImageView = _projectReader.getNextLine();
      boolean isGenerateLargeImageView = Boolean.valueOf(generateLargeImageView);
      project.setEnableLargeImageView(isGenerateLargeImageView);

      String generateMultiAngleImage = _projectReader.getNextLine();
      boolean isGenerateMultiAngleImage = Boolean.valueOf(generateMultiAngleImage);
      project.setGenerateMultiAngleImage(isGenerateMultiAngleImage);

      String generateComponentImage = _projectReader.getNextLine();
      boolean isGenerateComponentImage = Boolean.valueOf(generateComponentImage);
      project.setGenerateComponentImage(isGenerateComponentImage);
     
      String panelExtraClearDelay = _projectReader.getNextLine();
      int panelExtraClearDelayInMiliSeconds = Integer.valueOf(panelExtraClearDelay);
      project.setPanelExtraClearDelayInMiliSeconds(panelExtraClearDelayInMiliSeconds);
      
      String enlargeReconstructionRegion = _projectReader.getNextLine();
      boolean isEnlargeReconstructionRegion = Boolean.valueOf(enlargeReconstructionRegion);
      project.setEnlargeReconstructionRegion(isEnlargeReconstructionRegion);
      
      if (softwareVersion > Double.parseDouble(Version.getVersionNumber()))
      {
        // We are not suppose to open project that is created with newer software.
        throw new CannotOpenProjectCreatedWithNewerApplication(projName, softwareVersion, Version.getVersionNumber());
      }

      project.setName(projName);
      project.setTestProgramVersion(projectMajorVersion);
      project.setMinorVersion(projectMinorVersion);
      project.setCadXmlChecksum(projectCadXmlChecksum);
      project.setSettingsXmlChecksum(projectSettingsXmlChecksum);
      project.setLastModificationTimeInMillis(lastModificationTimeInMillis);
      MathUtilEnum mathUtilEnum = _enumStringLookup.getMathUtilEnum(displayUnits);
      project.setDisplayUnits(mathUtilEnum);
      project.setSoftwareVersionOfProjectLastSave(String.valueOf(softwareVersion));
      project.setTargetCustomerName(targetCustomerName);
      project.setProgrammerName(programmerName);

      String notes = _projectReader.readMultipleLinesOfText("notesBegin", "notesEnd");
      project.setNotes(notes);

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (ValueOutOfRangeException vex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(vex);
      throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (IllegalArgumentException iex)
    {
      //XCR-2522, Assert when open an empty system type recipe
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_fileName);
      dex.initCause(iex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }
}
