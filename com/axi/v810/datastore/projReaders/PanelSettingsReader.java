package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * Creates PanelSettings, AlignmentGroup
 * @author Bill Darbie
 */
class PanelSettingsReader
{
  private static PanelSettingsReader _instance;

  private ProjectReader _projectReader;

  // single|left|right alignmentGroupName numPadsOrFiducials
  private Pattern _alignmentGroupNameNoQualityPattern = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)$");
  // single|left|right alignmentGroupName numPadsOrFiducials
  private Pattern _alignmentGroupNameNoQualityPattern2 = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)\\s+(\\w+)\\s+(-\\w+|\\w+)$");
  // single|left|right alignmentGroupName numPadsOrFiducials useZHeight zHeightInNanometers useCustomizeAlignmentSetting userGain signalCompensation
  private Pattern _alignmentGroupNameNoQualityPattern3 = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)\\s+(\\w+)\\s+(-\\w+|\\w+)\\s+(\\w+)\\s+(\\d+)\\s+(\\d+\\.?\\d?)$");
  // single|left|right alignmentGroupName numPadsOrFiducials useZHeight zHeightInNanometers useCustomizeAlignmentSetting userGain signalCompensation StageSpeedEnum DRO Magnification
  private Pattern _alignmentGroupNameNoQualityPattern4 = Pattern.compile("^(single|left|right)\\s+(\\w+)\\s+(\\d+)\\s+(\\w+)\\s+(-\\w+|\\w+)\\s+(\\w+)\\s+(\\d+)\\s+(\\d+\\.?\\d?)\\s+(\\d+\\.?\\d?)\\s+(\\d+)$");
  // boardName refDes padName
  private Pattern _padPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)\\s+([^\\s]+)$");
  // fiducialName
  private Pattern _fiducialPanelPattern = Pattern.compile("^([^\\s]+)$");
  // boardName fiducialName
  private Pattern _fiducialBoardPattern = Pattern.compile("^([^\\s]+)\\s+([^\\s]+)$");

  private EnumToUniqueIDLookup _enumToIdLookup;

  /**
   * @author Bill Darbie
   */
  static synchronized PanelSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new PanelSettingsReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelSettingsReader()
  {
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void read(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectReader == null)
      _projectReader = ProjectReader.getInstance();

    init();

    String projName = project.getName();

    String fileName = "";
    int version = FileName.getProjectPanelSettingsLatestFileVersion();
    while (version > 0)
    {
      fileName = FileName.getProjectPanelSettingsFullPath(projName, version);
      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }

    try
    {
      _projectReader.openFile(fileName);

      Panel panel = project.getPanel();
      PanelSettings panelSettings = new PanelSettings();
      panel.setPanelSettings(panelSettings);
      panelSettings.setPanel(panel);

      String userWarnedOfThicknessImportanceStr = "";
      if (version >= 2)
      {
        userWarnedOfThicknessImportanceStr =  _projectReader.getNextLine();
        boolean warned = StringUtil.convertStringToBoolean(userWarnedOfThicknessImportanceStr);
        if (warned)
          panelSettings.setUserWasWarnedOfThicknessImportance();
      }
      String degreesRotationStr = _projectReader.getNextLine();
      int degressRotation = StringUtil.convertStringToInt(degreesRotationStr);
      String leftToRightFlipStr = _projectReader.getNextLine();
      String topToBottomFlipStr = _projectReader.getNextLine();

      boolean leftToRightFlip = StringUtil.convertStringToBoolean(leftToRightFlipStr);
      boolean topToBottomFlip = StringUtil.convertStringToBoolean(topToBottomFlipStr);

      panelSettings.setDegreesRotationRelativeToCad(degressRotation);
      panelSettings.setLeftToRightFlip(leftToRightFlip);
      panelSettings.setTopToBottomFlip(topToBottomFlip);
      
      if (version >= 3)
      {
        String isPanelBasedAlignmentStr = _projectReader.getNextLine();
        boolean isPanelBasedAlignment = StringUtil.convertStringToBoolean(isPanelBasedAlignmentStr);
        panelSettings.setIsPanelBasedAlignment(isPanelBasedAlignment);
      }
      
      if (version >= 4)
      {
        String splitLocationInNanometerStr = _projectReader.getNextLine();
        int splitLocationInNanometer = StringUtil.convertStringToInt(splitLocationInNanometerStr);
        if(splitLocationInNanometer > 0)
        {
          panelSettings.setSplitLocationInNanometer(splitLocationInNanometer);
        }
        else
        {
          panelSettings.disableCustomSplit();
        }
      }
      
      if (version >= 5)
      {
        String isUseNewThicknessTableStr = _projectReader.getNextLine();
        boolean isUseNewThicknessTable = StringUtil.convertStringToBoolean(isUseNewThicknessTableStr);
        panelSettings.setUseNewThicknessTable(isUseNewThicknessTable);
      }
      
      if (version >= 6)
      {
        String isBypassAlignmentTranformWithinAcceptableLimitStr = _projectReader.getNextLine();
        boolean isBypassAlignmentTranformWithinAcceptableLimit = StringUtil.convertStringToBoolean(isBypassAlignmentTranformWithinAcceptableLimitStr);
        panelSettings.setBypassAlignmentTranformWithinAcceptableLimit(isBypassAlignmentTranformWithinAcceptableLimit);
      }
      
      // unfocused regions slices to be fixed list
      String numUnfocusedRegionSlicesToBeFixedStr = _projectReader.getNextLine();
      int numUnfocusedRegionSlicesToBeFixed = StringUtil.convertStringToInt(numUnfocusedRegionSlicesToBeFixedStr);
      for (int i = 0; i < numUnfocusedRegionSlicesToBeFixed; ++i)
      {
        String regionName = _projectReader.getNextLine();
        String numSlicesStr = _projectReader.getNextLine();
        int numSlices = StringUtil.convertStringToInt(numSlicesStr);
        for (int j = 0; j < numSlices; ++j)
        {
          String sliceIdStr = _projectReader.getNextLine();
          int sliceId = StringUtil.convertStringToInt(sliceIdStr);
          SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(sliceId);

          panelSettings.addUnfocusedRegionSliceToBeFixed(regionName, sliceNameEnum);
        }
      }

      // unfocused region slices to Z offset list
      String numUnfocusedRegionSlicesToZoffsetStr = _projectReader.getNextLine();
      int numUnfocusedSlicesToZoffset = StringUtil.convertStringToInt(numUnfocusedRegionSlicesToZoffsetStr);
      for (int i = 0; i < numUnfocusedSlicesToZoffset; ++i)
      {
        String regionName = _projectReader.getNextLine();
        String numSlicesStr = _projectReader.getNextLine();
        int numSlices = StringUtil.convertStringToInt(numSlicesStr);
        for (int j = 0; j < numSlices; ++j)
        {
          String sliceIdStr = _projectReader.getNextLine();
          int sliceId = StringUtil.convertStringToInt(sliceIdStr);
          SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(sliceId);
          String zOffsetStr = _projectReader.getNextLine();
          int zOffset = StringUtil.convertStringToInt(zOffsetStr);

          panelSettings.addUnfocusedRegionSlicesToZoffsetInNanos(regionName, sliceNameEnum, zOffset);
        }
      }

      // unfocused region names to retest list
      String numUnfocusedRegionNamesToRetestStr = _projectReader.getNextLine();
      int numUnfocusedRegionNamesToRetest = StringUtil.convertStringToInt(numUnfocusedRegionNamesToRetestStr);
      for (int i = 0; i < numUnfocusedRegionNamesToRetest; ++i)
      {
        String unfocusedRegionName = _projectReader.getNextLine();
        panelSettings.addUnfocusedRegionNameToRetest(unfocusedRegionName);
      }

      // no test region names
      String numNoTestRegionNamesStr = _projectReader.getNextLine();
      int numNoTestRegionNames = StringUtil.convertStringToInt(numNoTestRegionNamesStr);
      for (int i = 0; i < numNoTestRegionNames; ++i)
      {
        String regionName = _projectReader.getNextLine();
        String numPadTypesStr = _projectReader.getNextLine();
        int numPadTypes = StringUtil.convertStringToInt(numPadTypesStr);
        List<PadType> padTypes = new ArrayList<PadType>();
        for(int j = 0; j < numPadTypes; ++j)
        {
          String boardTypeName = _projectReader.getNextLine();
          String refDes = _projectReader.getNextLine();
          String padName = _projectReader.getNextLine();
          PadType padType = panel.getPadType(boardTypeName, refDes, padName);
          padTypes.add(padType);
        }
        panelSettings.addNoTestRegionNameForProjectReader(regionName, padTypes);
      }

      String line = null;
      boolean single = false;
      while (true)
      {
        // Due to XXL spliting panel way is different from standard
        // Assign empty alignment groups if different system type
        if(project.getSystemType().equals(com.axi.v810.hardware.XrayTester.getSystemType()) == false)
        {
          panelSettings.assignAlignmentGroups();
          _projectReader.goToLastLine();
          break;
        }
        
        if (line == null)
          line = _projectReader.getNextLine();

        Matcher matcher = _alignmentGroupNameNoQualityPattern.matcher(line);
        Matcher matcher2 = _alignmentGroupNameNoQualityPattern2.matcher(line);
        Matcher matcher3 = _alignmentGroupNameNoQualityPattern3.matcher(line);
        Matcher matcher4 = _alignmentGroupNameNoQualityPattern4.matcher(line);
        
        String singleOrLeftOrRight = null;
        String alignmentGroupName = null;
        String numPadsAndFiducialsStr = null;
        String matchQualityStr = null;
        String useZHeightStr = null;
        String zHeightInNanometerStr = null;
        String useCustomizeAlignmentSettingStr = null;
        String userGainStr = null;
        String signalCompensationStr = null;
        String stageSpeedStr = null;
        String droStr = null;
        
        if (matcher.matches())
        {
          singleOrLeftOrRight = matcher.group(1);
          alignmentGroupName = matcher.group(2);
          numPadsAndFiducialsStr = matcher.group(3);
        }
        else if(matcher2.matches())
        {
          singleOrLeftOrRight = matcher2.group(1);
          alignmentGroupName = matcher2.group(2);
          numPadsAndFiducialsStr = matcher2.group(3);
          useZHeightStr = matcher2.group(4);
          zHeightInNanometerStr = matcher2.group(5);
        }
        else if(matcher3.matches())
        {
          singleOrLeftOrRight = matcher3.group(1);
          alignmentGroupName = matcher3.group(2);
          numPadsAndFiducialsStr = matcher3.group(3);
          useZHeightStr = matcher3.group(4);
          zHeightInNanometerStr = matcher3.group(5);
          useCustomizeAlignmentSettingStr = matcher3.group(6);
          userGainStr = matcher3.group(7);
          signalCompensationStr = matcher3.group(8);
        }     
        else if (matcher4.matches())
        {
          singleOrLeftOrRight = matcher4.group(1);
          alignmentGroupName = matcher4.group(2);
          numPadsAndFiducialsStr = matcher4.group(3);
          useZHeightStr = matcher4.group(4);
          zHeightInNanometerStr = matcher4.group(5);
          useCustomizeAlignmentSettingStr = matcher4.group(6);
          userGainStr = matcher4.group(7);
          signalCompensationStr = matcher4.group(8);
          stageSpeedStr = matcher4.group(9);
          droStr = matcher4.group(10);
        }
        
        //Swee yee - panel settings shown left and right alignment group for short panel, it is corrupted and will erase all the alignment group
        if(panelSettings.isLongPanel() == false && singleOrLeftOrRight.equals("single") == false)
        {
          panelSettings.assignAlignmentGroups();
          _projectReader.goToLastLine();
          break;
        }

        if (matcher.matches() || matcher2.matches() || matcher3.matches() || matcher4.matches())
        {
          int numPadsAndFiducials = StringUtil.convertStringToInt(numPadsAndFiducialsStr);
          double matchQuality;

          boolean useZheight = false;
          int zHeightInNanometer = 0;
          boolean useCustomizeAlignmentSetting = false;
          
          if(useZHeightStr != null)
            useZheight = StringUtil.convertStringToBoolean(useZHeightStr);
          if(zHeightInNanometerStr != null)
            zHeightInNanometer = StringUtil.convertStringToInt(zHeightInNanometerStr);
          if(useCustomizeAlignmentSettingStr != null)
            useCustomizeAlignmentSetting = StringUtil.convertStringToBoolean(useCustomizeAlignmentSettingStr);
                    
          AlignmentGroup alignmentGroup = new AlignmentGroup(panelSettings);
          if (singleOrLeftOrRight.equals("single"))
          {
            panelSettings.addAlignmentGroup(alignmentGroup);
            // create left alignment groups that are empty
            if (single == false)
            {
              panelSettings.assignEmptyLeftAlignmentGroups();
              single = true;
            }
          }
          else if (singleOrLeftOrRight.equals("left"))
            panelSettings.addLeftAlignmentGroup(alignmentGroup);
          else if (singleOrLeftOrRight.equals("right"))
            panelSettings.addRightAlignmentGroup(alignmentGroup);
          else
            throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

          alignmentGroup.setName(alignmentGroupName);
          if (matchQualityStr != null)
          {
            matchQuality = StringUtil.convertStringToDouble(matchQualityStr);
            alignmentGroup.setMatchQuality(matchQuality);
          }
          alignmentGroup.setUseZHeight(useZheight);
          alignmentGroup.setZHeightInNanometer(zHeightInNanometer);
          alignmentGroup.setUseCustomizeAlignmentSetting(useCustomizeAlignmentSetting);
          
          if(userGainStr != null)
            alignmentGroup.setUserGainEnum(UserGainEnum.getUserGainToEnumMapValue(StringUtil.convertStringToInt(userGainStr)));         
          
          if (signalCompensationStr != null)
            alignmentGroup.setSignalCompensationEnum(SignalCompensationEnum.getSignalCompensationEnum(signalCompensationStr));
          
          if (stageSpeedStr != null)
            alignmentGroup.setStageSpeedEnum(StageSpeedEnum.getStageSpeedToEnumMapValue(StringUtil.convertStringToDouble(stageSpeedStr)));
          
          if (droStr != null)
            alignmentGroup.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(StringUtil.convertStringToInt(droStr)));
          line = null;

          for (int i = 0; i < numPadsAndFiducials; ++i)
          {
            line = _projectReader.getNextLine();
            matcher = _padPattern.matcher(line);
            if (matcher.matches())
            {
              String boardName = matcher.group(1);
              String refDes = matcher.group(2);
              String padName = matcher.group(3);

              // find the correct pad
              if (panel.hasPad(boardName, refDes, padName) == false)
                throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
              Pad pad = panel.getPad(boardName, refDes, padName);
              alignmentGroup.addPad(pad);
              line = null;
            }
            else
            {
              matcher = _fiducialBoardPattern.matcher(line);
              if (matcher.matches())
              {
                String boardName = matcher.group(1);
                String fiducialName = matcher.group(2);

                // find the correct board fiducial
                if (panel.hasFiducial(boardName, fiducialName) == false)
                  throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
                Fiducial fiducial = panel.getFiducial(boardName, fiducialName);
                alignmentGroup.addFiducial(fiducial);
                line = null;
              }
              else
              {
                matcher = _fiducialPanelPattern.matcher(line);
                if (matcher.matches() == false)
                  throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());

                String fiducialName = matcher.group(1);

                // find the correct panel fiducial
                if (panel.hasFiducial(fiducialName) == false)
                  throw new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
                Fiducial fiducial = panel.getFiducial(fiducialName);
                alignmentGroup.addFiducial(fiducial);
                line = null;
              }
            }
          }
        }

        if (_projectReader.hasNextLine())
          line = _projectReader.getNextLine();

        if (line == null)
          break;
      }

      _projectReader.closeFile();
    }
    catch (DatastoreException dex)
    {
      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName, _projectReader.getLineNumber());
      dex.initCause(bex);

      _projectReader.closeFileDuringExceptionHandling();
      throw dex;
    }
    finally
    {
      init();
    }
  }
}
