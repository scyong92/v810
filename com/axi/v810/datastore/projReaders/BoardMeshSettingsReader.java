package com.axi.v810.datastore.projReaders;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardMeshSettingsReader 
{
  private static BoardMeshSettingsReader _instance;
  private String _fileName;
  private ProjectReader _projectReader;
  
  private Pattern _panelCoordinatePattern = Pattern.compile("^(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)\\s+(\\d*)$");
  
  /**
   * @author Cheah Lee Herng 
   */
  static synchronized BoardMeshSettingsReader getInstance()
  {
    if (_instance == null)
      _instance = new BoardMeshSettingsReader();

    return _instance;
  }
  
  /**
    * @author Cheah Lee Herng
   */
   private void init()
   {
     // Do nothing
   }
   
   /**
    * @author Cheah Lee Herng 
    */
   void read(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
   {
     Assert.expect(project != null);
     Assert.expect(boardToBoardInstanceNumberMap != null);

     init();

     String projName = project.getName();
     _fileName = "";
     
     for (Board board : project.getPanel().getBoards())
     {
       if (_projectReader == null)
       _projectReader = ProjectReader.getInstance();
       
       int version = FileName.getProjectMeshSettingsLatestFileVersion();
       int boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);

       while (version > 0)
       {
         _fileName = FileName.getProjectMeshSettingsFullPath(projName, boardInstanceNumber, version);
         if (FileUtilAxi.exists(_fileName))
           break;
          --version;
       }

       // We need to handle backward-compatibility issue.
       if (version == 0)
       {
         BoardMeshSettings boardMeshSettings = new BoardMeshSettings(board);
         board.setBoardMeshSettings(boardMeshSettings);       
       }
       else
       {
         try
         {
           _projectReader.openFile(_fileName);
        
           BoardMeshSettings boardMeshSettings = new BoardMeshSettings(board);                        
           board.setBoardMeshSettings(boardMeshSettings);
           
           // Get PanelSurfaceMapSettings, if available
          OpticalRegion opticalRegion = null;
          BoardSurfaceMapSettings boardSurfaceMapSettings = null;
          if (board.hasBoardSurfaceMapSettings())
          {
            boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
            if (boardSurfaceMapSettings.hasOpticalRegion())
              opticalRegion = boardSurfaceMapSettings.getOpticalRegion();
          }
           
           if (_projectReader.hasNextLine() == false)
            return;

           String line = _projectReader.getNextLine();          
           while (true)
           {
             if (line == null)
               break;
             
             // figure out which type of line we have
             int lineType = 0;
             Matcher matcher = _panelCoordinatePattern.matcher(line);
             if (matcher.matches())
             {
               lineType = 1;
             }
             if (lineType == 0) 
             {
               throw new FileCorruptDatastoreException(_fileName, _projectReader.getLineNumber());
             }
             if (lineType == 1)
             {
               String point1PanelCoordinateXInNanometersString = matcher.group(1);
               String point1PanelCoordinateYInNanometersString = matcher.group(2);            
               String point1RegionWidthInNanometersString = matcher.group(3);
               String point1RegionHeightInNanometersString = matcher.group(4);            
               String point2PanelCoordinateXInNanometersString = matcher.group(5);
               String point2PanelCoordinateYInNanometersString = matcher.group(6);
               String point2RegionWidthInNanometersString = matcher.group(7);
               String point2RegionHeightInNanometersString = matcher.group(8);
               String point3PanelCoordinateXInNanometersString = matcher.group(9);
               String point3PanelCoordinateYInNanometersString = matcher.group(10);
               String point3RegionWidthInNanometersString = matcher.group(11);
               String point3RegionHeightInNanometersString = matcher.group(12);

               MeshTriangle meshTriangle = new MeshTriangle(Integer.parseInt(point1PanelCoordinateXInNanometersString),
                                                            Integer.parseInt(point1PanelCoordinateYInNanometersString),
                                                            Integer.parseInt(point1RegionWidthInNanometersString),
                                                            Integer.parseInt(point1RegionHeightInNanometersString),
                                                            Integer.parseInt(point2PanelCoordinateXInNanometersString),
                                                            Integer.parseInt(point2PanelCoordinateYInNanometersString),
                                                            Integer.parseInt(point2RegionWidthInNanometersString),
                                                            Integer.parseInt(point2RegionHeightInNanometersString),
                                                            Integer.parseInt(point3PanelCoordinateXInNanometersString),
                                                            Integer.parseInt(point3PanelCoordinateYInNanometersString),
                                                            Integer.parseInt(point3RegionWidthInNanometersString),
                                                            Integer.parseInt(point3RegionHeightInNanometersString));
               meshTriangle.addBoard(board);
               boardMeshSettings.addMeshTriangle(meshTriangle);
               
               // Add MeshTriangle
               if (opticalRegion != null)
                 opticalRegion.addMeshTriangle(meshTriangle);

               if (_projectReader.hasNextLine() == false)
               {
                 line = null;
                 break;
               }
               line = _projectReader.getNextLine();
            }
           }
         }
         catch (DatastoreException dex)
         {
           _projectReader.closeFileDuringExceptionHandling();
           _projectReader = null;
           throw dex;
         }
         finally
         {
           init();

           if (_projectReader != null)
           {
             _projectReader.closeFile();
             _projectReader = null;
           }
         }
        }
      }
    }          
}
