package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class CannotCreateFileDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param fileName is the name of the file that is corrupt
   * @author Bill Darbie
   */
  public CannotCreateFileDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_CREATE_FILE_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
