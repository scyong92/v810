/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.datastore.algorithmLearning;



import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import static org.jfree.util.ObjectUtilities.equal;
/**
 *
 * @author sheng-chuan.yong
 */
public class Test_AlgorithmExpectedImageTemplateLearningReaderWriter extends UnitTest
{

  /**
   * @author sheng chuan
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmExpectedImageTemplateLearningReaderWriter());
  }
  
  /**
   * sheng chuan
   * @param is
   * @param os 
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Config.getInstance().loadIfNecessary();
      String projName = "families_all_rlv";
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      // test that things work when no file exists already
      String file = FileName.getAlgorithmExpectedImageLearningFullPath(projName);
      if (FileUtilAxi.exists(file))
        FileUtilAxi.delete(file);

      Project project = Project.importProjectFromNdfs(projName, warnings);
      AlgorithmExpectedImageTemplateLearningReaderWriter expectedImageTemplateLearningRW = new AlgorithmExpectedImageTemplateLearningReaderWriter(project, projName);

      expectedImageTemplateLearningRW.open();
      expectedImageTemplateLearningRW.close();
      
      int count = 0;
      float fcount = 0.0f;
      int imageWidth1 = 40;
      int imageHeight1 = 40;
      ExpectedImageTemplateLearning record1 = new ExpectedImageTemplateLearning();
      Image image = new Image(imageWidth1, imageHeight1);
      SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      Pad pad = new Pad();
      String sampleImageName = "sample.png";
      record1.setLearnedExpectedImage(image);
      record1.setSliceNameEnum(sliceNameEnum);
      record1.setImageName(sampleImageName);
      record1.setPad(pad);
      record1.setNumberOfExpectedImagesLearned(++count);
      
      expectedImageTemplateLearningRW.open();
      expectedImageTemplateLearningRW.writeExpectedImageTemplateLearning(record1);
      ExpectedImageTemplateLearning out = expectedImageTemplateLearningRW.readExpectedImageLearning(pad, sliceNameEnum);
      Expect.expect(equal(record1, out));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
  
}
