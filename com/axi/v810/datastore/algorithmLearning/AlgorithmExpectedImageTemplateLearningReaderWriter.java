/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.datastore.algorithmLearning;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;
import java.util.logging.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class AlgorithmExpectedImageTemplateLearningReaderWriter
{
  private Project _project;
  private String _projName;
  private FileChannel _fileChannel;
  private int _bufSize = 8192;
  private ByteBuffer _byteBuffer = ByteBuffer.allocate(_bufSize);
  private boolean _reading;
  private boolean _firstReadOrWrite = true;
  private String _tempProjOpenFileName;
  private String _tempOpenFileName;
  private String _learningFileName;
  private String _learningFilePathName;
  private String _currFileName;
  private Map<String, Set<Integer>> _padNameToSliceEnumMap = new HashMap<String, Set<Integer>>();
  private Map<String, String> _padAndSliceNameToImageFileMap = new HashMap<String, String>();
  
  private Map<String, Pair<Integer, Long>> _padAndSliceNameToIdAndFilePositionMap = new HashMap<String, Pair<Integer, Long>>();
  private Map<String, Integer> _padAndSliceNameToNumBytesInRecordMap = new HashMap<String, Integer>();
  private ArrayList<String> _savedImageFileList = new ArrayList<String>();
  private long _tailStartOffset;
  private long _fileSizeAtOpen;
  private EnumToUniqueIDLookup _enumToIdLookup;
  private int _recordId;
  private int _largestRecordId;
  private boolean _fileModified = false;

  private static final int _recordVersionNumber = 2;

  private Set<Integer> _unusedRecordIdSet = new TreeSet<Integer>();
  private Map<Integer, Set<Long>> _unusedRecordSizeToOffsetsMap = new TreeMap<Integer, Set<Long>>();

  private static int _numBytesForInteger = Integer.SIZE / 8;
  private static int _numBytesForLong = Long.SIZE / 8;
  private static int _numBytesForFloat = Float.SIZE / 8;
  private static int _numBytesForDouble = Double.SIZE / 8;
  private static int _numBytesForByte = Byte.SIZE / 8;
  // the top of the file has a version number and an offset to the header (which is really at the bottom of the file)
  private static int _numBytesForPreHeader = _numBytesForInteger + _numBytesForLong;

  private TimerUtil _timerUtil = new TimerUtil();
  /**
   * @author Bill Darbie
   */
  public AlgorithmExpectedImageTemplateLearningReaderWriter(Project project)
  {
    Assert.expect(project != null);
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
    _project = project;
    _projName = project.getName();
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmExpectedImageTemplateLearningReaderWriter(Project project, String projName)
  {
    Assert.expect(project != null);
    Assert.expect(projName != null);
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
    _project = project;
    _projName = projName;
  }

  /**
   * @author Bill Darbie
   */
  public void clearProjectReference()
  {
    _project = null;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isOpen()
  {
    if (_fileChannel == null)
      return false;

    return true;
  }
  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _reading = true;
    _firstReadOrWrite = true;
    _fileSizeAtOpen = 0;
    _padAndSliceNameToIdAndFilePositionMap.clear();
    _padAndSliceNameToNumBytesInRecordMap.clear();
    _padAndSliceNameToImageFileMap.clear();
    _savedImageFileList.clear();
    _unusedRecordIdSet.clear();
    _padNameToSliceEnumMap.clear();
    _unusedRecordSizeToOffsetsMap.clear();
    _tailStartOffset = 0;
    _fileModified = false;
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  public void open() throws DatastoreException
  {
    _timerUtil.reset();
    _timerUtil.start();

    init();

    // make the directory in case it does not exist
    String dir = Directory.getAlgorithmTemplateLearningDir(_projName);
    FileUtilAxi.mkdirs(dir);

    // get file namess
    _tempProjOpenFileName = FileName.getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(_projName);
    _tempOpenFileName = FileName.getAlgorithmExpectedImageTemplateLearningTempOpenFullPath(_projName);
    _learningFileName = FileName.getAlgorithmExpectedImageTemplateLearningFullPath(_projName);
    _learningFilePathName = FileName.getAlgorithmExpectedImageTemplateLearningAbsoultePath(_projName);

    // if any old temp file is still around delete it
    if (FileUtilAxi.exists(_tempOpenFileName))
      FileUtilAxi.delete(_tempOpenFileName);

    // copy the current file name to the temporary file
    // this way if the code dies before the close, the original file will still be intact
    // and useable
    if (FileUtilAxi.exists(_tempProjOpenFileName))
      FileUtilAxi.copy(_tempProjOpenFileName, _tempOpenFileName);

    // open the file
    openFile(_tempOpenFileName);

    _timerUtil.stop();
//    System.out.println("glb time to open Expected Image learned files: " + _timerUtil.getElapsedTimeInMillis());
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void openFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    _currFileName = fileName;

    try
    {
      // work on the temporary file from this point in.  This is done in case a crash happens
      // while the file is open.  A crash could cause the file to be corrupt.  This way the
      // corrupt file will be the temporary file and will cause no harm
      RandomAccessFile raf = new RandomAccessFile(_currFileName, "rw");
      _fileChannel = raf.getChannel();
      _fileSizeAtOpen = _fileChannel.size();
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_currFileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    if (_fileSizeAtOpen > 0)
    {
      readHeaderAndTail();
    }
    else
    {
      // no record ids to keep track of, reset largestRecordId so ti doesn't get too big
      _largestRecordId = 0;
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void openForCompact(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    // open the file
    _tempProjOpenFileName = fileName;
    _tempOpenFileName = null;

    openFile(_tempProjOpenFileName);
  }

  
  /**
   * @author Cheah Lee Herng
   */
  private void clearMaps()
  {
    if (_padNameToSliceEnumMap != null)
      _padNameToSliceEnumMap.clear();

    if (_padAndSliceNameToIdAndFilePositionMap != null)
      _padAndSliceNameToIdAndFilePositionMap.clear();
    
    if (_padAndSliceNameToNumBytesInRecordMap != null)
      _padAndSliceNameToNumBytesInRecordMap.clear();
      
    if (_padAndSliceNameToImageFileMap != null)
      _padAndSliceNameToImageFileMap.clear();
      
    if (_savedImageFileList != null)
      _savedImageFileList.clear();
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  public void close() throws DatastoreException
  {
    _timerUtil.reset();
    _timerUtil.start();

    boolean needToDeleteDir = _padAndSliceNameToImageFileMap.isEmpty();
    if (_currFileName == null)
      return;
    
    closeWithRename();

    if(needToDeleteDir)
    {
      String dir = Directory.getAlgorithmTemplateLearningDir(_projName);
      if (FileUtilAxi.exists(dir))
        FileUtilAxi.delete(dir);
    }
    
    _currFileName = null;
    _tempProjOpenFileName = null;
    _tempOpenFileName = null;

    
    // CR1018 Memory Leak fix by LeeHerng - Clear hash map
    clearMaps();

    _timerUtil.stop();
//    System.out.println("glb time to close Expected Image learned files: " + _timerUtil.getElapsedTimeInMillis());
  }
  
  private void removeUnusedImageTemplate() throws DatastoreException 
  {
    Assert.expect(_savedImageFileList.isEmpty() == false);
    
    for (String unusedImage : _savedImageFileList)
    {
      String imageDirectory = Directory.getAlgorithmTemplateLearningDir(_projName) + File.separator + unusedImage;

      if (FileUtilAxi.exists(imageDirectory))
        FileUtilAxi.delete(imageDirectory);
    }
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void close(boolean rename) throws DatastoreException
  {
    if (_fileChannel == null)
      return;

    if (_fileModified)
      writeHeaderAndTail();
    try
    {
      if (_fileModified)
      {
        if (_firstReadOrWrite || (_reading == false))
        {
          flushWithoutReload();
          _fileChannel.truncate(_fileChannel.position());
        }
      }

      TimerUtil timerUtil = new TimerUtil();
      timerUtil.start();

      // files previous to release 1.13 may have unused space that was not recorded
      boolean needToCompact = hasUnusedSpace();

      boolean renameFile = false;
      if (_fileModified || needToCompact)
      {
        if (needToCompact)
        {
          createCompactFile(_tempProjOpenFileName);
        }
        else
        {
          if (rename)
            renameFile = true;
        }
      }

      if (_fileChannel != null)
        _fileChannel.close();
      _fileChannel = null;

      // CR1018 Memory Leak fix by LeeHerng - Clear ByteBuffer
      if (_byteBuffer != null)
          _byteBuffer.clear();

      if (renameFile)
        FileUtilAxi.rename(_tempOpenFileName, _tempProjOpenFileName);

      if (_fileModified == false && needToCompact)
      {
        // A file with unused space was opened for reading but nothing has triggered the need to save the
        // compacted data. Do it now as a favor to the user.
        Assert.expect(FileUtilAxi.exists(_tempProjOpenFileName));
        FileUtilAxi.copy(_tempProjOpenFileName, _learningFileName);
      }

      timerUtil.stop();
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      if ((_tempOpenFileName != null) && (FileUtilAxi.exists(_tempOpenFileName)))
        FileUtilAxi.delete(_tempOpenFileName);

      init();
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void closeOnException()
  {
    try
    {
      if (_fileChannel != null)
        _fileChannel.close();
      _fileChannel = null;

      try
      {
        if (FileUtilAxi.exists(_tempOpenFileName))
          FileUtilAxi.delete(_tempOpenFileName);
      }
      catch (DatastoreException de)
      {
        de.printStackTrace();
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void closeWithRename() throws DatastoreException
  {
    close(true);
  }

  /**
   * @author Bill Darbie
   */
  private void closeWithoutRename() throws DatastoreException
  {
    close(false);
  }
  
  /**
   * Write out just the expected images without unused records
   * @author George Booth
   */
  private void createCompactFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    if (FileUtilAxi.exists(fileName))
      FileUtilAxi.delete(fileName);

    Assert.expect(_project != null);
    AlgorithmExpectedImageTemplateLearningReaderWriter compactReaderWriter = new AlgorithmExpectedImageTemplateLearningReaderWriter(_project);
    try
    {
      compactReaderWriter.openForCompact(fileName);

      // iterate over the expected images and write things out to the new temporary file
      for (Map.Entry<String, Pair<Integer, Long>> entry : _padAndSliceNameToIdAndFilePositionMap.entrySet())
      {
        String padAndSliceName = entry.getKey();
        if (doesPadExist(padAndSliceName))
        {
          ExpectedImageTemplateLearning expectedImageLearning = readExpectedImageLearning(padAndSliceName);
          compactReaderWriter.writeExpectedImageTemplateLearning(expectedImageLearning);
          expectedImageLearning.getLearnedExpectedImage().decrementReferenceCount();
        }
      }
    }
    finally
    {
      compactReaderWriter.closeWithoutRename();
    }
  }
  
  /**
   * Deletes the data file
   * @author George Booth
   */
  public void deleteExpectedImageLearning() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(_projName);
    if (FileUtilAxi.exists(fileName))
      FileUtilAxi.delete(fileName);
  }

  /**
   * Regression test only
   * @author George Booth
   */
  public void deleteExpectedImageLearning(Pad pad, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
    deleteExpectedImageLearning(padAndSliceName,sliceNameEnum);
  }
  
  /**
   * If neeeded, delete new or previous expected images
   * @author George Booth
   */
  public void deleteExpectedImageLearning(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    // new "boardType refDesig padName"
    String padFullName = getFullPadName(pad);
    if (_padNameToSliceEnumMap.containsKey(padFullName))
    {
      Set<Integer> sliceEnumSet = _padNameToSliceEnumMap.get(padFullName);
      for (int slice : sliceEnumSet)
      {
        SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(slice);
        String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
        deleteExpectedImageLearning(padAndSliceName,sliceNameEnum);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void deleteExpectedImageLearning(String padAndSliceName,SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(padAndSliceName != null);

    Pair<Integer, Long> pair = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    Assert.expect(pair != null);

    int id = pair.getFirst();
    long offset = pair.getSecond();
    int numBytesOnDiskForRecord = _padAndSliceNameToNumBytesInRecordMap.get(padAndSliceName);

    boolean added = _unusedRecordIdSet.add(id);
    Assert.expect(added);

    Set<Long> offsetSet = _unusedRecordSizeToOffsetsMap.get(numBytesOnDiskForRecord);
    if (offsetSet == null)
    {
      offsetSet = new TreeSet<Long>();
      _unusedRecordSizeToOffsetsMap.put(numBytesOnDiskForRecord, offsetSet);
    }
    added = offsetSet.add(offset);
    Assert.expect(added);
    _padAndSliceNameToIdAndFilePositionMap.remove(padAndSliceName);
    _padAndSliceNameToNumBytesInRecordMap.remove(padAndSliceName);
    String fullPadName = getFullPadName(padAndSliceName);
    _padNameToSliceEnumMap.remove(fullPadName);
    String imageToRemove = _padAndSliceNameToImageFileMap.get(padAndSliceName);
    String imageDirectory = Directory.getAlgorithmTemplateLearningDir(_projName)+File.separator+imageToRemove;
    
    _padAndSliceNameToImageFileMap.remove(padAndSliceName);
    if (FileUtilAxi.exists(imageDirectory))
      FileUtilAxi.delete(imageDirectory);

    _fileModified = true;
  }
  
  /**
   * Determines if expect images exist for the pad and slice name. Check both styles of learning:
   * (release 1.13) saved as "boardType refDesig padName"
   * (original) saved as "boardName refDesig padname"
   * @author George Booth
   */
  public boolean doesExpectedImageLearningExist(Pad pad, SliceNameEnum sliceNameEnum)
  {
    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
    boolean learningExists = _padAndSliceNameToIdAndFilePositionMap.containsKey(padAndSliceName);

    return learningExists;
  }
  
  /**
   * Determines if expect images exist for the pad. Check both styles of learning:
   * (release 1.13) saved as "boardType refDesig padName"
   * (original) saved as "boardName refDesig padname"
   * @author George Booth
   */
  public boolean doesExpectedImageLearningExist(Pad pad)
  {
    String padFullName = getFullPadName(pad);
    boolean learningExists = _padNameToSliceEnumMap.containsKey(padFullName);

    return learningExists;
  }
  
  /**
   * Determines if expect images exist for the pad. Check both styles of learning:
   * (release 1.13) saved as "boardType refDesig padName"
   * (original) saved as "boardName refDesig padname"
   * @author George Booth
   */
  public boolean doesExpectedImageLearningExist(Pad pad, Board board, SliceNameEnum sliceNameEnum)
  {
    String padFullName = getPadAndSliceName(board,pad,sliceNameEnum);
    boolean learningExists = _padAndSliceNameToIdAndFilePositionMap.containsKey(padFullName);

    return learningExists;
  }
  
  /**
   * @author George Booth
   */
  private boolean doesPadExist(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);

    String boardType = padAndSliceName.substring(0, firstSpace);
    String refDes = padAndSliceName.substring(firstSpace + 1, secondSpace);
    String padName = padAndSliceName.substring(secondSpace + 1, thirdSpace);
    String boardName = getFirstBoardNameOfBoardType(boardType);

    Assert.expect(_project != null);
    return _project.getPanel().hasPad(boardName, refDes, padName);
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private synchronized ExpectedImageTemplateLearning readExpectedImageLearning(String padAndSliceName) throws DatastoreException
  {
    Assert.expect(padAndSliceName != null);

    Pair<Integer, Long> pair = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    if (pair == null)
      throw new FileCorruptDatastoreException(_currFileName);

    int id = pair.getFirst();
    long offset = pair.getSecond();

    setReadPosition(offset);

    int recordVersion = readInt();
    if (recordVersion != _recordVersionNumber)
      throw new FileCorruptDatastoreException(_currFileName);

    int uniqueId = readInt();
    if (uniqueId != id)
      throw new FileCorruptDatastoreException(_currFileName);

    ExpectedImageTemplateLearning expectedImageLearning = new ExpectedImageTemplateLearning();

    expectedImageLearning.setNumberOfExpectedImagesLearned(readInt());
    
//    String imageName = readString();
    Image image = null;
    String selectedImageName = _padAndSliceNameToImageFileMap.get(padAndSliceName);
    String imageAbsoluteLocation = _learningFilePathName + File.separator +selectedImageName;
    try
    {
      if(UnitTest.unitTesting() == false)
        image = ImageIoUtil.loadTiffImage(imageAbsoluteLocation);
      else
      {
        image = new Image(40, 40);
      }
    }
    catch (FileDoesNotExistException ex)
    {
      throw new FileCorruptDatastoreException(_currFileName);
    }
    catch (CouldNotReadFileException ex)
    {
      throw new FileCorruptDatastoreException(_currFileName);
    }
    Pad pad = getPad(padAndSliceName);
    SliceNameEnum sliceNameEnum = getSliceNameEnum(padAndSliceName);
    expectedImageLearning.setSliceNameEnum(sliceNameEnum);
    expectedImageLearning.setImageName(selectedImageName);
    expectedImageLearning.setLearnedExpectedImage(image);
    expectedImageLearning.setPad(pad);
    
    return expectedImageLearning;
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  public synchronized ExpectedImageTemplateLearning readExpectedImageLearning(Pad pad, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(pad != null);

    String padAndSliceName = getPadAndSliceName(pad,sliceNameEnum);
    return readExpectedImageLearning(padAndSliceName);
  }
  
  public synchronized ExpectedImageTemplateLearning readExpectedImageLearning(Pad pad,Board board, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(pad != null);

    String padAndSliceName = getPadAndSliceName(board,pad,sliceNameEnum);
    return readExpectedImageLearning(padAndSliceName);
  }
  
  /**
   * @author Bill Darbie
   */
  private void flushAndLoadForWrite() throws DatastoreException
  {
    flushWithoutReload();
    loadByteBufferForWrite();
  }


  /**
   * @author Bill Darbie
   */
  private void flushWithoutReload() throws DatastoreException
  {
    try
    {
      if (_fileChannel != null)
      {
        // set the limit to the current postion and set the position to 0
        _byteBuffer.flip();
        // write out the buffer
        int numBytesWritten = _fileChannel.write(_byteBuffer);
        // set the position back to 0 and the limit to the capacity
        _byteBuffer.clear();
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }
  
  /**
   * @author George Booth
   */
  private String getFirstBoardNameOfBoardType(String boardType)
  {
    Assert.expect(boardType != null && boardType.equals("") == false);

    // see if boardType is a valid boardType; if not, it better be a board name (orig version)
    Assert.expect(_project != null);
    if (_project.getPanel().hasBoardType(boardType) == false)
    {
      // boardType should be orig style board name
      String boardName = boardType;
      boardType = null;
      if (_project.getPanel().hasBoard(boardName))
      {
        boardType = _project.getPanel().getBoard(boardName).getBoardType().getName();
      }
      else
      {
        Assert.expect(boardType != null, "board name \"" + boardName + "\"not found in project");
      }
    }
    // get first board name for this type
    String boardName = null;
    List<Board> boardList = _project.getPanel().getBoards();
    for (Board board : boardList)
    {
      String bt = board.getBoardType().getName();
      if (board.getBoardType().getName().equals(boardType))
      {
        boardName = board.getName();
        break;
      }
    }
    Assert.expect(boardName != null, "board type \"" + boardType + "\"not found in list of boards");
    return boardName;
  }
  
  /**
   * @author Bill Darbie
   */
  private String getFullPadName(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);
    String fullPadName = padAndSliceName.substring(0, thirdSpace);
    return fullPadName.intern();
  }
  
  /**
   * @author Bill Darbie
   */
  private void readHeaderAndTail() throws DatastoreException
  {
    // read the header
    setReadPosition(-_numBytesForPreHeader);
    int headerVersion = readInt();
    if (headerVersion != _recordVersionNumber)
      throw new FileCorruptDatastoreException(_currFileName);

    long headerOffset = readLong();
    _tailStartOffset = headerOffset;

    // read the tail
    setReadPosition(headerOffset);

    int recordVersionNumber = readInt();
    if (recordVersionNumber != _recordVersionNumber)
      throw new FileCorruptDatastoreException(_currFileName);
    
    int numJoints = readInt();
    for (int i = 0; i < numJoints; ++i)
    {
      String padAndSliceName = readString();
      int id = readInt();
      int recordSizeInBytes = readInt();

      String imageName = readString();
      String fullPadName = getFullPadName(padAndSliceName);
      int numSlices = readInt();
      for (int j = 0; j < numSlices; ++j)
      {
        Set<Integer> sliceSet = _padNameToSliceEnumMap.get(fullPadName);
        if (sliceSet == null)
        {
          sliceSet = new HashSet<Integer>();
          _padNameToSliceEnumMap.put(fullPadName, sliceSet);
        }
        sliceSet.add(readInt());
      }
      long offset = readLong();
      Object prev = _padAndSliceNameToIdAndFilePositionMap.put(padAndSliceName, new Pair<Integer, Long>(id, offset));
      Assert.expect(prev == null);
      prev = _padAndSliceNameToNumBytesInRecordMap.put(padAndSliceName, recordSizeInBytes);
      Assert.expect(prev == null);
      prev = _padAndSliceNameToImageFileMap.put(padAndSliceName, imageName);
      Assert.expect(prev == null);  
      
      //add all image that are currently learnt by component into a list
      if(_savedImageFileList.contains(imageName) == false)
        _savedImageFileList.add(imageName);
    }

    // numUnusedIds
    int numUnusedRecordIds = readInt();
    for (int i = 0; i < numUnusedRecordIds; ++i)
    {
      int unusedId = readInt();
      boolean added = _unusedRecordIdSet.add(unusedId);
      Assert.expect(added);
    }

    _largestRecordId = readInt();
    _recordId = _largestRecordId;
  }
  
  /**
   * @author Bill Darbie
   */
  private void writeHeaderAndTail() throws DatastoreException
  {
   // write the header
    setWritePosition(-_numBytesForPreHeader);
    // header version number
    writeInt(_recordVersionNumber);
    writeLong(_tailStartOffset);

    // write the tail
    setWritePosition(_tailStartOffset);

    // tail record version number
    writeInt(_recordVersionNumber);

    // first check to see if a record for this pad and slice already exists and that it is the
    // correct size
    _fileModified = true;
    // numJoints
    int numPads = _padAndSliceNameToIdAndFilePositionMap.size();
    writeInt(numPads);
    for (Map.Entry<String, Pair<Integer, Long>> entry : _padAndSliceNameToIdAndFilePositionMap.entrySet())
    {
      String padAndSliceName = entry.getKey();
      Pair<Integer, Long> pair = entry.getValue();
      int id = pair.getFirst();
      long offset = pair.getSecond();

      String fullPadName = getFullPadName(padAndSliceName);
      // pad name
      writeString(padAndSliceName);
      // id
      writeInt(id);
      // record size in bytes
      int recordSizeInBytes = _padAndSliceNameToNumBytesInRecordMap.get(padAndSliceName);
      writeInt(recordSizeInBytes);

      
      String imageName = _padAndSliceNameToImageFileMap.get(padAndSliceName);
      
      //remove those image that are still refer by some component
      if(_savedImageFileList.contains(imageName))
        _savedImageFileList.remove(imageName);
      
      //writ the reference image name
      writeString(imageName);
      
      Set<Integer> sliceEnumSet = _padNameToSliceEnumMap.get(fullPadName);
      int numSlices = sliceEnumSet.size();
      // numSlices (int)
      writeInt(numSlices);
      for (int slice : sliceEnumSet)
      {
        // sliceName (int)
        writeInt(slice);
      }

      // offset
      writeLong(offset);
    }
    // numUnusedRecordIds
    writeInt(_unusedRecordIdSet.size());
    for (int unusedId : _unusedRecordIdSet)
    {
      // unusedId
      writeInt(unusedId);
    }

    //those image data that remain in list will be redundant, since all images that still refer by someone is removed
    if(_savedImageFileList.isEmpty() == false)
      removeUnusedImageTemplate();
    
    // currRecordId (int)
    writeInt(_largestRecordId);
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  public void writeExpectedImageTemplateLearning(ExpectedImageTemplateLearning imageTemplate) throws DatastoreException
  {
    Assert.expect(imageTemplate != null);

    SliceNameEnum sliceNameEnum = imageTemplate.getSliceNameEnum();
    String imageName =  imageTemplate.getImageName();
    Pad pad = imageTemplate.getPad();
    String padFullName = getFullPadName(pad);
    String padAndSliceName = getPadAndSliceName(pad,sliceNameEnum);
//    // first check to see if a record for this pad and slice already exists and that it is the
//    // correct size
    _fileModified = true;
    boolean append = true;
    boolean newEntry = true;
    long offset = 0;
    _fileModified = true;

    int numBytes =0;
    Integer numBytesOnDiskForRecord = _padAndSliceNameToNumBytesInRecordMap.get(padAndSliceName);
    
    
    int calculatedRecordSize = getRecordSize();
    int id=0;
    if (numBytesOnDiskForRecord != null)
    {

      numBytes = numBytesOnDiskForRecord;
      Pair<Integer, Long> pair = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
      id = pair.getFirst();
      offset = pair.getSecond();
      if (numBytes == calculatedRecordSize)
      {
        // we already have a record on disk and it is the proper size, so write over it
        setWritePosition(offset);
        append = true;
        newEntry = false;
      }
      else
      {
        // we have a record on disk but it is not the correct size so remove the old record and write a new one somewhere else
        deleteExpectedImageLearning(padAndSliceName,sliceNameEnum);
        append = true;
        newEntry = true;
      }
    }

    if (append)
    {
      // check to see if an empty space of the correct size already exists
      Set<Long> offsetSet = _unusedRecordSizeToOffsetsMap.get(calculatedRecordSize);
      if (offsetSet != null)
      {
        // we have a space open, fill it instead of appending to the end of the file
        long emptyOffset = offsetSet.iterator().next();
        offsetSet.remove(emptyOffset);
        if (offsetSet.isEmpty())
          _unusedRecordSizeToOffsetsMap.remove(calculatedRecordSize);
        setWritePosition(emptyOffset);
        append = false;
        newEntry = true;
      }
    }

    if (append)
    {
      offset = _tailStartOffset;
      setWritePosition(offset);
    }

    int recordSize = 0;
    // record version #
    recordSize += writeInt(_recordVersionNumber);

    getNextAvailableRecordId();
    // id
    if (newEntry)
      recordSize += writeInt(_recordId);
    else
      recordSize += writeInt(id);
    
    // slice enum (int)
    recordSize += writeInt(imageTemplate.getSliceNameEnum().getId());

    Assert.expect(recordSize == getRecordSize());
    
    try
    {
      //save the image
      if(UnitTest.unitTesting() == false)
        ImageIoUtil.saveTiffImage(imageTemplate.getLearnedExpectedImage().getBufferedImage(), _learningFilePathName + File.separator + imageName);
    }
    catch (CouldNotCreateFileException ex)
    {
      throw new CannotWriteDatastoreException(_learningFilePathName);
    }
    
    if (newEntry)
    {
      _tailStartOffset += recordSize;
      Object prev = _padAndSliceNameToNumBytesInRecordMap.put(padAndSliceName, recordSize);
      Assert.expect(prev == null);
      prev = _padAndSliceNameToIdAndFilePositionMap.put(padAndSliceName, new Pair<Integer, Long>(_recordId, offset));
      Assert.expect(prev == null);
      _padAndSliceNameToImageFileMap.put(padAndSliceName, imageName);
      Assert.expect(prev == null);

      Set<Integer> sliceSet = _padNameToSliceEnumMap.get(padFullName);
      if (sliceSet == null)
      {
        sliceSet = new HashSet<Integer>();
        _padNameToSliceEnumMap.put(padFullName, sliceSet);
      }
      sliceSet.add(_enumToIdLookup.getSliceNameUniqueID(sliceNameEnum));
    }
    
    _padAndSliceNameToImageFileMap.put(padAndSliceName, imageName);
  }
  
  /**
   * @author Bill Darbie
   */
  private void getNextAvailableRecordId()
  {
   if (_unusedRecordIdSet.isEmpty())
    {
      ++_largestRecordId;
      _recordId = _largestRecordId;
    }
    else
    {
      _recordId = _unusedRecordIdSet.iterator().next();
      _unusedRecordIdSet.remove(_recordId);
    }
  }
  
  /**
   * @author George Booth
   */
  private Pad getPad(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    // Original version saved all joint data as "board refDesig, padName"
    // New version combines images from each board type and saved data as "boardType, refDesig, padName"

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);

    String boardType = padAndSliceName.substring(0, firstSpace);
    String refDes = padAndSliceName.substring(firstSpace + 1, secondSpace);
    String padName = padAndSliceName.substring(secondSpace + 1, thirdSpace);
    String boardName = getFirstBoardNameOfBoardType(boardType);

    Assert.expect(boardName.equals("") == false);
    return _project.getPanel().getPad(boardName, refDes, padName);
  }
  
  
  /**
   * Release 1.13
   * @author George Booth
   */
  private String getPadAndSliceName(Pad pad, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = getFullPadName(pad) + " " + _enumToIdLookup.getSliceNameUniqueID(sliceNameEnum);
    return padAndSliceName.intern();
  }
  
  /*
   * @author Kee Chin Seong : Speed up short learning board to board 10x speed
   */  
  private String getPadAndSliceName(Board board, Pad pad, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = board.getName() + " " +
                             pad.getComponent().getReferenceDesignator() + " " +
                             pad.getName() + " " + _enumToIdLookup.getSliceNameUniqueID(sliceNameEnum);
    return padAndSliceName.intern();
  }
  
  /**
   * @author Bill Darbie
   */
  private String getFullPadName(Pad pad)
  {
    Assert.expect(pad != null);

    String fullPadName = pad.getComponent().getBoard().getName() + " " +
                         pad.getComponent().getReferenceDesignator() + " " +
                         pad.getName();
    return fullPadName.intern();
  }
  
  /**
   * @author Bill Darbie
   */
  private long getReadPosition() throws DatastoreException
  {
    long position = 0;
    try
    {
      position = _fileChannel.position() - (_byteBuffer.limit() - _byteBuffer.position());
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return position;
  }
  
  /**
   * @author Bill Darbie
   */
  private int getRecordSize()
  {

    int recordSize = 0;
    // record version #
    recordSize += _numBytesForInteger;
    // id
    recordSize += _numBytesForInteger;
    // size of slice (int)
    recordSize += _numBytesForInteger;

    return recordSize;
  }
  
  /**
   * @author Bill Darbie
   */
  private SliceNameEnum getSliceNameEnum(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    String fullPadName = getFullPadName(padAndSliceName);
    String sliceName = padAndSliceName.substring(fullPadName.length() + 1, padAndSliceName.length());

    int sliceNumber = Integer.parseInt(sliceName);
    return _enumToIdLookup.getSliceNameEnum(sliceNumber);
  }
  
  /**
   * @author Bill Darbie
   */
  private long getWritePosition() throws DatastoreException
  {
    long position = 0;
    try
    {
      position = _fileChannel.position() + (long)_byteBuffer.position();
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return position;
  }
  
  /**
   * Determine if the file has areas that are unused
   * @author George Booth
   */
  private boolean hasUnusedSpace()
  {
    // see if the number of bytes in the records matches the tail offset
    long totalRecordSize = 0;
    for (Map.Entry<String, Integer> entry : _padAndSliceNameToNumBytesInRecordMap.entrySet())
    {
      totalRecordSize += (long)entry.getValue();
    }
    return totalRecordSize != _tailStartOffset;
  }
  
  /**
   * @author Bill Darbie
   */
  private void setWritePosition(long filePositionAfterHeader) throws DatastoreException
  {
    long position = filePositionAfterHeader + _numBytesForPreHeader;

    try
    {
      if (_firstReadOrWrite || _reading)
      {
        // we were reading, so clear the buffer so it can be used for writes
       _byteBuffer.clear();
       _fileChannel.position(position);
       loadByteBufferForWrite();
        _reading = false;
        _firstReadOrWrite = false;
      }
      else
      {
        // check to see if our buffer is already on this position
        long currBufferStartPosition = _fileChannel.position();
        long currBufferEndPosition = _fileChannel.position() + _byteBuffer.limit();

        if (position >= currBufferStartPosition && position < currBufferEndPosition)
        {
          // _byteBuffer already contains the passed in position, so set its position to the proper place
          long newPosition = position - currBufferStartPosition;
          Assert.expect(newPosition < Integer.MAX_VALUE);
          _byteBuffer.position((int)newPosition);
        }
        else
        {
          // we need to read into _byteBuffer on this position
          flushWithoutReload();
          _fileChannel.position(position);
          loadByteBufferForWrite();
        }
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void loadByteBufferForRead() throws DatastoreException
  {
    try
    {
      // set the _byteBuffer position to 0 and limit to capacity
      _byteBuffer.clear();
      // read the contents of the file into _byteBuffer
      int numBytesRead = _fileChannel.read(_byteBuffer);
      // make sure something was read
      Assert.expect(numBytesRead != -1);
      // set the _byteBuffers limit to the number of character read in
      // this would be less than the buffer size if we are near the end of
      // the file
      _byteBuffer.limit(numBytesRead);
      // set the postion back to the beginning of the buffer so reads
      // out of the buffer will start there
      _byteBuffer.rewind();
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void loadByteBufferForWrite() throws DatastoreException
  {
    try
    {
      // get the current file position
      long position = _fileChannel.position();
      // set the _byteBuffer position to 0 and limit to capacity
      _byteBuffer.clear();
      // read in the contents of the file at the current position
      // so that we can modify just some parts of the buffer and have
      // the other parts still match the original file
      int numBytesRead = _fileChannel.read(_byteBuffer);
      // if no bytes are read in then we are at the end of the file
      if (numBytesRead != -1)
      {
        // set the _byteBuffer position to 0 so we are in the right place
        // for writes into the buffer
        _byteBuffer.rewind();
        // set the file position back to where it was originally
        _fileChannel.position(position);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }
  
   /**
   * @author Bill Darbie
   */
  private int writeInt(int value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForInteger)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForInteger);
    }

    _byteBuffer.putInt(value);
    return _numBytesForInteger;
  }

  /**
   * @author Bill Darbie
   */
  private int writeLong(long value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForLong)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForLong);
    }

    _byteBuffer.putLong(value);
    return _numBytesForLong;
  }

  /**
   * @author Bill Darbie
   */
  private int writeFloat(float value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForFloat)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForFloat);
    }

    _byteBuffer.putFloat(value);
    return _numBytesForFloat;
  }

  /**
   * @author Bill Darbie
   */
  private int writeDouble(double value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForDouble)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForDouble);
    }

    _byteBuffer.putDouble(value);
    return _numBytesForDouble;
  }

  /**
   * @author Bill Darbie
   */
  private int writeBoolean(boolean value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForByte)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForByte);
    }

    if (value)
      _byteBuffer.put((byte)1);
    else
      _byteBuffer.put((byte)0);
    return _numBytesForByte;
  }


  /**
   * @author Bill Darbie
   */
  private int writeString(String string) throws DatastoreException
  {
    Assert.expect(string != null);

    byte[] bytes = string.getBytes();
    int numBytes = bytes.length;
    int numBytesTotal = writeInt(numBytes);
    numBytesTotal += numBytes;
    if (_byteBuffer.remaining() < numBytes)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > numBytes);
    }

    _byteBuffer.put(bytes);
    return numBytesTotal;
  }
  
  /**
   * @author Bill Darbie
   */
  private void setReadPosition(long filePositionAfterHeader) throws DatastoreException
  {
    long position = filePositionAfterHeader + _numBytesForPreHeader;

    try
    {
      if (_firstReadOrWrite || (_reading == false))
      {
        if (_firstReadOrWrite == false)
        {
          // dont flush if this is the first read or write we have done since the buffer
          // has not been loaded with anything yet
          flushWithoutReload();
        }
        _fileChannel.position(position);
        loadByteBufferForRead();
        _reading = true;
        _firstReadOrWrite = false;
      }
      else
      {
        // check to see if our buffer is already on this position
        long currBufferStartPosition = _fileChannel.position() - _byteBuffer.limit();
        long currBufferEndPosition = _fileChannel.position();

        if (position >= currBufferStartPosition && position < currBufferEndPosition)
        {
          // _byteBuffer already contains the passed in position, so set its position to the proper place
          long newPosition = position - currBufferStartPosition;
          Assert.expect(newPosition < Integer.MAX_VALUE);
          _byteBuffer.position((int)newPosition);
        }
        else
        {
          // we need to read into _byteBuffer on this position
          _fileChannel.position(position);
          loadByteBufferForRead();
        }
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }
  
  
  /**
   * @author Bill Darbie
   */
  private int readInt() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForInteger)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForInteger)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getInt();
  }

  /**
   * @author Bill Darbie
   */
  private double readDouble() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForDouble)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForDouble)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getDouble();
  }

  /**
   * @author Bill Darbie
   */
  private float readFloat() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForFloat)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForFloat)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getFloat();
  }

  /**
   * @author Bill Darbie
   */
  private long readLong() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForLong)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForLong)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getLong();
  }

  /**
   * @author Bill Darbie
   */
  private boolean readBoolean() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForByte)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForByte)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    byte bite = _byteBuffer.get();
    if (bite == 0)
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  private String readString() throws DatastoreException
  {
    int numBytes = readInt();
    try
    {
      if (_byteBuffer.remaining() < numBytes)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < numBytes)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    byte[] bytes = new byte[numBytes];
    _byteBuffer.get(bytes);
    return new String(bytes);
  }
}
