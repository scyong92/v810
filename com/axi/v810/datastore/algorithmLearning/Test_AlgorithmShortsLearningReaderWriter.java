package com.axi.v810.datastore.algorithmLearning;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_AlgorithmShortsLearningReaderWriter extends UnitTest
{
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmShortsLearningReaderWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String projName = "families_all_rlv";
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      // test that things work when no file exists already
      String file = FileName.getAlgorithmShortsLearningFullPath(projName);
      if (FileUtilAxi.exists(file))
        FileUtilAxi.delete(file);

      Project project = Project.importProjectFromNdfs(projName, warnings);
      AlgorithmShortsLearningReaderWriter shortsLearning = new AlgorithmShortsLearningReaderWriter(project, projName);

      shortsLearning.open();
      shortsLearning.close();

      // create some records
      int count = 0;
      ShortProfileLearning record1 = new ShortProfileLearning();
      record1.setAverageRegionGrayLevel(++count);
      int size = 10;
      float[] array = new float[size];
      for (int i = 0; i < size; ++i)
        array[i] = i;
      record1.setAverageLearnedThicknessProfile(array);

      count = 100;
      ShortProfileLearning record2 = new ShortProfileLearning();
      record2.setAverageRegionGrayLevel(++count);
      record2.setNumberOfThicknessProfilesLearned(++count);
      size = 10;
      array = new float[size];
      for (int i = 0; i < size; ++i)
        array[i] = i;
      record2.setAverageLearnedThicknessProfile(array);

      Pad pad1 = project.getPanel().getPads().get(0);
      Pad pad2 = project.getPanel().getPads().get(1);
      record1.setPad(pad1);
      record1.setSliceNameEnum(SliceNameEnum.PAD);
      record2.setPad(pad2);
      record2.setSliceNameEnum(SliceNameEnum.PAD);

      // put the record in the file
      TestProgram testProgram = project.getTestProgram();
      for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        AffineTransform transform = testSubProgram.getDefaultAggregateAlignmentTransform();
        project.getPanel().getPanelSettings().setRightManualAlignmentTransform(transform);
      }
      shortsLearning.open();
      shortsLearning.writeShortProfileLearning(record1);
      ShortProfileLearning out = shortsLearning.readShortProfileLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(equal(record1, out));

      // write the same record again and make sure things are good.
      shortsLearning.writeShortProfileLearning(record1);
      out = shortsLearning.readShortProfileLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(equal(record1, out));

      // test that delete works
      Expect.expect(shortsLearning.doesShortLearningProfileExist(pad1, SliceNameEnum.PAD));
      shortsLearning.deleteShortProfileLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(shortsLearning.doesShortLearningProfileExist(pad1, SliceNameEnum.PAD) == false);

      // now write another record
      shortsLearning.writeShortProfileLearning(record2);
      out = shortsLearning.readShortProfileLearning(pad2, SliceNameEnum.PAD);
      Expect.expect(equal(record2, out));

      // now change the size of a record and write it again
      size = 20;
      array = new float[size];
      for (int i = 0; i < size; ++i)
        array[i] = i;
      record2.setAverageLearnedThicknessProfile(array);

      // write it out
      shortsLearning.writeShortProfileLearning(record2);
      out = shortsLearning.readShortProfileLearning(pad2, SliceNameEnum.PAD);
      Expect.expect(equal(record2, out));

      // close the file
      shortsLearning.close();

      // now open it and try to read it
      shortsLearning.open();
      out = shortsLearning.readShortProfileLearning(pad2, SliceNameEnum.PAD);
      shortsLearning.close();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean equal(ShortProfileLearning lhs, ShortProfileLearning rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    if ((lhs.getAverageRegionGrayLevel() == rhs.getAverageRegionGrayLevel()) &&
        (lhs.getNumberOfThicknessProfilesLearned() == rhs.getNumberOfThicknessProfilesLearned()))
    {
      float[] lhsArray = lhs.getAverageLearnedThicknessProfile();
      float[] rhsArray = rhs.getAverageLearnedThicknessProfile();

      if (lhsArray.length == rhsArray.length)
      {
        for (int i = 0; i < lhsArray.length; ++i)
        {
          if (lhsArray[i] != rhsArray[i])
            return false;
        }
        return true;
      }
    }

    return false;
  }
}
