package com.axi.v810.datastore.algorithmLearning;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author George Booth
 * @author Bill Darbie
 */
public class Test_AlgorithmExpectedImageLearningReaderWriter extends UnitTest
{
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

  /**
   * @author George Booth
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmExpectedImageLearningReaderWriter());
  }

  /**
   * @author George Booth
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String projName = "families_all_rlv";
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      // test that things work when no file exists already
      String file = FileName.getAlgorithmExpectedImageLearningFullPath(projName);
      if (FileUtilAxi.exists(file))
        FileUtilAxi.delete(file);

      Project project = Project.importProjectFromNdfs(projName, warnings);
      AlgorithmExpectedImageLearningReaderWriter expectedImageLearningRW = new AlgorithmExpectedImageLearningReaderWriter(project, projName);

      expectedImageLearningRW.open();
      expectedImageLearningRW.close();

      // create some records
      int count = 0;
      float fcount = 0.0f;
      int imageWidth1 = 40;
      int imageHeight1 = 40;
      ExpectedImageLearning record1 = new ExpectedImageLearning(imageWidth1, imageHeight1);
      record1.setNumberOfExpectedImagesLearned(++count);
      int size1 = imageWidth1 * imageHeight1;
      float[] array1 = new float[size1];
      for (int i = 0; i < size1; ++i)
        array1[i] = i;
      record1.setCompositeImageArray(array1);
      record1.setNumberOfTestedPixels(++fcount);

      int imageWidth2 = 50;
      int imageHeight2 = 50;
      ExpectedImageLearning record2 = new ExpectedImageLearning(imageWidth2, imageHeight2);
      record2.setNumberOfExpectedImagesLearned(++count);
      int size2 = imageWidth2 * imageHeight2;
      float[] array2 = new float[size2];
      for (int i = 0; i < size2; ++i)
        array2[i] = i;
      record2.setCompositeImageArray(array2);
      record2.setNumberOfTestedPixels(++fcount);

      Pad pad1 = project.getPanel().getPads().get(0);
      Pad pad2 = project.getPanel().getPads().get(1);
      record1.setPad(pad1);
      record1.setSliceNameEnum(SliceNameEnum.PAD);
      record2.setPad(pad2);
      record2.setSliceNameEnum(SliceNameEnum.PAD);

      // put the record in the file
      TestProgram testProgram = project.getTestProgram();
      for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        AffineTransform transform = testSubProgram.getDefaultAggregateAlignmentTransform();
        project.getPanel().getPanelSettings().setRightManualAlignmentTransform(transform);
      }
      expectedImageLearningRW.open();
      expectedImageLearningRW.writeExpectedImageLearning(record1);
      ExpectedImageLearning out = expectedImageLearningRW.readExpectedImageLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(equal(record1, out));

      // write the same record again and make sure things are good.
      expectedImageLearningRW.writeExpectedImageLearning(record1);
      out = expectedImageLearningRW.readExpectedImageLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(equal(record1, out));

      // test that delete works
      Expect.expect(expectedImageLearningRW.doesExpectedImageLearningExist(pad1, SliceNameEnum.PAD));
      expectedImageLearningRW.deleteExpectedImageLearning(pad1, SliceNameEnum.PAD);
      Expect.expect(expectedImageLearningRW.doesExpectedImageLearningExist(pad1, SliceNameEnum.PAD) == false);

      // now write another record
      expectedImageLearningRW.writeExpectedImageLearning(record2);
      out = expectedImageLearningRW.readExpectedImageLearning(pad2, SliceNameEnum.PAD);
      Expect.expect(equal(record2, out));

      // now change the size of a record and write it again
      size2 = imageWidth2 * 2 * imageHeight2;
      array2 = new float[size2];
      for (int i = 0; i < size2; ++i)
        array2[i] = i;
      record2.setCompositeImageArray(array2);

      // write it out
      expectedImageLearningRW.writeExpectedImageLearning(record2);
      out = expectedImageLearningRW.readExpectedImageLearning(pad2, SliceNameEnum.PAD);
      Expect.expect(equal(record2, out));

      // close the file
      expectedImageLearningRW.close();

      // now open it and try to read it
      expectedImageLearningRW.open();
      out = expectedImageLearningRW.readExpectedImageLearning(pad2, SliceNameEnum.PAD);
      expectedImageLearningRW.close();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   * @author Bill Darbie
   */
  private boolean equal(ExpectedImageLearning lhs, ExpectedImageLearning rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    if (lhs.getNumberOfTestedPixels() != rhs.getNumberOfTestedPixels())
    {
      System.out.println("pixels did not match");
    }
    if (lhs.getNumberOfExpectedImagesLearned() != rhs.getNumberOfExpectedImagesLearned())
    {
      System.out.println("images did not match");
    }

    if ((lhs.getNumberOfTestedPixels() == rhs.getNumberOfTestedPixels()) &&
        (lhs.getNumberOfExpectedImagesLearned() == rhs.getNumberOfExpectedImagesLearned()))
    {
      float[] lhsArray = lhs.getCompositeImageArray();
      float[] rhsArray = rhs.getCompositeImageArray();

      if (lhsArray.length == rhsArray.length)
      {
        for (int i = 0; i < lhsArray.length; ++i)
        {
          if (lhsArray[i] != rhsArray[i])
          {
            System.out.println("arrays did not match at index = " + i);
            return false;
          }
        }
        return true;
      }
      else
      {
        System.out.println("array lengths did not match");
      }
    }
    else
    {
      System.out.println("pixels/images did not match");
    }
    return false;
  }
}
