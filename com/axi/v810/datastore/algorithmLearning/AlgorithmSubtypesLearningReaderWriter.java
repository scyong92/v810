package com.axi.v810.datastore.algorithmLearning;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * The algorithm learning information is stored in a single binary file.
 *
 *  Header Record
 *    record version number (int)
 *    number of bytes into file to get to the header (long)
 *
 *  SubtypeLearning Record
 *    record version # (int)
 *    uniqueId (int)
 *    measurementEnum (int)
 *    good/bad (boolean)
 *    populate/notPopulated
 *    value (float)
 *
 *
 *  Tail Record
 *    record version #
 *      numJoints
 *        boardName refDes padName slice (String)
 *        numSlices (int)
 *          sliceName (int)
 *        numMeasurements (int)
 *          uniqueId (int)
 *          number of bytes into file starting at the data section to get to that record (long)
 *
 *    numUnusedIds (int)
 *      unusedId (int)
 *
 *    numUnusedRecordOffsets (int)
 *      unusedRecordOffset (long)
 *
 *   currRecordId (int)
 *
 * There are 3 files used for learning.  This is done to make sure that the algorithmSubtyps.learned
 * file is always valid and always matches the learned data at the time of the last project save.
 *
 * There are 3 files used for this information.
 * - algorithmSubtypes.learned
 *   - always contains the latest learned information that existed on the last project save

 * - algorithmSubtype.learned.temp.projOpen
 *   - this file is created when a project is open
 *   - it is moved back to algorithmShorts.learned when the project is saved
 *   - by waiting for the project save to happen, this makes sure that the algorithmSubtypes.learned file matchs
 *     the learning state at the last project save only!

 * - algorithmSubtypes.learned.temp.open
 *   - this file is created and being written/read while learning is happening
 *   - it is moved back to algorithmSubtypes.learned.temp.projOpen when learning is complete
 *   - if an exception, assert, or program crash happens it will not be copied back.  This way
 *     a corrupt learning file will not be kept. *
 *
 * @author Bill Darbie
 */
public class AlgorithmSubtypesLearningReaderWriter
{
  private Project _project;
  private String _projName;
  private FileChannel _fileChannel;
  private int _bufSize = 8192;
  private ByteBuffer _byteBuffer = ByteBuffer.allocate(_bufSize);
  private boolean _reading;
  private boolean _firstReadOrWrite = true;
  private String _tempProjOpenFileName;
  private String _tempOpenFileName;
  private String _currFileName;
  private Map<String, Set<Integer>> _padNameToSliceEnumMap = new HashMap<String, Set<Integer>>();
  private Map<String, List<Pair<Integer, Long>>> _padAndSliceNameToIdAndFilePositionMap = new HashMap<String, List<Pair<Integer, Long>>>();
  private long _tailStartOffset;
  private long _fileSizeAtOpen;
  private EnumToUniqueIDLookup _enumToIdLookup;
  private int _recordId;
  private int _largestRecordId;
  private boolean _fileModified = false;

  private Set<Integer> _unusedRecordIdSet = new TreeSet<Integer>();
  private Set<Long> _unusedRecordOffsetsSet = new TreeSet<Long>();

  private static int _numBytesForInteger = Integer.SIZE / 8;
  private static int _numBytesForLong = Long.SIZE / 8;
  private static int _numBytesForFloat = Float.SIZE / 8;
  private static int _numBytesForDouble = Double.SIZE / 8;
  private static int _numBytesForByte = Byte.SIZE / 8;
  // the top of the file has a version number and an offset to the header (which is really at the bottom of the file)
  private static int _numBytesForPreHeader = _numBytesForInteger + _numBytesForLong;

  private TimerUtil _timerUtil = new TimerUtil();

  /**
   * @author Bill Darbie
   */
  public AlgorithmSubtypesLearningReaderWriter(Project project)
  {
    Assert.expect(project != null);
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
    _project = project;
    _projName = project.getName();
  }

  /**
   * @author Bill Darbie
   */
  public void clearProjectReference()
  {
    _project = null;
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _reading = true;
    _firstReadOrWrite = true;
    _fileSizeAtOpen = 0;
    _padAndSliceNameToIdAndFilePositionMap.clear();
    _padNameToSliceEnumMap.clear();
    _unusedRecordIdSet.clear();
    _unusedRecordOffsetsSet.clear();
    _tailStartOffset = 0;
    _fileModified = false;
  }

  /**
   * @author Bill Darbie
   */
  public void open() throws DatastoreException
  {
    init();

    // make the directory in case it does not exist
    String dir = Directory.getAlgorithmLearningDir(_projName);
    FileUtilAxi.mkdirs(dir);

    // get file names
    _tempProjOpenFileName = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(_projName);
    _tempOpenFileName = FileName.getAlgorithmSubtypesLearningTempOpenFullPath(_projName);

    // if any old temp file is still around delete it
    if (FileUtilAxi.exists(_tempOpenFileName))
      FileUtilAxi.delete(_tempOpenFileName);

    // copy the current file name to the temporary file
    // this way if the code dies before the close, the original file will still be intact
    // and useable
    if (FileUtilAxi.exists(_tempProjOpenFileName))
      FileUtilAxi.copy(_tempProjOpenFileName, _tempOpenFileName);

    // open the file
    openFile(_tempOpenFileName);
  }

  /**
   * @author Bill Darbie
   */
  private void openForCompact(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    // open the file
    _tempProjOpenFileName = fileName;
    _tempOpenFileName = null;

    openFile(_tempProjOpenFileName);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isOpen()
  {
    if (_fileChannel == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  private void openFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    _currFileName = fileName;

    TimerUtil timer = new TimerUtil();
    timer.start();
    _timerUtil.reset();
    _timerUtil.start();

    try
    {
      // work on the temporary file from this point in.  This is done in case a crash happens
      // while the file is open.  A crash could cause the file to be corrupt.  This way the
      // corrupt file will be the temporary file and will cause no harm
      RandomAccessFile raf = new RandomAccessFile(_currFileName, "rw");
      _fileChannel = raf.getChannel();
      _fileSizeAtOpen = _fileChannel.size();
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_currFileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    if (_fileSizeAtOpen > 0)
      readHeaderAndTail();
    _timerUtil.stop();
    timer.stop();

//    if (UnitTest.unitTesting() == false)
//      System.out.println("wpd time to open subtypes learned file: " + timer.getElapsedTimeInMillis());
  }

  /**
   * @author Bill Darbie
   */
  private void createCompactFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    if (FileUtilAxi.exists(fileName))
      FileUtilAxi.delete(fileName);

    Assert.expect(_project != null);
    AlgorithmSubtypesLearningReaderWriter compactReaderWriter = new AlgorithmSubtypesLearningReaderWriter(_project);
    try
    {
      compactReaderWriter.openForCompact(fileName);

      // create a map from offset to pad name in order of offset
      Map<Long, String> offsetToPadAndSliceName = new TreeMap<Long, String>();
      for (Map.Entry<String, List<Pair<Integer, Long>>> entry : _padAndSliceNameToIdAndFilePositionMap.entrySet())
      {
        String padAndSliceName = entry.getKey();
        List<Pair<Integer, Long>> pairs = entry.getValue();
        Assert.expect(pairs.isEmpty() == false);
        long smallestOffset = Long.MAX_VALUE;
        for (Pair<Integer, Long> pair : pairs)
        {
          int id = pair.getFirst();
          long offset = pair.getSecond();

          if (offset < smallestOffset)
            smallestOffset = offset;
        }
        offsetToPadAndSliceName.put(smallestOffset, padAndSliceName);
      }

      // now iterate over that list in order of offset and write things out to the new temporary file
      for (String padAndSliceName : offsetToPadAndSliceName.values())
      {
        for (LearnedJointMeasurement meas : readLearnedJointMeasurements(padAndSliceName))
          compactReaderWriter.writeLearnedJointMeasurement(meas);
      }
    }
    finally
    {
      compactReaderWriter.closeWithoutRename();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void closeWithRename() throws DatastoreException
  {
    close(true);
  }

  /**
   * @author Bill Darbie
   */
  private void closeWithoutRename() throws DatastoreException
  {
    close(false);
  }

  /**
   * @author Bill Darbie
   */
  public void close() throws DatastoreException
  {
    closeWithRename();
  }

  /**
   * @author Bill Darbie
   */
  private void close(boolean rename) throws DatastoreException
  {
    _timerUtil.start();

    if (_fileChannel == null)
      return;

    if (_fileModified)
      writeHeaderAndTail();
    try
    {
      if (_fileModified)
      {
        if (_firstReadOrWrite || (_reading == false))
        {
          flushWithoutReload();
          _fileChannel.truncate(_fileChannel.position());
        }
      }

      TimerUtil timerUtil = new TimerUtil();
      timerUtil.start();

      boolean renameFile = false;
      if (_fileModified)
      {
        if (_unusedRecordOffsetsSet.isEmpty() == false)
          createCompactFile(_tempProjOpenFileName);
        else
        {
          if (rename)
            renameFile = true;
        }
      }

      if (_fileChannel != null)
        _fileChannel.close();
      _fileChannel = null;

      if (renameFile)
        FileUtilAxi.rename(_tempOpenFileName, _tempProjOpenFileName);

      timerUtil.stop();
//      if (UnitTest.unitTesting() == false)
//        System.out.println("wpd time to compact short learning file: " + timerUtil.getElapsedTimeInMillis());
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      if ((_tempOpenFileName != null) && (FileUtilAxi.exists(_tempOpenFileName)))
        FileUtilAxi.delete(_tempOpenFileName);

      init();
    }
    _timerUtil.stop();
//    if (UnitTest.unitTesting() == false)
//      System.out.println("wpd total time to read shorts learning file: " + _timerUtil.getElapsedTimeInMillis());
  }

  /**
   * @author Bill Darbie
   */
  private void closeOnException()
  {
    try
    {
      if (_fileChannel != null)
        _fileChannel.close();
      _fileChannel = null;

      try
      {
        if (FileUtilAxi.exists(_tempOpenFileName))
          FileUtilAxi.delete(_tempOpenFileName);
      }
      catch (DatastoreException de)
      {
        de.printStackTrace();
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeHeaderAndTail() throws DatastoreException
  {
    // write the header
    setWritePosition(-_numBytesForPreHeader);
    // header version number
    writeInt(1);
    writeLong(_tailStartOffset);

    // write the tail
    setWritePosition(_tailStartOffset);

    // tail record version number
    writeInt(1);

    // numJoints
    int numPads = _padAndSliceNameToIdAndFilePositionMap.size();
    writeInt(numPads);
    for (Map.Entry<String, List<Pair<Integer, Long>>> entry : _padAndSliceNameToIdAndFilePositionMap.entrySet())
    {
      String padAndSliceName = entry.getKey();
      List<Pair<Integer, Long>> pairs = entry.getValue();
      int numMeasurements = pairs.size();

      String fullPadName = getFullPadName(padAndSliceName);

      // pad name
      writeString(padAndSliceName);

      Set<Integer> sliceEnumSet = _padNameToSliceEnumMap.get(fullPadName);
      int numSlices = sliceEnumSet.size();
      // numSlices (int)
      writeInt(numSlices);
      for (int slice : sliceEnumSet)
      {
        // sliceName (int)
        writeInt(slice);
      }

      // numMeasurements
      writeInt(numMeasurements);
      for (Pair<Integer, Long> pair : pairs)
      {
        int id = pair.getFirst();
        long offset = pair.getSecond();

        // id
        writeInt(id);

        // offset
        writeLong(offset);
      }
    }

    // numUnusedRecordIds
    writeInt(_unusedRecordIdSet.size());
    for (int unusedId : _unusedRecordIdSet)
    {
      // unusedRecordId
      writeInt(unusedId);
    }

    //  numUnusedRecordOffsets (int)
    writeInt(_unusedRecordOffsetsSet.size());
    for (long unusedRecordOffset : _unusedRecordOffsetsSet)
    {
      //  unusedRecordOffset (long)
      writeLong(unusedRecordOffset);
    }

    // currRecordId (int)
    writeInt(_largestRecordId);
  }

  /**
   * @author Bill Darbie
   */
  private Pad getPad(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);

    String boardName = padAndSliceName.substring(0, firstSpace);
    String refDes = padAndSliceName.substring(firstSpace + 1, secondSpace);
    String padName = padAndSliceName.substring(secondSpace + 1, thirdSpace);

    Assert.expect(_project != null);
    return _project.getPanel().getPad(boardName, refDes, padName);
  }

  /**
   * @author Bill Darbie
   */
  private boolean doesPadExist(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);

    String boardName = padAndSliceName.substring(0, firstSpace);
    String refDes = padAndSliceName.substring(firstSpace + 1, secondSpace);
    String padName = padAndSliceName.substring(secondSpace + 1, thirdSpace);

    Assert.expect(_project != null);
    return _project.getPanel().hasPad(boardName, refDes, padName);
  }

  /**
   * @author Bill Darbie
   */
  private String getFullPadName(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    int firstSpace = padAndSliceName.indexOf(" ");
    int secondSpace = padAndSliceName.indexOf(" ", firstSpace + 1);
    int thirdSpace = padAndSliceName.indexOf(" ", secondSpace + 1);
    String fullPadName = padAndSliceName.substring(0, thirdSpace);
    return fullPadName.intern();
  }

  /**
   * @author Bill Darbie
   */
  private SliceNameEnum getSliceNameEnum(String padAndSliceName)
  {
    Assert.expect(padAndSliceName != null);

    String fullPadName = getFullPadName(padAndSliceName);
    String sliceName = padAndSliceName.substring(fullPadName.length() + 1, padAndSliceName.length());

    int sliceNumber = Integer.parseInt(sliceName);
    return _enumToIdLookup.getSliceNameEnum(sliceNumber);
  }

  /**
   * @author Bill Darbie
   */
  private void readHeaderAndTail() throws DatastoreException
  {
    // read the header
    setReadPosition(-_numBytesForPreHeader);
    int headerVersion = readInt();
    if (headerVersion != 1)
      throw new FileCorruptDatastoreException(_currFileName);

    long headerOffset = readLong();
    _tailStartOffset = headerOffset;

    // read the tail
    setReadPosition(headerOffset);

    int recordVersionNumber = readInt();
    if (recordVersionNumber != 1)
      throw new FileCorruptDatastoreException(_currFileName);

    int numJoints = readInt();
    for (int i = 0; i < numJoints; ++i)
    {
      String padAndSliceName = readString();
      String fullPadName = getFullPadName(padAndSliceName);
      int numSlices = readInt();
      for (int k = 0; k < numSlices; ++k)
      {
        Set<Integer> sliceSet = _padNameToSliceEnumMap.get(fullPadName);
        if (sliceSet == null)
        {
          sliceSet = new HashSet<Integer>();
          _padNameToSliceEnumMap.put(fullPadName, sliceSet);
        }
        sliceSet.add(readInt());
      }

      int numMeasurements = readInt();
      for (int j = 0; j < numMeasurements; ++j)
      {
        int id = readInt();
        long offset = readLong();

        List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
        if (pairs == null)
        {
          pairs = new LinkedList<Pair<Integer, Long>>();
          Object prev = _padAndSliceNameToIdAndFilePositionMap.put(padAndSliceName, pairs);
          Assert.expect(prev == null);
        }
        pairs.add(new Pair<Integer, Long>(id, offset));
      }
    }

    // numUnusedRecordIds (int)
    int numUnusedRecordIds = readInt();
    for (int i = 0; i < numUnusedRecordIds; ++i)
    {
      // unusedRecordId (int)
      int unusedId = readInt();
      boolean added = _unusedRecordIdSet.add(unusedId);
      Assert.expect(added);
    }

    // numUnusedRecordOffsets (int)
    int numUnusedRecordOffsets = readInt();
    for (int i = 0; i < numUnusedRecordOffsets; ++i)
    {
      // unusedRecordOffset (long)
      long unusedOffset = readLong();
      boolean added = _unusedRecordOffsetsSet.add(unusedOffset);
      Assert.expect(added);
    }

    _largestRecordId = readInt();
    _recordId = _largestRecordId;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private String getPadAndSliceName(Pad pad, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = getFullPadName(pad) + " " + _enumToIdLookup.getSliceNameUniqueID(sliceNameEnum);
    return padAndSliceName.intern();
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private String getFullPadName(Pad pad)
  {
    Assert.expect(pad != null);

    String fullPadName = pad.getComponent().getBoard().getName() + " " +
                         pad.getComponent().getReferenceDesignator() + " " +
                         pad.getName();
    return fullPadName.intern();
  }

  /**
   * @author Bill Darbie
   */
  private int getRecordSize()
  {
    int recordSize = 0;
    // record version #
    recordSize += _numBytesForInteger;
    // id
    recordSize += _numBytesForInteger;
    // measurementEnum (int)
    recordSize += _numBytesForInteger;
    // good
    recordSize += _numBytesForByte;
    // populated
    recordSize += _numBytesForByte;
    // value
    recordSize += _numBytesForFloat;

    return recordSize;
  }

  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements(Pad pad, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
    deleteLearnedJointMeasurements(padAndSliceName);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Subtype subtype,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    MeasurementEnum measurementEnum,
                                                                    boolean populatedBoard,
                                                                    boolean goodJoint) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(measurementEnum != null);

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    for (Pad pad : subtype.getPads())
    {
      String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
      List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
      if (pairs != null)
      {
        for (Pair<Integer, Long> pair : pairs)
        {
          LearnedJointMeasurement learnedJointMeasurement = readLearnedJointMeasurement(padAndSliceName, pair);
          if (learnedJointMeasurement != null)
          {
            // Only add the measurement if it passes our filters.
            boolean passesFilters = (learnedJointMeasurement.getMeasurementEnum().equals(measurementEnum)) &&
                (learnedJointMeasurement.isPopulatedBoard() == populatedBoard) &&
                (learnedJointMeasurement.isGoodJoint() == goodJoint);
            if (passesFilters)
            {
              learnedJointMeasurements.add(learnedJointMeasurement);
            }
          }
        }
      }
    }

    return learnedJointMeasurements;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Subtype subtype,
                                                                    Pad pad,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    MeasurementEnum measurementEnum,
                                                                    boolean populatedBoard,
                                                                    boolean goodJoint) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(measurementEnum != null);

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    if (pairs != null)
    {
      for (Pair<Integer, Long> pair : pairs)
      {
        LearnedJointMeasurement learnedJointMeasurement = readLearnedJointMeasurement(padAndSliceName, pair);
        if (learnedJointMeasurement != null)
        {
          // Only add the measurement if it passes our filters.
          boolean passesFilters = (learnedJointMeasurement.getMeasurementEnum().equals(measurementEnum)) &&
              (learnedJointMeasurement.isPopulatedBoard() == populatedBoard) &&
              (learnedJointMeasurement.isGoodJoint() == goodJoint);
          if (passesFilters)
          {
            learnedJointMeasurements.add(learnedJointMeasurement);
          }
        }
      }
    }

    return learnedJointMeasurements;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Subtype subtype,
                                                                    MeasurementEnum measurementEnum,
                                                                    boolean populatedBoard,
                                                                    boolean goodJoint) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(measurementEnum != null);

    _timerUtil.start();

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    for (Pad pad : subtype.getPads())
    {
      String padName = pad.getName();
      Collection<Integer> sliceIds = _padNameToSliceEnumMap.get(padName);
      for (int sliceId : sliceIds)
      {
        SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(sliceId);

        String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
        List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
        if (pairs != null)
        {
          for (Pair<Integer, Long> pair : pairs)
          {
            LearnedJointMeasurement learnedJointMeasurement = readLearnedJointMeasurement(padAndSliceName, pair);
            if (learnedJointMeasurement != null)
            {
              // Only add the measurement if it passes our filters.
              boolean passesFilters = (learnedJointMeasurement.getMeasurementEnum().equals(measurementEnum)) &&
                  (learnedJointMeasurement.isPopulatedBoard() == populatedBoard) &&
                  (learnedJointMeasurement.isGoodJoint() == goodJoint);
              if (passesFilters)
              {
                learnedJointMeasurements.add(learnedJointMeasurement);
              }
            }
          }
        }
      }
    }

    _timerUtil.stop();

    return learnedJointMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements(Subtype subtype, MeasurementEnum measurementEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(measurementEnum != null);

    for (LearnedJointMeasurement learnedJointMeasurement : readLearnedJointMeasurements(subtype))
    {
      if (learnedJointMeasurement.getMeasurementEnum().equals(measurementEnum))
        deleteLearnedJointMeasurement(learnedJointMeasurement);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(_projName);
    if (FileUtilAxi.exists(fileName))
      FileUtilAxi.delete(fileName);
  }

  /**
   * @author Bill Darbie
   */
  private void deleteLearnedJointMeasurement(LearnedJointMeasurement learnedJointMeasurement) throws DatastoreException
  {
    Assert.expect(learnedJointMeasurement != null);

    int targetId = learnedJointMeasurement.getId();

    Pad pad = learnedJointMeasurement.getPad();
    SliceNameEnum sliceNameEnum = learnedJointMeasurement.getSliceNameEnum();
    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    for (Pair<Integer, Long> pair : pairs)
    {
      int id = pair.getFirst();
      long offset = pair.getSecond();
      if (targetId == id)
      {
        pairs.remove(pair);
        if (pairs.isEmpty())
          _padAndSliceNameToIdAndFilePositionMap.remove(padAndSliceName);
        boolean added = _unusedRecordIdSet.add(id);
        Assert.expect(added);
        added = _unusedRecordOffsetsSet.add(offset);
        Assert.expect(added);

        _fileModified = true;
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    for (Pad pad : subtype.getPads())
      deleteLearnedJointMeasurements(pad);
  }

  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    String padFullName = getFullPadName(pad);
    Set<Integer> sliceEnumSet = _padNameToSliceEnumMap.get(padFullName);
    if (sliceEnumSet != null)
    {
      for (int slice : sliceEnumSet)
      {
        SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(slice);
        String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);
        deleteLearnedJointMeasurements(padAndSliceName);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void deleteLearnedJointMeasurements(String padAndSliceName) throws DatastoreException
  {
    Assert.expect(padAndSliceName != null);

    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    Assert.expect(pairs != null);

    for (Pair<Integer, Long> pair : pairs)
    {
      int id = pair.getFirst();
      long offset = pair.getSecond();

      boolean added = _unusedRecordIdSet.add(id);
      Assert.expect(added);
      added = _unusedRecordOffsetsSet.add(offset);
      Assert.expect(added);

      _padAndSliceNameToIdAndFilePositionMap.remove(padAndSliceName);

      String fullPadName = getFullPadName(padAndSliceName);
      _padNameToSliceEnumMap.remove(fullPadName);

      _fileModified = true;
    }
  }

  /**
   * @author Bill Darbie
   */
  public void writeLearnedJointMeasurement(LearnedJointMeasurement learnedJointMeasurement) throws DatastoreException
  {
    Assert.expect(learnedJointMeasurement != null);

    _timerUtil.start();

    Pad pad = learnedJointMeasurement.getPad();
    String padFullName = getFullPadName(pad);
    SliceNameEnum sliceNameEnum = learnedJointMeasurement.getSliceNameEnum();
    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);

    // first check to see if a record for this pad and slice already exists and that it is the
    // correct size
    _fileModified = true;
    long offset = 0;

    int calculatedRecordSize = getRecordSize();

    // check to see if an empty space of the correct size already exists
    boolean append = true;
    if (_unusedRecordOffsetsSet.isEmpty() == false)
    {
      // we have a space open, fill it instead of appending to the end of the file
      offset = _unusedRecordOffsetsSet.iterator().next();
      _unusedRecordOffsetsSet.remove(offset);
      setWritePosition(offset);
      append = false;
    }

    if (append)
    {
      offset = _tailStartOffset;
      setWritePosition(offset);
    }

    int recordSize = 0;
    // record version #
    recordSize += writeInt(1);

    getNextAvailableRecordId();
    // uniqueId
    recordSize += writeInt(_recordId);
    // measurementEnum (int)
    recordSize += writeInt(_enumToIdLookup.getMeasurementUniqueID(learnedJointMeasurement.getMeasurementEnum()));
    // good
    recordSize += writeBoolean(learnedJointMeasurement.isGoodJoint());
    // populated
    recordSize += writeBoolean(learnedJointMeasurement.isPopulatedBoard());
    // value (float)
    recordSize += writeFloat(learnedJointMeasurement.getMeasurementValue());

    Assert.expect(recordSize == calculatedRecordSize);

    _tailStartOffset += recordSize;

    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    if (pairs == null)
    {
      pairs = new LinkedList<Pair<Integer, Long>>();
      _padAndSliceNameToIdAndFilePositionMap.put(padAndSliceName, pairs);
    }
    pairs.add(new Pair<Integer, Long>(_recordId, offset));

    Set<Integer> sliceSet = _padNameToSliceEnumMap.get(padFullName);
    if (sliceSet == null)
    {
      sliceSet = new HashSet<Integer>();
      _padNameToSliceEnumMap.put(padFullName, sliceSet);
    }
    sliceSet.add(_enumToIdLookup.getSliceNameUniqueID(sliceNameEnum));

    _timerUtil.stop();
  }

  /**
   * @author Bill Darbie
   */
  private void getNextAvailableRecordId()
  {
    if (_unusedRecordIdSet.isEmpty())
    {
      ++_largestRecordId;
      _recordId = _largestRecordId;
    }
    else
    {
      _recordId = _unusedRecordIdSet.iterator().next();
      _unusedRecordIdSet.remove(_recordId);
    }
  }

  /**
   * @author Bill Darbie
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    for (Pad pad : subtype.getPads())
      learnedJointMeasurements.addAll(readLearnedJointMeasurements(pad));

    return learnedJointMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    String padName = getFullPadName(pad);
    Set<Integer> sliceEnums = _padNameToSliceEnumMap.get(padName);
    if (sliceEnums != null)
    {
      for (int sliceEnum : sliceEnums)
      {
        SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(sliceEnum);

        learnedJointMeasurements.addAll(readLearnedJointMeasurements(pad, sliceNameEnum));
      }
    }

    return learnedJointMeasurements;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public List<LearnedJointMeasurement> readLearnedJointMeasurements(Pad pad, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);

    String padAndSliceName = getPadAndSliceName(pad, sliceNameEnum);

    _timerUtil.start();

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    for (Pair<Integer, Long> pair : pairs)
    {
      LearnedJointMeasurement learnedJointMeasurement = readLearnedJointMeasurement(padAndSliceName, pair);
      if (learnedJointMeasurement != null)
      {
        learnedJointMeasurements.add(learnedJointMeasurement);
      }
    }

    _timerUtil.stop();
    return learnedJointMeasurements;
  }
  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private List<LearnedJointMeasurement> readLearnedJointMeasurements(String padAndSliceName) throws DatastoreException
  {
    Assert.expect(padAndSliceName != null);

    _timerUtil.start();

    List<LearnedJointMeasurement> learnedJointMeasurements = new ArrayList<LearnedJointMeasurement>();
    List<Pair<Integer, Long>> pairs = _padAndSliceNameToIdAndFilePositionMap.get(padAndSliceName);
    for (Pair<Integer, Long> pair : pairs)
    {
      LearnedJointMeasurement learnedJointMeasurement = readLearnedJointMeasurement(padAndSliceName, pair);
      if (learnedJointMeasurement != null)
      {
        learnedJointMeasurements.add(learnedJointMeasurement);
      }
    }

    _timerUtil.stop();
    return learnedJointMeasurements;
  }

  /**
   * NOTE: This method can sometimes return null if there is no matching pad and slice name.
   *       This can occur if a pad has been renamed.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private LearnedJointMeasurement readLearnedJointMeasurement(String padAndSliceName,
                                                              Pair<Integer, Long> idAndOffsetPair) throws DatastoreException
  {
    Assert.expect(padAndSliceName != null);
    Assert.expect(idAndOffsetPair != null);

    int id = idAndOffsetPair.getFirst();
    long offset = idAndOffsetPair.getSecond();

    // now read the record
    setReadPosition(offset);
    int recordVersion = readInt();
    if (recordVersion != 1)
      throw new FileCorruptDatastoreException(_currFileName);
    int uniqueId = readInt();
    if (uniqueId != id)
      throw new FileCorruptDatastoreException(_currFileName);
    MeasurementEnum measurementEnum = _enumToIdLookup.getMeasurementEnum(readInt());

    boolean good = readBoolean();
    boolean populated = readBoolean();
    float value = readFloat();

    // create LearnedJointMeasurement
    // some pads may have been renamed, so check check that the pad still exists
    LearnedJointMeasurement learnedJointMeasurement = null;
    // XCR-3440 Software crashed when learn with Precision image set
    if (doesPadExist(padAndSliceName) && Float.isNaN(value) == false)
    {
      Pad pad = getPad(padAndSliceName);
      SliceNameEnum sliceNameEnum = getSliceNameEnum(padAndSliceName);

      learnedJointMeasurement = new LearnedJointMeasurement();
      learnedJointMeasurement.setId(uniqueId);
      learnedJointMeasurement.setPad(pad);
      learnedJointMeasurement.setSliceNameEnum(sliceNameEnum);
      learnedJointMeasurement.setMeasurementEnum(measurementEnum);
      learnedJointMeasurement.setMeasurementValue(value);
      learnedJointMeasurement.setPopulatedBoard(populated);
      learnedJointMeasurement.setGoodJoint(good);
    }

    return learnedJointMeasurement;
  }

  /**
   * @author Bill Darbie
   */
  private void flushAndLoadForWrite() throws DatastoreException
  {
    flushWithoutReload();
    loadByteBufferForWrite();
  }


  /**
   * @author Bill Darbie
   */
  private void flushWithoutReload() throws DatastoreException
  {
    try
    {
      if (_fileChannel != null)
      {
        // set the limit to the current postion and set the position to 0
        _byteBuffer.flip();
        // write out the buffer
        int numBytesWritten = _fileChannel.write(_byteBuffer);
        // set the position back to 0 and the limit to the capacity
        _byteBuffer.clear();
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private long getWritePosition() throws DatastoreException
  {
    long position = 0;
    try
    {
      position = _fileChannel.position() + (long)_byteBuffer.position();
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return position;
  }

  /**
   * @author Bill Darbie
   */
  private long getReadPosition() throws DatastoreException
  {
    long position = 0;
    try
    {
      position = _fileChannel.position() - (_byteBuffer.limit() - _byteBuffer.position());
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return position;
  }


  /**
   * @author Bill Darbie
   */
  private int writeInt(int value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForInteger)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForInteger);
    }

    _byteBuffer.putInt(value);
    return _numBytesForInteger;
  }

  /**
   * @author Bill Darbie
   */
  private int writeLong(long value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForLong)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForLong);
    }

    _byteBuffer.putLong(value);
    return _numBytesForLong;
  }

  /**
   * @author Bill Darbie
   */
  private int writeFloat(float value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForFloat)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForFloat);
    }

    _byteBuffer.putFloat(value);
    return _numBytesForFloat;
  }

  /**
   * @author Bill Darbie
   */
  private int writeDouble(double value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForDouble)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForDouble);
    }

    _byteBuffer.putDouble(value);
    return _numBytesForDouble;
  }

  /**
   * @author Bill Darbie
   */
  private int writeBoolean(boolean value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForByte)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > _numBytesForByte);
    }

    if (value)
      _byteBuffer.put((byte)1);
    else
      _byteBuffer.put((byte)0);
    return _numBytesForByte;
  }


  /**
   * @author Bill Darbie
   */
  private int writeString(String string) throws DatastoreException
  {
    Assert.expect(string != null);

    byte[] bytes = string.getBytes();
    int numBytes = bytes.length;
    int numBytesTotal = writeInt(numBytes);
    numBytesTotal += numBytes;
    if (_byteBuffer.remaining() < numBytes)
    {
      flushAndLoadForWrite();
      Assert.expect(_byteBuffer.remaining() > numBytes);
    }

    _byteBuffer.put(bytes);
    return numBytesTotal;
  }

  /**
   * @author Bill Darbie
   */
  private void setReadPosition(long filePositionAfterHeader) throws DatastoreException
  {
    long position = filePositionAfterHeader + _numBytesForPreHeader;

    try
    {
      if (_firstReadOrWrite || (_reading == false))
      {
        if (_firstReadOrWrite == false)
        {
          // dont flush if this is the first read or write we have done since the buffer
          // has not been loaded with anything yet
          flushWithoutReload();
        }
        _fileChannel.position(position);
        loadByteBufferForRead();
        _reading = true;
        _firstReadOrWrite = false;
      }
      else
      {
        // check to see if our buffer is already on this position
        long currBufferStartPosition = _fileChannel.position() - _byteBuffer.limit();
        long currBufferEndPosition = _fileChannel.position();

        if (position >= currBufferStartPosition && position < currBufferEndPosition)
        {
          // _byteBuffer already contains the passed in position, so set its position to the proper place
          long newPosition = position - currBufferStartPosition;
          Assert.expect(newPosition < Integer.MAX_VALUE);
          _byteBuffer.position((int)newPosition);
        }
        else
        {
          // we need to read into _byteBuffer on this position
          _fileChannel.position(position);
          loadByteBufferForRead();
        }
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void setWritePosition(long filePositionAfterHeader) throws DatastoreException
  {
    long position = filePositionAfterHeader + _numBytesForPreHeader;

    try
    {
      if (_firstReadOrWrite || _reading)
      {
        // we were reading, so clear the buffer so it can be used for writes
       _byteBuffer.clear();
       _fileChannel.position(position);
       loadByteBufferForWrite();
        _reading = false;
        _firstReadOrWrite = false;
      }
      else
      {
        // check to see if our buffer is already on this position
        long currBufferStartPosition = _fileChannel.position();
        long currBufferEndPosition = _fileChannel.position() + _byteBuffer.limit();

        if (position >= currBufferStartPosition && position < currBufferEndPosition)
        {
          // _byteBuffer already contains the passed in position, so set its position to the proper place
          long newPosition = position - currBufferStartPosition;
          Assert.expect(newPosition < Integer.MAX_VALUE);
          _byteBuffer.position((int)newPosition);
        }
        else
        {
          // we need to read into _byteBuffer on this position
          flushWithoutReload();
          _fileChannel.position(position);
          loadByteBufferForWrite();
        }
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void loadByteBufferForRead() throws DatastoreException
  {
    try
    {
      // set the _byteBuffer position to 0 and limit to capacity
      _byteBuffer.clear();
      // read the contents of the file into _byteBuffer
      int numBytesRead = _fileChannel.read(_byteBuffer);
      // make sure something was read
      Assert.expect(numBytesRead != -1);
      // set the _byteBuffers limit to the number of character read in
      // this would be less than the buffer size if we are near the end of
      // the file
      _byteBuffer.limit(numBytesRead);
      // set the postion back to the beginning of the buffer so reads
      // out of the buffer will start there
      _byteBuffer.rewind();
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void loadByteBufferForWrite() throws DatastoreException
  {
    try
    {
      // get the current file position
      long position = _fileChannel.position();
      // set the _byteBuffer position to 0 and limit to capacity
      _byteBuffer.clear();
      // read in the contents of the file at the current position
      // so that we can modify just some parts of the buffer and have
      // the other parts still match the original file
      int numBytesRead = _fileChannel.read(_byteBuffer);
      // if no bytes are read in then we are at the end of the file
      if (numBytesRead != -1)
      {
        // set the _byteBuffer position to 0 so we are in the right place
        // for writes into the buffer
        _byteBuffer.rewind();
        // set the file position back to where it was originally
        _fileChannel.position(position);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private int readInt() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForInteger)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForInteger)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getInt();
  }

  /**
   * @author Bill Darbie
   */
  private double readDouble() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForDouble)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForDouble)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getDouble();
  }

  /**
   * @author Bill Darbie
   */
  private float readFloat() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForFloat)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForFloat)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return _byteBuffer.getFloat();
  }

  /**
   * @author Bill Darbie
   */
  private long readLong() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForLong)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForLong)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
    return _byteBuffer.getLong();
  }

  /**
   * @author Bill Darbie
   */
  private boolean readBoolean() throws DatastoreException
  {
    try
    {
      if (_byteBuffer.remaining() < _numBytesForByte)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < _numBytesForByte)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    byte bite = _byteBuffer.get();
    if (bite == 0)
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  private String readString() throws DatastoreException
  {
    int numBytes = readInt();
    try
    {
      if (_byteBuffer.remaining() < numBytes)
      {
        long newPosition = _fileChannel.position() - _byteBuffer.remaining();
        _fileChannel.position(newPosition);
        loadByteBufferForRead();

        if (_byteBuffer.remaining() < numBytes)
          throw new FileCorruptDatastoreException(_currFileName);
      }
    }
    catch (IOException ioe)
    {
      closeOnException();

      CannotReadDatastoreException de = new CannotReadDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    byte[] bytes = new byte[numBytes];
    _byteBuffer.get(bytes);
    return new String(bytes);
  }
}
