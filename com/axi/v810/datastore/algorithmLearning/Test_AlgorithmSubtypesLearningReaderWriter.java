package com.axi.v810.datastore.algorithmLearning;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_AlgorithmSubtypesLearningReaderWriter extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmSubtypesLearningReaderWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String projName = "families_all_rlv";

    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();

      // test that things work when no file exists already
      String file = FileName.getAlgorithmSubtypesLearningFullPath(projName);
      if (FileUtilAxi.exists(file))
        FileUtilAxi.delete(file);

      Project project = Project.importProjectFromNdfs(projName, warnings);
      AlgorithmSubtypesLearningReaderWriter subtypesLearning = new AlgorithmSubtypesLearningReaderWriter(project);
      subtypesLearning.open();
      subtypesLearning.close();

      // create some records
      int count = 0;
      Pad pad1 = project.getPanel().getPads().get(0);
      Pad pad2 = project.getPanel().getPads().get(1);
      LearnedJointMeasurement record1 = new LearnedJointMeasurement(pad1,
                                                                    SliceNameEnum.PAD,
                                                                    MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL,
                                                                    ++count,
                                                                    true,
                                                                    true);

      LearnedJointMeasurement record2 = new LearnedJointMeasurement(pad2,
                                                                    SliceNameEnum.PAD,
                                                                    MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL,
                                                                    ++count,
                                                                    false,
                                                                    false);



      // put the record in the file
      subtypesLearning.open();
      subtypesLearning.writeLearnedJointMeasurement(record1);
      List<LearnedJointMeasurement> out = subtypesLearning.readLearnedJointMeasurements(pad1, SliceNameEnum.PAD);
      Expect.expect(out.size() == 1);
      Expect.expect(equal(record1, out.get(0)));

      // write the same record again and make sure things are good.
      subtypesLearning.writeLearnedJointMeasurement(record1);
      out = subtypesLearning.readLearnedJointMeasurements(pad1, SliceNameEnum.PAD);
      Expect.expect(out.size() == 2);
      Expect.expect(equal(record1, out.get(0)));
      Expect.expect(equal(record1, out.get(1)));

      // test that delete works
      subtypesLearning.deleteLearnedJointMeasurements(pad1);
      out = subtypesLearning.readLearnedJointMeasurements(pad1);
      Expect.expect(out.size() == 0);

      // now write another record
      subtypesLearning.writeLearnedJointMeasurement(record2);
      out = subtypesLearning.readLearnedJointMeasurements(pad2, SliceNameEnum.PAD);
      Expect.expect(out.size() == 1);
      Expect.expect(equal(record2, out.get(0)));

      // close the file
      subtypesLearning.close();

      // now open it and try to read it
      subtypesLearning.open();
      out = subtypesLearning.readLearnedJointMeasurements(pad2, SliceNameEnum.PAD);
      Expect.expect(out.size() == 1);
      Expect.expect(equal(record2, out.get(0)));
      subtypesLearning.close();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean equal(LearnedJointMeasurement lhs, LearnedJointMeasurement rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    if ((lhs.getMeasurementEnum().equals(rhs.getMeasurementEnum())) &&
        (lhs.getMeasurementValue() == rhs.getMeasurementValue()) &&
        (lhs.getPad() == rhs.getPad()) &&
        (lhs.getSliceNameEnum().equals(rhs.getSliceNameEnum())) &&
        (lhs.isGoodJoint() == rhs.isGoodJoint()) &&
        (lhs.isPopulatedBoard() == rhs.isPopulatedBoard()))
    {
      return true;
    }

    return false;
  }
}
