package com.axi.v810.datastore;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class is used to create log for command from iae to camera - setScan
 * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
 * @author swee-yee.wong
 */
public class IaeCameraCommandLogUtil 
{
  private static IaeCameraCommandLogUtil _instance;
  private FileLoggerAxi _logUtility;
  private String _logFileNameFullPath;
  private boolean _firstLog = true;
  
  /**
   * @author swee-yee.wong
   */
  private IaeCameraCommandLogUtil()
  {
    //do nothing
  }

   /**
   * @author swee-yee.wong
   */
  public static synchronized IaeCameraCommandLogUtil getInstance()
  {
    if (_instance == null)
      _instance = new IaeCameraCommandLogUtil();

    return _instance;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void log(String data) throws XrayTesterException
  {
    Assert.expect(data != null);
    _logFileNameFullPath = FileName.getIaeCameraCommandLogFullPath();
    if (_logFileNameFullPath.isEmpty() == false)
    {
      
      if(_firstLog == true)
      {
        try
        {
          if (FileUtil.exists(_logFileNameFullPath))
          {
            FileUtil.delete(_logFileNameFullPath);
          }
        }
        catch (CouldNotDeleteFileException fe)
        {
          System.out.println("Could not delete iaeCameraCommand log file");
          CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(fe.getFileName());
          dex.initCause(fe);
          throw dex;
        }
        _logUtility = new FileLoggerAxi(_logFileNameFullPath);
        _logUtility.appendWithoutDateTime("Pass\tCamera\t# Rows to Collect\tdirection\t# Triggers to Ignore (+ve)\t# Triggers to Ignore (-ve)");
        _firstLog = false;
      }
      else
      {
        _logUtility = new FileLoggerAxi(_logFileNameFullPath);
      }
      _logUtility.appendWithoutDateTime(data);
    }
  }

}
