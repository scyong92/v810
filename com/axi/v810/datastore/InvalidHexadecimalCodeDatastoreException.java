package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * If a hexadecimal code cannot be parsed, then throw this exception.
 *
 * @author Bob Balliew
 */
public class InvalidHexadecimalCodeDatastoreException extends DatastoreException
{
  /**
   * This is for hexadecimal parsing errors.
   *
   * @param token is what failed to parse as a hexadecimal code.
   * @author Bob Balliew
   */
  public InvalidHexadecimalCodeDatastoreException(String token)
  {
    super(new LocalizedString("DS_INVALID_HEXADECIMAL_CODE_EXCEPTION_KEY",
                              new Object[]{token}));

    Assert.expect(token != null);
  }
}
