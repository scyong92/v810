package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is corrupt in some way.
 * @author Bill Darbie
 */
public class FileCorruptDatastoreException extends DatastoreException
{
  private String _fileName;

  /**
   * Create an exception.  Make sure that the localizedString you pass
   * in constains the file name, line number, and an explanation of what
   * was expected compared to what was actually parsed in.
   * @param fileName is the name of the file that is corrupt
   * @param lineNumber is the line number where the problem is
   * @param expected is the string of what was expected
   * @param actual is the string of what was actually found
   * @author Keith Lee
   */
  public FileCorruptDatastoreException(String fileName,
                                       int lineNumber,
                                       String expected,
                                       String actual)
  {
    super(new LocalizedString("DS_ERROR_FILE_CORRUPT_KEY", new Object[]{fileName, new Integer(lineNumber), expected, actual}));
    Assert.expect(fileName != null);
    Assert.expect(expected != null);
    Assert.expect(actual != null);
    _fileName = fileName;
  }

  /**
   * This gives a general purpose message - try to use the constructor
   * above if possible instead of this one so the error message given
   * to the user will be more helpful.
   * @param fileName is the name of the file that is corrupt
   * @param lineNum is the line number in the file where the problem occurred.
   * @author Bill Darbie
   */
  public FileCorruptDatastoreException(String fileName, int lineNum)
  {
    super(new LocalizedString("DS_ERROR_FILE_CORRUPT_NO_EXPECT_KEY", new Object[]{fileName, new Integer(lineNum)}));
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * This method works the same as the one above except that it says that it does not know what
   * line number the problem was on.  Only use this method when you really cannot report the line
   * number.
   * @author Bill Darbie
   */
  public FileCorruptDatastoreException(String fileName,
                                       String expected,
                                       String actual)
  {
    super(new LocalizedString("DS_ERROR_FILE_CORRUPT_KEY", new Object[]{fileName, "unknown", expected, actual}));
    Assert.expect(fileName != null);
    Assert.expect(expected != null);
    Assert.expect(actual != null);
    _fileName = fileName;
  }

  /**
   * Use this constuctor when you do not know the linenumber and you cannot give
   * a more useful message (a binary file would be like this).
   * Use the constructor above if possible instead of this one.
   *
   * @author Bill Darbie
   */
  public FileCorruptDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_FILE_CORRUPT_NO_LINE_NUMBER_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
    _fileName = fileName;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    Assert.expect(_fileName != null);
    return _fileName;
  }
  }
