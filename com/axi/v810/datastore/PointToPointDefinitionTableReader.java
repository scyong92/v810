package com.axi.v810.datastore;

import java.io.*;
import java.util.regex.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.Config;

/**
 * @author Erica Wheatcroft
 */
public class PointToPointDefinitionTableReader
{
  private static final String _UNSIGNED_INT_PATTERN_TEXT = "\\d+";

  private static final String _UNITS_PATTERN_TEXT = "nanometers";
  private static final String _COMMENT_PATTERN_TEXT = "\\s*#.*";
  private static final String _DELIMITER_PATTERN_TEXT = "\\t";

  private static Pattern _pointToPointPattern = Pattern.compile("^" +
      "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
      "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
      "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
      "(" + _UNITS_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
      "$");

  private static Pattern _commentPattern = Pattern.compile("^" + _COMMENT_PATTERN_TEXT + "$");

  private static Pattern _checkSumPattern = Pattern.compile("^" + _UNSIGNED_INT_PATTERN_TEXT + "$");

  private Adler32 _checksumUtil = new Adler32();

  /**
   * @author Erica Wheatcroft
   */
  public PointToPointDefinitionTableReader()
  {
  }

  /**
   * @author Erica Wheatcroft
   */
  public void read(javax.swing.JTable table, String fileName) throws DatastoreException
  {
    Assert.expect(table != null);
    Assert.expect(fileName != null);

    com.axi.v810.gui.service.PointToPointMoveDefinitionTableModel model =
        (com.axi.v810.gui.service.PointToPointMoveDefinitionTableModel)table.getModel();

    BufferedReader reader = null;
    try
    {

      reader = new BufferedReader(new FileReader(fileName));
      String line = reader.readLine();
      if (line != null)
      {
        while (line != null)
        {
          Matcher commentMatcher = _commentPattern.matcher(line);
          Matcher checkSumMatcher = _checkSumPattern.matcher(line);
          Matcher pointMatcher = _pointToPointPattern.matcher(line);

          if (commentMatcher.matches())
          {
            // update the checksum as comments are included
            _checksumUtil.update(line.getBytes());
          }
          else if (pointMatcher.matches())
          {
            // add the line to the checksum util
            _checksumUtil.update(line.getBytes());
            parsePointToPointMove(model, pointMatcher);
          }
          else if (checkSumMatcher.matches())
          {
            String checkSumString = checkSumMatcher.group(0);
            long checkSumValue = 0;
            try
            {
              checkSumValue = StringUtil.convertStringToLong(checkSumString);
            }
            catch (BadFormatException bfe)
            {
              // this should not happen as the reg expression should have caught this
              Assert.expect(false);
            }
            if(Config.isDeveloperDebugModeOn())
            {
              // bypass checking if it's developer debug mode.
            }
            else if (checkSumValue != _checksumUtil.getValue())
            {
              FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
              throw de;
            }
            else
            {
              // the file has not been changed.. we are done .
            }
          }
          else
          {
            FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
            throw de;
          }
          line = reader.readLine();
        }
      }
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(fileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      DatastoreException de = new CannotReadDatastoreException(fileName);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      try
      {
        if (reader != null)
          reader.close();
      }
      catch (IOException ioe)
      {
        // do nothing.
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void parsePointToPointMove(com.axi.v810.gui.service.PointToPointMoveDefinitionTableModel model,
                             Matcher scanPathMatcher)
  {
    // create an entry into the table model
    StagePositionMutable entry = model.insertRow();

    // get the x start position
    String xStartPosition = scanPathMatcher.group(2);
    try
    {
      int xStartPositionInNanometers = StringUtil.convertStringToInt(xStartPosition);
      entry.setXInNanometers(xStartPositionInNanometers);
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the y start position
    String yStartPosition = scanPathMatcher.group(3);
    try
    {
      int yStartPositionInNanometers = StringUtil.convertStringToInt(yStartPosition);
      entry.setYInNanometers(yStartPositionInNanometers);
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }
  }
}
