package com.axi.v810.datastore;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.userAccounts.UserTypeEnum;

/**
 * @author Sunit Bhalla
 * This class maps enumerations to integers.  This allows the integers that are stored in the results and learning
 * databases to be mapped to the correct enumeration.  This class also verifies that all enumeration have a unique
 * integer mapping, and that no integer is used twice for a enumeration class.
 */
public class EnumToUniqueIDLookup
{
  private static EnumToUniqueIDLookup _instance;

  private Map<MeasurementEnum, Integer> _measurementEnumToIntegerMap = new HashMap<MeasurementEnum, Integer>();
  private Map<Integer, MeasurementEnum> _integerToMeasurementEnumMap = new HashMap<Integer, MeasurementEnum>();

  private Map<IndictmentEnum, Integer> _indictmentEnumToIntegerMap = new HashMap<IndictmentEnum, Integer>();
  private Map<Integer, IndictmentEnum> _integerToIndictmentEnumMap = new HashMap<Integer, IndictmentEnum>();

  private Map<MeasurementUnitsEnum, Integer> _measurementUnitsEnumToIntegerMap = new HashMap<MeasurementUnitsEnum, Integer>();
  private Map<Integer, MeasurementUnitsEnum> _integerToMeasurementUnitsEnumMap = new HashMap<Integer, MeasurementUnitsEnum>();

  private Map<AlgorithmEnum, Integer> _algorithmEnumToIntegerMap = new HashMap<AlgorithmEnum, Integer>();
  private Map<Integer, AlgorithmEnum> _integerToAlgorithmEnumMap = new HashMap<Integer, AlgorithmEnum>();

  private Map<AlgorithmSettingEnum, Integer> _algorithmSettingEnumToIntegerMap = new HashMap<AlgorithmSettingEnum, Integer>();
  private Map<Integer, AlgorithmSettingEnum> _integerToAlgorithmSettingEnumMap = new HashMap<Integer, AlgorithmSettingEnum>();

  private Map<JointTypeEnum, Integer> _jointTypeEnumToIntegerMap = new HashMap<JointTypeEnum, Integer>();
  private Map<Integer, JointTypeEnum> _integerToJointTypeEnumMap = new HashMap<Integer, JointTypeEnum>();

  private Map<SliceNameEnum, Integer> _sliceNameEnumToIntegerMap = new HashMap<SliceNameEnum, Integer>();
  private Map<Integer, SliceNameEnum> _integerToSliceNameEnumMap = new HashMap<Integer, SliceNameEnum>();

  private Map<UserTypeEnum, Integer> _userTypeEnumToIntegerMap = new HashMap<UserTypeEnum, Integer>();
  private Map<Integer, UserTypeEnum> _integerToUserTypeEnumMap = new HashMap<Integer, UserTypeEnum>();

  private Map<SliceNameEnum, SliceNameEnum> _sliceNameEnumToComponentSliceNameEnumMap = new HashMap<SliceNameEnum, SliceNameEnum>();
  private Map<SliceNameEnum, SliceNameEnum> _componentSliceNameEnumToSliceNameEnumMap = new HashMap<SliceNameEnum, SliceNameEnum>();
  private Map<SliceNameEnum, SliceNameEnum> _sliceNameEnumToDiagnosticSliceNameEnumMap = new HashMap<SliceNameEnum, SliceNameEnum>();
  private Map<SliceNameEnum, SliceNameEnum> _sliceNameEnumToEnhancedImageSliceNameEnumMap = new HashMap<SliceNameEnum, SliceNameEnum>();
 
  /**
   * @author Sunit Bhalla
   */
  public static synchronized EnumToUniqueIDLookup getInstance()
  {
    if (_instance == null)
      _instance = new EnumToUniqueIDLookup();

    return _instance;
  }

  /**
   * @author Sunit Bhalla
   */
  private EnumToUniqueIDLookup()
  {
    buildMeasurementEnumMaps();
    buildIndictmentEnumMaps();
    buildMeasurementUnitsEnumMaps();
    buildAlgorithmEnumMaps();
    buildAlgorithmSettingEnumMaps();
    buildJointTypeEnumMaps();
    buildSliceNameEnumMaps();
    buildUserTypeEnumMaps();
    buildComponentSliceNameEnumMaps();
    buildDiagnosticSliceNameEnumMaps();
    buildEnhancedImageSliceNameEnumMaps();
  }

  /**
   * @author Sunit Bhalla
   */
  private void buildMeasurementEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapMeasurementEnumToInteger(MeasurementEnum.SHORT_THICKNESS, 1);//Lim, Lay Ngor - edit enum name fron SHORT_WIDTH to SHORT_THICKNESS
    mapMeasurementEnumToInteger(MeasurementEnum.LOCATOR_X_LOCATION, 2);
    mapMeasurementEnumToInteger(MeasurementEnum.LOCATOR_Y_LOCATION, 3);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_DIAMETER, 4);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_THICKNESS, 5);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_REGION_OUTLIER, 6);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL, 7);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL, 8);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD, 9);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD, 10);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD, 11);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION, 12);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS, 13);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE, 14);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT, 16);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL, 17);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL, 18);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION, 19);
    mapMeasurementEnumToInteger(MeasurementEnum.PRESSFIT_GRAYLEVEL, 27);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_THICKNESS, 28);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_CENTER_THICKNESS, 29);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_LENGTH, 30);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_HEEL_THICKNESS, 31);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT, 32);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL, 34);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 35);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS, 38);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH, 39);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP, 40);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT, 41);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT, 42);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL, 43);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS, 44);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_LEARNING_HEEL_EDGE_PERCENT_OF_MAX_THICKNESS, 45);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_LEARNING_HEEL_EDGE_TO_PEAK_DISTANCE_PERCENT_OF_PAD_LENGTH_ALONG, 46);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_TOE_THICKNESS, 47);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 48);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_LEARNING_PIN_LENGTH, 49);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS, 50);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_LEARNING_CENTER_OFFSET, 51);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS, 52);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY, 53);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_VOIDING_PERCENT, 54);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL, 55);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS, 57);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 58);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_THICKNESS, 59);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO, 60);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_THICKNESS, 61);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM, 62);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE, 63);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE, 64);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES, 65);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT, 66);
    mapMeasurementEnumToInteger(MeasurementEnum.CALIBRATION_GRAY_LEVEL, 67);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL, 68);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL, 69);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_AVERAGE_PAD_ONE_THICKNESS, 70);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_AVERAGE_PAD_TWO_THICKNESS, 71);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS, 72);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS, 73);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_OPEN_PAD_ONE_OPEN_SIGNAL, 74);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_OPEN_PAD_TWO_OPEN_SIGNAL, 75);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL, 76);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS, 77);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS, 78);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH, 79);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS, 80);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_LEARNING_SIDE_FILLET_EDGE_PERCENT_OF_MAX_THICKNESS, 81);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_LEARNING_EDGE_TO_PEAK_DISTANCE, 82);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_LEARNING_CENTER_OFFSET, 83);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PROFILE_SHARPNESS_ALONG, 84);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_OVERALL_SHARPNESS, 85);
    mapMeasurementEnumToInteger(MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL, 86);
    mapMeasurementEnumToInteger(MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION, 87);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_HEEL_POSITION, 88);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD, 89);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL, 90);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH, 91);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS, 92);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION, 93);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_JOINT_OFFSET_FROM_CAD, 94);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH, 95);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS, 96);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS, 97);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS, 98);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS, 99);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE, 100);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE, 101);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE, 102);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM, 103);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES, 104);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_UPWARD_CURVATURE, 105);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_SHARPNESS, 106);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_WIDTH, 107);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM, 108);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM, 109);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_WIDTH, 110);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM, 111);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM, 112);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL, 113);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 114);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL, 115);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_OPEN_SIGNAL, 116);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_INSUFFICIENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 117);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_INSUFFICIENT_TOE_THICKNESS_PERCENT_OF_NOMINAL, 118);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_HEEL_EDGE_PERCENT_OF_MAX_THICKNESS, 119);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_EDGE_TO_HEEL_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG, 120);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_PIN_LENGTH, 121);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS, 122);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_EDGE_TO_CENTER_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG, 123);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_HEEL_EDGE_MAX_SLOPE_INDEX, 124);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_TOE_EDGE_MAX_SLOPE_INDEX, 125);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_EDGE_TO_TOE_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG, 126);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_UPWARD_CURVATURE_START_PERCENT_OF_FILLET_LENGTH_ALONG, 127);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_UPWARD_CURVATURE_END_PERCENT_OF_FILLET_LENGTH_ALONG, 128);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_OPEN_SIGNAL, 129);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_VOIDING_PERCENT, 130);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE, 131);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL, 132);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION, 133);
    // PE I did a manual merge here to resolve numbering
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER, 134);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT, 135);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_JOINT_OFFSET_FROM_CAD, 137);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_1, 138);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_1, 139);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_1, 140);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_1, 141);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_2, 142);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_2, 143);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_2, 144);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_2, 145);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_3, 146);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_3, 147);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_3, 148);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_3, 149);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_4, 150);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_4, 151);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_4, 152);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_4, 153);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_1, 154);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_1, 155);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_1, 156);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_1, 157);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_2, 158);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_2, 159);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_2, 160);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_2, 161);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_3, 162);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_3, 163);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_3, 164);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_3, 165);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_4, 166);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_4, 167);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_4, 168);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_4, 169);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_1, 170);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_1, 171);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_1, 172);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_1, 173);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_2, 174);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_2, 175);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_2, 176);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_2, 177);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_3, 178);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_3, 179);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_3, 180);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_3, 181);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_4, 182);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_4, 183);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_4, 184);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_4, 185);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_1, 186);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_1, 187);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_1, 188);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_1, 189);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_2, 190);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_2, 191);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_2, 192);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_2, 193);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_3, 194);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_3, 195);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_3, 196);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_3, 197);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_4, 198);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_4, 199);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_4, 200);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_4, 201);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_1, 202);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_1, 203);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_1, 204);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_1, 205);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_2, 206);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_2, 207);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_2, 208);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_2, 209);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_3, 210);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_3, 211);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_3, 212);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_3, 213);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_4, 214);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_4, 215);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_4, 216);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_4, 217);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_1, 218);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_1, 219);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_1, 220);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_1, 221);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_2, 222);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_2, 223);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_2, 224);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_2, 225);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_3, 226);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_3, 227);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_3, 228);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_3, 229);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_4, 230);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_4, 231);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_4, 232);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_4, 233);


    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_OPEN_NUMBER_FAILING_GAPS, 234);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_VOIDING_PERCENT, 235);

    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS, 236);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_PEAK_POSITION, 237);

    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT, 238);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT, 239);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST, 240);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_1, 241);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_2, 242);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_3, 243);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_4, 244);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_1, 245);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_2, 246);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_3, 247);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_4, 248);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_1, 249);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_2, 250);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_3, 251);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_4, 252);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_1, 253);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_2, 254);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_3, 255);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_4, 256);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_1, 257);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_2, 258);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_3, 259);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_4, 260);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_1, 261);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_2, 262);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_3, 263);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_4, 264);

    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE, 265);

    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION, 266);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME, 267);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_LEARNING_CENTER_OF_SOLDER_VOLUME, 268);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL, 269);

    // CR33212 & CR33224 - Lim, Seng Yew
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO, 270);
    // Fixed color - Bonus fix before ViTrox time.
    mapMeasurementEnumToInteger(MeasurementEnum.SHORT_LENGTH, 271);

    // CR33074 - Lim, Seng Yew
    // Added on 29-May-2009
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG, 272);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG, 273);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG, 274);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS, 275);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS, 276);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS, 277);

    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS, 278);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES, 279);

    
    // CR33222 - Rick Gaudette
    // Added on 09-September-2009
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER, 280);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_MASS_SHIFT_ACROSS, 281);

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    // CR33213-Capacitor Misalignment (Jan 2010) again by Lim, Seng Yew
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE, 282);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE, 283);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE, 284);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE,285);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE,286);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES, 287);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG_IN_PERCENTAGE, 288);

    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_HIP_VALUE, 289);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE, 290);

    //@author Jack Hwee- individual void
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT, 291);
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT, 292);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_INDIVIDUAL_VOIDING_PERCENT, 293);
    mapMeasurementEnumToInteger(MeasurementEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_PERCENT, 294);
    
    //Chin Seong, Kee - Request to add Excess to the Gullwing family of algorithms
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_TOE_THICKNESS_PERCENT_OF_NOMINAL, 295);
    
    // By Lee Herng - Fix wrong display of Measurement for HeelToPadCenterDifferenceFromNominal
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL, 296);
    
    // Wei Chin: Side Fillet Measurement for Ventura Connector
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_2_THICKNESS, 297);
    mapMeasurementEnumToInteger(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT, 298);
    
    
    // By Jack Hwee - wetting coverage for PTH
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE, 299);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY, 300);
    
    // Siew Yeng - PTH wetting coverage improvement
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE, 301);
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE, 302);

    // By Bee Hoon - PCAP Misalignment measured by slug edge
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_THRESHOLD, 303);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ACROSS, 304);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ALONG, 305);
    mapMeasurementEnumToInteger(MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_POSITION, 306);
    mapMeasurementEnumToInteger(MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT, 307);
    
    //By Lim, Lay Ngor - PIPA void volume
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE, 308);
    
    //Lim, Lay Ngor - XCR1766 - Add pin length measurement
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_JOINT_PIN_LENGTH, 309);

    //By Lee Herng - Used for OKI Dump All Measurement
    mapMeasurementEnumToInteger(MeasurementEnum.ALL_MEASUREMENT, 310);
    
    //By Lim, Lay Ngor - XCR1743:Benchmark
    mapMeasurementEnumToInteger(MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS, 311);
    
    //Kee Chin Seong - COMponent Voiding Midball
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_COMPONENT_MIDBALL_VOIDING, 312);

    //By Lim, Lay Ngor - XCR????: Broken Pin
    mapMeasurementEnumToInteger(MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE, 313);

    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE, 314);    

    //Lim, Lay Ngor - XCR???? - Clear Tombstone
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS, 315);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT, 316);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE, 317);
    
    //Lim, Lay Ngor - XCR???? - Opaque Tombstone
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS, 318);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH, 319);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO, 320);
    
    //sheng chuan - Clear Tombstone - pattern match
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT, 321);
    mapMeasurementEnumToInteger(MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING, 322);
    
    //By Kee Chin Seong
    mapMeasurementEnumToInteger(MeasurementEnum.SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL, 323);
    mapMeasurementEnumToInteger(MeasurementEnum.SHARED_VOIDING_EXONERATED_VOIDING_GREY_LEVEL, 324);
    mapMeasurementEnumToInteger(MeasurementEnum.SHARED_VOIDING_DIFFERENCE_FROM_NOMINAL_PERCENTAGE, 325);
    
    //Siew Yeng - XCR-3515 - Void Diameter
    mapMeasurementEnumToInteger(MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT, 326);
    
    // Siew Yeng - XCR-3532 - Open Fillet Length Region Outlier
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER, 327);
    mapMeasurementEnumToInteger(MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER, 328);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER, 329);
    mapMeasurementEnumToInteger(MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER, 330);
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    mapMeasurementEnumToInteger(MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL, 331);
    
    //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
    mapMeasurementEnumToInteger(MeasurementEnum.IMAGE_RESIZE_SCALE, 332);
    
    // Ensure that there is a map entry for each MeasurementEnum.
    int numberOfMeasurementEnums = com.axi.util.Enum.getNumEnums(MeasurementEnum.class);
    Assert.expect(_measurementEnumToIntegerMap.size() == numberOfMeasurementEnums);

    for (Map.Entry<MeasurementEnum, Integer> entry : _measurementEnumToIntegerMap.entrySet())
    {
      MeasurementEnum prev = _integerToMeasurementEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for MeasurementEnum : " + entry.getValue());
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private void buildIndictmentEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapIndictmentEnumToInteger(IndictmentEnum.SHORT, 1);
    mapIndictmentEnumToInteger(IndictmentEnum.OPEN, 2);
    mapIndictmentEnumToInteger(IndictmentEnum.INSUFFICIENT, 3);
    mapIndictmentEnumToInteger(IndictmentEnum.MISSING, 4);
    mapIndictmentEnumToInteger(IndictmentEnum.MISALIGNMENT, 5);
    mapIndictmentEnumToInteger(IndictmentEnum.VOIDING, 6);
    mapIndictmentEnumToInteger(IndictmentEnum.COMPONENT_VOIDING, 7);
    mapIndictmentEnumToInteger(IndictmentEnum.EXCESS, 8);
    mapIndictmentEnumToInteger(IndictmentEnum.INDIVIDUAL_VOIDING, 9); //@author Jack Hwee- individual void
    mapIndictmentEnumToInteger(IndictmentEnum.DUMMY, 10); //@author Siew Yeng - use for semi-automated mode

    // Ensure that there is a map entry for each IndictmentEnum.
    int numberOfIndictmentEnums = com.axi.util.Enum.getNumEnums(IndictmentEnum.class);
    Assert.expect(_indictmentEnumToIntegerMap.size() == numberOfIndictmentEnums);

    for (Map.Entry<IndictmentEnum, Integer> entry : _indictmentEnumToIntegerMap.entrySet())
    {
      IndictmentEnum prev = _integerToIndictmentEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for IndictmentEnum : " + entry.getValue());
    }
  }


  /**
   * @author Sunit Bhalla
   */
  private void buildMeasurementUnitsEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.NONE, 1);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.MILS, 2);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.NANOMETERS, 3);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.MILLIMETERS, 4);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT, 5);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS, 6);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG, 7);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE, 8);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.GRAYLEVEL, 9);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_GRAYLEVEL, 10);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_AVERAGE_CORRECTED_GRAYLEVEL, 11);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM, 12);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.INTER_QUARTILE_RANGE, 13);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PITCH, 14);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG, 15);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PIXELS, 16);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_PEAK_DISTANCE, 17);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED, 18);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS, 19);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_COMPONENT_BODY_LENGTH, 20);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.LOWER_TO_UPPER_FILLET_THICKNESS_RATIO, 21);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ACROSS, 22);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE, 23);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_SIDE_FILLET_LENGTH, 24);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PIN_LENGTH, 25);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_TOE_THICKNESS, 26);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_PAD_ONE_THICKNESS, 27);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH, 28);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.DEGREES, 29);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_CENTER_THICKNESS, 30);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_NIMINAL_FILLET_LENGTH, 31);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ALONG, 32);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS, 33);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.SQUARE_MILLIMETERS, 34);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.CUBIC_MILLIMETERS, 35);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.SQUARE_MILS, 36);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.CUBIC_MILS, 37);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.DEFINITE_VOIDING_GREY_LEVEL_VOIDED, 38);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.EXONERATED_VOIDING_GREY_LEVEL_VOIDED, 39);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.VOID_DIFFERENCE_FROM_NOMINAL_GREY_LEVEL, 40);
    mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum.PERCENT_OF_DIAMETER_VOIDED, 41); //Siew Yeng - XCR-3515
    
    // Ensure that there is a map entry for each MeasurementUnitsEnum.
    int numberOfMeasurementUnitsEnums = com.axi.util.Enum.getNumEnums(MeasurementUnitsEnum.class);
    Assert.expect(_measurementUnitsEnumToIntegerMap.size() == numberOfMeasurementUnitsEnums);

    for (Map.Entry<MeasurementUnitsEnum, Integer> entry : _measurementUnitsEnumToIntegerMap.entrySet())
    {
      MeasurementUnitsEnum prev = _integerToMeasurementUnitsEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for MeasurementUnitsEnum : " + entry.getValue());
    }
  }


  /**
   * @author Sunit Bhalla
   */
  private void buildAlgorithmEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapAlgorithmEnumToInteger(AlgorithmEnum.MEASUREMENT, 1);
    mapAlgorithmEnumToInteger(AlgorithmEnum.SHORT, 2);
    mapAlgorithmEnumToInteger(AlgorithmEnum.OPEN, 3);
    mapAlgorithmEnumToInteger(AlgorithmEnum.INSUFFICIENT, 4);
    mapAlgorithmEnumToInteger(AlgorithmEnum.MISALIGNMENT, 5);
    mapAlgorithmEnumToInteger(AlgorithmEnum.VOIDING, 6);
    mapAlgorithmEnumToInteger(AlgorithmEnum.EXCESS, 7);

    // Ensure that there is a map entry for each AlgorithmEnum.
    int numberOfAlgorithmEnums = com.axi.util.Enum.getNumEnums(AlgorithmEnum.class);
    Assert.expect(_algorithmEnumToIntegerMap.size() == numberOfAlgorithmEnums);

    for (Map.Entry<AlgorithmEnum, Integer> entry : _algorithmEnumToIntegerMap.entrySet())
    {
      AlgorithmEnum prev = _integerToAlgorithmEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for AlgorithmEnum : " + entry.getValue());
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private void buildAlgorithmSettingEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.TEMP_SETTING, 1);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL, 2);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE, 3);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, 4);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD, 5);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD, 6);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL, 7);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL, 8);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE, 9);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE, 10);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD, 11);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE, 12);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD, 13);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL, 14);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE, 15);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_THICKNESS, 16);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD, 17);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL, 18);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE, 19);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED, 20);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION, 21);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, 22);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, 23);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ACROSS, 24);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ALONG, 25);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, 26);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, 27);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_BORDER_WIDTH, 28);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE, 29);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LOCATE_METHOD, 400);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS, 30);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, 31);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, 32);
    // mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICE_OBSOLETE, 35);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL, 36);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL, 37);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST, 38);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL, 39);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE, 40);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL, 42);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE, 43);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, 45);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH, 46);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, 47);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, 48);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH, 49);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, 50);
    // Originally for CR30594 - NBS EA - Toe Edge Search Thickness to be based on "fillet", later customer decided not needed.
//    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_TECHNIQUE_OBSOLETE, 378);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, 51);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, 52);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 53);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT, 55);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 56);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL, 57);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD, 58);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE, 59);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL, 60);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD, 61);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE, 62);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL, 63);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD, 64);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE, 65);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL, 66);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD, 67);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE, 68);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL, 69);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD, 70);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE, 71);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL, 72);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION, 73);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, 75);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, 76);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_LENGTH, 77);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_OPAQUE_OPEN_SIGNAL, 78);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_FILLET_GAP, 79);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, 80);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS, 81);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, 82);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS, 83);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 84);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, 85);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET, 86);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, 87);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD, 89);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD, 92);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD, 93);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD, 94);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD, 95);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION, 96);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE, 97);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_OPEN_SIGNAL, 98);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_THICKNESS, 99);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, 100);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION, 101);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH, 102);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE, 103);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, 104);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE, 105);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS, 106);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS, 107);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE, 108);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, 109);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES, 110);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE, 111);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS, 112);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS, 113);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET, 115);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, 116);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET, 117);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL, 118);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, 119);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL, 120);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL, 121);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT, 122);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD, 123);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL, 124);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE, 125);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD, 126);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL, 127);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE, 128);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT, 129);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION, 130);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL, 131);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS, 132);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD, 133);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE, 134);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH, 135);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION, 136);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION, 137);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH, 141);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION, 142);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE, 143);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, 144);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, 145);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION, 146);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL, 147);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT, 150);
    // mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_BARREL_SLICE_OBSOLETE, 151);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL, 152);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH, 153);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL, 154);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL, 155);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION, 156);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL, 157);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD, 158);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_WIDTH, 159);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT, 160);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION, 161);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION, 162);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER, 163);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_OPEN_MINIMUM_SLUG_THICKNESS, 164);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION, 165);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, 166);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH, 167);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, 168);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET, 169);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, 170);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH, 171);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS, 172);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS, 173);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS, 174);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS, 175);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION, 176);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH, 177);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION, 178);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE, 179);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE, 180);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET, 181);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE, 182);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE, 183);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 184);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 185);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, 186);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL, 187);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE, 188);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE, 189);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES, 190);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, 191);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM, 192);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH, 193);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT, 194);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT, 195);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT, 196);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS, 197);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE, 198);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE, 199);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, 200);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH, 201);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, 202);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM, 203);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE, 204);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 205);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 206);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, 207);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL, 208);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_VOIDING_MAXIMUM_VOID_PERCENT, 209);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_VOIDING_NOISE_REDUCTION, 210);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE, 211);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE, 212);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH, 213);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_EXTRUSION_SLICE_OBSOLETE, 214);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL, 215);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION, 216);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION, 217);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE, 218);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL, 219);
    // mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_VARIABLE_HEIGHT_CONNECTOR_PAD_SLICE_OFFSET_OBSOLETE, 219);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER, 220);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL, 221);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH, 222);

    //EXPOSEDPAD: MEASUREMENT
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG, 233);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG, 234);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS, 235);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS, 236);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS, 237);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS, 238);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS, 239);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS, 240);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG, 241);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE, 242);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE, 243);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS, 244);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS, 245);
    // CTR_SEARCH_DIST is obsolete (see 269, 270)
    // mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CTR_SEARCH_DIST_OBSOLETE, 246);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG, 247);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS, 248);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG, 249);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS, 250);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH, 251);

    // EXPOSED PAD: OPEN
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG, 252);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS, 253);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG, 254);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS, 255);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG, 256);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS, 257);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG, 258);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS, 259);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES, 260);

    // EXPOSED PAD: INSUFFICIENT
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_INSUFFICIENT_MAX_VOID_PERCENT, 261);

    // EXPOSED PAD: VOIDING
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT, 262);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS, 263);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE, 264);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA, 265);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION, 266);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE, 267);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL, 268);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS, 269);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG, 270);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT, 350);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT, 351);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, 352);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT, 353);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT, 354);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL, 355);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION, 356);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION, 357);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION, 358);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION, 359);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT, 360);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, 361);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, 362);
    //mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, 386);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_PACKAGE_SLICEHEIGHT, 363);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_SLICEHEIGHT, 364);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_PCAP_SLUG_SLICEHEIGHT, 365);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT, 366);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT, 367);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT, 368);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS, 369);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS, 370);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS, 371);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION, 372);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, 373);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD, 374);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL, 375);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT, 376);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT, 377);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT, 379);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT, 380);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST, 381);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS, 382);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS, 383);


    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD, 384);
   //mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_USE_FOR_SURFACE_MODELING_OBSOLETE, 385);
     mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT, 386);
     //mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, 387);
     mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS, 388);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS, 389);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG, 390);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE, 391);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_TECHNIQUE, 392);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE, 393);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE, 394);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT, 395);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION, 396);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION, 397);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME, 398);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL, 399);

    // note: AlgorithmSettingEnum.LOCATE_METHOD = 400, out of order above
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS, 401);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH, 402);

     // CR33225 fix - Lee Herng
    // Threshold to set the minimum number of pixels between region inner edge location and region outer edge location
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION, 403);

    // CR33212 & CR33224 - Lim, Seng Yew
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS, 404);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX, 405);

    // CR33074 - Lim, Seng Yew - Added on 29-May-2009
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG, 406);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG, 407);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG, 408);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS, 409);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS, 410);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS, 411);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS, 412);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES, 413);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR, 414);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION, 415);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH,416);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT,417);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT,418);

    // CR33212 - Lim, Seng Yew
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE, 419);
	
	// thunderBolt - Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, 420);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT, 421);


    // CR33222 - Rick Gaudette
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER, 422);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE, 423);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL, 424);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT, 425);
    
    // CR32137 - Chong, Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD, 426);

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG, 427);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR, 428);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE, 429);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION, 430);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG, 431);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE, 432);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR, 433);
 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE, 434);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD, 435);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD, 436);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE, 437);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL, 438);

    //@author Jack Hwee- individual void
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD, 439);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD, 440);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE, 441);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL, 442);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, 443);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, 444);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, 445);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, 446);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS, 447);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, 448);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS, 449);
    //THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2, 450);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2, 451);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3, 452);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3, 453);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4, 454);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4, 455);

    // @author Cheah Lee Herng - AutoFocus Mid-Board Offset
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, 456);

    // Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, 457);

    // Jack Hwee
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD, 458);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD, 459);

    // Added by Seng Yew on 22-Apr-2011
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE, 460);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM, 461);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM, 462);
    
    //Jack Hwee on single pad voiding
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD, 463);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE, 464);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER, 465);
    
    // Added by Lee Herng 15 June 2011
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD, 466);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE, 467);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL, 468);
    
    // Added by Lee Herng 15 Nov 2011 - Grid Array
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_LOWERPAD, 469);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_LOWERPAD, 470);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD, 471);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD, 472);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_LOWERPAD, 473);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_LOWERPAD, 474);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_LOWERPAD, 475);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_LOWERPAD, 476);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD, 477);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD, 478);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD, 479);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD, 480);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD, 481);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_LOWERPAD, 482);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD, 483);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD, 484);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_LOWERPAD, 485);
    
    // Added by Lee Herng 16 Nov 2011 - ThroughHole
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2, 486);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3, 487);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4, 488);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL, 489);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL, 490);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL, 491);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2, 492);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3, 493);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4, 494);
    
    // Added by Lee Herng 20 Nov 2011 - Pressfit
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL, 495);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL, 496);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL, 497);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2, 498);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3, 499);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4, 500);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2, 501);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3, 502);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4, 503);

    //Chin Seong, Kee - Request add Excess to the Gullwing family
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 504);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 505);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_TOE_THICKNESS_PERCENT_OF_NOMINAL, 506);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES, 507);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD, 508);
    
    // Added by Lee Herng 28 Jan 2012 - Capacitor - Maximum Component Body Length
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_BODY_LENGTH, 509);
    
    // Added by Lee Herng 19 Feb 2012 - Enable/Disable Focus Confirmation
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION, 510);
    
    //added by Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL, 511);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL, 512);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL, 513);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL, 514);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL, 515);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL, 516);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5, 517);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6, 518);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7, 519);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8, 520);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9, 521);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10, 522);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5, 523);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6, 524);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7, 525);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8, 526);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9, 527);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10, 528);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5, 529);    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6, 530);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7, 531);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8, 532);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9, 533);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10, 534);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5, 535);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6, 536);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7, 537);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8, 538);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9, 539);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10, 540);
    
    // Added by Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5, 541);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6, 542);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7, 543);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL, 544);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL, 545);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL, 546);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5, 547);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6, 548);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7, 549);
    // Added by Wei Chin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8, 550);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9, 551);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10, 552);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL, 553);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL, 554);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL, 555);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8, 556);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9, 557);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10, 558);
    
	// Added by Lee Herng 22 Mar 2012 - Enable/Disable Multipass Inspection for Grid Array
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION, 559);

	// Added by Lee Herng 22 Mar 2012 - Enable/Disable Multipass Inspection for Grid Array
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE, 560);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, 561);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, 562);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1, 563);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2, 564);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1, 565);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2, 566);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1, 567);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2, 568);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1, 569);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2, 570);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1, 571);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2, 572);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1, 573);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2, 574);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1, 575);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2, 576);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1, 577);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2, 578);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1, 579);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2, 580);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_1, 581);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_2, 582);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1, 583);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2, 584);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1, 585);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2, 586);    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1, 587);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2, 588);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1, 589);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2, 590);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1, 591);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2, 592);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1, 593);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1, 594);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2, 595);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2, 596);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1, 597);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2, 598);
    //Jack Hwee
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR, 599);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO, 600);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE, 601); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD, 602); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE, 603);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY, 604);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL, 605); 
    
    // Wei Chin: add Side Fillet Measurement for Ventura Connector
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS, 606);
    
    // Lee Herng: enable/disable wetting coverage
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE, 607);
    
    // Siew Yeng
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE, 608);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE, 609);
    
	  // Bee Hoon: Slug Edge Detection for PCAP Misalignment
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS, 610);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD, 611);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS, 612);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG, 613);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION, 614);
	
    // Added by Bee Hoon: Single Pad separate XY limit and XY offset limit
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD, 615);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET, 616);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET, 617);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET, 618);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET, 619);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE, 620);
    
    // Lee Herng: Gullwing and Advanced Gullwing void compensation method and window kernel
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD, 621);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY, 622);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD, 623);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD, 624);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY, 625);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD, 626);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE, 627);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY, 628);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY, 629);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE, 630);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1, 631);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_2, 632);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1, 633);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2, 634);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1, 635);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2, 636);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_1, 637);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_2, 638);

    // Wei Chin: add Image Processing - Box Filter 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE, 639);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR, 640);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE, 641);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE, 642);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE, 643);
    
	/**
     * @author Lim, Lay Ngor PIPA
     */
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE, 644);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT, 645);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN, 646);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE, 647);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE, 648);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL, 649);

    //Lim, Lay Ngor & Kee Chin Seong- XCR1648: Add Maximum Slope Limit - START    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG, 650);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG, 651);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG, 652);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS, 653);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS, 654);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS, 655);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE, 656);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, 657);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES, 658);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE, 659);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, 660);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES, 661);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE, 662);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, 663);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM, 664);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE, 665);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE, 666);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, 667);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM, 668);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    
    // Added by Khang Wah, 2013-09-11, user defined initial wavelet level
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, 669);
    
    //Lim, Lay Ngor - XCR1723: Add FFTBandPassFilter for pre-processing - START
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE, 670);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE, 671);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE, 672);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES, 673);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION, 674);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE, 675);    
    //Lim, Lay Ngor - XCR1723: Add FFTBandPassFilter for pre-processing - END
	// Added by Lee Herng, 28 Oct 2013, BGA User-Defined Slice 3
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3, 676);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3, 677);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3, 678);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_3, 679);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3, 680);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3, 681);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3, 682);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3, 683);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3, 684);    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3, 685);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3, 686);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3, 687);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3, 688);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3, 689);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3, 690);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3, 691);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3, 692);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3, 693);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, 694);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.BACKGROUND_SENSITIVITY_VALUE, 695);
    
    //Lim, Lay Ngor - XCR1743:Benchmark
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER, 696);    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION, 697);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS, 698);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH, 699);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH, 700);

    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH, 701);

    // Added by Lee Herng, 2014-08-13, search range limit
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT, 702);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT, 703);
    
    //Siew Yeng - XCR-2169 - Large Pad Voiding - Additinal Layer for Floodfill Method
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL, 704);
    //Siew Yeng - XCR-2210 - Large Pad Largest Void - Combine two voids if the two voids are within an acceptable gap distance
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE, 705);

    //Kee Chin Seong - Component Midball Voiding
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID, 706);
    
    //Wei Chin - XCR-2387 - Save diagnostic Image
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES, 707);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH, 708);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH, 709);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE, 710);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE, 711);
    
    // Added by Lee Herng, 2015-03-27, psp local search
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PSP_LOCAL_SEARCH, 712);
    
    //bee-hoon.goh
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, 713);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, 714);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, 715);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, 716);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, 717);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, 718);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, 719);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, 720);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, 721);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, 722);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, 723);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, 724);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, 725);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, 726);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, 727);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, 728);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4, 729);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5, 730);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6, 731);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7, 732);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8, 733);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9, 734);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10, 735);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11, 736);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12, 737);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13, 738);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14, 739);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15, 740);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16, 741);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17, 742);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18, 743);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19, 744);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20, 745);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4, 746);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5, 747);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6, 748);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7, 749);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8, 750);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9, 751);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10, 752);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11, 753);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12, 754);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13, 755);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14, 756);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15, 757);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16, 758);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17, 759);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18, 760);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19, 761);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20, 762);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4, 763);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5, 764);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6, 765);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7, 766);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8, 767);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9, 768);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10, 769);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11, 770);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12, 771);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13, 772);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14, 773);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15, 774);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16, 775);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17, 776);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18, 777);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19, 778);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20, 779);
    
    // Swee Yee Wong - enable user defined midball to edge offset for grid array
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE, 780);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT, 781);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_4, 782);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4, 783);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_5, 784);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5, 785);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_6, 786);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6, 787);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_7, 788);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7, 789);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_8, 790);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8, 791);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_9, 792);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9, 793);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_10, 794);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10, 795);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_11, 796);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11, 797);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_12, 798);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12, 799);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_13, 800);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13, 801);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_14, 802);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14, 803);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_15, 804);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15, 805);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_16, 806);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16, 807);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_17, 808);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17, 809);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_18, 810);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18, 811);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_19, 812);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19, 813);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_20, 814);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20, 815);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4, 816);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4, 817);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5, 818);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5, 819);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6, 820);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6, 821);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7, 822);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7, 823);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8, 824);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8, 825);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9, 826);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9, 827);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10, 828);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10, 829);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11, 830);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11, 831);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12, 832);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12, 833);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13, 834);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13, 835);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14, 836);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14, 837);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15, 838);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15, 839);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16, 840);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16, 841);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17, 842);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17, 843);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18, 844);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18, 845);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19, 846);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19, 847);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20, 848);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20, 849);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4, 850);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4, 851); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5, 852);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5, 853); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6, 854);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6, 855); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7, 856);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7, 857); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8, 858);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8, 859); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9, 860);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9, 861); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10, 862);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10, 863); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11, 864);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11, 865); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12, 866);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12, 867); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13, 868);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13, 869); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14, 870);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14, 871); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15, 872);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15, 873); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16, 874);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16, 875); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17, 876);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17, 877); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18, 878);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18, 879); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19, 880);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19, 881); 
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20, 882);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20, 883); 
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4, 884);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4, 885);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5, 886);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5, 887);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6, 888);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6, 889);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7, 890);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7, 891);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8, 892);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8, 893);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9, 894);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9, 895);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10, 896);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10, 897);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11, 898);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11, 899);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12, 900);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12, 901);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13, 902);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13, 903);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14, 904);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14, 905);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15, 906);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15, 907);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16, 908);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16, 909);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17, 910);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17, 911);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18, 912);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18, 913);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19, 914);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19, 915);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20, 916);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20, 917);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4, 918);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5, 919);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6, 920);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7, 921);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8, 922);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9, 923);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10, 924);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11, 925);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12, 926);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13, 927);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14, 928);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15, 929);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16, 930);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17, 931);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18, 932);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19, 933);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20, 934);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4, 935);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5, 936);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6, 937);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7, 938);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8, 939);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9, 940);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10, 941);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11, 942);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12, 943);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13, 944);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14, 945);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15, 946);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16, 947);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17, 948);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18, 949);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19, 950);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20, 951);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4, 952);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5, 953);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6, 954);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7, 955);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8, 956);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9, 957);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10, 958);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11, 959);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12, 960);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13, 961);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14, 962);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15, 963);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16, 964);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17, 965);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18, 966);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19, 967);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20, 968);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4, 969);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4, 970);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5, 971);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5, 972);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6, 973);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6, 974);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7, 975);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7, 976);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8, 977);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8, 978);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9, 979);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9, 980);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10, 981);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10, 982);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11, 983);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11, 984);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12, 985);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12, 986);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13, 987);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13, 988);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14, 989);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14, 990);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15, 991);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15, 992);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16, 993);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16, 994);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17, 995);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17, 996);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18, 997);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18, 998);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19, 999);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19, 1000);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20, 1001);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20, 1002);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4, 1003);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5, 1004);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6, 1005);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7, 1006);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8, 1007);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9, 1008);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10, 1009);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11, 1010);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12, 1011);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13, 1012);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14, 1013);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15, 1014);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16, 1015);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17, 1016);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18, 1017);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19, 1018);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20, 1019);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4, 1020);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5, 1021);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6, 1022);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7, 1023);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8, 1024);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9, 1025);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10, 1026);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11, 1027);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12, 1028);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13, 1029);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14, 1030);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15, 1031);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16, 1032);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17, 1033);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18, 1034);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19, 1035);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20, 1036);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, 1037);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE, 1038);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION, 1039);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, 1040);

    //Broken Pin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS, 1041);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROJECTION_SOLDER_SIGNAL, 1042);
    
    //Lim, Lay Ngor - Broken Pin
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN, 1043);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE, 1044);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO, 1045);

    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE, 1046);

    //Siew Yeng - XCR-2814
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD, 1047);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1, 1048);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2, 1049);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3, 1050);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4, 1051);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5, 1052);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6, 1053);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7, 1054);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8, 1055);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9, 1056);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10, 1057);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11, 1058);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12, 1059);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13, 1060);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14, 1061);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15, 1062);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16, 1063);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17, 1064);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18, 1065);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19, 1066);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20, 1067);

    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION, 1068);
    
    //Kok Chun, Tan
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT,1069);
    
    // Chnee Khang Wah
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET,1070);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET,1071);
    
    //Lim, Lay Ngor - Clear Tombstone
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_CENTER_OFFSET, 1072);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD, 1073);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE, 1074);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE, 1075);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, 1076);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE, 1077);
    //Lim, Lay Ngor - Opaque Tombstone
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO, 1078);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE, 1079);    
    //sheng chuan - Clear tombstone - pattern match
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD, 1080);    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE, 1081);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE, 1082);
    
    // Added by Siew Yeng - XCR-3094
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS, 1083);
    
    //Lim, Lay Ngor - XCR-3359 - Opaque Tombstone
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION, 1084);
    
    //Added Kee Chin Seong - Circular Voiding Algorithm
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_VOIDING_DETECTION_SENSITIVITY, 1085);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_VOIDING_REGION_REFERENCE_POSITION, 1086);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_VOIDING_INNER_EDGE_POSITION, 1087);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_VOIDING_OUTER_EDGE_POSITION, 1088);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL, 1089);
    
    //Siew Yeng - XCR-3318 - Oval PTH
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ALONG, 1090);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ACROSS, 1091);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS, 1092);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER, 1093);
    
    //Siew Yeng - XCR-3515 - Void Diameter
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD, 1094);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE, 1095);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL, 1096);
    
    //Siew Yeng
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, 1097);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, 1098);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, 1099);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, 1100);
     
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, 1101);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, 1102);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, 1103);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, 1104);
    
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, 1105);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, 1106);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, 1107);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, 1108);
    
    // Kok Chun, Tan - Motion Blur
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE, 1109);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE, 1110);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET, 1111);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION, 1112);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION, 1113);
    // Kok Chun, Tan - Shading Removal
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE, 1114);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION, 1115);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE, 1116);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE, 1117);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND, 1118);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE, 1119);
    
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION, 1120);
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE, 1121);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE, 1122);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL, 1123);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2, 1124);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3, 1125);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4, 1126);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5, 1127);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6, 1128);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7, 1129);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8, 1130);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9, 1131);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10, 1132);
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST, 1133);

    // Added by Lee Herng, 2016-08-10
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.PSP_Z_OFFSET, 1134);

	//Siew Yeng - XCR-3841
    mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL, 1135);

    // Ensure that there is a map entry for each AlgorithmSettingEnum.
    int numberOfAlgorithmSettingEnums = com.axi.util.Enum.getNumEnums(AlgorithmSettingEnum.class);
    Assert.expect(_algorithmSettingEnumToIntegerMap.size() == numberOfAlgorithmSettingEnums);

    for (Map.Entry<AlgorithmSettingEnum, Integer> entry : _algorithmSettingEnumToIntegerMap.entrySet())
    {
      AlgorithmSettingEnum prev = _integerToAlgorithmSettingEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for AlgorithmSettingEnum : " + entry.getValue());
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private void buildJointTypeEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapJointTypeEnumToInteger(JointTypeEnum.CAPACITOR, 1);
    mapJointTypeEnumToInteger(JointTypeEnum.RESISTOR, 2);
    mapJointTypeEnumToInteger(JointTypeEnum.POLARIZED_CAP, 3);
    mapJointTypeEnumToInteger(JointTypeEnum.GULLWING, 4);
    mapJointTypeEnumToInteger(JointTypeEnum.JLEAD, 5);
    mapJointTypeEnumToInteger(JointTypeEnum.COLLAPSABLE_BGA, 6);
    mapJointTypeEnumToInteger(JointTypeEnum.NON_COLLAPSABLE_BGA, 7);
    mapJointTypeEnumToInteger(JointTypeEnum.CGA, 8);
    mapJointTypeEnumToInteger(JointTypeEnum.PRESSFIT, 9);
    mapJointTypeEnumToInteger(JointTypeEnum.THROUGH_HOLE, 10);
    mapJointTypeEnumToInteger(JointTypeEnum.SINGLE_PAD, 11);
    mapJointTypeEnumToInteger(JointTypeEnum.RF_SHIELD, 12);
    mapJointTypeEnumToInteger(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, 13);
    mapJointTypeEnumToInteger(JointTypeEnum.LEADLESS, 14);
    mapJointTypeEnumToInteger(JointTypeEnum.CHIP_SCALE_PACKAGE, 15);
    mapJointTypeEnumToInteger(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, 16);
    mapJointTypeEnumToInteger(JointTypeEnum.SMALL_OUTLINE_LEADLESS, 17);
    mapJointTypeEnumToInteger(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, 18);
    mapJointTypeEnumToInteger(JointTypeEnum.EXPOSED_PAD, 19);
    mapJointTypeEnumToInteger(JointTypeEnum.LGA, 20);
    mapJointTypeEnumToInteger(JointTypeEnum.OVAL_THROUGH_HOLE, 21); //Siew Yeng - XCR-3318 - Oval PTH
    // Wei Chin (Tall Cap)
//    mapJointTypeEnumToInteger(JointTypeEnum.TALL_CAPACITOR, 20);

    //    mapJointTypeEnumToInteger(JointTypeEnum.CALIBRATION_PAD, 20);

    // Ensure that there is a map entry for each JointTypeEnum.
    int numberOfJointTypeEnums = com.axi.util.Enum.getNumEnums(JointTypeEnum.class);
    Assert.expect(_jointTypeEnumToIntegerMap.size() == numberOfJointTypeEnums);

    for (Map.Entry<JointTypeEnum, Integer> entry : _jointTypeEnumToIntegerMap.entrySet())
    {
      JointTypeEnum prev = _integerToJointTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for JointTypeEnum : " + entry.getValue());
    }
   }

  /**
   * @author Sunit Bhalla
   */
  private void buildSliceNameEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapSliceNameEnumToInteger(SliceNameEnum.SURFACE_MODELING, 0);
    mapSliceNameEnumToInteger(SliceNameEnum.PAD, 1);
    mapSliceNameEnumToInteger(SliceNameEnum.CLEAR_CHIP_PAD, 2);
    mapSliceNameEnumToInteger(SliceNameEnum.OPAQUE_CHIP_PAD, 3);
    mapSliceNameEnumToInteger(SliceNameEnum.PCAP_SLUG, 4);
    mapSliceNameEnumToInteger(SliceNameEnum.PACKAGE, 5);
    mapSliceNameEnumToInteger(SliceNameEnum.MIDBALL, 6);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_PIN_SIDE, 7);
    mapSliceNameEnumToInteger(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL, 30);

    // Obsolete SliceNameEnum here
    // Added back to fix CR31476 - Assert when reading pre 1.12 Subtype learning file
    // by Seng-Yew Lim
    // START
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_10_OBSOLETE, 8);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_20_OBSOLETE, 9);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_30_OBSOLETE, 10);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_40_OBSOLETE, 11);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_50_OBSOLETE, 12);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_60_OBSOLETE, 13);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_70_OBSOLETE, 14);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_80_OBSOLETE, 15);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_90_OBSOLETE, 16);
    // END

    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, 17);
    mapSliceNameEnumToInteger(SliceNameEnum.VIRTUAL_LIVE_SLICE_1, 18);
    mapSliceNameEnumToInteger(SliceNameEnum.VIRTUAL_LIVE_VIRTUAL_SLICE, 19);

    // Obsolete SliceNameEnum here
    // Added back to fix CR31476 - Assert when reading pre 1.12 Subtype learning file
    // by Seng-Yew Lim
    // START
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_MINUS_10_OBSOLETE, 20);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_MINUS_20_OBSOLETE, 21);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_MINUS_30_OBSOLETE, 22);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_75_OBSOLETE, 23);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_25_OBSOLETE, 24);
    // END

    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL, 25);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_PROTRUSION, 26);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_INSERTION, 27);
    mapSliceNameEnumToInteger(SliceNameEnum.HIGH_SHORT, 28);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_2, 29);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_3, 31); //why number 30 cannot use ?? // because upper had used
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_4, 32);
    mapSliceNameEnumToInteger(SliceNameEnum.MID_BOARD_SLICE, 33);
    mapSliceNameEnumToInteger(SliceNameEnum.RFP_SLICE, 34);
	
	// Chnee Khang Wah, 2012-02-21, 2.5D
    // START
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_0, 35);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_1, 36);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_2, 37);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_3, 38);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_4, 39);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_5, 40);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_6, 41);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_7, 42);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_8, 43);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_9, 44);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_10, 45);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_11, 46);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_12, 47);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_13, 48);
    // END
	
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_5, 49);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_6, 50);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_7, 51);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_8, 52);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_9, 53);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_10, 54);

  	mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, 55);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, 56);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_2, 57);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_3, 58);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_4, 59);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_5, 60);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_6, 61);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_7, 62);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_8, 63);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_9, 64);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_10, 65);
    
    mapSliceNameEnumToInteger(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, 66);
    mapSliceNameEnumToInteger(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, 67);
    
    // Chnee Khang Wah, 2013-09-20, user defined initial wavelet level
    mapSliceNameEnumToInteger(SliceNameEnum.USER_DEFINED_WAVELET_LEVEL, 68);
    
    // Added by Lee Herng 28-Oct-2013
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, 69);
    
    // bee-hoon.goh, 2013-12-27, Component Image Slices
    mapSliceNameEnumToInteger(SliceNameEnum.PAD_COMPONENT, 70);
    mapSliceNameEnumToInteger(SliceNameEnum.MIDBALL_COMPONENT, 71);
    mapSliceNameEnumToInteger(SliceNameEnum.PACKAGE_COMPONENT, 72);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_COMPONENT, 73);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_INSERTION_COMPONENT, 74);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_PROTRUSION_COMPONENT, 75);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_PIN_SIDE_COMPONENT, 76);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE_COMPONENT, 77);
    mapSliceNameEnumToInteger(SliceNameEnum.HIGH_SHORT_COMPONENT, 78);
    mapSliceNameEnumToInteger(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL_COMPONENT, 79);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_2_COMPONENT, 80);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_3_COMPONENT, 81);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_4_COMPONENT, 82);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_0_COMPONENT, 83);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_1_COMPONENT, 84);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_2_COMPONENT, 85);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_3_COMPONENT, 86);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_4_COMPONENT, 87);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_5_COMPONENT, 88);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_6_COMPONENT, 89);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_7_COMPONENT, 90);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_8_COMPONENT, 91);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_9_COMPONENT, 92);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_10_COMPONENT, 93);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_11_COMPONENT, 94);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_12_COMPONENT, 95);
    mapSliceNameEnumToInteger(SliceNameEnum.CAMERA_13_COMPONENT, 96);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_5_COMPONENT, 97);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_6_COMPONENT, 98);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_7_COMPONENT, 99);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_8_COMPONENT, 100);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_9_COMPONENT, 101);
    mapSliceNameEnumToInteger(SliceNameEnum.THROUGHHOLE_BARREL_10_COMPONENT, 102);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1_COMPONENT, 103);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2_COMPONENT, 104);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3_COMPONENT, 105);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_2_COMPONENT, 106);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_3_COMPONENT, 107);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_4_COMPONENT, 108);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_5_COMPONENT, 109);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_6_COMPONENT, 110);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_7_COMPONENT, 111);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_8_COMPONENT, 112);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_9_COMPONENT, 113);
    mapSliceNameEnumToInteger(SliceNameEnum.CONNECTOR_BARREL_10_COMPONENT, 114);
    mapSliceNameEnumToInteger(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1_COMPONENT, 115);
    mapSliceNameEnumToInteger(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2_COMPONENT, 116);
    
    // Added by Lee Herng 16 Dec 2013 - To handle all slice in Measurement tab in AlgoTuner
    mapSliceNameEnumToInteger(SliceNameEnum.ALL_SLICE, 117);

    // Added by Lee Herng 19 Aug 2014 - To handle Psp Search Range
    mapSliceNameEnumToInteger(SliceNameEnum.PSP_SEARCH_RANGE_LOW_LIMIT, 118);
    mapSliceNameEnumToInteger(SliceNameEnum.PSP_SEARCH_RANGE_HIGH_LIMIT, 119);
    
    // Added by Wei Chin
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_PAD_SLICE, 120);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_1, 121);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_2, 122);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_MIDBALL_SLICE, 123);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_PACKAGE_SLICE, 124);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1, 125);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2, 126);
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3, 127);
    
    // bee-hoon.goh
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, 128);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, 129);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, 130);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, 131);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, 132);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, 133);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, 134);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, 135);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, 136);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, 137);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, 138);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, 139);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, 140);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, 141);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, 142);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, 143);
    mapSliceNameEnumToInteger(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, 144);
    
    //Siew Yeng - XCR-2683
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_PAD_SLICE, 145);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CLEAR_CHIP_PAD, 146);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_OPAQUE_CHIP_PAD, 147);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_PCAP_SLUG, 148);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL, 149);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_INSERTION, 150);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_PROTRUSION, 151);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_PIN_SIDE, 152);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_COMPONENT_SIDE, 153);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_BGA_LOWERPAD_ADDITIONAL, 154);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_2, 155);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_3, 156);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_4, 157);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_5, 158);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_6, 159);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_7, 160);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_8, 161);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_9, 162);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_10, 163);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_2, 164);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_3, 165);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_4, 166);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_5, 167);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_6, 168);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_7, 169);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_8, 170);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_9, 171);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_CONNECTOR_BARREL_10, 172);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_MIDBALL_SLICE, 173);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_PACKAGE_SLICE, 174);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_1, 175);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_2, 176);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_3, 177);
    
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_USER_DEFINED_SLICE_1, 178);
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_USER_DEFINED_SLICE_2, 179);
     
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_FOREIGN_INCLUSION, 180);
    mapSliceNameEnumToInteger(SliceNameEnum.FOREIGN_INCLUSION, 181);

    //Lim, Lay Ngor - Clear Tombstone
    mapSliceNameEnumToInteger(SliceNameEnum.DIAGNOSTIC_CLEAR_CHIP_SLICE, 182);
    
    //Siew Yeng - XCR-3025 - missing high short enhanced slice
    mapSliceNameEnumToInteger(SliceNameEnum.ENHANCED_HIGH_SHORT, 183);
    
    // XCR-3603 PSP Z-Offset
    mapSliceNameEnumToInteger(SliceNameEnum.PSP_Z_OFFSET, 184);

    // Ensure that there is a map entry for each SliceNameEnum.
    int numberOfSliceNameEnums = com.axi.util.Enum.getNumEnums(SliceNameEnum.class);
    Assert.expect(_sliceNameEnumToIntegerMap.size() == numberOfSliceNameEnums);

    for (Map.Entry<SliceNameEnum, Integer> entry : _sliceNameEnumToIntegerMap.entrySet())
    {
      SliceNameEnum prev = _integerToSliceNameEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for SliceNameEnum : " + entry.getValue());
    }
  }

  /**
   * @author George Booth
   */
  private void buildUserTypeEnumMaps()
  {
    // WARNING - DO NOT CHANGE THESE NUMBERS - CHANGING THEM WILL CAUSE ALL DATABASES
    //           ON CUSTOMER SITES TO BE UNUSABLE!!
    mapUserTypeEnumToInteger(UserTypeEnum.ADMINISTRATOR, 0);
    mapUserTypeEnumToInteger(UserTypeEnum.DEVELOPER, 1);
    mapUserTypeEnumToInteger(UserTypeEnum.OPERATOR, 2);
    mapUserTypeEnumToInteger(UserTypeEnum.SERVICE_EXPERT, 3);
    mapUserTypeEnumToInteger(UserTypeEnum.SERVICE, 4);
    mapUserTypeEnumToInteger(UserTypeEnum.LOGIN, 5);

    // Ensure that there is a map entry for each SliceNameEnum.
    int numberOfUserTypeEnums = com.axi.util.Enum.getNumEnums(UserTypeEnum.class);
    Assert.expect(_userTypeEnumToIntegerMap.size() == numberOfUserTypeEnums);

    for (Map.Entry<UserTypeEnum, Integer> entry : _userTypeEnumToIntegerMap.entrySet())
    {
      UserTypeEnum prev = _integerToUserTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for UserTypeEnum : " + entry.getValue());
    }
  }
  
  // @author bee-hoon.goh
  private void buildComponentSliceNameEnumMaps()
  {
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.PAD_COMPONENT, SliceNameEnum.PAD);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.MIDBALL_COMPONENT, SliceNameEnum.MIDBALL);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.PACKAGE_COMPONENT, SliceNameEnum.PACKAGE);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_INSERTION_COMPONENT, SliceNameEnum.THROUGHHOLE_INSERTION);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_PROTRUSION_COMPONENT, SliceNameEnum.THROUGHHOLE_PROTRUSION);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_PIN_SIDE_COMPONENT, SliceNameEnum.THROUGHHOLE_PIN_SIDE);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE_COMPONENT, SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.HIGH_SHORT_COMPONENT, SliceNameEnum.HIGH_SHORT);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL_COMPONENT, SliceNameEnum.BGA_LOWERPAD_ADDITIONAL);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_2_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_2);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_3_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_3);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_4_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_4);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_0_COMPONENT, SliceNameEnum.CAMERA_0);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_1_COMPONENT, SliceNameEnum.CAMERA_1);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_2_COMPONENT, SliceNameEnum.CAMERA_2);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_3_COMPONENT, SliceNameEnum.CAMERA_3);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_4_COMPONENT, SliceNameEnum.CAMERA_4);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_5_COMPONENT, SliceNameEnum.CAMERA_5);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_6_COMPONENT, SliceNameEnum.CAMERA_6);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_7_COMPONENT, SliceNameEnum.CAMERA_7);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_8_COMPONENT, SliceNameEnum.CAMERA_8);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_9_COMPONENT, SliceNameEnum.CAMERA_9);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_10_COMPONENT, SliceNameEnum.CAMERA_10);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_11_COMPONENT, SliceNameEnum.CAMERA_11);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_12_COMPONENT, SliceNameEnum.CAMERA_12);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CAMERA_13_COMPONENT, SliceNameEnum.CAMERA_13);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_5_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_5);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_6_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_6);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_7_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_7);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_8_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_8);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_9_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_9);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_10_COMPONENT, SliceNameEnum.THROUGHHOLE_BARREL_10);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1_COMPONENT, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2_COMPONENT, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3_COMPONENT, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_2_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_2);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_3_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_3);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_4_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_4);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_5_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_5);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_6_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_6);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_7_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_7);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_8_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_8);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_9_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_9);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_10_COMPONENT, SliceNameEnum.CONNECTOR_BARREL_10);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1_COMPONENT, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
    mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2_COMPONENT, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
    
    // Ensure that there is a map entry for each SliceNameEnum.

    for (Map.Entry<SliceNameEnum, SliceNameEnum> entry : _componentSliceNameEnumToSliceNameEnumMap.entrySet())
    {
      SliceNameEnum prev = _sliceNameEnumToComponentSliceNameEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null, "Duplicate index for SliceNameEnum : " + entry.getValue());
    }
  }

  /**
   * this is not 1 to 1 mapping, it can be same values but map to different key 
   * for example, BGA user defined slice 1 and large pad user defined slice 1 can have same diagnostic slice call DIAGNOSTIC_USER_DEFINED_SLICE_1
   * @author Wei Chin
   */
  private void buildDiagnosticSliceNameEnumMaps()
  {
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.PAD, SliceNameEnum.DIAGNOSTIC_PAD_SLICE);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.MIDBALL, SliceNameEnum.DIAGNOSTIC_MIDBALL_SLICE);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.PACKAGE, SliceNameEnum.DIAGNOSTIC_PACKAGE_SLICE);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3);
    
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_1);
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_2);

    //Broken Pin
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.CAMERA_0, SliceNameEnum.DIAGNOSTIC_FOREIGN_INCLUSION);

    //Lim, Lay Ngor - OKI Clear Tombstone
    mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum.CLEAR_CHIP_PAD, SliceNameEnum.DIAGNOSTIC_CLEAR_CHIP_SLICE);
  }
  
  /**
   * @author Siew Yeng
   */
  private void buildEnhancedImageSliceNameEnumMaps()
  {
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.PAD, SliceNameEnum.ENHANCED_PAD_SLICE);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CLEAR_CHIP_PAD, SliceNameEnum.ENHANCED_CLEAR_CHIP_PAD);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.OPAQUE_CHIP_PAD, SliceNameEnum.ENHANCED_OPAQUE_CHIP_PAD);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.PCAP_SLUG, SliceNameEnum.ENHANCED_PCAP_SLUG);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_INSERTION, SliceNameEnum.ENHANCED_THROUGHHOLE_INSERTION);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_PROTRUSION, SliceNameEnum.ENHANCED_THROUGHHOLE_PROTRUSION);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_PIN_SIDE, SliceNameEnum.ENHANCED_THROUGHHOLE_PIN_SIDE);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, SliceNameEnum.ENHANCED_THROUGHHOLE_COMPONENT_SIDE);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL, SliceNameEnum.ENHANCED_BGA_LOWERPAD_ADDITIONAL);
    //Siew Yeng - XCR-3025 - missing high short enhanced slice
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.HIGH_SHORT, SliceNameEnum.ENHANCED_BGA_LOWERPAD_ADDITIONAL);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_2, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_2);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_3, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_3);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_4, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_4);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_5, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_5);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_6, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_6);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_7, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_7);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_8, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_8);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_9, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_9);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.THROUGHHOLE_BARREL_10, SliceNameEnum.ENHANCED_THROUGHHOLE_BARREL_10);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_2, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_2);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_3, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_3);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_4, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_4);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_5, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_5);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_6, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_6);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_7, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_7);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_8, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_8);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_9, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_9);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.CONNECTOR_BARREL_10, SliceNameEnum.ENHANCED_CONNECTOR_BARREL_10);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.MIDBALL, SliceNameEnum.ENHANCED_MIDBALL_SLICE);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.PACKAGE, SliceNameEnum.ENHANCED_PACKAGE_SLICE);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_1);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_2);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, SliceNameEnum.ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_3);
    
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, SliceNameEnum.ENHANCED_USER_DEFINED_SLICE_1);
    mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, SliceNameEnum.ENHANCED_USER_DEFINED_SLICE_2);
  }

  /**
   * Maps the specified MeasurementEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate MeasurementEnums.
   * @author Sunit Bhalla
   */
  private void mapMeasurementEnumToInteger(MeasurementEnum measurementEnum, Integer integer)
  {
    Assert.expect(measurementEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_measurementEnumToIntegerMap != null);

    Integer previousEntry = _measurementEnumToIntegerMap.put(measurementEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified IndictmentEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate IndictmentEnums.
   * @author Sunit Bhalla
   */
  private void mapIndictmentEnumToInteger(IndictmentEnum indictmentEnum, Integer integer)
  {
    Assert.expect(indictmentEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_indictmentEnumToIntegerMap != null);

    Integer previousEntry = _indictmentEnumToIntegerMap.put(indictmentEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified MeasurementUnitsEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate MeasurementUnitsEnums.
   * @author Sunit Bhalla
   */
  private void mapMeasurementUnitsEnumToInteger(MeasurementUnitsEnum measurementUnitsEnum, Integer integer)
  {
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_measurementUnitsEnumToIntegerMap != null);

    Integer previousEntry = _measurementUnitsEnumToIntegerMap.put(measurementUnitsEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified AlgorithmEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate AlgorithmEnums.
   * @author Sunit Bhalla
   */
  private void mapAlgorithmEnumToInteger(AlgorithmEnum algorithmEnum, Integer integer)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_algorithmEnumToIntegerMap != null);

    Integer previousEntry = _algorithmEnumToIntegerMap.put(algorithmEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified AlgorithmSettingEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate AlgorithmSettingEnums.
   * @author Sunit Bhalla
   */
  private void mapAlgorithmSettingEnumToInteger(AlgorithmSettingEnum algorithmSettingEnum, Integer integer)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_algorithmSettingEnumToIntegerMap != null);

    Integer previousEntry = _algorithmSettingEnumToIntegerMap.put(algorithmSettingEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified JointTypeEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate JointTypeEnums.
   * @author Sunit Bhalla
   */
  private void mapJointTypeEnumToInteger(JointTypeEnum jointTypeEnum, Integer integer)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_jointTypeEnumToIntegerMap != null);

    Integer previousEntry = _jointTypeEnumToIntegerMap.put(jointTypeEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified SliceNameEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate SliceNameEnums.
   * @author Sunit Bhalla
   */
  private void mapSliceNameEnumToInteger(SliceNameEnum sliceNameEnum, Integer integer)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_sliceNameEnumToIntegerMap != null);

    Integer previousEntry = _sliceNameEnumToIntegerMap.put(sliceNameEnum, integer);
    Assert.expect(previousEntry == null);
    // ok, now add the slices for retesting unfocused regions
    for(SliceNameEnum newSliceName : sliceNameEnum.getUnfocusedRegionSliceNamesForRetest())
    {
      previousEntry = _sliceNameEnumToIntegerMap.put(newSliceName, newSliceName.getId());
      Assert.expect(previousEntry == null);
    }
  }

  /**
   * Maps the specified UserTypeEnum to the specified Integer.  Ensures (asserts) no attempts are made to insert
   * duplicate UserTypeEnums.
   * @author George Booth
   */
  private void mapUserTypeEnumToInteger(UserTypeEnum userTypeEnum, Integer integer)
  {
    Assert.expect(userTypeEnum != null);
    Assert.expect(integer != null);
    Assert.expect(_userTypeEnumToIntegerMap != null);

    Integer previousEntry = _userTypeEnumToIntegerMap.put(userTypeEnum, integer);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author bee-hoon.goh
   */
  private void mapComponentSliceNameEnumToSliceNameEnum(SliceNameEnum componentSliceNameEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentSliceNameEnum != null);
    Assert.expect(_componentSliceNameEnumToSliceNameEnumMap != null);

    SliceNameEnum previousEntry = _componentSliceNameEnumToSliceNameEnumMap.put(componentSliceNameEnum, sliceNameEnum);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Wei Chin
   */
  private void mapSliceNameEnumToDiagnosticSliceNameEnum(SliceNameEnum diagnosticSliceNameEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(diagnosticSliceNameEnum != null);
    Assert.expect(_sliceNameEnumToDiagnosticSliceNameEnumMap != null);

    SliceNameEnum previousEntry = _sliceNameEnumToDiagnosticSliceNameEnumMap.put(diagnosticSliceNameEnum, sliceNameEnum);
    Assert.expect(previousEntry == null);
  }
  
  /**
   * @author Siew Yeng
   */
  private void mapSliceNameEnumToEnhancedImageSliceNameEnum(SliceNameEnum enhancedImageSliceNameEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(enhancedImageSliceNameEnum != null);
    Assert.expect(_sliceNameEnumToEnhancedImageSliceNameEnumMap != null);

    SliceNameEnum previousEntry = _sliceNameEnumToEnhancedImageSliceNameEnumMap.put(enhancedImageSliceNameEnum, sliceNameEnum);
    Assert.expect(previousEntry == null);
  }
  
  /**
   * @author Sunit Bhalla
   */
  public MeasurementEnum getMeasurementEnum(int integer)
  {
    MeasurementEnum theEnum = _integerToMeasurementEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getMeasurementUniqueID(MeasurementEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _measurementEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author Sunit Bhalla
   */
  public IndictmentEnum getIndictmentEnum(int integer)
  {
    IndictmentEnum theEnum = _integerToIndictmentEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getIndictmentUniqueID(IndictmentEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _indictmentEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author Sunit Bhalla
   */
  public MeasurementUnitsEnum getMeasurementUnitsEnum(int integer)
  {
    MeasurementUnitsEnum theEnum = _integerToMeasurementUnitsEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getMeasurementUnitsUniqueID(MeasurementUnitsEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _measurementUnitsEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author Sunit Bhalla
   */
  public AlgorithmEnum getAlgorithmEnum(int integer)
  {
    AlgorithmEnum theEnum = _integerToAlgorithmEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getAlgorithmUniqueID(AlgorithmEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _algorithmEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author Sunit Bhalla
   */
  public AlgorithmSettingEnum getAlgorithmSettingEnum(int integer)
  {
    AlgorithmSettingEnum theEnum = _integerToAlgorithmSettingEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getAlgorithmSettingUniqueID(AlgorithmSettingEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _algorithmSettingEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }


  /**
   * @author Sunit Bhalla
   */
  public JointTypeEnum getJointTypeEnum(int integer)
  {
    JointTypeEnum theEnum = _integerToJointTypeEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getJointTypeUniqueID(JointTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _jointTypeEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author Sunit Bhalla
   */
  public SliceNameEnum getSliceNameEnum(int integer)
  {
    SliceNameEnum theEnum = _integerToSliceNameEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getSliceNameUniqueID(SliceNameEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _sliceNameEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }

  /**
   * @author George Booth
   */
  public UserTypeEnum getUserTypeEnum(int integer)
  {
    UserTypeEnum theEnum = _integerToUserTypeEnumMap.get(integer);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author George Booth
   */
  public int getUserTypeUniqueID(UserTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    Integer integer = _userTypeEnumToIntegerMap.get(theEnum);
    Assert.expect (integer != null);
    return integer;
  }
  
  /**
   * @author bee-hoon.goh
   */
  public SliceNameEnum getComponentSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    SliceNameEnum theEnum = _sliceNameEnumToComponentSliceNameEnumMap.get(sliceNameEnum);
    Assert.expect(theEnum != null);
    return theEnum;
  }
  
  /**
   * @author Wei Chin
   */
  public SliceNameEnum getDiagnosticSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    SliceNameEnum theEnum = _sliceNameEnumToDiagnosticSliceNameEnumMap.get(sliceNameEnum);
//    Assert.expect(theEnum != null);
//    It can be null value due to some slices don't have diagnostic image.   
    return theEnum;
  }
  
  /**
   * @author Siew Yeng
   */
  public SliceNameEnum getEnhancedImageSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    SliceNameEnum theEnum = _sliceNameEnumToEnhancedImageSliceNameEnumMap.get(sliceNameEnum);
//    Assert.expect(theEnum != null);
//    It can be null value due to some slices don't have diagnostic image.   
    return theEnum;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isDiagnosticSlice(SliceNameEnum sliceNameEnum)
  {
    return _sliceNameEnumToDiagnosticSliceNameEnumMap.containsValue(sliceNameEnum);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isEnhancedImageSlice(SliceNameEnum sliceNameEnum)
  {
    return _sliceNameEnumToEnhancedImageSliceNameEnumMap.containsValue(sliceNameEnum);
  }
  
  /**
   * @author Janan Wong
   */
  public Map<MeasurementEnum, Integer>  getMeasurementEnumToIntegerMap()
  {
    Assert.expect(_measurementEnumToIntegerMap != null);
    return _measurementEnumToIntegerMap;
  }
      
}