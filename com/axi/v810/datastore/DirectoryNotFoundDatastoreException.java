package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a directory is not found.
 * @author George A. David
 */
public class DirectoryNotFoundDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param directoryName is the name of the directory that is not found
   * @author George A. David
   */
  public DirectoryNotFoundDatastoreException(String directoryName)
  {
    super(new LocalizedString("DS_ERROR_DIRECTORY_NOT_FOUND_KEY", new Object[]{directoryName}));
    Assert.expect(directoryName != null);
  }
}
