package com.axi.v810.datastore;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * This class is thrown when there is subtype setting compatibility issue between version 4 and 5 
 * @author Khaw Chek Hau
 */
public class SubtypeSettingFileVersionNotCompatibleDatastoreException extends DatastoreException
{
  /**
   * @author Khaw Chek Hau
   */
  public SubtypeSettingFileVersionNotCompatibleDatastoreException(String missingSubtype, String subtypeSettingVer5FileName, String subtypeSettingVer4FileName)
  {
    super(new LocalizedString("DS_ERROR_SUBTYPE_SETTING_FILE_VERSION_NOT_COMPATIBLE_KEY", new Object[]
                              {missingSubtype, subtypeSettingVer5FileName, subtypeSettingVer4FileName}));
    Assert.expect(subtypeSettingVer5FileName != null && subtypeSettingVer4FileName != null);
  }
}
