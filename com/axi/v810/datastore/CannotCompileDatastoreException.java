package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when the cleaned NDFs cannot be compiled by the legacy compiler.
 * @author Horst Mueller
 */
public class CannotCompileDatastoreException extends DatastoreException
{
  public CannotCompileDatastoreException(String panelName)
  {
    super( new LocalizedString("DS_ERROR_CANNOT_COMPILE_EXCEPTION_KEY", new Object[]{panelName}) );
    Assert.expect(panelName != null);
  }
}

