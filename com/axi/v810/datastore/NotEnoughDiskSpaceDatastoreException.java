package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when there is not enough disk space to write out a file.
 * @author Laura Cormos
 */
public class NotEnoughDiskSpaceDatastoreException extends DatastoreException
{
  private String _baseFileName;
  private String _parentPath;
  private String _fileName;

  /**
   * Construct the class.
   * @param parentPath is the directory path to the file below
   * @param baseFileName is the name of the file that could not be written
   * @author Laura Cormos
   */
  public NotEnoughDiskSpaceDatastoreException(String parentPath, String baseFileName)
  {
    super(new LocalizedString("DS_NOT_ENOUGH_DISK_SPACE_KEY", new Object[]{parentPath, baseFileName}));
    Assert.expect(parentPath != null);
    Assert.expect(baseFileName != null);
    _fileName = parentPath + baseFileName;
    _baseFileName = baseFileName;
    _parentPath = parentPath;
  }

  /**
   * @author Laura Cormos
   */
  public String getFileName()
  {
    Assert.expect(_fileName != null);
    return _fileName;
  }

  /**
   * @author Laura Cormos
   */
  public String getParentPath()
  {
    Assert.expect(_parentPath != null);
    return _parentPath;
  }

  /**
   * @author Laura Cormos
   */
  public String getBaseFileName()
  {
    Assert.expect(_baseFileName != null);
    return _baseFileName;
  }
}
