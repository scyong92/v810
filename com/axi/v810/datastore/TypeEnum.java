package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;

/**
 * This class acts like a enumeration class that contains all the possible types that
 * the config files can hold.
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
public class TypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private String _name;

  public static final TypeEnum INTEGER = new TypeEnum(++_index, "integer");
  public static final TypeEnum LONG = new TypeEnum(++_index, "long");
  public static final TypeEnum DOUBLE = new TypeEnum(++_index, "double");
  public static final TypeEnum STRING = new TypeEnum(++_index, "string");
  public static final TypeEnum BOOLEAN = new TypeEnum(++_index, "boolean");
  public static final TypeEnum LIST_OF_STRINGS = new TypeEnum(++_index, "StringList");
  public static final TypeEnum BINARY = new TypeEnum(++_index, "Binary");

  private TypeEnum(int value, String name)
  {
    super(value);

    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @return the number of elements in this enumeration.
   * @author Bill Darbie
   */
  static int size()
  {
    return _index + 1;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    Assert.expect(_name != null);

    return _name;
  }
}
