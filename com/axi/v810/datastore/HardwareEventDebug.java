package com.axi.v810.datastore;

import com.axi.v810.hardware.*;
/**
 *
 * <p>Title: HardwareEventDebugOutput</p>
 *
 * <p>Description: Debug utility to show Events as they occur</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * @author George Booth
 */

public class HardwareEventDebug
{
  /**
   * @author George Booth
   */
  public HardwareEventDebug()
  {
    // do nothing
  }

  /**
   * synchronized to be thread safe
   * @author George Booth
   */
  public synchronized static void decodeAndOutputHardwareEvent(HardwareEvent event)
  {
    HardwareEventEnum eventEnum = event.getEventEnum();
    if (eventEnum instanceof XrayTesterEventEnum)
    {
      XrayTesterEventEnum xrayTesterEventEnum = (XrayTesterEventEnum)eventEnum;
      System.out.print("  event = XrayTesterEventEnum.");
      if (xrayTesterEventEnum.equals(XrayTesterEventEnum.STARTUP))
        System.out.print("STARTUP");
      else if (xrayTesterEventEnum.equals(XrayTesterEventEnum.SHUTDOWN))
        System.out.print("SHUTDOWN");
      else if (xrayTesterEventEnum.equals(XrayTesterEventEnum.HARDWARE_NOT_AVAILABLE))
        System.out.print("HARDWARE_NOT_AVAILABLE");
      else
        System.out.print(eventEnum.toString());
    }
    else if (eventEnum instanceof XraySourceEventEnum)
    {
      System.out.print("  event = XraySourceEventEnum.");
      XraySourceEventEnum xraySourceEventEnum = (XraySourceEventEnum)eventEnum;
      if (xraySourceEventEnum.equals(XraySourceEventEnum.ANODE_VOLTAGE))
        System.out.print("ANODE_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.CATHODE_CURRENT))
        System.out.print("CATHODE_CURRENT");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.FILAMENT_CURRENT_LIMIT))
        System.out.print("FILAMENT_CURRENT_LIMIT");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.FILAMENT_VOLTAGE))
        System.out.print("FILAMENT_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.FOCUS_VOLTAGE))
        System.out.print("FOCUS_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.GRID_VOLTAGE))
        System.out.print("GRID_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_ANODE_VOLTAGE))
        System.out.print("MIN_MAX_ANODE_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_CATHODE_CURRENT))
        System.out.print("MIN_MAX_CATHODE_CURRENT");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_FILAMENT_CURRENT))
        System.out.print("MIN_MAX_FILAMENT_CURRENT");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_FILAMENT_VOLTAGE))
        System.out.print("MIN_MAX_FILAMENT_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_GRID_VOLTAGE))
        System.out.print("MIN_MAX_GRID_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.MIN_MAX_ISO_PS_VOLTAGE))
        System.out.print("MIN_MAX_ISO_PS_VOLTAGE");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.OFF))
        System.out.print("OFF");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.ON))
        System.out.print("ON");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.POWER_OFF))
        System.out.print("POWER_OFF");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.SELF_TEST))
        System.out.print("SELF_TEST");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.SHUTDOWN))
        System.out.print("SHUTDOWN");
      else if (xraySourceEventEnum.equals(XraySourceEventEnum.STARTUP))
        System.out.print("STARTUP");
      else
        System.out.print(eventEnum.toString());
    }
    else if (eventEnum instanceof PanelHandlerEventEnum)
    {
      PanelHandlerEventEnum panelHandlerEventEnum = (PanelHandlerEventEnum)eventEnum;
      System.out.print("  event = PanelHandlerEventEnum.");
      if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.CANCEL_LOAD_PANEL))
        System.out.print("CANCEL_LOAD_PANEL");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.DISABLE))
        System.out.print("DISABLE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.ENABLE))
        System.out.print("ENABLE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.HOME_STAGE_RAILS))
        System.out.print("HOME_STAGE_RAILS");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.INITIALIZE))
        System.out.print("INITIALIZE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_ONE_OF_TWO))
        System.out.print("INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_ONE_OF_TWO");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_TWO_OF_TWO))
        System.out.print("INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_TWO_OF_TWO");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.LOAD_PANEL))
        System.out.print("LOAD_PANEL");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.MOVE_PANEL_TO_LEFT_SIDE))
        System.out.print("MOVE_PANEL_TO_LEFT_SIDE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.MOVE_PANEL_TO_RIGHT_SIDE))
        System.out.print("MOVE_PANEL_TO_RIGHT_SIDE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.RESET))
        System.out.print("RESET");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.SET_PANEL_FLOW_DIRECTION))
        System.out.print("SET_PANEL_FLOW_DIRECTION");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.SET_PANEL_LOADING_MODE))
        System.out.print("SET_PANEL_LOADING_MODE");
      else if (panelHandlerEventEnum.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
        System.out.print("UNLOAD_PANEL");
      else
        System.out.print(eventEnum.toString());
    }
    else if (eventEnum instanceof PanelPositionerEventEnum)
    {
      PanelPositionerEventEnum panelPositionerEventEnum = (PanelPositionerEventEnum)eventEnum;
      System.out.print("  event = PanelPositionerEventEnum.");
      if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.DISABLE_ALL_AXES))
        System.out.print("DISABLE_ALL_AXES");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.DISABLE_SCAN_PATH))
        System.out.print("DISABLE_SCAN_PATH");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.DISABLE_X_AXIS))
        System.out.print("DISABLE_X_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.DISABLE_Y_AXIS))
        System.out.print("DISABLE_Y_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.ENABLE_ALL_AXES))
        System.out.print("ENABLE_ALL_AXES");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.ENABLE_SCAN_PATH))
        System.out.print("ENABLE_SCAN_PATH");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.ENABLE_X_AXIS))
        System.out.print("ENABLE_X_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.ENABLE_Y_AXIS))
        System.out.print("ENABLE_Y_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.HOME_ALL_AXES))
        System.out.print("HOME_ALL_AXES");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.HOME_X_AXIS))
        System.out.print("HOME_X_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.HOME_Y_AXIS))
        System.out.print("HOME_Y_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.INITIALIZE))
        System.out.print("INITIALIZE");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.POINT_TO_POINT_MOVE_ALL_AXES))
        System.out.print("POINT_TO_POINT_MOVE_ALL_AXES");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.POINT_TO_POINT_MOVE_X_AXIS))
        System.out.print("POINT_TO_POINT_MOVE_X_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.POINT_TO_POINT_MOVE_Y_AXIS))
        System.out.print("POINT_TO_POINT_MOVE_Y_AXIS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.RUN_NEXT_SCAN_PASS))
        System.out.print("RUN_NEXT_SCAN_PASS");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.SET_POINT_TO_POINT_MOTION_PROFILE))
        System.out.print("SET_POINT_TO_POINT_MOTION_PROFILE");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.SET_POSITION_PULSE))
        System.out.print("SET_POSITION_PULSE");
      else if (panelPositionerEventEnum.equals(PanelPositionerEventEnum.SET_SCAN_MOTION_PROFILE))
        System.out.print("SET_SCAN_MOTION_PROFILE");
      else
        System.out.print(eventEnum.toString());
    }
    else if (eventEnum instanceof com.axi.v810.business.autoCal.CalEventEnum)
    {
      com.axi.v810.business.autoCal.CalEventEnum calEventEnum = (com.axi.v810.business.autoCal.CalEventEnum)eventEnum;
      System.out.print("  event = CalEventEnum.");
      if (calEventEnum.equals(com.axi.v810.business.autoCal.CalEventEnum.ADJUSTING_GRAYSCALE))
        System.out.print("ADJUSTING_GRAYSCALE");
      else if (calEventEnum.equals(com.axi.v810.business.autoCal.CalEventEnum.ADJUSTING_XRAYSPOT))
        System.out.print("ADJUSTING_XRAYSPOT");
      else if (calEventEnum.equals(com.axi.v810.business.autoCal.CalEventEnum.ADJUSTING_HYSTERESIS))
        System.out.print("ADJUSTING_HYSTERESIS");
      else if (calEventEnum.equals(com.axi.v810.business.autoCal.CalEventEnum.ADJUSTING_SYSTEMOFFSETS))
        System.out.print("ADJUSTING_SYSTEMOFFSETS");
      else if (calEventEnum.equals(com.axi.v810.business.autoCal.CalEventEnum.ADJUSTING_MAGNIFICATION))
        System.out.print("ADJUSTING_MAGNIFICATION");
      else
        System.out.print(eventEnum.toString());
    }
    else
    {
      System.out.print("  event = " + eventEnum.toString());
    }
    if (event.isStart() == false)
    {
      System.out.println(" complete");
    }
    else
    {
      System.out.println(" initiated");
    }
  }
}
