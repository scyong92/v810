package com.axi.v810.datastore;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * This class is thrown when a file cannot be closed.
 * @author John Dutton
 */
public class CannotCloseFileDatastoreException extends DatastoreException
{
  /**
   * @author John Dutton
   */
  public CannotCloseFileDatastoreException(String file)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_CLOSE_FILE_KEY", new Object[]
                              {file}));
    Assert.expect(file != null);
  }
}
