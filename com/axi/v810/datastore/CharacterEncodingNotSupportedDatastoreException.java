package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class CharacterEncodingNotSupportedDatastoreException extends DatastoreException
{
  private String _encodingString;

  /**
   * @author Bill Darbie
   */
  public CharacterEncodingNotSupportedDatastoreException(String encodingString)
  {
    super(new LocalizedString("DS_CHARACTER_ENCODING_NOT_SUPPORTED_KEY", new Object[]{encodingString}));
    Assert.expect(encodingString != null);
    _encodingString = encodingString;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    Assert.expect(_encodingString != null);
    return _encodingString;
  }
}
