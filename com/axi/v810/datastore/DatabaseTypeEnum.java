package com.axi.v810.datastore;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Keeps track of the different databases the system uses.
 *
 * @author Matt Wharton
 */
public class DatabaseTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private String _description;

  public static final DatabaseTypeEnum ALGORITHM_LEARNING_DATABASE = new DatabaseTypeEnum(
      ++_index, StringLocalizer.keyToString("DS_ALGORITHM_LEARNING_DATABASE_DESCRIPTION_KEY"));
  public static final DatabaseTypeEnum INSPECTION_RESULTS_DATABASE = new DatabaseTypeEnum(
      ++_index, StringLocalizer.keyToString("DS_INSPECTION_RESULTS_DATABASE_DESCRIPTION_KEY"));
  public static final DatabaseTypeEnum PACKAGE_LIBRARY_DATABASE = new DatabaseTypeEnum(
      ++_index, StringLocalizer.keyToString("DS_PACKAGE_LIBRARY_DATABASE_DESCRIPTION_KEY"));

  /**
   * @author Matt Wharton
   */
  private DatabaseTypeEnum(int id, String description)
  {
    super(id);
    _description = description;
  }

  /**
   * @author Matt Wharton
   */
  public String getDescription()
  {
    Assert.expect(_description != null);

    return _description;
  }
}
