package com.axi.v810.datastore;

import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for controlling performance timing and writing all performance
 * information to the performance log file.
 *
 * @author George Booth
 */
public class PerformanceLogUtil
{
  private static String version = "Version 1.2";

  private static PerformanceLogUtil _performanceLogUtil = null;

  private FileWriterUtilAxi _fileWriter;
  private long _performanceStartTimeInMillis;
  private static Map<String, PerformanceLogMilestoneEnum> _stringToPerformanceLogMilestoneEnum;

  private final NumberFormat _decimalFormat = new DecimalFormat("#0.000");
  private final SimpleDateFormat _headerSimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm:ss", new Locale("en", "US"));

  private static final boolean _DEVELOPER_PERFORMANCE = Config.isDeveloperPerformanceModeOn();

  /**
   * @author George Booth
   */
  private PerformanceLogUtil()
  {
    // make this private so getInstance() must be used
  }

  /**
   * @author George Booth
   */
  public synchronized static PerformanceLogUtil getInstance()
  {
    if (_performanceLogUtil == null)
    {
      _performanceLogUtil = new PerformanceLogUtil();

      checkForDuplicateMilestoneStrings();
    }

    return _performanceLogUtil;
  }

  /**
   * @author George Booth
   */
  public static void checkForDuplicateMilestoneStrings()
  {
    _stringToPerformanceLogMilestoneEnum = new HashMap<String, PerformanceLogMilestoneEnum>();
    Collection<PerformanceLogMilestoneEnum> allTypes = PerformanceLogMilestoneEnum.getAllTypes();
    for (PerformanceLogMilestoneEnum milestoneEnum : allTypes)
    {
      PerformanceLogMilestoneEnum previousEntry = _stringToPerformanceLogMilestoneEnum.put(milestoneEnum.toString(), milestoneEnum);
      Assert.expect(previousEntry == null, "Duplicate PerformanceLogMilestoneEnum string found: " + milestoneEnum.toString());
    }
  }

  /**
   * @author George Booth
   */
  public void openPerformanceLog() throws DatastoreException
  {
    String _performanceLogFileNameFullPath = FileName.getPerformanceLogFileNameFullPath();
    _fileWriter = new FileWriterUtilAxi(_performanceLogFileNameFullPath, true);
    _fileWriter.open();
    // add a header for this open
    Date currentDate = new Date();
    String date = _headerSimpleDateFormat.format(currentDate);
    _fileWriter.writeln(_decimalFormat.format(0.0) + ", ***** Performance log opened " + date + " *****");
    _fileWriter.close();
    // start timer at 0.0
    resetTimer();
  }

  /**
   * Prepend curent elapsed time to the software milestone
   * synchronized to be thread safe
   *
   * @author George Booth
   */
  public synchronized void logMilestone(PerformanceLogMilestoneEnum milestoneEnum)
  {
    Assert.expect(milestoneEnum != null);
    if (_DEVELOPER_PERFORMANCE && _fileWriter != null)
    {
      try
      {
        _fileWriter.append(getElapsedPerformanceTime() + ", " + milestoneEnum.toString());
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }

  /**
   * Prepend curent elapsed time to the software milestone and int parameter
   * synchronized to be thread safe
   *
   * @author George Booth
   */
  public synchronized void logMilestone(PerformanceLogMilestoneEnum milestoneEnum, int value)
  {
    Assert.expect(milestoneEnum != null);
    if (_DEVELOPER_PERFORMANCE && _fileWriter != null)
    {
      try
      {
        _fileWriter.append(getElapsedPerformanceTime() + ", " + milestoneEnum.toString() + ", " + value);
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }

  /**
   * Prepend curent elapsed time to the software milestone and String parameter
   * synchronized to be thread safe
   *
   * @author George Booth
   */
  public synchronized void logMilestone(PerformanceLogMilestoneEnum milestoneEnum, String value)
  {
    Assert.expect(milestoneEnum != null);
    if (_DEVELOPER_PERFORMANCE && _fileWriter != null)
    {
      try
      {
        _fileWriter.append(getElapsedPerformanceTime() + ", " + milestoneEnum.toString() +  value);
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }

  /**
   * Prepend curent elapsed time to the string
   * synchronized to be thread safe
   *
   * @author George Booth
   */
  public synchronized void logEventString(String eventString)
  {
    Assert.expect(eventString != null);
    if (_DEVELOPER_PERFORMANCE && _fileWriter != null)
    {
      try
      {
        _fileWriter.append(getElapsedPerformanceTime() + ", " + eventString);
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }

  /**
   * Resets the performance timer.
   *
   * @author George Booth
   */
  public void resetTimer()
  {
    _performanceStartTimeInMillis = System.currentTimeMillis();
  }

  /**
   * @return the number of milliseconds elapsed since performanceStart()
   *
   * @author George Booth
   */
  private long getElapsedPerformanceTimeInMillis()
  {
    long time = System.currentTimeMillis() - _performanceStartTimeInMillis;
    return time;
  }

  /**
   * Returns a string number of seconds elapsed since performanceStart()
   *
   * @author George Booth
   */
  private String getElapsedPerformanceTime()
  {
    String str = _decimalFormat.format((float)getElapsedPerformanceTimeInMillis() / 1000.0f);

    return str;
  }

}
