package com.axi.v810.datastore;

import java.util.*;
import java.text.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class specifically handles the logging for all SMEMA-related operation.
 * 
 * @author Cheah Lee Herng
 */
public class SmemaLogUtil
{
  private final SimpleDateFormat _headerSimpleDateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm:ss", new Locale("en", "US"));
  
  private static SmemaLogUtil _performanceLogUtil = null;
  
  private FileWriterUtilAxi _smemaLogFileWriter;
  private FileWriterUtilAxi _smemaConveyorSignalLogFileWriter;
  
  /**
   * @author Cheah Lee Herng
   */
  private SmemaLogUtil()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public synchronized static SmemaLogUtil getInstance()
  {
    if (_performanceLogUtil == null)
    {
      _performanceLogUtil = new SmemaLogUtil();     
    }

    return _performanceLogUtil;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void open() throws DatastoreException
  {
    long currentTime = System.currentTimeMillis();
    String smemaLogFileNameFullPath = FileName.getSmemaLogFileNameFullPath(currentTime);
    String smemaConveyorSignalLogFileNameFullPath = FileName.getSmemaConveyorSignalLogFileNameFullPath(currentTime);
    _smemaLogFileWriter = new FileWriterUtilAxi(smemaLogFileNameFullPath, true);
    _smemaLogFileWriter.open();
    // add a header for this open    
    _smemaLogFileWriter.writeln(_headerSimpleDateFormat.format(Calendar.getInstance().getTime()) + ", ***** SMEMA log opened *****");
    _smemaLogFileWriter.close();
    // SMEMA Conveyor Signal log
    _smemaConveyorSignalLogFileWriter = new FileWriterUtilAxi(smemaConveyorSignalLogFileNameFullPath, true);
    _smemaConveyorSignalLogFileWriter.open();
    // add a header for this open    
    _smemaConveyorSignalLogFileWriter.writeln(_headerSimpleDateFormat.format(Calendar.getInstance().getTime()) + ", ***** SMEMA Converyor Signal log opened *****");
    _smemaConveyorSignalLogFileWriter.close();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void close()
  {
    if (isSmemaLogOpen())
    {      
      _smemaLogFileWriter.close();
      _smemaLogFileWriter = null;
    }
    
    if (isSmemaConveyorSignalLogOpen())
    {
      _smemaConveyorSignalLogFileWriter.close();
      _smemaConveyorSignalLogFileWriter = null;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logSmemaMessage(String message) throws DatastoreException
  {
    Assert.expect(message != null);

    if (Config.isDeveloperDebugModeOn() && isInLineModeEnabled())
    {
      if (isSmemaLogOpen() == false)
        open();
      
      try
      {
        _smemaLogFileWriter.append(_headerSimpleDateFormat.format(Calendar.getInstance().getTime()) + ", " + message);
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logSmemaConveyorSignalMessage(String message) throws DatastoreException
  {
    Assert.expect(message != null);

    if (Config.isDeveloperDebugModeOn() && isInLineModeEnabled())
    {
      if (isSmemaConveyorSignalLogOpen() == false)
        open();
      
      try
      {
        _smemaConveyorSignalLogFileWriter.append(_headerSimpleDateFormat.format(Calendar.getInstance().getTime()) + ", " + message);
      }
      catch (DatastoreException dse)
      {
        // do nothing
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logStartWaitUntilPanelFromUpstreamSystemIsAvailable() throws DatastoreException
  {
    logSmemaMessage("Start Wait until panel from upstream system is available");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logCompleteWaitUntilPanelFromUpstreamSystemIsAvailable() throws DatastoreException
  {
    logSmemaMessage("Complete Wait until panel from upstream system is available");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logWaitUntilPanelFromUpstreamSystemIsAvailableTimeOut() throws DatastoreException
  {
    logSmemaMessage("TIMEOUT: Wait until panel from upstream system is available");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logStartWaitUntilDownstreamSystemReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Start Wait until downstream system ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logCompleteWaitUntilDownstreamSystemReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Complete Wait until downstream system ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logWaitUntilDownstreamSystemReadyToAcceptPanelTimeOut() throws DatastoreException
  {
    logSmemaMessage("TIMEOUT: Wait until downstream system ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logStartWaitUntilDownstreamSystemNotReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Start Wait until downstream system not ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logCompleteWaitUntilDownstreamSystemNotReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Complete Wait until downstream system not ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logWaitUntilDownstreamSystemNotReadyToAcceptPanelTimeOut() throws DatastoreException
  {
    logSmemaMessage("TIMEOUT: Wait until downstream system not ready to accept panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logXraySystemReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Xray System Ready to Accept Panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logXraySystemNotReadyToAcceptPanel() throws DatastoreException
  {
    logSmemaMessage("Xray System Not Ready to Accept Panel");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logPanelAvailableFromXrayTester() throws DatastoreException
  {
    logSmemaMessage("Panel available from Xray Tester");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logPanelNotAvailableFromXrayTester() throws DatastoreException
  {
    logSmemaMessage("Panel not available from Xray Tester");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logPanelOutUntested() throws DatastoreException
  {
    logSmemaMessage("Panel out untested");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logPanelOutPassed() throws DatastoreException
  {
    logSmemaMessage("Panel out passed");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void logPanelOutFailed() throws DatastoreException
  {
    logSmemaMessage("Panel out failed");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isSmemaLogOpen()
  {
    return (_smemaLogFileWriter != null);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isSmemaConveyorSignalLogOpen()
  {
    return (_smemaConveyorSignalLogFileWriter != null);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private boolean isInLineModeEnabled()
  {
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_SMEMA_ENABLE);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void logStartWaitUntilDownstreamSensorSignalClear() throws DatastoreException
  {
    logSmemaMessage("Start Wait until downstream sensor one and panel clear sensor clear");
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void logCompleteWaitUntilDownstreamSensorSignalClear() throws DatastoreException
  {
    logSmemaMessage("Complete Wait until downstream sensor one and panel clear sensor clear");
  }
}
