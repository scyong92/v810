package com.axi.v810.datastore;

import com.axi.v810.util.*;
import java.util.*;

/**
 * This class is used to create log for recipe error 
 * @author bee-hoon.goh
 */
public class ProjectErrorLogUtil 
{
  private static ProjectErrorLogUtil _instance;
  private FileLoggerAxi _logUtility;
  private String _logFileNameFullPath;
  private static boolean _isLandPatternNameNotFoundError = false;
  private List <String> _compPackageLongName = new ArrayList<String>();
  
  /**
   * @author Goh Bee Hoon
   */
  private ProjectErrorLogUtil()
  {
    //do nothing
  }

   /**
   * @author Goh Bee Hoon
   */
  public static synchronized ProjectErrorLogUtil getInstance()
  {
    if (_instance == null)
      _instance = new ProjectErrorLogUtil();

    return _instance;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public void log(String projectName, String headerDescription, String data) throws XrayTesterException
  {
    _logFileNameFullPath = FileName.getRecipeErrorLogFileNameFullPath(projectName);
    _logUtility = new FileLoggerAxi(_logFileNameFullPath); 
    _logUtility.append(headerDescription + " " + data);
  } 
  
  /**
   * @author Goh Bee Hoon
   */
  public void setIsLandPatternNameNotFoundError(boolean isLandPatternNameNotFoundError)
  {
      _isLandPatternNameNotFoundError = isLandPatternNameNotFoundError;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public boolean getIsLandPatternNameNotFoundError()
  {
    return _isLandPatternNameNotFoundError;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public void setUnusedCompPackage(List<String> compPackageLongName)
  {
    _compPackageLongName.addAll(compPackageLongName);
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public List<String> getUnusedCompPackage()
  {
    return _compPackageLongName;
  }
}
