package com.axi.v810.datastore;

import java.util.regex.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class InvalidRegularExpressionDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidRegularExpressionDatastoreException(PatternSyntaxException patternException)
  {
    super(new LocalizedString("DS_INVALID_REGULAR_EXPRESSION_EXCEPTION_KEY",
                              new Object[]{patternException.getPattern(),
                                           patternException.getMessage()}));
    Assert.expect(patternException != null);
  }
}
