package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when there is not enough space local hard drive space
 * to download the project.
 *
 * @author George A. David
 */
public class InsufficientSpaceToGetProjectDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectDir the directory that has insufficient disk space
   * @param projectName the name of the project that cannot be added to the repository
   * @author George A. David
   */
  public InsufficientSpaceToGetProjectDatastoreException(String projectDir, String projectName, String repositoryPath)
  {
    super(new LocalizedString("DS_ERROR_INSUFFICIENT_SPACE_TO_GET_PROJECT_KEY", new Object[]{projectDir, projectName, repositoryPath}));
    Assert.expect(projectDir != null);
    Assert.expect(projectName != null);
    Assert.expect(repositoryPath != null);
  }
}
