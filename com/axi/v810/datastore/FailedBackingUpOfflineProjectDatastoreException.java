package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to backup a project offline.
 *
 * @author George A. David
 */
public class FailedBackingUpOfflineProjectDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while backing it up
   * @author George A. David
   */
  public FailedBackingUpOfflineProjectDatastoreException(String projectName, String offlinePath)
  {
    super(new LocalizedString("DS_ERROR_FAILED_BACKING_UP_OFFLINE_PROJECT_KEY", new Object[]{projectName, offlinePath}));
    Assert.expect(projectName != null);
    Assert.expect(offlinePath != null);
  }
}
