package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is cannot be written for some reason.
 * @author Bill Darbie
 */
public class CannotWriteDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param fileName is the name of the file that cannot be written to for some reason
   * @author Bill Darbie
   */
  public CannotWriteDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_WRITE_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
