package com.axi.v810.datastore;

import java.io.*;
import java.text.*;

import com.axi.util.*;

/**
*       This class provides the interface for accessing the configuration
*       file used by the Display Adjustment Utility.
*       The instance of the class is associated with a configuration file.
*       setValue( double ) writes the value to the configuration file.
*       getValue() returns the value previously set by setValue().
*
*       @author Vincent Wong
*/
public class GammaSetting
{
  private String _filename;

  /** It registers the file for storage.
   *
   *  @param filename is the full pathname of the storage file for the setting.
   *  @author Vincent Wong
   */
  public GammaSetting( String filename )
  {
    Assert.expect( filename != null );

    _filename = filename;
  }

  /** Write the specified gamma value to the gamma setting file in the disk.
   *
   *  @param gammaValue is the value intended to be stored.
   *  @author Vincent Wong
   */
  public void setValue( double gammaValue ) throws DatastoreException
  {
    //----------------------------------------
    // Make a backup of the original file just in case something goes wrong
    //
    File original = new File( _filename );
    File backup = null;

    if ( original.exists() )
    {
      if ( original.canWrite() )
      {
        backup = new File( _filename + FileName.getBackupExtension() );
        original.renameTo( backup );
      }
      else
        original.delete(); // delete the read-only file and create a new one
    }

    //----------------------------------------
    // Attempt to overwrite the original file with the new value.
    //
    DataOutputStream output = null;

    try
    {
      output = new DataOutputStream( new FileOutputStream( _filename ) );
      DecimalFormat format = new DecimalFormat( "0.00" );
      output.writeBytes( format.format( gammaValue ) );
    }
    catch (IOException e)
    {
      if (backup != null)
      {
        original.delete(); // remove the file that caused the exception
        backup.renameTo( original ); // revert to the original content
      }

      DatastoreException dex = new DatastoreException(_filename);
      dex.initCause(e);
      throw dex;
    }
    finally
    {
      if ( output != null )
      {
        try
        {
          output.close();
        }
        catch (IOException e)
        {
          DatastoreException dex = new DatastoreException(_filename);
          dex.initCause(e);
          throw dex;
        }
      }

      if ( original.exists() && backup != null ) // kepp at least one copy
        backup.delete();
    }
  }


  /** Read the gamma value from the gamma setting file in the disk.
   *
   *  @return A value stored in the disk.
   *  @author Vincent Wong
   */
  public double getValue() throws FileCorruptDatastoreException, FileNotFoundDatastoreException, DatastoreException
  {
    BufferedReader input = null;
    double gammaValue = 0.; // initialize it to an invalid value

    try
    {
      input = new BufferedReader( new FileReader( _filename ) );
      String line = input.readLine();
      gammaValue = Double.parseDouble( line );
    }
    catch ( NumberFormatException e ) // invalid contents
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_filename);
      dex.initCause(e);
      throw dex; // unable to return a valid value
    }
    catch ( NullPointerException e ) // empty file
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_filename);
      dex.initCause(e);
      throw dex; // unable to return a valid value
    }
    catch ( FileNotFoundException e )
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_filename);
      dex.initCause(e);
      throw dex; // unable to return a valid value
    }
    catch ( IOException e ) // other IOExceptions are real errors
    {
      DatastoreException dex = new DatastoreException(_filename);
      dex.initCause(e);
      throw dex;
    }
    finally
    {
      if ( input != null )
      {
        try
        {
          input.close();
        }
        catch( IOException e )
        {
          DatastoreException dex = new DatastoreException(_filename);
          dex.initCause(e);
          throw dex;
        }
      }
    }

    return gammaValue;
  }

  /** Mark the configuration file to be read-only.
   *
   *  @return True if the operation is successful and false otherwise.
   *  @author Vincent Wong
   */
  public boolean setReadOnly()
  {
    File configFile = new File( _filename );

    return configFile.setReadOnly();
  }
}
