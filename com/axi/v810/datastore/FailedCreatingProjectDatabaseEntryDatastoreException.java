package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to create the repository file.
 *
 * @author George A. David
 */
public class FailedCreatingProjectDatabaseEntryDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while creating the entry
   * @author George A. David
   */
  public FailedCreatingProjectDatabaseEntryDatastoreException(String projectName)
  {
    super(new LocalizedString("DS_ERROR_FAILED_CREATING_PROJECT_DATABASE_ENTRY_KEY", new Object[]{projectName}));
    Assert.expect(projectName != null);
  }
}
