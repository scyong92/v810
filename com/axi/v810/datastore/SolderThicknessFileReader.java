package com.axi.v810.datastore;

import java.io.*;
import java.util.regex.*;

import com.axi.util.*;


/**
 * Reads the data file from disc into memory.
 * @author Eddie Williamson
 */
public class SolderThicknessFileReader
{
  private static SolderThicknessFileReader _instance = null;

  private Pattern _commentPattern = Pattern.compile("^(\\s*)([^#]*)(\\s*)(#.*)*$");
  private float[][] _datatable;
  private float _maxThickness;


  /**
   * @author Eddie Williamson
   */
  private SolderThicknessFileReader()
  {
  }


  /**
   * @author Eddie Williamson
   */
  public static synchronized SolderThicknessFileReader getInstance()
  {
    if (_instance == null)
    {
      _instance = new SolderThicknessFileReader();
    }
    return _instance;
  }
  
  /**
   * XCR-3237 Intermittent software crash when generate precision tuning image
   * verify if the solder thickness file is workable
   * @author Swee Yee Wong
   */
  public synchronized void verifyDataFile(String solderThicknessFilePath, int maxGreyLevel, int numGreyLevels)
      throws DatastoreException
  {
    Assert.expect(solderThicknessFilePath != null);

    LineNumberReader is = null;
    try
    {
      is = ParseUtil.openFile(solderThicknessFilePath);
      
      // The background values from the 5dx are backwards compared to Genesis
      // So we loop from 255 down to 0 to reverse them into the correct order
      for (int background = maxGreyLevel; background >= 0; background--)
      {
        String line = getNextLine(is, solderThicknessFilePath);
        String[] tokens = line.split("\\s+");
        // Minimal check for file format
        if (tokens.length != background + 1)
        {
          FileCorruptDatastoreException dex =
              new FileCorruptDatastoreException(solderThicknessFilePath, is.getLineNumber());
          throw dex;
        }
        try
        {
          // delta grey goes from 0 to background inclusive ( <= )
          for (int deltagrey = 0; deltagrey <= background; deltagrey++)
          {
            float value = new Float(tokens[deltagrey]).floatValue();
          }
        }
        catch(NumberFormatException ne)
        {
          FileCorruptDatastoreException dex =
              new FileCorruptDatastoreException(solderThicknessFilePath, is.getLineNumber());
          throw dex;
        }
      }
    }
    finally
    {
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ioe)
        {
          is = null;
          DatastoreException dex = new DatastoreException(solderThicknessFilePath);
          dex.initCause(ioe);
          throw dex;
        }
      }
    }
  }
  
    /**
   * Reads in the solder thickness table from the calib folder.
   * @author Eddie Williamson
   * @author Swee Yee Wong
   * @param solderThicknessFilePath Identifies which data table file to read
   * @param maxGreyLevel Maximum grey level allowed
   * @param numGreyLevels Number of grey levels expected in the data. Used to size the storage array.
   */
  public synchronized void readDataFile(String solderThicknessFilePath, int maxGreyLevel, int numGreyLevels)
      throws DatastoreException
  {
    Assert.expect(solderThicknessFilePath != null);
    
    LineNumberReader is = null;
    _maxThickness = 0.0F;
    float value;

    // Create storage for data, this is a triangular matrix
    _datatable = new float[numGreyLevels][];
    for (int background = maxGreyLevel; background >= 0; background--)
    {
      // to save memory allocate just enough room for each row
      _datatable[background] = new float[background + 1];
    }


    try
    {
      is = ParseUtil.openFile(solderThicknessFilePath);
      
      // The background values from the 5dx are backwards compared to Genesis
      // So we loop from 255 down to 0 to reverse them into the correct order
      for (int background = maxGreyLevel; background >= 0; background--)
      {
        String line = getNextLine(is, solderThicknessFilePath);
        String[] tokens = line.split("\\s+");
        // Minimal check for file format
        if (tokens.length != background + 1)
        {
          FileCorruptDatastoreException dex =
              new FileCorruptDatastoreException(solderThicknessFilePath, is.getLineNumber());
          throw dex;
        }
        // delta grey goes from 0 to background inclusive ( <= )
        for (int deltagrey = 0; deltagrey <= background; deltagrey++)
        {
          value = new Float(tokens[deltagrey]).floatValue();
          _datatable[background][deltagrey] = value;
          if (value > _maxThickness)
          {
            _maxThickness = value;
          }
        }
      }
    }
    finally
    {
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ioe)
        {
          is = null;
          DatastoreException dex = new DatastoreException(solderThicknessFilePath);
          dex.initCause(ioe);
          throw dex;
        }
      }
    }
  }

  /**
   * Get next line from the data file and clean it up.
   * Comments ('#' character to end of line) are skipped. Spaces at
   * beginning and end of line are removed. Skips blank lines.
   * @param pathToFile Full path to file. Only used if exception during file read.
   * @author Eddie Williamson
   */
  private synchronized String getNextLine(LineNumberReader is, String pathToFile)
      throws DatastoreException
  {
    String line = null;

    do
    {
      line = ParseUtil.readNextLine(is, pathToFile);
      
      //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
      if(line == null)
      {
        throw new FileCorruptDatastoreException(pathToFile, is.getLineNumber());
      }

      Matcher commentMatcher = _commentPattern.matcher(line);
      if (commentMatcher.matches() == false)
      {
        throw new FileCorruptDatastoreException(pathToFile, is.getLineNumber());
      }
      else
      {
        // get rid of any comments
        line = commentMatcher.group(2);
        // get rid of leading and trailing spaces
        line = line.trim();
      }
    }
    while (line.equals(""));

    return line;
  }


  /**
   * @author Eddie Williamson
   */
  public float[][] getDataTable()
  {
    return _datatable;
  }


  /**
   * Returns the maximum thickness found in the data file.
   * @author Eddie Williamson
   */
  public float getMaxThickness()
  {
    return _maxThickness;
  }
}
