/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author siew-yeng.phang
 */
public class AlgSliceNdfWriter
{
  private static final String _DEFINE_PIN_1_AS_WHOLE_DEVICE_TOKEN = ".DEFINE_PIN_1_AS_WHOLE_DEVICE:";
  private static final String _SLICE_ORDER_TOKEN = ".SLICE_ORDER:";
  
  private static AlgSliceNdfWriter _instance;
  private NdfWriter _ndfWriter;
  private Project _project;
  private Map<Subtype,Integer> _subtypeToSubtypeNoMap = new HashMap<Subtype,Integer>();

  /**
   * @author Bill
   */
  static synchronized AlgSliceNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new AlgSliceNdfWriter();

    return _instance;
  }
  
  /**
   * @author Bill Darbie
   */
  private AlgSliceNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(directory != null);
    
    _project = project;
    _ndfWriter = NdfWriter.getInstance();
    
    String fileName = directory + File.separator + FileName.getAlgSliceNdfFile();
    PrintWriter os = null;

    try
    {
      os = new PrintWriter(fileName);
      String algoFamily = null;
      
      for(JointTypeEnum jointTypeEnum : project.getPanel().getJointTypeEnums())
      {
        algoFamily = _ndfWriter.get5dxAlgorithmFamilyString(jointTypeEnum);
 
        if(algoFamily.equals("Res")||
          algoFamily.equals("Chip")||
          algoFamily.equals("Pcap")||
          algoFamily.equals("MelfRes")||
          algoFamily.equals("MelfCap")||
          algoFamily.equals("Misc"))
        {
          os.println(_DEFINE_PIN_1_AS_WHOLE_DEVICE_TOKEN+" "+algoFamily);
        }
      }
      os.println("");
      os.println("#SLICE ORDER: algorithmFamily subtype sliceHeight(s)");
      os.println("#@algorithmFamily subtype algorithmnName sliceNo");
      
      for(JointTypeEnum jointTypeEnum : project.getPanel().getJointTypeEnums())
      {
        algoFamily = _ndfWriter.get5dxAlgorithmFamilyString(jointTypeEnum);
        List<Subtype> subtypes = project.getPanel().getSubtypes(jointTypeEnum);
        
        //SLICE ORDER: algorithmFamily subtypeNo sliceHeight(s)
        int subtypeNo = 0;
        _subtypeToSubtypeNoMap = new HashMap<Subtype,Integer>();
           
        for(Subtype subtype : subtypes)
        {
          int sliceNo = 0;
          List<SliceNameEnum> slices = getSlices(jointTypeEnum, subtype);
          _subtypeToSubtypeNoMap.put(subtype, subtypeNo);
          
          os.print(_SLICE_ORDER_TOKEN+" "+ algoFamily+" "+subtypeNo); 
          for(SliceNameEnum slice : slices)
          {
            float sliceValue = 0;
            float sliceValueInMM = 0;
            String referencePoint = "TOP";
            
            if(slice == SliceNameEnum.THROUGHHOLE_BARREL)
            {
              UserDefinedSliceheights.changeSliceHeightUnit(false);//barrel in percent
              sliceValue = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT);    
              sliceNo++;
              
              os.print(" Barrel@"+sliceValue);
              continue;
            }
            else if(slice == SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE)
            {
              referencePoint = "BTM";
              UserDefinedSliceheights.changeSliceHeightUnit(true);
              sliceValueInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS);
              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
              sliceNo++;
            }
            else if(slice == SliceNameEnum.THROUGHHOLE_PIN_SIDE)
            {
              referencePoint = "BTM";
              UserDefinedSliceheights.changeSliceHeightUnit(true);
              sliceValueInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS);
              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
              sliceNo++;
            }
            else if(slice == SliceNameEnum.PAD)
            {
              sliceValueInMM = (Float) subtype.getAlgorithmSettingValue(UserDefinedSliceheights.getAlgoSettingsEnum(slice));
              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
              if(jointTypeEnum == JointTypeEnum.COLLAPSABLE_BGA)
              {
                //set a default slice value +5
                sliceValue = 5;
              }
              sliceNo++;
            }
            else if(slice == SliceNameEnum.MIDBALL)
            {
//              sliceValueInMM = (Float) subtype.getAlgorithmSettingValue(UserDefinedSliceheights.getAlgoSettingsEnum(slice));
//              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
               //set a default slice value 0
              sliceValue = 0;
              sliceNo++;
            }
            else if(slice == SliceNameEnum.PACKAGE)
            {
//              sliceValueInMM = (Float) subtype.getAlgorithmSettingValue(UserDefinedSliceheights.getAlgoSettingsEnum(slice));
//              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
               //set a default slice value -5
              sliceValue = -5;
              sliceNo++;
            }
            else
            {
              sliceValueInMM = (Float) subtype.getAlgorithmSettingValue(UserDefinedSliceheights.getAlgoSettingsEnum(slice));
              sliceValue = Math.round(MathUtil.convertMillimetersToMils(sliceValueInMM));
              sliceNo++;
            }

            if(sliceValue==0)
              os.print(" "+referencePoint);
            else if(sliceValue>0)
              os.print(" "+referencePoint+"+"+(int)sliceValue);
            else
              os.print(" "+referencePoint+(int)sliceValue);
          }
          
          os.println("");
          for(Algorithm algorithm :subtype.getEnabledAlgorithms())
          {
            Collection<AlgorithmSettingEnum> settings = algorithm.getAlgorithmSettingEnums();
            if(algorithm.getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT))
              continue;
            
            String algoName = algorithm.getName();
            os.print("@" + algoFamily + " " + subtypeNo + " " + algoFamily + "_" + algoName);
            
            for(int i = 1; i<=sliceNo;i++)
            {
              os.print(" "+i);
            }
            os.println("");
          }
          subtypeNo++;
        }
        _ndfWriter.addJointTypeToSubtypeToSubtypeNoMap(jointTypeEnum, _subtypeToSubtypeNoMap);
              
        os.println("");
      }
      os.println("");
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
  
  private List<SliceNameEnum> getSlices(JointTypeEnum jointTypeEnum, Subtype subtype)
  {
    InspectionFamily inspectionFamily = InspectionFamily.getInstance(jointTypeEnum);
    List<SliceNameEnum> slices = (List)inspectionFamily.getOrderedInspectionSlices(subtype);
    List<SliceNameEnum> arrangedSlices = new LinkedList<SliceNameEnum>();
    if(jointTypeEnum == JointTypeEnum.COLLAPSABLE_BGA)
    {
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.PAD)));
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.MIDBALL)));
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.PACKAGE)));
      return arrangedSlices;
    }
    else if(jointTypeEnum == JointTypeEnum.CAPACITOR)
    {
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.OPAQUE_CHIP_PAD)));
      return arrangedSlices;
    }
    else if(jointTypeEnum == JointTypeEnum.RESISTOR)
    {
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.CLEAR_CHIP_PAD)));
      return arrangedSlices;
    }
    else if(jointTypeEnum == JointTypeEnum.THROUGH_HOLE || jointTypeEnum == JointTypeEnum.OVAL_THROUGH_HOLE) //Siew Yeng - XCR-3318 - Oval PTH
    {
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.THROUGHHOLE_PIN_SIDE)));
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE)));
      arrangedSlices.add(slices.get(slices.indexOf(SliceNameEnum.THROUGHHOLE_BARREL)));
      return arrangedSlices;
    }
    
    return slices;
  }
}
