/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author siew-yeng.phang
 */
public class FovDataNdfWriter
{
  private static final String _UNIT_TOKEN = ".UNIT:";
  private static final String _FOV_TOKEN = ".FOV:";
  public static final int _FOV_400 = 400;
  public static final int _FOV_650 = 650;
  public static final int _FOV_800 = 800;
  public static final int _SNAP_MARGIN = 10;
  public static final int _IMAGE_RESOLUTION = 1024;
  private static FovDataNdfWriter _instance;

  /**
   * @author Bill
   */
  static synchronized FovDataNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new FovDataNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private FovDataNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  void write(String projectName, String directory) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(directory != null);

    String fileName = directory + File.separator + FileName.getFovDataNdfFile();
    PrintWriter os = null;

    try
    {
      //write an empty file
      os = new PrintWriter(fileName);
      os.println(_UNIT_TOKEN+" "+MathUtilEnum.MILS.toString());
      os.println("");
      os.println("#.FOV: <pitch> <FOV> <snap margin> <image resolution> <snap application>");
      os.println(_FOV_TOKEN+" 16.0 "+_FOV_400+" "+_SNAP_MARGIN+" "+_IMAGE_RESOLUTION+" ADD");
      os.println(_FOV_TOKEN+" 16.1 "+_FOV_650+" "+_SNAP_MARGIN+" "+_IMAGE_RESOLUTION+" ADD");
      os.println(_FOV_TOKEN+" 22.0 "+_FOV_650+" "+_SNAP_MARGIN+" "+_IMAGE_RESOLUTION+" ADD");
      os.println(_FOV_TOKEN+" 22.1 "+_FOV_800+" "+_SNAP_MARGIN+" "+_IMAGE_RESOLUTION+" ADD");
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
