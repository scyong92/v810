/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.text.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Write landpad.ndf
 * @author siew-yeng.phang
 */
class SurfaceMountLandPatternNdfWriter
{
  private static final String _UNIT_TOKEN = ".UNIT:";

  private static SurfaceMountLandPatternNdfWriter _instance;
  
  private final NumberFormat _decimalFormat = new DecimalFormat("#0.0000");

  /**
   * @author Bill
   */
  static synchronized SurfaceMountLandPatternNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new SurfaceMountLandPatternNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SurfaceMountLandPatternNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Bill Darbie
   * @author Siew Yeng, Phang
   */
  void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(directory != null);
    
    String fileName = directory + File.separator + FileName.getSurfaceMountLandPatternNdfFile();

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      //.UNIT
      os.println(_UNIT_TOKEN + " " + MathUtilEnum.MILS.toString());
      os.println("");

      //# @footPrintName padNumber X Y shape padXsize padYsize
      os.println("# @footPrintName padNumber X Y shape padXsize padYsize");
      os.println("");
      for (LandPattern landPattern : project.getPanel().getAllLandPatterns())
      {
        boolean printDivider = false;
        String landPatternName = landPattern.getName();
        for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
        {
          if (landPatternPad.isSurfaceMountPad())
          {
            String padName = landPatternPad.getName();
            ComponentCoordinate coord = landPatternPad.getCoordinateInNanoMeters();
            double xInMils = MathUtil.convertNanoMetersToMils(coord.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(coord.getY());
            double widthInMils = MathUtil.convertNanoMetersToMils(landPatternPad.getWidthInNanoMeters());
            double lengthInMils = MathUtil.convertNanoMetersToMils(landPatternPad.getLengthInNanoMeters());
            
            String shape = "R";
            ShapeEnum shapeEnum = landPatternPad.getShapeEnum();
            if (shapeEnum.equals(ShapeEnum.CIRCLE))
              shape = "C";

            // padNameOrNumber2 xLocation yLocation width length rectangle|circle rotationInDegrees surfaceMount|throughHole holeXLocation holeYLocation holeDiameter");
            os.println("@" + landPatternName + "  " + padName + " " + _decimalFormat.format(xInMils) + " " + 
              _decimalFormat.format(yInMils) + " " + shape + " " + _decimalFormat.format(widthInMils) + " " + 
              _decimalFormat.format(lengthInMils));
            printDivider = true;
          }
          else
            continue;
        }
        if (printDivider)
          os.println("#");
      }

      //Write board fiducial if exist
      for(BoardType boardType : project.getPanel().getBoardTypes())
      { 
        if(boardType.sideBoardType1Exists())
        {
          if(!boardType.getSideBoardType1().getFiducialTypes().isEmpty())
          {
            String landPatternName = "fiducial1";
            String padName = "1"; // 1 for fiducial on sideBoardType1 ; 2 for fiducial on sideBoardType2
            FiducialType fiducialType = boardType.getSideBoardType1().getFiducialTypes().get(0);
            double xInMils = 0;//default value
            double yInMils = 0;//default value
            double widthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getWidthInNanoMeters());
            double lengthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getLengthInNanoMeters());
            ShapeEnum shapeEnum = fiducialType.getShapeEnum();
            String shape = "C";
            if (shapeEnum.equals(ShapeEnum.RECTANGLE))
              shape = "R";

            os.println("@" + landPatternName + " " + padName + " " + _decimalFormat.format(xInMils) + " " + 
              _decimalFormat.format(yInMils) + " " + shape + " " + _decimalFormat.format(widthInMils) + " " + 
              _decimalFormat.format(lengthInMils));
          }   
        }
        if(boardType.sideBoardType2Exists())
        {
          if(!boardType.getSideBoardType2().getFiducialTypes().isEmpty())
          {
            String landPatternName = "fiducial2";
            String padName = "2"; // 1 for fiducial on sideBoardType1 ; 2 for fiducial on sideBoardType2
            FiducialType fiducialType = boardType.getSideBoardType2().getFiducialTypes().get(0);
            double xInMils = 0;//default value
            double yInMils = 0;//default value
            double widthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getWidthInNanoMeters());
            double lengthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getLengthInNanoMeters());
            ShapeEnum shapeEnum = fiducialType.getShapeEnum();
            String shape = "C";
            if (shapeEnum.equals(ShapeEnum.RECTANGLE))
              shape = "R";

            os.println("@" + landPatternName + " " + padName + " " + _decimalFormat.format(xInMils) + " " + 
              _decimalFormat.format(yInMils) + " " + shape + " " + _decimalFormat.format(widthInMils) + " " + 
              _decimalFormat.format(lengthInMils));
          }   
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
