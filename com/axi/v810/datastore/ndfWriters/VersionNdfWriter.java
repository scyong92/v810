/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author siew-yeng.phang
 */
public class VersionNdfWriter
{
  private static VersionNdfWriter _instance;

  /**
   * @author Bill
   */
  static synchronized VersionNdfWriter getInstance()
  {
    if (_instance == null)
    {
      _instance = new VersionNdfWriter();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private VersionNdfWriter()
  {
    // do nothing
  }

  /**
   * @author Siew Yeng, Phang
   */
  void write(String projectName, String directory) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(directory != null);

    String fileName = directory + File.separator + FileName.getVersionNdfFile();
    String version = "01.00";
    PrintWriter os = null;

    try
    {
      os = new PrintWriter(fileName);
      os.println("[VERSION]");
      os.println("Version=" + version);
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
