package com.axi.v810.datastore.ndfWriters;

import java.util.*;
import java.io.File;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.util.*;

/**
 * This class will write a project out as NDF.
 * @author siew-yeng.phang
 */
public class NdfWriter
{ 
  private PanelNdfWriter _panelNdfWriter;
  private ThroughHoleLandPatternNdfWriter _throughHoleLandPatternNdfWriter;
  private SurfaceMountLandPatternNdfWriter _surfaceMountLandPatternNdfWriter;
//  private PackageNdfWriter _packageNdfWriter;
//  private AlgSliceNdfWriter _algSliceNdfWriter;
//  private VersionNdfWriter _versionNdfWriter;
//  private FovDataNdfWriter _fovDataNdfWriter;
  private BoardNdfWriter _boardNdfWriter;
  private ComponentNdfWriter _componentNdfWriter;
  private PanelNameFileWriter _panelNameFileWriter;

  private static NdfWriter _instance;
  private Map<JointTypeEnum, String> _jointTypeEnumTo5dxAlgorithmFamilyStringMap = new HashMap<JointTypeEnum, String>();
  private Map<JointTypeEnum,Map<Subtype,Integer>> _jointTypeToSubtypeToSubtypeNoMapMap = new HashMap<JointTypeEnum,Map<Subtype,Integer>>();
  
  /**
   * @author Bill Darbie
   */
  public static synchronized NdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new NdfWriter();

    return _instance;
  }
  
  /**
   * @author Bill Darbie
   */
  private NdfWriter()
  {
    enumString();
    
    _panelNdfWriter = PanelNdfWriter.getInstance();
    _throughHoleLandPatternNdfWriter = ThroughHoleLandPatternNdfWriter.getInstance();
    _surfaceMountLandPatternNdfWriter = SurfaceMountLandPatternNdfWriter.getInstance();
    
    //Jeremy's advice:
    //It would be better that try not to generate the package.ndf or do subtyping because 
    //there are many rules to follow.  Just create the necessary files required for TestLink 
    //and let the customer build the program from there.  For example, you can assign different 
    //joint types to pins in the V810 but the 5DX doesn't accept that and you have to 'split' the 
    //package and land pattern information and do the subtyping correctly as well as generate the 
    //splitComponentMappings.xml file.  To do this programmatically or by hand requires a large 
    //amount of effort and is better taken care of by letting the user use TestLink to perform these steps. 
    
//    _packageNdfWriter = PackageNdfWriter.getInstance();
//    _versionNdfWriter = VersionNdfWriter.getInstance();
//    _fovDataNdfWriter = FovDataNdfWriter.getInstance();
//    _algSliceNdfWriter = AlgSliceNdfWriter.getInstance();
    _boardNdfWriter = BoardNdfWriter.getInstance();
    _componentNdfWriter = ComponentNdfWriter.getInstance();
    _panelNameFileWriter = PanelNameFileWriter.getInstance();
  }

  /**
   * @author Siew Yeng, Phang
   */
  public void exportProjectToNdf(final String projName, final String fileDestination) throws DatastoreException
  {
    Assert.expect(projName != null);
    Assert.expect(fileDestination != null);

    Project project = null;
    
    try
    {
      ProjectObservable.getInstance().setEnabled(false);
      
      //Siew Yeng - XCR-3524 - System crash when switch tab after export ndf
      if(Project.isCurrentProjectLoaded() && Project.getCurrentlyLoadedProject().getName().equalsIgnoreCase(projName))
      {
        project = Project.getCurrentlyLoadedProject();
      }
      else
        project = ProjectReader.getInstance().load(projName);
      
      String dirName = fileDestination + File.separator + HashUtil.hashName(projName);

      // create the directory if it does not exist
      if (FileUtilAxi.exists(dirName) == false)
      {
        FileUtilAxi.mkdirs(dirName);
      }

      System.out.println("Start writing ndf for project: "+projName);
      _panelNdfWriter.write(project, dirName);
      _throughHoleLandPatternNdfWriter.write(project, dirName);
      _surfaceMountLandPatternNdfWriter.write(project, dirName);
//      _packageNdfWriter.write(project, dirName);
//      _algSliceNdfWriter.write(project, dirName);
//      _versionNdfWriter.write(projName, dirName);
//      _fovDataNdfWriter.write(projName, dirName);
      _boardNdfWriter.write(project, dirName);
      _componentNdfWriter.write(project, dirName);
      _panelNameFileWriter.writePanelNameFile(projName,dirName);
      System.out.println("Complete export!");
      
    }
    catch(DatastoreException ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      project = null;
      ProjectObservable.getInstance().setEnabled(true);
    }
    
    MemoryUtil.garbageCollect();
  }
  
  private void enumString()
  {
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.COLLAPSABLE_BGA, "BGA2");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.NON_COLLAPSABLE_BGA, "BGA2");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.CGA, "CGA");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.CAPACITOR, "Cap");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, "Connector");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.GULLWING, "FPGullwing");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.JLEAD, "Jlead");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.SINGLE_PAD, "Paste");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.POLARIZED_CAP, "PCap");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.PRESSFIT, "PressFitConnector");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.THROUGH_HOLE, "PTH2");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.OVAL_THROUGH_HOLE, "PTH2"); //Siew Yeng - XCR-3318 - Oval PTH
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.RESISTOR, "Res");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.RF_SHIELD, "Connector");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.LEADLESS, "Gullwing");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.SMALL_OUTLINE_LEADLESS, "SOT");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, "FPGullwing");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.CHIP_SCALE_PACKAGE, "BGA2");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, "BGA2");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.EXPOSED_PAD, "Paste");
    mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum.LGA, "Paste");
  }
  /**
   * Maps the specified JointTypeEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate JointTypeEnums.
   *
   * @author Matt Wharton
   */
  private void mapJointTypeEnumTo5dxAlgorithmFamilyString(JointTypeEnum jointTypeEnum, String string)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(string != null);
    Assert.expect(_jointTypeEnumTo5dxAlgorithmFamilyStringMap != null);

    String previousEntry = _jointTypeEnumTo5dxAlgorithmFamilyStringMap.put(jointTypeEnum, string);
    Assert.expect(previousEntry == null);
  }
  
  /**
   * @author Bill Darbie
   */
  public String get5dxAlgorithmFamilyString(JointTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _jointTypeEnumTo5dxAlgorithmFamilyStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }
  
  public void addJointTypeToSubtypeToSubtypeNoMap(JointTypeEnum jointTypeEnum, Map<Subtype,Integer> subtypeToSubtypeNoMap)
  {
    Assert.expect(subtypeToSubtypeNoMap != null);
    _jointTypeToSubtypeToSubtypeNoMapMap.put(jointTypeEnum, subtypeToSubtypeNoMap);
//    int i=0;
//    for(JointTypeEnum theEnum: _jointTypeToSubtypeToSubtypeNoMapMap.keySet())
//    {
//      Map<Subtype,Integer> temp = _jointTypeToSubtypeToSubtypeNoMapMap.get(theEnum);
//      System.out.println(theEnum.getName()+" value size "+i+": "+temp.size());
//      i++;
//    }
//    System.out.println("");
  }
  
  public int getSubtypeNo(JointTypeEnum jointTypeEnum,Subtype subtype)
  {
    Assert.expect(_jointTypeToSubtypeToSubtypeNoMapMap != null);
    Assert.expect(subtype != null);
    
    Map<Subtype,Integer> temp = _jointTypeToSubtypeToSubtypeNoMapMap.get(jointTypeEnum);
    
    return temp.get(subtype);
  }
  
  public static void main(String[] args)
  {
    // Setup the assert logging.
    Assert.setLogFile("ndfRevertersInternalErrors", Version.getVersion());
    
    NdfWriter ndfWriter = new NdfWriter();
    String filePath = Directory.getLegacyNdfDir();
    try
    {
      Config.getInstance().loadIfNecessary();
      ndfWriter.exportProjectToNdf("Xbox1_revert",filePath); 
      ndfWriter.exportProjectToNdf("NEPCON_WIDE_EW_revert",filePath); 
      ndfWriter.exportProjectToNdf("API_4UP_revert",filePath);  
      ndfWriter.exportProjectToNdf("Single_BD_Split_revert",filePath);
      ndfWriter.exportProjectToNdf("TwoBoardPanelTest_revert",filePath); 
      ndfWriter.exportProjectToNdf("DIGBY_PANALIGN_revert",filePath); 
      ndfWriter.exportProjectToNdf("AGILENT-QUAD-APG-R1_revert",filePath); 
      ndfWriter.exportProjectToNdf("73-5051-03_PW_REV01_Tuned_V810_revert",filePath); 
      ndfWriter.exportProjectToNdf("73-10334-03_S8_PWEX_REV01_revert",filePath);
    }
    catch(DatastoreException ex)
    {
      ex.printStackTrace();
    }
  }
}
