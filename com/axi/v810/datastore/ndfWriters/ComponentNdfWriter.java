/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * write component.ndf
 * @author Siew Yeng, Phang
 */
public class ComponentNdfWriter
{
  private static ComponentNdfWriter _instance;
  
  private NdfWriter _ndfWriter;

  /**
   * @author Bill
   */
  static synchronized ComponentNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new ComponentNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ComponentNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  public void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project!=null);
    Assert.expect(directory!=null);
    
    _ndfWriter = NdfWriter.getInstance();
    
    List<BoardType> boardTypes = project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      if(boardTypes.size()>1)
        writeComponentNdf(directory, project, boardType, true);  
      else
        writeComponentNdf(directory, project, boardType, false);
    }
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  void writeComponentNdf(String directory, Project project, BoardType boardType, boolean isMultipleBoardType) throws DatastoreException
  {
    Assert.expect(project!=null);
    Assert.expect(directory!=null);
    Assert.expect(boardType!=null);
    
    String boardTypeName = boardType.getName();
    String sideBoardName = null;
    List<ComponentType> componentTypes =null;
    List<FiducialType> fiducialTypes = null;
    for(int i=0;i<2;i++)
    {
      if(i==0)
      {
        if(boardType.sideBoardType1Exists())
        {
          componentTypes = boardType.getSideBoardType1().getComponentTypes();
          if(isMultipleBoardType)
            sideBoardName = "TOP_A"+boardTypeName.charAt(boardTypeName.length()-1);
          else
            sideBoardName = "TOP";
          
          //board fiducials
          fiducialTypes = boardType.getSideBoardType1().getFiducialTypes();
        }
        else
          continue;
      }
      else
      {
        if(boardType.sideBoardType2Exists())
        {
          componentTypes = boardType.getSideBoardType2().getComponentTypes();
          if(isMultipleBoardType)
            sideBoardName = "BOTTOM_B"+boardTypeName.charAt(boardTypeName.length()-1);
          else
            sideBoardName = "BOTTOM";
          
          //board fiducials
          fiducialTypes = boardType.getSideBoardType2().getFiducialTypes();
        }
        else
          continue;
      }
      
      String dirName = directory + File.separator + HashUtil.hashName(sideBoardName);

      // create the directory if it does not exist
      if (FileUtilAxi.exists(dirName) == false)
      {
        FileUtilAxi.mkdirs(dirName); 
      }

      String fileName = dirName + File.separator + FileName.getComponentNdfFile();

      PrintWriter os = null;

      try
      {
        os = new PrintWriter(fileName);
      
        //@<ref des> <x location> <y location> <orientation> <footprint>
        os.println("#refdes  testStatus  packageName  SM/TH  SubtypeNumber");

        for (ComponentType componentType : componentTypes)
        {
          String refDes = componentType.getReferenceDesignator();
          String testStatus = "TRUE";
          if(componentType.isLoaded())
          {
            if(componentType.getComponentTypeSettings().areAllPadTypesNotTestable())
              testStatus="PARTIAL";  
          }
          else
            testStatus = "FALSE";

          CompPackage compPackage = componentType.getCompPackage();
          PadTypeEnum landPatternType = componentType.getPadTypeEnum();
          String landPatternTypeName = "TH";
          if(landPatternType.equals(PadTypeEnum.SURFACE_MOUNT))
            landPatternTypeName = "SM";
          String compPackageLongName = compPackage.getLongName();
          
          Subtype subtype = null;
          if(componentType.getComponentTypeSettings().usesOneSubtype())
            subtype = componentType.getComponentTypeSettings().getSubtype();
          else
            subtype = componentType.getPadTypeOne().getSubtype();
          
          //int subtypeNo = _ndfWriter.getSubtypeNo(compPackage.getJointTypeEnum(),subtype); 
          //os.println("@" + refDes + " " + testStatus + " " + compPackageLongName + " " + landPatternTypeName+" "+subtypeNo);
          os.println("@" + refDes + " " + testStatus + " " + compPackageLongName + " " + landPatternTypeName);
        }     
        
        //Write board fiducials if exist
        String compPackageName = "fiducial1";
        
        if(sideBoardName.startsWith("BOTTOM"))
          compPackageName = "fiducial2";
        
        for (FiducialType fiducialType : fiducialTypes)
        {
          String name = fiducialType.getName();
          String testStatus = "TRUE"; //default value
          String landPatternTypeName = "SM"; //default value     

          os.println("@" + name + " " + testStatus + " " + compPackageName + " " + landPatternTypeName);
        }
      }
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }
  }
}
