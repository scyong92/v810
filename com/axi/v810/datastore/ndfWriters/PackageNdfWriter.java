/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PackageNdfWriter
{
  private static final String _UNIT_TOKEN = ".UNIT:";
  private static PackageNdfWriter _instance;
  private static NdfWriter _ndfWriter;
  
  /**
   * @author Bill
   */
  static synchronized PackageNdfWriter getInstance()
  {
    if (_instance == null)
    {
      _instance = new PackageNdfWriter();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PackageNdfWriter()
  {
    
  }
  
  /**
   * @author Bill Darbie
   * @author Siew Yeng, Phang
   */
  void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(directory != null);
    
    _ndfWriter = NdfWriter.getInstance();

    String fileName = directory + File.separator + FileName.getPackageNdfFile();

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      //.UNIT
      os.println(_UNIT_TOKEN + " " + MathUtilEnum.MILS.toString());
      os.println("");

      //# @footPrintName padNumber X Y shape padXsize padYsize
      os.println("# @packageName pitch composition algoFamily pin X Y shape pinXsize pinYsize rotation");
      os.println("");
      for (CompPackage compPackage : project.getPanel().getCompPackages())
      {
        boolean printDivider = false;
        String compPackageName = compPackage.getLongName();
        
        for (PackagePin packagePin : compPackage.getPackagePins())
        {
          int interPadDistance = packagePin.getInterPadDistanceInNanoMeters();
          int padWidth = packagePin.getLandPatternPad().getWidthInNanoMeters();
          int padLength = packagePin.getLandPatternPad().getLengthInNanoMeters();
          double pinWidth = padWidth * 0.8;
          double pinLength = padLength * 0.2;
          double defaultPitch = 1.25 * padWidth;
          double pitchFromFormula = interPadDistance + padWidth; //pitch for 5dx = interpad distance + pad width
          double pitchInNanometers = 0;  
          
          if(packagePin.getJointTypeEnum()== JointTypeEnum.CAPACITOR||
            packagePin.getJointTypeEnum()== JointTypeEnum.POLARIZED_CAP||
            packagePin.getJointTypeEnum()== JointTypeEnum.RESISTOR)
//            packagePin.getJointTypeEnum()== JointTypeEnum.LEADLESS||)
//            packagePin.getJointTypeEnum()== JointTypeEnum.SINGLE_PAD)
          {
            pitchInNanometers = 1.25 * padWidth;
            
            if(packagePin.getPinOrientationEnum() == PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN||
               packagePin.getPinOrientationEnum() == PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN)
            {
              pitchInNanometers = 1.25 * padLength;
              pinWidth = padWidth * 0.2;
              pinLength = padLength * 0.8;
            }
          }
          else
          {
            if(packagePin.getPinOrientationEnum() == PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN||
              packagePin.getPinOrientationEnum() == PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN)
            {
              pitchFromFormula = interPadDistance + padLength;
              defaultPitch = 1.25 * padLength;
              
              pinWidth = padWidth * 0.2;
              pinLength = padLength * 0.8;
            }
            pitchInNanometers = Math.min(defaultPitch, pitchFromFormula);
          }
          if(interPadDistance==0)
            pitchInNanometers = defaultPitch;
          
          Assert.expect(pitchInNanometers!=0);
          
          double pitchInMils = MathUtil.roundToPlaces(MathUtil.convertNanoMetersToMils(pitchInNanometers),2);
          String composition = "CERAMIC"; //this field is ignore in NDF
          JointTypeEnum jointTypeEnum = packagePin.getJointTypeEnum();
          String algoFamily = _ndfWriter.get5dxAlgorithmFamilyString(jointTypeEnum);
          String pinName = packagePin.getName();
          ComponentCoordinate coord = packagePin.getLandPatternPad().getCoordinateInNanoMeters();
          double xInMils = MathUtil.roundToPlaces(MathUtil.convertNanoMetersToMils(coord.getX()),2);
          double yInMils = MathUtil.roundToPlaces(MathUtil.convertNanoMetersToMils(coord.getY()),2);

          String shape = "R";
          ShapeEnum shapeEnum = packagePin.getLandPatternPad().getShapeEnum();
          //pin shape is round for PTH
          if (shapeEnum.equals(ShapeEnum.CIRCLE)||
              jointTypeEnum == JointTypeEnum.THROUGH_HOLE || 
              jointTypeEnum == JointTypeEnum.OVAL_THROUGH_HOLE) //Siew Yeng - XCR-3318 - Oval PTH
          {
            shape = "C";
            pinWidth = Math.min(padWidth,padLength) * 0.7;
            pinLength = pinWidth;
          }
          
          double widthInMils = MathUtil.roundToPlaces(MathUtil.convertNanoMetersToMils(pinWidth),2);
          double lengthInMils = MathUtil.roundToPlaces(MathUtil.convertNanoMetersToMils(pinLength),2);
          double orientation = packagePin.getPinOrientationEnum().getDegrees();
          if(packagePin.getJointTypeEnum()== JointTypeEnum.SINGLE_PAD)
            orientation = 90;

          os.println("@" + compPackageName + "  " + pitchInMils + " " + composition + " " + algoFamily + " " + pinName + " " + xInMils + " " + yInMils + " " + shape + " " + widthInMils + " " + lengthInMils+" "+orientation);
          printDivider = true;
        }
        if (printDivider)
          os.println("#");
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
  
}
