/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.text.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * write panel.ndf
 * @author Siew Yeng, Phang
 */
class PanelNdfWriter
{
  private static final String _SUBPANEL_ID_TOKEN = ".SUBPANEL_ID:";
  private static final String _UNIT_TOKEN = ".UNIT:";
  private static final String _ROTATION_TOKEN = ".ROTATION:";
  private static final String _STAGE_SPEED_TOKEN = ".STAGE_SPEED:";
  private static final String _DIMENSIONS_TOKEN = ".DIMENSIONS:";
  private static final String _FIDUCIAL_TOKEN = ".FIDUCIAL:";

  private static PanelNdfWriter _instance;
  
  private final NumberFormat _decimalFormat = new DecimalFormat("#0.0000");

  /**
   * @author Bill
   */
  static synchronized PanelNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Bill Darbie
   * @author Siew Yeng, Phang
   */
  void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(directory != null);

    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    String fileName = directory + File.separator + FileName.getPanelNdfFile();

    PrintWriter os = null;

    try
    {
      os = new PrintWriter(fileName);
      //.SUBPANEL_ID
      os.println(_SUBPANEL_ID_TOKEN + " " + project.getName());

      //.UNIT
      os.println(_UNIT_TOKEN + " " + MathUtilEnum.MILS.toString());
      os.println(_STAGE_SPEED_TOKEN + " ");

      //.ROTATION
      int rotation = panelSettings.getDegreesRotationRelativeToCad();
      os.println(_ROTATION_TOKEN + " " + rotation);
      os.println("");

      //.DIMENSIONS
      os.println("# .DIMENSIONS panelWidth panelLength panelThickness");
      double panelWidthInMils = MathUtil.convertNanoMetersToMils(panel.getWidthInNanoMeters());
      double panelLengthInMils = MathUtil.convertNanoMetersToMils(panel.getLengthInNanoMeters());
      double panelThicknessInNanometers = 0;
      if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
        panelThicknessInNanometers = panel.getThicknessInNanometers() - XrayTester.getSystemPanelThicknessOffset();
      else
        panelThicknessInNanometers = panel.getThicknessInNanometers();
      
      double panelThicknessInMils = MathUtil.convertNanoMetersToMils(panelThicknessInNanometers);
      
      os.println(_DIMENSIONS_TOKEN + " " + _decimalFormat.format(panelWidthInMils) + " " + 
        _decimalFormat.format(panelLengthInMils) + " " + _decimalFormat.format(panelThicknessInMils));
      os.println("");

      //@<board side name> <orientation> <test boolean> <x location> <y location> <rotation> <other board side name> <x location> <y location> <rotation> [ <sensor position> ]
      os.println("# @boardSideName orientation testBoolean X Y rotation otherBoardSideName X Y rotation [panelStop]");
      os.println("");

      BoardSettings boardSettings = null;

      for (Board board : panel.getBoards())
      {
        boardSettings = board.getBoardSettings();

        PanelCoordinate panelCoordinate = board.getCoordinateInNanoMeters();
        double xInMils = MathUtil.convertNanoMetersToMils(panelCoordinate.getX());
        double yInMils = MathUtil.convertNanoMetersToMils(panelCoordinate.getY());

        int sideBoard1DegreesRotation = board.getDegreesRotationRelativeToPanel();
        int sideBoard2DegreesRotation = board.getDegreesRotationRelativeToPanel();
        if(board.getDegreesRotationRelativeToPanel() == 90 || board.getDegreesRotationRelativeToPanel() == 270)
          sideBoard2DegreesRotation = 360 - board.getDegreesRotationRelativeToPanel();

        String primaryBoardName = "TOP";
        String secondaryBoardName = "BOTTOM";

        if(panel.getBoardTypes().size()>1)
        {
          String boardTypeName = board.getBoardType().getName();
          primaryBoardName = primaryBoardName + "_A" + boardTypeName.charAt(boardTypeName.length()-1);
          secondaryBoardName = secondaryBoardName + "_B" + boardTypeName.charAt(boardTypeName.length()-1);
        }
        
        if(board.isBoardSingleSided())
        {
          if(board.bottomSideBoardExists())
          {
            primaryBoardName = "NONE";
            sideBoard1DegreesRotation = 0;
          }
          else
          {
            secondaryBoardName = "NONE";
            sideBoard2DegreesRotation = 0;
          }
        }

        String orientation  = "TOP";
        if(board.isLeftToRightFlip() || board.isTopToBottomFlip())
          orientation = "BOT";

        os.println("@" + primaryBoardName + " " + orientation + " " + boardSettings.isInspected() + " " + 
          _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + sideBoard1DegreesRotation + " " + 
          secondaryBoardName + " " + _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + sideBoard2DegreesRotation);
      }
      os.println("");

      //.FIDUCIAL: <1|2|3> <board side> <x location> <y location> <shape> <width> <length> <orientation>\
      os.println("# .FIDUCIAL: number boardSide X Y shape width length orientation");
      for (Fiducial fiducial : panel.getFiducials())
      {
        String sideBoard = "BOTTOM";
        if (fiducial.isPanelSide1())
        {
          sideBoard = "TOP";
        }
        FiducialType fiducialType = fiducial.getFiducialType();
        String name = fiducialType.getName();
        PanelCoordinate coord = fiducialType.getCoordinateInNanoMeters();
        double xInMils = MathUtil.convertNanoMetersToMils(coord.getX());
        double yInMils = MathUtil.convertNanoMetersToMils(coord.getY());
        double lengthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getLengthInNanoMeters());
        double widthInMils = MathUtil.convertNanoMetersToMils(fiducialType.getWidthInNanoMeters());
        ShapeEnum shapeEnum = fiducialType.getShapeEnum();
        String shape = "";
        if (shapeEnum.equals(ShapeEnum.CIRCLE))
          shape = "C";
        else if (shapeEnum.equals(ShapeEnum.RECTANGLE))
          shape = "R";
        else
          Assert.expect(false);

        //.FIDUCIAL: <1|2|3> <board side> <x location> <y location> <shape> <width> <length> <orientation>
        os.println(_FIDUCIAL_TOKEN+" "+name + " " + sideBoard + " " + _decimalFormat.format(xInMils) + " " + 
          _decimalFormat.format(yInMils) + " " + shape + " " + _decimalFormat.format(widthInMils) + " " + 
          _decimalFormat.format(lengthInMils));
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
