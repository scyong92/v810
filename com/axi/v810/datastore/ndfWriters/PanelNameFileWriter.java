/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * write dir_id.dat
 * @author siew-yeng.phang
 */
public class PanelNameFileWriter
{
  private static PanelNameFileWriter _instance;

  /**
   * @author Bill
   */
  static synchronized PanelNameFileWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelNameFileWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelNameFileWriter()
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  void writePanelNameFile(String projectName, String directory) throws DatastoreException
  {
    Assert.expect(projectName!=null);
    Assert.expect(directory!=null);
    
    //Panel Name File
    String fileName = directory + File.separator + FileName.getPanelNameFile();
   
    // create the directory if it does not exist
    if (FileUtilAxi.exists(directory))
    {
      PrintWriter os = null;
    
      try
      {
        os = new PrintWriter(fileName);
        os.println(projectName);
      }
      catch(FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  public void writeSideBoardNameFile(String projectName, String sideBoardName, String directory) throws DatastoreException
  {
    Assert.expect(projectName!=null);
    Assert.expect(sideBoardName!=null);
    Assert.expect(directory!=null);
    
    //Board Name File
    String fileName = directory + File.separator + FileName.getPanelNameFile();
    
    // create the directory if it does not exist
    if (FileUtilAxi.exists(directory))
    {
      PrintWriter os = null;
    
      try
      {
        os = new PrintWriter(fileName);
        os.println(sideBoardName);
      }
      catch(FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }
  }
}
