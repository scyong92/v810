/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.util.*;
import java.awt.geom.*;
import java.text.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * write board.ndf
 * @author Siew Yeng, Phang
 */
public class BoardNdfWriter
{
  private static final String _BOARD_ID_TOKEN = ".BOARD_ID:";
  private static final String _UNIT_TOKEN = ".UNIT:";
  private static final String _DIMENSIONS_TOKEN = ".DIMENSIONS:";
  
  private static BoardNdfWriter _instance;
  private PanelNameFileWriter _panelNameFileWriter;
  
  private final NumberFormat _decimalFormat = new DecimalFormat("#0.0000");

  /**
   * @author Bill
   */
  static synchronized BoardNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Bill Darbie
   * @author Siew Yeng, Phang
   */
  public void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project!=null);
    Assert.expect(directory!=null);

    List<BoardType> boardTypes = project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      if(boardTypes.size()>1)
        writeSideBoardNdf(directory, project, boardType, true);  
      else
        writeSideBoardNdf(directory, project, boardType, false);
    }
  }

  /**
   * @author Siew Yeng, Phang
   */
  void writeSideBoardNdf(String directory, Project project, BoardType boardType, boolean isMultipleBoardType) throws DatastoreException
  {
    Assert.expect(directory!=null);
    Assert.expect(project!=null);
    Assert.expect(boardType!=null);
        
    _panelNameFileWriter = PanelNameFileWriter.getInstance();
    String projName = project.getName();
    String boardTypeName = boardType.getName();
    String sideBoardName = null;
    List<ComponentType> componentTypes =null;
    for(int i=0;i<2;i++)
    {
      if(i==0)
      {
        if(boardType.sideBoardType1Exists())
        {
          componentTypes = boardType.getSideBoardType1().getComponentTypes();
          if(isMultipleBoardType)
            sideBoardName = "TOP_A"+boardTypeName.charAt(boardTypeName.length()-1);
          else
            sideBoardName = "TOP";
        }
        else
          continue;
      }
      else
      {
        if(boardType.sideBoardType2Exists())
        {
          componentTypes = boardType.getSideBoardType2().getComponentTypes();
          if(isMultipleBoardType)
            sideBoardName = "BOTTOM_B"+boardTypeName.charAt(boardTypeName.length()-1);
          else
            sideBoardName = "BOTTOM";
        }
        else
          continue;
      }
      
      String dirName = directory + File.separator + HashUtil.hashName(sideBoardName);

      // create the directory if it does not exist
      if (FileUtilAxi.exists(dirName) == false)
      {
        FileUtilAxi.mkdirs(dirName); 
      }
      
      //Write sideBoard name file dir_id.dat
      _panelNameFileWriter.writeSideBoardNameFile(projName, sideBoardName, dirName);
      
      String fileName = dirName + File.separator + FileName.getBoardNdfFile();
    
      PrintWriter os = null;

      try
      {
        os = new PrintWriter(fileName);
        //.BOARD_ID
        os.println(_BOARD_ID_TOKEN + " " + sideBoardName);
        //.UNIT
        os.println(_UNIT_TOKEN + " " + MathUtilEnum.MILS.toString());
        os.println("");
        //.DIMENSIONS
        os.println("# .DIMENSIONS boardWidth boardLength boardThickness");

        double boardWidthInNanometer = boardType.getWidthInNanoMeters();
        double boardLengthInNanometer = boardType.getLengthInNanoMeters();

        double boardWidthInMils = MathUtil.convertNanoMetersToMils(boardType.getWidthInNanoMeters());
        double boardLengthInMils = MathUtil.convertNanoMetersToMils(boardLengthInNanometer);
      
        double thicknessInNanometer = 0;
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
          thicknessInNanometer = project.getPanel().getThicknessInNanometers() - XrayTester.getSystemPanelThicknessOffset();
        else
          thicknessInNanometer = project.getPanel().getThicknessInNanometers();
      
        double boardThicknessInMils = MathUtil.convertNanoMetersToMils(thicknessInNanometer);

        os.println(_DIMENSIONS_TOKEN + " " + _decimalFormat.format(boardWidthInMils) + " " + 
          _decimalFormat.format(boardLengthInMils) + " " + _decimalFormat.format(boardThicknessInMils));
        os.println("");
        //@<ref des> <x location> <y location> <orientation> <footprint>
        os.println("# @refDes X Y orientation footprint");

        if(sideBoardName.startsWith("TOP"))
        {
          for (ComponentType componentType : componentTypes)
          {
            String refDes = componentType.getReferenceDesignator();
            BoardCoordinate boardCoordinate = componentType.getCoordinateInNanoMeters();
            double xInMils = MathUtil.convertNanoMetersToMils(boardCoordinate.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(boardCoordinate.getY());
            double degreesRotation = componentType.getDegreesRotationRelativeToBoard();
            LandPattern landPattern = componentType.getLandPattern();
            String landPatternName = landPattern.getName();

            os.println("@" + refDes + " " + _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + 
              degreesRotation + " " + landPatternName);
          }
          
          //Write board fiducial if exist
          List<FiducialType> fiducialTypes = boardType.getSideBoardType1().getFiducialTypes();
          for (FiducialType fiducialType : fiducialTypes)
          {
            String name = fiducialType.getName();
            PanelCoordinate coord = fiducialType.getCoordinateInNanoMeters();
            double xInMils = MathUtil.convertNanoMetersToMils(coord.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(coord.getY());
            double degreesRotation = 0;
            String landPatternName = "fiducial1";

            os.println("@" + name + " " + _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + 
              degreesRotation + " " + landPatternName);
          }
        }
        else //bottom side
        {          
          //reverse transform
          AffineTransform leftToRightFlipTrans = AffineTransform.getScaleInstance(-1, 1);
          leftToRightFlipTrans.preConcatenate(AffineTransform.getTranslateInstance(boardWidthInNanometer, 0));
                    
//          int componentTypeRotation = 0;
          
          for (ComponentType componentType : componentTypes)
          {
            String refDes = componentType.getReferenceDesignator();
 
//            // since the component is on the bottom take 360 - the rotation
//            componentTypeRotation = 360 - componentTypeRotation;
//
//            double degrees = componentType.getDegreesRotationRelativeToBoard() + componentTypeRotation;
//            degrees = MathUtil.getDegreesWithin0To359(degrees);
//
//            componentType.setDegreesRotationRelativeToBoard(degrees);
            BoardCoordinate boardCoordinate = componentType.getCoordinateInNanoMeters();
            Point2D oldPoint = new Point2D.Double(boardCoordinate.getX(), boardCoordinate.getY());
            Point2D newPoint = new Point2D.Double(0, 0);
            leftToRightFlipTrans.transform(oldPoint, newPoint);
            boardCoordinate.setLocation((int)Math.round(newPoint.getX()), (int)Math.round(newPoint.getY()));
            //Siew Yeng - XCR-3524 - shouldn't set coordinate to componentType
            //componentType.setCoordinateInNanoMeters(boardCoordinate);

            double xInMils = MathUtil.convertNanoMetersToMils(boardCoordinate.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(boardCoordinate.getY());
            double degreesRotation = componentType.getDegreesRotationRelativeToBoard();
            LandPattern landPattern = componentType.getLandPattern();
            String landPatternName = landPattern.getName();

            os.println("@" + refDes + " " + _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + 
              degreesRotation + " " + landPatternName);
          }
//          componentTypeRotation = 0;
//          preTrans = AffineTransform.getTranslateInstance( -boardWidthInNanometer, 0);
//          trans.preConcatenate(preTrans);
//          
//          if ((boardType.sideBoardType1Exists()) && (boardType.sideBoardType2Exists()))
//          {
//            int degreesRotation = 360 - sideBoard2DegreesRotation;
//            componentTypeRotation += degreesRotation;
//            preTrans = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation));
//            trans.preConcatenate(preTrans);
//          }
          
          //Write board fiducial if exist
          List<FiducialType> fiducialTypes = boardType.getSideBoardType2().getFiducialTypes();
          for (FiducialType fiducialType : fiducialTypes)
          {
            String name = fiducialType.getName();
            PanelCoordinate coord = fiducialType.getCoordinateInNanoMeters();
            double xInMils = MathUtil.convertNanoMetersToMils(coord.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(coord.getY());
            double degreesRotation = 0;
            String landPatternName = "fiducial2";

            os.println("@" + name + " " + _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + 
              degreesRotation + " " + landPatternName);
          }
        }
        os.println("");
      }
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }
  }
}
