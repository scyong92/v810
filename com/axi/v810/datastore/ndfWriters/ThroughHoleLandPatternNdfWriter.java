/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.ndfWriters;

import java.io.*;
import java.text.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Write padgeom.ndf
 * @author siew-yeng.phang
 */
class ThroughHoleLandPatternNdfWriter
{
  private static final String _UNIT_TOKEN = ".UNIT:";
  
  private static ThroughHoleLandPatternNdfWriter _instance;
  
  private final NumberFormat _decimalFormat = new DecimalFormat("#0.0000");

  /**
   * @author Bill
   */
  static synchronized ThroughHoleLandPatternNdfWriter getInstance()
  {
    if (_instance == null)
      _instance = new ThroughHoleLandPatternNdfWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ThroughHoleLandPatternNdfWriter()
  {
    // do nothing
  }
  
  /**
   * @author Bill Darbie
   * @author Siew Yeng, Phang
   */
  void write(Project project, String directory) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(directory != null);
    
    String fileName = directory + File.separator + FileName.getThroughHoleLandPatternNdfFile();

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);
      
      //.UNIT
      os.println(_UNIT_TOKEN+" "+MathUtilEnum.MILS.toString());
      os.println("");
      
      //# @footPrintName padNumber X-hole Y-hole holeDiameter shape X Y padXsize padYsize
      os.println("# @footPrintName padNumber X-hole Y-hole holeDiameter shape X Y padXsize padYsize");
      os.println("");
      for (LandPattern landPattern : project.getPanel().getAllLandPatterns())
      {
        boolean printDivider = false;
        String landPatternName = landPattern.getName();
        
        for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
        {
          if (landPatternPad.isThroughHolePad())
          {
            String padName = landPatternPad.getName();
            ThroughHoleLandPatternPad thLandPattern = (ThroughHoleLandPatternPad)landPatternPad;
            ComponentCoordinate panelCoordinate = thLandPattern.getHoleCoordinateInNanoMeters();
            double xHoleInMils = MathUtil.convertNanoMetersToMils(panelCoordinate.getX());
            double yHoleInMils = MathUtil.convertNanoMetersToMils(panelCoordinate.getY());
//            double holeDiamInMils = MathUtil.convertNanoMetersToMils(thLandPattern.getHoleDiameterInNanoMeters());
            //Siew Yeng - XCR-3318 - Oval PTH
            double holeWidthInMils = MathUtil.convertNanoMetersToMils(thLandPattern.getHoleWidthInNanoMeters());
            double holeLengthInMils = MathUtil.convertNanoMetersToMils(thLandPattern.getHoleLengthInNanoMeters());
            double holeDiamInMils = Math.min(holeWidthInMils, holeLengthInMils);
            
            ComponentCoordinate coord = landPatternPad.getCoordinateInNanoMeters();
            double xInMils = MathUtil.convertNanoMetersToMils(coord.getX());
            double yInMils = MathUtil.convertNanoMetersToMils(coord.getY());
            double widthInMils = MathUtil.convertNanoMetersToMils(landPatternPad.getWidthInNanoMeters());
            double lengthInMils = MathUtil.convertNanoMetersToMils(landPatternPad.getLengthInNanoMeters());
            String shape = "R";
            ShapeEnum shapeEnum = landPatternPad.getShapeEnum();
            if (shapeEnum.equals(ShapeEnum.CIRCLE))
              shape = "C";

            // padNameOrNumber2 xLocation yLocation width length rectangle|circle rotationInDegrees surfaceMount|throughHole holeXLocation holeYLocation holeDiameter");
            os.println("@"+landPatternName+ "  " + padName + " " + _decimalFormat.format(xHoleInMils) + " " + 
              _decimalFormat.format(yHoleInMils) + " " + _decimalFormat.format(holeDiamInMils) + " " + shape + " " +
              _decimalFormat.format(xInMils) + " " + _decimalFormat.format(yInMils) + " " + 
              _decimalFormat.format(widthInMils) + " " + _decimalFormat.format(lengthInMils));
            
            printDivider = true;
          }
          else
            continue;
        }
        if(printDivider)
          os.println("#");
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
