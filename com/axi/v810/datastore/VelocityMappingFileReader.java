package com.axi.v810.datastore;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class VelocityMappingFileReader
{
  private static VelocityMappingFileReader _instance = null;

  private Pattern _commentPattern = Pattern.compile("^(\\s*)((\\\\#|[^#])*+)(\\s*)(#.*)*$");

  /**
   * @author Kok Chun, Tan
   */
  private VelocityMappingFileReader()
  {
    // nothing
  }


  /**
   * @author Kok Chun, Tan
   */
  public static synchronized VelocityMappingFileReader getInstance()
  {
    if (_instance == null)
    {
      _instance = new VelocityMappingFileReader();
    }
    return _instance;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public synchronized Map<String, VelocityMappingParameters> readDataFile(String velocityMappingFilePath) throws DatastoreException
  {
    Assert.expect(velocityMappingFilePath != null);
    
    LineNumberReader is = null;
    Map<String, VelocityMappingParameters> velocityToVelocityMappingParametersMap = new HashMap <String, VelocityMappingParameters> ();
    String key = "";

    try
    {
      is = ParseUtil.openFile(velocityMappingFilePath);
      String line = getNextLine(is, velocityMappingFilePath);
      
      // first line not expect will null
      if(line == null)
        throw new FileCorruptDatastoreException(velocityMappingFilePath, is.getLineNumber());

      do
      {
        String[] data = line.split(",");
        key = data[0] + data[1];
        VelocityMappingParameters value = new VelocityMappingParameters(StringUtil.convertStringToInt(data[0]),
                                                                        StringUtil.convertStringToInt(data[1]),
                                                                        StringUtil.convertStringToDouble(data[2]),
                                                                        StringUtil.convertStringToDouble(data[3]),
                                                                        StringUtil.convertStringToDouble(data[4]));
        velocityToVelocityMappingParametersMap.put(key, value);
        line = getNextLine(is, velocityMappingFilePath);
      }
      while (line != null);
    }
    catch (BadFormatException ex)
    {
      throw new FileCorruptDatastoreException(velocityMappingFilePath, is.getLineNumber());
    }
    finally
    {
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException ioe)
        {
          is = null;
          DatastoreException dex = new DatastoreException(velocityMappingFilePath);
          dex.initCause(ioe);
          throw dex;
        }
      }
    }
    
    return velocityToVelocityMappingParametersMap;
  }

  /**
   * Get next line from the data file and clean it up.
   * Comments ('#' character to end of line) are skipped. Spaces at
   * beginning and end of line are removed. Skips blank lines.
   * @param pathToFile Full path to file. Only used if exception during file read.
   * @author Kok Chun, Tan
   */
  private synchronized String getNextLine(LineNumberReader is, String pathToFile) throws DatastoreException
  {
    String line = null;
    do
    {
      line = ParseUtil.readNextLine(is, pathToFile);
      
      if(line == null)
      {
        return null;
      }

      Matcher commentMatcher = _commentPattern.matcher(line);
      if (commentMatcher.matches() == false)
      {
        throw new FileCorruptDatastoreException(pathToFile, is.getLineNumber());
      }
      else
      {
        // get rid of any comments
        line = commentMatcher.group(2);
        // get rid of leading and trailing spaces
        line = line.trim();
        line = line.replaceAll("\\\\#", "#");
      }
    }
    while (line.equals(""));

    return line;
  }
}
