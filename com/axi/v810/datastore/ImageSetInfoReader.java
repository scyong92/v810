package com.axi.v810.datastore;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class reads the file imageSet.info.
 * Every image set (collection of images for
 * offline testing) has this file that
 * currently only describes the project
 * and version number associated with the
 * image set. The file can have comments
 * (which are prepended with a hash mark #)
 * or key value pairs (key=value).
 *
 * @author George A. David
 * @author Erica Wheatcroft
 */
public class ImageSetInfoReader
{
  public static String PROJECT_NAME_KEY = "projectName";
  public static String TEST_PROGRAM_VERSION_NUMBER_KEY = "testProgramVersionNumber";
  public static String IS_POPULATED = "isPopulated";
  public static String IMAGE_SET_NAME = "imageSetName";
  public static String IMAGE_SET_USER_DESCRIPTION = "imageSetUserDescription";
  public static String IMAGE_SET_TYPE = "imageSetType";
  public static String IMAGE_SET_SYSTEM_DESCRIPTION = "imageSetSystemDescription";
  public static String NUMBER_OF_IMAGES_SAVED = "numberOfImages";
  public static String DATE_SAVED = "date";
  public static String MACHINE_SERIAL_NUMBER = "machineSerialNumber";
  public static String IS_GENERATE_MULTI_ANGLE_IMAGES = "isGenerateMultiAngleImages";
  public static String HIGH_MAGNIFICATION = "highMagnification";
  public static String LOW_MAGNIFICATION = "lowMagnification";
  private ConfigFileReaderUtil _reader;
  private ImageSetData _imageSetData;

  /**
   * @author George A. David
   * @author Erica Wheatcroft
   */
  public ImageSetInfoReader()
  {
    _reader = new ConfigFileReaderUtil();
    List<String> validKeys = new LinkedList<String>();
    validKeys.add(PROJECT_NAME_KEY);
    validKeys.add(TEST_PROGRAM_VERSION_NUMBER_KEY);
    validKeys.add(IS_POPULATED);
    validKeys.add(IMAGE_SET_NAME);
    validKeys.add(IMAGE_SET_USER_DESCRIPTION);
    validKeys.add(IMAGE_SET_SYSTEM_DESCRIPTION);
    validKeys.add(IMAGE_SET_TYPE);
    validKeys.add(NUMBER_OF_IMAGES_SAVED);
    validKeys.add(DATE_SAVED);
    validKeys.add(MACHINE_SERIAL_NUMBER);
    validKeys.add(IS_GENERATE_MULTI_ANGLE_IMAGES);
    validKeys.add(HIGH_MAGNIFICATION);
    validKeys.add(LOW_MAGNIFICATION);
    _reader.setValidKeys(validKeys);
    
    List<String> optionalKeys = new LinkedList<String>();
    optionalKeys.add(IS_GENERATE_MULTI_ANGLE_IMAGES);
    optionalKeys.add(HIGH_MAGNIFICATION);
    optionalKeys.add(LOW_MAGNIFICATION);
    _reader.setOptionalKeys(optionalKeys);
  }


  /**
   * @author George A. David
   * @author Erica Wheatcroft
   */
  public void parseFile(String filePath) throws DatastoreException
  {
    Assert.expect(_reader != null);

    _reader.parseFile(filePath);
    _imageSetData = new ImageSetData();
    _imageSetData.setProjectName(_reader.getValue(PROJECT_NAME_KEY));
    try
    {
      _imageSetData.setTestProgramVersionNumber(Integer.parseInt(_reader.getValue(TEST_PROGRAM_VERSION_NUMBER_KEY)));
    }
    catch (NumberFormatException ex)
    {
      DatastoreException dex = new FileCorruptDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    String isPopulated = _reader.getValue(IS_POPULATED);
    if(isPopulated.equalsIgnoreCase("true"))
      _imageSetData.setBoardPopulated(true);
    else if (isPopulated.equalsIgnoreCase("false"))
      _imageSetData.setBoardPopulated(false);
    else
      throw new FileCorruptDatastoreException(filePath);

    _imageSetData.setImageSetName(_reader.getValue(IMAGE_SET_NAME));
    _imageSetData.setUserDescription(_reader.getValue(IMAGE_SET_USER_DESCRIPTION));
    _imageSetData.setSystemDescription(StringUtil.convertAcsiiStringToUnicodeString(_reader.getValue(IMAGE_SET_SYSTEM_DESCRIPTION)));

    try
    {
      _imageSetData.setNumberOfImagesSaved(Integer.parseInt(_reader.getValue(NUMBER_OF_IMAGES_SAVED)));
    }
    catch (NumberFormatException ex)
    {
      DatastoreException dex = new FileCorruptDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }

    _imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.getImageSetType(_reader.getValue(IMAGE_SET_TYPE)));

    try
    {
      _imageSetData.setDateInMils(Long.parseLong(_reader.getValue(DATE_SAVED)));
    }
    catch(NumberFormatException ex)
    {
      DatastoreException dex = new FileCorruptDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }

    _imageSetData.setMachineSerialNumber(_reader.getValue(MACHINE_SERIAL_NUMBER));
    
    // XCR1594 - Added new key to keep track of 2.5D images feature
    if (_reader.hasKey(IS_GENERATE_MULTI_ANGLE_IMAGES))
    {
      String isGenerateMultiAngleImages = _reader.getValue(IS_GENERATE_MULTI_ANGLE_IMAGES);
      if(isGenerateMultiAngleImages.equalsIgnoreCase("true"))
        _imageSetData.setGenerateMultiAngleImages(true);
      else if (isGenerateMultiAngleImages.equalsIgnoreCase("false"))
        _imageSetData.setGenerateMultiAngleImages(false);
      else
        throw new FileCorruptDatastoreException(filePath);
     }
    
    //XCR 3322 - Add new optional keys to record the high and low magnification info    
    if (_reader.hasKey(HIGH_MAGNIFICATION))
    {
      _imageSetData.setHighMagnification(_reader.getValue(HIGH_MAGNIFICATION));
    }

    if (_reader.hasKey(LOW_MAGNIFICATION))
    {
      _imageSetData.setLowMagnification(_reader.getValue(LOW_MAGNIFICATION));
    }
  }

  /**
   * @author George A. David
   */
  public ImageSetData getImageSetData()
  {
    Assert.expect(_imageSetData != null);

    return _imageSetData;
  }
}
