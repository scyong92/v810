package com.axi.v810.datastore;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;


/**
 * This exception is thrown when a properties file has a menu shortcut that is a duplicate
 * of another menu in the menubar.
 * @author George Booth
 */
public class InvalidMenuShortcutInPropertiesDatastoreException extends DatastoreException
{
  /**
   * @author George Booth
   */
  public InvalidMenuShortcutInPropertiesDatastoreException(String menu1, String shortcut, String menu2)
  {
    super(new LocalizedString("GUI_ERROR_INVALID_MENU_SHORTCUT_KEY",
                              new Object[]{menu1, shortcut, menu2,
                              FileName.getSoftwareConfigFullPath()}));

    Assert.expect(menu1 != null);
    Assert.expect(shortcut != null);
    Assert.expect(menu2 != null);
  }
}
