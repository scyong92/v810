package com.axi.v810.datastore.shopFloorSystem;

import java.util.*;
import java.text.*;
import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Khaw Chek Hau
 * @XCR3534: v810 SECS/GEM protocol implementation
 */
public class SECSGEMSystem extends AbstractShopFloorSystem
{
  private static SECSGEMSystem _instance; 
  private final SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  private final SimpleDateFormat _eventTimeFormat = new SimpleDateFormat("HH:mm:ss");             
  private SECSGEMEventEnum _secsgemEvent;
  private String csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");
  private String _secsgemLogFileName;
  private String endLineIndicator = "$";
  private String _dateTime;
  private String _eventTime;
  private String _eventValue;
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public static synchronized SECSGEMSystem getInstance()
  {
    if (_instance == null)
      _instance = new SECSGEMSystem();
    return _instance;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  SECSGEMSystem()
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void inspectionStart() throws DatastoreException
  {
    //do nothing
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void inspectionEnd() throws DatastoreException
  {
    //do nothing 
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void loadPanel() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void unloadPanel() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void inspectionAbort() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void getSerialNumber(String serialNumber) throws DatastoreException
  {
    //do nothing  
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void softwarePowerUp() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void startUpCompleted() throws DatastoreException
  {
    //do nothing 
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void startUpCompletedWithUserId() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void login() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void setLoginUserName(String userName) throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void softwarePowerOff() throws DatastoreException
  {
    //do nothing  
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void recipeLoading(String projectToLoad) throws DatastoreException
  {
    //do nothing 
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void recipeLoaded(String projectToLoad) throws DatastoreException
  {
    writeSECSGEMReport(SECSGEMEventEnum.CURRENT_ACTIVE_RECIPE, projectToLoad);  
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void recipeModified() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentError() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentAlarm() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentErrorCleared() throws DatastoreException
  {
    //do nothing
  }
    
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentAlarmCleared() throws DatastoreException
  {
    //do nothing
  }
    
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentStarved() throws DatastoreException
  {
    //do nothing 
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void equipmentUnStarved() throws DatastoreException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void executeCAMxProgram() throws IOException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void terminateCAMxProgram() throws IOException
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public void writeSECSGEMReport(SECSGEMEventEnum secsgemEvent, String secsgemEventValue) throws DatastoreException
  {
    Assert.expect(secsgemEvent != null);
    
    _secsgemEvent = secsgemEvent;
    _dateTime = _dateFormat.format(new Date());
    _eventTime = _eventTimeFormat.format(new Date());
    
    String dir = Directory.getSECSGEMDir();
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);
    
    if (_secsgemEvent == SECSGEMEventEnum.CURRENT_ACTIVE_RECIPE)
    {
      _eventValue = secsgemEventValue;
    }

    try
    {
      writeSECSGEMLogFile();
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_secsgemLogFileName);
      dex.initCause(ex);
      throw dex;
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_secsgemLogFileName);
      dex.initCause(io);
      throw dex;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  private void writeSECSGEMLogFile() throws IOException
  {            
    _secsgemLogFileName = FileName.getSECSGEMLogFilePath();    
    
    FileWriterUtil fileWriter = new FileWriterUtil(_secsgemLogFileName, false);
    try
    {
      fileWriter.open();
      fileWriter.writeln("Version = " + FileName.getSECSGEMLogLatestFileVersion());
      fileWriter.writeln(_dateTime + csvDelimiter + _eventTime + csvDelimiter + _secsgemEvent.toString() + csvDelimiter + _eventValue + endLineIndicator);
    }
    catch (CouldNotCreateFileException ex)
    {
      throw ex;
    }
    finally
    {
      fileWriter.close();   
    }
  }
}
