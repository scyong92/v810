package com.axi.v810.datastore.shopFloorSystem;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.text.*;
import java.io.*;

/**
 * @author Khaw Chek Hau
 */
public class CAMXShopFloorSystem extends AbstractShopFloorSystem
{
  private static CAMXShopFloorSystem _instance;
  private static final String _DATE_TIME = "dateTime";
  private static final String _ITEM_INSTANCE_ID = "itemInstanceId";
  private static final String _LANE_ID = "laneId";
  private static final String _ZONE_ID = "zoneId";
  private static final String _FROM_ZONE_ID = "fromZoneId";
  private static final String _TO_ZONE_ID = "toZoneId";
  private static final String _ABORT_ID = "abortId";
  private static final String _SCANNER_ID = "scannerId";
  private static final String _SOFTWARE_REV = "softwareRev";
  private static final String _HARDWARE_REV = "hardwareRev";
  private static final String _EVENT_INITIATOR = "eventInitiator";
  private static final String _RECIPE_ID = "recipeId";
  private static final String _LANE_LIST = "laneList";
  private static final String _ZONE_LIST = "zoneList";
  private static final String _ACTION = "action";
  private static final String _ERROR_ID = "errorId";
  private static final String _ERROR_INSTANCE_ID = "errorInstanceId";
  private static final String _ALARM_ID = "alarmId";
  private static final String _ALARM_INSTANCE_ID = "alarmInstanceId";
  private static final String _ALARM_TYPE = "alarmType";
  private static final String _CURRENT_STATE = "currentState";
  private static final String _PREVIOUS_STATE = "previousState";
  private static final String _EVENT_ID = "eventId";    
  private static final String _SPACE = " ";    
  
  private XrayTester _xrayTester;
  private Runtime _runtime;
  private Config _config;
  private String _panelSerialNumber = "";
  private int _zoneId;
  private String _CAMXEquipmentState;
  private String _currentUserName;
  private String _recipeName = null;
  private boolean _isErrorOn = false;
  private int _errorInstanceId = -1;
  private CAMXEventEnum _CAMXEventEnum;
  private DateFormat _format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SZ", Locale.CHINA);
  private String _dateTime;
  private String _serialNumberIsNotRead = "";
  private int _laneId;
  private String _abortMessage = "Aborted due to user abort the production";
  private String _scannerId = "null";

  /**
   * @author Khaw Chek Hau
   */
  public static synchronized CAMXShopFloorSystem getInstance()
  {
    if (_instance == null)
      _instance = new CAMXShopFloorSystem();
    return _instance;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public CAMXShopFloorSystem()
  {
    _runtime = Runtime.getRuntime();
    _config = Config.getInstance();
    _xrayTester = XrayTester.getInstance();
    
    _CAMXEquipmentState = "OFF";
    _zoneId = 0;
    _laneId = 1;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void inspectionStart() throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_WORK_START, null);
  }

  /**
   * @author Khaw Chek Hau
   */
  public void inspectionEnd() throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_WORK_COMPLETE, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void loadPanel() throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_TRANSFER_IN, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void unloadPanel() throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_TRANSFER_OUT, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void inspectionAbort() throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_WORK_ABORT, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void getSerialNumber(String serialNumber) throws DatastoreException
  {
    writeCAMXItemEventLog(CAMXEventEnum.ITEM_IDENTIFIER_READ, serialNumber);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void softwarePowerUp() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_INITIALIZATION_COMPLETE, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void startUpCompleted() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_SETUP_COMPLETE, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void startUpCompletedWithUserId() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_START_SELECTED, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void login() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_SETUP_SELECTED, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void setLoginUserName(String userName) throws DatastoreException
  {
    setLoginId(userName);
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void softwarePowerOff() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_POWER_OFF, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void recipeLoading(String projectToLoad) throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_RECIPE_SELECTED, projectToLoad);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void recipeLoaded(String projectToLoad) throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_RECIPE_READY, projectToLoad);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void recipeModified() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_SELECTED_RECIPE_MODIFIED, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentError() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_ERROR, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentAlarm() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_ALARM, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentErrorCleared() throws DatastoreException
  {
    if (_isErrorOn)
    {
      writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_ERROR_CLEARED, null);  
    }
  }
    
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentAlarmCleared() throws DatastoreException
  {
    if (_isErrorOn)
    {
      writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_ALARM_CLEARED, null);  
    }
  }
    
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentStarved() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_STARVED, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void equipmentUnStarved() throws DatastoreException
  {
    writeCAMXEquipmentEventLog(CAMXEventEnum.EQUIPMENT_UNSTARVED, null);  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void executeCAMxProgram() throws IOException
  {
    try
    {
      String CAMxExeFile = FileName.getCAMXProgramPath();
      String CAMXDir = Directory.getCAMXDir();
      
      try
      {
        if (FileUtilAxi.exists(CAMXDir) == false)
          FileUtilAxi.mkdirs(CAMXDir);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }
            
      File dir = new File(Directory.getCAMXDir());
      
      if ((CAMxExeFile.startsWith("\"") == false) && (CAMxExeFile.contains(" ")))
        CAMxExeFile = "\"" + CAMxExeFile + "\"";
      
      Runtime.getRuntime().exec(CAMxExeFile, null, dir); 
    }
    catch (IOException ioe) 
    {
      throw ioe;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void terminateCAMxProgram() throws IOException
  {
    try
    {
      Runtime.getRuntime().exec("taskkill /F /IM CAMX.exe");
    }
    catch (IOException ioe) 
    {
      //do nothing
    }
  }
           
  /**
   * @author Khaw Chek Hau
   */
  public void writeCAMXItemEventLog(CAMXEventEnum CAMxEventEnum, String panelSerialNumber) throws DatastoreException
  {
    Assert.expect(CAMxEventEnum != null);

    if (panelSerialNumber != null)
      _panelSerialNumber = panelSerialNumber;
    
    _CAMXEventEnum = CAMxEventEnum;

    _dateTime = _format.format(new Date()).replaceAll("(.*)(\\d\\d)$", "$1:$2");
    String FileNameTimeStamp = _dateTime.replace(":", ".");

    String dir = Directory.getCAMXDir();
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);
    
    String outputFileName;

    if (_CAMXEventEnum == _CAMXEventEnum.ITEM_WORK_START)
    {
      outputFileName = FileName.getCAMXOutputFilePath("InspectionBegin", FileNameTimeStamp);  
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.ITEM_WORK_COMPLETE)
    {
      outputFileName = FileName.getCAMXOutputFilePath("BoardFullUnload", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.ITEM_TRANSFER_IN)
    {
      outputFileName = FileName.getCAMXOutputFilePath("BoardTransferTov510", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.ITEM_TRANSFER_OUT)
    {
      outputFileName = FileName.getCAMXOutputFilePath("InspectionFinished", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.ITEM_IDENTIFIER_READ)
    {
      outputFileName = FileName.getCAMXOutputFilePath("BoardAvailUpstream", FileNameTimeStamp);   
    }
    else
      outputFileName = FileName.getCAMXOutputFilePath(_CAMXEventEnum.toString(), FileNameTimeStamp);


    FileWriterUtil fileWriterUtil = new FileWriterUtil(outputFileName, false);
    try
    {
      fileWriterUtil.open();      
      fileWriterUtil.write("<" + _CAMXEventEnum.toString() + _SPACE);
      fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
      fileWriterUtil.write(_ITEM_INSTANCE_ID + "=\"" + _panelSerialNumber + "\"" + _SPACE);

      if (_CAMXEventEnum == CAMXEventEnum.ITEM_WORK_START || _CAMXEventEnum == CAMXEventEnum.ITEM_WORK_COMPLETE)
      {
        fileWriterUtil.write(_LANE_ID + "=\"" + _laneId + "\"" + _SPACE);
        fileWriterUtil.write(_ZONE_ID + "=\"" + _zoneId + "\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.ITEM_WORK_ABORT)
      {
        fileWriterUtil.write(_LANE_ID + "=\"" + _laneId + "\"" + _SPACE);
        fileWriterUtil.write(_ZONE_ID + "=\"" + _zoneId + "\"" + _SPACE);
        fileWriterUtil.write(_ABORT_ID + "=\"" + _abortMessage + "\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.ITEM_IDENTIFIER_READ)
      {
        fileWriterUtil.write(_LANE_ID + "=\"" + _laneId + "\"" + _SPACE);
        fileWriterUtil.write(_ZONE_ID + "=\"" + _zoneId + "\"" + _SPACE);
        fileWriterUtil.write(_SCANNER_ID + "=\"" + _scannerId + "\"");  
      }
      else
        fileWriterUtil.write(_LANE_ID + "=\"" + _laneId + "\"");
      
      fileWriterUtil.write("><Extensions></Extensions></" + _CAMXEventEnum.toString() + ">");

      setZoneId(_CAMXEventEnum.getZoneId());
      setEquipmentStateChange(_CAMXEventEnum, _CAMXEventEnum.getEquipmentStateChange());
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(outputFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (_CAMXEventEnum == CAMXEventEnum.ITEM_TRANSFER_OUT)
        _panelSerialNumber = "";
      fileWriterUtil.close();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void writeCAMXEquipmentEventLog(CAMXEventEnum CAMxEventEnum, String projectName) throws DatastoreException
  {
    Assert.expect(CAMxEventEnum != null);

    _CAMXEventEnum = CAMxEventEnum;

    _dateTime = _format.format(new Date()).replaceAll("(.*)(\\d\\d)$", "$1:$2");
    String FileNameTimeStamp = _dateTime.replace(":", ".");
    String systemType = _xrayTester.getMachineDescription();

    if (projectName != null)
    {
      _recipeName = projectName;
    }

    String dir = Directory.getCAMXDir();
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);

    String outputFileName = "";

    if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_STARVED)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EqStarved", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_UNSTARVED)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EqUnstarved", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_INITIALIZATION_COMPLETE)
    {
      outputFileName = FileName.getCAMXOutputFilePath("equipmentInitializationComplete", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_SETUP_COMPLETE)
    {
      outputFileName = FileName.getCAMXOutputFilePath("equipmentSetupComplete", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_START_SELECTED)
    {
      outputFileName = FileName.getCAMXOutputFilePath("equipmentStartSelected", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_SETUP_SELECTED)
    {
      outputFileName = FileName.getCAMXOutputFilePath("equipmentSetupSelected", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_POWER_OFF)
    {
      outputFileName = FileName.getCAMXOutputFilePath("equipmentPowerOff", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_RECIPE_SELECTED)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EquipmentRecipeSelected", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_RECIPE_READY)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EquipmentRecipeReady", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_ALARM)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EquipmentAlarm", FileNameTimeStamp);   
    }
    else if (_CAMXEventEnum == _CAMXEventEnum.EQUIPMENT_ERROR)
    {
      outputFileName = FileName.getCAMXOutputFilePath("EquipmentError", FileNameTimeStamp);   
    }
    else
      outputFileName = FileName.getCAMXOutputFilePath(_CAMXEventEnum.toString(), FileNameTimeStamp);

    FileWriterUtil fileWriterUtil = new FileWriterUtil(outputFileName, false);
    try
    {
      fileWriterUtil.open();
      fileWriterUtil.write("<" + _CAMXEventEnum.toString() + _SPACE);
      if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_INITIALIZATION_COMPLETE)
      {
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_SOFTWARE_REV + "=\"" + Version.getVersion() + "\"" + _SPACE);  
        fileWriterUtil.write(_HARDWARE_REV + "=\"" + systemType + "\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_SETUP_SELECTED || _CAMXEventEnum == CAMXEventEnum.EQUIPMENT_START_SELECTED || 
          _CAMXEventEnum == CAMXEventEnum.EQUIPMENT_POWER_OFF)
      {
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_EVENT_INITIATOR + "=\"" + _currentUserName + "\"");  
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_RECIPE_SELECTED)
      {
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_RECIPE_ID + "=\"" + _recipeName + "\"" + _SPACE);    
        fileWriterUtil.write(_LANE_LIST + "=\"1\"" + _SPACE);
        fileWriterUtil.write(_ZONE_LIST + "=\"1\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_RECIPE_READY)
      {
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_RECIPE_ID + "=\"" + _recipeName + "\"" + _SPACE);    
        fileWriterUtil.write(_LANE_LIST + "=\"1\"" + _SPACE);
        fileWriterUtil.write(_ZONE_LIST + "=\"1-7\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_SELECTED_RECIPE_MODIFIED)
      {
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_RECIPE_ID + "=\"" + _recipeName + "\"" + _SPACE);    
        fileWriterUtil.write(_LANE_LIST + "=\"1\"" + _SPACE);
        fileWriterUtil.write(_ZONE_LIST + "=\"1-7\"" + _SPACE);
        fileWriterUtil.write(_ACTION + "=\"MODIFY\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_ERROR)
      {            
        int lower = 10000;
        int upper = 99999;
        _errorInstanceId = (int) (Math.random() * (upper - lower)) + lower;

        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_ERROR_ID + "=\"" + "HardwareException" + "\"" + _SPACE);    
        fileWriterUtil.write(_ERROR_INSTANCE_ID + "=\"" + _errorInstanceId + "\"" + _SPACE);
        fileWriterUtil.write(_LANE_LIST + "=\"1\"" + _SPACE);
        fileWriterUtil.write(_ZONE_LIST + "=\"1-4\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_ERROR_CLEARED)
      {  
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_ERROR_INSTANCE_ID + "=\"" + _errorInstanceId + "\"");
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_ALARM)
      {            
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
        fileWriterUtil.write(_ALARM_ID + "=\"" + "HardwareException" + "\"" + _SPACE);    
        fileWriterUtil.write(_ALARM_INSTANCE_ID + "=\"" + _errorInstanceId + "\"" + _SPACE);
        fileWriterUtil.write(_ALARM_TYPE + "=\"EQUIPMENTSAFETY\"" + _SPACE);
        fileWriterUtil.write(_LANE_LIST + "=\"1\"" + _SPACE);
        fileWriterUtil.write(_ZONE_LIST + "=\"1-4\"");
        setEquipmentErrorOn(true);
      }
      else if (_CAMXEventEnum == CAMXEventEnum.EQUIPMENT_ALARM_CLEARED)
      {  
        if (_isErrorOn)
        {
          fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
          fileWriterUtil.write(_ALARM_INSTANCE_ID + "=\"" + _errorInstanceId + "\"");
          setEquipmentErrorOn(false);
          _errorInstanceId = -1;
        }
      }
      else
        fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"");

      setEquipmentStateChange(_CAMXEventEnum, _CAMXEventEnum.getEquipmentStateChange());
      fileWriterUtil.write("><Extensions></Extensions></" + _CAMXEventEnum.toString() + ">"); 
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(outputFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void setEquipmentErrorOn(boolean isErrorOn) throws DatastoreException
  {
    _isErrorOn = isErrorOn;  
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void setEquipmentStateChange(CAMXEventEnum currentEventId, String equipmentStateChangeEnum) throws DatastoreException
  {
    if (equipmentStateChangeEnum == null)
      return;
    
    if (_CAMXEquipmentState.equals(equipmentStateChangeEnum) == false)
      writeEquipmentStateChangeLog(currentEventId, equipmentStateChangeEnum);
      
    _CAMXEquipmentState = equipmentStateChangeEnum;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void writeEquipmentStateChangeLog(CAMXEventEnum currentEventId, String equipmentStateChangeEnum) throws DatastoreException
  {
    CAMXEventEnum cAMXEventEnum = CAMXEventEnum.EQUIPMENT_CHANGE_STATE;
    _dateTime = _format.format(new Date()).replaceAll("(.*)(\\d\\d)$", "$1:$2");
    String FileNameTimeStamp = _dateTime.replace(":", ".");

    String dir = Directory.getCAMXDir();
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);

    String outputFileName = FileName.getCAMXOutputFilePath("EqChangeState", FileNameTimeStamp);

    FileWriterUtil fileWriterUtil = new FileWriterUtil(outputFileName, false);
    try
    {
      fileWriterUtil.open();
      fileWriterUtil.write("<" + cAMXEventEnum.toString() + _SPACE);
      fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
      fileWriterUtil.write(_CURRENT_STATE + "=\"" + equipmentStateChangeEnum + "\"" + _SPACE);
      fileWriterUtil.write(_PREVIOUS_STATE + "=\"" + _CAMXEquipmentState + "\"" + _SPACE);
      fileWriterUtil.write(_EVENT_ID + "=\"" + currentEventId.toString() + "\"");
      fileWriterUtil.write("><Extensions></Extensions></" + cAMXEventEnum.toString() + ">"); 
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(outputFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }
    
  /**
   * @author Khaw Chek Hau
   */
  public void setZoneId(int zoneId) throws DatastoreException
  {
    if (zoneId == -1)
      return;
    
    if (zoneId != _zoneId)
    {
      writeItemTransferZone(zoneId);
    }
    _zoneId = zoneId;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void writeItemTransferZone(int zoneId) throws DatastoreException
  {
    CAMXEventEnum cAMXEventEnum = CAMXEventEnum.ITEM_TRANSFER_ZONE;
    _dateTime = _format.format(new Date()).replaceAll("(.*)(\\d\\d)$", "$1:$2");
    String FileNameTimeStamp = _dateTime.replace(":", ".");

    String dir = Directory.getCAMXDir();
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);

    String outputFileName = FileName.getCAMXOutputFilePath("BoardTransferCompleted", FileNameTimeStamp);

    FileWriterUtil fileWriterUtil = new FileWriterUtil(outputFileName, false);
    try
    {
      fileWriterUtil.open();
      fileWriterUtil.write("<" + cAMXEventEnum.toString() + _SPACE);
      fileWriterUtil.write(_DATE_TIME + "=\"" + _dateTime + "\"" + _SPACE);
      fileWriterUtil.write(_ITEM_INSTANCE_ID + "=\"" + _panelSerialNumber + "\"" + _SPACE);
      fileWriterUtil.write(_FROM_ZONE_ID + "=\"" + _zoneId + "\"" + _SPACE);
      fileWriterUtil.write(_TO_ZONE_ID + "=\"" + zoneId + "\"" + _SPACE);
      fileWriterUtil.write(_LANE_ID + "=\"1\"");
      fileWriterUtil.write("><Extensions></Extensions></" + cAMXEventEnum.toString() + ">"); 
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(outputFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public void setLaneId(int laneId) 
  {    
    _laneId = laneId;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public int getLaneId() 
  {    
    return _laneId;
  }     
  
  /**
   * @author Khaw Chek Hau
   */
  public void setLoginId(String loginId) 
  {    
    _currentUserName = loginId;
  }    
}
