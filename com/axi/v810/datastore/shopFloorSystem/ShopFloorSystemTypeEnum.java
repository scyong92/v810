package com.axi.v810.datastore.shopFloorSystem;

/**
 * Enumeration of shop floor system
 *
 * @author Khaw Chek Hau
 */
public class ShopFloorSystemTypeEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  public static final ShopFloorSystemTypeEnum CAMX = new ShopFloorSystemTypeEnum(_index++);
  public static final ShopFloorSystemTypeEnum SECSGEM = new ShopFloorSystemTypeEnum(_index++);

  /**
   * @author Khaw Chek Hau
   */
  private ShopFloorSystemTypeEnum(int id)
  {
    super(id);
  }
}
