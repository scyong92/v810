package com.axi.v810.datastore.shopFloorSystem;

import java.io.*;

/**
 * @author Khaw Chek Hau
 * @XCR3534: v810 SECS/GEM protocol implementation
 */
public class SECSGEMEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static int _index = -1;
  private String _secsgemEvent;
  
  public static final SECSGEMEventEnum CURRENT_ACTIVE_RECIPE = new SECSGEMEventEnum(++_index, "CurrentActiveRecipe");
  
  /**
   * @author Khaw Chek Hau
   */
  private SECSGEMEventEnum(int id, String secsgemEvent)
  {
    super(id);
    _secsgemEvent = secsgemEvent;
  }

  /**
   * @author Khaw Chek Hau
   */
  public String toString()
  {
    return String.valueOf(_secsgemEvent);
  }
}
