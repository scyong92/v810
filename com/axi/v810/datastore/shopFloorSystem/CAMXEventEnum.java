package com.axi.v810.datastore.shopFloorSystem;

import java.io.*;
import java.util.*;

/**
 * @author Khaw Chek Hau
 */
public class CAMXEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static Map<String, CAMXEventEnum> _CAMXEventToEnumMap = new HashMap<String, CAMXEventEnum>();
  private static int _index = -1;
  private int _zoneId = -1;
  private String _equipmentStateChange;
  private String _CAMXEvent;
  
  public static final CAMXEventEnum ITEM_WORK_START = new CAMXEventEnum(++_index, 3, "READY-PROCESSING-EXECUTING", "ItemWorkStart"); //Start inpsection
  public static final CAMXEventEnum ITEM_WORK_COMPLETE = new CAMXEventEnum(++_index, 4, "READY-PROCESSING-ACTIVE", "ItemWorkComplete"); //End inspection
  public static final CAMXEventEnum ITEM_TRANSFER_IN = new CAMXEventEnum(++_index, -1, "READY-PROCESSING-ACTIVE", "ItemTransferIn"); //Load Panel 
  public static final CAMXEventEnum ITEM_TRANSFER_OUT = new CAMXEventEnum(++_index, -1, "READY-IDLE-STARVED", "ItemTransferOut"); //Unload Panel
  public static final CAMXEventEnum ITEM_PROCESS_STATUS = new CAMXEventEnum(++_index, -1, null, "ItemProcessStatus"); //Inspection result
  public static final CAMXEventEnum ITEM_WORK_ABORT = new CAMXEventEnum(++_index, 4, null, "ItemWorkAbort"); //Abort inspection
  public static final CAMXEventEnum ITEM_TRANSFER_ZONE = new CAMXEventEnum(++_index, -1, null, "ItemTransferZone"); //Zone change
  public static final CAMXEventEnum ITEM_IDENTIFIER_READ = new CAMXEventEnum(++_index, 1, null, "itemIdentifierRead"); //Serial number is read 
  
  public static final CAMXEventEnum EQUIPMENT_INITIALIZATION_COMPLETE = new CAMXEventEnum(++_index, -1, "SETUP", "EquipmentInitializationComplete"); //V810 software started
  public static final CAMXEventEnum EQUIPMENT_SETUP_COMPLETE = new CAMXEventEnum(++_index, -1, "READY-PROCESSING-ACTIVE", "EquipmentSetupComplete"); //CDA complete
  public static final CAMXEventEnum EQUIPMENT_SETUP_SELECTED = new CAMXEventEnum(++_index, -1, "SETUP", "EquipmentSetupSelected"); //After login, show login name
  public static final CAMXEventEnum EQUIPMENT_START_SELECTED = new CAMXEventEnum(++_index, -1, "READY-PROCESSING-ACTIVE", "EquipmentStartSelected"); //CDA complete & show login info
  public static final CAMXEventEnum EQUIPMENT_POWER_OFF = new CAMXEventEnum(++_index, -1, "OFF", "EquipmentPowerOff"); //Exit V810 or log off
  public static final CAMXEventEnum EQUIPMENT_RECIPE_SELECTED = new CAMXEventEnum(++_index, -1, null, "EquipmentRecipeSelected"); //Open Recipe 
  public static final CAMXEventEnum EQUIPMENT_RECIPE_READY = new CAMXEventEnum(++_index, -1, null, "EquipmentRecipeReady"); //After recipe is opened sucessfully 
  public static final CAMXEventEnum EQUIPMENT_SELECTED_RECIPE_MODIFIED = new CAMXEventEnum(++_index, -1, null, "EquipmentSelectedRecipeModified"); //Recipe status has been changed 
  public static final CAMXEventEnum EQUIPMENT_ERROR = new CAMXEventEnum(++_index, -1, "DOWN", "EquipmentError"); //V810 CDA exception 
  public static final CAMXEventEnum EQUIPMENT_ERROR_CLEARED = new CAMXEventEnum(++_index, -1, null, "EquipmentErrorCleared"); //V810 CDA exception resolved    
  public static final CAMXEventEnum EQUIPMENT_ALARM = new CAMXEventEnum(++_index, -1, "DOWN", "EquipmentAlarm"); 
  public static final CAMXEventEnum EQUIPMENT_ALARM_CLEARED = new CAMXEventEnum(++_index, -1, null, "EquipmentAlarmCleared");  
  public static final CAMXEventEnum EQUIPMENT_STARVED = new CAMXEventEnum(++_index, -1, null, "EquipmentStarved"); //Equipment is looking for next item loaded     
  public static final CAMXEventEnum EQUIPMENT_UNSTARVED = new CAMXEventEnum(++_index, -1, null, "EquipmentUnStarved"); //Equipment is not looking for next item loaded     
  public static final CAMXEventEnum EQUIPMENT_CHANGE_STATE = new CAMXEventEnum(++_index, -1, null, "EquipmentChangeState"); 
    
  /**
   * @author Khaw Chek Hau
   */
  private CAMXEventEnum(int id, int zoneId,  String equipmentStateChange,String CAMXEvent)
  {
    super(id);
    _zoneId = zoneId;
    _equipmentStateChange = equipmentStateChange;
    _CAMXEvent = CAMXEvent;
  }

  /**
   * @author Khaw Chek Hau
   */
  public String toString()
  {
    return String.valueOf(_CAMXEvent);
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public int getZoneId()
  {
    return _zoneId;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public String getEquipmentStateChange()
  {
    return _equipmentStateChange;
  }
}
