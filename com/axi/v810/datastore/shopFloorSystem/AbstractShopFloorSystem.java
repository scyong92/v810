package com.axi.v810.datastore.shopFloorSystem;

import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.*;
import com.axi.util.*;
import java.io.*;

/**
 * @author Khaw Chek Hau
 */
public abstract class AbstractShopFloorSystem 
{
  private static AbstractShopFloorSystem _instance; 
  private static ShopFloorSystemTypeEnum _shopFloorSystemTypeEnum;
  protected static Config _config;
  
  static
  {
    _config = Config.getInstance();
  }

  /**
   * @author Khaw Chek Hau
   */
  public static synchronized AbstractShopFloorSystem getInstance()
  {
    if (_instance == null)
    {
      String shopFloorSystemType = _config.getStringValue(SoftwareConfigEnum.SHOP_FLOOR_SYSTEM_TYPE);
      if (shopFloorSystemType.equalsIgnoreCase("CAMX"))
      {
        _instance = new CAMXShopFloorSystem();
        _shopFloorSystemTypeEnum = _shopFloorSystemTypeEnum.CAMX;
      }
      else if (shopFloorSystemType.equalsIgnoreCase("SECSGEM"))
      {
        _instance = new SECSGEMSystem();
        _shopFloorSystemTypeEnum = _shopFloorSystemTypeEnum.SECSGEM;
      }
      else
      {
        Assert.expect(false, "unknown shopFloorSystemType from config file, SoftwareConfigEnum bug?");
      }
    }
    return _instance;
  }

  /**
   * @author Khaw Chek Hau
   */
  public static ShopFloorSystemTypeEnum getShopFloorSystemTypeEnum()
  {
    return _shopFloorSystemTypeEnum;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void inspectionStart() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void inspectionEnd() throws DatastoreException;

  /**
   * @author Khaw Chek Hau
   */
  public abstract void loadPanel() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void unloadPanel() throws DatastoreException;

  /**
   * @author Khaw Chek Hau
   */
  public abstract void inspectionAbort() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void getSerialNumber(String serialNumber) throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void softwarePowerUp() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void startUpCompleted() throws DatastoreException;
   
  /**
   * @author Khaw Chek Hau
   */
  public abstract void startUpCompletedWithUserId() throws DatastoreException;

  /**
   * @author Khaw Chek Hau
   */
  public abstract void login() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void setLoginUserName(String userName) throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void softwarePowerOff() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void recipeLoading(String projectToLoad) throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void recipeLoaded(String projectToLoad) throws DatastoreException;  
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void recipeModified() throws DatastoreException;  
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentError() throws DatastoreException;  
    
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentAlarm() throws DatastoreException;  
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentErrorCleared() throws DatastoreException;  
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentAlarmCleared() throws DatastoreException; 
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentStarved() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void equipmentUnStarved() throws DatastoreException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void executeCAMxProgram() throws IOException;
  
  /**
   * @author Khaw Chek Hau
   */
  public abstract void terminateCAMxProgram() throws IOException;
}
