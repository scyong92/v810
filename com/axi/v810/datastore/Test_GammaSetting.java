package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Vincent Wong
 */
public class Test_GammaSetting extends UnitTest
{
  private String _fullpath = "display.cfg"; 

  //----------------------------------------
  // Main.
  //
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_GammaSetting() );
  }

  //----------------------------------------
  // Test procedure.
  //
  public void test( BufferedReader is, PrintWriter os )
  {
    GammaSetting gammaSetting = new GammaSetting( _fullpath );
    double gamma = 0.0;

    //----------------------------------------
    // Remove the config file if it is already there.
    //
    File configFile = new File( _fullpath );

    if ( configFile.exists() )
      configFile.delete(); // remove the config file


    //----------------------------------------
    // The config is not present, there should be a FileNotFoundDatastoreException.
    //
    try
    {
      gamma = gammaSetting.getValue();
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( true );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( false,
      "FileCorruptDatastoreException not expected in case of config file not present." );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
      "DatastoreException not expected in case of config file not present." );
    }


    //----------------------------------------
    // Do it again. The config is still not present,
    // there should be an IOException.
    //
    try
    {
      gamma = gammaSetting.getValue();
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( true );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( false,
      "FileCorruptDatastoreException not expected in case of config file not present." );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
      "DatastoreException not expected in case of config file not present." );
    }


    //----------------------------------------
    // Set value. It should work. A config file should be created.
    //
    try
    {
      gamma = 2.0;
      gammaSetting.setValue( gamma );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "DatastoreException not expected when calling setValue()." );
    }

    Expect.expect( configFile.exists(), "Config file is missing." );


    //----------------------------------------
    // Set the same value again. It should still work.
    // The config file should still be there.
    //
    try
    {
      gamma = 2.0;
      gammaSetting.setValue( gamma );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "DatastoreException not expected when calling setValue()." );
    }

    Expect.expect( configFile.exists(), "Config file is missing." );

    //----------------------------------------
    // Read the value back. It should still work.
    // The config file should still be there.
    //
    try
    {
      gamma = 0.0; // invalidate the value
      gamma = gammaSetting.getValue();
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }

    Expect.expect( gamma == 2.0, "Unexpected gamma value returned." );
    Expect.expect( configFile.exists(), "Config file is missing." );


    //----------------------------------------
    // Try to write and read back some unlikely values.
    // Since there is no checking, it should work.
    //
    try
    {
      gammaSetting.setValue( 0.0 );
      gamma = gammaSetting.getValue();
      Expect.expect( gamma == 0.0, "Unexpected gamma value returned." );

      gammaSetting.setValue( 2 ); // it should work with integer
      gamma = gammaSetting.getValue();
      Expect.expect( gamma == 2, "Unexpected gamma value returned." );

      gammaSetting.setValue( -9999.0 ); // ridiculous but it should work 
      gamma = gammaSetting.getValue();
      Expect.expect( gamma == -9999.0, "Unexpected gamma value returned." );

      gammaSetting.setValue( 19990.9 ); // ditto
      gamma = gammaSetting.getValue();
      Expect.expect( gamma == 19990.9, "Unexpected gamma value returned." );
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "DatastoreException not expected when calling setValue()." );
    }


    //----------------------------------------
    // If the config file is read-only, there should be a DatastoreException.
    //
    try
    {
      gammaSetting.setValue( 1.0 ); // set a value
      gamma = gammaSetting.getValue();
      Expect.expect( gamma == 1.0, "Unexpected gamma value returned." );

      configFile.setReadOnly();
      gammaSetting.setValue( 3.2 );
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( false,
        "Exceptions not expected when config file is present." );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( true );
    }
   

    //----------------------------------------
    // Try to read some invalid contents from an arbitrary file.
    // IOException is expected.
    //
    GammaSetting gammaSetting2 = new GammaSetting( "testorder" );

    try
    {
      gamma = gammaSetting2.getValue();
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( false, "FileNotFoundDatastoreException not expected." );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( true );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false, "DatastoreException not expected." );
    }


    //----------------------------------------
    // Create an empty config file. getValue() should return the default gamma
    // value and then put the value in the file.
    //
    File tempFile = null;

    try
    {
      tempFile = File.createTempFile( "temp", "cfg" ); // empty file
    }
    catch ( IOException e )
    {
      Expect.expect( false, "Unable to create temp file for testing." ); 
    }

    GammaSetting gammaSetting3 = new GammaSetting( tempFile.getAbsolutePath() );

    try
    {
      gamma = gammaSetting3.getValue();
    }
    catch ( FileNotFoundDatastoreException e )
    {
      Expect.expect( false, "FileNotFoundDatastoreException not expected." );
    }
    catch ( FileCorruptDatastoreException e )
    {
      Expect.expect( true );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false, "DatastoreException not expected." );
    }

    if ( tempFile != null )
      tempFile.delete();
   

    //----------------------------------------
    // Clean up after the test.
    //
    if ( configFile.exists() )
      configFile.delete(); // remove the config file
  }
}
