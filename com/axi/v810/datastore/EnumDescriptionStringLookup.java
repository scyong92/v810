package com.axi.v810.datastore;


import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum;
/**
 *
 * @author sham
 */
public class EnumDescriptionStringLookup
{

  private static EnumDescriptionStringLookup _instance;
  private Map<AlgorithmSettingEnum, String> _algorithmSettingEnumToStringMap = new HashMap<AlgorithmSettingEnum, String>();
  private Map<String, AlgorithmSettingEnum> _stringToAlgorithmSettingEnumMap = new HashMap<String, AlgorithmSettingEnum>();

  /**
   * @author Bill Darbie
   * @author sham
   */
  public static synchronized EnumDescriptionStringLookup getInstance()
  {
    if (_instance == null)
    {
      _instance = new EnumDescriptionStringLookup();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   * @author George A. David
   * @author sham
   */
  private EnumDescriptionStringLookup()
  {


    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION, "This threshold sets to enable or disable joint based edge detection with graylevel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD, "This threshold determines maximum acceptable solder thickness, as a percentage of maximum thickness to find diameter edge for Pad Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL, "This threshold determines maximum acceptable solder thickness, as a percentage of maximum thickness to find diameter edge for Midball Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE, "This threshold determines maximum acceptable solder thickness, as a percentage of maximum thickness to find diameter edge for Package Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE, "This threshold sets to determine expected distance between two adjacent solder ball centers, as a percentage of pitch.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, "This threshold determines the technique used to detect edge.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD, "This threshold sets the expected ball diameter for Pad Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD, "This threshold sets the expected ball thickness for Pad Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL, "This threshold sets the expected ball diameter for Midball Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL, "This threshold sets the expected ball thickness for Midball Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE, "This threshold sets the expected ball diameter for Package Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE, "This threshold sets the expected ball thickness for Package Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION, "This threshold defines the background region inner edge location,as a percentage of the distance between joints.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION, "This threshold defines the background region outer edge location,as a percentage of the distance between joints.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS, "This threshold sets the expected degrees of rotation for calculated eccentricity axis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS, "When enable, Fixed Eccentricity Axis value used to calculate eccentricity axis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT , "When enable, all slices will included in shorts analysis."); //lam
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD, "This threshold sets the minimum acceptable diameter  of joint for Pad Slice, as a percentage of the Nominal Diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL, "This threshold sets the minimum acceptable diameter  of joint for Midball Slice, as a percentage of the Nominal Diameter");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE, "This threshold sets the minimum acceptable diameter  of joint for Package Slice, as a percentage of the Nominal Diameter");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD, "This threshold sets the maximum acceptable diameter  of joint for Pad Slice, as a percentage of the Nominal Diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL, "This threshold sets the maximum acceptable diameter  of joint for Midball Slice, as a percentage of the Nominal Diameter");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE, "This threshold sets the maximum acceptable diameter  of joint for Package Slice, as a percentage of the Nominal Diameter");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD, "This threshold sets to determine the maximum acceptable neighbor outlier for Pad Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL, "This threshold sets to determine the maximum acceptable neighbor outlier for Midball Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE, "This threshold sets to determine the maximum acceptable neighbor outlier for Package Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD, "This threshold sets to determine the minimum acceptable neighbor outlier for Pad Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL, "This threshold sets to determine the minimum acceptable neighbor outlier for Midball Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE, "This threshold sets to determine the minimum acceptable neighbor outlier for Package Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_THICKNESS, "This threshold determines maximum acceptable thickness of joint, as a percentage of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD, "This threshold sets to determine the maximum acceptable outlier region for Pad Slice, as a percentage of image/region nominal pad diameter.");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL, "This threshold sets to determine the maximum acceptable outlier region for Midball Slice, as a percentage of image/region nominal midball diameter.");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE, "This threshold sets to determine the maximum acceptable outlier region for Package Slice, as a percentage of image/region nominal package diameter.");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD, "This threshold sets to determine the minimum acceptable outlier region for Pad Slice, as a percentage of image/region nominal pad diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL, "This threshold sets to determine the minimum acceptable outlier region for Midball Slice, as a percentage of image/region nominal midball diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE, "This threshold sets to determine the minimum acceptable outlier region for Package Slice, as a percentage of image/region nominal package diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER, "This threshold sets to determine the maximum acceptable percentage of joints with marginal diameter, as a percentage of total number of joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL, "This threshold sets to determine the minimum acceptable diameter, as a percentage of nominal diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD, "This threshold sets to determine minimum acceptable of eccentricity for Pad Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD, "This threshold sets to determine maximum acceptable of eccentricity for Pad Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL, "This threshold sets to determine minimum acceptable of eccentricity for Midball Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL, "This threshold sets to determine maximum acceptable of eccentricity for Midball Slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER, "This threshold sets to determine the maximum acceptable void limit for neighbor outlier, as a percentage of the diameter that is voided. If a joint has this level of voiding, or larger, then any Neighbor Outlier calls will be ignored.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD, "This threshold sets to determine maximum acceptable hip outlier for Pad Slice, as a fraction of inter quartile range.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD, "This threshold sets to determine minimum acceptable hip outlier for Pad Slice, as a fraction of inter quartile range.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED, "This threshold sets to determine the maximum acceptable offset of joint. Measured offset is compared with surrounding joints within the image or region.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION, "This threshold sets to determine the maximum acceptable offset of joint. Measured position is compared to the CAD position.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD, "This threshold sets the minimum acceptable thickness of joint for Pad Slice, as a percentage of nominal thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE, "This threshold sets the minimum acceptable thickness of joint for Package Slice, as a percentage of nominal thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL, "This threshold sets the minimum acceptable thickness of joint for Midball Slice, as a percentage of nominal thickness.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD, "This threshold sets the compensation method on profile, especially with solder having voids.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION, "When turn on, it will reduces the sensitivity of void detection. It really just enlarge the profile across to 100% to average the void.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY, "This threshold sets the window kernel of Gaussian smoothing, the higher the value, the smoother the profile thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH, "This threshold sets to determine the profile width, as a percentage of the pad length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH, "This threshold sets to determine the profile length, as a percentage of the pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION, "This threshold determines the distance between background profile from which the average gray levels will be taken into account in the calculation of solder thickness, as a percentage of interpad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET, "This threshold customize the distance between background profile from which the average gray levels will be taken into account in the calculation of solder thickness, it will override 'Background Region Location' when have value > 0.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine the location of heel edge search thickness, as a percentage of maximum pad thickness along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, "This threshold determines the search range of heel edge, as a percentage of maximum pad length along. A higher value extends the search region further towards the right of the solder profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, "This threshold sets to determine the distance of center from heel, as a percentage of the pin length. A higher value pushes the center towards the toe but the positions of the toe peak and toe edge are not affected.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH, "This threshold determines the distance from heel peak to toe peak.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine the location of toe edge search thickness, as a percentage of maximum pad thickness along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, "This threshold sets to determine the expected fillet length, as a fraction of joint fillet length.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, "This threshold determines the expected fillet thickness of solder joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, "This threshold sets the acceptable heel thickness, as a fraction of solder joint thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS, "This threshold sets the acceptable toe thickness, as a fraction of solder joint thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION, "gullwingNominalHeelPosition");//not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "This threshold sets to determine the minimum acceptable heel thickness of joint, as percentage of nominal thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "This threshold sets to determine the minimum acceptable fillet length of joint, as percentage of nominal fillet length.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "This threshold sets to determine the maximum acceptable fillet length of joint, as percentage of nominal fillet length.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT, "This threshold sets to determine maximum acceptable thickness ratio of center relative to heel, as a percentage of center thickness relative to heel thickness.");
    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is less than the set value.");
      
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL, "This threshold sets to determine the minimum acceptable fillet thickness, as a percentage of nominal fillet thickness.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL, "This threshold sets to determine the maximum acceptable heel position distance from nominal, as a percentage of Pad Length Along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD, "This threshold sets to determine maximum acceptable distance between actual joint location with cad position.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS, "This threshold sets to determine maximum acceptable distance between fillet peak position with expected fillet peak position get from neighbours' region.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION, "This threshold sets to determine across profile location, as a percentage of fillet length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH, "This threshold sets to determine across profile width, as a percentage of fillet length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE, "This threshold sets to determine side fillet center offset from edge, as percentage of distance between side fillet edges.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine side fillet edge thickness, as percentage of max pad profile thickness across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE, "This threshold sets to determine side fillet search distance, as percentage of distance between side edges.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS, "This threshold define background regions to enable.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH, "This threshold sets to determine the expected background region width, as a fraction of inter pad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE, "This threshold determines whether to use location information from CAD or from Locator to define pad center.");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER, "This measurement specifies the nominal heel-to-pad-center distance for the joint.");//not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL, "This threshold specifies the maximum absolute difference of the heel-to-pad-center measurement compared to it's nominal value. If the absolute difference between this measurement and the nominal value is greater than this threshold the joint is failed for misalignment.");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT, "This threshold examines the center of mass shift across the joint as a percentage of the pad width. If the absolute value of the center of mass shift is greater than this percentage the joint is failed for misalignment.");//not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS, "This threshold sets to determine the maximum acceptable thickness ratio of center relative to heel across joint, as a percentage of center thickness relative to heel thickness across joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS, "This threshold sets to determine the maximum acceptable thickness ratio of center relative to second heel across joint, as a percentage of center thickness relative to second heel thickness across joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS, "This threshold sets to determine the minimum acceptable concavity ratio across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE, "This threshold sets to determine minimum acceptable leading edge slope between heel fillet edge and heel. The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, "This threshold sets to determine the minimum acceptable sum of slope changes, as the sum of changes in slope along the entire joint profile. Most good joints have relatively steep fillet edges and sharp slope changes at the termination and pad interfaces.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES, "This threshold determines the minimum acceptable slope changes, as the sum of changes in slope along the joint profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE, "This threshold sets to determine the minimum acceptable leading edge slope between toe fillet edge and toe.The joint is considered open if the measured value is less than the set value.");
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE, "This threshold sets to determine maximum acceptable leading edge slope between heel fillet edge and heel. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, "This threshold sets to determine the maximum acceptable sum of slope changes, as the sum of changes in slope along the entire joint profile. Most good joints have relatively steep fillet edges and sharp slope changes at the termination and pad interfaces.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES, "This threshold determines the maximum acceptable slope changes, as the sum of changes in slope along the joint profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE, "This threshold sets to determine the maximum acceptable leading edge slope between toe fillet edge and toe.The joint is considered open if the measured value is less than the set value.");
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE, "This threshold allows users to define additional inspectable slice.");

    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is less than the set value.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.TEMP_SETTING, "temp");//not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, "This threshold sets the width of the locator search region as a percentage of the pad length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, "This threshold sets the length of the locator search region as a percentage of the pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ACROSS, "This threshold changes the final located position of the pad across the pad as a percentage of pad length across. A positive value for this threshold shifts the located position to the left relative to the pin orientation. A negative number shifts the located position to the right relative to the pin orientation.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, "This threshold sets the search distance for the rectangular locator template along the joint, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, "This threshold sets the search distance for the rectangular locator template across the joint, as a percentage of pad length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_BORDER_WIDTH, "This threshold sets to define the size of the border around the joint to use.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE, "This threshold sets to determine the expected snapback distance. If the located position varies by more than the set threshold from the average location for an inspection region, we move the located position back to the average location.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_METHOD, "This threshold defines the method used to locate region of interest.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE, "This threshold sets to determine slices used to inspection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION, "When enable, it uses the same Minimum Short Thickness threshold to flag shorts defects");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, "This threshold sets to determine the minimum short thickness required to detect short for protrusion slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS, "This threshold sets to determine the minimum short thickness required to detect short for 2D/projection slice.");//Broken Pin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION, "When enable, high short detection analysis will be enable.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT, "This threshold sets to determine the expected slice height for high short detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, "This threshold sets to determine the minimum short thickness required to detect short.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS, "This threshold sets the maximum acceptable thickness of joint, as a fraction of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, "This threshold sets the minimum acceptable short thickness of a joint on Component Side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, "This threshold sets the minimum acceptable short thickness of a joint on Pin Side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, "This threshold determines the inner edge region, as a percentage of Inter Pad Distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, "This threshold determines the outer edge region, as a percentage of Inter Pad Distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION, "This threshold determines the region reference used in short analysis. Located Position is recommended due to the slight variations in location of components in different images. CAD Position may have a tendency to increase the number of false shorts.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD, "When enable, the shorts profile consists of test region head is included in shorts analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL, "When enable, the shorts profile consists of test region tail is included in shorts analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT, "When enable, the shorts profile consists of test region right is included in shorts analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT, "When enable, the shorts profile consists of test region left is included in shorts analysis.");
    //Siew Yeng - XCR-3318 - Oval PTH
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS, "When enable, the shorts profile consists of test region corner is included in shorts analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER, "When enable, the shorts profile consists of test region inner corner is included in shorts analysis.");
    
    //Lim, Lay Ngor - XCR1743:Benchmark
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER, "When enable, the shorts profile consists of test region center is included in shorts analysis for solder ball detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION, "When enable, the shorts analysis for solder ball detection will consists of multiple ROI for center short region.");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS, "This threshold sets the maximum acceptable thickness of solder ball detected compare to its learn background in the center of the clear chip, as a fraction of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH, "This threshold determines the percentage of center effective length of clear chip for solder ball detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH, "This threshold determines the percentage of center effective width of clear chipfor solder ball detection.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION, "This threshold sets the minimum detectable numbers of pixels in short ROI region.");
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH, "This threshold sets the minimum detectable short length of joint.");    
    //Lim, Lay Ngor - XCR2100 - Broken Pin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN, "To enable the Broken Pin detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE, "This threshold sets the maximum acceptable broken pin area size in pixels.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO, "This threshold sets the ratio of broken pin threshold.");

    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICE_OBSOLETE, "throughholeBarrelSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT, "This threshold determines the expected protrusion slice height. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the protrusion slice away from the board. The direction depends on whether the component is loaded on the top side or bottom side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT, "This threshold determines the expected insertion slice height. The slice height is taken with reference to the specified Component Side Slice Height.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, "This threshold determines the expected slice height for analyzing high shorts on the component side of the joint, as a percentage of barrel height. The slice height is taken with reference to the specified Pin Side Slice Height.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, "This threshold determines the expected slice height for analyzing high shorts on the component side of the joint, as thickness unit. The slice height is taken with reference to the specified Pin Side Slice Height.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT, "This threshold determines the expected pin side slice height, as a percentage of the through-hole depth, which is approximately the board/panel thickness. Increasing the percentage moves the pin side reference further into the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION, "This threshold sets to determine slice to detect.");
    //mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_USE_FOR_SURFACE_MODELING_OBSOLETE, "throughholeUseForSurfaceModeling");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH, "This threshold sets to determine the expected diameter of the pin.");
    //Siew Yeng - XCR-3318 - Oval PTH
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ALONG, "This threshold sets to determine the expected size along of the pin.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ACROSS, "This threshold sets to determine the expected size across of the pin.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL, "This threshold sets to determine the expected pin search diameter, as a percentage of pin diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for pin side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST, "This threshold determines the expected diameter to test, as a percentage of joint diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION, "This threshold defines the background region inner edge location,as a percentage of the distance between joints.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION, "This threshold defines the background region outer edge location,as a percentage of the distance between joints.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_EXTRUSION_SLICE_OBSOLETE, "throughholeExtrusionSlice");//not in use
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for protrusion slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for insertion slice.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for protrusion slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for protrusion slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for insertion slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for insertion slice.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for pin side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for pin side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE, "This threshold enables or disable the wetting coverage of throughhole.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE, "This threshold sets to determine the minimum acceptable percentage for wetting coverage of throughhole.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY, "This threshold sets to determine the sensitivity of thresholding for wetting coverage of throughhole. Decreasing the value will increases the processing time.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE, "This threshold sets to determine the maximum acceptable percentage for voiding of throughhole.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE, "This threshold sets to determine the minimum acceptable degree of coverage of throughhole.");
 
    /**
     * @author Lim, Lay Ngor PIPA
     */
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE, "This threshold sets to determine the maximum acceptable percentage for void volume of throughhole.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT, "This threshold enables or disable the void volume measurement for throughhole.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN, "This threshold enables or disable the void volume direction of throughhole. Set TRUE for direction from component side to pin slice(PIP), set FALSE for direction from pin to component side slice(PTH)");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for component side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for component side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for component side.");
   
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE, "This threshold sets to determine the maximum acceptable excess solder signal for pin side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10, "This threshold sets to determine the maximum acceptable excess solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE, "This threshold sets to determine the maximum acceptable excess solder signal for component side.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST, "This threshold determines the expected excess diameter to test, as a percentage of joint diameter.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH, "This threshold sets to define the expected pin width.");
    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_BARREL_SLICE_OBSOLETE, "pressfitBarrelSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, "This threshold sets to determine the expected pad fillet thickness for opaque type joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS, "This threshold sets to determine the expected pad fillet thickness for clear type joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT, "This threshold defines the location of the background profiles relative to the default position. The distance is measured as a fraction of the pad width starting at the center of the pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, "This threshold sets to determine the expected body thickness required for test as opaque.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, "This threshold sets the expected body length in equivalent mils of solder of the body for an opaque component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH, "This threshold sets the expected body length in equivalent mils of solder of the body for an clear component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, "This threshold sets the expected search length for Open Signal. If this threshold changed, remember to click Update Nominal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET, "This threshold determines the points of maximum solder thickness on the joints, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, "This threshold defines the distance of the lower fillet from the Inner Fillet location, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT, "This threshold sets to determine the expected component side thickness, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS, "This threshold sets to determine the expected pad fillet thickness to measure, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX, "This threshold sets to determine the maximum acceptable component tilt ratio for opaque type component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE, "This threshold sets to determine the technique used to locate edge for opaque type component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD, "This threshold determines method used to update nominal fillet thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR, "This threshold determines expected offset along joint for Clear Component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE, "This threshold determines expected offset along joint for Opaque Component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION, "This threshold sets to determine the expected nominal shift rotation.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG, "This threshold sets to define final position profile along.");
    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE, "This threshold sets to determine the component across profile size, in percentage.");
    //Lim, Lay Ngor - Clear Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CENTER_OFFSET, "This threshold sets to determine the distance of center from upper fillet, as a percentage of the upper to lower fillet length. A higher value pushes the center towards the lower fillet but the positions of the lower fillet peak and lower fillet edge are not affected.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD, "This threshold sets to determine the method of upper fillet detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine the location of upper fillet edge search thickness, as a percentage of maximum pad thickness along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE, "This threshold determines the search range of upper fillet edge, as a percentage of maximum pad length along. A higher value extends the search region further towards the right or left of the solder profile.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_LENGTH, "This threshold sets to determine the minimum acceptable body length, as a percentage of Nominal Component Body Length. The joint passes if its calculated value is greater than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_BODY_LENGTH, "This threshold sets to determine the maximum acceptable body length, as a percentage of Nominal Component Body Length. The joint passes if its calculated value is lower than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_OPAQUE_OPEN_SIGNAL, "This threshold sets the maximum acceptable Opaque Open Signal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_OPEN_SIGNAL, "This threshold sets the minimum acceptable Clear Open Signal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_FILLET_GAP, "This threshold sets to determine the maximum acceptable fillet gap, as a second derivative of the solder profile. Joint passes if the measured value is less than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_THICKNESS, "This threshold sets to determine the minimum acceptable body thickness for opaque type. The joint passes if the measured value is more than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS, "This threshold sets to determine the minimum acceptable body thickness for clear type. The joint passes if the measured value is more than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_WIDTH, "This threshold sets to determine the minimum acceptable body width across. Component passes if measured value is higher than set threshold.");
    //Lim, Lay Ngor - Clear Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE, "This threshold sets to determine maximum acceptable thickness ratio of center relative to upper fillet, as a percentage of center thickness relative to upper fillet thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE, "This threshold sets to determine maximum acceptable solder area percentage compare to pad size. The joint passes if the measured value is less than the set threshold.");
    //ShengChuan - Clear Tombstone, LayNgor - Move from open to measurement tab.
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD, "This threshold sets to determine the detection method use for double checking the open joint.[Recommend to use Roundness Calculation]");        
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE, "This threshold sets to determine minimum acceptable template match percentage compare to the learn pad. The joint passes if the measured value is more than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE, "This threshold sets to determine maximum acceptable pad roundness percentage. The rounder the detected pad will increase the chances of the pad to be an open joint . The joint passes if the measured value is less than the set threshold.");
    //Lim, Lay Ngor - Opaque Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION, "This threshold sets to determine the enable or disable of opaque fillet width detection."); //XCR3359
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO, "This threshold sets to determine the minimum acceptable fillet width across ratio for joint one & joint two. The joint passes if measured value is higher than set threshold and also detected partial fillet thickness is higher than partial fillet thickness percentage. This threshold works with Component Side Search Thickness store at Measurement-Additional tab.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE, "This threshold sets to determine the minimum acceptable partial fillet thickness, as a percentage of body thickness for each joint. The joint passes if measured value is higher than set threshold.");    

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG, "This threshold sets to determine the maximum acceptable component shift along, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS, "This threshold sets to determine the maximum acceptable component shift across, as a percentage of pad length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION, "This threshold sets to determine the maximum acceptable component rotation, in degree.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE, "This threshold sets to determine the maximum acceptable misalignment between pad1 along Offset and nominal along shift for opaque type.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR, "This threshold sets to determine the maximum acceptable misalignment between pad1 along Offset and nominal along shift for clear type.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, "This threshold sets to determine the minimum acceptable fillet thickness, as a percentage of nominal fillet thickness. The joint passes if its calculated value is greater than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS, "This threshold sets the maximum acceptable fillet thickness of joint, as a percentage of Nominal Fillet Thickness.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS, "This threshold sets to determine the expected thickness of pad one.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS, "This threshold sets to determine the expected thickness of pad two.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, "This threshold sets to determine the expected open signal search length, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET, "This threshold determines the points of maximum solder thickness on the joints of a polarized capacitor, as a percentage of pad length along. A more positive value brings the 2 yellow lines closer together towards the middle of the component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET, "This threshold determines the points under the component just inside the solder joint, as a percentage of pad length along. A more positive value pushes the 2 black lines closer together towards the middle of the component.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT, "This threshold sets to determine the expected fillet edge, as a percentage of maximum pad 1 thickness along. A higher value pushes the Upper Fillet, Lower Fillet and Outer Fillet Edge Percent towards the middle of the component on each joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT, "This threshold sets to determine the final location of background profile which the average gray levels will be taken in order to calculate solder thickness., as a percentage of interpad distance.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL, "This threshold determines the minimum acceptable polarity signal. A smaller polarity value means the part is rotated incorrectly. A more positive value means the part is rotated correctly. The joint fails if its calculated value is less than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS, "This threshold determines whether the pcap misalignment is measured by Asymmetry method or Slug Edge Detection method.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD, "This threshold determines the edge of pcap slug measured by the gray value across the cmponent profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS, "This threshold determines the height of the LHS/RHS region of interest.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG, "This threshold determines the width of the LHS/RHS region of interest.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION, "This threshold determines the position of the LHS/RHS region of interest");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, "This threshold determines the minimum acceptable fillet thickness, as a percentage of nominal fillet thickness. The joint fails if its calculated value is less than the set threshold.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS, "This threshold determines the maximum acceptable fillet thickness, as a percentage of nominal fillet thickness. The joint fails if its calculated value is more than the set threshold.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL, "This threshold sets to determine the minimum acceptable open signal for pad one.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL, "This threshold sets to determine the minimum acceptable open signal for pad two.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_MINIMUM_SLUG_THICKNESS, "This threshold sets to determine the minimum acceptable slug thickness.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD, "This threshold sets to determines the void analysis method used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD, "This threshold sets to determine sensitivity used for floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD, "This threshold sets to determine joint area used for median method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD, "This threshold sets to determine the percentage of diameters to be included in void analysis for Pad Slice, as a percentage of the measured diameter of the joint. Increasing the value will increase the amount of the diameter to be analyzed for voids.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE, "This threshold sets to determine the percentage of diameters to be included in void analysis for Package Slice, as a percentage of the measured diameter of the joint. Increasing the value will increase the amount of the diameter to be analyzed for voids.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL, "This threshold sets to determine the percentage of diameters to be included in void analysis for Midball Slice, as a percentage of the measured diameter of the joint. Increasing the value will increase the amount of the diameter to be analyzed for voids.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD, "This threshold sets the maximum acceptable thickness of joint for Pad Slice, as a fraction of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE, "This threshold sets the maximum acceptable thickness of joint for Package Slice, as a fraction of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL, "This threshold sets the maximum acceptable thickness of joint for Midball Slice, as a fraction of Nominal Thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD, "This threshold sets the maximum acceptable void area of joint for Pad Slice, as a percentage of total joint area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE, "This threshold sets the maximum acceptable void area of joint for Package Slice, as a percentage of total joint area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL, "This threshold sets the maximum acceptable void area of joint for Midball Slice, as a percentage of total joint area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD, "This threshold sets to determine marginal void area of joint for Pad Slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE, "This threshold sets to determine marginal void area of joint for Package Slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL, "This threshold sets to determine marginal void area of joint for Midball Slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD, "This threshold sets to determine the maximum acceptable percent of voiding fail for pad slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE, "This threshold sets to determine the maximum acceptable percent of voiding fail for package slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL, "This threshold sets to determine the maximum acceptable percent of voiding fail for midball slice, as percent of joints in subtype that are voided. Increasing this value allows more joints to be flagged as Marginal Failing before component is deemed as failing.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION, "This threshold define the level of noise reduction used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE, "This threshold sets to determine the number of image use to create median image for voiding analysis.");

    //@author Jack Hwee- individual void
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD, "This threshold sets the maximum acceptable individual void area of joint for Pad Slice, as a percentage of total joint area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE, "This threshold sets the maximum acceptable individual void area of joint for Package Slice, as a percentage of total joint area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL, "This threshold sets the maximum acceptable individual void area of joint for Midball Slice, as a percentage of total joint area.");
    //Siew Yeng - XCR-3515 - Void Diameter
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD, "This threshold sets the maximum acceptable individual void diameter of joint for Pad Slice, as a percentage of Pad slice joint diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE, "This threshold sets the maximum acceptable individual void diameter of joint for Package Slice, as a percentage of Package slice joint diameter.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL, "This threshold sets the maximum acceptable individual void diameter of joint for Midball Slice, as a percentage of Diameter slice joint diameter.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH,"This threshold sets to determine the expected pad width along joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT,"This threshold sets to determine the expected pad height along joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT,"This threshold sets to determine the expected percentage of inter pad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT,"This threshold defines the location of the background profiles relative to the default position.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD, "This threshold sets to determine the expected background region of pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD, "This threshold sets to determine expected void thickness threshold of pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD, "This threshold sets to determine the maximum acceptable percentage of joint voiding. If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as being voided. This value is usually larger than the component-level voiding.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD, "This threshold sets to determine the maximum acceptable percentage of area fail, as a percentage of total area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION, "This threshold define the level of noise reduction used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL, "This threshold define the level of noise reduction used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD, "This threshold sets to determine to use standard method or customize method in untested border size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE, "This threshold sets to determine the expected untested border size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET, "This threshold sets to determine the offset value from the left side of joint ROI.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET, "This threshold sets to determine the offset value from the bottom side of joint ROI.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET, "This threshold sets to determine the offset value from the right side of joint ROI.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET, "This threshold sets to determine the offset value from the top side of joint ROI.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE, "This threshold sets to determine the expected background gray level, as percentage of the gray levels.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD, "This threshold sets to determine the maximum acceptable voiding, as percentage of total pixels tested. If the area of pixels classified as voiding is greater than this percent, the joint will indicted as being voided.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE, "This threshold sets to determine the acceptable gap distance for joint indivual voiding. Increasing gap distance will combine the nearby void within the gap distance as one void area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION, "This threshold sets to determine to enable or disable this sub region.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, "This threshold sets to determine the algorithm version to use. \nAlgorithm Version 2 is the latest voiding algorithm with added subregion feature. \nAlgorithm Version 1 does not support subregion feature.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD, "This threshold sets to determines the void analysis method used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE, "This threshold sets to determine sensitivity used for image layer 1 floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER, "This threshold sets to determine the number of images to be AND together in floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES, "This threshold sets to determines whether to use image masking for void analysis method used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR, "This threshold sets to determine the radius of Gaussian Blur used for FloodFil algorithm.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO, "This threshold sets to determine sensitivity used for image layer 2 floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE, "This threshold sets to determine sensitivity used for image layer 3 floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL, "This threshold sets to determine sensitivity used for image layer Pad Border floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD, "This threshold sets to use multi-thresholder from imageJ for floodfill method voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL, "This threshold sets to use polynomial fitting from imageJ for floodfill method voiding analysis. The void area for this method varies with Untested Border size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD, "This threshold sets the method use to locate the mask image with odd-shaped single pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY, "This threshold sets to determine the outer region of pad for voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY, "This threshold sets to determine sensitivity used to detect voiding in variable pad.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG, "This threshold sets to determine the minimum acceptable leading slope along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG, "This threshold sets to determine the minimum acceptable trailing slope along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG, "This threshold sets to determine the minimum acceptable sum of slopes along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS, "This threshold sets to determine the minimum acceptable leading slope across pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS, "This threshold sets to determine the minimum acceptable trailing slope across pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS, "This threshold sets to determine the minimum acceptable sum of slopes across pad.");
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG, "This threshold sets to determine the maximum acceptable leading slope along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG, "This threshold sets to determine the maximum acceptable trailing slope along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG, "This threshold sets to determine the maximum acceptable sum of slopes along pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS, "This threshold sets to determine the maximum acceptable leading slope across pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS, "This threshold sets to determine the maximum acceptable trailing slope across pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS, "This threshold sets to determine the maximum acceptable sum of slopes across pad.");
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS, "This threshold sets to determine the minimum acceptable thickness, as a fraction of nominal thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES, "This threshold determines the maximum acceptable slope changes, as the sum of changes in slope along the joint profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR, "This threshold sets to determine the expected smoothing factor.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION, "This threshold sets to determine the percentage of area to used for histogram profiling.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD, "This threshold sets to determine the method use for insufficient calculation and detection.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY, "This threshold sets to determine the floodfill sensitivity for detecting insufficient area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD, "This threshold sets to determine the minimum acceptable insufficient area for pad.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE, "This threshold sets to determine the expected untested border size.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION, "When turned ON, gray levels beyond the area bounded by white rectangle are taken into account.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH, "This threshold sets to determine the profile width, as a percentage of the pad length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH, "This threshold sets to determine the profile length, as a percentage of the pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION, "This threshold determines the distance between background profile from which the average gray levels will be taken into account in the calculation of solder thickness, as a percentage of interpad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET, "This threshold customize the distance between background profile from which the average gray levels will be taken into account in the calculation of solder thickness, it will override 'Background Region Location' when have value > 0.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine the location of heel edge search thickness, as a percentage of maximum pad distance along. A higher positive number would put the heel edge closer to the center of the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, "This threshold determines the search range of heel edge, as a percentage of as a percentage of Heel-to-Toe edge distance.A larger positive number extends the brown region from the heel edge towards the center of the joint profile to search for the heel peak.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET, "This threshold sets to determine the distance of center from heel, as a percentage of the pin length. A higher value pushes the center towards the toe but the positions of the toe peak and toe edge are not affected.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, "This threshold sets to determine the expected toe edge thickness as a percent of nominal toe thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH, "This threshold sets to determine the expected fillet length, as a fraction of joint fillet length.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS, "This threshold determines the expected fillet thickness of solder joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS, "This threshold sets the acceptable heel thickness, as a fraction of solder joint thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS, "This threshold sets the acceptable toe thickness, as a fraction of solder joint thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS, "This threshold sets the acceptable center thickness, as a fraction of solder joint thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE, "This threshold determines whether Thickness or Slope techniques are used to determine the fillet edge locations and thus fillet length. If Thickness technique used, the edges are determined by the Heel Edge Search Thickness and Toe Edge Search Thickness parameters. If Slope technique is selected, the fillet edges are determined based on the maximum slope calculated.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE, "This threshold define the technique used to determine feature location.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET, "This threshold determines the expected location of toe, as a percentage of heel-toe edge distance(fillet length).");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE, "This threshold marks the beginning of the cyan region to measure upward curvature, as a percentage of heel-to-toe edge distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE, "This threshold marks the end of the cyan region to measure upward curvature, as a percentage of heel-to-toe edge distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE, "This threshold define the technique used to determine toe location.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE, "This threshold sets the fixed location of toe, toe is located at the specified number of mils away from the detected heel peak.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE, "This threshold sets the method used to locate the center.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH, "This threshold sets to to remove noise and smoothen out an erratic joint profile, as a number from 1 to 12. A higher positive number increases the smoothing level as seen in the images below.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_TECHNIQUE, "This threshold define the technique used to determine background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION, "This threshold sets to determine the expected solder thickness location, as a percentage of pad length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME, "This threshold sets to determine the expected  center of Solder Volume.");//not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "This threshold sets to determine minimum acceptable length, as a percentage of fillet length. The joint fails open if the measured percentage is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "This threshold sets to determine maximum acceptable length, as a percentage of fillet length. The joint fails open if the measured percentage is more than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, "This threshold sets to determine maximum acceptable center thickness, as a percentage of nominal center thickness. The joint fails open if the measured percentage is more than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL, "This threshold sets to determine maximum acceptable of thickness between toe and center, depending on how the Thickness Multipliers are configured.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE, "This threshold sets to determine the minimum acceptable upward curvature. An open or defective joint, in contrast, may exhibit a lower value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE, "This threshold sets to determine minimum acceptable leading edge slope between heel fillet edge and heel. The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES, "This threshold determines the minimum acceptable slope changes, as the sum of changes in slope along the joint profile.");
    //Lim, Lay Ngor & Kee Chin Seong - XCR1648: Add Maximum Slope Limit - START    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE, "This threshold sets to determine maximum acceptable leading edge slope between heel fillet edge and heel. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, "This threshold sets to determine the maximum acceptable sum of slope changes, as the sum of changes in slope along the entire joint profile. Most good joints have relatively steep fillet edges and sharp slope changes at the termination and pad interfaces.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES, "This threshold determines the maximum acceptable slope changes, as the sum of changes in slope along the joint profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, "This threshold sets to determine the maximum acceptable heel across leading-trailing slope, as the sum of the leading and trailing slopes along the width across heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM, "This threshold sets to determine the maximum acceptable sum of slopes across heel.");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE, "This threshold sets to determine the maximum acceptable leading edge slope between toe fillet edge and toe.The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE, "This threshold measured as the grey level per pixel along the total fillet applied to the center (default value is 100).The joint is considered open if the measured value is greater than the set value.");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, "This threshold sets to determine the maximum acceptable sum of the leading and trailing slopes along the width across heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM, "This threshold sets to determine the maximum acceptable sum of slopes across center.");    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, "This threshold sets to determine the minimum acceptable heel across leading-trailing slope, as the sum of the leading and trailing slopes along the width across heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM, "This threshold sets to determine the minimum acceptable sum of slopes across heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH, "This threshold sets to determine the minimum acceptable center across width, as a distance between center across trailing edge and center across leading edge.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT, "This threshold sets to determine heel weight for calculates open signal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT, "This threshold sets to determine toe weight for calculates open signal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT, "This threshold sets to determine center weight for calculates open signal.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS, "This threshold sets to determine the minimum acceptable heel sharpness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE, "This threshold sets to determine the minimum acceptable leading edge slope between toe fillet edge and toe.The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE, "This threshold measured as the grey level per pixel along the total fillet applied to the center (default value is -1000).The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, "This threshold sets to determine the minimum acceptable sum of slope changes, as the sum of changes in slope along the entire joint profile. Most good joints have relatively steep fillet edges and sharp slope changes at the termination and pad interfaces.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH, "This threshold defines the distance between the leading and trailing edges of the heel across profile.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, "This threshold measured as the sum of the leading and trailing slopes along the width across heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM, "This threshold sets to determine the minimum acceptable sum of slopes across center.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE, "This threshold sets to determine the  maximum acceptable difference between neighbor fillet length and fillet length.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT, "This threshold sets to determine the maximum acceptable ration of center to toe.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT, "This threshold sets to determine the maximum acceptable ration of center to heel.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST, "When enabled,toe test perform if Center to Heel Thickness Test Fail.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION, "This threshold sets to determine the minimum acceptable solder at user specified location.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL, "This threshold sets to determine the maximum acceptable center of solder volume location percent of nominal.");//not in use
    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable fillet length region outlier percentage. The joint is considered open if the measured value is less than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the maximum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is greater than the set value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "This threshold sets to determine the minimum acceptable center thickness region outlier percentage. The joint is considered open if the measured value is less than the set value.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL, "This threshold determines the minimum acceptable fillet thickness, as a percentage of nominal fillet thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "This threshold determines the minimum acceptable heel thickness, as a percentage of nominal heel thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, "This threshold determines the minimum acceptable center thickness, as a percentage of nominal center thickness.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL, "This threshold determines the minimum acceptable toe thickness, as a percent of nominal toe thickness.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_VOIDING_MAXIMUM_VOID_PERCENT, "This threshold determines the maximum acceptable pad area, as a percentage of pad area.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_VOIDING_NOISE_REDUCTION, "This threshold define the level of noise reduction used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, "This threshold determines the maximum acceptable pad area for individual void, as a percentage of pad area.");

    //EXPOSED PAD : MEASUREMENT
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS, "This threshold sets to determine expected number of measurement profiles across the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS, "This  threshold sets the expected position of profile across the joint, as a percentage of pad width across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS, "This threshold sets to determine expected profile width across the joint, as a percentage of pad width across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS, "This threshold sets the expected numbers of gap across the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS, "This threshold determines the type of background profile across used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS, "This threshold sets to determine expected number of measurement profiles along the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG, "This  threshold sets the expected position of profile along the joint, as a percentage of pad width along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG, "This threshold sets to determine expected profile width along the joint, as a percentage of pad width along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS, "This threshold sets the expected numbers of gap along the joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG, "This threshold determines the type of background profile along used.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS, "This threshold sets to determine location of the background profiles across the joint, as a percentage of interpad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS, "This threshold sets to determine the size of the search area for profile gaps across joint. The search area is nominally centered in a segment of the profile determined by the expected number of gaps to be measured.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS, "This threshold sets to determine the final position to locate the Outer Gap and Inner Gap properly across the joint, as a percentage of pad width across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG, "This threshold sets to determine location of the background profiles along the joint, as a percentage of interpad distance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG, "This threshold sets to determine the size of the search area for profile gaps along joint. The search area is nominally centered in a segment of the profile determined by the expected number of gaps to be measured.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG, "This threshold sets to determine the final position to locate the Outer Gap and Inner Gap properly along the joint, as a percentage of pad width along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS, "This threshold sets to determine the expected areas that have solder.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE, "This threshold sets to determine the inner gap edges. The inner gap will be detected if value of (0.5*maximum slope ) is less than Gap Falloff Slope.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE, "This threshold sets to determine the inner gap edges. The inner gap will be detected if current slope is less than Gap Falloff Slope.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH, "This threshold sets to reduce noise in joint profile. By increasing the Profile Smoothing Length a less erratic profile may be obtained.");
    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CTR_SEARCH_DIST_OBSOLETE, "exposedPadMeasurementCenterSearchDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE, "This threshold determines the type of background profile used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE, "This threshold sets to determine the expected background value, as a fraction of grey level.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT, "When turn on, contrast enhancement will be applied.");

    // EXPOSED PAD : OPEN
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS, "This threshold sets to determine the maximum acceptable percentage inner gap length of the outer gap across joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS, "This threshold sets to determine maximum acceptable outer gap length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS, "This threshold sets to determine minimum acceptable outer gap thickness across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG, "This threshold sets to determine the maximum acceptable percentage inner gap length of the outer gap along joint.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG, "This threshold sets to determine maximum acceptable outer gap length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG, "This threshold sets to determine minimum acceptable outer gap thickness along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS, "This threshold sets to determine maximum acceptable inner gap length across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG, "This threshold sets to determine maximum acceptable inner gap length along.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES, "This threshold sets the maximum acceptable numbers of failures.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS, "This threshold sets to determine minimum acceptable filled gap thickness across.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG, "This threshold sets to determine minimum acceptable filled gap thickness along.");

    // EXPOSED PAD : INSUFFICIENT
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_INSUFFICIENT_MAX_VOID_PERCENT, "exposedPadInsufficientMaximumVoidPercent");//not in use

    // EXPOSED PAD : VOIDING
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT, "This threshold sets to determine the maximum acceptable void area, as a percentage of total area tested.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS, "This threshold determines the expected solder thickness of joint for voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE, "This threshold sets to determine maximum acceptable gray level difference between a pixel in the image under analysis and the learned image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA, "This threshold sets to determine maximum acceptable void area. If the total area of contiguous voided pixels grouped together is greater than this value, the group of pixels is considered a void.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION, "This threshold sets to determine expected level of noise reduction. Higher level of noise reduction will decrease the sensitivity of voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE, "This threshold determines the technique used for voiding analysis.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, "exposedPadIndividualVoidingMaximumVoidPercent"); //not in use

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, "This threshold changes final position of Pad Slice. A positive offset moves the pad slice away from the board; a negative offset moves the pad slice into the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, "This threshold defines BGA lower pad slice height.");//not use in algo, used by test generation
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PACKAGE_SLICEHEIGHT, "This threshold changes final position of Package Slice. A positive offset moves the pad slice away from the board; a negative offset moves the pad slice into the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_SLICEHEIGHT, "This threshold changes final position of Midball Slice. A positive offset moves the pad slice away from the board; a negative offset moves the pad slice into the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PCAP_SLUG_SLICEHEIGHT, "This threshold defines the pcap slug slice height.");//not use in algo, used by test generation
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT, "This threshold defines opaque chip slice height.");//not use in algo, used by test generation
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT, "This threshold defines clear chip slice height.");//not use in algo, used by test generation
    // Swee Yee Wong - enable user defined midball to edge offset for grid array
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE, "Use user defined offset value from midball to pad and package focus slice height, set off to use autofocus method.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT, "This threshold changes final position of Pad and Package Slice. Larger offset moves the pad and package further from midball, smaller offset moves the pad and package nearer to midball.");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT, "When enable, slice height predicted from surrounding.");

    // added by wei chin 14-Jul
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT, "When turn on, contrast enhancement will be applied.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ALONG, "This threshold changes the final located position of the pad along the pad as a percentage of pad length along.");

    // Added by Khang Wah, 2013-09-11, user defined initial wavelet level
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, "Improve system speed when set to fast, improve image quality when set to slow");

    // Added by Wei Chin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.BACKGROUND_SENSITIVITY_VALUE, "Customize background sensitivity threshold");
    
    // Added by Lim, Lay Ngor
    //Image Enhancer
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE, "This threshold sets to enable the application of resize on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE, "This threshold sets to determines the resize scale for the image. Value larger than 1 is to enlarge the image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE, "This threshold sets to enable the application of CLAHE on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE, "This threshold sets to determines the object block size of CLAHE on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE, "This threshold sets to enable the application of background filter on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR, "This threshold sets to determine the iteration of background filter to be apply on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH, "This threshold sets to determines the object width of background filter on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH, "This threshold sets to determines the object length of background filter on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE, "This threshold sets to enable the application of band pass filter on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE, "This threshold sets to determines the size of large structures to be filter out.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE, "This threshold sets to determines the size of small structures to be filter out.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES, "This threshold sets to enable the filter out of horizontal or vertical noise on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE, "This threshold sets to determine the percentage of pixels in image that are allowed to become saturated.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION, "This threshold determines the percentage of tolerance direction which respect to the selected suppress stripes technique.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE, "This threshold sets to enable the application of R filter on image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE, "This threshold sets to save enhanced image to repair station.");//Siew Yeng - XCR-2683

    // Added by Lim, Seng Yew
    // Graylevel normalization feature - normalize recon images immediately after received from IRP, and before classification.
    // TODO: This feature has a limitation. If there is one reconstructed image with 2 or more very different shading (means graylevel), then the normalization will not be effective.
    //       Need to implement a way to exclude this very different background. Alternatively we can set min and max graylevel manually.
    // WARNING: Take note that using this feature will give inaccurate thickness value.
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE, "Graylevel normalization feature - This threshold enables the normalization of reconstructed images immediately after received from IRP but before classification. WARNING: Take note that using this feature will give inaccurate thickness value but still consistent across same subtype.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM, "Graylevel normalization feature - This threshold is the minimum graylevel used to normalize reconstructed images. Normalization of reconstructed images will be done immediately after received from IRP but before classification.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM, "Graylevel normalization feature - This threshold is the maximum graylevel used to normalize reconstructed images. Normalization of reconstructed images will be done immediately after received from IRP but before classification.");

    // Added by Lee Herng, 2014-08-13, search range limit
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_LOCAL_SEARCH, "This threshold enable/disable PSP Local Search function. When set to On, system will perform localized AutoFocus search based on surface map value. When set to Off, system will directly use surface map value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT, "This threshold sets the minimum PSP search range. It only take effect when PSP is used.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT, "This threshold sets the maximum PSP search range. It only take effect when PSP is used.");
    
    // Added by Lee Herng, 2016-08-10, psp z-offset
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_Z_OFFSET, "This threshold sets the Z-offset that controls the initial PSP value. It only take effect when PSP is used.");
    
    // Added by Lee Herng, 2014-08-22
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION, "This threshold allows users to control whether to apply joints re-test feature.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE, "This threshold allows users to define additional inspectable slice for Grid Array family.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE, "This threshold allows users to define additional inspectable slice for single pad or LGA family.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, "This threshold allows users to define the offset value used to control the starting point of the mid-board. This value will affect the AutoFocus performance.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION, "This threshold enable/disable the inclusion of neighours that fail HIP defect as part of neighbour outlier calculation. Set it to ON to exclude HIP neighbours. Set it to OFF to include HIP neighbours.");
        
    // Added by Wei Chin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, "This threshold defined the correct offset fot reference from plane (z-height value of the bottom surface board).");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, "Defined the slice setup unit, either in percentage of board thickness or the actual thickness value.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, "This threshold defined the number of slices needed to inspect.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10, "This threshold determines the expected barrel slice height, as thickness unit. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board. ( in thickness unit )");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10, "This threshold determines the expected barrel slice height, as a percentage of through-hole depth. Slice height is taken with reference to the specified Pin Side Slice Height. Increasing this value moves the slice towards the component side of the board.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10, "This threshold sets to determine the maximum acceptable different value compared to nominal solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10, "This threshold sets to determine the maximum acceptable different value compared to region solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL, "This threshold sets to determine the expected solder signal for barrel slice.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL, "This threshold sets to determine the expected corrected gray level, in number of gray levels. Corrected means comparing to a white background.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the nominal, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10, "This threshold sets to determine the maximum acceptable difference between corrected gray level compared to the regional average, as a fraction of corrected gray level. If the difference is more than this set value, the joint fails Open.");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL, "This threadhold sets to determine the maximum fillet thickness of excess in nominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "This threadhold sets to determine the maximum heel thickness of excess in nominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_TOE_THICKNESS_PERCENT_OF_NOMINAL, "This threadhold sets to determine the maximum toe thickness of excess in nominal");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID, "When set this threshold more than 0, the voiding joint more than this percent will be failed at component level"); 
    
    // Added by Lee Herng 12 Sept 2014
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, "This threshold sets user-defined slice 1 height offset value from midball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, "This threshold sets user-defined slice 2 height offset value from midball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, "This threshold sets user-defined slice 3 height offset value from midball");
    
    //Wei Chin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES, "This threshold enable save diagnostic image to repair station.");
    
    //Siew Yeng - XCR-3094
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS, "This threshold enable save all joint image when fail component level.");
    
    // Kok Chun, Tan - Motion Blur
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE, "This threshold enable Motion Blur filter feature.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION, "This threshold is the direction of filter apply to image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE, "This threshold is the filter kernel size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET, "This threshold is the graylevel offset when the filter apply.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION, "This threshold is the number of itertion the filter apply.");
    
    // Kok Chun, Tan - Shading Removal
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE, "This threshold enable Motion Blur filter feature.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION, "This threshold is the direction of filter apply to image.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE, "This threshold is the filter kernel size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE, "This threshold is the filter kernel size.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE, "This threshold is the filtering technique used in removing the shading.");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND, "This threshold is the background graylevel.");
    
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION, "This threshold sets to enable the artifact based region seperation in horizontally or vertically.");
    
    buildStringToEnumMaps();
  }

  /**
   * @author Bill Darbie
   */
  private void buildStringToEnumMaps()
  {
    for (Map.Entry<AlgorithmSettingEnum, String> entry : _algorithmSettingEnumToStringMap.entrySet())
    {
      AlgorithmSettingEnum prev = _stringToAlgorithmSettingEnumMap.put(entry.getValue(), entry.getKey());
     // Assert.expect(prev == null);
    }
  }

  /**
   * @author Bill Darbie
   */
  public String getAlgorithmSettingString(AlgorithmSettingEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _algorithmSettingEnumToStringMap.get(theEnum);
   // Assert.expect(string != null);
    return string;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author sham
   */
  private void mapAlgorithmSettingEnumToString(AlgorithmSettingEnum algorithmSettingEnum, String string)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(string != null);
    Assert.expect(_algorithmSettingEnumToStringMap != null);

   _algorithmSettingEnumToStringMap.put(algorithmSettingEnum, string);

  }
}
