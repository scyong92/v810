package com.axi.v810.datastore;

import java.io.*;
import java.util.regex.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.datastore.config.Config;
import java.util.LinkedList;

/**
 * @author Erica Wheatcroft
 */
public class ScanPathDefinitionTableReader
{
  private static final String _UNSIGNED_INT_PATTERN_TEXT = "\\d+";
  // @todo fix this regarding localization
  private static final String _DIRECTION_PATTERN_TEXT = "forward|reverse";
  private static final String _UNITS_PATTERN_TEXT = "nanometers";
  private static final String _COMMENT_PATTERN_TEXT = "\\s*#.*";
  private static final String _DELIMITER_PATTERN_TEXT = "\\t";

  private static Pattern _scanPathPattern = Pattern.compile("^" +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _DIRECTION_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _DIRECTION_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNSIGNED_INT_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "(" + _UNITS_PATTERN_TEXT + ")" + _DELIMITER_PATTERN_TEXT +
                                                            "$");

  private static Pattern _commentPattern = Pattern.compile("^" + _COMMENT_PATTERN_TEXT + "$");

  private static Pattern _checkSumPattern = Pattern.compile("^" + _UNSIGNED_INT_PATTERN_TEXT + "$");

  private Adler32 _checksumUtil = new Adler32();

  /**
   * @author Erica Wheatcroft
   */
  public ScanPathDefinitionTableReader()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void read(javax.swing.JTable table, String fileName) throws DatastoreException
  {
    Assert.expect(table != null);
    Assert.expect(fileName != null);

    com.axi.v810.gui.service.ScanPathDefinitionTableModel model =
                                (com.axi.v810.gui.service.ScanPathDefinitionTableModel) table.getModel();   
    read(model, fileName);
  }
  /**
   * @author Anthony Fong - Split the function to accept javax.swing.JTable or ScanPathDefinitionTableModel
   */
  public void read(com.axi.v810.gui.service.ScanPathDefinitionTableModel model, String fileName) throws DatastoreException
  {    
    Assert.expect(model != null);
    Assert.expect(fileName != null);
    BufferedReader reader = null;
    try
    {

      reader = new BufferedReader(new FileReader(fileName));
      String line = reader.readLine();
      if(line != null)
      {
        while (line != null)
        {
          Matcher commentMatcher = _commentPattern.matcher(line);
          Matcher checkSumMatcher = _checkSumPattern.matcher(line);
          Matcher scanPathMatcher = _scanPathPattern.matcher(line);

          if(commentMatcher.matches())
          {
            // update the checksum as comments are included
            _checksumUtil.update(line.getBytes());
          }
          else if(scanPathMatcher.matches())
          {
            // add the line to the checksum util
            _checksumUtil.update(line.getBytes());
            parseScanPath(model, scanPathMatcher);
          }
          else if(checkSumMatcher.matches())
          {
            String checkSumString = checkSumMatcher.group(0);
            long checkSumValue = 0;
            try
            {
              checkSumValue = StringUtil.convertStringToLong(checkSumString);
            }
            catch(BadFormatException bfe)
            {
              // this should not happen as the reg expression should have caught this
              Assert.expect(false);
            }
            if(Config.isDeveloperDebugModeOn())
            {
              // bypass checking if it's developer debug mode.
            }
            else if(checkSumValue != _checksumUtil.getValue())
            {
              FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
              throw de;
            }
            else
            {
              // the file has not been changed.. we are done .
            }
          }
          else
          {
            FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
            throw de;
          }
          line = reader.readLine();
        }
      }
    }
    catch(FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(fileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      DatastoreException de = new CannotReadDatastoreException(fileName);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      try
      {
        if(reader!=null)
          reader.close();
      }
      catch(IOException ioe)
      {
        // do nothing.
      }
    }
  }
  
  /**
   * @author Anthony Fong
   */
  public java.util.List<ScanPass> readDefinedScanPath(String fileName) throws DatastoreException
  {    
    Assert.expect(fileName != null);
    BufferedReader reader = null;
    java.util.List<ScanPass> scanPath = new LinkedList<ScanPass>();
    _checksumUtil.reset();
    try
    {

      reader = new BufferedReader(new FileReader(fileName));
      String line = reader.readLine();
      if(line != null)
      {
        while (line != null)
        {
          Matcher commentMatcher = _commentPattern.matcher(line);
          Matcher checkSumMatcher = _checkSumPattern.matcher(line);
          Matcher scanPathMatcher = _scanPathPattern.matcher(line);

          if(commentMatcher.matches())
          {
            // update the checksum as comments are included
            _checksumUtil.update(line.getBytes());
          }
          else if(scanPathMatcher.matches())
          {
            // add the line to the checksum util
            _checksumUtil.update(line.getBytes());
            scanPath.add(parseScanPathFromMatcher(scanPathMatcher));
          }
          else if(checkSumMatcher.matches())
          {
            String checkSumString = checkSumMatcher.group(0);
            long checkSumValue = 0;
            try
            {
              checkSumValue = StringUtil.convertStringToLong(checkSumString);
            }
            catch(BadFormatException bfe)
            {
              // this should not happen as the reg expression should have caught this
              Assert.expect(false);
            }
            if(Config.isDeveloperDebugModeOn())
            {
              // bypass checking if it's developer debug mode.
            }
            else if(checkSumValue != _checksumUtil.getValue())
            {
              FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
              throw de;
            }
            else
            {
              // the file has not been changed.. we are done .
            }
          }
          else
          {
            FileCorruptDatastoreException de = new FileCorruptDatastoreException(fileName);
            throw de;
          }
          line = reader.readLine();
        }
      }
    }
    catch(FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(fileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      DatastoreException de = new CannotReadDatastoreException(fileName);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      try
      {
        if(reader!=null)
          reader.close();
      }
      catch(IOException ioe)
      {
        // do nothing.
      }
    }
    
    return scanPath;
  }
  
  /**
   * @author Anthony Fong
   */
 private ScanPass parseScanPathFromMatcher(Matcher scanPathMatcher)
  {
    // create a default scan pass object
    ScanPass _scanPass = new ScanPass(); 
    StagePositionMutable _stagePositionMutable = new StagePositionMutable();
    ScanPassDirectionEnum _scanPassDirectionEnum = ScanPassDirectionEnum.FORWARD;
    ScanStepDirectionEnum _scanStepDirectionEnum = ScanStepDirectionEnum.FORWARD;
  
    int _scanPassLengthInNanometers = 0;
    int _estimatedPassLengthInNanometers = 0;
    int _estimatedStepWidthInNanometers = 0;
    
    // get the scan path
    String direction = scanPathMatcher.group(2);
    if(direction.equalsIgnoreCase(ScanPassDirectionEnum.FORWARD.toString()))
      _scanPassDirectionEnum = ScanPassDirectionEnum.FORWARD;
    else if(direction.equalsIgnoreCase(ScanPassDirectionEnum.REVERSE.toString()))
      _scanPassDirectionEnum = ScanPassDirectionEnum.REVERSE;
    else
      Assert.expect(false);

    // get the x start position
    String xStartPosition = scanPathMatcher.group(3);
    try
    {
      int xStartPositionInNanometers = StringUtil.convertStringToInt(xStartPosition);
      _stagePositionMutable.setXInNanometers(xStartPositionInNanometers);
    }
    catch(BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the y start position
    String yStartPosition = scanPathMatcher.group(4);
    try
    {
      int yStartPositionInNanometers = StringUtil.convertStringToInt(yStartPosition);
      _stagePositionMutable.setYInNanometers(yStartPositionInNanometers);
    }
    catch(BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the length
    String length = scanPathMatcher.group(5);
    try
    {
      int lengthInNanometers = StringUtil.convertStringToInt(length);
      _scanPassLengthInNanometers = lengthInNanometers;
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the estinmated length
    String estimatedlength = scanPathMatcher.group(6);
    try
    {
      int estimatedLengthInNanometers = StringUtil.convertStringToInt(estimatedlength);
      _estimatedPassLengthInNanometers = estimatedLengthInNanometers;
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the scan step direction
    String scanStepDirection = scanPathMatcher.group(7);
    if (scanStepDirection.equalsIgnoreCase(ScanStepDirectionEnum.FORWARD.toString()))
      _scanStepDirectionEnum = ScanStepDirectionEnum.FORWARD;
    else if (scanStepDirection.equalsIgnoreCase(ScanStepDirectionEnum.REVERSE.toString()))
      _scanStepDirectionEnum = ScanStepDirectionEnum.REVERSE;
    else
      Assert.expect(false);

    // get the estimated scan step length
    String estimatedScanSteplength = scanPathMatcher.group(8);
    try
    {
      int estimatedScanStepInNanometers = StringUtil.convertStringToInt(estimatedScanSteplength);
      _estimatedStepWidthInNanometers = estimatedScanStepInNanometers;
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }
   
 
    // set the starting point
    _scanPass.setStartPointInNanoMeters(_stagePositionMutable);

    // now calculate the end position
    StagePositionMutable endPosition = new StagePositionMutable();
    endPosition.setXInNanometers(_stagePositionMutable.getXInNanometers());
    if(_scanPassDirectionEnum.equals(ScanPassDirectionEnum.FORWARD))
      endPosition.setYInNanometers(_stagePositionMutable.getYInNanometers() + _scanPassLengthInNanometers);
    else
      endPosition.setYInNanometers(_stagePositionMutable.getYInNanometers() - _scanPassLengthInNanometers);

    // set the end point
    _scanPass.setEndPointInNanoMeters(endPosition);

    return _scanPass;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void parseScanPath(com.axi.v810.gui.service.ScanPathDefinitionTableModel model, Matcher scanPathMatcher)
  {
    // create an entry into the table model
    com.axi.v810.gui.service.ScanPassDefinitionTableEntry entry = model.insertRow();

    // get the scan path
    String direction = scanPathMatcher.group(2);
    if(direction.equalsIgnoreCase(ScanPassDirectionEnum.FORWARD.toString()))
      entry.setScanPassDirection(ScanPassDirectionEnum.FORWARD);
    else if(direction.equalsIgnoreCase(ScanPassDirectionEnum.REVERSE.toString()))
      entry.setScanPassDirection(ScanPassDirectionEnum.REVERSE);
    else
      Assert.expect(false);

    // get the x start position
    String xStartPosition = scanPathMatcher.group(3);
    try
    {
      int xStartPositionInNanometers = StringUtil.convertStringToInt(xStartPosition);
      entry.setXInNanometers(xStartPositionInNanometers);
    }
    catch(BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the y start position
    String yStartPosition = scanPathMatcher.group(4);
    try
    {
      int yStartPositionInNanometers = StringUtil.convertStringToInt(yStartPosition);
      entry.setYInNanometers(yStartPositionInNanometers);
    }
    catch(BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the length
    String length = scanPathMatcher.group(5);
    try
    {
      int lengthInNanometers = StringUtil.convertStringToInt(length);
      entry.setScanPassLengthInNanometers(lengthInNanometers);
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the estinmated length
    String estimatedlength = scanPathMatcher.group(6);
    try
    {
      int estimatedLengthInNanometers = StringUtil.convertStringToInt(estimatedlength);
      entry.setEstimatedPassLengthInNanometers(estimatedLengthInNanometers);
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }

    // get the scan step direction
    String scanStepDirection = scanPathMatcher.group(7);
    if (scanStepDirection.equalsIgnoreCase(ScanStepDirectionEnum.FORWARD.toString()))
      entry.setScanStepDirection(ScanStepDirectionEnum.FORWARD);
    else if (scanStepDirection.equalsIgnoreCase(ScanStepDirectionEnum.REVERSE.toString()))
      entry.setScanStepDirection(ScanStepDirectionEnum.REVERSE);
    else
      Assert.expect(false);

    // get the estimated scan step length
    String estimatedScanSteplength = scanPathMatcher.group(8);
    try
    {
      int estimatedScanStepInNanometers = StringUtil.convertStringToInt(estimatedScanSteplength);
      entry.setEstimatedStepWidthInNanometers(estimatedScanStepInNanometers);
    }
    catch (BadFormatException bfe)
    {
      // this should not happen as the reg expression and the table should have caught this value
      Assert.expect(false);
    }
  }
}
