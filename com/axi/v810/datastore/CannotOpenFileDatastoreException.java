package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file could not be opened
 * @author Bill Darbie
 */
public class CannotOpenFileDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public CannotOpenFileDatastoreException(String file)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_OPEN_FILE_KEY", new Object[]{file}));
    Assert.expect(file != null);
  }
}
