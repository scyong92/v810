package com.axi.v810.datastore;

import com.axi.util.*;

public class SQLDatastoreException extends DatastoreException
{
  /**
   * Default constructor for SQLDatabaseException
   *
   * @author Cheah Lee Herng
   */
  public SQLDatastoreException (String databaseName, String errorMessage)
  {
    super(new LocalizedString("DS_ERROR_OCCURED_WHILE_ACCESSING_DATABASE_KEY", new Object[]{databaseName, errorMessage}));
  }
}
