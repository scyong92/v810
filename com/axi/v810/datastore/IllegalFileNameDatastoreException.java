package com.axi.v810.datastore;

import java.lang.*;

import com.axi.util.*;

/**
 * If a contains illegal characters, then throw this exception.
 *
 * @author Bob Balliew
 */
public class IllegalFileNameDatastoreException extends DatastoreException
{
  /**
   * This is for illegal filename characters.
   *
   * @param filename is the filename containing illegal characters.
   * @author Bob Balliew
   */
  public IllegalFileNameDatastoreException(String filename)
  {
    super(new LocalizedString("DS_ILLEGAL_FILENAME_EXCEPTION_KEY",
                              new Object[] {filename}));

    Assert.expect(filename != null);
  }
}
