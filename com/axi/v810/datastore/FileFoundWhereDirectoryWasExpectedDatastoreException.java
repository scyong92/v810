package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class FileFoundWhereDirectoryWasExpectedDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public FileFoundWhereDirectoryWasExpectedDatastoreException(String directoryName)
  {
    super(new LocalizedString("DS_ERROR_FILE_FOUND_WHERE_DIRECTORY_WAS_EXPECTED_KEY", new Object[]{directoryName}));
    Assert.expect(directoryName != null);
  }
}
