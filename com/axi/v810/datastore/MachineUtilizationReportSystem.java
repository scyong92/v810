package com.axi.v810.datastore;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.text.*;
import java.io.*;
import java.util.regex.*;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * @author Khaw Chek Hau 
 * XCR2923 : Machine Utilization Report Tools Implementation  
 */
public class MachineUtilizationReportSystem
{
  private static MachineUtilizationReportSystem _instance; 
  private XrayTester _xrayTester;
  private ManufacturingEquipmentInterface _manufacturingEquipmentInt;
  private final SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  private final SimpleDateFormat _eventTimeFormat = new SimpleDateFormat("HH:mm:ss");
  private final SimpleDateFormat _dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private final SimpleDateFormat _defaultTimeFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");               
  private Pattern _machineUtilizationDailyEventPattern_ver1 = Pattern.compile("^([^.]+)\\,(\\w+)\\,(\\w+)\\,([^.]+)\\,(-?\\d+)\\,(-?\\d+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)\\,([^.]+)$"); 
  private Pattern _machineUtilizationDailyEventPattern_ver2 = Pattern.compile("^(\\d+\\W\\d+\\W\\d+)\\,(\\w+)\\,(N\\WA|\\w+)\\,(\\d+\\:\\d+\\:\\d+)\\,(-?\\d+)\\,(-?\\d+)\\,(N\\WA|[^,]+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(\\d+\\:\\d+\\:\\d+)\\,(N\\WA|[^,]+)\\,(\\d+\\:\\d+\\:\\d+)\\,(-?\\d+)$"); 
  private Pattern _errorTimeLogPattern = Pattern.compile("^(\\w+)\\s+(\\w+)\\s+(-?\\d+)\\s+([^.]+)\\s+(\\w+)\\s+(-?\\d+)$");   
  private String _dailyEventFileName;
  private String _errorLogFileName;
  private String _badFormatFileName;
  private String _summaryFileName;
  private UserTypeEnum _currentUserType;
  private MachineUtilizationReportEventEnum _machineEvent;
  private String _dateTime;
  private String _eventTime;
  private int _accumulatedFaultTime = 0;
  private int _differentOfDays = 0;
  private boolean _isFaultExist = false;
  private boolean _isServicing = false;
  private boolean _isProgramming = false;
  private boolean _isSetupRunning = false;
  private boolean _isEventExceedOneDayDuration = false;
  private String csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");
  private static final String _ZERO_TIME = "00:00:00";
  private static final String _ZERO_COUNT = "0";
  private static final String _ONE_DAY_TIME = "24:00:00";
  private static final String _NA = "N/A";
  private static final String _BACKUP_EXTENSION = "_v810Backup";
  private static final String _TEMP_EXTENSION = "_temp";
  private static final String _SPACE = " ";
  private static final int _DAY_END_DURATION_IN_SECONDS = 86400;
  private static final String _DAY_END_EVENT_TIME_IN_STRING = "24:00:00";
  private static final String _DAILY_EVENT_TITLE_VER_1 = "Date,Machine Event,User Type,Event time,Accumulated Total Panels Run,Accumulated Total Boards Run,Inspection Time,Machine Production Running Time," +
                                                         "Machine Upstream Idle Time,Machine Downstream Idle Time,Machine Interrupted Time,Machine Service Time,Machine Fault Time,Panel Loading Time," +
                                                         "Panel Unloading Time,Other Time";
  private static final String _DAILY_EVENT_TITLE_VER_2 = "Date,Machine Event,User Type,Event time,Accumulated Total Panels Run,Accumulated Total Boards Run,Inspection Time,Machine Production Running Time," +
                                                         "Machine Upstream Idle Time,Machine Downstream Idle Time,Machine Interrupted Time,Machine Service Time,Machine Fault Time,Panel Loading Time," +
                                                         "Panel Unloading Time,Other Time,Machine Setup Time,Event Information,Machine Software Off Time,Error Count";
  private static final String _SUMMARY_REPORT_TITLE_VER_1 = "Date,Machine Serial Number,Latest User Type,Last Modified Time,Total Completed Panels,Total Completed Boards,Total Running Time,Total Upstream Idle Time," +
                                                            "Total Downstream Idle Time,Total Interrupted Time,Total Service Time,Total Fault Time,Total Panel Loading Time,Total Panel Unloading Time,Others";
  private static final String _SUMMARY_REPORT_TITLE_VER_2 = "Date,Machine Serial Number,Latest User Type,Last Modified Time,Total Completed Panels,Total Completed Boards,Total Running Time,Total Upstream Idle Time," +
                                                            "Total Downstream Idle Time,Total Interrupted Time,Total Service Time,Total Fault Time,Total Panel Loading Time,Total Panel Unloading Time,Others,"
                                                            + "Total Machine Setup Time,Total Machine Software Off Time,Total Error Count";

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public static synchronized MachineUtilizationReportSystem getInstance()
  {
    if (_instance == null)
      _instance = new MachineUtilizationReportSystem();
    return _instance;
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public MachineUtilizationReportSystem()
  {
    _xrayTester = XrayTester.getInstance();
    _manufacturingEquipmentInt = ManufacturingEquipmentInterface.getInstance();
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void initiateMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent) throws DatastoreException
  {
    writeMachineUtilizationReport(machineEvent, 0, 0, 0, null);  
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void initiateMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent, long eventTimer1InMillis, long eventTimer2InMillis) throws DatastoreException
  {
    writeMachineUtilizationReport(machineEvent, 0, eventTimer1InMillis, eventTimer2InMillis, null);  
  }

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void initiateMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent, int numberOfBoards, long eventTimer1InMillis, String projectName) throws DatastoreException
  {
    writeMachineUtilizationReport(machineEvent, numberOfBoards, eventTimer1InMillis, 0, projectName);  
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void initiateMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent, String eventInformation) throws DatastoreException
  {
    writeMachineUtilizationReport(machineEvent, 0, 0, 0, eventInformation);  
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void initiateMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent, long eventTimer1InMillis) throws DatastoreException
  {
    writeMachineUtilizationReport(machineEvent, 0, eventTimer1InMillis, 0, null);  
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void writeMachineUtilizationReport(MachineUtilizationReportEventEnum machineEvent, int numberOfBoards, long eventTimer1InMillis, long eventTimer2InMillis, String eventInformation) throws DatastoreException
  {
    Assert.expect(machineEvent != null);
    
    _machineEvent = machineEvent;
    
    String userType = _NA;
    if (_currentUserType != null)
      userType = _currentUserType.getName();
    
    if (eventInformation != null)
    {
      if (eventInformation.contains(","))
        eventInformation = eventInformation.replace(",", "?");
    }
    
    double eventTimer1InSeconds = Math.ceil((double) eventTimer1InMillis / 1000);
    double eventTimer2InSeconds = Math.ceil((double) eventTimer2InMillis / 1000);
    int downstreamIdleTime = 0;
    int panelUnloadingTime = 0;
    int panelLoadingTime = 0;
    int numberOfPanelRun = 0;
    int inspectionTimeInSeconds = 0;
    int lineNumber = 0;
    int errorCount = 0;
    String systemType = _xrayTester.getMachineDescription();
    String numberOfPanelRunStr = Integer.toString(numberOfPanelRun);
    String numberOfBoardRunStr = Integer.toString(numberOfBoards);
    String inspectionTimeString;
    String currentProductionTimeStr;
    String eventInformationStr = _NA;
    
    _summaryFileName = FileName.getMachineUtilizationSummaryFullPath();  
    _dailyEventFileName = FileName.getMachineUtilizationReportFullPath();
    _errorLogFileName = FileName.getErrorTimeLogFullPath();
    _dateTime = _dateFormat.format(new Date());
    _eventTime = _eventTimeFormat.format(new Date());
        
    if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN)
    {
      _isFaultExist = true;
      errorCount = 1;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_ERROR_CLEARED)
    {
      _isFaultExist = false;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.UNLOAD_PANEL)
    {     
      if (eventTimer2InSeconds == 0)
        eventTimer2InSeconds = 1;
      
      downstreamIdleTime = (int) eventTimer1InSeconds;
      panelUnloadingTime = (int) eventTimer2InSeconds;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.LOAD_PANEL)
    {      
      if (eventTimer1InSeconds == 0)
        eventTimer1InSeconds = 1;
      
      panelLoadingTime = (int) eventTimer1InSeconds;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.PRODUCTION_RUN)
    {
      inspectionTimeInSeconds = (int) eventTimer1InSeconds;  
      
      if (eventInformation != null)
        eventInformationStr = eventInformation;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.START_PROGRAMMING)
    {
      _isProgramming = true;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_PROGRAMMING)
    {
      _isProgramming = false;  
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING)
    {
      _isSetupRunning = true;
      
      if (eventInformation != null)
        eventInformationStr = eventInformation;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_SETUP_DONE)
    {
      _isSetupRunning = false;
      
      if (eventInformation != null)
        eventInformationStr = eventInformation;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.START_SERVICE)
    {
      _isServicing = true;
    }
    else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_SERVICE)
    {
      _isServicing = false;
    }
    
    inspectionTimeString = StringUtil.convertSecondsToString(inspectionTimeInSeconds);
    currentProductionTimeStr = StringUtil.convertSecondsToString(inspectionTimeInSeconds);
    
    if (inspectionTimeInSeconds == 0)
    {
      inspectionTimeString = _NA;  
    }

    String dir = Directory.getMachineUtilizationReportDir();
    String backupDir = Directory.getMachineUtilizationBackupReportDir();
    
    if (FileUtilAxi.exists(dir) == false)
      FileUtilAxi.mkdirs(dir);
    
    if (FileUtilAxi.exists(backupDir) == false)
      FileUtilAxi.mkdirs(backupDir);

    try
    {
      //first time log event after MU installation
      if (FileUtilAxi.exists(_dailyEventFileName) == false)
      {        
        writeDailyEventFile(userType, numberOfPanelRunStr, numberOfBoardRunStr, inspectionTimeString, currentProductionTimeStr, _ZERO_TIME, _ZERO_TIME, _ZERO_TIME, _ZERO_TIME,
                            _ZERO_TIME, _ZERO_TIME, _ZERO_TIME, _eventTime, _ZERO_TIME, eventInformationStr, _ZERO_TIME, _ZERO_COUNT, true);
        
        if (FileUtilAxi.exists(_summaryFileName))
        {
          File summaryFile = new File(_summaryFileName);
          File summaryBackupFile = new File(_summaryFileName + _BACKUP_EXTENSION);
          
          summaryFile.renameTo(summaryBackupFile);
        }
        
        writeSummaryFile(_dateTime, systemType, userType, _eventTime, numberOfPanelRunStr, numberOfBoardRunStr, currentProductionTimeStr, _ZERO_TIME, _ZERO_TIME, _ZERO_TIME, 
                         _ZERO_TIME, _ZERO_TIME, _ZERO_TIME, _ZERO_TIME, _eventTime, _ZERO_TIME, _ZERO_TIME, _ZERO_COUNT, true);
      }
      else 
      {
        Path readerPath = Paths.get(_dailyEventFileName);
        Charset charset = Charset.forName("UTF-8");
        BufferedReader reader = Files.newBufferedReader(readerPath, charset);
        LineNumberReader lineReader = new LineNumberReader(reader);       
        String line;
        String lastLine = null;
        lineNumber = 0;
        boolean rewriteFile = false;
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(_dailyEventFileName + _TEMP_EXTENSION)));
        
        while ((line = lineReader.readLine()) != null) 
        {
          if (line.equals(_DAILY_EVENT_TITLE_VER_1)) 
          {
            line = _DAILY_EVENT_TITLE_VER_2;
            rewriteFile = true;
          }
          
          if (rewriteFile)
          {
            if (line.equals(_DAILY_EVENT_TITLE_VER_2) == false)
              line = line + ",00:00:00,N/A,00:00:00,0";
              
            writer.println(line);
          }
          
          lastLine = line;
          lineNumber++;
        }
        
        reader.close();
        lineReader.close(); 
        writer.close();
        
        if (rewriteFile)
        {
          File oriName = new File(_dailyEventFileName);
          oriName.delete(); // remove the old file
          new File(_dailyEventFileName + _TEMP_EXTENSION).renameTo(oriName); // Rename temp file
          rewriteSummaryFile();
        }
        else
          FileUtil.deleteFileOrDirectory(_dailyEventFileName + _TEMP_EXTENSION);
        
        Matcher matcher = _machineUtilizationDailyEventPattern_ver2.matcher(lastLine);
        matcher.reset(lastLine); 
        
        if (matcher.find() == false) 
        {
          _badFormatFileName = _dailyEventFileName;
          throw new BadFormatException(new LocalizedString("MACHINE_UTILIZATION_REPORT_ERROR_MESSAGE_KEY", null));
        }

        String readDate = matcher.group(1);
        String readMachineEvent = matcher.group(2);
        String readUserTypeStr = matcher.group(3);
        String readEventTimeStr = matcher.group(4);
        String readTotalPanelStr = matcher.group(5);
        String readTotalBoardsStr = matcher.group(6);
        String readInspectionTimeStr = matcher.group(7);
        String readProductionTimeStr = matcher.group(8);
        String readUpstreamIdleTimeStr = matcher.group(9);
        String readDownstreamIdleTimeStr = matcher.group(10);
        String readInterruptedTimeStr = matcher.group(11);
        String readServiceTimeStr = matcher.group(12);
        String readFaultTimeStr = matcher.group(13);
        String readLoadingTimeStr = matcher.group(14);
        String readUnloadingTimeStr = matcher.group(15);
        String readotherTimeStr = matcher.group(16);      
        String readSetupTimeStr = matcher.group(17); 
        String readEventRemarkStr= matcher.group(18); 
        String readOffTimeStr = matcher.group(19);
        String readErrorCountStr = matcher.group(20);
        
        int readTotalPanel = StringUtil.convertStringToInt(readTotalPanelStr);
        int readTotalBoards = StringUtil.convertStringToInt(readTotalBoardsStr);
        int readEventTimeInSeconds = StringUtil.convertStringToTimeInSeconds(readEventTimeStr);
        int readProductionTimeInSeconds = StringUtil.convertStringToTimeInSeconds(readProductionTimeStr);
        int readUpstreamIdleTimeInSeconds = StringUtil.convertStringToTimeInSeconds(readUpstreamIdleTimeStr);
        int readDownstreamIdleTimeInSeconds = StringUtil.convertStringToTimeInSeconds(readDownstreamIdleTimeStr);
        int readInterruptedTime = StringUtil.convertStringToTimeInSeconds(readInterruptedTimeStr);
        int readServiceTime = StringUtil.convertStringToTimeInSeconds(readServiceTimeStr);
        int readFaultTime = StringUtil.convertStringToTimeInSeconds(readFaultTimeStr);
        int readLoadingTime = StringUtil.convertStringToTimeInSeconds(readLoadingTimeStr);
        int readUnloadingTime = StringUtil.convertStringToTimeInSeconds(readUnloadingTimeStr);
        int readSetupTime = StringUtil.convertStringToTimeInSeconds(readSetupTimeStr);
        int readOffTime = StringUtil.convertStringToTimeInSeconds(readOffTimeStr);
        int readOtherTime = StringUtil.convertStringToTimeInSeconds(readotherTimeStr);
        int readErrorCount = StringUtil.convertStringToInt(readErrorCountStr);
        
        if (_machineEvent == MachineUtilizationReportEventEnum.CHECK_SOFTWARE_PREVIOUS_STATUS)
        {
          if (readMachineEvent.equals(MachineUtilizationReportEventEnum.SOFTWARE_POWER_OFF.toString()) == false || 
                readMachineEvent.equals(MachineUtilizationReportEventEnum.LOG_OUT.toString()) == false)
          {
            if (FileUtil.exists(_errorLogFileName) == false)
              return;

            readerPath = Paths.get(_errorLogFileName);
            reader = Files.newBufferedReader(readerPath, charset);
            lineReader = new LineNumberReader(reader);    
            line = null;
            String dateLine = null;

            while ((line = lineReader.readLine()) != null) 
            {
              Matcher matcherErrorLog = _errorTimeLogPattern.matcher(line);
              
              if (matcherErrorLog.find())
              {
                dateLine = line;
                break;
              }
            }
            
            if (dateLine == null)
              return;
                   
            Date errorTime = new Date();
            Date previousTime = new Date();
            String previousTimeStr = readDate + _SPACE + readEventTimeStr;          

            try
            {
              errorTime = _defaultTimeFormat.parse(dateLine);
              previousTime = _dateTimeFormat.parse(previousTimeStr);
            } 
            catch (ParseException e) 
            {
              e.printStackTrace();
            }

            if (errorTime.getTime() > previousTime.getTime())
            {
              _machineEvent = MachineUtilizationReportEventEnum.SOFTWARE_ERROR;
              _dateTime = _dateFormat.format(errorTime);
              _eventTime = _eventTimeFormat.format(errorTime);
              errorCount = 1;
            }
            else
              return;
          }
          else
            return;
        }
        
        //Backup dailyEventFile and write a new dailyEventFile to prevent huge data in single file
        if (lineNumber > 1000)
        { 
          backupAndRewriteDailyEventFile();
        }
        
        //If current event's date with previous event are same date
        if (_dateTime.equals(readDate))
        {
          int currentEventTimeInSeconds = StringUtil.convertStringToTimeInSeconds(_eventTime);
          int totalProductionTime = readProductionTimeInSeconds;
          int totalUpstreamIdleTimeInSeconds = readUpstreamIdleTimeInSeconds;
          int totalDownstreamIdleTimeInSeconds = readDownstreamIdleTimeInSeconds;
          int totalInterruptedTime = StringUtil.convertStringToTimeInSeconds(readInterruptedTimeStr);
          int totalServiceTime = readServiceTime;
          int totalFaultTime = readFaultTime;
          int totalLoadingTime = readLoadingTime;
          int totalUnloadingTime = readUnloadingTime;
          int totalSetupTime = readSetupTime;
          int totalOffTime = readOffTime;

          if (_machineEvent == MachineUtilizationReportEventEnum.SOFTWARE_ERROR)
          {
            if (readMachineEvent.equals(MachineUtilizationReportEventEnum.START_PROGRAMMING.toString()))
            {
              totalInterruptedTime = totalInterruptedTime + (currentEventTimeInSeconds - readEventTimeInSeconds);   
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN.toString()))
            {
              totalFaultTime = totalFaultTime + currentEventTimeInSeconds - readEventTimeInSeconds;  
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING.toString()))
            {
              totalSetupTime = totalSetupTime + (currentEventTimeInSeconds - readEventTimeInSeconds);  
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.START_SERVICE.toString()))
            {
              totalServiceTime = totalServiceTime + (currentEventTimeInSeconds - readEventTimeInSeconds); 
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.PRODUCTION_RUN.toString()))
            {
              totalProductionTime = totalProductionTime + (currentEventTimeInSeconds - readEventTimeInSeconds); 
            }
            else
            {
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + (currentEventTimeInSeconds - readEventTimeInSeconds);  
            } 
          }
          else if (_machineEvent == MachineUtilizationReportEventEnum.SOFTWARE_POWER_ON)
          {
            totalOffTime = totalOffTime + currentEventTimeInSeconds - readEventTimeInSeconds; 
          }
          else if ((_isFaultExist || _machineEvent == MachineUtilizationReportEventEnum.MACHINE_ERROR_CLEARED) && 
               _machineEvent != MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN)
          {
            totalFaultTime = totalFaultTime + currentEventTimeInSeconds - readEventTimeInSeconds;   
          }
          else 
          {
            if (_machineEvent == MachineUtilizationReportEventEnum.LOGIN)
            {
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + (currentEventTimeInSeconds - readEventTimeInSeconds);  
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_PROGRAMMING)
            {
              totalInterruptedTime = totalInterruptedTime + (currentEventTimeInSeconds - readEventTimeInSeconds);   
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.LOAD_PANEL)
            {
              totalLoadingTime = totalLoadingTime + panelLoadingTime;    
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + calculateLeftOverEventDuration((currentEventTimeInSeconds - readEventTimeInSeconds), panelLoadingTime);    
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.UNLOAD_PANEL)
            {
              totalUnloadingTime = totalUnloadingTime + panelUnloadingTime;
              totalDownstreamIdleTimeInSeconds = totalDownstreamIdleTimeInSeconds + downstreamIdleTime;
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + calculateLeftOverEventDuration((currentEventTimeInSeconds - readEventTimeInSeconds), panelUnloadingTime + downstreamIdleTime);
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.PRODUCTION_RUN)
            {
              if (numberOfBoards != 0)
              {
                readTotalPanel++; 
                readTotalBoards = numberOfBoards + readTotalBoards;
              }
              totalProductionTime = totalProductionTime + inspectionTimeInSeconds;
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + calculateLeftOverEventDuration((currentEventTimeInSeconds - readEventTimeInSeconds), inspectionTimeInSeconds);
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_SETUP_DONE)
            {
              totalSetupTime = totalSetupTime + (currentEventTimeInSeconds - readEventTimeInSeconds);
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_SERVICE)
            {
              totalServiceTime = totalServiceTime + (currentEventTimeInSeconds - readEventTimeInSeconds);
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.START_PROGRAMMING || 
                       _machineEvent == MachineUtilizationReportEventEnum.START_SERVICE)
            {
              totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + (currentEventTimeInSeconds - readEventTimeInSeconds);    
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_LOADING_RECIPE)
            {
              totalInterruptedTime = totalInterruptedTime + (currentEventTimeInSeconds - readEventTimeInSeconds);   
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_CHANGE_OVER)
            {
              totalProductionTime = totalProductionTime + (currentEventTimeInSeconds - readEventTimeInSeconds);    
            }
            else
            {  
              if (_isSetupRunning && _machineEvent != MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING)
              {
                totalSetupTime = totalSetupTime + (currentEventTimeInSeconds - readEventTimeInSeconds);
                _isSetupRunning = false;
              }
              else if (_isServicing)
              {
                totalServiceTime = totalServiceTime + (currentEventTimeInSeconds - readEventTimeInSeconds);  
              }
              else if (_isProgramming)
              {
                totalInterruptedTime = totalInterruptedTime + (currentEventTimeInSeconds - readEventTimeInSeconds);  
              }
              else
              {
                totalUpstreamIdleTimeInSeconds = totalUpstreamIdleTimeInSeconds + (currentEventTimeInSeconds - readEventTimeInSeconds);   
              }
            }
          }
          errorCount = readErrorCount + errorCount;
          
          String totalProductionTimeStr = StringUtil.convertSecondsToString(totalProductionTime);
          String totalUpstreamIdleTimeStr = StringUtil.convertSecondsToString(totalUpstreamIdleTimeInSeconds);
          String totalDownstreamIdleTimeStr = StringUtil.convertSecondsToString(totalDownstreamIdleTimeInSeconds);
          String totalInterruptedTimeStr = StringUtil.convertSecondsToString(totalInterruptedTime);
          String totalServiceTimeStr = StringUtil.convertSecondsToString(totalServiceTime);
          String totalFaultTimeStr = StringUtil.convertSecondsToString(totalFaultTime);
          String totalLoadingTimeStr = StringUtil.convertSecondsToString(totalLoadingTime);
          String totalUnloadingTimeStr = StringUtil.convertSecondsToString(totalUnloadingTime);
          String totalSetupTimeStr = StringUtil.convertSecondsToString(totalSetupTime);
          String totalOffTimeStr = StringUtil.convertSecondsToString(totalOffTime);
          String totalErrorCountStr = Integer.toString(errorCount);
          
          writeDailyEventFile(userType, Integer.toString(readTotalPanel), Integer.toString(readTotalBoards), inspectionTimeString, totalProductionTimeStr,
                              totalUpstreamIdleTimeStr, totalDownstreamIdleTimeStr, totalInterruptedTimeStr, totalServiceTimeStr, totalFaultTimeStr, 
                              totalLoadingTimeStr, totalUnloadingTimeStr, readotherTimeStr, totalSetupTimeStr, eventInformationStr, totalOffTimeStr, totalErrorCountStr, false);
          
          boolean writeTitle = false;
          
          if (FileUtilAxi.exists(_summaryFileName))
            deleteLastLineOnSummaryFile();
          else
            writeTitle = true;
          
          writeSummaryFile(_dateTime, systemType, userType, _eventTime, Integer.toString(readTotalPanel), Integer.toString(readTotalBoards),
                           totalProductionTimeStr, totalUpstreamIdleTimeStr, totalDownstreamIdleTimeStr, totalInterruptedTimeStr, totalServiceTimeStr, 
                           totalFaultTimeStr, totalLoadingTimeStr, totalUnloadingTimeStr, readotherTimeStr, totalSetupTimeStr, totalOffTimeStr, totalErrorCountStr, writeTitle);
        } 
        else // Next Day compared with previous event
        {
          int totalUpstreamIdleTime = readUpstreamIdleTimeInSeconds;
          int totalInterruptedTime = readInterruptedTime;
          int totalServiceTime = readServiceTime;
          int totalFaultTime = readFaultTime;
          int totalLoadingTime = readLoadingTime;
          int totalUnloadingTime = readUnloadingTime;
          int totalDownstreamIdleTime = readDownstreamIdleTimeInSeconds;
          int totalProductionTime = readProductionTimeInSeconds;
          int totalSetupTime = readSetupTime;
          int totalOffTime = readOffTime;
          int totalErrorCount = readErrorCount;
          
          int currentEventTime = StringUtil.convertStringToTimeInSeconds(_eventTime);
          int currentUpstreamIdleTime = 0;
          int currentDownStreamIdleTime = 0;
          int currentInterruptedTime = 0;
          int currentServiceTime = 0;
          int currentFaultTime = 0;
          int currentPanelLoadingTime = 0;
          int currentPanelUnloadingTime = 0;    
          int currentNumberOfPanel = 0;
          int nextDayInspectionTime = 0;
          int currentSetupTime = 0;
          int currentOffTime = 0;
          int currentErrorCount = 0;

          if (_machineEvent == MachineUtilizationReportEventEnum.SOFTWARE_ERROR)
          {
            if (readMachineEvent.equals(MachineUtilizationReportEventEnum.START_PROGRAMMING.toString()))
            {
              totalInterruptedTime = totalInterruptedTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentInterruptedTime = currentEventTime;
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN.toString()))
            {
              totalFaultTime = totalFaultTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentFaultTime = currentEventTime;
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING.toString()))
            {
              totalSetupTime = totalSetupTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentSetupTime = currentEventTime;
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.START_SERVICE.toString()))
            {
              totalServiceTime = totalServiceTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds; 
              currentServiceTime = currentEventTime;
            }
            else if (readMachineEvent.equals(MachineUtilizationReportEventEnum.PRODUCTION_RUN.toString()))
            {
              totalProductionTime = totalProductionTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds; 
              nextDayInspectionTime = currentEventTime;
            }
            else
            {
              totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentUpstreamIdleTime = currentEventTime; 
            }
          }
          else if (_machineEvent == MachineUtilizationReportEventEnum.SOFTWARE_POWER_ON) 
          {
            totalOffTime = totalOffTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;  
            currentOffTime = currentEventTime; 
          }
          else if ((_isFaultExist || _machineEvent == MachineUtilizationReportEventEnum.MACHINE_ERROR_CLEARED) && 
                  _machineEvent != MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN) //Calc Fault time 
          {
            totalFaultTime = totalFaultTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;  
            currentFaultTime = currentEventTime; 
          }
          else 
          {
            if (_machineEvent == MachineUtilizationReportEventEnum.LOGIN)
            {
              totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentUpstreamIdleTime = currentEventTime;
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_PROGRAMMING)
            {
              totalInterruptedTime = totalInterruptedTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentInterruptedTime = currentEventTime;
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.LOAD_PANEL)
            {
              if (currentEventTime < panelLoadingTime)
              {
                checkIfDateDifferentMoreThanOneDay(readDate);
                currentPanelLoadingTime = currentEventTime;
                totalLoadingTime = totalLoadingTime + panelLoadingTime - currentEventTime;
                totalUpstreamIdleTime = totalUpstreamIdleTime + calculateLeftOverEventDuration(_DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds, (panelLoadingTime - currentEventTime));
              }
              else
              {
                currentPanelLoadingTime = panelLoadingTime;  
                currentUpstreamIdleTime = calculateLeftOverEventDuration(currentEventTime, panelLoadingTime);
                totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              }                  
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.UNLOAD_PANEL)
            {
              if (currentEventTime < (panelUnloadingTime + downstreamIdleTime))
              {
                checkIfDateDifferentMoreThanOneDay(readDate);
                currentPanelUnloadingTime = 0;
                currentDownStreamIdleTime = currentEventTime;
                totalUnloadingTime = totalUnloadingTime + panelUnloadingTime;
                totalDownstreamIdleTime = totalDownstreamIdleTime + downstreamIdleTime - currentEventTime;
                totalUpstreamIdleTime = totalUpstreamIdleTime + calculateLeftOverEventDuration(_DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds, (downstreamIdleTime - currentEventTime));
              }
              else
              {
                currentPanelUnloadingTime = panelUnloadingTime;
                currentDownStreamIdleTime = downstreamIdleTime;
                totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentUpstreamIdleTime = calculateLeftOverEventDuration(currentEventTime, panelUnloadingTime + downstreamIdleTime);
              }                
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.PRODUCTION_RUN)
            {
              if (numberOfBoards != 0)
              {
                currentNumberOfPanel = 1;
              }
              
              if (currentEventTime < inspectionTimeInSeconds)
              {
                checkIfDateDifferentMoreThanOneDay(readDate);
                nextDayInspectionTime = currentEventTime;
                totalProductionTime = totalProductionTime + inspectionTimeInSeconds - currentEventTime;
                totalUpstreamIdleTime = totalUpstreamIdleTime + calculateLeftOverEventDuration(_DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds, (inspectionTimeInSeconds - currentEventTime));
              }
              else
              {
                nextDayInspectionTime = inspectionTimeInSeconds;
                totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentUpstreamIdleTime = calculateLeftOverEventDuration(currentEventTime, inspectionTimeInSeconds); 
              }              
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.MACHINE_SETUP_DONE)
            {              
              totalSetupTime = totalSetupTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentSetupTime = currentEventTime;
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_SERVICE)
            {              
              totalServiceTime = totalServiceTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentServiceTime = currentEventTime;
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.START_PROGRAMMING || 
                       _machineEvent == MachineUtilizationReportEventEnum.START_SERVICE)
            {
              totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentUpstreamIdleTime = currentEventTime;
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_LOADING_RECIPE)
            {
              totalInterruptedTime = totalInterruptedTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              currentInterruptedTime = currentEventTime; 
            }
            else if (_machineEvent == MachineUtilizationReportEventEnum.STOP_CHANGE_OVER)
            {
              totalProductionTime = totalProductionTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
              nextDayInspectionTime = currentEventTime; 
            }
            else
            {
              if (_isSetupRunning && _machineEvent != MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING)
              {
                totalSetupTime = totalSetupTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentSetupTime = currentEventTime;
                _isSetupRunning = false;
              }
              else if (_isServicing)
              {
                totalServiceTime = totalServiceTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentServiceTime = currentEventTime;
              }
              else if (_isProgramming)
              {
                totalInterruptedTime = totalInterruptedTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentInterruptedTime = currentEventTime;
              }
              else
              {
                totalUpstreamIdleTime = totalUpstreamIdleTime + _DAY_END_DURATION_IN_SECONDS - readEventTimeInSeconds;
                currentUpstreamIdleTime = currentEventTime;
              }
            }
          }
          currentErrorCount = currentErrorCount + errorCount;
          int totalEventDuration = (totalProductionTime + totalUpstreamIdleTime + totalDownstreamIdleTime + totalInterruptedTime + totalServiceTime + totalFaultTime + 
                                  totalLoadingTime + totalUnloadingTime + totalSetupTime + totalOffTime + readOtherTime);
          
          if (totalEventDuration > _DAY_END_DURATION_IN_SECONDS)
          {
            int leftOverTime = totalEventDuration - _DAY_END_DURATION_IN_SECONDS;
            if (totalUpstreamIdleTime > leftOverTime)
            {
              totalUpstreamIdleTime = totalUpstreamIdleTime - leftOverTime;
            }
            else if ((totalUpstreamIdleTime + totalDownstreamIdleTime) > leftOverTime)
            {
              totalUpstreamIdleTime = 0;
              totalDownstreamIdleTime = totalDownstreamIdleTime - (leftOverTime - totalUpstreamIdleTime);
            }
            else
            {
              totalUpstreamIdleTime = 0;  
              totalDownstreamIdleTime = 0;
            }
          }
          
          String currentUpstreamIdleTimeStr = StringUtil.convertSecondsToString(currentUpstreamIdleTime);
          String currentDownStreamIdleTimeStr = StringUtil.convertSecondsToString(currentDownStreamIdleTime);
          String currentInterruptedTimeStr = StringUtil.convertSecondsToString(currentInterruptedTime);
          String currentServiceTimeStr = StringUtil.convertSecondsToString(currentServiceTime);
          String currentFaultTimeStr = StringUtil.convertSecondsToString(currentFaultTime);
          String currentPanelLoadingTimeStr = StringUtil.convertSecondsToString(currentPanelLoadingTime);
          String currentPanelUnloadingTimeStr = StringUtil.convertSecondsToString(currentPanelUnloadingTime);
          String currentNumberOfPanelStr = Integer.toString(currentNumberOfPanel);
          String nextDayInspectionTimeStr = StringUtil.convertSecondsToString(nextDayInspectionTime);
          String currentSetupTimeStr = StringUtil.convertSecondsToString(currentSetupTime);
          String currentOffTimeStr = StringUtil.convertSecondsToString(currentOffTime);
          String currentErrorCountStr = Integer.toString(currentErrorCount);
                  
          String totalUpstreamIdleTimeStr = StringUtil.convertSecondsToString(totalUpstreamIdleTime);
          String totalInterruptedTimeStr = StringUtil.convertSecondsToString(totalInterruptedTime);
          String totalServiceTimeStr = StringUtil.convertSecondsToString(totalServiceTime);
          String totalFaultTimeStr = StringUtil.convertSecondsToString(totalFaultTime);
          String totalLoadingTimeStr = StringUtil.convertSecondsToString(totalLoadingTime);
          String totalUnloadingTimeStr = StringUtil.convertSecondsToString(totalUnloadingTime);
          String totalDownstreamIdleTimeStr = StringUtil.convertSecondsToString(totalDownstreamIdleTime);
          String totalProductionTimeStr = StringUtil.convertSecondsToString(totalProductionTime);
          String totalSetupTimeStr = StringUtil.convertSecondsToString(totalSetupTime);
          String totalOffTimeStr = StringUtil.convertSecondsToString(totalOffTime);
          String totalErrorCountStr = Integer.toString(totalErrorCount);
            
          writeDailyEventFile(userType, currentNumberOfPanelStr, numberOfBoardRunStr, inspectionTimeString, nextDayInspectionTimeStr, currentUpstreamIdleTimeStr,currentDownStreamIdleTimeStr,
                              currentInterruptedTimeStr, currentServiceTimeStr, currentFaultTimeStr,currentPanelLoadingTimeStr, currentPanelUnloadingTimeStr, _ZERO_TIME, 
                              currentSetupTimeStr, eventInformationStr, currentOffTimeStr, currentErrorCountStr, false);  
          
          if (FileUtilAxi.exists(_summaryFileName) == false)
          {
            writeSummaryFile(readDate, systemType, userType, _DAY_END_EVENT_TIME_IN_STRING, readTotalPanelStr, readTotalBoardsStr, totalProductionTimeStr, totalUpstreamIdleTimeStr, totalDownstreamIdleTimeStr,
                             totalInterruptedTimeStr, totalServiceTimeStr, totalFaultTimeStr, totalLoadingTimeStr, totalUnloadingTimeStr, readotherTimeStr, totalSetupTimeStr, totalOffTimeStr, totalErrorCountStr, true); 
            writeSummaryFile(_dateTime, systemType, userType, _eventTime, currentNumberOfPanelStr, numberOfBoardRunStr, nextDayInspectionTimeStr, currentUpstreamIdleTimeStr,currentDownStreamIdleTimeStr, 
                             currentInterruptedTimeStr, currentServiceTimeStr, currentFaultTimeStr, currentPanelLoadingTimeStr, currentPanelUnloadingTimeStr, _ZERO_TIME, currentSetupTimeStr, currentOffTimeStr, currentErrorCountStr, false);
          }
          else
          {
            deleteLastLineOnSummaryFile();
            writeSummaryFile(readDate, systemType, userType, _DAY_END_EVENT_TIME_IN_STRING, readTotalPanelStr, readTotalBoardsStr, totalProductionTimeStr, totalUpstreamIdleTimeStr, totalDownstreamIdleTimeStr,
                             totalInterruptedTimeStr, totalServiceTimeStr, totalFaultTimeStr, totalLoadingTimeStr, totalUnloadingTimeStr, readotherTimeStr, totalSetupTimeStr, totalOffTimeStr, totalErrorCountStr, false);
            
            if (_isEventExceedOneDayDuration)
            {
              _isEventExceedOneDayDuration = false;
              Date newDate = new Date();
              Date lastDate = new Date();
              String newDateStr = null;
              
              try
              {
                lastDate = _dateFormat.parse(readDate);
              }
              catch (ParseException e) 
              {
                e.printStackTrace();
              }
              
              String productionTimeStr = _ZERO_TIME;
              String downstreamIdleTimeStr = _ZERO_TIME;
              String interruptedTimeStr = _ZERO_TIME;
              String serviceTimeStr = _ZERO_TIME;
              String faultTimeStr = _ZERO_TIME;
              String loadingTimeStr = _ZERO_TIME;
              String unloadingTimeStr = _ZERO_TIME;
              String setupTimeStr = _ZERO_TIME;
              String offTimeStr = _ZERO_TIME;
              
              if (_machineEvent == MachineUtilizationReportEventEnum.PRODUCTION_RUN)
                productionTimeStr = _ONE_DAY_TIME;
              else if (_machineEvent == MachineUtilizationReportEventEnum.UNLOAD_PANEL)
                downstreamIdleTimeStr = _ONE_DAY_TIME;
              else if (_machineEvent == MachineUtilizationReportEventEnum.LOAD_PANEL)
                loadingTimeStr = _ONE_DAY_TIME;
                      
              for (int i = 1; i <= (_differentOfDays - 1); i++)
              {
                newDate = new Date(lastDate.getTime() +  (i * 1000 * 60 * 60 * 24));
                newDateStr = _dateFormat.format(newDate);
                writeSummaryFile(newDateStr, systemType, readUserTypeStr, _DAY_END_EVENT_TIME_IN_STRING, "0", "0", productionTimeStr, _ZERO_TIME, downstreamIdleTimeStr,
                                 interruptedTimeStr, serviceTimeStr, faultTimeStr, loadingTimeStr, unloadingTimeStr, _ZERO_TIME, setupTimeStr, offTimeStr, _ZERO_COUNT, false);
              }
            }
            
            writeSummaryFile(_dateTime, systemType, userType, _eventTime, currentNumberOfPanelStr, numberOfBoardRunStr, nextDayInspectionTimeStr, currentUpstreamIdleTimeStr,currentDownStreamIdleTimeStr, 
                             currentInterruptedTimeStr, currentServiceTimeStr, currentFaultTimeStr, currentPanelLoadingTimeStr, currentPanelUnloadingTimeStr, _ZERO_TIME, currentSetupTimeStr, currentOffTimeStr, currentErrorCountStr, false);
          }
        }        
      }
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_dailyEventFileName);
      dex.initCause(ex);
      throw dex;
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_dailyEventFileName);
      dex.initCause(io);
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_badFormatFileName, lineNumber);
      dex.initCause(bex);

      throw dex;
    }
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void writeDailyEventFile(String userType, String totalPanelStr, String totalBoardStr, String inspectionTimeStr, String totalProductionRunTimeStr, String totalUpstreamIdleTimeStr,
                                      String totalDownstreamIdleTimeStr, String totalInterruptedTimeStr, String totalServiceTimeStr, String totalFaultTimeStr, String totalLoadingTime,
                                      String totalUnloadingTime, String otherTimeStr, String setupTimeStr, String eventRemark, String offTimeStr, String errorCountStr, boolean writeNewFile) throws IOException
  {    
    FileWriterUtil fileWriter = new FileWriterUtil(_dailyEventFileName, true);
    try
    {
      fileWriter.open();
      
      if (writeNewFile)
      {
        fileWriter.writeln(_DAILY_EVENT_TITLE_VER_2);
      }
      
      fileWriter.writeln(_dateTime + csvDelimiter + _machineEvent.toString() + csvDelimiter + userType + csvDelimiter + _eventTime + csvDelimiter + totalPanelStr + csvDelimiter +
                         totalBoardStr + csvDelimiter + inspectionTimeStr + csvDelimiter + totalProductionRunTimeStr + csvDelimiter + totalUpstreamIdleTimeStr + csvDelimiter + 
                         totalDownstreamIdleTimeStr + csvDelimiter + totalInterruptedTimeStr + csvDelimiter + totalServiceTimeStr + csvDelimiter + totalFaultTimeStr + csvDelimiter + 
                         totalLoadingTime + csvDelimiter + totalUnloadingTime + csvDelimiter + otherTimeStr + csvDelimiter + setupTimeStr + csvDelimiter + eventRemark + csvDelimiter + 
                         offTimeStr + csvDelimiter + errorCountStr);
    }
    catch (CouldNotCreateFileException ex)
    {
      throw ex;
    }
    finally
    {
      fileWriter.close();   
    }
  }

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void writeSummaryFile(String date,String systemType,String currentUserType,String lastModifiedTime, String panelNumberStr, String boardNumberStr,
                                  String totalRunningTimeStr, String totalUpstreamIdleTimeStr, String totalDownstreamIdleTimeStr, String totalInterruptedTimeStr, 
                                  String totalServiceTime, String totalFaultTime, String panelLoadingTime, String panelUnloadingTime, String otherTimeStr, 
                                  String totalSetupTime, String totalOffTime, String totalErrorCount, boolean writeTitle) throws IOException
  {
    FileWriterUtil fileWriter = new FileWriterUtil(_summaryFileName, true);
    try
    {
      fileWriter.open();
      
      if (writeTitle)
        fileWriter.writeln(_SUMMARY_REPORT_TITLE_VER_2);
      
      fileWriter.writeln(date + csvDelimiter + systemType + csvDelimiter + currentUserType + csvDelimiter + lastModifiedTime + csvDelimiter + panelNumberStr + csvDelimiter +
                         boardNumberStr + csvDelimiter + totalRunningTimeStr + csvDelimiter + totalUpstreamIdleTimeStr + csvDelimiter + totalDownstreamIdleTimeStr + csvDelimiter + 
                         totalInterruptedTimeStr + csvDelimiter + totalServiceTime + csvDelimiter + totalFaultTime + csvDelimiter + panelLoadingTime + csvDelimiter + 
                         panelUnloadingTime + csvDelimiter + otherTimeStr + csvDelimiter + totalSetupTime + csvDelimiter + totalOffTime + csvDelimiter + totalErrorCount);
    }
    catch (CouldNotCreateFileException ex)
    {
      throw ex;
    }
    finally
    {
      fileWriter.close();   
    }
  }

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void deleteLastLineOnSummaryFile() throws IOException
  {    
    File summaryFile = new File(_summaryFileName);
    File tempSummaryFile = new File(_summaryFileName + _TEMP_EXTENSION);
    Path readerPath = Paths.get(_summaryFileName);
    Charset charset = Charset.forName("UTF-8");
    
    try
    {
      BufferedReader reader = Files.newBufferedReader(readerPath, charset);
      BufferedWriter writer = new BufferedWriter(new FileWriter(tempSummaryFile));
      LineNumberReader lineReader = new LineNumberReader(reader);       
      String line;
      String lastLine = null;
      int lineNumber = 0;

      while ((line = lineReader.readLine()) != null) 
      {
        lastLine = line;
        lineNumber++;
      }
      
      reader.close();
      lineReader.close();

      reader = Files.newBufferedReader(readerPath, charset);
      lineReader = new LineNumberReader(reader); 

      while ((line = lineReader.readLine()) != null) 
      {
        lineNumber--;
        if(lineNumber == 0)
          continue;

        writer.write(line + System.getProperty("line.separator"));
      }

      writer.close(); 
      reader.close(); 
      lineReader.close();
      
      boolean successfulDelete = summaryFile.delete();      
      boolean successfulRename = tempSummaryFile.renameTo(summaryFile);
      
      if (successfulRename == false || successfulDelete == false)
      {
        throw new CouldNotCreateFileException(_summaryFileName);  
      }
    }
    catch (IOException io)
    {
      throw io;  
    }
  }

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public void setLoginUserType(UserTypeEnum userType) throws DatastoreException
  {    
    Assert.expect(userType != null);
    
    _currentUserType = userType;
  }  
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public UserTypeEnum getLoginUserType() throws DatastoreException
  {    
    Assert.expect(_currentUserType != null);
    
    return _currentUserType;
  }  
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public boolean isFaultExist() throws DatastoreException
  {        
    return _isFaultExist;
  } 
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public boolean isProgramming() throws DatastoreException
  {        
    return _isProgramming;
  } 
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public boolean isServicing() throws DatastoreException
  {        
    return _isServicing;
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  public boolean isSetupRunning() throws DatastoreException
  {        
    return _isSetupRunning;
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void checkIfDateDifferentMoreThanOneDay(String readDate)
  {
    Assert.expect(readDate != null);
    
    _differentOfDays = 0;
    Date lastDate = new Date();
    _isEventExceedOneDayDuration = false;

    try 
    {
      lastDate = _dateFormat.parse(readDate);
      Date currentDate = _dateFormat.parse(_dateTime);

      _differentOfDays = (int)( (currentDate.getTime() - lastDate.getTime()) / (1000 * 60 * 60 * 24) );
      //currentDate must be newer date compared to lastDate
      Assert.expect(_differentOfDays >= 0);
    } 
    catch (ParseException e) 
    {
      e.printStackTrace();
    }
    
    if (_differentOfDays >= 2)
    {
      _isEventExceedOneDayDuration = true;  
    }
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void rewriteSummaryFile() throws DatastoreException
  {
    try
    {
      Path readerPath = Paths.get(_summaryFileName);
      Charset charset = Charset.forName("UTF-8");
      BufferedReader reader = Files.newBufferedReader(readerPath, charset);
      LineNumberReader lineReader = new LineNumberReader(reader);       
      String line;
      PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(_summaryFileName + _TEMP_EXTENSION)));

      while ((line = lineReader.readLine()) != null) 
      {
        if (line.equals(_SUMMARY_REPORT_TITLE_VER_1)) 
        {
          line = _SUMMARY_REPORT_TITLE_VER_2;
        }
        writer.println(line);
      }

      reader.close();
      lineReader.close(); 
      writer.close();

      File oriName = new File(_summaryFileName);
      oriName.delete(); // remove the old file
      new File(_summaryFileName + _TEMP_EXTENSION).renameTo(oriName); // Rename temp file
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_summaryFileName);
      dex.initCause(io);
      throw dex;
    }
  }

  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private void backupAndRewriteDailyEventFile() throws DatastoreException
  {
    String timeStamp = "_" + _dateTime + "_" + _eventTime.replace(":", ".");
    String dailyBackupFile = FileName.getMachineUtilizationBackupReportFullPath(timeStamp);

    try
    {
      FileUtil.rename(_dailyEventFileName, dailyBackupFile);
    }
    catch (CouldNotRenameFileException ex)
    {
      CannotWriteDatastoreException dex = new CannotWriteDatastoreException(_dailyEventFileName);
      dex.initCause(ex);
      throw dex;
    }   
    catch (CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(_dailyEventFileName);
      dex.initCause(ex);
      throw dex;
    }     
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_dailyEventFileName);
      dex.initCause(ex);
      throw dex;
    }
    
    FileWriterUtil fileWriter = new FileWriterUtil(_dailyEventFileName, true);
    try
    {
      fileWriter.open();
      fileWriter.writeln(_DAILY_EVENT_TITLE_VER_2);
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_dailyEventFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriter.close();   
    }
  }
  
  /**
   * @author Khaw Chek Hau 
   * Machine Utilization Report Tool  
   */
  private int calculateLeftOverEventDuration(int eventTimeDifferent, int eventDuration)
  {
    int leftoverEventDuration = eventTimeDifferent - eventDuration;
    
    if (leftoverEventDuration < 0)
    {
      return 0;
    }
    
    return leftoverEventDuration;
  }
}
