package com.axi.v810.datastore.virtuallive;

import java.io.*;
import java.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageSetDataXMLWriter
{

  private static VirtualLiveImageSetDataXMLWriter _virtualLiveImageSetDataXMLWriter;
  private String _xmlFilePath = null;  
  private PrintWriter _os;
  public static String VERSION = "Version";
  public static String IMAGE_NAME = "ImageName";
  public static String COMPONENT_NAME = "ComponentName";
  public static String X_CENTER = "XCenter";
  public static String Y_CENTER = "YCenter";
  public static String WIDTH = "Width";
  public static String HEIGHT = "Height";
  public static String ENLARGE_UNCERTAINTY = "enlargeUncertaintyInNanoMeter";
  public static String Z_SIZE = "zSizeInNanoMeter";
  public static String INTEGRATION_LEVEL = "integrationLevel";
  public static String BYPASS_ALIGNMENT = "bypassAlignment";
  public static String MAGNIFICATION_KEY = "magnificationKey";
  public static String MIN_Z_HEIGHT_KEY = "minZHeightInNanoMeters";
  public static String MAX_Z_HEIGHT_KEY = "maxZHeightInNanoMeters";
  public static String GAIN_KEY = "userGain";
  public static String STAGE_SPEED_KEY = "stageSpeed";
  private List<String> _elementValueList = new ArrayList<String>();  
  private List<String> _nodeValueList = new ArrayList<String>();


  /**
   * @author sham
   */
  public static VirtualLiveImageSetDataXMLWriter getInstance()
  {
    if(_virtualLiveImageSetDataXMLWriter == null)
    {
      _virtualLiveImageSetDataXMLWriter = new VirtualLiveImageSetDataXMLWriter();
    }
    return _virtualLiveImageSetDataXMLWriter;
  }

  /**
   * @author sham
   */
  public VirtualLiveImageSetDataXMLWriter()
  {
    //
  }

  /**
   * @author sham
   */
  public void createVirtualLiveImageSetDataXMLWriter(String filePath)
  {
    _xmlFilePath = filePath;
  }

  /**
   * @author sham
   */
  public void deleteVirtualLiveImageSetDataXMLWriter()
  {
    _os = null;
    _elementValueList.clear();
    _nodeValueList.clear();
  }

  /**
   * @author sham
   */
  public void addImageSetData(String elementValue,String nodeValue)
  {
    _elementValueList.add(elementValue);
    _nodeValueList.add(nodeValue);
  }

  /**
   * @author sham
   */
  public void writeToXMLFile() throws DatastoreException
  {  
    Assert.expect(_elementValueList !=null);
    Assert.expect(_nodeValueList !=null);

    String xmlFileName = null;
    try
    {
      if (FileUtil.exists(_xmlFilePath) == false)
      {
        try
        {
          FileUtil.mkdirs(_xmlFilePath);
        }
        catch (FileFoundWhereDirectoryWasExpectedException ex)
        {
          FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_xmlFilePath);
          dex.initCause(ex);
          throw dex;
        }
        catch (CouldNotCreateFileException ex)
        {
          CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(_xmlFilePath);
          dex.initCause(ex);
          throw dex;
        }
      }
      xmlFileName = _xmlFilePath + FileName.getVirtualLiveImageSetDataXMLFileName();
      if(FileUtil.exists(xmlFileName))
      {
        try
        {
          FileUtil.delete(xmlFileName);
        }
        catch (CouldNotDeleteFileException ex)
        {
          CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(_xmlFilePath);
          dex.initCause(ex);
          throw dex;
        }
      }
      File xmlFile = new File(xmlFileName);
      if(!xmlFile.exists())
      {
        try
        {
          xmlFile.createNewFile();
        }
        catch (IOException ex)
        {
          CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(_xmlFilePath);
          dex.initCause(ex);
          throw dex;
        }
      }
      _os = new PrintWriter(xmlFileName);
      _os.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
      _os.println("<VirtualliveImageSetData>");
      _os.println("<" + VERSION +">2</" + VERSION + ">");
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_xmlFilePath);
      dex.initCause(ex);
      throw dex;
    }
    for(int i=0;i<_elementValueList.size();i++)
    {
      _os.println("<" + _elementValueList.get(i) + ">" + _nodeValueList.get(i) + "</" + _elementValueList.get(i) + ">");
    }
    _os.println("</VirtualliveImageSetData>");
    _os.close();
  }
}
