package com.axi.v810.datastore.virtuallive;

import java.io.*;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.*;
import com.axi.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageSetDataXMLReader
{

  Document _dom;

  /**
   * @author sham
   */
  public VirtualLiveImageSetDataXMLReader(String filePath)
  {
    try
    {
      File file = new File(filePath);
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
      _dom = documentBuilder.parse(file);
      _dom.getDocumentElement().normalize();
    }
    catch (SAXException ex)
    {
      //
    }
    catch (IOException ex)
    {
      //
    }
    catch (ParserConfigurationException ex)
    {
      //
    }
  }

  /**
   * @author sham
   */
  public java.util.List<String> getData(String elementName)
  {
    Assert.expect(elementName != null);

    java.util.List<String> list = new ArrayList<String>();
    NodeList nodeList = _dom.getElementsByTagName(elementName);
    if(nodeList != null && nodeList.getLength() > 0)
    {
      Element e = null;
      NodeList innerNodeList = null;
      for (int i = 0; i < nodeList.getLength(); i++)
      {
        e = (Element) nodeList.item(i);
        innerNodeList = e.getChildNodes();
        list.add(String.valueOf(((Node) innerNodeList.item(0)).getNodeValue()));
      }
    }

    return list;
  }

  /**
   * @author sham
   */
  public int getCurrentXMLVersion()
  {
    Assert.expect(_dom != null);

    int version;
    if(getData(VirtualLiveImageSetDataXMLWriter.VERSION).size() > 0)
    {
      version = Integer.parseInt(getData(VirtualLiveImageSetDataXMLWriter.VERSION).get(0));
    }
    else
    {
      version = 0;
    }

    return version;
  }
}
