package com.axi.v810.datastore;

import java.util.*;
import java.text.*;
import java.io.*;
import java.util.regex.*;
import java.nio.file.*;
import java.nio.charset.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Khaw Chek Hau
 * @XCR3670: V-ONE AXI latest status monitoring log
 */
public class MachineLatestStatusLogWriter
{
  private static MachineLatestStatusLogWriter _instance; 
  private MachineLatestStatusElementEventEnum _machineElementEvent;
  private String _dateTime;
  private String _machineLatestStatusLogFileName;
  private String _v810SoftwareVersion;
  private String _currentActiveRecipeName;
  private String _productionRunningRecipeName;
  private String _loginUserName;
  private String _productionRunningRecipeVariation;
  private String _serialNumbers;
  private String _inspectionTime;
  private String _inspectionEndTime;
  private String _panelPinsProcessed;
  private String _panelPinsDefective;
  private String _panelComponentsDefective;
  private String _badFormatFileName;
  private final SimpleDateFormat _dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss:SSS");
  private static final String _VERSION = "Version";
  private static final String _CATEGORY = "Category";
  private static final String _EQUAL_SIGN = "=";
  private static final String _DOLLAR_SIGN = "$";
  private static final String _HASH_SIGN = "#";
  private static final int _VERSION_NUMBER = 1;
  private Pattern _machineLatestStatusLogPattern_ver1 = Pattern.compile("^(\\w+)\\=(.*)\\$$");

  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public static synchronized MachineLatestStatusLogWriter getInstance()
  {
    if (_instance == null)
      _instance = new MachineLatestStatusLogWriter();
    return _instance;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public MachineLatestStatusLogWriter()
  {
    //do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public void writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum machineElementEvent, String elementContent) throws DatastoreException
  {
    Assert.expect(machineElementEvent != null);
    
    _dateTime = _dateTimeFormat.format(new Date());
    _machineElementEvent = machineElementEvent;
    
    _machineLatestStatusLogFileName = FileName.getMachineLatestStatusLogFullPath(FileName.getMachineLatestStatusLogLatestFileVersion());
  
    if (FileUtilAxi.exists(_machineLatestStatusLogFileName))
    {
      readExistingMachineLatestStatusLogFile();
    }

    if (_machineElementEvent.equals(MachineLatestStatusElementEventEnum.SOFTWARE_VERSION))
    {
      _v810SoftwareVersion = elementContent;  
    }
    else if (_machineElementEvent.equals(MachineLatestStatusElementEventEnum.CURRENT_ACTIVE_PROGRAM))
    {
      _currentActiveRecipeName = elementContent;  
    }
    else if (_machineElementEvent.equals(MachineLatestStatusElementEventEnum.LOGIN_USER_NAME))
    {
      _loginUserName = elementContent;  
    }

    writeMachineLatestStatusLogFile();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public void writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum machineElementEvent, 
                                          String productionRunningRecipeName,
                                          String productionRunningRecipeVariation,
                                          String serialNumbers,
                                          String panelPinsProcessed,
                                          String panelPinsDefective,
                                          String panelComponentsDefective,
                                          String inspectionTime,
                                          String inspectionEndTime) throws DatastoreException
  {
    Assert.expect(machineElementEvent != null);
    
    _dateTime = _dateTimeFormat.format(new Date());
    _machineElementEvent = machineElementEvent;
        
    _machineLatestStatusLogFileName = FileName.getMachineLatestStatusLogFullPath(FileName.getMachineLatestStatusLogLatestFileVersion());   
      
    _productionRunningRecipeName = productionRunningRecipeName;  
    _currentActiveRecipeName = productionRunningRecipeName;
    _productionRunningRecipeVariation = productionRunningRecipeVariation;  
    _serialNumbers = serialNumbers;  
    _panelPinsProcessed = panelPinsProcessed;  
    _panelPinsDefective = panelPinsDefective;  
    _panelComponentsDefective = panelComponentsDefective;  
    _inspectionTime = inspectionTime;  
    _inspectionEndTime = inspectionEndTime;  
      
    writeMachineLatestStatusLogFile();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  private void readExistingMachineLatestStatusLogFile() throws DatastoreException
  {
    Path readerPath = Paths.get(_machineLatestStatusLogFileName);
    Charset charset = Charset.forName("UTF-8");
    String line;
    int lineNumber = 0;
    
    try
    {
      BufferedReader reader = Files.newBufferedReader(readerPath, charset);
      LineNumberReader lineReader = new LineNumberReader(reader);      
      Matcher matcher;

      while ((line = lineReader.readLine()) != null) 
      {
        lineNumber++;
        
        if(line.equals(_HASH_SIGN))
          break;
        
        if (line.startsWith(_VERSION + _EQUAL_SIGN))
          continue;

        matcher = _machineLatestStatusLogPattern_ver1.matcher(line);
        matcher.reset(line); 

        if (matcher.find() == false) 
        {
          _badFormatFileName = _machineLatestStatusLogFileName;
          throw new BadFormatException(new LocalizedString("V_ONE_MACHINE_STATUS_MONITORING_REPORT_MESSAGE_KEY", null));
        }

        String readHeader = matcher.group(1);
        String readContent = matcher.group(2);

        if (readHeader.equals(MachineLatestStatusElementEventEnum.SOFTWARE_VERSION.toString()))
        {
          _v810SoftwareVersion = readContent;
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.CURRENT_ACTIVE_PROGRAM.toString()))
        {
          _currentActiveRecipeName = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.LOGIN_USER_NAME.toString()))
        {
          _loginUserName = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.PRODUCTION_RUNNING_RECIPE.toString()))
        {
          _productionRunningRecipeName = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.PRODUCTION_RUNNING_VARIATION.toString()))
        {
          _productionRunningRecipeVariation = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.SERIAL_NUMBER.toString()))
        {
          _serialNumbers = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.PANEL_PINS_PROCESSED.toString()))
        {
          _panelPinsProcessed = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.PANEL_PINS_DEFECTIVE.toString()))
        {
          _panelPinsDefective = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.PANEL_COMPONENTS_DEFECTIVE.toString()))
        {
          _panelComponentsDefective = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.INSPECTION_TIME.toString()))
        {
          _inspectionTime = readContent;   
        }
        else if (readHeader.equals(MachineLatestStatusElementEventEnum.INSPECTION_END_TIME.toString()))
        {
          _inspectionEndTime = readContent;   
        }
      }
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_machineLatestStatusLogFileName);
      dex.initCause(io);
      throw dex;
    }
    catch (BadFormatException bex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_badFormatFileName, lineNumber);
      dex.initCause(bex);

      throw dex;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  private void writeMachineLatestStatusLogFile() throws DatastoreException
  {
    FileWriterUtil fileWriter = new FileWriterUtil(_machineLatestStatusLogFileName, false);
    try
    {
      fileWriter.open();

      fileWriter.writeln(_VERSION + _EQUAL_SIGN + _VERSION_NUMBER);
      fileWriter.writeln(_CATEGORY + _EQUAL_SIGN + MachineLatestStatusCategoryEventEnum.MACHINE_INFO.toString() + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.LAST_UPDATE_TIME.toString() + _EQUAL_SIGN + _dateTime + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.SOFTWARE_VERSION.toString() + _EQUAL_SIGN + _v810SoftwareVersion + _DOLLAR_SIGN);
      fileWriter.writeln(_CATEGORY + _EQUAL_SIGN + MachineLatestStatusCategoryEventEnum.ACTIVE_PROGRAM.toString() + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.CURRENT_ACTIVE_PROGRAM.toString() + _EQUAL_SIGN + _currentActiveRecipeName + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.LOGIN_USER_NAME.toString() + _EQUAL_SIGN + _loginUserName + _DOLLAR_SIGN);
      fileWriter.writeln(_CATEGORY + _EQUAL_SIGN + MachineLatestStatusCategoryEventEnum.LAST_RUNNING_PRODUCTION + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.PRODUCTION_RUNNING_RECIPE.toString() + _EQUAL_SIGN + _productionRunningRecipeName + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.PRODUCTION_RUNNING_VARIATION.toString() + _EQUAL_SIGN + _productionRunningRecipeVariation + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.SERIAL_NUMBER.toString() + _EQUAL_SIGN + _serialNumbers + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.PANEL_PINS_PROCESSED.toString() + _EQUAL_SIGN + _panelPinsProcessed + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.PANEL_PINS_DEFECTIVE.toString() + _EQUAL_SIGN + _panelPinsDefective + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.PANEL_COMPONENTS_DEFECTIVE.toString() + _EQUAL_SIGN + _panelComponentsDefective + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.INSPECTION_TIME.toString() + _EQUAL_SIGN + _inspectionTime + _DOLLAR_SIGN);
      fileWriter.writeln(MachineLatestStatusElementEventEnum.INSPECTION_END_TIME.toString() + _EQUAL_SIGN + _inspectionEndTime + _DOLLAR_SIGN);
      fileWriter.writeln(_HASH_SIGN);
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_machineLatestStatusLogFileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriter.close();   
    }
  }
}
