package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when an invalid type is found in a config file
 * for a particular key
 * @author Erica Wheatcroft
 */
public class InvalidTypeDatastoreException extends DatastoreException
{  /**
   * Construct the class.
   * @param key is the key that contains an invalid type
   * @param configFileName is the config file that the key belongs in
   * @param type is the type the defined in HardwareConfigEnum
   * @param wrongValue the wrong value that was given
   * @author Erica Wheatcroft
   */
  public InvalidTypeDatastoreException(String key, String configFileName, TypeEnum type, String wrongValue)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_INVALID_TYPE_KEY", new Object[]{key, configFileName, type, wrongValue}));
    Assert.expect(key != null);
    Assert.expect(configFileName != null);
    Assert.expect(type != null);
    Assert.expect(wrongValue != null);
  }
}
