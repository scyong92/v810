package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to backup a project.
 *
 * @author George A. David
 */
public class FailedTransferringProjectOfflineDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while backing it up
   * @author George A. David
   */
  public FailedTransferringProjectOfflineDatastoreException(String projectName, String offlinePath)
  {
    super(new LocalizedString("DS_ERROR_FAILED_TRANSFERRING_PROJECT_OFFLINE_KEY", new Object[]{projectName, offlinePath}));
    Assert.expect(projectName != null);
    Assert.expect(offlinePath != null);
  }
}
