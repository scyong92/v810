package com.axi.v810.datastore;

import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.XrayTester;
import com.axi.v810.util.*;

/**
 * This class is used to create log for recipe error 
 * @author swee-yee.wong
 */
public class CameraCommandRepliesLogUtil 
{
  private static CameraCommandRepliesLogUtil _instance;
  private FileLoggerAxi _logUtility;
  private String _logFileNameFullPath;
  private boolean _firstLog = true;
  
  /**
   * @author swee-yee.wong
   */
  private CameraCommandRepliesLogUtil()
  {
    //do nothing
  }

   /**
   * @author swee-yee.wong
   */
  public static synchronized CameraCommandRepliesLogUtil getInstance()
  {
    if (_instance == null)
      _instance = new CameraCommandRepliesLogUtil();

    return _instance;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void log(String data) throws XrayTesterException
  {
    if(Config.getInstance().isCameraDebugModeOn())
    {
      _logFileNameFullPath = FileName.getCameraCommandRepliesLogFullPath();
      if (_logFileNameFullPath.isEmpty() == false)
      {
        _logUtility = new FileLoggerAxi(_logFileNameFullPath);
        if(_firstLog == true)
        {
          _logUtility.append("Version: " + Version.getVersion() + " " + XrayTester.getInstance().getMachineDescription());
          _firstLog = false;
        }
        _logUtility.append(data);
      }
    }
  }

}
