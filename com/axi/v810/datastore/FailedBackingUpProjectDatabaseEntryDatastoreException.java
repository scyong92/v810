package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to backup the repository entry.
 *
 * @author George A. David
 */
public class FailedBackingUpProjectDatabaseEntryDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while backing it up
   * @author George A. David
   */
  public FailedBackingUpProjectDatabaseEntryDatastoreException(String projectName, String repositoryPath)
  {
    super(new LocalizedString("DS_ERROR_FAILED_BACKING_UP_PROJECT_DATABASE_ENTRY_KEY", new Object[]{projectName, repositoryPath}));
    Assert.expect(projectName != null);
    Assert.expect(repositoryPath!= null);
  }
}
