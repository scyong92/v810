package com.axi.v810.datastore;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import java.text.*;
import java.io.*;
import java.util.regex.*;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * @author Khaw Chek Hau
 * @VOne Machine Utilization Feature
 */
public class VOneMachineStatusMonitoringSystem
{
  private static VOneMachineStatusMonitoringSystem _instance; 
  private final SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  private final SimpleDateFormat _eventTimeFormat = new SimpleDateFormat("HH:mm:ss");
  private final SimpleDateFormat _fileTimeFormat = new SimpleDateFormat("HH,mm,ss,SSS");              
  private VOneMachineStatusMonitoringEventEnum _machineEvent;
  private boolean _isFaultExist = false;
  private boolean _isServicing = false;
  private boolean _isProgramming = false;
  private boolean _isSetupRunning = false;
  private boolean _isProductionRunning = false;
  private String csvDelimiter = StringLocalizer.keyToString("TABLE_MODEL_CSV_REPORT_DELIMITER_KEY");
  private String _vOneReportFileName;
  private String endLineIndicator = "$";
  private Pattern _vOneLogFileNamePattern_ver1 = Pattern.compile("^([^?]+)(" + _VONE_LOG_FILE_NAME_HEADER + ")(\\d+\\-\\d+\\-\\d+)\\_(\\d+\\,\\d+\\,\\d+\\,\\d+)\\.(csv)$");
  private final SimpleDateFormat _fileDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd_HH,mm,ss,SSS"); 
  private String _dateTime;
  private String _eventTime;
  private String _fileNameTime;
  private static final int _EVENT_LINE_NUMBER_PER_LOG_FILE = 10;
  private static final String _VONE_LOG_FILE_NAME_HEADER = "VOneLogFile_";
  private static final String _CSV_FILE_EXTENSION = ".csv";
  private static final String _UNDERSCORE = "_";
  private List<String> _logFilesList = new ArrayList<String>();
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public static synchronized VOneMachineStatusMonitoringSystem getInstance()
  {
    if (_instance == null)
      _instance = new VOneMachineStatusMonitoringSystem();
    return _instance;
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public VOneMachineStatusMonitoringSystem()
  {
    //Do nothing
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public void writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum machineEvent) throws DatastoreException
  {
    Assert.expect(machineEvent != null);
    
    _machineEvent = machineEvent;
    _dateTime = _dateFormat.format(new Date());
    _eventTime = _eventTimeFormat.format(new Date());
    _fileNameTime = _fileTimeFormat.format(new Date());
    int lineNumber = 0;
        
    String vOneMachineStatusMonitoringReportDir = Directory.getVOneMachineStatusMonitoringReportDir();
    
    if (FileUtilAxi.exists(vOneMachineStatusMonitoringReportDir) == false)
      FileUtilAxi.mkdirs(vOneMachineStatusMonitoringReportDir);
    
    if (_machineEvent == VOneMachineStatusMonitoringEventEnum.MACHINE_ERROR_START)
    {
      _isFaultExist = true;
      _isProductionRunning = false;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.MACHINE_ERROR_CLEARED)
    {
      _isFaultExist = false;
      _machineEvent = previousMachineState();
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PROGRAMMING_START)
    {
      _isProgramming = true;  
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PROGRAMMING_STOP)
    {
      _isProgramming = false;  
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.SERVICE_START)
    {
      _isServicing = true;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.SERVICE_STOP)
    {
      _isServicing = false;
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PRODUCTION_RUNNING)
    {
      _isProductionRunning = true;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PRODUCTION_STOP)
    {
      _isProductionRunning = false;
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PANEL_LOADING_END)
    {
      if (_isProductionRunning)
        _machineEvent = VOneMachineStatusMonitoringEventEnum.PRODUCTION_RUNNING;
      else
        _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.PANEL_UNLOADING_END)
    {
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.MACHINE_SETUP_END)
    {
      if (_isProductionRunning)
        _machineEvent = VOneMachineStatusMonitoringEventEnum.PRODUCTION_RUNNING;
      else
        _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.RECIPE_LOADING_START)
    {
      _machineEvent = VOneMachineStatusMonitoringEventEnum.PROGRAMMING_START;  
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.RECIPE_LOADING_END)
    {
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;  
    }
    else if (_machineEvent == VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING)
    {
      _machineEvent = previousMachineState();
    }

    try
    {
      if (_vOneReportFileName == null) //If there's no any log file being using
      {
        List<String> logFilesList = new ArrayList<String>();
        
        logFilesList = getLogFileListFromDirectory(vOneMachineStatusMonitoringReportDir);
        
        if (logFilesList.isEmpty())
        {
          writeVOneReportLogFile(true, false);
          return;
        }
        else
        {
          defineVOneReportFileName(logFilesList);  
        }
      }
      
      if (_vOneReportFileName != null) //If there is any file found or using
      {
        List<String> logFilesList = new ArrayList<String>();
        
        logFilesList = getLogFileListFromDirectory(vOneMachineStatusMonitoringReportDir);
        
        if (logFilesList.isEmpty())
        {
          writeVOneReportLogFile(true, false);
          return;
        }
        else
        {
          defineVOneReportFileName(logFilesList);  
        }
        
        Path readerPath = Paths.get(_vOneReportFileName);
        Charset charset = Charset.forName("UTF-8");
        BufferedReader reader = Files.newBufferedReader(readerPath, charset);
        LineNumberReader lineReader = new LineNumberReader(reader);       
        String line;
        String lastLine = null;
        lineNumber = 0;

        while ((line = lineReader.readLine()) != null) 
        {
          lastLine = line;
          lineNumber++;
        }
        
        reader.close();
        lineReader.close(); 

        if (lastLine == null || lastLine.contains("#"))
        {
          writeVOneReportLogFile(true, false);
          return;
        }
        
        if (lineNumber == _EVENT_LINE_NUMBER_PER_LOG_FILE)
        {
          writeVOneReportLogFile(false, true); 
          return;
        }
                
        writeVOneReportLogFile(false, false);
      }
      else //if no file name found
      {
        writeVOneReportLogFile(true, false);
      }
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_vOneReportFileName);
      dex.initCause(ex);
      throw dex;
    }
    catch (IOException io)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_vOneReportFileName);
      dex.initCause(io);
      throw dex;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  private void writeVOneReportLogFile(boolean writeNewFile, boolean writeLastLine) throws IOException
  {            
    if (writeNewFile)
    {      
      _vOneReportFileName = Directory.getVOneMachineStatusMonitoringReportDir() + File.separator + _VONE_LOG_FILE_NAME_HEADER + 
                            _dateTime + _UNDERSCORE + _fileNameTime + _CSV_FILE_EXTENSION;    
    }
    
    FileWriterUtil fileWriter = new FileWriterUtil(_vOneReportFileName, true);
    try
    {
      fileWriter.open();
      if (writeNewFile)
      {
        fileWriter.writeln("Version = " + FileName.getVOneMachineStatusMonitoringLogLatestFileVersion());        
      }
      fileWriter.writeln(_dateTime + csvDelimiter + _machineEvent.toString() + csvDelimiter + _eventTime + endLineIndicator);
      
      if (writeLastLine)
      {
        fileWriter.writeln("#");  
      }
    }
    catch (CouldNotCreateFileException ex)
    {
      throw ex;
    }
    finally
    {
      fileWriter.close();   
    }
  }

  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public boolean isFaultExist() throws DatastoreException
  {        
    return _isFaultExist;
  } 
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public boolean isProgramming() throws DatastoreException
  {        
    return _isProgramming;
  } 
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public boolean isServicing() throws DatastoreException
  {        
    return _isServicing;
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public boolean isSetupRunning() throws DatastoreException
  {        
    return _isSetupRunning;
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public boolean isProductionRunning() throws DatastoreException
  {        
    return _isProductionRunning;
  }
  
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  private VOneMachineStatusMonitoringEventEnum previousMachineState()
  {
    if (_isProgramming)
      _machineEvent = VOneMachineStatusMonitoringEventEnum.PROGRAMMING_START;
    else if (_isServicing)
      _machineEvent = VOneMachineStatusMonitoringEventEnum.SERVICE_START;
    else
      _machineEvent = VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING;
    
    return _machineEvent;
  }
    
  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  private void defineVOneReportFileName(List<String> logFilesList)
  {
    Date currentDate = new Date();
    Date latestDate = new Date();

    for (String file: logFilesList)
    {            
      Matcher matcher = _vOneLogFileNamePattern_ver1.matcher(file);
      matcher.reset(file); 

      if (matcher.find() == false) 
      {
        continue;
      }

      String date = matcher.group(3);
      String time = matcher.group(4);

      try
      {
        currentDate = _fileDateTimeFormat.parse(date + _UNDERSCORE + time);
      }
      catch (ParseException e) 
      {
        e.printStackTrace();
      }

      if (file.equalsIgnoreCase(logFilesList.get(0)))
      {
        latestDate = currentDate;  
        _vOneReportFileName = file;
      }
      else
      {
        if (currentDate.getTime() > latestDate.getTime())
        {
          latestDate = currentDate;  
          _vOneReportFileName = file;
        }
      }
    }
  }

  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  private List<String> getLogFileListFromDirectory(String vOneMachineStatusMonitoringReportDir) throws IOException
  {
    //Loop all the files in the directory
    List<String> logFilesList = new ArrayList<String>();
    java.io.File folderToScan = new java.io.File(vOneMachineStatusMonitoringReportDir); 
    java.io.File[] listOfFiles = folderToScan.listFiles();

    for (java.io.File file : listOfFiles)
    {
      if (file.isFile()) 
      {
        Matcher matcher = _vOneLogFileNamePattern_ver1.matcher(file.toString());
        matcher.reset(file.toString()); 

        if (matcher.find()) 
        {
          logFilesList.add(file.toString());
        }
      }
    }
    
    return logFilesList;
  }
}
