package com.axi.v810.datastore.database;

/**
 * @author Cheah Lee Herng
 */
public class ApacheDerbySqlManager extends AbstractSqlManager
{
  private static ApacheDerbySqlManager _instance;

  /**
   * @author Cheah Lee Herng
   */
  private ApacheDerbySqlManager()
  {
    // Do nothing
  }

  /**
   * @return ApacheDerbySqlManager
   * @author Cheah Lee Herng
   */
  public synchronized static ApacheDerbySqlManager getInstance()
  {
    if (_instance == null)
      _instance = new ApacheDerbySqlManager();

    return _instance;
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getUpdateThresholdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(THRESHOLD_VALUE_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("=");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" WHERE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(".");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND EXISTS(SELECT 1 FROM ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" stt ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ON rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" rt ON rt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND rt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append("=?)");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected String getAutoIncrementSqlSyntax()
  {
    return "GENERATED ALWAYS AS IDENTITY";
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected String getDateTimeSqlSyntax()
  {
    return "timeStamp";
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected String getLongVarCharSqlSyntax()
  {
    return "VARCHAR(32672)";
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected String getSmallIntSqlSyntax()
  {
    return "smallint";
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected String getCurrentDateTimeSqlSyntax()
  {
    return "CURRENT_TIMESTAMP";
  }
}
