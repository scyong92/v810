package com.axi.v810.datastore.database;

import java.sql.*;

import com.axi.v810.datastore.*;

/**
 * @author Cheah Lee Herng
 */
public class ApacheDerbyDatabase extends AbstractDatabase
{
  private static final String _DERBY_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
  private static final String _DERBY_DBURL = "jdbc:derby:";
  private static final String _USERNAME = "APP";
  private static final String _PASSWORD = "";

  private static ApacheDerbyDatabase _instance;
  private static boolean _isDatabaseShutdown = false;
  private static boolean _isDatabaseDriverLoaded = false;

  /**
   * @author Cheah Lee Herng
   */
  private ApacheDerbyDatabase()
  {
    // Do nothing
  }

  /**
   * @return ApacheDerbyDatabase
   * @author Cheah Lee Herng
   */
  public synchronized static ApacheDerbyDatabase getInstance()
  {
    if (_instance == null)
      _instance = new ApacheDerbyDatabase();

    return _instance;
  }

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  protected void loadDatabaseDriver() throws DatastoreException
  {
    RelationalDatabaseUtil.loadDatabaseDriver(_DERBY_DRIVER, DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE);
    _isDatabaseShutdown = false;
    _isDatabaseDriverLoaded = true;
  }

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void shutdownDatabase() throws DatastoreException
  {
    if (_isDatabaseShutdown == false && _isDatabaseDriverLoaded)
    {
      try
      {
        DriverManager.getConnection("jdbc:derby:;shutdown=true");
      }
      catch (SQLException sqlEx)
      {
        // A clean shutdown always throws SQL Exception XJ015
        if (((sqlEx.getErrorCode() == 50000) && (sqlEx.getSQLState().equals("XJ015"))))
        {
          _isDatabaseShutdown = true;
          _isDatabaseDriverLoaded = false;
        }
        else
        {
          // If the error code or SQLState is different, we have
          // an unexpected exception (shutdown failed)
          DatastoreException dex = new DatabaseDatastoreException(DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE, sqlEx);
          dex.initCause(sqlEx);
          throw dex;
        }
      }
    }
  }

  /**
   * @param libraryPath String
   * @param createDatabase boolean
   * @return String
   * @author Cheah Lee Herng
   */
  public String getJdbcUrl(String libraryPath, boolean createDatabase)
  {
    if (createDatabase)
      return _DERBY_DBURL + libraryPath + ";create=true";
    else
      return _DERBY_DBURL + libraryPath;
  }

  /**
   * @return SqlManager
   * @author Cheah Lee Herng
   */
  public AbstractSqlManager getSqlManager()
  {
    return ApacheDerbySqlManager.getInstance();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getDatabaseUserName()
  {
    return _USERNAME;
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getDatabasePassword()
  {
    return _PASSWORD;
  }
}
