package com.axi.v810.datastore.database;

import com.axi.v810.datastore.*;
import com.axi.util.Assert;

/**
 * This is an abstract class to maintain a set of names for table names and column names used in database operation.
 * This class also provides methods to get Sql strings.
 *
 * @author Cheah Lee Herng
 */
public abstract class AbstractSqlManager
{
  // DBVersion table
  protected static final String DB_VERSION_TABLE = "DBVersion";
  protected static final String DB_VERSION_ID_COLUMN = "VersionID";
  protected static final String DB_VERSION_COLUMN = "Version";
  
  // RefSubtypes table
  protected static final String REF_SUBTYPE_TABLE = "RefSubtypes";
  protected static final String REF_SUBTYPE_ID_COLUMN = "RefSubtypeID";
  protected static final String SUBTYPE_NAME_COLUMN = "SubtypeName";
  protected static final String COMMENT_COLUMN = "Comment";
  protected static final String SUBTYPE_COMMENT_COLUMN = "SubtypeComment";
  protected static final String REF_JOINT_TYPE_ID_COLUMN = "RefJointTypeID";  

  // RefThresholds table
  protected static final String REF_THRESHOLD_TABLE = "RefThresholds";
  protected static final String REF_THRESHOLD_ID_COLUMN = "RefThresholdID";
  protected static final String REF_ALGORITHM_NAME_ID_COLUMN = "RefAlgorithmNameID";
  protected static final String THRESHOLD_NAME_COLUMN = "ThresholdName";
  protected static final String THRESHOLD_COMMENT_COLUMN = "ThresholdComment";

  // RefShape table
  protected static final String REF_SHAPE_TABLE = "RefShape";
  protected static final String REF_SHAPE_ID_COLUMN = "RefShapeID";
  protected static final String SHAPE_NAME_COLUMN = "Shape";

  // RefDataType table
  protected static final String REF_DATA_TYPE_TABLE = "RefDataType";
  protected static final String REF_DATA_TYPE_ID_COLUMN = "RefDataTypeID";
  protected static final String DATA_TYPE_NAME_COLUMN = "DataTypeName";

  // RefAlgorithmNames table
  protected static final String REF_ALGORITHM_NAME_TABLE = "RefAlgorithmNames";
  protected static final String ALGORITHM_NAME_COLUMN = "AlgorithmName";

  // RefJointType table
  protected static final String REF_JOINT_TYPE_TABLE = "RefJointType";
  protected static final String JOINT_TYPE_NAME_COLUMN = "JointTypeName";
  
  // RefUserGain table
  protected static final String REF_USER_GAIN_TABLE = "RefUserGain";  
  protected static final String USER_GAIN_NAME_COLUMN = "UserGainName";
  
  // RefIntegrationLevel table
  protected static final String REF_INTEGRATION_LEVEL_TABLE = "RefIntegrationLevel";  
  protected static final String INTEGRATION_LEVEL_NAME_COLUMN = "IntegrationLevelName";
  
  // RefArtifactCompensation table
  protected static final String REF_ARTIFACT_COMPENSATION_TABLE = "RefArtifactCompensation";
  protected static final String ARTIFACT_COMPENSATION_NAME_COLUMN = "ArtifactCompensationName";
  
  // RefMagnfication table
  protected static final String REF_MAGNIFICATION_TABLE = "RefMagnification";
  protected static final String MAGNIFICATION_NAME_COLUMN = "MagnificationName";
  
  // RefDynamicRangeOptimization table
  protected static final String REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE = "RefDynamicRangeOptimization";
  protected static final String DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN = "DynamicRangeOptimizationName";
  
  // RefFocusMethod table
  protected static final String REF_FOCUS_METHOD_TABLE = "RefFocusMethod";
  protected static final String FOCUS_METHOD_NAME_COLUMN = "FocusMethodName";
  
  // RefComponent table
  protected static final String REF_COMPONENT_TABLE = "RefComponent";
  protected static final String REF_COMPONENT_ID_COLUMN = "RefComponentID";
  protected static final String COMPONENT_NAME_COLUMN = "ComponentName";

  // Project table
  protected static final String PROJECT_TABLE = "Projects";
  protected static final String PROJECT_ID_COLUMN = "ProjectID";
  protected static final String PROJECT_NAME_COLUMN = "ProjectName";
  protected static final String PROJECT_LOCATION_COLUMN = "ProjectLocation";

  // ProjectToSubtype table
  protected static final String PROJECT_TO_SUBTYPE_TABLE = "ProjectToSubtype";
  protected static final String REF_USER_GAIN_ID_COLUMN = "RefUserGainID";
  protected static final String REF_INTEGRATION_LEVEL_ID_COLUMN = "RefIntegrationLevelID";
  protected static final String REF_ARTIFACT_COMPENSATION_ID_COLUMN = "RefArtifactCompensationID";
  protected static final String REF_MAGNIFICATION_ID_COLUMN = "RefMagnificationID";
  protected static final String REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN = "RefDynamicRangeOptimizationID";
  protected static final String REF_FOCUS_METHOD_ID_COLUMN = "RefFocusMethodID";

  // Packages table
  protected static final String PACKAGE_TABLE = "Packages";
  protected static final String PACKAGE_ID_COLUMN = "PackageID";
  protected static final String PACKAGE_NAME_COLUMN = "PackageName";
  protected static final String PACKAGE_LONG_NAME_COLUMN = "PackageLongName";
  protected static final String PACKAGE_CHECKSUM_COLUMN = "PackageChecksum";

  // SubtypeToThreshold table
  protected static final String SUBTYPE_TO_THRESHOLD_TABLE = "SubtypeToThreshold";

  // LandPatterns table
  protected static final String LAND_PATTERN_TABLE = "LandPatterns";
  protected static final String LAND_PATTERN_ID_COLUMN = "LandPatternID";
  protected static final String LAND_PATTERN_NAME_COLUMN = "LandPatternName";
  protected static final String NUMBER_OF_PADS_COLUMN = "NumberOfPads";
  protected static final String LAND_PATTERN_WIDTH_COLUMN = "Width";
  protected static final String LAND_PATTERN_LENGTH_COLUMN = "Length";

  // LandPatternPads table
  protected static final String LAND_PATTERN_PAD_TABLE = "LandPatternPads";
  protected static final String LAND_PATTERN_PAD_ID_COLUMN = "LandPatternPadID";
  protected static final String PAD_NAME_COLUMN = "PadName";
  protected static final String IS_SURFACE_MOUNT_COLUMN = "IsSurfaceMount";
  protected static final String IS_PAD_ONE_COLUMN = "IsPadOne";
  protected static final String ROTATION_COLUMN = "Rotation";
  protected static final String X_LOCATION_COLUMN = "XLocation";
  protected static final String Y_LOCATION_COLUMN = "YLocation";
  protected static final String X_DIMENSION_COLUMN = "XDimension";
  protected static final String Y_DIMENSION_COLUMN = "YDimension";
  protected static final String HOLE_DIAMETER_COLUMN = "HoleDiameter";
  protected static final String HOLE_X_LOCATION_COLUMN = "HoleXLocation";
  protected static final String HOLE_Y_LOCATION_COLUMN = "HoleYLocation";
  //Siew Yeng - XCR-3318
  protected static final String HOLE_X_DIMENSION_COLUMN = "HoleXDimension";
  protected static final String HOLE_Y_DIMENSION_COLUMN = "HoleYDimension";

  // PackagePins table
  protected static final String PACKAGE_PIN_TABLE = "PackagePins";
  protected static final String PACKAGE_PIN_ID_COLUMN = "PackagePinID";
  protected static final String ORIENTATION_COLUMN = "Orientation";
  protected static final String JOINT_HEIGHT_COLUMN = "JointHeight";
  protected static final String INSPECTABLE_COLUMN = "Inspectable";
  protected static final String TESTABLE_COLUMN = "Testable";

  // SubtypeSchemes table
  protected static final String SUBTYPE_SCHEME_TABLE = "SubtypeSchemes";
  protected static final String SUBTYPE_SCHEME_ID_COLUMN = "SubtypeSchemeID";
  protected static final String PAD_JOINT_TYPES_COLUMN = "PadJointTypes";
  protected static final String PAD_CONSOLIDATE_INFO_COLUMN = "PadConsolidateInfo";
  protected static final String USE_ONE_SUBTYPE_COLUMN = "useOneSubtype";
  protected static final String USE_ONE_JOINT_TYPE_COLUMN = "useOneJointType";
  protected static final String CHECKSUM_COLUMN = "Checksum";
  protected static final String SUBTYPE_SCHEME_DATE_CREATED_COLUMN = "SubtypeSchemeDateCreated";
  protected static final String SUBTYPE_SCHEME_DATE_UPDATED_COLUMN = "SubtypeSchemeDateUpdated";

  // SubtypeSchemeToPackagePin table
  protected static final String SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE = "SubtypeSchemeToPackagePin";
  
  // SubtypeToComponent table
  protected static final String SUBTYPE_TO_COMPONENT_TABLE = "SubtypeToComponent";
  
  // SubtypeToAlgorithm table
  protected static final String SUBTYPE_TO_ALGORITHM_TABLE = "SubtypeToAlgorithm";
  protected static final String ALGORITHM_ENABLE_COLUMN = "AlgorithmEnable";

  protected static final String THRESHOLD_VALUE_COLUMN = "ThresholdValue";

  protected static final String DATE_CREATED_COLUMN = "DateCreated";
  protected static final String DATE_UPDATED_COLUMN = "DateUpdated";
  protected static final String USER_CREATED_COLUMN = "UserCreated";
  protected static final String USER_UPDATED_COLUMN = "UserUpdated";

  protected static final String ALGO_VERSION_COLUMN = "AlgoVersion";

  /**
   * This method returns create land pattern sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateLandPatternSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(NUMBER_OF_PADS_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(LAND_PATTERN_WIDTH_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(LAND_PATTERN_LENGTH_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create package pin sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreatePackageSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PACKAGE_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PACKAGE_LONG_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create subtype scheme sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateSubtypeSchemeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PAD_JOINT_TYPES_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PAD_CONSOLIDATE_INFO_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USE_ONE_JOINT_TYPE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USE_ONE_SUBTYPE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref joint type sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefJointTypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref user gain sql string.
   * @return String
   * @author Cheah Lee Herng 
   */
  public final String getCreateRefUserGainSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(USER_GAIN_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref integration level sql string.
   * @return String
   * @author Cheah Lee Herng 
   */
  public final String getCreateRefIntegrationLevelSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(INTEGRATION_LEVEL_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref artifact compensation sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefArtifactCompensationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(ARTIFACT_COMPENSATION_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref magnification sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefMagnificationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(MAGNIFICATION_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref dynamic range optimization sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefDynamicRangeOptimizationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref focus method sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefFocusMethodSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_FOCUS_METHOD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(FOCUS_METHOD_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create ref component sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefComponentSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_COMPONENT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(COMPONENT_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref subtype sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefSubtypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(SUBTYPE_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(") VALUES (?,?,?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create project sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public String getCreateProjectSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PROJECT_LOCATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create project to subtype sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateProjectToSubtypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");    
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(",");    
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create subtype to component sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateSubtypeToComponentSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(SUBTYPE_TO_COMPONENT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns update subtype to component sql string.
   * 
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getUpdateSubtypeToComponentSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(SUBTYPE_TO_COMPONENT_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("=").append(getCurrentDateTimeSqlSyntax()).append(",");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append("=?");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns create subtype to algorithm sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateSubtypeToAlgorithmSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(ALGORITHM_ENABLE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getSubtypeToComponentBySubtypeComponentInfoSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(SUBTYPE_TO_COMPONENT_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getSubtypeToAlgorithmBySubtypeInfoSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);    
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref algorithm name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefAlgorithmNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(ALGORITHM_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref threshold sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefThresholdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(THRESHOLD_NAME_COLUMN);
    stringBuilder.append(") VALUES(?,?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref data type sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefDataTypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(DATA_TYPE_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create ref shape sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateRefShapeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(SHAPE_NAME_COLUMN);
    stringBuilder.append(") VALUES(?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create subtype to threshold sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateSubtypeToThresholdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(THRESHOLD_VALUE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(ALGO_VERSION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES (?,?,?,?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create land pattern pad sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateLandPatternPadSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PAD_NAME_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(X_LOCATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(Y_LOCATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(X_DIMENSION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(Y_DIMENSION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(ROTATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(IS_SURFACE_MOUNT_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(IS_PAD_ONE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(HOLE_DIAMETER_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(HOLE_X_DIMENSION_COLUMN);//Siew Yeng - XCR-3318 - Oval PTH
    stringBuilder.append(",");
    stringBuilder.append(HOLE_Y_DIMENSION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(HOLE_X_LOCATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(HOLE_Y_LOCATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getUpdateLandPatternPadSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(PAD_NAME_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(X_LOCATION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(Y_LOCATION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(X_DIMENSION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(Y_DIMENSION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(ROTATION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(IS_SURFACE_MOUNT_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(IS_PAD_ONE_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(HOLE_DIAMETER_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(HOLE_X_DIMENSION_COLUMN);//Siew Yeng - XCR-3318 - Oval PTH
    stringBuilder.append("=?,");
    stringBuilder.append(HOLE_Y_DIMENSION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(HOLE_X_LOCATION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(HOLE_Y_LOCATION_COLUMN);
    stringBuilder.append("=?,");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("=").append(getCurrentDateTimeSqlSyntax()).append(",");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? WHERE ");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create package pin sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreatePackagePinSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(ORIENTATION_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(JOINT_HEIGHT_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(INSPECTABLE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(TESTABLE_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,?,?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns create subtype scheme to package pin sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateSubtypeSchemeToPackagePinSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,?,?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref joint type based on joint type name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefJointTypeByJointTypeNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref user gain based on user gain name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefUserGainByUserGainNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(USER_GAIN_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref integration level based on integration level name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefIntegrationLevelByIntegrationLevelNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(INTEGRATION_LEVEL_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref artifact compensation based on artifact compensation name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefArtifactCompensationByArtifactCompensationNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(ARTIFACT_COMPENSATION_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref magnification based on magnification name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefMagnificationByMagnificationNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(MAGNIFICATION_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref dynamic range optimization based on dynamic range optimization name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefDynamicRangeOptimizationByDynamicRangeOptimizationNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref focus method based on focus method name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefFocusMethodByFocusMethodNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_FOCUS_METHOD_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(FOCUS_METHOD_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns ref component based on component name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefComponentByComponentNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_COMPONENT_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(COMPONENT_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getProjectToSubtypeByProjectIdAndRefSubtypeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns project information based on project name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getProjectByProjectNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref algorithm name information based on algorithm name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefAlgorithmNameByAlgorithmNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(ALGORITHM_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref threshold information based on threshold name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefThresholdByThresholdNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(THRESHOLD_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref data type information based on data type name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefDataTypeByDataTypeNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(DATA_TYPE_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref shape information based on shape name sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefShapeByShapeNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(SHAPE_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref subtype information sql string based on project name,
   * subtype name and joint type name.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getRefSubtypeBySubtypeNameAndJointTypeNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);    
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(SUBTYPE_NAME_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND rjt.");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append("=?");    

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns update subtype to threshold sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getUpdateSubtypeToThresholdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(THRESHOLD_VALUE_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("= ");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());;
    stringBuilder.append(", ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append("WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append("AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append("AND ");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public final String getUpdatePackagePinSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(ORIENTATION_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(INSPECTABLE_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(TESTABLE_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns update subtype schemes sql string.
   * @author Poh Kheng
   */
  public final String getUpdateSubtypeSchemesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(PAD_CONSOLIDATE_INFO_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(PAD_JOINT_TYPES_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("= ");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());;
    stringBuilder.append(", ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append("WHERE ");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append("=? ");

    return stringBuilder.toString().intern();
  }

  /**
  * This method returns get matched library subtype scheme by checksum sql string.
  *
  * @return String
  * @author Cheah Lee Herng
  */
  public final String getMatchedLibrarySubtypeSchemeByCheckSumSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT sspp.*, pac.*, rs.*, pp.*, lp.*, lpp.*, rjt.* ");
    stringBuilder.append(" FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" pac ON pac.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = ss.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" pp ON pp.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = pac.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append(" sspp ON sspp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" = ss.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ON rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = sspp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" lp ON lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = pac.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" AND lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" WHERE ss.");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" ORDER BY sspp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(", sspp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);

    return stringBuilder.toString().intern();
  }

  /**
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getMatchedLibrarySubtypeSchemeExistSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * return String
   * @author Cheah Lee Herng
   */
  public final String getLibraryPackageByCheckSumSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns library subtype threshold information sql string.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLibrarySubtypeThresholdValueSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT rs.*, stt.*, rt.*, rdt.*, rjt.*, rug.*, ril.*, rm.*, rac.*, rdro.*, p.*, rs." + COMMENT_COLUMN +
                         " AS " +  SUBTYPE_COMMENT_COLUMN + ", stt." + COMMENT_COLUMN +
                         " AS " + THRESHOLD_COMMENT_COLUMN );
    stringBuilder.append(" FROM ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" stt ON stt.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" rt ON rt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" rdt ON rdt.");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" p ON p.");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(PROJECT_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" pts ON pts.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" rug on rug.");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" ril on ril.");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" rac on rac.");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" rm on rm.");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" rdro on rdro.");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);    
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(SUBTYPE_NAME_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND rjt.");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns library subtype algorithm information sql string.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLibrarySubtypeAlgorithmInfoBySubtypeNameAndJointTypeNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT ran.");
    stringBuilder.append(ALGORITHM_NAME_COLUMN);
    stringBuilder.append(", sta.");
    stringBuilder.append(ALGORITHM_ENABLE_COLUMN);
    stringBuilder.append(" FROM ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" sta ON sta.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" ran ON ran.");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" = sta.");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(SUBTYPE_NAME_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" AND rjt.");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append("=?");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns library subtype algorithm information sql string.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLibrarySubtypeAlgorithmInfoBySubtypeIdAndProjectNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT ran.");
    stringBuilder.append(ALGORITHM_NAME_COLUMN);
    stringBuilder.append(", sta.");
    stringBuilder.append(ALGORITHM_ENABLE_COLUMN);
    stringBuilder.append(" FROM ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" sta ON sta.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" ran ON ran.");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" = sta.");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" p ON p.");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" = sta.");
    stringBuilder.append(PROJECT_ID_COLUMN);    
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND p.");    
    stringBuilder.append(PROJECT_NAME_COLUMN);
    stringBuilder.append("=?");
    
    return stringBuilder.toString().intern();
  }

  /**
   * This method returns createRefDataType table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefDataTypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(DATA_TYPE_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns RefShape table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefShapeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(SHAPE_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns RefAlgorithmNames table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefJointTypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(JOINT_TYPE_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns RefJointType table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefAlgorithmNamesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(ALGORITHM_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefUserGain table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefUserGainSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_GAIN_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefIntegrationLevel table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefIntegrationLevelSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(INTEGRATION_LEVEL_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefArtifactCompensation table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefArtifactCompensationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(ARTIFACT_COMPENSATION_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefMagnification table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefMagnificationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(MAGNIFICATION_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefDynamicRangeOptimization table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefDynamicRangeOptimizationSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefFocusMethod table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefFocusMethodSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_FOCUS_METHOD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(FOCUS_METHOD_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns RefComponent table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefComponentSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_COMPONENT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(COMPONENT_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns Ref_Subtypes table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefSubtypesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(SUBTYPE_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getLongVarCharSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");    
    stringBuilder.append(" CONSTRAINT PK_RefSubtypeId PRIMARY KEY (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_RefSubtype_RefJointTypeId FOREIGN KEY(");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);        
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns RefThresholds table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableRefThresholdsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" int NOT NULL,");
    stringBuilder.append(THRESHOLD_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(" CONSTRAINT PK_RefThresholdId PRIMARY KEY (");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_RefThresholds_RefAlgorithmNameId FOREIGN KEY(");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns Projects table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableProjectsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(PROJECT_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append(PROJECT_LOCATION_COLUMN);
    stringBuilder.append(" varchar(2000), ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ProjecToSubtype table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableProjecToSubtypeSqlString(int refUserGainID, 
                                                             int refIntegrationLevelID, 
                                                             int refArtificialCompensationID,
                                                             int refMagnificationID,
                                                             int refDROID)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");    
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL DEFAULT ").append(refUserGainID).append(", ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(" int NOT NULL DEFAULT ").append(refIntegrationLevelID).append(", ");     
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL DEFAULT ").append(refArtificialCompensationID).append(", ");    
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL DEFAULT ").append(refMagnificationID).append(", ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL DEFAULT ").append(refDROID).append(", ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL , ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_ProjectID_RefSubtypeID PRIMARY KEY (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_ProjectID FOREIGN KEY(");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefSubtypeID FOREIGN KEY(");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("),");
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefUserGainID FOREIGN KEY(");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append("),");    
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefIntegrationLevelID FOREIGN KEY(");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append("),");
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefArtifactCompensationID FOREIGN KEY(");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append("),");    
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefDynamicRangeOptimizationID FOREIGN KEY(");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append("),");    
    stringBuilder.append("CONSTRAINT FK_ProjectToSubtype_RefMagnificationID FOREIGN KEY(");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns SubtypeToThreshold table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableSubtypeToThresholdSqlString(int projectID)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(projectID);
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(THRESHOLD_VALUE_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL,");
    stringBuilder.append(ALGO_VERSION_COLUMN);
    stringBuilder.append(" int NOT NULL,");
    stringBuilder.append(COMMENT_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getLongVarCharSqlSyntax());
    stringBuilder.append(",");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_SubtypesToThreshold PRIMARY KEY (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypesToThreshold_ProjectId FOREIGN KEY(");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypesToThreshold_RefSubtypeId FOREIGN KEY(");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypesToThreshold_RefTresholdId FOREIGN KEY(");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypesToThreshold_RefDataTypeId FOREIGN KEY(");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns SubtypeSchemes table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableSubtypeSchemesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(PAD_JOINT_TYPES_COLUMN);
    stringBuilder.append(" CLOB NOT NULL, ");
    stringBuilder.append(PAD_CONSOLIDATE_INFO_COLUMN);
    stringBuilder.append(" CLOB NOT NULL,");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append(" bigint NOT NULL, ");
    stringBuilder.append(USE_ONE_SUBTYPE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL,");
    stringBuilder.append(USE_ONE_JOINT_TYPE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL ,");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL , ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL , ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_SubytypeSchemes PRIMARY KEY (");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeSchemes_PackageId FOREIGN KEY(");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns Packages table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTablePackagesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append("(");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(PACKAGE_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(PACKAGE_LONG_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append(" bigint NOT NULL, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL , ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_Packages PRIMARY KEY (");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_Packages_LandPatternId FOREIGN KEY(");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns PackagePins table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTablePackagePinsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append("(");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(ORIENTATION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(JOINT_HEIGHT_COLUMN);
    stringBuilder.append(" DECIMAL(10,2) NOT NULL, ");
    stringBuilder.append(INSPECTABLE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(TESTABLE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_PackagePins PRIMARY KEY (");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_PackagePins_LandPatternPadId FOREIGN KEY(");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_PackagePins_RefJointTypeId FOREIGN KEY(");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_PackagePins_PackageId FOREIGN KEY(");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns LandPatterns table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableLandPatternsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append("(");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(LAND_PATTERN_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(NUMBER_OF_PADS_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(LAND_PATTERN_WIDTH_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(LAND_PATTERN_LENGTH_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255)  NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns LandPatternPads table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableLandPatternPadsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append("(");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(PAD_NAME_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL,");
    stringBuilder.append(X_LOCATION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(Y_LOCATION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(X_DIMENSION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(Y_DIMENSION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(ROTATION_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(IS_SURFACE_MOUNT_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL,");
    stringBuilder.append(IS_PAD_ONE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL,");
    stringBuilder.append(HOLE_DIAMETER_COLUMN);
    stringBuilder.append(" int, ");
    stringBuilder.append(HOLE_X_DIMENSION_COLUMN); //Siew Yeng - XCR-3318 - Oval PTH
    stringBuilder.append(" int, ");
    stringBuilder.append(HOLE_Y_DIMENSION_COLUMN);
    stringBuilder.append(" int, ");
    stringBuilder.append(HOLE_X_LOCATION_COLUMN);
    stringBuilder.append(" int, ");
    stringBuilder.append(HOLE_Y_LOCATION_COLUMN);
    stringBuilder.append(" int, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255)  NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_LandPatternPads PRIMARY KEY (");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_LandPatternPads_LandPatternId FOREIGN KEY(");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_LandPatternPads_RefShapeId FOREIGN KEY(");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns SubtypeSchemeToPackagePin table sql string.
   *
   * @return String
   */
  public final String getCreateTableSubtypeSchemeToPackagePinSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append("(");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append(" CONSTRAINT PK_SubtypeSchemeToPackagePin PRIMARY KEY (");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeSchemeToPackagePin_SubtypeSchemeId FOREIGN KEY(");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeSchemeToPackagePin_RefSubtypeId FOREIGN KEY(");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeSchemeToPackagePin_PackagePinId FOREIGN KEY(");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns SubtypeToComponent table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableSubtypeToComponentSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(SUBTYPE_TO_COMPONENT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL , ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");    
    stringBuilder.append(" CONSTRAINT PK_ProjectID_RefSubtypeID_RefComponentID PRIMARY KEY (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeToComponent_ProjectID FOREIGN KEY(");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("), ");    
    stringBuilder.append("CONSTRAINT FK_SubtypeToComponent_RefSubtypeID FOREIGN KEY(");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeToComponent_RefComponentID FOREIGN KEY(");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_COMPONENT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeToComponent_RefFocusMethodID FOREIGN KEY(");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_FOCUS_METHOD_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns SubtypeToAlgorithm table sql string.
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getCreateTableSubtypeToAlgorithmSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(" int NOT NULL, ");
    stringBuilder.append(ALGORITHM_ENABLE_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getSmallIntSqlSyntax());
    stringBuilder.append(" NOT NULL,");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL , ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");    
    stringBuilder.append(" CONSTRAINT PK_ProjectID_RefSubtypeID_RefAlgorithmNameID PRIMARY KEY (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeToAlgorithm_ProjectID FOREIGN KEY(");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("), ");    
    stringBuilder.append("CONSTRAINT FK_SubtypeToAlgorithm_RefSubtypeID FOREIGN KEY(");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("), ");
    stringBuilder.append("CONSTRAINT FK_SubtypeToAlgorithm_RefAlgorithmNameID FOREIGN KEY(");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_ALGORITHM_NAME_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append("))");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns DBVersion sql string.
   * 
   * @return String
   */
  public final String getCreateTableDBVersionSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATE TABLE ");
    stringBuilder.append(DB_VERSION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(DB_VERSION_ID_COLUMN);
    stringBuilder.append(" int NOT NULL ");
    stringBuilder.append(getAutoIncrementSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(DB_VERSION_COLUMN);
    stringBuilder.append(" int NOT NULL , ");
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(" NOT NULL, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append(" ");
    stringBuilder.append(getDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(" varchar(255) NOT NULL, ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append(" varchar(255), ");
    stringBuilder.append("PRIMARY KEY (");
    stringBuilder.append(DB_VERSION_ID_COLUMN);
    stringBuilder.append("))");

    return stringBuilder.toString().intern();
  }
  
  /**
   * This method returns Create DB Version Sql string.
   * 
   * @return String
   */
  public final String getCreateDBVersionSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("INSERT INTO ");
    stringBuilder.append(DB_VERSION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(DB_VERSION_COLUMN);
    stringBuilder.append(",");    
    stringBuilder.append(DATE_CREATED_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(USER_CREATED_COLUMN);
    stringBuilder.append(") VALUES(?,");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(",?)");

    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Siew Yeng
   */
  public String getCurrentDBVersionSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(DB_VERSION_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(DB_VERSION_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns delete database sql string.
   *
   * @param databaseName String
   * @return String
   * @author Cheah Lee Herng
   */
  public String getDeleteDatabaseSqlString(String databaseName)
  {
    Assert.expect(databaseName != null);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DROP DATABASE ");
    stringBuilder.append(databaseName);
    stringBuilder.append(";");

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns package information by package name and land pattern ID sql string.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public String getPackageByPackageLongNameAndLandPatternIDSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PACKAGE_LONG_NAME_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getLandPatternByLandPatternNameAndNumberOfPadsSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(LAND_PATTERN_NAME_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(NUMBER_OF_PADS_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getLandPatternAndLandPatternPadSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" lp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" ORDER BY lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(", lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getPackageAndPackagePinSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" p INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" pp ON pp.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = p.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" ORDER BY p.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(", pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);

    return stringBuilder.toString().intern();
  }

  /**
   * This method returns ref threshold information by ref subtype Id sql string.
   *
   * @return String
   * @author Cheah Lee Herng
   */
  public String getRefThresholdByRefSubtypeIdAndProjectNameSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT rs.*, stt.* , rdt.* , rjt.* , rt.* , p.* , rs." + COMMENT_COLUMN +
                         " AS " +  SUBTYPE_COMMENT_COLUMN + ", stt."+ COMMENT_COLUMN +
                         " AS " + THRESHOLD_COMMENT_COLUMN + " FROM ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" stt ON stt.");    
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_THRESHOLD_TABLE);
    stringBuilder.append(" rt ON rt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" p ON p.");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_DATA_TYPE_TABLE);
    stringBuilder.append(" rdt ON rdt.");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" = stt.");
    stringBuilder.append(REF_DATA_TYPE_ID_COLUMN);
    stringBuilder.append(" WHERE rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append("p.");
    stringBuilder.append(PROJECT_NAME_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This return list of subtype scheme which matches the land pattern name
   *
   * @return String
   * @author Poh Kheng
   */
  public final String getMatchSubtypeSchemesSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT lp.*, p.*, ss.*, ss.");
    stringBuilder.append(DATE_CREATED_COLUMN).append(" AS ").append(SUBTYPE_SCHEME_DATE_CREATED_COLUMN);
    stringBuilder.append(", ss.");
    stringBuilder.append(DATE_UPDATED_COLUMN).append(" AS ").append(SUBTYPE_SCHEME_DATE_UPDATED_COLUMN);
    stringBuilder.append(", p.");
    stringBuilder.append(CHECKSUM_COLUMN).append(" AS ").append(PACKAGE_CHECKSUM_COLUMN);
    stringBuilder.append(" FROM ");
    stringBuilder.append(LAND_PATTERN_TABLE).append(" lp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" p ON p.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE);
    stringBuilder.append(" ss ON ss.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = p.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" WHERE UPPER(lp.");
    stringBuilder.append(LAND_PATTERN_NAME_COLUMN);
    stringBuilder.append(") LIKE ? ");
    stringBuilder.append(" ORDER BY p.").append(PACKAGE_ID_COLUMN);
    stringBuilder.append(", lp.").append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(", lp.").append(NUMBER_OF_PADS_COLUMN);

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLandPatternPadByLandPatternIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT lpp.*, rs.* FROM ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE).append(" lpp, ");
    stringBuilder.append(REF_SHAPE_TABLE).append(" rs ");
    stringBuilder.append(" WHERE ");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("=?");
    stringBuilder.append(" AND lpp.").append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" = rs.").append(REF_SHAPE_ID_COLUMN);

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLandPatternPadByLibrarySubtypeSchemeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT lpp.*, rs.*, lp.* FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE).append(" sstp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE).append(" pp ON pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN).append(" = sstp.").append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE).append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN).append(" = pp.").append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SHAPE_TABLE).append(" rs ON rs.").append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" = lpp.").append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_TABLE).append(" lp ON lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN).append(" = lpp.").append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" WHERE sstp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLandPatternPadByLibraryPackagePinIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT lp.*, lpp.*, rs.* FROM ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" lp INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" pp ON pp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" = lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" rs ON rs.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" = lpp.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" WHERE pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getPackagePinByPackageNameAndJointTypeSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT pp.*, lpp.*, rs.*, rjt.* FROM ");
    stringBuilder.append(PACKAGE_PIN_TABLE).append(" pp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE).append(" sstpp ON sstpp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE).append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" rs ON rs.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" = lpp.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" pac ON pac.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" WHERE pac.");
    stringBuilder.append(PACKAGE_LONG_NAME_COLUMN);
    stringBuilder.append("=?");
    stringBuilder.append(" AND sstpp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * This sql string return the list of subtype by using subtype scheme id
   * @author Poh Kheng
   */
  public final String getLibrarySubtypeBySubtypeSchemeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT DISTINCT rs.*, rjt.*, rug.*, ril.*, rm.*, rac.*, rdro.*, p.* FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TABLE).append(" ss ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append(" sspp ON sspp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" = ss.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");    
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ON rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = sspp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");    
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" pts ON pts.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PROJECT_TABLE);
    stringBuilder.append(" p on p.");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(PROJECT_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" rug on rug.");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" ril on ril.");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" rac on rac.");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" rm on rm.");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" rdro on rdro.");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(" = pts.");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);    
    stringBuilder.append(" WHERE ss.").append(SUBTYPE_SCHEME_ID_COLUMN).append(" = ? ");       
    stringBuilder.append(" ORDER BY rs.").append(SUBTYPE_NAME_COLUMN);

    return stringBuilder.toString();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getLibraryComponentTypeByLibrarySubtypeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT DISTINCT rc.*, fm.* FROM ");
    stringBuilder.append(REF_COMPONENT_TABLE).append(" rc, ");
    stringBuilder.append(SUBTYPE_TO_COMPONENT_TABLE).append(" stc, ");
    stringBuilder.append(REF_FOCUS_METHOD_TABLE).append(" fm, ");
    stringBuilder.append(PROJECT_TABLE).append(" p ");
    stringBuilder.append(" WHERE stc.").append(REF_SUBTYPE_ID_COLUMN).append(" = ? ");
    stringBuilder.append(" AND stc.").append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(" = rc.").append(REF_COMPONENT_ID_COLUMN);
    stringBuilder.append(" AND stc.").append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(" = fm.").append(REF_FOCUS_METHOD_ID_COLUMN);
    stringBuilder.append(" AND stc.").append(PROJECT_ID_COLUMN);
    stringBuilder.append(" = p.").append(PROJECT_ID_COLUMN);
    stringBuilder.append(" ORDER BY rc.").append(COMPONENT_NAME_COLUMN);

    return stringBuilder.toString();
  }
  
  /**   
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLibraryPackageByLibrarySubtypeSchemeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT sstpp.*, p.*, pp.*, lp.*, lpp.*, rs.*, rjt.*, rsp.*, p.");
    stringBuilder.append(CHECKSUM_COLUMN).append(" AS ").append(PACKAGE_CHECKSUM_COLUMN);
    stringBuilder.append(" FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append(" sstpp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" pp ON sstpp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" p ON p.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_TABLE);
    stringBuilder.append(" lp ON lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" = p.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" ON rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = sstpp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ");
    stringBuilder.append(" ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);    
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SHAPE_TABLE);
    stringBuilder.append(" rsp ON rsp.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);
    stringBuilder.append(" = lpp.");
    stringBuilder.append(REF_SHAPE_ID_COLUMN);    
    stringBuilder.append(" WHERE ");
    stringBuilder.append(" sstpp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" =? ");
    stringBuilder.append("ORDER BY sstpp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(", p.");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append(", pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(", lp.");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(", lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    
    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getLibraryPackagePinByLibrarySubtypeSchemeIdSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(SUBTYPE_SCHEME_TO_PACKAGE_PIN_TABLE);
    stringBuilder.append(" sstp ");
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(PACKAGE_PIN_TABLE);
    stringBuilder.append(" pp ON sstp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(PACKAGE_PIN_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" lpp ON lpp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" = pp.");
    stringBuilder.append(LAND_PATTERN_PAD_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_SUBTYPE_TABLE);
    stringBuilder.append(" rs ");
    stringBuilder.append(" ON rs.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" = sstp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN ");
    stringBuilder.append(REF_JOINT_TYPE_TABLE);
    stringBuilder.append(" rjt ");
    stringBuilder.append(" ON rjt.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" = rs.");
    stringBuilder.append(REF_JOINT_TYPE_ID_COLUMN);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(" sstp.");
    stringBuilder.append(SUBTYPE_SCHEME_ID_COLUMN);
    stringBuilder.append(" =? ");
    stringBuilder.append("ORDER BY sstp.");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(", lpp.");
    stringBuilder.append(PAD_NAME_COLUMN);
    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getSubtypeToThresholdBySubtypeInfoSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("SELECT * FROM ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public final String getMatchSubtypeSchemeSql()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" SELECT SS.*, P.*, LP.* FROM SUBTYPESCHEMES SS ");
    stringBuilder.append(" INNER JOIN " + PACKAGE_TABLE + " P ON ");
    stringBuilder.append(" SS." + PACKAGE_ID_COLUMN + " = P." + PACKAGE_ID_COLUMN);
    stringBuilder.append(" INNER JOIN " + LAND_PATTERN_TABLE + " LP ON ");
    stringBuilder.append(" P." + LAND_PATTERN_ID_COLUMN + " = LP." + LAND_PATTERN_ID_COLUMN);
    stringBuilder.append(" WHERE SS." + CHECKSUM_COLUMN + " = ? ");

    return stringBuilder.toString().intern();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getUpdatePackageSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(PACKAGE_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(PACKAGE_NAME_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(PACKAGE_LONG_NAME_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(LAND_PATTERN_ID_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(CHECKSUM_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("=");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PACKAGE_ID_COLUMN);
    stringBuilder.append("=?");

    return stringBuilder.toString().intern();
  }
  
  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public final String getUpdateSubtypeToAlgorithmSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(SUBTYPE_TO_ALGORITHM_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(ALGORITHM_ENABLE_COLUMN);
    stringBuilder.append("=?, ");
    stringBuilder.append(DATE_UPDATED_COLUMN);
    stringBuilder.append("=");
    stringBuilder.append(getCurrentDateTimeSqlSyntax());
    stringBuilder.append(", ");
    stringBuilder.append(USER_UPDATED_COLUMN);
    stringBuilder.append("=? ");
    stringBuilder.append(" WHERE ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append("=? AND ");
    stringBuilder.append(REF_ALGORITHM_NAME_ID_COLUMN);
    stringBuilder.append("=?");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getAlterTableProjectToSubtypeAddRefUserGainIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getAlterTableProjectToSubtypeAddRefIntegrationLevelIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getAlterTableProjectToSubtypeAddRefArtifactCompensationIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getAlterTableProjectToSubtypeAddRefMagnificationIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final String getAlterTableProjectToSubtypeAddRefDynamicRangeOptimizationIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableSubtypeToThresholdAddProjectIDColumnSqlString(int defaultValue)
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(" int DEFAULT ").append(defaultValue);
    stringBuilder.append(" NOT NULL");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableProjectToSubtypeAddRefUserGainIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD CONSTRAINT FK_ALTER_ProjectToSubtype_RefSubtypeID FOREIGN KEY(");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_USER_GAIN_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_USER_GAIN_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableProjectToSubtypeAddRefIntegrationLevelIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD CONSTRAINT FK_ALTER_ProjectToSubtype_RefIntegrationLevelID FOREIGN KEY(");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_INTEGRATION_LEVEL_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_INTEGRATION_LEVEL_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableProjectToSubtypeAddRefArtifactCompensationIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD CONSTRAINT FK_ALTER_ProjectToSubtype_RefArtifactCompensationID FOREIGN KEY(");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_ARTIFACT_COMPENSATION_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableProjectToSubtypeAddRefMagnificationIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD CONSTRAINT FK_ALTER_ProjectToSubtype_RefMagnificationID FOREIGN KEY(");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_MAGNIFICATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_MAGNIFICATION_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableProjectToSubtypeAddRefDynamicRangeOptimizationIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(PROJECT_TO_SUBTYPE_TABLE);
    stringBuilder.append(" ADD CONSTRAINT FK_ALTER_ProjectToSubtype_RefDynamicRangeOptimizationID FOREIGN KEY(");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(") REFERENCES ");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE);
    stringBuilder.append(" (");
    stringBuilder.append(REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableSubtypeToThresholdRemovePrimaryKeyConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" DROP CONSTRAINT PK_SubtypesToThreshold");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public final String getAlterTableSubtypeToThresholdAddProjectIDConstraintSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(SUBTYPE_TO_THRESHOLD_TABLE);
    stringBuilder.append(" ADD CONSTRAINT PK_SubtypesToThreshold PRIMARY KEY(");
    stringBuilder.append(PROJECT_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_SUBTYPE_ID_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(REF_THRESHOLD_ID_COLUMN);
    stringBuilder.append(")");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * Use only for DBversion upgrade to version 3
   * @author Siew Yeng
   */
  public final String getAlterTableLandPatternPadAddHoleWidthColumnSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(HOLE_X_DIMENSION_COLUMN);
    stringBuilder.append(" int");
    
    return stringBuilder.toString().intern();
  }
  
  /**
   * Use only for DBversion upgrade to version 3
   * @author Siew Yeng
   */
  public final String getAlterTableLandPatternPadAddHoleLengthColumnSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ALTER TABLE ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" ADD ");
    stringBuilder.append(HOLE_Y_DIMENSION_COLUMN);
    stringBuilder.append(" int");
    
    return stringBuilder.toString().intern();
  }

  /**
   * Use only for DBversion upgrade to version 3
   * @author Siew Yeng
   */
  public final String getUpdateLandPatternPadHoleWidthHoleLengthColumnSqlString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UPDATE ");
    stringBuilder.append(LAND_PATTERN_PAD_TABLE);
    stringBuilder.append(" SET ");
    stringBuilder.append(HOLE_X_DIMENSION_COLUMN);
    stringBuilder.append("=");
    stringBuilder.append(HOLE_DIAMETER_COLUMN);
    stringBuilder.append(",");
    stringBuilder.append(HOLE_Y_DIMENSION_COLUMN);
    stringBuilder.append("=");
    stringBuilder.append(HOLE_DIAMETER_COLUMN);

    return stringBuilder.toString().intern();
  }
  
  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public abstract String getUpdateThresholdSqlString();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected abstract String getAutoIncrementSqlSyntax();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected abstract String getDateTimeSqlSyntax();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected abstract String getLongVarCharSqlSyntax();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected abstract String getSmallIntSqlSyntax();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  protected abstract String getCurrentDateTimeSqlSyntax();
}
