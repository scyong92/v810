package com.axi.v810.datastore.database;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This is an abstract class to encapsulate different type of database.
 * Everytime there is a new database type coming in, we need to extend from this abstract class.
 * And also, for every new database coming in, we need to create a new SqlManager which will extend from
 * AbstractSqlManager to cater for particular type of Sql syntax.
 *
 * @author Cheah Lee Herng
 */
public abstract class AbstractDatabase
{
  private static Config _config;
  private static DatabaseEnum _database;
  private static AbstractDatabase _instance;

  /**
   * @author Cheah Lee Herng
   */
  static
  {
   _config = Config.getInstance();
   _database = getDatabase();
  }

  /**
   * @return AbstractDatabase
   * @author Cheah Lee Herng
   */
  public synchronized static AbstractDatabase getInstance()
  {
    if (_database.equals(DatabaseEnum.APACHE_DERBY))
    {
      _instance = ApacheDerbyDatabase.getInstance();
    }
    else
      Assert.expect(false);

    return _instance;
  }

  /**
   * @return DatabaseTypeEnum
   * @author Cheah Lee Herng
   */
  private static DatabaseEnum getDatabase()
  {
    String databaseName = _config.getStringValue(SoftwareConfigEnum.DATABASE);
    return DatabaseEnum.getEnum(databaseName);
  }

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  protected abstract void loadDatabaseDriver() throws DatastoreException;

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void startupDatabase() throws DatastoreException
  {
    loadDatabaseDriver();
  }

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public abstract void shutdownDatabase() throws DatastoreException;

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public abstract String getJdbcUrl(String libraryPath, boolean createDatabase);

  /**
   * @return SqlManager
   * @author Cheah Lee Herng
   */
  public abstract AbstractSqlManager getSqlManager();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public abstract String getDatabaseUserName();

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public abstract String getDatabasePassword();
}
