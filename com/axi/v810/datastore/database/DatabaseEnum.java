package com.axi.v810.datastore.database;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class DatabaseEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private final String _name;
  private static Map<String, DatabaseEnum> _databaseNameToDatabaseEnumMap = new HashMap<String, DatabaseEnum>();;

  public static final DatabaseEnum APACHE_DERBY = new DatabaseEnum(++_index, "ApacheDerby");

  /**
   * @param id int
   * @param name String
   * @author Cheah Lee Herng
   */
  private DatabaseEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
   _name = name.intern();

   DatabaseEnum previousValue = _databaseNameToDatabaseEnumMap.put(name, this);
   Assert.expect(previousValue == null);
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @param databaseName String
   * @return DatabaseEnum
   * @author Cheah Lee Herng
   */
  public static DatabaseEnum getEnum(String databaseName)
  {
    Assert.expect(databaseName != null);

    DatabaseEnum databaseEnum = _databaseNameToDatabaseEnumMap.get(databaseName);
    Assert.expect(databaseEnum != null);

    return databaseEnum;
  }
}
