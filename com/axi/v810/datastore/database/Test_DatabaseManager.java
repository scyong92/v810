package com.axi.v810.datastore.database;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.packageLibrary.*;

/**
 * @author Poh Kheng
 */
class Test_DatabaseManager extends UnitTest
{

  /**
   * @author Poh Kheng
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_DatabaseManager());
  }

  /**
   * @author Poh Kheng
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    debugTest();
  }

  /**
   * @author Poh Kheng
   */
  private static void debugTest()
  {
    try
    {
      String projectName = "0027parts";

      Project project = loadProject(projectName, false);
      List<String> libraryPaths = new ArrayList<String>();
      libraryPaths.add(LibraryUtil.getInstance().convertToDatabasePath("C:/Program Files/v810/lib/test5"));
      SearchFilterPersistance searchFilterCriteria = new SearchFilterPersistance();
      searchFilterCriteria.setJointTypeFilter(false);
      searchFilterCriteria.setLandPatternNameFilter("1206", SearchOptionEnum.CONTAINS);
      searchFilterCriteria.setIncludesNonMatches(false);
      ImportLibrarySettingPersistance importLibrarySettingPersistance = new ImportLibrarySettingPersistance();
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LANDPATTERN, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_NOMINAL, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_THRESHOLD, true);


      List<SubtypeScheme> subtypeSchemes = new ArrayList<SubtypeScheme>();
      for(CompPackage compPackage : project.getPanel().getCompPackages())
      {
        for(SubtypeScheme subtypeScheme : compPackage.getSubtypeSchemes())
        {
          subtypeSchemes.add(subtypeScheme);
        }

      }

      DatabaseManager.getInstance().getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(subtypeSchemes, libraryPaths,searchFilterCriteria, importLibrarySettingPersistance);


      for(SubtypeScheme subtypeScheme : subtypeSchemes)
      {
        System.out.println(" --------------------------------------------- ");
        System.out.println("subtypeScheme.getCompPackage().getLongName(): " + subtypeScheme.getCompPackage().getLongName());
        System.out.println("subtypeScheme.getCheckSum(): " + subtypeScheme.getCheckSum());


        for (Map.Entry<String, List<LibraryPackage>> entry : subtypeScheme.getLibraryPathToLibrarySubtypeSchemesMap().entrySet())
        {
          String libraryPath = entry.getKey();
          System.out.println("--libraryPath: " + libraryPath);

          for (LibraryPackage libraryPackage : entry.getValue())
          {
            System.out.println("----libraryPackage.getLandPatternID(): "+libraryPackage.getLandPatternId());
            System.out.println("----libraryPackage.getPackageName(): "+libraryPackage.getPackageName());
            System.out.println("----libraryPackage.getPackageLongName(): "+libraryPackage.getPackageLongName());
            System.out.println("----libraryPackage.getLandPatternId(): "+libraryPackage.getLandPattern().getLandPatternId());
            DatabaseManager.getInstance().getLibraryLandPatternPads(libraryPath, libraryPackage.getLandPattern());

            for(LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getLibrarySubtypeSchemes())
            {
              System.out.println("------librarySubtypeScheme.getSubtypeSchemeID(): "+librarySubtypeScheme.getLibrarySubtypeSchemeId());
              System.out.println("------librarySubtypeScheme.getMatchPercentage(): "+librarySubtypeScheme.getMatchPercentage());

              for(LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
              {
                System.out.println("--------librarySubtype.getSubtypeID(): "+librarySubtype.getSubtypeId());
                System.out.println("--------librarySubtype.getSubtypeName(): "+librarySubtype.getSubtypeName());
                System.out.println("--------librarySubtype.getJointTypeName(): "+librarySubtype.getJointTypeName());
              }
            }
          }
        }
      }


    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Poh Kheng
   */
  private static Project loadProject(String projectName, boolean alwaysImport)
  {
//    System.out.println(FileName.getLegacyPanelNdfFullPath(projectName));
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      if(alwaysImport || Project.doesProjectExistLocally(projectName) == false)
      {
        project = Project.importProjectFromNdfs(projectName, warnings);
      }
      else
        project = Project.load(projectName, new BooleanRef());
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
      Assert.expect(false);
    }

    return project;
  }
}
