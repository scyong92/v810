package com.axi.v810.datastore.database;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.userAccounts.UserAccountsManager;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * This is a class to handle database operation.
 *
 * @author Cheah Lee Herng
 */
public class DatabaseManager
{
  private static final DatabaseTypeEnum PACKAGE_LIBRARY_DATABASE_TYPE_ENUM = DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE;
  private static final String LEGACY = "Legacy";

  private static DatabaseManager _instance;
  private static DatabaseProgressObservable _databaseProgressObservable = DatabaseProgressObservable.getInstance();
  private static List<Connection> _connections = new ArrayList<Connection>();
  private static AbstractDatabase _database = AbstractDatabase.getInstance();
  private static AbstractSqlManager _sqlManager = _database.getSqlManager();
  private static boolean _cancelDatabaseOperation = false;
  private static final double LAND_PATTERN_GEOMETRY_PERCENTAGE = (50.0/100.0)*100.0;
  private static final double LAND_PATTERN_ROTATION_PERCENTAGE = (50.0/100.0)*100.0;
  private static final int UPDATE = 1;
  private static final int ALWAYS_CREATE_NEW = 2;
  private static final int CREATE_IF_NOT_EXIST = 3;
  
  //Siew Yeng - XCR-3318
  // dbVersion 2: support user gain, IL, IC, DRO, Magnification
  // dbVersion 3: add two columm landpattern pad hole width and hole length at landpatternpad table
  private static final int LATEST_DB_VERSION = 3;

  /**
   * @author Cheah Lee Herng
   */
  private DatabaseManager()
  {
    // Do nothing
  }

  /**
   * @return DatabaseManager
   *
   * @author Cheah Lee Herng
   */
  public static synchronized DatabaseManager getInstance()
  {
    if (_instance == null)
      _instance = new DatabaseManager();

    return _instance;
  }

  /**
   * @param databasePathToSubtypeSchemesMap Map
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void exportSubtypeScheme(Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap) throws DatastoreException
  {
    Assert.expect(databasePathToSubtypeSchemesMap != null);

    int packageId = 0;
    int landPatternPadId = 0;
    int correctedLandPatternPadId = 0;
    int holeDiameter = 0;
    //Siew Yeng - XCR-3318
    int holeWidth = 0;
    int holeLength = 0;
    int holeXLocation = 0;
    int holeYLocation = 0;
    int packagePinId = 0;
    int correctedPackagePinId = 0;
    int updateLandPatternPadId = 0;
    int updatePackagePinId = 0;
    long packageCheckSum = 0;
    boolean isPackageExistAndOverwritable = false;
    BooleanRef refSubtypeIdExistInDatabase = new BooleanRef(true);
    String subtypeShortName;
    String subtypeLongName;
    List<Integer> landPatternPadIds = new ArrayList<Integer>();
    List<Integer> packagePinIds = new ArrayList<Integer>();
    List<Integer> usedlandPatternPadIds = new ArrayList<Integer>();
    List<Integer> subtypeSchemeIds = new ArrayList<Integer>();
    List<Integer> usedPackagePinIds = new ArrayList<Integer>();
    List<Integer> updateLandPatternPadIds = new ArrayList<Integer>();
    List<Integer> updatePackagePinIds = new ArrayList<Integer>();

    Map<String, Integer> landPatternNameToLandPatternIdMap = new HashMap<String,Integer>();
    Map<String, Integer> packageLongNameToPackageIdMap = new HashMap<String,Integer>();
    Map<String, Integer> packageLongNameToLandPatternIdMap = new HashMap<String,Integer>();
    Map<String, Integer> jointTypeNameToRefJointTypeIdMap = new HashMap<String,Integer>();
    Map<String, Integer> userGainNameToRefUserGainIdMap = new HashMap<String,Integer>();
    Map<String, Integer> integrationLevelNameToRefIntegrationLevelIdMap = new HashMap<String,Integer>();
    Map<String, Integer> artifactCompensationNameToRefArtifactCompensationIdMap = new HashMap<String,Integer>();
    Map<String, Integer> magnificationNameToRefMagnificationIdMap = new HashMap<String,Integer>();
    Map<String, Integer> dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap = new HashMap<String,Integer>();
    Map<String, Integer> focusMethodNameToRefFocusMethodIdMap = new HashMap<String,Integer>();
    Map<String, Integer> componentRefDesToRefComponentIdMap = new HashMap<String,Integer>();
    Map<String, Integer> subtypeLongNameToSubtypeIdMap = new HashMap<String, Integer>();
    Map<Integer, String> subtypeLongNameToSubtypeCommentMap = new HashMap<Integer,String>();
    Map<String, Integer> projectNameToProjectIdMap = new HashMap<String,Integer>();
    Map<String, Integer> algorithmNameToRefAlgorithmNameIdMap = new HashMap<String,Integer>();
    Map<String, Integer> thresholdNameToRefThresholdIdMap = new HashMap<String,Integer>();
    Map<String, Integer> dataTypeNameToRefDataTypeIdMap = new HashMap<String,Integer>();
    Map<String, Integer> shapeNameToRefShapeIdMap = new HashMap<String,Integer>();
    Map<Integer,List<Integer>> landPatternIdToLandPatternPadIdsMap =  new HashMap<Integer,List<Integer>>();
    Map<Integer,List<Integer>> packageIdToPackagePinIdsMap =  new HashMap<Integer,List<Integer>>();

    // Startup database
    _database.startupDatabase();

    // Export subtype schemes for each database path
    for (Map.Entry<String, List<SubtypeScheme>> mapEntry : databasePathToSubtypeSchemesMap.entrySet())
    {
      if (cancelDatabaseOperation())
        break;

      // Get JDBC connection
      String databaseJdbcUrl = _database.getJdbcUrl(mapEntry.getKey(), false);

      // Create new connection
      Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                    _database.getDatabaseUserName(),
                                                                    _database.getDatabasePassword(),
                                                                    PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
      _connections.add(connection);
      
      // To support new Package Library functionality, we need to make sure 
      // RefUserGain, RefIntegrationLevel, RefMagnification and RefComponent table are created
      updateTableScheme(connection);

      // Create prepared statement
      PreparedStatement createLandPatternPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateLandPatternSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectLandPatternPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternByLandPatternNameAndNumberOfPadsSqlString());
      PreparedStatement createPackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreatePackageSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectPackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getPackageByPackageLongNameAndLandPatternIDSqlString());
      PreparedStatement selectPackageByCheckSumPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryPackageByCheckSumSqlString());
      PreparedStatement updatePackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdatePackageSqlString());
      PreparedStatement createSubtypeSchemePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeSchemeSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement createRefJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefJointTypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefJointTypeByJointTypeNameSqlString());            
      PreparedStatement createRefUserGainPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefUserGainSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefUserGainPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefUserGainByUserGainNameSqlString());      
      PreparedStatement createRefIntegrationLevelPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefIntegrationLevelSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefIntegrationLevelPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefIntegrationLevelByIntegrationLevelNameSqlString());      
      PreparedStatement createRefArtifactCompensationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefArtifactCompensationSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefArtifactCompensationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefArtifactCompensationByArtifactCompensationNameSqlString());      
      PreparedStatement createRefMagnificationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefMagnificationSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefMagnificationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefMagnificationByMagnificationNameSqlString());            
      PreparedStatement createRefDynamicRangeOptimizationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefDynamicRangeOptimizationSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefDynamicRangeOptimizationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefDynamicRangeOptimizationByDynamicRangeOptimizationNameSqlString());      
      PreparedStatement createRefFocusMethodPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefFocusMethodSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefFocusMethodPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefFocusMethodByFocusMethodNameSqlString());      
      PreparedStatement createRefComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefComponentSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefComponentByComponentNameSqlString());      
      PreparedStatement selectProjectToSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getProjectToSubtypeByProjectIdAndRefSubtypeIdSqlString());
      PreparedStatement createSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefSubtypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefSubtypeBySubtypeNameAndJointTypeNameSqlString());
      PreparedStatement createProjectPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateProjectSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectProjectPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getProjectByProjectNameSqlString());
      PreparedStatement createProjectToSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateProjectToSubtypeSqlString());      
      PreparedStatement createSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToComponentSqlString());
      PreparedStatement updateSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateSubtypeToComponentSqlString());
      PreparedStatement selectSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getSubtypeToComponentBySubtypeComponentInfoSqlString());      
      PreparedStatement createSubtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToAlgorithmSqlString());
      PreparedStatement selectSubtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getSubtypeToAlgorithmBySubtypeInfoSqlString());      
      PreparedStatement createRefAlgorithmNamePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefAlgorithmNameSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefAlgorithmNamePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefAlgorithmNameByAlgorithmNameSqlString());
      PreparedStatement selectRefThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefThresholdByThresholdNameSqlString());
      PreparedStatement createRefThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefThresholdSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement selectRefDataTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefDataTypeByDataTypeNameSqlString());
      PreparedStatement createRefDataTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefDataTypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement createSubtypeToThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToThresholdSqlString());
      PreparedStatement updateSubtypeToThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateSubtypeToThresholdSqlString());
      PreparedStatement selectRefShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefShapeByShapeNameSqlString());
      PreparedStatement createRefShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefShapeSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement createLandPatternPadPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateLandPatternPadSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement updateLandPatternPadPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateLandPatternPadSqlString());
      PreparedStatement createPackagePinPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreatePackagePinSqlString(), Statement.RETURN_GENERATED_KEYS);
      PreparedStatement updatePackagePinPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdatePackagePinSqlString());
      PreparedStatement createSubtypeSchemeToPackagePinPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeSchemeToPackagePinSqlString());     
      
      // Populate land pattern Id to list of land pattern pads map
      populateLandPatternIdToLandPatternPadIdsMapFromDatabase(connection, landPatternIdToLandPatternPadIdsMap);

      // Populate package Id list
      populatePackageIdToPackagePinsMap(connection, packageIdToPackagePinIdsMap);

      try
      {
        List<SubtypeScheme> mapEntrySubtypeSchemes = mapEntry.getValue();
        for (SubtypeScheme subtypeScheme : mapEntrySubtypeSchemes)
        {
          // Create land pattern
          int landPatternId = getLandPatternIdCreateIfNotExist(createLandPatternPreparedStatement,
                                                               selectLandPatternPreparedStatement,
                                                               landPatternNameToLandPatternIdMap,
                                                               subtypeScheme.getLandPatternName(),
                                                               subtypeScheme.getNumOfLandPatternPads(),
                                                               subtypeScheme.getLandPattern().getWidthInNanoMeters(),
                                                               subtypeScheme.getLandPattern().getLengthInNanoMeters());

          if (subtypeScheme.getCompPackage().hasLibraryCheckSum())
          {
            packageCheckSum = subtypeScheme.getCompPackage().getLibraryCheckSum();

            // Check if package exists in database based on checksum
            packageId = getLibraryPackageIdByCheckSum(selectPackageByCheckSumPreparedStatement, packageCheckSum);

            if (packageId > 0 && subtypeScheme.getCompPackage().isOverwriteToLibrary())
            {
              isPackageExistAndOverwritable = true;

              // Update package
              updateLibraryPackage(updatePackagePreparedStatement,
                                   subtypeScheme.getCompPackage().getShortName(),
                                   subtypeScheme.getCompPackage().getLongName(),
                                   landPatternId,
                                   packageCheckSum,
                                   packageId);
            }
          }

          if (isPackageExistAndOverwritable == false)
          {
            // Create package
            packageId = getPackageIdCreateIfNotExist(createPackagePreparedStatement,
                                                     selectPackagePreparedStatement,
                                                     packageLongNameToPackageIdMap,
                                                     packageLongNameToLandPatternIdMap,
                                                     subtypeScheme.getPackageShortName(),
                                                     subtypeScheme.getPackageLongName(),
                                                     landPatternId,
                                                     subtypeScheme.getCompPackage().getCheckSum());
          }

          // Create subtype scheme
          int subtypeSchemeId = createSubtypeScheme(createSubtypeSchemePreparedStatement,
                                                    packageId,
                                                    subtypeScheme.getPadJointTypeNames(),
                                                    subtypeScheme.getConcateForCalculationString(),
                                                    subtypeScheme.getCheckSum(),
                                                    subtypeScheme.usesOneJointTypeEnum(),
                                                    subtypeScheme.usesOneSubtype(),
                                                    UserAccountsManager.getCurrentUserLoginName());

          // Get project Id
          int projectId = getProjectIdCreateIfNotExist(createProjectPreparedStatement,
                                                       selectProjectPreparedStatement,
                                                       projectNameToProjectIdMap,
                                                       subtypeScheme.getProjectName());

          // Loop through every pad type
          List<PadType> padTypes = subtypeScheme.getPadTypes();
          for (PadType padType : padTypes)
          {
            Subtype subtype = padType.getPadTypeSettings().getSubtype();
            PackagePin packagePin = padType.getPackagePin();
            LandPatternPad landPatternPad = padType.getLandPatternPad();                        

            // Get ref joint type Id for package pin
            int packagePinRefJointTypeId = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                                         selectRefJointTypePreparedStatement,
                                                                         jointTypeNameToRefJointTypeIdMap,
                                                                         packagePin.getJointTypeEnum().getName());

            // Get ref joint type if for subtype
            int subtypeRefJointTypeId = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                                          selectRefJointTypePreparedStatement,
                                                                          jointTypeNameToRefJointTypeIdMap,
                                                                          subtype.getJointTypeEnum().getName());
            
            // Get ref user gain Id for subtype
            int subtypeRefUserGainId = getRefUserGainIdCreateIfNotExist(createRefUserGainPreparedStatement,
                                                                        selectRefUserGainPreparedStatement,
                                                                        userGainNameToRefUserGainIdMap,
                                                                        subtype.getSubtypeAdvanceSettings().getUserGain().toString());
            
            // Get ref integration level Id for subtype
            int subtypeRefIntegrationLevelId = getRefIntegrationLevelIdCreateIfNotExist(createRefIntegrationLevelPreparedStatement,
                                                                                      selectRefIntegrationLevelPreparedStatement,
                                                                                      integrationLevelNameToRefIntegrationLevelIdMap,
                                                                                      subtype.getSubtypeAdvanceSettings().getSignalCompensation().toString());
            
            // Get ref artifact compensation Id for subtype
            int subtypeRefArtifactCompensationId = getRefArtifactCompensationIdCreateIfNotExist(createRefArtifactCompensationPreparedStatement,
                                                                                                selectRefArtifactCompensationPreparedStatement,
                                                                                                artifactCompensationNameToRefArtifactCompensationIdMap,
                                                                                                subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().toString());
            
            // Get ref magnification Id for subtype
            int subtypeRefMagnificationId = getRefMagnificationIdCreateIfNotExist(createRefMagnificationPreparedStatement,
                                                                                  selectRefMagnificationPreparedStatement,
                                                                                  magnificationNameToRefMagnificationIdMap,
                                                                                  subtype.getSubtypeAdvanceSettings().getMagnificationType().getName());
            
            // Get ref dynamic range optimization Id for subtype
            int subtypeRefDynamicRangeOptimizationId = getRefDynamicRangeOptimizationIdCreateIfNotExist(createRefDynamicRangeOptimizationPreparedStatement,
                                                                                                        selectRefDynamicRangeOptimizationPreparedStatement,
                                                                                                        dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap,
                                                                                                        subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel().toString());            

            // Get subtype long name
            if (subtypeScheme.hasNewSubTypeName(subtype))
            {
              subtypeShortName = subtypeScheme.getNewSubtypeName(subtype);
              subtypeLongName = subtypeScheme.getNewSubtypeName(subtype) + "_" + subtype.getJointTypeEnum().getName();
            }
            else
            {
              subtypeShortName = subtype.getShortName();
              subtypeLongName = subtype.getLongName();
            }

            // Get subtype Id
            int refSubtypeId = getRefSubtypeIdCreateIfNotExist(createSubtypePreparedStatement,
                                                               selectRefSubtypePreparedStatement,
                                                               subtypeLongNameToSubtypeIdMap,
                                                               subtypeShortName,
                                                               subtypeLongName,
                                                               subtype.getJointTypeEnum().getName(),
                                                               subtype.getUserComment(),                                                               
                                                               subtypeRefJointTypeId,                                                               
                                                               refSubtypeIdExistInDatabase);

            if (refSubtypeIdExistInDatabase.getValue() == false)
            {
              // Create project to subtype if record not exist
              createProjectToSubtypeIfNotExist(createProjectToSubtypePreparedStatement,
                                               selectProjectToSubtypePreparedStatement,
                                               projectId,
                                               refSubtypeId,
                                               subtypeRefUserGainId,
                                               subtypeRefIntegrationLevelId,
                                               subtypeRefArtifactCompensationId,
                                               subtypeRefMagnificationId,
                                               subtypeRefDynamicRangeOptimizationId);
              
              // Create algorithm
              createAlgorithm(createRefAlgorithmNamePreparedStatement,
                              selectRefAlgorithmNamePreparedStatement,
                              createSubtypeToAlgorithmPreparedStatement,
                              selectSubtypeToAlgorithmPreparedStatement,
                              algorithmNameToRefAlgorithmNameIdMap,
                              projectId,
                              subtype,
                              refSubtypeId);

              // Create ref threshold
              createThreshold(createRefAlgorithmNamePreparedStatement,
                              selectRefAlgorithmNamePreparedStatement,
                              createRefThresholdPreparedStatement,
                              selectRefThresholdPreparedStatement,
                              createRefDataTypePreparedStatement,
                              selectRefDataTypePreparedStatement,
                              createSubtypeToThresholdPreparedStatement,
                              algorithmNameToRefAlgorithmNameIdMap,
                              thresholdNameToRefThresholdIdMap,
                              dataTypeNameToRefDataTypeIdMap,
                              projectId,
                              subtype,
                              refSubtypeId);                            
            }
            else
            {
              // Create algorithm
              createAlgorithm(createRefAlgorithmNamePreparedStatement,
                              selectRefAlgorithmNamePreparedStatement,
                              createSubtypeToAlgorithmPreparedStatement,
                              selectSubtypeToAlgorithmPreparedStatement,
                              algorithmNameToRefAlgorithmNameIdMap,
                              projectId,
                              subtype,
                              refSubtypeId);
              
              // Update ref threshold
              updateThreshold(createRefAlgorithmNamePreparedStatement,
                              selectRefAlgorithmNamePreparedStatement,
                              createRefThresholdPreparedStatement,
                              selectRefThresholdPreparedStatement,
                              createRefDataTypePreparedStatement,
                              selectRefDataTypePreparedStatement,
                              updateSubtypeToThresholdPreparedStatement,
                              algorithmNameToRefAlgorithmNameIdMap,
                              thresholdNameToRefThresholdIdMap,
                              dataTypeNameToRefDataTypeIdMap,
                              projectId,
                              subtype,
                              refSubtypeId);

            }
            
            // Create ref component
            for(ComponentType componentType : subtype.getComponentTypes())
            {
              for(Component component : componentType.getComponents())
              {
                int refComponentId = getRefComponentIdCreateIfNotExist(createRefComponentPreparedStatement,
                                                                       selectRefComponentPreparedStatement,
                                                                       componentRefDesToRefComponentIdMap,
                                                                       component.getReferenceDesignator());
                
                int refFocusMethodId = getRefFocusMethodIdCreateIfNotExist(createRefFocusMethodPreparedStatement,
                                                                           selectRefFocusMethodPreparedStatement,
                                                                           focusMethodNameToRefFocusMethodIdMap,
                                                                           componentType.getGlobalSurfaceModel().toString());
                
                boolean isSubtypeToComponentExist = isSubtypeToComponentExist(selectSubtypeToComponentPreparedStatement,
                                                                              projectId,
                                                                              refSubtypeId,
                                                                              refComponentId);
                if (isSubtypeToComponentExist)
                {
                  updateSubtypeToComponent(updateSubtypeToComponentPreparedStatement,
                                           projectId,
                                           refSubtypeId,
                                           refComponentId,
                                           refFocusMethodId,
                                           UserAccountsManager.getCurrentUserLoginName());
                }
                else
                {                  
                  createSubtypeToComponent(createSubtypeToComponentPreparedStatement,
                                           projectId,
                                           refSubtypeId,
                                           refComponentId,
                                           refFocusMethodId,
                                           UserAccountsManager.getCurrentUserLoginName());
                }
              }
            }

            // Create ref shape
            int refShapeId = getRefShapeIdCreateIfNotExist(createRefShapePreparedStatement,
                                                           selectRefShapePreparedStatement,
                                                           shapeNameToRefShapeIdMap,
                                                           landPatternPad.getShapeEnum().getName());

            // Get land pattern pad info
            ComponentCoordinate componentCoordinate = landPatternPad.getCoordinateInNanoMeters();
            if (landPatternPad instanceof ThroughHoleLandPatternPad)
            {
              ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
              ComponentCoordinate throughHoleComponentCoordinate = throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
//              holeDiameter = throughHoleLandPatternPad.getHoleDiameterInNanoMeters();
              //Siew Yeng - XCR-3318
              holeWidth = throughHoleLandPatternPad.getHoleWidthInNanoMeters();
              holeLength = throughHoleLandPatternPad.getHoleLengthInNanoMeters();
              holeDiameter = Math.min(holeWidth, holeLength); //Siew Yeng - set hole diameter for old db version(dbversion <3)
              holeXLocation = throughHoleComponentCoordinate.getX();
              holeYLocation = throughHoleComponentCoordinate.getY();
            }
            else if (landPatternPad instanceof SurfaceMountLandPatternPad)
            {
              holeDiameter = 0;
              //Siew Yeng - XCR-3318
              holeWidth = 0;
              holeLength = 0;
              holeXLocation = 0;
              holeYLocation = 0;
            }

            if (landPatternIdToLandPatternPadIdsMap.containsKey(landPatternId) == false)
            {
              // Create land pattern pad
              landPatternPadId = createLandPatternPad(createLandPatternPadPreparedStatement,
                                                      landPatternId,
                                                      landPatternPad.getName(),
                                                      componentCoordinate.getX(),
                                                      componentCoordinate.getY(),
                                                      landPatternPad.getWidthInNanoMeters(),
                                                      landPatternPad.getLengthInNanoMeters(),
                                                      landPatternPad.getDegreesRotation(),
                                                      refShapeId,
                                                      landPatternPad.isSurfaceMountPad(),
                                                      landPatternPad.isPadOne(),
                                                      holeDiameter,
                                                      holeWidth,
                                                      holeLength,
                                                      holeXLocation,
                                                      holeYLocation,
                                                      UserAccountsManager.getCurrentUserLoginName());

              landPatternPadIds.add(landPatternPadId);
            }
            else
            {
              if (isPackageExistAndOverwritable)
              {
                // Get land pattern pad Id
                updateLandPatternPadId = getCorrectedLandPatternPadId(landPatternIdToLandPatternPadIdsMap.get(landPatternId), updateLandPatternPadIds);

                // Update land pattern pad
                updateLandPatternPad(updateLandPatternPadPreparedStatement,
                                     updateLandPatternPadId,
                                     landPatternPad.getName(),
                                     componentCoordinate.getX(),
                                     componentCoordinate.getY(),
                                     landPatternPad.getWidthInNanoMeters(),
                                     landPatternPad.getLengthInNanoMeters(),
                                     landPatternPad.getDegreesRotation(),
                                     refShapeId,
                                     landPatternPad.isSurfaceMountPad(),
                                     landPatternPad.isPadOne(),
                                     holeDiameter,
                                     holeWidth,
                                     holeLength,
                                     holeXLocation,
                                     holeYLocation,
                                     UserAccountsManager.getCurrentUserLoginName());
              }
            }

            if (landPatternIdToLandPatternPadIdsMap.containsKey(landPatternId))
            {
              // Get corrected land pattern pad Id
              correctedLandPatternPadId = getCorrectedLandPatternPadId(landPatternIdToLandPatternPadIdsMap.get(landPatternId),
                                                                       usedlandPatternPadIds);
            }
            else
            {
              correctedLandPatternPadId = landPatternPadId;
            }

            if (packageIdToPackagePinIdsMap.containsKey(packageId) == false)
            {
              // Create package pin
              packagePinId = createPackagePin(createPackagePinPreparedStatement,
                                              correctedLandPatternPadId,
                                              packagePinRefJointTypeId,
                                              packagePin.getPinOrientationEnum().getDegrees(),
                                              packagePin.getJointHeightInNanoMeters(),
                                              packageId,
                                              subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                                              subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                                              UserAccountsManager.getCurrentUserLoginName());

              packagePinIds.add(packagePinId);
            }
            else
            {
              if (isPackageExistAndOverwritable)
              {
                // Get corrected package pin Id
                updatePackagePinId = getCorrectedPackagePinId(packageIdToPackagePinIdsMap.get(packageId), updatePackagePinIds);

                // Update package pin
                updatePackagePin(updatePackagePinPreparedStatement,
                                 packagePinRefJointTypeId,
                                 packagePin.getPinOrientationEnum().getDegrees(),
                                 subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                                 subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                                 updatePackagePinId,
                                 UserAccountsManager.getCurrentUserLoginName());

              }
            }

            if (packageIdToPackagePinIdsMap.containsKey(packageId))
            {
              // Get corrected package pin Id
              correctedPackagePinId = getCorrectedPackagePinId(packageIdToPackagePinIdsMap.get(packageId), usedPackagePinIds);
            }
            else
            {
              correctedPackagePinId = packagePinId;
            }

            // Create subtype scheme to package pin record
            createSubtypeSchemeToPackagePin(createSubtypeSchemeToPackagePinPreparedStatement,
                                            subtypeSchemeId,
                                            refSubtypeId,
                                            correctedPackagePinId,
                                            UserAccountsManager.getCurrentUserLoginName());

            // Reset used land pattern pad Id list
            if (landPatternIdToLandPatternPadIdsMap.containsKey(landPatternId))
            {
              if (usedlandPatternPadIds.size() == landPatternIdToLandPatternPadIdsMap.get(landPatternId).size())
              {
                usedlandPatternPadIds.clear();
              }

              if (updateLandPatternPadIds.size() == landPatternIdToLandPatternPadIdsMap.get(landPatternId).size())
              {
                updateLandPatternPadIds.clear();
              }
            }

            // Reset used package Pin Id list
            if (packageIdToPackagePinIdsMap.containsKey(packageId))
            {
              if (usedPackagePinIds.size() == packageIdToPackagePinIdsMap.get(packageId).size())
              {
                usedPackagePinIds.clear();
              }

              if (updatePackagePinIds.size() == packageIdToPackagePinIdsMap.get(packageId).size())
              {
                updatePackagePinIds.clear();
              }
            }
          }

          // Increase progress bar percentage.
          // If user cancels export operation, rollback database transaction.
          if (UnitTest.unitTesting() == false)
          {
            if (cancelDatabaseOperation())
            {
              rollBackTransaction();
              break;
            }
            else
            {
              _databaseProgressObservable.incrementPercentDone();
            }
          }

          // Assign into list
          subtypeSchemeIds.add(subtypeSchemeId);

          // Assign into map
          if (landPatternIdToLandPatternPadIdsMap.containsKey(landPatternId) == false)
          {
            landPatternIdToLandPatternPadIdsMap.put(landPatternId, landPatternPadIds);
          }

          if (packageIdToPackagePinIdsMap.containsKey(packageId) == false)
          {
            packageIdToPackagePinIdsMap.put(packageId, packagePinIds);
          }

          // Create new land pattern pad Id list
          landPatternPadIds = new ArrayList<Integer>();

          // Create new package pin Id list
          packagePinIds = new ArrayList<Integer>();

          // Reset flag
          isPackageExistAndOverwritable = false;
        }
      }
      catch(DatastoreException dex)
      {
        rollBackTransaction();
        throw dex;
      }

      landPatternNameToLandPatternIdMap.clear();
      landPatternIdToLandPatternPadIdsMap.clear();
      packageIdToPackagePinIdsMap.clear();
      packageLongNameToPackageIdMap.clear();
      packageLongNameToLandPatternIdMap.clear();
      jointTypeNameToRefJointTypeIdMap.clear();
      userGainNameToRefUserGainIdMap.clear();
      integrationLevelNameToRefIntegrationLevelIdMap.clear();
      artifactCompensationNameToRefArtifactCompensationIdMap.clear();
      magnificationNameToRefMagnificationIdMap.clear();
      dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap.clear();
      focusMethodNameToRefFocusMethodIdMap.clear();
      componentRefDesToRefComponentIdMap.clear();
      subtypeLongNameToSubtypeIdMap.clear();
      subtypeLongNameToSubtypeCommentMap.clear();
      projectNameToProjectIdMap.clear();
      algorithmNameToRefAlgorithmNameIdMap.clear();
      thresholdNameToRefThresholdIdMap.clear();
      dataTypeNameToRefDataTypeIdMap.clear();
      shapeNameToRefShapeIdMap.clear();
      landPatternPadIds.clear();
      packagePinIds.clear();
      usedlandPatternPadIds.clear();
      usedPackagePinIds.clear();
      updateLandPatternPadIds.clear();
      updatePackagePinIds.clear();
    }

    // Commit transaction
    if (cancelDatabaseOperation() == false)
      saveIntoDatabase();
  }

  /**
   * @param databasePathToSubtypeSchemesMap Map
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void updateSubtypeScheme(Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap) throws DatastoreException
  {
    Assert.expect(databasePathToSubtypeSchemesMap != null);

    Map<String, Integer> projectNameToProjectIdMap = new HashMap<String,Integer>();
    Map<String, Integer> dataTypeNameToRefDataTypeIDMap = new HashMap<String,Integer>();
    Map<String, Integer> refAlgorithmNameToRefAlgorithmNameIDMap = new HashMap<String,Integer>();
    Map<String, Integer> thresholdNameToRefThresholdIdMap = new HashMap<String,Integer>();
    Map<String, Integer> subtypeNameToRefSubtypeIDMap = new HashMap<String,Integer>();
    Map<String, Integer> jointTypeNameToRefJointTypeIdMap = new HashMap<String,Integer>();    
    Map<String, Integer> userGainNameToRefUserGainIdMap = new HashMap<String,Integer>();
    Map<String, Integer> integrationLevelNameToRefIntegrationLevelIdMap = new HashMap<String,Integer>();
    Map<String, Integer> artifactCompensationNameToRefArtifactCompensationIdMap = new HashMap<String,Integer>();
    Map<String, Integer> magnificationNameToRefMagnificationIdMap = new HashMap<String,Integer>(); 
    Map<String, Integer> dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap = new HashMap<String,Integer>(); 
    Map<String, Integer> componentRefDesToRefComponentIdMap = new HashMap<String,Integer>();
    Map<String, Integer> focusMethodNameToRefFocusMethodIdMap = new HashMap<String,Integer>();
    Map<String, Integer> packageLongNameToPackageIdMap = new HashMap<String,Integer>();
    Map<String, Integer> packageLongNameToLandPatternIdMap = new HashMap<String,Integer>();
    Map<String, Integer> landPatternNameToLandPatternIdMap = new HashMap<String,Integer>();
    Map<String, Integer> shapeNameToRefShapeIdMap = new HashMap<String,Integer>();
    List<SubtypeScheme> subtypeSchemes = new ArrayList<SubtypeScheme>();
    BooleanRef refSubtypeIdExistInDatabase = new BooleanRef(true);
    Algorithm algorithmForCurrentSubtype = null;
    int projectId = 0;
    int subtypeID = 0;
    int refDataTypeID = 0;
    int refAlgorithmNameID = 0;
    int refThresholdID = 0;
    int refJointTypeID = 0;
    int refUserGainID = 0;
    int refIntegrationLevelID = 0;
    int refArtifactCompensationID = 0;
    int refMagnificationID = 0;
    int refDynamicRangeOptimizationID = 0;
    int landPatternId = 0;
    int packageId = 0;
    int currentOperationState = 0;
    boolean createNewPackage = false;

    PreparedStatement updatePreparedStatement;
    PreparedStatement createProjectPreparedStatement;
    PreparedStatement selectProjectPreparedStatement;
    PreparedStatement createLandPatternPreparedStatement;
    PreparedStatement selectLandPatternPreparedStatement;
    PreparedStatement createLandPatternPadPreparedStatement;
    PreparedStatement createRefShapePreparedStatement;
    PreparedStatement selectRefShapePreparedStatement;
    PreparedStatement createRefJointTypePreparedStatement;
    PreparedStatement selectRefJointTypePreparedStatement;    
    PreparedStatement createRefUserGainPreparedStatement;
    PreparedStatement selectRefUserGainPreparedStatement;    
    PreparedStatement createRefIntegrationLevelPreparedStatement;
    PreparedStatement selectRefIntegrationLevelPreparedStatement;    
    PreparedStatement createRefArtifactCompensationPreparedStatement;
    PreparedStatement selectRefArtifactCompensationPreparedStatement;    
    PreparedStatement createRefMagnificationPreparedStatement;
    PreparedStatement selectRefMagnificationPreparedStatement;    
    PreparedStatement createRefDynamicRangeOptimizationPreparedStatement;
    PreparedStatement selectRefDynamicRangeOptimizationPreparedStatement;    
    PreparedStatement createRefComponentPreparedStatement;
    PreparedStatement selectRefComponentPreparedStatement;
    PreparedStatement createRefFocusMethodPreparedStatement;
    PreparedStatement selectRefFocusMethodPreparedStatement;
    PreparedStatement createRefSubtypePreparedStatement;
    PreparedStatement selectRefSubtypePreparedStatement;
    PreparedStatement createProjectToSubtypePreparedStatement;
    PreparedStatement selectProjectToSubtypePreparedStatement;
    PreparedStatement createRefDataTypePreparedStatement;
    PreparedStatement selectRefDataTypePreparedStatement;
    PreparedStatement createSubtypeToAlgorithmPreparedStatement;            
    PreparedStatement selectSubtypeToAlgorithmPreparedStatement;
    PreparedStatement updateSubtypeToAlgorithmPreparedStatement;
    PreparedStatement createRefAlgorithmNamePreparedStatement;
    PreparedStatement selectRefAlgorithmNamePreparedStatement;
    PreparedStatement createRefThresholdPreparedStatement;
    PreparedStatement selectRefThresholdPreparedStatement;
    PreparedStatement createSubtypeToThresholdPreparedStatement;
    PreparedStatement selectSubtypeToThresholdPreparedStatement;
    PreparedStatement createSubtypeToComponentPreparedStatement;
    PreparedStatement updateSubtypeToComponentPreparedStatement;
    PreparedStatement selectSubtypeToComponentPreparedStatement;
    PreparedStatement createPackagePinPreparedStatement;
    PreparedStatement updatePackagePinPreparedStatement;
    PreparedStatement updateLandPatternPadPreparedStatement;
    PreparedStatement selectPackageByCheckSumPreparedStatement;
    PreparedStatement createPackagePreparedStatement;
    PreparedStatement selectPackagePreparedStatement;
    PreparedStatement updatePackagePreparedStatement;

    // Startup database
    _database.startupDatabase();

    for (Map.Entry<String, List<SubtypeScheme>> mapEntry : databasePathToSubtypeSchemesMap.entrySet())
    {
      if (cancelDatabaseOperation())
        break;

      // Get subtype schemes list
      subtypeSchemes = mapEntry.getValue();

      // Get JDBC connection
      String databaseJdbcUrl = _database.getJdbcUrl(mapEntry.getKey(), false);

      // Create new connection
      Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                    _database.getDatabaseUserName(),
                                                                    _database.getDatabasePassword(),
                                                                    PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
      _connections.add(connection);
      
      // To support new Package Library functionality, we need to make sure 
      // RefUserGain, RefIntegrationLevel and RefMagnification table are created
      updateTableScheme(connection);

      // Construct Update statement
      updatePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateThresholdSqlString());
      createProjectPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateProjectSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectProjectPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getProjectByProjectNameSqlString());
      createLandPatternPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateLandPatternSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectLandPatternPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternByLandPatternNameAndNumberOfPadsSqlString());
      createLandPatternPadPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateLandPatternPadSqlString(), Statement.RETURN_GENERATED_KEYS);
      createRefShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefShapeSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefShapeByShapeNameSqlString());
      createRefJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefJointTypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefJointTypeByJointTypeNameSqlString());      
      createRefUserGainPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefUserGainSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefUserGainPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefUserGainByUserGainNameSqlString());      
      createRefIntegrationLevelPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefIntegrationLevelSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefIntegrationLevelPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefIntegrationLevelByIntegrationLevelNameSqlString());            
      createRefArtifactCompensationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefArtifactCompensationSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefArtifactCompensationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefArtifactCompensationByArtifactCompensationNameSqlString());            
      createRefMagnificationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefMagnificationSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefMagnificationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefMagnificationByMagnificationNameSqlString());      
      createRefDynamicRangeOptimizationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefDynamicRangeOptimizationSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefDynamicRangeOptimizationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefDynamicRangeOptimizationByDynamicRangeOptimizationNameSqlString());            
      createRefComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefComponentSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefComponentByComponentNameSqlString());            
      createRefFocusMethodPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefFocusMethodSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefFocusMethodPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefFocusMethodByFocusMethodNameSqlString());      
      createRefSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefSubtypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefSubtypeBySubtypeNameAndJointTypeNameSqlString(), Statement.RETURN_GENERATED_KEYS);
      createProjectToSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateProjectToSubtypeSqlString());
      selectProjectToSubtypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getProjectToSubtypeByProjectIdAndRefSubtypeIdSqlString());
      createRefDataTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefDataTypeSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefDataTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefDataTypeByDataTypeNameSqlString());
      createSubtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToAlgorithmSqlString());
      selectSubtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getSubtypeToAlgorithmBySubtypeInfoSqlString());
      updateSubtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateSubtypeToAlgorithmSqlString());
      createRefAlgorithmNamePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefAlgorithmNameSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefAlgorithmNamePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefAlgorithmNameByAlgorithmNameSqlString());
      createRefThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefThresholdSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectRefThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefThresholdByThresholdNameSqlString());
      createSubtypeToThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToThresholdSqlString());
      selectSubtypeToThresholdPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getSubtypeToThresholdBySubtypeInfoSqlString());      
      createSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateSubtypeToComponentSqlString());
      updateSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateSubtypeToComponentSqlString());
      selectSubtypeToComponentPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getSubtypeToComponentBySubtypeComponentInfoSqlString());      
      createPackagePinPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreatePackagePinSqlString(), Statement.RETURN_GENERATED_KEYS);
      updatePackagePinPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdatePackagePinSqlString());
      updateLandPatternPadPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdateLandPatternPadSqlString());
      selectPackageByCheckSumPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryPackageByCheckSumSqlString());
      createPackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreatePackageSqlString(), Statement.RETURN_GENERATED_KEYS);
      selectPackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getPackageByPackageLongNameAndLandPatternIDSqlString());
      updatePackagePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getUpdatePackageSqlString());

      try
      {
        // Update subtype schemes
        for (SubtypeScheme subtypeScheme : subtypeSchemes)
        {
          // Get project Id
          projectId = getProjectIdCreateIfNotExist(createProjectPreparedStatement,
                                                   selectProjectPreparedStatement,
                                                   projectNameToProjectIdMap,
                                                   subtypeScheme.getProjectName());

          // Determine operation state
          if (subtypeScheme.getCompPackage().isOverwriteToLibrary())
          {
            long packageCheckSum = subtypeScheme.getCompPackage().getLibraryCheckSum();

            // Check if package exists in database based on checksum
            packageId = getLibraryPackageIdByCheckSum(selectPackageByCheckSumPreparedStatement, packageCheckSum);
            if (packageId > 0)
              currentOperationState = UPDATE;
            else
              currentOperationState = CREATE_IF_NOT_EXIST;
          }
          else
          {
            if (subtypeScheme.getCompPackage().hasLibraryCheckSum() && subtypeScheme.getCompPackage().isModifiedAfterImported())
              currentOperationState = ALWAYS_CREATE_NEW;
            else
              currentOperationState = CREATE_IF_NOT_EXIST;
          }

          if (currentOperationState == UPDATE)
          {
            // Get land pattern Id
            landPatternId = getLandPatternId(landPatternNameToLandPatternIdMap,
                                             selectLandPatternPreparedStatement,
                                             subtypeScheme.getLandPatternName(),
                                             subtypeScheme.getNumOfLandPatternPads());

            // Get package Id
            packageId = getPackageID(packageLongNameToPackageIdMap,
                                     packageLongNameToLandPatternIdMap,
                                     selectPackagePreparedStatement,
                                     subtypeScheme.getCompPackage().getShortName(),
                                     subtypeScheme.getCompPackage().getLongName(),
                                     landPatternId);

            // Update land pattern pad and package pin
            updateLandPatternPadAndPackagePin(connection,
                                              updateLandPatternPadPreparedStatement,
                                              createRefShapePreparedStatement,
                                              selectRefShapePreparedStatement,
                                              updatePackagePinPreparedStatement,
                                              createRefJointTypePreparedStatement,
                                              selectRefJointTypePreparedStatement,
                                              jointTypeNameToRefJointTypeIdMap,
                                              shapeNameToRefShapeIdMap,
                                              subtypeScheme,
                                              landPatternId,
                                              packageId);
          }
          else if (currentOperationState == ALWAYS_CREATE_NEW)
          {
            // Create land pattern
            landPatternId = createLandPattern(landPatternNameToLandPatternIdMap,
                                              createLandPatternPreparedStatement,
                                              subtypeScheme.getLandPatternName(),
                                              subtypeScheme.getNumOfLandPatternPads(),
                                              subtypeScheme.getLandPattern().getWidthInNanoMeters(),
                                              subtypeScheme.getLandPattern().getLengthInNanoMeters(),
                                              UserAccountsManager.getCurrentUserLoginName());

            // Create package
            packageId = createPackage(createPackagePreparedStatement,
                                      subtypeScheme.getPackageShortName(),
                                      subtypeScheme.getPackageLongName(),
                                      landPatternId,
                                      subtypeScheme.getCompPackage().getCheckSum(),
                                      UserAccountsManager.getCurrentUserLoginName());

            // Create land pattern pad and package pin
            createLandPatternPadAndPackagePin(createLandPatternPadPreparedStatement,
                                              createRefShapePreparedStatement,
                                              selectRefShapePreparedStatement,
                                              createPackagePinPreparedStatement,
                                              createRefJointTypePreparedStatement,
                                              selectRefJointTypePreparedStatement,
                                              jointTypeNameToRefJointTypeIdMap,
                                              shapeNameToRefShapeIdMap,
                                              subtypeScheme,
                                              landPatternId,
                                              packageId);
          }
          else
          {
            // Get land pattern Id
            landPatternId = getLandPatternIdCreateIfNotExist(createLandPatternPreparedStatement,
                                                             selectLandPatternPreparedStatement,
                                                             landPatternNameToLandPatternIdMap,
                                                             subtypeScheme.getLandPatternName(),
                                                             subtypeScheme.getNumOfLandPatternPads(),
                                                             subtypeScheme.getLandPattern().getWidthInNanoMeters(),
                                                             subtypeScheme.getLandPattern().getLengthInNanoMeters());

            // Get package Id
            packageId = getPackageID(packageLongNameToPackageIdMap,
                                     packageLongNameToLandPatternIdMap,
                                     selectPackagePreparedStatement,
                                     subtypeScheme.getPackageShortName(),
                                     subtypeScheme.getPackageLongName(),
                                     landPatternId);
            if (packageId == 0)
            {
              createNewPackage = true;
              packageId = createPackage(createPackagePreparedStatement,
                                        subtypeScheme.getPackageShortName(),
                                        subtypeScheme.getPackageLongName(),
                                        landPatternId,
                                        subtypeScheme.getCompPackage().getCheckSum(),
                                        UserAccountsManager.getCurrentUserLoginName());
            }
            else
            {
              // Update package
              updateLibraryPackage(updatePackagePreparedStatement,
                                   subtypeScheme.getCompPackage().getShortName(),
                                   subtypeScheme.getCompPackage().getLongName(),
                                   landPatternId,
                                   subtypeScheme.getCompPackage().getCheckSum(),
                                   packageId);
            }

            if (createNewPackage)
            {
              // Create/Update land pattern pad and package pin
              createLandPatternPadAndPackagePinCreateIfNotExist(connection,
                                                                createLandPatternPadPreparedStatement,
                                                                updateLandPatternPadPreparedStatement,
                                                                createRefShapePreparedStatement,
                                                                selectRefShapePreparedStatement,
                                                                createPackagePinPreparedStatement,
                                                                updatePackagePinPreparedStatement,
                                                                createRefJointTypePreparedStatement,
                                                                selectRefJointTypePreparedStatement,
                                                                jointTypeNameToRefJointTypeIdMap,
                                                                shapeNameToRefShapeIdMap,
                                                                subtypeScheme,
                                                                landPatternId,
                                                                packageId,
                                                                createNewPackage);
            }
            else
            {
              // Update land pattern pad and package pin
              updateLandPatternPadAndPackagePin(connection,
                                                updateLandPatternPadPreparedStatement,
                                                createRefShapePreparedStatement,
                                                selectRefShapePreparedStatement,
                                                updatePackagePinPreparedStatement,
                                                createRefJointTypePreparedStatement,
                                                selectRefJointTypePreparedStatement,
                                                jointTypeNameToRefJointTypeIdMap,
                                                shapeNameToRefShapeIdMap,
                                                subtypeScheme,
                                                landPatternId,
                                                packageId);
            }
          }

          // Get list of subtypes for current subtype scheme
          List<Subtype> subtypes = subtypeScheme.getSubtypes();
          for (Subtype subtype : subtypes)
          {
            // Get ref joint type Id
            refJointTypeID = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                               selectRefJointTypePreparedStatement,
                                                               jointTypeNameToRefJointTypeIdMap,
                                                               subtype.getJointTypeEnum().getName());
            
            // Get ref user gain Id for subtype
            refUserGainID = getRefUserGainIdCreateIfNotExist(createRefUserGainPreparedStatement,
                                                             selectRefUserGainPreparedStatement,
                                                             userGainNameToRefUserGainIdMap,
                                                             subtype.getSubtypeAdvanceSettings().getUserGain().toString());
            
            // Get ref integration level Id for subtype
            refIntegrationLevelID = getRefIntegrationLevelIdCreateIfNotExist(createRefIntegrationLevelPreparedStatement,
                                                                             selectRefIntegrationLevelPreparedStatement,
                                                                             integrationLevelNameToRefIntegrationLevelIdMap,
                                                                             subtype.getSubtypeAdvanceSettings().getSignalCompensation().toString());
            
            // Get ref artifact compensation Id for subtype
            refArtifactCompensationID = getRefArtifactCompensationIdCreateIfNotExist(createRefArtifactCompensationPreparedStatement,
                                                                                     selectRefArtifactCompensationPreparedStatement,
                                                                                     artifactCompensationNameToRefArtifactCompensationIdMap,
                                                                                     subtype.getSubtypeAdvanceSettings().getArtifactCompensationState().toString());
            
            // Get ref magnification Id for subtype
            refMagnificationID = getRefMagnificationIdCreateIfNotExist(createRefMagnificationPreparedStatement,
                                                                       selectRefMagnificationPreparedStatement,
                                                                       magnificationNameToRefMagnificationIdMap,
                                                                       subtype.getSubtypeAdvanceSettings().getMagnificationType().getName());
            
            // Get ref dynamic range optimization Id for subtype
            refDynamicRangeOptimizationID = getRefDynamicRangeOptimizationIdCreateIfNotExist(createRefDynamicRangeOptimizationPreparedStatement,
                                                                                             selectRefDynamicRangeOptimizationPreparedStatement,
                                                                                             dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap,
                                                                                             subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel().toString());

            // Get subtype Id
            subtypeID = getRefSubtypeIdCreateIfNotExist(createRefSubtypePreparedStatement,
                                                        selectRefSubtypePreparedStatement,
                                                        subtypeNameToRefSubtypeIDMap,
                                                        subtype.getShortName(),
                                                        subtype.getLongName(),
                                                        subtype.getJointTypeEnum().getName(),
                                                        subtype.getUserComment(),                                                        
                                                        refJointTypeID,                                                        
                                                        refSubtypeIdExistInDatabase);

            // Create project to subtype if record not exist
            createProjectToSubtypeIfNotExist(createProjectToSubtypePreparedStatement,
                                             selectProjectToSubtypePreparedStatement,
                                             projectId,
                                             subtypeID,
                                             refUserGainID,
                                             refIntegrationLevelID,
                                             refArtifactCompensationID,
                                             refMagnificationID,
                                             refDynamicRangeOptimizationID);
            
            // Create ref component
            for(ComponentType componentType : subtype.getComponentTypes())
            {
              for(Component component : componentType.getComponents())
              {
                int refComponentId = getRefComponentIdCreateIfNotExist(createRefComponentPreparedStatement,
                                                                       selectRefComponentPreparedStatement,
                                                                       componentRefDesToRefComponentIdMap,
                                                                       component.getReferenceDesignator());
                
                int refFocusMethodId = getRefFocusMethodIdCreateIfNotExist(createRefFocusMethodPreparedStatement,
                                                                           selectRefFocusMethodPreparedStatement,
                                                                           focusMethodNameToRefFocusMethodIdMap,
                                                                           componentType.getGlobalSurfaceModel().toString());
                
                boolean isSubtypeToComponentExist = isSubtypeToComponentExist(selectSubtypeToComponentPreparedStatement,
                                                                              projectId,
                                                                              subtypeID,
                                                                              refComponentId);
                if (isSubtypeToComponentExist)
                {
                  updateSubtypeToComponent(updateSubtypeToComponentPreparedStatement,
                                           projectId,
                                           subtypeID,
                                           refComponentId,
                                           refFocusMethodId,
                                           UserAccountsManager.getCurrentUserLoginName());
                }
                else
                {                  
                  createSubtypeToComponent(createSubtypeToComponentPreparedStatement,
                                           projectId,
                                           subtypeID,
                                           refComponentId,
                                           refFocusMethodId,
                                           UserAccountsManager.getCurrentUserLoginName());
                }
              }
            }
            
            List<AlgorithmEnum> algorithmEnums = subtype.getAlgorithmEnums();
            for (AlgorithmEnum algorithmEnum : algorithmEnums)
            {
              // Get ref algorithm name Id
              refAlgorithmNameID = getRefAlgorithmNameIdCreateIfNotExist(createRefAlgorithmNamePreparedStatement,
                                                                         selectRefAlgorithmNamePreparedStatement,
                                                                         refAlgorithmNameToRefAlgorithmNameIDMap,
                                                                         algorithmEnum.getName());
              
              if (doesSubtypeToAlgorithmExist(selectSubtypeToAlgorithmPreparedStatement, projectId, subtypeID, refAlgorithmNameID) == false)
              {
                createSubtypeToAlgorithm(createSubtypeToAlgorithmPreparedStatement, 
                                         projectId, 
                                         subtypeID, 
                                         refAlgorithmNameID,
                                         subtype.isAlgorithmEnabled(algorithmEnum),
                                         UserAccountsManager.getCurrentUserLoginName());
              }
              else
              {
                // Add to batch
                RelationalDatabaseUtil.setBoolean(updateSubtypeToAlgorithmPreparedStatement, 1, subtype.isAlgorithmEnabled(algorithmEnum));
                RelationalDatabaseUtil.setString(updateSubtypeToAlgorithmPreparedStatement, 2, UserAccountsManager.getCurrentUserLoginName()); 
                RelationalDatabaseUtil.setInt(updateSubtypeToAlgorithmPreparedStatement, 3, projectId);
                RelationalDatabaseUtil.setInt(updateSubtypeToAlgorithmPreparedStatement, 4, subtypeID);
                RelationalDatabaseUtil.setInt(updateSubtypeToAlgorithmPreparedStatement, 5, refAlgorithmNameID);                               
                RelationalDatabaseUtil.addBatch(updateSubtypeToAlgorithmPreparedStatement);
              }
            }

            List<AlgorithmSettingEnum> algorithmSettingEnums = subtype.getTunedAlgorithmSettingEnums();
            for (AlgorithmSettingEnum algorithmSettingEnum : algorithmSettingEnums)
            {
              Serializable serializableValue = subtype.getAlgorithmSettingValue(algorithmSettingEnum);
              String thresholdValueDataType = getTypeString(serializableValue);

              // Get ref data type Id
              refDataTypeID = getRefDataTypeIdCreateIfNotExist(createRefDataTypePreparedStatement,
                                                               selectRefDataTypePreparedStatement,
                                                               dataTypeNameToRefDataTypeIDMap,
                                                               thresholdValueDataType);

              // Get algorithm for current subtype
              algorithmForCurrentSubtype = getAlgorithmForCurrentSubtype(subtype.getAlgorithms(), algorithmSettingEnum);

              // Get ref algorithm name Id
              refAlgorithmNameID = getRefAlgorithmNameIdCreateIfNotExist(createRefAlgorithmNamePreparedStatement,
                                                                         selectRefAlgorithmNamePreparedStatement,
                                                                         refAlgorithmNameToRefAlgorithmNameIDMap,
                                                                         algorithmForCurrentSubtype.getAlgorithmEnum().getName());

              // Get ref threshold Id
              refThresholdID = getRefThresholdIdCreateIfNotExist(createRefThresholdPreparedStatement,
                                                                 selectRefThresholdPreparedStatement,
                                                                 thresholdNameToRefThresholdIdMap,
                                                                 EnumStringLookup.getInstance().getAlgorithmSettingString(algorithmSettingEnum),
                                                                 refAlgorithmNameID);

              // Check if subtype to threshold exist
              if (doesSubtypeToThresholdExist(selectSubtypeToThresholdPreparedStatement, projectId, subtypeID, refThresholdID) == false)
              {
                // Create new subtype to threshold
                createSubtypeToThreshold(createSubtypeToThresholdPreparedStatement,
                                         projectId,
                                         subtypeID,
                                         refThresholdID,
                                         getThresholdValue(serializableValue),
                                         refDataTypeID,
                                         1.0,
                                         subtype.getAlgorithmSettingUserComment(algorithmSettingEnum),
                                         UserAccountsManager.getCurrentUserLoginName());
              }
              else
              {
                // Add to batch
                RelationalDatabaseUtil.setString(updatePreparedStatement, 1, getThresholdValue(serializableValue));
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 2, refDataTypeID);
                RelationalDatabaseUtil.setString(updatePreparedStatement, 3, subtype.getAlgorithmSettingUserComment(algorithmSettingEnum));
                RelationalDatabaseUtil.setString(updatePreparedStatement, 4, UserAccountsManager.getCurrentUserLoginName());
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 5, subtypeID);
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 6, refThresholdID);
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 7, subtypeID);
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 8, refThresholdID);
                RelationalDatabaseUtil.setInt(updatePreparedStatement, 9, refJointTypeID);
                RelationalDatabaseUtil.addBatch(updatePreparedStatement);
              }
            }
          }

          if (UnitTest.unitTesting() == false)
          {
            if (cancelDatabaseOperation())
            {
              rollBackTransaction();
              break;
            }
            else
            {
              _databaseProgressObservable.incrementPercentDone();
            }
          }

          // Reset
          packageId = 0;
          createNewPackage = false;
        }
        // Execute batch update
        RelationalDatabaseUtil.executeBatch(updateSubtypeToAlgorithmPreparedStatement);
        RelationalDatabaseUtil.executeBatch(updatePreparedStatement);
      }
      catch(DatastoreException dex)
      {
        rollBackTransaction();
        throw dex;
      }

      // Release memory
      projectNameToProjectIdMap.clear();
      dataTypeNameToRefDataTypeIDMap.clear();
      refAlgorithmNameToRefAlgorithmNameIDMap.clear();
      thresholdNameToRefThresholdIdMap.clear();
      subtypeNameToRefSubtypeIDMap.clear();
      jointTypeNameToRefJointTypeIdMap.clear();      
      userGainNameToRefUserGainIdMap.clear();
      integrationLevelNameToRefIntegrationLevelIdMap.clear();
      artifactCompensationNameToRefArtifactCompensationIdMap.clear();
      magnificationNameToRefMagnificationIdMap.clear();
      componentRefDesToRefComponentIdMap.clear();
      packageLongNameToPackageIdMap.clear();
      packageLongNameToLandPatternIdMap.clear();
      landPatternNameToLandPatternIdMap.clear();
      shapeNameToRefShapeIdMap.clear();

      updatePreparedStatement = null;
      createProjectPreparedStatement = null;
      selectProjectPreparedStatement = null;
      createLandPatternPreparedStatement = null;
      selectLandPatternPreparedStatement = null;
      createLandPatternPadPreparedStatement = null;
      createRefShapePreparedStatement = null;
      selectRefShapePreparedStatement = null;
      createRefJointTypePreparedStatement = null;
      selectRefJointTypePreparedStatement = null;
      createRefSubtypePreparedStatement = null;
      selectRefSubtypePreparedStatement = null;
      createProjectToSubtypePreparedStatement = null;
      selectProjectToSubtypePreparedStatement = null;
      createRefDataTypePreparedStatement = null;      
      selectRefDataTypePreparedStatement = null;
      createSubtypeToAlgorithmPreparedStatement = null;
      selectSubtypeToAlgorithmPreparedStatement = null;
      updateSubtypeToAlgorithmPreparedStatement = null;
      createRefAlgorithmNamePreparedStatement = null;
      selectRefAlgorithmNamePreparedStatement = null;
      createRefThresholdPreparedStatement = null;
      selectRefThresholdPreparedStatement = null;
      createSubtypeToThresholdPreparedStatement = null;
      selectSubtypeToThresholdPreparedStatement = null;
      createPackagePinPreparedStatement = null;
      updatePackagePinPreparedStatement = null;
      updateLandPatternPadPreparedStatement = null;
      selectPackageByCheckSumPreparedStatement = null;
      createPackagePreparedStatement = null;
      selectPackagePreparedStatement = null;
      updatePackagePreparedStatement = null;
    }

    // Commit transaction
    if (cancelDatabaseOperation() == false)
      saveIntoDatabase();
  }

  /**
   * @param libraryPath String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createDatabase(String libraryPath) throws DatastoreException
  {
    Assert.expect(libraryPath != null);

    // Startup database
    _database.startupDatabase();

    try
    {
      // Get JDBC connection
      String jdbcUrl = _database.getJdbcUrl(libraryPath, true);
      Connection connection = RelationalDatabaseUtil.openConnection(jdbcUrl,
                                                                    _database.getDatabaseUserName(),
                                                                    _database.getDatabasePassword(),
                                                                    PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create DBVersion table
//      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableDBVersionSqlString());
      //Siew Yeng - XCR-3318 - set DBversion when create new library
      createTableDBVersionIfNecessary(connection);

      // Create RefDataType table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefDataTypeSqlString());
      
      // Create RefUserGain table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefUserGainSqlString());
      
      // Create RefIntegrationLevel table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefIntegrationLevelSqlString());
      
      // Create RefArtifactCompensation table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefArtifactCompensationSqlString());
      
      // Create RefMagnification table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefMagnificationSqlString());
      
      // Create RefDynamicRangeOptimization table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefDynamicRangeOptimizationSqlString());
      
      // Create RefFocusMethod table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefFocusMethodSqlString());

      // Create RefShape table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefShapeSqlString());

      // Create RefAlgorithmNames table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefAlgorithmNamesSqlString());

      // Create RefJointType table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefJointTypeSqlString());

      // Create Ref_Subtypes table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefSubtypesSqlString());

      // Create RefThresholds table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefThresholdsSqlString());
      
      // Create RefComponent table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefComponentSqlString());

      // Create Project table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableProjectsSqlString());
      
      // Pre-insert default data into Ref table
      preInsertDefaultData(connection);

      // Create LandPatterns table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableLandPatternsSqlString());

      // Create LandPatternPads table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableLandPatternPadsSqlString());

      // Create Packages table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTablePackagesSqlString());

      // Create PackagePins table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTablePackagePinsSqlString());

      // Create SubtypeSchemes table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeSchemesSqlString());

      // Create SubtypeSchemeToPackagePin table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeSchemeToPackagePinSqlString());
      
      // Create SubtypeToComponent table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeToComponentSqlString());
      
      // Create SubtypeToAlgorithm table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeToAlgorithmSqlString());

      // Commit changes
      RelationalDatabaseUtil.commit(connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(statement);

      // Close connection
      RelationalDatabaseUtil.closeConnection(connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
    }
    finally
    {
      _database.shutdownDatabase();
    }
  }

  /**
   *
   * @param subtypeScheme SubtypeScheme
   * @return boolean
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public boolean doesMatchedLibrarySubtypeSchemeExist(SubtypeScheme subtypeScheme, Connection connection) throws DatastoreException
  {
    Assert.expect(subtypeScheme != null);
    Assert.expect(connection != null);

    boolean doesMatchedLibrarySubtypeSchemeExist = false;
    PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getMatchedLibrarySubtypeSchemeExistSqlString());
    ResultSet resultSet;

    try
    {
      RelationalDatabaseUtil.setLong(preparedStatement, 1, subtypeScheme.getCheckSum());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      if (resultSet.next())
        doesMatchedLibrarySubtypeSchemeExist = true;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return doesMatchedLibrarySubtypeSchemeExist;
  }

  /**
   * @param databasePathToSubtypeSchemesMap Map
   * @return Map
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public Map<SubtypeScheme, Boolean> doesMatchedLibrarySubtypeSchemeExist(Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap)
      throws DatastoreException
  {
    Assert.expect(databasePathToSubtypeSchemesMap != null);

    Map<SubtypeScheme, Boolean> subtypeSchemeToBooleanMap = new HashMap<SubtypeScheme,Boolean>();
    boolean doesMatchedLibrarySubtypeSchemeExist = false;

    // Startup database
    _database.startupDatabase();

    try
    {
      // Export subtype schemes for each database path
      for (Map.Entry<String, List<SubtypeScheme>> mapEntry : databasePathToSubtypeSchemesMap.entrySet())
      {
        // Get JDBC connection
        String databaseJdbcUrl = _database.getJdbcUrl(mapEntry.getKey(), false);

        // Create new connection
        Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                      _database.getDatabaseUserName(),
                                                                      _database.getDatabasePassword(),
                                                                      PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

        List<SubtypeScheme> sortedSubtypeSchemes = mapEntry.getValue();
        for (SubtypeScheme currentSubtypeScheme : sortedSubtypeSchemes)
        {
          doesMatchedLibrarySubtypeSchemeExist = doesMatchedLibrarySubtypeSchemeExist(currentSubtypeScheme, connection);
          subtypeSchemeToBooleanMap.put(currentSubtypeScheme, doesMatchedLibrarySubtypeSchemeExist);
        }
      }
    }
    finally
    {
      _database.shutdownDatabase();
    }
    return subtypeSchemeToBooleanMap;
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param checkSum long
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getLibraryPackageIdByCheckSum(PreparedStatement preparedStatement, long checkSum) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(checkSum > 0);

    ResultSet resultSet;
    int libraryPackageId = 0;

    try
    {
      RelationalDatabaseUtil.setLong(preparedStatement, 1, checkSum);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      if (resultSet.next())
      {
        libraryPackageId = resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN);
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return libraryPackageId;
  }

  /**
   * This method creates a new land pattern record.
   *
   * @param preparedStatement PreparedStatement
   * @param landPatternName String
   * @param numOfLandPatternPads int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createLandPattern(Map<String, Integer> landPatternNameToLandPatternIdMap,
                               PreparedStatement preparedStatement,
                               String landPatternName,
                               int numOfLandPatternPads,
                               int landPatternWidth,
                               int landPatternLength,
                               String userCreated) throws DatastoreException
  {
    Assert.expect(landPatternNameToLandPatternIdMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(landPatternName != null);
    Assert.expect(numOfLandPatternPads > 0);
    Assert.expect(landPatternWidth > 0);
    Assert.expect(landPatternLength > 0);
    Assert.expect(userCreated != null);

    int landPatternId = 0;
    if (landPatternNameToLandPatternIdMap.containsKey(landPatternName))
      landPatternId = landPatternNameToLandPatternIdMap.get(landPatternName);
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, landPatternName);
      RelationalDatabaseUtil.setInt(preparedStatement, 2, numOfLandPatternPads);
      RelationalDatabaseUtil.setInt(preparedStatement, 3, landPatternWidth);
      RelationalDatabaseUtil.setInt(preparedStatement, 4, landPatternLength);
      RelationalDatabaseUtil.setString(preparedStatement, 5, userCreated);
      RelationalDatabaseUtil.executeUpdate(preparedStatement);
      landPatternId = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
      landPatternNameToLandPatternIdMap.put(landPatternName, landPatternId);
    }
    Assert.expect(landPatternId > 0);

    return landPatternId;
  }

  /**
   * This method creates a new package record.
   *
   * @param preparedStatement PreparedStatement
   * @param packageShortName String
   * @param packageLongName String
   * @param landPatternID int
   * @param checkSum long
   * @param userCreated String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createPackage(PreparedStatement preparedStatement,
                           String packageShortName,
                           String packageLongName,
                           int landPatternID,
                           long checkSum,
                           String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(packageShortName != null);
    Assert.expect(packageLongName != null);
    Assert.expect(landPatternID > 0);
    Assert.expect(userCreated != null);
    Assert.expect(checkSum > 0);

    RelationalDatabaseUtil.setString(preparedStatement, 1, packageShortName);
    RelationalDatabaseUtil.setString(preparedStatement, 2, packageLongName);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, landPatternID);
    RelationalDatabaseUtil.setLong(preparedStatement, 4, checkSum);
    RelationalDatabaseUtil.setString(preparedStatement, 5, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int packageID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(packageID > 0);

    return packageID;
  }

  /**
   * This method creates a new subtype scheme record.
   *
   * @param preparedStatement PreparedStatement
   * @param packageID int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createSubtypeScheme(PreparedStatement preparedStatement,
                                 int packageID,
                                 String padJointTypeNames,
                                 String padConsolidateCorelationInfo,
                                 long checkSum,
                                 boolean usesOneJointTypeEnum,
                                 boolean usesOneSubtype,
                                 String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(packageID > 0);
    Assert.expect(padJointTypeNames != null);
    Assert.expect(padConsolidateCorelationInfo != null);
    Assert.expect(checkSum > 0);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, packageID);
    RelationalDatabaseUtil.setString(preparedStatement, 2, padJointTypeNames);
    RelationalDatabaseUtil.setString(preparedStatement, 3, padConsolidateCorelationInfo);
    RelationalDatabaseUtil.setLong(preparedStatement, 4, checkSum);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, LibraryUtil.getInstance().convertBooleanToInt(usesOneJointTypeEnum));
    RelationalDatabaseUtil.setInt(preparedStatement, 6, LibraryUtil.getInstance().convertBooleanToInt(usesOneSubtype));
    RelationalDatabaseUtil.setString(preparedStatement, 7, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int subtypeSchemeID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(subtypeSchemeID > 0);

    return subtypeSchemeID;
  }

  /**
   * This method creates a new ref joint type record.
   *
   * @param preparedStatement PreparedStatement
   * @param jointTypeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefJointType(PreparedStatement preparedStatement,
                                String jointTypeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(jointTypeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, jointTypeName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refJointTypeID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refJointTypeID > 0);

    return refJointTypeID;
  }

  /**
   * This method creates a new ref subtype record.
   *
   * @param preparedStatement PreparedStatement
   * @param subtypeShortName String
   * @param subtypeComment String
   * @param refJointTypeID int
   * @param refUserGainID int
   * @param refIntegrationLevelID int
   * @param refMagnificationID int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefSubtype(PreparedStatement preparedStatement,
                              String subtypeShortName,
                              String subtypeComment,
                              int refJointTypeID) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeShortName != null);
    Assert.expect(subtypeComment != null);
    Assert.expect(refJointTypeID > 0);    
        
    RelationalDatabaseUtil.setString(preparedStatement, 1, subtypeShortName);
    RelationalDatabaseUtil.setString(preparedStatement, 2, subtypeComment);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, refJointTypeID);    
    
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refSubtypeID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refSubtypeID > 0);

    return refSubtypeID;
  }

  /**
   * This method creates a new project record.
   *
   * @param preparedStatement PreparedStatement
   * @param projectName String
   * @param projectDirectory String
   * @param userCreated String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createProject(PreparedStatement preparedStatement,
                           String projectName,
                           String projectDirectory,
                           String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectName != null);
    Assert.expect(projectDirectory != null);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, projectName);
    RelationalDatabaseUtil.setString(preparedStatement, 2, projectDirectory);
    RelationalDatabaseUtil.setString(preparedStatement, 3, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int projectID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(projectID > 0);

    return projectID;
  }

  /**
   * This method creates a new project to subtype record.
   *
   * @param preparedStatement PreparedStatement
   * @param projectID int
   * @param subtypeID int
   * @param userCreated String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createProjectToSubtype(PreparedStatement preparedStatement,
                                     int projectID,
                                     int subtypeID,
                                     int userGainID,
                                     int integrationLevelID,
                                     int artifactCompensationID,
                                     int magnificationID,
                                     int dynamicRangeOptimizationID,
                                     String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(subtypeID > 0);    
    Assert.expect(userGainID > 0);
    Assert.expect(integrationLevelID > 0);
    Assert.expect(artifactCompensationID > 0);
    Assert.expect(magnificationID > 0);
    Assert.expect(dynamicRangeOptimizationID > 0);    
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, subtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, userGainID);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, integrationLevelID);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, artifactCompensationID);
    RelationalDatabaseUtil.setInt(preparedStatement, 6, magnificationID);
    RelationalDatabaseUtil.setInt(preparedStatement, 7, dynamicRangeOptimizationID);
    RelationalDatabaseUtil.setString(preparedStatement, 8, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }
  
  /**
   * This method creates a new subtype to component record.
   *
   * @param preparedStatement PreparedStatement
   * @param projectID int
   * @param refSubtypeID int
   * @param refComponentID int
   * @param userCreated String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createSubtypeToComponent(PreparedStatement preparedStatement,
                                       int projectID,
                                       int refSubtypeID,
                                       int refComponentID,
                                       int refFocusMethodID,
                                       String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(refSubtypeID > 0);
    Assert.expect(refComponentID > 0);
    Assert.expect(refFocusMethodID > 0);
    Assert.expect(userCreated != null);
    
    RelationalDatabaseUtil.setInt(preparedStatement, 1, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, refSubtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, refComponentID);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, refFocusMethodID);
    RelationalDatabaseUtil.setString(preparedStatement, 5, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }
  
  /**
   * This method updates SubtypeToComponent record.
   * 
   * @param preparedStatement
   * @param projectID
   * @param refSubtypeID
   * @param refComponentID
   * @param refFocusMethodID
   * @param userUpdated
   * @throws DatastoreException 
   */
  public void updateSubtypeToComponent(PreparedStatement preparedStatement,
                                       int projectID,
                                       int refSubtypeID,
                                       int refComponentID,
                                       int refFocusMethodID,
                                       String userUpdated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(refSubtypeID > 0);
    Assert.expect(refComponentID > 0);
    Assert.expect(refFocusMethodID > 0);
    Assert.expect(userUpdated != null);
    
    RelationalDatabaseUtil.setInt(preparedStatement, 1, refFocusMethodID);
    RelationalDatabaseUtil.setString(preparedStatement, 2, userUpdated);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, refSubtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, refComponentID);        
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }
  
  /**
   * This method creates a new subtype to algorithm record.
   * 
   * @param preparedStatement
   * @param projectID
   * @param refSubtypeID
   * @param refAlgorithmNameID
   * @throws DatastoreException 
   */
  public void createSubtypeToAlgorithm(PreparedStatement preparedStatement,
                                       int projectID,
                                       int refSubtypeID,
                                       int refAlgorithmNameID,
                                       boolean isAlgorithmEnable,
                                       String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(refSubtypeID > 0);
    Assert.expect(refAlgorithmNameID > 0);
    
    RelationalDatabaseUtil.setInt(preparedStatement, 1, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, refSubtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, refAlgorithmNameID);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 4, isAlgorithmEnable);
    RelationalDatabaseUtil.setString(preparedStatement, 5, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * This method creates a new ref algorithm name record.
   *
   * @param preparedStatement PreparedStatement
   * @param algorithnName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefAlgorithmName(PreparedStatement preparedStatement,
                                    String algorithnName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(algorithnName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, algorithnName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refAlgorithmNameID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refAlgorithmNameID > 0);

    return refAlgorithmNameID;
  }

  /**
   * This method creates a new ref threshold record.
   *
   * @param preparedStatement PreparedStatement
   * @param refAlgorithmNameID int
   * @param thresholdName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefThreshold(PreparedStatement preparedStatement,
                                int refAlgorithmNameID,
                                String thresholdName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(refAlgorithmNameID > 0);
    Assert.expect(thresholdName != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, refAlgorithmNameID);
    RelationalDatabaseUtil.setString(preparedStatement, 2, thresholdName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refThresholdID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refThresholdID > 0);

    return refThresholdID;
  }

  /**
   * This method creates a new ref data type record.
   *
   * @param preparedStatement PreparedStatement
   * @param dataTypeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefDataType(PreparedStatement preparedStatement,
                               String dataTypeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(dataTypeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, dataTypeName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refDataTypeID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refDataTypeID > 0);

    return refDataTypeID;
  }

  /**
   * This method creates a new ref shape record.
   *
   * @param preparedStatement PreparedStatement
   * @param shapeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createRefShape(PreparedStatement preparedStatement, String shapeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(shapeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, shapeName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refShapeID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refShapeID > 0);

    return refShapeID;
  }
  
  /**
   * This method creates a new ref user gain record.
   * 
   * @param preparedStatement
   * @param userGainName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefUserGain(PreparedStatement preparedStatement, String userGainName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(userGainName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, userGainName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refUserGainID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refUserGainID > 0);

    return refUserGainID;
  }
  
  /**
   * This method creates a new ref integration level record.
   * 
   * @param preparedStatement
   * @param integrationLevelName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefIntegrationLevel(PreparedStatement preparedStatement, String integrationLevelName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(integrationLevelName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, integrationLevelName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refIntegrationLevelID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refIntegrationLevelID > 0);

    return refIntegrationLevelID;
  }
  
  /**
   * This method creates a new ref artifact compensation record.
   * 
   * @param preparedStatement
   * @param integrationLevelName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefArtifactCompensation(PreparedStatement preparedStatement, String artifactCompensationName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(artifactCompensationName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, artifactCompensationName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refArtifactCompensationID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refArtifactCompensationID > 0);

    return refArtifactCompensationID;
  }
  
  /**
   * This method creates a new ref magnification record.
   * 
   * @param preparedStatement
   * @param magnificationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefMagnification(PreparedStatement preparedStatement, String magnificationName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(magnificationName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, magnificationName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refMagnificationID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refMagnificationID > 0);

    return refMagnificationID;
  }
  
  /**
   * This method creates a new ref dynamic range optimization record.
   * 
   * @param preparedStatement
   * @param dynamicRangeOptimizationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefDynamicRangeOptimization(PreparedStatement preparedStatement, String dynamicRangeOptimizationName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(dynamicRangeOptimizationName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, dynamicRangeOptimizationName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refDynamicRangeOptimizationID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refDynamicRangeOptimizationID > 0);

    return refDynamicRangeOptimizationID;
  }
  
  /**
   * This method creates a new ref focus method record.
   * 
   * @param preparedStatement
   * @param focusMethodName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefFocusMethod(PreparedStatement preparedStatement, String focusMethodName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(focusMethodName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, focusMethodName);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refFocusMethodID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refFocusMethodID > 0);

    return refFocusMethodID;
  }
  
  /**
   * This method creates a new ref component record.
   * 
   * @param preparedStatement
   * @param componentRefDes
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int createRefComponent(PreparedStatement preparedStatement, String componentRefDes) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(componentRefDes != null);
    
    RelationalDatabaseUtil.setString(preparedStatement, 1, componentRefDes);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int refComponentID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(refComponentID > 0);

    return refComponentID;
  }

  /**
   * This method creates a new subtype to threshold record.
   *
   * @param preparedStatement PreparedStatement
   * @param subtypeID int
   * @param thresholdID int
   * @param thresholdValue String
   * @param refDataTypeID int
   * @param algoVersion int
   * @param thresholdComment String
   * @param userCreated String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createSubtypeToThreshold(PreparedStatement preparedStatement,
                                       int projectID,
                                       int subtypeID,
                                       int thresholdID,
                                       String thresholdValue,
                                       int refDataTypeID,
                                       double algoVersion,
                                       String thresholdComment,
                                       String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(subtypeID > 0);
    Assert.expect(thresholdID > 0);
    Assert.expect(thresholdValue != null);
    Assert.expect(refDataTypeID > 0);
    Assert.expect(algoVersion > 0);
    Assert.expect(thresholdComment != null);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, subtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, thresholdID);
    RelationalDatabaseUtil.setString(preparedStatement, 4, thresholdValue);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, refDataTypeID);
    RelationalDatabaseUtil.setDouble(preparedStatement, 6, algoVersion);
    RelationalDatabaseUtil.setString(preparedStatement, 7, thresholdComment);
    RelationalDatabaseUtil.setString(preparedStatement, 8, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   *
   * @param preparedStatement PreparedStatement
   * @param projectID int
   * @param subtypeID int
   * @param thresholdID int
   * @param thresholdValue String
   * @param refDataTypeID int
   * @param thresholdComment String
   * @param userCreated String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void updateSubtypeToThreshold(PreparedStatement preparedStatement,
                                       int projectID,
                                       int subtypeID,
                                       int thresholdID,
                                       String thresholdValue,
                                       int refDataTypeID,
                                       String thresholdComment,
                                       String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectID > 0);
    Assert.expect(subtypeID > 0);
    Assert.expect(thresholdID > 0);
    Assert.expect(thresholdValue != null);
    Assert.expect(refDataTypeID > 0);
    Assert.expect(thresholdComment != null);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, thresholdValue);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, refDataTypeID);
    RelationalDatabaseUtil.setString(preparedStatement, 3, thresholdComment);
    RelationalDatabaseUtil.setString(preparedStatement, 4, UserAccountsManager.getCurrentUserLoginName());
    RelationalDatabaseUtil.setInt(preparedStatement, 5, projectID);
    RelationalDatabaseUtil.setInt(preparedStatement, 6, subtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 7, thresholdID);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param refSubtypeId int
   * @param refThresholdId int
   * @return boolean
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public boolean doesSubtypeToThresholdExist(PreparedStatement preparedStatement,
                                          int projectId,
                                          int refSubtypeId,
                                          int refThresholdId)  throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectId > 0);
    Assert.expect(refSubtypeId > 0);
    Assert.expect(refThresholdId > 0);

    boolean doesSubtypeToThresholdExist = false;
    ResultSet resultSet;
    try
    {
      RelationalDatabaseUtil.setInt(preparedStatement, 1, projectId);
      RelationalDatabaseUtil.setInt(preparedStatement, 2, refSubtypeId);
      RelationalDatabaseUtil.setInt(preparedStatement, 3, refThresholdId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      if (resultSet.next())
      {
        doesSubtypeToThresholdExist = true;
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return doesSubtypeToThresholdExist;
  }
  
  /**   
   * @param preparedStatement
   * @param projectId
   * @param refSubtypeId
   * @param refAlgorithmNameId
   * @return
   * @throws DatastoreException 
   */
  public boolean doesSubtypeToAlgorithmExist(PreparedStatement preparedStatement,
                                             int projectId,
                                             int refSubtypeId,
                                             int refAlgorithmNameId) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectId > 0);
    Assert.expect(refSubtypeId > 0);
    Assert.expect(refAlgorithmNameId > 0);

    boolean doesSubtypeToAlgorithmExist = false;
    ResultSet resultSet;
    try
    {
      RelationalDatabaseUtil.setInt(preparedStatement, 1, projectId);
      RelationalDatabaseUtil.setInt(preparedStatement, 2, refSubtypeId);
      RelationalDatabaseUtil.setInt(preparedStatement, 3, refAlgorithmNameId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      if (resultSet.next())
      {
        doesSubtypeToAlgorithmExist = true;
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return doesSubtypeToAlgorithmExist;
  }

  /**
   * This method creates a new land pattern record.
   *
   * @param preparedStatement PreparedStatement
   * @param landPatternID int
   * @param landPatternPadName String
   * @param XLocation int
   * @param YLocation int
   * @param XDimension int
   * @param YDimension int
   * @param rotation int
   * @param refShapeID int
   * @param isSurfaceMountPad boolean
   * @param isPadOne boolean
   * @param holeDiameter int
   * @param holeXLocation int
   * @param holeYLocation int
   * @param userCreated String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   * @author Siew Yeng - add parameter holeWidth, holeLength
   */
  public int createLandPatternPad(PreparedStatement preparedStatement,
                                  int landPatternID,
                                  String landPatternPadName,
                                  int XLocation,
                                  int YLocation,
                                  int XDimension,
                                  int YDimension,
                                  double rotation,
                                  int refShapeID,
                                  boolean isSurfaceMountPad,
                                  boolean isPadOne,
                                  int holeDiameter,
                                  int holeWidth,
                                  int holeLength,
                                  int holeXLocation,
                                  int holeYLocation,
                                  String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(landPatternID > 0);
    Assert.expect(landPatternPadName != null);
    Assert.expect(XDimension >= 0);
    Assert.expect(YDimension >= 0);
    Assert.expect(refShapeID > 0);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, landPatternID);
    RelationalDatabaseUtil.setString(preparedStatement, 2, landPatternPadName);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, XLocation);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, YLocation);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, XDimension);
    RelationalDatabaseUtil.setInt(preparedStatement, 6, YDimension);
    RelationalDatabaseUtil.setDouble(preparedStatement, 7, rotation);
    RelationalDatabaseUtil.setInt(preparedStatement, 8, refShapeID);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 9, isSurfaceMountPad);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 10, isPadOne);
    RelationalDatabaseUtil.setDouble(preparedStatement, 11, holeDiameter);
    RelationalDatabaseUtil.setDouble(preparedStatement, 12, holeWidth);
    RelationalDatabaseUtil.setDouble(preparedStatement, 13, holeLength);
    RelationalDatabaseUtil.setDouble(preparedStatement, 14, holeXLocation);
    RelationalDatabaseUtil.setDouble(preparedStatement, 15, holeYLocation);
    RelationalDatabaseUtil.setString(preparedStatement, 16, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int landPatternPadID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(landPatternPadID > 0);

    return landPatternPadID;
  }  

  /**
   * 
   * @param preparedStatement
   * @param landPatternPadID
   * @param landPatternPadName
   * @param XLocation
   * @param YLocation
   * @param XDimension
   * @param YDimension
   * @param rotation
   * @param refShapeID
   * @param isSurfaceMountPad
   * @param isPadOne
   * @param holeDiameter
   * @param holeWidth 
   * @param holeLength
   * @param holeXLocation
   * @param holeYLocation
   * @param userUpdated
   * @throws DatastoreException 
   * @author Cheah Lee Herng
   * @author Siew Yeng - add parameter holeWidth, holeLength
   */
  public void updateLandPatternPad(PreparedStatement preparedStatement,
                                  int landPatternPadID,
                                  String landPatternPadName,
                                  int XLocation,
                                  int YLocation,
                                  int XDimension,
                                  int YDimension,
                                  double rotation,
                                  int refShapeID,
                                  boolean isSurfaceMountPad,
                                  boolean isPadOne,
                                  int holeDiameter,
                                  int holeWidth,
                                  int holeLength,
                                  int holeXLocation,
                                  int holeYLocation,
                                  String userUpdated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(landPatternPadID > 0);
    Assert.expect(XDimension >= 0);
    Assert.expect(YDimension >= 0);
    Assert.expect(refShapeID > 0);
    Assert.expect(userUpdated != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, landPatternPadName);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, XLocation);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, YLocation);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, XDimension);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, YDimension);
    RelationalDatabaseUtil.setDouble(preparedStatement, 6, rotation);
    RelationalDatabaseUtil.setInt(preparedStatement, 7, refShapeID);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 8, isSurfaceMountPad);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 9, isPadOne);
    RelationalDatabaseUtil.setDouble(preparedStatement, 10, holeDiameter);
    RelationalDatabaseUtil.setDouble(preparedStatement, 11, holeWidth);
    RelationalDatabaseUtil.setDouble(preparedStatement, 12, holeLength);
    RelationalDatabaseUtil.setDouble(preparedStatement, 13, holeXLocation);
    RelationalDatabaseUtil.setDouble(preparedStatement, 14, holeYLocation);
    RelationalDatabaseUtil.setString(preparedStatement, 15, userUpdated);
    RelationalDatabaseUtil.setInt(preparedStatement, 16, landPatternPadID);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * This method creates a new package pin record.
   *
   * @param preparedStatement PreparedStatement
   * @param landPatternPadID int
   * @param refJointTypeID int
   * @param rotation int
   * @param jointHeight int
   * @param packageID int
   * @param userCreated String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int createPackagePin(PreparedStatement preparedStatement,
                              int landPatternPadID,
                              int refJointTypeID,
                              int rotation,
                              int jointHeight,
                              int packageID,
                              boolean inspectable,
                              boolean testable,
                              String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(landPatternPadID > 0);
    Assert.expect(refJointTypeID > 0);
    Assert.expect(packageID > 0);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, landPatternPadID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, refJointTypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, rotation);
    RelationalDatabaseUtil.setInt(preparedStatement, 4, jointHeight);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 5, inspectable);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 6, testable);
    RelationalDatabaseUtil.setInt(preparedStatement, 7, packageID);
    RelationalDatabaseUtil.setString(preparedStatement, 8, UserAccountsManager.getCurrentUserLoginName());
    RelationalDatabaseUtil.executeUpdate(preparedStatement);

    int packagePinID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(packagePinID > 0);

    return packagePinID;
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param refJointTypeId int
   * @param orientation int
   * @param inspectable boolean
   * @param testable boolean
   * @param packagePinId int
   * @param userCreated String
   * @throws DatastoreException
   *
   *  @author Cheah Lee Herng
   */
  public void updatePackagePin(PreparedStatement preparedStatement,
                               int refJointTypeId,
                               int orientation,
                               boolean inspectable,
                               boolean testable,
                               int packagePinId,
                               String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(refJointTypeId > 0);
    Assert.expect(packagePinId > 0);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1 , refJointTypeId);
    RelationalDatabaseUtil.setInt(preparedStatement, 2 , orientation);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 3 , inspectable);
    RelationalDatabaseUtil.setBoolean(preparedStatement, 4, testable);
    RelationalDatabaseUtil.setInt(preparedStatement, 5 , packagePinId);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * This method creates a new subtype scheme to package pin record.
   *
   * @param preparedStatement PreparedStatement
   * @param subtypeSchemeID int
   * @param subtypeID int
   * @param packagePinID int
   * @param userCreated String
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createSubtypeSchemeToPackagePin(PreparedStatement preparedStatement,
                                              int subtypeSchemeID,
                                              int subtypeID,
                                              int packagePinID,
                                              String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeSchemeID > 0);
    Assert.expect(subtypeID > 0);
    Assert.expect(packagePinID > 0);
    Assert.expect(userCreated != null);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, subtypeSchemeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, subtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, packagePinID);
    RelationalDatabaseUtil.setString(preparedStatement, 4, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * This method gets project to subtype information based on project name
   * and joint type name.
   *
   * @param preparedStatement PreparedStatement
   * @param subtypeName String
   * @param projectName String
   * @param jointTypeName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getProjectToSubtypeByProjectNameAndJointTypeName(PreparedStatement preparedStatement,
                                                                    String subtypeName,
                                                                    String projectName,
                                                                    String jointTypeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeName != null);
    Assert.expect(projectName != null);
    Assert.expect(jointTypeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, subtypeName);
    RelationalDatabaseUtil.setString(preparedStatement, 2, projectName);
    RelationalDatabaseUtil.setString(preparedStatement, 3, jointTypeName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets project information based on project name.
   *
   * @param preparedStatement PreparedStatement
   * @param projectName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getProjectByProjectName(PreparedStatement preparedStatement,
                                           String projectName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(projectName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, projectName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets ref algorithm name information based on algorithm name.
   *
   * @param preparedStatement PreparedStatement
   * @param algorithmName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getRefAlgorithmNameByAlgorithmName(PreparedStatement preparedStatement,
                                                      String algorithmName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(algorithmName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, algorithmName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets ref threshold information based on threshold name.
   *
   * @param preparedStatement PreparedStatement
   * @param thresholdName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getRefThresholdByThresholdName(PreparedStatement preparedStatement,
                                                  String thresholdName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(thresholdName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, thresholdName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets ref data type information based on data type name.
   *
   * @param preparedStatement PreparedStatement
   * @param dataTypeName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getRefDataTypeByDataTypeName(PreparedStatement preparedStatement,
                                                String dataTypeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(dataTypeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, dataTypeName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets ref shape information based on shape name.
   *
   * @param preparedStatement PreparedStatement
   * @param shapeName String
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getRefShapeByShapeName(PreparedStatement preparedStatement,
                                          String shapeName) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(shapeName != null);

    RelationalDatabaseUtil.setString(preparedStatement, 1, shapeName);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method gets ref threshold information based on ref subtype id.
   *
   * @param preparedStatement PreparedStatement
   * @param refSubtypeId int
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getRefThresholdByRefSubtypeId(PreparedStatement preparedStatement,
                                                 int refSubtypeId) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(refSubtypeId > 0);

    RelationalDatabaseUtil.setInt(preparedStatement, 1, refSubtypeId);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   *
   * @param preparedStatement PreparedStatement
   * @param checkSum long
   * @return ResultSet
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public ResultSet getMatchedLibrarySubtypeSchemeByChecksum(PreparedStatement preparedStatement,
                                                            long checkSum) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(checkSum > 0);

    RelationalDatabaseUtil.setLong(preparedStatement, 1, checkSum);
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    return resultSet;
  }

  /**
   * This method updates subtype to threshold.
   *
   * @param preparedStatement PreparedStatement
   * @param thresholdValue String
   * @param refDataTypeID int
   * @param thresholdComment String
   * @param userUpdated String
   * @param subtypeID int
   * @param thresholdID int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void updateSubtypeToThreshold(PreparedStatement preparedStatement,
                                       String thresholdValue,
                                       int refDataTypeID,
                                       String thresholdComment,
                                       String userUpdated,
                                       int subtypeID,
                                       int thresholdID) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(thresholdValue != null);
    Assert.expect(refDataTypeID > 0);
    Assert.expect(thresholdComment != null);
    Assert.expect(userUpdated != null);
    Assert.expect(subtypeID > 0);
    Assert.expect(thresholdID > 0);

    RelationalDatabaseUtil.setString(preparedStatement, 1, thresholdValue);
    RelationalDatabaseUtil.setInt(preparedStatement, 2, refDataTypeID);
    RelationalDatabaseUtil.setString(preparedStatement, 3, thresholdComment);
    RelationalDatabaseUtil.setString(preparedStatement, 4, userUpdated);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, subtypeID);
    RelationalDatabaseUtil.setInt(preparedStatement, 6, thresholdID);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
  }

  /**
   * Check if package exists in the map.
   *
   * @param packageLongNameToPackageIdMap Map
   * @param packageLongNameToLandPatternIdMap Map
   * @param preparedStatement PreparedStatement
   * @param packageShortName String
   * @param packageLongName String
   * @param landPatternId int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getPackageID(Map<String, Integer> packageLongNameToPackageIdMap,
                          Map<String, Integer> packageLongNameToLandPatternIdMap,
                          PreparedStatement preparedStatement,
                          String packageShortName,
                          String packageLongName,
                          int landPatternId) throws DatastoreException
  {
    Assert.expect(packageLongNameToPackageIdMap != null);
    Assert.expect(packageLongNameToLandPatternIdMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(packageShortName != null);
    Assert.expect(packageLongName != null);
    Assert.expect(landPatternId > 0);

    int packageID = 0;
    if (packageLongNameToPackageIdMap.containsKey(packageLongName) &&
        packageLongNameToLandPatternIdMap.containsKey(packageLongName))
    {
      int landPatternIDFromMap = packageLongNameToLandPatternIdMap.get(packageLongName);
      if (landPatternIDFromMap == landPatternId)
      {
        packageID = packageLongNameToPackageIdMap.get(packageLongName);
      }
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, packageLongName);
      RelationalDatabaseUtil.setInt(preparedStatement, 2, landPatternId);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      packageID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.PACKAGE_ID_COLUMN);

      if (packageID > 0)
      {
        packageLongNameToPackageIdMap.put(packageLongName, packageID);
        packageLongNameToLandPatternIdMap.put(packageLongName, landPatternId);
      }
    }

    return packageID;
  }

  /**
   * @param landPatternNameToLandPatternIdMap Map
   * @param preparedStatement PreparedStatement
   * @param landPatternName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternId(Map<String, Integer> landPatternNameToLandPatternIdMap,
                              PreparedStatement preparedStatement,
                              String landPatternName,
                              int numberOfPads) throws DatastoreException
  {
    Assert.expect(landPatternNameToLandPatternIdMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(landPatternName != null);
    Assert.expect(numberOfPads > 0);

    int landPatternId = 0;
    if (landPatternNameToLandPatternIdMap.containsKey(landPatternName))
    {
      landPatternId = landPatternNameToLandPatternIdMap.get(landPatternName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, landPatternName);
      RelationalDatabaseUtil.setInt(preparedStatement, 2, numberOfPads);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      landPatternId = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.LAND_PATTERN_ID_COLUMN);

      if (landPatternId > 0)
      {
        landPatternNameToLandPatternIdMap.put(landPatternName, landPatternId);
      }
    }

    return landPatternId;
  }

  /**
   * Check if project exists.
   *
   * @param projectNameToProjectIDMap Map
   * @param preparedStatement PreparedStatement
   * @param projectName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getProjectID(Map<String, Integer> projectNameToProjectIDMap,
                          PreparedStatement preparedStatement,
                          String projectName) throws DatastoreException
  {
    Assert.expect(projectNameToProjectIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(projectName != null);

    int projectID = 0;
    if (projectNameToProjectIDMap.containsKey(projectName))
    {
      projectID = projectNameToProjectIDMap.get(projectName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, projectName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      projectID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.PROJECT_ID_COLUMN);
    }

    return projectID;
  }

  /**
   *
   * @param refAlgorithmNameToGeneratedRefAlgorithmNameIDMap Map
   * @param preparedStatement PreparedStatement
   * @param algorithmName String
   * @return boolean
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefAlgorithmNameID(Map<String, Integer> refAlgorithmNameToGeneratedRefAlgorithmNameIDMap,
                                          PreparedStatement preparedStatement,
                                          String algorithmName) throws DatastoreException
  {
    Assert.expect(refAlgorithmNameToGeneratedRefAlgorithmNameIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(algorithmName != null);

    int refAlgorithmNameID = 0;
    if (refAlgorithmNameToGeneratedRefAlgorithmNameIDMap.containsKey(algorithmName))
    {
      refAlgorithmNameID = refAlgorithmNameToGeneratedRefAlgorithmNameIDMap.get(algorithmName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, algorithmName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refAlgorithmNameID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_ALGORITHM_NAME_ID_COLUMN);

      if (refAlgorithmNameID > 0)
      {
        refAlgorithmNameToGeneratedRefAlgorithmNameIDMap.put(algorithmName, refAlgorithmNameID);
      }
    }

    return refAlgorithmNameID;
  }

  /**
   * @param thresholdNameToThresholdIDMap Map
   * @param preparedStatement PreparedStatement
   * @param thresholdName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefThresholdID(Map<String, Integer> thresholdNameToThresholdIDMap,
                               PreparedStatement preparedStatement,
                               String thresholdName) throws DatastoreException
  {
    Assert.expect(thresholdNameToThresholdIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(thresholdName != null);

    int refThresholdID = 0;
    if (thresholdNameToThresholdIDMap.containsKey(thresholdName))
    {
      refThresholdID = thresholdNameToThresholdIDMap.get(thresholdName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, thresholdName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refThresholdID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_THRESHOLD_ID_COLUMN);

      if (refThresholdID > 0)
      {
        thresholdNameToThresholdIDMap.put(thresholdName, refThresholdID);
      }
    }

    return refThresholdID;
  }

  /**
   * @param dataTypeNameToRefDataTypeIDMap Map
   * @param preparedStatement PreparedStatement
   * @param dataTypeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefDataTypeID(Map<String, Integer> dataTypeNameToRefDataTypeIDMap,
                                     PreparedStatement preparedStatement,
                                     String dataTypeName) throws DatastoreException
  {
    Assert.expect(dataTypeNameToRefDataTypeIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(dataTypeName != null);

    int refDataTypeID = 0;
    if (dataTypeNameToRefDataTypeIDMap.containsKey(dataTypeName))
    {
      refDataTypeID = dataTypeNameToRefDataTypeIDMap.get(dataTypeName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, dataTypeName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refDataTypeID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_DATA_TYPE_ID_COLUMN);

      if (refDataTypeID > 0)
      {
        dataTypeNameToRefDataTypeIDMap.put(dataTypeName, refDataTypeID);
      }
    }

    return refDataTypeID;
  }

  /**
   * @param refShapeIDToRefShapeIDMap Map
   * @param preparedStatement PreparedStatement
   * @param shapeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefShapeID(Map<String, Integer> refShapeIDToRefShapeIDMap,
                                  PreparedStatement preparedStatement,
                                  String shapeName) throws DatastoreException
  {
    Assert.expect(refShapeIDToRefShapeIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(shapeName != null);

    int refShapeID = 0;
    if (refShapeIDToRefShapeIDMap.containsKey(shapeName))
    {
      refShapeID = refShapeIDToRefShapeIDMap.get(shapeName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, shapeName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refShapeID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_SHAPE_ID_COLUMN);

      if (refShapeID > 0)
      {
        refShapeIDToRefShapeIDMap.put(shapeName, refShapeID);
      }
    }

    return refShapeID;
  }
  
  /**
   * @param refUserGainNameToRefUserGainIDMap
   * @param preparedStatement
   * @param userGainName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefUserGainID(Map<String, Integer> refUserGainNameToRefUserGainIDMap,
                              PreparedStatement preparedStatement,
                              String userGainName) throws DatastoreException
  {
    Assert.expect(refUserGainNameToRefUserGainIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(userGainName != null);
    
    int refUserGainID = 0;
    if (refUserGainNameToRefUserGainIDMap.containsKey(userGainName))
    {
      refUserGainID = refUserGainNameToRefUserGainIDMap.get(userGainName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, userGainName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refUserGainID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_USER_GAIN_ID_COLUMN);

      if (refUserGainID > 0)
      {
        refUserGainNameToRefUserGainIDMap.put(userGainName, refUserGainID);
      }
    }
    
    return refUserGainID;
  }
  
  /**
   * @param refIntegrationLevelNameToRefIntegrationLevelIDMap
   * @param preparedStatement
   * @param integrationLevelName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefIntegrationLevelID(Map<String, Integer> refIntegrationLevelNameToRefIntegrationLevelIDMap,
                                      PreparedStatement preparedStatement,
                                      String integrationLevelName) throws DatastoreException
  {
    Assert.expect(refIntegrationLevelNameToRefIntegrationLevelIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(integrationLevelName != null);
    
    int refIntegrationLevelID = 0;
    if (refIntegrationLevelNameToRefIntegrationLevelIDMap.containsKey(integrationLevelName))
    {
      refIntegrationLevelID = refIntegrationLevelNameToRefIntegrationLevelIDMap.get(integrationLevelName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, integrationLevelName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refIntegrationLevelID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_INTEGRATION_LEVEL_ID_COLUMN);

      if (refIntegrationLevelID > 0)
      {
        refIntegrationLevelNameToRefIntegrationLevelIDMap.put(integrationLevelName, refIntegrationLevelID);
      }
    }
    
    return refIntegrationLevelID;
  }
  
  /**
   * @param refArtifactCompensationNameToRefArtifactCompensationIDMap
   * @param preparedStatement
   * @param artifactCompensationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefArtifactCompensationID(Map<String, Integer> refArtifactCompensationNameToRefArtifactCompensationIDMap,
                                          PreparedStatement preparedStatement,
                                          String artifactCompensationName) throws DatastoreException
  {
    Assert.expect(refArtifactCompensationNameToRefArtifactCompensationIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(artifactCompensationName != null);
    
    int refArtifactCompensationID = 0;
    if (refArtifactCompensationNameToRefArtifactCompensationIDMap.containsKey(artifactCompensationName))
    {
      refArtifactCompensationID = refArtifactCompensationNameToRefArtifactCompensationIDMap.get(artifactCompensationName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, artifactCompensationName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refArtifactCompensationID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_ARTIFACT_COMPENSATION_ID_COLUMN);

      if (refArtifactCompensationID > 0)
      {
        refArtifactCompensationNameToRefArtifactCompensationIDMap.put(artifactCompensationName, refArtifactCompensationID);
      }
    }
    
    return refArtifactCompensationID;
  }
  
  /**
   * @param refMagnificationNameToRefMagnificationIDMap
   * @param preparedStatement
   * @param magnificationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefMagnificationID(Map<String, Integer> refMagnificationNameToRefMagnificationIDMap,
                                   PreparedStatement preparedStatement,
                                   String magnificationName) throws DatastoreException
  {
    Assert.expect(refMagnificationNameToRefMagnificationIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(magnificationName != null);
    
    int refMagnificationID = 0;
    if (refMagnificationNameToRefMagnificationIDMap.containsKey(magnificationName))
    {
      refMagnificationID = refMagnificationNameToRefMagnificationIDMap.get(magnificationName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, magnificationName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refMagnificationID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_MAGNIFICATION_ID_COLUMN);

      if (refMagnificationID > 0)
      {
        refMagnificationNameToRefMagnificationIDMap.put(magnificationName, refMagnificationID);
      }
    }
    
    return refMagnificationID;
  }
  
  /**
   * @param refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap
   * @param preparedStatement
   * @param dynamicRangeOptimizationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefDynamicRangeOptimizationID(Map<String, Integer> refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap,
                                              PreparedStatement preparedStatement,
                                              String dynamicRangeOptimizationName) throws DatastoreException
  {
    Assert.expect(refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(dynamicRangeOptimizationName != null);
    
    int refDynamicRangeOptimizationID = 0;
    if (refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap.containsKey(dynamicRangeOptimizationName))
    {
      refDynamicRangeOptimizationID = refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap.get(dynamicRangeOptimizationName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, dynamicRangeOptimizationName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refDynamicRangeOptimizationID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_DYNAMIC_RANGE_OPTIMIZATION_ID_COLUMN);

      if (refDynamicRangeOptimizationID > 0)
      {
        refDynamicRangeOptimizationNameToRefDynamicRangeOptimizationIDMap.put(dynamicRangeOptimizationName, refDynamicRangeOptimizationID);
      }
    }
    
    return refDynamicRangeOptimizationID;
  }
  
  /**
   * @param refFocusMethodNameToRefFocusMethodIDMap
   * @param preparedStatement
   * @param focusMethodName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefFocusMethodID(Map<String, Integer> refFocusMethodNameToRefFocusMethodIDMap,
                                 PreparedStatement preparedStatement,
                                 String focusMethodName) throws DatastoreException
  {
    Assert.expect(refFocusMethodNameToRefFocusMethodIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(focusMethodName != null);
    
    int refFocusMethodID = 0;
    if (refFocusMethodNameToRefFocusMethodIDMap.containsKey(focusMethodName))
    {
      refFocusMethodID = refFocusMethodNameToRefFocusMethodIDMap.get(focusMethodName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, focusMethodName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refFocusMethodID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_FOCUS_METHOD_ID_COLUMN);

      if (refFocusMethodID > 0)
      {
        refFocusMethodNameToRefFocusMethodIDMap.put(focusMethodName, refFocusMethodID);
      }
    }
    
    return refFocusMethodID;
  }

  /**
   *
   * @param jointTypeNameToRefJointTypeIDMap Map
   * @param preparedStatement PreparedStatement
   * @param jointTypeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefJointTypeID(Map<String, Integer> jointTypeNameToRefJointTypeIDMap,
                                      PreparedStatement preparedStatement,
                                      String jointTypeName) throws DatastoreException
  {
    Assert.expect(jointTypeNameToRefJointTypeIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(jointTypeName != null);

    int refJointTypeID = 0;
    if (jointTypeNameToRefJointTypeIDMap.containsKey(jointTypeName))
    {
      refJointTypeID = jointTypeNameToRefJointTypeIDMap.get(jointTypeName);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, jointTypeName);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refJointTypeID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_JOINT_TYPE_ID_COLUMN);

      if (refJointTypeID > 0)
      {
        jointTypeNameToRefJointTypeIDMap.put(jointTypeName, refJointTypeID);
      }
    }

    return refJointTypeID;
  }

  /**
   * @param subtypeLongNameToRefSubtypeIdMap Map
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefSubtypeId(Map<String,Integer> subtypeLongNameToRefSubtypeIdMap,
                             PreparedStatement preparedStatement,
                             String subtypeShortName,
                             String subtypeLongName,
                             String jointTypeName) throws DatastoreException
  {
    Assert.expect(subtypeLongNameToRefSubtypeIdMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeShortName != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(jointTypeName != null);    

    int refSubtypeId = 0;
    if (subtypeLongNameToRefSubtypeIdMap.containsKey(subtypeLongName))
    {
      refSubtypeId = subtypeLongNameToRefSubtypeIdMap.get(subtypeLongName);
    }
    else
    {      
      RelationalDatabaseUtil.setString(preparedStatement, 1, subtypeShortName);
      RelationalDatabaseUtil.setString(preparedStatement, 2, jointTypeName);      
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refSubtypeId = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_SUBTYPE_ID_COLUMN);

      if (refSubtypeId > 0)
      {
        subtypeLongNameToRefSubtypeIdMap.put(subtypeLongName, refSubtypeId);
      }
    }

    return refSubtypeId;
  }
  
  /**   
   * @param componentRefDesToRefComponentIDMap Map
   * @param preparedStatement PreparedStatement
   * @param componentRefDes String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefComponentId(Map<String, Integer> componentRefDesToRefComponentIDMap,
                               PreparedStatement preparedStatement,
                               String componentRefDes) throws DatastoreException
  {
    Assert.expect(componentRefDesToRefComponentIDMap != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(componentRefDes != null);
    
    int refComponentID = 0;
    if (componentRefDesToRefComponentIDMap.containsKey(componentRefDes))
    {
      refComponentID = componentRefDesToRefComponentIDMap.get(componentRefDes);
    }
    else
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, componentRefDes);
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      refComponentID = RelationalDatabaseUtil.getIntResultSetData(resultSet, _sqlManager.REF_COMPONENT_ID_COLUMN);

      if (refComponentID > 0)
      {
        componentRefDesToRefComponentIDMap.put(componentRefDes, refComponentID);
      }
    }

    return refComponentID;
  }

  /**
   * This method gets a matched library subtype with populated threshold value.
   *
   * @param subtype Subtype
   * @return LibrarySubtype
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public LibrarySubtype getLibrarySubtype(Subtype subtype, String libraryPath) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(libraryPath != null);

    LibrarySubtype librarySubtype = null;
    PreparedStatement preparedStatement;
    PreparedStatement libraryComponentTypePreparedStatement;
    PreparedStatement subtypeToAlgorithmPreparedStatement;    
    ResultSet resultSet;

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibrarySubtypeThresholdValueSqlString());
      libraryComponentTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryComponentTypeByLibrarySubtypeIdSqlString());
      RelationalDatabaseUtil.setString(preparedStatement, 1, subtype.getShortName());
      RelationalDatabaseUtil.setString(preparedStatement, 2, subtype.getJointTypeEnum().getName());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while (resultSet.next())
      {
        if(librarySubtype == null)
        {
          librarySubtype = new LibrarySubtype();
          librarySubtype.setSubtypeId(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
          librarySubtype.setSubtypeName(resultSet.getString(_sqlManager.SUBTYPE_NAME_COLUMN));
          librarySubtype.setProjectSubtype(subtype);
          librarySubtype.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
          librarySubtype.setSubtypeComment(resultSet.getString(_sqlManager.SUBTYPE_COMMENT_COLUMN));
          
          // Setting UserGainEnum
          if (resultSet.getString(_sqlManager.USER_GAIN_NAME_COLUMN).equals(LEGACY))
          {
            librarySubtype.setLegacyUserGainEnum(true);
            librarySubtype.setUserGainEnum(UserGainEnum.ONE);
          }
          else
            librarySubtype.setUserGainEnum(UserGainEnum.getUserGainToEnumMapValue(Integer.parseInt(resultSet.getString(_sqlManager.USER_GAIN_NAME_COLUMN))));

          // Setting SignalCompensationEnum
          if (resultSet.getString(_sqlManager.INTEGRATION_LEVEL_NAME_COLUMN).equals(LEGACY))
          {
            librarySubtype.setLegacySignalCompensationEnum(true);
            librarySubtype.setSignalCompensationEnum(SignalCompensationEnum.DEFAULT_LOW);
          }
          else
            librarySubtype.setSignalCompensationEnum(SignalCompensationEnum.getSignalCompensationEnum(resultSet.getString(_sqlManager.INTEGRATION_LEVEL_NAME_COLUMN)));

          // Setting ArtifactCompensationEnum
          if (resultSet.getString(_sqlManager.ARTIFACT_COMPENSATION_NAME_COLUMN).equals(LEGACY))
          {
            librarySubtype.setLegacyArtifactCompensationEnum(true);
            librarySubtype.setArtifactCompensationStateEnum(ArtifactCompensationStateEnum.NOT_APPLICABLE);
          }
          else
            librarySubtype.setArtifactCompensationStateEnum(ArtifactCompensationStateEnum.getArtifactCompensationStateEnum(resultSet.getString(_sqlManager.ARTIFACT_COMPENSATION_NAME_COLUMN)));

          // Setting MagnificationTypeEnum
          if (resultSet.getString(_sqlManager.MAGNIFICATION_NAME_COLUMN).equals(LEGACY))
          {
            librarySubtype.setLegacyMagnificationTypeEnum(true);
            librarySubtype.setMagnificationTypeEnum(MagnificationTypeEnum.LOW);
          }
          else
            librarySubtype.setMagnificationTypeEnum(MagnificationTypeEnum.getMagnificationToEnumMapValue(resultSet.getString(_sqlManager.MAGNIFICATION_NAME_COLUMN)));

          // Setting DynamicRangeOptimizationEnum
          if (resultSet.getString(_sqlManager.DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN).equals(LEGACY))
          {
            librarySubtype.setLegacyDynamicRangeOptimizationLevelEnum(true);
            librarySubtype.setDynamicRangeOptimizationLevelEnum(DynamicRangeOptimizationLevelEnum.ONE);
          }
          else
            librarySubtype.setDynamicRangeOptimizationLevelEnum(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(
                    Integer.parseInt(resultSet.getString(_sqlManager.DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN))));
        }

        AlgorithmSettingEnum algorithmSettingEnum = EnumStringLookup.getInstance().getAlgorithmSettingEnum(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN));
        Serializable value = getSerializedValue(resultSet.getString(_sqlManager.DATA_TYPE_NAME_COLUMN), resultSet.getString(_sqlManager.THRESHOLD_VALUE_COLUMN));

        librarySubtype.addThresholdNameToTunedValue(algorithmSettingEnum, value);
        librarySubtype.addThresholdNameToAlgoVersion(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN),
                                                        resultSet.getInt(_sqlManager.ALGO_VERSION_COLUMN));
        librarySubtype.addThresholdNameToThresholdComment(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN),
                                                             resultSet.getString(_sqlManager.THRESHOLD_COMMENT_COLUMN));
      }
      resultSet.close();
      resultSet = null;
      
      if (librarySubtype != null)
      {
        getLibraryComponentByLibrarySubtype(libraryComponentTypePreparedStatement, librarySubtype);
      }
      else
      {
        librarySubtype = new LibrarySubtype();
        librarySubtype.setSubtypeName(subtype.getShortName());
        librarySubtype.setProjectSubtype(subtype);
        librarySubtype.setJointTypeName(subtype.getJointTypeEnum().getName());
        librarySubtype.setSubtypeComment("");
      }
      
      // Clear list before populate with new data
      librarySubtype.clearAlgorithmEnumToEnabledFlagMap();
      
      // Retrieve SubtypeToAlgorithm information
      subtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibrarySubtypeAlgorithmInfoBySubtypeNameAndJointTypeNameSqlString());
      RelationalDatabaseUtil.setString(subtypeToAlgorithmPreparedStatement, 1, subtype.getShortName());
      RelationalDatabaseUtil.setString(subtypeToAlgorithmPreparedStatement, 2, subtype.getJointTypeEnum().getName());
      resultSet = RelationalDatabaseUtil.executeQuery(subtypeToAlgorithmPreparedStatement);
      
      while (resultSet.next())
      {
        AlgorithmEnum algorithmEnum = EnumStringLookup.getInstance().getAlgorithmEnum(resultSet.getString(_sqlManager.ALGORITHM_NAME_COLUMN).toLowerCase());
        boolean isAlgorithmEnabled = LibraryUtil.getInstance().convertIntToBoolean(resultSet.getInt(_sqlManager.ALGORITHM_ENABLE_COLUMN));
        
        librarySubtype.addAlgorithmEnumToEnabledFlag(algorithmEnum, isAlgorithmEnabled);
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    catch(BadFormatException bEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, bEx);
      dex.initCause(bEx);
      throw dex;
    }

    return librarySubtype;
  }

  /**
   * This method checks if a database already exists.
   *
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean doesDatabaseExists(String databasePath) throws DatastoreException
  {
    Assert.expect(databasePath != null);

    if (FileUtil.existsDirectory(databasePath))
    {
      try
      {
        // Startup database engine
        _database.startupDatabase();

        // Open connection
        String jdbcUrl = _database.getJdbcUrl(databasePath, false);
        Connection connection = RelationalDatabaseUtil.openConnection(jdbcUrl,
                                                                      _database.getDatabaseUserName(),
                                                                      _database.getDatabasePassword(),
                                                                      PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

        if (connection != null)
        {
          RelationalDatabaseUtil.closeConnection(connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
          return true;
        }
        else
          return false;
      }
      catch(DatastoreException dex)
      {
        // It might be path is not the corect database path and not able to start the database correctly
        return false;
      }
    }

    return false;
  }

  /**
   *
   * @param algorithmSettingsEnum AlgorithmSettingEnum
   * @param algorithms List
   * @return Algorithm
   *
   * @author Cheah Lee Herng
   */
  public Algorithm getAlgorithmForCurrentSubtype(List<Algorithm> algorithms,
                                                 AlgorithmSettingEnum algorithmSettingsEnum)
  {
    Assert.expect(algorithms != null);
    Assert.expect(algorithmSettingsEnum != null);

    Algorithm algorithmForCurrentSubtype = null;
    for (Algorithm algorithm : algorithms)
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingsEnum))
      {
        algorithmForCurrentSubtype = algorithm;
        break;
      }
    }

    Assert.expect(algorithmForCurrentSubtype != null);
    return algorithmForCurrentSubtype;
  }

  /**
   *
   * @param value Serializable
   * @return String
   *
   * @author Cheah Lee Herng
   */
  private String getTypeString(Serializable value)
  {
    Assert.expect(value != null);

    String typeStr = null;
    if (value instanceof Boolean)
      typeStr = "boolean";
    else if (value instanceof Integer)
      typeStr = "int";
    else if (value instanceof String)
      typeStr = "string";
    else if (value instanceof Float)
      typeStr = "float";
    else
      Assert.expect(false);

    return typeStr;
  }

  /**
   *
   * @param value Serializable
   * @return String
   *
   * @author Cheah Lee Herng
   */
  private String getThresholdValue(Serializable value)
  {
    Assert.expect(value != null);

    String thresholdStr = null;
    if (value instanceof Boolean)
      thresholdStr = ((Boolean)value).toString();
    else if (value instanceof Integer)
      thresholdStr = ((Integer)value).toString();
    else if (value instanceof String)
      thresholdStr = (String)value;
    else if (value instanceof Float)
      thresholdStr = ((Float)value).toString();
    else
      Assert.expect(false);

    return thresholdStr;
  }

  /**
   *
   * @param tunedValueType String
   * @param tunedValueStr String
   * @return Serializable
   * @throws BadFormatException
   *
   * @author Cheah Lee Herng
   */
  private Serializable getSerializedValue(String tunedValueType, String tunedValueStr) throws BadFormatException
  {
    Assert.expect(tunedValueType != null);
    Assert.expect(tunedValueStr != null);

    Serializable value = null;
    if (tunedValueType.equals("int"))
      value = StringUtil.convertStringToInt(tunedValueStr);
    else if (tunedValueType.equals("string"))
      value = (String)tunedValueStr;
    else if (tunedValueType.equals("float"))
      value = StringUtil.convertStringToFloat(tunedValueStr);
    else
      Assert.expect(false);

    return value;
  }

  /**
   *
   * @param subtypeSchemes List
   * @param libraryPathList List
   * @param searchFilterCriteria SearchFilter
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(List<SubtypeScheme> subtypeSchemes,
                                                                 List<String> libraryPathList,
                                                                 SearchFilterPersistance searchFilterCriteria,
                                                                 ImportLibrarySettingPersistance importLibrarySettingPersistance) throws DatastoreException
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(searchFilterCriteria != null);
    Assert.expect(importLibrarySettingPersistance != null);

    String currentLibraryPath;

    for(String libraryPath : libraryPathList)
    {
      //break the loop if user clicked on Cancel button while searching
      if (LibraryUtil.getInstance().cancelDatabaseOperation())
        break;

      currentLibraryPath = libraryPath;
      getRefineMatchLibraryPackageBySubtypeSchemes(subtypeSchemes, currentLibraryPath, searchFilterCriteria, importLibrarySettingPersistance);
    }
  }

  /**
   * This will do the refine matches filtering according to what user have choose to filter.
   * The filter is base on the search filter criteria class
   *
   * @author Lee Herng
   * @author Poh Kheng
   * @edited By Kee Chin Seong
   */
  public List<CompPackage> getRefineMatchLibraryPackageBySubtypeSchemes(List<SubtypeScheme> subtypeSchemes,
                                                                        String libraryPath,
                                                                        SearchFilterPersistance searchFilterCriteria,
                                                                        ImportLibrarySettingPersistance importLibrarySettingPersistance) throws DatastoreException
  {

    Assert.expect(subtypeSchemes != null);
    Assert.expect(libraryPath != null);
    Assert.expect(searchFilterCriteria != null);
    Assert.expect(importLibrarySettingPersistance != null);

    List<CompPackage> libraryCompPackages = new ArrayList<CompPackage>();
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    try
    {
      PreparedStatement preparedStatementLibrarySubtypeBySubtypeSchemeId = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibrarySubtypeBySubtypeSchemeIdSqlString());
      PreparedStatement preparedStatementLibraryComponentTypeByLibrarySubtypeId = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryComponentTypeByLibrarySubtypeIdSqlString());
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getMatchSubtypeSchemesSqlString());
      RelationalDatabaseUtil.setString(preparedStatement, 1, searchFilterCriteria.getLandPatternNameFilter().toUpperCase());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      int librarySubtypeSchemeID;
      int numberOfPads;
      String libraryPadJointTypes;
      String libraryPadsConsolidateInfo;
      boolean overAllMatchFlag = false;
      boolean geometryMatchFlag = false;
      boolean jointTypeMatchFlag = false;
      boolean landPatternNameMatchFlag = false;
      boolean useOnlyOneJointType;
      boolean useOnlyOneSubtype;
      String jointTypeName;
      int libraryLandPatternID; //Siew Yeng - XCR-3318
      int libraryLandPatternWidth;
      int libraryLandPatternLength;
      double overallMatchPercentage;
      java.util.Date dateCreated;
      java.util.Date dateUpdated;
      
      // Clear SubtypeScheme map before start perform matching
      //Siew Yeng - XCR-3318 - remove this as this will always clear match result when looping through multiple libraries
//      for(SubtypeScheme subtypeScheme : subtypeSchemes)
//      {
//        subtypeScheme.clear();
//      }

      // loop thru all match subtype schemes from the library
      while (resultSet.next())
      {
        //break the loop if user clicked on Cancel button while searching
        if (LibraryUtil.getInstance().cancelDatabaseOperation())
          break;

        libraryPadJointTypes = resultSet.getString(_sqlManager.PAD_JOINT_TYPES_COLUMN);
        libraryPadsConsolidateInfo = resultSet.getString(_sqlManager.PAD_CONSOLIDATE_INFO_COLUMN);
        librarySubtypeSchemeID = resultSet.getInt(_sqlManager.SUBTYPE_SCHEME_ID_COLUMN);
        numberOfPads = resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN);
        libraryLandPatternWidth = resultSet.getInt(_sqlManager.LAND_PATTERN_WIDTH_COLUMN);
        libraryLandPatternLength = resultSet.getInt(_sqlManager.LAND_PATTERN_LENGTH_COLUMN);
        libraryLandPatternID = resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN);
        
        //Getting Land Pattern Name column
        String landpatternName = resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN);

        useOnlyOneJointType = LibraryUtil.getInstance().convertIntToBoolean(resultSet.getInt(_sqlManager.USE_ONE_JOINT_TYPE_COLUMN));
        useOnlyOneSubtype = LibraryUtil.getInstance().convertIntToBoolean(resultSet.getInt(_sqlManager.USE_ONE_SUBTYPE_COLUMN));

        dateCreated = resultSet.getDate(_sqlManager.SUBTYPE_SCHEME_DATE_CREATED_COLUMN);
        dateUpdated = resultSet.getDate(_sqlManager.SUBTYPE_SCHEME_DATE_UPDATED_COLUMN);

        // compare each library subtype scheme to each subtype scheme from the project
        for (SubtypeScheme subtypeScheme : subtypeSchemes)
        {
          overallMatchPercentage=0.0;
          overAllMatchFlag = false;
          geometryMatchFlag = true;
          jointTypeMatchFlag = false;
          landPatternNameMatchFlag = false;
          //break the loop if user clicked on Cancel button while searching
          if (LibraryUtil.getInstance().cancelDatabaseOperation())
            break;

          CompPackage compPackage = subtypeScheme.getCompPackage();

          // matching is only for the same number of pads
          if (importLibrarySettingPersistance.getImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LANDPATTERN) == false)
          {
            if (compPackage.getLandPattern().getLandPatternPads().size() != numberOfPads)
              continue;
          }

          // this is to do the joint type matching criteria
          // this is for NON mixed case
          if (searchFilterCriteria.isJointTypeFilterOn() == true)
          {
            if (subtypeScheme.usesOneJointTypeEnum() == true && 
                subtypeScheme.usesOneSubtype() == true && 
                useOnlyOneSubtype == true)
            {
              // need to match the same joint type so we can assign for different number of pads.
              if (subtypeScheme.getPadJointTypeNames().substring(0, subtypeScheme.getPadJointTypeNames().indexOf(","))
                  .equals(libraryPadJointTypes.substring(0, libraryPadJointTypes.indexOf(","))))
                jointTypeMatchFlag = true;
            }
            // this is for mixed case
            else if (searchFilterCriteria.isJointTypeFilterOn() == true && libraryPadJointTypes.equals(compPackage.getJointTypeNames()))
            {
              jointTypeMatchFlag = true;
            }
          }
          else
          {
            jointTypeMatchFlag = true;
          }

          //Siew Yeng - XCR-3318
          PreparedStatement landPatternPadPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternPadByLandPatternIdSqlString());
          RelationalDatabaseUtil.setInt(landPatternPadPreparedStatement, 1, libraryLandPatternID);
          ResultSet landPatternPadResultSet = RelationalDatabaseUtil.executeQuery(landPatternPadPreparedStatement);
          String libraryPadsHoleConsolidateInfo = "";
          while (landPatternPadResultSet.next())
          {
            if(libraryPadsHoleConsolidateInfo.isEmpty() == false)
              libraryPadsHoleConsolidateInfo += ";";
            
            libraryPadsHoleConsolidateInfo+= landPatternPadResultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN) + ",";
            libraryPadsHoleConsolidateInfo+= landPatternPadResultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN) + ",";
            libraryPadsHoleConsolidateInfo+= landPatternPadResultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN) + ",";
            libraryPadsHoleConsolidateInfo+= landPatternPadResultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN);
          }
      
          // Calculate based on coordinate and area
          overallMatchPercentage = LibraryUtil.getInstance().calculateMatchPercentageByImagePixel(subtypeScheme.getCompPackage().getLandPattern(),
                                                                                                         libraryPadsConsolidateInfo,
                                                                                                         libraryLandPatternWidth,
                                                                                                         libraryLandPatternLength,
                                                                                                         numberOfPads,
                                                                                                         libraryPadsHoleConsolidateInfo);

          // this is to do land pattern name matching only apply when there is land pattern name matching only
          if (searchFilterCriteria.isLandPatternNameFilterOn() == true)
          {
            //using criteria check
            if (searchFilterCriteria.getLandPatternName().isEmpty() == false)
            {
              if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.START_WITH) == true)
              {
                if((subtypeScheme.getLandPatternName().startsWith(landpatternName)) == true)
                    landPatternNameMatchFlag = true;
              }
              else if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.END_WITH) == true)
              {
                if((subtypeScheme.getLandPatternName().endsWith(landpatternName)) == true)
                    landPatternNameMatchFlag = true;
              }
              else if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.CONTAINS) == true)
              {
                if((subtypeScheme.getLandPatternName().matches(landpatternName)) == true)
                    landPatternNameMatchFlag = true;
              }
              // this only apply for NON mixed type as different number of pads might happen
              else if (subtypeScheme.usesOneJointTypeEnum() == true && 
                  subtypeScheme.usesOneSubtype() == true && 
                  useOnlyOneSubtype == true)
              {
                // need to match the same joint type so we can assign for different number of pads.
                if (subtypeScheme.getPadJointTypeNames().substring(0, subtypeScheme.getPadJointTypeNames().indexOf(","))
                    .equals(libraryPadJointTypes.substring(0, libraryPadJointTypes.indexOf(","))) &&
                    resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN).contains(searchFilterCriteria.getLandPatternName()))
                {
                  landPatternNameMatchFlag = true;
                }
              }
            }
            else
            {
              if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.START_WITH) == true)
              {
                if((subtypeScheme.getLandPatternName().startsWith(landpatternName)) == true)
                   landPatternNameMatchFlag = true;
              }
              else if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.END_WITH) == true)
              {
                if((subtypeScheme.getLandPatternName().endsWith(landpatternName)) == true)
                    landPatternNameMatchFlag = true;
              }
              else if(searchFilterCriteria.getLandPatternNameSearchBy().equals(SearchOptionEnum.CONTAINS) == true)
              {
                 if((subtypeScheme.getLandPatternName().matches(landpatternName)) == true)
                    landPatternNameMatchFlag = true;
              }
            }
          }
          else
          {
            landPatternNameMatchFlag = true;
          }

          if (jointTypeMatchFlag && (geometryMatchFlag || searchFilterCriteria.includesNonMatches()) && landPatternNameMatchFlag)
            overAllMatchFlag = true;

          // to inculde only match result
          if (overAllMatchFlag)
          {
            // Create library land pattern
            LibraryLandPattern libraryLandPattern = new LibraryLandPattern();
            libraryLandPattern.setLandPatternName(resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN));
            libraryLandPattern.setNumberOfPads(resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN));
            libraryLandPattern.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));

            // Create library package. Will only create a new library package is it is not exist in the map
            LibraryPackage libraryPackage = null;
            if(subtypeScheme.isLibraryPackageInLibraryPathMap(libraryPath, 
                                                              resultSet.getString(_sqlManager.PACKAGE_LONG_NAME_COLUMN),
                                                              libraryLandPattern))
            {
              libraryPackage = subtypeScheme.getExistingLibraryPackage(libraryPath, resultSet.getString(_sqlManager.PACKAGE_LONG_NAME_COLUMN));
            }
            else
            {
              libraryPackage = new LibraryPackage();
              libraryPackage.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
              libraryPackage.setPackageName(resultSet.getString(_sqlManager.PACKAGE_NAME_COLUMN));
              libraryPackage.setPackageLongName(resultSet.getString(_sqlManager.PACKAGE_LONG_NAME_COLUMN));
              libraryPackage.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
              libraryPackage.setLibraryLandPattern(libraryLandPattern);
            }

            // Create library subtype scheme
            LibrarySubtypeScheme librarySubtypeScheme = new LibrarySubtypeScheme();
            librarySubtypeScheme.setLibrarySubtypeSchemeId(librarySubtypeSchemeID);
            librarySubtypeScheme.setLibraryPackage(libraryPackage);
            librarySubtypeScheme.setMatchPercentage(overallMatchPercentage);
            librarySubtypeScheme.setUseOneJointType(useOnlyOneJointType);
            librarySubtypeScheme.setUseOneSubtype(useOnlyOneSubtype);

            librarySubtypeScheme.setDateCreated(dateCreated);

            if (dateUpdated != null)
              librarySubtypeScheme.setDateUpdated(dateUpdated);

            // Get all the subtype for the subtype scheme
            getLibrarySubtypeBySubtypeScheme(preparedStatementLibrarySubtypeBySubtypeSchemeId, librarySubtypeScheme);
            
            // Get all the component type for the subtype
            getLibraryComponentByLibrarySubtype(preparedStatementLibraryComponentTypeByLibrarySubtypeId, librarySubtypeScheme);

            jointTypeName = getJointTypeNameByLibrarySubtypeScheme(libraryPackage, librarySubtypeScheme);

            libraryPackage.addJointTypeNameToLibrarySubtypeSchemesMap(jointTypeName, librarySubtypeScheme);
            libraryPackage.addJointTypeNameToLibraryPackageCheckSumMap(jointTypeName, resultSet.getLong(_sqlManager.PACKAGE_CHECKSUM_COLUMN));
            compPackage.addLibraryPathToLibraryPackageMap(libraryPath, libraryPackage);
            subtypeScheme.addLibraryPathToLibraryPackageMap(libraryPath, libraryPackage);
          }
        }
      }
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    finally
    {
      //Siew Yeng - XCR-3318
      RelationalDatabaseUtil.closeConnection(connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
    }

    return libraryCompPackages;
  }


  /**
   * This will group the CompPackage according to number of pad for the packages.
   *
   * @author Poh Kheng
   */
  public Map<Integer, List<CompPackage>> categoriesCompPackageByNumberOfPad(List<CompPackage> compPackages) throws DatastoreException
  {
    Map<Integer, List<CompPackage>> numberOfPadtoCompPackageMap = new HashMap<Integer, List<CompPackage>>();
    boolean ascending = true;
    int numberOfPad;

    Collections.sort(compPackages, new CompPackageComparator(ascending, CompPackageComparatorEnum.NUMBER_OF_PADS));

    for (CompPackage compPackage : compPackages)
    {
      numberOfPad = compPackage.getLandPattern().getNumberOfLandPatternPads();
      if (numberOfPadtoCompPackageMap.containsKey(numberOfPad))
      {
        numberOfPadtoCompPackageMap.get(numberOfPad).add(compPackage);
      }
      else
      {
        List<CompPackage> componentPadLists = new LinkedList<CompPackage>();
        componentPadLists.add(compPackage);
        numberOfPadtoCompPackageMap.put(new Integer(numberOfPad), componentPadLists);
      }
    }

    return numberOfPadtoCompPackageMap;
  }

  /**
   * Thsi would return the list of library subtype which is related to the library subtype scheme
   *
   * @author Poh Kheng
   */
  public void getLibrarySubtypeBySubtypeScheme(PreparedStatement preparedStatement,
                                                               LibrarySubtypeScheme librarySubtypeScheme) throws DatastoreException
  {
    Assert.expect(librarySubtypeScheme != null);

    try
    {
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtypeScheme.getLibrarySubtypeSchemeId());
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      //make sure the list is empty.
      librarySubtypeScheme.clearLibrarySubtypeList();

      while (resultSet.next())
      {
        // Get required information
        LibrarySubtype librarySubtype = new LibrarySubtype();
        librarySubtype.setSubtypeId(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
        librarySubtype.setSubtypeName(resultSet.getString(_sqlManager.SUBTYPE_NAME_COLUMN));
        librarySubtype.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
        librarySubtype.setSubtypeComment(resultSet.getString(_sqlManager.COMMENT_COLUMN));

        // Setting UserGainEnum
        if (resultSet.getString(_sqlManager.USER_GAIN_NAME_COLUMN).equals(LEGACY))
        {
          librarySubtype.setLegacyUserGainEnum(true);
          librarySubtype.setUserGainEnum(UserGainEnum.ONE);
        }
        else
          librarySubtype.setUserGainEnum(UserGainEnum.getUserGainToEnumMapValue(Integer.parseInt(resultSet.getString(_sqlManager.USER_GAIN_NAME_COLUMN))));
        
        // Setting SignalCompensationEnum
        if (resultSet.getString(_sqlManager.INTEGRATION_LEVEL_NAME_COLUMN).equals(LEGACY))
        {
          librarySubtype.setLegacySignalCompensationEnum(true);
          librarySubtype.setSignalCompensationEnum(SignalCompensationEnum.DEFAULT_LOW);
        }
        else
          librarySubtype.setSignalCompensationEnum(SignalCompensationEnum.getSignalCompensationEnum(resultSet.getString(_sqlManager.INTEGRATION_LEVEL_NAME_COLUMN)));
        
        // Setting ArtifactCompensationEnum
        if (resultSet.getString(_sqlManager.ARTIFACT_COMPENSATION_NAME_COLUMN).equals(LEGACY))
        {
          librarySubtype.setLegacyArtifactCompensationEnum(true);
          librarySubtype.setArtifactCompensationStateEnum(ArtifactCompensationStateEnum.NOT_APPLICABLE);
        }
        else
          librarySubtype.setArtifactCompensationStateEnum(ArtifactCompensationStateEnum.getArtifactCompensationStateEnum(resultSet.getString(_sqlManager.ARTIFACT_COMPENSATION_NAME_COLUMN)));
        
        // Setting MagnificationTypeEnum
        if (resultSet.getString(_sqlManager.MAGNIFICATION_NAME_COLUMN).equals(LEGACY))
        {
          librarySubtype.setLegacyMagnificationTypeEnum(true);
          librarySubtype.setMagnificationTypeEnum(MagnificationTypeEnum.LOW);
        }
        else
          librarySubtype.setMagnificationTypeEnum(MagnificationTypeEnum.getMagnificationToEnumMapValue(resultSet.getString(_sqlManager.MAGNIFICATION_NAME_COLUMN)));
                
        // Setting DynamicRangeOptimizationEnum
        if (resultSet.getString(_sqlManager.DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN).equals(LEGACY))
        {
          librarySubtype.setLegacyDynamicRangeOptimizationLevelEnum(true);
          librarySubtype.setDynamicRangeOptimizationLevelEnum(DynamicRangeOptimizationLevelEnum.ONE);
        }
        else
          librarySubtype.setDynamicRangeOptimizationLevelEnum(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(
                  Integer.parseInt(resultSet.getString(_sqlManager.DYNAMIC_RANGE_OPTIMIZATION_NAME_COLUMN))));
        
        // Set library project name
        librarySubtypeScheme.setLibraryProjectName(resultSet.getString(_sqlManager.PROJECT_NAME_COLUMN));
        
        // Assign LibrarySubtype into LibrarySubtypeScheme
        librarySubtypeScheme.addLibrarySubtype(librarySubtype);
      }
    }
    catch (SQLException sqlEx)
    {
      SQLDatastoreException sqlDex = new SQLDatastoreException("", sqlEx.getMessage());
      sqlDex.initCause(sqlEx);
      throw sqlDex;
    }
  }
  
  /**
   * This function retrieves a list of LibraryComponentType from Database based on LibrarySubtype Id.
   * 
   * @author Cheah Lee Herng 
   */
  public void getLibraryComponentByLibrarySubtype(PreparedStatement preparedStatement, LibrarySubtypeScheme librarySubtypeScheme) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(librarySubtypeScheme != null);
    
    try
    {
      for(LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
      {
        RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtype.getSubtypeId());
        ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

        //make sure the list is empty.
        librarySubtype.clearLibraryComponentTypeList();

        while (resultSet.next())
        {
          // Get required information
          LibraryComponentType libraryComponentType = new LibraryComponentType();
          libraryComponentType.setComponentTypeId(resultSet.getInt(_sqlManager.REF_COMPONENT_ID_COLUMN));
          libraryComponentType.setReferenceDesignator(resultSet.getString(_sqlManager.COMPONENT_NAME_COLUMN));
          
          // Setting UserGainEnum
          if (resultSet.getString(_sqlManager.FOCUS_METHOD_NAME_COLUMN).equals(LEGACY))
          {
            libraryComponentType.setLegacyGlobalSurfaceModelEnum(true);
            libraryComponentType.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.AUTOFOCUS);
          }
          else
            libraryComponentType.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.getGlobalSurfaceModelEnum(resultSet.getString(_sqlManager.FOCUS_METHOD_NAME_COLUMN)));
                    
          librarySubtype.addLibraryComponentType(libraryComponentType);
        }
      }            
    }
    catch (SQLException sqlEx)
    {
      SQLDatastoreException sqlDex = new SQLDatastoreException("", sqlEx.getMessage());
      sqlDex.initCause(sqlEx);
      throw sqlDex;
    }
  }
  
  /**
   * This function retrieves a list of LibraryComponentType from Database based on LibrarySubtype Id.
   * 
   * @author Cheah Lee Herng 
   */
  public void getLibraryComponentByLibrarySubtype(PreparedStatement preparedStatement, LibrarySubtype librarySubtype) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(librarySubtype != null);
    
    try
    {
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtype.getSubtypeId());
      ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      
      //make sure the list is empty.
      librarySubtype.clearLibraryComponentTypeList();
      
      while (resultSet.next())
      {
        // Get required information
        LibraryComponentType libraryComponentType = new LibraryComponentType();
        libraryComponentType.setComponentTypeId(resultSet.getInt(_sqlManager.REF_COMPONENT_ID_COLUMN));
        libraryComponentType.setReferenceDesignator(resultSet.getString(_sqlManager.COMPONENT_NAME_COLUMN));

        // Setting UserGainEnum
        if (resultSet.getString(_sqlManager.FOCUS_METHOD_NAME_COLUMN).equals(LEGACY))
        {
          libraryComponentType.setLegacyGlobalSurfaceModelEnum(true);
          libraryComponentType.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.AUTOFOCUS);
        }
        else
          libraryComponentType.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.getGlobalSurfaceModelEnum(resultSet.getString(_sqlManager.FOCUS_METHOD_NAME_COLUMN)));

        librarySubtype.addLibraryComponentType(libraryComponentType);
      }
    }
    catch (SQLException sqlEx)
    {
      SQLDatastoreException sqlDex = new SQLDatastoreException("", sqlEx.getMessage());
      sqlDex.initCause(sqlEx);
      throw sqlDex;
    }
  }

  /**
   * @param librarySubtypeScheme LibrarySubtypeScheme
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getJointTypeNameByLibrarySubtypeScheme(LibraryPackage libraryPackage,
                                                       LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(libraryPackage != null);
    Assert.expect(librarySubtypeScheme != null);

    String jointTypeName = null;
    String tempJointTypeName = null;

    if (librarySubtypeScheme.useOneJointType())
    {
      jointTypeName = librarySubtypeScheme.getLibrarySubtypes().get(0).getJointTypeName();
    }
    else
    {
      for (Map.Entry<String, List<LibrarySubtypeScheme>> mapEntry : libraryPackage.getJointTypeNameToLibrarySubtypeSchemesMap().entrySet())
      {
        String currentJointTypeName = mapEntry.getKey();
        if (currentJointTypeName.startsWith("Mix"))
        {
          if (tempJointTypeName == null || currentJointTypeName.compareToIgnoreCase(tempJointTypeName) > 0)
          {
            tempJointTypeName = currentJointTypeName;
          }
        }
      }

      if (tempJointTypeName == null)
      {
        jointTypeName = "Mix_1";
      }
      else
      {
        final String mixJointTypeSubString = "Mix_";
        int index = tempJointTypeName.indexOf("Mix_");
        if (index >= 0 && index < mixJointTypeSubString.length())
        {
            int runningNumber = Integer.parseInt(tempJointTypeName.substring(mixJointTypeSubString.length(), tempJointTypeName.length()));
            jointTypeName = "Mix_" + (runningNumber + 1);
        }
        else
            Assert.expect(false, "Unexpected mix joint type name : " + tempJointTypeName);
      }
    }

    Assert.expect(jointTypeName != null);
    return jointTypeName;
  }

  /**
   *
   * @return List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public LibraryLandPattern getLibraryLandPatternPads(String libraryPath, LibraryLandPattern libraryLandPattern) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryLandPattern != null);

    PreparedStatement preparedStatement;
    ResultSet resultSet;

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternPadByLandPatternIdSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, libraryLandPattern.getLandPatternId());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while(resultSet.next())
      {
        LibraryLandPatternPad libraryLandPatternPad = new LibraryLandPatternPad();
        libraryLandPatternPad.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryLandPatternPad.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
        libraryLandPatternPad.setPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryLandPatternPad.setXLocation(resultSet.getInt(_sqlManager.X_LOCATION_COLUMN));
        libraryLandPatternPad.setYLocation(resultSet.getInt(_sqlManager.Y_LOCATION_COLUMN));
        libraryLandPatternPad.setWidthInNanoMeters(resultSet.getInt(_sqlManager.X_DIMENSION_COLUMN));
        libraryLandPatternPad.setLengthInNanoMeters(resultSet.getInt(_sqlManager.Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setDegreesRotation(resultSet.getInt(_sqlManager.ROTATION_COLUMN));
        libraryLandPatternPad.setRefShapeId(resultSet.getInt(_sqlManager.REF_SHAPE_ID_COLUMN));
        libraryLandPatternPad.setIsSurfaceMount(resultSet.getBoolean(_sqlManager.IS_SURFACE_MOUNT_COLUMN));
        libraryLandPatternPad.setIsPadOne(resultSet.getBoolean(_sqlManager.IS_PAD_ONE_COLUMN));
//        libraryLandPatternPad.setHoleDiameter(resultSet.getInt(_sqlManager.HOLE_DIAMETER_COLUMN));
        //Siew Yeng - XCR-3318
        libraryLandPatternPad.setHoleWidth(resultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleLength(resultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleXLocation(resultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN));
        libraryLandPatternPad.setHoleYLocation(resultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN));
        libraryLandPatternPad.setShapeName(resultSet.getString(_sqlManager.SHAPE_NAME_COLUMN));
        libraryLandPatternPad.setLibraryLandPattern(libraryLandPattern);

        libraryLandPattern.addLibraryLandPatternPad(libraryLandPatternPad);
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return libraryLandPattern;
  }

  /**
   * @param libraryPath String
   * @param libraryPackage LibraryPackage
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void getLibraryPackagePins(String libraryPath, LibraryPackage libraryPackage) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackage != null);

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    // Create prepared statement
    PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getPackagePinByPackageNameAndJointTypeSqlString());

    LibrarySubtypeScheme librarySubtypeScheme = libraryPackage.getLibrarySubtypeSchemes().get(0);
    libraryPackage.clearLibraryPackagePinSet();
    getLibraryPackagePins(preparedStatement, libraryPackage, librarySubtypeScheme.getLibrarySubtypeSchemeId());
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param libraryPackage LibraryPackage
   * @param librarySubtypeSchemeId int
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void getLibraryPackagePins(PreparedStatement preparedStatement,
                                    LibraryPackage libraryPackage,
                                    int librarySubtypeSchemeId) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(libraryPackage != null);
    Assert.expect(librarySubtypeSchemeId > 0);

    ResultSet resultSet;
    try
    {
      RelationalDatabaseUtil.setString(preparedStatement, 1, libraryPackage.getPackageLongName());
      RelationalDatabaseUtil.setInt(preparedStatement, 2, librarySubtypeSchemeId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while(resultSet.next())
      {
        LibraryLandPatternPad libraryLandPatternPad = new LibraryLandPatternPad();
        libraryLandPatternPad.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryLandPatternPad.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
        libraryLandPatternPad.setPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryLandPatternPad.setXLocation(resultSet.getInt(_sqlManager.X_LOCATION_COLUMN));
        libraryLandPatternPad.setYLocation(resultSet.getInt(_sqlManager.Y_LOCATION_COLUMN));
        libraryLandPatternPad.setWidthInNanoMeters(resultSet.getInt(_sqlManager.X_DIMENSION_COLUMN));
        libraryLandPatternPad.setLengthInNanoMeters(resultSet.getInt(_sqlManager.Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setDegreesRotation(resultSet.getInt(_sqlManager.ROTATION_COLUMN));
        libraryLandPatternPad.setRefShapeId(resultSet.getInt(_sqlManager.REF_SHAPE_ID_COLUMN));
        libraryLandPatternPad.setIsSurfaceMount(resultSet.getBoolean(_sqlManager.IS_SURFACE_MOUNT_COLUMN));
        libraryLandPatternPad.setIsPadOne(resultSet.getBoolean(_sqlManager.IS_PAD_ONE_COLUMN));
//        libraryLandPatternPad.setHoleDiameter(resultSet.getInt(_sqlManager.HOLE_DIAMETER_COLUMN));
        //Siew Yeng - XCR-3318
        libraryLandPatternPad.setHoleWidth(resultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleLength(resultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleXLocation(resultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN));
        libraryLandPatternPad.setHoleYLocation(resultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN));
        libraryLandPatternPad.setShapeName(resultSet.getString(_sqlManager.SHAPE_NAME_COLUMN));

        LibraryPackagePin libraryPackagePin = new LibraryPackagePin();
        libraryPackagePin.setPackagePinId(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));
        libraryPackagePin.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryPackagePin.setRefJointTypeId(resultSet.getInt(_sqlManager.REF_JOINT_TYPE_ID_COLUMN));
        libraryPackagePin.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
        libraryPackagePin.setOrientation(resultSet.getInt(_sqlManager.ORIENTATION_COLUMN));
        libraryPackagePin.setJointHeight(resultSet.getDouble(_sqlManager.JOINT_HEIGHT_COLUMN));
        libraryPackagePin.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
        libraryPackagePin.setLandPatternPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryPackagePin.setLibraryLandPatternPad(libraryLandPatternPad);
        libraryPackagePin.setLibraryPackage(libraryPackage);

        libraryPackage.addLibraryPackagePin(libraryPackagePin);
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }
  
  /**
   * This function retrieves LibraryPackage object from Package Library database, based on LibrarySubtypeSchemeId.
   * 
   * @param libraryPath
   * @param librarySubtypeSchemeId
   * @return LibraryPackage object
   * @throws DatastoreException 
   * @author Cheah Lee Herng
   */
  public List<LibraryPackage> getLibraryPackageByLibrarySubtypeSchemeId(String libraryPath,
                                                                        int librarySubtypeSchemeId) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(librarySubtypeSchemeId > 0);
    
    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
    
    return getLibraryPackageByLibrarySubtypeSchemeId(connection, librarySubtypeSchemeId);
  }
  
  /**   
   * @param connection
   * @param librarySubtypeSchemeId
   * @return LibraryPackage object
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  private List<LibraryPackage> getLibraryPackageByLibrarySubtypeSchemeId(Connection connection,
                                                                         int librarySubtypeSchemeId) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(librarySubtypeSchemeId > 0);
        
    Map<Integer, LibraryPackage> libraryPackageIdToLibraryPackage = new HashMap<Integer, LibraryPackage>();
    Map<Integer, LibrarySubtype> subtypeIdToLibrarySubtype = new HashMap<Integer,LibrarySubtype>();
    Map<Integer, LibraryLandPattern> libraryLandPatternIdToLibraryLandPattern = new HashMap<Integer, LibraryLandPattern>();
    Map<Integer, LibraryLandPatternPad> libraryLandPatternPadIdToLibraryLandPatternPad = new HashMap<Integer, LibraryLandPatternPad>();
    Map<Integer, LibraryPackagePin> libraryPackagePinIdToLibraryPackagePin = new HashMap<Integer, LibraryPackagePin>();
    Map<String, Integer> libraryPackageStringToLibraryPackageId = new HashMap<String, Integer>();
    List<LibraryPackage> libraryPackages = new ArrayList<LibraryPackage>();
    
    PreparedStatement preparedStatement;
    ResultSet resultSet;
        
    LibrarySubtype librarySubtype = null;
    LibraryPackage libraryPackage = null;
    LibraryPackagePin libraryPackagePin = null;    
    LibraryLandPattern libraryLandPattern = null;
    LibraryLandPatternPad libraryLandPatternPad = null;
    
    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryPackageByLibrarySubtypeSchemeIdSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtypeSchemeId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      
      while (resultSet.next())
      {
        if (libraryPackageIdToLibraryPackage.containsKey(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN)) == false)          
        {
          libraryPackage = new LibraryPackage();
          libraryPackage.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
          libraryPackage.setPackageName(resultSet.getString(_sqlManager.PACKAGE_NAME_COLUMN));
          libraryPackage.setPackageLongName(resultSet.getString(_sqlManager.PACKAGE_LONG_NAME_COLUMN));
          libraryPackage.setCheckSum(resultSet.getLong(_sqlManager.PACKAGE_CHECKSUM_COLUMN));
          libraryPackageIdToLibraryPackage.put(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN), libraryPackage);
        }
        else
        {
          libraryPackage = libraryPackageIdToLibraryPackage.get(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
        }
                     
        if (subtypeIdToLibrarySubtype.containsKey(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN)) == false)
        {
          librarySubtype = new LibrarySubtype();
          librarySubtype.setSubtypeId(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
          librarySubtype.setSubtypeName(resultSet.getString(_sqlManager.SUBTYPE_NAME_COLUMN));
          librarySubtype.setSubtypeComment(resultSet.getString(_sqlManager.COMMENT_COLUMN));
          librarySubtype.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
          subtypeIdToLibrarySubtype.put(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN),librarySubtype);
        }
        else
        {
          librarySubtype = subtypeIdToLibrarySubtype.get(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
        }
        
        if (libraryLandPatternIdToLibraryLandPattern.containsKey(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN)) == false)
        {
          libraryLandPattern = new LibraryLandPattern();
          libraryLandPattern.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
          libraryLandPattern.setLandPatternName(resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN));
          libraryLandPattern.setNumberOfPads(resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN));
          libraryLandPatternIdToLibraryLandPattern.put(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN), libraryLandPattern);
        }
        else
        {
          libraryLandPattern = libraryLandPatternIdToLibraryLandPattern.get(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
        }
        
        if (libraryLandPatternPadIdToLibraryLandPatternPad.containsKey(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN)) == false)
        {
          libraryLandPatternPad = new LibraryLandPatternPad();
          libraryLandPatternPad.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
          libraryLandPatternPad.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
          libraryLandPatternPad.setPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
          libraryLandPatternPad.setXLocation(resultSet.getInt(_sqlManager.X_LOCATION_COLUMN));
          libraryLandPatternPad.setYLocation(resultSet.getInt(_sqlManager.Y_LOCATION_COLUMN));
          libraryLandPatternPad.setWidthInNanoMeters(resultSet.getInt(_sqlManager.X_DIMENSION_COLUMN));
          libraryLandPatternPad.setLengthInNanoMeters(resultSet.getInt(_sqlManager.Y_DIMENSION_COLUMN));
          libraryLandPatternPad.setDegreesRotation(resultSet.getInt(_sqlManager.ROTATION_COLUMN));
          libraryLandPatternPad.setRefShapeId(resultSet.getInt(_sqlManager.REF_SHAPE_ID_COLUMN));
          libraryLandPatternPad.setIsSurfaceMount(resultSet.getBoolean(_sqlManager.IS_SURFACE_MOUNT_COLUMN));
          libraryLandPatternPad.setIsPadOne(resultSet.getBoolean(_sqlManager.IS_PAD_ONE_COLUMN));
//          libraryLandPatternPad.setHoleDiameter(resultSet.getInt(_sqlManager.HOLE_DIAMETER_COLUMN));
          //Siew Yeng - XCR-3318
          libraryLandPatternPad.setHoleWidth(resultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN));
          libraryLandPatternPad.setHoleLength(resultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN));
          libraryLandPatternPad.setHoleXLocation(resultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN));
          libraryLandPatternPad.setHoleYLocation(resultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN));
          libraryLandPatternPad.setShapeName(resultSet.getString(_sqlManager.SHAPE_NAME_COLUMN));
          libraryLandPatternPadIdToLibraryLandPatternPad.put(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN), libraryLandPatternPad);
        }
        else
        {
          libraryLandPatternPad = libraryLandPatternPadIdToLibraryLandPatternPad.get(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        }
        
        if (libraryPackagePinIdToLibraryPackagePin.containsKey(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN)) == false)
        {
          libraryPackagePin = new LibraryPackagePin();
          libraryPackagePin.setPackagePinId(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));
          libraryPackagePin.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
          libraryPackagePin.setRefJointTypeId(resultSet.getInt(_sqlManager.REF_JOINT_TYPE_ID_COLUMN));
          libraryPackagePin.setOrientation(resultSet.getInt(_sqlManager.ORIENTATION_COLUMN));
          libraryPackagePin.setJointHeight(resultSet.getInt(_sqlManager.JOINT_HEIGHT_COLUMN));
          libraryPackagePin.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
          libraryPackagePin.setLandPatternPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
          libraryPackagePin.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
          libraryPackagePin.setInspected(resultSet.getBoolean(_sqlManager.INSPECTABLE_COLUMN));
          libraryPackagePinIdToLibraryPackagePin.put(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN), libraryPackagePin);
        }
        else
        {
          libraryPackagePin = libraryPackagePinIdToLibraryPackagePin.get(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));
        }
        
        // Construct comparison string
        String libraryPackageString = libraryLandPattern.getLandPatternId() + "_" + librarySubtype.getSubtypeId() + "_" + 
                                      libraryPackagePin.getPackagePinId() + "_" + libraryLandPatternPad.getLandPatternPadId();
        if (libraryPackageStringToLibraryPackageId.containsKey(libraryPackageString) == false)
        {
          libraryPackageStringToLibraryPackageId.put(libraryPackageString, libraryPackage.getPackageId());
        }
      }
      resultSet.close();
      resultSet = null;
      
      for(Map.Entry<String, Integer> entry : libraryPackageStringToLibraryPackageId.entrySet())
      {
        String libraryPackageString = entry.getKey();
        int libraryPackageId = entry.getValue();
        
        String[] libraryPackageStringArray = libraryPackageString.split("_");
        int landPatternId = Integer.parseInt(libraryPackageStringArray[0]);
        int subtypeId = Integer.parseInt(libraryPackageStringArray[1]);
        int packagePinId = Integer.parseInt(libraryPackageStringArray[2]);
        int landPatternPadId = Integer.parseInt(libraryPackageStringArray[3]);
        
        LibraryPackage assignLibraryPackage = libraryPackageIdToLibraryPackage.get(libraryPackageId);
        LibraryPackagePin assignLibraryPackagePin = libraryPackagePinIdToLibraryPackagePin.get(packagePinId);
        LibraryLandPattern assignLibraryLandPattern = libraryLandPatternIdToLibraryLandPattern.get(landPatternId);
        LibraryLandPatternPad assignLibraryLandPatternPad = libraryLandPatternPadIdToLibraryLandPatternPad.get(landPatternPadId);
        LibrarySubtype assignLibrarySubtype = subtypeIdToLibrarySubtype.get(subtypeId);
        
        assignLibraryLandPattern.addLibraryLandPatternPad(assignLibraryLandPatternPad);
        assignLibraryPackagePin.setLibraryLandPatternPad(assignLibraryLandPatternPad);
        assignLibraryPackagePin.setLibraryPackage(assignLibraryPackage);
        assignLibraryPackagePin.setLibrarySubtype(assignLibrarySubtype);
        
        assignLibraryPackage.addLibraryPackagePin(assignLibraryPackagePin);
        assignLibraryPackage.setLibraryLandPattern(assignLibraryLandPattern);
        
        if (libraryPackages.contains(assignLibraryPackage) == false)
          libraryPackages.add(assignLibraryPackage);
      }
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    finally
    {
      libraryPackageIdToLibraryPackage.clear();
      subtypeIdToLibrarySubtype.clear();
      libraryLandPatternIdToLibraryLandPattern.clear();
      libraryLandPatternPadIdToLibraryLandPatternPad.clear();
      libraryPackagePinIdToLibraryPackagePin.clear();
      libraryPackageStringToLibraryPackageId.clear();
    }
    
    return libraryPackages;
  }

  /**
   * @param libraryPath String
   * @param librarySubtypeSchemeId int
   * @return List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public Set <LibraryPackagePin> getLibraryPackagePinsByLibrarySubtypeSchemeId(String libraryPath,
                                                                               int librarySubtypeSchemeId) throws DatastoreException

  {
    Assert.expect(libraryPath != null);
    Assert.expect(librarySubtypeSchemeId > 0);

    Set<LibraryPackagePin> libraryPackagePinSet = new TreeSet<LibraryPackagePin>(new LibraryPackagePinComparator(true));

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    libraryPackagePinSet = getLibraryPackagePinsByLibrarySubtypeSchemeId(connection, librarySubtypeSchemeId);

    return libraryPackagePinSet;
  }

  /**
   * @param connection Connection
   * @param librarySubtypeSchemeId int
   * @return Set
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public Set <LibraryPackagePin> getLibraryPackagePinsByLibrarySubtypeSchemeId(Connection connection,
                                                                               int librarySubtypeSchemeId) throws DatastoreException

  {
    Assert.expect(connection != null);
    Assert.expect(librarySubtypeSchemeId > 0);

    Set<LibraryPackagePin> libraryPackagePinSet = new TreeSet<LibraryPackagePin>(new LibraryPackagePinComparator(true));
    Map<Integer, LibrarySubtype> subtypeIdToLibrarySubtype = new HashMap<Integer,LibrarySubtype>();
    PreparedStatement preparedStatement;
    ResultSet resultSet;
    LibrarySubtype librarySubtype = new LibrarySubtype();

    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibraryPackagePinByLibrarySubtypeSchemeIdSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtypeSchemeId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while (resultSet.next())
      {
        if (subtypeIdToLibrarySubtype.containsKey(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN)) == false)
        {
          librarySubtype = new LibrarySubtype();
          librarySubtype.setSubtypeId(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
          librarySubtype.setSubtypeName(resultSet.getString(_sqlManager.SUBTYPE_NAME_COLUMN));
          librarySubtype.setSubtypeComment(resultSet.getString(_sqlManager.COMMENT_COLUMN));
          librarySubtype.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
          subtypeIdToLibrarySubtype.put(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN),librarySubtype);
        }
        else
        {
          librarySubtype = subtypeIdToLibrarySubtype.get(resultSet.getInt(_sqlManager.REF_SUBTYPE_ID_COLUMN));
        }
        LibraryPackagePin libraryPackagePin = new LibraryPackagePin();
        libraryPackagePin.setPackagePinId(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));
        libraryPackagePin.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryPackagePin.setRefJointTypeId(resultSet.getInt(_sqlManager.REF_JOINT_TYPE_ID_COLUMN));
        libraryPackagePin.setOrientation(resultSet.getInt(_sqlManager.ORIENTATION_COLUMN));
        libraryPackagePin.setJointHeight(resultSet.getInt(_sqlManager.JOINT_HEIGHT_COLUMN));
        libraryPackagePin.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
        libraryPackagePin.setLandPatternPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryPackagePin.setJointTypeName(resultSet.getString(_sqlManager.JOINT_TYPE_NAME_COLUMN));
        libraryPackagePin.setInspected(resultSet.getBoolean(_sqlManager.INSPECTABLE_COLUMN));
        libraryPackagePin.setLibrarySubtype(librarySubtype);
        libraryPackagePinSet.add(libraryPackagePin);
      }
      resultSet.close();
      resultSet = null;
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return libraryPackagePinSet;
  }

  /**
   * @param libraryPath String
   * @param libraryPackagePin LibraryPackagePin
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void getLibraryLandPatternPadByLibraryPackagePinId(String libraryPath,
                                                            LibraryPackagePin libraryPackagePin) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackagePin != null);

    PreparedStatement preparedStatement;
    ResultSet resultSet;

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternPadByLibraryPackagePinIdSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, libraryPackagePin.getPackagePinId());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while (resultSet.next())
      {
        LibraryLandPattern libraryLandPattern = new LibraryLandPattern();
        libraryLandPattern.setLandPatternName(resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN));
        libraryLandPattern.setNumberOfPads(resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN));
        libraryLandPattern.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));

        LibraryLandPatternPad libraryLandPatternPad = new LibraryLandPatternPad();
        libraryLandPatternPad.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryLandPatternPad.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
        libraryLandPatternPad.setPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryLandPatternPad.setXLocation(resultSet.getInt(_sqlManager.X_LOCATION_COLUMN));
        libraryLandPatternPad.setYLocation(resultSet.getInt(_sqlManager.Y_LOCATION_COLUMN));
        libraryLandPatternPad.setWidthInNanoMeters(resultSet.getInt(_sqlManager.X_DIMENSION_COLUMN));
        libraryLandPatternPad.setLengthInNanoMeters(resultSet.getInt(_sqlManager.Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setDegreesRotation(resultSet.getInt(_sqlManager.ROTATION_COLUMN));
        libraryLandPatternPad.setRefShapeId(resultSet.getInt(_sqlManager.REF_SHAPE_ID_COLUMN));
        libraryLandPatternPad.setIsSurfaceMount(resultSet.getBoolean(_sqlManager.IS_SURFACE_MOUNT_COLUMN));
        libraryLandPatternPad.setIsPadOne(resultSet.getBoolean(_sqlManager.IS_PAD_ONE_COLUMN));
//        libraryLandPatternPad.setHoleDiameter(resultSet.getInt(_sqlManager.HOLE_DIAMETER_COLUMN));
        //Siew Yeng - XCR-3318
        libraryLandPatternPad.setHoleWidth(resultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleLength(resultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleXLocation(resultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN));
        libraryLandPatternPad.setHoleYLocation(resultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN));
        libraryLandPatternPad.setShapeName(resultSet.getString(_sqlManager.SHAPE_NAME_COLUMN));
        libraryLandPatternPad.setLibraryLandPattern(libraryLandPattern);

        libraryPackagePin.setLibraryLandPatternPad(libraryLandPatternPad);
      }
      resultSet.close();
      resultSet = null;
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param connection Connection
   * @param librarySubtypeSchemeId int
   * @return List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public List<LibraryLandPatternPad> getLibraryLandPatternPadByLibrarySubtypeSchemeId(Connection connection,
                                                                                      int librarySubtypeSchemeId) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(librarySubtypeSchemeId > 0);

    List<LibraryLandPatternPad> libraryLandPatternPads = new ArrayList<LibraryLandPatternPad>();
    PreparedStatement preparedStatement;
    ResultSet resultSet;

    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternPadByLibrarySubtypeSchemeIdSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtypeSchemeId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while (resultSet.next())
      {
        LibraryLandPattern libraryLandPattern = new LibraryLandPattern();
        libraryLandPattern.setLandPatternName(resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN));
        libraryLandPattern.setNumberOfPads(resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN));
        libraryLandPattern.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));

        LibraryLandPatternPad libraryLandPatternPad = new LibraryLandPatternPad();
        libraryLandPatternPad.setLandPatternPadId(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        libraryLandPatternPad.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
        libraryLandPatternPad.setPadName(resultSet.getString(_sqlManager.PAD_NAME_COLUMN));
        libraryLandPatternPad.setXLocation(resultSet.getInt(_sqlManager.X_LOCATION_COLUMN));
        libraryLandPatternPad.setYLocation(resultSet.getInt(_sqlManager.Y_LOCATION_COLUMN));
        libraryLandPatternPad.setWidthInNanoMeters(resultSet.getInt(_sqlManager.X_DIMENSION_COLUMN));
        libraryLandPatternPad.setLengthInNanoMeters(resultSet.getInt(_sqlManager.Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setDegreesRotation(resultSet.getInt(_sqlManager.ROTATION_COLUMN));
        libraryLandPatternPad.setRefShapeId(resultSet.getInt(_sqlManager.REF_SHAPE_ID_COLUMN));
        libraryLandPatternPad.setIsSurfaceMount(resultSet.getBoolean(_sqlManager.IS_SURFACE_MOUNT_COLUMN));
        libraryLandPatternPad.setIsPadOne(resultSet.getBoolean(_sqlManager.IS_PAD_ONE_COLUMN));
//        libraryLandPatternPad.setHoleDiameter(resultSet.getInt(_sqlManager.HOLE_DIAMETER_COLUMN));
        //Siew Yeng - XCR-3318
        libraryLandPatternPad.setHoleWidth(resultSet.getInt(_sqlManager.HOLE_X_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleLength(resultSet.getInt(_sqlManager.HOLE_Y_DIMENSION_COLUMN));
        libraryLandPatternPad.setHoleXLocation(resultSet.getInt(_sqlManager.HOLE_X_LOCATION_COLUMN));
        libraryLandPatternPad.setHoleYLocation(resultSet.getInt(_sqlManager.HOLE_Y_LOCATION_COLUMN));
        libraryLandPatternPad.setShapeName(resultSet.getString(_sqlManager.SHAPE_NAME_COLUMN));
        libraryLandPatternPad.setLibraryLandPattern(libraryLandPattern);

        libraryLandPatternPads.add(libraryLandPatternPad);
      }
      resultSet.close();
      resultSet = null;
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    return libraryLandPatternPads;
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param refSubtypeId int
   * @return Map
   * @throws DatastoreException
   *
   * @author Tan Hock Zoon
   */
  public Map<AlgorithmSettingEnum, Serializable> getAlgorithmSettingEnumToValueByRefSubtypeId(PreparedStatement preparedStatement, int refSubtypeId) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(refSubtypeId > 0);

    Map<AlgorithmSettingEnum, Serializable> algorithmSettingEnumToValueMap = new HashMap<AlgorithmSettingEnum,Serializable>();
    ResultSet resultSet;

    try
    {
      RelationalDatabaseUtil.setInt(preparedStatement, 1, refSubtypeId);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while (resultSet.next())
      {
        try
        {
          AlgorithmSettingEnum algorithmSettingEnum = EnumStringLookup.getInstance().getAlgorithmSettingEnum(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN));
          Serializable value = getSerializedValue(resultSet.getString(_sqlManager.DATA_TYPE_NAME_COLUMN), resultSet.getString(_sqlManager.THRESHOLD_VALUE_COLUMN));

          algorithmSettingEnumToValueMap.put(algorithmSettingEnum, value);
        }
        catch(BadFormatException bex)
        {
          DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, bex);
          dex.initCause(bex);
          throw dex;
        }
      }
      resultSet.close();
      resultSet = null;
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return algorithmSettingEnumToValueMap;
  }

  /**
   * @param libraryPath String
   * @param librarySubtype LibrarySubtype
   * @param projectName
   * @return LibrarySubtype
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public LibrarySubtype getLibrarySubtypeByRefSubtypeIdAndProjectName(String libraryPath, 
                                                                      LibrarySubtype librarySubtype,
                                                                      String projectName) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(librarySubtype != null);
    Assert.expect(projectName != null);

    PreparedStatement preparedStatement;
    PreparedStatement subtypeToAlgorithmPreparedStatement;
    ResultSet resultSet;

    // Startup database
    _database.startupDatabase();

    // Get JDBC connection
    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

    // Create new connection
    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                  _database.getDatabaseUserName(),
                                                                  _database.getDatabasePassword(),
                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

    try
    {
      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefThresholdByRefSubtypeIdAndProjectNameSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, librarySubtype.getSubtypeId());
      RelationalDatabaseUtil.setString(preparedStatement, 2, projectName);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      //make sure the map is clear
      librarySubtype.clearThresholdNameToTunedValueMap();
      librarySubtype.clearThresholdNameToThresholdCommentMap();
      librarySubtype.clearAlgorithmEnumToEnabledFlagMap();
      
      while (resultSet.next())
      {
        try
        {
          boolean isAlgorithmSettingEnumExist = EnumStringLookup.getInstance().isAlgorithmSettingEnumExist(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN));
          if (isAlgorithmSettingEnumExist)
          {            
              AlgorithmSettingEnum algorithmSettingEnum = EnumStringLookup.getInstance().getAlgorithmSettingEnum(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN));
              Serializable value = getSerializedValue(resultSet.getString(_sqlManager.DATA_TYPE_NAME_COLUMN), resultSet.getString(_sqlManager.THRESHOLD_VALUE_COLUMN));
           
              librarySubtype.addThresholdNameToTunedValue(algorithmSettingEnum, value);
              librarySubtype.addThresholdNameToThresholdComment(resultSet.getString(_sqlManager.THRESHOLD_NAME_COLUMN),
                                                            resultSet.getString(_sqlManager.THRESHOLD_COMMENT_COLUMN));
          }
        }
        catch(BadFormatException bex)
        {
          DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, bex);
          dex.initCause(bex);
          throw dex;
        }
      }
      resultSet.close();
      resultSet = null;            
      
      // Retrieve SubtypeToAlgorithm data
      subtypeToAlgorithmPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLibrarySubtypeAlgorithmInfoBySubtypeIdAndProjectNameSqlString());
      RelationalDatabaseUtil.setInt(subtypeToAlgorithmPreparedStatement, 1, librarySubtype.getSubtypeId());
      RelationalDatabaseUtil.setString(subtypeToAlgorithmPreparedStatement, 2, projectName);
      resultSet = RelationalDatabaseUtil.executeQuery(subtypeToAlgorithmPreparedStatement);
      
      while (resultSet.next())
      {
        AlgorithmEnum algorithmEnum = EnumStringLookup.getInstance().getAlgorithmEnum(resultSet.getString(_sqlManager.ALGORITHM_NAME_COLUMN).toLowerCase());
        boolean isAlgorithmEnabled = LibraryUtil.getInstance().convertIntToBoolean(resultSet.getInt(_sqlManager.ALGORITHM_ENABLE_COLUMN));
        
        librarySubtype.addAlgorithmEnumToEnabledFlag(algorithmEnum, isAlgorithmEnabled);
      }
      resultSet.close();
      resultSet = null;
    }
    catch (SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return librarySubtype;
  }

  /**
   * This method is to commit changes to the database permanently.
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void commitTransaction() throws DatastoreException
  {
    try
    {
      for(Connection connection : _connections)
      {
        RelationalDatabaseUtil.commit(connection);
      }
    }
    finally
    {
      _connections.clear();
    }
  }

  /**
   * This method is to perform rollback operation on a list of connections.
   *
   * @param ex Exception
   * @throws DatastoreException
   */
  public void rollBackTransaction(Exception ex) throws DatastoreException
  {
    Assert.expect(ex != null);

    try
    {
      for(Connection connection : _connections)
      {
        RelationalDatabaseUtil.rollBack(connection);
      }
    }
    finally
    {
      _connections.clear();
    }

    DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, ex);
    dex.initCause(ex);
    throw dex;
  }

  /**
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void rollBackTransaction() throws DatastoreException
  {
    try
    {
      for(Connection connection : _connections)
      {
        RelationalDatabaseUtil.rollBack(connection);
      }
    }
    finally
    {
      _connections.clear();
    }
  }

  /**
   * This method commit data into database. If error occurs, it will rollback all the transaction.
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void saveIntoDatabase() throws DatastoreException
  {
    try
    {
      commitTransaction();
    }
    catch (DatastoreException commitDex)
    {
      try
      {
        rollBackTransaction(commitDex);
      }
      catch (DatastoreException rollBackDex)
      {
        // Do nothing here because the root cause of the problem is contained inside commitDex.
      }

      throw commitDex;
    }
  }

  /**
   * @param cancelDatabaseOperation boolean
   * @author Cheah Lee Herng
   */
  public void setCancelDatabaseOperation(boolean cancelDatabaseOperation)
  {
    _cancelDatabaseOperation = cancelDatabaseOperation;
  }

  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  public boolean cancelDatabaseOperation()
  {
    return _cancelDatabaseOperation;
  }

  /**
   * @param createPackagePreparedStatement PreparedStatement
   * @param selectPackagePreparedStatement PreparedStatement
   * @param packageLongNameToPackageIdMap Map
   * @param packageLongNameToLandPatternIdMap Map
   * @param packageShortName String
   * @param packageLongName String
   * @param landPatternId int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getPackageIdCreateIfNotExist(PreparedStatement createPackagePreparedStatement,
                                          PreparedStatement selectPackagePreparedStatement,
                                          Map<String,Integer> packageLongNameToPackageIdMap,
                                          Map<String,Integer> packageLongNameToLandPatternIdMap,
                                          String packageShortName,
                                          String packageLongName,
                                          int landPatternId,
                                          long checkSum) throws DatastoreException
  {
    Assert.expect(createPackagePreparedStatement != null);
    Assert.expect(selectPackagePreparedStatement != null);
    Assert.expect(packageLongNameToPackageIdMap != null);
    Assert.expect(packageLongNameToLandPatternIdMap != null);
    Assert.expect(packageShortName != null);
    Assert.expect(packageLongName != null);
    Assert.expect(landPatternId > 0);
    Assert.expect(checkSum > 0);

    int packageId = 0;

    // Check if package exists
    packageId = getPackageID(packageLongNameToPackageIdMap,
                             packageLongNameToLandPatternIdMap,
                             selectPackagePreparedStatement,
                             packageShortName,
                             packageLongName,
                             landPatternId);
    if (packageId == 0)
    {
      // Create package
      packageId = createPackage(createPackagePreparedStatement,
                                packageShortName,
                                packageLongName,
                                landPatternId,
                                checkSum,
                                UserAccountsManager.getCurrentUserLoginName());

      packageLongNameToPackageIdMap.put(packageLongName, packageId);
      packageLongNameToLandPatternIdMap.put(packageLongName, landPatternId);
    }
    Assert.expect(packageId > 0);

    return packageId;
  }

  /**
   * @param createLandPatternPreparedStatement PreparedStatement
   * @param selectLandPatternPreparedStatement PreparedStatement
   * @param landPatternNameToLandPatternIdMap Map
   * @param landPatternName String
   * @param numberOfLandPatternPads int
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternIdCreateIfNotExist(PreparedStatement createLandPatternPreparedStatement,
                                              PreparedStatement selectLandPatternPreparedStatement,
                                              Map<String,Integer> landPatternNameToLandPatternIdMap,
                                              String landPatternName,
                                              int numberOfLandPatternPads,
                                              int landPatternWidth,
                                              int landPatternLength) throws DatastoreException
  {
    Assert.expect(createLandPatternPreparedStatement != null);
    Assert.expect(selectLandPatternPreparedStatement != null);
    Assert.expect(landPatternNameToLandPatternIdMap != null);
    Assert.expect(landPatternName != null);
    Assert.expect(numberOfLandPatternPads > 0);

    int landPatternId = 0;

    // Check if land pattern exists
    landPatternId = getLandPatternId(landPatternNameToLandPatternIdMap,
                                     selectLandPatternPreparedStatement,
                                     landPatternName,
                                     numberOfLandPatternPads);

    if (landPatternId == 0)
    {
      // Create land pattern
      landPatternId = createLandPattern(landPatternNameToLandPatternIdMap,
                                        createLandPatternPreparedStatement,
                                        landPatternName,
                                        numberOfLandPatternPads,
                                        landPatternWidth,
                                        landPatternLength,
                                        UserAccountsManager.getCurrentUserLoginName());

      landPatternNameToLandPatternIdMap.put(landPatternName, landPatternId);
    }

    return landPatternId;
  }

  /**
   * @param createProjectPreparedStatement PreparedStatement
   * @param selectProjectPreparedStatement PreparedStatement
   * @param projectNameToProjectIdMap Map
   * @param projectName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getProjectIdCreateIfNotExist(PreparedStatement createProjectPreparedStatement,
                                          PreparedStatement selectProjectPreparedStatement,
                                          Map<String,Integer> projectNameToProjectIdMap,
                                          String projectName) throws DatastoreException
  {
    Assert.expect(createProjectPreparedStatement != null);
    Assert.expect(selectProjectPreparedStatement != null);
    Assert.expect(projectNameToProjectIdMap != null);
    Assert.expect(projectName != null);

    int projectId = 0;

    // Check if project exists
    projectId = getProjectID(projectNameToProjectIdMap,
                             selectProjectPreparedStatement,
                             projectName);
    if (projectId == 0)
    {
      // Create project
      projectId = createProject(createProjectPreparedStatement,
                                projectName,
                                Directory.getProjectsDir(),
                                UserAccountsManager.getCurrentUserLoginName());

      projectNameToProjectIdMap.put(projectName, projectId);
    }
    Assert.expect(projectId > 0);

    return projectId;
  }

  /**
   * @param createRefJointTypePreparedStatement PreparedStatement
   * @param selectRefJointTypePreparedStatement PreparedStatement
   * @param jointTypeNameToRefJointTypeIdMap Map
   * @param jointTypeName String
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefJointTypeIdCreateIfNotExist(PreparedStatement createRefJointTypePreparedStatement,
                                               PreparedStatement selectRefJointTypePreparedStatement,
                                               Map<String,Integer> jointTypeNameToRefJointTypeIdMap,
                                               String jointTypeName) throws DatastoreException
  {
    Assert.expect(createRefJointTypePreparedStatement != null);
    Assert.expect(selectRefJointTypePreparedStatement != null);
    Assert.expect(jointTypeNameToRefJointTypeIdMap != null);
    Assert.expect(jointTypeName != null);

    int refJoinTypeId = 0;

    // Check if ref joint type exists
    refJoinTypeId = getRefJointTypeID(jointTypeNameToRefJointTypeIdMap,
                                      selectRefJointTypePreparedStatement,
                                      jointTypeName);
    if (refJoinTypeId == 0)
    {
      // Create ref joint type
      refJoinTypeId = createRefJointType(createRefJointTypePreparedStatement, jointTypeName);

      jointTypeNameToRefJointTypeIdMap.put(jointTypeName, refJoinTypeId);
    }
    Assert.expect(refJoinTypeId > 0);

    return refJoinTypeId;
  }

  /**
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefSubtypeIdCreateIfNotExist(PreparedStatement createRefSubtypePreparedStatement,
                                             PreparedStatement selectRefSubtypePreparedStatement,
                                             Map<String,Integer> subtypeLongNameToRefSubtypeIdMap,
                                             String subtypeShortName,
                                             String subtypeLongName,
                                             String jointTypeName,
                                             String subtypeComment,                                             
                                             int refJointTypeId,                                             
                                             BooleanRef refSubtypeIdExistInDatabase) throws DatastoreException
  {
    Assert.expect(createRefSubtypePreparedStatement != null);
    Assert.expect(selectRefSubtypePreparedStatement != null);
    Assert.expect(subtypeLongNameToRefSubtypeIdMap != null);
    Assert.expect(subtypeShortName != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(jointTypeName != null);    
    Assert.expect(refJointTypeId > 0);    
    Assert.expect(refSubtypeIdExistInDatabase != null);

    int refSubtypeId = 0;
    refSubtypeIdExistInDatabase.setValue(true);

    // Check if ref subtype exists
    refSubtypeId = getRefSubtypeId(subtypeLongNameToRefSubtypeIdMap,
                                   selectRefSubtypePreparedStatement,
                                   subtypeShortName,
                                   subtypeLongName,
                                   jointTypeName);
    if (refSubtypeId == 0)
    {
      refSubtypeIdExistInDatabase.setValue(false);

      // Create ref subtype
      refSubtypeId = createRefSubtype(createRefSubtypePreparedStatement,
                                      subtypeShortName,
                                      subtypeComment,
                                      refJointTypeId);

      subtypeLongNameToRefSubtypeIdMap.put(subtypeLongName, refSubtypeId);
    }
    Assert.expect(refSubtypeId > 0);

    return refSubtypeId;
  }

  /**
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefAlgorithmNameIdCreateIfNotExist(PreparedStatement createRefAlgorithmNamePreparedStatement,
                                                   PreparedStatement selectRefAlgorithmNamePreparedStatement,
                                                   Map<String, Integer> algorithmNameToRefAlgorithmNameIdMap,
                                                   String algorithmName) throws DatastoreException
  {
    Assert.expect(createRefAlgorithmNamePreparedStatement != null);
    Assert.expect(selectRefAlgorithmNamePreparedStatement != null);
    Assert.expect(algorithmNameToRefAlgorithmNameIdMap != null);
    Assert.expect(algorithmName != null);

    int refAlgorithmNameId = 0;

    // Check if ref algorithm name exists
    refAlgorithmNameId = getRefAlgorithmNameID(algorithmNameToRefAlgorithmNameIdMap,
                                               selectRefAlgorithmNamePreparedStatement,
                                               algorithmName);

    if (refAlgorithmNameId == 0)
    {
      // Create ref algorithm name
      refAlgorithmNameId = createRefAlgorithmName(createRefAlgorithmNamePreparedStatement, algorithmName);

      algorithmNameToRefAlgorithmNameIdMap.put(algorithmName, refAlgorithmNameId);
    }
    Assert.expect(refAlgorithmNameId > 0);

    return refAlgorithmNameId;
  }

  /**
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefThresholdIdCreateIfNotExist(PreparedStatement createRefThresholdPreparedStatement,
                                               PreparedStatement selectRefThresholdPreparedStatement,
                                               Map<String, Integer> thresholdNameToRefThresholdIdMap,
                                               String thresholdName,
                                               int refAlgorithmNameId) throws DatastoreException
  {
    Assert.expect(createRefThresholdPreparedStatement != null);
    Assert.expect(selectRefThresholdPreparedStatement != null);
    Assert.expect(thresholdNameToRefThresholdIdMap != null);
    Assert.expect(thresholdName != null);
    Assert.expect(refAlgorithmNameId > 0);

    int refThresholdId = 0;

    // Check if ref threshold exists
    refThresholdId = getRefThresholdID(thresholdNameToRefThresholdIdMap,
                                       selectRefThresholdPreparedStatement,
                                       thresholdName);

    if (refThresholdId == 0)
    {
      // Create ref threshold
      refThresholdId = createRefThreshold(createRefThresholdPreparedStatement,
                                          refAlgorithmNameId,
                                          thresholdName);

      thresholdNameToRefThresholdIdMap.put(thresholdName, refThresholdId);
    }
    Assert.expect(refThresholdId > 0);

    return refThresholdId;
  }

  /**
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefDataTypeIdCreateIfNotExist(PreparedStatement createRefDataTypePreparedStatement,
                                              PreparedStatement selectRefDataTypePreparedStatement,
                                              Map<String, Integer> dataTypeNameToRefDataTypeIdMap,
                                              String dataTypeName) throws DatastoreException
  {
    Assert.expect(createRefDataTypePreparedStatement != null);
    Assert.expect(selectRefDataTypePreparedStatement != null);
    Assert.expect(dataTypeNameToRefDataTypeIdMap != null);
    Assert.expect(dataTypeName != null);

    int refDataTypeId = 0;

    // Check if ref threshold exists
    refDataTypeId = getRefDataTypeID(dataTypeNameToRefDataTypeIdMap,
                                       selectRefDataTypePreparedStatement,
                                       dataTypeName);

    if (refDataTypeId == 0)
    {
      // Create ref data type
      refDataTypeId = createRefDataType(createRefDataTypePreparedStatement, dataTypeName);

      dataTypeNameToRefDataTypeIdMap.put(dataTypeName, refDataTypeId);
    }
    Assert.expect(refDataTypeId > 0);

    return refDataTypeId;
  }

  /**
   * @return int
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public int getRefShapeIdCreateIfNotExist(PreparedStatement createRefShapePreparedStatement,
                                           PreparedStatement selectRefShapePreparedStatement,
                                           Map<String, Integer> shapeNameToRefShapeIdMap,
                                           String shapeName) throws DatastoreException
  {
    Assert.expect(createRefShapePreparedStatement != null);
    Assert.expect(selectRefShapePreparedStatement != null);
    Assert.expect(shapeNameToRefShapeIdMap != null);
    Assert.expect(shapeName != null);

    int refShapeId = 0;

    // Check if ref shape exists
    refShapeId = getRefShapeID(shapeNameToRefShapeIdMap,
                               selectRefShapePreparedStatement,
                               shapeName);
    if (refShapeId == 0)
    {
      // Create ref shape
      refShapeId = createRefShape(createRefShapePreparedStatement, shapeName);

      shapeNameToRefShapeIdMap.put(shapeName, refShapeId);
    }
    Assert.expect(refShapeId > 0);

    return refShapeId;
  }
  
  /**
   * @param createRefUserGainPreparedStatement
   * @param selectRefUserGainPreparedStatement
   * @param userGainNameToRefUserGainIdMap
   * @param userGainName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefUserGainIdCreateIfNotExist(PreparedStatement createRefUserGainPreparedStatement,
                                              PreparedStatement selectRefUserGainPreparedStatement,
                                              Map<String, Integer> userGainNameToRefUserGainIdMap,
                                              String userGainName) throws DatastoreException
  {
    Assert.expect(createRefUserGainPreparedStatement != null);
    Assert.expect(selectRefUserGainPreparedStatement != null);
    Assert.expect(userGainNameToRefUserGainIdMap != null);
    Assert.expect(userGainName != null);
    
    int refUserGainId = 0;
    
    // Check if ref magnification exists
    refUserGainId = getRefUserGainID(userGainNameToRefUserGainIdMap,
                                     selectRefUserGainPreparedStatement,
                                     userGainName);
    
    if (refUserGainId == 0)
    {
      // Create ref user gain
      refUserGainId = createRefMagnification(createRefUserGainPreparedStatement, userGainName);
      
      userGainNameToRefUserGainIdMap.put(userGainName, refUserGainId);
    }
    Assert.expect(refUserGainId > 0);
    
    return refUserGainId;
  }
  
  /**
   * @param createRefIntegrationLevelPreparedStatement
   * @param selectRefIntegrationLevelPreparedStatement
   * @param integrationLevelNameToRefIntegrationLevelIdMap
   * @param integrationLevelName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefIntegrationLevelIdCreateIfNotExist(PreparedStatement createRefIntegrationLevelPreparedStatement,
                                                      PreparedStatement selectRefIntegrationLevelPreparedStatement,
                                                      Map<String, Integer> integrationLevelNameToRefIntegrationLevelIdMap,
                                                      String integrationLevelName) throws DatastoreException
  {
    Assert.expect(createRefIntegrationLevelPreparedStatement != null);
    Assert.expect(selectRefIntegrationLevelPreparedStatement != null);
    Assert.expect(integrationLevelNameToRefIntegrationLevelIdMap != null);
    Assert.expect(integrationLevelName != null);
    
    int refIntegrationLevelId = 0;
    
    // Check if ref integration level exists
    refIntegrationLevelId = getRefIntegrationLevelID(integrationLevelNameToRefIntegrationLevelIdMap,
                                                     selectRefIntegrationLevelPreparedStatement,
                                                     integrationLevelName);
    
    if (refIntegrationLevelId == 0)
    {
      // Create ref integration level
      refIntegrationLevelId = createRefIntegrationLevel(createRefIntegrationLevelPreparedStatement, integrationLevelName);
      
      integrationLevelNameToRefIntegrationLevelIdMap.put(integrationLevelName, refIntegrationLevelId);
    }
    Assert.expect(refIntegrationLevelId > 0);
    
    return refIntegrationLevelId;
  }
  
  /**   
   * @param createRefArtifactCompensationPreparedStatement
   * @param selectRefArtifactCompensationPreparedStatement
   * @param artifactCompensationNameToRefArtifactCompensationIdMap
   * @param artifactCompensationName
   * @return
   * @throws DatastoreException 
   * @author Cheah Lee Herng
   */
  public int getRefArtifactCompensationIdCreateIfNotExist(PreparedStatement createRefArtifactCompensationPreparedStatement,
                                                          PreparedStatement selectRefArtifactCompensationPreparedStatement,
                                                          Map<String, Integer> artifactCompensationNameToRefArtifactCompensationIdMap,
                                                          String artifactCompensationName) throws DatastoreException
  {
    Assert.expect(createRefArtifactCompensationPreparedStatement != null);
    Assert.expect(selectRefArtifactCompensationPreparedStatement != null);
    Assert.expect(artifactCompensationNameToRefArtifactCompensationIdMap != null);
    Assert.expect(artifactCompensationName != null);
    
    int refArtifactCompensationId = 0;
    
    // Check if ref artifact compensation level exists
    refArtifactCompensationId = getRefArtifactCompensationID(artifactCompensationNameToRefArtifactCompensationIdMap,
                                                             selectRefArtifactCompensationPreparedStatement,
                                                             artifactCompensationName);
    
    if (refArtifactCompensationId == 0)
    {
      // Create ref artifact compensation
      refArtifactCompensationId = createRefArtifactCompensation(createRefArtifactCompensationPreparedStatement, artifactCompensationName);
      
      artifactCompensationNameToRefArtifactCompensationIdMap.put(artifactCompensationName, refArtifactCompensationId);
    }
    Assert.expect(refArtifactCompensationId > 0);
    
    return refArtifactCompensationId;
  }
  
  /**
   * @param createRefMagnificationPreparedStatement
   * @param selectRefMagnificationPreparedStatement
   * @param magnificationNameToRefMagnificationIdMap
   * @param magnificationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefMagnificationIdCreateIfNotExist(PreparedStatement createRefMagnificationPreparedStatement,
                                                   PreparedStatement selectRefMagnificationPreparedStatement,
                                                   Map<String, Integer> magnificationNameToRefMagnificationIdMap,
                                                   String magnificationName) throws DatastoreException
  {
    Assert.expect(createRefMagnificationPreparedStatement != null);
    Assert.expect(selectRefMagnificationPreparedStatement != null);
    Assert.expect(magnificationNameToRefMagnificationIdMap != null);
    Assert.expect(magnificationName != null);
    
    int refMagnificationId = 0;
    
    // Check if ref magnification exists
    refMagnificationId = getRefMagnificationID(magnificationNameToRefMagnificationIdMap,
                                               selectRefMagnificationPreparedStatement,
                                               magnificationName);
    if (refMagnificationId == 0)
    {
      // Create ref magnification
      refMagnificationId = createRefMagnification(createRefMagnificationPreparedStatement, magnificationName);
      
      magnificationNameToRefMagnificationIdMap.put(magnificationName, refMagnificationId);
    }
    Assert.expect(refMagnificationId > 0);
    
    return refMagnificationId;
  }
  
  /**
   * @param createRefDynamicRangeOptimizationPreparedStatement
   * @param selectRefDynamicRangeOptimizationPreparedStatement
   * @param dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap
   * @param dynamicRangeOptimizationName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefDynamicRangeOptimizationIdCreateIfNotExist(PreparedStatement createRefDynamicRangeOptimizationPreparedStatement,
                                                              PreparedStatement selectRefDynamicRangeOptimizationPreparedStatement,
                                                              Map<String, Integer> dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap,
                                                              String dynamicRangeOptimizationName) throws DatastoreException
  {
    Assert.expect(createRefDynamicRangeOptimizationPreparedStatement != null);
    Assert.expect(selectRefDynamicRangeOptimizationPreparedStatement != null);
    Assert.expect(dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap != null);
    Assert.expect(dynamicRangeOptimizationName != null);
    
    int refDynamicRangeOptimizationId = 0;
    
    // Check if ref dynamic Range Optimization exists
    refDynamicRangeOptimizationId = getRefDynamicRangeOptimizationID(dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap,
                                                                     selectRefDynamicRangeOptimizationPreparedStatement,
                                                                     dynamicRangeOptimizationName);
    if (refDynamicRangeOptimizationId == 0)
    {
      // Create ref dynamic range optimization
      refDynamicRangeOptimizationId = createRefDynamicRangeOptimization(createRefDynamicRangeOptimizationPreparedStatement, dynamicRangeOptimizationName);
      
      dynamicRangeOptimizationNameToRefDynamicRangeOptimizationIdMap.put(dynamicRangeOptimizationName, refDynamicRangeOptimizationId);
    }
    Assert.expect(refDynamicRangeOptimizationId > 0);
    
    return refDynamicRangeOptimizationId;
  }
  
  /**
   * @param createRefFocusMethodPreparedStatement
   * @param selectRefFocusMethodPreparedStatement
   * @param focusMethodNameToRefFocusMethodIdMap
   * @param focusMethodName
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefFocusMethodIdCreateIfNotExist(PreparedStatement createRefFocusMethodPreparedStatement,
                                                 PreparedStatement selectRefFocusMethodPreparedStatement,
                                                 Map<String, Integer> focusMethodNameToRefFocusMethodIdMap,
                                                 String focusMethodName) throws DatastoreException
  {
    Assert.expect(createRefFocusMethodPreparedStatement != null);
    Assert.expect(selectRefFocusMethodPreparedStatement != null);
    Assert.expect(focusMethodNameToRefFocusMethodIdMap != null);
    Assert.expect(focusMethodName != null);
    
    int refFocusMethodId = 0;
    
    // Check if ref focus method exists
    refFocusMethodId = getRefFocusMethodID(focusMethodNameToRefFocusMethodIdMap,
                                           selectRefFocusMethodPreparedStatement,
                                           focusMethodName);
    if (refFocusMethodId == 0)
    {
      // Create ref dynamic range optimization
      refFocusMethodId = createRefFocusMethod(createRefFocusMethodPreparedStatement, focusMethodName);

      focusMethodNameToRefFocusMethodIdMap.put(focusMethodName, refFocusMethodId);
    }
    Assert.expect(refFocusMethodId > 0);
    
    return refFocusMethodId;
  }
  
  /**
   * @param createRefComponentPreparedStatement
   * @param selectRefComponentPreparedStatement
   * @param componentRefDesToRefComponentIdMap
   * @param componentRefDes
   * @return int
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  public int getRefComponentIdCreateIfNotExist(PreparedStatement createRefComponentPreparedStatement,
                                               PreparedStatement selectRefComponentPreparedStatement,
                                               Map<String, Integer> componentRefDesToRefComponentIdMap,
                                               String componentRefDes) throws DatastoreException
  {
    Assert.expect(createRefComponentPreparedStatement != null);
    Assert.expect(selectRefComponentPreparedStatement != null);
    Assert.expect(componentRefDesToRefComponentIdMap != null);
    Assert.expect(componentRefDes != null);
    
    int refComponentId = 0;
    
    // Check if ref magnification exists
    refComponentId = getRefComponentId(componentRefDesToRefComponentIdMap,
                                       selectRefComponentPreparedStatement,
                                       componentRefDes);
    if (refComponentId == 0)
    {
      // Create ref component
      refComponentId = createRefComponent(createRefComponentPreparedStatement, componentRefDes);
      
      componentRefDesToRefComponentIdMap.put(componentRefDes, refComponentId);
    }
    Assert.expect(refComponentId > 0);
    
    return refComponentId;
  }

  /**
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void createProjectToSubtypeIfNotExist(PreparedStatement createProjectToSubtypePreparedStatement,
                                               PreparedStatement selectProjectToSubtypePreparedStatement,
                                               int projectId,
                                               int refSubtypeId,
                                               int userGainId,
                                               int integrationLevelId,
                                               int artifactCompensationId,
                                               int magnificationId,
                                               int dynamicRangeOptimizationId) throws DatastoreException
  {
    Assert.expect(createProjectToSubtypePreparedStatement != null);
    Assert.expect(selectProjectToSubtypePreparedStatement != null);
    Assert.expect(projectId > 0);
    Assert.expect(refSubtypeId > 0);
    Assert.expect(userGainId > 0);
    Assert.expect(integrationLevelId > 0);
    Assert.expect(artifactCompensationId > 0);
    Assert.expect(magnificationId > 0);
    Assert.expect(dynamicRangeOptimizationId > 0);

    boolean isProjectToSubtypeExist = false;
    ResultSet resultSet = null;
    try
    {
      RelationalDatabaseUtil.setInt(selectProjectToSubtypePreparedStatement, 1, projectId);
      RelationalDatabaseUtil.setInt(selectProjectToSubtypePreparedStatement, 2, refSubtypeId);      
      resultSet = RelationalDatabaseUtil.executeQuery(selectProjectToSubtypePreparedStatement);

      if (resultSet.next())
      {
        isProjectToSubtypeExist = true;
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    if (isProjectToSubtypeExist == false)
    {
      // Create project to subtype
      createProjectToSubtype(createProjectToSubtypePreparedStatement,
                             projectId,
                             refSubtypeId,
                             userGainId,
                             integrationLevelId,
                             artifactCompensationId,
                             magnificationId,
                             dynamicRangeOptimizationId,
                             UserAccountsManager.getCurrentUserLoginName());
    }
  }
  
  /**   
   * @param selectSubtypeToComponentPreparedStatement
   * @param projectId
   * @param refSubtypeId
   * @param refComponentId
   * @return TRUE  if SubtypeToComponent exists
   *         FALSE if SubtypeToComponent does not exists
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public boolean isSubtypeToComponentExist(PreparedStatement selectSubtypeToComponentPreparedStatement,
                                           int projectId,
                                           int refSubtypeId,
                                           int refComponentId) throws DatastoreException
  {
    Assert.expect(selectSubtypeToComponentPreparedStatement != null);
    Assert.expect(projectId > 0);
    Assert.expect(refSubtypeId > 0);
    Assert.expect(refComponentId > 0);
    
    boolean isSubtypeToComponentExist = false;
    ResultSet resultSet = null;
    try
    {
      RelationalDatabaseUtil.setInt(selectSubtypeToComponentPreparedStatement, 1, projectId);
      RelationalDatabaseUtil.setInt(selectSubtypeToComponentPreparedStatement, 2, refSubtypeId);
      RelationalDatabaseUtil.setInt(selectSubtypeToComponentPreparedStatement, 3, refComponentId);      
      resultSet = RelationalDatabaseUtil.executeQuery(selectSubtypeToComponentPreparedStatement);

      if (resultSet.next())
      {
        isSubtypeToComponentExist = true;
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    return isSubtypeToComponentExist;
  }
  
  /**
   * This method creates new SubtypeToAlgorithm record in database
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void createSubtypeToAlgorithmIfNotExist(PreparedStatement createSubtypeToAlgorithmPreparedStatement,
                                                 PreparedStatement selectSubtypeToAlgorithmPreparedStatement,
                                                 int projectId,
                                                 int refSubtypeId,
                                                 int refAlgorithmNameId,
                                                 boolean isAlgorithmEnable) throws DatastoreException
  {
    Assert.expect(createSubtypeToAlgorithmPreparedStatement != null);
    Assert.expect(selectSubtypeToAlgorithmPreparedStatement != null);
    Assert.expect(projectId > 0);
    Assert.expect(refSubtypeId > 0);
    Assert.expect(refAlgorithmNameId > 0);
    
    boolean isSubtypeToAlgorithmExist = false;
    ResultSet resultSet = null;
    try
    {
      RelationalDatabaseUtil.setInt(selectSubtypeToAlgorithmPreparedStatement, 1, projectId);
      RelationalDatabaseUtil.setInt(selectSubtypeToAlgorithmPreparedStatement, 2, refSubtypeId);
      RelationalDatabaseUtil.setInt(selectSubtypeToAlgorithmPreparedStatement, 3, refAlgorithmNameId);
      resultSet = RelationalDatabaseUtil.executeQuery(selectSubtypeToAlgorithmPreparedStatement);

      if (resultSet.next())
      {
        isSubtypeToAlgorithmExist = true;
      }
      resultSet.close();
      resultSet = null;
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
    
    if (isSubtypeToAlgorithmExist == false)
    {
      // Create subtype to algorithm
      createSubtypeToAlgorithm(createSubtypeToAlgorithmPreparedStatement,
                               projectId,
                               refSubtypeId,
                               refAlgorithmNameId,
                               isAlgorithmEnable,
                               UserAccountsManager.getCurrentUserLoginName());
    }
  }
  
  /**
   * This method creates new algorithm record in database
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void createAlgorithm(PreparedStatement createRefAlgorithmNamePreparedStatement,
                              PreparedStatement selectRefAlgorithmNamePrepardStatement,
                              PreparedStatement createSubtypeToAlgorithmPreparedStatement,
                              PreparedStatement selectSubtypeToAlgorithmPrepardStatement,
                              Map<String,Integer> algorithmNameToRefAlgorithmNameIdMap,
                              int projectId,
                              Subtype subtype,
                              int refSubtypeId) throws DatastoreException
  {
    Assert.expect(createRefAlgorithmNamePreparedStatement != null);
    Assert.expect(selectRefAlgorithmNamePrepardStatement != null);
    Assert.expect(createSubtypeToAlgorithmPreparedStatement != null);
    Assert.expect(selectRefAlgorithmNamePrepardStatement != null);
    Assert.expect(algorithmNameToRefAlgorithmNameIdMap != null);
    Assert.expect(projectId > 0);
    Assert.expect(subtype != null);
    Assert.expect(refSubtypeId > 0);
    
    int refAlgorithmNameId = 0;
    
    List<AlgorithmEnum> algorithmEnums = subtype.getAlgorithmEnums();
    for(AlgorithmEnum algorithmEnum : algorithmEnums)
    {
      // Get ref algorihtm name Id
      refAlgorithmNameId = getRefAlgorithmNameIdCreateIfNotExist(createRefAlgorithmNamePreparedStatement,
                                                                 selectRefAlgorithmNamePrepardStatement,
                                                                 algorithmNameToRefAlgorithmNameIdMap,
                                                                 algorithmEnum.getName());
      
      // Create subtype to algorithm if not exists
      createSubtypeToAlgorithmIfNotExist(createSubtypeToAlgorithmPreparedStatement,
                                         selectSubtypeToAlgorithmPrepardStatement,
                                         projectId,
                                         refSubtypeId,
                                         refAlgorithmNameId,
                                         subtype.isAlgorithmEnabled(algorithmEnum));
    }
  }

  /**
   * This method creates new threshold record in database
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void createThreshold(PreparedStatement createRefAlgorithmNamePreparedStatement,
                              PreparedStatement selectRefAlgorithmNamePrepardStatement,
                              PreparedStatement createRefThresholdPreparedStatement,
                              PreparedStatement selectRefThresholdPrepardStatement,
                              PreparedStatement createRefDataTypePreparedStatement,
                              PreparedStatement selectRefDataTypePreparedStatement,
                              PreparedStatement createSubtypeToThresholdPreparedStatement,
                              Map<String,Integer> algorithmNameToRefAlgorithmNameIdMap,
                              Map<String,Integer> thresholdNameToRefThresholdIdMap,
                              Map<String,Integer> dataTypeNameToRefThresholdIdMap,
                              int projectId,
                              Subtype subtype,
                              int refSubtypeId) throws DatastoreException
  {
    Assert.expect(createRefAlgorithmNamePreparedStatement != null);
    Assert.expect(selectRefAlgorithmNamePrepardStatement != null);
    Assert.expect(createRefThresholdPreparedStatement != null);
    Assert.expect(selectRefThresholdPrepardStatement != null);
    Assert.expect(createRefDataTypePreparedStatement != null);
    Assert.expect(selectRefDataTypePreparedStatement != null);
    Assert.expect(createSubtypeToThresholdPreparedStatement != null);
    Assert.expect(algorithmNameToRefAlgorithmNameIdMap != null);
    Assert.expect(thresholdNameToRefThresholdIdMap != null);
    Assert.expect(dataTypeNameToRefThresholdIdMap != null);
    Assert.expect(projectId > 0);
    Assert.expect(subtype != null);
    Assert.expect(refSubtypeId > 0);

    int refAlgorithmNameId = 0;
    int refThresholdId = 0;
    int refDataTypeId = 0;
    String thresholdValueDataType = null;

    List<AlgorithmSettingEnum> algorithmSettingEnums = subtype.getTunedAlgorithmSettingEnums();
    for (AlgorithmSettingEnum algorithmSettingEnum : algorithmSettingEnums)
    {
      // Get algorithm enum
      AlgorithmEnum algorithmEnum = getAlgorithmForCurrentSubtype(subtype.getAlgorithms(), algorithmSettingEnum).getAlgorithmEnum();

      // Get ref algorihtm name Id
      refAlgorithmNameId = getRefAlgorithmNameIdCreateIfNotExist(createRefAlgorithmNamePreparedStatement,
                                                                 selectRefAlgorithmNamePrepardStatement,
                                                                 algorithmNameToRefAlgorithmNameIdMap,
                                                                 algorithmEnum.getName());

      // Get ref threshold Id
      refThresholdId = getRefThresholdIdCreateIfNotExist(createRefThresholdPreparedStatement,
                                                         selectRefThresholdPrepardStatement,
                                                         thresholdNameToRefThresholdIdMap,
                                                         EnumStringLookup.getInstance().getAlgorithmSettingString(algorithmSettingEnum),
                                                         refAlgorithmNameId);

      // Threshold information
      Serializable serializableValue = subtype.getAlgorithmSettingValue(algorithmSettingEnum);
      thresholdValueDataType = getTypeString(serializableValue);

      // Get ref data type Id
      refDataTypeId = getRefDataTypeIdCreateIfNotExist(createRefDataTypePreparedStatement,
                                                       selectRefDataTypePreparedStatement,
                                                       dataTypeNameToRefThresholdIdMap,
                                                       thresholdValueDataType);

      // Create new subtype to threshold
      createSubtypeToThreshold(createSubtypeToThresholdPreparedStatement,
                               projectId,
                               refSubtypeId,
                               refThresholdId,
                               getThresholdValue(serializableValue),
                               refDataTypeId,
                               1.0,
                               subtype.getAlgorithmSettingUserComment(algorithmSettingEnum),
                               UserAccountsManager.getCurrentUserLoginName());
    }
  }

  /**
   * This method updates existing threshold record in database.
   *
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void updateThreshold(PreparedStatement createRefAlgorithmNamePreparedStatement,
                              PreparedStatement selectRefAlgorithmNamePrepardStatement,
                              PreparedStatement createRefThresholdPreparedStatement,
                              PreparedStatement selectRefThresholdPrepardStatement,
                              PreparedStatement createRefDataTypePreparedStatement,
                              PreparedStatement selectRefDataTypePreparedStatement,
                              PreparedStatement updateSubtypeToThresholdPreparedStatement,
                              Map<String,Integer> algorithmNameToRefAlgorithmNameIdMap,
                              Map<String,Integer> thresholdNameToRefThresholdIdMap,
                              Map<String,Integer> dataTypeNameToRefThresholdIdMap,
                              int projectId,
                              Subtype subtype,
                              int refSubtypeId) throws DatastoreException
  {
    Assert.expect(createRefAlgorithmNamePreparedStatement != null);
    Assert.expect(selectRefAlgorithmNamePrepardStatement != null);
    Assert.expect(createRefThresholdPreparedStatement != null);
    Assert.expect(selectRefThresholdPrepardStatement != null);
    Assert.expect(createRefDataTypePreparedStatement != null);
    Assert.expect(selectRefDataTypePreparedStatement != null);
    Assert.expect(updateSubtypeToThresholdPreparedStatement != null);
    Assert.expect(algorithmNameToRefAlgorithmNameIdMap != null);
    Assert.expect(thresholdNameToRefThresholdIdMap != null);
    Assert.expect(dataTypeNameToRefThresholdIdMap != null);
    Assert.expect(projectId > 0);
    Assert.expect(subtype != null);
    Assert.expect(refSubtypeId > 0);

    int refAlgorithmNameId = 0;
    int refThresholdId = 0;
    int refDataTypeId = 0;
    String thresholdValueDataType = null;

    List<AlgorithmSettingEnum> algorithmSettingEnums = subtype.getTunedAlgorithmSettingEnums();
    for (AlgorithmSettingEnum algorithmSettingEnum : algorithmSettingEnums)
    {
      // Get algorithm enum
      AlgorithmEnum algorithmEnum = getAlgorithmForCurrentSubtype(subtype.getAlgorithms(), algorithmSettingEnum).getAlgorithmEnum();

      // Get ref algorihtm name Id
      refAlgorithmNameId = getRefAlgorithmNameIdCreateIfNotExist(createRefAlgorithmNamePreparedStatement,
                                                                 selectRefAlgorithmNamePrepardStatement,
                                                                 algorithmNameToRefAlgorithmNameIdMap,
                                                                 algorithmEnum.getName());

      // Get ref threshold Id
      refThresholdId = getRefThresholdIdCreateIfNotExist(createRefThresholdPreparedStatement,
                                                         selectRefThresholdPrepardStatement,
                                                         thresholdNameToRefThresholdIdMap,
                                                         EnumStringLookup.getInstance().getAlgorithmSettingString(algorithmSettingEnum),
                                                         refAlgorithmNameId);

      // Threshold information
      Serializable serializableValue = subtype.getAlgorithmSettingValue(algorithmSettingEnum);
      thresholdValueDataType = getTypeString(serializableValue);

      // Get ref data type Id
      refDataTypeId = getRefDataTypeIdCreateIfNotExist(createRefDataTypePreparedStatement,
                                                       selectRefDataTypePreparedStatement,
                                                       dataTypeNameToRefThresholdIdMap,
                                                       thresholdValueDataType);

      // Update subtype to threshold
      updateSubtypeToThreshold(updateSubtypeToThresholdPreparedStatement,
                               projectId,
                               refSubtypeId,
                               refThresholdId,
                               getThresholdValue(serializableValue),
                               refDataTypeId,
                               subtype.getAlgorithmSettingUserComment(algorithmSettingEnum),
                               UserAccountsManager.getCurrentUserLoginName());
    }
  }

  /**
   * @param landPatternPadIds List
   * @param usedlandPatternPadIds List
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getCorrectedLandPatternPadId(List<Integer> landPatternPadIds, List<Integer> usedlandPatternPadIds)
  {
    Assert.expect(landPatternPadIds != null);
    Assert.expect(usedlandPatternPadIds != null);

    int correctedLandPatternPadId = 0;

    if ((landPatternPadIds.size() == usedlandPatternPadIds.size()) == false)
    {
      for(Integer landPatternPadId : landPatternPadIds)
      {
        if (usedlandPatternPadIds.contains(landPatternPadId) == false)
        {
          correctedLandPatternPadId = landPatternPadId;
          usedlandPatternPadIds.add(landPatternPadId);
          break;
        }
      }
    }
    Assert.expect(correctedLandPatternPadId > 0);

    return correctedLandPatternPadId;
  }

  /**
   * @param packagePinIds List
   * @param usedPackagePinIds List
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getCorrectedPackagePinId(List<Integer> packagePinIds, List<Integer> usedPackagePinIds)
  {
    Assert.expect(packagePinIds != null);
    Assert.expect(usedPackagePinIds != null);

    int correctedPackagePinId = 0;

    if ((packagePinIds.size() == usedPackagePinIds.size()) == false)
    {
      for(Integer packagePinId : packagePinIds)
      {
        if (usedPackagePinIds.contains(packagePinId) == false)
        {
          correctedPackagePinId = packagePinId;
          usedPackagePinIds.add(packagePinId);
          break;
        }
      }
    }
    Assert.expect(correctedPackagePinId > 0);

    return correctedPackagePinId;
  }

  /**
   * @param libraryPath String
   * @param checkSum long
   * @return LibrarySubtypeScheme
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
//  public LibrarySubtypeScheme getLibrarySubtypeSchemeByProjectSubtypeScheme(String libraryPath,
//                                                                            long checkSum) throws DatastoreException
//
//  {
//    Assert.expect(libraryPath != null);
//    Assert.expect(checkSum > 0);
//
//    boolean libarrySubtypeSchemeIsNotSet = true;
//    boolean libraryPackageIsNotSet = true;
//    boolean libraryLandPatternIsNotSet = true;
//
//    PreparedStatement preparedStatement;
//    ResultSet resultSet;
//
//    LibrarySubtypeScheme librarySubtypeScheme = null;
//
//    // Startup database
//    _database.startupDatabase();
//
//    // Get JDBC connection
//    String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);
//
//    // Create new connection
//    Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
//                                                                  _database.getDatabaseUserName(),
//                                                                  _database.getDatabasePassword(),
//                                                                  PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
//
//    try
//    {
//      preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getMatchSubtypeSchemeSql());
//      RelationalDatabaseUtil.setLong(preparedStatement, 1, checkSum);
//      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
//
//      while (resultSet.next())
//      {
//        if(librarySubtypeScheme == null)
//          librarySubtypeScheme = new LibrarySubtypeScheme();
//
//        if (libarrySubtypeSchemeIsNotSet)
//        {
//          librarySubtypeScheme.setSubtypeSchemeId(resultSet.getInt(_sqlManager.SUBTYPE_SCHEME_ID_COLUMN));
//          librarySubtypeScheme.setUseOneJointType(resultSet.getBoolean(_sqlManager.USE_ONE_JOINT_TYPE_COLUMN));
//          librarySubtypeScheme.setUseOneSubtype(resultSet.getBoolean(_sqlManager.USE_ONE_SUBTYPE_COLUMN));
//          libarrySubtypeSchemeIsNotSet = false;
//          Set<LibraryPackagePin> libaryPackagePins = getLibraryPackagePinsByLibrarySubtypeSchemeId(libraryPath, librarySubtypeScheme.getSubtypeSchemeId());
//          librarySubtypeScheme.setLibraryPackagePins(libaryPackagePins);
//          Set <LibrarySubtype> librarySubtypes = new HashSet<LibrarySubtype>();
//          for (LibraryPackagePin libraryPackagePin : libaryPackagePins)
//          {
//            LibrarySubtype librarySubtype = libraryPackagePin.getLibrarySubtype();
//            librarySubtype = getLibrarySubtypeByRefSubtypeIdAndProjectName(libraryPath, librarySubtype);
//            librarySubtypes.add(librarySubtype);
//          }
//          librarySubtypeScheme.setLibrarySubtypes(new ArrayList<LibrarySubtype>(librarySubtypes));
//        }
//
//        if (libraryPackageIsNotSet)
//        {
//          LibraryPackage libraryPackage = new LibraryPackage();
//          libraryPackage.setPackageName(resultSet.getString(_sqlManager.PACKAGE_NAME_COLUMN));
//          libraryPackage.setPackageLongName(resultSet.getString(_sqlManager.PACKAGE_LONG_NAME_COLUMN));
//          libraryPackage.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
//          libraryPackageIsNotSet = false;
//          librarySubtypeScheme.setLibraryPackage(libraryPackage);
//        }
//
//        if (libraryLandPatternIsNotSet)
//        {
//          LibraryLandPattern libraryLandPattern = new LibraryLandPattern();
//          libraryLandPattern.setLandPatternId(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN));
//          libraryLandPattern.setLandPatternName(resultSet.getString(_sqlManager.LAND_PATTERN_NAME_COLUMN));
//          libraryLandPattern.setNumberOfPads(resultSet.getInt(_sqlManager.NUMBER_OF_PADS_COLUMN));
//          libraryLandPattern = getLibraryLandPatternPads(libraryPath, libraryLandPattern);
//          librarySubtypeScheme.getLibraryPackage().setLibraryLandPattern(libraryLandPattern);
//          libraryLandPatternIsNotSet = false;
//        }
//      }
//      resultSet.close();
//      resultSet = null;
//    }
//    catch (SQLException sqlEx)
//    {
//      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
//      dex.initCause(sqlEx);
//      throw dex;
//    }
//    return librarySubtypeScheme;
//  }

  /**
   * @param connection Connection
   * @param landPatternIdToLandPatternPadIdsMap Map
   *
   * @author Cheah Lee Herng
   */
  public void populateLandPatternIdToLandPatternPadIdsMapFromDatabase(Connection connection,
                                                                      Map<Integer,List<Integer>> landPatternIdToLandPatternPadIdsMap) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(landPatternIdToLandPatternPadIdsMap != null);

    PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getLandPatternAndLandPatternPadSqlString());
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    try
    {
      while(resultSet.next())
      {
        if (landPatternIdToLandPatternPadIdsMap.containsKey(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN)) == false)
        {
          List<Integer> landPatternPads = new ArrayList<Integer>();
          landPatternPads.add(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));

          landPatternIdToLandPatternPadIdsMap.put(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN), landPatternPads);
        }
        else
        {
          landPatternIdToLandPatternPadIdsMap.get(resultSet.getInt(_sqlManager.LAND_PATTERN_ID_COLUMN)).add(resultSet.getInt(_sqlManager.LAND_PATTERN_PAD_ID_COLUMN));
        }
      }
      resultSet.close();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param connection Connection
   * @param packageIdToPackagePinIdsMap List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void populatePackageIdToPackagePinsMap(Connection connection,
                                                Map<Integer,List<Integer>> packageIdToPackagePinIdsMap) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(packageIdToPackagePinIdsMap != null);

    PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getPackageAndPackagePinSqlString());
    ResultSet resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

    try
    {
      while(resultSet.next())
      {
        if (packageIdToPackagePinIdsMap.containsKey(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN)) == false)
        {
          List<Integer> packagePinPads = new ArrayList<Integer>();
          packagePinPads.add(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));

          packageIdToPackagePinIdsMap.put(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN), packagePinPads);
        }
        else
        {
          packageIdToPackagePinIdsMap.get(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN)).add(resultSet.getInt(_sqlManager.PACKAGE_PIN_ID_COLUMN));
        }
      }
      resultSet.close();
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }

  /**
   * @param connection Connection
   * @param subtypeScheme SubtypeScheme
   * @return LibrarySubtypeScheme
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public LibrarySubtypeScheme getLibrarySubtypeSchemeBySubtypeCheckSum(Connection connection,
                                                                       SubtypeScheme subtypeScheme) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(subtypeScheme != null);

    PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getMatchedLibrarySubtypeSchemeExistSqlString());
    ResultSet resultSet;
    LibrarySubtypeScheme librarySubtypeScheme = new LibrarySubtypeScheme();

    try
    {
      RelationalDatabaseUtil.setLong(preparedStatement, 1, subtypeScheme.getCheckSum());
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      if (resultSet.next())
      {
        librarySubtypeScheme.setSubtypeSchemeId(resultSet.getInt(_sqlManager.SUBTYPE_SCHEME_ID_COLUMN));
        librarySubtypeScheme.setPackageId(resultSet.getInt(_sqlManager.PACKAGE_ID_COLUMN));
        librarySubtypeScheme.setUseOneSubtype(resultSet.getBoolean(_sqlManager.USE_ONE_SUBTYPE_COLUMN));
        librarySubtypeScheme.setUseOneJointType(resultSet.getBoolean(_sqlManager.USE_ONE_JOINT_TYPE_COLUMN));
      }
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }

    return librarySubtypeScheme;
  }

  /**
   * @param connection Connection
   * @param preparedStatement PreparedStatement
   * @param subtypeScheme SubtypeScheme
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void updatePackagePin(Connection connection, PreparedStatement preparedStatement, SubtypeScheme subtypeScheme) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeScheme != null);

    int counter = 0;
    int refJointTypeId = 0;
    LibrarySubtypeScheme librarySubtypeScheme = getLibrarySubtypeSchemeBySubtypeCheckSum(connection, subtypeScheme);
    Set<LibraryPackagePin> libraryPackagePinSet = getLibraryPackagePinsByLibrarySubtypeSchemeId(connection,
                                                                                                librarySubtypeScheme.getSubtypeSchemeId());
    PreparedStatement refJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefJointTypeSqlString(), Statement.RETURN_GENERATED_KEYS);
    PreparedStatement selectRefJointTypePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefJointTypeByJointTypeNameSqlString());
    Map<String, Integer> jointTypeNameToRefJointTypeIdMap = new HashMap<String,Integer>();
    List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(libraryPackagePinSet);
    List<PadType> padTypes = subtypeScheme.getPadTypes();
    if (padTypes.size() == libraryPackagePinSet.size())
    {
      for (PadType padType : padTypes)
      {
        // Get ref joint type Id for package pin
        refJointTypeId = getRefJointTypeIdCreateIfNotExist(refJointTypePreparedStatement,
                                                           selectRefJointTypePreparedStatement,
                                                           jointTypeNameToRefJointTypeIdMap,
                                                           padType.getPackagePin().getJointTypeEnum().getName());

        // Update library package pin
        updatePackagePin(preparedStatement,
                         refJointTypeId,
                         padType.getPinOrientationEnum().getDegrees(),
                         subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                         subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                         libraryPackagePins.get(counter).getPackagePinId(),
                         UserAccountsManager.getCurrentUserLoginName());

        counter++;
      }
    }
  }

  /**
   * @param connection Connection
   * @param preparedStatement PreparedStatement
   * @param subtypeScheme SubtypeScheme
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  public void updateLandPatternPad(Connection connection, PreparedStatement preparedStatement, SubtypeScheme subtypeScheme) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(preparedStatement != null);
    Assert.expect(subtypeScheme != null);

    int counter = 0;
    int refShapeId = 0;
    int holeDiameter = 0;
    //Siew Yeng - XCR-3318
    int holeWidth = 0;
    int holeLength = 0;
    int holeXLocation = 0;
    int holeYLocation = 0;

    PreparedStatement selectRefShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getRefShapeByShapeNameSqlString());
    PreparedStatement refShapePreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefShapeSqlString(), Statement.RETURN_GENERATED_KEYS);
    Map<String, Integer> shapeNameToRefShapeIdMap = new HashMap<String,Integer>();

    LibrarySubtypeScheme librarySubtypeScheme = getLibrarySubtypeSchemeBySubtypeCheckSum(connection, subtypeScheme);
    List<LibraryLandPatternPad> libraryLandPatternPads = getLibraryLandPatternPadByLibrarySubtypeSchemeId(connection,
                                                                                                          librarySubtypeScheme.getSubtypeSchemeId());
    List<LandPatternPad> landPatternPads = subtypeScheme.getCompPackage().getLandPattern().getLandPatternPads();

    if (landPatternPads.size() == libraryLandPatternPads.size())
    {
      for(LandPatternPad landPatternPad : landPatternPads)
      {
        // Create ref shape
        refShapeId = getRefShapeIdCreateIfNotExist(refShapePreparedStatement,
                                                   selectRefShapePreparedStatement,
                                                   shapeNameToRefShapeIdMap,
                                                   landPatternPad.getShapeEnum().getName());


        if (landPatternPad instanceof ThroughHoleLandPatternPad)
        {
          ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
          ComponentCoordinate throughHoleComponentCoordinate = throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
//          holeDiameter = throughHoleLandPatternPad.getHoleDiameterInNanoMeters();
          //Siew Yeng - XCR-3318
          holeWidth = throughHoleLandPatternPad.getHoleWidthInNanoMeters();
          holeLength = throughHoleLandPatternPad.getHoleLengthInNanoMeters();
          holeDiameter = Math.min(holeWidth, holeLength); //Siew Yeng - set hole diameter for old db version(dbversion <3)
          holeXLocation = throughHoleComponentCoordinate.getX();
          holeYLocation = throughHoleComponentCoordinate.getY();
        }
        else if (landPatternPad instanceof SurfaceMountLandPatternPad)
        {
          holeDiameter = 0;
          holeWidth = 0;
          holeLength = 0;
          holeXLocation = 0;
          holeYLocation = 0;
        }

        // Update library land pattern pad
        updateLandPatternPad(preparedStatement,
                             libraryLandPatternPads.get(counter).getLandPatternPadId(),
                             landPatternPad.getName(),
                             landPatternPad.getCoordinateInNanoMeters().getX(),
                             landPatternPad.getCoordinateInNanoMeters().getY(),
                             landPatternPad.getWidthInNanoMeters(),
                             landPatternPad.getLengthInNanoMeters(),
                             landPatternPad.getDegreesRotation(),
                             refShapeId,
                             landPatternPad.isSurfaceMountPad(),
                             landPatternPad.isPadOne(),
                             holeDiameter,
                             holeXLocation,
                             holeYLocation,
                             holeWidth,
                             holeLength,
                             UserAccountsManager.getCurrentUserLoginName());

        counter++;
      }
    }

    selectRefShapePreparedStatement = null;
    refShapePreparedStatement = null;
    shapeNameToRefShapeIdMap.clear();
  }

  /**
   * @param preparedStatement PreparedStatement
   * @param packageShortName String
   * @param packageLongName String
   * @param landPatternId int
   * @param checkSum long
   * @param libraryPackageId int
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void updateLibraryPackage(PreparedStatement preparedStatement,
                                   String packageShortName,
                                   String packageLongName,
                                   int landPatternId,
                                   long checkSum,
                                   int libraryPackageId) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(packageShortName != null);
    Assert.expect(packageLongName != null);
    Assert.expect(landPatternId > 0);
    Assert.expect(libraryPackageId > 0);
    Assert.expect(checkSum > 0);

    RelationalDatabaseUtil.setString(preparedStatement, 1, packageShortName);
    RelationalDatabaseUtil.setString(preparedStatement, 2, packageLongName);
    RelationalDatabaseUtil.setInt(preparedStatement, 3, landPatternId);
    RelationalDatabaseUtil.setLong(preparedStatement, 4, checkSum);
    RelationalDatabaseUtil.setInt(preparedStatement, 5, libraryPackageId);
  }

  /**
   * @param createLandPatternPadPreparedStatement PreparedStatement
   * @param createRefShapePreparedStatement PreparedStatement
   * @param selectRefShapePreparedStatement PreparedStatement
   * @param createPackagePinPreparedStatement PreparedStatement
   * @param createRefJointTypePreparedStatement PreparedStatement
   * @param selectRefJointTypePreparedStatement PreparedStatement
   * @param jointTypeNameToRefJointTypeIdMap Map
   * @param shapeNameToRefShapeIdMap Map
   * @param subtypeScheme SubtypeScheme
   * @param landPatternId int
   * @param packageId int
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void createLandPatternPadAndPackagePin(PreparedStatement createLandPatternPadPreparedStatement,
                                                PreparedStatement createRefShapePreparedStatement,
                                                PreparedStatement selectRefShapePreparedStatement,
                                                PreparedStatement createPackagePinPreparedStatement,
                                                PreparedStatement createRefJointTypePreparedStatement,
                                                PreparedStatement selectRefJointTypePreparedStatement,
                                                Map<String, Integer> jointTypeNameToRefJointTypeIdMap,
                                                Map<String, Integer> shapeNameToRefShapeIdMap,
                                                SubtypeScheme subtypeScheme,
                                                int landPatternId,
                                                int packageId) throws DatastoreException
  {
    Assert.expect(createLandPatternPadPreparedStatement != null);
    Assert.expect(createRefShapePreparedStatement != null);
    Assert.expect(selectRefShapePreparedStatement != null);
    Assert.expect(createPackagePinPreparedStatement != null);
    Assert.expect(createRefJointTypePreparedStatement != null);
    Assert.expect(selectRefJointTypePreparedStatement != null);
    Assert.expect(jointTypeNameToRefJointTypeIdMap != null);
    Assert.expect(shapeNameToRefShapeIdMap != null);
    Assert.expect(subtypeScheme != null);
    Assert.expect(landPatternId > 0);
    Assert.expect(packageId > 0);

    int holeDiameter = 0;
    int holeWidth = 0;
    int holeLength = 0;
    int holeXLocation = 0;
    int holeYLocation = 0;
    List<PadType> padTypes = subtypeScheme.getPadTypes();
    for(PadType padType : padTypes)
    {
      LandPatternPad landPatternPad = padType.getLandPatternPad();
      PackagePin packagePin = padType.getPackagePin();

      if (landPatternPad instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
        ComponentCoordinate throughHoleComponentCoordinate = throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
//        holeDiameter = throughHoleLandPatternPad.getHoleDiameterInNanoMeters();
        //Siew Yeng - XCR-3318
        holeWidth = throughHoleLandPatternPad.getHoleWidthInNanoMeters();
        holeLength = throughHoleLandPatternPad.getHoleLengthInNanoMeters();
        holeDiameter = Math.min(holeWidth, holeLength); //Siew Yeng - keep hole diameter for old db version(dbversion <3)
        holeXLocation = throughHoleComponentCoordinate.getX();
        holeYLocation = throughHoleComponentCoordinate.getY();
      }
      else if (landPatternPad instanceof SurfaceMountLandPatternPad)
      {
        holeDiameter = 0;
        holeWidth = 0;
        holeLength = 0;
        holeXLocation = 0;
        holeYLocation = 0;
      }

      // Create ref shape
      int refShapeId = getRefShapeIdCreateIfNotExist(createRefShapePreparedStatement,
                                                     selectRefShapePreparedStatement,
                                                     shapeNameToRefShapeIdMap,
                                                     landPatternPad.getShapeEnum().getName());

      // Create land pattern pad
      int landPatternPadId = createLandPatternPad(createLandPatternPadPreparedStatement,
                                                  landPatternId,
                                                  landPatternPad.getName(),
                                                  landPatternPad.getCoordinateInNanoMeters().getX(),
                                                  landPatternPad.getCoordinateInNanoMeters().getY(),
                                                  landPatternPad.getWidthInNanoMeters(),
                                                  landPatternPad.getLengthInNanoMeters(),
                                                  landPatternPad.getDegreesRotation(),
                                                  refShapeId,
                                                  landPatternPad.isSurfaceMountPad(),
                                                  landPatternPad.isPadOne(),
                                                  holeDiameter,
                                                  holeWidth,
                                                  holeLength,
                                                  holeXLocation,
                                                  holeYLocation,
                                                  UserAccountsManager.getCurrentUserLoginName());

      // Create ref joint type
      int refJointTypeId = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                             selectRefJointTypePreparedStatement,
                                                             jointTypeNameToRefJointTypeIdMap,
                                                             packagePin.getJointTypeEnum().getName());

      // Create package pin
      createPackagePin(createPackagePinPreparedStatement,
                       landPatternPadId,
                       refJointTypeId,
                       packagePin.getPinOrientationEnum().getDegrees(),
                       packagePin.getJointHeightInNanoMeters(),
                       packageId,
                       subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                       subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                       UserAccountsManager.getCurrentUserLoginName());
    }
  }

  /**
   * @param connection Connection
   * @param createLandPatternPadPreparedStatement PreparedStatement
   * @param updateLandPatternPadPreparedStatement PreparedStatement
   * @param createRefShapePreparedStatement PreparedStatement
   * @param selectRefShapePreparedStatement PreparedStatement
   * @param createPackagePinPreparedStatement PreparedStatement
   * @param updatePackagePinPreparedStatement PreparedStatement
   * @param createRefJointTypePreparedStatement PreparedStatement
   * @param selectRefJointTypePreparedStatement PreparedStatement
   * @param jointTypeNameToRefJointTypeIdMap Map
   * @param shapeNameToRefShapeIdMap Map
   * @param subtypeScheme SubtypeScheme
   * @param landPatternId int
   * @param packageId int
   * @param createNewPackage boolean
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void createLandPatternPadAndPackagePinCreateIfNotExist(Connection connection,
                                                                PreparedStatement createLandPatternPadPreparedStatement,
                                                                PreparedStatement updateLandPatternPadPreparedStatement,
                                                                PreparedStatement createRefShapePreparedStatement,
                                                                PreparedStatement selectRefShapePreparedStatement,
                                                                PreparedStatement createPackagePinPreparedStatement,
                                                                PreparedStatement updatePackagePinPreparedStatement,
                                                                PreparedStatement createRefJointTypePreparedStatement,
                                                                PreparedStatement selectRefJointTypePreparedStatement,
                                                                Map<String, Integer> jointTypeNameToRefJointTypeIdMap,
                                                                Map<String, Integer> shapeNameToRefShapeIdMap,
                                                                SubtypeScheme subtypeScheme,
                                                                int landPatternId,
                                                                int packageId,
                                                                boolean createNewPackage) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(createLandPatternPadPreparedStatement != null);
    Assert.expect(updateLandPatternPadPreparedStatement != null);
    Assert.expect(createRefShapePreparedStatement != null);
    Assert.expect(selectRefShapePreparedStatement != null);
    Assert.expect(createPackagePinPreparedStatement != null);
    Assert.expect(updatePackagePinPreparedStatement != null);
    Assert.expect(createRefJointTypePreparedStatement != null);
    Assert.expect(selectRefJointTypePreparedStatement != null);
    Assert.expect(jointTypeNameToRefJointTypeIdMap != null);
    Assert.expect(shapeNameToRefShapeIdMap != null);
    Assert.expect(subtypeScheme != null);
    Assert.expect(landPatternId > 0);
    Assert.expect(packageId > 0);

    int holeDiameter = 0;
    int holeWidth = 0;
    int holeLength = 0;
    int holeXLocation = 0;
    int holeYLocation = 0;
    int landPatternPadId = 0;
    int counter = 0;
    List<PadType> padTypes = subtypeScheme.getPadTypes();
    LibrarySubtypeScheme librarySubtypeScheme = getLibrarySubtypeSchemeBySubtypeCheckSum(connection, subtypeScheme);
    List<LibraryLandPatternPad> libraryLandPatternPads = getLibraryLandPatternPadByLibrarySubtypeSchemeId(connection,
                                                                                                          librarySubtypeScheme.getSubtypeSchemeId());
    Set<LibraryPackagePin> libraryPackagePinSet = getLibraryPackagePinsByLibrarySubtypeSchemeId(connection,
                                                                                                librarySubtypeScheme.getSubtypeSchemeId());
    List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(libraryPackagePinSet);
    for(PadType padType : padTypes)
    {
      LandPatternPad landPatternPad = padType.getLandPatternPad();
      PackagePin packagePin = padType.getPackagePin();

      if (landPatternPad instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
        ComponentCoordinate throughHoleComponentCoordinate = throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
//        holeDiameter = throughHoleLandPatternPad.getHoleDiameterInNanoMeters();
        //Siew Yeng - XCR-3318
        holeWidth = throughHoleLandPatternPad.getHoleWidthInNanoMeters();
        holeLength = throughHoleLandPatternPad.getHoleLengthInNanoMeters();
        holeDiameter = Math.min(holeWidth, holeLength); //Siew Yeng - set hole diameter for old db version(dbversion <3)
        holeXLocation = throughHoleComponentCoordinate.getX();
        holeYLocation = throughHoleComponentCoordinate.getY();
      }
      else if (landPatternPad instanceof SurfaceMountLandPatternPad)
      {
        holeDiameter = 0;
        holeWidth = 0;
        holeLength = 0;
        holeXLocation = 0;
        holeYLocation = 0;
      }

      // Create ref shape
      int refShapeId = getRefShapeIdCreateIfNotExist(createRefShapePreparedStatement,
                                                     selectRefShapePreparedStatement,
                                                     shapeNameToRefShapeIdMap,
                                                     landPatternPad.getShapeEnum().getName());

      // Create/Update land pattern pad
      if (createNewPackage)
      {
        // Create land pattern pad
        landPatternPadId = createLandPatternPad(createLandPatternPadPreparedStatement,
                                                landPatternId,
                                                landPatternPad.getName(),
                                                landPatternPad.getCoordinateInNanoMeters().getX(),
                                                landPatternPad.getCoordinateInNanoMeters().getY(),
                                                landPatternPad.getWidthInNanoMeters(),
                                                landPatternPad.getLengthInNanoMeters(),
                                                landPatternPad.getDegreesRotation(),
                                                refShapeId,
                                                landPatternPad.isSurfaceMountPad(),
                                                landPatternPad.isPadOne(),
                                                holeDiameter,
                                                holeWidth,
                                                holeLength,
                                                holeXLocation,
                                                holeYLocation,
                                                UserAccountsManager.getCurrentUserLoginName());
      }
      else
      {
        // Update land pattern pad
        updateLandPatternPad(updateLandPatternPadPreparedStatement,
                             libraryLandPatternPads.get(counter).getLandPatternPadId(),
                             landPatternPad.getName(),
                             landPatternPad.getCoordinateInNanoMeters().getX(),
                             landPatternPad.getCoordinateInNanoMeters().getY(),
                             landPatternPad.getWidthInNanoMeters(),
                             landPatternPad.getLengthInNanoMeters(),
                             landPatternPad.getDegreesRotation(),
                             refShapeId,
                             landPatternPad.isSurfaceMountPad(),
                             landPatternPad.isPadOne(),
                             holeDiameter,
                             holeWidth,
                             holeLength,
                             holeXLocation,
                             holeYLocation,
                             UserAccountsManager.getCurrentUserLoginName());
      }

      // Create ref joint type
      int refJointTypeId = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                             selectRefJointTypePreparedStatement,
                                                             jointTypeNameToRefJointTypeIdMap,
                                                             packagePin.getJointTypeEnum().getName());

      // Create/Update package pin
      if (createNewPackage)
      {
        // Create package pin
        createPackagePin(createPackagePinPreparedStatement,
                         landPatternPadId,
                         refJointTypeId,
                         packagePin.getPinOrientationEnum().getDegrees(),
                         packagePin.getJointHeightInNanoMeters(),
                         packageId,
                         subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                         subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                         UserAccountsManager.getCurrentUserLoginName());
      }
      else
      {
        // Update package pin
        updatePackagePin(updatePackagePinPreparedStatement,
                         refJointTypeId,
                         packagePin.getPinOrientationEnum().getDegrees(),
                         subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                         subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                         libraryPackagePins.get(counter).getPackagePinId(),
                         UserAccountsManager.getCurrentUserLoginName());
      }

      counter++;
    }
  }

  /**
   * @param connection Connection
   * @param updateLandPatternPadPreparedStatement PreparedStatement
   * @param createRefShapePreparedStatement PreparedStatement
   * @param selectRefShapePreparedStatement PreparedStatement
   * @param updatePackagePinPreparedStatement PreparedStatement
   * @param createRefJointTypePreparedStatement PreparedStatement
   * @param selectRefJointTypePreparedStatement PreparedStatement
   * @param jointTypeNameToRefJointTypeIdMap Map
   * @param shapeNameToRefShapeIdMap Map
   * @param subtypeScheme SubtypeScheme
   * @param landPatternId int
   * @param packageId int
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  public void updateLandPatternPadAndPackagePin(Connection connection,
                                                PreparedStatement updateLandPatternPadPreparedStatement,
                                                PreparedStatement createRefShapePreparedStatement,
                                                PreparedStatement selectRefShapePreparedStatement,
                                                PreparedStatement updatePackagePinPreparedStatement,
                                                PreparedStatement createRefJointTypePreparedStatement,
                                                PreparedStatement selectRefJointTypePreparedStatement,
                                                Map<String, Integer> jointTypeNameToRefJointTypeIdMap,
                                                Map<String, Integer> shapeNameToRefShapeIdMap,
                                                SubtypeScheme subtypeScheme,
                                                int landPatternId,
                                                int packageId) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(updateLandPatternPadPreparedStatement != null);
    Assert.expect(createRefShapePreparedStatement != null);
    Assert.expect(selectRefShapePreparedStatement != null);
    Assert.expect(updatePackagePinPreparedStatement != null);
    Assert.expect(createRefJointTypePreparedStatement != null);
    Assert.expect(selectRefJointTypePreparedStatement != null);
    Assert.expect(jointTypeNameToRefJointTypeIdMap != null);
    Assert.expect(shapeNameToRefShapeIdMap != null);
    Assert.expect(subtypeScheme != null);
    Assert.expect(landPatternId > 0);
    Assert.expect(packageId > 0);

    int holeDiameter = 0;
    int holeWidth = 0;
    int holeLength = 0;
    int holeXLocation = 0;
    int holeYLocation = 0;
    int counter = 0;
    List<PadType> padTypes = subtypeScheme.getPadTypes();
    LibrarySubtypeScheme librarySubtypeScheme = getLibrarySubtypeSchemeBySubtypeCheckSum(connection, subtypeScheme);
    List<LibraryLandPatternPad> libraryLandPatternPads = getLibraryLandPatternPadByLibrarySubtypeSchemeId(connection,
                                                                                                          librarySubtypeScheme.getSubtypeSchemeId());
    Set<LibraryPackagePin> libraryPackagePinSet = getLibraryPackagePinsByLibrarySubtypeSchemeId(connection,
                                                                                                librarySubtypeScheme.getSubtypeSchemeId());
    List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(libraryPackagePinSet);
    for(PadType padType : padTypes)
    {
      LandPatternPad landPatternPad = padType.getLandPatternPad();
      PackagePin packagePin = padType.getPackagePin();

      if (landPatternPad instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad throughHoleLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
        ComponentCoordinate throughHoleComponentCoordinate = throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
//        holeDiameter = throughHoleLandPatternPad.getHoleDiameterInNanoMeters();
        //Siew Yeng - XCR-3318
        holeWidth = throughHoleLandPatternPad.getHoleWidthInNanoMeters();
        holeLength = throughHoleLandPatternPad.getHoleLengthInNanoMeters();
        holeDiameter = Math.min(holeWidth, holeLength); //Siew Yeng - keep hole diameter for old db version(dbversion <3)
        holeXLocation = throughHoleComponentCoordinate.getX();
        holeYLocation = throughHoleComponentCoordinate.getY();
      }
      else if (landPatternPad instanceof SurfaceMountLandPatternPad)
      {
        holeDiameter = 0;
        holeWidth = 0;
        holeLength = 0;
        holeXLocation = 0;
        holeYLocation = 0;
      }

      // Create ref shape
      int refShapeId = getRefShapeIdCreateIfNotExist(createRefShapePreparedStatement,
                                                     selectRefShapePreparedStatement,
                                                     shapeNameToRefShapeIdMap,
                                                     landPatternPad.getShapeEnum().getName());

      // Update land pattern pad
      updateLandPatternPad(updateLandPatternPadPreparedStatement,
                           libraryLandPatternPads.get(counter).getLandPatternPadId(),
                           landPatternPad.getName(),
                           landPatternPad.getCoordinateInNanoMeters().getX(),
                           landPatternPad.getCoordinateInNanoMeters().getY(),
                           landPatternPad.getWidthInNanoMeters(),
                           landPatternPad.getLengthInNanoMeters(),
                           landPatternPad.getDegreesRotation(),
                           refShapeId,
                           landPatternPad.isSurfaceMountPad(),
                           landPatternPad.isPadOne(),
                           holeDiameter,
                           holeWidth,
                           holeLength,
                           holeXLocation,
                           holeYLocation,
                           UserAccountsManager.getCurrentUserLoginName());

      // Get ref joint type
      int refJointTypeId = getRefJointTypeIdCreateIfNotExist(createRefJointTypePreparedStatement,
                                                             selectRefJointTypePreparedStatement,
                                                             jointTypeNameToRefJointTypeIdMap,
                                                             packagePin.getJointTypeEnum().getName());

      // Update package pin
      updatePackagePin(updatePackagePinPreparedStatement,
                       refJointTypeId,
                       packagePin.getPinOrientationEnum().getDegrees(),
                       subtypeScheme.getPadNameToInspectableMap().get(padType.getName()),
                       subtypeScheme.getPadNameToTestableMap().get(padType.getName()),
                       libraryPackagePins.get(counter).getPackagePinId(),
                       UserAccountsManager.getCurrentUserLoginName());

      counter++;
    }
  }
  
  /**
   * This method will update missing tables which are newly designed to handle
   * new Package Library functionality.
   * 
   * @param connection
   * @throws DatastoreException 
   */
  public void updateTableScheme(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    // Only update new table scheme
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.DB_VERSION_TABLE) == false)
    {
      createTableDBVersionIfNecessary(connection);
      preInsertDefaultData(connection);
      createTableRefComponentIfNecessary(connection);
      createTableSubtypeToComponentIfNecessary(connection);
      createTableSubtypeToAlgorithmIfNecessary(connection);
      //XCR-3530 - System crash when click Search button when import 5.7 package library
      updateLandPatternPadTableScheme(connection); 
    }
    else
    {
      //Siew Yeng - XCR-3318
      updateDBVersionIfNecessary(connection);
    }
  }
  
  /**
   * This function inserts "Legacy" record into Reference table.
   * 
   * @author Cheah Lee Herng 
   */
  public void preInsertDefaultData(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    int legacyProjectID = createLegacyProject(connection);
    int legacyRefUserGainID = createTableRefUserGainIfNecessary(connection);
    int legacyRefIntegrationLevelID = createTableRefIntegrationLevelIfNecessary(connection);
    int legacyRefArtifactCompensationID = createTableRefArtifactCompensationIfNecessary(connection);
    int legacyRefMagnificationID = createTableRefMagnificationIfNecessary(connection);
    int legacyRefDROID = createTableRefDynamicRangeOptimizationIfNecessary(connection);
    int legacyRefFocusMethodID = createTableRefFocusMethodIfNecessary(connection);
    
    updateProjectToSubtypeTableScheme(connection, 
                                      legacyRefUserGainID, 
                                      legacyRefIntegrationLevelID, 
                                      legacyRefArtifactCompensationID,
                                      legacyRefMagnificationID,
                                      legacyRefDROID,
                                      legacyRefFocusMethodID);
    updateSubtypeToThresholdTableScheme(connection, legacyProjectID);
  }
  
  /**
   * This method checks if DBVersion table exists in the database.
   * If not, it will automatically create the table and populate it with latest DB version.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public void createTableDBVersionIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.DB_VERSION_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create DBVersion table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableDBVersionSqlString());
    }
    
    // Create PreparedStatement to update DB version
    PreparedStatement createDBVersionPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateDBVersionSqlString(), Statement.RETURN_GENERATED_KEYS);    
    
    // Insert new DB Version record
    createDBVersion(createDBVersionPreparedStatement, LATEST_DB_VERSION, UserAccountsManager.getCurrentUserLoginName());
  }
  
  /**
   * This method creates new DB Version record.
   * 
   * @param preparedStatement
   * @param dbVersion
   * @param userCreated
   * @return New DB Version ID
   * @throws DatastoreException 
   * 
   * @author Cheah Lee Herng
   */
  private int createDBVersion(PreparedStatement preparedStatement,
                              int dbVersion,
                              String userCreated) throws DatastoreException
  {
    Assert.expect(preparedStatement != null);
    Assert.expect(userCreated != null);
    
    RelationalDatabaseUtil.setInt(preparedStatement, 1, dbVersion);
    RelationalDatabaseUtil.setString(preparedStatement, 2, userCreated);
    RelationalDatabaseUtil.executeUpdate(preparedStatement);
    
    int dbVersionID = RelationalDatabaseUtil.getAutoGeneratedId(preparedStatement);
    Assert.expect(dbVersionID > 0);
    
    return dbVersionID;
  }
  
  /**
   * This method checks if RefUserGain table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefUserGainIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_USER_GAIN_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create RefUserGain table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefUserGainSqlString());                        
    }
    
    // Insert default item
    PreparedStatement createRefUserGainPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefUserGainSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefUserGain(createRefUserGainPreparedStatement, "Legacy");
  }
  
  /**
   * This method checks if RefIntegrationLevel table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefIntegrationLevelIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_INTEGRATION_LEVEL_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create RefIntegrationLevel table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefIntegrationLevelSqlString());            
      
      // Clean up
      statement = null;
    }
    
    // Insert default item
    PreparedStatement createRefIntegrationLevelPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefIntegrationLevelSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefIntegrationLevel(createRefIntegrationLevelPreparedStatement, "Legacy");
  }
  
  /**
   * This method checks if RefArtifactCompensation table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefArtifactCompensationIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_ARTIFACT_COMPENSATION_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create RefArtifactCompensation table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefArtifactCompensationSqlString());
      
      // Clean up
      statement = null;
    }
    
    // Insert default item
    PreparedStatement createRefArtifactCompensationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, 
            _sqlManager.getCreateRefArtifactCompensationSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefArtifactCompensation(createRefArtifactCompensationPreparedStatement, "Legacy");    
  }
  
  /**
   * This method checks if RefMagnification table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefMagnificationIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_MAGNIFICATION_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create DBVersion table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefMagnificationSqlString());     
      
      // Clean up
      statement = null;
    }
    
    // Insert default item
    PreparedStatement createRefMagnificationPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefMagnificationSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefMagnification(createRefMagnificationPreparedStatement, "Legacy");
  }
  
  /**
   * This method checks if RefDynamicRangeOptimization table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefDynamicRangeOptimizationIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_DYNAMIC_RANGE_OPTIMIZATION_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create RefDynamicRangeOptimization table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefDynamicRangeOptimizationSqlString());           
      
      // Clean up
      statement = null;
    }
    
    // Insert default item
    PreparedStatement createRefDROPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, 
            _sqlManager.getCreateRefDynamicRangeOptimizationSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefDynamicRangeOptimization(createRefDROPreparedStatement, "Legacy");
  }
  
  /**
   * This method checks if RefFocusMethod table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public int createTableRefFocusMethodIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_FOCUS_METHOD_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create RefFocusMethod table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefFocusMethodSqlString());
      
      // Clean up
      statement = null;
    }
    
    // Insert default item
    PreparedStatement createRefFocusMethodPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateRefFocusMethodSqlString(), Statement.RETURN_GENERATED_KEYS);
    return createRefFocusMethod(createRefFocusMethodPreparedStatement, "Legacy");
  }
  
  /**
   * This method checks if RefComponent table exists in the database.
   * If not, it will automatically create the table.
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public void createTableRefComponentIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.REF_COMPONENT_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create DBVersion table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableRefComponentSqlString());
    }
  }
  
  /**
   * This method checks if SubtypeToComponent table exists in the database.
   * If not, it will automatically create the table. 
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public void createTableSubtypeToComponentIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.SUBTYPE_TO_COMPONENT_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create SubtypeToComponent table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeToComponentSqlString());
    }
  }
  
  /**
   * This method checks if SubtypeToAlgorithm table exists in the database.
   * If not, it will automatically create the table. 
   * 
   * @param connection
   * @throws DatastoreException
   * 
   * @author Cheah Lee Herng
   */
  public void createTableSubtypeToAlgorithmIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.SUBTYPE_TO_ALGORITHM_TABLE) == false)
    {
      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(connection);
      
      // Create SubtypeToAlgorithm table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeToAlgorithmSqlString());
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int createLegacyProject(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    int projectID = -1;
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.PROJECT_TABLE))
    {
      // Create statement object            
      PreparedStatement createProjectPreparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCreateProjectSqlString(), Statement.RETURN_GENERATED_KEYS);
      
      // Create project
      projectID = createProject(createProjectPreparedStatement,
                                "LegacyProject",
                                Directory.getProjectsDir(),
                                UserAccountsManager.getCurrentUserLoginName());
    }
    return projectID;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void updateProjectToSubtypeTableScheme(Connection connection, 
                                                int legacyRefUserGainID, 
                                                int legacyRefIntegrationlevelID, 
                                                int legacyRefArtifactCompensationID,
                                                int legacyRefMagnificationID,
                                                int legacyRefDROID,
                                                int legacyRefFocusMethodID) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(legacyRefUserGainID > 0);
    Assert.expect(legacyRefIntegrationlevelID > 0);
    Assert.expect(legacyRefArtifactCompensationID > 0);
    Assert.expect(legacyRefMagnificationID > 0);
    Assert.expect(legacyRefDROID > 0);
    Assert.expect(legacyRefFocusMethodID > 0);
    
    // Create statement object
    Statement statement = RelationalDatabaseUtil.createStatement(connection);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.PROJECT_TO_SUBTYPE_TABLE))
    {
      // Add RefUserGainID column with constraint into ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefUserGainIDColumnSqlString(legacyRefUserGainID));            
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefUserGainIDConstraintSqlString());
      
      // Add RefIntegrationLevelID column with constraint into ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefIntegrationLevelIDColumnSqlString(legacyRefIntegrationlevelID));            
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefIntegrationLevelIDConstraintSqlString());
      
      // Add RefArtifactCompensationID column with constraint into ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefArtifactCompensationIDColumnSqlString(legacyRefArtifactCompensationID));            
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefArtifactCompensationIDConstraintSqlString());
      
      // Add RefMagnificationID column with constraint into ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefMagnificationIDColumnSqlString(legacyRefMagnificationID));            
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefMagnificationIDConstraintSqlString());
      
      // Add RefDynamicRangeOptimizationID column with constraint into ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefDynamicRangeOptimizationIDColumnSqlString(legacyRefDROID));            
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableProjectToSubtypeAddRefDynamicRangeOptimizationIDConstraintSqlString());                                  
    }
    else
    {
      // Create ProjectToSubtype table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableProjecToSubtypeSqlString(legacyRefUserGainID, 
                                                                                                         legacyRefIntegrationlevelID,
                                                                                                         legacyRefArtifactCompensationID,
                                                                                                         legacyRefMagnificationID,
                                                                                                         legacyRefDROID));
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void updateSubtypeToThresholdTableScheme(Connection connection, int legacyProjectID) throws DatastoreException
  {
    Assert.expect(connection != null);
    Assert.expect(legacyProjectID > 0);
    
    // Create statement object
    Statement statement = RelationalDatabaseUtil.createStatement(connection);
    
    if (RelationalDatabaseUtil.isTableExists(connection, _sqlManager.SUBTYPE_TO_THRESHOLD_TABLE))
    {
      // Add ProjectID column and constraint into SubtypeToThreshold table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableSubtypeToThresholdAddProjectIDColumnSqlString(legacyProjectID));
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableSubtypeToThresholdRemovePrimaryKeyConstraintSqlString());      
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableSubtypeToThresholdAddProjectIDConstraintSqlString());
    }
    else
    {
      // Create SubtypeToThreshold table
      RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getCreateTableSubtypeToThresholdSqlString(legacyProjectID));
    }
  }
  
  /**
   * This method will update database to the latest version
   * @author Siew Yeng
   */
  private void updateDBVersionIfNecessary(Connection connection) throws DatastoreException
  {
    Assert.expect(connection != null);
    
    ResultSet resultSet;
    int lastSavedDBVersion = LATEST_DB_VERSION;

    try
    {
      PreparedStatement preparedStatement = RelationalDatabaseUtil.createPreparedStatement(connection, _sqlManager.getCurrentDBVersionSqlString());
      RelationalDatabaseUtil.setInt(preparedStatement, 1, lastSavedDBVersion);
      resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);

      while(lastSavedDBVersion > 1)
      {
        if(resultSet.next())
          break;
        
        lastSavedDBVersion--;
        RelationalDatabaseUtil.setInt(preparedStatement, 1, lastSavedDBVersion);
        resultSet = RelationalDatabaseUtil.executeQuery(preparedStatement);
      }
      
      if(lastSavedDBVersion != LATEST_DB_VERSION)
      { 
        if(lastSavedDBVersion < 3)
        {
          updateLandPatternPadTableScheme(connection);
        }

        //Update DBVersion to the latest version
        createTableDBVersionIfNecessary(connection);
        
        // Commit changes
        RelationalDatabaseUtil.commit(connection);
      }
    }
    catch(SQLException sqlEx)
    {
      DatastoreException dex = new DatabaseDatastoreException(PACKAGE_LIBRARY_DATABASE_TYPE_ENUM, sqlEx);
      dex.initCause(sqlEx);
      throw dex;
    }
  }
  
  /**
   * Update libraries to latest version
   * @author Siew Yeng
   */
  public void updateLibraryIfNecessary(List<String> libraryPathList) throws DatastoreException
  {
    try
    {
      _database.startupDatabase();
      
      for(String libraryPath : libraryPathList)
      {
        //break the loop if user clicked on Cancel button while searching
        if (LibraryUtil.getInstance().cancelDatabaseOperation())
          break;
        
        // Get JDBC connection
        String databaseJdbcUrl = _database.getJdbcUrl(libraryPath, false);

        // Create new connection
        Connection connection = RelationalDatabaseUtil.openConnection(databaseJdbcUrl,
                                                                    _database.getDatabaseUserName(),
                                                                    _database.getDatabasePassword(),
                                                                    PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

        updateTableScheme(connection);

        //XCR-3530 - System crash when click Search button when import 5.7 package library
        RelationalDatabaseUtil.commit(connection);
        
        RelationalDatabaseUtil.closeConnection(connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
      }
    }
    finally
    {
      _database.shutdownDatabase();
    }
  }
  
  /**
   * Only use to update DB version < 3
   * @author Siew Yeng
   */
  public void updateLandPatternPadTableScheme(Connection connection) throws DatastoreException
  {
    // Create statement object
    Statement statement = RelationalDatabaseUtil.createStatement(connection);
    //Add column HoleXDimension and HoleYDimension into LandPatternPad table
    RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableLandPatternPadAddHoleWidthColumnSqlString());      
    RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getAlterTableLandPatternPadAddHoleLengthColumnSqlString());   
    //Set value of HoleXDimesion and HoleYDimension = HoleDiamater
    RelationalDatabaseUtil.executeUpdate(statement, _sqlManager.getUpdateLandPatternPadHoleWidthHoleLengthColumnSqlString());

    // Close statement
    RelationalDatabaseUtil.closeStatement(statement);
  }
}
