package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * This interface defines what makes a panel in place sensor.
 * @author Rex Shang
 */
interface PanelInPlaceSensorInt
{
  /**
   * Extends the Panel In Place Sensor.
   * @author Rex Shang
   */
  void extend() throws XrayTesterException;

  /**
   * Check to see if the Panel In Place Sensor is extended.
   * @return boolean true if the sensor is extended and false when the sensor
   * is retracted.
   * @author Rex Shang
   */
  boolean isExtended() throws XrayTesterException;

  /**
   * Retracts the Panel In Place Sensor.
   * @author Rex Shang
   */
  void retract() throws XrayTesterException;


 //XXL Based Anthony Jan 2013
 /**
   * Check to see if the Panel In Place Sensor is retracted.
   * @return boolean true if the sensor is retracted
   * @author Rex Shang
   */
  boolean isRetracted() throws XrayTesterException;

  /**
   * Check to see if a panel is in place.
   * @return boolean
   * @author Rex Shang
   */
  boolean isPanelInPlace() throws XrayTesterException;

  /**
   * Wait untill a panel is in position.
   * @param timeOut long
   * @param abortable boolean
   * @author Rex Shang
   */
  void waitUntilPanelInPlace(long timeOut, boolean abortable) throws XrayTesterException;
  
  void checkXraySafetyLevel() throws XrayTesterException;
}
