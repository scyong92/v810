package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Greg Esparza
 */
public class ScanMotionProfileEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<MagnificationEnum, ScanMotionProfileEnum> _magnificationEnumToScanMotionProfileEnumMap;
  private static Map<Integer, ScanMotionProfileEnum> _idToEnumMap;
  private static Map<String, ScanMotionProfileEnum> _nameToScanMotionProfileMap;
  private static Config _config = Config.getInstance();
  private String _name; //Ngie Xing

  public static final ScanMotionProfileEnum PROFILE0;
  public static final ScanMotionProfileEnum PROFILE1;
  public static final ScanMotionProfileEnum PROFILE2; // for s2ex only, speed for M23
  
  /**
   * @author Greg Esparza
   */
  static
  {
    _idToEnumMap = new HashMap<Integer, ScanMotionProfileEnum>();
    _nameToScanMotionProfileMap = new HashMap<String, ScanMotionProfileEnum>();
    
    PROFILE0 = new ScanMotionProfileEnum(++_index, "M19", MagnificationEnum.NOMINAL);
    PROFILE1 = new ScanMotionProfileEnum(++_index, "M11", MagnificationEnum.H_NOMINAL);
    PROFILE2 = new ScanMotionProfileEnum(++_index, "M23", MagnificationEnum.NOMINAL);
  }
  
  public static final ScanMotionProfileEnum LOWMAG_PROFILE3 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE3", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE4 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE4", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE5 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE5", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE6 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE6", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE7 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE7", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE8 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE8", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE9 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE9", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE10 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE10", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE11 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE11", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE12 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE12", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE13 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE13", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE14 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE14", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE15 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE15", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE16 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE16", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE17 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE17", MagnificationEnum.NOMINAL);
  public static final ScanMotionProfileEnum LOWMAG_PROFILE18 = new ScanMotionProfileEnum(++_index, "LOWMAG_PROFILE18", MagnificationEnum.NOMINAL);
  // Slow Speed high mag
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE19 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE19", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE20 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE20", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE21 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE21", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE22 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE22", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE23 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE23", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE24 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE24", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE25 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE25", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE26 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE26", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE27 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE27", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE28 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE28", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE29 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE29", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE30 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE30", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE31 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE31", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE32 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE32", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE33 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE33", MagnificationEnum.H_NOMINAL);
  public static final ScanMotionProfileEnum HIGHMAG_PROFILE34 = new ScanMotionProfileEnum(++_index, "HIGHMAG_PROFILE34", MagnificationEnum.H_NOMINAL);
  /**
   * @author Greg Esparza
   */
  private ScanMotionProfileEnum(int id, String name, MagnificationEnum magnificationEnum)
  {
    super(id);

    _name = name.intern();
    Object previousNameEntry = _nameToScanMotionProfileMap.put(name, this);
    Assert.expect(previousNameEntry == null);

    _idToEnumMap.put(new Integer(id), this);
  }

  /**
   * Get the enum given the id
   *
   * @author Greg Esparza
   */
  public static ScanMotionProfileEnum getEnum(int id)
  {
    ScanMotionProfileEnum motionProfile = _idToEnumMap.get(new Integer(id));
    Assert.expect(motionProfile != null);

    return motionProfile;
  }

  /**
   * @author Ngie Xing
   */
  public static ScanMotionProfileEnum getScanMotionProfileEnum(String scanMotionProfileEnumName)
  {
    Assert.expect(scanMotionProfileEnumName != null);
    ScanMotionProfileEnum scanMotionProfileEnum = _nameToScanMotionProfileMap.get(scanMotionProfileEnumName);

    Assert.expect(scanMotionProfileEnum != null);
    return scanMotionProfileEnum;
  }

  /**
   * @author Ngie Xing
   */
  public String getName()
  {
    return _name;
  }
}
