package com.axi.v810.hardware;

import java.util.*;

/**
 * This interface is used to capture native hardware information such as
 * revision id, manufacture information, etc.
 * @author Greg Esparza
 */
public interface HardwareConfigurationDescriptionInt
{
  /**
    * @author Greg Esparza
    */
   public String getKey();

   /**
    * @author Greg Esparza
    */
   public List<String> getParameters();
}
