package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspHardwareModuleVersion2 extends PspHardwareEngine 
{
  private OpticalCameraArray _opticalCameraArray;
  private EthernetBasedLightEngineProcessor _eblep;
  
  /**
   * @author Cheah Lee Herng
   */
  public PspHardwareModuleVersion2()
  {
    super();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void buildHardwareList() 
  {
    if (_opticalCameraArray == null)
      _opticalCameraArray = OpticalCameraArray.getInstance();
    
    if (_eblep == null)
      _eblep = EthernetBasedLightEngineProcessor.getInstance();
    
    _allHardwareList.add(_opticalCameraArray);
    _allHardwareList.add(_eblep);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOnLED(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.turnOnLED(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.turnOnLED(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOffLED(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.turnOffLED(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.turnOffLED(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayWhiteLight(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.displayWhiteLight(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.displayWhiteLight(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern1(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.displayFringePattern1(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.displayFringePattern1(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern2(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.displayFringePattern2(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.displayFringePattern2(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern3(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.displayFringePattern3(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.displayFringePattern3(EthernetBasedLightEngineEnum.ENGINE_1);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern4(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.displayFringePattern4(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.displayFringePattern4(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern1(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern1(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern1(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern2(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern2(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern2(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern3(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern3(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern3(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern4(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern4(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern4(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern5(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern5(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern5(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern6(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern6(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern6(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern7(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern7(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern7(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerFringePattern8(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerFringePattern8(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerFringePattern8(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void triggerWhiteLight(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.triggerWhiteLight(EthernetBasedLightEngineEnum.ENGINE_0);
    else
      _eblep.triggerWhiteLight(EthernetBasedLightEngineEnum.ENGINE_1);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(PspModuleEnum moduleEnum, int current) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _eblep.adjustCurrent(EthernetBasedLightEngineEnum.ENGINE_0, current);
    else
      _eblep.adjustCurrent(EthernetBasedLightEngineEnum.ENGINE_1, current);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void sendTriggerSequence(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftNameEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftNameEnum != null);
    
    if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_ONE))
      triggerFringePattern1(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_TWO))
      triggerFringePattern2(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_THREE))
      triggerFringePattern3(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_FOUR))
      triggerFringePattern4(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_FIVE))
      triggerFringePattern5(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_SIX))
      triggerFringePattern6(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_SEVEN))
      triggerFringePattern7(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.SHIFT_EIGHT))
      triggerFringePattern8(moduleEnum);
    else if (opticalShiftNameEnum.equals(OpticalShiftNameEnum.WHITE_LIGHT))
      triggerWhiteLight(moduleEnum);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getImageRoiWidth()
  {
    return AbstractOpticalCamera.getRoiWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getImageRoiHeight()
  {
    return AbstractOpticalCamera.getRoiHeight();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void abort() throws XrayTesterException 
  {
    _opticalCameraArray.abort();
    _eblep.abort();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOnAllLED() throws XrayTesterException 
  {
    turnOnLED(PspModuleEnum.FRONT);
    turnOnLED(PspModuleEnum.REAR);
  }

 /**
  * @author Cheah Lee Herng
  */
  public void turnOffAllLED() throws XrayTesterException 
  {
    turnOffLED(PspModuleEnum.FRONT);
    turnOffLED(PspModuleEnum.REAR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableOpticalCameraHardwareTrigger(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.initialize();
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getImageWidth() 
  {
    return AbstractOpticalCamera.getImageWidth();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getImageHeight() 
  {
    return AbstractOpticalCamera.getImageHeight();
  }
}
