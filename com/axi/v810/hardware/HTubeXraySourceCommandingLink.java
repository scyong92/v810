package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import gnu.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;


/**
 * The HTubeXraySourceCommandingLink is an implementation class for HTubeXraySource.
 *   If you are not doing maintenance programming on the HTubeXraySource class, then
 *   stop reading now.<p/>
 *
 * This class is responsible for the low-level communication protocol with the firmware
 *  that controls the Hamamatsu L9181-18 X-ray source hardware (and simulating the same).<p/>
 *
 * Specifically,<ul>
 *   <li>configuring the proper serialPort as 38400,8,NONE,1 with no flow control</li>
 *   <li>resetting the serial link</li>
 *   <li>sending CR delimitted chunks of text to the serial port</li>
 *   <li>verifying the echo of CR delimitted chunks sent (bit buckets the echo)</li>
 *   <li>receiving CR delimitted chunks of text from serial port</li>
 * </ul>
 *
 * THREAD SAFETY: This class is synchronized at the level of request/response exchange
 *   on the serial port (assuming no programmer errors).  The HTubeXraySource class should,
 *   therefore, be re-entrant.<p/>
 *
 * NOTE: This class will blow up (throw checked Exceptions) if a serial port is not available
 *   and it is NOT run in simulation mode.  Checking XrayTester.isHardwareAvailable() is
 *   generally not allowed.<p/>
 *
 * "HARDWARE" DESCRIPTION: The Hamamatsu X-ray source is an integrated unit with the
 *   x-ray tube, high voltage power supply and control electronics in a single unit.<p/>
 *
 * See: L9181-18Ver1.2.pdf<p/>
 *
 * This class does not implement HardwareObject because it was not clear to the original
 *   author what the intent and obligations of HardwareObject derivatives are.
 *     -- "Better to be reckoned a fool than to open your mouth and remove all doubt."
 *
 * @author Greg Loring
 * @author George Loring Booth
 *
 */

public class HTubeXraySourceCommandingLink implements Simulatable
{
  // RECEIVE_TIMEOUT serves as both the first character and inter-character timeout;
  //  raised from 1 second to 1.5 seconds based on observed hardware behavior
  private static final int RECEIVE_TIMEOUT = 3000; // 1500; // ms

  // constants for implementing CR framing (CR serves as an end-of-message marker)
  private static final String CR_RE = "\r";
  private static final byte CR = '\r';

  private static Config _config = Config.getInstance();
  private static boolean _simulateLinkTiming = false;

  // simulation state
  private boolean _configuredInSimMode = false;
  private final Map<String, String> _commandAndResponseMap = new TreeMap<String,String>();
  private boolean _isThisObjectInSimMode;
  private String _simulatedResponseForSTS = "STS 2";
  private String _simulatedResponseForHIV = "HIV 0";
  private String _simulatedResponseForCUR = "CUR 0";
  private String _simulatedResponseForCFS = "CFS 0";
  private String _simulatedResponseForSWS = "SWS 1 5";
  private String _simulatedResponseForSHV = "SHV 130";
  private String _simulatedResponseForSCU = "SCU 300";
  private String _simulatedResponseForSFC = "SFC 0";
  private String _simulatedResponseForZTE = "ZTE 1";
  private String _simulatedResponseForZTB = "ZTB 1";
  private String _simulatedResponseForZTR = "ZTR 5 24.5 24.3 24.4 0 0 0 32 31";

  // timings state
  StatisticalTimer _callTimer = new StatisticalTimer("HTubeXraySource:sendAndReceive");

  private final String _portName;
  private SerialPort _serialPort;
  private byte[] _inputBuffer;
  private Queue<String> _responseQueue;


  /**
   * @author Greg Loring
   * @author George Booth
   */
  public HTubeXraySourceCommandingLink(String portName)
  {
    Assert.expect(portName != null);

    // initialize simulation state (also used for syntax validation)
    // control commands
    _commandAndResponseMap.put("XON", "XON");         // warm-up and x-rays on
    _commandAndResponseMap.put("XOF", "XOF");         // x-rays off
    _commandAndResponseMap.put("HIV", "HIV 0");         // set voltage to xxx kv
    _commandAndResponseMap.put("CUR", "CUR 0");         // set current to xxx ma
    _commandAndResponseMap.put("WUP", "WUP");         // warm-up start
    _commandAndResponseMap.put("CFS", "CFS 0");         // focal spot mode
    _commandAndResponseMap.put("TSF", "TSF");         // self-test start
    _commandAndResponseMap.put("RST", "RST");         // overload protection reset
    // status commands (responses vary with conditions)
    _commandAndResponseMap.put("STS", "STS 2");       // status check
    _commandAndResponseMap.put("SPH", "SPH 0");       // preheat monitor
    _commandAndResponseMap.put("SAR", "SAR 3 50 30 0 0 0 0");       // batch sttaus check
    _commandAndResponseMap.put("SNR", "SNR 0 10 0 0");     // batch NOT READY check
    _commandAndResponseMap.put("SHV", "SHV 130");     // output voltage check (kv)
    _commandAndResponseMap.put("SCU", "SCU 300");     // output current check (ua)
    _commandAndResponseMap.put("SPV", "SPV 130");     // preset tube voltage check (kv)
    _commandAndResponseMap.put("SPC", "SPC 300");     // preset tube current check (ua)
    _commandAndResponseMap.put("SVI", "SVI 130 300");    // preset voltage and current check (kv ua)
    _commandAndResponseMap.put("SWS", "SWS 1 5");     // warm-up step check
    _commandAndResponseMap.put("SWE", "SWE 1");       // warm-up ststus check
    _commandAndResponseMap.put("SFC", "SFC 0");       // focal spot mode check
    _commandAndResponseMap.put("SIN", "SIN 0");       // interlock check
    _commandAndResponseMap.put("ZTE", "ZTE 1");       // self-test completion check
    _commandAndResponseMap.put("ZTB", "ZTB 1");       // self-test result check
    _commandAndResponseMap.put("ZTR", "ZTR 5 24.5 24.3 24.4 0 0 0 32 31");   // self-test detailed result check
    _commandAndResponseMap.put("STM", "STM 125");     // x-ray source power on time check (hours)
    _commandAndResponseMap.put("SXT", "SXT 4");       // x-ray emission time check (hours)
    _commandAndResponseMap.put("SER", "SER 0");       // hardware error check
    _commandAndResponseMap.put("SBT", "SBT 0");       // button battery check

    // Comment by LeeHerng 21-May-2010
    // In first demo system, we use HTube model "L9181-18".
    // But starting from second system onwards, we use model HTube "L9181-05".
    // So we should allow to accept either response "TYP L9181-18" or "TYP L9181-05".
    _commandAndResponseMap.put("TYP", "TYP L9181-05");  // model name check
    
    _commandAndResponseMap.put("AST", "AST");         // Undocumented keepalive timeout command
	
    // XCR-3696 Additional X-ray Tube Status Log
    _commandAndResponseMap.put("SAT", "SAT");

    // initialize hardware state
    _portName = portName;
    _serialPort = null; // already is, but more obvious if you see it
    _inputBuffer = new byte[1024]; // largest ever expected is 33 (ZTR status)
    _responseQueue = new LinkedList<String>();
  }

  /**
   * @todo GLB figure out serial port timing
   * the real hardware on a real serial port takes almost exactly ??? seconds
   * with a very small standard deviation between samples (except for the
   * first call after the JVM start up which takes a little over 5 seconds)
   * HOWEVER, calling this sleep is breaking regression tests that have
   * timing dependencies, like Test_HardwareTaskEngine...
   * SO, call this method when you want it
   *
   * @author Greg Loring
   * @author George Booth
   */
  static void setSimulateLinkTiming()
  {
    _simulateLinkTiming = true;
  }

  /**
   * @author Greg Loring
   * @author George Booth
   */
  public void setSimulationMode()
  {
    _isThisObjectInSimMode = true;
  }

  /**
   * @author Greg Loring
   * @author George Booth
   */
  public void clearSimulationMode()
  {
    _isThisObjectInSimMode = false;
  }

  /**
   * sim mode may be on for this object because it was turned on explicitly via
   * setSimulationMode() or because there is some indication in the overall
   * system configuration that all hardware related objects should be operating
   * in sim mode.  In either case, this object will not interact with hardware.
   *
   * @author Greg Loring
   * @author George Booth
   */
  public boolean isSimulationModeOn()
  {
    return _isThisObjectInSimMode || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
  }

  /**
   * reset the commanding link according to the established protocol
   *
   * @author Greg Loring
   * @author George Booth
   */
  synchronized void reset() throws HardwareException
  {
     if (isSimulationModeOn() == false)
    {
      configureSerialPortOnce();
    }
  }

  /**
   * get the operation status and return the appropriate enum
   *
   * @author George Booth
   */
  public synchronized HTubeXraySourceStatusEnum getOperationStatus() throws HardwareException
  {
    // make sure link is established
    this.configureSerialPortOnce();

    String response = sendAndReceive("STS");
    return HTubeXraySourceStatusEnum.getEnum(response);
  }

  /**
   * get the simulated operation status and return the appropriate enum. The param is the desired status.
   * Simulation Mode only, used for testing
   *
   * @author George Booth
   */
  public synchronized HTubeXraySourceStatusEnum getOperationStatus(String status) throws HardwareException
  {
    Assert.expect(isSimulationModeOn());

    String response = sendAndReceive("STS");

    return HTubeXraySourceStatusEnum.getEnum(status);
  }

  /**
   * send a chunk of text (adding CR delimitter) to the serial port, verify the echo,
   * and receive a chunk of text (removing CR delimitter) from the serial port
   *
   * @author Greg Loring
   * @author George Booth
   */
  synchronized String sendAndReceive(String request) throws HardwareException
  {
    // syntax validation
    Assert.expect(request != null);
    Assert.expect(_commandAndResponseMap.containsKey(request.substring(0, 3)));

    // serial port or sim?
    String response = null;
    if (isSimulationModeOn())
    {
      response = simulatedSendAndReceive(request);
    }
    else
    {
      configureSerialPortOnce();
      response = serialPortSendAndReceive(request);
    }

    // a response of ERR 0 NOC means that an invalid command was sent: i.e., a syntax error
    Assert.expect(response.equals("ERR 0 NOC") == false);

    return response;
  }

  /**
   * if the serial port isn't ready, then configure and reset it
   *
   * @author Greg Loring
   * @author George Booth
   */
  public synchronized void configureSerialPortOnce() throws XraySourceHardwareException
  {
    if (_serialPort == null && _configuredInSimMode == false)
    {
//      System.out.println("configureSerialPortOnce()");
      configureSerialPort();
      serialPortReset();
    }
  }

  /**
   * configure the proper serial port to 38400,8,1,NONE with no flow control
   *
   * @author Greg Loring
   * @author George Booth
   */
  synchronized void configureSerialPort() throws XraySourceHardwareException
  {
//    System.out.println("configureSerialPort()");
    Assert.expect(_serialPort == null);

    if (isSimulationModeOn())
    {
      _configuredInSimMode = true;
      return;
    }
    // does portName even exist?
    CommPortIdentifier commPortId = null;
    try
    {
      commPortId = CommPortIdentifier.getPortIdentifier(_portName);
    }
    catch (NoSuchPortException nspx)
    {
      throw XraySourceHardwareException.getSerialCommunicationNoSuchPortException(_portName);
    }

    // does portName reference a serial port?
    if (commPortId.getPortType() != CommPortIdentifier.PORT_SERIAL)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }

    // grab the serial port
    CommPort commPort;
    try
    {
      commPort = commPortId.open("HTubeXraySource", 0); // may throw javax.comm.PortInUseException
    }
    catch (PortInUseException piux)
    {
      throw XraySourceHardwareException.getSerialCommunicationPortInUseException(_portName, piux.currentOwner);
    }

    // the javax.comm framing stuff is just not working out very well:
    //  it returns embedded CRs; it returns on a timeout with partial frames.
//    // let the javax.comm stuff do the CR framing
//    try
//    {
//      commPort.enableReceiveFraming(CR);
//      Assert.expect(commPort.isReceiveFramingEnabled());
//      Assert.expect(commPort.getReceiveFramingByte() == CR);
//    }
//    catch (UnsupportedCommOperationException ucox)
//    {
//      throw new IllegalStateException("code port problem?: these features were supported on WIN32");
//    }

    // set the receive timeout in millis
    try
    {
      commPort.enableReceiveTimeout(RECEIVE_TIMEOUT); // ms
    }
    catch (UnsupportedCommOperationException ucox)
    {
      throw XraySourceHardwareException.getSerialCommunicationUnsupportedReceiveTimeoutException(_portName, RECEIVE_TIMEOUT);
    }

    // set the baud rate, etc.
    try
    {
//      System.out.println("  setting baud rate");
      _serialPort = (SerialPort)commPort;
      _serialPort.setSerialPortParams(38400, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
      _serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
    }
    catch (UnsupportedCommOperationException ucox)
    {
      _serialPort = null;
      throw XraySourceHardwareException.getSerialCommunicationUnsupportedParametersException(_portName, 38400, 8, 1, "NONE");
    }
  }

  /**
   * reset the commanding link according to the established protocol
   *
   * NOTE: any serial link that is set to full-duplex and whose far end ignores CRs will
   *  "pass" this reset method (complete without errors); therefore, reset is NOT a good
   *  method for asking "am I talking to what I think I am talking to."
   *
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized void serialPortReset() throws XraySourceHardwareException
  {
//    System.out.println("serialPortReset()");
    // dump any read-ahead responses
    _responseQueue.clear();

    try
    {
    /*
      // do this bit a few times, because 2 CRs didn't seem to be enough
      for (int i = 0; i < 3; i++)
      {
        // wait for any current activity to end
        while (_serialPort.getInputStream().read() != -1)
        {
          // just dumping unexpected input on the floor until the RECEIVE_TIMEOUT expires
        }

       // send one CR and receive any amount of unexpected input followed by a CR in reply
        _serialPort.getOutputStream().write((byte)CR);
      }
      receive(); // receive() removes the CR; throw the actual input on the floor
*/
      // send another CR and expect just it to come back
      String response;
//      System.out.println("  Looping on initial <CR>");
      do
      {
        if (isSimulationModeOn())
        {
          response = "ERR 0 NOC";
        }
        else
        {
          _serialPort.getOutputStream().write((byte)CR);
          response = receive();
        }
//        System.out.println("    response = " + response);
      }
      while (response.equals("ERR 0 NOC") == false);

    }
    catch (IOException iox)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }
  }

  /**
   * send a message (adding CR delimitter) to the serial port, verify the echo,
   *  and receive a message (removing CR delimitter) from the serial port
   *
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized String serialPortSendAndReceive(String request) throws XraySourceHardwareException
  {
//    System.out.println("serialPortSendAndReceive(" + request + ")");
    try
    {
      _callTimer.start();
      String response = sendAndVerifyEcho(request);
      _callTimer.stop();
      trace(_callTimer);
      return response;
    }
    catch (IOException iox)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }
  }
  
  /*
   * @Author Kee Chin Seong 
   * - To Return HTube Command Result 
   */
  public synchronized void checkHTubeModelResult(String request) throws HardwareException
  {
    Assert.expect(request != null);
    
    String response = "";
    // get the X-ray Source type
    try
    {
        if (isSimulationModeOn())
        {
          response = "TYP L9181-05";
        }
        else
        {
          response = sendAndVerifyEcho("TYP");
        }
    }
    catch (IOException iox)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }

    if (response.equals("TYP L9181-05") == false && 
        response.equals("TYP L9181-18") == false &&
        response.equals("TYP L9181-44") == false)
    {
      throw XraySourceHardwareException.getSerialCommunicationProtocolHardwareException(_portName, "TYP L9181-05 or TYP L9181-18 or TYP L9181-44", response);
    }
  }

  /**
   * send a chunk of text (adding CR delimitter) to the serial port and verify the echo
   *
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized String sendAndVerifyEcho(String request) throws IOException,
      XraySourceHardwareException
  {
 //   System.out.print("sendAndVerifyEcho(" +  request + ")");
    trace("request", request);

    // assume that any pending input is caused by this class getting out of sync with the hardware:
    // actually, on at least one piece of hardware, each and every time the hardware interlocks are
    // tripped, the top gun controller puts 8A0,0000<CR>0000 on the RS-232 link, as if it had received
    // a "set cathode current to zero" command
//    flushUnexpectedInput();

    // convert from String to bytes using US-ASCII (do not use the default charset!)
    _serialPort.getOutputStream().write(request.getBytes("US-ASCII"));
    _serialPort.getOutputStream().write((byte)CR);

//    System.out.println("  request  = " + request + ", request.substring(0, 3) = " + request.substring(0, 3));

    String response = receive();
//    System.out.println("  response = " + response);

    if (response.substring(0, 3).equals(request.substring(0, 3)) == false)
    {
      throw XraySourceHardwareException.getSerialCommunicationProtocolHardwareException(_portName, request, response);
    }
    return response;
  }

  /**
   * get CR framed input message from the serial port; removes the CR delimitter
   *
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized String receive() throws IOException, XraySourceHardwareException
  {
//    System.out.println("  receive()");
    // likely to get more than one frame (message + CR) in a read, so fill and drain a response queue
    if (_responseQueue.isEmpty())
    {
//      System.out.println("    responseQueue is empty");
      // collect input until we have at least one complete frame (accumlated input ends with a CR)
      StringBuffer framesBuffer = new StringBuffer();
      do
      {
        // read with timeout (RECEIVE_TIMEOUT)
        int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
//        System.out.println("    numBytesRead = " + numBytesRead);
        if (numBytesRead < 1)
        {
//          System.out.println("      numBytesRead < 1");
          throw XraySourceHardwareException.getSerialCommunicationTimeoutException(_portName);
        }

        // convert from bytes to String using US-ASCII (do not use the default charset!)
        String chunk = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");
        trace("chunk", chunk.replaceAll(CR_RE, "<CR>"));

        framesBuffer.append(chunk);
//        if (framesBuffer.charAt(framesBuffer.length() - 1) != CR)
//        {
//          System.out.println("    last char of framebuffer != CR");
//        }
      }
      while (framesBuffer.charAt(framesBuffer.length() - 1) != CR); // last character not a CR

      String frames = framesBuffer.toString();
      trace("frames", frames.replaceAll(CR_RE, "<CR>"));

      // break the data read into response frames
      StringBuffer responseBuffer = new StringBuffer();
      for (int i = 0; i < frames.length(); i++)
      {
        char c = frames.charAt(i);
        if (c == CR)
        {
          String response = responseBuffer.toString();
          trace("response", response);

          boolean status = _responseQueue.offer(response);
          Assert.expect(status);

          responseBuffer.delete(0, responseBuffer.length());
        }
        else
        {
          responseBuffer.append(c);
        }
      }
    }
    Assert.expect(_responseQueue.isEmpty() == false);

    String result = _responseQueue.remove();
    trace("result", result);
    return result;
  }

  /**
   * flush whatever data are currently ready for input, printing them to a log for debug purposes
   *
   *   NOTE: some of the ugliest debug logging that I have ever seen!
   *
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized void flushUnexpectedInput() throws IOException
  {
    StringBuffer unexpectedInputBuffer = new StringBuffer();

    while (_responseQueue.isEmpty() == false)
    {
      unexpectedInputBuffer.append("queue=");
      unexpectedInputBuffer.append(_responseQueue.remove());
      unexpectedInputBuffer.append("<CR>");
    }

    int numBytesReady = _serialPort.getInputStream().available();
    if (numBytesReady > 0)
    {
      int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
      String input = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");
      unexpectedInputBuffer.append("port=");
      unexpectedInputBuffer.append(input.replaceAll(CR_RE, "<CR>"));
      unexpectedInputBuffer.append(';');
    }

    String unexpectedInput = unexpectedInputBuffer.toString();
    if ((unexpectedInput.length() > 0) && (unexpectedInput.startsWith("port=8A0,0000") == false))
    {
      String eol = System.getProperty("line.separator");
      String msg = "unexpected input on " + _portName + ": " + unexpectedInput;

      StringBuffer stackTraceBuffer = new StringBuffer(msg);
      stackTraceBuffer.append(eol);
      StringWriter sout = new StringWriter();
      PrintWriter out = new PrintWriter(sout);
      Throwable isx = new Throwable(); // NOT for throwing...
      isx.printStackTrace(out); // ...just want the stack trace (but just the first few lines)
      out.flush();
      BufferedReader in = new BufferedReader(new StringReader(sout.toString()));
      String line = in.readLine(); // skip first line with meaningless Exception message
      while ((line = in.readLine()) != null)
      {
        if (line.contains("com.axi.v810.hardware.HTubeXraySource"))
        {
          stackTraceBuffer.append(line);
          stackTraceBuffer.append(eol);
        }
        else
        {
          break;
        }
      }
      debug(stackTraceBuffer);
    }
  }

  /* ***********************************************
   * Simulation methods
   * **********************************************/

  /**
   * @author Greg Loring
   * @author George Booth
   */
  private synchronized String simulatedSendAndReceive(String request)
  {
    Assert.expect(isSimulationModeOn());
    String command = request.substring(0, 3);
    Assert.expect(_commandAndResponseMap.containsKey(command));

    trace("request", request);
    // echo most commands
    String response = _commandAndResponseMap.get(command);
    // special commands
    if (command.equals("STS"))
    {
      response = _simulatedResponseForSTS;
    }
    else if (command.equals("HIV"))
    {
      response = request;
    }
    else if (command.equals("CUR"))
    {
      response = request;
    }
    else if (command.equals("CFS"))
    {
      response = request;
    }
    else if (command.equals("SWS"))
    {
      response = _simulatedResponseForSWS;
    }
    else if (command.equals("SHV"))
    {
      response = _simulatedResponseForSHV;
    }
    else if (command.equals("SCU"))
    {
      response = _simulatedResponseForSCU;
    }
    else if (command.equals("SFC"))
    {
      response = _simulatedResponseForSFC;
    }
    else if (command.equals("ZTE"))
    {
      response = _simulatedResponseForZTE;
    }
    else if (command.equals("ZTB"))
    {
      response = _simulatedResponseForZTB;
    }
    else if (command.equals("ZTR"))
    {
      response = _simulatedResponseForZTR;
    }
//    System.out.println("    command = " + request + ", response = " + response);

    // the real hardware on a real serial port takes almost exactly .2 seconds
    //  with a very small standard deviation between samples (except for the
    //  first call after the JVM start up which takes a little over 5 seconds)
    // HOWEVER, calling this sleep is breaking regression tests that have
    //  timing dependencies, like Test_HardwareTaskEngine...
    if (_simulateLinkTiming)
      sleep(200);

    return response;
  }

  /**
   * set a specific response for STS
   * @author George Booth
   */
  public void setSimulatedResponseForSTS(String response)
  {
    _simulatedResponseForSTS = response;
  }

  /**
   * set a specific response for HIV
   * @author George Booth
   */
  public void setSimulatedResponseForHIV(String response)
  {
    _simulatedResponseForHIV = response;
  }

  /**
   * set a specific response for CUR
   * @author George Booth
   */
  public void setSimulatedResponseForCUR(String response)
  {
    _simulatedResponseForCUR = response;
  }

  /**
   * set a specific response for CFS
   * @author George Booth
   */
  public void setSimulatedResponseForCFS(String response)
  {
    _simulatedResponseForCFS = response;
  }

  /**
   * set a specific response for SWS
   * @author George Booth
   */
  public void setSimulatedResponseForSWS(String response)
  {
    _simulatedResponseForSWS = response;
  }

  /**
   * set a specific response for SHV
   * @author George Booth
   */
  public void setSimulatedResponseForSHV(String response)
  {
    _simulatedResponseForSHV = response;
  }

  /**
   * set a specific response for SCU
   * @author George Booth
   */
  public void setSimulatedResponseForSCU(String response)
  {
    _simulatedResponseForSCU = response;
  }

  /**
   * set a specific response for SCU
   * @author George Booth
   */
  public void setSimulatedResponseForSFC(String response)
  {
    _simulatedResponseForSFC = response;
  }

  /**
   * set a specific response for ZTE
   * @author George Booth
   */
  public void setSimulatedResponseForZTE(String response)
  {
    _simulatedResponseForZTE = response;
  }

  /**
   * set a specific response for ZTB
   * @author George Booth
   */
  public void setSimulatedResponseForZTB(String response)
  {
    _simulatedResponseForZTB = response;
  }

  /**
   * set a specific response for ZTR
   * @author George Booth
   */
  public void setSimulatedResponseForZTR(String response)
  {
    _simulatedResponseForZTR = response;
  }

  /**
   * for simulating RS-232 communication and hardware processing delays
   *
   * @author Greg Loring
   * @author George Booth
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  /**
   * @author Greg Loring
   * @author George Booth
   */
  private void trace(String name, String value)
  {
    // symbol/value trace logging is turned off; to turn it on, uncomment out the next line
//   trace(_portName + ": " + name + "=" + value + ';');
  }

  /**
   * @author Greg Loring
   * @author George Booth
   */
  private void trace(Object o)
  {
    // object trace logging is turned off; to turn it on, uncomment out the next line
//    debug(o);
  }

  /**
   * @author Greg Loring
   * @author George Booth
   */
  private void debug(Object o)
  {
    Date date = new Date(System.currentTimeMillis());
    System.out.println("    " + date.toString() + ":" + o.toString());
  }


}
