package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This abstract class handles common functions of either the left or right
 * panel in place sensor.
 * @author Rex Shang
 */
public abstract class PanelInPlaceSensor extends HardwareObject implements PanelInPlaceSensorInt
{
  protected DigitalIo _digitalIo;
  protected HardwareObservable _hardwareObservable;
  private WaitForHardwareCondition _waitForHardwareCondition;
  protected static long _PANEL_DEBOUNCE_TIME_IN_MILLI_SECONDS = 200; //200
  protected static long _PANEL_DEBOUNCE_TIME_FOR_OPTICAL_SENSOR_IN_MILLI_SECONDS = 20; //Siew Yeng - XCR-3346
  private boolean _needToEnableSafetyLevelSensorForPanelLoading = false;
  private boolean _needToBypassOpticalSensorSignal = false;

  /**
   * This constructor is not public on purpose.  We should always use getInstance().
   * @author Rex Shang
   */
  protected PanelInPlaceSensor()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }
  
  public void setNeedToEnableSafetyLevelSensorForPanelLoading(boolean needToEnableSafetyLevelSensorForPanelLoading)
  {
    _needToEnableSafetyLevelSensorForPanelLoading = needToEnableSafetyLevelSensorForPanelLoading;
  }
  
  /**
   * @author Yong Sheng Chuan
   * XCR-3623, Motion error when try to unload board
   */
  public void setNeedToBypassOpticalSensorSignal(boolean needToBypassOpticalSensorSignal)
  {
    _needToBypassOpticalSensorSignal = needToBypassOpticalSensorSignal;
  }
  
  /**
   * Wait for panel to be in position.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long the time in milli seconds that we wait for panel to be
   * in place.  0 to indicate wait forever.
   * @param abortable used to indicate whether the wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilPanelInPlace(long timeOutInMilliSeconds, boolean abortable) throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);

    long panelDebounceTime;
    
    //Siew Yeng - XCR-3346 - Failed to inspect board on XXL system
    if(_digitalIo.isOpticalStopControllerEndStopperInstalled() && DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP() == false)
    {
      panelDebounceTime = _PANEL_DEBOUNCE_TIME_FOR_OPTICAL_SENSOR_IN_MILLI_SECONDS;
    }
    else
    {
      panelDebounceTime = _PANEL_DEBOUNCE_TIME_IN_MILLI_SECONDS;
    }
    
    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             panelDebounceTime,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        if(_needToEnableSafetyLevelSensorForPanelLoading)
          checkXraySafetyLevel();
        if (DigitalIo.isOpticalStopControllerInstalled() && _needToBypassOpticalSensorSignal == false && UnitTest.unitTesting() == false)
        {
          if (_digitalIo.isOpticalStopControllerError() == true)
          {
            _digitalIo.setEnableXXLOpticalStopController(false);
            System.out.println("Optical Stop Controller fail to align panel");
            throw new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_PANEL_TO_BE_IN_PLACE);
          }
        }
        return isPanelInPlace();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        Interlock.getInstance().setIntelockByPassCheckingStatus(false);
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_PANEL_TO_BE_IN_PLACE);
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

  /**
   * Abort the wait.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    super.abort();
    if (_waitForHardwareCondition != null)
      _waitForHardwareCondition.abort();
  }

}
