package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class ImageReconstructionProcessorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static ImageReconstructionProcessorEventEnum INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSORS = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processors, Initialize");
  public static ImageReconstructionProcessorEventEnum RESTARTING_IMAGE_RECONSTRUCTION_PROCESSORS = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processors, Restart");
  public static ImageReconstructionProcessorEventEnum STARTING_IMAGE_RECONSTRUCTION_PROCESSORS = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processors, Start");
  public static ImageReconstructionProcessorEventEnum STOPPING_IMAGE_RECONSTRUCTION_PROCESSORS = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processors, Stop");
  public static ImageReconstructionProcessorEventEnum REBOOTING_IMAGE_RECONSTRUCTION_PROCESSORS = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processors, Reboot");

  public static ImageReconstructionProcessorEventEnum INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSOR = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processor, Initialize");
  public static ImageReconstructionProcessorEventEnum RESTARTING_IMAGE_RECONSTRUCTION_PROCESSOR = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processor, Restart");
  public static ImageReconstructionProcessorEventEnum STARTING_IMAGE_RECONSTRUCTION_PROCESSOR = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processor, Start");
  public static ImageReconstructionProcessorEventEnum STOPPING_IMAGE_RECONSTRUCTION_PROCESSOR = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processor, Stop");
  public static ImageReconstructionProcessorEventEnum REBOOTING_IMAGE_RECONSTRUCTION_PROCESSOR = new ImageReconstructionProcessorEventEnum(++_index, "Image Reconstruction Processor, Reboot");

  private String _name;

  /**
   * @author Rex Shang
   */
  private ImageReconstructionProcessorEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
