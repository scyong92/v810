package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the reading does not end with the postamble, then throw this exception.
 *
 * @author Bob Balliew
 */
class PostambleNotFoundHardwareException extends HardwareException
{
  /**
   * This is for reading with an incorrect postamble.
   *
   * @param expectedPostamble is the expected postamble.
   * @param entireReading is the entire reading returned from the barcode scanner.
   * @param scannerOrder is the (zero based) barcode scanner read order.
   * @author Bob Balliew
   */
  public PostambleNotFoundHardwareException(String expectedPostamble, String entireReading, int scannerOrder)
  {
    super(new LocalizedString("HW_POSTAMBLE_NOT_FOUND_EXCEPTION_KEY",
                              new Object[]{expectedPostamble, entireReading, new Integer(scannerOrder + 1)}));

    Assert.expect(expectedPostamble != null);
    Assert.expect(entireReading != null);
    Assert.expect(scannerOrder >= 0);
  }
}
