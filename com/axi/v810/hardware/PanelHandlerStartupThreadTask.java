package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class PanelHandlerStartupThreadTask extends ThreadTask<Object>
{
  private PanelHandler _panelHandler;

  /**
   * @author Bill Darbie
   */
  PanelHandlerStartupThreadTask()
  {
    super("Start panel handler thread task");

    _panelHandler = PanelHandler.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws XrayTesterException
  {
    _panelHandler.startup();
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelHandler.abort();
  }
}
