package com.axi.v810.hardware;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class test the XraySource class.
 * @author George A. David
 */
public class Test_XraySource extends UnitTest
{
  // rounding error is .5 * unitScale / cmdScale
  private static final double ANODE_KILO_VOLT_TOLERANCE      = 0.5 *  165.0 / 4095.0;
  private static final double CATHODE_MICRO_AMP_TOLERANCE    = 0.5 *  204.7 / 4095.0;
  private static final double FILAMENT_VOLT_TOLERANCE        = 0.5 *  8.191 / 4095.0;
  private static final double FILAMENT_AMP_TOLERANCE         = 0.5 *  8.191 / 4095.0;
  private static final double GRID_VOLT_TOLERANCE            = 0.5 * 4095.0 / 4095.0;
  private static final double ISOLATED_SUPPLY_VOLT_TOLERANCE = 0.5 *  40.95 / 4095.0;
  private static final double SF6_PRESSURE_UNIT_TOLERANCE    = 0.5 * 4095.0 / 4095.0; /** @todo replace with real max value */
  private static final double SF6_DEGREES_C_TOLERANCE        = 0.5 *  150.0 / 4095.0;
  private static final double GUN_DEGREES_C_TOLERANCE        = 0.5 * 4095.0 / 4095.0; /** @todo replace with real max value */

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_XraySource());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XraySource source = null;
    try
    {
      try
      {
        Config.getInstance().loadIfNecessary();
      }
      catch (DatastoreException ex1)
      {
        ex1.printStackTrace();
        Assert.expect(false);
      }

      // is the factory method configured to produce an object that i can test?
      String type = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE);
      if (!type.equals("standard"))
      {
        os.println("can't test a " + type + " XraySource, dropping out");
        return;
      }

      AbstractXraySource abstractSource = AbstractXraySource.getInstance();
      Expect.expect(abstractSource != null);
      Expect.expect(abstractSource == AbstractXraySource.getInstance());
      Expect.expect(abstractSource instanceof XraySource);
      source = (XraySource) abstractSource;

      // make sure this thing isn't changing out from under us
      Expect.expect(source == AbstractXraySource.getInstance());

      if (source.isSimulationModeOn())
      {
        doSimTesting(source);
      }
      else
      {
        doHardwareTesting(source);
      }
    }
    catch(XrayTesterException ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }
  }

  void doSimTesting(XraySource source) throws XrayTesterException
  {
    // test hardware reported errors methods (must clear them out before any more tests!)
    XraySourceHardwareReportedErrorsException xshrex = source.getHardwareReportedErrors();
    Expect.expect(xshrex != null);
    String errors = xshrex.getMessage();
    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   != -1);
    Expect.expect(errors.indexOf("Interlock 1 Off")  != -1);
    Expect.expect(errors.indexOf("Interlock 2 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 3 Off")  == -1);
    Expect.expect(errors.indexOf("Regulation Error") == -1);
    Expect.expect(errors.indexOf("Arc Error")        == -1);
    Expect.expect(errors.indexOf("Over Voltage")     == -1);
    Expect.expect(errors.indexOf("Over Current")     == -1);
    Expect.expect(errors.indexOf("Temperature Limit") == -1);
    Expect.expect(errors.indexOf("Communication error on fiber link") == -1);
    source.resetHardwareReportedErrors();
    Expect.expect(source.getHardwareReportedErrors() == null);

    // operational value getters
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),                   0.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingAnodeVoltageInKiloVolts(), 158.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingAnodeVoltageInKiloVolts(), 161.0, ANODE_KILO_VOLT_TOLERANCE);

    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),                   0.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingCathodeCurrentInMicroAmps(),  97.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingCathodeCurrentInMicroAmps(), 103.0, CATHODE_MICRO_AMP_TOLERANCE);

    fuzzyExpect(source.getFilamentVoltageInVolts(),                 0.2, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentVoltageInVolts(), 4.5, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentVoltageInVolts(), 5.0, FILAMENT_VOLT_TOLERANCE);

    fuzzyExpect(source.getFilamentCurrentInAmps(),                 0.1,  FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentCurrentInAmps(), 1.75, FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentCurrentInAmps(), 2.25, FILAMENT_AMP_TOLERANCE);

    fuzzyExpect(source.getGridVoltageInVolts(),                 4095.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingGridVoltageInVolts(), 1900.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingGridVoltageInVolts(), 2100.0, GRID_VOLT_TOLERANCE);

    fuzzyExpect(source.getIsolatedSupplyVoltageInVolts(),                 20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingIsolatedSupplyVoltageInVolts(), 19.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingIsolatedSupplyVoltageInVolts(), 21.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);

    // auxiliary value getters
    fuzzyExpect(source.getSF6PressureInWhats(),       1234.0, SF6_PRESSURE_UNIT_TOLERANCE);
    fuzzyExpect(source.getSF6TemperatureInDegreesC(),  100.0, SF6_DEGREES_C_TOLERANCE);
    fuzzyExpect(source.getGunTemperatureInDegreesC(), 4095.0, GUN_DEGREES_C_TOLERANCE);
    Expect.expect(source.getCumulativeFilamentOnTimeInHours() == 1);
    Expect.expect(source.getFirmwareRevisionCode().equals("0010"));
    Expect.expect(source.getSerialNumber().equals("060100001"));

    source.startup();
    Expect.expect(source.areXraysOn());

    // operational value getters and resetters
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),                 160.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingAnodeVoltageInKiloVolts(), 158.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingAnodeVoltageInKiloVolts(), 161.0, ANODE_KILO_VOLT_TOLERANCE);
    source.resetMinMaxOperatingAnodeVoltageInKiloVolts();
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),                 160.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingAnodeVoltageInKiloVolts(), 160.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingAnodeVoltageInKiloVolts(), 160.0, ANODE_KILO_VOLT_TOLERANCE);

    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),                 100.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingCathodeCurrentInMicroAmps(),  97.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingCathodeCurrentInMicroAmps(), 103.0, CATHODE_MICRO_AMP_TOLERANCE);
    source.resetMinMaxOperatingCathodeCurrentInMicroAmps();
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),                 100.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingCathodeCurrentInMicroAmps(), 100.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingCathodeCurrentInMicroAmps(), 100.0, CATHODE_MICRO_AMP_TOLERANCE);

    fuzzyExpect(source.getFilamentVoltageInVolts(),                 4.75, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentVoltageInVolts(), 4.5,  FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentVoltageInVolts(), 5.0,  FILAMENT_VOLT_TOLERANCE);
    source.resetMinMaxOperatingFilamentVoltageInVolts();
    fuzzyExpect(source.getFilamentVoltageInVolts(),                 4.75, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentVoltageInVolts(), 4.75, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentVoltageInVolts(), 4.75, FILAMENT_VOLT_TOLERANCE);

    fuzzyExpect(source.getFilamentCurrentInAmps(),                 2.0,  FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentCurrentInAmps(), 1.75, FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentCurrentInAmps(), 2.25, FILAMENT_AMP_TOLERANCE);
    source.resetMinMaxOperatingFilamentCurrentInAmps();
    fuzzyExpect(source.getFilamentCurrentInAmps(),                 2.0, FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingFilamentCurrentInAmps(), 2.0, FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingFilamentCurrentInAmps(), 2.0, FILAMENT_AMP_TOLERANCE);

    fuzzyExpect(source.getGridVoltageInVolts(),                 2000.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingGridVoltageInVolts(), 1900.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingGridVoltageInVolts(), 2100.0, GRID_VOLT_TOLERANCE);
    source.resetMinMaxOperatingGridVoltageInVolts();
    fuzzyExpect(source.getGridVoltageInVolts(),                 2000.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingGridVoltageInVolts(), 2000.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingGridVoltageInVolts(), 2000.0, GRID_VOLT_TOLERANCE);

    fuzzyExpect(source.getIsolatedSupplyVoltageInVolts(),                 20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingIsolatedSupplyVoltageInVolts(), 19.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingIsolatedSupplyVoltageInVolts(), 21.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    source.resetMinMaxOperatingIsolatedSupplyVoltageInVolts();
    fuzzyExpect(source.getIsolatedSupplyVoltageInVolts(),                 20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMinimumOperatingIsolatedSupplyVoltageInVolts(), 20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);
    fuzzyExpect(source.getMaximumOperatingIsolatedSupplyVoltageInVolts(), 20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);

    source.off();
    Expect.expect(source.areXraysOn() == false);

    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),         0.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),       0.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getFilamentVoltageInVolts(),          0.2, FILAMENT_VOLT_TOLERANCE);
    fuzzyExpect(source.getFilamentCurrentInAmps(),           0.1, FILAMENT_AMP_TOLERANCE);
    fuzzyExpect(source.getGridVoltageInVolts(),           4095.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getIsolatedSupplyVoltageInVolts(),   20.0, ISOLATED_SUPPLY_VOLT_TOLERANCE);

    // set "turn on" values
    source.setTurnOnAnodeVoltageInKiloVolts(80.0);
    source.setTurnOnCathodeCurrentInMicroAmps(50.0);
    source.setTurnOnGridVoltageInVolts(1750.0);
    source.setFilamentCurrentLimitInAmps(3.0); // sim echoes the current limit when on, but that is *NOT*
                                               //  how the actual hardware will work! (see class javadoc)
    source.on();
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),     80.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),   50.0, CATHODE_MICRO_AMP_TOLERANCE);
    fuzzyExpect(source.getGridVoltageInVolts(),        1750.0, GRID_VOLT_TOLERANCE);
    fuzzyExpect(source.getFilamentCurrentInAmps(),        3.0, FILAMENT_AMP_TOLERANCE);
  }

  private void fuzzyExpect(double actual, double expected, double tolerance)
  {
    Expect.expect(MathUtil.fuzzyEquals(actual, expected, tolerance));
  }

  void doHardwareTesting(XraySource source) throws XrayTesterException
  {
    Expect.expect(source.areXraysOn() == false);

    source.startup();
    Expect.expect(source.areXraysOn());

    source.off();
    Expect.expect(source.areXraysOn() == false);

    source.on();
    Expect.expect(source.areXraysOn());

    source.off();
    Expect.expect(source.areXraysOn() == false);

    source.on();
    Expect.expect(source.areXraysOn());

    source.shutdown();
    Expect.expect(source.areXraysOn() == false);

//    int volts = 10;
//    source.setVoltageInKiloVolts(volts);
//    Expect.expect(source.getVoltageInKiloVolts() == volts);
//
//    int amps = 10;
//    source.setCurrentInMicroAmps(amps);
//    Expect.expect(source.getCurrentInMicroAmps() == amps);
//
//    source.getFilamentCurrentInMicroAmps();
//    source.getFilamentVoltageInKiloVolts();
//    source.getGridCurrentInMicroAmps();
//    source.getGridVoltageInKiloVolts();
//    source.getMaxCurrentInMicroAmps();
//    source.getMinCurrentInMicroAmps();
//    source.getMaxVoltageInKiloVolts();
//    source.getMinVoltageInKiloVolts();
  }

}
