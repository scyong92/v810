package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class is used to wait for a hardware to go through its debounce
 * cycle.
 * @author Rex Shang
 */
abstract class WaitForHardwareCondition
{
  private DigitalIo _digitalIo;
  private long _timeOutInMilliSeconds = 0;
  private long _hardwareSettlingTimeInMilliSeconds = 0;
  private long _pollingIntervalInMilliSeconds = 0;
  private HardwareTaskEngine _hardwareTaskEngine;
  private boolean _abortable = false;
  private boolean _aborting = false;
  private boolean _isSimulationModeOn = false;

  /**
   * @author Rex Shang
   */
  WaitForHardwareCondition(long timeOutInMilliSeconds, boolean abortable)
  {
    this(timeOutInMilliSeconds, 0, abortable);
  }

  /**
   * @author Rex Shang
   */
  WaitForHardwareCondition(long timeOutInMilliSeconds, long settlingTimeInMilliSeconds, boolean abortable)
  {
    this(timeOutInMilliSeconds, settlingTimeInMilliSeconds, DigitalIo.getInstance().getHardwarePollingIntervalInMilliSeconds(), abortable);
  }

  /**
   * Constructor
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param settlingTimeInMilliSeconds hardware debounce time.
   * @param abortable boolean
   * @author Rex Shang
   */
  WaitForHardwareCondition(long timeOutInMilliSeconds, long settlingTimeInMilliSeconds, long pollingIntervalInMilliSeconds, boolean abortable)
  {
    Assert.expect(timeOutInMilliSeconds >= 0, "Invalid timeOutInMilliSeconds.");
    Assert.expect(settlingTimeInMilliSeconds >= 0, "Invalid hardwareSettlingTime.");
    Assert.expect(pollingIntervalInMilliSeconds > 0, "Invalid pollingIntervalInMilliSeconds.");

    _digitalIo = DigitalIo.getInstance();

    if (timeOutInMilliSeconds != 0)
    {
      Assert.expect(timeOutInMilliSeconds > pollingIntervalInMilliSeconds, "timeOutInMilliSeconds needs to be greater than polling cycle.");
      Assert.expect(timeOutInMilliSeconds > settlingTimeInMilliSeconds, "Invalid timeOutInMilliSeconds.");
    }
    if (settlingTimeInMilliSeconds != 0)
    {
      // If we use settling time, make sure we check the condition at least twice.
      Assert.expect(settlingTimeInMilliSeconds > pollingIntervalInMilliSeconds, "Settling time is not greater than polling cycle.");
    }

    _timeOutInMilliSeconds = timeOutInMilliSeconds;
    _hardwareSettlingTimeInMilliSeconds = settlingTimeInMilliSeconds;
    _pollingIntervalInMilliSeconds = pollingIntervalInMilliSeconds;

    _abortable = abortable;
    _aborting = false;

    // Cache the simlation mode so we don't have to check it later.
    _isSimulationModeOn = XrayTester.getInstance().isSimulationModeOn();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @author Rex Shang
   */
  void waitUntilConditionIsMet() throws XrayTesterException
  {
    boolean conditionIsMetThroughDebounce = false;
    boolean conditionIsMet = false;
    long maxLoopCount = (long)Math.ceil((double)_timeOutInMilliSeconds / (double)_pollingIntervalInMilliSeconds);
    long settlingLoopCount = (long)Math.ceil((double)_hardwareSettlingTimeInMilliSeconds / (double)_pollingIntervalInMilliSeconds);
    long conditionMetStartLoopCount = -1;
    long count = 0;

    clearAbort();

    while (conditionIsMetThroughDebounce == false
           && (_abortable == false || isAborting() == false)
           && _isSimulationModeOn == false)
    {
      _digitalIo.waitForHardwareInMilliSeconds(_pollingIntervalInMilliSeconds);

      conditionIsMet = getCondition();

      if (conditionIsMet == false)
      {
        // Reset the timer.
        conditionMetStartLoopCount = -1;
      }
      else
      {
        // Start the timer.
        if (conditionMetStartLoopCount == -1)
          conditionMetStartLoopCount = count;
      }

      // See if we have consistent sensor condition over the debounce interval.
      if (conditionMetStartLoopCount != -1)
      {
        if (count >= (conditionMetStartLoopCount + settlingLoopCount))
          conditionIsMetThroughDebounce = true;
      }

      // Check see if we timed out.
      if (conditionIsMetThroughDebounce == false && _timeOutInMilliSeconds != 0 && count > maxLoopCount)
      {
        XrayTesterException hardwareTimeoutException = getHardwareTimeoutException();
        Assert.expect(hardwareTimeoutException != null, "hardwareTimeoutException is null.");
        _hardwareTaskEngine.throwHardwareException(hardwareTimeoutException);
      }

      count++;
    }
  }

  /**
   * Sets abort flag to indicate an abort event is underway.
   * @throws XrayTesterException overriding class might throw exception due
   * to interaction with hardware.
   * @author Rex Shang
   */
  void abort() throws XrayTesterException
  {
    _aborting = true;
  }

  /**
   * Clears abort flag.
   * @author Rex Shang
   */
  void clearAbort()
  {
    _aborting = false;
  }

  /**
   * Check to see if an abort event is underway.
   * @return boolean inidicating an abort event is happening.
   * @author Rex Shang
   */
  boolean isAborting()
  {
    return _aborting;
  }

  /**
   * All sub class extend this method to indicate the desired the condition.
   * @author Rex Shang
   */
  abstract boolean getCondition() throws XrayTesterException;

  /**
   * All sub classes need to use this to return the time out exception.
   * @author Rex Shang
   */
  abstract XrayTesterException getHardwareTimeoutException();

}
