package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import static com.axi.v810.hardware.HardwareObject.getConfig;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the Xray Actuator.  The Xray Actuator is used to control
 * x-ray up and down movement.  For Variable Magnification, the x-ray tube is able
 * to move up and down in order to change the system magnification factor.
 * @author Anthony Fong
 */
public class XrayActuator extends HardwareObject implements XrayActuatorInt
{
  private static XrayActuatorInt _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;


  private HTubeXraySource _xraySource = null;

  //Variable Mag Anthony August 2011
  private InnerBarrier _innerBarrier = null;
  private static boolean _isInstalled = false;
  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private XrayActuator()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
    
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _xraySource = (HTubeXraySource)xraySource;
    _innerBarrier = InnerBarrier.getInstance();

  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized XrayActuatorInt getInstance()
  {
    if (_instance == null)
    {
       if(XrayCylinderActuator.getInstance().isInstalled()==true && 
          XrayCPMotorActuator.getInstance().isInstalled()==false)
       {
         _instance = (XrayActuatorInt) XrayCylinderActuator.getInstance();

       }
       else if(XrayCylinderActuator.getInstance().isInstalled()==false && 
          XrayCPMotorActuator.getInstance().isInstalled()==true)
       {
         _instance = (XrayActuatorInt) XrayCPMotorActuator.getInstance();
       }
       
       _isInstalled = getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) ||
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) ;
    }

    return _instance;
  }
  /**
   * Turn Off the X-Ray.
   * @author Anthony Fong
   */
  public void turnOffXRays(boolean serviceMode) throws XrayTesterException
  {
    _xraySource.turnOffXRays(serviceMode);
  }
  /**
   * Turn On the X-Ray.
   * @author Anthony Fong
   */
  public void turnOnImagingXRays() throws XrayTesterException
  {
    _xraySource.turnOnImagingXRays();
  }
  
  /**
   * Move the X-Ray Actuator down.
   * @author Anthony Fong
   */
  public void down(boolean loadConfig) throws XrayTesterException
  {
    _instance.down(loadConfig);
  }

  /**
   * Move the X-Ray Actuator up.
   * @author Anthony Fong
   */
  public void up(boolean loadConfig) throws XrayTesterException
  {
    _instance.up(loadConfig);
  }
  
  /**
   * Move the X-Ray Actuator up to position 1.
   * @author Swee Yee Wong
   */
  public void upToPosition1(boolean loadConfig) throws XrayTesterException
  {
    _instance.upToPosition1(loadConfig);
  }
  
  /**
   * Move the X-Ray Actuator up to position 2.
   * @author Swee Yee Wong
   */
  public void upToPosition2(boolean loadConfig) throws XrayTesterException
  {
    _instance.upToPosition2(loadConfig);
  }

  /**
   * Move the X-Ray Actuator to home.
   * @author Anthony Fong
   */
  public void home(boolean loadConfig, boolean isHighMag) throws XrayTesterException
  {
    _instance.home(loadConfig, isHighMag);
  }

  /**
   * @return long returns how long last up/down action time.
   * @author Anthony Fong
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _instance.getLastHardwareActionTimeInMilliSeconds();
  }
 
  /**
   * @return true if the X-Ray Actuator is for sure down.
   * @throws XrayTesterException exception will be thrown if the X-Ray Actuator is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isDown() throws XrayTesterException
  {
    return _instance.isDown();
  }

  /**
   * @return true if the X-Ray cylinder is for sure up.
   * @throws XrayTesterException exception will be thrown if the X-Ray Actuator is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isUp() throws XrayTesterException
  {
    return _instance.isUp();
  }
  
  /**
   * @return true if the X-Ray cylinder is for sure up to position 1.
   * @throws XrayTesterException exception will be thrown if the X-Ray Actuator is neither
   * up nor down.
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition1() throws XrayTesterException
  {
    return _instance.isUpToPosition1();
  }
  
  /**
   * @return true if the X-Ray cylinder is for sure up to position 2.
   * @throws XrayTesterException exception will be thrown if the X-Ray Actuator is neither
   * up nor down.
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition2() throws XrayTesterException
  {
    return _instance.isUpToPosition2();
  }
  
  /**
   * @return true if the X-Ray cylinder is for sure down.
   * @throws XrayTesterException exception will be thrown if the X-Ray Actuator is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isHome() throws XrayTesterException
  {
    return _instance.isHome();
  }

  /**
   * @return boolean whether the X-Ray Actuator Interface is installed.
   * @author Anthony Fong
   */
//  public boolean isActuatorInstalled()
//  {
//    return _isInstalled;
//  }
  
  /**
   * For Variable Magnification, the X-Ray Actuator is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray Actuator is installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    _isInstalled = getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) ||
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) ;
    return _isInstalled;
  }
  
  /**
   * For Variable Magnification, the X-Ray Actuator is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray CPMotor Actuator is installed.
   * @author Anthony Fong
   */
  public static boolean isZAxisMotorInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false &&
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == true ;
  }
    
  /**
   * For Variable Magnification, the X-Ray Actuator is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray Cylinder Actuator is installed.
   * @author Anthony Fong
   */
  public static boolean isXRayCylinderInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == true &&
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == false;
  }
}
