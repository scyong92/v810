package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspSettingEnum extends com.axi.util.Enum 
{
  private static int _index = 1;
  private int _configuration;
  private static Map<Integer, PspSettingEnum> _pspConfigurationToVersionEnumMap;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    _pspConfigurationToVersionEnumMap = new HashMap<Integer, PspSettingEnum>();
  }
  
  public static final PspSettingEnum SETTING_BOTH  = new PspSettingEnum(++_index, 1);
  public static final PspSettingEnum SETTING_FRONT = new PspSettingEnum(++_index, 2);
  public static final PspSettingEnum SETTING_REAR  = new PspSettingEnum(++_index, 3);
  
  /**
   * @author Cheah Lee Herng 
   */
  private PspSettingEnum(int id, int configuration)
  {
    super(id);
    _configuration = configuration;
    
    _pspConfigurationToVersionEnumMap.put(_configuration, this);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public static PspSettingEnum getEnum(int configuration)
  {
    PspSettingEnum pspConfigurationEnum = _pspConfigurationToVersionEnumMap.get(configuration);
    Assert.expect(pspConfigurationEnum != null);

    return pspConfigurationEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getConfiguration()
  {
    return _configuration;
  }
}
