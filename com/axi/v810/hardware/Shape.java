package com.axi.v810.hardware;

/**
 * Describes a particular shape
 */
class Shape
{
  public static final int CIRCLE = 0;
  public static final int RECTANGLE = 1;
}




