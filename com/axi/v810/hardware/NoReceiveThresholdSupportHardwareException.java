package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port does not support receive threshold, then throw this exception.
 *
 * @author Bob Balliew
 */
class NoReceiveThresholdSupportHardwareException extends HardwareException
{
  /**
   * This is for receive threshold failure on the serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Bob Balliew
   */
  public NoReceiveThresholdSupportHardwareException(String portName)
  {
    super(new LocalizedString("HW_NO_RECEIVE_THRESHOLD_SUPPORT_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }
}
