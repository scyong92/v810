package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left panel in place sensor
 * @author Rex Shang
 */
class Test_LeftPanelInPlaceSensor extends UnitTest
{
  private LeftPanelInPlaceSensor _leftPanelInPlaceSensor = null;

  private Test_LeftPanelInPlaceSensor()
  {
    _leftPanelInPlaceSensor = LeftPanelInPlaceSensor.getInstance();
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // retract sensor and verify
      _leftPanelInPlaceSensor.retract();
      Expect.expect(_leftPanelInPlaceSensor.isExtended() == false);

      // extend sensor and verify
      _leftPanelInPlaceSensor.extend();
      Expect.expect(_leftPanelInPlaceSensor.isExtended() == true);

      // Verify that panel is in place
      //_leftPanelInPlaceSensor.isPanelInPlace()
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    Test_LeftPanelInPlaceSensor test_LeftPanelInPlaceSensor = new Test_LeftPanelInPlaceSensor();
    UnitTest.execute(test_LeftPanelInPlaceSensor);
  }

}
