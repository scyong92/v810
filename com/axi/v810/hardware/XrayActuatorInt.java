package com.axi.v810.hardware;

//Variable Mag Anthony August 2011
import com.axi.v810.util.*;

/**
 * This interface will be implemented by all the X-rays Actuator objects.
 * The X-rays Actuator on the machine are there to adjust the system magnification factor (Low Mag or High Mag).
 * @author Anthony Fong
 */
public interface XrayActuatorInt
{

  /**
   * Move to Home position in order to close the inner barrier
   * @author Anthony Fong
   */
  abstract void home(boolean loadConfig, boolean isHighMag) throws XrayTesterException;

  /**
   * Move up the Actuator. (Low Mag)
   * @author Anthony Fong
   */
  abstract void up(boolean loadConfig) throws XrayTesterException;
  
  /**
   * Move up the Actuator to position 1. (Low Mag)
   * @author Swee Yee Wong
   */
  abstract void upToPosition1(boolean loadConfig) throws XrayTesterException;
  
  /**
   * Move up the Actuator to position 2. (Low Mag)
   * @author Swee Yee Wong
   */
  abstract void upToPosition2(boolean loadConfig) throws XrayTesterException;

  /**
   * Move the current Actuator down. (High Mag)
   * @author Anthony Fong
   */
  abstract void down(boolean loadConfig) throws XrayTesterException;
  
  /**
   * @return true if the Actuator is up.
   * @author Anthony Fong
   */
  abstract boolean isHome() throws XrayTesterException;
  
  /**
   * @return true if the Actuator is up.
   * @author Anthony Fong
   */
  abstract boolean isUp() throws XrayTesterException;
  
  /**
   * @return true if the Actuator is up to position 1.
   * @author Swee Yee Wong
   */
  abstract boolean isUpToPosition1() throws XrayTesterException;
  
  /**
   * @return true if the Actuator is up to position 2.
   * @author Swee Yee Wong
   */
  abstract boolean isUpToPosition2() throws XrayTesterException;

  /**
   * @return true if the Actuator is down.
   * @author Anthony Fong
   */
  abstract boolean isDown() throws XrayTesterException;
  
    /**
   * @return long returns how long last up/down action time.
   * @author Anthony Fong
   */
  abstract long getLastHardwareActionTimeInMilliSeconds();
   
  /**
   * For Variable Magnification, the X-Ray Actuator is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray Actuator is installed.
   * @author Anthony Fong
   */
  //abstract boolean isActuatorInstalled();
}
