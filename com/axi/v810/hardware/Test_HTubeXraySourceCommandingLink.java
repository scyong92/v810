package com.axi.v810.hardware;

import com.axi.util.Expect;
import com.axi.v810.datastore.config.HardwareConfigEnum;
import com.axi.v810.datastore.config.Config;
import com.axi.util.Assert;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.util.UnitTest;
import java.io.BufferedReader;
import com.axi.v810.datastore.DatastoreException;
import com.axi.util.MathUtil;
import java.io.PrintWriter;

class Test_HTubeXraySourceCommandingLink extends UnitTest
{
    private Config _config = Config.getInstance();

    /**
     * @author Greg Loring
     */
    public static void main(String[] args)
    {
      UnitTest.execute(new Test_HTubeXraySourceCommandingLink());
    }

    /**
     * @author Greg Loring
     */
    public void test(BufferedReader in, PrintWriter out)
    {
      try
      {
        try
        {
          _config.loadIfNecessary();
        }
        catch (DatastoreException de)
        {
          de.printStackTrace();
          Assert.expect(false);
        }

        // is the factory method configured to produce an object that i can test?
        // XCR-3604 Unit Test Phase 2
        XraySourceTypeEnum sourceTypeEnum = AbstractXraySource.getInstance().getSourceTypeEnum();
        if (sourceTypeEnum.equals(XraySourceTypeEnum.HTUBE) == false)
        {
          out.println("Not an HTube XraySource, dropping out");
          return;
        }

        HTubeXraySourceCommandingLink commandingLink = new HTubeXraySourceCommandingLink("Port1");
        Expect.expect(commandingLink != null);
        Expect.expect(commandingLink instanceof HTubeXraySourceCommandingLink);

        commandingLink.setSimulationMode();
        Expect.expect(commandingLink.isSimulationModeOn());
        doSimModeTests(commandingLink, in, out);
        commandingLink.clearSimulationMode();

        //In order to get an accurate reading of commandingLink sim mode OFF, we need
        //to remove the confounder of HARDWARE_SIMULATION mode because it is a logical OR
        //of the two values
        boolean wasInHardwareSimMode = _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
        try
        {
          //If hardware sim is on, turn it off
          if(wasInHardwareSimMode)
          {
            _config.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, false);
          }
          //Now we should see that the legacy source is not in sim mode
          Expect.expect(commandingLink.isSimulationModeOn() == false);
          
          // XCR-3604 Unit Test Phase 2
          System.out.println("HardwareWorkerThread test complete");
        }
        //No matter what happened, restore Hardware sim to the previous state
        finally
        {
          if(wasInHardwareSimMode)
          {
            _config.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, true);
          }
        }
      }
      catch(Exception x)
      {
        x.printStackTrace();
      }
    }

    private void doSimModeTests(HTubeXraySourceCommandingLink commandingLink,
                                BufferedReader in,
                                PrintWriter out)  throws Exception
    {
      Expect.expect(commandingLink.sendAndReceive("XON").equals("XON"));         // warm-up and x-rays on
      Expect.expect(commandingLink.sendAndReceive("XOF").equals("XOF"));         // x-rays off
      Expect.expect(commandingLink.sendAndReceive("HIV 130").equals("HIV 130")); // set voltage to 130 kv
      Expect.expect(commandingLink.sendAndReceive("CUR 300").equals("CUR 300")); // set current to 300 ma
      Expect.expect(commandingLink.sendAndReceive("CUR 275").equals("CUR 275")); // set current to 275 ma
      Expect.expect(commandingLink.sendAndReceive("CUR 125").equals("CUR 125")); // set current to 125 ma
      Expect.expect(commandingLink.sendAndReceive("WUP").equals("WUP"));         // warm-up start
      Expect.expect(commandingLink.sendAndReceive("CFS 0").equals("CFS 0"));     // small focal spot mode
      Expect.expect(commandingLink.sendAndReceive("CFS 1").equals("CFS 1"));     // middle focal spot mode
      Expect.expect(commandingLink.sendAndReceive("CFS 2").equals("CFS 2"));     // large focal spot mode
      Expect.expect(commandingLink.sendAndReceive("TSF").equals("TSF"));         // self-test start
      Expect.expect(commandingLink.sendAndReceive("RST").equals("RST"));         // overload protection reset
      // status commands (responses vary with conditions
      Expect.expect(commandingLink.sendAndReceive("STS").equals("STS 2"));       // status check
      Expect.expect(commandingLink.sendAndReceive("SPH").equals("SPH 0"));       // preheat monitor
      Expect.expect(commandingLink.sendAndReceive("SAR").equals("SAR 3 50 30 0 0 0 0"));       // batch sttaus check
      Expect.expect(commandingLink.sendAndReceive("SNR").equals("SNR 0 10 0 0"));     // batch NOT READY check
      Expect.expect(commandingLink.sendAndReceive("SHV").equals("SHV 130"));     // output voltage check (kv)
      Expect.expect(commandingLink.sendAndReceive("SCU").equals("SCU 300"));     // output current check (ua)
      Expect.expect(commandingLink.sendAndReceive("SPV").equals("SPV 130"));     // preset tube voltage check (kv)
      Expect.expect(commandingLink.sendAndReceive("SPC").equals("SPC 300"));     // preset tube current check (ua)
      Expect.expect(commandingLink.sendAndReceive("SVI").equals("SVI 130 300"));    // preset voltage and current check (kv ua)
      Expect.expect(commandingLink.sendAndReceive("SWS").equals("SWS 1 5"));     // warm-up step check
      Expect.expect(commandingLink.sendAndReceive("SWE").equals("SWE 1"));       // warm-up ststus check
      Expect.expect(commandingLink.sendAndReceive("SFC").equals("SFC 0"));       // focal spot mode check
      Expect.expect(commandingLink.sendAndReceive("SIN").equals("SIN 0"));       // interlock check
      Expect.expect(commandingLink.sendAndReceive("ZTE").equals("ZTE 1"));       // self-test completion check
      Expect.expect(commandingLink.sendAndReceive("ZTB").equals("ZTB 1"));       // self-test result check
      Expect.expect(commandingLink.sendAndReceive("ZTR").equals("ZTR 5 24.5 24.3 24.4 0 0 0 32 31"));   // self-test detailed result check
      Expect.expect(commandingLink.sendAndReceive("STM").equals("STM 125"));     // x-ray source power on time check (hours)
      Expect.expect(commandingLink.sendAndReceive("SXT").equals("SXT 4"));       // x-ray emission time check (hours)
      Expect.expect(commandingLink.sendAndReceive("SER").equals("SER 0"));       // hardware error check
      Expect.expect(commandingLink.sendAndReceive("SBT").equals("SBT 0"));       // button battery check
      Expect.expect(commandingLink.sendAndReceive("TYP").equals("TYP L9181-05"));  // model name check

    }

  }
