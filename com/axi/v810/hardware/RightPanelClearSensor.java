package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * This class controls the right panel clear sensor (AKA panel crunch sensor).
 *
 * @author Rex Shang
 */
public class RightPanelClearSensor extends PanelClearSensor
{
  private static RightPanelClearSensor _instance;

  /**
   * @author Rex Shang
   */
  private RightPanelClearSensor()
  {
    super();
    _instance = null;
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized RightPanelClearSensor getInstance()
  {
    if (_instance == null)
      _instance = new RightPanelClearSensor();

    return _instance;
  }

  /**
   * @return true if the panel clear sensor is blocked.
   * @author Rex Shang
   */
  public boolean isBlocked() throws XrayTesterException
  {
    return _digitalIo.isRightPanelClearSensorBlocked();
  }

}
