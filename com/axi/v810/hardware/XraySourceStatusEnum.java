package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class XraySourceStatusEnum extends com.axi.util.Enum
{
  private static Map<String, XraySourceStatusEnum> _statusStringToXraySourceStatusEnumMap = new HashMap<String, XraySourceStatusEnum>();

  private static int _index = 0;
  public static final XraySourceStatusEnum READY = new XraySourceStatusEnum(_index++, "REDY");
  public static final XraySourceStatusEnum STANDBY = new XraySourceStatusEnum(_index++, "STBY");
  public static final XraySourceStatusEnum LONG_TERM_SHUTDOWN = new XraySourceStatusEnum(_index++, "DOWN");
  public static final XraySourceStatusEnum STARTING_XRAYS = new XraySourceStatusEnum(_index++, "RAMP");
  public static final XraySourceStatusEnum ERROR = new XraySourceStatusEnum(_index++, "EROR");

  /**
   * @author George A. David
   */
  private XraySourceStatusEnum(int id, String statusString)
  {
    super(id);;
    Object previous = _statusStringToXraySourceStatusEnumMap.put(statusString.toUpperCase(), this);
    Assert.expect(previous == null);
  }

  /**
   * @author George A. David
   */
  public static XraySourceStatusEnum getEnum(String statusString)
  {
    XraySourceStatusEnum enumeration = _statusStringToXraySourceStatusEnumMap.get(statusString.toUpperCase());

    Assert.expect(enumeration != null);
    return enumeration;
  }
}
