package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for SerialCommunicationsProtocolHardwareException.
*
* @author Jeff Ryer
*/
public class Test_SerialCommunicationProtocolHardwareException extends UnitTest
{

  /**
  * @author Jeff Ryer
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_SerialCommunicationProtocolHardwareException());
  }

  /**
  * @author Jeff Ryer
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    SerialCommunicationProtocolHardwareException ex = new SerialCommunicationProtocolHardwareException("portName", "expectedReply", "actualReply");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
