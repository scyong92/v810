package com.axi.v810.hardware;

/**
 * please do not change the sequence
 * @author Yong Sheng Chuan
 */
public class ScanPassBreakPointEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ScanPassBreakPointEnum SURFACE_MAP = new ScanPassBreakPointEnum(++_index);
  public static ScanPassBreakPointEnum STAGE_SPEED = new ScanPassBreakPointEnum(++_index);
  public static ScanPassBreakPointEnum USER_GAIN = new ScanPassBreakPointEnum(++_index);
  public static ScanPassBreakPointEnum SURFACE_MODEL = new ScanPassBreakPointEnum(++_index);
  
  /**
   * @author Yong Sheng Chuan
   */
  private ScanPassBreakPointEnum(int id)
  {
    super(id);
  }
}
