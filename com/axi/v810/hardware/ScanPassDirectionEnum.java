package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 * @author Greg Esparza
 */
public class ScanPassDirectionEnum extends com.axi.util.Enum
{
  private LocalizedString _name;

  public static ScanPassDirectionEnum FORWARD = new ScanPassDirectionEnum(1, new LocalizedString("GUI_FORWARD_DIRECTION_KEY", null));
  public static ScanPassDirectionEnum REVERSE = new ScanPassDirectionEnum(-1, new LocalizedString("GUI_REVERSE_DIRECTION_KEY", null));
  public static ScanPassDirectionEnum BIDIRECTION = new ScanPassDirectionEnum(0, new LocalizedString("GUI_BIDIRECTION_KEY", null));

  /**
   * @author Roy Williams
   * @author Greg Esparza
   */
  private ScanPassDirectionEnum(int id, LocalizedString name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author Roy Williams
   * @author Greg Esparza
   */
  public String toString()
  {
    return StringLocalizer.keyToString(_name);
  }
}


