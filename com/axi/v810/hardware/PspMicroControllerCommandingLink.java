package com.axi.v810.hardware;

import com.axi.v810.util.XrayTesterException;
import java.io.*;
import java.util.*;

import gnu.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Cheah Lee Herng
 */
public class PspMicroControllerCommandingLink implements Simulatable
{
    private static final int RECEIVE_TIMEOUT = 3000;

    // constants for implementing CR framing (CR serves as an end-of-message marker)
    private static final String CR_RE = "\r";
    private static final byte CR = '\r';

    private static Config _config = Config.getInstance();
    private static boolean _simulateLinkTiming = false;

    // simulation state
    private boolean _configuredInSimMode = false;
    private final Map<String, List<String>> _commandAndResponseMap = new TreeMap<String,List<String>>();
    private String _simulatedResponseForAxiUsage                = "e0";
    private String _simulatedResponseForInitializeProjector1    = "10";
    private String _simulatedResponseForStartProjector1Sequence = "11";
    private String _simulatedResponseForAllProjectorOn          = "b0";
    private String _simulatedResponseForAllProjectorOff         = "b5";
    private String _simulatedResponseForControllerStatus        = "a0";
    private String _simulatedResponseForAllProjectorLEDEnable   = "c0";
    private String _simulatedResponseForProjectorOneLEDEnable   = "c1";
    private String _simulatedResponseForProjectorTwoLEDEnable   = "c2";
    private String _simulatedResponseForAllProjectorLEDDisable  = "c5";
    private String _simulatedResponseForProjectorOneLEDDisable  = "c6";
    private String _simulatedResponseForProjectorTwoLEDDisable  = "c7";
    private String _simulatedResponseForMicroControllerFirmwareVersion = "10";  // Old microcontroller firmware version
    private List<String> _expectedErrorCode = new ArrayList<String>();
    private boolean _isThisObjectInSimMode;

    // timings state
    StatisticalTimer _callTimer = new StatisticalTimer("PspMicroController:sendAndReceive");

    private final String _portName;
    private SerialPort _serialPort;
    private byte[] _inputBuffer;
    private Queue<String> _responseQueue;

    /**
     * @author Cheah Lee Herng
     */
    public PspMicroControllerCommandingLink(String portName)
    {
        Assert.expect(portName != null);
        
        _commandAndResponseMap.put("rs", new ArrayList<String>(Arrays.asList("rs")));
        _commandAndResponseMap.put("0e", new ArrayList<String>(Arrays.asList("0e")));
        _commandAndResponseMap.put("10", new ArrayList<String>(Arrays.asList("10")));
        _commandAndResponseMap.put("11", new ArrayList<String>(Arrays.asList("11")));
        _commandAndResponseMap.put("20", new ArrayList<String>(Arrays.asList("20")));
        _commandAndResponseMap.put("21", new ArrayList<String>(Arrays.asList("21")));
        _commandAndResponseMap.put("40", new ArrayList<String>(Arrays.asList("40")));
        _commandAndResponseMap.put("b0", new ArrayList<String>(Arrays.asList("b0")));
        _commandAndResponseMap.put("b5", new ArrayList<String>(Arrays.asList("b5")));
        _commandAndResponseMap.put("a0", new ArrayList<String>(Arrays.asList("a0")));       // Controller Status
        _commandAndResponseMap.put("c0", new ArrayList<String>(Arrays.asList("c0")));       // All projector LED enable
        _commandAndResponseMap.put("c1", new ArrayList<String>(Arrays.asList("c1")));       // Projector #1 LED enable
        _commandAndResponseMap.put("c2", new ArrayList<String>(Arrays.asList("c2")));       // Projector #2 LED enable
        _commandAndResponseMap.put("c5", new ArrayList<String>(Arrays.asList("c5")));       // All projector LED disable
        _commandAndResponseMap.put("c6", new ArrayList<String>(Arrays.asList("c6")));       // Projector #1 LED disable
        _commandAndResponseMap.put("c7", new ArrayList<String>(Arrays.asList("c7")));       // Projector #2 LED disable
        _commandAndResponseMap.put("f0", new ArrayList<String>(Arrays.asList("10","11","12")));  // MicroController firmware version
        
        // Populate expected error code
        _expectedErrorCode.add("f1f2f4f8"); 
        _expectedErrorCode.add("f1");
        _expectedErrorCode.add("f2");
        _expectedErrorCode.add("f4");
        _expectedErrorCode.add("f8");
        _expectedErrorCode.add("f1f2");
        _expectedErrorCode.add("f1f4");
        _expectedErrorCode.add("f1f8");
        _expectedErrorCode.add("f2f4");
        _expectedErrorCode.add("f2f8");
        _expectedErrorCode.add("f4f8");
        _expectedErrorCode.add("f2f4f8");
        
        // initialize hardware state
        _portName = portName;
        _serialPort = null; // already is, but more obvious if you see it
        _inputBuffer = new byte[1024]; // largest ever expected is 33 (ZTR status)
        _responseQueue = new LinkedList<String>();
    }

    /**
     * @author Cheah Lee Herng
     */
    static void setSimulateLinkTiming()
    {
        _simulateLinkTiming = true;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void setSimulationMode()
    {
        _isThisObjectInSimMode = true;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void clearSimulationMode() throws XrayTesterException
    {
        _isThisObjectInSimMode = false;
    }

    /**
     * sim mode may be on for this object because it was turned on explicitly via
     * setSimulationMode() or because there is some indication in the overall
     * system configuration that all hardware related objects should be operating
     * in sim mode.  In either case, this object will not interact with hardware.
     *
     * @author Cheah Lee Herng
     */
    public boolean isSimulationModeOn()
    {
        return _isThisObjectInSimMode || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    }

    /**
     * reset the commanding link according to the established protocol
     *
     * @author Cheah Lee Herng
     */
    synchronized void reset() throws HardwareException
    {
     if (isSimulationModeOn() == false)
     {
      configureSerialPortOnce();
     }
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized PspMicroControllerStatusEnum getOperationStatus() throws HardwareException
    {
        // make sure link is established
        this.configureSerialPortOnce();
               
        String response = sendAndReceive("a0");
        return PspMicroControllerStatusEnum.getEnum(response);
    }

    /**
     * @author Cheah Lee Herng
     */
    synchronized String sendAndReceive(String request) throws HardwareException
    {
        // syntax validation
        Assert.expect(request != null);
        Assert.expect(_commandAndResponseMap.containsKey(request.substring(0, 2)));

        // serial port or sim?
        String response;
        if (isSimulationModeOn())
        {
          response = simulatedSendAndReceive(request);
        }
        else
        {
          configureSerialPortOnce();
          response = serialPortSendAndReceive(request);
        }

        return response;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    synchronized void sendAndNoReceive(String request) throws HardwareException
    {
      // syntax validation
      Assert.expect(request != null);
      
      if (isSimulationModeOn() == false)
      {
        configureSerialPortOnce();
        serialPortSend(request);
      }
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized void configureSerialPortOnce() throws MicroControllerHardwareException
    {
        if (_serialPort == null && isSimulationModeOn() == false)
        {
          configureSerialPort();          
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    synchronized void configureSerialPort() throws MicroControllerHardwareException
    {
        Assert.expect(_serialPort == null);

        if (isSimulationModeOn())
        {
          _configuredInSimMode = true;
          return;
        }
        // does portName even exist?
        CommPortIdentifier commPortId = null;
        try
        {   
          commPortId = CommPortIdentifier.getPortIdentifier(_portName);
        }
        catch (NoSuchPortException nspx)
        {
          throw MicroControllerHardwareException.getSerialCommunicationNoSuchPortException(_portName);
        }

        // does portName reference a serial port?
        if (commPortId.getPortType() != CommPortIdentifier.PORT_SERIAL)
        {
          throw MicroControllerHardwareException.getSerialCommunicationErrorException(_portName);
        }

        // grab the serial port
        CommPort commPort;
        try
        {
          commPort = commPortId.open("PspMicroController", 0); // may throw javax.comm.PortInUseException
        }
        catch (PortInUseException piux)
        {
          throw MicroControllerHardwareException.getSerialCommunicationPortInUseException(_portName, piux.currentOwner);
        }

        // set the receive timeout in millis
        try
        {
          commPort.enableReceiveTimeout(RECEIVE_TIMEOUT); // ms
        }
        catch (UnsupportedCommOperationException ucox)
        {
          throw MicroControllerHardwareException.getSerialCommunicationUnsupportedReceiveTimeoutException(_portName, RECEIVE_TIMEOUT);
        }

        // set the baud rate, etc.
        try
        {
          _serialPort = (SerialPort)commPort;
          _serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
          _serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
        }
        catch (UnsupportedCommOperationException ucox)
        {
          _serialPort = null;
          throw MicroControllerHardwareException.getSerialCommunicationUnsupportedParametersException(_portName, 115200, 8, 1, "NONE");
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private synchronized void serialPortReset() throws MicroControllerHardwareException
    {
        // dump any read-ahead responses
        _responseQueue.clear();
        
        try
        {
          int numberOfLooping = 3;
          
          // send another CR and expect just it to come back
          String response;
          do
          {
            if (isSimulationModeOn())
            {
              response = "a0";
            }
            else
            {
              _serialPort.getOutputStream().write((byte)CR);
              response = receive();
            }
          }
          while (numberOfLooping-- > 0);
        }
        catch (IOException iox)
        {
          throw MicroControllerHardwareException.getSerialCommunicationErrorException(_portName);
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private synchronized String serialPortSendAndReceive(String request) throws MicroControllerHardwareException
    {
        Assert.expect(request != null);
        Assert.expect(_commandAndResponseMap.containsKey(request.substring(0, 2)));
        
        try
        {
          _callTimer.start();
          String response = sendAndVerifyEcho(request);
          _callTimer.stop();
          return response;
        }
        catch (IOException iox)
        {
          throw MicroControllerHardwareException.getSerialCommunicationErrorException(_portName);
        }
    }
    
    /**
     * This function only sends requests 
     * 
     * @author Cheah Lee Herng 
     */
    private synchronized void serialPortSend(String request) throws MicroControllerHardwareException
    {
      Assert.expect(request != null);
      
      try
      {
        _callTimer.start();
        send(request);
        _callTimer.stop();
      }
      catch (IOException iox)
      {
        throw MicroControllerHardwareException.getSerialCommunicationErrorException(_portName);
      }
    }

    /**
     * @author Cheah Lee Herng
     */
    private synchronized String sendAndVerifyEcho(String request) throws IOException,
      MicroControllerHardwareException
            
    {        
        flushUnexpectedInput();
        
        _serialPort.getOutputStream().write(request.getBytes("US-ASCII"));
        _serialPort.getOutputStream().write((byte)CR);

        String response = receive();
        if (_commandAndResponseMap.get(request).contains(response.substring(0, 2)) == false &&
            _expectedErrorCode.contains(response) == false)
        {
          throw MicroControllerHardwareException.getSerialCommunicationProtocolHardwareException(_portName, request, response);
        }
        return response;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private synchronized void send(String request) throws IOException, MicroControllerHardwareException
    {
      flushUnexpectedInput();
      
      _serialPort.getOutputStream().write(request.getBytes("US-ASCII"));
      _serialPort.getOutputStream().write((byte)CR);
    }

    /**
   * get CR framed input message from the serial port; removes the CR delimitter
   *
   * @author Cheah Lee Herng
   */
  private synchronized String receive() throws IOException, MicroControllerHardwareException
  {
    if (_responseQueue.isEmpty())
    {
      StringBuffer framesBuffer = new StringBuffer();
      do
      {
        // read with timeout (RECEIVE_TIMEOUT)
        int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
        if (numBytesRead < 1)
        {
          throw MicroControllerHardwareException.getSerialCommunicationTimeoutException(_portName);
        }

        // convert from bytes to String using US-ASCII (do not use the default charset!)
        String chunk = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");        

        framesBuffer.append(chunk);
      }
      while (framesBuffer.charAt(framesBuffer.length() - 1) != CR); // last character not a CR

      String frames = framesBuffer.toString();

      // break the data read into response frames
      StringBuffer responseBuffer = new StringBuffer();
      for (int i = 0; i < frames.length(); i++)
      {
        char c = frames.charAt(i);
        if (c == CR)
        {
          String response = responseBuffer.toString();          

          boolean status = _responseQueue.offer(response);
          Assert.expect(status);

          responseBuffer.delete(0, responseBuffer.length());
        }
        else
        {
          responseBuffer.append(c);
        }
      }
    }
    Assert.expect(_responseQueue.isEmpty() == false);

    String result = _responseQueue.remove();    
    return result;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private synchronized void flushUnexpectedInput() throws IOException
  {
    StringBuffer unexpectedInputBuffer = new StringBuffer();

    while (_responseQueue.isEmpty() == false)
    {
      unexpectedInputBuffer.append("queue=");
      unexpectedInputBuffer.append(_responseQueue.remove());
      unexpectedInputBuffer.append("<CR>");
    }

    int numBytesReady = _serialPort.getInputStream().available();
    if (numBytesReady > 0)
    {
      int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
      String input = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");
      unexpectedInputBuffer.append("port=");
      unexpectedInputBuffer.append(input.replaceAll(CR_RE, "<CR>"));
      unexpectedInputBuffer.append(';');
    }

    String unexpectedInput = unexpectedInputBuffer.toString();
    if ((unexpectedInput.length() > 0) && (unexpectedInput.startsWith("port=8A0,0000") == false))
    {
      String eol = System.getProperty("line.separator");
      String msg = "unexpected input on " + _portName + ": " + unexpectedInput;

      StringBuffer stackTraceBuffer = new StringBuffer(msg);
      stackTraceBuffer.append(eol);
      StringWriter sout = new StringWriter();
      PrintWriter out = new PrintWriter(sout);
      Throwable isx = new Throwable(); // NOT for throwing...
      isx.printStackTrace(out); // ...just want the stack trace (but just the first few lines)
      out.flush();
      BufferedReader in = new BufferedReader(new StringReader(sout.toString()));
      String line = in.readLine(); // skip first line with meaningless Exception message
      while ((line = in.readLine()) != null)
      {
        if (line.contains("com.axi.v810.hardware.HTubeXraySource"))
        {
          stackTraceBuffer.append(line);
          stackTraceBuffer.append(eol);
        }
        else
        {
          break;
        }
      }      
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private synchronized String simulatedSendAndReceive(String request)
  {
    Assert.expect(isSimulationModeOn());
    String command = request.substring(0, 2);
    Assert.expect(_commandAndResponseMap.containsKey(command));    
    
    // special commands
    String response = _commandAndResponseMap.get(command).get(0);
    if (command.equals("0e"))
    {
        response = _simulatedResponseForAxiUsage;
    }
    else if (command.equals("10"))
    {
        response = _simulatedResponseForInitializeProjector1;
    }
    else if (command.equals("11"))
    {
        response = _simulatedResponseForStartProjector1Sequence;
    }
    else if (command.equals("b0"))
    {
        response = _simulatedResponseForAllProjectorOn;
    }
    else if (command.equals("b5"))
    {
        response = _simulatedResponseForAllProjectorOff;
    }
    else if (command.equals("a0"))
    {
        response = _simulatedResponseForControllerStatus;
    }
    else if (command.equals("c0"))
    {
        response = _simulatedResponseForAllProjectorLEDEnable;
    }
    else if (command.equals("c1"))
    {
        response = _simulatedResponseForProjectorOneLEDEnable;
    }
    else if (command.equals("c2"))
    {
        response = _simulatedResponseForProjectorTwoLEDEnable;
    }
    else if (command.equals("c5"))
    {
        response = _simulatedResponseForAllProjectorLEDDisable;
    }
    else if (command.equals("c6"))
    {
        response = _simulatedResponseForProjectorOneLEDDisable;
    }
    else if (command.equals("c7"))
    {
        response = _simulatedResponseForProjectorTwoLEDDisable;
    }
    else if (command.equals("f0"))
    {
      response = _simulatedResponseForMicroControllerFirmwareVersion;
    }

    // the real hardware on a real serial port takes almost exactly .2 seconds
    //  with a very small standard deviation between samples (except for the
    //  first call after the JVM start up which takes a little over 5 seconds)
    // HOWEVER, calling this sleep is breaking regression tests that have
    //  timing dependencies, like Test_HardwareTaskEngine...
    if (_simulateLinkTiming)
      sleep(200);

    return response;
  }

  /**
   * set a specific response for Ready
   * @author Cheah Lee Herng
   */
  public void setSimulatedResponseForReady(String response)
  {
    _simulatedResponseForControllerStatus = response;
  }

  /*
   * @author Cheah Lee Herng
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
}
