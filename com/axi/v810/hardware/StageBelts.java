package com.axi.v810.hardware;

import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the stage belts that moves the panel.
 * @author Rex Shang
 */
public class StageBelts extends HardwareObject implements StageBeltInt
{
  private static StageBelts _instance;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private static Config _config = Config.getInstance();

  /**
   * @author Rex Shang
   */
  private StageBelts()
  {
    _instance = null;
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  /**
   * @return StageBelt an instance of this class.
   * @author Rex Shang
   */
  public static synchronized StageBelts getInstance()
  {
    if (_instance == null)
      _instance = new StageBelts();

    return _instance;
  }

  /**
   * Set the stage belt direction from left to right.
   * @author Rex Shang
   */
  public void setDirectionLeftToRight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageBeltsEventEnum.BELTS_DIRECTION_LEFT_TO_RIGHT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.setStageBeltsDirectionLeftToRight();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageBeltsEventEnum.BELTS_DIRECTION_LEFT_TO_RIGHT);
  }

  /**
   * Sets the stage belt direction from right to left.
   * @author Rex Shang
   */
  public void setDirectionRightToLeft() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageBeltsEventEnum.BELTS_DIRECTION_RIGHT_TO_LEFT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.setStageBeltDirectionRightToLeft();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageBeltsEventEnum.BELTS_DIRECTION_RIGHT_TO_LEFT);
  }

  /**
   * @return boolean ture if the direction of the belts are left to right.
   * @author Rex Shang
   */
  public boolean areStageBeltsDirectionLeftToRight() throws XrayTesterException
  {
    return _digitalIo.areStageBeltsDirectionLeftToRight();
  }

  /**
   * Sets the stage belt in motion.
   * @author Rex Shang
   */
  public void on() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageBeltsEventEnum.BELTS_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnStageBeltsOn();

      // Wait for the belt to move.
      _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getStageBeltsStartMovingTimeInMilliSeconds());

      // Check the belt state.
      if (areStageBeltsOn() == false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToTurnOnStageBeltsException());
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageBeltsEventEnum.BELTS_ON);
  }

  /**
   * Stops the stage belt.
   * @author Rex Shang
   */
  public void off() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageBeltsEventEnum.BELTS_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnStageBeltsOff();

      // Wait for the belt to move.
      _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getStageBeltsStopMovingTimeInMilliSeconds());

      // Check the belt state.
      if (areStageBeltsOn())
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToTurnOffStageBeltsException());
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageBeltsEventEnum.BELTS_OFF);
  }

  /**
   * @return boolean true if the stage belts are currently moving.
   * @author Rex Shang
   */
  public boolean areStageBeltsOn() throws XrayTesterException
  {
    return _digitalIo.areStageBeltsOn();
  }

  /**
   * @author George A. David
   */
  public static int getNominalBeltThicknessInNanometers()
  {
    return _config.getIntValue(HardwareConfigEnum.NOMINAL_BELT_THICKNESS_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMaxBeltThicknessToleranceInNanoMeters()
  {
    return _config.getIntValue(HardwareConfigEnum.MAX_BELT_THICKNESS_TOLERANCE_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMinBeltThicknessToleranceInNanoMeters()
  {
    return _config.getIntValue(HardwareConfigEnum.MIN_BELT_THICKNESS_TOLERANCE_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMaxBeltThicknessWearAndTearLossInNanoMeters()
  {
    return _config.getIntValue(HardwareConfigEnum.MAX_BELT_THICKNESS_WEAR_AND_TEAR_LOSS_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getBeltRestingSurfaceDistanceBelowSystemCouponInNanoMeters()
  {
    return _config.getIntValue(HardwareConfigEnum.BELT_RESTING_SURFACE_DISTANCE_FROM_SYSTEM_COUPON_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMinumumBeltTopDistanceBelowSystemCouponInNanoMeters()
  {
    return getBeltRestingSurfaceDistanceBelowSystemCouponInNanoMeters() -
           (getNominalBeltThicknessInNanometers() + getMaxBeltThicknessToleranceInNanoMeters());
  }

  /**
   * @author George A. David
   */
  public static int getMaximumBeltTopDistanceBelowSystemCouponInNanoMeters()
  {
    return getBeltRestingSurfaceDistanceBelowSystemCouponInNanoMeters() -
           (getNominalBeltThicknessInNanometers() - getMaxBeltThicknessToleranceInNanoMeters() - getMaxBeltThicknessWearAndTearLossInNanoMeters());
  }

  /**
   * @author George A. David
   */
  public static int getNominalBeltTopDistanceBelowSystemCouponInNanoMeters()
  {
    return getBeltRestingSurfaceDistanceBelowSystemCouponInNanoMeters() -
           getNominalBeltThicknessInNanometers();
  }
}
