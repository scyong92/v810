package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
 * @author Greg Esparza
 */
public class AxiTdiXrayCameraBoardVersionEnum extends com.axi.util.Enum implements Serializable
{
  private static final int _VERSION_NUMBER_OF_CHARACTERS = 6;
  private static final String _VERSION_FIELD_DELIMITER = "\\.";
  private static Map<String, AxiTdiXrayCameraBoardVersionEnum> _versionToEnumMap = new HashMap<String, AxiTdiXrayCameraBoardVersionEnum>();
  private static int _index = -1;
  private String _majorVersion;

  public static AxiTdiXrayCameraBoardVersionEnum BOARD_VERSION_A = new AxiTdiXrayCameraBoardVersionEnum(++_index, "A");
  public static AxiTdiXrayCameraBoardVersionEnum BOARD_VERSION_B = new AxiTdiXrayCameraBoardVersionEnum(++_index, "B");

  /**
   * @author Greg Esparza
   */
  private AxiTdiXrayCameraBoardVersionEnum(int id, String majorVersion)
  {
    super(id);

    Assert.expect(majorVersion != null);
    _majorVersion = majorVersion;
    _versionToEnumMap.put(majorVersion, this);
  }

  /**
   * @author Greg Esparza
   */
  public static AxiTdiXrayCameraBoardVersionEnum getEnum(String version)
  {
    Assert.expect(version != null);

    // The format for the version [A-Z].[XXXX] where [A-Z] is the major version field
    // and [XXXX] is the Engineering Date Code (EDC) [Month Month Day Day].  The EDC
    // value is a standard that is used for Printed Circuit Board Assembly version control.
    Assert.expect(version.length() == _VERSION_NUMBER_OF_CHARACTERS);

    // Since we only care about the Major Version field we will only return the enum associated with it.
    String majorVersion = getMajorVersionName(version);

    AxiTdiXrayCameraBoardVersionEnum versionEnum = _versionToEnumMap.get(majorVersion);
    Assert.expect(versionEnum != null);

    return versionEnum;
  }

  /**
   * @author Greg Esparza
   */
  static boolean isBoardVersionSupported(String version)
  {
    Assert.expect(version != null);

    String majorVersion = getMajorVersionName(version);
    return _versionToEnumMap.containsKey(majorVersion);
  }

  /**
   * @author Greg Esparza
   */
  static String getMajorVersionName(String actualVersion)
  {
    Assert.expect(actualVersion != null);
    String[] versionFields = actualVersion.split(_VERSION_FIELD_DELIMITER);
    Assert.expect(versionFields.length == 2);

    return versionFields[0];
  }
  
  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public String toString()
  {
    return _majorVersion;
  }
}
