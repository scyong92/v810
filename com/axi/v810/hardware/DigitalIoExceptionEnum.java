package com.axi.v810.hardware;

/**
 * Used by DigitalIoException to help populate the exception message.
 * @author Rex Shang
 */
public class DigitalIoExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static DigitalIoExceptionEnum LEFT_OUTER_BARRIER_FAILED_TO_OPEN = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum LEFT_OUTER_BARRIER_FAILED_TO_CLOSE = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum LEFT_OUTER_BARRIER_IS_STUCK = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum RIGHT_OUTER_BARRIER_FAILED_TO_OPEN = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum RIGHT_OUTER_BARRIER_FAILED_TO_CLOSE = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum RIGHT_OUTER_BARRIER_IS_STUCK = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_TURN_ON_STAGE_BELTS = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_TURN_OFF_STAGE_BELTS = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_EXTEND_LEFT_PANEL_IN_PLACE_SENSOR = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_RETRACT_LEFT_PANEL_IN_PLACE_SENSOR = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_EXTEND_RIGHT_PANEL_IN_PLACE_SENSOR = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_RETRACT_RIGHT_PANEL_IN_PLACE_SENSOR = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_OPEN_PANEL_CLAMPS = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_CLOSE_PANEL_CLAMPS = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum INNER_BARRIER_FAILED_TO_OPEN = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum INNER_BARRIER_FAILED_TO_CLOSE = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum FAILED_TO_SET_INNER_BARRIER_BIT = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum INNER_BARRIER_IS_STUCK = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum NATIVE_HARDWARE_EXCEPTION = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum RMI_EXCEPTION = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum DIGITAL_IO_NOT_INITIALIZED = new DigitalIoExceptionEnum(++_index);
	
  //Variable Mag Anthony August 2011
  public static DigitalIoExceptionEnum XRAY_CYLINDER_FAILED_TO_GO_DOWN = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_CYLINDER_FAILED_TO_GO_UP = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_CYLINDER_IS_STUCK = new DigitalIoExceptionEnum(++_index);

  //XXL Optical Stop Sensor - Anthony (May 2013)
  public static DigitalIoExceptionEnum DIGITAL_IO_OPTICAL_STOP_SENSOR_NOT_INITIALIZED = new DigitalIoExceptionEnum(++_index);
  
  //S2EX Anthony 2014
  public static DigitalIoExceptionEnum XRAY_Z_AXIS_FAILED_TO_GO_HOME = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_Z_AXIS_FAILED_TO_GO_LOW_MAG = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_Z_AXIS_FAILED_TO_GO_HIGH_MAG = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_Z_AXIS_IS_STUCK = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum DIGITAL_IO_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_MAGNIFICATION_INVALID = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum DIGITAL_IO_FAILED_TO_SET_Z_AXIS_POSITION_BIT = new DigitalIoExceptionEnum(++_index);
 
  //S2EX Anthony 2014
  public static DigitalIoExceptionEnum XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HOME = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_LOW_MAG = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HIGH_MAG = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_IS_STUCK = new DigitalIoExceptionEnum(++_index);
  public static DigitalIoExceptionEnum DIGITAL_IO_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOTOR_POSITION_LOOKUP_FOR_MAGNIFICATION_INVALID = new DigitalIoExceptionEnum(++_index);

  //New Optical Stop Sensor - Anthony (2015)
  public static DigitalIoExceptionEnum DIGITAL_IO_OPTICAL_STOP_CONTROLLER_END_STOPPER_RESUMED_AS_NORMAL_PIP_NOT_INITIALIZED = new DigitalIoExceptionEnum(++_index);
  // XCR 3656: System crash when load S2EX V810 application if opticalStopControllerEndStopperInstalled=false
  public static DigitalIoExceptionEnum DIGITAL_IO_OPTICAL_STOP_SENSOR_SETTINGS_INVALID = new DigitalIoExceptionEnum(++_index);
 
  /**
   * @author Rex Shang
   */
  private DigitalIoExceptionEnum(int id)
  {
    super(id);
  }

}
