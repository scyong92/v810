package com.axi.v810.hardware;

import java.lang.reflect.*;
import java.io.*;
import java.util.*;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * debugging aid: dumb terminal interface (with command completion)
 *
 * @author Greg Loring
 */
public class TestMan_AbstractXraySource implements Observer, RunnableWithExceptions
{
  private static ProgressObservable _progressObservable = ProgressObservable.getInstance();

  // put the full set of possible commands in one place (for command completion)
  private final Set<String> _allCommands;

  // what methods can you call on the legacy X-ray source?
  private final Map<String, Method> _doerMap; // public void ...()
  private final Map<String, Method> _testerMap; // public boolean ...()
  private final Map<String, Method> _getterMap; // public double get...()
  private final Map<String, Method> _setterMap; // public void set...(double)

  // these methods update ProgressObservable &/| implement abort functionality
  private final Set<String> _slowDoerMethods;

  // the X-ray source to command
  private AbstractXraySource _xraySource;

  // the commands are mostly method names, so they are long for a CLI:
  //  provide command completion
  private String _cmdPrefix;

  private boolean _stopAbort = false;
  private boolean _doPoll = false;

  /**
   * dumb terminal style interface (with command completion) for rapid debugging
   *
   * @author Greg Loring
   */
  public static void main(String[] args) throws Exception
  {
    // parse command line args
    if (args.length > 0)
    {
      System.err.println("usage: " + TestMan_AbstractXraySource.class.getName() + " [-h|--help]");
      System.exit(1);
    }

    Config.getInstance().loadIfNecessary(); // only call the config once, so don't persist singleton instance

    LegacyXraySource.simulateTiming();
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    TestMan_AbstractXraySource cmdLineUI = new TestMan_AbstractXraySource(xraySource);
    HardwareWorkerThread.getInstance().invokeAndWait(cmdLineUI);
  }

  /**
   * @author Greg Loring
   */
  public synchronized void update(Observable observable, Object report)
  {
    Expect.expect(observable == _progressObservable);
    Expect.expect(report instanceof ProgressReport);
    println("progress update: " + report);
  }

  /**
   * main()-only
   *
   * @author Greg Loring
   */
  private TestMan_AbstractXraySource(AbstractXraySource xraySource)
  {
    _xraySource = xraySource;
    _cmdPrefix = "";

    // put the full set of possible commands in one place (for command completion)
    _allCommands = new TreeSet<String>();
    _allCommands.add("exit");
    _allCommands.add("quit");
    _allCommands.add("help");
    _allCommands.add("?");

    // what methods can you call on the legacy X-ray source?
    _doerMap = new TreeMap<String, Method>(); // public void ...()
    _testerMap = new TreeMap<String, Method>(); // public boolean ...()
    _getterMap = new TreeMap<String, Method>(); // public double get...()
    _setterMap = new TreeMap<String, Method>(); // public void set...(double)
    for (Method method : _xraySource.getClass().getMethods())
    {
      // getMethods() returns public member functions, so no public test needed
      if (method.getParameterTypes().length == 0)
      {
        if (method.getReturnType() == boolean.class)
        {
          addMethod(method, _testerMap);
        }
        else if (method.getName().startsWith("get"))
        {
          if (   (method.getReturnType() == double.class)
              || (method.getReturnType() == int.class)
              || (method.getReturnType() == String.class)
              || method.getName().equals("getHardwareReportedErrors"))
          {
            addMethod(method, _getterMap);
          }
        }
        else if (method.getReturnType() == void.class)
        {
          // avoid the doers from Object
          String name = method.getName();
          if (  (name.equals("notify") == false)
             && (name.equals("notifyAll") == false)
             && (name.equals("wait") == false))
          {
            addMethod(method, _doerMap);
          }
        }
        else if (   method.getName().equals("selfTest") // selfTest doer is weird 'cuz it returns a String
                 && (method.getReturnType() == String.class)
                 && (method.getParameterTypes().length == 0))
        {
          addMethod(method, _doerMap);
        }
      }
      else if (   (method.getParameterTypes().length == 1)
               && (method.getParameterTypes()[0] == double.class))
      {
        addMethod(method, _setterMap);
      }
      // no need for a final else: if it don't match, i'm not interested in it
    }

    // these doer methods update ProgressObservable &/| implement abort functionality
    _slowDoerMethods = new LinkedHashSet<String>();
    _slowDoerMethods.add("startup");
    _slowDoerMethods.add("on");
    _slowDoerMethods.add("selfTest"); // Genesis standard XraySource-only
  }

  /**
   * @author Greg Loring
   */
  private void addMethod(Method method, Map<String, Method> methodMap)
  {
    String name = method.getName();

    Assert.expect(methodMap.containsKey(name) == false);
    methodMap.put(method.getName(), method);

    Assert.expect(_allCommands.contains(name) == false);
    _allCommands.add(method.getName());
  }

  /**
   * @author Greg Loring
   */
  public void run() throws Exception
  {
    // register as a progress observer so i can print progress updates to System.out
    _progressObservable.addObserver(this);

    new Thread(new Runnable()
    {
      public void run()
      {
        runPollingThread();
      }
    }).start();

    // "dumb terminal" input reader
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "US-ASCII"));

    println("Enter ? for help.");

    // "dumb terminal" command loop
    while (true)
    {
      printPrompt();

      // read command line (with command completion)
      String line = in.readLine();
      if (line == null)
      {
        break;
      }
      line = _cmdPrefix + line;
      String cmd;
      String arg;
      if (line.indexOf(' ') != -1)
      {
        int spaceIndex = line.indexOf(' ');
        cmd = line.substring(0, spaceIndex);
        arg = line.substring(spaceIndex + 1).trim();
      }
      else
      {
        cmd = line;
        arg = null;
      }
      cmd = completeCommand(cmd, arg);
      if (cmd == null)
      {
        continue;
      }

      // parse and execute command line
      if (cmd.equals("?") || cmd.equals("help"))
      {
        printHelp();
      }
      else if (cmd.equals("exit") || cmd.equals("quit"))
      {
        System.exit(0);
      }
      else
      {
        try
        {
          if (cmd.equals("getHardwareReportedErrors")) // one-off method gets one-off treatment
          {
            XraySourceHardwareReportedErrorsException xshrex = _xraySource.getHardwareReportedErrors();
            if (xshrex != null)
            {
              println(xshrex.getMessage());
            }
            else
            {
              println("The X-ray source hardware is NOT reporting any errors...");
            }
          }
          else if (_doerMap.containsKey(cmd))
          {
            callDoer(_doerMap.get(cmd), in);
          }
          else if (_testerMap.containsKey(cmd))
          {
            callMethod(_testerMap.get(cmd));
          }
          else if (_getterMap.containsKey(cmd))
          {
            callMethod(_getterMap.get(cmd));
          }
          else if (_setterMap.containsKey(cmd))
          {
            callMethod(_setterMap.get(cmd), new Double(arg));
          }
          else
          {
            Assert.expect(false, "completeCommand() should prevent invalid commands getting here");
          }
        }
        catch (XrayTesterException xtx)
        {
          printStackTrace(xtx);
        }
      }
    }
  }

  /**
   * @author Greg Loring
   */
  private void printHelp()
  {
    println("?|help");
    println("exit|quit");
    printReflectedMethods();
  }

  /**
   * @author Greg Loring
   */
  private void printReflectedMethods()
  {
    printReflectedMethods("doers", _doerMap, null);
    printReflectedMethods("testers", _testerMap, null);
    printReflectedMethods("getters", _getterMap, null);
    printReflectedMethods("setters", _setterMap, "<double>");
    println("*: marks slow methods (* is NOT part of the command)");
  }

  /**
   * @author Greg Loring
   */
  private void printReflectedMethods(String category, Map<String, Method> methodMap, String argProto)
  {
    print(category);
    println(":");
    for (Method method : methodMap.values())
    {
      printReflectedMethod(method, argProto);
    }
  }

  /**
   * @author Greg Loring
   */
  private void printReflectedMethod(Method method, String argProto)
  {
    print("  ");
    if (_slowDoerMethods.contains(method.getName()))
    {
      print("*");
    }
    print(method.getName());
    if (argProto != null)
    {
      print(" ");
      print(argProto);
    }
    println();
  }

  /**
   * @author Greg Loring
   */
  private void printPrompt()
  {
    print(_xraySource.getClass().getSimpleName());
    print("? ");
    print(_cmdPrefix);
  }

  /**
   * @author Greg Loring
   */
  private String completeCommand(String cmd, String arg)
  {
    if (_allCommands.contains(cmd))
    {
      _cmdPrefix = "";
    }
    else
    {
      // second pass: find a partial match(es)
      Set<String> partialMatches = new TreeSet<String>();
      for (String fullCmd : _allCommands)
      {
        if (fullCmd.startsWith(cmd))
        {
          partialMatches.add(fullCmd);
        }
      }

      if (partialMatches.isEmpty())
      {
        // no partial matches
        _cmdPrefix = "";
        cmd = null;
        printHelp();
      }
      else if (partialMatches.size() == 1)
      {
        // one partial match
        cmd = partialMatches.iterator().next();

        // check if argument is required, and if so, is it absent?
        if (_setterMap.containsKey(cmd) && (arg == null))
        {
          // required argument is missing
          _cmdPrefix = cmd + " ";
          cmd = null;
        }
        else
        {
          // return the full command
          _cmdPrefix = "";
          print(cmd);
          if (arg != null)
            print(" " + arg);
          println();
        }
      }
      else
      {
        // found more than one partial matches
        printPossibilities(partialMatches);
        _cmdPrefix = stretchPartialMatch(cmd, partialMatches);
        cmd = null;
      }
    }

    return cmd;
  }

  /**
   * @author Greg Loring
   */
  private void printPossibilities(Set<String> partialMatches)
  {
    int col = 0;
    for (String fullCmd : partialMatches)
    {
      if ((col + fullCmd.length()) > 80)
      {
        println();
        col = 0;
      }
      print(fullCmd);
      print(' ');
      col += fullCmd.length() + 1;
    }
    println();
  }

  /**
   * @author Greg Loring
   */
  private String stretchPartialMatch(String cmd, Set<String> partialMatches)
  {
    StringBuffer sb = new StringBuffer(cmd);
    loop: while (true)
    {
      char commonChar = 0;
      for (String fullCmd : partialMatches)
      {
        if (fullCmd.length() < (sb.length() + 1))
        {
          break loop;
        }
        char c = fullCmd.charAt(sb.length());
        if (commonChar == 0)
        {
          commonChar = c;
        }
        if (c != commonChar)
        {
          break loop;
        }
      }
      sb.append(commonChar);
    }
    String result = sb.toString();
    return result;
  }

  /**
   * @author Greg Loring
   */
  private void callDoer(Method method, BufferedReader in) throws XrayTesterException
  {
    if (_slowDoerMethods.contains(method.getName()))
    {
      callSlowDoer(method, in);
    }
    else
    {
      callMethod(method);
    }
    sleep(300); // give a chance for any linering progress reports or stdout writes to complete
  }

  /**
   * @author Greg Loring
   */
  private void callSlowDoer(Method method, BufferedReader in) throws XrayTesterException
  {
    Thread abortThread = null;
    print("call abort() # seconds after calling " + method.getName() + "() [#/N]? ");
    try
    {
      String response = in.readLine();
      if ((response.length() == 1) && (Character.isDigit(response.charAt(0))))
      {
        final int n = response.charAt(0) - '0';
        _stopAbort = false;
        abortThread = new Thread(new Runnable()
        {
          public void run()
          {
            runAbortInNSeconds(n);
          }
        });
        abortThread.start();
      }
    }
    catch (Exception x)
    {
      Assert.expect(false, "unexpected " + x);
    }

    try
    {
      _doPoll = true;
      callMethod(method);
    }
    finally
    {
      _stopAbort = true;
      _doPoll = false;

      if (abortThread != null)
      {
        join(abortThread);
      }

      // give the observer thread a chance to spit out the last progress update,
      //  so it doesn't get mixed up with the next prompt
      sleep(300);
    }
  }

  /**
   * reflection method invocation exception handling
   *  and slow method watchdog thread (w/optional abort)
   *
   * @author Greg Loring
   */
  private void callMethod(Method method, Object... args) throws XrayTesterException
  {
    try
    {
      Object result = method.invoke(_xraySource, args);
      if (result != null)
      {
        println(result);
      }
    }
    catch (InvocationTargetException itx)
    {
      if (itx.getCause() instanceof XrayTesterException)
      {
        throw (XrayTesterException) itx.getCause();
      }
      else
      {
        throw new IllegalStateException("unexpected", itx.getCause());
      }
    }
    catch (Exception x)
    {
      throw new IllegalStateException("unexpected", x);
    }
  }

  /**
   * @author Greg Loring
   */
  private void runAbortInNSeconds(final int n)
  {
    // "interruptable" wait for about n seconds
    final int intervals = 10;
    for (int i = 0; i < intervals; i++)
    {
       sleep((n * 1000) / intervals);
       if (_stopAbort)
       {
         break;
       }
    }

    // call abort (unless "slow" method already finished)
    if (_stopAbort)
    {
      println("NOT calling abort()");
    }
    else
    {
      print("calling abort()...");
      try
      {
        _xraySource.abort();
        println("DONE");
      }
      catch (XrayTesterException xtx)
      {
        println("EXCEPTION!");
        xtx.printStackTrace();
      }
    }
  }

  private void runPollingThread()
  {
    while (true)
    {
      if (_doPoll)
      {
        try
        {
          double kV = _xraySource.getAnodeVoltageInKiloVolts();
          double uA = _xraySource.getCathodeCurrentInMicroAmps();
          println("anode [kV] = " + kV + ", cathode [uA] = " + uA);
        }
        catch (XrayTesterException xtx)
        {
          throw new IllegalStateException(xtx);
        }
      }
      else
        sleep(200);
    }
  }

  /**
   * sleep for millis no matter how many times i'm interrupted
   *
   * @author Greg Loring
   */
  private void sleep(long millis)
  {
    long start = System.currentTimeMillis();
    while (true)
    {
      long elapsed = System.currentTimeMillis() - start;
      if (elapsed >= millis)
      {
        break;
      }
      else
      {
        try
        {
          Thread.sleep(millis - elapsed);
          break;
        }
        catch (InterruptedException ix)
        {
          // just go around again
        }
      }
    }
  }

  /**
   * join the thread no matter how many times i'm interrupted
   *
   * @author Greg Loring
   */
  private void join(Thread thread)
  {
    Assert.expect(thread != null);

    while (true)
    {
      try
      {
        thread.join();
        break;
      }
      catch (InterruptedException ix)
      {
        // just go around again
      }
    }
  }

  /**
   * have multiple threads writing to System.out
   *
   * @author Greg Loring
   */
  private synchronized void print(Object obj)
  {
    System.out.print(obj);
  }

  /**
   * have multiple threads writing to System.out
   *
   * @author Greg Loring
   */
  private synchronized void println(Object obj)
  {
    System.out.println(obj);
  }

  /**
   * have multiple threads writing to System.out
   *
   * @author Greg Loring
   */
  private synchronized void println()
  {
    System.out.println();
  }

  /**
   * have multiple threads writing to System.out
   *
   * @author Greg Loring
   */
  private synchronized void printStackTrace(Throwable t)
  {
    t.printStackTrace(System.out);
  }

}
