package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;

public class AdjustmentExceptionEnum extends HardwareTaskExceptionEnum
{
  private static HashMap<AdjustmentExceptionEnum, LocalizedString> _enumToStringMap;
  private static int _index = -1;

  public static AdjustmentExceptionEnum PROBLEM_DURING_SEGMENT_DARK_CAL_STEP =
      new AdjustmentExceptionEnum(++_index);
  public static AdjustmentExceptionEnum PROBLEM_DURING_SEGMENT_LIGHT_CAL_STEP =
      new AdjustmentExceptionEnum(++_index);
  public static AdjustmentExceptionEnum PROBLEM_DURING_PIXEL_DARK_CAL_STEP =
      new AdjustmentExceptionEnum(++_index);
  public static AdjustmentExceptionEnum PROBLEM_DURING_PIXEL_LIGHT_CAL_STEP =
      new AdjustmentExceptionEnum(++_index);


  //Initialize the map of enum's to localized strings
  static
  {
     _enumToStringMap =
      new HashMap<AdjustmentExceptionEnum, LocalizedString>(_index + 1);

     Assert.expect(_enumToStringMap.put(PROBLEM_DURING_SEGMENT_DARK_CAL_STEP, new LocalizedString(
      "ACD_PROBLEM_DURING_SEGMENT_DARK_CAL_STEP_KEY", null)) == null);
     Assert.expect(_enumToStringMap.put(PROBLEM_DURING_SEGMENT_LIGHT_CAL_STEP, new LocalizedString(
      "ACD_PROBLEM_DURING_SEGMENT_LIGHT_CAL_STEP_KEY", null)) == null);
     Assert.expect(_enumToStringMap.put(PROBLEM_DURING_PIXEL_DARK_CAL_STEP, new LocalizedString(
      "ACD_PROBLEM_DURING_PIXEL_DARK_CAL_STEP_KEY", null)) == null);
     Assert.expect(_enumToStringMap.put(PROBLEM_DURING_PIXEL_LIGHT_CAL_STEP, new LocalizedString(
      "ACD_PROBLEM_DURING_PIXEL_LIGHT_CAL_STEP_KEY", null)) == null);

     Assert.expect(_enumToStringMap.size() == _index + 1);
   }

  /**
   * @author Reid Hayhow
   */
  private AdjustmentExceptionEnum(int id)
  {
    super(id);
  }

  public LocalizedString getLocalizedString()
  {
    LocalizedString myString = _enumToStringMap.get(this);

    Assert.expect(myString != null);
    return myString;
  }
}
