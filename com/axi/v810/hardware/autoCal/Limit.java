package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;

/**
 * Contains the limits for a particular C&D test.  Each C&D test should create its
 * own Limits object with values that are needed for that particular test.
 *
 * @author  Bill Darbie
 */
public class Limit
{
  private Object _low;
  private Object _high;
  private String _units;
  private List<Limit> _limitList = new ArrayList<Limit>();
  private String _limitName = null;
  /** @todo wpd - Reid - create a LimitGroup and remove this _limitList */

  /**
   * Creates a limits object. The low, high and nominal parameters must be the
   * same class type, which must be a subclass of type Comparable. The only
   * exception unless the test is a go/nogo test that will be returning a
   * Boolean result type, in which case the low, high, nominal and units must
   * all be null.
   *
   * @param low is the lower limit. If this is null then the result will not be
   *   checked against any lower limit.
   * @param high is the upper limit. If this is null then the result will not
   *   be checked against any upper limit.
   * @param units is a description of what the units of the limits are
   * @param limitName String
   * @author Bill Darbie
   */
  public Limit(Object low, Object high, String units, String limitName)
  {
    // low, high, nominal and units must all be null if the result is type Boolean
    // this is checked in didTestPass when the Results type is known

    // make sure low and high are the same types (for a Comparable result case)
    if ((low != null) && (high != null))
    {
      Assert.expect(low.getClass().getName().equals(high.getClass().getName()));
    }

    // for a Boolean result case, if low and high are null, units must be null
    // NOTE: The name can/should still have a value
    if ((low == null) && (high == null))
    {
      Assert.expect(units == null);
    }
    _low = low;
    _high = high;
    _units = units;
    _limitName = limitName;
  }

  /**
   * @author Bill Darbie
   */
  Object getLowLimit()
  {
    return _low;
  }

  /**
   * @author Bill Darbie
   */
  Object getHighLimit()
  {
    return _high;
  }

  /**
   * @author Bill Darbie
   */
  String getUnits()
  {
    return _units;
  }

  /**
   * Given the Results object passed in determine if the test passed or failed.
   * The result must be either of type Comparable or type Boolean. For a result
   * of type Comparable, if the result is within or equal to the low or high
   * limits the test is considered to pass. If the high or low limit is set to
   * null the result is not compared against that limit. For a result of type
   * Boolean, this Limits object must have it low, high, nominal and units
   * properties all set to null. For these tests, if a HardwareException
   * indicates a failure, then it can be trapped in the test and force the test
   * to Fail.
   *
   * @param results contains all the result information of the test that was run
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public boolean didTestPass(Result results)
  {
    // the test had better be run before this method is called
    boolean pass = true;

    Assert.expect(results != null);
    if (results.getStatus().equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED))
    {
      return false;
    }

    //If a test is deferred, it is in limbo, it didn't really pass but it certainly
    //didn't fail
    if(results.getStatus().equals(HardwareTaskStatusEnum.DEFERRED))
    {
      return true;
    }

    //If we have a list of limits and results, check the values of the list and
    //not this 'container' result
    if (_limitList.size() == results.getResultList().size() && _limitList.size() != 0)
    {
      //Assure that nobody made a mistake and set the 'container' limit or result
      //with a value
      Assert.expect(_high == null);
      Assert.expect(_low == null);
      Assert.expect(_units == null);

      Assert.expect(results.getResult() == null);

      //For simplicity, it is easier to get the list from the results and use it
      //for looping
      List<Result> resultList = results.getResultList();

      for (int i = 0; i < _limitList.size(); i++)
      {
        pass &= _limitList.get(i).didTestPass(resultList.get(i));
      }
      // We will set the status of this result to reflect the status of all sub
      // results before returning the pass/fail
      if (pass)
      {
        results.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        results.setStatus(HardwareTaskStatusEnum.FAILED);
      }
      return pass;
    }
    //We only have one Result/Limit pair or we are at a leaf of the lists
    else
    {
      Assert.expect(_limitList.size() == 0);
      Assert.expect(results.getResultList().size() == 0);
    }

    Object obj = results.getResult();
    Assert.expect(obj != null);

    if (obj instanceof Boolean)
    {
      // We have an object that is a Boolean type
      // This is only OK if _low, _nominal, _high and _units are all null
      Assert.expect(_low == null);
      Assert.expect(_high == null);
      Assert.expect(_units == null);

      Boolean result = (Boolean)obj;
      pass = result.booleanValue();
    }
    else if (obj instanceof Comparable)
    {
      // _high and _low should not BOTH be null, instead use a Boolean type result!
      if ((_high == null) && (_low == null))
      {
        Assert.expect(false);
      }

      // For type Comparable, both Limits and Results must not be null and must be the same
      Assert.expect(_units != null);

      Comparable result = (Comparable)obj;

      // if _high OR _low is null there is no limit to compare to
      // the limit class must match the result class
      //
      if (_high != null)
      {
        Assert.expect(_high.getClass().getName().equals(result.getClass().getName()));
        if (result.compareTo(_high) > 0)
        {
          pass = false;
        }
      }

      if (_low != null)
      {
        Assert.expect(_low.getClass().getName().equals(result.getClass().getName()));
        if (result.compareTo(_low) < 0)
        {
          pass = false;
        }
      }
    }
    else
    {
      // obj is not a supported type, must be either Comparable or Boolean
      Assert.expect(false, "Result type is not Comparable or Boolean.");
    }

    if (pass)
    {
      results.setStatus(HardwareTaskStatusEnum.PASSED);
    }
    else
    {
      results.setStatus(HardwareTaskStatusEnum.FAILED);
    }

    return pass;
  }

  public void addSubLimit(Limit limit)
  {
    _limitList.add(limit);
  }

  /**
   * Get the list of Limits for this Limit object.
   *
   * @author Reid Hayhow
   */
  public List<Limit> getLimitList()
  {
    return _limitList;
  }

  /**
   *
   * @author Reid Hayhow
   */
  public String getLimitName()
  {
    return _limitName;
  }

  /**
   * Factory method to create a simple limit for a task that should complete.
   *
   * @author Reid Hayhow
   */
  public static Limit createShouldPassLimit()
  {
    return new Limit(null, null, null, "NoLimit");
  }

  /**
   * toString
   *
   * @author Reid Hayhow
   */
  public String toString()
  {
    //Handle comparable limits differently than boolean limits
    if (_high != null && _low != null)
    {
      Assert.expect(_high instanceof Comparable);
      Assert.expect(_low instanceof Comparable);

      //return a string with the range
      return _high.toString() + " to " + _low.toString();
    }
    else
    {
      Assert.expect(_high == null && _low == null);

      //Indicate that the limit is boolean
      return "pass/fail";
    }
  }

}
