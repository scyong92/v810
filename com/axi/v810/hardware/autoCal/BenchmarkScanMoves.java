package com.axi.v810.hardware.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import java.io.File;

/**
 * This panel allows the user to define and run their scan paths.
 *
 * @author Erica Wheatcroft
 */
public class BenchmarkScanMoves
{
  private static BenchmarkScanMoves _benchmarkScanMoves = null;
  private PanelPositioner _panelPositioner = null;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private boolean _loopContinuosly = false;
  private int _numberOfLoops = 1;
  private int _delayBetweenScanPass = 0;
  private boolean _runOneScanPassAtATime = false;
  private boolean _runOneScanPassHasStarted = false;
  private boolean _scanCancelled = false;
  private boolean _independentAxisAreInSim = false;
  // flag used while the user is initiating the scan pass run.
  private boolean _scanInProgress = false;
  private int _indexOfRunningScanPass = 0;
  private MathUtilEnum _currentUnits = null;
  private boolean _aborted = false;
  private XrayTesterException _exception;
  private java.util.List<ScanPass> _scanPath = null;
  private HardwareTaskStatusEnum _result = HardwareTaskStatusEnum.NOT_RUN;
  private String _longPathScanFilePathForCDNABenchmark ="";
  
  /**
   * @author Anthony Fong
   */
  public HardwareTaskStatusEnum runScanPaths(int numberOfLoops) throws XrayTesterException
  {
    _result = HardwareTaskStatusEnum.NOT_RUN;
    try
    {
      _numberOfLoops = numberOfLoops;
      // first check to make sure the user has not disabled the x or y axis.
      if (areAnyAxesDisabled() == false)
      {
        _runOneScanPassAtATime = false;
        _result = runScanPath();
      }
      else
      {
        // they have an axis disabled, notify the user to enable it and return out of the function
        String message = "Please enable all axes and try again.";
        javax.swing.JOptionPane.showMessageDialog(null, StringUtil.format(message, 50), "Cannot Run Scan Path", javax.swing.JOptionPane.ERROR_MESSAGE);
        return HardwareTaskStatusEnum.NOT_RUN;
      }
    }
    catch (XrayTesterException xte)
    {
      String msg = xte.getMessage();
      //throw xte;
      abort(xte);
    }
    return _result;
  }


  /**
   * @author Greg Esparza
   */
  public static synchronized BenchmarkScanMoves getInstance()
  {
    if (_benchmarkScanMoves == null)
    {
      _benchmarkScanMoves = new BenchmarkScanMoves();
    }

    return _benchmarkScanMoves;
  }

  /**
   * @author Erica Wheatcroft
   */
  BenchmarkScanMoves()
  {
    try
    {
      start();
    }
    catch (XrayTesterException xte)
    {
      //throw xte;
      abort(xte);
    }
    _loopContinuosly = false;
    _numberOfLoops = 1;
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean areAnyAxesDisabled() throws XrayTesterException
  {
    Assert.expect(_panelPositioner != null);
    try
    {
      if (_panelPositioner.isXaxisEnabled() && _panelPositioner.isYaxisEnabled())
      {
        return false;
      }
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }

    return true;
  }

  /**
   * ready to run and should perform sync
   *
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public void start() throws XrayTesterException
  {
    _panelPositioner = PanelPositioner.getInstance();

    java.util.List<MotionControllerIdEnum> ids = MotionControllerIdEnum.getAllEnums();
    try
    {
      // now see if any of the independent axis are in sim mode
      if (MotionControl.getInstance(ids.get(0)).isStartupRequired() == false)
      {
        if (MotionControl.getInstance(ids.get(0)).isSimulationModeOn() == false)
        {
          if (MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS)
            || MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS))
          {
            _independentAxisAreInSim = true;
          }
          else
          {
            _independentAxisAreInSim = false;
          }
        }
        else
        {
          // since all of motion control is in sim we can run scan paths.
          _independentAxisAreInSim = false;
        }
      }
      else
      {
        // a start up is required so we can assume the independent axis are not in sim mode
        _independentAxisAreInSim = false;
      }
    }
    catch (XrayTesterException xte)
    {
      _independentAxisAreInSim = false;
      throw xte;
    }
    setMotionProfile();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setMotionProfile() throws XrayTesterException
  {
    try
    {
      if (_panelPositioner.isStartupRequired() == false)
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {

            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == false)
            {
              // now lets set up the scan profile. this is important so that the values entered by the user are
              // correctly validated.
              try
              {
                //Always use Scan Profile 0 (M19) for Benchmarking
                 _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
             
              }
              catch (XrayTesterException xte)
              {
                String msg = xte.getMessage();
                abort(xte);
              }
            }
          }
        });
      }
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void commandHardware(boolean firstScanPassRun, java.util.List<ScanPass> scanPath) throws XrayTesterException
  {
    for (ScanPass scanPass : scanPath)
    {
      if (_scanCancelled == false)
      {
        _panelPositioner.runNextScanPass();
      }
      try
      {
        if (firstScanPassRun)
        {
          Thread.sleep(_delayBetweenScanPass * 2);
        }
        else
        {
          Thread.sleep(_delayBetweenScanPass);
        }
      }
      catch (InterruptedException ie)
      {
        Assert.expect(false);
      }
      firstScanPassRun = false;
    }
    boolean scanPathDone = false;
    do
    {
      // NOTE: the actuall call runScanPass() is some what of a blocking call but not 100%. Once the hardware
      // starts moving the call returns. So we want to make sure the hardware is done before we disable
      // the scan path. Yes there is potential that this loop could be infinite if the hardware locks up. But
      // we want to have the panel positioner updated to rebort those problems.
      scanPathDone = _panelPositioner.isScanPathDone();
    }
    while (scanPathDone == false && _scanCancelled == false);
  }

  /**
   * @author Anthony Fong
   */
  private HardwareTaskStatusEnum runScanPath() throws XrayTesterException
  {
    Assert.expect(_panelPositioner != null);
    
    if (_scanPath == null)
    {
      return HardwareTaskStatusEnum.NOT_RUN;
    }
    _scanCancelled = false;

    try
    {
      // run the scan pass - check to make sure we are controlling when the next scan pass is ran
      Assert.expect(_runOneScanPassAtATime == false);

      // just run the scan path once.
      setupHardwareForScanPath(_scanPath);
      commandHardware(true, _scanPath);

    }
    catch (XrayTesterException xte)
    {
      return HardwareTaskStatusEnum.FAILED;
      //_parent.handleXrayTesterException(xte);
    }

    return HardwareTaskStatusEnum.PASSED;

  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupHardwareForScanPath(java.util.List<ScanPass> scanPath) throws XrayTesterException
  {
    // just encase an abort occured lets clear out an old one if it exists.
    _panelPositioner.disableScanPath();

    int distanceInNanoMetersPerPulse = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    int numberOfStartingPulses = cameraTrigger.getNumberOfRequiredSampleTriggers();
    // Swee Yee Wong - Include starting pulse information for scan pass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    // validate the scan pass data by enabling the scan path in panel positioner
    _panelPositioner.enableScanPath(scanPath, distanceInNanoMetersPerPulse, numberOfStartingPulses);
    
    _panelPositioner.setPositionPulse(distanceInNanoMetersPerPulse, numberOfStartingPulses);

    // If we abort right before panelPositioner.enableScanPath(), we need
    // to clear the state.
    if (_scanCancelled)
    {
      _panelPositioner.abort();
    }
  }
    
  /**
   * @author Anthony Fong
   */
  public String getLongPathScanFilePathForCDNABenchmark()
  {
    return _longPathScanFilePathForCDNABenchmark;
  }
  /**
   * @author Anthony Fong
   */
  public void loadScanPathConfigurationFile() throws DatastoreException
  {      
    if (_scanPath != null)
    {
      _scanPath.clear();
      _scanPath = null;
    }
    String _scanMovesConfigurationDir = Directory.getScantMoveDirectoryForCDNABenchmarking();
    _longPathScanFilePathForCDNABenchmark = _scanMovesConfigurationDir + File.separator + FileName.getCDNABenchmarkLongScanPathFileName();

    if (new File(_longPathScanFilePathForCDNABenchmark).exists() == false)
    {
      return;
    }
    
    // read the file and populated the table
    ScanPathDefinitionTableReader reader = new ScanPathDefinitionTableReader();
    try
    {
      _scanPath = reader.readDefinedScanPath(_longPathScanFilePathForCDNABenchmark);
    }
    catch (FileCorruptDatastoreException fcde)
    {
      // the file was corrupt so give the user an error message and then clear out the table model.
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_PATH_CONFIG_FILE_CORRUPT_KEY");
      javax.swing.JOptionPane.showMessageDialog(null,
        StringUtil.format(message, 50),
        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
        javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    catch (DatastoreException de)
    {
      javax.swing.JOptionPane.showMessageDialog(null,
        StringUtil.format(de.getMessage(), 50),
        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
        javax.swing.JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * @author George A. David
   */
  private void abort(XrayTesterException xex)
  {
    if (_exception != null)
    {
      return;
    }

    _exception = xex;
    try
    {
      abort();
    }
    catch (XrayTesterException ex)
    {
      // we already have an exception, let's
      // just print this out.
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public void abort() throws XrayTesterException
  {
    Assert.expect(_panelPositioner != null);

    _aborted = true;

    try
    {
      _panelPositioner.abort();
    }
    catch (XrayTesterException ex)
    {
      if (_exception == null)
      {
        _exception = ex;
        throw ex;
      }
    }
  }
}
