package com.axi.v810.hardware.autoCal;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is used to generate the exceptions that are thrown by Hardware Tasks
 * when they fail (without an exception) during execution in an automated setting.
 *
 * It is also used by Hardware Tasks if they need to throw an exception for other
 * reasons, typically when a non XrayTester exception is encountered and needs
 * to be 'wrapped' as an XrayTester exception.
 *
 * @author Reid Hayhow
 */
public class HardwareTaskExecutionException extends HardwareException
{
  private HardwareTaskExceptionEnum _hardwareTaskExceptionEnum;

  /**
   * Hardware Task Exception constructor. Preserves the cause and stack trace.
   *
   * @author Reid Hayhow
   */
  public HardwareTaskExecutionException(HardwareTaskExceptionEnum hardwareTaskExceptionEnum, Throwable ex)
  {
    super(hardwareTaskExceptionEnum.getLocalizedString());

    Assert.expect(hardwareTaskExceptionEnum != null);
    Assert.expect(ex != null);

    // Save the exception enum so we know more about what happened
    _hardwareTaskExceptionEnum = hardwareTaskExceptionEnum;

    //preserve the original stack trace and cause
    setStackTrace(ex.getStackTrace());
    initCause(ex);
  }

  /**
   * Constructor for hardware task exceptions that don't have a different initial
   * cause (ie. they originate with the Confirmation itself as opposed to being
   * wrapped and passed along).
   *
   * @author Reid Hayhow
   */
  public HardwareTaskExecutionException(HardwareTaskExceptionEnum hardwareTaskExceptionEnum)
  {
    super(hardwareTaskExceptionEnum.getLocalizedString());

    Assert.expect(hardwareTaskExceptionEnum != null);
    // Save the exception enum so we know more about what happened
    _hardwareTaskExceptionEnum = hardwareTaskExceptionEnum;
  }

  /**
   * A generic constructor to wrap passing result messages into an exception to
   * provide failure information to exceptions that are thrown if a Confirmation
   * or Adjustment fails.
   *
   * @author Reid Hayhow
   */
  public HardwareTaskExecutionException(HardwareTask task)
  {
    //Pass up the localized task name, result of the task execution and suggested action
    super(new LocalizedString(StringLocalizer.keyToString("CD_THE_PREFIX_KEY") + " " +
                              task.getName() + " " +
                              StringLocalizer.keyToString("CD_FAILED_KEY") +
                              System.getProperty("line.separator") +
                              System.getProperty("line.separator") +
                              //Pass up the task status message
                              task.getResult().getTaskStatusMessage() +
                              System.getProperty("line.separator") +
                              System.getProperty("line.separator") +
                              //And pass up the action the user should take
                              task.getResult().getSuggestedUserActionMessage(), null));

    Assert.expect(task != null);
  }

  /**
   * Method to wrap the messages from individual hardware tasks with a standard
   * header and footer.
   *
   * @author Reid Hayhow
   */
  public String getLocalizedMessage()
  {
    //Get the localized header
    StringBuilder finalMessage = new StringBuilder(StringLocalizer.keyToString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY"));

    //Append the message from the constructor. This message is already localized
    finalMessage.append(StringLocalizer.keyToString(getLocalizedString()));

    //Get the localized footer and append it
    finalMessage.append(StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY"));

    //Now return the whole string
    return finalMessage.toString();
  }

  /**
   *
   * @author Reid Hayhow
   */
  public void setExceptionEnum(HardwareTaskExceptionEnum exceptionEnum)
  {
    Assert.expect(exceptionEnum != null);

    _hardwareTaskExceptionEnum = exceptionEnum;
  }

  /**
   *
   * @author Reid Hayhow
   */
  public HardwareTaskExceptionEnum getExceptionEnum()
  {
    Assert.expect(_hardwareTaskExceptionEnum != null);

    return _hardwareTaskExceptionEnum;
  }
}
