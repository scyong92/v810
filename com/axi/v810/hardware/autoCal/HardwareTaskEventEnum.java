package com.axi.v810.hardware.autoCal;

import com.axi.v810.hardware.HardwareEventEnum;
import com.axi.util.Assert;

public class HardwareTaskEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

//  public static HardwareTaskEventEnum MANUAL_HARDWARE_TASK_EXECUTION = new HardwareTaskEventEnum(++_index);
  public static HardwareTaskEventEnum HARDWARE_TASK_EXECUTION_EVENT = new HardwareTaskEventEnum(++_index, "Hardware Task, Execution Event");
  public static HardwareTaskEventEnum HARDWARE_TASK_ENGINE_DONT_CARE_MAJOR_EVENT = new HardwareTaskEventEnum(++_index, "Hardware Task, Engine Dont Care Major Event");

  private String _name;

  /**
   * @author Reid Hayhow
   * @author George Booth
   */
  private HardwareTaskEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author George Booth
   */
  public String toString()
  {
    return _name;
 }

}
