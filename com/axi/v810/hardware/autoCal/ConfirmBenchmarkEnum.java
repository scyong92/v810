package com.axi.v810.hardware.autoCal;

import com.axi.util.Enum;

/**
 * Enum of Benchmark for components like actuator, cylinder and motor
 * @author Anthony Fong
 */
public class ConfirmBenchmarkEnum extends Enum
{
  private static int _index = -1;

  public static ConfirmBenchmarkEnum LEFT_OUTER_BARRIER_OPEN_CLOSE = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum RIGHT_OUTER_BARRIER_OPEN_CLOSE = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum INNER_BARRIER_OPEN_CLOSE = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum PANEL_CLAMPS_OPEN_CLOSE = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum LEFT_PIP_IN_OUT = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum RIGHT_PIP_IN_OUT = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum XRAY_TUBE_ACTUATOR_UP_DOWN = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum PANEL_POSITIONER_LOAD_UNLOAD = new ConfirmBenchmarkEnum(++_index);
  public static ConfirmBenchmarkEnum PANEL_HANDLER_RAIL_WIDTH_ADJUST = new ConfirmBenchmarkEnum(++_index);  
  public static ConfirmBenchmarkEnum LONG_PATH_SCANNING = new ConfirmBenchmarkEnum(++_index);


  /**
   * @author Anthony Fong
   */
  protected ConfirmBenchmarkEnum(int id)
  {
    super(id);
  }
}
