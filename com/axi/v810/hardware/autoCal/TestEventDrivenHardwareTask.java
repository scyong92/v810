package com.axi.v810.hardware.autoCal;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test subclass for the event driven hardware task. This class has a built-in
 * execution counter and get method that allows for testing the execution of the
 * task by the HardwareTaskEngine.
 *
 * @author Reid Hayhow
 */
public class TestEventDrivenHardwareTask extends EventDrivenHardwareTask
{
  private String _name = null;
  public int _executionCount = 0;
  private XrayTesterException _exception = null;

  private boolean _throwExceptionOnExecute = false;
  private int _exceptionToThrow = -1;
  public final static int DATASTORE_EXCEPTION = 0;
  public final static int HARDWARE_EXCEPTION = 1;
  public final static int BUSINESS_EXCEPTION = 2;
  public final static int XRAY_TESTER_EXCEPTION = 3;

  /**
   * The constructor for this test class. It allows for the construction of
   * tasks that respond to multiple events and at a varying frequency.
   *
   * @author Reid Hayhow
   */
  public TestEventDrivenHardwareTask(String name, int frequency, long timeoutInSeconds)
  {
    super(name, frequency, timeoutInSeconds, ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);

    _name = name;
  }

  /**
   * TestEventDrivenHardwareTask
   *
   * @author Reid Hayhow
   */
  public TestEventDrivenHardwareTask(String name, int frequency)
  {
    super(name, frequency, 0, ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);
    _name = name;
  }

  public TestEventDrivenHardwareTask(String name, XrayTesterException exception)
  {
    super(name, exception, ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);
    _name = name;
    _exception = exception;
  }

  /**
   * This method contains the task code. In this case it just prints a message
   * we can use to diff agains.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //Throw an exception if requested BEFORE incrementing internal counter
    if(_throwExceptionOnExecute)
    {
      switch (_exceptionToThrow)
      {
        case DATASTORE_EXCEPTION:
          throw new DatastoreException("fake file");
        case HARDWARE_EXCEPTION:
          throw new HardwareException(new LocalizedString("bogus", null));
        case BUSINESS_EXCEPTION:
          throw new com.axi.v810.business.autoCal.EdgeMTFBusinessException();
        case XRAY_TESTER_EXCEPTION:
          throw new XrayTesterException(new LocalizedString("Something went wrong!", null));
        default:
          Assert.expect(false);
      }
    }

    _executionCount++;

    try
    {
      //A way to allow future tests to actually cancel a task that is executing
      Thread.sleep(50);
    }
    catch (InterruptedException ex)
    {
      //do nothing
    }
  }

  public boolean canRunOnThisHardware()
  {
    return true;
  }

  protected void cleanUp() throws XrayTesterException
  {
    //Do nothing
  }

  protected void createLimitsObject()
  {
    _limits = new Limit(new Double(2.0), new Double(12.5), "bar", "Regression Test Default Limit");
  }

  protected void createResultsObject()
  {
    _result = new Result(new Double(5.1), new String("Result for Event Driven Task"));
  }

  protected void setUp() throws XrayTesterException
  {
    //do nothing
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void setThrowException(int exceptionType)
  {
    _throwExceptionOnExecute = true;
    _exceptionToThrow = exceptionType;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void unsetThrowException()
  {
    _throwExceptionOnExecute = false;
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
