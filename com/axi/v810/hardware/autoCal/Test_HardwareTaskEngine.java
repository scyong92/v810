package com.axi.v810.hardware.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author Reid Hayhow
 */
public class Test_HardwareTaskEngine extends UnitTest implements Observer
{
  //HardwareObservable instance, so we can watch for updates
  private HardwareObservable _hardwareObservable;

  //Counter to keep track of updates
  private int _updateCounter = 0;
  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_HardwareTaskEngine());
  }

  /**
   * The actual test main code used to test the HardwareTaskEngine class. Create
   * a set of tasks to run, register them with the task engine along with the
   * corresponding event to trigger them and run in a loop that triggers the
   * events a set number of times. There is no implied order or logic to the
   * events now, but eventually there could be.
   *
   * @author Reid Hayhow
   * @param is BufferedReader
   * @param os PrintWriter
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ExecuteParallelThreadTasks<Object> taskExecutor = new ExecuteParallelThreadTasks<Object>();

    HardwareTaskEngine engine = HardwareTaskEngine.getInstance();

    //Get the hardwareObservable instance and register with it for updates
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareObservable.addObserver(this);

    //Test to see if system is in adjustment. It should NOT be
    Expect.expect(engine.isCalibrated() == false);
    Expect.expect(engine.isConfirmed() == false);

    //Check the automated list of tasks to make sure they are all listed correctly
    List<EventDrivenHardwareTask> autoList =
      com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList.getInstance().getListOfRequiredTasks();

    //Check each task to make sure it is an adjustment (always automatic) or is
    //an automatic confirmation
    for(EventDrivenHardwareTask task : autoList)
    {
      Expect.expect(task.getHardwareTaskType() == HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE ||
                    task.getHardwareTaskType() == HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE,
                    task.getName() + " is not of the expected type");
    }

    try
    {
      //Test camera calibrations (set to run ASAP)
      engine.setCameraCalibrationsToRunASAP();

      //Test all calibrations (set to run ASAP)
      engine.setAllCalibrationsToRunASAP();
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace(os);
    }


    //Calling a secondary event start, end or primary event end BEFORE
    //the engine has seen a primary event start should be handled cleanly
    //without a crash
    try
    {
      engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, true);
      engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, false);
      engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, true, false);
    }
    catch (XrayTesterException ex2)
    {
      Assert.expect(false);
    }

    HardwareTimeoutException ex = new HardwareTimeoutException(HardwareTimeoutExceptionEnum.
        WAIT_FOR_PANEL_TO_BE_IN_PLACE);

    //EventDrivenTask with frequency of 1 that throws exceptions
    TestEventDrivenHardwareTask taskExceptionGenerator =
        new TestEventDrivenHardwareTask("Event Driven Task to generate exceptions", 1);
      taskExceptionGenerator.setThrowException(TestEventDrivenHardwareTask.HARDWARE_EXCEPTION);

    //EventDrivenTask with frequency of 1
    TestEventDrivenHardwareTask taskOne =
        new TestEventDrivenHardwareTask("Event Driven Task One", 1);
    //EventDrivenTask with frequency of 2
    TestEventDrivenHardwareTask taskTwo =
        new TestEventDrivenHardwareTask("Event Driven Task Two", 2);
    //EventDrivenTask with frequency of 3
    TestEventDrivenHardwareTask taskThree =
        new TestEventDrivenHardwareTask("Event Driven Task Three", 3);
    //EventDrivenTask with frequency of 5
    TestEventDrivenHardwareTask taskFive =
        new TestEventDrivenHardwareTask("Event Driven Task Five", 5);
    //EventDrivenTask with frequency of 10
    TestEventDrivenHardwareTask taskTen =
        new TestEventDrivenHardwareTask("Event Driven Task Ten", 10);
    //EventDrivenTask with frequency of 1 to test triggering on end of state
    TestEventDrivenHardwareTask oneEnd =
        new TestEventDrivenHardwareTask("Event Driven Task OneEnd", 1);

    //EventDrivenTask with frequency of 2 to test triggering on end of state
    //for Major event only (Major/Major pair)
    TestEventDrivenHardwareTask majorEnd =
        new TestEventDrivenHardwareTask("Event Driven Task majorEnd", 2);

    //Task triggered by an exception
    TestEventDrivenHardwareTask taskToRunOnException =
        new TestEventDrivenHardwareTask("Event Driven Task for Exception", ex);

    //EventDrivenTask with frequency of 1
    TestEventDrivenHardwareTask taskMajorMinor =
        new TestEventDrivenHardwareTask("Event Driven Task Major Minor", 1);

    //EventDrivenTask with frequency of 1
    TestEventDrivenHardwareTask taskMinorAnyMajor =
        new TestEventDrivenHardwareTask("Event Driven Task Minor any Major", 1);

    //This tests the simple case of sub-tasks that have a super task executing
    //every time, first the super task or task master
    TestEventDrivenHardwareTask taskMaster =
        new TestEventDrivenHardwareTask("Event Driven Task Master", 1);

    //Then the sub tasks
    TestEventDrivenHardwareTask taskSub1 =
        new TestEventDrivenHardwareTask("Event Driven Task Sub1", 1);

    //NOTE: Although I set this to run every other event, it runs at the
    //frequency of the taskMaster, ie every event
    TestEventDrivenHardwareTask taskSub2 =
        new TestEventDrivenHardwareTask("Event Driven Task Sub2", 2);

    //Add the sub tasks to the task master
    taskMaster.addSubTest(taskSub1);
    taskMaster.addSubTest(taskSub2);

    //Test a master task that is triggered every three times with sub tasks
    TestEventDrivenHardwareTask taskMaster2 =
        new TestEventDrivenHardwareTask("Event Driven Task Master 2", 3);

      //Create sub tasks for the second task master
    TestEventDrivenHardwareTask taskSub3 =
        new TestEventDrivenHardwareTask("Event Driven Task Sub 3", 1);

    TestEventDrivenHardwareTask taskSub4 =
        new TestEventDrivenHardwareTask("Event Driven Task Sub 4", 1);

      //Add the tasks to the task master
    taskMaster2.addSubTest(taskSub3);
    taskMaster2.addSubTest(taskSub4);

    //Now add all the tasks and task masters to the engine
    engine.addTask(DigitalIoEventEnum.INITIALIZE,
                   PanelHandlerEventEnum.UNLOAD_PANEL, taskExceptionGenerator, true);

    engine.addTask(PanelHandlerEventEnum.UNLOAD_PANEL,
                   PanelHandlerEventEnum.UNLOAD_PANEL, taskOne, true);
    engine.addTask(LeftOuterBarrierEventEnum.BARRIER_CLOSE,
                   LeftOuterBarrierEventEnum.BARRIER_CLOSE, taskTwo, true);
    engine.addTask(RightOuterBarrierEventEnum.BARRIER_CLOSE,
                   RightOuterBarrierEventEnum.BARRIER_CLOSE, taskThree, true);
    engine.addTask(LeftOuterBarrierEventEnum.BARRIER_OPEN,
                   LeftOuterBarrierEventEnum.BARRIER_OPEN, taskFive, true);
    engine.addTask(RightOuterBarrierEventEnum.BARRIER_OPEN,
                   RightOuterBarrierEventEnum.BARRIER_OPEN, taskTen, true);

    //Test end event triggering on minor/major and at the end of the event
    engine.addTask(PanelHandlerEventEnum.UNLOAD_PANEL,
                   LeftOuterBarrierEventEnum.BARRIER_CLOSE, oneEnd, false);

    engine.addTask(PanelHandlerEventEnum.CANCEL_LOAD_PANEL,
                   PanelHandlerEventEnum.CANCEL_LOAD_PANEL, taskMaster, true);
    engine.addTask(PanelClampsEventEnum.CLAMPS_CLOSE,
                   PanelClampsEventEnum.CLAMPS_CLOSE, taskMaster2, true);

    engine.addTask(PanelHandlerEventEnum.CANCEL_LOAD_PANEL,
                   PanelClampsEventEnum.CLAMPS_CLOSE, taskMajorMinor, true);

    engine.addTask(HardwareTaskEventEnum.HARDWARE_TASK_ENGINE_DONT_CARE_MAJOR_EVENT,
                   LightStackEventEnum.GREEN_LIGHT_OFF, taskMinorAnyMajor, true);

    engine.addTask(PanelHandlerEventEnum.CANCEL_LOAD_PANEL,
                   PanelHandlerEventEnum.CANCEL_LOAD_PANEL, majorEnd, false);

    engine.addTask(ex.getExceptionType(), taskToRunOnException);

    //None should have an execution count at the start
    Expect.expect(taskExceptionGenerator._executionCount == 0, "Execution Count test failed for exceptionGenerator");

    Expect.expect(taskOne._executionCount == 0, "Execution Count test failed for taskOne");
    Expect.expect(taskTwo._executionCount == 0, "Execution Count test failed for taskTwo");
    Expect.expect(taskThree._executionCount == 0, "Execution Count test failed for taskThree");
    Expect.expect(taskFive._executionCount == 0, "Execution Count test failed for taskFive");
    Expect.expect(taskTen._executionCount == 0, "Execution Count test failed for taskTen");
    Expect.expect(oneEnd._executionCount == 0, "Execution Count test failed for oneEnd");

    Expect.expect(taskMaster._executionCount == 0, "Execution Count test failed for taskMaster");
    Expect.expect(taskSub1._executionCount == 0, "Execution Count test failed for taskSub1");
    Expect.expect(taskSub2._executionCount == 0, "Execution Count test failed for taskSub2");

    Expect.expect(taskMaster2._executionCount == 0, "Execution Count test failed for taskMaster2");
    Expect.expect(taskSub3._executionCount == 0, "Execution Count test failed for taskSub3");
    Expect.expect(taskSub4._executionCount == 0, "Execution Count test failed for taskSub4");

    Expect.expect(taskMajorMinor._executionCount == 0, "Execution Count test failed for taskMajorMinor");
    Expect.expect(taskMinorAnyMajor._executionCount == 0, "Execution Count test failed for minorAnyMajor");
    Expect.expect(majorEnd._executionCount == 0, "Execution Count test failed for majorEnd");

    Expect.expect(taskToRunOnException._executionCount == 0, "Execution Count test failed for taskException");

    //Now send the engine a bunch of events in a loop *before* the tester is started up
    //The hardware task engine should ignore them
    for (int i = 0; i < 11; i++)
    {
      try
      {
        triggerEventsAndException(engine, ex, i);
      }
      //I have a 'throw' in the looping test that should throw up to here
      catch (XrayTesterException XrayE)
      {
        if (!(XrayE.equals(ex)))
        {
          XrayE.printStackTrace();
          Assert.expect(false);
        }
      }
    }

    //None should have an execution count since the xray tester was not in a state
    //where the task engine will run them
    Expect.expect(taskExceptionGenerator._executionCount == 0, "Execution Count test failed for exceptionGenerator");
    Expect.expect(taskOne._executionCount == 0, "Execution Count test failed for taskOne");
    Expect.expect(taskTwo._executionCount == 0, "Execution Count test failed for taskTwo");
    Expect.expect(taskThree._executionCount == 0, "Execution Count test failed for taskThree");
    Expect.expect(taskFive._executionCount == 0, "Execution Count test failed for taskFive");
    Expect.expect(taskTen._executionCount == 0, "Execution Count test failed for taskTen");
    Expect.expect(oneEnd._executionCount == 0, "Execution Count test failed for oneEnd");

    Expect.expect(taskMaster._executionCount == 0, "Execution Count test failed for taskMaster");
    Expect.expect(taskSub1._executionCount == 0, "Execution Count test failed for taskSub1");
    Expect.expect(taskSub2._executionCount == 0, "Execution Count test failed for taskSub2");

    Expect.expect(taskMaster2._executionCount == 0, "Execution Count test failed for taskMaster2");
    Expect.expect(taskSub3._executionCount == 0, "Execution Count test failed for taskSub3");
    Expect.expect(taskSub4._executionCount == 0, "Execution Count test failed for taskSub4");

    Expect.expect(taskMajorMinor._executionCount == 0, "Execution Count test failed for taskMajorMinor");
    Expect.expect(taskMinorAnyMajor._executionCount == 0, "Execution Count test failed for minorAnyMajor");
    Expect.expect(majorEnd._executionCount == 0, "Execution Count test failed for majorEnd");

    Expect.expect(taskToRunOnException._executionCount == 0, "Execution Count test failed for taskException");

    //Now enable the xray tester, so we can run
    engine.disableEvents();
    try
    {
      XrayTester.getInstance().startup();
    }
    catch (XrayTesterException ex1)
    {
      ex1.printStackTrace(os);
      Assert.expect(false);
    }
    engine.enableEvents();

    //Test that throwing an exception from within a task should reset eventsEnable
    try
    {
      //set-up with major event
      engine.hardwareEvent(DigitalIoEventEnum.INITIALIZE, true, true);

      //this minor event will trigger the task. The task throws an exception
      //before incrementing
      engine.hardwareEvent(PanelHandlerEventEnum.UNLOAD_PANEL, true, false);
      //better not get here!
      Assert.expect(false);
    }
    catch (XrayTesterException ex3)
    {
      Expect.expect(engine.eventsAreEnabled() == true);
    }

    // Since HardwareTask encountered exception, it will abort all waiting task
    // in HardwareTaskEngine.  We need to clear abort.
    engine.clearAbort();

    //Close out the Major event to leave things tidy
    try
    {
      engine.hardwareEvent(DigitalIoEventEnum.INITIALIZE, false, true);
    }
    catch (XrayTesterException ex4)
    {
      ex4.printStackTrace(os);
    }

    //Still should not have incremented because of exception
    Expect.expect(taskExceptionGenerator._executionCount == 0, "Execution Count test failed for exceptionGenerator");

    //Now send the engine a bunch of events in a loop. These will trigger the tasks
    for (int i = 0; i < 11; i++)
    {
      try
      {
        triggerEventsAndException(engine, ex, i);
      }
      //I have a 'throw' in the looping test that should throw up to here
      catch (XrayTesterException XrayE)
      {
        if ((XrayE.equals(ex)) == false)
        {
          XrayE.printStackTrace();
        }
        Assert.expect(engine.eventsAreEnabled() == true);
      }
    }

    //Based on the frequency the task should run, the order of the events, etc. we
    //can assert the execution count

    //TaskOne executes every event seen
    Expect.expect(taskOne._executionCount == 11, "Execution Count test failed for taskOne " +
                  taskOne._executionCount);
    //TaskTwo executes every other event, 0, 2, 4, 6, 8, 10
    Expect.expect(taskTwo._executionCount == 6, "Execution Count test failed for taskTwo " +
                  taskTwo._executionCount);
    //TaskThree executes every third event, 0, 3, 6, 9
    Expect.expect(taskThree._executionCount == 4, "Execution Count test failed for taskThree " +
                  taskThree._executionCount);
    //TaskFive executes every fifth event, 0, 5, 10
    Expect.expect(taskFive._executionCount == 3, "Execution Count test failed for taskFive " +
                  taskFive._executionCount);
    //TaskTen executes every tenth event, 0, 10
    Expect.expect(taskTen._executionCount == 2, "Execution Count test failed for taskTen " +
                  taskTen._executionCount);

    //Task oneEnd executes on the end and executes every time, count should be 11
    Expect.expect(oneEnd._executionCount == 11, "Execution Count test failed for oneEnd");

    Expect.expect(taskMaster._executionCount == 0, "Execution Count test failed for taskMaster " +
                  taskMaster._executionCount);
    Expect.expect(taskSub1._executionCount == 11, "Execution Count test failed for taskSub1 " +
                  taskSub1._executionCount);
    //NOTE: since this is a sub-task, it executes at the frequency of the container
    Expect.expect(taskSub2._executionCount == 11, "Execution Count test failed for taskSub2 " +
                  taskSub2._executionCount);

    Expect.expect(taskMaster2._executionCount == 0, "Execution Count test failed for taskMaster2 " +
                  taskMaster._executionCount);
    Expect.expect(taskSub3._executionCount == 4, "Execution Count test failed for taskSub3 " +
                  taskSub3._executionCount);
    //NOTE: since this is a sub-task, it executes at the frequency of the container
    Expect.expect(taskSub4._executionCount == 4, "Execution Count test failed for taskSub4 " +
                  taskSub4._executionCount);

    //TaskMajorMinor executes every Major/Minor pair  seen
    Expect.expect(taskMajorMinor._executionCount == 11, "Execution Count test failed for taskMajorMinor " +
                  taskMajorMinor._executionCount);

    Expect.expect(taskMinorAnyMajor._executionCount == 22, "Execution Count test failed for minorAnyMajor" +
                  taskMinorAnyMajor._executionCount);

    //This task executes every other time it sees the Major event, so it executes 6 times
    Expect.expect(majorEnd._executionCount == 6, "Execution Count test failed for majorEnd");

    Expect.expect(taskToRunOnException._executionCount == 11, "Execution Count test failed for taskException");

    //now disable events from the engine and make sure it doesn't run anything!
    engine.disableEvents();
    engine.disableExceptionHandling();

    //Send the engine a bunch of events in a loop. Since the engine is
    //disabled, no tasks will run
    for (int i = 0; i < 5; i++)
    {
      try
      {
        triggerEventsAndException(engine, ex, i);
      }
      //I have a 'throw' in the looping test that should throw up to here
      catch (XrayTesterException XrayE)
      {
        if (!(XrayE.equals(ex)))
        {
          XrayE.printStackTrace();
          Assert.expect(false);
        }
      }
    }

    engine.enableEvents();
    engine.enableExceptionHandling();

    //These counts are identical to the ones above

    Expect.expect(taskOne._executionCount == 11, "Execution Count test failed for taskOne " +
                  taskOne._executionCount);
    Expect.expect(taskTwo._executionCount == 6, "Execution Count test failed for taskTwo " +
                  taskTwo._executionCount);
    Expect.expect(taskThree._executionCount == 4, "Execution Count test failed for taskThree " +
                  taskThree._executionCount);
    Expect.expect(taskFive._executionCount == 3, "Execution Count test failed for taskFive " +
                  taskFive._executionCount);
    Expect.expect(taskTen._executionCount == 2, "Execution Count test failed for taskTen " +
                  taskTen._executionCount);
    Expect.expect(oneEnd._executionCount == 11, "Execution Count test failed for oneEnd");
    Expect.expect(taskMaster._executionCount == 0, "Execution Count test failed for taskMaster " +
                  taskMaster._executionCount);
    Expect.expect(taskSub1._executionCount == 11, "Execution Count test failed for taskSub1 " +
                  taskSub1._executionCount);
    Expect.expect(taskSub2._executionCount == 11, "Execution Count test failed for taskSub2 " +
                  taskSub2._executionCount);
    Expect.expect(taskMaster2._executionCount == 0, "Execution Count test failed for taskMaster2 " +
                  taskMaster._executionCount);
    Expect.expect(taskSub3._executionCount == 4, "Execution Count test failed for taskSub3 " +
                  taskSub3._executionCount);
    Expect.expect(taskSub4._executionCount == 4, "Execution Count test failed for taskSub4 " +
                  taskSub4._executionCount);
    Expect.expect(taskMajorMinor._executionCount == 11, "Execution Count test failed for taskMajorMinor " +
                  taskMajorMinor._executionCount);
    Expect.expect(taskMinorAnyMajor._executionCount == 22, "Execution Count test failed for minorAnyMajor" +
                  taskMinorAnyMajor._executionCount);
    Expect.expect(majorEnd._executionCount == 6, "Execution Count test failed for majorEnd");
    Expect.expect(taskToRunOnException._executionCount == 11, "Execution Count test failed for taskException");


    //************************Test Event Driven Tasks with timeouts*********************/
    TestEventDrivenHardwareTask taskTimeOne =
      new TestEventDrivenHardwareTask("Event Driven Task Timed One", 1, 1);

    TestEventDrivenHardwareTask taskTimeTwo =
      new TestEventDrivenHardwareTask("Event Driven Task Timed Two", 2, 1);

    engine.addTask(PanelPositionerEventEnum.ENABLE_ALL_AXES,
                   PanelPositionerEventEnum.ENABLE_ALL_AXES,
                   taskTimeOne, true);
    engine.addTask(PanelPositionerEventEnum.DISABLE_ALL_AXES,
                   PanelPositionerEventEnum.DISABLE_ALL_AXES,
                   taskTimeTwo, true);

    Expect.expect(taskTimeOne._executionCount == 0, "Execution Count test failed for " +
                  taskTimeOne.getName());

    for (int i = 0; i < 2; i++)
    {
      try
      {
        engine.hardwareEvent(PanelPositionerEventEnum.ENABLE_ALL_AXES, true, true);
        engine.hardwareEvent(PanelPositionerEventEnum.DISABLE_ALL_AXES, true, true);
        //NOTE: this shouldn't trigger anything here
        engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
      }
      catch (XrayTesterException XrayE)
      {
        XrayE.printStackTrace();
      }
    }

    //TaskOne executes every event seen, but both have a 1 second timeout, so they both
    //should only run once
    Expect.expect(taskTimeOne._executionCount == 1, "Execution Count test failed for taskTimeOne " +
                  taskTimeOne._executionCount);

    Expect.expect(taskTimeTwo._executionCount == 1, "Execution Count test failed for taskTimeTwo " +
                  taskTimeTwo._executionCount);

    //Sleep a bit to give it a chance to execute
    try
    {
      Thread.sleep(300);
    }
    catch (InterruptedException ie)
    {
    }

    for (int i = 0; i < 2; i++)
    {
      try
      {
        engine.hardwareEvent(PanelPositionerEventEnum.ENABLE_ALL_AXES, true, true);
        engine.hardwareEvent(PanelPositionerEventEnum.DISABLE_ALL_AXES, true, true);
        //NOTE: this shouldn't trigger anything here
        engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
      }
      catch (XrayTesterException XrayE)
      {
        XrayE.printStackTrace();
      }
    }

    Expect.expect(taskTimeOne._executionCount == 1, "Execution Count test failed for taskTimeOne " +
                  taskTimeOne._executionCount);

    Expect.expect(taskTimeTwo._executionCount == 1, "Execution Count test failed for taskTimeTwo " +
                  taskTimeTwo._executionCount);

    //Sleep a bit to give it a chance to execute
    try
    {
      Thread.sleep(900);
    }
    catch (InterruptedException ie)
    {
    }

    try
    {
      engine.hardwareEvent(PanelPositionerEventEnum.ENABLE_ALL_AXES, true, true);
      engine.hardwareEvent(PanelPositionerEventEnum.DISABLE_ALL_AXES, true, true);
      //NOTE: this shouldn't trigger anything here
      engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
    }
    catch (XrayTesterException XrayE)
    {
      XrayE.printStackTrace();
    }

    //TaskTimeOne will execute because time has elapsed and it executes on every event
    Expect.expect(taskTimeOne._executionCount == 2, "Execution Count test failed for taskTimeOne " +
                  taskTimeOne._executionCount);
    //TaskTimeTwo will *NOT* execute because although time has elapsed, it waits
    //for the second event
    Expect.expect(taskTimeTwo._executionCount == 1, "Execution Count test failed for taskTimeTwo " +
                  taskTimeTwo._executionCount);

    try
    {
      engine.hardwareEvent(PanelPositionerEventEnum.ENABLE_ALL_AXES, true, true);
      engine.hardwareEvent(PanelPositionerEventEnum.DISABLE_ALL_AXES, true, true);
      //NOTE: this shouldn't trigger anything here
      engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
    }
    catch (XrayTesterException XrayE)
    {
      XrayE.printStackTrace();
    }

    //NOTE:TaskTimeOne does not execute again because it just did
    Expect.expect(taskTimeOne._executionCount == 2, "Execution Count test failed for taskTimeOne " +
                  taskTimeOne._executionCount);
    //TaskTimeTwo will now execute
    Expect.expect(taskTimeTwo._executionCount == 2, "Execution Count test failed for taskTimeTwo " +
                  taskTimeTwo._executionCount);

    for (int i = 0; i < 3; i++)
    {
      try
      {
        engine.hardwareEvent(PanelPositionerEventEnum.ENABLE_ALL_AXES, true, true);
        engine.hardwareEvent(PanelPositionerEventEnum.DISABLE_ALL_AXES, true, true);
        //NOTE: this shouldn't trigger anything here
        engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
      }
      catch (XrayTesterException XrayE)
      {
        XrayE.printStackTrace();
      }
    }
    //Both tasks have now executed and will wait for another second
    Expect.expect(taskTimeOne._executionCount == 2, "Execution Count test failed for taskTimeOne " +
                  taskTimeOne._executionCount);
    Expect.expect(taskTimeTwo._executionCount == 2, "Execution Count test failed for taskTimeTwo " +
                  taskTimeTwo._executionCount);

    //************************Test Time Driven Tasks *********************/
//    TestTimeDrivenHardwareTask tTaskOne =
//        new TestTimeDrivenHardwareTask("Timed Task One", 500);
//
//    TestTimeDrivenHardwareTask tTaskTwo =
//        new TestTimeDrivenHardwareTask("Timed Task Two", 1000);
//
//    TestTimeDrivenHardwareTask tTaskThree =
//        new TestTimeDrivenHardwareTask("Timed Task Three", 1500);
//
//    //Reset the update counter before the time driven tasks
//    _updateCounter = 0;
//    int sleepTime = 0;
    //Add the task
//    engine.addTask(tTaskOne);
//    engine.addTask(tTaskTwo);
//    engine.addTask(tTaskThree);
//
//    engine.startTimerThreads();
//
//    StringBuffer buff = new StringBuffer("\n");
//
//    //Sleep a bit to give it a chance to execute
//    try
//    {
//      int safety = 0;
//      while(_updateCounter < 6 && safety < 40)
//      {
//        buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                    "\n\t task two exe count = " + tTaskTwo._executionCount +
//                    "\n\t task three exe count = " +tTaskThree._executionCount);
//        Thread.sleep(25);
//        sleepTime += 25;
//        if(tTaskOne._executionCount == 1 &&
//           tTaskTwo._executionCount == 1 &&
//           tTaskThree._executionCount == 1)
//        {
//          safety = 39;
//        }
//        safety++;
//      }
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//      buff.append("\nupdates =" + _updateCounter + " sleep number =" + safety + " sleep 25 \n");
//
//    }
//    catch (InterruptedException ie)
//    {
//      // do nothing
//    }
//
//    //All tasks should have executed once by now
//    Expect.expect(tTaskOne._executionCount == 1, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount + buff);
//    Expect.expect(tTaskTwo._executionCount == 1, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount + buff);
//    Expect.expect(tTaskThree._executionCount == 1, "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount + buff);
//
//    try
//    {
//      buff.append("sleep 50 \n");
//      sleepTime += 50;
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//      Thread.sleep(50);
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//    }
//    catch (InterruptedException ie)
//    {
//      // do nothing
//    }
//
//    //None of the tasks should have executed a second time yet
//    Expect.expect(tTaskOne._executionCount == 1, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount + buff);
//    Expect.expect(tTaskTwo._executionCount == 1, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount + buff);
//    Expect.expect(tTaskThree._executionCount == 1, "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount + buff);
//
//
//    try
//    {
//      int safety = 0;
//      while(_updateCounter < 8 && safety < 40)
//      {
//        buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                    "\n\t task two exe count = " + tTaskTwo._executionCount +
//                    "\n\t task three exe count = " +tTaskThree._executionCount);
//        Thread.sleep(25);
//        sleepTime += 25;
//        if(tTaskOne._executionCount == 2 &&
//           tTaskTwo._executionCount == 1 &&
//           tTaskThree._executionCount == 1)
//        {
//          safety = 39;
//        }
//        safety++;
//      }
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//      buff.append("\nupdates =" + _updateCounter + " sleep number =" + safety + " sleep 25 \n");
//
//    }
//    catch (InterruptedException ie)
//    {
//    }
//
//    //Task one should have executed one more time by now
//    Expect.expect(tTaskOne._executionCount == 2, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount + buff);
//    //Neither of the other tasks should have executed
//    Expect.expect(tTaskTwo._executionCount == 1, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount + buff);
//    Expect.expect(tTaskThree._executionCount == 1, "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount + buff);
//
//    try
//    {
//      int safety = 0;
//      while(_updateCounter < 12 && safety < 40)
//      {
//        buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                    "\n\t task two exe count = " + tTaskTwo._executionCount +
//                    "\n\t task three exe count = " +tTaskThree._executionCount);
//        Thread.sleep(25);
//        sleepTime += 25;
//        safety++;
//      }
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//      buff.append("\nupdates =" + _updateCounter + " sleep number =" + safety + " sleep 25, total sleep time =" + sleepTime + " \n");
//
//    }
//    catch (InterruptedException ie)
//    {
//    }
//
//    TestHardwareTaskEngineTaskThread engineTaskThread = new TestHardwareTaskEngineTaskThread();
//    engineTaskThread.setHardwareTaskToCancel(engine);
//
//    taskExecutor.submitThreadTask(engineTaskThread);
//
//    try
//    {
//      taskExecutor.waitForTasksToComplete();
//    }
//    catch (Exception ex5)
//    {
//      ex5.printStackTrace(os);
//    }
//    //stop time driven tasks from executing further
//    //engine.cancel();
//
//    //Expect that we will see 2X the total execution count (3+2+1) x 2 = 12 updates to observer
////    Expect.expect((_updateCounter >= 11 && _updateCounter <= 15), "After cancel, should not see more updates, expected 12-14 updates, saw " + _updateCounter);
//
//    //Now both task one and two should have executed an additional time
//    Expect.expect(tTaskOne._executionCount == 3 || tTaskOne._executionCount == 4, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount + buff);
//    Expect.expect(tTaskTwo._executionCount == 2, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount + buff);
//    //But task three won't have executed by this time
//    Expect.expect((tTaskThree._executionCount == 1 || tTaskThree._executionCount == 2), "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount + buff);
//
//    try
//    {
//      Thread.sleep(600);
//      sleepTime += 600;
//    }
//    catch (InterruptedException ie)
//    {
//    }
//
//    //There should not have been *any* updates, since the hardware task engine still thinks
//    //things are canceling/aborting and should not be running any tasks
//    Expect.expect((_updateCounter >= 11 && _updateCounter <= 15), "After cancel, should not see more updates even after sleep, expected 12 updates, saw " + _updateCounter);
//
//    //All the execution counts should be the same as above
//    Expect.expect(tTaskOne._executionCount == 3 || tTaskOne._executionCount == 4, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount);
//    Expect.expect(tTaskTwo._executionCount == 2, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount);
//    Expect.expect((tTaskThree._executionCount == 1 || tTaskThree._executionCount == 2), "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount);
//
//    //OK, tell the task engine that we are done with the abort. It should allow the 'pended tasks'
//    //to run
//    //At this point it is likely that both tTaskOne and tTaskThree are ready to run.
//    //tTaskThree should still have at least 500 ms left before it wants to run
//    engine.clearAbort();
//
//    try
//    {
//      int safety = 0;
//      while(_updateCounter < 16 && safety < 40)
//      {
//        buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                    "\n\t task two exe count = " + tTaskTwo._executionCount +
//                    "\n\t task three exe count = " +tTaskThree._executionCount);
//        sleepTime+=25;
//        Thread.sleep(25);
//        safety++;
//      }
//      buff.append("\n\t task one exe count = " + tTaskOne._executionCount +
//                  "\n\t task two exe count = " + tTaskTwo._executionCount +
//                  "\n\t task three exe count = " +tTaskThree._executionCount);
//      buff.append("\nupdates =" + _updateCounter + " sleep number =" + safety + " sleep 25, total sleep time =" + sleepTime + " \n");
//    }
//    catch (InterruptedException ie)
//    {
//    }
//
//    Expect.expect(_updateCounter >= 14 && _updateCounter <= 18, "Expected 14-18 updates, saw " + _updateCounter + buff);
//
//    //Now both task one and three should have executed an additional time
//    Expect.expect(tTaskOne._executionCount == 4 || tTaskOne._executionCount == 5, "Execution Count test failed for tTaskOne " +
//                  tTaskOne._executionCount + buff);
//    Expect.expect(tTaskThree._executionCount == 2 || tTaskThree._executionCount == 3, "Execution Count test failed for tTaskThree " +
//                  tTaskThree._executionCount + buff);
//
//    //But task two won't have executed by this time
//    Expect.expect(tTaskTwo._executionCount == 2 || tTaskTwo._executionCount == 3, "Execution Count test failed for tTaskTwo " +
//                  tTaskTwo._executionCount + buff);
  }

  /**
   * Reuse the code to trigger selected events and throw an exception. This can
   * be used when events are enabled to run a task AND can test that no tasks
   * are run when events are disabled
   *
   * @author Reid Hayhow
   */
  private void triggerEventsAndException(HardwareTaskEngine engine, XrayTesterException ex, int loopCount) throws XrayTesterException
  {
    //These calls to the engine to notify it of events contain two booleans
    //the first is eventStart the second is majorEvent

    //Unload Panel start as a major event, this is mapped and should execute Task One every time
    engine.hardwareEvent(PanelHandlerEventEnum.UNLOAD_PANEL, true, true);

    //Unload Panel start as a minor event, should not execute (there is no task for it)
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_OPEN, true, false);

    //Should trigger minorAnyMajor every time
    engine.hardwareEvent(LightStackEventEnum.GREEN_LIGHT_OFF, true, false);

    //Unload Panel end as a minor event, also should not execute (there is no task for it)
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_OPEN, false, false);

    //Barrier close does have an event that triggers on end event only
    //So this call shouldn't trigger it
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, true, false);

    //This call should trigger OneEnd every time
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, false);

    //Unload Panel end as a major event, also should not execute
    //Tasks are only executed for the start of the event
    engine.hardwareEvent(PanelHandlerEventEnum.UNLOAD_PANEL, false, true);

    //TaskTwo every other time
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, true, true);
    //TaskThree every third time
    engine.hardwareEvent(RightOuterBarrierEventEnum.BARRIER_CLOSE, true, true);
    //TaskFive every fifth time
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_OPEN, true, true);
    //TaskTen every tenth time
    engine.hardwareEvent(RightOuterBarrierEventEnum.BARRIER_OPEN, true, true);

    //These calls won't trigger oneEnd because they are outside a major event
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, false);
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, false);

    //This will trigger an task but it also the Major event, pre-condition for a
    //task to follow, Sub1 and Sub2 every time
    engine.hardwareEvent(PanelHandlerEventEnum.CANCEL_LOAD_PANEL, true, true);

    //Should trigger minorAnyMajor every time
    engine.hardwareEvent(LightStackEventEnum.GREEN_LIGHT_OFF, true, false);

    //Now trigger the minor start event, should execute the task MajorMinor every time
    engine.hardwareEvent(PanelClampsEventEnum.CLAMPS_CLOSE, true, false);

    //Now trigger the minor start event, should NOT execute the task
    engine.hardwareEvent(PanelClampsEventEnum.CLAMPS_CLOSE, false, false);

    //These calls won't trigger oneEnd because they are inside a major event
    //but it is the wrong event type
    engine.hardwareEvent(LeftOuterBarrierEventEnum.BARRIER_CLOSE, false, false);

    //And finally close the Major event, this will trigger another task, majorEnd every other time
    engine.hardwareEvent(PanelHandlerEventEnum.CANCEL_LOAD_PANEL, false, true);

    // sub 3 and sub 4 every third time
    engine.hardwareEvent(PanelClampsEventEnum.CLAMPS_CLOSE, true, true);

    // Execute exception
    engine.throwHardwareException(ex);

   //NOTE: this shouldn't trigger anything here
    engine.hardwareEvent(StageBeltsEventEnum.BELTS_ON, true, true);
  }

  /**
   * Method so we participate in the Observer interface. This simply counts
   * the number of times we see a HardwareTaskEventEnum which is issued by a
   * HardwareTask as it executes.
   *
   * @author Reid Hayhow
   */
  public void update(Observable observable, Object arg)
  {
    //We only care about HardwareObservables, probably not necessary
    if(observable instanceof HardwareObservable)
    {
      //The arg should be a HardwareEvent
      if(arg instanceof HardwareEvent)
      {
        //Cast the arg to be an event
        HardwareEvent event = (HardwareEvent)arg;

        //Get the specific enum for this event, so we can look at it more closely
        HardwareEventEnum eventEnum = event.getEventEnum();

        //We only care about HardwareTaskEventEnum
        if(eventEnum instanceof HardwareTaskEventEnum)
        {
          //If we see a HardwareTaskEventEnum, increment the counter
          _updateCounter++;

        }
      }
    }
  }
}
