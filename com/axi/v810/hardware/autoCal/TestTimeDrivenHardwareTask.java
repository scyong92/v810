package com.axi.v810.hardware.autoCal;

import com.axi.v810.hardware.autoCal.TimeDrivenHardwareTask;
import com.axi.v810.util.XrayTesterException;
import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * This is a test class for TimeDrivenHardwareTasks. It implements the methods
 * necessary to provide simple tests for executing tasks at different time
 * intervals.
 *
 * @author Reid Hayhow
 */
public class TestTimeDrivenHardwareTask extends TimeDrivenHardwareTask
{
  private String _name = null;
  private int _interval = 0;
  protected int _executionCount = 0;

  /**
   * Constructor for Test_TimeDrivenHardwareTask. It allows for different names
   * and execution frequencies
   *
   * @param name String
   * @param interval int
   * @author Reid Hayhow
   */
  public TestTimeDrivenHardwareTask(String name, int interval)
  {
    super(name, interval, false);

    _name = name;
    _interval = interval;

  }

  /**
   * This method just keeps count of the number of times it was called
   *
   * @throws XrayTesterException
   * @author Reid Hayhow
   */
  protected void executeTaskOnWorkerThread() throws XrayTesterException
  {
    //We must be on the worker thread
    Assert.expect(HardwareWorkerThread.isHardwareWorkerThread());

    //Update the counter
    _executionCount++;

    try
    {
      //A way to allow future tests to actually cancel a task that is executing
      Thread.sleep(10);
    }
    catch (InterruptedException ex)
    {
      //do nothing
    }
  }

  /**
   * canRunOnThisHardware
   * Always return true for this test
   *
   * @return boolean
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * cleanUp
   *
   * @throws XrayTesterException
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
  }

  /**
   * createLimitsObject
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(new Double(-2.5), new Double(15.2), "baz", "Regression Test Default Limit");
  }

  /**
   * createResultsObject
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    _result = new Result(new Double(7.4), new String("Result for TimeDrivenTask"));
  }

  protected void setUp() throws XrayTesterException
  {
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
