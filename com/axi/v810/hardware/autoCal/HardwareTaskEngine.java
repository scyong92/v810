package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class controls when HardwareTasks get run.
 * There are three ways that a task will be run.
 * 1. A hardware event can trigger an EventDrivenHardwareTask.  This happens when
 *    HardwareObservable.stateChanged() calls hardwareEvent(HardwareEventEnum)
 * 2. A TimeDrivenHardwareTask executes because a certain amount of time has elapsed
 *    since the last time it was run.
 * 3. The system is determined to have been idle for a long period of time.  In this
 *    case some TimeDrivenHardwareTasks may be run earlier than they would have been
 *    to minimize the chance of them running when the system is in use.
 *
 * Note that for this to work all calls from the business layer to the hardware layer
 * must be put on the HardwareWorkerThread.invokeAndWait(RunnableWithExceptions).
 * This includes the PanelInspection.inspect() call, the CandDTest.execute()
 * and any calls used by HardwareStatusGUI
 *
 * We cannot do it in the hardware layer because the inspection code needs to run
 * on multiple threads for it to work properly.  So everything must be done at
 * the business layer.
 *
 * If this is not done multiple
 * threads will be calling into the hardware layer causing unpredictable behavior.
 *
 * Note that this is the same way Swing works with its threading.
 *
 * @author Bill Darbie
 */
// STILL NEEDS - serialize last time run of each task into a single file
//               possibly a properties file or maybe a Java serialized file
//             - what package - I am thinking hardware/autoCal maybe
public class HardwareTaskEngine
{
  private static HardwareTaskEngine _instance;

  private List<TimeDrivenHardwareTask> _timeDrivenHardwareTasks = new ArrayList<TimeDrivenHardwareTask>();

  //This is the map of major events that maps to minor events that holds a list of tasks
  //to execute when the minor event is a state-change start
  private Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>> > _hardwareEventEnumStartToHardwareTaskMap =
      new HashMap<HardwareEventEnum,Map<HardwareEventEnum,List<EventDrivenHardwareTask>>>();

  //This is the map of major events that maps to minor events that holds a list of tasks
  //to execute when the minor event is a state-change end
  private Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>> > _hardwareEventEnumEndToHardwareTaskMap =
      new HashMap<HardwareEventEnum,Map<HardwareEventEnum,List<EventDrivenHardwareTask>>>();

  private Map<HardwareExceptionEnum, EventDrivenHardwareTask> _hardwareExceptionEnumToHardwareTaskMap = new HashMap<HardwareExceptionEnum, EventDrivenHardwareTask>();

  //Fully qualified class name required to avoid architectural check failure
  private com.axi.v810.business.autoCal.AdjustmentsAndConfirmationsLogger _calFileLogger =
      com.axi.v810.business.autoCal.AdjustmentsAndConfirmationsLogger.getInstance();

  // serialize (maybe read/write properties files) this somehow!!
  //Initialize this to start up time, otherwise calculations are wierd
  private long _timeOfLastEventInMillis = System.currentTimeMillis();
  // this thread determines when the system has been idle for a long period of tim
  private WorkerThread _idleSystemThread = new WorkerThread("detectIdleSystemThread");
  private boolean _eventsEnabled = true;
  private boolean _exceptionHandlingEnabled = true;
  private boolean _running = false;
  private int _hoursToBeConsideredIdle = 2;
  private HardwareObservable _hardwareObservable;
  private HardwareEventEnum _primaryEvent = null;
  private XrayTester _xrayTesterInstance;
  AbstractXraySource _xraySource;

  private boolean _isAborting = false;
  private Config _config;

  // Member variables for saving and restoring state
  private boolean _xraysWereOnBeforeTaskExecution = false;
  private boolean _systemStateSaved = false;

  private com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList _listOfAllAutomatedTasks = null;
  private com.axi.v810.business.testExec.TestExecutionTimer _timer;
  private com.axi.v810.business.testExec.TestExecution _testExecutionInstance;
  //XCR-3797, Failed to cancel CDNA task after start up
  private Object _abortLock = new Object();

  //NOTE: the explicit class paths are needed to not violate architecture rules
  //that hardware layer not 'touch' the business layer. autoCal is a strange
  //hybrid where this rule can be bent (Base classes live in hardware layer
  //but sub-classes live in business layer so they can access business layer
  //image manipulation features)

  /**
   * @author Bill Darbie
   */
  public synchronized static HardwareTaskEngine getInstance()
  {
    if (_instance == null)
    {
      _instance = new HardwareTaskEngine();
    }
    _instance.run();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private HardwareTaskEngine()
  {
    _config = Config.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public void throwHardwareException(XrayTesterException hex) throws XrayTesterException
  {
    Assert.expect(hex != null);

    runTaskIfNecessary(hex);
  }

  /**
   * @author Bill Darbie
   */
  public XrayTesterException notifyObserversOfException(XrayTesterException hex)
  {
    XrayTesterException ex = hex;
    try
    {
      runTaskIfNecessary(hex);
    }
    catch (XrayTesterException xex)
    {
      ex = xex;
    }

    return ex;
  }

  /**
   * @author Greg Esparza
   */
  private void handleHardwareExceptionTask(HardwareException he) throws XrayTesterException
  {
    // find the exception in the exception to potentially bad components list
    EventDrivenHardwareTask task = _hardwareExceptionEnumToHardwareTaskMap.get(he.getExceptionType());
    if (task != null && _xrayTesterInstance.isStartupRequired() == false)
    {
      disableExceptionHandling();
      // execute the tasks here
      try
      {
        task.execute(he);
      }
      finally
      {
        enableExceptionHandling();
      }
    }

    throw he;
  }

  /**
   * @author Bill Darbie
   */
  private void runTaskIfNecessary(XrayTesterException xte) throws XrayTesterException
  {
    //Make sure we have an instance of the xray tester object
    initializeMembersWithPotentialCyclicReferences();

    if (exceptionHandlingEnabled())
    {
      // This class only supports hardware events.  If the exception is anything other than a
      // hardware exception then we will do nothing.
      if (xte instanceof HardwareException)
        handleHardwareExceptionTask((HardwareException)xte);
    }

    throw xte;
  }

  /**
   * @author Reid Hayhow
   */
  private void run()
  {
    if (_running == false)
    {
      _running = true;

      startTimerThreads();
      startIdleSystemThread();
    }
  }

  /**
   * @author Reid Hayhow
   */
  void addTask(HardwareExceptionEnum exceptionType, EventDrivenHardwareTask eventDrivenHardwareTask)
  {
    Assert.expect(exceptionType != null);
    Assert.expect(eventDrivenHardwareTask != null);
    Assert.expect(UnitTest.unitTesting());

    EventDrivenHardwareTask prev = _hardwareExceptionEnumToHardwareTaskMap.put(exceptionType, eventDrivenHardwareTask);
    Assert.expect(prev == null);
  }

  /**
   * @author Reid Hayhow
   */
  void addTask(HardwareEventEnum majorEvent,
                      HardwareEventEnum minorEvent,
                      EventDrivenHardwareTask task,
                      boolean triggerOnStartOfState)
  {
    Assert.expect(majorEvent != null);
    Assert.expect(minorEvent != null);
    Assert.expect(task != null);
    Assert.expect(UnitTest.unitTesting());

    //Handle adding tasks to the map of tasks to trigger on state changed START
    if(triggerOnStartOfState)
    {
      //Get the map of minor events to list of tasks to run from the
      //map of mayor events to minor events
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventStartMap =
      _hardwareEventEnumStartToHardwareTaskMap.get(majorEvent);

      //If the minor event map is null, create it and add it to the higher level map
      if (minorEventStartMap == null)
      {
        minorEventStartMap = new HashMap<HardwareEventEnum, List<EventDrivenHardwareTask>>();
        _hardwareEventEnumStartToHardwareTaskMap.put(majorEvent, minorEventStartMap);
      }

      //Don't clobber existing entries in the lower level map
      //NOTE is is possible for the minor event map to exist but not contain
      //the exact major to minor map we are about to insert
      Assert.expect(minorEventStartMap.containsKey(minorEvent) == false);

      //Since it wasn't in the map (I just asserted if it was), we also need to
      //create the list to hold it
      List<EventDrivenHardwareTask> taskList = new ArrayList<EventDrivenHardwareTask>();

      //Add the actual task to the list
      taskList.add(task);


      //Add the minor event and taskList pair to the minor event map
      minorEventStartMap.put(minorEvent, taskList);
    }
    //Handle adding tasks to the map of tasks to trigger on state changed END
    else
    {
      //See notes above for more details, this is identical to code above
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventEndMap =
      _hardwareEventEnumEndToHardwareTaskMap.get(majorEvent);

      //If the map is null, create it and add it to the higher level map
      if (minorEventEndMap == null)
      {
        minorEventEndMap = new HashMap<HardwareEventEnum, List<EventDrivenHardwareTask>>();
        _hardwareEventEnumEndToHardwareTaskMap.put(majorEvent, minorEventEndMap);
      }

      //Don't clobber existing entries in the lower level map
      Assert.expect(minorEventEndMap.containsKey(minorEvent) == false);

      //Since it wasn't in the map, we also need to create the list to hold it
      List<EventDrivenHardwareTask> taskList = new ArrayList<EventDrivenHardwareTask>();

      //Add the task to the task list
      taskList.add(task);

      //Add the minor event and taskList pair to the lower level map
      minorEventEndMap.put(minorEvent, taskList);
    }
  }

  /**
   * @author Reid Hayhow
   */
  void addTask(TimeDrivenHardwareTask timeDrivenHardwareTask)
  {
    Assert.expect(timeDrivenHardwareTask != null);
    Assert.expect(UnitTest.unitTesting());

    _timeDrivenHardwareTasks.add(timeDrivenHardwareTask);
  }

  /**
   * Start a thread that will determine when the system has been idle.  If an idle condition
   * is detected, run some TimeDrivenHardwareTasks ealier than they would run otherwise
   * to minimize the chance of them running during an inspection and affecting test throughput.
   * @author Bill Darbie
   */
  private void startIdleSystemThread()
  {
    _idleSystemThread.invokeLater(new Runnable()
    {
      public void run()
      {
        long timeToBeConsideredIdleInMillis = _hoursToBeConsideredIdle * 60 * 60 * 1000;
        long sleepTimeInMillis = timeToBeConsideredIdleInMillis;
        while(true)
        {
          try
          {
            Thread.sleep(sleepTimeInMillis);
          }
          catch(InterruptedException ie)
          {
            // do nothing
          }
          long timeSinceLastEventInMillis = getTimeSinceLastEventInMillis();
          if (timeSinceLastEventInMillis > timeToBeConsideredIdleInMillis)
          {
            // the system has been idle for a long time - run some
            // TimeDrivenHardwareTasks (do long ones first)
            // after they are run set the correct sleep time
            List<TimeDrivenHardwareTask> tasks = getTasksToRunWhileIdle();
            try
            {
              for (TimeDrivenHardwareTask task : tasks)
              {
                // check that we are still idle before running each task in case
                // the user started doing some work on the machine
                timeSinceLastEventInMillis = getTimeSinceLastEventInMillis();
                if (timeSinceLastEventInMillis > timeToBeConsideredIdleInMillis)
                {
                  boolean invokedFromSystemIdleThread = true;
                  task.execute(invokedFromSystemIdleThread);
                }
                else
                {
                  // an event has occurred which means that the system is no longer
                  // idle, so break out of the loop and do not try to run more tasks
                  break;
                }
              }
            }
            catch (XrayTesterException hex)
            {
              // pass this to an Observer/Observable
              if (_hardwareObservable == null)
                _hardwareObservable = HardwareObservable.getInstance();

              _hardwareObservable.notifyObserversOfException(hex);
            }
          }
          //The time to sleep is shorter if the last event we saw was two hours
          //ago than if the last event was a second ago, so compensate for this
          sleepTimeInMillis = timeToBeConsideredIdleInMillis - timeSinceLastEventInMillis;

          //But if it has been so long since we have seen an event that the sleep
          //time would go negative, sleep for the default time instead
          if (sleepTimeInMillis < 0)
            sleepTimeInMillis = timeToBeConsideredIdleInMillis;
        }
      }
    });
  }

  /**
   * @author Bill Darbie
   */
  private synchronized long getTimeSinceLastEventInMillis()
  {
    long currTimeInMillis = System.currentTimeMillis();
    return currTimeInMillis - _timeOfLastEventInMillis;
  }

  /**
   * @return a List of TimeDrivenHardwareTask objects in order.  The first task
   *         is in more need of being run than the next.
   * @author Bill Darbie
   */
  private List<TimeDrivenHardwareTask> getTasksToRunWhileIdle()
  {
    Map<Long, TimeDrivenHardwareTask> longRunningTaskMap = new TreeMap<Long, TimeDrivenHardwareTask>();
    Map<Long, TimeDrivenHardwareTask> shortRunningTaskMap = new TreeMap<Long, TimeDrivenHardwareTask>();
    for (TimeDrivenHardwareTask task : _timeDrivenHardwareTasks)
    {
      long timeBeforeNextRunInMillis = task.getTimeUntilNextRunInMillis();
      if (task.isLongTask())
        longRunningTaskMap.put(new Long(timeBeforeNextRunInMillis), task);
      else
        shortRunningTaskMap.put(new Long(timeBeforeNextRunInMillis), task);
    }

    // put long tasks first, with the ones that have not been run for
    // the longest time first
    List<TimeDrivenHardwareTask> tasks = new ArrayList<TimeDrivenHardwareTask>(longRunningTaskMap.values());
    // then put in short tasks, again with the ones that have not been run for
    // the longest first
    tasks.addAll(shortRunningTaskMap.values());

    Assert.expect(tasks != null);
    return tasks;
  }

  /**
   * For each TimeDrivenHardwareTask we start up a thread.  That thread will sleep
   * until it is time for the task to be run.  After the task is run the thread
   * will sleep until it is time to be run again.
   * @author Bill Darbie
   */
  protected void startTimerThreads()
  {
    for ( final TimeDrivenHardwareTask task: _timeDrivenHardwareTasks)
    {

      if(task.isTimerThreadStarted() == true)
      {
        continue;
      }
      WorkerThread thread = new WorkerThread("thread for task: " + task.getName());
      thread.invokeLater(new Runnable()
      {
        //The minimum amount of time this thread should sleep so that it will
        //always yield to other processes and not lock the system
        private static final long _MIN_SLEEP_TIME = 100;

        public void run()
        {
          while(true)
          {
            // check time and determine how long to sleep
            long freqInMillis = task.getFrequencyInMillis();
            long timeElapsedInMillis = task.getElapsedTimeSinceLastRunInMillis();
            long sleepTime = freqInMillis - timeElapsedInMillis;
            if (sleepTime < 0)
            {
              //This used to be 0, but this can hammer the system, so in order to
              //be nice this thread will sleep a minimal time
              sleepTime = _MIN_SLEEP_TIME;
            }
            try
            {
              Thread.sleep(sleepTime);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }

            try
            {
              // Make sure I can access bypass mode
              initializeMembersWithPotentialCyclicReferences();

              //Don't execute this task if the tester is aborting or if Bypass mode is on
              if(_isAborting == false && _testExecutionInstance.isBypassModeEnabled() == false)
              {
                boolean invokedFromSystemIdleThread = false;
                // execute the task
                task.execute(invokedFromSystemIdleThread);
              }
            }
            catch(XrayTesterException hex)
            {
              // pass this to an Observer/Observable
              if (_hardwareObservable == null)
                _hardwareObservable = HardwareObservable.getInstance();

              _hardwareObservable.notifyObserversOfException(hex);
            }
          }
        }
      });

      task.setTimerStarted(true);
    }
  }

  /**
   * Hardware events will be ignored after this is called.  HardwareTask.execute() calls
   * this before executing a task. It is also called by any process that wants to
   * turn off the automated execution of tasks (ie. because they may confuse
   * troubleshooting, diagnostics, etc.)
   *
   * @author Bill Darbie
   */
  public void disableEvents()
  {
    _eventsEnabled = false;
  }

  /**
   * @author Bill Darbie
   */
  public void enableEvents()
  {
    _eventsEnabled = true;
  }

  /**
   * Getter method to see if events are enabled. Package private for now, unless
   * I see a reason to open it up further.
   *
   * @author Reid Hayhow
   */
  boolean eventsAreEnabled()
  {
    return _eventsEnabled;
  }

  /**
   * This method gets called from HardwareObservable.stateChanged(). It handles
   * the lookup and running of EventDrivenHardwareTasks based on the Major(Primary)
   * Minor(Secondary) event pair as well as if the event is considered a start
   * event or an end event. It will not trigger tasks if events are not enabled
   * in the HardwareTaskEngine.
   *
   * @author Reid Hayhow
   */
  public void hardwareEvent(HardwareEventEnum event,
                            boolean eventStart,
                            boolean primary) throws XrayTesterException
  {
    //Make sure we have an instance of the xray tester object
    initializeMembersWithPotentialCyclicReferences();

    //Events are ignored if we are running a task already or if the Service
    //GUI is up
    if (eventsAreEnabled() && _testExecutionInstance.isBypassModeEnabled() == false)
    {
      synchronized(this)
      {
        _timeOfLastEventInMillis = System.currentTimeMillis();
      }

      //Don't call this function without an event
      Assert.expect(event != null);

      //If it is the start of a primary event, save the primary event for later
      //reference/context. It is used as the first key into the event to task
      //map
      if(primary && eventStart)
      {
        _primaryEvent = event;
      }

      //If we get a Minor event first we are lost because we need a Major
      //event to use as the first key into the event to task map. Return and
      //trust that eventually we will get a primary event we can work with
      if(_primaryEvent == null)
      {
        return;
      }

      //NOTE:If this is a primary/Major event, the map can have an entry in which
      //both keys are the primary event. This allows the TaskEngine to execute a
      //task for primary events as well as primary/secondary event pairs
      List<EventDrivenHardwareTask> taskList =
          getTaskForMajorMinorEventPair(_primaryEvent, event, eventStart);

      //If we didn't find anything for the actual Major/Minor event pair, check
      //to see if we see anything that matches for the minor event with a DON'T Care
      //on the Major Event
      if(taskList == null)
      {
        taskList = getTaskForAllMinorEvents(
                                      event,
                                      eventStart);
      }

      //In order to log the major minor event pairs, we can't purge the major
      //event yet, but we have to guarantee it is purged before we depart, so
      //use a try/finally block
      try
      {
        //if we find a list of tasks to run based on the Major and Minor events
        //combined with the start/end state OR tasks for any minor event, then run them
        if (taskList != null)
        {
          //Log the start of execution major/minor event pair in developer debug mode
          if(_config.isDeveloperDebugModeOn())
          {
            _calFileLogger.append("Start, major hardware event =" +
                                  _primaryEvent.toString() +
                                  ", minor event =" +
                                  event.toString() +
                                  ", start =" +
                                  eventStart);
          }

          //Call synchronized method to guarantee no collisions
          executeTaskList(taskList, event, eventStart);

          //Log the end major/minor event pair in developer debug mode
          if(_config.isDeveloperDebugModeOn())
          {
            _calFileLogger.append("Done, major hardware event =" +
                                  _primaryEvent.toString() +
                                  ", minor event =" + event.toString() +
                                  ", start =" +
                                  eventStart);
          }
        }

      }
      finally
      {
        //If this is the end of a primary event, purge that event
        if (primary && eventStart == false)
        {
          _primaryEvent = null;
        }
      }//end of finally
    }//end of if(_eventsEnabled)
  }

  /**
   * Synchronized method that loops through a list of tasks and executes them.
   * This prevents multiple events from attempting to run multiple different tasks
   * at the same time. The first event trigger to start executing a task list
   * will get to complete all the tasks in its list before any other task gets
   * a chance.
   *
   * @author Reid Hayhow
   */
  private synchronized void executeTaskList(List<EventDrivenHardwareTask> taskList,
                                            HardwareEventEnum minorEvent,
                                            boolean minorEventIsStart)
                                            throws XrayTesterException
  {
    // Local variable to store any exceptions we may encounter
    XrayTesterException exception = null;

    // Assume that the system state has not been saved
    _systemStateSaved = false;

    // Don't run if startup is requred
    if(_xrayTesterInstance.isStartupRequired() == false)
    {
      try
      {
        for (EventDrivenHardwareTask task : taskList)
        {
          // Save the state of the system only if it hasn't already been saved during execution of
          // this task list. This operation takes time so it is only done once per task list
          saveSystemStateIfNecessary(task);

          //handle cancel at the lowest level possible, thus assuring it is applied
          //to all tasks that were not canceled directly by the cancel method.
          if (_isAborting == false)
          {
            task.execute(_primaryEvent, minorEvent, minorEventIsStart);
          }
          else
          {
            //Bail out on abort
            return;
          }
        }
      }
      // Catch any exceptions that are thrown by execution so that they aren't
      // clobbered later on
      catch (XrayTesterException initialException)
      {
        exception = initialException;
      }
      finally
      {
        // We only save the state of the system if a task was going to run, and
        // if a task ran we need to guarantee the system is restored to its original state
        try
        {
          restoreSystemStateIfNecessary();
        }
        // Catch any exceptions thrown by querying or changing xray state
        catch (XrayTesterException secondaryException)
        {
          // If there was a pre-existing exception, preserve it and log this one
          if (exception != null)
          {
            secondaryException.printStackTrace();
          }
          // Otherwise this was the first exception we have seen, save it
          else
          {
            exception = secondaryException;
          }
        }

        // No matter what, if an exception was encountered, throw it
        if (exception != null)
        {
          throw exception;
        }
      }
    }
    else
    {
      if(_config.isDeveloperDebugModeOn())
      {
        _calFileLogger.append("No tasks run because system was not started!");
      }
    }
  }

  /**
   * Method to unconditionally save off the system state
   *
   * @author Reid Hayhow
   */
  public void saveSystemState() throws XrayTesterException
  {
    // Do this only once. It cannot be done at object instanciation because of a race
    // condition
    if( _xraySource == null)
    {
      _xraySource = AbstractXraySource.getInstance();
    }

    // Only save the state of xrays if they have been started
    if(_xraySource.isStartupRequired() == false)
    {
      // Save the state of xrays
      _xraysWereOnBeforeTaskExecution = _xraySource.areXraysOn();

      // OK, we have saved the state of the system as best we can for now
      _systemStateSaved = true;

      if(_config.isDeveloperDebugModeOn())
      {
        _calFileLogger.append("saved xray state before task execution, xrays were on before is " +
                              _xraysWereOnBeforeTaskExecution);
      }
    }
  }

  /**
   * Save the state of the system only if a task is going to execute and could
   * change the state of the system. Return a boolean to indicate if the state
   * has been saved.
   *
   * @author Reid Hayhow
   */
  private void saveSystemStateIfNecessary(EventDrivenHardwareTask task) throws XrayTesterException
  {
    // Only save the state of the system if it has not already been saved
    if(_systemStateSaved == false)
    {
      // If the state fo the system hasn't changed AND a task needs to execute
      // then save the state of the x-rays so we can restore them after execution
      if (task.peekIsExecuteNeeded())
      {
        saveSystemState();
      }
    }
  }

  /**
   * Restore the system to the saved state no matter if it is different from the
   * previous state.
   *
   * @author Reid Hayhow
   */
  public void restoreSystemState() throws XrayTesterException
  {
    // The system state must have been saved before this is safe!
    Assert.expect(_systemStateSaved == true);
    // And in order to have saved system state, _xraySource cannot be null
    Assert.expect(_xraySource != null);

    // Only attempt to turn on/off xrays if the source is started
    if(_xraySource.isStartupRequired() == false)
    {
      // We only want to re-enable events if they were enabled to begin with,
      // so save the current state before changing it
      boolean eventsWereEnabled = eventsAreEnabled();

      try
      {
        // Disable events so that we don't trigger recursion of events -> Confirm -> events
        disableEvents();

        // Handle if xrays were on but aren't now
        if (_xraysWereOnBeforeTaskExecution == true)
        {
          if (_config.isDeveloperDebugModeOn())
          {
            _calFileLogger.append("restore xrays to on");
          }
          _xraySource.on();
        }
        // Handle case of xrays were off and are on now
        else if (_xraysWereOnBeforeTaskExecution == false)
        {
          if (_config.isDeveloperDebugModeOn())
          {
            _calFileLogger.append("restore xrays to off");
          }
          _xraySource.off();
        }
      }
      finally
      {
        // We have done our best to restore the system to the prior state
        _systemStateSaved = false;

        // If we are running/restoring state from a UI, we want to ignore events, so don't
        // blindly re-enable events.
        if (eventsWereEnabled)
        {
          // enable events on the way out IF they were enabled before, otherwise no future event triggered tasks will get a chance to run
          enableEvents();
        }
      }
    }
  }

  /**
   * Method to restore the state of xrays to the initial state they were in before any
   * tasks executed.
   *
   * @author Reid Hayhow
   */
  private void restoreSystemStateIfNecessary() throws XrayTesterException
  {
    // If the system state was saved then we need to try to restore it
    if (_systemStateSaved)
    {
      restoreSystemState();
    }
  }

  /**
   * Wrapper method to get values for a list of tasks that should execute because
   * of a minor event, regardless of the major event.
   *
   * @author Reid Hayhow
   */
  private List<EventDrivenHardwareTask> getTaskForAllMinorEvents(HardwareEventEnum event, boolean eventStart)
  {
    return getTaskForMajorMinorEventPair(HardwareTaskEventEnum.HARDWARE_TASK_ENGINE_DONT_CARE_MAJOR_EVENT,
                                         event,
                                         eventStart);

  }

  /**
   * Can't get the XrayTester instance in the constructor because the
   * HardwareObservable has an instance of this class *AND* the XrayTester has
   * an instance of the HardwareObservable. Getting the XrayTester instance at
   * construction time creates a cyclic dependency and two HardwareObservables
   *
   * @author Reid Hayhow
   */
  private void initializeMembersWithPotentialCyclicReferences()
  {
    if(_xrayTesterInstance == null)
    {
      _xrayTesterInstance = XrayTester.getInstance();
    }

    if(_testExecutionInstance == null)
    {
      _testExecutionInstance = com.axi.v810.business.testExec.TestExecution.getInstance();
    }
  }

  /**
   * cancel any running tasks
   *
   * @author Reid Hayhow
   */
  public void cancel()
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    synchronized (_abortLock)
    {
      _isAborting = true;

      //Cancel any time driven tasks that may be running
      for (TimeDrivenHardwareTask task : _timeDrivenHardwareTasks)
      {
        task.cancel();
      }

      //Cancel any event driven tasks that may be running, starting with the
      //tasks triggered on state change START
      //
      //For each Major Event that can trigger a list of tasks, get the map of Minor
      //events that can trigger the list of tasks
      for (Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventStartTriggerMap
        : _hardwareEventEnumStartToHardwareTaskMap.values())
      {
        //For all the Minor events, get the list of tasks they can trigger
        for (List<EventDrivenHardwareTask> taskList : minorEventStartTriggerMap.values())
        {
          //For each task in that list of tasks, cancel it
          for (EventDrivenHardwareTask task : taskList)
          {
            task.cancel();
          }
        }
      }
    //Cancel any event driven tasks that may be running, now the tasks triggered
      //on state change END
      for (Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventEndTriggerMap
        : _hardwareEventEnumEndToHardwareTaskMap.values())
      {
        for (List<EventDrivenHardwareTask> taskList : minorEventEndTriggerMap.values())
        {
          for (EventDrivenHardwareTask task : taskList)
          {
            task.cancel();
          }
        }
      }
    }
  }

  /**
   * cancel any running tasks without blocking the thread that calls
   *
   * @author Reid Hayhow
   */
  public void cancelNoWait()
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    synchronized(_abortLock)
    {
      _isAborting = true;

      //Cancel any time driven tasks that may be running
      for (TimeDrivenHardwareTask task : _timeDrivenHardwareTasks)
      {
        task.cancelNoWait();
      }

      //Cancel any event driven tasks that may be running, starting with the
      //tasks triggered on state change START
      //
      //For each Major Event that can trigger a list of tasks, get the map of Minor
      //events that can trigger the list of tasks
      for (Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventStartTriggerMap
        : _hardwareEventEnumStartToHardwareTaskMap.values())
      {
        //For all the Minor events, get the list of tasks they can trigger
        for (List<EventDrivenHardwareTask> taskList : minorEventStartTriggerMap.values())
        {
          //For each task in that list of tasks, cancel it
          for (EventDrivenHardwareTask task : taskList)
          {
            task.cancelNoWait();
          }
        }
      }
      //Cancel any event driven tasks that may be running, now the tasks triggered
      //on state change END
      for (Map<HardwareEventEnum, List<EventDrivenHardwareTask>> minorEventEndTriggerMap
        : _hardwareEventEnumEndToHardwareTaskMap.values())
      {
        for (List<EventDrivenHardwareTask> taskList : minorEventEndTriggerMap.values())
        {
          for (EventDrivenHardwareTask task : taskList)
          {
            task.cancel();
          }
        }
      }
    }
  }


  /**
   * clearAbort clears the aborting flag and indicates that the hardware is ready
   * to execute tasks again after an abort has been triggered.
   *
   * @author Reid Hayhow
   */
  public void clearAbort()
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    synchronized(_abortLock)
    {
      _isAborting = false;
    }
  }

  /**
   * Public access to set up the Event Driven Task map triggered on the start
   * of the state transition
   *
   * @author Reid Hayhow
   */
  public void initializeStartEventDrivenMap(Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>> newMap)
  {
    _hardwareEventEnumStartToHardwareTaskMap = newMap;
  }

  /**
   * Public access to set up the Event Driven Task map triggered on the end of
   * of the state transition
   *
   * @author Reid Hayhow
   */
  public void initializeEndEventDrivenMap(Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>> newMap)
  {
    _hardwareEventEnumEndToHardwareTaskMap = newMap;
  }

  /**
   * Public access to set up the Exception Driven Task map
   *
   * @author Reid Hayhow
   */
  public void initializeExceptionDrivenMap(Map<HardwareExceptionEnum, EventDrivenHardwareTask> newMap)
  {
    _hardwareExceptionEnumToHardwareTaskMap = newMap;
  }

  /**
   * Public access to set up the Time Driven Task list *and* start any of the
   * timer threads.
   *
   * @author Reid Hayhow
   */
  public void initializeTimeDrivenMap(List<TimeDrivenHardwareTask> newList)
  {
    //Guarantee an empty list, otherwise we can loose references to timer threads
    Assert.expect(_timeDrivenHardwareTasks.isEmpty());

    _timeDrivenHardwareTasks = newList;
    startTimerThreads();
  }

  /**
   * Encapsulate the process of getting an event back from the 2D map of
   * Major/Minor trigger events
   *
   * @author Reid Hayhow
   */
  private List<EventDrivenHardwareTask> getTaskForMajorMinorEventPair(HardwareEventEnum majorEvent,
      HardwareEventEnum minorEvent,
      boolean eventStart)
  {
    //Events can trigger a task at their start or at their end. First handle
    //the START case
    if(eventStart)
    {
      //Get the map (if any) of minor events that can trigger tasks when they
      //are seen in the context of the specified major event
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>>
          minorMapStart = _hardwareEventEnumStartToHardwareTaskMap.get(majorEvent);

      //if the minor event map is found, return the list (if any) of tasks
      //to run
      if (minorMapStart != null)
      {
        return minorMapStart.get(minorEvent);
      }
    }
    //Now handle the case of an event END
    else
    {
      //See notes above for details, this is almost identical except that
      //we search the end map instead of the start map
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>>
          minorMapEnd = _hardwareEventEnumEndToHardwareTaskMap.get(majorEvent);

      if (minorMapEnd != null)
      {
        return minorMapEnd.get(minorEvent);
      }
    }

    //If we are here, then no Major event key was found in the map, so certainly
    //there cannot be a task to run
    return null;
  }

  /**
   * Method to allow interested parties know if the system is in Adjustment.
   *
   * @author Reid Hayhow
   */
  public boolean isCalibrated()
  {
    //Start by assuming the system is OK
    boolean systemIsCalibrated = true;

    //Retreive the list of automated tasks. This cannot be initialized during
    //the construction of this class because it leads to recursive uglyness
    if(_listOfAllAutomatedTasks == null)
    {
      _listOfAllAutomatedTasks = com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList.getInstance();
    }

    //Now get the list of required tasks from the list of all automated tasks
    List<EventDrivenHardwareTask> listOfRequiredTasks = _listOfAllAutomatedTasks.getListOfRequiredTasks();

    //Loop over the list of required tasks and see if they are 'fresh'
    for (EventDrivenHardwareTask requiredTask : listOfRequiredTasks)
    {
      //NOTE: isExecuteNeeded should only be set to false after a task has run AND
      //passed, so this should be better than just checking if the task passed
      //the last time it ran. There is a small chance that this will return true
      //because the task has timed out *but* the system has not seen the event
      //necessary to trigger the running of the task. I don't think this is a problem
      //but wanted to note it here.
      if (requiredTask.getHardwareTaskType() == HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE &&
          requiredTask.peekIsExecuteNeeded() == true)
      {
        //If the task needs to be executed then the system is out of cal or has
        //a confirmation that hasn't passed.
        systemIsCalibrated = false;

        //One cal or confirm hasn't been run, so don't bother checking any more
        //because the system is not in Cal or failed a confirm
        break;
      }
    }
    return systemIsCalibrated;
  }

  /**
   * Method to allow interested parties know if the system has passed all required
   * Confirmations.
   *
   * @author Reid Hayhow
   */
  public boolean isConfirmed()
  {
    //Start by assuming the system is OK
    boolean systemIsConfirmed = true;

    //Retreive the list of automated tasks. This cannot be initialized during
    //the construction of this class because it leads to recursive uglyness
    if(_listOfAllAutomatedTasks == null)
    {
      _listOfAllAutomatedTasks = com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList.getInstance();
    }

    //Now get the list of required tasks from the list of all automated tasks
    List<EventDrivenHardwareTask> listOfRequiredTasks = _listOfAllAutomatedTasks.getListOfRequiredTasks();

    //Loop over the list of required tasks and see if they are 'fresh'
    for (EventDrivenHardwareTask requiredTask : listOfRequiredTasks)
    {
      //NOTE: isExecuteNeeded should only be set to false after a task has run AND
      //passed, so this should be better than just checking if the task passed
      //the last time it ran. There is a small chance that this will return true
      //because the task has timed out *but* the system has not seen the event
      //necessary to trigger the running of the task. I don't think this is a problem
      //but wanted to note it here.
      if (requiredTask.getHardwareTaskType() == HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE &&
          requiredTask.peekIsExecuteNeeded() == true)
      {
        //If the task needs to be executed then the system is out of cal or has
        //a confirmation that hasn't passed.
        systemIsConfirmed = false;

        //One cal or confirm hasn't been run, so don't bother checking any more
        //because the system is not in Cal or failed a confirm
        break;
      }
    }
    return systemIsConfirmed;
  }

  /**
   * Setter to allow TestExecution to pass the central timer down to the
   * HardwareTaskEngine.
   *
   * @author Reid Hayhow
   */
  public void setTestExecutionTimer(com.axi.v810.business.testExec.TestExecutionTimer timer)
  {
    Assert.expect(timer != null);

    _timer = timer;

    //Pass the timer on to all HardwareTasks
    HardwareTask.setTimerObject(_timer);
  }

  /**
   * @author Bill Darbie
   */
  public boolean areAllCalibrationsRunAtSoftwareStartup() throws DatastoreException
  {
    return _config.getBooleanValue(HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP);
  }

  /**
   * @author Bill Darbie
   */
  public void setRunAllCalibrationsOnSoftwareStartup(boolean runAllCalsAtStartup) throws DatastoreException
  {
    _config.setValue(HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP, runAllCalsAtStartup);
  }

  /**
   * Method to help recover from a camera crash. It sets all calibrations and
   * confirmations that need to be run for cameras to run as soon as possible
   *
   * @author Reid Hayhow
   */
  public void setCameraCalibrationsToRunASAP() throws DatastoreException
  {
    com.axi.v810.business.autoCal.CalGrayscale.getInstance().setTaskToRunASAP();
    com.axi.v810.business.autoCal.ConfirmCameraCalibration.getInstance().setTaskToRunASAP();

    com.axi.v810.business.autoCal.ConfirmCameraDarkImageTask.getInstance().setTaskToRunASAP();
    com.axi.v810.business.autoCal.ConfirmCameraLightImageWithNoPanel.getInstance().setTaskToRunASAP();

    /** @todo rfh remove this once LP3 is gone*/
    com.axi.v810.business.autoCal.ConfirmCameraGroupOneLightImageTask.getInstance().setTaskToRunASAP();
    com.axi.v810.business.autoCal.ConfirmCameraGroupTwoLightImageTask.getInstance().setTaskToRunASAP();
    com.axi.v810.business.autoCal.ConfirmCameraGroupThreeLightImageTask.getInstance().setTaskToRunASAP();
  }

  /**
   * Method to set all required calibrations and confirmations to run as soon as
   * possible.
   *
   * @author Reid Hayhow
   */
  public void setAllCalibrationsToRunASAP() throws DatastoreException
  {
    //Retreive the list of automated tasks. This cannot be initialized during
    //the construction of this class because it leads to recursive uglyness
    if(_listOfAllAutomatedTasks == null)
    {
      _listOfAllAutomatedTasks = com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList.getInstance();
    }

    //Now get the list of required tasks from the list of all automated tasks
    List<EventDrivenHardwareTask> listOfRequiredTasks = _listOfAllAutomatedTasks.getListOfRequiredTasks();

    //Loop over the list of required tasks and set them to run as soon as possible
    for (EventDrivenHardwareTask requiredTask : listOfRequiredTasks)
    {
      requiredTask.setTaskToRunASAP();
    }
  }


  /**
   *
   * @author Reid Hayhow
   */
  private boolean exceptionHandlingEnabled()
  {
    return _exceptionHandlingEnabled;
  }

  /**
   *
   * @author Reid Hayhow
   */
  public void disableExceptionHandling()
  {
    _exceptionHandlingEnabled = false;
  }

  /**
   *
   * @author Reid Hayhow
   */
  public void enableExceptionHandling()
  {
    _exceptionHandlingEnabled = true;
  }
}
