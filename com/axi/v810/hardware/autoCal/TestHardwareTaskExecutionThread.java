package com.axi.v810.hardware.autoCal;

import com.axi.util.*;

/**
 * Class to allow multi-thread testing of the HardwareTask
 *
 * @author Reid Hayhow
 */
public class TestHardwareTaskExecutionThread extends ThreadTask<Object>
{
  private HardwareTask _task;
  private boolean _executeTask = false;

  TestHardwareTaskExecutionThread()
  {
    super("TestHardwareTaskExecutionThread");
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void cancel() throws Exception
  {
    //Do nothing
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void clearCancel()
  {
    //do nothing
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected Object executeTask() throws Exception
  {
    Assert.expect(_task != null);

    //Determine if we should execute the task or cancel it
    if (_executeTask == false)
    {
      _task.cancel();
    }
    else
    {
      _task.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                    HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                    true);
    }

    return null;
  }

  /**
   * Setter method to get a handle to the task we will be working with
   *
   * @author Reid Hayhow
   */
  public void setHardwareTaskToCancel(HardwareTask task)
  {
    _task = task;
  }

  /**
   * When this ThreadTask runs, it should execute the specified task
   *
   * @author Reid Hayhow
   */
  public void setToExecuteTask()
  {
    _executeTask = true;
  }

  /**
   * When this ThreadTask runs, it should cancel the specified task
   *
   * @author Reid Hayhow
   */
  public void setToCancelTask()
  {
    _executeTask = false;
  }
}
