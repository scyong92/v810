package com.axi.v810.hardware.autoCal;

import com.axi.v810.util.*;

/**
 * Interface between the Service GUI and hardware tasks that it may want to
 * execute.
 *
 * @author Reid Hayhow
 */
public interface HardwareTaskExecutionInterface
{

  /**
   * Execute this task without checking timeout, task frequency, or any other
   * factor that might prevent it from executing right now. This does check
   * to make sure the task can run on the hardware. It also returns the
   * status of the task execution.
   *
   * @author Reid Hayhow
   */
  public boolean executeUnconditionally() throws XrayTesterException;


  /**
   * Get the name of the task. The name is a string that has already been
   * localized.
   *
   * @author Reid Hayhow
   */
  public String getName();
  public int getLoopCount();
  public void setLoopCount(int loopCount);

  /**
   * Method to inform the task that it should cancel. This method will block
   * until the task has stoped running. It is up to the task to decide when
   * it is OK for it to terminate, so this method should not be called by any
   * thread that cannot be blocked for a significant amount of time.
   *
   * @author Reid Hayhow
   */
  public void cancel();

  /**
   * Method to inform the task that it should cancel WHEN IT IS READY. This
   * is not guaranteed to cause the task to stop immediately. The task gets
   * to choose when it is at a good state to stop executing and how/if it needs
   * to restore the hardware to a known good state.
   *
   * @author Reid Hayhow
   */
  public void cancelNoWait();


  /**
   * Disable this HardwareTask and all children under it.
   *
   * @author Reid Hayhow
   */
  public void disable();


  /**
   * Enable this HardwareTask and all children under it.
   *
   * @author Reid Hayhow
   */
  public void enable();

  /**
   * Method to expose the enabled/disabled status of a taskInterface. It will
   * return true if the task is enabled, false otherwise
   *
   * @author Reid Hayhow
   */
  public boolean isEnabled();

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware();

  /**
   * Access to the high level status of the task. The enum provides a moderate
   * level of detail about the status of the task.
   *
   * @author Reid Hayhow
   */
  public HardwareTaskStatusEnum getTaskStatus();

  /**
   * Method to allow interface users to clear the task status for a given task.
   * This will set the HardwareTaskStatusEnum to NOT_RUN.
   *
   * @author Reid Hayhow
   */
  public void clearTaskStatus();

  /**
   * Method to execute tasks that need to be run before this task is executed.
   * It is based on each tasks internal timeout, so not all dependent tasks will
   * always be run. The method does *not* execute the task itself.
   *
   * @author Reid Hayhow
   */
  public boolean executeDependentTasks(int loopCount) throws XrayTesterException;
}
