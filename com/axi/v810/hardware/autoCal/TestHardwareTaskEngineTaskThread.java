package com.axi.v810.hardware.autoCal;

import com.axi.util.*;

/**
 * Class to allow multi-thread testing of the HardwareTaskEngine
 *
 * @author Reid Hayhow
 */
public class TestHardwareTaskEngineTaskThread extends ThreadTask<Object>
{
  private HardwareTaskEngine _engine;

  /**
   * TestHardwareTaskEngineTaskThread
   *
   * @author Reid Hayhow
   */
  TestHardwareTaskEngineTaskThread()
  {
    super("TestHardwareTaskEngineTaskThread");
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void cancel() throws Exception
  {
    //Do nothing
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void clearCancel()
  {
    //do nothing
  }

  /**
   * Call the HardwareTaskEngine cancel method (blocking) on a separate thread
   *
   * @author Reid Hayhow
   */
  protected Object executeTask() throws Exception
  {
    Assert.expect(_engine != null);

    _engine.cancel();

    return null;
  }

  /**
   * Setter method to get a handle to the HardwareTaskEngine
   *
   * @author Reid Hayhow
   */
  public void setHardwareTaskToCancel(HardwareTaskEngine engine)
  {
    _engine = engine;
  }
}
