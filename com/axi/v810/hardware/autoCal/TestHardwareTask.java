package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test harness for the generic HardwareTask class. This class extends on
 * HardwareTask to provide some simple internal counters and values that simplify
 * testing the basic HardwareTask class internals.
 *
 * @author Reid Hayhow
 */
public class TestHardwareTask extends HardwareTask
{
  protected boolean _isExecuteNeeded = true;
  private int _timesExecuted = 0;
  protected boolean _canRunOnThisHardware = true;
  private String _resultNameString = new String("Results for HardwareTask");
  private String _rootResultNameString = new String("Root results for HardwareTask");

  private boolean _throwExceptionOnExecute = false;
  private boolean _throwExceptionOnSetup = false;
  private boolean _throwExceptionOnCleanup = false;
  private int _exceptionToThrow = -1;
  public final static int DATASTORE_EXCEPTION = 0;
  public final static int HARDWARE_EXCEPTION = 1;
  public final static int BUSINESS_EXCEPTION = 2;
  public final static int XRAY_TESTER_EXCEPTION = 3;


  /**
   * TestHardwareTask
   */
  TestHardwareTask(String name)
  {
    super(name, ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);
    //Remove the default values created by the HardwareTask constructor
    _result = null;
    _limits = null;
  }

  public int getTimesExecuted()
  {

    return _timesExecuted;
  }

  /**
   * By default all tests can run on any hardware configuration.
   *
   * @return true if this test is capable of being run on the currently
   *   defined hardware.
   */
  public boolean canRunOnThisHardware()
  {
    return _canRunOnThisHardware;
  }

  /**
   *
   * @throws XrayTesterException
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(_throwExceptionOnCleanup)
    {
      switch(_exceptionToThrow)
      {
        case DATASTORE_EXCEPTION :
          throw new DatastoreException("fake file");
        case HARDWARE_EXCEPTION :
          throw new HardwareException(new LocalizedString("bogus", null));
        case BUSINESS_EXCEPTION :
          throw new com.axi.v810.business.autoCal.EdgeMTFBusinessException();
        case XRAY_TESTER_EXCEPTION :
          throw new XrayTesterException(new LocalizedString("Something went wrong!", null));
        default:
          Assert.expect(false);
      }
    }
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   */
  protected void createLimitsObject()
  {
    //Only create a new limits object if one does not exist, otherwise we
    //stomp on the ones created by the unit test
    if (_limits == null)
    {
      _limits = new Limit(new Integer( -2), new Integer(11), "foo", "XRay Tube Current");
    }
  }

  /**
   * TimeDrivenHardwareTask and EventDrivenHardwareTask need to implement this
   * method.
   *
   * @return boolean
   */
  protected boolean isExecuteNeeded()
  {
    return _isExecuteNeeded;
  }

  /**
   * This method should contain the task code.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //prove that I can do this test here
    _limits.didTestPass(_result);
    if (_limits.getLimitList().isEmpty() == false
        && _result.getResultList().isEmpty() == false)
    {
      _limits.getLimitList().get(1).didTestPass(_result.getResultList().get(1));
    }

    //Throw an exception if requested BEFORE incrementing internal counter
    if(_throwExceptionOnExecute)
    {
      switch(_exceptionToThrow)
      {
        case DATASTORE_EXCEPTION :
          throw new DatastoreException("fake file");
        case HARDWARE_EXCEPTION :
          throw new HardwareException(new LocalizedString("bogus", null));
        case BUSINESS_EXCEPTION :
          throw new com.axi.v810.business.autoCal.EdgeMTFBusinessException();
        case XRAY_TESTER_EXCEPTION :
          throw new XrayTesterException(new LocalizedString("Something went wrong!", null));
        default:
          Assert.expect(false);
      }
    }

    //If we got here, increment counter
    _timesExecuted++;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void setThrowExceptionOnExecute(int exceptionType)
  {
    _throwExceptionOnExecute = true;
    _exceptionToThrow = exceptionType;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void unsetThrowExceptionOnExecute()
  {
    _throwExceptionOnExecute = false;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void setThrowExceptionOnSetup(int exceptionType)
  {
    _throwExceptionOnSetup = true;
    _exceptionToThrow = exceptionType;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void unsetThrowExceptionOnSetup()
  {
    _throwExceptionOnSetup = false;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void setThrowExceptionOnCleanUp(int exceptionType)
  {
    _throwExceptionOnCleanup = true;
    _exceptionToThrow = exceptionType;
  }

  /**
   * Access to allow me to test exception handling within HardwareTasks
   *
   * @author Reid Hayhow
   */
  public void unsetThrowExceptionOnCleanup()
  {
    _throwExceptionOnCleanup = false;
  }

  protected void createResultsObject()
  {
    //Only set this up the first time, we want to clobber it later
    if (_result == null)
    {
      _result = new Result(new Integer(4), _resultNameString);
    }
    //Need to set this to NOT_RUN because I bypass the Result constructor
    //that would set this state and I want to be sure the state is correct
    _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
  }

  void setResultsObjectToFailLow()
  {
    _result = new Result(new Integer( -3), _resultNameString);
  }

  void setResultsObjectToFailHigh()
  {
    _result = new Result(new Integer(12), _resultNameString);
  }

  void setResultsObjectToPass()
  {
    _result = new Result(new Integer(10), _resultNameString);
  }

  /**
   * A function to create a list of results that will pass based on the limits
   * created in the corresponding function below
   *
   * @author Reid Hayhow
   */
  void createPassingResultsList()
  {
    _result = new Result(_rootResultNameString);
    _result.addSubResult(new Result(new Integer(10), _resultNameString));
    _result.addSubResult(new Result(new Integer(30), _resultNameString));
    _result.addSubResult(new Result(new Double(1.5), _resultNameString));
    _result.addSubResult(new Result(new Double(3.0), _resultNameString));

    Result subSubResult = new Result(_rootResultNameString);

    subSubResult.addSubResult(new Result(new Integer(10), _resultNameString));
    subSubResult.addSubResult(new Result(new Double(1.0), _resultNameString));

    _result.addSubResult(subSubResult);

  }

  /**
   * A function to create a list of limits
   *
   * @author Reid Hayhow
   */
  void createPassingLimitsList()
  {
    _limits = new Limit(null, null, null, "System Fiducal Cal for Camera 1");
    _limits.addSubLimit(new Limit(new Integer( -2), new Integer(11), "nanometers", "X Offset"));
    _limits.addSubLimit(new Limit(new Integer( -2), new Integer(41), "nanometers", "Y Offset"));
    _limits.addSubLimit(new Limit(new Double( -20.0), new Double(2.99), "pixels", "Row Offset"));
    _limits.addSubLimit(new Limit(new Double( -20.0), new Double(5.22), "pixels", "Column Offset"));

    Limit subSubLimit = new Limit(null, null, null, "Sub offset for camera 1");

    subSubLimit.addSubLimit(new Limit(new Integer( -2), new Integer(11), "goo", "Q Offset"));
    subSubLimit.addSubLimit(new Limit(new Double( -20.0), new Double(2.99), "moo", "R Offset"));

    _limits.addSubLimit(subSubLimit);
  }

  /**
   * A function to create a list of results that will fail based on the limits
   * created in the corresponding function above
   *
   * @author Reid Hayhow
   */
  void createFailingResultsList()
  {
    _result = new Result(_rootResultNameString);
    _result.getResultList().clear();
    _result.addSubResult(new Result(new Integer(10), _resultNameString));
    _result.addSubResult(new Result(new Integer(30), _resultNameString));
    _result.addSubResult(new Result(new Double(1.5), _resultNameString));
    _result.addSubResult(new Result(new Double(3.0), _resultNameString));

    Result subSubResult = new Result(_rootResultNameString);

    subSubResult.addSubResult(new Result(new Integer(10), _resultNameString));
    //This is the one test that will fail
    subSubResult.addSubResult(new Result(new Double(5.0), _resultNameString));

    _result.addSubResult(subSubResult);

  }

  /**
   * A workaround to be able to check the internal status of the HardwareTask
   * from the Unit Test class
   *
   * @return boolean
   * @author Reid Hayhow
   */
  public boolean taskPassed()
  {
    if (_result.getStatus().equals(HardwareTaskStatusEnum.PASSED))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Public function to demonstrate how to build up a list of the Limit Names
   *
   * @param currentLimit Limit
   */
  public void printLimits(Limit currentLimit)
  {
    printLimitName(currentLimit, _limits.getLimitName());
  }

  /**
   * Simple method to descend the Limit tree and collect the results into a
   * string.
   *
   * @param currentLimit Limit
   * @param limitString String
   */
  private void printLimitName(Limit currentLimit, String limitString )
  {
    //If we have a complex limit with a list under it, process it
    if(currentLimit.getLimitList().isEmpty() == false)
    {
      List<Limit> limitList = currentLimit.getLimitList();
      for(Limit limit : limitList)
      {
        //No matter what, append the current limit name to the string
        StringBuffer stringBuff = new StringBuffer(limitString);
        stringBuff.append("." + limit.getLimitName());

        //If this limit is complex itself (contains a list), then descend recursively
        //and process it
        if(limit.getLimitList().isEmpty() == false)
        {
          printLimitName(limit, stringBuff.toString());
        }
        //Otherwise we are done, just print the name
        else
        {
          System.out.println(stringBuff.toString());
        }
      }
    }
    //Otherwise we have a simple limit, no deeper to go, print it
    else
    {
      System.out.print(limitString);
    }
  }

  protected void setUp() throws XrayTesterException
  {
    if(_throwExceptionOnSetup)
    {
      switch(_exceptionToThrow)
      {
        case DATASTORE_EXCEPTION :
          throw new DatastoreException("fake file");
        case HARDWARE_EXCEPTION :
          throw new HardwareException(new LocalizedString("bogus", null));
        case BUSINESS_EXCEPTION :
          throw new com.axi.v810.business.autoCal.EdgeMTFBusinessException();
        case XRAY_TESTER_EXCEPTION :
          throw new XrayTesterException(new LocalizedString("Something went wrong!", null));
        default:
          Assert.expect(false);
      }
    }
  }

  /**
   * Default implementation of how Event Driven Hardware tasks notify of
   * failures. They throw exceptions which are caught by the GUI. This works
   * because they are in a direct throw path back to the UI.
   *
   * @author Reid Hayhow
   */
  protected void notifyOfFailure() throws HardwareTaskExecutionException
  {
    HardwareTaskStatusEnum taskStatus = _result.getStatus();
    if(taskStatus.equals(HardwareTaskStatusEnum.CANCELED) == false &&
       taskStatus.equals(HardwareTaskStatusEnum.NOT_RUN) == false )
    {
      HardwareTaskExecutionException ex = new HardwareTaskExecutionException(this);
      throw ex;
    }
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
