package com.axi.v810.hardware.autoCal;

import com.axi.util.Enum;

/**
 * @author Reid Hayhow
 */
public class HardwareTaskTypeEnum extends Enum
{
  private static int _index = -1;

  //Currently all Adjustments are automatic but it is possible that there may be
  //manual adjustments in the future
  public static HardwareTaskTypeEnum ADJUSTMENT_TASK_TYPE = new HardwareTaskTypeEnum(++_index);

  //Confirmations can be automatic or manual. All required Confirmations are also
  //automatic
  public static HardwareTaskTypeEnum AUTOMATIC_CONFIRMATION_TASK_TYPE = new HardwareTaskTypeEnum(++_index);
  public static HardwareTaskTypeEnum MANUAL_CONFIRMATION_TASK_TYPE = new HardwareTaskTypeEnum(++_index);

  //Currently there is only one diagnostic and it is manual. Eventually there
  //may be diagnostics that are automatically triggered by a failed Adjustment
  //or Confirmation
  public static HardwareTaskTypeEnum AUTOMATIC_DIAGNOSTIC_TASK_TYPE = new HardwareTaskTypeEnum(++_index);
  public static HardwareTaskTypeEnum MANUAL_DIAGNOSTIC_TASK_TYPE = new HardwareTaskTypeEnum(++_index);

  /**
   * @author Reid Hayhow
   */
  private HardwareTaskTypeEnum(int id)
  {
    super(id);
  }
}
