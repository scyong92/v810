package com.axi.v810.hardware.autoCal;

import com.axi.util.*;

/**
 * This is the Observable class for all Hardware Tasks that are run. When each
 * Hardware Task finishes executing a task, it will call the testFinished()
 * method. This method will then notify any registered observers that something
 * in the Hardware Task has changed. I put in a separate class to be the
 * Observable class rather than having each individual task be Observable to
 * keep things simple. This way an Observer only has to register with one
 * Observable instead of many. Note that the update method will be passed a COPY
 * of the results of the Hardware Task that just ran.
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
public class HardwareTaskObservable extends ThreadSafeObservable
{
  //instance member of this observable
  private static HardwareTaskObservable _instance;

  /**
   * Private constructor as part of singleton pattern
   *
   * @author Reid Hayhow
   */
  private HardwareTaskObservable()
  {
    super("HardwareTaskObservable thread");
  }

  /**
   * Instance access since we only want one observable thread out there
   *
   * @author Reid Hayhow
   */
  public static synchronized HardwareTaskObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new HardwareTaskObservable();
    }

    return _instance;
  }

  /**
   * When a test is completed it calls this method to indicate that a test was
   * just run and some of its data is likely to be different. Note that the
   * update() method will be called each time a test run is complete.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public void testFinished(Result result)
  {
    Assert.expect(result != null);

    setChanged();
    notifyObservers(result);
  }

}
