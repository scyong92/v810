package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * The HardwareTaskEngine will execute a HardwareTask when it needs to be run.
 * The HardwareTaskEngine class determines when to run each task based on the
 * information that it gets from the task object. The base HardwareTask class
 * should also log everything it is doing to a file so we can use that to make
 * sure it is working properly!
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
abstract public class HardwareTask implements HardwareTaskExecutionInterface
{
  //Private class variables
  private static XrayTester _xrayTesterInstance;
  private static HardwareTaskObservable _hardwareTaskObservable;
  private static boolean _stopOnFail = false;
  private static final long _MAX_TASK_CANCEL_WAIT_TIME = 5000;
  private static com.axi.v810.business.testExec.TestExecutionTimer _timer;
  private static Config _config = Config.getInstance();
  protected boolean _isHighMagTask = false;

  //Private variables
  private String _name;
  private long _lastTimeRunInMillis = 0;
  private boolean _running = false;
  private List<HardwareTask> _subTasks;
  private boolean _ignoreCancel = false;
  private ConfigEnum _lastTimeRunTimestampEnum;

  //Package private variables

  //Protected variables
  protected static HardwareTaskEngine _hardwareTaskEngine;
  protected static HardwareObservable _hardwareObservable;
  protected boolean _taskCancelled = false;
  protected Limit _limits;
  protected Result _result;
  protected boolean _enabled = true;
  protected boolean _updateObservers = true;
  protected ParseLimits _parsedLimits;
  protected boolean _forceLogging = false;
  protected HardwareTaskTypeEnum _taskType;
  protected ProgressReporterEnum _progressReporterEnum;
  protected ProgressObservable _progressObservable = ProgressObservable.getInstance();


  //Information set by the hardware task engine to provide execution context
  protected XrayTesterException _exception;
  protected HardwareEventEnum _majorEvent;
  protected HardwareEventEnum _minorEvent;
  protected boolean _minorEventIsStart;
  protected boolean _eventInformationIsValid = false;
  private boolean _triggeredFromSystemIdleThread = false;
  private boolean _timeDrivenEventStatusValid = false;


  protected com.axi.v810.business.autoCal.AdjustmentsAndConfirmationsLogger _adjustAndConfirmFileLogger =
      com.axi.v810.business.autoCal.AdjustmentsAndConfirmationsLogger.getInstance();
  
  private boolean _enableCDNABenchmark = false;
  private TimerUtil _taskTimer = new TimerUtil();
  private long _taskTime = 0;
  private int _loopCount = 0;
  private long _totalTaskTime = 0;
  private long _totalTaskCount = 0;
    
  //Log utility members For Benchmark
  protected static FileLoggerAxi _loggerForBenchmark = null;
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();

  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
  
  /**
   * @author Bill Darbie
   * @author Reid Hayhow
   * Static instance initializer, done once only for super class
   */
  static
  {
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareTaskObservable = HardwareTaskObservable.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _xrayTesterInstance = XrayTester.getInstance();

    //Initialize the timer class in a static initializer for UnitTesting ONLY
    if(UnitTest.unitTesting() == true)
    {
      _timer = new com.axi.v810.business.testExec.TestExecutionTimer();
    }
  }

  /**
   * @author Reid Hayhow
   *  Instance initializer, done every time for super and sub-classes
   */
  {
    _parsedLimits = new ParseLimits();
    _parsedLimits.parse(this);

    //Guarantee that we have a _result and _limit object
    //These will be overwritten by the individual tasks
    //using the createLimitsObject and createResultsObject methods
    //BUT this is necessary because those methods may allocate results and limits
    //based on data not available at object creation time (especially at the
    //HardwareTask level)
    _result = Result.createFailingResult(_name);
    _limits = Limit.createShouldPassLimit();
  }

  /**
   * Constructor for HardwareTask class Takes a name that identifies the task
   * (ie. HysteresisAdjustmentTask)
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  HardwareTask(String name, ProgressReporterEnum progressReporter)
  {
    Assert.expect(name != null);
    _name = name;
    _progressReporterEnum = progressReporter;

    _subTasks = new ArrayList<HardwareTask>();

    _enableCDNABenchmark = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK);

    if (_enableCDNABenchmark)
    {
      //Create the logger For Benchmark
      if (_loggerForBenchmark == null)
      {
        _loggerForBenchmark = new FileLoggerAxi(FileName.getConfirmBenchmarkFullPath()
          + FileName.getLogFileExtension(),
          Config.getInstance().getIntValue(SoftwareConfigEnum.XRAY_SOURCE_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES));
      }
    }
  }

  /**
   * @author Anthony Fong
   */
  static public FileLoggerAxi getLoggerForBenchmark()
  {
    return _loggerForBenchmark;
  }

  /**
   * Set ALL tasks to stop execution on failure. This is only used for a list of
   * sub-tasks, to stop later sub-tasks from being executed if a previous task
   * fails.
   * NOTE: recursive descent through sub-tasks is not needed because _stopOnFail
   * is a static class variable
   *
   * @author Reid Hayhow
   */
  public static void setStopOnFail(boolean stopOnFail)
  {
    _stopOnFail = stopOnFail;
  }

  /**
   * Get method for the string name of the Hardware Task
   *
   * @author Reid Hayhow
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Anthony Fong
   */
  public void setLoopCount(int loopCount)
  {
    _loopCount = loopCount;
  }
    
  /**
   * @author Anthony Fong
   */
  public int getLoopCount()
  {
    return _loopCount;
  }
  
  /**
   * @author Anthony Fong
   */
  public void resetTaskTimer()
  {
    _taskTimer.reset();
    _taskTime = 0;
  }
    
  /**
   * @author Anthony Fong
   */
  public long getTaskTime()
  {
    return _taskTime;
  }
  /**
   * @author Anthony Fong
   */
  public void updateTaskTime(Result result)
  {
    if (_enableCDNABenchmark == false)
    {
      return;
    }
    _taskTime = _taskTimer.getElapsedTimeInMillis();

    if (_loopCount == 0)
    {
      _totalTaskTime = _taskTime;
      _totalTaskCount = 1;
    }
    else
    {
      _totalTaskTime += _taskTime;
      _totalTaskCount++;
      _taskTime = _totalTaskTime / (_totalTaskCount);
    }
    com.axi.v810.business.autoCal.BenchmarkJsonFileEnum benchmarkFileEnum = null;

    if (this instanceof com.axi.v810.business.autoCal.ConfirmAllCommunication)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.System_Communication_Network_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmHTubeXRaySource)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.XRay_Source_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmAreaModeImages)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Area_Mode_Image_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalGrayscale)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Camera_Grayscale_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraCalibration)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Camera_Adjustment_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraDarkImageTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Camera_Dark_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraLightImageWithNoPanel)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Camera_Light_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalXraySpotCoordinateEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.XRay_Spot_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHysteresisEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Stage_Hysteresis_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalSystemFiducialEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.System_Offsets_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalMagnification)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Magnification_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmLowMagSystemMTF)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Base_Image_Quality_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmMagnificationCoupon)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.System_Image_Quality_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHighMagGrayscale)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Camera_Grayscale_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraHighMagCalibration)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Camera_Adjustment_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraHighMagDarkImageTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Camera_Dark_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmCameraHighMagLightImageWithNoPanel)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Camera_Light_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHighMagXraySpotCoordinateEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_XRay_Spot_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHighMagHysteresisEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Stage_Hysteresis_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHighMagSystemFiducialEventDrivenHardwareTask)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_System_Offsets_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.CalHighMagnification)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.High_Magnification_Adjustment;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmHighMagSystemMTF)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_Base_Image_Quality_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmMagnificationCouponForHighMag)
    {
      benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.HighMag_System_Image_Quality_Confirmation;
    }
    else if (this instanceof com.axi.v810.business.autoCal.ConfirmBenchmarkTask)
    {
      com.axi.v810.business.autoCal.ConfirmBenchmarkTask task = (com.axi.v810.business.autoCal.ConfirmBenchmarkTask) this;
      if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.LEFT_OUTER_BARRIER_OPEN_CLOSE))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Left_Outer_Barrier_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.RIGHT_OUTER_BARRIER_OPEN_CLOSE))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Right_Outer_Barrier_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.INNER_BARRIER_OPEN_CLOSE))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Inner_Barrier_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.PANEL_CLAMPS_OPEN_CLOSE))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Panel_Clamps_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.LEFT_PIP_IN_OUT))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Left_PIP_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.RIGHT_PIP_IN_OUT))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Right_PIP_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.XRAY_TUBE_ACTUATOR_UP_DOWN))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.XRay_Tube_Actuator_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Panel_Positioner_Motion_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Panel_Handler_AWA_Benchmark;
      }
      else if (task.getBenchmarkingEnum().equals(ConfirmBenchmarkEnum.LONG_PATH_SCANNING))
      {
        benchmarkFileEnum = com.axi.v810.business.autoCal.BenchmarkJsonFileEnum.Long_Path_Scanning_Benchmark;
      }
    }
    else
    {
      //do nothing
    }

    result.setBenchmarkTimingData(_taskTime, _totalTaskCount, benchmarkFileEnum);
  }

  /**
   * Add an observer for the Hardware Task class
   *
   * @author Reid Hayhow
   */
  public static synchronized void addObserver(Observer observer)
  {
    _hardwareTaskObservable.addObserver(observer);
  }

  /**
   * Remove ALL observers of the Hardware Task class
   *
   * @author Reid Hayhow
   */
  public static synchronized void deleteObservers()
  {
    _hardwareTaskObservable.deleteObservers();
  }

  /**
   * Remove an observer of the Hardware Task class
   *
   * @author Reid Hayhow
   */
  public static synchronized void deleteObserver(Observer observer)
  {
    _hardwareTaskObservable.deleteObserver(observer);
  }

  /**
   * Add a subtest to this test.
   *
   * @param test is the subtest that this test will contain.
   * @author Reid Hayhow
   */
  public void addSubTest(HardwareTask test)
  {
    _subTasks.add(test);
  }

  /**
   * Disable this test and any subtests that it contains.
   *
   * @author Reid Hayhow
   */
  public void disable()
  {
    _enabled = false;

    if (_subTasks.size() > 0)
    {
      Iterator<HardwareTask> iter = _subTasks.iterator();

      while (iter.hasNext())
      {
        HardwareTask task = iter.next();
        task.disable();
      }
    }
  }

  /**
   * Enable the test and sub-tests so they will do something when execute() is
   * called
   *
   * @author Reid Hayhow
   */
  public void enable()
  {
    _enabled = true;

    if (_subTasks.size() > 0)
    {
      Iterator<HardwareTask> iter = _subTasks.iterator();

      while (iter.hasNext())
      {
        HardwareTask task = iter.next();
        task.enable();
      }
    }
  }

  /**
   * Check to see if the current test is enabled or disabled. Since the
   * disable/enable functions are recursive, the only situation where this could
   * be incorrect is where the user creates a task, disables it and then adds it
   * as a sub-task.
   *
   * @author Reid Hayhow
   */
  public boolean isEnabled()
  {
    return _enabled;
  }

  /**
   * By default all tests can run on any hardware configuration. If a test
   * cannot run on some configurations override this method and make it return
   * false when it does not make sense to run it.
   *
   * @author Reid Hayhow
   */
  public abstract boolean canRunOnThisHardware();

  /**
   * Create a Limits object specific to the particular test being run. This
   * method should set the _limits instance variable. Note that the base class
   * HardwareTask will read the limits from the limits file and put them into
   * the protected variables _lowLimit
   *
   * @author Reid Hayhow
   */
  protected abstract void createLimitsObject() throws DatastoreException;

  /**
   * Control to set/unset the running status
   *
   * @author Reid Hayhow
   */
  void setRunning(boolean running)
  {
    _running = running;

    //Leverage this decision point to start/stop the test execution timer
    if(running == true)
    {
      _taskTimer.start();
      _timer.startCalibrationTimer();
    }
    else
    {
      _taskTimer.stop();
      _timer.stopCalibrationTimer();
    }
  }

  /**
   * Get method to return running status
   *
   * @author Reid Hayhow
   */
  boolean getRunning()
  {
    return _running;
  }

  /**
   * This method allows a single entry point for the Hardware Task Engine. All
   * abstract sub-classes must implement an isExecuteNeeded method, based on
   * their triger mechanism. All Hardware Task instances must provide an
   * executeTask method to actually execute.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  private boolean execute() throws XrayTesterException
  {
    //Assume the task won't run or will fail
    boolean taskRanSuccessfully = false;

    //If the task thinks it needs to execute, call executeUnconditionally()
    //which will make sure it is OK in this hardware
    boolean isExecuteNeeded = false;
    
    isExecuteNeeded = isExecuteNeeded();
    
    if (isExecuteNeeded)
    {

      if(_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) == true)
      {
        _adjustAndConfirmFileLogger.append(getName() + " execute called from event trigger");
      }

      taskRanSuccessfully = executeUnconditionally();

      //If the task didn't run successfully, throw an exception
      if(taskRanSuccessfully == false)
      {
        notifyOfFailure();
      }
    }
    else
    {
      if(_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE) == true)
      {
        _adjustAndConfirmFileLogger.append(getName() + " chose not to run");
      }
    }

    //return the actual status of the execution or false if not run
    return taskRanSuccessfully;
  }

  /**
   * Abstract method to notify interested parties of a failure. Must be overridden
   * by the task to provide more specific messages.
   *
   * @author Reid Hayhow
   */
  protected abstract void notifyOfFailure() throws HardwareTaskExecutionException;

  /**
   * Much the same as the regular execute, disableEvents so that the task itself
   * does not cause other tasks to run, call recursiveExecute and then
   * enableEvents again. This method allows a single entry point for callers who
   * want the task to execute regardless of the status of isExecuteNeeded. The
   * recursiveExecute method will use the isExecuteNeeded method to determine if
   * the task should be executed. All Hardware Task instances must provide an
   * executeTask method to actually execute.
   *
   * @author Reid Hayhow
   */
  public synchronized boolean executeUnconditionally() throws XrayTesterException
  {
    boolean taskPassed = false;
    boolean exception = false;

    resetTaskTimer();
    
    //Everything passes in hardware sim mode
    if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true
      && UnitTest.unitTesting() == false)
    {
      // Uncomment For Manual Debug Purposes in Simulation Mode
//      if (this instanceof com.axi.v810.business.autoCal.ConfirmBenchmarkTask)
//      {
//      }
//      else
      {
        fudgeResultsForHardwareSimMode();
        return true;
      }
    }

    XrayCameraArray.getInstance().setDefaultCamerasLight();

    //Create new results and limits for this task container
    //This should be done even if it cannot run on the hardware, just to guarantee
    //the status is set to NOT_RUN
    initializeResultsAndLimits();

    //For the GUI, we only check to see if we can run on this hardware,
    //unlike the normal execute that checks to make sure an execute is needed
    if (canRunOnThisHardware())
    {
      //Force logging for this use case because calls to this method are user
      //directed and the user will want to see results
      /**@todo revisit forced logging now that all calls go through here*/
      _forceLogging = true;

      // Guarantee that the system is ready to run a task before executing.
      // This needs to be done before observer notification or disable of events
      if (_xrayTesterInstance.isStartupRequired())
      {
        // If the tester isn't started up we can't run, indicate this and return
        // Don't startup because a startup may be in progress, causing nasty
        // circularity!
        taskPassed = false;

        // Handle the result information for this case
        handleResultForTaskNotRunBecauseHardwareNotStarted();

        // Clear the canceled flag, just in case it got set somehow
        _taskCancelled = false;

        return taskPassed;
      }

      //Assure the engine is ignoring events before we send our execution event
      boolean eventsWereEnabled = _hardwareTaskEngine.eventsAreEnabled();
      if (eventsWereEnabled)
      {
        _hardwareTaskEngine.disableEvents();
      }

      //Notify observers that a hardwareTask is running
      //This also frees up task instances to do multi-threading
      //NOTE:This needs to be done outside the try/finally block that will
      //setEnabled() back to true. The setEnabled() call will cause an assert
      //because it was never disabled if stateChangedBegin threw an exception
      _hardwareObservable.stateChangedBegin(this, HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT);
      _hardwareObservable.setEnabled(false);

      try
      {
        //Xray tube crash preventive action
        //ensure panel was cleared after the panel being unloaded before performing CDA
        if(this instanceof com.axi.v810.business.autoCal.ConfirmPanelHandlingTask)
        {
            //bypass checkPanelClearedAfterPanelUnloadedBeforeCDA();
            //this task required panel loading and unloading function
        }
        else
        {
              if (PanelHandler.getInstance().checkPanelClearedAfterPanelUnloadedBeforeCDA() == false)
              {
                // Skip the CDA
                taskPassed = false;
                return taskPassed;
              }
        }
        
        //bypass unnecessary task when in passive mode
        if (isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
        {
          // Skip the CDA
          
          // Handle the not enabled case
          // The task didn't run, but didn't really fail. Failure may stop other tasks from running
          taskPassed = true;

          // Indicate the fact that the task hasn't run
          _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);

          // Log the fact that it was disabled into the log file
          _adjustAndConfirmFileLogger.append(getName() + " was not run because it is not enabled.");
          
          //Set the timestamp to NOW since the task passed. This will also set
          //the timestamp for container tasks.
          setLastTimeRunToNow();
          
          return taskPassed;
        }
        
        if (_isHighMagTask == true && PanelHandler.getInstance().isPanelLoaded())
          throw new com.axi.v810.business.autoCal.PanelInterferesWithAdjustmentAndConfirmationTaskException(_name);
        
        if (this instanceof com.axi.v810.business.autoCal.ConfirmBenchmarkTask)
        {
        }
        else
        {
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
          {
            taskPassed = false;
            return taskPassed;
          }
        }
                
        //Call the setup ONCE only
        setUp();

        //Run the task
        taskPassed = recursiveExecute();

        if (taskPassed)
        {
          //Set the timestamp to NOW since the task passed. This will also set
          //the timestamp for container tasks.
          setLastTimeRunToNow();
        }
        else
        {
          setTaskToRunASAP();
        }
      }
      //If we see an exception we MUST enable HardwareTaskEngine events before
      //leaving
      //
      //NOTE: This cannot go in the finally block, because then the task engine
      //would see the HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT end
      //event and would loose the correct state information
      catch( XrayTesterException xte)
      {
        exception = true;

        // Preempt all further tasks.
        _hardwareTaskEngine.cancelNoWait();
        
        //Then throw the original exception along
        throw xte;
      }
      finally
      {
        //Always set the event enabled for the observable
        _hardwareObservable.setEnabled(true);

        //Clear the task canceled status before leaving. This must be done in the
        //finally block because someone else may have encountered an exception,
        //called cancel and then thrown the exception.
        _taskCancelled = false;

        //Now try to cleanup, it may throw an exception but that can't
        //be avoided
        try
        {
          if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)==false) 
          {
            cleanUp();
            //For safety purposes, move the Z Axis to Home and Ready position
            if(XrayActuator.isInstalled() == true)
            {
              //Bypass the previous cylinder implementation to maintian the Speed Performance 
//              if (XrayCylinderActuator.isInstalled() == true)
//              {
//                //move x-rays Cylinder to Up and Safe position.
//                XrayActuator.getInstance().up(false);
//              }
//              else 
              if (XrayCPMotorActuator.isInstalled() == true)
              {
                //move x-rays Z Axis to Home and Safe position.
                XrayActuator.getInstance().home(false,false);
              }
            }          
          }
        }
        catch (XrayTesterException ex)
        {
          if (exception)
          {
            // we have had a previous exception, so just print this new one to the log file and throw the original one
            ex.printStackTrace();
          }
          else
          {
            exception = true;
            // there was no previous exception, so throw this one
            throw ex;
          }
        }
        finally
        {
          if (exception == false)
          {
            //Notify observers we are done ONLY if no exception was seen
            _hardwareObservable.stateChangedEnd(this, HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT);
          }

          //Don't always enable the task engine. Someone higher up may have disabled
          //events (ie. for UI execution), so only enable events if they were enabled
          //before the task executed
          //
          //NOTE: This is a duplicate of the handler in the catch block above, necessary
          //because of the HardwareObservable 'contract' about setEnabled() being
          //in the finally block, before the end event is sent. The task engine needs
          //to ignore the end event *and* it always needs to be reset to enableEvents
          if (eventsWereEnabled)
          {
            _hardwareTaskEngine.enableEvents();
          }

          //Notify anyone waiting (cancel) on our monitor that it is cleared
          notifyAll();

          //Restore the state of the logging
          /**@todo revisit forced logging now that all calls go through here*/
          _forceLogging = false;
        }// End of finally to handle throws from cleanUp() method
      }// End of finally block from task execution
    } //end if(canRunOnThisHardware)
    else
    {
      // Handle the case where the task can't run on this hardware
      // First, set the status to indicate that it was not run
      _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);

      // But it didn't exactly fail. Failure might cause further tasks to not execute
      taskPassed = true;

      // Update observers with the NOT_RUN result
      if (_updateObservers)
      {
        updateTaskTime(_result);
        _hardwareTaskObservable.testFinished(new Result(_result, _limits));
      }

      // Log the fact that the task didn't run in the log file
      _adjustAndConfirmFileLogger.append(getName() + " is not runnable on this hardware.");
    }

    XrayCameraArray.getInstance().setUserDefinedCamerasLight();

    return taskPassed;
  }

  /**
   * Method to handle setting up the result information for a task that could not
   * run because the hardware has not been started up as well as updating any observers
   * of this fact.
   *
   * @author Reid Hayhow
   */
  private void handleResultForTaskNotRunBecauseHardwareNotStarted() throws XrayTesterException
  {
    // The task couldn't run
    _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);

    // Get the name of this task to use as a parameter to the result message
    Object[] params = {getName()};

    // Create the localized strings with parameters
    LocalizedString status = new LocalizedString("ACD_SYSTEM_NOT_STARTED_TASK_STATUS_KEY", params);
    LocalizedString suggestedAction = new LocalizedString("ACD_SYSTEM_NOT_STARTED_SUGGESTED_ACTION_KEY", params);

    // Set the result messages with the localized string
    _result.setTaskStatusMessage(StringLocalizer.keyToString(status));
    _result.setSuggestedUserActionMessage(StringLocalizer.keyToString(suggestedAction));

    // If we need to update observers, do so
    if(_updateObservers)
    {
      updateTaskTime(_result);
      _hardwareTaskObservable.testFinished(new FailedHardwareTaskResult(_result, _limits));

      //Log the fact that a task didn't run
      _adjustAndConfirmFileLogger.append(getName() + " Not run, system not started.");
    }
  }

  /**
   * TimeDrivenHardwareTask and EventDrivenHardwareTask need to implement this
   * method. It will determine if the task needs to run. If it does, then
   * executeTask() will be called.
   *
   * @author Bill Darbie
   */
  abstract protected boolean isExecuteNeeded();

  /**
   * This method should contain the task code. It should check that the task was
   * sucessful. If it was not then a diagnostic routine should be run to
   * determine the hardware failure. An XrayTesterException should be thrown to
   * indicate any errors.
   *
   * @author Bill Darbie
   */
  abstract protected void executeTask() throws XrayTesterException;

  /**
   * This method gets called by execute(). It is a recursive funtion. The
   * execute() method takes care of the _testCancelled flag. That flag could not
   * be handled properly in a recursive method so I created this separate method
   * that actually does the recursion. If this test contains subtests, call
   * their execute() method. otherwise perform the test. Note that this test
   * will use the _results and _limits objects to determine pass/fail status.
   * NOTE: If there are sub-tasks for this HardwareTask, the root HardwareTask
   * will *not* be executed!
   *
   * @author Bill Darbie
   */
  private boolean recursiveExecute() throws XrayTesterException
  {
    boolean pass = false;

    if (_enabled)
    {
      //Handle recursive execute of sub tasks
      if (_subTasks.size() > 0)
      {
        //execute the task and preserve the result
        pass = handleSubTaskExecution();
      }
      //This is a leaf node of a list of subTasks
      else
      {
        //Log the fact that a task is starting
        _adjustAndConfirmFileLogger.append("Starting " + getName());
        
        //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
        {
          try
          {
            if (_machineUtilizationReportSystem.isServicing() == false && _machineUtilizationReportSystem.isProgramming() == false)
            {
              _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.MACHINE_SETUP_RUNNING, getName());
            }
          }
          catch (DatastoreException ex)
          {
            ex.printStackTrace();
          }
        }
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if (_vOneMachineStatusMonitoringSystem.isServicing() == false && _vOneMachineStatusMonitoringSystem.isProgramming() == false)
              _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.MACHINE_SETUP_START);
          }
          catch (DatastoreException ex)
          {
            ex.printStackTrace();
          }
        }

        //Create new results and limits for this sub task before execution
        initializeResultsAndLimits();

        try
        {
          // check if test is cancelled before calling setUp and executeTest
          if ((_taskCancelled == false) || (_ignoreCancel == true))
          {
            setRunning(true);

            executeTask();
          }

          // check if test is cancelled again since it might have been cancelled during the
          //executeTest() call. It should probably be set there as well, but make sure!
          if ((_taskCancelled) && (_ignoreCancel == false))
          {
            _result.setStatus(HardwareTaskStatusEnum.CANCELED);
            pass = false;
          }
          else
          {
              pass = _limits.didTestPass(_result);

            //Everything passes in sim mode
            if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
            {
              //Unless we are unit testing, then it needs to be real
              if(UnitTest.unitTesting() == false)
              {
                pass = true;
              }
            }
          }
        }
        catch (XrayTesterException exception)
        {
          // No matter what, log something if an exception was encountered
          _adjustAndConfirmFileLogger.append(getName() + " Exception encountered during execution...");
          _adjustAndConfirmFileLogger.append(exception.getLocalizedMessage());

          // After an exception, set the status
          _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);

          // Copy over any exception messages to the task status
          _result.setTaskStatusMessage(exception.getLocalizedMessage());

          // Also add the localized message indicating what the user should do
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_UNKNOWN_EXCEPTION_DURING_TASK_EXECUTION_SUGGESTED_ACTION_KEY"));

          // let the observable class know that a test just completed
          // pass out a copy of this object
          if (_updateObservers)
          {
            updateTaskTime(_result);
            // NOTE: The observer will be given a result for every HardwareTask
            // executed, even if they are sub-tasks of a task
            _hardwareTaskObservable.testFinished(new FailedHardwareTaskResult(_result, _limits));
          }

          //Make sure the task is set to fail
          pass = false;

          //Throw the exception up the chain
          throw exception;
        }
        //Assure that the task is set to not be running any more
        finally
        {
          setRunning(false);
          updateTaskTime(_result);
          //Clear the task canceled flag, just in case an exception set it.
          _taskCancelled = false;

          reportTaskDone();
        }

        //Set the stop time before updating the observers
        setResultsStopTimeStamp(_result);

        // let the observable class know that a test just completed
        // pass out a copy of this object
        if (_updateObservers)
        {
          // NOTE: The observer will be given a result for every HardwareTask
          // executed, even if they are sub-tasks of a task
          if (pass)
          {
            _hardwareTaskObservable.testFinished(new Result(_result, _limits));

            //This task passed, so set the timestamp to now
            setLastTimeRunToNow();

            //Log the fact that a task just finished
            _adjustAndConfirmFileLogger.append("Finished " + getName() + ", passed.");
          }
          //If the task fails, pass out a sub-class of the result class, instead
          //of the base class. This allows anyone who doesn't care about passing
          //results to safely ignore them
          else
          {
            _hardwareTaskObservable.testFinished(new FailedHardwareTaskResult(_result, _limits));

            setTaskToRunASAP();

            //Log the fact that a task just finished
            _adjustAndConfirmFileLogger.append("Finished " + getName() + ", failed.");
          }
          
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
          {
            try
            {
              if (_machineUtilizationReportSystem.isServicing() == false && _machineUtilizationReportSystem.isProgramming() == false)
              {
                _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.MACHINE_SETUP_DONE, getName());
              }
            }
            catch (DatastoreException ex)
            {
              ex.printStackTrace();
            }
          }
          
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
          {
            try
            {
              if (_vOneMachineStatusMonitoringSystem.isServicing() == false && _vOneMachineStatusMonitoringSystem.isProgramming() == false)
                _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.MACHINE_SETUP_END);
            }
            catch (DatastoreException ex)
            {
              ex.printStackTrace();
            }
          }
        }
      } //end of else to handle leaf node
    } //end of if(_enabled)
    else
    {
      // Handle the not enabled case
      // The task didn't run, but didn't really fail. Failure may stop other tasks from running
      pass = true;

      // Indicate the fact that the task hasn't run
      _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);

      // Log the fact that it was disabled into the log file
      _adjustAndConfirmFileLogger.append(getName() + " was not run because it is not enabled.");
    }

    return pass;
  }


  /**
   * Method to encapsulate preparing the result and limit objects for execution.
   * Moved to a separate function to avoid code duplication in recursiveExecute.
   *
   * @author Reid Hayhow
   */
  private void initializeResultsAndLimits() throws DatastoreException
  {
    //If the task is not enabled, but an execute was requested, flush
    //the old results and create a fresh set
    createResultsObject();
    Assert.expect(_result != null);
    //Set the start time just before executing the task
    setResultsStartTimeStamp(_result);

    _parsedLimits.parse(this);
    createLimitsObject();
    // the limits object better be set
    Assert.expect(_limits != null);
  }

  /**
   * Method to handle the execution of sub tasks. It will recurse through the
   * sub tasks, excuting them in order. It also handles setting the result for
   * the sub task container correctly based on the sub task execution results.
   *
   * @author Reid Hayhow
   */
  private boolean handleSubTaskExecution() throws XrayTesterException
  {
    //Assume the overall task will pass. Individual tasks are assumed to fail
    boolean pass = true;

    //Assume things will pass. Code below will catch any failures
    //and use them to overwrite the pass
    _result.setStatus(HardwareTaskStatusEnum.PASSED);

    //Also, clear out the root result status message to we have a clean slate
    _result.setTaskStatusMessage("");

    //Loop over sub-tasks and execute them

    for (HardwareTask subTask : _subTasks)
    {
      //Assume subTasks will fail until proven otherwise
      boolean subTaskPass = false;

      //Since the sub tasks may throw an exception, we need to be able to
      //handle this at the result container level by setting the result
      //correctly
      try
      {
        //Xray tube crash preventive action
        //ensure panel was cleared after the panel being unloaded before performing CDA
        if(this instanceof com.axi.v810.business.autoCal.ConfirmPanelHandlingTask)
        {
            //bypass checkPanelClearedAfterPanelUnloadedBeforeCDA();
            //this task required panel loading and unloading function
        }
        else
        {
              if (PanelHandler.getInstance().checkPanelClearedAfterPanelUnloadedBeforeCDA() == false)
              {
                // Skip the CDA
                pass = false;
                return pass;
              }
        }
        
        //bypass unnecessary task when in passive mode
        if (isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
        {
          // Skip the CDA

          // Handle the not enabled case
          // The task didn't run, but didn't really fail. Failure may stop other tasks from running
          pass = true;

          // Indicate the fact that the task hasn't run
          _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);

          // Log the fact that it was disabled into the log file
          _adjustAndConfirmFileLogger.append(getName() + " was not run because it is not enabled.");
          
          //Set the timestamp to NOW since the task passed. This will also set
          //the timestamp for container tasks.
          setLastTimeRunToNow();
          
          return pass;
        }
        
        if (_isHighMagTask == true && PanelHandler.getInstance().isPanelLoaded())
          throw new com.axi.v810.business.autoCal.PanelInterferesWithAdjustmentAndConfirmationTaskException(_name);

        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
        {
            pass = false;
            return pass;
        }
        // Allow the sub task a chance to set things up.
        subTask.setUp();

        subTaskPass = subTask.recursiveExecute();

        //Accumulate the pass/fail data to return to caller
        pass = pass && subTaskPass;
      }
      catch (XrayTesterException xte)
      {
        // An exception guarantees that the subTask didn't pass and neither did the whole task
        subTaskPass = false;
        pass = false;
        throw xte;
      }
      finally
      {
        //Always copy over messages, status first
        StringBuilder buffer = new StringBuilder(_result.getTaskStatusMessage());
        //If the buffer contains something, add a newline to create a more readable message
        if(buffer.length() > 0)
        {
          buffer.append(System.getProperty("line.separator"));
        }
        buffer.append(subTask.getResult().getTaskStatusMessage());
        _result.setTaskStatusMessage(buffer.toString());

        //Then the action message. In this case, don't append this, just copy it
        //over. Otherwise duplicate failures may have the same suggested action
        _result.setSuggestedUserActionMessage(subTask.getResult().getSuggestedUserActionMessage());

        // if the task threw an exception, always set the result
        // container to the status of the sub task
        if (subTaskPass == false)
        {
          //Set the parent status to the status of the child node, this
          //guarantees it won't pass
          _result.setStatus(subTask.getTaskStatus());
        } //end of if(subTaskPass == false)

        // Give the subTask a chance to clean up after itself
        // NOTE: Because this is a sub-task, events have already been disabled, so
        // it is OK to just allow the cleanUp call to throw without a try/catch
        subTask.cleanUp();
        
        //For safety purposes, move the Z Axis to Home and Ready position
        if(XrayActuator.isInstalled() == true)
        {
          if (XrayCylinderActuator.isInstalled() == true)
          {
            //move x-rays Cylinder to Up and Safe position.
            XrayActuator.getInstance().up(false);
          }
          else 
          if (XrayCPMotorActuator.isInstalled() == true)
          {
            //move x-rays Z Axis to Home and Safe position.
            XrayActuator.getInstance().home(false,false);
          }
        }   

      } // end of finally block

      // if the task failed an exception, always set the result
      // container to the status of the sub task
      if (subTaskPass == false)
      {
        // If _stopOnFail is set we need to break out of the execution loop but only
        // after setting the task container status according to the status of the sub task
        // NOTE: this cannot go into a finally block because it breaks out of the exception handler
        if (_stopOnFail)
        {
          break;
        }
      }
    } //end of while(it.hasNext())
    return pass;
  }

  /**
   * This method works exactly like the execute() method except that it will not
   * call update on any Observers when the test completes and it cannot be
   * cancelled.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public void executeNonInteractively() throws XrayTesterException
  {
    disableObserverUpdates();
    _ignoreCancel = true;

    // Wrap the execute in a try block so we can cleanup afterwards
    try
    {
      executeUnconditionally();
    }
    // Re-enable the observer updates and ignore cancel to previous state
    finally
    {
      enableObserverUpdates();
      _ignoreCancel = false;
    }
  }

  /**
   * This method works exactly like the execute() method except that it will not
   * call update on any Observers when the test completes. This could be useful
   * for a Confirmation that needs to call another Confirmation test (or
   * possibly an Adjustment) and use their results to determine if it is a
   * passing or failing test.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public boolean executeWithoutUpdate() throws XrayTesterException
  {
    boolean taskPassed = false;

    //Turn off updates
    disableObserverUpdates();

    try
    {
      //Execute the task and save the status of the task
      taskPassed = executeUnconditionally();
    }
    finally
    {
      //Always turn observers back on, even if execute throws an exception
      enableObserverUpdates();
    }

    //Return the status of the task
    return taskPassed;
  }

  /**
   * Recursive method to disable observer updates for this task and all sub-tasks
   *
   * @author Reid Hayhow
   */
  private void disableObserverUpdates()
  {
    // Disable observer updates for this task
    _updateObservers = false;

    // Get the list of sub-tasks and disable them as well
    List<HardwareTask> subTaskList = getSubTests();
    for(HardwareTask subTask :subTaskList)
    {
      subTask.disableObserverUpdates();
    }
  }

  /**
   * Recursive method to enable observer updates for this task and all sub-tasks
   *
   * @author Reid Hayhow
   */
  private void enableObserverUpdates()
  {
    // Enable observer updates for this task
    _updateObservers = true;

    // Enable observer updates for all sub tasks as well
    List<HardwareTask> subTaskList = getSubTests();
    for(HardwareTask subTask :subTaskList)
    {
      subTask.enableObserverUpdates();
    }
  }

  /**
   * Stop the currently running task as quickly as possible. NOTE: It is up to
   * the executeTask method of sub-classes to poll on the _abort variable in
   * order to respond to an abort call.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public synchronized void cancel()
  {
    if (getRunning() && !(_ignoreCancel))
    {
      _taskCancelled = true;

      try
      {
        wait(_MAX_TASK_CANCEL_WAIT_TIME);
      }
      catch (InterruptedException ex)
      {
        //do nothing?
      }
    }
  }

  /**
   * Preserve the possibility of having a non-blocking abort method
   *
   * @author Reid Hayhow
   */
  public void cancelNoWait()
  {
    if (getRunning() && !(_ignoreCancel))
    {
      _taskCancelled = true;
    }
  }
  /**
   * This method provides a single point of contact where tasks can put their
   * clean-up code that may be reused (for example executeTask and abortTask may
   * both use the same cleanUp code)
   *
   * @author Reid Hayhow
   */
  protected abstract void cleanUp() throws XrayTesterException;

  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   *
   * @author Reid Hayhow
   */
  protected abstract void setUp() throws XrayTesterException;

  /**
   * Return the time since this task was last run in milliseconds.
   *
   * @author Bill Darbie
   */
  public long getElapsedTimeSinceLastRunInMillis()
  {
    long currTimeInMillis = System.currentTimeMillis();
    long elapsedTimeInMillis = currTimeInMillis - _lastTimeRunInMillis;
    return elapsedTimeInMillis;
  }

  /**
   * Method to set the execution timestamp to the current time. This should be
   * used with caution because the various execute methods should update this
   * time correctly. It is necessary because there is at least one case where
   * sub-tasks have different timeouts than a root-task and they need to be
   * able to guarantee that the root-task correctly reports when ALL sub-tasks
   * have been run.
   *
   * @author Reid Hayhow
   */
  private void setLastTimeRunToNow() throws DatastoreException
  {
    //If the task was deferred, don't set the timeout, just return
    if(_result.getStatus() == HardwareTaskStatusEnum.DEFERRED)
    {
      return;
    }

    _lastTimeRunInMillis = System.currentTimeMillis();

    //Use the hardware.calib enum (if any) to save the timestamp
    if(_lastTimeRunTimestampEnum != null)
    {
      _config.setValue(_lastTimeRunTimestampEnum, _lastTimeRunInMillis);
    }
  }

  /**
   * Return the number of tests that are enabled in this test and any of its
   * subtests if they exist. NOTE: This function returns the count of sub-tasks
   * enabled OR the status of the task itself. It does NOT count both the task
   * and sub-tasks that are enabled.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public int getEnabledTestCount()
  {
    int numEnabledTests = 0;
    if (isEnabled())
    {
      if (_subTasks.size() == 0)
      {
        // this is a leaf node and it is enabled so increment the count
        ++numEnabledTests;
      }
      else
      {
        // we are not in a leaf - call all the subtests to find out
        // what their count is and add it in to the total count
        Iterator<HardwareTask> it = _subTasks.iterator();
        while (it.hasNext())
        {
          HardwareTask test = (HardwareTask)it.next();
          numEnabledTests += test.getEnabledTestCount();
        }
      }
    }
    return numEnabledTests;
  }

  /**
   * createResultsObject Create the results object for this test result. Each
   * sub-class will need to decide exactly what kind of results to use.
   *
   * @author Reid Hayhow
   */
  protected abstract void createResultsObject() throws DatastoreException;

  /**
   * getSubTests
   *
   * @author Reid Hayhow
   */
  public List<HardwareTask> getSubTests()
  {
    return _subTasks;
  }

  /**
   * Recursively decend throught the results lists and set the start time stamp
   * on all.
   *
   * @author Reid Hayhow
   */
  private void setResultsStartTimeStamp(Result result)
  {
    //Set the start time stamp for this result
    result.startTimeStamp();

    //Recursively set the start time stamp on all subResults
    for (Result subResult : result.getResultList())
    {
      setResultsStartTimeStamp(subResult);
    }
  }

  /**
   * Recursively decend throught the results lists and set the stop time stamp
   * on all.
   *
   * @author Reid Hayhow
   */
  private void setResultsStopTimeStamp(Result result)
  {
    //Set the stop time stamp for this result
    result.stopTimeStamp();

    //Recursively set the stop time stamp on all subResults
    for (Result subResult : result.getResultList())
    {
      setResultsStopTimeStamp(subResult);
    }
  }

  /**
   * Method to get access to the result. Useful for displaying
   * detailed failures
   *
   * I may replace this method with a better approach for accessing Result and
   * Limit information
   *
   * @author Reid Hayhow
   */
  public Result getResult()
  {
    return _result;
  }

  /**
   * Method to get access to the limits. Useful for displaying
   * detailed failures
   *
   * @author Reid Hayhow
   */
  public Limit getLimit()
  {
    return _limits;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    return getName();
  }

  /**
   * Access to the high level status of the Task.
   *
   * @author Reid Hayhow
   */
  public HardwareTaskStatusEnum getTaskStatus()
  {
    return _result.getStatus();
  }

  /**
   * Allow users to clear the status of the last, saved result. The result is
   * always cleared out before a task is executed, so this should be safe.
   *
   * @author Reid Hayhow
   */
  public void clearTaskStatus()
  {
    _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
  }

  /**
   * Method to allow execution of tasks that need to be run before a given task
   * is executed. This method goes through a list of tasks that have to be run,
   * executes them (if necessary).
   *
   * @author Reid Hayhow
   */
  public boolean executeDependentTasks(int loopCount) throws XrayTesterException
  {
    //Variable to sum up the execution status of the dependent tasks
    boolean executionStatus = true;

    //The list of tasks and order of tasks resides in the XrayHardwareAutomatedTaskList class
    //NOTE: the full class reference is to avoid breaking architectural boundaries
    //and test cases
    com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList xrayTaskList =
      com.axi.v810.business.autoCal.XrayHardwareAutomatedTaskList.getInstance();

    //Get the special list of required tasks, in order, from the xrayTaskList
    List<EventDrivenHardwareTask> listOfRequiredTasks = xrayTaskList.getListOfRequiredTasks();

    //Get an iterator to the list
    ListIterator<EventDrivenHardwareTask> requiredTasksListIterator = listOfRequiredTasks.listIterator();

    //Loop over the list of tasks as long as it has more members,  we have
    //not encountered this task and none of the dependent tasks have failed
    while(requiredTasksListIterator.hasNext())
    {
      //Get the next task in the list
      EventDrivenHardwareTask nextRequiredTask = requiredTasksListIterator.next();

      //If the next task IS this task, don't execute it and stop searching
      if(nextRequiredTask.equals(this))
      {
        break;
      }
      else
      {
        //In order to bypass any exception that may be thrown from the execute()
        //we check here to see if it needs to be executed and then call
        //executeUnconditionally() which won't throw an exception
        if(nextRequiredTask.peekIsExecuteNeeded() && nextRequiredTask.isEnabled())
        {
          nextRequiredTask.setLoopCount(loopCount);
          //Execute the task and accumulate the status
          executionStatus &= nextRequiredTask.executeUnconditionally();
          _result.setTaskStatusMessage("executeDependentTasks");
          //nextRequiredTask.updateTaskTime(_result);
        }

        //If stopOnFail is set and the task fails, bail out
        if((_stopOnFail == true && executionStatus == false))
        {
          break;
        }
      }
    }

    
    //Return the execution status
    return executionStatus;
  }

  /**
   * Find out which type of task this is, Adjustment, Confirmation or Diagnostic
   *
   * @author Reid Hayhow
   */
  public HardwareTaskTypeEnum getHardwareTaskType()
  {
    return _taskType;
  }

  /**
   * Set method for the static timer used by all HardwareTasks to report their
   * overall execution time.
   *
   * NOTE: Explicit classpath is required to comply with architectural testing
   * that assures the hardware layer doesn't import the business layer
   *
   * @author Reid Hayhow
   */
  public static void setTimerObject(com.axi.v810.business.testExec.TestExecutionTimer timer)
  {
    Assert.expect(timer != null);

    _timer = timer;
  }

  /**
   * Set method for sub-classes to set the config enum to be used for saving the
   * last time run timestamp. When it sets the enum it may sets the
   * private member variable, _lastTimeRunInMillis to the time stored in the file
   * if a separate hardware.calib value, RUN_ALL_CALS_ON_SOFTWARE_STARTUP, is set
   *
   * @author Reid Hayhow
   */
  protected void setTimestampEnum(ConfigEnum timestampEnum)
  {
    //Set the timestamp enum for this class. Used to save the timestamp to disk
    //every time the task runs successfully
    _lastTimeRunTimestampEnum = timestampEnum;

    //While we are at it, check to see if the timestamp should be used
    //to override the internal time last run based on hardware.calib enum
    if(_config.getBooleanValue(HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP) == true)
    {
      _lastTimeRunInMillis = _config.getLongValue(_lastTimeRunTimestampEnum);
    }
  }

  /**
   * Method to set the task to run as soon as possible.
   *
   * @author Reid Hayhow
   */
  public void setTaskToRunASAP() throws DatastoreException
  {
    //By setting this to 0, we indicate that the task hasn't run since system time 0
    _lastTimeRunInMillis = 0;

    if(_lastTimeRunTimestampEnum != null)
    {
      _config.setValue(_lastTimeRunTimestampEnum, _lastTimeRunInMillis);
    }
  }

  /**
   * Handle setting up limits and results for sim mode so that things pass
   *
   * @author Reid Hayhow
   */
  private void fudgeResultsForHardwareSimMode()
  {
    //Assure that it is OK to fake the results
    Assert.expect(_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true);
    Assert.expect(UnitTest.unitTesting() == false);

    if (_enableCDNABenchmark)
    {
      setRunning(true);
      //simulate dummy processing time
      try
      {
        Thread.sleep(10);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
      setRunning(false);
    }
    
    //Create limit that should pass
    _limits = Limit.createShouldPassLimit();

    //Create passing result and set status accordingly
    _result = Result.createPassingResult(_name);
    _result.setStatus(HardwareTaskStatusEnum.PASSED);
        
    if (_enableCDNABenchmark)
    {
      _result.setTaskStatusMessage("Run under Simulation Mode");
      _result.setSuggestedUserActionMessage("Every task should take 10 ms to complete");

      // Update observers with the dummy result
      if (_updateObservers)
      {
        updateTaskTime(_result);
        if (_taskTime > 10)
        {
          _result.setStatus(HardwareTaskStatusEnum.FAILED);
          _result.setSuggestedUserActionMessage("Your PC is slow");
        }
        _hardwareTaskObservable.testFinished(new Result(_result, _limits));
      }
    }
  }

  /**
   * Execute the task from a pair of event triggers. This method provides the two event triggers
   * as well as a boolean to indicate if the minor event was a start or end event. It also
   * restores the state of the events in the task to a known state on exit.
   *
   * @author Reid Hayhow
   */
  protected boolean execute(HardwareEventEnum majorEvent,
                            HardwareEventEnum minorEvent,
                            boolean minorEventIsStart) throws XrayTesterException
  {
    Assert.expect(majorEvent != null);
    Assert.expect(minorEvent != null);

    _majorEvent = majorEvent;
    _minorEvent = minorEvent;
    _minorEventIsStart = minorEventIsStart;
    _eventInformationIsValid = true;

    boolean taskPassed = false;

    try
    {
      taskPassed = execute();
    }

    finally
    {
      _majorEvent = null;
      _minorEvent = null;
      _eventInformationIsValid = false;
    }

    return taskPassed;
  }

  /**
   * execute from a thrown execption. This provides an exception to the task so
   * it can determine any additional context.
   *
   * @author Reid Hayhow
   */
  protected boolean execute(XrayTesterException exception) throws XrayTesterException
  {
    Assert.expect(exception != null);

    boolean taskPassed = false;

    _exception = exception;

    try
    {
      taskPassed = execute();
    }
    finally
    {
      _exception = null;
    }

    return taskPassed;
  }

  /**
   * execute for time based tasks. Provides a boolean to indicate if the timeout
   * was triggered from the system idle thread (optional execution) or from the full
   * timeout.
   *
   * @author Reid Hayhow
   */
  protected boolean execute(boolean triggeredFromSystemIdleThread) throws XrayTesterException
  {
    Assert.expect(this instanceof TimeDrivenHardwareTask);

    _triggeredFromSystemIdleThread = triggeredFromSystemIdleThread;
    _timeDrivenEventStatusValid = true;

    boolean taskPassed = false;

    try
    {
      taskPassed = execute();
    }
    finally
    {
     _timeDrivenEventStatusValid = false;
    }

    return taskPassed;
  }

  /**
   * Centralized method for progress reporting by EventDrivenHardwareTasks
   *
   * @author Reid Hayhow
   */
  protected void reportProgress(int percentageDone)
  {
    Assert.expect(_progressReporterEnum != null);
    Assert.expect(_progressObservable != null);

    _progressObservable.reportProgress(_progressReporterEnum, percentageDone);
  }

  /**
   * Centralized method for reporting a task is done
   *
   * @author Reid Hayhow
   */
  protected void reportTaskDone()
  {
    Assert.expect(_progressReporterEnum != null);
    Assert.expect(_progressObservable != null);

    _taskTime = _taskTimer.getElapsedTimeInMillis();
    _progressObservable.reportProgress(_progressReporterEnum, 100);
  }

 /**
  * @author Cheah Lee Herng
  */
  public boolean isByPassDependantTasks()
  {
    return false;
  }
  
  /** 
   * @author Anthony Fong
   */   
  public abstract boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode();
}
