package com.axi.v810.hardware.autoCal;

import com.axi.util.Assert;

public class FailedHardwareTaskResult extends Result
{
  public FailedHardwareTaskResult(Result result, Limit limit)
  {
    super(result, limit);
  }
}
