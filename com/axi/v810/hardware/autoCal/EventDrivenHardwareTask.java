package com.axi.v810.hardware.autoCal;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;

/**
 * This is the base class for all hardware tasks that need to be run after a specific
 * hardware event (like a panel being loaded into the machine) has occurred.
 * @author Bill Darbie
 * @author Reid Hayhow
 */
abstract public class EventDrivenHardwareTask extends HardwareTask
{
  private XrayTesterException _exception = null;
  private int _frequency = 0; // 1 = on each event, 2 = every 2nd time, etc.
  private int _numEvents = -1;
  private long _timeoutInSeconds = 0;
  private long _lastTimeRun = 0;
  private final int MILLISECONDS_PER_SECOND = 1000;
  protected boolean _automatedTask = false; 


  /**
   * Constructor for Event Driven Hardware Tasks.
   *
   * The timeout parameter controls how long a task will wait before it
   * starts responding to HardwareEvents. If set to 0 the Task will respond
   * to all HardwareEvents (obeying the frequency parameter). The timeout
   * parameter is checked BEFORE the frequency parameter is checked.
   *
   * The frequency parameter controls how often this task will run. If this is
   * set to 1 then every time the event happens, this task will run. If this
   * is 2 then the task will run every 2nd time the event occurs, etc.
   *
   * @author Reid Hayhow
   */
  protected EventDrivenHardwareTask(String name,
                                    int frequency,
                                    long timeoutInSeconds,
                                    ProgressReporterEnum progressReporter)
  {
    super(name, progressReporter);
    Assert.expect(frequency > 0);
    _frequency = frequency;
    _timeoutInSeconds = timeoutInSeconds;
  }

  /**
   * This constructor takes the XrayTesterException parameter for responding to an
   * exception. It does *not* take a frequency or timeout parameter because we
   * want to respond to all exceptions.
   *
   * @param name the name of the task
   * @param exception is the XrayTesterException that will trigger this task to
   *   run
   * @author Reid Hayhow
   */
  EventDrivenHardwareTask(String name, XrayTesterException exception,
                          ProgressReporterEnum progressReporter)
  {
    super(name, progressReporter);
    Assert.expect(exception != null);
    _exception = exception;
    _frequency = 1;
    _timeoutInSeconds = 0;
  }

  /**
   *
   * @see HardwareTask#executeTask()
   * @author Bill Darbie
   * @throws XrayTesterException
   */
  protected abstract void executeTask() throws XrayTesterException;

  /**
   * Determine if the task should be run. If it should run, the HardwareTask
   * class will handle the run. This method checks to see if enough time is
   * elapsed (based on the timeout) to even respond to the event. If enough time
   * has elapsed, then it checks the frequency to see if enough events have been
   * seen to trigger an execution. If the task is successfully executed, the
   * HardwareTask execute method resets the timer
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   * @return true if the task needs to be executed, false otherwise
   * @see HardwareTask#execute
   */
  protected boolean isExecuteNeeded()
  {
    boolean run = false;

    //We need to hide event counting until timeout expires
    if(getTimeUntilNextRunInMillis() > 0)
    {
      return run;
    }
    ++_numEvents;
    // keep reseting to 0 so we will never hit the maximum integer no matter
    // how long the program runs
    if (_numEvents == _frequency)
      _numEvents = 0;
    if (_numEvents % _frequency == 0  )
    {
      run = true;
      _automatedTask = true;
    }
    else
    {
      run = false;
    }

    return run;
  }

  /**
   * The isExecuteNeeded Method changes the state of the task by incrementing the
   * number of events it thinks is has seen. This method provides a way to look ahead
   * and see if a task will execute the next time isExecuteNeeded is called WITHOUT
   * changing the state of the task.
   *
   * @return boolean
   * @author Reid Hayhow
   */
  public boolean peekIsExecuteNeeded()
  {
    boolean run = false;

    // If the timeout has not elapsed we don't run or increment events
    if(getTimeUntilNextRunInMillis() > 0)
    {
      return run;
    }
    // Don't increment events *but* see if incrementing by one would execute the task
    if ((_numEvents + 1) % _frequency == 0  )
    {
      run = true;
    }
    else
    {
      run = false;
    }

    return run;
  }

  /**
   * Will return 0 if the task should be run NOW
   *
   * @author Reid Hayhow
   * @return the time (long) until the next run should be performed.
   */
  long getTimeUntilNextRunInMillis()
  {
    long elapsedTimeInMillis = getElapsedTimeSinceLastRunInMillis();
    long timeUntilNextRunInMillis = (_timeoutInSeconds * MILLISECONDS_PER_SECOND) - elapsedTimeInMillis;
    if (timeUntilNextRunInMillis < 0)
      timeUntilNextRunInMillis = 0;

    return timeUntilNextRunInMillis;
  }

  /**
   * @see HardwareTask#setUp()
   * @throws XrayTesterException
   * @author Reid Hayhow
   */
  protected abstract void setUp() throws XrayTesterException;

  /**
   * Default implementation of how Event Driven Hardware tasks notify of
   * failures. They throw exceptions which are caught by the GUI. This works
   * because they are in a direct throw path back to the UI.
   *
   * @author Reid Hayhow
   */
  protected void notifyOfFailure() throws HardwareTaskExecutionException
  {
    HardwareTaskStatusEnum taskStatus = _result.getStatus();
    if(taskStatus.equals(HardwareTaskStatusEnum.CANCELED) == false &&
       taskStatus.equals(HardwareTaskStatusEnum.NOT_RUN) == false )
    {
      HardwareTaskExecutionException ex = new HardwareTaskExecutionException(this);
      throw ex;
    }
  }
}
