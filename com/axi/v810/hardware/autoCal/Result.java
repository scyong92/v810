package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class contains test results. This includes the raw data collected from a
 * test being run as well as summary information such as pass/fail and if the
 * test has actually been run.
 *
 * @author Reid Hayhow
 */
public class Result
{
  private static final String _NONE = StringLocalizer.keyToString("CD_NONE_KEY");

  // contains the result that is used to determine pass/fail
  // this is typically a single number like a Double or Integer
  private Object _result;
  private HardwareTaskStatusEnum _status = HardwareTaskStatusEnum.NOT_RUN;
  private Date _startDate;
  private Date _stopDate;
  private Limit _limit;
  private String _taskStatusMessage = _NONE;
  private String _suggestedUserActionMessage = _NONE;
  /** @todo wpd - Reid - create a ResultGroup and remove this _resultList */
  private List<Result> _resultList = new ArrayList<Result>();
  private String _nameOfResult;
  private long _taskTime;  
  private long _loopCount; //1 base
  private com.axi.v810.business.autoCal.BenchmarkJsonFileEnum _benchmarkFileEnum;  
  /**
   * Result constructor for an actual result as opposed to a result container.
   * The name of the task that created the Result is input as well as the
   * comparable object.
   *
   * @author Reid Hayhow
   */
  public Result(Object result, String taskName)
  {
    _result = result;
    _status = HardwareTaskStatusEnum.NOT_RUN;
    _nameOfResult = taskName;
    _startDate = new Date(System.currentTimeMillis());
    _stopDate = new Date(System.currentTimeMillis());
    _taskTime = 0;    
    _loopCount = 1;
    _benchmarkFileEnum = null;
  }

  /**
   * Result constructor for 'empty' Result objects that will just contain a list
   * of results. It also needs the name of the task that created it.
   * @author Reid Hayhow
   */
  public Result(String taskName)
  {
    _nameOfResult = taskName;
    _status = HardwareTaskStatusEnum.NOT_RUN;
    _startDate = new Date(System.currentTimeMillis());
    _stopDate = new Date(System.currentTimeMillis());
    _taskTime = 0;
    _loopCount = 1;
    _benchmarkFileEnum = null;
  }

  /**
   * Results copy constructor. This is used to create the Result object that is
   * passed to the GUI through an observer/observable pattern.
   *
   * @author Reid Hayhow
   */
  public Result(Result resultToBeCopied, Limit limit)
  {
    _result = resultToBeCopied.getResult();
    _status = resultToBeCopied.getStatus();
    _startDate = resultToBeCopied.getStartDate();
    _stopDate = resultToBeCopied.getStopDate();
    _taskStatusMessage = resultToBeCopied.getTaskStatusMessage();
    _suggestedUserActionMessage = resultToBeCopied.getSuggestedUserActionMessage();
    _nameOfResult = resultToBeCopied._nameOfResult;
    _limit = limit;
    _taskTime = resultToBeCopied.getTaskTime();
    _loopCount = resultToBeCopied.getLoopCount();
    _benchmarkFileEnum = resultToBeCopied.getBenchmarkFileEnum();
  }

  /**
   * Set the results. The result object type must match the type stored for the
   * high and low limits so the compareTo() method can work properly.
   *
   * @author Reid Hayhow
   */
  public void setResults(Object result)
  {
    Assert.expect(result != null);

    // assume that since setResults() is being called that the test was run
    _result = result;
  }

  /**
   * @author Reid Hayhow
   * @return the comparable or boolean object with the result
   */
  Object getResult()
  {
    //NOTE - _result can be null in container result objects
    return _result;
  }

  /**
   * Set the status enum based on a task
   *
   * @author Reid Hayhow
   */
  public void setStatus(HardwareTaskStatusEnum status)
  {
    Assert.expect(status != null);

    _status = status;
  }

  /**
   * Access to the status stored in this result
   *
   * @author Reid Hayhow
   */
  public HardwareTaskStatusEnum getStatus()
  {
    Assert.expect(_status != null);

    return _status;
  }

  /**
   * Return the status string (instead of the enum value itself)
   *
   * @author Reid Hayhow
   */
  public String getStatusString()
  {
    Assert.expect(_status != null);

    return _status.toString();
  }

  /**
   * Called to record the time when the test was begun
   *
   * @author Bill Darbie
   */
  public void startTimeStamp()
  {
    _startDate = new Date();
  }

  /**
   * Called to record the time when the test ended.
   *
   * @author Bill Darbie
   */
  public void stopTimeStamp()
  {
    _stopDate = new Date();
  }

  /**
   * @author Anthony Fong
   */
  public void setBenchmarkTimingData(long taskTime, long loopCount,
    com.axi.v810.business.autoCal.BenchmarkJsonFileEnum benchmarkFileEnum)
  {
    _taskTime = taskTime;
    _loopCount = loopCount;
    _benchmarkFileEnum = benchmarkFileEnum;
  }  
  
  /**
   * @author Anthony Fong
   */
  private void setTaskTime(long taskTime)
  {
    _taskTime = taskTime;
  }  
  
  /**
   * @author Anthony Fong
   */
  public long getTaskTime()
  {
    return _taskTime;
  }  
  
   /**
   * @author Anthony Fong
   */
  public void setLoopCount(long loopCount)
  {
    _loopCount = loopCount;
  }  
  
  /**
   * @author Anthony Fong
   */
  public long getLoopCount()
  {
    return _loopCount;
  }  
    /**
   * @author Anthony Fong
   */
  public void setBenchmarkFileEnum(com.axi.v810.business.autoCal.BenchmarkJsonFileEnum benchmarkFileEnum)
  {
    _benchmarkFileEnum = benchmarkFileEnum;
  }  
  
  /**
   * @author Anthony Fong
   */
  public com.axi.v810.business.autoCal.BenchmarkJsonFileEnum getBenchmarkFileEnum()
  {
    return _benchmarkFileEnum;
  }   
  
  /**
   * Method to set a message that describes the status of the task. Examples
   * include a specific list of failed cameras, or some detailes of a failed
   * image quality test. This message should be independent of the action a
   * specific user may take to correct the problem (if any).
   *
   * @author Reid Hayhow
   */
  public void setTaskStatusMessage(String taskStatusMessage)
  {
    Assert.expect(taskStatusMessage != null);

    _taskStatusMessage = taskStatusMessage;
  }

  /**
   *
   * @author Bill Darbie
   * @return Date, the start time of the task
   */
  public Date getStartDate()
  {
    Assert.expect(_startDate != null);

    return _startDate;
  }

  /**
   *
   * @author Bill Darbie
   * @return Date, the stop time of the task
   */
  public Date getStopDate()
  {
    Assert.expect(_stopDate != null);

    return _stopDate;
  }

  /**
   * toString method that displays result data
   *
   * @return a string containing result information
   */
  public String toString()
  {
    StringBuffer message = new StringBuffer();

    //Handle possibly null members
    if(_result != null)
    {
      message.append(" Result = " + _result.toString());
    }
    if(_startDate != null)
    {
      message.append(" Start Date = " + _startDate.toString());
    }
    if(_stopDate != null)
    {
     message.append(" Stop Date = " + _stopDate.toString());
    }
    if(_status != null)
    {
      message.append(" Status = " + _status.toString());
    }
    return message.toString();
  }

  public String getResultString()
  {
    Assert.expect(_result != null);

    return _result.toString();
  }

  /**
   * Access to the task status message (if any) generated during the task execution
   * This is distinct from any action that may be recommended (see the
   * getSuggestedUserActionMessage() method for actions a user may take.
   *
   * @author Reid Hayhow
   */
  public String getTaskStatusMessage()
  {
    return _taskStatusMessage;
  }

  /**
   * Add a sub result to a result container class.
   *
   * @author Reid Hayhow
   */
  public void addSubResult(Result result)
  {
    _resultList.add(result);
  }

  /**
   * Get the list of Results for this Result object.
   *
   * @author Reid Hayhow
   */
  public List<Result> getResultList()
  {
    return _resultList;
  }

  /**
   * Method to provide access to the name of the result. The name of the result
   * is identical to the name of the task that created it.
   *
   * @author Reid Hayhow
   */
  public String getResultName()
  {
    Assert.expect(_nameOfResult != null);

    return _nameOfResult;
  }

  /**
   * Setter for the suggested action a user should take (if a failure happened)
   * User action message is discinct from the failure information
   *
   * @author Reid Hayhow
   */
  public void setSuggestedUserActionMessage(String suggestedUserActionMessage)
  {
    _suggestedUserActionMessage = suggestedUserActionMessage;
  }
  /**
   * Getter for the suggested action a user should take (if a failure happened)
   * User action message is discinct from the failure information
   *
   * @author Reid Hayhow
   */
  public String getSuggestedUserActionMessage()
  {
    return _suggestedUserActionMessage;
  }

  /**
   * Factory method for simple passing result.
   *
   * @author Reid Hayhow
   */
  public static Result createPassingResult(String taskName)
  {
    return new Result(new Boolean(true), taskName);
  }

  /**
   * Factory method for a simple failing result.
   *
   * @author Reid Hayhow
   */
  public static Result createFailingResult(String taskName)
  {
    return new Result(new Boolean(false), taskName);
  }
}
