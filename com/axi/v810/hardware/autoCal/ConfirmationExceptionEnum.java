package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;

public class ConfirmationExceptionEnum extends HardwareTaskExceptionEnum
{
  private static HashMap<ConfirmationExceptionEnum, LocalizedString> _enumToStringMap;
  private static int _index = -1;

  public static ConfirmationExceptionEnum PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum INCORRECT_CAMERA_LIST_IN_HARDWARE_CALIB =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum CONFIRM_SYSTEM_FIDUCIAL_B_CANNOT_HAVE_PANEL_IN_SYSTEM =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum PROBLEM_EXECUTING_PANEL_HANDLING_TEST =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum PROBLEM_ABORTING_PANEL_HANDLING_TEST =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum PROBLEM_SAVING_CAL_FILE_DATA =
    new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH =
    new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum INCORRECT_OPTICAL_CAMERA_LIST =
      new ConfirmationExceptionEnum(++_index);
  public static ConfirmationExceptionEnum PROBLEM_EXECUTING_BENCHMARK_TEST =
      new ConfirmationExceptionEnum(++_index);
 public static ConfirmationExceptionEnum PROBLEM_ABORTING_BENCHMARK_TEST =
      new ConfirmationExceptionEnum(++_index);   

  //Initialize the map of enum's to localized strings
  static
  {
    _enumToStringMap =
        new HashMap<ConfirmationExceptionEnum, LocalizedString>(_index + 1);

    Assert.expect(_enumToStringMap.put(PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM, new LocalizedString(
        "CD_EXCEPTION_PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(INCORRECT_CAMERA_LIST_IN_HARDWARE_CALIB, new LocalizedString(
      "CD_INCORRECT_CAMERA_LIST_IN_HARDWARE_CALIB_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(CONFIRM_SYSTEM_FIDUCIAL_B_CANNOT_HAVE_PANEL_IN_SYSTEM, new LocalizedString(
      "CD_EXCEPTION_CONFIRM_SYSTEM_FIDUCIAL_B_CANNOT_HAVE_PANEL_IN_SYSTEM_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(PROBLEM_EXECUTING_PANEL_HANDLING_TEST, new LocalizedString(
      "CD_EXCEPTION_PROBLEM_EXECUTING_PANEL_HANDLING_TEST_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(PROBLEM_ABORTING_PANEL_HANDLING_TEST, new LocalizedString(
      "CD_EXCEPTION_PROBLEM_ABORTING_PANEL_HANDLING_TEST_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(PROBLEM_SAVING_CAL_FILE_DATA, new LocalizedString(
      "CD_EXCEPTION_PROBLEM_SAVING_CAL_FILE_DATA_KEY", null)) == null);

    Assert.expect(_enumToStringMap.put(CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH, new LocalizedString(
      "CD_EXCEPTION_CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH_KEY", null)) == null);
    
    Assert.expect(_enumToStringMap.put(INCORRECT_OPTICAL_CAMERA_LIST, new LocalizedString(
      "CD_INCORRECT_OPTICAL_CAMERA_LIST_KEY", null)) == null);
    
    Assert.expect(_enumToStringMap.put(PROBLEM_EXECUTING_BENCHMARK_TEST, new LocalizedString(
      "CD_EXCEPTION_PROBLEM_EXECUTING_BENCHMARK_TEST_KEY", null)) == null);
    
    Assert.expect(_enumToStringMap.put(PROBLEM_ABORTING_BENCHMARK_TEST, new LocalizedString(
      "CD_EXCEPTION_PROBLEM_ABORTING_BENCHMARK_TEST_KEY", null)) == null);

    Assert.expect(_enumToStringMap.size() == _index + 1);
  }

  /**
   * @author Reid Hayhow
   */
  private ConfirmationExceptionEnum(int id)
  {
    super(id);
  }

  /**
   * getLocalizedString
   *
   * @author Reid Hayhow
   */
  public LocalizedString getLocalizedString()
  {
    LocalizedString myString = _enumToStringMap.get(this);

    Assert.expect(myString != null);
    return myString;
  }
}
