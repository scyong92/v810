package com.axi.v810.hardware.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Reid Hayhow
 * @version 1.0
 */
public class Test_HardwareTask extends UnitTest
{
  public Test_HardwareTask()
  {
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_HardwareTask());
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ExecuteParallelThreadTasks<Object> taskExecutor = new ExecuteParallelThreadTasks<Object>();

    TestHardwareTask task = new TestHardwareTask("Task One");

    //Set to stop on first failure
    task.setStopOnFail(true);

    //disable and confirm disabled
    task.disable();
    Expect.expect(task.isEnabled() == false, "Failed disable test");

    //enable and confirm enabled
    task.enable();
    Expect.expect(task.isEnabled() == true, "Failed enable test");

    //cancelTask checks to see if task is running before setting boolean
    //In order to avoid threadlock on the blocking abort, use a separate thread
    TestHardwareTaskExecutionThread abortThread = new TestHardwareTaskExecutionThread();

    //Tell it which task to abort
    abortThread.setHardwareTaskToCancel(task);

    //submit the task
    taskExecutor.submitThreadTask(abortThread);

    //Now wait for it to finsh
    try
    {
      taskExecutor.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      ex.printStackTrace(os);
    }

    //Shouldn't have set the _taskCanceled boolean
    Expect.expect(task._taskCancelled == false, "Failed abort test for non-running test");

    //Cancel should work after setRunning(true)
    task.setRunning(true);

    //Create a new abort thread
    abortThread = new TestHardwareTaskExecutionThread();
    abortThread.setHardwareTaskToCancel(task);

    //Submit this thread. It will block on the abort
    taskExecutor.submitThreadTask(abortThread);

    //Also create a thread to execute the task. Otherwise we don't get a notifyAll
    //to wake up the abortThread
    TestHardwareTaskExecutionThread executeThread = new TestHardwareTaskExecutionThread();

    //Tell it which task we are interested in
    executeThread.setHardwareTaskToCancel(task);

    //Set the thread to execute the task, not abort it
    executeThread.setToExecuteTask();

    //Guarantee that the abort 'took' by checking the private variable
    while(task._taskCancelled == false)
    {
      try
      {
        Thread.sleep(50);
      }
      catch (InterruptedException ex1)
      {
        ex1.printStackTrace(os);
      }
    }

    //Submit the execute task thread
    taskExecutor.submitThreadTask(executeThread);

    //Now wait for them to run
    try
    {
      taskExecutor.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      ex.printStackTrace(os);
    }

    //Execute task should not increment *but* will reset the canceled bool
    Expect.expect(task.getTimesExecuted() == 0, "Execute count test for task failed: " + task.getTimesExecuted());


    //The task should have a name
    Expect.expect(task.getName() != null, "Saw a null name");

    //The task should tell us it can run on this hardware
    Expect.expect(task.canRunOnThisHardware() == true, "Failed canRunOnThisHardware test");

    //Before the first run, the elapsed time should be time since epoch
    Expect.expect(task.getElapsedTimeSinceLastRunInMillis() == System.currentTimeMillis(), "Elapsed time init failed: " +
                  task.getElapsedTimeSinceLastRunInMillis());


    //Execute tests, run it once and make sure it fails since hw isn't started
    boolean taskPassed = executeHardwareTask(task);

    // The task will fail and will have a status NOT_RUN until startup is done
    Expect.expect(taskPassed == false, "Don't run without starting up hardware!");
    Expect.expect(task.getResult().getStatus() == HardwareTaskStatusEnum.NOT_RUN);

    // Startup the tester in sim mode
    try
    {
      XrayTester.getInstance().setSimulationMode();
      XrayTester.getInstance().startup();

    }
    catch (XrayTesterException startupException)
    {
      Expect.expect(false, startupException.getLocalizedMessage());
    }

    //Execute tests, run it once and make sure it ran
    taskPassed = executeHardwareTask(task);

    //After execution the elapsed time should be close to or equal to 0
    Expect.expect(task.getElapsedTimeSinceLastRunInMillis() < 30, "Elapsed time after execute failed: " +
                  task.getElapsedTimeSinceLastRunInMillis());

    Expect.expect(task.getTimesExecuted() == 1, "Execute count test for task failed: " + task.getTimesExecuted());

    try
    {
      //Now set the task to run as soon as possible
      task.setTaskToRunASAP();
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace(os);
    }
    //The elapsed time should be rather large
    Expect.expect( task.getElapsedTimeSinceLastRunInMillis() > 10000000);

    //Run it again and make sure it ran
    executeHardwareTask(task);

    Expect.expect(task.getTimesExecuted() == 2, "Execute count test for task failed: " + task.getTimesExecuted());

    //set to not run on this hardware, run it again and make sure it doesn't run
    task._canRunOnThisHardware = false;

    executeHardwareTask(task);

    Expect.expect(task.getTimesExecuted() == 2, "Execute count test for task failed: " + task.getTimesExecuted());

    task._canRunOnThisHardware = true;

    //Cause the task to throw exceptions and make sure they are handled correctly
    // NOTE this causes the task execution count to increase by one
    testExceptions(task, task);

    //Testing with sub-tasks
    TestHardwareTask task2 = new TestHardwareTask("Task Two");
    TestHardwareTask task3 = new TestHardwareTask("Task Three");

    Expect.expect(task2.getTimesExecuted() == 0, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 0, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    //Now test sub-task execution. Add three sub-tasks
    task.addSubTest(task2);

    //Task3 is added twice
    task.addSubTest(task3);
    task.addSubTest(task3);

    executeHardwareTask(task);

    Expect.expect(task2.getTimesExecuted() == 1, "Execute count test for task2 failed: " + task2.getTimesExecuted());

    //Task3 is run twice
    Expect.expect(task3.getTimesExecuted() == 2, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    //The task that holds sub-tasks does NOT get executed!
    Expect.expect(task.getTimesExecuted() == 3, "Execute count test for task failed: " + task.getTimesExecuted());

    //Test a task that fails due to results that are too high
    task2.setResultsObjectToFailHigh();

    executeHardwareTask(task);

    //Task two will execute a second time, fail and task three won't execute at all
    Expect.expect(task2.getTimesExecuted() == 2, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 2, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    task2.setResultsObjectToFailLow();

    executeHardwareTask(task);

    //Task two will execute a second time, fail and task three won't execute at all
    Expect.expect(task2.getTimesExecuted() == 3, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 2, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    Expect.expect(task2._result.getStatus().equals(HardwareTaskStatusEnum.FAILED), "Task 2 result should be failed");
    Expect.expect(task3._result.getStatus().equals(HardwareTaskStatusEnum.PASSED), "Task 3 result should be passed");


    //Now set the tasks not to stop on failure
    task.setStopOnFail(false);

    task3.setResultsObjectToFailHigh();

    executeHardwareTask(task);

    //Task two will execute a second time, fail and task three will execute twice
    Expect.expect(task2.getTimesExecuted() == 4, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 4, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    Expect.expect(task2._result.getStatus().equals(HardwareTaskStatusEnum.FAILED), "Task 2 result should be failed");
    Expect.expect(task3._result.getStatus().equals(HardwareTaskStatusEnum.FAILED), "Task 3 result should be failed");

    //Reset tasks to pass
    task2.setResultsObjectToPass();
    task3.setResultsObjectToPass();

    //Disable this task and all sub tasks
    task.disable();

    Expect.expect(task.getEnabledTestCount() == 0, "EnabledTestCount failed " + task.getEnabledTestCount());

    executeHardwareTask(task);

    //Execution count should remain the same
    Expect.expect(task.getTimesExecuted() == 3, "Execute count test for task failed: " + task.getTimesExecuted());
    Expect.expect(task2.getTimesExecuted() == 4, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 4, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    Expect.expect(task._result.getStatus().equals(HardwareTaskStatusEnum.NOT_RUN));
    Expect.expect(task2._result.getStatus().equals(HardwareTaskStatusEnum.NOT_RUN));
    Expect.expect(task3._result.getStatus().equals(HardwareTaskStatusEnum.NOT_RUN));

    //Disable this task and all sub tasks
    task.enable();

    //Expect to see 3 tasks enabled, one for task2 and two for the two copies of task3
    Expect.expect(task.getEnabledTestCount() == 3, "EnabledTestCount failed " + task.getEnabledTestCount());

    executeHardwareTask(task);

    //Execution count should remain the same for task, increase by 1 for task2 and increase by 2 for task2
    Expect.expect(task.getTimesExecuted() == 3, "Execute count test for task failed: " + task.getTimesExecuted());
    Expect.expect(task2.getTimesExecuted() == 5, "Execute count test for task2 failed: " + task2.getTimesExecuted());
    Expect.expect(task3.getTimesExecuted() == 6, "Execute count test for task3 failed: " + task3.getTimesExecuted());

    Expect.expect(task._result.getStatus().equals(HardwareTaskStatusEnum.PASSED));
    Expect.expect(task2._result.getStatus().equals(HardwareTaskStatusEnum.PASSED));
    Expect.expect(task3._result.getStatus().equals(HardwareTaskStatusEnum.PASSED));

    //Test exception handling in sub-tasks

    testExceptions(task, task3);

    //Add testing for recursive Results and Limits
    TestHardwareTask taskWithMultiResults = new TestHardwareTask("Task With Multi Results");

    //Set the list of limits and results so the task will pass
    taskWithMultiResults.createPassingLimitsList();
    taskWithMultiResults.createPassingResultsList();

    Expect.expect(taskWithMultiResults.getTimesExecuted() == 0,
                  "Execute count test for task with multi results failed: "
                  + taskWithMultiResults.getTimesExecuted());

    executeHardwareTask(taskWithMultiResults);

    Expect.expect(taskWithMultiResults.taskPassed() == true, "Task should have a passed status");

    Expect.expect(taskWithMultiResults.getTimesExecuted() == 1,
                  "Execute count test for task with multi results failed: "
                  + taskWithMultiResults.getTimesExecuted());

    //Now cause it to fail, it will execute once but will be considered failing
    taskWithMultiResults.createFailingResultsList();

    executeHardwareTask(taskWithMultiResults);

    Expect.expect(taskWithMultiResults.getTimesExecuted() == 2,
                  "Execute count test for task with multi results failed: "
                  + taskWithMultiResults.getTimesExecuted());

    Expect.expect(taskWithMultiResults.taskPassed() == false, "Task should have a failed status");
  }

  private boolean executeHardwareTask(TestHardwareTask task)
  {
    // Save the state of the task, pass/fail
    boolean taskPassed = false;

    try
    {
      taskPassed = task.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                                HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                                true);
    }
    catch (XrayTesterException ex)
    {
      //Now that failing tasks will throw an exception, this regression test
      //'expects' to catch an exception if the task failed
      if(task.getResult().getStatus().equals(HardwareTaskStatusEnum.FAILED) == false)
      {
        ex.printStackTrace();
      }
    }

    // Return if the task passed
    return taskPassed;
  }

  private void testExceptions(TestHardwareTask taskToExecute, TestHardwareTask taskToThrowException)
  {
    try
    {
      taskToThrowException.setThrowExceptionOnExecute(TestHardwareTask.DATASTORE_EXCEPTION);

      taskToExecute.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            true);
      //shouldn't get to this
      Expect.expect(false);
    }
    catch(DatastoreException ex)
    {
      //do nothing
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
      //shouldn't see one of these!
      Expect.expect(false);
    }

    Expect.expect(taskToExecute._result.getStatus().equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED));

    //Now throw a HardwareExceptions
    try
    {
      taskToThrowException.setThrowExceptionOnExecute(TestHardwareTask.HARDWARE_EXCEPTION);

      taskToExecute.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            true);
      //shouldn't get to this
      Expect.expect(false);
    }
    catch(HardwareException ex)
    {
      //do nothing
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
      //shouldn't see one of these!
      Expect.expect(false);
    }

    Expect.expect(taskToExecute._result.getStatus().equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED));

    //Now throw a EdgeMTFBusinessExceptions
    try
    {
      taskToThrowException.setThrowExceptionOnExecute(TestHardwareTask.BUSINESS_EXCEPTION);
      taskToExecute.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            true);
      //shouldn't get to this
      Expect.expect(false);
    }
    catch(com.axi.v810.business.BusinessException ex)
    {
      //do nothing
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
      //shouldn't see one of these!
      Expect.expect(false);
    }

    Expect.expect(taskToExecute._result.getStatus().equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED));

    //finally generic Xray Tester exception. This will cause an assert, not the
    //normal Xray Tester Exception
    try
    {
      taskToExecute.setThrowExceptionOnExecute(TestHardwareTask.XRAY_TESTER_EXCEPTION);

      taskToExecute.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            true);
      //shouldn't get to this
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      //do nothing
    }

    //The exception should pass through without complaint or assert
    Expect.expect(taskToExecute._result.getStatus().equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED));

    // Check exceptions from cleanup
    try
    {
      taskToExecute.unsetThrowExceptionOnExecute();
      taskToThrowException.unsetThrowExceptionOnExecute();
      taskToThrowException.setThrowExceptionOnCleanUp(TestHardwareTask.DATASTORE_EXCEPTION);

      taskToExecute.execute(HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            HardwareTaskEventEnum.HARDWARE_TASK_EXECUTION_EVENT,
                            true);
      //shouldn't get to this
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      //do nothing
    }
    Expect.expect(HardwareTaskEngine.getInstance().eventsAreEnabled() == true);
    taskToExecute.unsetThrowExceptionOnCleanup();
//    Expect.expect(_hardwareTaskEngine.eventsEnabled() == true);
  }
}
