package com.axi.v810.hardware.autoCal;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;

/**
 * This is the base class for all hardware tasks that need to be run
 * at a certain frequency.
 * @author Bill Darbie
 * @author Reid Hayhow
 */
abstract public class TimeDrivenHardwareTask extends HardwareTask
{
  private long _frequencyInMillis;
  private boolean _longTask;
  private boolean _timerThreadStarted = false;

  //TimeDrivenHardwareTasks are not guaranteed to be on the HWWorker Thread
  //So get a handle to the instance so we can us it to execute tasks
  private static HardwareWorkerThread _hardwareWorkerThread;

  //Only get the instance once
  static
  {
     _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  }

  /**
   * Constructor for Time Driven Hardware Tasks. Takes as input the task name
   * the frequency at which the task should run and a boolean indicating
   * if the task is a long task.
   *
   * @author Reid Hayhow
   */
  protected TimeDrivenHardwareTask(String name,
                         int frequencyInMillis,
                         boolean longTask)
  {
    super(name, ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);
    Assert.expect(frequencyInMillis > 0);
    _frequencyInMillis = frequencyInMillis;
    _longTask = longTask;
  }

  /**
   *
   * @return how frequently in milliseconds this task should be run
   * @author Reid Hayhow
   */
  long getFrequencyInMillis()
  {
    return _frequencyInMillis;
  }

  /**
   *
   * @author Reid Hayhow
   * @return how long in milliseconds it will be till the task is run next
   */
  long getTimeUntilNextRunInMillis()
  {
    long elapsedTimeInMillis = getElapsedTimeSinceLastRunInMillis();
    long timeUntilNextRunInMillis = _frequencyInMillis - elapsedTimeInMillis;
    if (timeUntilNextRunInMillis < 0)
      timeUntilNextRunInMillis = 0;

    return timeUntilNextRunInMillis;
  }

  /**
   *
   * @author Reid Hayhow
   * @return true if the task is a long task
   */
  boolean isLongTask()
  {
    return _longTask;
  }


  /**
   * Abstract method containing the actual work to be done by this task
   *
   * @author Reid Hayhow
   */
  protected abstract void executeTaskOnWorkerThread() throws XrayTesterException;

  /**
   * In TimeDrivenHardwareTasks, the executeTask method is used to thread
   * the actual execution off on the HardwareWorkerThread, so this method is final.
   * The actual implementation of the task execution should be done on the new
   * abstract method, executeTaskOnWorkerThread().
   *
   * @author Reid Hayhow
   */
  protected final void executeTask() throws XrayTesterException
  {
    //invoke this task on the Hardware worker thread
    _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        //Disable TaskEngine events so we don't trigger a different task
        _hardwareTaskEngine.disableEvents();

        //We have to re-enable events, even if the execution throws, so try...
        try
        {
          //Execute the actual work for this task
          executeTaskOnWorkerThread();
        }
        finally
        {
          //Always re-enable events for the HardwareTaskEngine
          _hardwareTaskEngine.enableEvents();
        }
      }
    });
  }

  /**
   * Determine if the task should be run. If it should be run the HardwareTask
   * execute method will take care of the actual execution
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   * @return true if the task should be executed (based on timeout)
   */
  protected boolean isExecuteNeeded()
  {
    boolean run = false;
    // do not run the TimeDriven task if it has already run
    // in less than half the time it is required to run
    long timeElapsedSinceLastRun = getElapsedTimeSinceLastRunInMillis();
    if (timeElapsedSinceLastRun > (_frequencyInMillis / 2))
    {
      run = true;
    }
    return run;
  }

  /**
   * Get method to allow external check to see if a task timer thread
   * has been started
   *
   * @return true if the timer thread was started, false otherwise
   * @author Reid Hayhow
   */
  public boolean isTimerThreadStarted()
  {
    return _timerThreadStarted;
  }

  /**
   * Set method to tell the task that its timer thread has been started
   * @author Reid Hayhow
   */
  public void setTimerStarted(boolean started)
  {
    _timerThreadStarted = started;
  }

  /**
   * @see HardwareTask#setUp()
   * @author Reid Hayhow
   */
  protected abstract void setUp() throws XrayTesterException;

  /**
   * Default implementation of how Time Driven Hardware tasks notify of
   * failures. They don't do anything special because they are off on their own
   * thread. Any observers have already been notified of the failure at the end
   * of recursiveExecute.
   *
   * @author Reid Hayhow
   */
  protected void notifyOfFailure() throws HardwareTaskExecutionException
  {
    //Do nothing because we have already notified observers at the end of recursiveExecute()
  }
}
