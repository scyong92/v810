package com.axi.v810.hardware.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for parsing in limits for any autoCal/HardwareTask.
 * This includes Adjustments, Confirmations or Diagnostics
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
public class ParseLimits
{
  private static final String _LIMITSFILE = "com.axi.v810.properties.CustomerLimits";
  //The Prod Limits is not currently available
  //  private static final String _PROD_LIMITSFILE = "com.axi.mtdMfg.agt5dx.properties.ProductionLimits";
  private static ResourceBundle _customerBundle;
  private static ResourceBundle _productionBundle;
  private static boolean _productionMode;

  private String _lowLimit;
  private String _highLimit;
  private String _booleanLimit;

  private String _lowLimitKey = "";
  private String _highLimitKey = "";
  private String _booleanLimitKey = "";

  private String _className;

  private String _propertiesFullPath = FileName.getCustomerLimitsPropertiesFullPath();

  static
  {
    // get a bundle reference to the customer limits file
    _customerBundle = ResourceHandler.getPropertyBundle(_LIMITSFILE);

    // find out if we are running in production mode or not
    Config config = Config.getInstance();
  }


  /**
   * Parse in the limits for the Hardware Task passed in. If we are in
   * production mode, the production limits are used if found. If the limits are
   * not in the production limits file or we are not in production mode, the
   * limits are searched for in the customer limits file. If the limits are not
   * found, an empty string is returned for the limits.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  void parse(HardwareTask test)
  {
    _lowLimit = "";
    _highLimit = "";
    _booleanLimit = "";

    _className = ClassNameUtil.getName(test);
    _lowLimitKey = _className + ".lowLimit";
    _highLimitKey = _className + ".highLimit";
    _booleanLimitKey = _className + ".boolean";

    boolean limitsFound = false;
    if (_productionMode)
    {
      limitsFound = getLimits(_productionBundle);
      if (limitsFound == false)
      {
        getLimits(_customerBundle);
      }
    }
    else
    {
      getLimits(_customerBundle);
    }
  }

  /**
   * Read the limits for the current test from the limits file.
   *
   * @return true if the limts are found in the production file, false
   *   otherwise.
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  private boolean getLimits(ResourceBundle bundle)
  {
    boolean found = true;
    try
    {
      _lowLimit = bundle.getString(_lowLimitKey);
    }
    catch (MissingResourceException mre)
    {
      found = false;
    }
    try
    {
      _highLimit = bundle.getString(_highLimitKey);
    }
    catch (MissingResourceException mre)
    {
      found = false;
    }
    try
    {
      _booleanLimit = bundle.getString(_booleanLimitKey);
    }
    catch (MissingResourceException mre)
    {
      found = false;
    }

    return found;
  }

  /**
   *
   * @return a string representation of the low limit for the test.
   * @author Bill Darbie
   */
  private String getLowLimit()
  {
    Assert.expect(_lowLimit != null);
    //Can't have both a number limit and a boolean limit for the same key!
    Assert.expect(_booleanLimit.length() == 0);

    return _lowLimit;
  }

  /**
   *
   * @return a string representation of the high limit for the test.
   * @author Bill Darbie
   */
  private String getHighLimit()
  {
    Assert.expect(_highLimit != null);
    //Can't have both a number limit and a boolean limit for the same key!
    Assert.expect(_booleanLimit.length() == 0);

    return _highLimit;
  }

  /**
   *
   * @return a string representation of the boolean limit for the test.
   * @author Reid Hayhow
   */
  private String getBooleanLimit()
  {
    Assert.expect(_booleanLimit != null);
    //Can't have both a number limit and a boolean limit for the same key!
    Assert.expect(_lowLimit.length() == 0);
    Assert.expect(_highLimit.length() == 0);

    return _booleanLimit;
  }

  /**
   *
   * @return an expanded string representation of the key with the suffix appended.
   * @author Roy Williams
   * @author Bill Darbie
   */
  private String getExpandedKey(String baseKey, String keySuffix)
  {
    Assert.expect(baseKey != null);
    Assert.expect(keySuffix != null);

    //Kind of a hack, if I see a non-empty key here, then the string passed down
    //to getLimits() does not start with a '.' The method getLimit() will add a
    //'.', so if the key is empty, just send down 'highLimit'
    String expandedKey = keySuffix;
    if (baseKey.length() != 0)
    {
      expandedKey = baseKey + "." + keySuffix;
    }
    return expandedKey;
  }

  /**
   *
   * @return a string representation of the low limit for the test.
   * @author Roy Williams
   */
  private String getLowLimitKey(String baseKey)
  {
    Assert.expect(baseKey != null);

    return getExpandedKey(baseKey, "lowLimit");
  }

  /**
   *
   * @return a string representation of the high limit for the test.
   * @author Roy Williams
   */
  private String getHighLimitKey(String key)
  {
    Assert.expect(key != null);

    return getExpandedKey(key, "highLimit");
  }

  /**
   *
   * @return a string representation of the high limit for the test.
   * @author Bill Darbie
   */
  private String getBoolLimit(String key)
  {
    Assert.expect(key != null);

    return getLimits(_customerBundle, key + ".boolean");
  }


  /**
   * Generalized function to get a limit for a specific class from the
   * limit file. It returns an empty string if the limit is not found.
   *
   * @author Reid Hayhow
   */
  private String getLimits(ResourceBundle bundle, String limit)
  {
    String limitValue = "";
    try
    {
      limitValue = bundle.getString(_className + "." + limit);
    }
    catch (MissingResourceException mre)
    {
      //Do nothing
    }
    return limitValue;
  }

  /**
   * Centralized conversion method for Double values
   *
   * @author Reid Hayhow
   */
  private Double convertDoubleValue(String key, String value) throws InvalidValueDatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(value != null);

    Double returnValue = null;

    try
    {
      returnValue = StringUtil.convertStringToDouble(value);
    }
    catch (BadFormatException ex)
    {
      //Display the enum key (aka name), file name, and the value that wasn't an int
      InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(key,
                                                                                                value,
                                                                                                _propertiesFullPath);
      invalidValueException.initCause(ex);
      throw invalidValueException;
    }

    return returnValue;
  }

  /**
   * Centralized conversion method for Integer values
   *
   * @author Reid Hayhow
   */
  private Integer convertIntegerValue(String key, String value) throws InvalidValueDatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(value != null);

    Integer returnValue = null;

    try
    {
      returnValue = StringUtil.convertStringToInt(value);
    }
    catch (BadFormatException ex)
    {
      //Display the enum key (aka name), file name, and the value that wasn't an int
      InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(key,
                                                                                                value,
                                                                                                _propertiesFullPath);
      invalidValueException.initCause(ex);
      throw invalidValueException;
    }

    return returnValue;
  }

  /**
   * Centralized conversion method for Integer values
   *
   * @author Reid Hayhow
   */
  private Long convertLongValue(String key, String value) throws InvalidValueDatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(value != null);

    Long returnValue = null;

    try
    {
      returnValue = StringUtil.convertStringToLong(value);
    }
    catch (BadFormatException ex)
    {
      //Display the enum key (aka name), file name, and the value that wasn't an int
      InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(key,
                                                                                                value,
                                                                                                _propertiesFullPath);
      invalidValueException.initCause(ex);
      throw invalidValueException;
    }

    return returnValue;
  }

  /**
   * Method to get the Double representation of a low limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Double getLowLimitDouble(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getLowLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertDoubleValue(expandedKey, value);
  }

  /**
   * Method to get the Double representation of a high limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Double getHighLimitDouble(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getHighLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertDoubleValue(expandedKey, value);
  }

  /**
   * Method to get the Integer representation of a low limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Integer getLowLimitInteger(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getLowLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertIntegerValue(expandedKey, value);
  }

  /**
   * Method to get the Integer representation of a high limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Integer getHighLimitInteger(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getHighLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertIntegerValue(expandedKey, value);
  }

  /**
   * Method to get the Boolean representation of a boolean limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Boolean getBooleanLimit(String key) throws InvalidValueDatastoreException
  {
    Boolean returnValue = null;
    String value = getBoolLimit(key);
    try
    {
      returnValue = StringUtil.convertStringToBoolean(value);
    }
    catch (BadFormatException ex)
    {
      //Display the enum key (aka name), file name, and the value that wasn't an int
      InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(key,
                                                                                                value,
                                                                                                _propertiesFullPath);
      invalidValueException.initCause(ex);
      throw invalidValueException;
    }
    return returnValue;
  }

  /**
   * Method to get the Long representation of a low limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Long getLowLimitLong(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getLowLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertLongValue(expandedKey, value);
  }

  /**
   * Method to get the Long representation of a high limit from the
   * limit file.
   *
   * @author Reid Hayhow
   */
  public Long getHighLimitLong(String key) throws InvalidValueDatastoreException
  {
    String expandedKey = getHighLimitKey(key);
    String value = getLimits(_customerBundle, expandedKey);
    return convertLongValue(expandedKey, value);
  }
}
