package com.axi.v810.hardware.autoCal;

import com.axi.util.*;
import com.axi.util.Enum;

/**
 * Abstract class that can be extended upon to create exception enums for
 * Adjustments, Confirmations or Diagnostics.
 *
 * @author Reid Hayhow
 */
public abstract class HardwareTaskExceptionEnum extends Enum
{
  /**
   * @author Reid Hayhow
   */
  protected HardwareTaskExceptionEnum(int id)
  {
    super(id);
  }

  public abstract LocalizedString getLocalizedString();

}
