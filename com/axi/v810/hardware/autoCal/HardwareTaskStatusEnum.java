package com.axi.v810.hardware.autoCal;

import com.axi.util.Assert;
import com.axi.v810.util.StringLocalizer;

/**
 * Enumeration for the status of a HardwareTask
 *
 * @author Reid Hayhow
 */
public class HardwareTaskStatusEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  //I couldn't identify a need for INIT, but am leaving breadcrumbs
  //public static final HardwareTaskStatusEnum INIT = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum NOT_RUN = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum PASSED = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum FAILED = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum PARTIAL_PASS = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum PARTIAL_FAIL = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum CANCELED = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum EXCEPTION_ENCOUNTERED = new HardwareTaskStatusEnum(++_index);
  public static final HardwareTaskStatusEnum DEFERRED = new HardwareTaskStatusEnum(++_index);

  /**
   *
   * @author Reid Hayhow
   */
  private HardwareTaskStatusEnum(int enumValue)
  {
    super(enumValue);
  }

  /**
   * Method to display a meaningful string for this enum. Necessary to display
   * status in GUI.
   *
   * @author Reid Hayhow
   */
  public String toString()
  {
    String returnString = null;
    if(this == NOT_RUN)
    {
      returnString =  StringLocalizer.keyToString("CD_NOT_RUN_KEY");
    }
    else if(this == PASSED)
    {
      returnString = StringLocalizer.keyToString("CD_PASSED_KEY");
    }
    else if(this == FAILED)
    {
      returnString = StringLocalizer.keyToString("CD_FAILED_KEY");
    }
    else if(this == PARTIAL_PASS)
    {
      returnString = StringLocalizer.keyToString("CD_PARTIAL_PASS_KEY");
    }
    else if(this == PARTIAL_FAIL)
    {
      returnString = StringLocalizer.keyToString("CD_PARTIAL_FAIL_KEY");
    }
    else if(this == CANCELED)
    {
      returnString = StringLocalizer.keyToString("CD_CANCELED_KEY");
    }
    else if(this == EXCEPTION_ENCOUNTERED)
    {
      returnString = StringLocalizer.keyToString("CD_EXCEPTION_KEY");
    }
    else if(this == DEFERRED)
    {
      returnString = StringLocalizer.keyToString("CD_DEFERRED_KEY");
    }
    else
    {
      Assert.expect(false,"Impossible HardwareTaskStatus status!");
    }

    return returnString;
  }
}
