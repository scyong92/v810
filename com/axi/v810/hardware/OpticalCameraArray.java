package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraArray extends HardwareObject implements HardwareStartupShutdownInt
{
    private HardwareObservable _hardwareObservable;    
    private static OpticalCameraArray _instance;
    private static Map<OpticalCameraIdEnum, AbstractOpticalCamera> _idToCameraMap;

    private static PspSettingEnum _settingEnum;
    
    private static int _numberOfCameras;

    /**
     * @author Cheah Lee Herng
     */
    static
    {
        _idToCameraMap = new LinkedHashMap<OpticalCameraIdEnum, AbstractOpticalCamera>();
        _instance = null;
        buildCamerasIfNecessary();
    }

    public static synchronized OpticalCameraArray getInstance()
    {
        if (_instance == null)
            _instance = new OpticalCameraArray();

        return _instance;
    }

    /**
     * @author Cheah Lee Herng
     */
    private OpticalCameraArray()
    {
        _hardwareObservable = HardwareObservable.getInstance();        
        _idToCameraMap = new HashMap<OpticalCameraIdEnum, AbstractOpticalCamera>();
        _instance = null;
    }

    /**
     * @author Cheah Lee Herng
     */
    private static synchronized void buildCamerasIfNecessary()
    {
      if (_idToCameraMap.isEmpty())
      {
        _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));
        
        AbstractOpticalCamera opticalCamera = null;
        List<OpticalCameraIdEnum> idEnums = OpticalCameraIdEnum.getAllEnums();
        _numberOfCameras = idEnums.size();

        for (OpticalCameraIdEnum id : idEnums)
        {
          opticalCamera = AbstractOpticalCamera.getInstance(id);
          
          if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH) ||
              (_settingEnum.equals(PspSettingEnum.SETTING_FRONT) && id.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1)) ||
              (_settingEnum.equals(PspSettingEnum.SETTING_REAR) && id.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2)))
          {
            _idToCameraMap.put(id, opticalCamera);
          }
        }
      }
    }

    /**
     * @author Cheah Lee Herng
     */
    public static synchronized AbstractOpticalCamera getCamera(OpticalCameraIdEnum id)
    {
      Assert.expect(id != null);

      buildCamerasIfNecessary();
      AbstractOpticalCamera opticalCamera = _idToCameraMap.get(id);
      Assert.expect(opticalCamera != null);

      return opticalCamera;
    }

    /**
     * @author Cheah Lee Herng
     */
    public static int getNumberOfCameras()
    {
      return _numberOfCameras;
    }

    /**
     * @author Cheah Lee Herng
     */
    public static List<AbstractOpticalCamera> getCameras()
    {
      buildCamerasIfNecessary();

      List<AbstractOpticalCamera> opticalCameras = new ArrayList<AbstractOpticalCamera>();
      for (Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _idToCameraMap.entrySet())
        opticalCameras.add(entry.getValue());

      return opticalCameras;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void startup() throws XrayTesterException
    {
        _hardwareObservable.stateChangedBegin(this, OpticalCameraEventEnum.INITIALIZE);
        _hardwareObservable.setEnabled(false);

        try
        {
          for (AbstractOpticalCamera opticalCamera : getCameras())
            opticalCamera.startup();
        }
        catch (XrayTesterException xte)
        {
          // we need to clear the map to force a rebuild of all camera instances
          _idToCameraMap.clear();
          throw xte;
        }
        catch (Exception ex)
        {
          // we need to clear the map to force a rebuild of all camera instances
          _idToCameraMap.clear();
          Assert.logException(ex);
        }
        finally
        {
          _hardwareObservable.setEnabled(true);
        }

        _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.INITIALIZE);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void shutdown() throws XrayTesterException
    {      
        try
        {
          for (AbstractOpticalCamera opticalCamera : getCameras())
            opticalCamera.shutdown();
        }
        catch (XrayTesterException xte)
        {
          throw xte;
        }
        catch (Exception ex)
        {
          Assert.logException(ex);
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        return doCamerasRequireStartup();
    }
    
    /**
     * @author Greg Esparza
    */
    public void abort() throws XrayTesterException
    {        
        try
        {
          for (AbstractOpticalCamera opticalCamera : getCameras())
              opticalCamera.abort();
        }
        catch (XrayTesterException xte)
        {
          throw xte;
        }
        catch (Exception ex)
        {
          Assert.logException(ex);
        }
    }

    /**
    * @author Cheah Lee Herng
    */
    private boolean doCamerasRequireStartup() throws XrayTesterException
    {
      boolean camerasRequireStartup = false;

      for (AbstractOpticalCamera opticalCamera : getCameras())
        camerasRequireStartup = camerasRequireStartup || opticalCamera.isStartupRequired();

      return camerasRequireStartup;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    @Override
    public void setSimulationMode() throws XrayTesterException
    {
      super.setSimulationMode();      
      for (AbstractOpticalCamera opticalCamera : getCameras())
        opticalCamera.setSimulationMode();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    @Override
    public void clearSimulationMode() throws XrayTesterException
    {
      super.clearSimulationMode();
      for (AbstractOpticalCamera opticalCamera : getCameras())
        opticalCamera.clearSimulationMode();
    }
}
