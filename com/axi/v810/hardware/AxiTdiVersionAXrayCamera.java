package com.axi.v810.hardware;

import java.net.*;
import java.nio.*;
import java.util.*;
import java.io.*;
import java.security.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;


/**
 * The interface to a single X-ray line scan camera.  The xray tester hardware may have as many as 16
 * of these per system. The camera hardware actually can have several detector rows.
 * The hardware will handle taking the multiple rows and combining them into one.
 * The camera hardware also does all flat fielding correction.
 *
 * Each camera logically has two internal memory buffers.  Each buffer can hold one full scan path
 * for the largest panel allowable in the virgo system.
 *
 * While one buffer is being filled with data  the other buffer can be sending out data.
 *
 * The camera will output a single line of pixels (1x1288 pixels) with each pixel
 * being an 8 bit value.  This is the current implementation, but it will be done so that
 * it will not be hard to have the software handle a different number of pixels and a
 * different number of bits per pixel.
 *
 * Each camera will send out the image it is told to along with the following information
 * The packet will have:
 *   - the camera ID
 *   - the valid recipient ID
 *   - the data (see below for details of what goes here)
 *   - error check bits
 * The data field will contain:
 *   - the scan pass number
 *   - the x,y location of the top left corner of the entire scan image relative to the upper left corner of the scan image
 *   - the width and length of the pixels in the projection region
 *   - a packet ID (if we do not use TCP/IP)
 *   - the total number of packets (if we do not use TCP/IP)
 *   - a region of the projection
 *
 * The camera can scan in data in either the positive or negative y direction.
 * After a scan the camera sends out regions that are specified with x, y, width, and length
 * in pixels.  The origin (0,0) is always at the same point no matter which
 * way the scan was.  The camera will always expect all regions to be referenced with
 * 0,0 being at the top left most point of the area scanned in.
 *
 * The calibrations must be run in this order
 *  1. Sensor segment offset
 *  2. Sensor segment gain
 *  3. Sensor pixel offset
 *  4. Sensor pixel gain
 *
 * Each camera will have a unique MAC address.
 *
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
class AxiTdiVersionAXrayCamera extends AxiTdiXrayCamera
{
  private static final AxiTdiXrayCameraBoardVersionEnum _majorBoardVersion = AxiTdiXrayCameraBoardVersionEnum.BOARD_VERSION_A;

  private static final int _BOOTLOADER_UPDATE_TIME_IN_MILLISECONDS = 20000;
  private static final int _KERNEL_UPDATE_TIME_IN_MILLISECONDS = 20000;
  private static final int _FILE_SYSTEM_UPDATE_TIME_IN_MILLISECONDS = 200000;
  private static final int _APPLICATION_UPDATE_TIME_IN_MILLISECONDS = 2500;
  private static final int _DRIVER_UPDATE_TIME_IN_MILLISECONDS = 3000;
  private static final int _FPGA_UPDATE_TIME_IN_MILLISECONDS = 210000;

  private static final int _LIGHT_SENSITIVITY_ADJUSTMENT_SYSTEM_POSITION_THRESHOLD_DISTANCE_IN_NANOMETERS = 150000000;
  private static final int _ENABLE_IMAGE_CORRECTION_MEDIAN_FILTER = 1;
  private static final int _DISABLE_IMAGE_CORRECTION_MEDIAN_FILTER = 0;
  private static final String _INTERNAL = "internal";
  private static final String _EXTERNAL = "external";
  private static final String _ENABLE_EXTERNAL_PIXEL_CLOCK = _EXTERNAL;
  private static final String _ENABLE_TRIGGER_DETECTION = "enabled";
  private static final String _DISABLE_TRIGGER_DETECTION = "disabled";
  private static final String _ENABLE_SIMULATED_TRIGGERS = _INTERNAL;
  private static final String _DISABLE_SIMULATED_TRIGGERS = _EXTERNAL;

  private static final double _USER_GAIN_MIN = 0;
  private static final double _USER_GAIN_MAX = 32;
  private static final double _USER_OFFSET_MIN = -255.0;
  private static final double _USER_OFFSET_MAX = 255.0;

  private static final int _PREAMP_GAIN_INDEX_0 = 0;
  private static final int _PREAMP_GAIN_INDEX_1 = 1;
  private static final int _PREAMP_GAIN_INDEX_2 = 2;
  private static final int _PREAMP_GAIN_INDEX_3 = 3;

  private static final String _SIMULATION_IMAGE_INFORMATION = "0 0 0 1087 2000";
  private static final double _SIMULATION_CAMERA_TEMPERATURE_IN_FAHRENHEIT = 75.0;
  private static final double _SIMULATION_CAMERA_USER_GAIN = 1.0;
  private static final double _SIMULATION_CAMERA_USER_OFFSET = 0.0;

  private static final String _CALIBRATION_MODE_GET_DATA = "\\s+";
  private static final String _SILENT_PERIOD_REQUIRED = "0";
  private static final String _SCAN_PRORGRAM_DATA_DELIMITER = " ";
  private static final String _SIMULATION_FLASH_MODULE_TYPE = "00020000";
  private static final String _NO_BOARD_VERSION_INFORMATION = "";

  private static final int _SENSOR_NUMBER_OF_SEGMENTS = 4;
  private static final int _SENSOR_NUMBER_OF_UNUSED_PIXELS = 4;
  private static final int _SENSOR_WIDTH_IN_PIXELS = 1088;
  private static final int _SENSOR_COLUMN_PITCH_IN_NANOMETERS = 100000;
  private static final int _SENSOR_ROW_PITCH_IN_NANOMETERS = 100000;
  private static final int _SENSOR_NUMBER_OF_ROWS = 24;
  private static final int _MAXIMUM_NUMBER_OF_IMAGES_CAN_STORE = 6;
  private static final int _NUMBER_OF_TRIGGERS_TO_COMPLETE_ACQUISITION = 3;
  //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  private static final int _EXTRA_DELAY_TO_MAKE_HYSTERESIS_MORE_POSITIVE_IN_NANOMETERS = 72000;
  private static final int _CAMERA_TRANSFER_RATE = 256;

  private static final String _GET_BOARD_VERSION_COMMAND = "./opt/getBoardVersion";
  private static final String _GET_BOOTLOADER_VERSION_COMMAND = "sh ./opt/getBootloaderVersion.sh";
  private static final String _GET_KERNEL_VERSION_COMMAND = "sh ./opt/getKernelVersion.sh";
  private static final String _GET_FILESYSTEM_VERSION_COMMAND = "sh ./opt/getFileSystemVersion.sh";
  private static final String _GET_APPLICATION_VERSION_COMMAND = "getAppVersion";
  private static final String _GET_DRIVER_VERSION_COMMAND = "getDriverVersion";
  private static final String _GET_FPGA_VERSION_COMMAND = "getFpgaVersion";
  private static final String _REBOOT_COMMAND = "./sbin/reboot";
  private static final String _INIT_ACQUISITION_COMMAND = "initAcquisition";
  private static final String _SET_SCAN_PATH_COMMAND = "setScanPath";
  private static final String _SET_SCAN_PASS_COMMAND = "setScanPass";
  private static final String _CLEAR_SCAN_PATH_COMMAND = "clearScanPath";
  private static final String _MEASURE_CHANNEL_GAIN_COMMAND = "measureChannelGain";
  private static final String _MEASURE_CHANNEL_OFFSET_COMMAND = "measureChannelOffset";
  private static final String _MEASURE_PIXEL_GAIN_COMMAND = "measurePixelGain";
  private static final String _MEASURE_PIXEL_OFFSET_COMMAND = "measurePixelOffset";
  //Variable Mag Anthony August 2011
  private static final String _SAVE_VARIABLE_MAG_CALIBRATION_CONFIGURATION_COMMAND = "saveconfiguration";
  private static final String _LOAD_VARIABLE_MAG_CALIBRATION_CONFIGURATION_COMMAND = "loadconfiguration";

  private static final String _GET_CALIBRATION_TABLE_COMMAND = "getCalibrationTable";
  private static final String _SET_TRIGGER_SOURCE_COMMAND = "setTriggerSource";
  private static final String _GET_IMAGE_DATA_COMMAND = "getImageData";
  private static final String _GET_IMAGE_INFORMATION_COMMAND = "getImageInfo";
  private static final String _ENABLE_TRIGGER_INPUT_COMMAND = "triggers";
  private static final String _READ_TEMPERATURE_COMMAND = "readTemperature";
  private static final String _SET_PREAMP_GAIN_COMMAND = "setPreampGain";
  private static final String _GET_USER_GAIN_COMMAND = "getUserGain";
  private static final String _SET_USER_GAIN_COMMAND = "setUserGain";
  private static final String _GET_USER_OFFSET_COMMAND = "getUserOffset";
  private static final String _SET_USER_OFFSET_COMMAND = "setUserOffset";
  private static final String _SET_CAL_POINT_VALUES_COMMAND = "setCalPointValues";
  private static final String _GET_FREE_BUFFER_COUNT = "getFreeBufferCount";
  private static final String _GET_SNAP_SHOT_COMMAND = "getSnapShot";
  private static final String _SET_MEDIAN_FILTER_COMMAND = "setMedianFilter";
  private static final String _SET_PIXEL_CLOCK_SOURCE_COMMAND = "setPixelClockSource";
  private static final String _GET_SILENT_PERIOD_STATE_COMMAND = "getSilentPeriodState";
  private static final String _ABORT_ACQUISITION_COMMAND = "abortAcquisition";
  private static final String _GET_UNCAPTURED_PROJECTION_COUNT_COMMAND = "getUncapturedProjectionCount";
  private static final String _GET_ELAPSED_CAL_TIME_COMMAND = "getElapsedCalTime";
  private static final String _REMOVE_ALL_FILES_COMMAND = "rm -f ";
  private static final String _CREATE_FILE_COMMAND = "touch ";
  private static final String _CHANGE_FILE_PERMISSION_COMMAND = "chmod 777 ";
  private static final String _GENERATE_MD5_CHECKSUM_COMMAND = "md5sum ";
  private static final String _MAKE_DIRECTORY_COMMAND = "mkdir -p ";
  private static final String _COPY_FILE_COMMAND = "cp -f ";

  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER = ",";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_APPLICATION_KEY_NAME = "application";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_DRIVER_KEY_NAME = "driver";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_FPGA_KEY_NAME = "fpga";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_FILESYSTEM_KEY_NAME = "fileSystem";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_BOOTLOADER_KEY_NAME = "bootLoader";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_KERNEL_KEY_NAME = "kernel";
  private static final String _CALIBRATION_DATA_LINE_DELIMITER = ";";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_BOOTSCRIPT_KEY_NAME = "bootscript";
  private static final String _RUNTIME_INFORMATION_CONFIG_FILE_RCSHFILE_KEY_NAME = "rcshfile";

  private static final String _BOOTLOADER_DEST_DEV_FILE_NAME = "mtdblock0";

  private static final int _NUMBER_OF_PIXEL_CALIBRATION_DATA_ATTRIBUTES = 5;
  private static final int _NUMBER_OF_SEGMENT_CALIBRATION_ATTRIBUTES = 5;
  private static final String _REXEC_CLIENT_USER_NAME = "root";
  private static final String _REXEC_CLIENT_PASSWORD = "root";
  private static final int _REBOOT_START_TIME_IN_MILLISECONDS = 2000;
  private static boolean _printProgramDownloadedToCameras = _config.getBooleanValue(HardwareConfigEnum.XRAY_CAMERA_LOGGING_ENABLED) ||
                                                            _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);

  //Swee-Yee.Wong - constant used to calculate camera calPoint Values
  private static final double _MAGNIFICATION_TO_Y_AXIS_SCAN_MOTION_VELOCITY_CONSTANT = 2083330000;
  
  private int _numberOfLineScanImagesToAcquire;
  private boolean _triggerDetectionEnabled;
  private TftpClientAxi _tftpClientAxi;
  private RexecClientAxi _rExecClientAxi;
  private boolean _rebootRequired;

  private String _bootLoaderFileName;
  private String _bootLoaderLocalFilePath;
  private String _bootLoaderRemoteTempPath;
  private String _bootLoaderRemoteRuntimePath;
  private String _bootLoaderVersion;
  private String _bootLoaderChecksum;

  private String _kernelFileName;
  private String _kernelLocalFilePath;
  private String _kernelRemoteTempPath;
  private String _kernelRemoteRuntimePath;
  private String _kernelVersion;
  private String _kernelChecksum;

  private String _fileSystemFileName;
  private String _fileSystemLocalFilePath;
  private String _fileSystemRemoteTempPath;
  private String _fileSystemRemoteRuntimePath;
  private String _fileSystemVersion;
  private String _fileSystemChecksum;
  private boolean _fileSystemUpdated;

  private String _applicationFileName;
  private String _applicationLocalFilePath;
  private String _applicationRemoteTempPath;
  private String _applicationRemoteRuntimePath;
  private String _applicationVersion;
  private String _applicationChecksum;

  private String _driverFileName;
  private String _driverLocalFilePath;
  private String _driverRemoteTempPath;
  private String _driverRemoteRuntimePath;
  private String _driverVersion;
  private String _driverChecksum;

  private String _fpgaFileName;
  private String _fpgaLocalFilePath;
  private String _fpgaRemoteTempPath;
  private String _fpgaRemoteRuntimePath;
  private String _fpgaVersion;
  private String _fpgaChecksum;

  private String _bootscriptFileName;
  private String _bootscriptLocalFilePath;
  private String _bootscriptRemoteTempPath;
  private String _bootscriptRemoteRuntimePath;
  private String _bootscriptVersion;
  private String _bootscriptChecksum;

  private String _rcshFileFileName;
  private String _rcshFileLocalFilePath;
  private String _rcshFileRemoteTempPath;
  private String _rcshFileRemoteRuntimePath;
  private String _rcshFileVersion;
  private String _rcshFileChecksum;

  private double _currentUserGain = 1.0001;
  private double _currentUserOffset = 0.0001;
  private String _currentVariableMagCalibrationConfigFile = "";
  
  //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  private final HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private List<String> _upgradeFirmwareMessageList;
  private String _boardVersion;

  private static final String _ENABLE_SAFEGUARD_COMMAND = "safeguards";
  private static final String _ENABLE_SAFEGUARDS = "enable";
  private static final String _DISABLE_SAFEGUARDS = "disable";

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  AxiTdiVersionAXrayCamera(XrayCameraIdEnum cameraId, String boardVersion, IntCoordinate systemPositionInNanometers)
  {
    super(cameraId, boardVersion, systemPositionInNanometers);

    _boardVersion = boardVersion;
    _fileSystemUpdated = false;
    _rebootRequired = false;
    _tftpClientAxi = new TftpClientAxi(_serverIpAddress, TftpClientAxiCommunicationModeEnum.TFTP_BINARY_MODE);
    _rExecClientAxi = new RexecClientAxi(_serverIpAddress, _REXEC_CLIENT_USER_NAME, _REXEC_CLIENT_PASSWORD);
    _triggerDetectionEnabled = false;
    _numberOfLineScanImagesToAcquire = 0;
    _upgradeFirmwareMessageList = new ArrayList<>();
           
    //double Gain = _config.getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_GAIN );
    //double Offset = _config.getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_OFFSET);

  }

  /**
   * @author Greg Esparza
   */
  public void startup() throws XrayTesterException
  {
    int numberOfRetries = 2;
    int retryCount = 1;
    boolean rebootRecovery = false;

    clearAbort();

    try
    {
      do
      {
        setStartupBeginProgressState();
        _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.INITIALIZE);
        _hardwareObservable.setEnabled(false);
        _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.XRAY_CAMERA_INITIALIZE, getStartupTimeInMilliseconds());

        try
        {
          // apm -- added a nice simulation mode to speed up overall software development
          if ((UnitTest.unitTesting() == false) &&  isSimulationModeOn())
          {
            _initialized = true;
            return;
          }


          if (isAborting())
            return;

          verifyCommunication();
          if (isAborting())
            return;

          verifyRuntimeFiles();
          if (isAborting())
            return;

          // CR1043 fix by LeeHerng - Check camera firmware version
          verifyCameraFirmwareVersionWithoutAutomaticRuntimeFileUpdate();
          if (isAborting())
            return;

          clearScanProgram();
          if (isAborting())
            return;

          stopSimulatedTriggers();
          if (isAborting())
            return;

          disableTriggerDetection();
          if (isAborting())
            return;

          setPixelClockMode(_ENABLE_EXTERNAL_PIXEL_CLOCK);
          if (isAborting())
            return;

          setLightSensitivity();
          if (isAborting())
            return;

          setConfigurationDescription();
          if (isAborting())
            return;

          _initialized = true;
        }
        catch (XrayCameraHardwareException he)
        {
          handleCriticalErrors(he);

          if (retryCount < numberOfRetries)
          {
            setStartupEndProgressState();
            rebootRecovery = true;
          }
          else
            _hardwareTaskEngine.throwHardwareException(he);
        }

        if (rebootRecovery)
        {
          reboot();
          rebootRecovery = false;
        }

        ++retryCount;
      }
      while (_initialized == false);
    }
    finally
    {
      setStartupEndProgressState();
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.XRAY_CAMERA_INITIALIZE);
      _hardwareObservable.setEnabled(true);
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.INITIALIZE);
    }
  }

  /**
   * @author Greg Esparza
   */
  public void shutdown() throws XrayTesterException
  {
    _initialized = false;

    try
    {
      abortAcquisition();
      disconnect();
    }
    catch (XrayTesterException xte)
    {
      // do nothing
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    if (_initialized == false)
    {
      setRuntimeFileInformationReady(false);
      return true;
    }
    else
      return false;
  }

  /**
   * @author Greg Esparza
   */
  public boolean isCalibrated() throws XrayTesterException
  {
    boolean isCalibrated = true;

    if (isSimulationModeOn() == false)
    {
      try
      {
        sendCommandAndGetReplies(_GET_ELAPSED_CAL_TIME_COMMAND);
      }
      catch (XrayCameraHardwareException he)
      {
        if (he.getExceptionType().equals(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_GRAYSCALE_ADJUSTMENT_REQUIRED))
          isCalibrated = false;
      }
    }

    return isCalibrated;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getSensorWidthInPixels()
  {
    return _SENSOR_WIDTH_IN_PIXELS;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getSensorWidthInPixelsWithOverHeadByte()
  {
    return _SENSOR_WIDTH_IN_PIXELS;
  }

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getNumberOfUnusedPixelsOnEndOfSensor()
  {
     return _SENSOR_NUMBER_OF_UNUSED_PIXELS;
  }

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getNumberOfSensorPixelsUsed()
  {
    return getSensorWidthInPixelsWithOverHeadByte() - getNumberOfUnusedPixelsOnEndOfSensor();
  }

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getSensorColumnPitchInNanometers()
  {
    return _SENSOR_COLUMN_PITCH_IN_NANOMETERS;
  }

  /**
   * @author Greg Esparza
   */
  public int getSensorRowPitchInNanometers()
  {
    return _SENSOR_ROW_PITCH_IN_NANOMETERS;
  }

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getNumberOfSensorRows()
  {
    return _SENSOR_NUMBER_OF_ROWS;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getNumberOfSensorRowsWithOverHeadByte()
  {
    return _SENSOR_NUMBER_OF_ROWS;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getNumberOfSensorRowsUsed()
  {
    return (int)Math.floor((double)(getNumberOfSensorRows() / getCameraBinningMode()));
  }

  /**
   * @author Greg Esparza
   */
  public int getSensorWidthInNanometers()
  {
    return getSensorWidthInPixels() * getSensorColumnPitchInNanometers();
  }

  /**
   * @author Greg Esparza
   */
  public int getSensorHeightInNanometers()
  {
    return getNumberOfSensorRows() * getSensorRowPitchInNanometers();
  }

  /**
   * @author Greg Esparza
   */
  public int getNumberOfTriggersRequiredToCompleteAcquisition()
  {
    return _NUMBER_OF_TRIGGERS_TO_COMPLETE_ACQUISITION;
  }

  /**
   * @author Greg Esparza
   */
  public int getMaximumNumberOfImagesCameraCanStore()
  {
    return _MAXIMUM_NUMBER_OF_IMAGES_CAN_STORE;
  }

  /**
   * @author Greg Esparza
   */
  public void abort() throws XrayTesterException
  {
    abortAcquisition();
  }

  /**
   * Pass in all information needed before a panel is scanned.  This call will be made
   * once before the entire panel is scanned.
   *
   * It sets things like how long the camera should delay after getting the trigger to begin
   * taking readings and how many readings the sensor should take.  Since each camera is in a
   * different physical location, the time delay after the trigger needs to be different
   * for each one.

   * setScanPass %d 0 0 10000 0 1 \
   *        0       0       1087            9999    192.168.128.105
   *
   * setScanPass %d 1 0 10000 1 1 \
   *        0       0       1087            9999    192.168.128.105
   *
   *
   *
   * @param scanPath must be accessed to determine scan direction of each ProjectionSetting
   * @param numberOfLinesToSkipDuringPositiveYscan is the number of lines for the camera hardware to skip before it begins taking exposures.
   * @param numberOfLinesToSkipDuringNegativeYscan is the number of lines for the camera hardware to skip before it begins taking exposures.
   * @param projectionSettingsList are the settings to be applied to this camera.
   *
   * @author George A. David
   * @author Roy Williams
   * @author Greg Esparza
   */
  public void setScan(List<? extends ScanPass> scanPath,
                      int numberOfLinesToSkipDuringPositiveYscan,
                      int numberOfLinesToSkipDuringNegativeYscan,
                      List<ProjectionSettings> projectionSettingsList) throws XrayTesterException
  {
    Assert.expect(scanPath != null);
    Assert.expect(numberOfLinesToSkipDuringPositiveYscan >= 0);
    Assert.expect(numberOfLinesToSkipDuringNegativeYscan >= 0);
    Assert.expect(projectionSettingsList != null);

    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.SET_SCAN_PROGRAM);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();
      clearScanProgram();
      _numberOfLineScanImagesToAcquire = scanPath.size();
      setScanPath(scanPath.size(), numberOfLinesToSkipDuringPositiveYscan, numberOfLinesToSkipDuringNegativeYscan);

      if (isAborting())
        return;
      
      // Swee Yee Wong - log scan pass information that send to camera
      // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
      logIaeCameraCommands("Pass\tCamera\t# Rows to Collect\tdirection\t# Triggers to Ignore (+ve)\t# Triggers to Ignore (-ve)");

      int numberOfLinesToCapture = -1;
      StringBuffer parameters = null;
      for (ProjectionSettings settings : projectionSettingsList)
      {
        parameters = new StringBuffer();
        parameters.append(settings.getScanPassNumber());
        parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
        parameters.append(settings.getStartCaptureLine());
        parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
        numberOfLinesToCapture = settings.getNumberOfCaptureLines();
        parameters.append(numberOfLinesToCapture);
        parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
        
        if (scanPath.get(settings.getScanPassNumber()).isStageDirectionForward())
          parameters.append(0);
        else
          parameters.append(1);

        parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
        List<ProjectionRegion> regions = settings.getProjectionRegions();
        parameters.append(regions.size());

        if (regions.size() > 0)
        {
          parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
          for (ProjectionRegion region : regions)
          {
            ImageRectangle regionRect = region.getProjectionRegionRectangle();
            int beginX = regionRect.getMinX();
            int beginY = regionRect.getMinY();
            int endX = regionRect.getMaxX();
            int endY = regionRect.getMaxY();
            InetAddress address = region.getProjectionDataDestinationAddress();
            int startPortNumber = region.getStartPortNumber();    // Bee Hoon, Single Server   
            parameters.append(beginX);
            parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
            parameters.append(beginY);
            parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
            parameters.append(endX);
            parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
            parameters.append(endY);
            parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
            
            if (isUnitTestModeOn())
              parameters.append(IPaddressUtil.getRemoteLoopbackAddress().getHostAddress());
            else
            {             
              if(startPortNumber == 0)
              {
                parameters.append(address.getHostAddress());
              }
              else
              {
                parameters.append(address.getHostAddress()).append(":").append(startPortNumber + this.getId()); 
              }            
            }
            parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
          }
        }

        // Format:
        //
        // setScanPass <pass-number> <start-line-number> <number-of-lines-to-capture> <scan-direction> <number-of-regions> <left> <top> <right> <bottom> <ip-address> <port>...)
        //
        // where the following definitions can be used....
        //
        // start-line-number          => always 0 for this release.
        // number-of-lines-to-capture => number of triggers after camera delay for this camera.
        //                            => hang can result if not enough triggers are delivered
        //                            => from stage.
        // scan-direction             => 0 is forward and 1 is reverse.
        // number-of-regions          => IRP's should be requesting 8 regions = 4 IRPs and 2 ports per IRP
        // ROI is Region of Interest  => area to send back to IMAGE_RECIEVER.

        if (isAborting())
          return;

        _xRayCameraLogUtil.log("Setting scan pass information", parameters.toString());
        if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        {
          String command = _SET_SCAN_PASS_COMMAND + " " + parameters;
          if (_printProgramDownloadedToCameras)
            System.out.println("  " + command);
          List<String> replies = sendCommandAndGetReplies(command.trim());
          Assert.expect(replies.isEmpty());
        }
        // Swee Yee Wong - log scan pass information that send to camera
        // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
        if (scanPath.get(settings.getScanPassNumber()).isStageDirectionForward())
          logIaeCameraCommands(settings.getScanPassNumber() + "\t" + getId() + "\t" + numberOfLinesToCapture + "\t0\t" + numberOfLinesToSkipDuringPositiveYscan + "\t" + numberOfLinesToSkipDuringNegativeYscan);
        else
          logIaeCameraCommands(settings.getScanPassNumber() + "\t" + getId() + "\t" + numberOfLinesToCapture + "\t1\t" + numberOfLinesToSkipDuringPositiveYscan + "\t" + numberOfLinesToSkipDuringNegativeYscan);
          
      }
      
      _maxImageCaptureBuffers = getFreeImageBufferCount();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.SET_SCAN_PROGRAM);
  }

  /**
   * @author George A. David
   */
  private void setScanPath(int scanPathSize,
                           int numberOfLinesToSkipDuringPositiveYscan,
                           int numberOfLinesToSkipDuringNegativeYscan) throws XrayTesterException
  {
    StringBuffer parameters = new StringBuffer();
    parameters.append(scanPathSize);
    parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
    parameters.append(numberOfLinesToSkipDuringPositiveYscan);
    parameters.append(_SCAN_PRORGRAM_DATA_DELIMITER);
    parameters.append(numberOfLinesToSkipDuringNegativeYscan);

    if (_printProgramDownloadedToCameras)
      System.out.println(_xRayCameraLogName + " " + parameters.toString());
    _xRayCameraLogUtil.log("Setting scan path information", parameters.toString());
    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_SET_SCAN_PATH_COMMAND + " " + parameters.toString());
      Assert.expect(replies.isEmpty());
    }
  }

  /**
   * @author Greg Esparza
   */
  public int getNumberOfLineScanImagesAcquired() throws XrayTesterException
  {
    List<String> replies = sendCommandAndGetReplies(_GET_UNCAPTURED_PROJECTION_COUNT_COMMAND);
    Assert.expect(replies.size() == 1);

    int numberOfRemainingLineScanImagesToAcquire = 0;
    try
    {
      numberOfRemainingLineScanImagesToAcquire = StringUtil.convertStringToInt(replies.get(0));
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidNumberOfLineScanImagesAcquiredValue(_cameraId,
                                                                                                                           _serverIpAddress,
                                                                                                                           _portNumber,
                                                                                                                           bfe.getLocalizedMessage());
      he.initCause(bfe);
      _hardwareTaskEngine.throwHardwareException(he);
    }

    int numberOfLineScanImagesAcquired = _numberOfLineScanImagesToAcquire - numberOfRemainingLineScanImagesToAcquire;

    Assert.expect(numberOfLineScanImagesAcquired >= 0);
    return numberOfLineScanImagesAcquired;
  }

  /**
   * @author Greg Esparza
   */
  public Image getLineScanImage() throws XrayTesterException
  {
    Image image = null;

    clearAbort();

    AxiTdiXrayCameraImageInformation xRayCameraImageInformation = getImageInformation();

    if (isAborting())
      return null;

    int widthInPixels = xRayCameraImageInformation.getRegionOfInterestWidthInPixels();
    int heightInPixels = xRayCameraImageInformation.getRegionOfInterestHeightInPixels();
    int imageSizeInPixels = widthInPixels * heightInPixels;

    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      ByteBuffer rawBytesFromCamera = getBytesFromCamera(_GET_IMAGE_DATA_COMMAND, imageSizeInPixels);

      if (isAborting())
        return null;

      int orientationInDegrees = 0;
      int xOriginInPixels = xRayCameraImageInformation.getRegionOfInterestXoriginInPixels();
      int yOriginInPixels = xRayCameraImageInformation.getRegionOfInterestYoriginInPixels();

      try
      {
        ImageRectangle imageRectangle = new ImageRectangle(xOriginInPixels, yOriginInPixels, widthInPixels, heightInPixels);
        RegionOfInterest regionOfInterest = new RegionOfInterest(imageRectangle, orientationInDegrees, RegionShapeEnum.RECTANGULAR);
        image = Image.createFloatImageFromByteBuffer(rawBytesFromCamera, widthInPixels, heightInPixels, regionOfInterest);
      }
      catch (Throwable throwable)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetLineScanImageException(_cameraId, _serverIpAddress, _portNumber, throwable.getLocalizedMessage());
        he.initCause(throwable);
        _hardwareTaskEngine.throwHardwareException(he);
      }
    }
    else
      image = Image.createUnmonitoredFloatImage(widthInPixels, heightInPixels);


    return image;
  }
  
  
  /**
   * @author Eric Littlefield
   */
   public int getFreeImageBufferCount() throws XrayTesterException
   {
     int freeBufferCount = 0;

     if (isAborting() == false)
     {
       if (isSimulationModeOn() == false)
       {
         List<String> replies = sendCommandAndGetReplies(_GET_FREE_BUFFER_COUNT);
         Assert.expect(replies.size() == 1); // does this mean 1 character or 1 word???

         try
         {
           freeBufferCount = StringUtil.convertStringToInt(replies.get(0));
         }
         catch (BadFormatException bfe)
         {
           XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetFreeImageBufferStatusException(_cameraId,
                                                                                                                               _serverIpAddress,
                                                                                                                               _portNumber,
                                                                                                                               bfe.getLocalizedMessage());
           he.initCause(bfe);
           _hardwareTaskEngine.throwHardwareException(he);
         }
       }
     }

     return freeBufferCount;
  }

  /**
   * @author George A. David
   */
  public void runSensorSegmentOffsetCalibration() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_SENSOR_SEGMENT_OFFSET_CALIBRATION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        sendCommandAndGetReplies(_MEASURE_CHANNEL_OFFSET_COMMAND);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_SENSOR_SEGMENT_OFFSET_CALIBRATION);
  }

  /**
   * @author George A. David
   */
  public void runSensorSegmentGainCalibration() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_SENSOR_SEGMMENT_GAIN_CALIBRATION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        sendCommandAndGetReplies(_MEASURE_CHANNEL_GAIN_COMMAND);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_SENSOR_SEGMMENT_GAIN_CALIBRATION);
  }

  /**
   * @author George A. David
   */
  public void runSensorPixelOffsetCalibration() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_SENSOR_PIXEL_OFFSET_CALIBRATION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        sendCommandAndGetReplies(_MEASURE_PIXEL_OFFSET_COMMAND);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_SENSOR_PIXEL_OFFSET_CALIBRATION);
  }

  /**
   * @author George A. David
   */
  public void runSensorPixelGainCalibration() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_SENSOR_PIXEL_GAIN_CALIBRATION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        sendCommandAndGetReplies(_MEASURE_PIXEL_GAIN_COMMAND);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_SENSOR_PIXEL_GAIN_CALIBRATION);
  }

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  public void runSaveVariableMagCalibrationConfiguration(String configFileName) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_SAVE_VARIABLE_MAG_CALIBRATION_CONFIGURATION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
      {
        List<String> replies = sendCommandAndGetReplies(_SAVE_VARIABLE_MAG_CALIBRATION_CONFIGURATION_COMMAND + " " + configFileName);
        //Assert.expect(replies.isEmpty());
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_SAVE_VARIABLE_MAG_CALIBRATION_CONFIGURATION);
  }

 /**
   * @author Anthony Fong
   */
  public void runLoadVariableMagCalibrationConfiguration(String configFileName) throws XrayTesterException
  {
    if (_currentVariableMagCalibrationConfigFile.equalsIgnoreCase(configFileName))
    {
      //skip runLoadVariableMagCalibrationConfiguration
    }
    else
    {
      _currentVariableMagCalibrationConfigFile = configFileName;
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.RUN_LOAD_VARIABLE_MAG_CALIBRATION_CONFIGURATION);
      _hardwareObservable.setEnabled(false);

      try
      {
        clearAbort();

        if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
        {
          List<String> replies = sendCommandAndGetReplies(_LOAD_VARIABLE_MAG_CALIBRATION_CONFIGURATION_COMMAND + " " + configFileName);
          //Assert.expect(replies.isEmpty());
        }
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }

      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.RUN_LOAD_VARIABLE_MAG_CALIBRATION_CONFIGURATION);
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isReadyForAcquisition() throws XrayTesterException
  {
    boolean readyForAcquisition = false;

    clearAbort();

    if ((isImageBufferFree()) && (isSilentPeriodRequired() == false))
      readyForAcquisition = true;

    return readyForAcquisition;
  }

  /**
   * Tell the camera to snap and get the area mode image
   *
   * @author Greg Esparza
   * @author George A. David
   */
  public Image getAreaModeImage() throws XrayTesterException
  {
    Image image = null;

    clearAbort();

    if (isSimulationModeOn() == false || isUnitTestModeOn())
    {
      if (isAborting())
        return null;

      //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
      int imageWidthInPixels = getSensorWidthInPixelsWithOverHeadByte();
      int imageLengthInPixels = getNumberOfSensorRowsWithOverHeadByte();
      int imageSize = imageWidthInPixels * imageLengthInPixels;
      ByteBuffer byteBuffer = getBytesFromCamera(_GET_SNAP_SHOT_COMMAND, imageSize);

      image = Image.createFloatImageFromByteBuffer(byteBuffer, imageWidthInPixels, imageLengthInPixels);
    }
    else
    {
      image = Image.createAlignedFloatImage(getSensorWidthInPixelsWithOverHeadByte(), getNumberOfSensorRowsWithOverHeadByte());
    }

    Assert.expect(image != null);
    return image;
  }

  /**
   * @author Greg Esparza
   */
  public void enableTriggerDetection() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.ENABLE_TRIGGER_DETECTION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();
      setTriggerDetection(_ENABLE_TRIGGER_DETECTION);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.ENABLE_TRIGGER_DETECTION);
  }

  /**
   * @author Greg Esparza
   */
  public void disableTriggerDetection() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.DISABLE_TRIGGER_DETECTION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();
      setTriggerDetection(_DISABLE_TRIGGER_DETECTION);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.DISABLE_TRIGGER_DETECTION);
  }

  /**
   * @author Greg Esparza
   */
  public boolean isTriggerDetectionEnabled() throws XrayTesterException
  {
    return _triggerDetectionEnabled;
  }

  /**
   * @author George A. David
   */
  public void initializeAcquisition() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.INITIALIZE_ACQUISITION);
    _hardwareObservable.setEnabled(false);

    clearAbort();

    try
    {
      if (isSimulationModeOn() == false || isUnitTestModeOn())
      {
        if (isAborting())
          return;

        List<String> replies = sendCommandAndGetReplies(_INIT_ACQUISITION_COMMAND);
        Assert.expect(replies.isEmpty());
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.INITIALIZE_ACQUISITION);
  }

  /**
   * @author Greg Esparza
   */
  public void enableImageCorrection() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.ENABLE_IMAGE_CORRECTION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();
      setMedianFilter(_ENABLE_IMAGE_CORRECTION_MEDIAN_FILTER);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.ENABLE_IMAGE_CORRECTION);
  }

  /**
   * @author Greg Esparza
   */
  public void disableImageCorrection() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.DISABLE_IMAGE_CORRECTION);
    _hardwareObservable.setEnabled(false);

    try
    {
      clearAbort();
      setMedianFilter(_DISABLE_IMAGE_CORRECTION_MEDIAN_FILTER);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.DISABLE_IMAGE_CORRECTION);
  }

  /**
   * @author Greg Esparza
   */
  protected void disconnect() throws XrayTesterException
  {
    super.disconnect();

    if (isSimulationModeOn() == false)
      _tftpClientAxi.close();
  }

  /**
   * @param userGain
   * @param userOffset
   * @throws XrayTesterException
   *
   * @author Anthony Fong
   */
  public void setCameraUserGainAndOffset(double userGain, double userOffset) throws XrayTesterException
  {
    if (isSimulationModeOn() == false || (isUnitTestModeOn()))
    {
      setUserGain(userGain);
      setUserOffset(userOffset);
    }
    else
    {
      setUserGain(_SIMULATION_CAMERA_USER_GAIN);
      setUserOffset(_SIMULATION_CAMERA_USER_OFFSET);
    }
  }

  /**
   * @author Greg Esparza
   */
  protected void setLightSensitivity() throws XrayTesterException
  {

    String type = _config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE);
    if (type.equals("legacy"))
    {
      if (_systemPositionDistanceFromOriginInNanometers < _LIGHT_SENSITIVITY_ADJUSTMENT_SYSTEM_POSITION_THRESHOLD_DISTANCE_IN_NANOMETERS)
      {
        setPreampGain(_PREAMP_GAIN_INDEX_1);
      }
      else
      {
        setPreampGain(_PREAMP_GAIN_INDEX_2);
      }
    }
    else if (type.equals("standard"))
    {
      if (_systemPositionDistanceFromOriginInNanometers < _LIGHT_SENSITIVITY_ADJUSTMENT_SYSTEM_POSITION_THRESHOLD_DISTANCE_IN_NANOMETERS)
      {
        setPreampGain(_PREAMP_GAIN_INDEX_1);
      }
      else
      {
        setPreampGain(_PREAMP_GAIN_INDEX_2);
      }
    }
    else if (type.equals("htube"))
    {
      // set default Ligt Sensitivity (Gain: 1, Offset: 0)
//      setDefaultLightSensitivity();
      
      switch (getId())
      {
        case 0:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_0_PREAMP_GAIN));
          break;
        case 1:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_1_PREAMP_GAIN));
          break;
        case 2:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_2_PREAMP_GAIN));
          break;
        case 3:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_3_PREAMP_GAIN));
          break;
        case 4:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_4_PREAMP_GAIN));
          break;
        case 5:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_5_PREAMP_GAIN));
          break;
        case 6:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_6_PREAMP_GAIN));
          break;
        case 7:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_7_PREAMP_GAIN));
          break;
        case 8:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_8_PREAMP_GAIN));
          break;
        case 9:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_9_PREAMP_GAIN));
          break;
        case 10:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_10_PREAMP_GAIN));
          break;
        case 11:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_11_PREAMP_GAIN));
          break;
        case 12:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_12_PREAMP_GAIN));
          break;
        case 13:
          setPreampGain(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_13_PREAMP_GAIN));
          break;
        default:
          Assert.expect(false, "unknown axiType1Camera from config file, HardwareConfigEnum bug?");
      }

      setCalPointValues(_config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MIN),
        _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MAX),
        _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MIN),
        _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MAX));

    }
    else
    {
      Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
    }

  }

  /**
   * @author Wei Chin
   */
  public void setUserDefinedLightSensitivity() throws XrayTesterException
  {
    setCameraUserGainAndOffset(_config.getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_GAIN),
      _config.getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_OFFSET));
  }

  /**
   * @author Wei Chin
   */
  public void setDefaultLightSensitivity() throws XrayTesterException
  {
    setCameraUserGainAndOffset ( _SIMULATION_CAMERA_USER_GAIN , _SIMULATION_CAMERA_USER_OFFSET );
  }

  /**
   * @author Greg Esparza
   */
  private int getStartupTimeInMilliseconds()
  {
    int startupTimeInMilliseconds = _BOOTLOADER_UPDATE_TIME_IN_MILLISECONDS +
                                    _KERNEL_UPDATE_TIME_IN_MILLISECONDS +
                                    _FILE_SYSTEM_UPDATE_TIME_IN_MILLISECONDS +
                                    _APPLICATION_UPDATE_TIME_IN_MILLISECONDS +
                                    _DRIVER_UPDATE_TIME_IN_MILLISECONDS +
                                    _FPGA_UPDATE_TIME_IN_MILLISECONDS;

    return startupTimeInMilliseconds;
  }

  /**
   * @author Greg Esparza
   */
  private void handleCriticalErrors(XrayCameraHardwareException he) throws XrayTesterException
  {
    Assert.expect(he != null);

    XrayCameraHardwareExceptionEnum exceptionType = he.getExceptionType();

    if ((exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_GET_POWER_STATUS)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_NOT_POWERED_ON)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_CONNECT)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_GET_COMMUNICATION_PING_STATUS)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_COMMUNICATION_PING)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_EXECUTE_COMMAND)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_INVALID_COMMAND)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_INVALID_COMMAND_PARAMETER)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_INVALID_SEQUENCE)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_SYSTEM_ERROR)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_GET_BOARD_VERSION)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_BOARD_VERSION_MISMATCH)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_LOCAL_RUNTIME_FILE_DOES_NOT_EXIST)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_SET_UP_RUNTIME_FILE_INFORMATION)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_CORE_RUNTIME_FILE_UPDATE)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FILE_SYSTEM_FLASH_MODULE_NOT_SUPPORTED)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_GET_FLASH_MODULE_TYPE)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_FAILED_TO_REBOOT)) ||
        (exceptionType.equals(XrayCameraHardwareExceptionEnum.AXI_TDI_UNSUPPORTED_BOARD_VERSION)))
       _hardwareTaskEngine.throwHardwareException(he);
  }

  /**
   * @author Greg Esparza
   */
  public void reboot() throws XrayTesterException
  {
    boolean rebootOk = false;
    String rebootCommand = _REBOOT_COMMAND;

    try
    {
      if (isSimulationModeOn() == false)
      {
        if (_fileSystemUpdated)
          rebootCommand = "./bin/busybox reboot";

        if (isSimulationModeOn() == false)
          _rExecClientAxi.sendMessage(rebootCommand);

        disconnect();

        // We need to wait a little for the reboot cycle to get started.  Otherwise, if we try connect too
        // quickly after we command the camera to reboot, it might actually connect which would be misleading
        // that the connection was successful.
        waitForRebootStartToComplete();

        int retryCount = 0;
        int numberOfRetries = 2000;

        do
        {
          try
          {
            // If the connect is successful, this means that the application has accepted the connection so it should
            // be running by now.  However, we will do one more check to make sure it is responding to commands.
            connect();
            if (isApplicationRunning() == false)
              _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFailedToRebootException(_cameraId,
                                                                                                                          _serverIpAddress,
                                                                                                                          _portNumber,
                                                                                                                          null));

            rebootOk = true;
          }
          catch (XrayTesterException xte)
          {
            if (retryCount > numberOfRetries)
            {
              XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToRebootException(_cameraId,
                                                                                                                _serverIpAddress,
                                                                                                                _portNumber,
                                                                                                                xte.getLocalizedMessageWithoutHeaderAndFooter());
              he.initCause(xte);
              _hardwareTaskEngine.throwHardwareException(he);
            }
          }

          waitForCommandToComplete(100);
          ++retryCount;
        }
        while ((retryCount < numberOfRetries) && (rebootOk == false));
      }
    }
    catch (XrayCameraHardwareException he)
    {
      handleCriticalErrors(he);
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFailedToRebootException(_cameraId,
                                                                                                                  _serverIpAddress,
                                                                                                                  _portNumber,
                                                                                                                  he.getLocalizedMessageWithoutHeaderAndFooter()));
    }
    catch (XrayTesterException xte)
    {
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFailedToRebootException(_cameraId,
                                                                                                                  _serverIpAddress,
                                                                                                                  _portNumber,
                                                                                                                  xte.getLocalizedMessageWithoutHeaderAndFooter()));
    }
  }

  /**
   * @author George A. David
   */
  private void clearScanProgram() throws XrayTesterException
  {
    _numberOfLineScanImagesToAcquire = 0;
    if (isSimulationModeOn() == false || isUnitTestModeOn())
      sendCommandAndGetReplies(_CLEAR_SCAN_PATH_COMMAND);
  }

  /**
   * @author Greg Esparza
   */
  private void abortAcquisition() throws XrayTesterException
  {
    if (isSimulationModeOn() == false)
      sendCommandAndGetReplies(_ABORT_ACQUISITION_COMMAND);
  }

  /**
   * @author Greg Esparza
   */
  private boolean isSilentPeriodRequired() throws XrayTesterException
  {
    boolean silentPeriodRequired = false;

    if (isAborting() == false)
    {
      if (isSimulationModeOn() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_GET_SILENT_PERIOD_STATE_COMMAND);
        Assert.expect(replies.size() == 1);
        if (replies.get(0).equalsIgnoreCase(_SILENT_PERIOD_REQUIRED))
          silentPeriodRequired = true;
      }
    }

    return silentPeriodRequired;
  }

  /**
   * @author Greg Esparza
   */
   private boolean isImageBufferFree() throws XrayTesterException
   {
     boolean imageBufferFree = false;

     if (isAborting() == false)
     {
       if (isSimulationModeOn() == false)
       {
         List<String> replies = sendCommandAndGetReplies(_GET_FREE_BUFFER_COUNT);
         Assert.expect(replies.size() == 1);

         try
         {
           int numberOfFreeBuffers = StringUtil.convertStringToInt(replies.get(0));
           if (numberOfFreeBuffers > 0)
             imageBufferFree = true;
         }
         catch (BadFormatException bfe)
         {
           XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetFreeImageBufferStatusException(_cameraId,
                                                                                                                               _serverIpAddress,
                                                                                                                               _portNumber,
                                                                                                                               bfe.getLocalizedMessage());
           he.initCause(bfe);
           _hardwareTaskEngine.throwHardwareException(he);
         }
       }
       else
         imageBufferFree = true;
     }

     return imageBufferFree;
  }

  /**
   * @author Greg Esparza
   */
  private void setPreampGain(int gain) throws XrayTesterException
  {
    Assert.expect(gain >= _PREAMP_GAIN_INDEX_0);
    Assert.expect(gain <= _PREAMP_GAIN_INDEX_3);

    if (isAborting())
      return;

    if (isSimulationModeOn() == false)
    {
      List<String> replies = sendCommandAndGetReplies(_SET_PREAMP_GAIN_COMMAND + " " + gain);
      Assert.expect(replies.isEmpty());
    }
  }

  /**
   * @return
   * @throws XrayTesterException
   *
   * @author Anthony Fong
   */
  public double getUserGain() throws XrayTesterException
  {
    double userGain = 0.0;

    if (isSimulationModeOn() == false || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_GET_USER_GAIN_COMMAND);
      Assert.expect(replies.size() == 1);
      try
      {
        userGain = StringUtil.convertStringToDouble(replies.get(0));
      }
      catch (BadFormatException bfe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidTemperatureValueException(_cameraId,
          _serverIpAddress,
          _portNumber,
          bfe.getLocalizedMessage());
        he.initCause(bfe);
        _hardwareTaskEngine.throwHardwareException(he);
      }
    }
    else
    {
      userGain = _SIMULATION_CAMERA_USER_GAIN;
    }

    return userGain;
  }

  /**
   * @param gain
   * @throws XrayTesterException
   *
   * @author Anthony Fong
   */
  public void setUserGain(double gain) throws XrayTesterException
  {
    if (_currentUserGain == gain)
    {
      //skip setUserGain
    }
    else
    {
      Assert.expect(gain >= _USER_GAIN_MIN);
      Assert.expect(gain <= _USER_GAIN_MAX);

      _currentUserGain = gain;
      if (isAborting())
      {
        return;
      }

      if (isSimulationModeOn() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_SET_USER_GAIN_COMMAND + " " + gain);
        Assert.expect(replies.isEmpty());

        if (gain != getUserGain())
        {
          XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetFlashModuleType(_cameraId,
            _serverIpAddress, _portNumber, "User Gain invalid");
          _hardwareTaskEngine.throwHardwareException(he);
        }
      }
    }
  }

  /**
   * @return
   * @throws XrayTesterException
   *
   * @author Anthony Fong
   */
  public double getUserOffset() throws XrayTesterException
  {
    double userOffset = 0.0;

    if (isSimulationModeOn() == false || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_GET_USER_OFFSET_COMMAND );
      Assert.expect(replies.size() == 1);
      try
      {
        userOffset = StringUtil.convertStringToDouble(replies.get(0));
      }
      catch (BadFormatException bfe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidTemperatureValueException(_cameraId,
                                                                                                                   _serverIpAddress,
                                                                                                                   _portNumber,
                                                                                                                   bfe.getLocalizedMessage());
        he.initCause(bfe);
        _hardwareTaskEngine.throwHardwareException(he);
      }
    }
    else
      userOffset = _SIMULATION_CAMERA_USER_OFFSET;

    return userOffset;
  }

  /**
   * @param offset
   * @throws XrayTesterException
   * @author Anthony Fong
   */
  public void setUserOffset(double offset) throws XrayTesterException
  {
    if (_currentUserOffset == offset)
    {
      //skip setUserOffset
    }
    else
    {
      Assert.expect(offset >= _USER_OFFSET_MIN);
      Assert.expect(offset <= _USER_OFFSET_MAX);

      _currentUserOffset = offset;
      if (isAborting())
      {
        return;
      }

      if (isSimulationModeOn() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_SET_USER_OFFSET_COMMAND + " " + offset);
        Assert.expect(replies.isEmpty());

        if (offset != getUserOffset())
        {
          XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetFlashModuleType(_cameraId,
            _serverIpAddress, _portNumber, "User Offset Invalid");
          _hardwareTaskEngine.throwHardwareException(he);
        }
      }
    }
  }

  /**
   * 
   * @param segmentMin
   * @param segmentMax
   * @param pixelMin
   * @param pixelMax
   * @throws XrayTesterException
   */
  private void setCalPointValues(int segmentMin, int segmentMax, int pixelMin, int pixelMax) throws XrayTesterException
  {
    Assert.expect(segmentMin >= 0 && segmentMin <= 255);
    Assert.expect(segmentMax >= 0 && segmentMax <= 255);
    Assert.expect(pixelMin >= 0 && pixelMin <= 255);
    Assert.expect(pixelMax >= 0 && pixelMax <= 255);

    if (isAborting())
      return;

    if (isSimulationModeOn() == false)
    {
      List<String> replies = sendCommandAndGetReplies(_SET_CAL_POINT_VALUES_COMMAND + " " + Integer.toString(segmentMin) + " " + Integer.toString(segmentMax) + " " + Integer.toString(pixelMin) + " " + Integer.toString(pixelMax));
      Assert.expect(replies.isEmpty());
    }
  }
  /**
   * @author Greg Esparza
   */
  private void setTriggerDetectionStateFromCommand(String triggerDetection)
  {
    Assert.expect(triggerDetection != null);

    if (triggerDetection.equalsIgnoreCase(_ENABLE_TRIGGER_DETECTION))
      _triggerDetectionEnabled = true;
    else
      _triggerDetectionEnabled = false;
  }

  /**
   * @author Greg Esparza
   */
  private void setTriggerDetection(String triggerDetection) throws XrayTesterException
  {
    Assert.expect(triggerDetection != null);

    if (isAborting())
      return;

    if (isSimulationModeOn() == false)
    {
      List<String> replies = sendCommandAndGetReplies(_ENABLE_TRIGGER_INPUT_COMMAND + " " + triggerDetection);

      // Note: this command returns the previous setting which is not used.  Instead, the camera should provide a real
      //       command to get the current state.  But, for now, we will just set a data member variable that will keep
      //       track of the last commanded state.
      Assert.expect(replies.size() == 1);
    }

    setTriggerDetectionStateFromCommand(triggerDetection);
  }

  /**
   * @author Greg Esparza
   */
  private void setConfigurationDescription()
  {
    List<String> parameters = new ArrayList<String>();

    parameters.add(_modelType.getName());
    parameters.add(_boardVersion);
    parameters.add(String.valueOf(getId()));
    parameters.add(_serverIpAddress);
    parameters.add(String.valueOf(_portNumber));

    parameters.add(_bootLoaderLocalFilePath);
    parameters.add(_bootLoaderFileName);
    parameters.add(_bootLoaderRemoteTempPath);
    parameters.add(_bootLoaderRemoteRuntimePath);
    parameters.add(_bootLoaderVersion);
    parameters.add(_bootLoaderChecksum);

    parameters.add(_kernelLocalFilePath);
    parameters.add(_kernelFileName);
    parameters.add(_kernelRemoteTempPath);
    parameters.add(_kernelRemoteRuntimePath);
    parameters.add(_kernelVersion);
    parameters.add(_kernelChecksum);

    parameters.add(_fileSystemLocalFilePath);
    parameters.add(_fileSystemFileName);
    parameters.add(_fileSystemRemoteTempPath);
    parameters.add(_fileSystemRemoteRuntimePath);
    parameters.add(_fileSystemVersion);
    parameters.add(_fileSystemChecksum);

    parameters.add(_applicationLocalFilePath);
    parameters.add(_applicationFileName);
    parameters.add(_applicationRemoteTempPath);
    parameters.add(_applicationRemoteRuntimePath);
    parameters.add(_applicationVersion);
    parameters.add(_applicationChecksum);

    parameters.add(_driverLocalFilePath);
    parameters.add(_driverFileName);
    parameters.add(_driverRemoteTempPath);
    parameters.add(_driverRemoteRuntimePath);
    parameters.add(_driverVersion);
    parameters.add(_driverChecksum);

    parameters.add(_fpgaLocalFilePath);
    parameters.add(_fpgaFileName);
    parameters.add(_fpgaRemoteTempPath);
    parameters.add(_fpgaRemoteRuntimePath);
    parameters.add(_fpgaVersion);
    parameters.add(_fpgaChecksum);

    parameters.add(_rcshFileLocalFilePath);
    parameters.add(_rcshFileFileName);
    parameters.add(_rcshFileRemoteTempPath);
    parameters.add(_rcshFileRemoteRuntimePath);
    parameters.add(_rcshFileVersion);
    parameters.add(_rcshFileChecksum);

    _configDescription.clear();
    _configDescription.add(new HardwareConfigurationDescription("HW_AXI_TYPE1_VERSION_A_XRAY_CAMERA_CONFIGURATION_KEY", parameters));
  }

  /**
   * @author Greg Esparza
   */
  private AxiTdiXrayCameraImageInformation getImageInformation() throws XrayTesterException
  {
    String imageInformationStr = "";
    int retryCount = 1;
    int numberOfRetries = 150;
    AxiTdiXrayCameraImageInformation xRayCameraImageInformation = new AxiTdiXrayCameraImageInformation();

    do
    {
      if (isAborting())
        return null;

      if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
      {
        List<String> replies = sendCommandAndGetReplies(_GET_IMAGE_INFORMATION_COMMAND);
        Assert.expect(replies.size() == 1);
        imageInformationStr = (String)replies.get(0);
      }
      else
        imageInformationStr = _SIMULATION_IMAGE_INFORMATION;

      try
      {
        xRayCameraImageInformation.set(imageInformationStr);
      }
      catch (XrayCameraHardwareException xche)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetImageInformationException(_cameraId,
                                                                                                                       _serverIpAddress,
                                                                                                                       _portNumber,
                                                                                                                       xche.getLocalizedMessageWithoutHeaderAndFooter());
        he.initCause(xche);
        _hardwareTaskEngine.throwHardwareException(he);
      }

      ++retryCount;
      waitForCommandToCompleteInMilliseconds(100);
    }
    while ((xRayCameraImageInformation.isImageAvailable() == false) && (retryCount <= numberOfRetries));

    if (retryCount > numberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiTimedOutWaitingForImageInformationException(_cameraId,
                                                                                                                                      _serverIpAddress,
                                                                                                                                      _portNumber));

    return xRayCameraImageInformation;
  }

  /**
   * @author George A. David
   */
  private double getTemperatureInDegreesFahrenheit() throws XrayTesterException
  {
    double degrees = 0.0;

    if (isSimulationModeOn() == false || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_READ_TEMPERATURE_COMMAND);
      Assert.expect(replies.size() == 1);
      try
      {
        degrees = StringUtil.convertStringToDouble(replies.get(0));
      }
      catch (BadFormatException bfe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidTemperatureValueException(_cameraId,
                                                                                                                   _serverIpAddress,
                                                                                                                   _portNumber,
                                                                                                                   bfe.getLocalizedMessage());
        he.initCause(bfe);
        _hardwareTaskEngine.throwHardwareException(he);
      }
    }
    else
      degrees = _SIMULATION_CAMERA_TEMPERATURE_IN_FAHRENHEIT;

    return degrees;
  }

  /**
   * @author Greg Esparza
   */
  private void setTriggerSource(String triggerSource) throws XrayTesterException
  {
    Assert.expect(triggerSource != null);

    if (isSimulationModeOn() == false)
    {
      List<String> replies = sendCommandAndGetReplies(_SET_TRIGGER_SOURCE_COMMAND + " " + triggerSource);
      Assert.expect(replies.isEmpty());
    }
  }
  /**
   * @author George A. David
   */
  private void stopSimulatedTriggers() throws XrayTesterException
  {
    setTriggerSource(_DISABLE_SIMULATED_TRIGGERS);
  }

  /**
   * @author George A. David
   */
  private void setMedianFilter(int enabled) throws XrayTesterException
  {
    if (isAborting())
      return;

    try
    {
      if (isSimulationModeOn() == false || isUnitTestModeOn())
      {
        List<String> replies = sendCommandAndGetReplies(_SET_MEDIAN_FILTER_COMMAND + " " + enabled);
        Assert.expect(replies.isEmpty());
      }
    }
    catch (XrayTesterException xte)
    {
      _hardwareTaskEngine.throwHardwareException(xte);
    }
  }

  /**
   * @author George A. David
   */
  private void setPixelClockMode(String pixelClockMode) throws XrayTesterException
  {
    Assert.expect(pixelClockMode != null);

    try
    {
      if (isSimulationModeOn() == false || isUnitTestModeOn())
      {
        List<String> replies = sendCommandAndGetReplies(_SET_PIXEL_CLOCK_SOURCE_COMMAND + " " + pixelClockMode);
        Assert.expect(replies.size() == 0);
      }
    }
    catch (XrayTesterException xte)
    {
      _hardwareTaskEngine.throwHardwareException(xte);
    }
  }

  /**
   * @author Greg Esparza
   */
  private String getBootLoaderVersion() throws XrayTesterException
  {
    String bootLoaderVersion = "";

    if (isSimulationModeOn() == false)
      bootLoaderVersion =_rExecClientAxi.sendMessageAndReceiveReply(_GET_BOOTLOADER_VERSION_COMMAND);
    else
      bootLoaderVersion = _bootLoaderVersion;

    return bootLoaderVersion;
  }

  /**
   * @author Greg Esparza
   */
  private String getKernelVersion() throws XrayTesterException
  {
    String kernelVersion = "";

    if (isSimulationModeOn() == false)
      kernelVersion =_rExecClientAxi.sendMessageAndReceiveReply(_GET_KERNEL_VERSION_COMMAND);
    else
      kernelVersion = _kernelVersion;

    return kernelVersion;
  }

  /**
   * @author Greg Esparza
   */
  private String getFileSystemVersion() throws XrayTesterException
  {
    String fileSystemVersion = "";

    if (isSimulationModeOn() == false)
      fileSystemVersion = _rExecClientAxi.sendMessageAndReceiveReply(_GET_FILESYSTEM_VERSION_COMMAND);
    else
      fileSystemVersion = _fileSystemVersion;

    return fileSystemVersion;
  }

  /**
   * @author Greg Esparza
   */
  private String getApplicationVersion() throws XrayTesterException
  {
    String applicationVersion = "";

    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_GET_APPLICATION_VERSION_COMMAND);
      Assert.expect(replies.size() == 1);
      applicationVersion = replies.get(0);
    }
    else
      applicationVersion = _applicationVersion;

    return applicationVersion;
  }

  /**
   * @author Greg Esparza
   */
  private String getDriverVersion() throws XrayTesterException
  {
    String driverVersion = "";

    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_GET_DRIVER_VERSION_COMMAND);
      Assert.expect(replies.size() == 1);
      driverVersion = replies.get(0);
    }
    else
      driverVersion = _driverVersion;

    return driverVersion;
  }

  /**
   * @author Greg Esparza
   */
  private String getFpgaVersion() throws XrayTesterException
  {
    String fpgaVersion = "";

    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      List<String> replies = sendCommandAndGetReplies(_GET_FPGA_VERSION_COMMAND);
      Assert.expect(replies.size() == 1);
      fpgaVersion = replies.get(0);
    }
    else
      fpgaVersion = _fpgaVersion;

    return fpgaVersion;
  }

  /**
   * The MD5 checksum is also used as the version for regular files in the file system
   * which do not have a separate versioning mechanism, such as rc.sh.
   * @author Kay Lannen
   */
  private String getRcshFileVersion() throws XrayTesterException
  {
    String fileVersion = "";

    if ((isSimulationModeOn() == false) || (isUnitTestModeOn()))
    {
      String filePath = _rcshFileRemoteRuntimePath + _rcshFileFileName;
      String remoteMD5 = _rExecClientAxi.sendMessageAndReceiveReply(_GENERATE_MD5_CHECKSUM_COMMAND + filePath);
      StringTokenizer st = new StringTokenizer(remoteMD5);
      fileVersion = st.nextToken();
    }
    else
      fileVersion = _rcshFileVersion;

    return fileVersion;
  }


  /**
   * @author Greg Esparza
   */
  private void verifyAllRuntimeFileVersions() throws XrayTesterException
  {
    verifyBootLoaderVersion();
    verifyKernelVersion();
    verifyFileSystemVersion();
    verifyApplicationVersion();
    verifyDriverVersion();
    verifyFpgaVersion();
  }

  /**
   * @author Greg Esparza
   */
  private void runtimeFileUpdateSetup() throws XrayTesterException
  {
    _tftpClientAxi.close();
    _tftpClientAxi.open();
    _rebootRequired = false;
  }

  /**
   * @author Greg Esparza
   */
  private void runtimeFileUpdateCleanup() throws XrayTesterException
  {
    if (_rebootRequired)
    {
      TimerUtil timerUtil = new TimerUtil();
      timerUtil.reset();
      timerUtil.start();

      reboot();

      _xRayCameraLogUtil.log("Runtime file clean up reboot time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
      //verifyAllRuntimeFileVersions();
    }

    _rebootRequired = false;
    _tftpClientAxi.close();
  }

  /**
   * @author Greg Esparza
   */
  private void waitForCommandToComplete(int waitTimeInMilliseconds)
  {
    Assert.expect(waitTimeInMilliseconds >= 0);

    try
    {
      Thread.sleep(waitTimeInMilliseconds);
    }
    catch (InterruptedException ie)
    {
      // do nothing
    }
  }

  /**
   * @author Greg Esparza
   */
  private void waitForRebootStartToComplete()
  {
     waitForCommandToComplete(_REBOOT_START_TIME_IN_MILLISECONDS);
  }

  /**
   * @author Greg Esparza
   */
  private boolean isApplicationRunning()
  {
    boolean applicationRunning = false;

    try
    {
      String version = getApplicationVersion();
      if ((version != null) && (version.length() > 0))
        applicationRunning = true;
    }
    catch (XrayTesterException xte)
    {
      // do nothing
    }

    return applicationRunning;
  }

  /**
   * @author Greg Esparza
   */
  private void waitForApplicationToStart() throws XrayTesterException
  {
    boolean applicationRunning = false;
    int numberOfRetries = 50;
    int  retryCount = 0;

    do
    {
      applicationRunning = isApplicationRunning();
      waitForCommandToComplete(100);
      ++retryCount;
    }
    while ((applicationRunning == false) && (retryCount < numberOfRetries));

    if (retryCount >= numberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiApplicationFailedToStartException(_cameraId,
                                                                                                                            _serverIpAddress,
                                                                                                                            _portNumber));
  }

  /**
   * @author Greg Esparza
   */
  private void waitForApplicationToStop() throws XrayTesterException
  {
    boolean applicationRunning = true;
    int numberOfRetries = 50;
    int  retryCount = 0;

    do
    {
      applicationRunning = isApplicationRunning();
      waitForCommandToComplete(100);
      ++retryCount;
    }
    while ((applicationRunning) && (retryCount < numberOfRetries));

    if (retryCount >= numberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiApplicationFailedToStopException(_cameraId,
                                                                                                                           _serverIpAddress,
                                                                                                                           _portNumber));
  }

  /**
   * @author Greg Esparza
   */
  private void waitForDriverToStart() throws XrayTesterException
  {
    boolean driverRunning = false;
    int numberOfRetries = 50;
    int  retryCount = 0;

    do
    {
      driverRunning = isDriverRunning();
      waitForCommandToComplete(100);
      ++retryCount;
    }
    while ((driverRunning == false) && (retryCount < numberOfRetries));

    if (retryCount >= numberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiDriverFailedToStartException(_cameraId,
                                                                                                                       _serverIpAddress,
                                                                                                                       _portNumber));
  }

  /**
   * @author Greg Esparza
   */
  private void waitForDriverToStop() throws XrayTesterException
  {
    boolean driverRunning = false;
    int numberOfRetries = 50;
    int  retryCount = 0;

    do
    {
      driverRunning = isDriverRunning();
      waitForCommandToComplete(100);
      ++retryCount;
    }
    while ((driverRunning) && (retryCount < numberOfRetries));

    if (retryCount >= numberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiDriverFailedToStartException(_cameraId,
                                                                                                                       _serverIpAddress,
                                                                                                                       _portNumber));
  }

  /**
   * @author Greg Esparza
   */
  private void startApplication() throws XrayTesterException
  {
    if (isApplicationRunning() == false)
    {
      // Now, when we change the attributes to enable execution the kernel will start the application.
      // A reboot is not required.
      String applicationRemoteRuntimeFileNameFullPath = _applicationRemoteRuntimePath + _applicationFileName;
      setRemoteRuntimeFileAttributes(applicationRemoteRuntimeFileNameFullPath, "a+x");
      waitForApplicationToStart();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void stopApplication() throws XrayTesterException
  {
    if (isApplicationRunning())
    {
      // Set the file so it cannot execute
      String applicationRemoteRuntimeFileNameFullPath = _applicationRemoteRuntimePath + _applicationFileName;
      setRemoteRuntimeFileAttributes(applicationRemoteRuntimeFileNameFullPath, "a-x");

      // Now, stop it from running
      String command = "kill `pidof " + _applicationFileName + "`";
      _rExecClientAxi.sendMessage(command);
      waitForApplicationToStop();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void removeRemoteRuntimeFile(String remoteFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(remoteFileNameFullPath != null);

    String command = "rm";
    _rExecClientAxi.sendMessage(command + " " + remoteFileNameFullPath);
  }

  /**
   * @author Greg Esparza
   */
  private void startDriver() throws XrayTesterException
  {
    if (isDriverRunning() == false)
    {
      // The driver is now in the runtime location so we can safely start it up.
      String driverRemoteRuntimeFileNameFullPath = _driverRemoteRuntimePath + _driverFileName;
      setRemoteRuntimeFileAttributes(driverRemoteRuntimeFileNameFullPath, "a-w+x");

      String command = "./sbin/insmod";
      _rExecClientAxi.sendMessage(command + " " + driverRemoteRuntimeFileNameFullPath);
      waitForDriverToStart();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void stopDriver() throws XrayTesterException
  {
    if (isDriverRunning())
    {
      String driverRemoteRuntimeFileNameFullPath = _driverRemoteRuntimePath + _driverFileName;
      String command = "./sbin/rmmod";
      _rExecClientAxi.sendMessage(command + " " + driverRemoteRuntimeFileNameFullPath);
      waitForDriverToStop();
    }
  }

  /**
   * @author Greg Esparza
   */
  private boolean isDriverRunning() throws XrayTesterException
  {
    boolean driverRunning = false;

    String driverName = FileUtilAxi.getNameWithoutExtension(_driverFileName);
    String command = "/sbin/lsmod";
    String status = _rExecClientAxi.sendMessageAndReceiveReply(command);
    if (status.contains(driverName))
      driverRunning = true;

    return driverRunning;
  }
  /**
   * @author Greg Esparza
   */
  private void createRemoteRuntimeFile(String remoteFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(remoteFileNameFullPath != null);

    String command = "touch ";
    _rExecClientAxi.sendMessage(command + remoteFileNameFullPath);
  }

  /**
   * @author Greg Esparza
   */
  private void setRemoteRuntimeFileAttributes(String remoteFileNameFullPath, String attributes) throws XrayTesterException
  {
    Assert.expect(remoteFileNameFullPath != null);
    Assert.expect(attributes != null);

    String command = "chmod";
    _rExecClientAxi.sendMessage(command + " " + attributes + " " + remoteFileNameFullPath);
  }

  /**
   * @author Greg Esparza
   */
  private void remoteRuntimeFileCopy(String sourceRemoteFileNameFullPath, String destinationRemoteFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(sourceRemoteFileNameFullPath != null);
    Assert.expect(destinationRemoteFileNameFullPath != null);

    String command = "cp";
    _rExecClientAxi.sendMessage(command + " " + sourceRemoteFileNameFullPath + " " + destinationRemoteFileNameFullPath);
  }

  /**
   * @author Greg Esparza
   */
  private void verifyRemoteFileChecksum(String remoteFileNameFullPath, String expectedChecksumValue) throws XrayTesterException
  {
    Assert.expect(remoteFileNameFullPath != null);
    Assert.expect(expectedChecksumValue != null);

    String command = "sh /opt/getChecksum.sh ";
    String actualChecksumValue = _rExecClientAxi.sendMessageAndReceiveReply(command + remoteFileNameFullPath);
    if (expectedChecksumValue.equalsIgnoreCase(actualChecksumValue) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidRuntimeFileChecksumException(_cameraId,
                                                                                                                              _serverIpAddress,
                                                                                                                              _portNumber,
                                                                                                                              remoteFileNameFullPath,
                                                                                                                              actualChecksumValue,
                                                                                                                              expectedChecksumValue));
  }

  /**
   * @author Greg Esparza
   */
  private void updateFlashMemoryAndVerify(String localFileNameFullPath,
                                          String sourceRemoteFileNameFullPath,
                                          String destinationRemoteFileNameFullPath,
                                          String expectedChecksumValue) throws XrayTesterException
  {
    long fileSizeInBytes = FileUtil.getFileSizeInBytes(localFileNameFullPath);

    remoteRuntimeFileCopy(sourceRemoteFileNameFullPath, destinationRemoteFileNameFullPath);
    removeRemoteRuntimeFile(sourceRemoteFileNameFullPath);
    String command = "dd bs=" + String.valueOf(fileSizeInBytes) + " count=1";
    String remoteSourceFileCommand = "if=" + destinationRemoteFileNameFullPath;
    String remoteDestinationFileCommand = "of=" + sourceRemoteFileNameFullPath ;
    try
    {
      _rExecClientAxi.sendMessage(command + " " + remoteSourceFileCommand + " " + remoteDestinationFileCommand);
    }
    catch (XrayTesterException xte)
    {
      // do nothing
    }
    verifyRemoteFileChecksum(sourceRemoteFileNameFullPath, expectedChecksumValue);
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateBootLoader() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String bootLoaderRemoteTempFileNameFullPath = _bootLoaderRemoteTempPath + _bootLoaderFileName;
    String bootLoaderLocalFileNameFullPath = _bootLoaderLocalFilePath + _bootLoaderFileName;
    String bootLoaderRemoteRuntimeFileNameFullPath = bootLoaderRemoteRuntimeFileNameFullPath = _bootLoaderRemoteRuntimePath + "mtdblock0";

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading Boot Loader...\n" +
                                "sourceFile: " + bootLoaderLocalFileNameFullPath + "\n" +
                                "destFile: " + bootLoaderRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {
        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(bootLoaderRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(bootLoaderRemoteTempFileNameFullPath, "a+w");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(bootLoaderLocalFileNameFullPath, bootLoaderRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(bootLoaderRemoteTempFileNameFullPath, _bootLoaderChecksum);
          // Copy the file from the camera's temporary location to the final, runtime location.
          updateFlashMemoryAndVerify(bootLoaderLocalFileNameFullPath,
            bootLoaderRemoteTempFileNameFullPath,
            bootLoaderRemoteRuntimeFileNameFullPath,
            _bootLoaderChecksum);

          updateOk = true;
          _rebootRequired = true;
        }
        catch (XrayTesterException xte)
        {
          if (retryCount > numberOfRetries)
          {
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateCoreRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              bootLoaderRemoteTempFileNameFullPath,
              bootLoaderLocalFileNameFullPath,
              bootLoaderRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));
          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("Boot Loader update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateKernel() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String kernelRemoteTempFileNameFullPath = _kernelRemoteTempPath + _kernelFileName;
    String kernelLocalFileNameFullPath = _kernelLocalFilePath + _kernelFileName;;
    String kernelRemoteRuntimeFileNameFullPath = _kernelRemoteRuntimePath + "mtdblock1";

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading Kernel...\n" +
                                "sourceFile: " + kernelLocalFileNameFullPath + "\n" +
                                "destFile: " + kernelRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {

        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(kernelRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(kernelRemoteTempFileNameFullPath, "a+w");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(kernelLocalFileNameFullPath, kernelRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(kernelRemoteTempFileNameFullPath, _kernelChecksum);
          // Copy the file from the camera's temporary location to the final, runtime location.
          updateFlashMemoryAndVerify(kernelLocalFileNameFullPath,
            kernelRemoteTempFileNameFullPath,
            kernelRemoteRuntimeFileNameFullPath,
            _kernelChecksum);

          updateOk = true;
          _rebootRequired = true;
        }
        catch (XrayTesterException xte)
        {
          if (retryCount > numberOfRetries)
          {
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateCoreRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              kernelRemoteTempFileNameFullPath,
              kernelLocalFileNameFullPath,
              kernelRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));

          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("Kernel update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateFileSystem() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String fileSystemRemoteTempFileNameFullPath = _fileSystemRemoteTempPath + _fileSystemFileName;
    String fileSystemLocalFileNameFullPath = _fileSystemLocalFilePath + _fileSystemFileName;
    String fileSystemRemoteRuntimeFileNameFullPath = _fileSystemRemoteRuntimePath + _fileSystemFileName;

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading File System...\n" +
                                "sourceFile: " + fileSystemLocalFileNameFullPath + "\n" +
                                "destFile: " + fileSystemRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {
          stopApplication();
          stopDriver();
          _fileSystemUpdated = false;

        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(fileSystemRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(fileSystemRemoteTempFileNameFullPath, "a+w-x");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(fileSystemLocalFileNameFullPath, fileSystemRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(fileSystemRemoteTempFileNameFullPath, _fileSystemChecksum);
          // Complete the install of the file system
          String command = "/opt/writeFStoRAM w";
          _rExecClientAxi.sendMessage(command + " " + fileSystemRemoteTempFileNameFullPath);

          _fileSystemUpdated = true;
          reboot();
          _tftpClientAxi.open();
          updateOk = true;
        }
        catch (XrayCameraHardwareException he)
        {
          handleCriticalErrors(he);

          if (retryCount >= numberOfRetries)
          {
            _fileSystemUpdated = false;
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateCoreRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              fileSystemRemoteTempFileNameFullPath,
              fileSystemLocalFileNameFullPath,
              fileSystemRemoteRuntimeFileNameFullPath,
              he.getLocalizedMessage()));
          }
        }
        catch (XrayTesterException xte)
        {
          if (retryCount >= numberOfRetries)
          {
            _fileSystemUpdated = false;
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateCoreRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              fileSystemRemoteTempFileNameFullPath,
              fileSystemLocalFileNameFullPath,
              fileSystemRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));
          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("File System update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * @author Greg Esparza
   */
  private void updateApplicationAndDriver() throws XrayTesterException
  {
    if (isSimulationModeOn() == false)
    {
      stopApplication();
      stopDriver();
      updateApplication();
      updateDriver();
      startDriver();
      startApplication();
    }
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateApplication() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String applicationRemoteTempFileNameFullPath = _applicationRemoteTempPath + _applicationFileName;
    String applicationLocalFileNameFullPath = _applicationLocalFilePath + _applicationFileName;
    String applicationRemoteRuntimeFileNameFullPath = _applicationRemoteRuntimePath + _applicationFileName;

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading Application...\n" +
                                "sourceFile: " + applicationLocalFileNameFullPath + "\n" +
                                "destFile: " + applicationRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {
          removeRemoteRuntimeFile(applicationRemoteRuntimeFileNameFullPath);
        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(applicationRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(applicationRemoteTempFileNameFullPath, "a+w-x");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(applicationLocalFileNameFullPath, applicationRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(applicationRemoteTempFileNameFullPath, _applicationChecksum);
          // Copy the file from the camera's temporary location to the final, runtime location.
          remoteRuntimeFileCopy(applicationRemoteTempFileNameFullPath, applicationRemoteRuntimeFileNameFullPath);
          // We will start the application so a reboot is not required

          updateOk = true;
        }
        catch (XrayTesterException xte)
        {
          if (retryCount > numberOfRetries)
          {
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              applicationRemoteTempFileNameFullPath,
              applicationLocalFileNameFullPath,
              applicationRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));
          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("Application update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateDriver() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String driverRemoteTempFileNameFullPath = _driverRemoteTempPath + _driverFileName;
    String driverLocalFileNameFullPath = _driverLocalFilePath + _driverFileName;
    String driverRemoteRuntimeFileNameFullPath = _driverRemoteRuntimePath + _driverFileName;

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading Driver...\n" +
                                "sourceFile: " + driverLocalFileNameFullPath + "\n" +
                                "destFile: " + driverRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {
        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(driverRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(driverRemoteTempFileNameFullPath, "a+w");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(driverLocalFileNameFullPath, driverRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(driverRemoteTempFileNameFullPath, _driverChecksum);
          // Copy the file from the camera's temporary location to the final, runtime location.
          remoteRuntimeFileCopy(driverRemoteTempFileNameFullPath, driverRemoteRuntimeFileNameFullPath);

          updateOk = true;
        }
        catch (XrayTesterException xte)
        {
          if (retryCount > numberOfRetries)
          {
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              driverRemoteTempFileNameFullPath,
              driverLocalFileNameFullPath,
              driverRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));
          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("Driver update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   * @author Greg Esparza
   */
  private void updateFpga() throws XrayTesterException
  {
    boolean updateOk = false;
    int numberOfRetries = 1;
    String fpgaRemoteTempFileNameFullPath = _fpgaRemoteTempPath + _fpgaFileName;
    String fpgaLocalFileNameFullPath = _fpgaLocalFilePath + _fpgaFileName;
    String fpgaRemoteRuntimeFileNameFullPath = _fpgaRemoteRuntimePath + _fpgaFileName;

    int retryCount = 1;

    TimerUtil timerUtil = new TimerUtil();
    timerUtil.reset();
    timerUtil.start();

    try
    {
      _upgradeFirmwareMessageList.add("Upgrading FPGA...\n" +
                                "sourceFile: " + fpgaLocalFileNameFullPath + "\n" +
                                "destFile: " + fpgaRemoteRuntimeFileNameFullPath + "\n" +
                                "=====================================================\n");
      _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
      do
      {
        try
        {
          stopApplication();
          stopDriver();
        // The TFTP server is not smart enough to copy new, non-existing file on the camera
          // so we have to create a dummy one just to get started.  Also, we need to set its
          // attributes for full read/write priviledges.
          createRemoteRuntimeFile(fpgaRemoteTempFileNameFullPath);
          setRemoteRuntimeFileAttributes(fpgaRemoteTempFileNameFullPath, "a+w-x");
          // Send the file from the local machine to a temporary location on the camera.
          _tftpClientAxi.sendFile(fpgaLocalFileNameFullPath, fpgaRemoteTempFileNameFullPath);
          // Make sure that the file did not get corrupted during the file transfer.
          verifyRemoteFileChecksum(fpgaRemoteTempFileNameFullPath, _fpgaChecksum);
          // Complete the install of the fpga
          String command = "/opt/playxsvf";
          _rExecClientAxi.sendMessage(command + " " + fpgaRemoteTempFileNameFullPath);

          _rebootRequired = true;
          updateOk = true;
        }
        catch (XrayTesterException xte)
        {
          if (retryCount > numberOfRetries)
          {
            _rebootRequired = false;
            runtimeFileUpdateCleanup();
            _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateRuntimeFileException(_cameraId,
              _serverIpAddress,
              _portNumber,
              fpgaRemoteTempFileNameFullPath,
              fpgaLocalFileNameFullPath,
              fpgaRemoteRuntimeFileNameFullPath,
              xte.getLocalizedMessageWithoutHeaderAndFooter()));
          }
        }

        ++retryCount;
      }
      while (updateOk == false);
    }
    finally
    {
      _upgradeFirmwareMessageList.add("");
      _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPDATE_FIRMWARE);
    }

    timerUtil.stop();
    _xRayCameraLogUtil.log("FPGA update time: ", timerUtil.getElapsedTimeInMillis() + " milliseconds");
  }

  /**
   * @author Greg Esparza
   */
  private void verifyBootLoaderVersion() throws XrayTesterException
  {
    String expectedVersion = _bootLoaderVersion;
    String actualVersion = getBootLoaderVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidBootLoaderVersionException(_cameraId,
                                                                                                                            _serverIpAddress,
                                                                                                                            _portNumber,
                                                                                                                            actualVersion,
                                                                                                                            expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyBootLoader() throws XrayTesterException
  {
    try
    {
      verifyBootLoaderVersion();
    }
    catch (XrayTesterException xte)
    {
      if (isSimulationModeOn() == false)
        updateBootLoader();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyKernelVersion() throws XrayTesterException
  {
    String expectedVersion = _kernelVersion;
    String actualVersion = getKernelVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidKernelVersionException(_cameraId,
                                                                                                                        _serverIpAddress,
                                                                                                                        _portNumber,
                                                                                                                        actualVersion,
                                                                                                                        expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyKernel() throws XrayTesterException
  {
    try
    {
      verifyKernelVersion();
    }
    catch (XrayTesterException xte)
    {
      if (isSimulationModeOn() == false)
        updateKernel();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyFileSystemVersion() throws XrayTesterException
  {
    String expectedVersion = _fileSystemVersion;
    String actualVersion = getFileSystemVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidFileSystemVersionException(_cameraId,
                                                                                                                        _serverIpAddress,
                                                                                                                        _portNumber,
                                                                                                                        actualVersion,
                                                                                                                        expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyFileSystem() throws XrayTesterException
  {
    try
    {
      verifyFileSystemVersion();
    }
    catch (XrayTesterException xte)
    {
      if (isSimulationModeOn() == false)
        updateFileSystem();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyApplicationVersion() throws XrayTesterException
  {
    String expectedVersion = _applicationVersion;
    String actualVersion = getApplicationVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidApplicationVersionException(_cameraId,
                                                                                                                             _serverIpAddress,
                                                                                                                             _portNumber,
                                                                                                                             actualVersion,
                                                                                                                             expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyApplication() throws XrayTesterException
  {
    try
    {
      verifyApplicationVersion();
    }
    catch (XrayTesterException xte)
    {
      // If we failed getting the version information, then the application is either
      // not installed or not running.  So, we should just force an update.
      updateApplicationAndDriver();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyDriverVersion() throws XrayTesterException
  {
    String expectedVersion = _driverVersion;
    String actualVersion = getDriverVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidDriverVersionException(_cameraId,
                                                                                                                        _serverIpAddress,
                                                                                                                        _portNumber,
                                                                                                                        actualVersion,
                                                                                                                        expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyDriver() throws XrayTesterException
  {
    try
    {
      verifyDriverVersion();
    }
    catch (XrayTesterException xte)
    {
      // If we failed getting the version information, then the application is either
      // not installed or not running.  So, we should just force an update.
      updateApplicationAndDriver();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyFpgaVersion() throws XrayTesterException
  {
    String expectedVersion = _fpgaVersion;
    String actualVersion = getFpgaVersion();
    if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidFpgaVersionException(_cameraId,
                                                                                                                      _serverIpAddress,
                                                                                                                      _portNumber,
                                                                                                                      actualVersion,
                                                                                                                      expectedVersion));
  }

  /**
   * @author Greg Esparza
   */
  private void verifyFpga() throws XrayTesterException
  {
    boolean fpgaUpdateRequired = false;
    boolean applicationAndDriverUpdateRequired = false;

    try
    {
      if (isApplicationRunning() == false)
      {
        applicationAndDriverUpdateRequired = true;
        fpgaUpdateRequired = true;
      }
      else
      {
        verifyFpgaVersion();
      }
    }
    catch (XrayTesterException xte)
    {
      fpgaUpdateRequired = true;
    }

    if (applicationAndDriverUpdateRequired)
      updateApplicationAndDriver();

    if (fpgaUpdateRequired)
    {
      if (isSimulationModeOn() == false)
        updateFpga();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyLocalRuntimeFileNameFullPath(String localRuntimeFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(localRuntimeFileNameFullPath != null);

    if (FileUtilAxi.exists(localRuntimeFileNameFullPath) == false)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiLocalRuntimeFileDoesNotExistException(_cameraId,
                                                                                                                                _serverIpAddress,
                                                                                                                                _portNumber,
                                                                                                                                localRuntimeFileNameFullPath));
  }

  /**
   * @author Greg Esparza
   */
  private void setBootLoaderInformation() throws XrayTesterException
  {
    String bootLoaderInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_BOOTLOADER_KEY_NAME);

    String[] bootLoaderInformationValues = bootLoaderInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(bootLoaderInformationValues.length == 5);

    _bootLoaderLocalFilePath = _runtimeInformationDirectory;
    _bootLoaderFileName = bootLoaderInformationValues[0].trim();
    _bootLoaderRemoteTempPath = bootLoaderInformationValues[1].trim();
    _bootLoaderRemoteRuntimePath = bootLoaderInformationValues[2].trim();
    _bootLoaderVersion = bootLoaderInformationValues[3].trim();
    _bootLoaderChecksum = bootLoaderInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_bootLoaderLocalFilePath + _bootLoaderFileName);
  }

  /**
   * @author Greg Esparza
   */
  private void setKernelInformation() throws XrayTesterException
  {
    String kernelInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_KERNEL_KEY_NAME);

    String[] kernelInformationValues = kernelInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(kernelInformationValues.length == 5);

    _kernelLocalFilePath = _runtimeInformationDirectory;
    _kernelFileName = kernelInformationValues[0].trim();
    _kernelRemoteTempPath = kernelInformationValues[1].trim();
    _kernelRemoteRuntimePath = kernelInformationValues[2].trim();
    _kernelVersion = kernelInformationValues[3].trim();
    _kernelChecksum = kernelInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_kernelLocalFilePath + _kernelFileName);
  }

  /**
   * @author Greg Esparza
   */
  private String getFlashModuleType() throws XrayTesterException
  {
    String flashModuleType = "";

    try
    {
      if (isSimulationModeOn() == false)
      {
        String command = "cat /proc/mtd | awk {'print $3'} | tail -n 1";
        flashModuleType = _rExecClientAxi.sendMessageAndReceiveReply(command);
      }
      else
        flashModuleType = _SIMULATION_FLASH_MODULE_TYPE;
    }
    catch (XrayTesterException xte)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetFlashModuleType(_cameraId,
                                                                                                           _serverIpAddress,
                                                                                                           _portNumber,
                                                                                                           xte.getLocalizedMessageWithoutHeaderAndFooter());
      he.initCause(xte);
      throw he;
    }

    return flashModuleType;
  }

  /**
   * @author Greg Esparza
   */
  private void setFileSystemInformation() throws XrayTesterException
  {
    String actualFlashModuleType = getFlashModuleType();
    String fileSystemInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_FILESYSTEM_KEY_NAME + "_" + actualFlashModuleType);
    if (fileSystemInformation == null)
    {
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFileSystemFlashModuleNotSupportedException(_cameraId,
                                                                                                                                     _serverIpAddress,
                                                                                                                                     _portNumber,
                                                                                                                                     actualFlashModuleType));

    }


    String[] fileSystemInformationValues = fileSystemInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(fileSystemInformationValues.length == 5);

    _fileSystemLocalFilePath = _runtimeInformationDirectory;

    _fileSystemFileName = fileSystemInformationValues[0].trim();
    _fileSystemRemoteTempPath = fileSystemInformationValues[1].trim();
    _fileSystemRemoteRuntimePath = fileSystemInformationValues[2].trim();
    _fileSystemVersion = fileSystemInformationValues[3].trim();
    _fileSystemChecksum = fileSystemInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_fileSystemLocalFilePath + _fileSystemFileName);
  }

  /**
   * @author Greg Esparza
   */
  private void setApplicationInformation() throws XrayTesterException
  {
    String applicationInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_APPLICATION_KEY_NAME);

    String[] applicationInformationValues = applicationInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(applicationInformationValues.length == 5);

    _applicationLocalFilePath = _runtimeInformationDirectory;
    _applicationFileName = applicationInformationValues[0].trim();
    _applicationRemoteTempPath = applicationInformationValues[1].trim();
    _applicationRemoteRuntimePath = applicationInformationValues[2].trim();
    _applicationVersion = applicationInformationValues[3].trim();
    _applicationChecksum = applicationInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_applicationLocalFilePath + _applicationFileName);
  }

  /**
   * @author Greg Esparza
   */
  private void setDriverInformation() throws XrayTesterException
  {
    String driverInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_DRIVER_KEY_NAME);

    String[] driverInformationValues = driverInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(driverInformationValues.length == 5);

    _driverLocalFilePath = _runtimeInformationDirectory;
    _driverFileName = driverInformationValues[0].trim();
    _driverRemoteTempPath = driverInformationValues[1].trim();
    _driverRemoteRuntimePath = driverInformationValues[2].trim();
    _driverVersion = driverInformationValues[3].trim();
    _driverChecksum = driverInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_driverLocalFilePath + _driverFileName);
  }

  /**
   * @author Greg Esparza
   */
  private void setFpgaInformation() throws XrayTesterException
  {
    String fpgaInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_FPGA_KEY_NAME);

    String[] fpgaInformationValues = fpgaInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(fpgaInformationValues.length == 5);

    _fpgaLocalFilePath = _runtimeInformationDirectory;
    _fpgaFileName = fpgaInformationValues[0].trim();
    _fpgaRemoteTempPath = fpgaInformationValues[1].trim();
    _fpgaRemoteRuntimePath = fpgaInformationValues[2].trim();
    _fpgaVersion = fpgaInformationValues[3].trim();
    _fpgaChecksum = fpgaInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_fpgaLocalFilePath + _fpgaFileName);
  }

  /**
   * @author Kay Lannen
   */
  private void setRcshFileInformation() throws XrayTesterException
  {
    String rcshFileInformation = (String)_runtimeFileNameToInformationMap.get(_RUNTIME_INFORMATION_CONFIG_FILE_RCSHFILE_KEY_NAME);

    String[] rcshFileInformationValues = rcshFileInformation.split(_RUNTIME_INFORMATION_CONFIG_FILE_VALUES_DELIMITER);
    Assert.expect(rcshFileInformationValues.length == 5);

    _rcshFileLocalFilePath = _runtimeInformationDirectory;
    _rcshFileFileName = rcshFileInformationValues[0].trim();
    _rcshFileRemoteTempPath = rcshFileInformationValues[1].trim();
    _rcshFileRemoteRuntimePath = rcshFileInformationValues[2].trim();
    _rcshFileVersion = rcshFileInformationValues[3].trim();
    _rcshFileChecksum = rcshFileInformationValues[4].trim();

    verifyLocalRuntimeFileNameFullPath(_rcshFileLocalFilePath + _rcshFileFileName);
  }

  /**
   * @author Greg Esparza
   */
  private void setRuntimeFilesInformation() throws XrayTesterException
  {
    try
    {
      verifyRuntimeFileInformationArchiveExists();
      setRuntimeFileInformationArchive();
      ConfigFileReaderUtil configFileReader = new ConfigFileReaderUtil();
      _runtimeInformationDirectory = Directory.getAgilentTdiXrayCameraBoardVersionConfigDir(_boardVersion);
      _runtimeInformationFileNameFullPath = _runtimeInformationDirectory + FileName.getAgilentXrayCameraRuntimeFileConfigFileName();
      configFileReader.parseFile(_runtimeInformationFileNameFullPath);
      _runtimeFileNameToInformationMap = configFileReader.getKeyValueMap();
    }
    catch (FileNotFoundDatastoreException de)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiUnsupportedBoardVersionException(_cameraId,
                                                                                                                 _serverIpAddress,
                                                                                                                 _portNumber,
                                                                                                                 _boardVersion);
      he.initCause(de);
      _hardwareTaskEngine.throwHardwareException(he);
    }
    catch (XrayCameraHardwareException he)
    {
      throw he;
    }
    catch (XrayTesterException xte)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToSetupRuntimeFileInformationException(_cameraId,
                                                                                                                             _serverIpAddress,
                                                                                                                             _portNumber,
                                                                                                                             xte.getLocalizedMessageWithoutHeaderAndFooter());
      he.initCause(xte);
      _hardwareTaskEngine.throwHardwareException(he);
    }

    setBootLoaderInformation();
    setKernelInformation();
    setFileSystemInformation();
    setFpgaInformation();
    setDriverInformation();
    setApplicationInformation();
    setRcshFileInformation();
    }

  /**
   * @author Greg Esparza
   */
  protected String getBoardVersion() throws XrayTesterException
  {
    String boardVersion = null;

    if (isSimulationModeOn() == false)
      boardVersion =_rExecClientAxi.sendMessageAndReceiveReply(_GET_BOARD_VERSION_COMMAND);
    else
      boardVersion = _boardVersion;

    return boardVersion;
  }

  /**
   * @author Greg Esparza
   */
  private void verifyBoardVersion() throws XrayTesterException
  {
    String actualVersion = "";

    // Added by Jack Hwee - To make sure expected board version is get from board itself, not from hardware.config
    if (_byPassBoardVersionInHardwareConfigEnabled)
      _boardVersion = getBoardVersion();
    
    String expectedVersion = _boardVersion;
   
    try
    {
      actualVersion = getBoardVersion();
      if (actualVersion.equalsIgnoreCase(expectedVersion) == false)
      {
        // get the major version.
        // if the major version is not what this object support then error.  Otherwise, just update both the
        // configuration version and the object's internal version.
        if (AxiTdiXrayCameraBoardVersionEnum.isBoardVersionSupported(actualVersion) == false)
          _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiUnsupportedBoardVersionException(_cameraId,
                                                                                                                               _serverIpAddress,
                                                                                                                               _portNumber,
                                                                                                                               actualVersion));

        // The board version is supported so now we only need to check if this version instance will support what is actual
        // installed in the system.  If the major versions are the same, then we will update the configuration enum and
        // the internal board version data member.  Otherwise, we will error.
        _config.setValue(getBoardVersionEnumFromId(_cameraId), actualVersion);
        AxiTdiXrayCameraBoardVersionEnum actualMajorVersion = AxiTdiXrayCameraBoardVersionEnum.getEnum(actualVersion);
        AxiTdiXrayCameraBoardVersionEnum expectedMajorVersion = AxiTdiXrayCameraBoardVersionEnum.getEnum(expectedVersion);

        // This instance will not support the actual camera installed in the system.  As a result, the user will have to
        // shutdown and restart the application so we can create the appropriate instance.
        if (actualMajorVersion.equals(expectedMajorVersion) == false)
        {

          _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiBoardVersionMismatchException(_cameraId,
                                                                                                                            _serverIpAddress,
                                                                                                                            _portNumber,
                                                                                                                            actualVersion,
                                                                                                                            expectedVersion));
        }
      }
    }
    catch (CommunicationHardwareException che)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetBoardVersionException(_cameraId,
                                                                                                                 _serverIpAddress,
                                                                                                                 _portNumber,
                                                                                                                 che.getLocalizedMessageWithoutHeaderAndFooter());
      he.initCause(che);
      _hardwareTaskEngine.throwHardwareException(he);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void verifyRuntimeFiles() throws XrayTesterException
  {
    verifyBoardVersion();
    if (isAborting())
      return;

    setRuntimeFilesInformation();
    if (isAborting())
      return;

    runtimeFileUpdateSetup();

    if (_automaticRuntimeFileUpdateEnabled)
    {
      verifyBootLoader();
      if (isAborting())
        return;

      verifyKernel();
      if (isAborting())
        return;

      verifyFileSystem();
      if (isAborting())
        return;

      verifyApplication();
      if (isAborting())
        return;

      verifyDriver();
      if (isAborting())
        return;

      verifyFpga();
    }

    runtimeFileUpdateCleanup();
  }

  /**
   * @author Greg Esparza
   */
  private List<XrayCameraSegmentCalibrationData> getSegmentCalibrationData(String[] rawCalibrationDataFromCamera) throws XrayTesterException
  {
    Assert.expect(rawCalibrationDataFromCamera != null);

    List<XrayCameraSegmentCalibrationData> xRayCameraSegmentCalibrationData = new ArrayList<XrayCameraSegmentCalibrationData>();
    String dataValueDelimiter = _CALIBRATION_MODE_GET_DATA;

    try
    {
	  //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
      for (int i = 0; i < getCameraSensorNumberOfSegments(); ++i)
      {
        String segmentDataLine = rawCalibrationDataFromCamera[i].trim();
        String[] segmentDataValues = segmentDataLine.split(dataValueDelimiter);
        Assert.expect(segmentDataValues.length == _NUMBER_OF_SEGMENT_CALIBRATION_ATTRIBUTES);

        int segmentNumber = StringUtil.convertStringToInt(segmentDataValues[0]);
        double forwardGain = StringUtil.convertStringToDouble(segmentDataValues[1]);
        double forwardOffset = StringUtil.convertStringToDouble(segmentDataValues[2]);
        double reverseGain = StringUtil.convertStringToDouble(segmentDataValues[3]);
        double reverseOffset = StringUtil.convertStringToDouble(segmentDataValues[4]);
        XrayCameraSegmentCalibrationData segmentData = new XrayCameraSegmentCalibrationData(segmentNumber,
                                                                                            forwardGain,
                                                                                            forwardOffset,
                                                                                            reverseGain,
                                                                                            reverseOffset);
        xRayCameraSegmentCalibrationData.add(segmentData);
      }
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidSegmentDataValueException(_cameraId,
                                                                                                                 _serverIpAddress,
                                                                                                                 _portNumber,
                                                                                                                 bfe.getLocalizedMessage());
      he.initCause(bfe);
      _hardwareTaskEngine.throwHardwareException(he);
    }

    Assert.expect(xRayCameraSegmentCalibrationData.isEmpty() == false);
    Assert.expect(xRayCameraSegmentCalibrationData.size() == getCameraSensorNumberOfSegments());
    return xRayCameraSegmentCalibrationData;
  }

  /**
   * @author Greg Esparza
   */
  private List<XrayCameraPixelCalibrationData> getPixelCalibrationData(String[] rawCalibrationDataFromCamera) throws XrayTesterException
  {
    Assert.expect(rawCalibrationDataFromCamera != null);

    List<XrayCameraPixelCalibrationData> xRayCameraPixelCalibrationData = new ArrayList<XrayCameraPixelCalibrationData>();
    String dataValueDelimiter = _CALIBRATION_MODE_GET_DATA;
    //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	int numberOfCalibratedPixels =  rawCalibrationDataFromCamera.length - getCameraSensorNumberOfSegments();

    try
    {
      int startOfPixelData = getCameraSensorNumberOfSegments();
      int endOfPixelData = startOfPixelData + numberOfCalibratedPixels;
      for (int i = startOfPixelData; i < endOfPixelData; ++i)
      {
        String pixelDataLine = rawCalibrationDataFromCamera[i].trim();
        String[] pixelDataValues = pixelDataLine.split(dataValueDelimiter);
        Assert.expect(pixelDataValues.length == _NUMBER_OF_PIXEL_CALIBRATION_DATA_ATTRIBUTES);

        int pixelNumber = StringUtil.convertStringToInt(pixelDataValues[0]);
        double forwardGain = StringUtil.convertStringToDouble(pixelDataValues[1]);
        double forwardOffset = StringUtil.convertStringToDouble(pixelDataValues[2]);
        double reverseGain = StringUtil.convertStringToDouble(pixelDataValues[3]);
        double reverseOffset = StringUtil.convertStringToDouble(pixelDataValues[4]);
        XrayCameraPixelCalibrationData pixelData = new XrayCameraPixelCalibrationData(pixelNumber,
                                                                                      forwardGain,
                                                                                      forwardOffset,
                                                                                      reverseGain,
                                                                                      reverseOffset);
        xRayCameraPixelCalibrationData.add(pixelData);
      }
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidPixelDataValueException(_cameraId,
                                                                                                               _serverIpAddress,
                                                                                                               _portNumber,
                                                                                                               bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }

    Assert.expect(xRayCameraPixelCalibrationData.isEmpty() == false);
    return xRayCameraPixelCalibrationData;
  }

  /**
   * @author Greg Esparza
   */
  private String getRawCalibrationDataWithoutUnecessaryDelimiters(String rawCalibrationData)
  {
    Assert.expect(rawCalibrationData != null);
    Assert.expect(rawCalibrationData.length() > 0);

    rawCalibrationData = rawCalibrationData.trim();

    StringBuffer correctedRawCalibrationData = new StringBuffer(rawCalibrationData);
    if (rawCalibrationData.startsWith(_CALIBRATION_DATA_LINE_DELIMITER))
      correctedRawCalibrationData.deleteCharAt(0);

    if (rawCalibrationData.endsWith(_CALIBRATION_DATA_LINE_DELIMITER))
      correctedRawCalibrationData.deleteCharAt(correctedRawCalibrationData.length() - 1);

    return correctedRawCalibrationData.toString();
  }

  /**
   * @author Greg Esparza
   */
  public XrayCameraCalibrationData getCalibrationData() throws XrayTesterException
  {
    clearAbort();

    if (isSimulationModeOn() == false)
    {
      if (isAborting() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_GET_CALIBRATION_TABLE_COMMAND);
        Assert.expect(replies.size() == 1);

        String rawCalibrationData = getRawCalibrationDataWithoutUnecessaryDelimiters(replies.get(0));
        String[] calibrationData = rawCalibrationData.split(_CALIBRATION_DATA_LINE_DELIMITER);

        _calibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(calibrationData), getPixelCalibrationData(calibrationData));
      }
    }
    else
    {
      String[] simulationCalibrationData = getSimulationCalibrationData();
      _calibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(simulationCalibrationData), getPixelCalibrationData(simulationCalibrationData));
    }

    return _calibrationData;
  }


  /**
   * @author Anthony Fong
   */
  public XrayCameraCalibrationData setLowMagCalibrationData() throws XrayTesterException
  {
    clearAbort();

    if (isSimulationModeOn() == false)
    {
      if (isAborting() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_GET_CALIBRATION_TABLE_COMMAND);
        Assert.expect(replies.size() == 1);

        String rawCalibrationData = getRawCalibrationDataWithoutUnecessaryDelimiters(replies.get(0));
        String[] calibrationData = rawCalibrationData.split(_CALIBRATION_DATA_LINE_DELIMITER);

        _lowMagCalibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(calibrationData), getPixelCalibrationData(calibrationData));
      }
    }
    else
    {
      String[] simulationCalibrationData = getSimulationCalibrationData();
      _lowMagCalibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(simulationCalibrationData), getPixelCalibrationData(simulationCalibrationData));
    }

    return _lowMagCalibrationData;
  }
  /**
   * @author Anthony Fong
   */
  public XrayCameraCalibrationData getLowMagCalibrationData() throws XrayTesterException
  {
    return _lowMagCalibrationData;
  }
  
  /**
   * @author Anthony Fong
   */
  public XrayCameraCalibrationData setHighMagCalibrationData() throws XrayTesterException
  {
    clearAbort();

    if (isSimulationModeOn() == false)
    {
      if (isAborting() == false)
      {
        List<String> replies = sendCommandAndGetReplies(_GET_CALIBRATION_TABLE_COMMAND);
        Assert.expect(replies.size() == 1);

        String rawCalibrationData = getRawCalibrationDataWithoutUnecessaryDelimiters(replies.get(0));
        String[] calibrationData = rawCalibrationData.split(_CALIBRATION_DATA_LINE_DELIMITER);

        _highMagCalibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(calibrationData), getPixelCalibrationData(calibrationData));
      }
    }
    else
    {
      String[] simulationCalibrationData = getSimulationCalibrationData();
      _highMagCalibrationData.set(getTemperatureInDegreesFahrenheit(), getSegmentCalibrationData(simulationCalibrationData), getPixelCalibrationData(simulationCalibrationData));
    }

    return _highMagCalibrationData;
  }

  /**
   * @author Anthony Fong
   */
  public XrayCameraCalibrationData getHighMagCalibrationData() throws XrayTesterException
  {
    return _highMagCalibrationData;
  }

  /**
   * Generate fake data for use in sim mode. It is within the normal
   * limits of the actual hardware although not distributed or representative
   * of the actual hardware values.
   *
   * Below is a note from the documentation of the new calibration data format...
   *
   * The calibration table format for download and upload to the camera is defined as
   *
   * 4 segment calibration definitions
   * 1088 pixel calibration definitions
   * <Terminating new line character>
   * Where a segment calibration definition is:
   * <segment number: integer>   <fwd gain: real>   <fwd offset: real> <rev gain: real>   <rev offset: real> <semicolon>
   *
   * And where a pixel calibration definition is:
   * <pixel number: integer>   <fwd gain: real>   <fwd offset: real> <rev gain: real>   <rev offset: real> <semicolon>
   *
   * @author Reid Hayhow
   * Swee Yee Wong - XCR-2630 Support New X-Ray Camera
   */
  private String[] getSimulationCalibrationData()
  {
    String[] simulationCalibrationData = new String[getCameraSensorNumberOfSegments() + getNumberOfSensorPixelsUsed()];

    for (int i = 0; i < getCameraSensorNumberOfSegments(); ++i)
      simulationCalibrationData[i] = new String("0 1.0 155.0 2.0 -100.0");

    int pixelNumber = 0;
    int startOfPixelData = getCameraSensorNumberOfSegments();
    int endOfPixelData = startOfPixelData + getNumberOfSensorPixelsUsed();

    for (int i = startOfPixelData; i < endOfPixelData; i++)
    {
      pixelNumber = i - getCameraSensorNumberOfSegments();

      switch (i)
      {
        //Special cases to test limits and range checking
        case 50:
          simulationCalibrationData[i] = new String(pixelNumber + " 1.9 1.0 0.99 1.0");
          break;
        case 550:
          simulationCalibrationData[i] = new String(pixelNumber + " 1.0 -29.0 1.0 1.0");
          break;
        case 789:
          simulationCalibrationData[i] = new String(pixelNumber + " 1.0 3.9 1.0 1.0");
          break;
        case 1083:
          simulationCalibrationData[i] = new String(pixelNumber + " 1.0 1.0 1.0 -28.0");
          break;
          //For most cases just use the same data
        default:
          simulationCalibrationData[i] = new String(pixelNumber + " 1.0 1.0 1.0 1.0");
      }
    }

    return simulationCalibrationData;
  }


  /**
  * @author Ronald Lim
  */

  public boolean updateDriverRequired()throws XrayTesterException
  {
    boolean updateRequired = false;

    if (isSimulationModeOn())
    {
      return updateRequired = false;
    }


    String expectedVersion = _driverVersion;
    String actualVersion = getDriverVersion();
    //System.out.println("Expected driver version = " + expectedVersion);
    //System.out.println("Actual driver version = " + actualVersion);

	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("");
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);
    if (expectedVersion.equalsIgnoreCase(actualVersion) == true)
    {
      updateRequired = false;
    }
    else
    {
      updateRequired = true;
    }
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("Update Driver required: " + updateRequired + "\n" +
                            "Expected Version: " + expectedVersion + "\n" +
                            "Actual Version: " + actualVersion + "\n" +
                            "=====================================================\n");
    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);

    return updateRequired;
  }

  /**
   * @author Ronald Lim
   */

  public boolean updateApplicationRequired()throws XrayTesterException
  {
    boolean updateRequired = false;

    if (isSimulationModeOn())
    {
      return updateRequired = false;
    }

    String expectedVersion = _applicationVersion;
    String actualVersion = getApplicationVersion();
    //System.out.println("Expected application version = " + expectedVersion);
    //System.out.println("Actual application version = " + actualVersion);
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("");
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);
    if (expectedVersion.equalsIgnoreCase(actualVersion) == true)
    {
      updateRequired = false;
    }
    else
    {
      updateRequired = true;
    }
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("Update Application required: " + updateRequired + "\n" +
                                "Expected Version: " + expectedVersion + "\n" +
                                "Actual Version: " + actualVersion + "\n" +
                                "=====================================================\n");
    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);

    return updateRequired;
  }

  /**
   * @author Ronald Lim
   */

  public boolean updateBootloaderRequired()throws XrayTesterException
  {
    boolean updateRequired = false;

   if (isSimulationModeOn())
   {
     return updateRequired = false;
   }

    String expectedVersion = _bootLoaderVersion ;
    String actualVersion = getBootLoaderVersion();
    //System.out.println("Expected bootloader version = " + expectedVersion);
    //System.out.println("Actual bootloader version = " + actualVersion);

	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("");
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);
    if (expectedVersion.equalsIgnoreCase(actualVersion) == true)
    {
      updateRequired = false;
    }
    else
    {
      updateRequired = true;
    }
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("Update Boot Loader required: " + updateRequired + "\n" +
                            "Expected Version: " + expectedVersion + "\n" +
                            "Actual Version: " + actualVersion + "\n" +
                            "=====================================================\n");
    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);

    return updateRequired;
  }

  /**
   * @author Kay Lannen
   */
  public boolean updateRcshRequired() throws XrayTesterException
  {
    boolean updateRequired = false;

    if (isSimulationModeOn())
    {
      return updateRequired = false;
    }

    String expectedVersion = _rcshFileVersion;
    String actualVersion = getRcshFileVersion();
    //System.out.println("Expected rc.sh version = " + expectedVersion);
    //System.out.println("Actual rc.sh version = " + actualVersion);

	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("");
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);
    if (expectedVersion.trim().toLowerCase().compareTo(actualVersion.trim().toLowerCase()) == 0)
    {
      updateRequired = false;
    }
    else
    {
      updateRequired = true;
    }
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("Update Rc.sh required: " + updateRequired + "\n" +
                            "Expected Version: " + expectedVersion + "\n" +
                            "Actual Version: " + actualVersion + "\n" +
                            "=====================================================\n");
    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.VERIFY_FILE_VERSION);

    return updateRequired;
  }

  /**
   * @author Kay Lannen
   */
  public boolean updateCameraFirmwareRequired() throws XrayTesterException
  {
    return( updateDriverRequired() || updateApplicationRequired() ||
            updateBootloaderRequired() || updateRcshRequired() );
  }


  /**
    * @author Ronald Lim
    */
  private void addToWorkList(String sourceFile, String destFile, List<Pair<String,String>> workList)throws FileNotFoundDatastoreException
  {
    Assert.expect(sourceFile != null);
    Assert.expect(destFile != null);
    Assert.expect(workList != null);

    Pair<String,String> workElement = new Pair<String,String>();

    File file = new File(sourceFile);

    if (file.exists())
    {
      workElement.setFirst(sourceFile);
      workElement.setSecond(destFile);

      workList.add(workElement);
    }
    else
    {
      throw new FileNotFoundDatastoreException(sourceFile);
    }
  }

  /**
    * @author Ronald Lim
    */
   private void createRemoteUploadDirectory(String remoteFileNameFullPath)throws XrayTesterException
   {
     _rExecClientAxi.sendMessage(_MAKE_DIRECTORY_COMMAND + remoteFileNameFullPath);
     _rExecClientAxi.sendMessage(_CHANGE_FILE_PERMISSION_COMMAND + remoteFileNameFullPath);
   }

  /**
   * @author Ronald Lim
   */
  private String generateMd5Checksum (String filename) throws XrayTesterException
  {
    Assert.expect(filename != null);
    StringBuffer hexString = new StringBuffer();

    try
    {
      BinaryFileReaderAxi FileReader = new BinaryFileReaderAxi();
      byte[] fileByteArray = FileReader.readFileInByte(filename);

      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.reset();

      byte[] digest = messageDigest.digest(fileByteArray);

      for (int i = 0; i < digest.length; i++)
      {
        String hex = MathUtil.convertByteToHex(digest[i]);
        hexString.append(hex);
      }
   }
   catch(NoSuchAlgorithmException nsae)
   {
     Assert.logException(nsae);
   }

   return hexString.toString();
 }



  /**
   * @author Ronald Lim
   */

  private void sendFile (String sourceFilename, String destinationFilename) throws XrayTesterException
  {
    Assert.expect(sourceFilename != null);
    Assert.expect(destinationFilename != null);

    boolean updateOk = false;
    int numberOfRetries = 1;
    int retryCount = 1;
    File fileToUpload = new File(sourceFilename);

    if (fileToUpload.exists())
    {
      do
      {
        try
        {
          _tftpClientAxi.open();
          _tftpClientAxi.sendFile(sourceFilename, destinationFilename);
          _tftpClientAxi.close();

          updateOk = true;
        }
        catch (XrayTesterException xte)
        {
          if(retryCount > numberOfRetries)
          {
            throw xte;
          }
        }
        ++retryCount;
      }
      while (updateOk == false);
    }
    else
    {
      throw new FileNotFoundDatastoreException(sourceFilename);
    }
  }


  /**
   * @author Ronald Lim
   */

  private void uploadFile (String sourceFilename, String destFilename) throws XrayTesterException
  {
    Assert.expect(sourceFilename != null);
    Assert.expect(destFilename != null);
    
	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("sourceFile: " + sourceFilename + "\n" +
                                "destFile: " + destFilename + "\n" +
                                "=====================================================\n");
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.UPLOAD_CAMERA_FIRMFARE_FILE);

    File file = new File(destFilename);

    String remoteAbsoluteFilePath = _applicationRemoteTempPath + file.getName();

    _rExecClientAxi.sendMessage(_REMOVE_ALL_FILES_COMMAND + remoteAbsoluteFilePath);
    _rExecClientAxi.sendMessage(_CREATE_FILE_COMMAND + remoteAbsoluteFilePath);
    _rExecClientAxi.sendMessage(_CHANGE_FILE_PERMISSION_COMMAND + remoteAbsoluteFilePath);

    sendFile(sourceFilename, remoteAbsoluteFilePath);

    String localMD5 = generateMd5Checksum(sourceFilename);
    //System.out.println("Local MD5 Checksum = " + localMD5);

    String remoteMD5 = _rExecClientAxi.sendMessageAndReceiveReply(_GENERATE_MD5_CHECKSUM_COMMAND  + remoteAbsoluteFilePath);

    StringTokenizer st = new StringTokenizer(remoteMD5);
    remoteMD5 = st.nextToken();

    //System.out.println("Remote md5 Checksum = " + remoteMD5);

    if (remoteMD5.trim().toLowerCase().compareTo(localMD5.trim().toLowerCase()) != 0)
    {
      String details = "Checksum failed on upload file. File corruption likely.";
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiFaileToUpdateRuntimeFileException(_cameraId,
                                                                                                                            _serverIpAddress,
                                                                                                                            _portNumber,
                                                                                                                            remoteAbsoluteFilePath,
                                                                                                                            sourceFilename,
                                                                                                                            destFilename,
                                                                                                                            details));
    }


    _rExecClientAxi.sendMessage(_COPY_FILE_COMMAND + remoteAbsoluteFilePath + " " + destFilename);

    remoteMD5 = _rExecClientAxi.sendMessageAndReceiveReply(_GENERATE_MD5_CHECKSUM_COMMAND + destFilename);

    st = new StringTokenizer(remoteMD5);
    remoteMD5 = st.nextToken();

    //System.out.println("Remote MD5 Checksum = " + remoteMD5);

    if (remoteMD5.trim().toLowerCase().compareTo(localMD5.trim().toLowerCase()) != 0)
    {
      //System.out.println("Checksum failed on uploaded file. File corruption likely." );
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiInvalidRuntimeFileChecksumException(_cameraId,
                                                                                                                              _serverIpAddress,
                                                                                                                              _portNumber,
                                                                                                                              sourceFilename,
                                                                                                                              remoteMD5,
                                                                                                                              localMD5));
    }

	//Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _upgradeFirmwareMessageList.add("");
    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.UPLOAD_CAMERA_FIRMFARE_FILE);
  }

  /**
   * @author Ronald Lim
   * @edited By Kee Chin Seong - Force Update Firmware
   */
  public void updateCameraFirmware (boolean isForceUpdate) throws XrayTesterException
  {
    if (isSimulationModeOn())
    {
      return;
    }

    // CR1063 fix by LeeHerng - Add a checking to make sure that
    // if a user wants to upgrade camera firmware without initializing cameras,
    // we need to do it here before uploading the firmware.
    // Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
//    if(isForceUpdate == false)
//    {
      if (_initialized == false)
      {
          verifyCommunication();
          if (isAborting())
              return;
      }

      verifyRuntimeFiles();
      if (isAborting())
        return;
//    }
//    else
//    {
//      setRuntimeFilesInformation();
//      if (isAborting())
//        return;
//
//      runtimeFileUpdateSetup();
//
//      runtimeFileUpdateCleanup();
//    }

    LinkedList <Pair<String,String>> workList = new LinkedList<Pair<String,String>>();

    if (isForceUpdate == true || updateApplicationRequired())
    {
      String localAbsoluteFilePath = _applicationLocalFilePath + _applicationFileName;
      String destAbsoluteFilePath = _applicationRemoteRuntimePath + _applicationFileName;
      addToWorkList(localAbsoluteFilePath,destAbsoluteFilePath,workList);
    }

    if (isForceUpdate == true || updateDriverRequired())
    {
      String localAbsoluteFilePath = _driverLocalFilePath + _driverFileName;
      String destAbsoluteFilePath = _driverRemoteRuntimePath + _driverFileName;
      addToWorkList(localAbsoluteFilePath, destAbsoluteFilePath, workList);
    }

    if (isForceUpdate == true || updateRcshRequired())
    {
      String localAbsoluteFilePath = _rcshFileLocalFilePath + _rcshFileFileName;
      String destAbsoluteFilePath = _rcshFileRemoteRuntimePath + _rcshFileFileName;
      addToWorkList(localAbsoluteFilePath, destAbsoluteFilePath, workList);
    }

    if (isForceUpdate == true || updateBootloaderRequired())
    {
      String localAbsoluteFilePath = _bootLoaderLocalFilePath + _bootLoaderFileName;
      String destAbsoluteFilePath = _bootLoaderRemoteRuntimePath + _BOOTLOADER_DEST_DEV_FILE_NAME;
      addToWorkList(localAbsoluteFilePath, destAbsoluteFilePath, workList);
    }

    //The remote Temp path for all camera firmware files are common. Here I used _applicationRemoteTempPath .
    createRemoteUploadDirectory(_applicationRemoteTempPath);

    for (Pair<String,String> workElement : workList)
    {
      uploadFile(workElement.getFirst(),workElement.getSecond());
    }

    _fileSystemUpdated = true;

  }

  /**
   * @throws XrayTesterException
   * @author Cheah, Lee Herng
   */
  private void verifyCameraFirmwareVersionWithoutAutomaticRuntimeFileUpdate() throws XrayTesterException
  {
      if (isSimulationModeOn() || UnitTest.unitTesting())
          return;

      if (_automaticRuntimeFileUpdateEnabled == false)
      {
          verifyBootLoaderVersion();
          if (isAborting())
              return;

          verifyKernelVersion();
          if (isAborting())
              return;

          verifyFileSystemVersion();
          if (isAborting())
              return;

          verifyApplicationVersion();
          if (isAborting())
              return;

          verifyDriverVersion();
          if (isAborting())
              return;

          verifyFpgaVersion();
          if (isAborting())
              return;
      }
  }

  /**
   * @author sham
   */
  public void setUserDefinedLightSensitivity(double userGain) throws XrayTesterException
  {
    Assert.expect(userGain > 0);

    setCameraUserGainAndOffset(userGain,
      _config.getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_OFFSET));
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void setCalPointValuesForVariableMag(boolean isLowMag) throws XrayTesterException
  {
    double yAxisScanMotionVelocity = 0.0f;
    int segmentMin = _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MIN);
    int segmentMax = _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_SEGMENT_MAX);
    int pixelMin = _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MIN);
    int pixelMax = _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MAX);
    double magnification = 0;

    if(isLowMag)
    {
      String yAxisScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      magnification = MagnificationEnum.getNominalMagnificationAtReferencePlane();
      try
      {
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile0VelocityString);
      }
      catch (BadFormatException ex)
      {
        System.out.println("estimateTestSubProgramExecutionTime" + ex);
        return;
      }
    }
    else
    {
      String yAxisScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      magnification = MagnificationEnum.getNominalHighMagnificationAtReferencePlane();
      try
      {
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile1VelocityString);
      }
      catch (BadFormatException ex)
      {
        System.out.println("yAxisScanMotionVelocity" + ex);
        return;
      }
    }

    segmentMax = (int) (segmentMax * yAxisScanMotionVelocity * magnification / _MAGNIFICATION_TO_Y_AXIS_SCAN_MOTION_VELOCITY_CONSTANT);
    pixelMax = (int) (pixelMax * yAxisScanMotionVelocity * magnification / _MAGNIFICATION_TO_Y_AXIS_SCAN_MOTION_VELOCITY_CONSTANT);
    setCalPointValues(segmentMin, segmentMax, pixelMin, pixelMax);
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public String getUpgradingInfoMessage()
  {
    Assert.expect(_upgradeFirmwareMessageList != null);
    String message = _upgradeFirmwareMessageList.get(0);
    _upgradeFirmwareMessageList.remove(0);
    return message;
  }

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getCameraBinningMode()
  {
    // versionA xray camera doesn't have Binning mode, so just set it to 1
    return 1;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getCameraMaxPocketHeightInPixels()
  {
    return (int)Math.round((double)(getCameraMaxPocketHeightInNanometers() / getSensorRowPitchInNanometers()));
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getCameraReadingPositionFromMaxPocketHeightInPixels()
  {
    double offsetFromMaxPocketHeightInNanometers = (double)((getCameraMaxPocketHeightInNanometers() - getSensorHeightInNanometers())/2);
    Assert.expect(offsetFromMaxPocketHeightInNanometers >= 0.0);
    double cameraReadingPositionFromMaxPocketHeightInNanometers = offsetFromMaxPocketHeightInNanometers + getSensorHeightInNanometers();
    return (int)Math.round((double)(cameraReadingPositionFromMaxPocketHeightInNanometers/getSensorRowPitchInNanometers()));
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getCameraExtraDelayToMakeHysteresisMorePositiveInNanometers()
  {
    return _EXTRA_DELAY_TO_MAKE_HYSTERESIS_MORE_POSITIVE_IN_NANOMETERS;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public int getCameraSensorNumberOfSegments()
  {
    return _SENSOR_NUMBER_OF_SEGMENTS;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public String getCameraBoardVersion()
  {
    Assert.expect(_boardVersion != null);
    return _boardVersion;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void enableSafeGuards() throws XrayTesterException
  {
    setSafeGuards(_ENABLE_SAFEGUARDS);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void disableSafeGuards() throws XrayTesterException
  {
    setSafeGuards(_DISABLE_SAFEGUARDS);
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void setSafeGuards(String safeGuards) throws XrayTesterException
  {
    Assert.expect(safeGuards != null);

    if (isAborting())
      return;

    if (isSimulationModeOn() == false)
    {
      List<String> replies = sendCommandAndGetReplies(_ENABLE_SAFEGUARD_COMMAND + " " + safeGuards);
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void saveUncalibratedLogFiles(String targetLogDirectory, int frequencyTimeIntervalInMilliseconds, int maxLogFiles) throws DatastoreException, XrayTesterException
  {
    //do nothing, Gen1 camera doesn't have this command
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void enableSaveCalibrationLog() throws XrayTesterException
  {
    //do nothing, Gen1 camera doesn't have this command
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void disableSaveCalibrationLog() throws XrayTesterException
  {
    //do nothing, Gen1 camera doesn't have this command
  }
  
  /**
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author Swee-Yee.Wong
   */
  private void logIaeCameraCommands(String message)
  {
    Assert.expect(message != null);
    if (_config.isMotionControlLogEnabled())
    {
      try
      {
        IaeCameraCommandLogUtil.getInstance().log(message);
      }
      catch (XrayTesterException e)
      {
        System.out.println("Failed to log iaeCameraCommands. \n" + e.getMessage());
      }
    }
  }
}

