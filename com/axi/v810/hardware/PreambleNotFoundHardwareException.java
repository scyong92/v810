package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the reading does not start with the preamble, then throw this exception.
 *
 * @author Bob Balliew
 */
class PreambleNotFoundHardwareException extends HardwareException
{
  /**
   * This is for reading with an incorrect preamble.
   *
   * @param expectedPreamble is the expected preamble.
   * @param entireReading is the entire reading returned from the barcode scanner.
   * @param scannerOrder is the (zero based) barcode scanner read order.
   * @author Bob Balliew
   */
  public PreambleNotFoundHardwareException(String expectedPreamble, String entireReading, int scannerOrder)
  {
    super(new LocalizedString("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY",
                              new Object[]{expectedPreamble, entireReading, new Integer(scannerOrder + 1)}));

    Assert.expect(expectedPreamble != null);
    Assert.expect(entireReading != null);
    Assert.expect(scannerOrder >= 0);
  }
}
