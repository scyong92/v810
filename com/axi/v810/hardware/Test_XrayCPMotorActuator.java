package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left outer barrier
 * @author Rex Shang
 */
class Test_XrayCylinderActuator extends UnitTest
{
  private XrayCylinderActuator _xrayCylinderActuator = null;

  private Test_XrayCylinderActuator()
  {
    _xrayCylinderActuator = XrayCylinderActuator.getInstance();
  }

  public static void main(String[] args)
  {
    Test_XrayCylinderActuator _test_XrayCylinder = new Test_XrayCylinderActuator();
    UnitTest.execute(_test_XrayCylinder);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Move the xray cylinder up and verify 
      _xrayCylinderActuator.up(false); //false = without loading camera config
      Expect.expect(_xrayCylinderActuator.isUp() == true);

      // Move the  xray cylinder down and verify.
      _xrayCylinderActuator.down(false); //false = without loading camera config
      Expect.expect(_xrayCylinderActuator.isDown() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
