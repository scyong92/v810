package com.axi.v810.hardware;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraHardwareExceptionEnum extends HardwareExceptionEnum
{
    private static int _index = -1;

    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_INITIALIZE = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_STOP = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_WINDOW_HANDLE = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_SHOW_LIVE_VIDEO = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_DRAW_IMAGE = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_ENABLE_HARDWARE_TRIGGER = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_ACQUIRE_OFFLINE_IMAGES = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_FREEZE_VIDEO = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_NUMBER_OF_CONNECTED_CAMERAS = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_ENABLE_AUTO_GAIN = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_ENABLE_AUTO_SENSOR_GAIN = new OpticalCameraHardwareExceptionEnum(++_index);    
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_IS_GAIN_BOOST_SUPPORTED = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_SET_GAIN_BOOST = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_MASTER_GAIN_FACTOR = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_SET_MASTER_GAIN_FACTOR = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_FIRMWARE_VERSION = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_SERIAL_NUMBER = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_MANUFACTURER = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_FINAL_QUALITY_CHECK = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_FRAMES_PER_SECOND = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_SET_FRAMES_PER_SECOND = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_SET_MAGNIFICATION_DATA = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_ACQUIRE_CALIBRATION_IMAGES = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_PERFORM_CALIBRATION = new OpticalCameraHardwareExceptionEnum(++_index);
    //XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_FPGA_VERSION = new OpticalCameraHardwareExceptionEnum(++_index);
    public static OpticalCameraHardwareExceptionEnum AXI_OPTICAL_FAILED_TO_GET_MODEL = new OpticalCameraHardwareExceptionEnum(++_index);
    /**
     * @author Cheah Lee Herng
     */
    private OpticalCameraHardwareExceptionEnum(int id)
    {
        super(id);
    }
}
