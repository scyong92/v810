package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;

/**
 * @author Jack Hwee
 */
public class SentechOpticalCamera extends AbstractOpticalCamera
{
  public static final int OPTICAL_IMAGE_CENTER_X = 752;
  public static final int OPTICAL_IMAGE_CENTER_Y = 480;
  public static final int OPTICAL_IMAGE_ROI_WIDTH = 10;
  public static final int OPTICAL_IMAGE_ROI_HEIGHT = 10;
  public static final int OPTICAL_IMAGE_WIDTH = 640;
  public static final int OPTICAL_IMAGE_HEIGHT = 470;
  public static final int OPTICAL_MAX_IMAGE_WIDTH = 1600;
  public static final int OPTICAL_MAX_IMAGE_HEIGHT = 1200;
  
  public static final int OPTICAL_ACTUAL_IMAGE_WIDTH_STD_XXL_IN_NANOMETER = 27000000;  
  public static final int OPTICAL_ACTUAL_IMAGE_HEIGHT_STD_XXL_IN_NANOMETER = 19000000;
  public static final int OPTICAL_ACTUAL_IMAGE_WIDTH_S2EX_IN_NANOMETER = 31000000;  
  public static final int OPTICAL_ACTUAL_IMAGE_HEIGHT_S2EX_IN_NANOMETER = 21000000;
  public static final int OPTICAL_ACTUAL_MAX_IMAGE_WIDTH_IN_NANOMETER = 56300000; // the actual value obtained from hardware team.
  public static final int OPTICAL_ACTUAL_MAX_IMAGE_HEIGHT_IN_NANOMETER = 42230000; // the actual value obtained from hardware team.
  public static final int OPTICAL_LARGER_FOV_IMAGE_WIDTH = 900;
  public static final int OPTICAL_LARGER_FOV_IMAGE_HEIGHT = 800;
  public static final int OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_WIDTH_IN_NANOMETER = 37000000;
  public static final int OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_HEIGHT_IN_NANOMETER = 31000000;

  public static final double OPTICAL_CAMERA_FRAMES_PER_SECOND = 60.0;

  private static final int _IS_SUCCESS    = 0;
  //XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
  private static final int _IS_NO_SUCCESS = -1;

  private static final int _JNI_INITIALIZE_TIME_IN_MILLISECONDS = 650;

  private static Map<OpticalCameraIdEnum, SentechOpticalCamera> _cameraIdToInstanceMap;    
  private int _windowHandle = 0;

  private boolean _isHeightMapValueAvailable = false;
  private float[] _heightMapValue;

  private String _model;
  private String _firmwareVersion;
  private String _manufacturer;
  
  //XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
  private String _fpgaVersion;
  private static final String _SENTECH_MANUFACTURER_NAME = "Sensor Technology (Sentech)";
  
  private WorkerThread _surfaceMappingWorkerThread = new WorkerThread("surface mapping worker thread");
  private BooleanLock  _stopSentechLiveVideo = new BooleanLock(true);

  /**
   * Build camera Id list for later retrieval.
   * 
   * @author Jack Hwee
   */
  static void setCameraIdList()
  {
    _config = Config.getInstance();
    _cameraIdToInstanceMap = new HashMap<OpticalCameraIdEnum, SentechOpticalCamera>();
  }

  /**
   * Load native library.
   * 
   * @author Jack Hwee
   **/
  static
  {
    System.loadLibrary("nativeSentech");
    System.loadLibrary("jawt");
    setCameraIdList();
  }

  /**
   * Retrieve an instance of uEyeOpticalCamera based on camera Id.
   * 
   * @author Jack Hwee
   */
  static SentechOpticalCamera getAnInstance(OpticalCameraIdEnum cameraId)
  {
    SentechOpticalCamera instance = null;

    if (_cameraIdToInstanceMap.containsKey(cameraId))
      instance = _cameraIdToInstanceMap.get(cameraId);
    else
    {
      instance = new SentechOpticalCamera(cameraId);
      _cameraIdToInstanceMap.get(cameraId);
    }

    Assert.expect(instance != null);
    return instance;
  }

  /**
   * uEyeOptical Camera constructor.
   * 
   * @author Jack Hwee
   */
  protected SentechOpticalCamera(OpticalCameraIdEnum cameraId)
  {
    super(cameraId);

    Assert.expect(cameraId != null);        
    _cameraId = cameraId;
  }

  /**
   * Initialize uEyeOpticalCamera for later usage.
   * In order to initialize successfully, we need to pass in device id and 
   * window handle.
   * 
   * @throws OpticalCameraInitializeException
   * @author Jack Hwee
   */
  public synchronized void initialize() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    boolean isMirrorMode = true;  // Once we use SenTech camera, we will need Mirror mode

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeInitialize(_cameraId.getId(), _windowHandle, isMirrorMode);            
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToInitialize(_cameraId.getId(),
                                                                             _cameraId.getDeviceId(), 
                                                                             result,
                                                                             SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                             "Failed to initialize optical camera");

    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToInitialize(_cameraId.getId(),
                                                                                                                                      _cameraId.getDeviceId(), 
                                                                                                                                      result,
                                                                                                                                      SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                      "Failed to initialize optical camera");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }        
  }

  /**
   * Perform start-up operation to make sure SentechOpticalCamera operates normally,
   * including connectivity.
   * 
   * @author Jack Hwee
   */
  public void startup() throws XrayTesterException
  {
    int numberOfRetries = 2;
    int retryCount = 1;

    clearAbort();

    try
    {
      do
      {
        setStartupBeginProgressState();
        _hardwareObservable.stateChangedBegin(this, OpticalCameraEventEnum.INITIALIZE);
        _hardwareObservable.setEnabled(false);
        _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.OPTICAL_CAMERA_INITIALIZE, getStartupTimeInMilliseconds());

        try
        {
          if ((UnitTest.unitTesting() == false) &&  isSimulationModeOn())
          {
            _initialized = true;
            return;
          }

          if (isAborting())
            return;

          initialize();
          if (isAborting())
            return;

          loadCalibrationData(getOpticalCalibrationDirectoryFullPath());
          if (isAborting())
            return;

          //XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
          _model = getModel();
          if (isAborting())
            return;

          _firmwareVersion = getFirmwareVersion();
          if (isAborting())
            return;

          _manufacturer = getManufacturer();
          if (isAborting())
            return;

          _fpgaVersion = getFpgaVersion();
          if (isAborting())
            return;

          setConfigurationDescription();
          if (isAborting())
            return;

          _initialized = true;
        }
        catch (OpticalCameraHardwareException he)
        {
          handleCriticalErrors(he);

          if (retryCount < numberOfRetries)
          {
            setStartupEndProgressState();
          }
          else
            _hardwareTaskEngine.throwHardwareException(he);
        }

        ++retryCount;
      }
      while (_initialized == false);            
    }
    finally
    {
      setStartupEndProgressState();
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.OPTICAL_CAMERA_INITIALIZE);
      _hardwareObservable.setEnabled(true);
      _hardwareObservable.stateChangedEnd(this, OpticalCameraEventEnum.INITIALIZE);
    }
  }

  /**
   * Handle errors returned from camera native.
   * 
   * @author Cheah Lee Herng
   */
  private void handleCriticalErrors(OpticalCameraHardwareException he) throws XrayTesterException
  {
    Assert.expect(he != null);

    OpticalCameraHardwareExceptionEnum exceptionType = he.getExceptionType();

    if ((exceptionType.equals(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_INITIALIZE)))
      _hardwareTaskEngine.throwHardwareException(he);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void shutdown() throws XrayTesterException
  {
    _initialized = false;
    exitCamera();
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    if (_initialized == false)
      return true;
    else
      return false;
  }

  /**
   * @author Cheah Lee Herng
   */
  private int getStartupTimeInMilliseconds()
  {
    int startupTimeInMilliseconds = _JNI_INITIALIZE_TIME_IN_MILLISECONDS;
    return startupTimeInMilliseconds;
  }

  /**
   * @author Jack Hwee
   */
  public synchronized void exitCamera() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    // To make sure the camera is ready to be closed, else it will crash.
    try 
    {
      // XCR-2711 Java auto close when run test without running dependent tests for manual optical confirmations task
      // Lengthen the delay time from 100ms to 500ms to give system ample time for stablization period.
      Thread.sleep(500);
    } 
    catch (InterruptedException ex) 
    {
      ex.printStackTrace();
    }

    int result = _IS_NO_SUCCESS;
    try
    {          
      result = nativeExitCamera(_cameraId.getId());
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToStop(_cameraId.getId(), 
                                                                       _cameraId.getDeviceId(), 
                                                                       result,
                                                                       SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                       "Failed to stop optical camera");
      _windowHandle = 0;
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToStop(_cameraId.getId(), 
                                                                                                                               _cameraId.getDeviceId(), 
                                                                                                                               result,
                                                                                                                               SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                               "Failed to stop optical camera");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }    

  /**
   * @author Jack Hwee
   */
  public synchronized void displayLiveVideo() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    _surfaceMappingWorkerThread.invokeLater(new Runnable()
    {
      int result = _IS_NO_SUCCESS;

      public void run()
      {
        try
        {
          do
          {
            try
            {                 
              result = nativeLiveVideo(_cameraId.getId(), _windowHandle);
              if (result != _IS_SUCCESS)
                throw OpticalCameraHardwareException.getAxiOpticalFailedToShowLiveVideo(_cameraId.getId(), 
                                                                                        _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to display live video");
            }
            catch (NativeHardwareException nhe)
            {
              OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToShowLiveVideo(_cameraId.getId(),
                                                                                                _cameraId.getDeviceId(), 
                                                                                                result,
                                                                                                SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                                "Failed to display live video");
              opticalCameraHardwareException.initCause(nhe);
              _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
            }
          } while (_stopSentechLiveVideo.isFalse());              
        }
        catch (XrayTesterException xte)
        {
          // do nothing 
        }
      }
    });
    ;
  }
    
  /**
   * @author Jack Hwee
   */
  public synchronized void setStopLiveVideoBoolean(boolean bool) 
  {
    _stopSentechLiveVideo.setValue(bool);
  }
    
  /**
   * Get the JAWT window handle which needs to be used in uEyeOpticalCamera
   * API. This window handle needs to be used to draw on the canvas.
   * 
   * @author Cheah Lee Herng
   */
  public synchronized int getWindowHandle(java.awt.Component window) throws XrayTesterException
  {
    Assert.expect(window != null);

    int windowHandle = 0;
    try
    {
      windowHandle = nativeGetWindowHandle(window);
      _windowHandle = windowHandle;
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetWindowHandle(_cameraId.getId(),
                                                                                      _cameraId.getDeviceId(), "Failed to get optical camera handle");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
    return windowHandle;
  }

 /**
  * Make uEyeOpticalCamera in hardware external trigger mode.
  * 
  * @author Jack Hwee
  */
  public synchronized void enableHardwareTrigger() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeEnableHardwareTrigger(_cameraId.getId());
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToEnableHardwareTrigger(_cameraId.getId(), 
                                                                                        _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to enable hardware trigger");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToEnableHardwareTrigger(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                                "Failed to enable hardware trigger");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * This function can only be called after enableHardwareTrigger() function.
   * When calling acquireOfflineImages, uEyeOpticalCamera will wait until it has
   * been triggered to capture image.
   * 
   * @author Jack Hwee
   */
  public synchronized void acquireOfflineImages(String fullPathFileName, boolean forceTrigger) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    Assert.expect(fullPathFileName != null);
    
    int result = _IS_NO_SUCCESS;  
    try
    {
      result = nativeAcquireOfflineImages(_cameraId.getId(), fullPathFileName, forceTrigger);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToAcquireOfflineImages(_cameraId.getId(), 
                                                                                       _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to acquire offline images");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToAcquireOfflineImages(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                                                                                "Failed to acquire offline images");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public int getNumberOfConnectedCameras() throws XrayTesterException
  {
    int numberOfConnectedCameras = 0;  
    try
    {
      numberOfConnectedCameras = nativeGetNumberOfCameras();        
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetNumberOfConnectedCameras("Failed to freeze video");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
    return numberOfConnectedCameras;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
    Assert.expect(_cameraId != null);
    return _cameraId;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void enableAutoGain(boolean enable) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
      
    int result = _IS_NO_SUCCESS;  
    try
    {
      result = nativeEnableAutoGain(_cameraId.getId(), enable);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoGain(_cameraId.getId(), 
                                                                                 _cameraId.getDeviceId(), 
                                                                                 result,
                                                                                 SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                 "Failed to enable auto gain");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoGain(_cameraId.getId(),
                                                                                                                                         _cameraId.getDeviceId(), 
                                                                                                                                         result,
                                                                                                                                         SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                         "Failed to enable auto gain");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized int getMasterGainFactor() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeGetMasterGainFactor(_cameraId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetMasterGainFactor(_cameraId.getId(),
                                                                                                                                              _cameraId.getDeviceId(), 
                                                                                                                                              result,
                                                                                                                                              SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                              "Failed to get master gain factor");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
    return result;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void setMasterGainFactor(int gainFactor) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeSetMasterGainFactor(_cameraId.getId(), gainFactor);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToSetMasterGainFactor(_cameraId.getId(), 
                                                                                      _cameraId.getDeviceId(), 
                                                                                      result,
                                                                                      SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                      "Failed to set master gain factor");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetMasterGainFactor(_cameraId.getId(),
                                                                                                                                              _cameraId.getDeviceId(), 
                                                                                                                                              result,
                                                                                                                                              SentechOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                              "Failed to set master gain factor");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   * @edited hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  private void setConfigurationDescription()
  {
    List<String> parameters = new ArrayList<String>();

    parameters.add(_model);
    parameters.add(_firmwareVersion);
    parameters.add(_manufacturer);
    parameters.add(_fpgaVersion);

    _configDescription.clear();
    _configDescription.add(new HardwareConfigurationDescription("HW_SENTECH_OPTICAL_CAMERA_CONFIGURATION_KEY", parameters));
  }
  
  public void setDebugMode(boolean status)
  {
    Assert.expect(_cameraId != null);
    nativeSetDebugMode(_cameraId.getId(), status);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isTriggerModeReady()
  {
    Assert.expect(_cameraId != null);
    return nativeIsTriggerModeReady();
  }
  
  /**
   * @author Cheah Lee Herng
   * @ 
   */
  public synchronized void performCalibration(OpticalCalibrationData opticalCalibrationData) throws XrayTesterException
  {
    Assert.expect(opticalCalibrationData != null);
    
    int result = _IS_NO_SUCCESS;
    boolean isSuccess = false;  
    try
    {
      if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      {
        isSuccess = nativePerformCalibration(_cameraId.getId(), 
                                             opticalCalibrationData.getKnownHeight1(), 
                                             opticalCalibrationData.getKnownHeight2(), 
                                             opticalCalibrationData.getCalibrationDirectoryFullPath());
        result = _IS_SUCCESS;
      }
      else
      {
        isSuccess = nativePerformNewCalibration(_cameraId.getId(), 
                                                opticalCalibrationData.getKnownHeight1(), 
                                                opticalCalibrationData.getCalibrationDirectoryFullPath());
        result = _IS_SUCCESS;
      }
      if (isSuccess == false)
      {
        result = _IS_NO_SUCCESS;
        throw OpticalCameraHardwareException.getAxiOpticalFailedToPerformCalibration(_cameraId.getId(), 
                                                                                     _cameraId.getDeviceId(), 
                                                                                     result,
                                                                                     SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                     "Failed to perform calibration");
      }
    }
    catch (NativeHardwareException nhe)
    {
      result = _IS_NO_SUCCESS;
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToPerformCalibration(_cameraId.getId(),
                                                                                                                                              _cameraId.getDeviceId(), 
                                                                                                                                              result,
                                                                                                                                              SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                                                                              "Failed to perform calibration");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
    
 /**
  * By default fullPathFileName is NULL, means no images will be saved.
  * @author Jack Hwee
  */
  public synchronized void acquireObjectFringeImages(String fullPathFileName, int planeId, boolean forceTrigger) throws XrayTesterException
  {
    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      nativeAcquireObjectFringeImages(_cameraId.getId(), planeId, fullPathFileName, forceTrigger);
    else
    {
      int result = _IS_NO_SUCCESS;  
      try
      {
        result = nativeAcquireCalibrationImages(_cameraId.getId(), planeId, fullPathFileName, forceTrigger);
        if (result != _IS_SUCCESS)
          throw OpticalCameraHardwareException.getAxiOpticalFailedToAcquireCalibrationImages(_cameraId.getId(), 
                                                                                             _cameraId.getDeviceId(), 
                                                                                             result,
                                                                                             SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                             "Failed to acquire calibration images");
      }
      catch (NativeHardwareException nhe)
      {
        OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToAcquireCalibrationImages(_cameraId.getId(),
                                                                                                                                                     _cameraId.getDeviceId(), 
                                                                                                                                                     result,
                                                                                                                                                     SentechOpticalCameraStatusEnum.getStatusName(result), 
                                                                                                                                                     "Failed to acquire calibration images");
        opticalCameraHardwareException.initCause(nhe);
        _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
    }
  }
    
 /**
  * @author Jack Hwee
  */
  public synchronized void loadCalibrationData(String fullPath)
  {
    Assert.expect(_cameraId != null);
    Assert.expect(fullPath != null);

    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      nativeLoadCalibrationData(_cameraId.getId(), fullPath);
    else
      nativeLoadNewCalibrationData(_cameraId.getId(), fullPath);
  }
    
 /**
  * @author Jack Hwee
  */
  public synchronized void setInspectionROI(int roiCount, int[] centerX, int[] centerY, int[] width, int[] height)
  {
    Assert.expect(_cameraId != null);

    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))  
      nativeSetInspectionROI(_cameraId.getId(), roiCount, centerX, centerY, width, height);
    else
      nativeSetNewInspectionROI(_cameraId.getId(), roiCount, centerX, centerY, width, height);    
  }
    
  /**
  * @author Jack Hwee
  */
  public synchronized void generateHeightMap(OpticalInspectionData opticalInspectionData, String fullPathFileName, boolean returnActualHeightMapValue) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    Assert.expect(opticalInspectionData != null);

    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
    {
      _heightMapValue = nativeGenerateHeightMap(_cameraId.getId(), 
                                                opticalInspectionData.getOpticalCalibrationProfileEnum().getHeightAdjustment(), 
                                                fullPathFileName, 
                                                opticalInspectionData.getOpticalCalibrationProfileEnum().getOffset(), 
                                                opticalInspectionData.getForceTrigger());
      if (_heightMapValue.length == 0)
        _isHeightMapValueAvailable = false;
      else
        _isHeightMapValueAvailable = true;
    }
    else
    {
      _heightMapValue = nativeGenerateNewHeightMap(_cameraId.getId(), 
                                                   fullPathFileName, 
                                                   opticalInspectionData.getInspectionQualityThreshold(), 
                                                   opticalInspectionData.getRefLowerPhaseLimit(),
                                                   opticalInspectionData.getForceTrigger(),
                                                   returnActualHeightMapValue);
      if (_heightMapValue == null)
      {
        _isHeightMapValueAvailable = false;
        throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(opticalInspectionData.getPspModuleEnum());
      }
      else
      {
        if (_heightMapValue.length == 0)
          _isHeightMapValueAvailable = false;
        else
          _isHeightMapValueAvailable = true;
      }
    }
  }
    
  /**
   * @author Jack Hwee
   */
  public boolean isHeightMapValueAvailable()
  {
    return _isHeightMapValueAvailable;
  }

  /**
   * @author Jack Hwee
   */
  public void setHeightMapValueAvailable(boolean isHeightMapValueAvailable)
  {
    _isHeightMapValueAvailable = isHeightMapValueAvailable;
  }

  /**
   * @author Jack Hwee
   */
  public float[] getHeightMapValue()
  {
    Assert.expect(_heightMapValue.length > 0);
    return _heightMapValue;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public synchronized float[] getNewCoordinateXInPixel() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    return nativeGetNewCoordinateXInPixel(_cameraId.getId());
  }

  /**
   * @author Cheah Lee Herng
   */
  public synchronized float[] getNewCoordinateYInPixel() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    return nativeGetNewCoordinateYInPixel(_cameraId.getId());
  }

  /**
   * @author Jack Hwee
   */
  public synchronized void initializePspAlgorithm()
  {
    Assert.expect(_cameraId != null);
    nativeInitializePspAlgorithm(_cameraId.getId());
  }
    
  /**
   *  Provide magnification data to PSP Algorithm library so that when
   *  performing new point adjustment, it knows how to compensate back to the 
   *  correct plane.
   * 
   * @author Cheah Lee Herng
   */
  public synchronized void setMagnificationData(double[] magnificationData) throws XrayTesterException
  {
    Assert.expect(magnificationData.length == 12);    // We expect array size of 12

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeSetMagnificationData(_cameraId.getId(), magnificationData);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToSetMagnificationData(_cameraId.getId(), 
                                                                                       _cameraId.getDeviceId(),
                                                                                       "Failed to set magnification data");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetMagnificationData(_cameraId.getId(),
                                                                                                                                               _cameraId.getDeviceId(),
                                                                                                                                               "Failed to set magnification data");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public synchronized void generateOfflineHeightMap(com.axi.util.image.Image image,
                                                    com.axi.util.image.RegionOfInterest roi,
                                                    float floatAdjustment, 
                                                    int minusPlusSet) throws XrayTesterException
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);
    Assert.expect(_cameraId != null);

    _heightMapValue = nativeGenerateOfflineHeightMap(_cameraId.getId(), 
                                                     floatAdjustment, 
                                                     minusPlusSet, 
                                                     image.getNativeDefinition(), 
                                                     roi.getMinX(), 
                                                     roi.getMinY(), 
                                                     roi.getWidth(), 
                                                     roi.getHeight());
    if (_heightMapValue.length == 0)
      _isHeightMapValueAvailable = false;
    else
      _isHeightMapValueAvailable = true;
  }    
    
  private native int nativeInitialize(int cameraDeviceId, int windowHandle, boolean isMirrorMode) throws NativeHardwareException;  
  private native int nativeLiveVideo(int cameraDeviceId, int windowHandle) throws NativeHardwareException;
  private native int nativeGetWindowHandle(java.awt.Component window) throws NativeHardwareException;
  private native int nativeExitCamera(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeEnableHardwareTrigger(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeAcquireOfflineImages(int cameraDeviceId, String pathName, boolean forceTrigger) throws NativeHardwareException;
  private native int nativeGetNumberOfCameras() throws NativeHardwareException;
  private native int nativeEnableAutoGain(int cameraDeviceId, boolean enable) throws NativeHardwareException;
  private native int nativeGetMasterGainFactor(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeSetMasterGainFactor(int cameraDeviceId, int gainFactor) throws NativeHardwareException;
  private native static boolean nativeSetDebugMode(int cameraDeviceId, boolean status);
  private native static int nativeAcquireObjectFringeImages(int cameraDeviceId, int planeId, String pathName, boolean forceTrigger);
  private native static int nativeAcquireCalibrationImages(int cameraDeviceId, int planeId, String pathName, boolean forceTrigger) throws NativeHardwareException;
  private native static boolean nativePerformCalibration(int cameraDeviceId, double referencePlaneKnownHeight, double objectPlanePlusThreeKnownHeight, String fullPath) throws NativeHardwareException;
  private native static boolean nativePerformNewCalibration(int cameraDeviceId, double calibrationKnownHeight, String fullPath) throws NativeHardwareException;
  private native static boolean nativeLoadCalibrationData(int cameraDeviceId, String fullPath);
  private native static boolean nativeLoadNewCalibrationData(int cameraDeviceId, String fullPath);
  private native static boolean nativeSetInspectionROI(int cameraDeviceId, int roiCount, int[] centerX, int[] centerY, int[] width, int[] height);
  private native static boolean nativeSetNewInspectionROI(int cameraDeviceId, int roiCount, int[] centerX, int[] centerY, int[] width, int[] height);
  private native static float[] nativeGenerateHeightMap(int cameraDeviceId, float floatAdjustment, String fullPathFileName, int minusPlusSet, boolean forceTrigger);
  private native static float[] nativeGenerateNewHeightMap(int cameraDeviceId, String fullPathFileName, int inspQualityThreshold, int refLowerPhaseLimit, boolean forceTrigger, boolean returnActualHeightMapValue);
  private native static void nativeInitializePspAlgorithm(int cameraDeviceId);
  private native static float[] nativeGetNewCoordinateXInPixel(int cameraDeviceId);
  private native static float[] nativeGetNewCoordinateYInPixel(int cameraDeviceId);
  private native static int nativeSetMagnificationData(int cameraDeviceId, double[] magnificationData) throws NativeHardwareException;
  private native static float[] nativeGenerateOfflineHeightMap(int cameraDeviceId, float floatAdjustment, int minusPlusSet, int imageDefinition[], int roiX, int roiY, int roiWidth, int roiHeight); 
  private native static boolean nativeIsTriggerModeReady();
  private native static boolean nativeSetCropImageOffset(int cameraDeviceId, int frontModuleOffsetXInPixel, int rearModuleOffsetXInPixel);
  private native static boolean nativeSetImageSizeInPixel(int imageWidthInPixel, int imageHeightInPixel);
  //XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
  private native static int nativeGetFirmwareVersion(int cameraDeviceId) throws NativeHardwareException;
  private native static int nativeGetFpgaVersion(int cameraDeviceId) throws NativeHardwareException;
  private native static String nativeGetModel(int cameraDeviceId) throws NativeHardwareException;  

  public void enableAutoSensorGain(boolean enable) throws XrayTesterException 
  {
    //Not supported yet.
  }

  public void enableGainBoost(boolean enable) throws XrayTesterException 
  {
    //Not supported yet.
  }

  public void setFramesPerSecond(double framesPerSecond) throws XrayTesterException 
  {
    //Not supported yet.
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setCropImageOffset(int frontModuleOffsetXInPixel, int rearModuleOffsetXInPixel)
  {
    Assert.expect(_cameraId != null);
    nativeSetCropImageOffset(_cameraId.getId(), frontModuleOffsetXInPixel, rearModuleOffsetXInPixel);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setImageSizeInPixel(int imageWidthInPixel, int imageHeightInPixel)
  {
    Assert.expect(imageWidthInPixel >= 0);
    Assert.expect(imageHeightInPixel >= 0);
    
    nativeSetImageSizeInPixel(imageWidthInPixel, imageHeightInPixel);
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  public synchronized String getFirmwareVersion() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
      
    int firmwareVersion = 0;    
    try
    {
      firmwareVersion = nativeGetFirmwareVersion(_cameraId.getId());      
      if (firmwareVersion == _IS_NO_SUCCESS)
      {
        throw OpticalCameraHardwareException.getAxiOpticalFailedToGetFirmwareVersion(_cameraId.getId(),
                                                                                     _cameraId.getDeviceId(),
                                                                                     "Failed to get firmware version");
      }
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetFirmwareVersion(_cameraId.getId(),
                                                                                                                                             _cameraId.getDeviceId(),
                                                                                                                                             "Failed to get firmware version");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
      
    return String.format("%04X",firmwareVersion);
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  public synchronized String getFpgaVersion() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
      
    int fpgaVersion = 0;
    try
    {
      fpgaVersion = nativeGetFpgaVersion(_cameraId.getId());
      if (fpgaVersion == _IS_NO_SUCCESS)
      {
        throw OpticalCameraHardwareException.getAxiOpticalFailedToGetFpgaVersion(_cameraId.getId(),
                                                                                 _cameraId.getDeviceId(),
                                                                                 "Failed to get FPGA version");
      }
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetFpgaVersion(_cameraId.getId(),
                                                                                                                                         _cameraId.getDeviceId(),
                                                                                                                                         "Failed to get FPGA version");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
      
    return String.format("%04X",fpgaVersion);
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  public synchronized String getModel() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
      
    String model = "";
    try
    {
      model = nativeGetModel(_cameraId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetModel(_cameraId.getId(),
                                                                                                                                   _cameraId.getDeviceId(),
                                                                                                                                   "Failed to get model");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
      
    return model;
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  public synchronized String getManufacturer() throws XrayTesterException
  {
    return _SENTECH_MANUFACTURER_NAME;
  }
}


