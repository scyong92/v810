package com.axi.v810.hardware;

import java.rmi.*;

public interface RemoteNativeHardwareConfigurationInterface extends Remote
{
  public void remoteSetConfigurationParameters(String key, String value)  throws RemoteException;
}
