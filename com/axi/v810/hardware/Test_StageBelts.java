package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for stage belts
 * @author Rex Shang
 */
class Test_StageBelts extends UnitTest
{
  private StageBelts _stageBelts = null;

  private Test_StageBelts()
  {
    _stageBelts = StageBelts.getInstance();
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // set belt direction from left to right and verify
      _stageBelts.setDirectionLeftToRight();
      Expect.expect(_stageBelts.areStageBeltsDirectionLeftToRight() == true);

      // set belt direction from right to left and verify
      _stageBelts.setDirectionRightToLeft();
      Expect.expect(_stageBelts.areStageBeltsDirectionLeftToRight() == false);

      // turn on belt and verify
      _stageBelts.on();
      Expect.expect(_stageBelts.areStageBeltsOn() == true);

      // turn off belt and verify
      _stageBelts.off();
      Expect.expect(_stageBelts.areStageBeltsOn() == false);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    Test_StageBelts test_StageBelts = new Test_StageBelts();
    UnitTest.execute(test_StageBelts);
  }

}
