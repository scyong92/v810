package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for stage viewing light
 * @author Rex Shang
 */
class Test_StageViewingLight extends UnitTest
{
  private StageViewingLight _stageViewingLight = null;

  private Test_StageViewingLight()
  {
    _stageViewingLight = StageViewingLight.getInstance();
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // turn on light and verify
      _stageViewingLight.on();
      Expect.expect(_stageViewingLight.isOn() == true);

      // turn off light and verify
      _stageViewingLight.off();
      Expect.expect(_stageViewingLight.isOn() == false);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    Test_StageViewingLight test_StageViewingLight = new Test_StageViewingLight();
    UnitTest.execute(test_StageViewingLight);
  }
}
