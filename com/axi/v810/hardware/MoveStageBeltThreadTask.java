package com.axi.v810.hardware;

import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class MoveStageBeltThreadTask extends ThreadTask<Object>
{
  private HardwareTaskEngine _hardwareTaskEngine;
  private PanelHandler _panelHandler;
  private PanelClearSensor _panelClearSensor;
  private StageBelts _stageBelts;
  private LeftPanelClearSensor _leftPanelClearSensor;
  private BlockingQueue _stageBeltLockingQueue;

  /**
   * @author Rex Shang
   */
  MoveStageBeltThreadTask(PanelClearSensor panelClearSensor, BlockingQueue<Object> stageBeltLockingQueue)
  {
    super("Move stage belt thread task");
    Assert.expect(panelClearSensor != null);

    _stageBeltLockingQueue = stageBeltLockingQueue;
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _panelClearSensor = panelClearSensor;
    _leftPanelClearSensor = LeftPanelClearSensor.getInstance();
    _stageBelts = StageBelts.getInstance();
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    // Make sure the panel clear sensor is waiting first on the other thread.
    Object lock = null;
    while (lock == null)
    {
      try
      {
        lock = _stageBeltLockingQueue.take();
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
    }

    // Set the direction of travel.  Check to see if the sensor is unexpectedly
    // blocked.
    if (_panelClearSensor.equals(_leftPanelClearSensor))
    {
      if (_panelClearSensor.isBlocked())
        _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getLeftPanelClearSensorUnexpectedBlockedException());

      _stageBelts.setDirectionRightToLeft();
    }
    else
    {
      if (_panelClearSensor.isBlocked())
        _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getRightPanelClearSensorUnexpectedBlockedException());
      _stageBelts.setDirectionLeftToRight();
    }

    _stageBelts.on();

    // Panel has left the panel in place sensor, update information.
    _panelHandler.setPanelIsAgainst(null);
    _panelHandler.savePersistentPanelInformation();

    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelHandler.abort();
    _panelClearSensor.abort();
    _leftPanelClearSensor.abort();
    _stageBelts.abort();
  }
}
