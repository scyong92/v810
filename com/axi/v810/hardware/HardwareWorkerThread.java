package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class uses the WorkerThread class and wraps its invokeAndWait(RunnableWithExceptions)
 * call with one that only throws HardwareException instead of throwing Exception.  It
 * also uses getInstance() to make sure that all the hardware code is using the same
 * thread.
 *
 * @author Bill Darbie
 */
public class HardwareWorkerThread
{
  private static HardwareWorkerThread _instance;
  private static HardwareWorkerThread _instance2;
  private static HardwareWorkerThread _instance3;
  private static WorkerThread _workerThread = new WorkerThread("HardwareWorkerThread");
  private static WorkerThread _workerThread2 = new WorkerThread("HardwareWorkerThread");
  private static WorkerThread _workerThread3 = new WorkerThread("HardwareWorkerThread");
  /**
   * @author Bill Darbie
   */
  public static synchronized HardwareWorkerThread getInstance()
  {
    if (_instance == null)
      _instance = new HardwareWorkerThread();

    return _instance;
  }
  /**
 * @author YingKat Choor
 */
public static synchronized HardwareWorkerThread getInstance2()
{
  if (_instance2 == null)
    _instance2 = new HardwareWorkerThread();

  return _instance2;
}

/**
 * @author Cheah Lee Herng 
 */
public static synchronized HardwareWorkerThread getInstance3()
{
  if (_instance3 == null)
    _instance3 = new HardwareWorkerThread();

  return _instance3;
} 


  /**
   * @return true if the thread calling this method is this classes worker thread
   * @author Bill Darbie
   * @author YingKat Choor
   */
  public static synchronized boolean isHardwareWorkerThread()
  {
    if (Thread.currentThread() == _workerThread.getThread())
    {
      return true;
    }
    else if (Thread.currentThread() == _workerThread2.getThread())
    {
      return true;
    }
    else if (Thread.currentThread() == _workerThread3.getThread())
    {
      return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  private HardwareWorkerThread()
  {
    // do not allow the constructor to be used
  }

  /**
   * @author Bill Darbie
   * @author YingKat Choor
   */
  public void invokeAndWait(RunnableWithExceptions runnable) throws XrayTesterException
  {
    try
    {
      if(this == _instance)
      {
        _workerThread.invokeAndWait(runnable);
      }
      else if (this == _instance2)
      {
        _workerThread2.invokeAndWait(runnable);
      }
      else if (this == _instance3)
      {
        _workerThread3.invokeAndWait(runnable);
      }
    }
    catch(XrayTesterException ex)
    {
      throw ex;
    }
    catch (RuntimeException ex)
    {
      throw ex;
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Bill Darbie
   * @author YingKat Choor
   */
  public void invokeLater(Runnable runnable)
  {
    try
    {
      if (this == _instance)
      {
        _workerThread.invokeLater(runnable);
      }
      else if (this == _instance2)
      {
        _workerThread2.invokeLater(runnable);
      }
      else if (this == _instance3)
      {
        _workerThread3.invokeLater(runnable);
      }
    }
    catch(Throwable ex)
    {
      Assert.logException(ex);
    }
  }
}
