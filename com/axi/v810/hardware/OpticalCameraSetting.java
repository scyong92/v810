package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * This class encapsulates all the optical camera settings needed.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalCameraSetting 
{
  private static final int DEFAULT_MAGNIFICATION_DATA_COUNT = 12;
  private static final int DEFAULT_ROI_COUNT                = 0;
  private static final double DEFAULT_FRAME_RATE_PER_SECOND = 60.0;
  
  private double[] _magnificationData;
  
  // Image ROI properties
  private int _roiCount;
  private int[] _roiCenterX;
  private int[] _roiCenterY;
  private int[] _roiWidth;
  private int[] _roiHeight;
  
  private double _frameRatePerSecond;
  
  private boolean _enableAutoGain;
  private boolean _enableAutoSensorGain;
  private boolean _enableGainBoost;
  
  private int _frontModuleCropImageOffsetXInPixel;
  private int _rearModuleCropImageOffsetXInPixel;
  
  private boolean _liveViewMode;
  
  /**
   * Default Constructor
   * 
   * @author Cheah Lee Herng
   */
  public OpticalCameraSetting()
  {
    _magnificationData = new double[DEFAULT_MAGNIFICATION_DATA_COUNT];
    
    _roiCount = DEFAULT_ROI_COUNT;
    _roiCenterX = new int[DEFAULT_ROI_COUNT];
    _roiCenterY = new int[DEFAULT_ROI_COUNT];
    _roiWidth = new int[DEFAULT_ROI_COUNT];
    _roiHeight = new int[DEFAULT_ROI_COUNT];
    
    _frameRatePerSecond = DEFAULT_FRAME_RATE_PER_SECOND;
    
    _enableAutoGain = false;
    _enableAutoSensorGain = false;
    _enableGainBoost = false;
    
    _frontModuleCropImageOffsetXInPixel = 0;
    _rearModuleCropImageOffsetXInPixel = 0;
    
    _liveViewMode = false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraSetting(int frontModuleCropImageOffsetInPixel,
                              int rearModuleCropImageOffsetInPixel)
  {
    _magnificationData = new double[DEFAULT_MAGNIFICATION_DATA_COUNT];
    
    _roiCount = DEFAULT_ROI_COUNT;
    _roiCenterX = new int[DEFAULT_ROI_COUNT];
    _roiCenterY = new int[DEFAULT_ROI_COUNT];
    _roiWidth = new int[DEFAULT_ROI_COUNT];
    _roiHeight = new int[DEFAULT_ROI_COUNT];
    
    _frameRatePerSecond = DEFAULT_FRAME_RATE_PER_SECOND;
    
    _enableAutoGain = false;
    _enableAutoSensorGain = false;
    _enableGainBoost = false;
    
    _frontModuleCropImageOffsetXInPixel = frontModuleCropImageOffsetInPixel;
    _rearModuleCropImageOffsetXInPixel = rearModuleCropImageOffsetInPixel;
    
    _liveViewMode = false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraSetting(int frontModuleCropImageOffsetInPixel,
                              int rearModuleCropImageOffsetInPixel,
                              boolean liveViewMode)
  {
    _magnificationData = new double[DEFAULT_MAGNIFICATION_DATA_COUNT];
    
    _roiCount = DEFAULT_ROI_COUNT;
    _roiCenterX = new int[DEFAULT_ROI_COUNT];
    _roiCenterY = new int[DEFAULT_ROI_COUNT];
    _roiWidth = new int[DEFAULT_ROI_COUNT];
    _roiHeight = new int[DEFAULT_ROI_COUNT];
    
    _frameRatePerSecond = DEFAULT_FRAME_RATE_PER_SECOND;
    
    _enableAutoGain = false;
    _enableAutoSensorGain = false;
    _enableGainBoost = false;
    
    _frontModuleCropImageOffsetXInPixel = frontModuleCropImageOffsetInPixel;
    _rearModuleCropImageOffsetXInPixel = rearModuleCropImageOffsetInPixel;
    
    _liveViewMode = liveViewMode;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraSetting(double[] magnificationData, 
                              int roiCount, 
                              int[] roiCenterX, 
                              int[] roiCenterY, 
                              int[] roiWidth, 
                              int[] roiHeight,
                              double frameRatePerSecond,
                              boolean enableAutoGain,
                              boolean enableAutoSensorGain,
                              boolean enableGainBoost,
                              int frontModuleCropImageOffsetInPixel,
                              int rearModuleCropImageOffsetInPixel)
  {
    _magnificationData = magnificationData;
    
    _roiCount = roiCount;
    _roiCenterX = roiCenterX;
    _roiCenterY = roiCenterY;
    _roiWidth = roiWidth;
    _roiHeight = roiHeight;
    
    _frameRatePerSecond = frameRatePerSecond;
    
    _enableAutoGain = enableAutoGain;
    _enableAutoSensorGain = enableAutoSensorGain;
    _enableGainBoost = enableGainBoost;
    
    _frontModuleCropImageOffsetXInPixel = frontModuleCropImageOffsetInPixel;
    _rearModuleCropImageOffsetXInPixel = rearModuleCropImageOffsetInPixel;
    
    _liveViewMode = false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraSetting(int roiCount, 
                              int[] roiCenterX, 
                              int[] roiCenterY, 
                              int[] roiWidth, 
                              int[] roiHeight,
                              double frameRatePerSecond,
                              boolean enableAutoGain,
                              boolean enableAutoSensorGain,
                              boolean enableGainBoost,
                              int frontModuleCropImageOffsetInPixel,
                              int rearModuleCropImageOffsetInPixel)
  {
    _magnificationData = null;
    
    _roiCount = roiCount;
    _roiCenterX = roiCenterX;
    _roiCenterY = roiCenterY;
    _roiWidth = roiWidth;
    _roiHeight = roiHeight;
    
    _frameRatePerSecond = frameRatePerSecond;
    
    _enableAutoGain = enableAutoGain;
    _enableAutoSensorGain = enableAutoSensorGain;
    _enableGainBoost = enableGainBoost;
    
    _frontModuleCropImageOffsetXInPixel = frontModuleCropImageOffsetInPixel;
    _rearModuleCropImageOffsetXInPixel = rearModuleCropImageOffsetInPixel;
    
    _liveViewMode = false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public double[] getMagnificationData()
  {
    Assert.expect(_magnificationData != null);
    return _magnificationData;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getRoiCount()
  {
    return _roiCount;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int[] getRoiCenterX()
  {
    Assert.expect(_roiCenterX != null);
    return _roiCenterX;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int[] getRoiCenterY()
  {
    Assert.expect(_roiCenterY != null);
    return _roiCenterY;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int[] getRoiWidth()
  {
    Assert.expect(_roiWidth != null);
    return _roiWidth;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int[] getRoiHeight()
  {
    Assert.expect(_roiHeight != null);
    return _roiHeight;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getFrameRatePerSecond()
  {
    return _frameRatePerSecond;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isAutoGainEnabled()
  {
    return _enableAutoGain;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isAutoSensorGainEnabled()
  {
    return _enableAutoSensorGain;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isGainBoostEnabled()
  {
    return _enableGainBoost;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasMagnificationData()
  {
    if (_magnificationData == null || _magnificationData.length == 0)
      return false;
    else
      return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasRoiData()
  {
    if (_roiCount == 0 || 
        _roiCenterX == null || _roiCenterX.length == 0 ||
        _roiCenterY == null || _roiCenterY.length == 0 ||
        _roiWidth == null || _roiWidth.length == 0 ||
        _roiHeight == null || _roiHeight.length == 0)
    {
      return false;
    }
    else
      return true;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getFrontModuleCropImageOffsetXInPixel()
  {
    return _frontModuleCropImageOffsetXInPixel;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRearModuleCropImageOffsetXInPixel()
  {
    return _rearModuleCropImageOffsetXInPixel;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isLiveViewMode()
  {
    return _liveViewMode;
  }
}
