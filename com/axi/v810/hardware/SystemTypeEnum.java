package com.axi.v810.hardware;

import com.axi.util.*;

import java.util.*;

/**
 * Enumerates all of the available system types.
 *
 * @author George A. David
 */
public class SystemTypeEnum extends com.axi.util.Enum
{
  public static int _index =-1;
  private static Map<String, SystemTypeEnum> _nameToSystemTypeEnumMap = new HashMap<String, SystemTypeEnum>();
  public static final SystemTypeEnum STANDARD = new SystemTypeEnum(++_index, "standard");
  public static final SystemTypeEnum THROUGHPUT = new SystemTypeEnum(++_index, "throughput");
  public static final SystemTypeEnum XXL = new SystemTypeEnum(++_index, "XXL");
  public static final SystemTypeEnum S2EX = new SystemTypeEnum(++_index, "S2EX"); //Ngie Xing, added for Series 3
//  public static final SystemTypeEnum XXL_S2EX = new SystemTypeEnum(++_index, "XXL_S2EX");

  //Ngie Xing, XCR-2455 - Assert when open wrong system type recipes 
  //there will be times when a software of an older version loads a recipe from a later version software
  //Or when we/customer chooose to edit project.X.settings manually and got it wrong
  //with this we will set it to "UNKNOWN" system type instead of letting the software crash
  //this will not be included in _nameToSystemTypeEnumMap
  public static final String _UNKNONWN_TYPE = "UNKNOWN";
  public static final SystemTypeEnum UNKNOWN = new SystemTypeEnum(++_index, _UNKNONWN_TYPE);
  
  private String _name;

  /**
   * @author George A. David
   */
  private SystemTypeEnum(int id, String name)
  {
    super(id);

    Assert.expect(name != null);
    _name = name;

    //Ngie Xing, XCR-2455 - Assert when open wrong system type recipes
    //do not include unknown type in _nameToSystemTypeEnumMap because this is not an official System Type,
    //anything in this map will need to have their own set of hardware calib settings
    if(_name != _UNKNONWN_TYPE)
    {
      _nameToSystemTypeEnumMap.put(_name, this);
    }
    
  }

  /**
   * @author George A. David
   */
  public static List<SystemTypeEnum> getAllSystemTypes()
  {
    Assert.expect(_nameToSystemTypeEnumMap != null);
    return new ArrayList<SystemTypeEnum>(_nameToSystemTypeEnumMap.values());
  }

  /**
   * @author George A. David
   */
  public static SystemTypeEnum getSystemType(String name)
  {
    Assert.expect(name != null);

    SystemTypeEnum systemType = _nameToSystemTypeEnumMap.get(name);

    //Ngie Xing, XCR-2455 - Assert when open wrong system type recipes
    //if the systemType is null, set it to UNKNOWN
    if(systemType == null)
      systemType = UNKNOWN;

    Assert.expect(systemType != null);
    return systemType;
  }

  /**
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Poh Kheng
   */
  public String toString()
  {
    Assert.expect(_name != null);

    return _name;
  }
}
