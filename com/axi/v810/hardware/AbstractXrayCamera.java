package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public abstract class AbstractXrayCamera extends HardwareObject implements HardwareStartupShutdownInt, Comparable<AbstractXrayCamera>
{
  private static final String _XRAY_CAMERA_LOG_NAME_PREFIX = "Camera";
  private static Map<XrayCameraIdEnum, AbstractXrayCamera> _idToInstanceMap;
  private String _name;
  protected static ProgressObservable _progressObservable;
  protected static Config _config;
  protected static XrayCameraModelTypeEnum _modelType;
  protected HardwareObservable _hardwareObservable;
  protected int _systemPositionDistanceFromOriginInNanometers;
  protected IntCoordinate _systemPositionCoordinatesInNanometers;
  protected boolean _initialized;
  protected int _numberOfTriggersToCompleteAcquisition;
  protected XrayCameraIdEnum _cameraId;
  protected HardwareTaskEngine _hardwareTaskEngine;
  protected XrayCameraCalibrationData _calibrationData;
  protected XrayCameraCalibrationData _lowMagCalibrationData;
  protected XrayCameraCalibrationData _highMagCalibrationData;

  protected List<HardwareConfigurationDescriptionInt> _configDescription;
  protected XrayCameraLogUtil _xRayCameraLogUtil;
  protected String _xRayCameraLogName;
  protected int _maxImageCaptureBuffers;
  
  // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  // set the backward compatible target camera properties
  protected static int _BACKWARD_COMPATIBLE_SENSOR_NUMBER_OF_SEGMENTS = 4;
  protected static int _BACKWARD_COMPATIBLE_SENSOR_NUMBER_OF_UNUSED_PIXELS = 4;
  protected static int _BACKWARD_COMPATIBLE_SENSOR_WIDTH_IN_PIXELS = 1088;
  protected static int _BACKWARD_COMPATIBLE_SENSOR_COLUMN_PITCH_IN_NANOMETERS = 100000;
  protected static int _BACKWARD_COMPATIBLE_SENSOR_ROW_PITCH_IN_NANOMETERS = 100000;
  protected static int _BACKWARD_COMPATIBLE_SENSOR_NUMBER_OF_ROWS = 24;
  protected static int _BACKWARD_COMPATIBLE_MAXIMUM_NUMBER_OF_IMAGES_CAN_STORE = 6;
  protected static int _BACKWARD_COMPATIBLE_NUMBER_OF_TRIGGERS_TO_COMPLETE_ACQUISITION = 3;
  protected static int _BACKWARD_COMPATIBLE_EXTRA_DELAY_TO_MAKE_HYSTERESIS_MORE_POSITIVE_IN_NANOMETERS = 72000;
  protected static int _BACKWARD_COMPATIBLE_BINNING_MODE = 1;
  protected static int _BACKWARD_COMPATIBLE_PIXEL_RESOLUTION_IN_BIT = 8;
  protected static int _BACKWARD_COMPATIBLE_CAMERA_TRANSFER_RATE = 256;
  protected static int _BACKWARD_COMPATIBLE_CONVOLUTION_FILTER = 0;
  protected static int _BACKWARD_COMPATIBLE_MEDIAN_FILTER = 0;
  protected static int _BACKWARD_COMPATIBLE_FLAT_FIELDING = 0;
  

  /**
   * @author George A. David
   * @author Roy Williams
   * @author Greg Esparza
   */
  static
  {
    _progressObservable = ProgressObservable.getInstance();
    _idToInstanceMap = new HashMap<XrayCameraIdEnum, AbstractXrayCamera>();
    _config = Config.getInstance();
  }

  /**
   * @author George A. David
   * @author Greg Esparza
   */
  public synchronized static AbstractXrayCamera getInstance(XrayCameraIdEnum cameraId, IntCoordinate systemPositionInNanometers)
  {
    Assert.expect(cameraId != null);
    Assert.expect(systemPositionInNanometers != null);

    AbstractXrayCamera instance = null;

    _modelType = getModelType();

    if (_idToInstanceMap.containsKey(cameraId))
    {
      instance = _idToInstanceMap.get(cameraId);
    }
    else
    {
      if (_modelType.equals(XrayCameraModelTypeEnum.AXI_TDI))
      {
        instance = AxiTdiXrayCamera.getAnInstance(cameraId, systemPositionInNanometers);
      }
      else
      {
        Assert.expect(false);
      }
    }

    Assert.expect(instance != null);
    _idToInstanceMap.put(cameraId, instance);

    return instance;
  }

  /**
   * @author Anthony Fong
   */
  public abstract void setUserGain(double gain) throws XrayTesterException;

  public abstract void setUserOffset(double gain) throws XrayTesterException;

  public abstract double getUserGain() throws XrayTesterException;

  public abstract double getUserOffset() throws XrayTesterException;

  /**
   * @author Anthony Fong
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   */
  public abstract String getServerIpAddress();

  public abstract String getServerMacAddress();
  /**
   * @author Wei Chin
   */
  public abstract void setUserDefinedLightSensitivity() throws XrayTesterException;

  /**
   * @author Wei Chin
   */
  public abstract void setDefaultLightSensitivity() throws XrayTesterException;

  public abstract void setCameraUserGainAndOffset(double userGain, double userOffset ) throws XrayTesterException;
  /**
   * @author Greg Esparza
   */
  public abstract boolean isCalibrated() throws XrayTesterException;


  /**
   * @author Greg Esparza
   */
  public abstract int getNumberOfLineScanImagesAcquired() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract Image getLineScanImage() throws XrayTesterException;

  /**
   * @author George A. David
   */
  public abstract Image getAreaModeImage() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void setScan(List<? extends ScanPass> scanPath,
                               int numberOfLinesToSkipDuringPositiveYscan,
                               int numberOfLinesToSkipDuringNegativeYscan,
                               List<ProjectionSettings> projectionSettingsList) throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void enableTriggerDetection() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void disableTriggerDetection() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract boolean isTriggerDetectionEnabled() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void runSensorSegmentOffsetCalibration() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void runSensorSegmentGainCalibration() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void runSensorPixelOffsetCalibration() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void runSensorPixelGainCalibration() throws XrayTesterException;

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  public abstract void runSaveVariableMagCalibrationConfiguration(String configFileName) throws XrayTesterException;
  public abstract void runLoadVariableMagCalibrationConfiguration(String configFileName) throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void enableImageCorrection() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void disableImageCorrection() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract boolean isReadyForAcquisition() throws XrayTesterException;

  /**
   * @author George A. David
   */
  public abstract void initializeAcquisition() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract XrayCameraCalibrationData getCalibrationData() throws XrayTesterException;

  /**
   * @author Anthony Fong
   */
  public abstract XrayCameraCalibrationData getLowMagCalibrationData() throws XrayTesterException;
  public abstract XrayCameraCalibrationData setLowMagCalibrationData() throws XrayTesterException;

  /**
   * @author Anthony Fong
   */
  public abstract XrayCameraCalibrationData getHighMagCalibrationData() throws XrayTesterException;
  public abstract XrayCameraCalibrationData setHighMagCalibrationData() throws XrayTesterException;
  
  public abstract void setCalPointValuesForVariableMag(boolean isLowMag) throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void enableUnitTestMode(String imageDataTestDirectory) throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public abstract void disableUnitTestMode();

  /**
   * @author Greg Esparza
   */
  public abstract CommunicationType getCommunicationType();

  /**
   * @author Greg Esparza
   */
  public abstract int getNumberOfTriggersRequiredToCompleteAcquisition();

  /**
   * @author Greg Esparza
   */
  public abstract int getMaximumNumberOfImagesCameraCanStore();

  /**
   * @author Greg Esparza
   */
  public abstract int getNumberOfUnusedPixelsOnEndOfSensor();

  /**
   * @author Greg Esparza
   */
  public abstract int getNumberOfSensorPixelsUsed();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getSensorWidthInPixels();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getSensorWidthInPixelsWithOverHeadByte();

  /**
   * @author Greg Esparza
   */
  public abstract int getSensorColumnPitchInNanometers();

  /**
   * @author Greg Esparza
   */
  public abstract int getSensorRowPitchInNanometers();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getNumberOfSensorRows();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getNumberOfSensorRowsWithOverHeadByte();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getNumberOfSensorRowsUsed();

  /**
   * @author Greg Esparza
   */
  public abstract int getSensorWidthInNanometers();

  /**
   * @author Greg Esparza
   */
  public abstract int getSensorHeightInNanometers();
  
  /**
   * @author Eric Littlefield
   */
  public abstract int getFreeImageBufferCount() throws XrayTesterException;


  /**
   * @author Greg Esparza
   */
  public List<HardwareConfigurationDescriptionInt> getConfigurationDescription()
  {
    Assert.expect(_configDescription != null);
    Assert.expect(_configDescription.isEmpty() == false);

    return _configDescription;
  }

  /**
   * @author Bill Darbie
   */
  public int getId()
  {
    return _cameraId.getId();
  }

  /**
   * @author Greg Esparza
   */
  public int compareTo(AbstractXrayCamera rhs)
  {
    return getId() - rhs.getId();
  }

  /**
   * @author Greg Esparza
   */
  public String toString()
  {
    return _name;
  }
  
  public int getMaxImageBufferCount()
  {
    return _maxImageCaptureBuffers;
  }
  

  /**
   * @author Greg Esparza
   */
  protected AbstractXrayCamera(XrayCameraIdEnum cameraId, IntCoordinate systemPositionInNanometers)
  {
    Assert.expect(cameraId != null);
    Assert.expect(systemPositionInNanometers != null);

    _cameraId = cameraId;
    _calibrationData = new XrayCameraCalibrationData(_cameraId);
    _lowMagCalibrationData = new XrayCameraCalibrationData(_cameraId);
    _highMagCalibrationData = new XrayCameraCalibrationData(_cameraId);
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _configDescription = new ArrayList<HardwareConfigurationDescriptionInt>();
    _systemPositionCoordinatesInNanometers = systemPositionInNanometers;
    _xRayCameraLogUtil = new XrayCameraLogUtil(_cameraId);
    _xRayCameraLogName = _XRAY_CAMERA_LOG_NAME_PREFIX + " " + String.valueOf(_cameraId.getId());
    _name = StringLocalizer.keyToString(new LocalizedString("HW_XRAY_CAMERA_NAME_KEY", null)) + " " + _cameraId.getId();;
    setSystemPositionDistanceFromOriginInNanometers();
  }

  /**
   * @author Greg Esparza
   */
  private void setSystemPositionDistanceFromOriginInNanometers()
  {
    _systemPositionDistanceFromOriginInNanometers = (int)MathUtil.calculateDistance(0, 0, _systemPositionCoordinatesInNanometers.getX(), _systemPositionCoordinatesInNanometers.getY());
  }

  /**
   * @author Greg Esparza
   */
  private static XrayCameraModelTypeEnum getModelType()
  {
    String modelTypeStr = _config.getStringValue(HardwareConfigEnum.XRAY_CAMERA_MODEL);
    return XrayCameraModelTypeEnum.getEnum(modelTypeStr);
  }

  /**
   * @author Kay Lannen
   */
  public abstract boolean updateCameraFirmwareRequired() throws XrayTesterException;

  /**
   * @author Ronald Lim
   * @edited By Kee Chin Seong - Force Update Firmware
   */
  public abstract void updateCameraFirmware (boolean isForceUpdate) throws XrayTesterException;

  /**
   * @author Ronald Lim
   */
  public abstract void reboot() throws XrayTesterException;

  /**
   * @author sham
   */
  public abstract void setUserDefinedLightSensitivity(double userGain) throws XrayTesterException;
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract String getUpgradingInfoMessage();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getCameraBinningMode();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getCameraMaxPocketHeightInPixels();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getCameraReadingPositionFromMaxPocketHeightInPixels();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getCameraExtraDelayToMakeHysteresisMorePositiveInNanometers();

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract int getCameraSensorNumberOfSegments();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract String getCameraBoardVersion();
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract void enableSafeGuards() throws XrayTesterException;
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract void disableSafeGuards() throws XrayTesterException;

  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract void saveUncalibratedLogFiles(String targetLogDirectory, int frequencyTimeIntervalInMilliseconds, int maxLogFiles) throws DatastoreException, XrayTesterException;
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract void enableSaveCalibrationLog() throws XrayTesterException;
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public abstract void disableSaveCalibrationLog() throws XrayTesterException;
}
