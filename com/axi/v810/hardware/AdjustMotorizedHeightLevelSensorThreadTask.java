package com.axi.v810.hardware;

import com.axi.util.*;
import static com.axi.v810.hardware.XrayTester.isLowMagnification;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class AdjustMotorizedHeightLevelSensorThreadTask extends ThreadTask<Object>
{
  //private HardwareTaskEngine _hardwareTaskEngine;
  boolean _isHighMag = false;
  /**
   * @author Rex Shang
   */
  AdjustMotorizedHeightLevelSensorThreadTask(boolean isHighMag)
  {
    super("Adjust Motorized Height Level Sensor Thread Task");
    _isHighMag = isHighMag;
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    //This must be called after _magnification value has been changed
    if(XrayMotorizedHeightLevelSensor.isInstalled())
    {  
      try
      {
          //For double safety purpose we need to check and confirm 
          //to check low magnification 19 micron need move up for low mag
          if(_isHighMag)
          {
            //Need to go Down for High Mag
            XrayMotorizedHeightLevelSensor.getInstance().down(false);
          }
          else
          {
            //Need to go Up for Low Mag
            XrayMotorizedHeightLevelSensor.getInstance().up(false);
          }      
      }
      catch (XrayTesterException xte)
      {            
        throw xte;
      }
    }

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Rex Shang
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
