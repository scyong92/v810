package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
* This is a unit test for the LightStack class
* @author Tony Turner
*/
public class Test_LightStack extends UnitTest
{
  private LightStack _lightStack;

  public static void main(String[] args)
  {
    Test_LightStack testLightStack = new Test_LightStack();
    UnitTest.execute (testLightStack);
  }

  public void test(BufferedReader parm1, PrintWriter parm2)
  {
    _lightStack = LightStack.getInstance();

    try
    {
      // turn on red light
      _lightStack.turnOnRedLight();
      // test to see that the _lightStack thinks that the red light is on
      Expect.expect (_lightStack.isRedLightOn() == true);
      // turn off red light
      _lightStack.turnOffRedLight();
      // test to see that the _lightStack thinks that the red light is on
      Expect.expect (_lightStack.isRedLightOn() == false);

      // turn on yellow light
      _lightStack.turnOnYellowLight();
      // test to see that the _lightStack thinks that the yellow light is on
      Expect.expect (_lightStack.isYellowLightOn() == true);
      // turn off red light
      _lightStack.turnOffYellowLight();
      // test to see that the _lightStack thinks that the yellow light is on
      Expect.expect (_lightStack.isYellowLightOn() == false);

      // turn on green light
      _lightStack.turnOnGreenLight();
      // test to see that the _lightStack thinks that the green light is on
      Expect.expect (_lightStack.isGreenLightOn() == true);
      // turn off green light
      _lightStack.turnOffGreenLight();
      // test to see that the _lightStack thinks that the green light is on
      Expect.expect (_lightStack.isGreenLightOn() == false);

      if (_lightStack.isBuzzerEnabled())
      {
        // turn on blue light
        _lightStack.turnBuzzerOn();
        // test to see that the _lightStack thinks that the blue light is on
        Expect.expect (_lightStack.isBuzzerOn() == true);
        // turn off blue light
        _lightStack.turnBuzzerOff();
        // test to see that the _lightStack thinks that the blue light is on
        Expect.expect (_lightStack.isBuzzerOn() == false);
      }

    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
