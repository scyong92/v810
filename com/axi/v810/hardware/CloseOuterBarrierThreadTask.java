package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class CloseOuterBarrierThreadTask extends ThreadTask<Object>
{
  private HardwareTaskEngine _hardwareTaskEngine;
  private BarrierInt _outerBarrier;
  private PanelClearSensor _panelClearSensor;

  /**
   * @author Rex Shang
   */
  CloseOuterBarrierThreadTask(BarrierInt outerBarrier)
  {
    super("Close outer barrier thread task");
    Assert.expect(outerBarrier != null, "outerBarrier is null.");

    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _outerBarrier = outerBarrier;

    if (_outerBarrier.equals(LeftOuterBarrier.getInstance()))
      _panelClearSensor = LeftPanelClearSensor.getInstance();
    else if (_outerBarrier.equals(RightOuterBarrier.getInstance()))
      _panelClearSensor = RightPanelClearSensor.getInstance();
    else
      Assert.expect(false, "BarrierInt passed in is neither left nor right outer barrier.");
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    if (_outerBarrier.isOpen())
    {
      // Kok Chun, Tan - XCR-3565 - check smema sensor signal during close outer barrier
      if (PanelHandler.isCheckSmemaSensorSignalDuringUnloadingModeEnabled() && 
          PanelHandler.isInLineModeEnabled() &&
          PanelHandler.getInstance().isDownstreamSameSideWithClosingOuterBarrier(_outerBarrier))
      {
        ManufacturingEquipmentInterface.getInstance().waitUntilDownstreamSensorSignalClear(
          ManufacturingEquipmentInterface._WAIT_FOR_SMEMA_CLEAR_TIME_OUT_IN_MILLI_SECONDS, false, _panelClearSensor);
      }
      else
      {
        if (_panelClearSensor.isBlocked())
        {
          if (_outerBarrier.equals(LeftOuterBarrier.getInstance()))
            _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getLeftPanelClearSensorUnexpectedBlockedException());
          else
            _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getRightPanelClearSensorUnexpectedBlockedException());
        }
      }
      
      _outerBarrier.close();
    }

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Rex Shang
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
