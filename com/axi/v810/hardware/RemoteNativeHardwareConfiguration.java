package com.axi.v810.hardware;

import java.rmi.*;
import java.rmi.server.*;

/**
 * @author Rick
 */
public class RemoteNativeHardwareConfiguration extends UnicastRemoteObject implements
    RemoteNativeHardwareConfigurationInterface
{
  protected static boolean _debug = false;

  /**
   * @author Rick Gaudette
   */
  public RemoteNativeHardwareConfiguration() throws RemoteException
  {
    // do nothing
  }

  /**
   * @param key
   * @param value
   * @author Chong, Wei Chin
   */
  public void remoteSetConfigurationParameters(String key, String value)
  {
    vprint("remoteSetConfigurationParameters");
    nativeSetConfigurationParameters(key, value);
  }

  // Define Native Interface
  private native void nativeSetConfigurationParameters(String key, String value);

  /**
   * @param message
   * @author Chong, Wei Chin
   */
  private void vprint(String message)
  {
    if(_debug)
      System.err.println(message);
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */  
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
}
