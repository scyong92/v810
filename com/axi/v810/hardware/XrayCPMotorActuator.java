package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the Xray CP Motor Actuator.  The Xray CP Motor Actuator is used to control
 * x-ray up and down movement.  For Variable Magnification, the x-ray tube is able
 * to move up and down, to change the system magnification factor.
 * @author Anthony Fong
 */
public class XrayCPMotorActuator extends HardwareObject implements XrayActuatorInt
{
  private static XrayCPMotorActuator _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the X-Ray CPMotor is up or down.
  private boolean _xrayCPMotorIsDown;
  private boolean _xrayCPMotorIsNotUp;
  private boolean _xrayCPMotorIsUp;
  private boolean _xrayCPMotorIsUpToPosition1;
  private boolean _xrayCPMotorIsUpToPosition2;
  private boolean _xrayCPMotorIsNotUpToPosition1;
  private boolean _xrayCPMotorIsNotUpToPosition2;
  private boolean _xrayCPMotorIsNotDown;  
  private boolean _xrayCPMotorIsHome;
  private boolean _xrayCPMotorIsNotHome;
  private boolean _xrayCPMotorIsNotDownAndUp;
  private static boolean _WAIT_NOT_ABORTABLE = false;

  private static boolean _homeSuccessfully = false;
 
  private HTubeXraySource _xraySource = null;

  //Variable Mag Anthony August 2011
  private InnerBarrier _innerBarrier = null;

 
  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private XrayCPMotorActuator()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
    
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _xraySource = (HTubeXraySource)xraySource;
    _innerBarrier = InnerBarrier.getInstance();
    
  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized XrayCPMotorActuator getInstance()
  {
    if (_instance == null)
      _instance = new XrayCPMotorActuator();

    return _instance;
  }
  /**
   * Turn Off the X-Ray.
   * @author Anthony Fong
   */
  public void turnOffXRays(boolean serviceMode) throws XrayTesterException
  {
    _xraySource.turnOffXRays(serviceMode);
  }
  /**
   * Turn On the X-Ray.
   * @author Anthony Fong
   */
  public void turnOnImagingXRays() throws XrayTesterException
  {
    _xraySource.turnOnImagingXRays();
  }
  
  /**
   * Move the X-Ray CPMotor down (High Mag).
   * @author Anthony Fong
   */
  public void down(boolean loadConfig) throws XrayTesterException
  { 
    if(_homeSuccessfully == false && isSimulationModeOn() == false)
    {
      logHardwareStatus("ERROR - Xray tube: Xray tube failed to go high mag. Previous X-ray tube homing is not successful, please initialize Digital IO again.");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisFailedToGoHomeException());
    }
  
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupHighMagImagingXrayParameters();
        XrayCameraArray.getInstance().loadHighMagGrayScale();            
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForHighMag();
      }
    }
    
//    System.out.println("CPMotor Down");
    boolean initStateIsDown = isDown();
    if(initStateIsDown)
      return;
    
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
        _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HIGH_MAG);
        _hardwareObservable.setEnabled(false);

        try
        {
          // Time the close action.
          _hardwareActionTimer.reset();
          _hardwareActionTimer.start();

          _digitalIo.moveZAxisToHighMagPosition();
          if (_digitalIo.isInnerBarrierCommandedClose())
          {
            logHardwareStatus("WARNING: Xray tube failed to go high mag. Inner barrier is commanded closed in other thread, xray tube movement is aborted.");
            printXrayTubePositionStatus();
            printInnerBarrierPositionStatus();
            return;
          }
          waitUntilHighMag();
//          if(!initStateIsDown)
//          {
//            sleep(500);
//          }
          if (isSimulationModeOn())
          {
            updateSimulationSensorStatusForDown();
          }
        }
        finally
        {
          _hardwareActionTimer.stop();
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HIGH_MAG);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go high mag. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }

  /**
   * Move the X-Ray CPMotor up.
   * @author Anthony Fong
   */
  public void up(boolean loadConfig) throws XrayTesterException
  {
    if(_homeSuccessfully == false && isSimulationModeOn() == false)
    {
      logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag. Previous X-ray tube homing is not successful, please initialize Digital IO again.");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisFailedToGoHomeException());
    }
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupLowMagImagingXrayParameters();      
        XrayCameraArray.getInstance().loadLowMagGrayScale();        
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
      }
    }
    
//    System.out.println("CPMotor Up");
    boolean initStateIsUp = isUp();

    if(initStateIsUp)
      return;
        
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
      _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG);
      _hardwareObservable.setEnabled(false);

      try
      {
        // Time the open action.
        _hardwareActionTimer.reset();
        _hardwareActionTimer.start();
        _digitalIo.moveZAxisToLowMagPosition();
        if(_digitalIo.isInnerBarrierCommandedClose())
        {
          logHardwareStatus("WARNING: Xray tube failed to go low mag. Inner barrier is commanded closed in other thread, xray tube movement is aborted.");
          printXrayTubePositionStatus();
          printInnerBarrierPositionStatus();
          return;
        }
        waitUntilLowMag();
  //      if(!initStateIsUp)
  //      {
  //        sleep(500);
  //      }
        if (isSimulationModeOn())
        {
          updateSimulationSensorStatusForUp();
        }
      }
      finally
      {
        _hardwareActionTimer.stop();
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go low mag. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }
  
  /**
   * Move the X-Ray CPMotor up to position 1 - 23um.
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public void upToPosition1(boolean loadConfig) throws XrayTesterException
  {
    if(_homeSuccessfully==false && isSimulationModeOn() == false)
    {
      logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag position 1. Previous X-ray tube homing is not successful, please initialize Digital IO again.");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());
    }
    
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupLowMagImagingXrayParameters();      
        XrayCameraArray.getInstance().loadLowMagGrayScale();        
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
      }
    }

    boolean initStateIsUpToPosition1 = isUpToPosition1();

    if(initStateIsUpToPosition1)
      return;
        
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
      _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION1);
      _hardwareObservable.setEnabled(false);

      try
      {
        // Time the open action.
        _hardwareActionTimer.reset();
        _hardwareActionTimer.start();
        _digitalIo.moveZAxisToPosition1();
        if(_digitalIo.isInnerBarrierCommandedClose())
        {
          logHardwareStatus("WARNING: Xray tube failed to go low mag position 1. Inner barrier is commanded closed in other thread, xray tube movement is aborted.");
          printXrayTubePositionStatus();
          printInnerBarrierPositionStatus();
          return;
        }
        waitUntilLowMagPosition1();
  //      if(!initStateIsUp)
  //      {
  //        sleep(500);
  //      }
        if (isSimulationModeOn())
        {
          updateSimulationSensorStatusForUpToPosition1();
        }
      }
      finally
      {
        _hardwareActionTimer.stop();
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION1);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go low mag position 1. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }
  
  /**
   * Move the X-Ray CPMotor up to position 2 - 19um.
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public void upToPosition2(boolean loadConfig) throws XrayTesterException
  {
    if(_homeSuccessfully==false && isSimulationModeOn() == false)
    {
      logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag position 2. Previous X-ray tube homing is not successful, please initialize Digital IO again.");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());
    }
    
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupLowMagImagingXrayParameters();      
        XrayCameraArray.getInstance().loadLowMagGrayScale();        
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
      }
    }

    boolean initStateIsUpToPosition2 = isUpToPosition2();

    if(initStateIsUpToPosition2)
      return;
        
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
      _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION2);
      _hardwareObservable.setEnabled(false);

      try
      {
        // Time the open action.
        _hardwareActionTimer.reset();
        _hardwareActionTimer.start();
        _digitalIo.moveZAxisToPosition2();
        if(_digitalIo.isInnerBarrierCommandedClose())
        {
          logHardwareStatus("WARNING: Xray tube failed to go low mag position 2. Inner barrier is commanded closed in other thread, xray tube movement is aborted.");
          printXrayTubePositionStatus();
          printInnerBarrierPositionStatus();
          return;
        }
        waitUntilLowMagPosition2();
  //      if(!initStateIsUp)
  //      {
  //        sleep(500);
  //      }
        if (isSimulationModeOn())
        {
          updateSimulationSensorStatusForUpToPosition2();
        }
      }
      finally
      {
        _hardwareActionTimer.stop();
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION2);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go low mag position 2. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }

  
  /**
   * Move the X-Ray CPMotor Startup Homing is used during startup to double confirm that the CP Motor is properly home.
   * CP Motor may fall down if overloaded or tuning parameters are incorrect.
   * @author Anthony Fong
   */
  public void startupHoming() throws XrayTesterException
  {       
    _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HOME);
    _hardwareObservable.setEnabled(false);

    _homeSuccessfully = false;
    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      boolean newHomingMethod = true;
      if(newHomingMethod)
      {
        _digitalIo.disableZAxis();
        sleep(5); //Need to turn OFF ZAxis enable signal for rising edge triggering
        _digitalIo.moveZAxisToHomePosition();
        waitUntilHome();
        //Swee Yee - increase delay from 500 to 1000
        sleep(500);
        waitUntilHome();
        
        //Swee Yee - double check for homing
        
        if(isSimulationModeOn() == false)
        {
          if (_digitalIo.isXrayZAxisNotHome())
          {
            _homeSuccessfully = false;
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());
          }
        }
      }
      else
      {
        _digitalIo.disableZAxis();
        sleep(5); //Need to turn OFF ZAxis enable signal for rising edge triggering
        _digitalIo.moveZAxisToHomePosition();
        waitUntilHome();
        sleep(50);
        waitUntilNotHome();
        sleep(50);
        waitUntilHome();
        
        //Swee Yee - double check for homing
        if(isSimulationModeOn() == false)
        {
          if (_digitalIo.isXrayZAxisNotHome())
          {
            _homeSuccessfully = false;
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());
          }
        }
      }
      if(isSimulationModeOn())
      {
          updateSimulationSensorStatusForHome();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    
    _homeSuccessfully = true;
    _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HOME);
  }  
  
  /**
   * Move the X-Ray CPMotor home.
   * @author Anthony Fong
   */
  public void home(boolean loadConfig, boolean isHighMag) throws XrayTesterException
  {
    //Swee Yee Wong - should always expect homesuccessfully here, whenever homing exception thrown, it will go through here.
//    if(_homeSuccessfully == false && isSimulationModeOn() == false)
//    {
//      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisFailedToGoHomeException());
//    }
    
    if(loadConfig && isSimulationModeOn() == false)
    {
      if(isHighMag==true)
      {
        if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
        {
          _xraySource.setupHighMagImagingXrayParameters();
          XrayCameraArray.getInstance().loadHighMagGrayScale();            
          PanelPositioner.getInstance().resetYaxisHysteresisOffsetForHighMag();
        }
      }
      else
      {
        if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
        {
          _xraySource.setupLowMagImagingXrayParameters();
          XrayCameraArray.getInstance().loadLowMagGrayScale();        
          PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
        }
      }
    }
//    System.out.println("Z Axis Home");
    boolean initStateIsHome = isHome();

    if(initStateIsHome)
      return;
        
    _homeSuccessfully = false;
    
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
      _hardwareObservable.stateChangedBegin(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HOME);
      _hardwareObservable.setEnabled(false);

      try
      {
        // Time the open action.
        _hardwareActionTimer.reset();
        _hardwareActionTimer.start();
        _digitalIo.moveZAxisToHomePosition();
        waitUntilHome();
  //      if(!initStateIsUp)
  //      {
        
        if(isSimulationModeOn() == false)
        {
          if (_digitalIo.isXrayZAxisNotHome())
          {
            _homeSuccessfully = false;
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());
          }
        }
  //      }
        //buffer 0.5 seconds for xray tube to reach Home Slot Sensor's position
        //we don't want the inner barrier to close too early
        //sleep(500);
        
        if(isSimulationModeOn())
        {
          updateSimulationSensorStatusForHome();
        }
      }
      finally
      {
        _hardwareActionTimer.stop();
        _hardwareObservable.setEnabled(true);
      }
      _homeSuccessfully = true;
      _hardwareObservable.stateChangedEnd(this, XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HOME);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go home. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }
  
  /**
   * @return long returns how long last open/close action time.
   * @author Anthony Fong
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Anthony Fong
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _xrayCPMotorIsDown = _digitalIo.isXrayZAxisDown();
    _xrayCPMotorIsNotUp = _digitalIo.isXrayZAxisNotUp();

    _xrayCPMotorIsUp = _digitalIo.isXrayZAxisUp();
    _xrayCPMotorIsNotDown = _digitalIo.isXrayZAxisNotDown();
      
    _xrayCPMotorIsHome = _digitalIo.isXrayZAxisHome();
    _xrayCPMotorIsNotHome = _digitalIo.isXrayZAxisNotHome();
    _xrayCPMotorIsNotDownAndUp = _digitalIo.isXrayZAxisNoDownAndUp();
    
    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = _digitalIo.isXrayZAxisUpToPosition1();
    _xrayCPMotorIsUpToPosition2 = _digitalIo.isXrayZAxisUpToPosition2();
    _xrayCPMotorIsNotUpToPosition1 = _digitalIo.isXrayZAxisNotUpToPosition1();
    _xrayCPMotorIsNotUpToPosition2 = _digitalIo.isXrayZAxisNotUpToPosition2();
     
  }

  /**
   * When the X-Ray CPMotor is being commanded to up/down, there is a window of time
   * where the CPMotor is neither up nor down.
   * @return true if the CPMotor sensor are giving consistent state information.
   * @author Anthony Fong
   */
  private boolean isHardwareDownSettled() throws XrayTesterException
  {
    boolean isSettled = (_xrayCPMotorIsDown == _xrayCPMotorIsNotUp) && (_xrayCPMotorIsDown == _xrayCPMotorIsNotHome);
    return isSettled;
  }
  private boolean isHardwareUpSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayCPMotorIsUp == _xrayCPMotorIsNotDown && (_xrayCPMotorIsUp == _xrayCPMotorIsNotHome);
    return isSettled;
  }
  
  // this function only used in service panel
  private boolean isHardwareUpToPosition1Settled() throws XrayTesterException
  {
    boolean isSettled = _xrayCPMotorIsUpToPosition1 == _xrayCPMotorIsNotDown && (_xrayCPMotorIsUpToPosition1 == _xrayCPMotorIsNotHome) && (_xrayCPMotorIsUpToPosition1 == _xrayCPMotorIsNotUpToPosition2);
    return isSettled;
  }
  // this function only used in service panel
  private boolean isHardwareUpToPosition2Settled() throws XrayTesterException
  {
    boolean isSettled = _xrayCPMotorIsUpToPosition2 == _xrayCPMotorIsNotDown && (_xrayCPMotorIsUpToPosition2 == _xrayCPMotorIsNotHome) && (_xrayCPMotorIsUpToPosition2 == _xrayCPMotorIsNotUpToPosition1);
    return isSettled;
  }
  private boolean isHardwareHomeSettled() throws XrayTesterException
  {
    //boolean isSettled = _xrayCPMotorIsHome == _xrayCPMotorIsNotDownAndUp;
    boolean isSettled = _xrayCPMotorIsHome == _xrayCPMotorIsNotDown && (_xrayCPMotorIsHome == _xrayCPMotorIsNotUp);
    return isSettled;
  }
  /**
   * @return true if the X-Ray CPMotor is for sure home.
   * @throws XrayTesterException exception will be thrown if the X-Ray CPMotor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isHome_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is home.
    updateSensorStatus();

    if (isHardwareHomeSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());

    return _xrayCPMotorIsHome;
  }
  
  public boolean isHome() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is home.
    if(isSimulationModeOn() == false)
      updateSensorStatus();


    return _xrayCPMotorIsHome;
  }
  
  /**
   * @return true if the X-Ray CPMotor is for sure down or low mag.
   * @throws XrayTesterException exception will be thrown if the X-Ray CPMotor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isDown_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is down or low mag.
    updateSensorStatus();

    if (isHardwareDownSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());

    return _xrayCPMotorIsDown;
  }
  public boolean isDown() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is down or low mag.
    if(isSimulationModeOn() == false)
      updateSensorStatus();
    return _xrayCPMotorIsDown;
  }

  /**
   * @return true if the X-Ray CPMotor is for sure up or high mag.
   * @throws XrayTesterException exception will be thrown if the X-Ray CPMotor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isUp_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is up or high mag.
    updateSensorStatus();

    if (isHardwareUpSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayZAxisIsStuckException());

    return _xrayCPMotorIsUp;
  }
  
  public boolean isUp() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is up or high mag.
    if(isSimulationModeOn() == false)
      updateSensorStatus();

    return _xrayCPMotorIsUp;
  }
  
  /**
   * Check the X-Ray CPMotor up to position 1 - 23um.
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition1() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is up or high mag.
    if(isSimulationModeOn() == false)
      updateSensorStatus();

    return _xrayCPMotorIsUpToPosition1;
  }
  
  /**
   * Check the X-Ray CPMotor up to position 2 - 19um.
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition2() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray CPMotor is up or high mag.
    if(isSimulationModeOn() == false)
      updateSensorStatus();

    return _xrayCPMotorIsUpToPosition2;
  }
    
  /**
   * @author Anthony Fong
   */
  private void waitUntilHighMag() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveHighMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsDown = false;
        updateSensorStatus();
        if (isHardwareDownSettled())
          xrayCPMotorIsDown = _xrayCPMotorIsDown;

        return xrayCPMotorIsDown;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Xray tube: Xray tube failed to go high mag, please check Xray-tube position sensor.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getXrayZAxisFailedToGoHighMagException();
      }
    };
    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilLowMag() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveLowMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsUp = false;
        updateSensorStatus();
        if (isHardwareUpSettled())
          xrayCPMotorIsUp = _xrayCPMotorIsUp;

        return xrayCPMotorIsUp;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag, please check Xray-tube position sensor.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getXrayZAxisFailedToGoLowMagException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
  /**
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  private void waitUntilLowMagPosition1() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveLowMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsUpToPosition1 = false;
        updateSensorStatus();
        if (isHardwareUpToPosition1Settled())
          xrayCPMotorIsUpToPosition1 = _xrayCPMotorIsUpToPosition1;

        return xrayCPMotorIsUpToPosition1;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag position 1, please check Xray-tube position sensor.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getXrayZAxisFailedToGoLowMagException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
  /**
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  private void waitUntilLowMagPosition2() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveLowMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsUpToPosition2 = false;
        updateSensorStatus();
        if (isHardwareUpToPosition2Settled())
          xrayCPMotorIsUpToPosition2 = _xrayCPMotorIsUpToPosition2;

        return xrayCPMotorIsUpToPosition2;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Xray tube: Xray tube failed to go low mag position 2, please check Xray-tube position sensor.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getXrayZAxisFailedToGoLowMagException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
  /**
   * @author Anthony Fong
   */
  private void waitUntilHome() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveHomeTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsHome = false;
        updateSensorStatus();
        if (isHardwareHomeSettled())
          xrayCPMotorIsHome = _xrayCPMotorIsHome;

        return xrayCPMotorIsHome;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Xray tube: Xray tube failed to go home, please check Xray-tube position sensor.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getXrayZAxisFailedToGoHomeException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
  /**
   * @author Anthony Fong
   */
  private void waitUntilNotHome() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayZAxisMoveHomeTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayZAxisSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCPMotorIsNotHome = false;
        updateSensorStatus();
        if (_xrayCPMotorIsNotHome && _xrayCPMotorIsNotDown &&  _xrayCPMotorIsNotUp)
          xrayCPMotorIsNotHome = _xrayCPMotorIsNotHome;

        return xrayCPMotorIsNotHome;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayZAxisFailedToGoHomeException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
    /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Anthony Fong
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
   
//  /**
//   * @return boolean whether the X-Ray Actuator Interface is installed.
//   * @author Anthony Fong
//   */
//  public boolean isActuatorInstalled()
//  {
//    return isInstalled();
//  }

  /**
   * For Variable Magnification, the X-Ray CPMotor is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray CPMotor is installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false &&   
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == true;
  }

  private void updateSimulationSensorStatusForHome()
  {
    _xrayCPMotorIsDown = false;
    _xrayCPMotorIsNotDown = true;

    _xrayCPMotorIsUp = false;
    _xrayCPMotorIsNotUp = true;

    _xrayCPMotorIsHome = true;
    _xrayCPMotorIsNotHome = false;

    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = false;
    _xrayCPMotorIsUpToPosition2 = false;
    _xrayCPMotorIsNotUpToPosition1 = true;
    _xrayCPMotorIsNotUpToPosition2 = true;
  }
  
  private void updateSimulationSensorStatusForUp()
  {
    _xrayCPMotorIsDown = false;
    _xrayCPMotorIsNotDown = true;

    _xrayCPMotorIsUp = true;
    _xrayCPMotorIsNotUp = false;

    _xrayCPMotorIsHome = false;
    _xrayCPMotorIsNotHome = true;

    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = false;
    _xrayCPMotorIsUpToPosition2 = false;
    _xrayCPMotorIsNotUpToPosition1 = true;
    _xrayCPMotorIsNotUpToPosition2 = true;
  }
  
  private void updateSimulationSensorStatusForUpToPosition1()
  {
    _xrayCPMotorIsDown = false;
    _xrayCPMotorIsNotDown = true;

    _xrayCPMotorIsUp = false;
    _xrayCPMotorIsNotUp = true;

    _xrayCPMotorIsHome = false;
    _xrayCPMotorIsNotHome = true;

    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = true;
    _xrayCPMotorIsUpToPosition2 = false;
    _xrayCPMotorIsNotUpToPosition1 = false;
    _xrayCPMotorIsNotUpToPosition2 = true;
  }
  
  private void updateSimulationSensorStatusForUpToPosition2()
  {
    _xrayCPMotorIsDown = false;
    _xrayCPMotorIsNotDown = true;

    _xrayCPMotorIsUp = false;
    _xrayCPMotorIsNotUp = true;

    _xrayCPMotorIsHome = false;
    _xrayCPMotorIsNotHome = true;

    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = false;
    _xrayCPMotorIsUpToPosition2 = true;
    _xrayCPMotorIsNotUpToPosition1 = true;
    _xrayCPMotorIsNotUpToPosition2 = false;
  }
  
  private void updateSimulationSensorStatusForDown()
  {
    _xrayCPMotorIsDown = true;
    _xrayCPMotorIsNotDown = false;

    _xrayCPMotorIsUp = false;
    _xrayCPMotorIsNotUp = true;

    _xrayCPMotorIsHome = false;
    _xrayCPMotorIsNotHome = true;

    //Swee Yee Wong - these 4 flags are only used in service panel
    _xrayCPMotorIsUpToPosition1 = false;
    _xrayCPMotorIsUpToPosition2 = false;
    _xrayCPMotorIsNotUpToPosition1 = true;
    _xrayCPMotorIsNotUpToPosition2 = true;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public void printXrayTubePositionStatus()
  {
    try
    {
      if (XrayCPMotorActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayZAxisHome())
        {
          logHardwareStatus("X-ray tube at home position");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition1())
        {
          logHardwareStatus("X-ray tube at low mag position 1");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition2())
        {
          logHardwareStatus("X-ray tube at low mag position 2");
        }
        if (DigitalIo.getInstance().isXrayZAxisDown())
        {
          logHardwareStatus("X-ray tube at high mag position");
        }
        if (DigitalIo.getInstance().isXrayZAxisHome() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition1() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition2() == false && DigitalIo.getInstance().isXrayZAxisDown() == false)
        {
          logHardwareStatus("ERROR - Xray tube: X-ray tube position status unknown");
        }
      }
      else if (XrayCylinderActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayCylinderUp())
        {
          logHardwareStatus("X-ray tube at home/low mag position");
        }
        if (DigitalIo.getInstance().isXrayCylinderDown())
        {
          logHardwareStatus("X-ray tube at high mag position 1");
        }
        if (DigitalIo.getInstance().isXrayCylinderUp() == false && DigitalIo.getInstance().isXrayCylinderDown() == false)
        {
          logHardwareStatus("ERROR - Xray tube: X-ray tube position status unknown");
        }
      }
    }
    catch (XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      logHardwareStatus("ERROR - Xray tube: Failed to get xray tube position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public void printInnerBarrierPositionStatus()
  {
    try
    {
      if(DigitalIo.getInstance().isInnerBarrierInstalled())
      {
        if (DigitalIo.getInstance().isInnerBarrierOpen())
        {
          logHardwareStatus("Inner barrier is opened");
        }
        if (DigitalIo.getInstance().isInnerBarrierClosed())
        {
          logHardwareStatus("Inner barrier is closed");
        }
        if (DigitalIo.getInstance().isInnerBarrierOpen() == false && DigitalIo.getInstance().isInnerBarrierClosed() == false)
        {
          logHardwareStatus("ERROR - Xray tube: Inner barrier position status unknown");
        }
      }
    }
    catch(XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      logHardwareStatus("ERROR - Xray tube: Failed to get inner barrier position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private void logHardwareStatus(String message)
  {
    try
    {
      HardwareStatusLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to log hardwareStatus. \n" + e.getMessage());
    }
  }
}
