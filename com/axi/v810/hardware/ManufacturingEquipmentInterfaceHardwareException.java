package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ManufacturingEquipmentInterfaceHardwareException extends HardwareException
{
  private ManufacturingEquipmentInterfaceHardwareExceptionEnum _exceptionType;

  /**
   * @author Rex Shang
   */
  private ManufacturingEquipmentInterfaceHardwareException(ManufacturingEquipmentInterfaceHardwareExceptionEnum exceptionEnum, String key, Object[] parameters)
  {
    super(new LocalizedString(key, parameters));
    Assert.expect(exceptionEnum != null, "exceptionEnum is null.");

    _exceptionType = exceptionEnum;
  }

  /**
   * @author Rex Shang
   */
  public static ManufacturingEquipmentInterfaceHardwareException getUpstreamSystemUnexpectedlyHasPanelException()
  {
    ManufacturingEquipmentInterfaceHardwareException exception = new ManufacturingEquipmentInterfaceHardwareException(ManufacturingEquipmentInterfaceHardwareExceptionEnum.UPSTREAM_SYSTEM_UNEXPECTEDLY_HAS_PANEL_EXCEPTION,
                                                                                                                      "HW_SMEMA_UPSTREAM_SYSTEM_UNEXPECTEDLY_HAS_PANEL_EXCEPTION_KEY",
                                                                                                                      null);

    return exception;
  }

  /**
   * @author Rex Shang
   */
  public ManufacturingEquipmentInterfaceHardwareExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }

}
