package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class Test_ImageReconstructionService extends UnitTest
{
  /**
   * @author Rex Shang
   */
  private Test_ImageReconstructionService()
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public static void main(String args[])
  {
    Test_ImageReconstructionService myTest = new Test_ImageReconstructionService();
    if (UnitTest.unitTesting())
      UnitTest.execute(myTest);
    else
      myTest.debugTests(args);
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
   try
    {
      new ImageReconstructionService(null, 0);
      Expect.expect(false);
    }
    catch (IllegalArgumentException iae)
    {
      // Do nothing.
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      new ImageReconstructionService("", -1);
      Expect.expect(false);
    }
    catch (IllegalArgumentException iae)
    {
      // Do nothing.
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    ImageReconstructionService service = new ImageReconstructionService("", 0);

    try
    {
      service.getProgramVersion(null, "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.getProgramVersion("", null);
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.isProgramRunning(null, "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.isProgramRunning("", null);
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.startProgram(null, "", "", "", "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.startProgram("", null, "", "", "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.startProgram("", "", null, "", "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.startProgram("", "", "", null, "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.startProgram("", "", "", "", null);
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.stopProgram(null, "");
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      service.stopProgram("", null);
      Expect.expect(false);
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }
  }

  /**
   * @author George A. David
   */
  private static void debugTests(final String args[])
  {
    try
    {
      HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
      {
        public void run()
        {
          //      String path = "d:\\vstore\\salseroSnapGenesis\\axi\\cpp\\embeddedHardware\\reconstructionService\\src\\Debug\\";
          //      String name = "reconstructionManager.exe";
          //    String path = "C:\\apps\\Rational\\ClearCase\\bin\\";
//          String path = "d:\\home\\george\\Documents\\myScripts\\";
//          String path = "d:\\home\\george\\Documents\\public\\test 1";
//          String path = "c:\\";
//          String path = Directory.getLocalImageReconstructionProcessorBinDir();
          String path = "%AXI_XRAY_HOME%\\irpBin";
          //    String path = "C:\\apps\\mksnt\\mksnt\\";
          //      String path = "\\\\mtdsalsero\\ClearCase\\bin\\";
          //      String path = "\\\\mtdsalsero\\c$\\apps\\Rational\\ClearCase\\bin\\";
          //    String path = "\\\\axibuild\\c$\\apps\\Rational\\ClearCase\\bin\\";
          //    String path = "\\\\axibuild\\c_root\\apps\\Rational\\ClearCase\\bin\\";
          //    String path = "\\\\mtdsalsero\\c_root\\apps\\Rational\\ClearCase\\bin\\";
          //    String path = "\\\\192.168.128.102\\bin\\";
          //      String path = "\\\\axibuild\\ClearCase\\bin\\";
          //    String name = "clearfindco.exe";
//          String name = "irpServiceTest.bat";
//          String name = "commandLineTest.exe";
          //    String name = "echo.exe";
          String name = "IRP.exe";
//          String arguments = "g-saveProjections d:/vstore/salseroSnapGenesis";
          //    String arguments = "d:/vstore/salseroSnapGenesis";
          //    String arguments = "test >> d:\\home\\george\\Documents\\log\\echoTest.log";
          String arguments = "";
          String additionalEnv = "";
          String pathExtension = "";

          ImageReconstructionService service = new ImageReconstructionService("localhost", 4545); // local machine
//          ImageReconstructionService service = new ImageReconstructionService(
//              ImageReconstructionProcessor.getInstance(ImageReconstructionEngineEnum.IRE0)); // IRE0
          //    ImageReconstructionService service = new ImageReconstructionService("156.140.108.160", 4545); // local machine
          //    ImageReconstructionService service = new ImageReconstructionService("156.140.108.149", 4545); // axibuild
          //    ImageReconstructionService service = new ImageReconstructionService("192.168.128.240", 4545); // lp2 irp1
          try
          {
//            Config.getInstance().forceLoad();
//            ImageReconstructionHardware irpHardware = ImageReconstructionHardware.getInstance();
//            irpHardware.debugStartup();
//            if (args.length > 0)
//            {
//              String command = args[0];
//              if (command.equalsIgnoreCase("restartHardware"))
//              {
//                irpHardware.restartHardware();
//              }
//              else if (command.equalsIgnoreCase("startIRPs"))
//              {
//                irpHardware.startImageReconstructionProcessors();
//              }
//              else if (command.equalsIgnoreCase("stopIRPs"))
//              {
//                irpHardware.stopImageReconstructionProcessors();
//              }
//              else if (command.equalsIgnoreCase("restartIRPs"))
//              {
//                irpHardware.restartImageReconstructionProcessors();
//              }
//              else if (command.equalsIgnoreCase("isRunning"))
//              {
//                System.out.println("Is running: " + irpHardware.areServicesRunning());
//              }
              //        else if (command.equalsIgnoreCase("isReachable"))
              //          System.out.println("Is reachable: " + irpHardware.isReachable());
              //        else
//              Assert.expect(false);
//            }
            //      else
            //        System.out.println("Is Reachable: " + irpHardware.isReachable());
            //      service.getRamInKiloBytes();
            service.stopProgram("", name);

            ImageReconstructionService service2 = new ImageReconstructionService("localhost", 4545);
            try
            {
              Expect.expect(service2.isProgramRunning("", name) == false);
            }
            catch (ImageReconstructionProcessorHardwareException ex)
            {
              // Do nothing.
            }
            Expect.expect(service.isProgramRunning("", name) == false);
//            service.startProgram("", name, arguments, additionalEnv, pathExtension);
            arguments = "-port 9121";
            service.startProgram(path, name, arguments, additionalEnv, pathExtension);
            Expect.expect(service.isProgramRunning("", name));

            arguments = "-port 9122";
            service.startProgram(path, name, arguments, additionalEnv, pathExtension);
            Expect.expect(service.isProgramRunning("", name));

            service.stopProgram("", name);

            service.stopProgram(path, name);
            service.startProgram(path, name, arguments, additionalEnv, pathExtension);
            Expect.expect(service.isProgramRunning(path, name));
//      service.getRamInKiloBytes();
            service.stopProgram(path, name);
            service.startProgram(path, name, arguments, additionalEnv, pathExtension);
            int i = 0;
            while (i++ < 1000)
            {
              service.isProgramRunning(path, name);
            }
            service.isProgramRunning(path, name);
            service.isProgramRunning(path, name);
            service.isProgramRunning(path, name);

            service.startProgram(path, name, arguments, additionalEnv, pathExtension);
            service.isProgramRunning(path, name);
            service.isProgramRunning(path, name);
            service.isProgramRunning(path, name);
            service.stopProgram(path, name);
            service.isProgramRunning(path, name);
            //      service.getRamInKiloBytes();
            //      service.isProgramRunning(path, name);
            //      service.getRamInKiloBytes();
            //      service.stopProgram(path, name);
            //      service.getRamInKiloBytes();
            //      service.restartHardware();
            //      service.diconnect();
          }
          catch (Exception ex)
          {
            ex.printStackTrace();
          }
        }
      });
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
