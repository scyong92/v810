package com.axi.v810.hardware;

import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class handles all functions of the clamps that hold the panel in the stage.
 *
 * @author Rex Shang
 */
public class PanelClamps extends HardwareObject
{
  private static PanelClamps _instance;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private WaitForHardwareCondition _waitForHardwareCondition;
  private static boolean _WAIT_NOT_ABORTABLE = false;
  /**
   * This contructor is not public on purpose.  Use getInstance() to construct
   * a new class.
   *
   * @author Rex Shang
   */
  private PanelClamps()
  {
    _instance = null;
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized PanelClamps getInstance()
  {
    if (_instance == null)
      _instance = new PanelClamps();

    return _instance;
  }

  /**
   * @return true if the panel clamps are closed, false if they are open.
   * @author Rex Shang
   */
  public boolean areClampsClosed() throws XrayTesterException
  {
    return _digitalIo.arePanelClampsClosed();
  }

  /**
   * @return true if the panel clamps are closed, false if they are open.
   * @author Anthony Fong
   */
  public boolean areClampsOpened() throws XrayTesterException
  {
    return _digitalIo.arePanelClampsOpened();
  }

  /**
   * Open the clamps that hold the panel in the stage.
   * @author Rex Shang
   */
  public void open() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelClampsEventEnum.CLAMPS_OPEN);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.openPanelClamps();

      //XXL Based Anthony Jan 2013
      if(XrayTester.isXXLBased()|| XrayTester.isS2EXEnabled())
      {
          waitUntilPanelClampOpened();

          // Check current state.
          if (areClampsClosed())
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToOpenPanelClampsException());
          }
      }
      else
      {
          // Wait for the clamps to open.
          _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getPanelClampsOpeningTimeInMilliSeconds());

          // Check the current state.
          if (areClampsClosed())
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToOpenPanelClampsException());
          }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelClampsEventEnum.CLAMPS_OPEN);
  }

  /**
   * Close the clamps that hold the panel in the stage.
   * @author Rex Shang
   */
  public void close() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelClampsEventEnum.CLAMPS_CLOSE);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isXXLBased()|| XrayTester.isS2EXEnabled())
      {
        LeftPanelInPlaceSensor.getInstance().retract();
        RightPanelInPlaceSensor.getInstance().retract();
      }
      _digitalIo.closePanelClamps();
      //XXL Based Anthony Jan 2013
      if(XrayTester.isXXLBased()|| XrayTester.isS2EXEnabled())
      {
          waitUntilPanelClampClosed();

          // Check current state.
          if (areClampsOpened())
          {
             HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToClosePanelClampsException());
          }
      }
      else
      {
          // Wait for the clamps to close.
          _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getPanelClampsClosingTimeInMilliSeconds());

          // Check the current state.
          if (areClampsClosed() == false)
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToClosePanelClampsException());
          }
       }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelClampsEventEnum.CLAMPS_CLOSE);
  }

   //XXL Based Anthony Jan 2013
   /**
   * Wait for Panel Clamp to closed.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long the time in milli seconds that we wait for panel to be
   * in place.  0 to indicate wait forever.
   * @param abortable used to indicate whether the wait can be aborted.
   * @author Anthony Fong
   */
  public void waitUntilPanelClampClosed() throws XrayTesterException
  {
     _waitForHardwareCondition = new WaitForHardwareCondition(_digitalIo.getPanelClampCylinderTimeOutInMilliSeconds(),
                                                             _digitalIo.getPanelClampSettlingTimeOutInMilliSeconds(),
                                                             _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        return areClampsClosed();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getFailedToClosePanelClampsException();
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

   //XXL Based Anthony Jan 2013
   /**
   * Wait for Panel Clamp to closed.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long the time in milli seconds that we wait for panel to be
   * in place.  0 to indicate wait forever.
   * @param abortable used to indicate whether the wait can be aborted.
   * @author Anthony Fong
   */
  public void waitUntilPanelClampOpened() throws XrayTesterException
  {
     _waitForHardwareCondition = new WaitForHardwareCondition(_digitalIo.getPanelClampCylinderTimeOutInMilliSeconds(),
                                                             _digitalIo.getPanelClampSettlingTimeOutInMilliSeconds(),
                                                             _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        return areClampsOpened();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getFailedToOpenPanelClampsException();
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }


}
