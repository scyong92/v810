package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class RightOuterBarrierEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static RightOuterBarrierEventEnum BARRIER_OPEN = new RightOuterBarrierEventEnum(++_index, "Right Outer Barrier, Open");
  public static RightOuterBarrierEventEnum BARRIER_CLOSE = new RightOuterBarrierEventEnum(++_index, "Right Outer Barrier, Close");

  private String _name;

  /**
   * @author Rex Shang
   */
  private RightOuterBarrierEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }
}
