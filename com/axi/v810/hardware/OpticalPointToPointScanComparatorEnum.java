package com.axi.v810.hardware;

/**
 *
 * @author Administrator
 */
public class OpticalPointToPointScanComparatorEnum extends com.axi.util.Enum
{
    private static int _index = -1;
    
    public static OpticalPointToPointScanComparatorEnum PANEL_COORDINATE_X_IN_NANOMETERS = new OpticalPointToPointScanComparatorEnum(++_index);
    public static OpticalPointToPointScanComparatorEnum PANEL_COORDINATE_Y_IN_NANOMETERS = new OpticalPointToPointScanComparatorEnum(++_index);
    public static OpticalPointToPointScanComparatorEnum OPTICAL_CAMERA_POSITION = new OpticalPointToPointScanComparatorEnum(++_index);    
    
    /**
     * @author Cheah Lee Herng 
     */
    private OpticalPointToPointScanComparatorEnum(int id)
    {
        super(id);
    }
}
