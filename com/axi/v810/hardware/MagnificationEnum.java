package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * This class contains the target magnifications that are supported by the current
 * hardware
 * 10000 nanometers (10 micron) per pixel (high magnification)
 * 16000 nanometers (16 micron) per pixel (low magnification)
 * @author Peter Esbensen
 * @author Horst Mueller
 * @author Bill Darbie
 */
public class MagnificationEnum extends com.axi.util.Enum
{
  private static Map<Integer, MagnificationEnum> _magnificationToMagnificationEnumMap = new HashMap<Integer, MagnificationEnum>();
  private static int _index = -1;
  private static int _nominalMagnificationNanometersPerPixel = -1;
  private static int _nominalHighMagnificationNanometersPerPixel = 1;
  private static int _minimumSliceHeightMagnificationNanometersPerPixel = -1;
  private static int _maximumSliceHeightMagnificationNanometersPerPixel = -1;
  private static int _minimumSliceHeightHighMagnificationNanometersPerPixel = -1;
  private static int _maximumSliceHeightHighMagnificationNanometersPerPixel = -1;
  private static Config _config = Config.getInstance();

  private int _nanoMetersPerPixel;
  
  // Added by Chnee Khang Wah
  private static double _magnificationAtMaximumSliceHeight;
  private static double _magnificationAtMinimumSliceHeight;
  private static double _highMagnificationAtMaximumSliceHeight;
  private static double _highMagnificationAtMinimumSliceHeight;
  
  private static List<MagnificationEnum> _magnificationEnumList = new ArrayList<MagnificationEnum>(); 
  private transient static Map<MagnificationEnum, Object> _lastValuesCleared = new HashMap<MagnificationEnum, Object>();

  //Ngie Xing
  //calculate 1 pixel for how many microns 19micron 
  //get 1 pixel for how many microns ???
  //low mag and high mag
  //will change in S2EX, need to know xray camera to xray source find sung wey, need to know for low mag and high mag
  /**
   * Calculate pixel sizes at the various magnifications
   * @author Dave Ferguson
   */
  static
  {
    setupMagnification();
  }

  public static MagnificationEnum NOMINAL = new MagnificationEnum(++_index, _nominalMagnificationNanometersPerPixel); 
  public static MagnificationEnum MAX_SLICE_HEIGHT = new MagnificationEnum(++_index, _maximumSliceHeightMagnificationNanometersPerPixel);
  public static MagnificationEnum MIN_SLICE_HEIGHT = new MagnificationEnum(++_index, _minimumSliceHeightMagnificationNanometersPerPixel);
  public static MagnificationEnum H_NOMINAL = new MagnificationEnum(++_index, _nominalHighMagnificationNanometersPerPixel); 
  public static MagnificationEnum H_MAX_SLICE_HEIGHT = new MagnificationEnum(++_index, _maximumSliceHeightHighMagnificationNanometersPerPixel);
  public static MagnificationEnum H_MIN_SLICE_HEIGHT = new MagnificationEnum(++_index, _minimumSliceHeightHighMagnificationNanometersPerPixel);
  
  /**
   * @author Horst Mueller
   */
  private MagnificationEnum(int index, int nanoMetersPerPixel)
  {
    super(index);
    Assert.expect(nanoMetersPerPixel > 0);
    _nanoMetersPerPixel = nanoMetersPerPixel;
    _magnificationEnumList.add(this);
    Object previous = _magnificationToMagnificationEnumMap.put(new Integer(nanoMetersPerPixel), this);
    Assert.expect(previous == null);
  }
  
  private int getValue()
  {
    return _nanoMetersPerPixel;
  }
  
  private void setValue(int nanoMetersPerPixel)
  {
    _nanoMetersPerPixel = nanoMetersPerPixel;
  }

  /**
   * @author Horst Mueller
   */
  public static MagnificationEnum getEnum(int id)
  {
    MagnificationEnum enumeration = (MagnificationEnum)_magnificationToMagnificationEnumMap.get(new Integer(id));

    Assert.expect(enumeration != null);
    return enumeration;
  }
  
  /**
   * @author Matt Wharton
   */
  public int getNanoMetersPerPixel()
  {
    return _nanoMetersPerPixel;
  }
  
  /**
   * @author Wei Chin
   */
  public void setNanoMetersPerPixel(int nanoMetersPerPixel)
  {
    _nanoMetersPerPixel = nanoMetersPerPixel;
  }


  /**
   * @author George A. David
   */
  public static double getNominalMagnificationAtReferencePlane()
  {
    return _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);
  }

  /**
   * @author Wei Chin
   */
  public static double getMagnificationAtReferencePlane()
  {
    return _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
  }

  /**
   * @author Seng-Yew Lim
   * For Thunderbolt project. Try to reduce scan passes according to panel thickness.
   * This method will return the slice height magnification based on panel thickness instead of max slice height.
   */
  public static int getMaxPanelSliceHeightMagnificationNanometersPerPixel(double panelThickness, double boardZOffset)
  {
    int value;
    // Nomiminal magnification
    double nominalMagnificationAtReferencePlane = getNominalMagnificationAtReferencePlane();

    // Both COLUMN and ROW pitch are of the same value.
    // Just get any camera so we can get sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    int physicalCameraPixelPitchInNanometers = xRayCamera.getSensorColumnPitchInNanometers();
    // Top depth of focus (i.e. maximum slice height above nominal plane)
    int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
    // calculate based on panel thickness instead of hardcode 208 mils
    double zHeightMaximumSliceHeightFromNominalInNanometers = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(panelThickness, boardZOffset);
    int zHeightAtNominalInNanometers = zHeightFromCameraArrayToXraySourceInNanometers -
                                       (int) Math.round(zHeightFromCameraArrayToXraySourceInNanometers / nominalMagnificationAtReferencePlane);
    double zHeightAtMaximumSliceHeightInNanometers = zHeightAtNominalInNanometers + zHeightMaximumSliceHeightFromNominalInNanometers;
    double magnficationAtMaximumSliceHeight = zHeightFromCameraArrayToXraySourceInNanometers /(zHeightFromCameraArrayToXraySourceInNanometers - zHeightAtMaximumSliceHeightInNanometers);
    value = (int) Math.round(physicalCameraPixelPitchInNanometers / magnficationAtMaximumSliceHeight);
    return value;    
  }
  
  /**
   * @author Wei Chin
   * For Thunderbolt project. Try to reduce scan passes according to panel thickness.
   * This method will return the slice height magnification based on panel thickness instead of max slice height.
   */
  public static int getMaxHighMagPanelSliceHeightMagnificationNanometersPerPixel(double panelThickness, double boardZOffset)
  {    
    int value;
    // Nomiminal magnification
    double nominalMagnificationAtReferencePlane = getNominalHighMagnificationAtReferencePlane();

    // Both COLUMN and ROW pitch are of the same value.
    // Just get any camera so we can get sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    int physicalCameraPixelPitchInNanometers = xRayCamera.getSensorColumnPitchInNanometers();
    // Top depth of focus (i.e. maximum slice height above nominal plane)
    int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
    // calculate based on panel thickness instead of hardcode 208 mils
    double zHeightMaximumSliceHeightFromNominalInNanometers = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(panelThickness, boardZOffset);
    int zHeightAtNominalInNanometers = zHeightFromCameraArrayToXraySourceInNanometers -
                                       (int) Math.round(zHeightFromCameraArrayToXraySourceInNanometers / nominalMagnificationAtReferencePlane);
    double zHeightAtMaximumSliceHeightInNanometers = zHeightAtNominalInNanometers + zHeightMaximumSliceHeightFromNominalInNanometers;
    double magnficationAtMaximumSliceHeight = zHeightFromCameraArrayToXraySourceInNanometers /(zHeightFromCameraArrayToXraySourceInNanometers - zHeightAtMaximumSliceHeightInNanometers);
    value = (int) Math.round(physicalCameraPixelPitchInNanometers / magnficationAtMaximumSliceHeight);
    return value;
  }
  
  /**
   * @author Wei Chin
   */
  public static double getNominalHighMagnificationAtReferencePlane()
  {
    return _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL);
  }

  /**
   * @author Wei Chin
   */
  public static double getHighMagnificationAtReferencePlane()
  {
    return _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
  }

  /**
   * @author Wei Chin
   */
  public static MagnificationEnum getCurrentNorminal()
  {
    if(XrayTester.isLowMagnification())
      return MagnificationEnum.NOMINAL;
    else
      return MagnificationEnum.H_NOMINAL;
  }
  
  /**
   * @author Wei Chin
   */
  public static MagnificationEnum getCurrentMaxSliceHeight()
  {
    if(XrayTester.isLowMagnification())
      return MagnificationEnum.MAX_SLICE_HEIGHT;
    else
      return MagnificationEnum.H_MAX_SLICE_HEIGHT;
  }

  /**
   * @author Wei Chin
   */
  public static MagnificationEnum getCurrentMinSliceHeight()
  {
    if(XrayTester.isLowMagnification())
      return MagnificationEnum.MIN_SLICE_HEIGHT;
    else
      return MagnificationEnum.H_MIN_SLICE_HEIGHT;
  }  
  
  /**
   * @author Chnee Khang Wah
   */
  public static double getMagnificationAtMaximumSliceHeight()
  {
    return _magnificationAtMaximumSliceHeight;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public static double getMagnificationAtMinimumSliceHeight()
  {
    return _magnificationAtMinimumSliceHeight;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public static double getHighMagnificationAtMaximumSliceHeight()
  {
    return _highMagnificationAtMaximumSliceHeight;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public static double getHighMagnificationAtMinimumSliceHeight()
  {
    return _highMagnificationAtMinimumSliceHeight;
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * load the new config enum value due to system type change
   */
  private static void setupMagnification()
  {
    double nominalMagnificationAtReferencePlane = getNominalMagnificationAtReferencePlane();
    double nominalHighMagnificationAtReferencePlane =  getNominalHighMagnificationAtReferencePlane();

    // Both COLUMN and ROW pitch are of the same value.
    // Just get any camera so we can get sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    int physicalCameraPixelPitchInNanometers = xRayCamera.getSensorColumnPitchInNanometers();

    _nominalMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / nominalMagnificationAtReferencePlane);
    _nominalHighMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / nominalHighMagnificationAtReferencePlane);
     
//     Assert.expect(_nominalMagnificationNanometersPerPixel / 1000 * 1000 ==
//                   _nominalMagnificationNanometersPerPixel,
//                   "Set 'referencePlaneMagnificationNominal' to higher resoulution/digits.\n" +
//                   _nominalMagnificationNanometersPerPixel + " is not allowed\n");
//     Assert.expect(_nominalMagnificationNanometersPerPixel == 19000);

    // Top depth of focus (i.e. maximum slice height above nominal plane)
    int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();

    double zHeightMaximumSliceHeightFromNominalInNanometers = XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers();

    int zHeightAtNominalInNanometers = zHeightFromCameraArrayToXraySourceInNanometers -
                                        (int) Math.round(zHeightFromCameraArrayToXraySourceInNanometers / nominalMagnificationAtReferencePlane);

    double zHeightAtMaximumSliceHeightInNanometers = zHeightAtNominalInNanometers + zHeightMaximumSliceHeightFromNominalInNanometers;

    _magnificationAtMaximumSliceHeight = zHeightFromCameraArrayToXraySourceInNanometers /(zHeightFromCameraArrayToXraySourceInNanometers - zHeightAtMaximumSliceHeightInNanometers);

    _maximumSliceHeightMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / _magnificationAtMaximumSliceHeight);

    // Bottom depth of focus (i.e. minimum slice height below nominal plane)
    double zHeightMinimumSliceHeightFromNominalInNanometers = XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers();

    double zHeightAtMinimumSliceHeightInNanometers = zHeightAtNominalInNanometers + zHeightMinimumSliceHeightFromNominalInNanometers;

    _magnificationAtMinimumSliceHeight = zHeightFromCameraArrayToXraySourceInNanometers /
                                          (zHeightFromCameraArrayToXraySourceInNanometers - zHeightAtMinimumSliceHeightInNanometers);

    _minimumSliceHeightMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / _magnificationAtMinimumSliceHeight);
     
    // Top depth of focus (i.e. maximum slice height above nominal plane)
    int zHeightFromCameraArrayTo2ndXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();

    int zHeightAtHighMagNominalInNanometers = zHeightFromCameraArrayTo2ndXraySourceInNanometers -
                                          (int) Math.round(zHeightFromCameraArrayTo2ndXraySourceInNanometers / nominalHighMagnificationAtReferencePlane);

    double highMagZHeightAtMaximumSliceHeightInNanometers = zHeightAtHighMagNominalInNanometers + zHeightMaximumSliceHeightFromNominalInNanometers;

    _highMagnificationAtMaximumSliceHeight = zHeightFromCameraArrayTo2ndXraySourceInNanometers /(zHeightFromCameraArrayTo2ndXraySourceInNanometers - highMagZHeightAtMaximumSliceHeightInNanometers);

    _maximumSliceHeightHighMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / _highMagnificationAtMaximumSliceHeight);

    double highMagZHeightAtMinimumSliceHeightInNanometers = zHeightAtHighMagNominalInNanometers + zHeightMinimumSliceHeightFromNominalInNanometers;

    _highMagnificationAtMinimumSliceHeight = zHeightFromCameraArrayTo2ndXraySourceInNanometers /
                                              (zHeightFromCameraArrayTo2ndXraySourceInNanometers - highMagZHeightAtMinimumSliceHeightInNanometers);

    _minimumSliceHeightHighMagnificationNanometersPerPixel = (int) Math.round(physicalCameraPixelPitchInNanometers / _highMagnificationAtMinimumSliceHeight);
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * reset the magnification enum due to config value have been change
   */
  public static void resetMagnification()
  {
    setupMagnification();
    _lastValuesCleared.clear();
    for (MagnificationEnum enumeration : _magnificationEnumList)
    {
      _lastValuesCleared.put(enumeration, enumeration.getValue());
      enumeration.setValue(0);
    }
    
    NOMINAL.setValue(_nominalMagnificationNanometersPerPixel);
    MAX_SLICE_HEIGHT.setValue(_maximumSliceHeightMagnificationNanometersPerPixel);
    MIN_SLICE_HEIGHT.setValue(_minimumSliceHeightMagnificationNanometersPerPixel);
    H_NOMINAL.setValue(_nominalHighMagnificationNanometersPerPixel);
    H_MIN_SLICE_HEIGHT.setValue(_minimumSliceHeightHighMagnificationNanometersPerPixel);
    H_MAX_SLICE_HEIGHT.setValue(_maximumSliceHeightHighMagnificationNanometersPerPixel);
  }
   
  /**
   * @author weng-jian.eoh
   * @return 
   * 
   * XCR-3589 Combo STD and XXL software GUI
   */
  public static List<MagnificationEnum> getMagnificationEnums()
  {
    return _magnificationEnumList;
  }
}
