package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ManufacturingEquipmentInterfaceEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static ManufacturingEquipmentInterfaceEventEnum MACHINE_READY_TO_ACCEPT_PANEL = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Machine Ready To Accept Panel");
  public static ManufacturingEquipmentInterfaceEventEnum MACHINE_NOT_READY_TO_ACCEPT_PANEL = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Machine Not Ready To Accept Panel");
  public static ManufacturingEquipmentInterfaceEventEnum PANEL_AVAILABLE = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Panel Available");
  public static ManufacturingEquipmentInterfaceEventEnum PANEL_NOT_AVAILABLE = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Panel Not Available");
  public static ManufacturingEquipmentInterfaceEventEnum PANEL_OUT_PASSED = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Panel Out Passed");
  public static ManufacturingEquipmentInterfaceEventEnum PANEL_OUT_FAILED = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Panel Out Failed");
  public static ManufacturingEquipmentInterfaceEventEnum PANEL_OUT_UNTESTED = new ManufacturingEquipmentInterfaceEventEnum(++_index, "Manufacturing Equipment Interface, Panel Out Untested");

  private String _name;

  /**
   * @author Rex Shang
   */
  private ManufacturingEquipmentInterfaceEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
