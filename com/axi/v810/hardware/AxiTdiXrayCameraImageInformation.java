package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
class AxiTdiXrayCameraImageInformation
{
  private static final String _IMAGE_INFORMATION_DATA_DELIMITER = "\\s+";
  private static final int _NUMBER_OF_IMAGE_INFORMATION_DATA_ITEMS = 5;
  private static final int _IMAGE_NOT_AVAILABLE = -1;

  private int _scanImageNumber;
  private int _imageLeftPositionInPixels;
  private int _imageTopPositionInPixels;
  private int _imageRightPositionInPixels;
  private int _imageBottomPositionInPixels;
  private boolean _imageIsAvailable;

  /**
   * @author Greg Esparza
   */
  AxiTdiXrayCameraImageInformation()
  {
    _scanImageNumber = -1;
    _imageLeftPositionInPixels = -1;
    _imageTopPositionInPixels = -1;
    _imageRightPositionInPixels = -1;
    _imageBottomPositionInPixels = -1;
    _imageIsAvailable = false;
  }

  /**
   * @author Greg Esparza
   */
  void set(String imageInformationStr) throws XrayTesterException
  {
    Assert.expect(imageInformationStr != null);
    Assert.expect(imageInformationStr.length() > 0);

    String[] imageInformationData = imageInformationStr.split(_IMAGE_INFORMATION_DATA_DELIMITER);
    Assert.expect(imageInformationData.length == _NUMBER_OF_IMAGE_INFORMATION_DATA_ITEMS);

    setScanImageNumber(imageInformationData[0]);
    setImageLeftPositionInPixels(imageInformationData[1]);
    setImageTopPositionInPixels(imageInformationData[2]);
    setImageRightPositionInPixels(imageInformationData[3]);
    setImageBottomPositionInPixels(imageInformationData[4]);
  }

  /**
   * @author Greg Esparza
   */
  int getScanNumber()
  {
    Assert.expect(_scanImageNumber >= 0);
    return _scanImageNumber;
  }

  /**
   * @author Greg Esparza
   */
  int getRegionOfInterestXoriginInPixels()
  {
    Assert.expect(_imageLeftPositionInPixels >= 0);
    return _imageLeftPositionInPixels;
  }

  /**
   * @author Greg Esparza
   */
  int getRegionOfInterestYoriginInPixels()
  {
    Assert.expect(_imageTopPositionInPixels >= 0);
    return _imageTopPositionInPixels;
  }

  /**
   * @author Greg Esparza
   */
  int getRegionOfInterestWidthInPixels()
  {
    Assert.expect(_imageLeftPositionInPixels >= 0);
    Assert.expect(_imageRightPositionInPixels >= 0);
    int widthInPixels = (_imageRightPositionInPixels - _imageLeftPositionInPixels) + 1;
    return widthInPixels;
  }

  /**
   * @author Greg Esparza
   */
  int getRegionOfInterestHeightInPixels()
  {
    Assert.expect(_imageTopPositionInPixels >= 0);
    Assert.expect(_imageBottomPositionInPixels >= 0);
    int heightInPixels = (_imageBottomPositionInPixels - _imageTopPositionInPixels) + 1;
    return heightInPixels;
  }

  /**
   * @author Greg Esparza
   */
  boolean isImageAvailable()
  {
    return _imageIsAvailable;
  }

  /**
   * @author Greg Esparza
   */
  private void setScanImageNumber(String scanPassNumberStr) throws XrayTesterException
  {
    Assert.expect(scanPassNumberStr != null);

    try
    {
      _scanImageNumber = StringUtil.convertStringToInt(scanPassNumberStr);
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidImageInformationImageNumberException(bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }

    if (_scanImageNumber > _IMAGE_NOT_AVAILABLE)
      _imageIsAvailable = true;
    else
      _imageIsAvailable = false;
  }

  /**
   * @author Greg Esparza
   */
  private void setImageLeftPositionInPixels(String imageLeftPositionInPixelsStr) throws XrayTesterException
  {
    Assert.expect(imageLeftPositionInPixelsStr != null);

    try
    {
      _imageLeftPositionInPixels = StringUtil.convertStringToInt(imageLeftPositionInPixelsStr);
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidImageInformationLeftPositionInPixelsException(bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setImageRightPositionInPixels(String imageRightPositionInPixelsStr) throws XrayTesterException
  {
    Assert.expect(imageRightPositionInPixelsStr != null);

    try
    {
      _imageRightPositionInPixels = StringUtil.convertStringToInt(imageRightPositionInPixelsStr);
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidImageInformationRightPositionInPixelsException(bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setImageTopPositionInPixels(String imageTopPositionInPixelsStr) throws XrayTesterException
  {
    Assert.expect(imageTopPositionInPixelsStr != null);

    try
    {
      _imageTopPositionInPixels = StringUtil.convertStringToInt(imageTopPositionInPixelsStr);
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidImageInformationTopPositionInPixelsException(bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setImageBottomPositionInPixels(String imageBottomPositionInPixelsStr) throws XrayTesterException
  {
    Assert.expect(imageBottomPositionInPixelsStr != null);

    try
    {
      _imageBottomPositionInPixels = StringUtil.convertStringToInt(imageBottomPositionInPixelsStr);
    }
    catch (BadFormatException bfe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiInvalidImageInformationBottomPositionInPixelsException(bfe.getLocalizedMessage());
      he.initCause(bfe);
      throw he;
    }
  }
}
