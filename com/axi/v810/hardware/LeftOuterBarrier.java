package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the left outer barrier.  The left outer
 * barrier exists on both loader and loaderless systems.
 * @author Rex Shang
 */
public class LeftOuterBarrier extends HardwareObject implements BarrierInt
{
  protected static Config _config;
  private static LeftOuterBarrier _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the barrier is open or closed.
  private boolean _barrierIsOpen;
  private boolean _barrierIsNotClosed;

  private static boolean _WAIT_NOT_ABORTABLE = false;

  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Rex Shang
   */
  private LeftOuterBarrier()
  {
    _config = Config.getInstance();
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized LeftOuterBarrier getInstance()
  {
    if (_instance == null)
      _instance = new LeftOuterBarrier();

    return _instance;
  }

  /**
   * Close the barrier.
   * @author Rex Shang
   */
  public void close() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LeftOuterBarrierEventEnum.BARRIER_CLOSE);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the close action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();

      _digitalIo.closeLeftOuterBarrier();
      waitUntilClosed();
      try
      {
        Thread.sleep(_config.getIntValue(HardwareConfigEnum.OUTER_BATTIERS_CLOSE_DELAY_FOR_INTERLOCK_CONFIRMATION));
      }
      catch (InterruptedException ix)
      {
        // don't expect this, but don't really care how sleep returns either
      }

      AbstractXraySource xraySource = AbstractXraySource.getInstance();
      if(xraySource.isXrayFilterInstalled())
      {
        try
        {
          if(xraySource instanceof HTubeXraySource)
          {
            HTubeXraySource htubeXraySource = (HTubeXraySource)xraySource;
            if(htubeXraySource.isStartupRequired() == false)
            {
              htubeXraySource.setupLowMagImagingXrayParameters();;
            }
          }
        }
        catch(XrayTesterException xe)
        {
          // do nothing
        }
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, LeftOuterBarrierEventEnum.BARRIER_CLOSE);
  }

  /**
   * Open the current barrier.
   * @author Rex Shang
   */
  public void open() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LeftOuterBarrierEventEnum.BARRIER_OPEN);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();

      AbstractXraySource xraySource = AbstractXraySource.getInstance();
      if(xraySource.isXrayFilterInstalled())
      {
        if(xraySource instanceof HTubeXraySource)
        {
          try
          {
            HTubeXraySource htubeXraySource = (HTubeXraySource)xraySource;
            if(htubeXraySource.isStartupRequired() == false && htubeXraySource.areXraysOn())
            {
              htubeXraySource.setupOpenOuterBarrierXrayParameters();
            }
          }
          catch(XrayTesterException xe)
          {
            // do nothing
          }
        }
      }
	  //XCR1775- Safety interlock checking- Anthony
      if (InnerBarrier.isInstalled())
      {
        if (_digitalIo.isInnerBarrierClosed() == false)
        {
          //logging for abnormality detection
          System.out.println("DigitalIo:Inner barrier was opened. Close it now before open left outer barrier.");
          InnerBarrier.getInstance().close();
        }
      }
      _digitalIo.openLeftOuterBarrier();
      waitUntilOpen();
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, LeftOuterBarrierEventEnum.BARRIER_OPEN);
  }

  /**
   * @return long returns how long last open/close action time.
   * @author Rex Shang
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Rex Shang
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _barrierIsOpen = _digitalIo.isLeftOuterBarrierOpen();
    _barrierIsNotClosed = _digitalIo.isLeftOuterBarrierNotClosed();
  }

  /**
   * When the barrier is being commanded to open/close, there is a window of time
   * where the barrier is neither open nor closed.
   * @return true if the barrier sensor are giving consistent state information.
   * @author Rex Shang
   */
  private boolean isHardwareSettled() throws XrayTesterException
  {
    boolean isSettled = _barrierIsOpen == _barrierIsNotClosed;
    return isSettled;
  }

  /**
   * @return true if the barrier is open.
   * @author Rex Shang
   */
  synchronized public boolean isOpen() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the barrier is open.
    updateSensorStatus();

    if (isHardwareSettled() == false)    
    {
      //Sometimes isHardwareSettled() return false while Outer Barrieris is opening or closing
      //Return true if Hardware is Settled within 3 seconds or throw Exception when time up. 
      if (waitForHardwareSettled(3000)==false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getLeftOuterBarrierIsStuckException());
      }
    }
    return _barrierIsOpen;
  }
  
  /**
   * @author Anthony Fong
   */
  private boolean waitForHardwareSettled(long timeToWaitInMilliSeconds) throws XrayTesterException
  {
    boolean doneSignalDetected = false;
    //if (XrayTester.isS2EXEnabled())
    {
      try
      {
        long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
        boolean continueCheck = true;
        while (continueCheck && (deadLineInMilliSeconds > System.currentTimeMillis()))
        {
          try
          {
            updateSensorStatus();
            if (isHardwareSettled())
            {
              doneSignalDetected = true;
              continueCheck = false;
            }
            Thread.sleep(0);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }
          finally
          {
          }
        }
//        if (doneSignalDetected == false)
//        {
//          //Inner Barrier Is Stuck!!!
//          HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getInnerBarrierIsStuckException());
//        }
      }
      catch (XrayTesterException xte)
      {
        // Throw the original exception.
        throw xte;
      }
      finally
      {
      }
    }
    return doneSignalDetected;
  }
  
  /**
   * @author Rex Shang
   */
  private void waitUntilOpen() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getOuterBarrierOpenTimeOutInMilliSeconds(),
                                                                   _digitalIo.getOuterBarrierSettlingTimeOutInMilliSeconds(),

                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean barrierIsOpen = false;

        updateSensorStatus();
        if (isHardwareSettled())
          barrierIsOpen = _barrierIsOpen;

        return barrierIsOpen;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getLeftOuterBarrierFailedToOpenException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Rex Shang
   */
  private void waitUntilClosed() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getOuterBarrierCloseTimeOutInMilliSeconds(),
                                                                   _digitalIo.getOuterBarrierSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean barrierIsClosed = false;

        updateSensorStatus();
        if (isHardwareSettled())
          //barrierIsClosed = _barrierIsOpen == false;
          //To prevent inner barrier neither close nor open - Anthony 
          barrierIsClosed = _barrierIsNotClosed == false;

        return barrierIsClosed;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getLeftOuterBarrierFailedToCloseException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }

  /**
   * @return boolean whether the barrier is installed.
   * @author Rex Shang
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_LEFT_OUTER_BARRIER_INSTALLED);
  }

}
