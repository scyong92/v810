package com.axi.v810.hardware;

/**
 * @author Roy Williams
 */
public class ProjectionTypeEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ProjectionTypeEnum INSPECTION_OR_VERIFICATION = new ProjectionTypeEnum(++_index);
  public static final ProjectionTypeEnum INSPECTION_AND_ALIGNMENT   = new ProjectionTypeEnum(++_index);
  public static final ProjectionTypeEnum ALIGNMENT                  = new ProjectionTypeEnum(++_index);

  /**
   *
   * @author Roy Williams
   */
  private ProjectionTypeEnum(int ndx)
  {
    super(ndx);
  }
}
