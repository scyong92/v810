package com.axi.v810.hardware;

import com.axi.v810.util.InfoHandlerPanel;
import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ImageReconstructionProcessorHardwareException extends HardwareException
{
  private ImageReconstructionProcessorHardwareExceptionEnum _type;

  /**
   * Construct a new exception based on an enum.
   * @author Rex Shang
   */
  private ImageReconstructionProcessorHardwareException(ImageReconstructionProcessorHardwareExceptionEnum exceptionEnum, LocalizedString message)
  {
    super(message);
    Assert.expect(message != null);

    _type = exceptionEnum;
  }

  /**
   * Construct a new exception based on an enum.
   * @author Rex Shang
   */
  private ImageReconstructionProcessorHardwareException(ImageReconstructionProcessorHardwareExceptionEnum exceptionEnum, String key, Object[] parameters)
  {
    this(exceptionEnum, new LocalizedString(key, parameters));

    Assert.expect(exceptionEnum != null, "exceptionEnum is null.");
    Assert.expect(key != null, "key string is null.");
  }

  /**
   * @author Rex Shang
   */
  public ImageReconstructionProcessorHardwareExceptionEnum getType()
  {
    return _type;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getApplicationVersionMismatchException()
  {
    ImageReconstructionProcessorHardwareException exception =
      new ImageReconstructionProcessorHardwareException(ImageReconstructionProcessorHardwareExceptionEnum.IMAGE_RECONSTRUCTION_APPLICATION_VERSION_MISMATCH,
                                                        new LocalizedString("HW_IMAGE_RECONSTRUCTION_PROCESSOR_APPLICATION_VERSION_MISMATCH_EXCEPTION_KEY", null));
    return exception;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getFailedToRebootImageReconstructionHardwareException(String ireId, String serverIpAddress)
  {
    Assert.expect(ireId != null);
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {ireId, serverIpAddress};

    ImageReconstructionProcessorHardwareException exception = new ImageReconstructionProcessorHardwareException(
        ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_REBOOT_IMAGE_RECONSTRUCTION_HARDWARE,
        "HW_FAILED_REBOOTING_IMAGE_RECONSTRUCTION_HARDWARE_KEY",
        exceptionParameters);
    return exception;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getFailedToStartImageReconstructionApplicationException(String ireId, String serverIpAddress)
  {
    Assert.expect(ireId != null);
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {ireId, serverIpAddress};

    ImageReconstructionProcessorHardwareException exception = new ImageReconstructionProcessorHardwareException(
        ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_START_IMAGE_RECONSTRUCTION_APPLICATION,
        "HW_FAILED_STARTING_IMAGE_RECONSTRUCTION_PROCESSORS_KEY",
        exceptionParameters);
    Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.SW_COMM_SET_IRP_HWL_STARTUP_FAILED, exceptionParameters);
    return exception;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getFailedToStopImageReconstructionApplicationException(String ireId, String serverIpAddress)
  {
    Assert.expect(ireId != null);
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {ireId, serverIpAddress};

    ImageReconstructionProcessorHardwareException exception = new ImageReconstructionProcessorHardwareException(
        ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_STOP_IMAGE_RECONSTRUCTION_APPLICATION,
        "HW_FAILED_STOPPING_IMAGE_RECONSTRUCTION_PROCESSORS_KEY",
        exceptionParameters);
    return exception;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getFailedToConnectToImageReconstructionServiceException(String ireId, String serverIpAddress, int portNumber, Exception causedBy)
  {
    Assert.expect(ireId != null);
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");
    Assert.expect(portNumber > 0);
    Assert.expect(causedBy != null);

    String[] exceptionParameters = new String[]
                                   {ireId, serverIpAddress, String.valueOf(portNumber)};

    ImageReconstructionProcessorHardwareException exception = new ImageReconstructionProcessorHardwareException(
        ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_CONNECT_TO_IMAGE_RECONSTRUCTION_SERVICE,
        "HW_FAILED_CONNECT_IMAGE_RECONSTRUCTION_SERVICE_KEY",
        exceptionParameters);

    exception.initCause(causedBy);
    return exception;
  }

  /**
   * @author Rex Shang
   */
  static ImageReconstructionProcessorHardwareException getFailedToCommunicateWithImageReconstructionServiceException(String ireId, String serverIpAddress, int portNumber, Exception causedBy)
  {
    Assert.expect(ireId != null);
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");
    Assert.expect(portNumber > 0);
    Assert.expect(causedBy != null);

    String[] exceptionParameters = new String[]
                                   {ireId, serverIpAddress, String.valueOf(portNumber)};

    ImageReconstructionProcessorHardwareException exception = new ImageReconstructionProcessorHardwareException(
        ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_COMMUNICATE_WITH_IMAGE_RECONSTRUCTION_SERVICE,
        "HW_FAILED_COMMUNICATION_IMAGE_RECONSTRUCTION_SERVICE_KEY",
        exceptionParameters);

    exception.initCause(causedBy);
    return exception;
  }

}
