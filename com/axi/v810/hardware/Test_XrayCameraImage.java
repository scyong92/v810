package com.axi.v810.hardware;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;

import com.axi.util.*;

public class Test_XrayCameraImage extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_XrayCameraImage());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    short[] pixelArray = new short[1024 * 1024];
    int factor = 1024 * 1024 / 65546 + 1; // 2^16 = 65546
    for(int i = 0; i < pixelArray.length; ++i)
      pixelArray[i] = (short)(i / factor);

    XrayCameraImage image = new XrayCameraImage(pixelArray);
    BufferedImage bImage = image.getImage();
    MyFrame frame = new MyFrame(bImage);
    frame.setSize(new Dimension(image.getWidthInPixels(), image.getLengthInPixels()));
    frame.setVisible(true);

    try
    {
      Thread.sleep(10000);
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }

  }

}

class MyFrame extends JFrame
{
  private BufferedImage _image;

  MyFrame(BufferedImage image)
  {
    _image = image;
  }

  public void paint(Graphics g)
  {
    Graphics2D g2 = (Graphics2D)g;
    g2.drawImage(_image, null, 0, 0);
  }
}
