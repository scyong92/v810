package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class SocketConnectionFailedHardwareException extends HardwareException
{
  private String _ipAddress;
  private int _portNumber;
  private String _message;

  /**
   * @author Bill Darbie
   */
  public SocketConnectionFailedHardwareException(String ipAddress, int portNumber)
  {
    super(new LocalizedString("HW_SOCKET_CONNECTION_FAILED_KEY", new Object[] {ipAddress, new Integer(portNumber)}));
    _portNumber = portNumber;
    Assert.expect(ipAddress != null);
    _ipAddress = ipAddress;
  }

  /**
   * @author Roy Williams
   */
  public SocketConnectionFailedHardwareException(String ipAddress, int portNumber, String message)
  {
    super(new LocalizedString("HW_SOCKET_CONNECTION_FAILED_KEY", new Object[] {ipAddress, new Integer(portNumber), message}));
    _portNumber = portNumber;
    Assert.expect(ipAddress != null);
    _ipAddress = ipAddress;
    Assert.expect(message != null);
    _message = message;
  }

  /**
   * @author Bill Darbie
   */
  public String getIpAddress()
  {
    return _ipAddress;
  }

  /**
   * @author Bill Darbie
   */
  public int getPortNumber()
  {
    return _portNumber;
  }

  /**
   * @author Roy Williams
   */
  public String message()
  {
    return _message;
  }
}
