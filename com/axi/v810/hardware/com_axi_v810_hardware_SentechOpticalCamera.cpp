#include <map>
#include <string>

#ifdef _MSC_VER
  #pragma warning(disable: 4786)
#endif  

#include "windows.h"
#include "tchar.h"
#include "Wingdi.h"	
#include "jawt.h"
#include "jawt_md.h"
#include "WinUser.h"
#include "com/axi/util/jniUtil.h"
#include "cpp/util/src/sptAssert.h"
#include "com_axi_v810_hardware_SentechOpticalCamera.h"
#include "thirdParty/Sentech/include/StCamD.h"
#include "thirdParty/Sentech/include/StTrgApi.h"
#include "thirdParty/Sentech/include/vtfx.h"
#include "com/axi/util/image/jniImageUtil.h"
#include "thirdParty/Psp3DAlgo/include/cpsp3dalgo.h"
#include "ippi.h"

#define FRAMESCOUNT 15
#define TRIGGERMODE 2049

using namespace std;

class CAMERAINFO
{                 
public:
  HANDLE m_hCamera;
  double exposureTime;
  int imageCount;
  char* pcImageMemory; 
  char* accumulateMemory; 
  char* ppcMemLast;
  INT lMemoryId;   
  
  unsigned char* pInitialPhaseImg1;
  unsigned char* pInitialPhaseImg2;
  unsigned char* pInitialPhaseImg3;
  unsigned char* pInitialPhaseImg4;
  unsigned char* pObjImg1;
  unsigned char* pObjImg2;
  unsigned char* pObjImg3;
  unsigned char* pObjImg4;
  unsigned char* pRefImg1;
  unsigned char* pRefImg2;
  unsigned char* pRefImg3;
  unsigned char* pRefImg4;
  unsigned char* pObjPlusThreeImg1;
  unsigned char* pObjPlusThreeImg2;
  unsigned char* pObjPlusThreeImg3;
  unsigned char* pObjPlusThreeImg4;

  // ****************************************************
  // New variables supporting new PSP algorithm library
  unsigned char* pNewObjImg1;
  unsigned char* pNewObjImg2;
  unsigned char* pNewObjImg3;
  unsigned char* pNewObjImg4;
  unsigned char* pNewObjImg5;
  unsigned char* pNewObjImg6;
  unsigned char* pNewObjImg7;
  unsigned char* pNewObjImg8;
  
  unsigned char* pNewRefImg1;
  unsigned char* pNewRefImg2;
  unsigned char* pNewRefImg3;
  unsigned char* pNewRefImg4;
  unsigned char* pNewRefImg5;
  unsigned char* pNewRefImg6;
  unsigned char* pNewRefImg7;
  unsigned char* pNewRefImg8;
  
  std::vector<Roi3DStruct> inspectionRoiVector;
  
  int numberOfProjector;
  bool debugMode;
  int planeId;
  bool isFrontModule;
  
  // ****************************************************
  
  int *pNewCoordinateXInPixel;
  int *pNewCoordinateYInPixel;
  float *pHeightMap;
  
  unsigned int roiCount;
  unsigned int *centerX;
  unsigned int *centerY;
  unsigned int *width;
  unsigned int *height;
  
  int offsetValue;
  
  double *magnificationData;
  
  Cpsp3DAlgo* newPspAlgorithm;
  
  // Control cropped image offset
  int frontModuleCropImageOffsetXInPixel;
  int rearModuleCropImageOffsetXInPixel;
  
  // Default Constructor
  CAMERAINFO()
  {
    m_hCamera = 0;
    exposureTime = 0;
    imageCount = 0;
    pcImageMemory = NULL;     
    ppcMemLast = NULL;
    lMemoryId = 0;    
    
    pInitialPhaseImg1 = NULL;
    pInitialPhaseImg2 = NULL;
    pInitialPhaseImg3 = NULL;
    pInitialPhaseImg4 = NULL;
    
    pObjImg1 = NULL;
    pObjImg2 = NULL;
    pObjImg3 = NULL;
    pObjImg4 = NULL;
    
    pRefImg1 = NULL;
    pRefImg2 = NULL;
    pRefImg3 = NULL;
    pRefImg4 = NULL;
    
    pObjPlusThreeImg1 = NULL;
    pObjPlusThreeImg2 = NULL;
    pObjPlusThreeImg3 = NULL;
    pObjPlusThreeImg4 = NULL;

    pNewObjImg1 = NULL;
    pNewObjImg2 = NULL;
    pNewObjImg3 = NULL;
    pNewObjImg4 = NULL;
    pNewObjImg5 = NULL;
    pNewObjImg6 = NULL;
    pNewObjImg7 = NULL;
    pNewObjImg8 = NULL;
    
    pNewRefImg1 = NULL;
    pNewRefImg2 = NULL;
    pNewRefImg3 = NULL;
    pNewRefImg4 = NULL;
    pNewRefImg5 = NULL;
    pNewRefImg6 = NULL;
    pNewRefImg7 = NULL;
    pNewRefImg8 = NULL;
    
    pNewCoordinateXInPixel = new int [256];
    pNewCoordinateYInPixel = new int [256];
    pHeightMap = new float [256];
    
    roiCount = 0;
    centerX = new unsigned int [256];
    centerY = new unsigned int [256];
    width = new unsigned int [256];
    height = new unsigned int [256];
    offsetValue = 0;
    magnificationData = new double [12];
    

    // Support new 3D Algorithm library
    newPspAlgorithm = new Cpsp3DAlgo();
    numberOfProjector = 1;
    debugMode = false;
    planeId = -1;
	
	isFrontModule = true;
	
	frontModuleCropImageOffsetXInPixel = 0;
	rearModuleCropImageOffsetXInPixel = 0;
  }
  
  void reset()
  {
    imageCount = 0;
    
    // Reset pointer values
    for(unsigned int i=0; i < 256; ++i)
    {
      pNewCoordinateXInPixel[i] = 0;
      pNewCoordinateYInPixel[i] = 0;
      pHeightMap[i] = 0.0;
      
      centerX[i] = 0;
      centerY[i] = 0;
      width[i] = 0;
      height[i] = 0;
    }
    
    // Reset magnification data
    for(unsigned int i=0; i < 12; ++i)
    {
      magnificationData[i] = 0.0;
    }

    // Reset ROI pointer
    inspectionRoiVector.clear();
  }
};

static map<int, CAMERAINFO*> _deviceIdToInstanceMap;
static CAMERAINFO* _pCameraInfo = 0;
                                                                                                                        
double _setExposureTime = 16.667;     

funcTransferEndCallback* transferEndCallback; 
BOOL _isTriggered = FALSE;
DWORD _nMaxWidth = 640;
DWORD _nMaxHeight = 470;
int _windowHandle = 0;
BOOL _isMirrorMode = FALSE;
DWORD _triggerTimeoutInMS = 1000;
BOOL _isTriggerModeReady = FALSE;
DWORD _waitForTriggerModeReadyTimeout = 5000;  // 5 seconds
DWORD _sleepWaitInMiliseconds = 1;

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static void setDeviceIdToInstanceMap(int deviceId)
{
  map<int, CAMERAINFO*>::iterator it = _deviceIdToInstanceMap.find(deviceId);
  CAMERAINFO* pCameraInfo = 0;

  if (it == _deviceIdToInstanceMap.end())
  {
    pCameraInfo = new CAMERAINFO();
    
    _deviceIdToInstanceMap.insert(make_pair(deviceId, pCameraInfo));

    pCameraInfo = 0;
  }
}

//===========================
// Save a bitmap image file.
// @author Jack Hwee
//===========================
void SaveBitmap(const char* szPath, BYTE* pImageBuffer, int iWidth, int iHeight, int iByteCount)
{
   HANDLE              fh;    
   DWORD               dwWritten;
     
	// Open or create an bitmap file handle.
	fh = CreateFile(szPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL, NULL);
	
	// save bitmap file header
	BITMAPFILEHEADER fileHeader;
	fileHeader.bfType = 0x4d42;
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	if( iByteCount == 1 )
		fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + 256*sizeof(RGBQUAD); // For Mono images.
	else
		fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);                       // For Color images

  WriteFile(fh, reinterpret_cast<char*>(&fileHeader), sizeof(fileHeader), &dwWritten, NULL);

	// save bitmap info header
	BITMAPINFOHEADER infoHeader;
	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = iWidth;
	infoHeader.biHeight = -iHeight;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = iByteCount*8;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

  WriteFile(fh, reinterpret_cast<char*>(&infoHeader), sizeof(infoHeader), &dwWritten, NULL);

	// save color table (sample used here is 8-bit grayscale)
	if(infoHeader.biBitCount == 8) 
	{
		RGBQUAD table[256];
		for(size_t iClr = 0; iClr < 256; iClr++)
		{
			table[iClr].rgbRed = table[iClr].rgbGreen = table[iClr].rgbBlue = static_cast<BYTE>(iClr);
		}

    WriteFile(fh, reinterpret_cast<char*>(table), 256*sizeof(RGBQUAD), &dwWritten, NULL);
	}
	else if(infoHeader.biBitCount == 4) {
	// 16 colors
	}
	else if(infoHeader.biBitCount == 1) {
	// only two colors (doesn't necessarily have to be always black and white!)
	}

	WriteFile(fh, reinterpret_cast<char*>(pImageBuffer), iWidth * iHeight * iByteCount, &dwWritten, NULL);

	// Close and clear the bitmap file handle.
  CloseHandle(fh);
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static CAMERAINFO* getCameraInfo(int deviceId)
{
  map<int, CAMERAINFO*>::iterator it = _deviceIdToInstanceMap.find(deviceId);
  sptAssert(it != _deviceIdToInstanceMap.end());

  return it->second;  
}

/**
 *  Function which allows us to log information into a text file. This is for
 *  debugging purpose. 
 */ 
void WriteLog(const char* szString)
{
  FILE* pFile = fopen("C://logFile.txt", "a");
  fprintf(pFile, "%s\n",szString);
  fclose(pFile);
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static BYTE* cropImage(BYTE *pbyteImageBuffer, DWORD dwWidth, DWORD dwHeight)
{
  BYTE *croppedPbyteImageBuffer = NULL;
 
  //Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  croppedPbyteImageBuffer = new BYTE[dwBufferSize];
  
  int heightOffset =  (dwHeight - _nMaxHeight) / 2;
  //int widthOffset = ((heightOffset - 1) * dwWidth)  + ((dwWidth - _nMaxWidth) / 2);
  int widthOffset = 0;
  
  // Adjust image offset based on camera Id
  if (_pCameraInfo->isFrontModule)
  {
	// For front module, we need to offset from left-hand side of image
	widthOffset = ((heightOffset - 1) * dwWidth) + _pCameraInfo->frontModuleCropImageOffsetXInPixel;
  }
  else
  {
	// For rear module, we need to offset from right-hand side of image
	//int rearModuleOffsetXFromLeftInPixel = dwWidth - (_pCameraInfo->rearModuleCropImageOffsetXInPixel + _nMaxWidth);
	//widthOffset = ((heightOffset - 1) * dwWidth) + rearModuleOffsetXFromLeftInPixel;

	widthOffset = ((heightOffset - 1) * dwWidth) + _pCameraInfo->rearModuleCropImageOffsetXInPixel;	
  }
	
  int iBpp(1);
  for (int j = heightOffset, jj = 0; j<(heightOffset + _nMaxHeight); j++, jj++)
  { 
    for (int i = widthOffset, ii =0; i<(widthOffset + _nMaxWidth); i++, ii++)
    {
       croppedPbyteImageBuffer[ii + (_nMaxWidth * jj)] = pbyteImageBuffer[i + (dwWidth * jj)];    
    }
  }
  
  return croppedPbyteImageBuffer;
} 

///////////////////////////////////////////////////////////////////////////////
// 
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static void CALLBACK nativeAcquireOfflineImagesCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{ 
  BOOL success = TRUE;
  BYTE *pbyteImageBuffer = NULL;

  //Allocate Memory
	DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
	pbyteImageBuffer = new BYTE[dwBufferSize];
	
	if (_isMirrorMode == TRUE)
  {
    BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
    BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }
	
  int iBpp(1);
  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);

  if(pvContext != NULL)
    SaveBitmap((const char*)pvContext,  pbyteImageBuffer, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);     
 
  if(pbyteImageBuffer)
  {
	  delete[] pbyteImageBuffer;
  }

  _isTriggered = TRUE;                                                                                        
};

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static void CALLBACK nativeGenerateHeightMapCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{
  BOOL success = TRUE;
  BYTE *pbyteImageBuffer = NULL;
 
	//Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  pbyteImageBuffer = new BYTE[dwBufferSize];
  
  if (_isMirrorMode == TRUE)
  {
    BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
    BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }
 
  int iBpp(1);
 
  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);
 
   if(_pCameraInfo->imageCount == 0) 
    {     
     _pCameraInfo->pObjImg1 = pbyteImageBuffer;       
 
     if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);         
    }  
    else if(_pCameraInfo->imageCount == 1) 
    {
      _pCameraInfo->pObjImg2 = pbyteImageBuffer;

      if(pvContext != NULL) 
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);              
    }
    else if(_pCameraInfo->imageCount == 2) 
    {
      _pCameraInfo->pObjImg3 = pbyteImageBuffer;

      if(pvContext != NULL) 
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);      	        
    }  
    else if(_pCameraInfo->imageCount == 3) 
    {
      _pCameraInfo->pObjImg4 = pbyteImageBuffer;

      if(pvContext != NULL) 
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);            
    }               

    StTrg_ClearBuffer(_pCameraInfo->m_hCamera); 
  
    _pCameraInfo->imageCount++;   
    _isTriggered = TRUE;                                                                                        
};

// Cheah Lee Herng
static void CALLBACK nativeMirrorAndRotateCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{
  BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
  BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
 
  if (STCAM_COLOR_ARRAY_MONO == wColorArray)
  {
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }

  BYTE *pbyteImageBuffer = NULL;
  CGfxDraw		m_gfx; 
	//Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  pbyteImageBuffer = new BYTE[dwBufferSize];
  
  int iBpp(1);
 
  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);
  
  HDC hdc = GetDC((HWND)_windowHandle); 

  HDC hdc2 = CreateCompatibleDC(hdc);
  HBITMAP bmp = CreateCompatibleBitmap(hdc,_nMaxWidth,_nMaxHeight);
  HBITMAP oldBmp = (HBITMAP) SelectObject(hdc2, bmp); 

  m_gfx.SetImg(pbyteImageBuffer, _nMaxWidth, _nMaxHeight);
  m_gfx.Draw(hdc);
    
  // Final draw to pDC
 	BitBlt(hdc,
         0,0, 
         _nMaxWidth, _nMaxHeight, 
         hdc2, 
         0,0, 
         SRCPAINT);
   
	SelectObject(hdc2,oldBmp);
	
	ReleaseDC((HWND)_windowHandle, hdc);

  DeleteDC (hdc2);
  DeleteObject(bmp);
	DeleteObject(hdc2);	 
	
	_isTriggered = TRUE;

  if(pbyteImageBuffer)
  {
    delete[] pbyteImageBuffer;
  }                 
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static void CALLBACK nativeGenerateNewHeightMapCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{
  BOOL success = TRUE;
  BYTE *pbyteImageBuffer = NULL;
 
  //Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  pbyteImageBuffer = new BYTE[dwBufferSize];
  
  if (_isMirrorMode == TRUE)
  {
    BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
    BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }
 
  int iBpp(1);
 
  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);
  
  if(_pCameraInfo->imageCount == 0) 
  {     
    _pCameraInfo->pNewObjImg1 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);         
  }
  else if(_pCameraInfo->imageCount == 1) 
  {
    _pCameraInfo->pNewObjImg2 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 2) 
  {
    _pCameraInfo->pNewObjImg3 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 3) 
  {
    _pCameraInfo->pNewObjImg4 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 4) 
  {
    _pCameraInfo->pNewObjImg5 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg5, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 5) 
  {
    _pCameraInfo->pNewObjImg6 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg6, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 6) 
  {
    _pCameraInfo->pNewObjImg7 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg7, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  else if(_pCameraInfo->imageCount == 7) 
  {
    _pCameraInfo->pNewObjImg8 = pbyteImageBuffer;       
    if(pvContext != NULL) 
      SaveBitmap((const char*)pvContext, _pCameraInfo->pNewObjImg8, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
  }
  
  StTrg_ClearBuffer(_pCameraInfo->m_hCamera); 
  
  _pCameraInfo->imageCount++;   
  _isTriggered = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
static void CALLBACK nativeAcquireObjectFringeImagesCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{
  BOOL success = TRUE;
  unsigned char *pbyteImageBuffer = NULL;
 
	//Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  pbyteImageBuffer = new unsigned char[dwBufferSize];
  
  if (_isMirrorMode == TRUE)
  {
    BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
    BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }

  int iBpp(1);

  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);
       
  if(_pCameraInfo->imageCount == 0) 
    {
      _pCameraInfo->pObjImg1 = pbyteImageBuffer;
      
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);          
    }  
    else if(_pCameraInfo->imageCount == 1) 
    {
      _pCameraInfo->pObjImg2 = pbyteImageBuffer;  
      
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);      
    }
    else if(_pCameraInfo->imageCount == 2) 
    {
      _pCameraInfo->pObjImg3 = pbyteImageBuffer; 
                   
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);  
    }  
    else if(_pCameraInfo->imageCount == 3) 
    {
      _pCameraInfo->pObjImg4 = pbyteImageBuffer;
                   
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);   
    }   
    else if(_pCameraInfo->imageCount == 4) 
    {
      _pCameraInfo->pRefImg1 = pbyteImageBuffer;    
         
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pRefImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);     
    }       
    else if(_pCameraInfo->imageCount == 5) 
    {
      _pCameraInfo->pRefImg2 = pbyteImageBuffer;     
               
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pRefImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);      
    }      
    else if(_pCameraInfo->imageCount == 6) 
    {
      _pCameraInfo->pRefImg3 = pbyteImageBuffer;  
                
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pRefImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);       
    }       
    else if(_pCameraInfo->imageCount == 7) 
    {
      _pCameraInfo->pRefImg4 = pbyteImageBuffer;      
            
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pRefImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);       
    }
    else if(_pCameraInfo->imageCount == 8) 
    {
      _pCameraInfo->pObjPlusThreeImg1 = pbyteImageBuffer;        
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjPlusThreeImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 9) 
    {
     _pCameraInfo->pObjPlusThreeImg2 = pbyteImageBuffer;           
     
     if(pvContext != NULL)
       SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjPlusThreeImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 10) 
    {
      _pCameraInfo->pObjPlusThreeImg3 = pbyteImageBuffer;         
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjPlusThreeImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);      
    }
    else if(_pCameraInfo->imageCount == 11) 
    {
      _pCameraInfo->pObjPlusThreeImg4 = pbyteImageBuffer; 
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pObjPlusThreeImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);   
    }                                       

    StTrg_ClearBuffer(_pCameraInfo->m_hCamera); 

    _pCameraInfo->imageCount++;   
    _isTriggered = TRUE;                                                                                        
};

///////////////////////////////////////////////////////////////////////////////
//
// @author Cheah Lee Herng
//////////////////////////////////////////////////////////////////////////////
static void CALLBACK nativeAcquireCalibrationImagesCallback(HANDLE hCamera, DWORD dwFrameNo, DWORD dwWidth, DWORD dwHeight, WORD wColorArray, PBYTE pbyteRaw, PVOID pvContext)
{
  BOOL success = TRUE;
  unsigned char *pbyteImageBuffer = NULL;
 
	//Allocate Memory
  DWORD	dwBufferSize = _nMaxWidth * _nMaxHeight;	
  pbyteImageBuffer = new unsigned char[dwBufferSize];

  if (_isMirrorMode == TRUE)
  {
    BYTE	g_byteMirrorMode = STCAM_MIRROR_HORIZONTAL;
    BYTE	g_byteRotationMode = STCAM_ROTATION_OFF;
    StTrg_MirrorRotation(g_byteMirrorMode, g_byteRotationMode, &dwWidth, &dwHeight, &wColorArray, pbyteRaw);
  }
  int iBpp(1);

  pbyteImageBuffer = cropImage(pbyteRaw, dwWidth, dwHeight);
       
  // Depending on plane id, we will increment the image pointer accordingly.
  // Currently we only support Ref plane and +3 plane.
  if (_pCameraInfo->planeId == 0) // Reference Plane
  {
    if(_pCameraInfo->imageCount == 0 || _pCameraInfo->imageCount == 8) 
    {
      _pCameraInfo->pNewRefImg1 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 1 || _pCameraInfo->imageCount == 9) 
    {
      _pCameraInfo->pNewRefImg2 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 2 || _pCameraInfo->imageCount == 10) 
    {
      _pCameraInfo->pNewRefImg3 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 3 || _pCameraInfo->imageCount == 11) 
    {
      _pCameraInfo->pNewRefImg4 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 4 || _pCameraInfo->imageCount == 12) 
    {
      _pCameraInfo->pNewRefImg5 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg5, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 5 || _pCameraInfo->imageCount == 13) 
    {
      _pCameraInfo->pNewRefImg6 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg6, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 6 || _pCameraInfo->imageCount == 14) 
    {
      _pCameraInfo->pNewRefImg7 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg7, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 7 || _pCameraInfo->imageCount == 15) 
    {
      _pCameraInfo->pNewRefImg8 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewRefImg8, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
  }
  else if (_pCameraInfo->planeId == 2) // +3 Plane
  {
    if(_pCameraInfo->imageCount == 0 || _pCameraInfo->imageCount == 8) 
    {
      _pCameraInfo->pNewObjImg1 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg1, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 1 || _pCameraInfo->imageCount == 9) 
    {
      _pCameraInfo->pNewObjImg2 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg2, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 2 || _pCameraInfo->imageCount == 10) 
    {
      _pCameraInfo->pNewObjImg3 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg3, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 3 || _pCameraInfo->imageCount == 11) 
    {
      _pCameraInfo->pNewObjImg4 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg4, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 4 || _pCameraInfo->imageCount == 12) 
    {
      _pCameraInfo->pNewObjImg5 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg5, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 5 || _pCameraInfo->imageCount == 13) 
    {
      _pCameraInfo->pNewObjImg6 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg6, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 6 || _pCameraInfo->imageCount == 14) 
    {
      _pCameraInfo->pNewObjImg7 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg7, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
    else if(_pCameraInfo->imageCount == 7 || _pCameraInfo->imageCount == 15) 
    {
      _pCameraInfo->pNewObjImg8 = pbyteImageBuffer;
      if(pvContext != NULL)
        SaveBitmap((const char*)pvContext,  _pCameraInfo->pNewObjImg8, (int)_nMaxWidth, (int)_nMaxHeight, (int)iBpp);
    }
  }
  
  StTrg_ClearBuffer(_pCameraInfo->m_hCamera); 

  _pCameraInfo->imageCount++;   
  _isTriggered = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Jack Hwee
//////////////////////////////////////////////////////////////////////////////
int reInitializeOpticalCamera(jint cameraDeviceId)
{
   int result = 0;
   HANDLE	m_aCamera1;
   HANDLE	m_aCamera2;
   BOOL success = TRUE;
   DWORD pdwCameraID = -1;
   TCHAR	cameraName[200]; 
	 memset(cameraName, 0, sizeof(TCHAR) * 200);
	 
   setDeviceIdToInstanceMap(cameraDeviceId);
   _pCameraInfo = getCameraInfo(cameraDeviceId);

   int numberOfOpticalCameraConnected = StCam_CameraCount();
  
   // If we have an open camera, close it
	 if (_pCameraInfo->m_hCamera != 0)
   {
     StTrg_Close(_pCameraInfo->m_hCamera);
     //XCR-2692: Proper handling when Optical Camera failed to initialize
     _pCameraInfo->m_hCamera = 0;
   }

	 m_aCamera1 = StTrg_Open();
	 m_aCamera2 = StTrg_Open();
	
	 if (cameraDeviceId == 1)
   {
       if (m_aCamera1)
       {          
           success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);
                                                                                                                                             
           if(!success)
      		 {     
      	      StTrg_Close(m_aCamera1); 
      	      return 1;	                
      	   }           
           
           if (_tcsstr(cameraName, _T("1")) != NULL) 
           {
      	      _pCameraInfo->m_hCamera = 	m_aCamera1;
      	      StTrg_Close(m_aCamera2);      	    
      	   }
           else
           {
              StTrg_Close(m_aCamera1);    
              
              success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);
    
              if((!success) || _tcsstr(cameraName, _T("1")) == NULL)
              {
                  StTrg_Close(m_aCamera2); 
                  return 1;
              } 
              else
              {
                  _pCameraInfo->m_hCamera = 	m_aCamera2; 
                  
              } 	     	                           
           }	
  	   }
  	   else
  	   {
  	      success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);
  	     
  	      if(!success)
  		    {
    	      StTrg_Close(m_aCamera2);
            return 1;
  	      } 
  	        	      
  	      if (_tcsstr(cameraName, _T("1")) != NULL) 
  	      {
  	         _pCameraInfo->m_hCamera = 	m_aCamera2;
  	      }
  	      else
  	      {
  	         StTrg_Close(m_aCamera2);
  	         return 1;
  	      }
  	      
  	      StTrg_Close(m_aCamera1);
  	   }

		// Set front module flag
		_pCameraInfo->isFrontModule = true;
   }
   else
   {
       if (m_aCamera2)
       {
           if (numberOfOpticalCameraConnected == 1) 
             success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);
           else
             success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);
 
           if(!success)
      		 {
      		    if (numberOfOpticalCameraConnected == 1)
      		    {
      	        StTrg_Close(m_aCamera1);
      	      }
      	      else
      	      {
      	        StTrg_Close(m_aCamera2); 
      	      }
      	   }
      	         	   
           if (_tcsstr(cameraName, _T("2")) != NULL) 
           {      
	            if (numberOfOpticalCameraConnected == 1) 
	            {
      	        _pCameraInfo->m_hCamera = m_aCamera1;
      	        StTrg_Close(m_aCamera2);
      	      }
      	      else
      	      {
      	        _pCameraInfo->m_hCamera = m_aCamera2;
      	        StTrg_Close(m_aCamera1);
              }
      	      
      	   }
      	   else
      	   {
      	      if (numberOfOpticalCameraConnected == 1)
      		    {
      	        StTrg_Close(m_aCamera1);
      	        return 1;
      	      }
      	      else
      	      { 	        
      	        StTrg_Close(m_aCamera2); 
      	        
      	        success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);
      	        
      	        if((!success) || (_tcsstr(cameraName, _T("2")) == NULL))
      	        {
                   StTrg_Close(m_aCamera1); 
                   return 1;
                }
                else
                {
                   _pCameraInfo->m_hCamera = m_aCamera1;
         
                }
      	        
      	      }
      	      
      	   }
  	   }
  	   else
  	   {
  	      success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);

  	      if(!success)
  		    {
    	      StTrg_Close(m_aCamera1);
            return 1;
  	      } 
  	      
  	      if (_tcsstr(cameraName, _T("2")) != NULL)  
  	      {
  	         _pCameraInfo->m_hCamera = 	m_aCamera1;
  	      }
  	      else
  	      {
  	         StTrg_Close(m_aCamera1);
  	         return 1;
  	      }
  	         
  	      StTrg_Close(m_aCamera2);
  	   }

		// Set rear module flag
		_pCameraInfo->isFrontModule = false;
   }      
 
   if(!_pCameraInfo->m_hCamera)
   {
      result = 3;
      return result;
   }
  
   success = StTrg_SetScanMode(_pCameraInfo->m_hCamera, STCAM_SCAN_MODE_NORMAL, 0, 0, 0, 0);
   
   StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)STCAM_TRIGGER_MODE_TYPE_FREE_RUN);   
  
   StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_OUT_PIN_MODE_DISABLE);

   if(!success)
   {
  	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
   }
  
   return result;
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeInitialize(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jint windowHandle, jboolean mirrorMode)
{  
  try
  {
    int result = 0;
    HANDLE	m_aCamera1;
    HANDLE	m_aCamera2;
    BOOL success = TRUE;
    DWORD pdwCameraID = -1;
    TCHAR	cameraName[200]; 
    memset(cameraName, 0, sizeof(TCHAR) * 200);
	  
    _isMirrorMode = mirrorMode;
	 
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    int numberOfOpticalCameraConnected = StCam_CameraCount();
  
    // If we have an open camera, close it
    if (_pCameraInfo->m_hCamera != 0)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      //XCR-2692: Proper handling when Optical Camera failed to initialize
      _pCameraInfo->m_hCamera = 0;
    }
 
    m_aCamera1 = StTrg_Open();
    m_aCamera2 = StTrg_Open();
	
    if (cameraDeviceId == 1)
    {
      if (m_aCamera1)
      {          
        success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);

        if(!success)
  	  	{     
  	      StTrg_Close(m_aCamera1); 
  	      return 1;	                
  	    }           
       
        if (_tcsstr(cameraName, _T("1")) != NULL) 
        {
  	      _pCameraInfo->m_hCamera = 	m_aCamera1;
  	      StTrg_Close(m_aCamera2);      	    
  	    }
        else
        {
          StTrg_Close(m_aCamera1);    
          
          success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);

          if((!success) || _tcsstr(cameraName, _T("1")) == NULL)
          {
            StTrg_Close(m_aCamera2); 
            return 1;
          } 
          else
          {
           _pCameraInfo->m_hCamera = 	m_aCamera2;        
          } 	     	                           
        }	
	    }
	    else
	    {
	      success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);
	     
	      if(!success)
		    {
  	      StTrg_Close(m_aCamera2);
          return 1;
	      } 
	        	      
	      if (_tcsstr(cameraName, _T("1")) != NULL) 
	      {
	        _pCameraInfo->m_hCamera = 	m_aCamera2;
	      }
	      else
	      {
	        StTrg_Close(m_aCamera2);
	        return 1;
	      }      
	      StTrg_Close(m_aCamera1);
	    }

      // Set front module flag
      _pCameraInfo->isFrontModule = true;
    }
    else
    {
      if (m_aCamera2)
      {
        if (numberOfOpticalCameraConnected == 1) 
          success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);
        else
          success = StTrg_ReadCameraUserID(m_aCamera2, &pdwCameraID, cameraName, 200);

        if(!success)
  	  	{
  		    if (numberOfOpticalCameraConnected == 1)
  		    {
  	        StTrg_Close(m_aCamera1);
  	      }
  	      else
  	      {
  	        StTrg_Close(m_aCamera2); 
  	      }
  	    }
  	         	   
        if (_tcsstr(cameraName, _T("2")) != NULL) 
        {      
          if (numberOfOpticalCameraConnected == 1) 
          {
  	        _pCameraInfo->m_hCamera = m_aCamera1;
  	        StTrg_Close(m_aCamera2);
  	      }
  	      else
  	      {
  	        _pCameraInfo->m_hCamera = m_aCamera2;
  	        StTrg_Close(m_aCamera1);
          } 	      
  	    }
  	    else
  	    {
  	      if (numberOfOpticalCameraConnected == 1)
  		    {
  	        StTrg_Close(m_aCamera1);
  	        return 1;
  	      }
  	      else
  	      { 	        
  	        StTrg_Close(m_aCamera2); 
  	        
  	        success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);
  	        
  	        if((!success) || (_tcsstr(cameraName, _T("2")) == NULL))
  	        {
              StTrg_Close(m_aCamera1); 
              return 1;
            }
            else
            {
              _pCameraInfo->m_hCamera = m_aCamera1;     
            } 	        
  	      }  	      
    	  }
  	  }
  	  else
  	  {
  	    success = StTrg_ReadCameraUserID(m_aCamera1, &pdwCameraID, cameraName, 200);

  	    if(!success)
  		  {
    	     StTrg_Close(m_aCamera1);
           return 1;
  	    } 
  	      
  	    if (_tcsstr(cameraName, _T("2")) != NULL)  
  	    {
  	      _pCameraInfo->m_hCamera = 	m_aCamera1;
  	    }
  	    else
  	    {
  	      StTrg_Close(m_aCamera1);
  	      return 1;
  	    }  	         
  	    StTrg_Close(m_aCamera2);
  	  }

      // Set rear module flag
      _pCameraInfo->isFrontModule = false;
    }      
 
    if(!_pCameraInfo->m_hCamera)
    {
      result = 3;
      return result;
    }
  
    success = StTrg_SetScanMode(_pCameraInfo->m_hCamera, STCAM_SCAN_MODE_NORMAL, 0, 0, 0, 0);
    success = StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)STCAM_TRIGGER_MODE_TYPE_FREE_RUN);
    success = StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_OUT_PIN_MODE_DISABLE);
	
    // Reset everything
    _pCameraInfo->reset();	

    if(!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      return -1;
    }
 
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeLiveVideo(JNIEnv *jEnv, jobject javaObject, jint cameraDeviceId, jint windowHandle)
{  
  try
  {
    int result = 0;
    int num = 0;
 
    BYTE	*pbyteImageBuffer = NULL;
    CGfxDraw		m_gfx; 
    BOOL success = TRUE;
    DWORD pdwCameraID = -1;
    TCHAR	cameraName[200];
	  memset(cameraName, 0, sizeof(TCHAR) * 200);
    int sentechCameraId = -1;
                           
    HANDLE	m_aCamera1;
    HANDLE	m_aCamera2;
	
	_windowHandle = windowHandle;
    _isTriggered = FALSE;
    
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
 
    if(!_pCameraInfo->m_hCamera)
    {
       result = reInitializeOpticalCamera(cameraDeviceId);
       
       if (result != 0) 
         return result;
    }
   
    if (success)
    {        
      if(_pCameraInfo->m_hCamera)
      {    
        success = StTrg_SetRcvMsgWnd(_pCameraInfo->m_hCamera, (HWND) windowHandle);
   
        if(!success)
    		{
    	     StTrg_Close(_pCameraInfo->m_hCamera);
           return -1;
    	  }
  
        // Cheah Lee Herng
        if (_isMirrorMode == TRUE)
        {
          transferEndCallback = new funcTransferEndCallback(nativeMirrorAndRotateCallback); 
          success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, nativeMirrorAndRotateCallback, NULL) ;
                
          if(!success)
          {
             StTrg_Close(_pCameraInfo->m_hCamera);
             return -1;
          }
        }
   
        success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);
        
        if(!success)
    		{
    	     StTrg_Close(_pCameraInfo->m_hCamera);
           return -1;
    	  }
 
        //Get Image Size
  		  DWORD	dwImageSizeMode;
  		  DWORD	dwOffsetX,dwOffsetY;
  		  DWORD	dwWidth,dwHeight;
  		  WORD	wScanMode;
  	
  		  StTrg_GetScanMode(_pCameraInfo->m_hCamera, &wScanMode,
  				&dwOffsetX, &dwOffsetY, &dwWidth, &dwHeight);
  	
  		  if(!success)
    		{
    	     StTrg_Close(_pCameraInfo->m_hCamera);
           return -1;
    	  }
  				 
  			//Allocate Memory
  	   	DWORD	dwBufferSize = dwWidth * dwHeight;
  	   	
  	  	pbyteImageBuffer = new BYTE[dwBufferSize];

  	  	//Take Snap Shot
  	  	DWORD	dwNumberOfByteTrans,dwFrameNo;
  	  	DWORD	dwMilliseconds = 1000;
  	 
  	    success = StTrg_TakeRawSnapShot(_pCameraInfo->m_hCamera,pbyteImageBuffer, dwBufferSize, &dwNumberOfByteTrans, &dwFrameNo, dwMilliseconds);
 
  	    if(!success)
    		{
    	     StTrg_Close(_pCameraInfo->m_hCamera);
           return -1;
    	  }
    	  
    	  if (_isMirrorMode == FALSE)
    	  {
    	    BYTE *cropPbyteImageBuffer = NULL;
    	    CGfxDraw		m_gfx;
    	    cropPbyteImageBuffer = new BYTE[dwBufferSize];
        
          //Allocate Memory
          DWORD	dwBufferSize1 = _nMaxWidth * _nMaxHeight;
          cropPbyteImageBuffer = new BYTE[dwBufferSize1];
  
          cropPbyteImageBuffer = cropImage(pbyteImageBuffer, dwWidth, dwHeight);
		
  	    	HDC hdc = GetDC((HWND)windowHandle); 

          HDC hdc2 = CreateCompatibleDC(hdc);
          HBITMAP bmp = CreateCompatibleBitmap(hdc,_nMaxWidth,_nMaxHeight);
          HBITMAP oldBmp = (HBITMAP) SelectObject(hdc2, bmp); 
  
          m_gfx.SetImg(cropPbyteImageBuffer, _nMaxWidth, _nMaxHeight);
          m_gfx.Draw(hdc);
  	      
          // Final draw to pDC
        	BitBlt(hdc,
                 0,0, 
                 _nMaxWidth, _nMaxHeight, 
                 hdc2, 
                 0,0, 
                 SRCPAINT);
         
  	  		SelectObject(hdc2,oldBmp);
  			
  	  		ReleaseDC((HWND)windowHandle, hdc);
  		
  	      DeleteDC (hdc2);
  	      DeleteObject(bmp);
  	  		DeleteObject(hdc2);	
  	  		
  	  		if(cropPbyteImageBuffer)
  	      {
  		      delete[] cropPbyteImageBuffer;
  	      }
        }
	  
  		success = StTrg_StopTransfer(_pCameraInfo->m_hCamera);

      	if (_isMirrorMode == TRUE)
        {
          do {  
			Sleep(100);
          } while (_isTriggered == FALSE );
        }		                        
  
  		if(!success)
    	{
    	   StTrg_Close(_pCameraInfo->m_hCamera);
           return -1;
    	}
 
        if(pbyteImageBuffer)
  	    {
  		    delete[] pbyteImageBuffer;
  	    }
  	    
		// Lee Herng 28 Aug 2013 - Comment out the calling of StTrg_ClearBuffer
		//                         because it will cause V810 GUI disappear if 
		//						   you click Cancel button in Live Video		
  	    //success = StTrg_ClearBuffer(_pCameraInfo->m_hCamera);
     }
     else
     {
        result = -1;
     }
   }
   
    return result;
  
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeExitCamera(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
   try
   {  
      BOOL success = TRUE;
      setDeviceIdToInstanceMap(cameraDeviceId);
      _pCameraInfo = getCameraInfo(cameraDeviceId);
  
      if(_pCameraInfo->m_hCamera != 0)
      {
	      StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)STCAM_TRIGGER_MODE_TYPE_FREE_RUN);   
  
        StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_OUT_PIN_MODE_DISABLE);
   
        success = StTrg_ClearBuffer(_pCameraInfo->m_hCamera); 
      
        StTrg_Close(_pCameraInfo->m_hCamera); 
     
        _pCameraInfo->m_hCamera = 0; 
   
        if(!success)
	    	{
          return -1;
	      }
      }
             
      return 0;  
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetWindowHandle(JNIEnv *pEnv, jobject javaObject, jobject comp)
{
 try
  {
    HMODULE hAWT = 0;
    HWND hWnd = 0;
    typedef jboolean (JNICALL *PJAWT_GETAWT)(JNIEnv*, JAWT*);
    JAWT awt;
    JAWT_DrawingSurface* ds;
    JAWT_DrawingSurfaceInfo* dsi;
    JAWT_Win32DrawingSurfaceInfo* dsi_win;
    jboolean result;
    jint lock;
  
    //Load AWT Library
    if(!hAWT)
        //for Java 1.4
        hAWT = LoadLibrary("jawt.dll");
      
    if(!hAWT)
        //for Java 1.3
        hAWT = LoadLibrary("awt.dll");

    if(hAWT)
    {
      PJAWT_GETAWT JAWT_GetAWT = (PJAWT_GETAWT)GetProcAddress(hAWT, "JAWT_GetAWT");
      if(JAWT_GetAWT)
      {
        awt.version = JAWT_VERSION_1_4; // Init here with JAWT_VERSION_1_3 or JAWT_VERSION_1_4
        //Get AWT API Interface
        result = JAWT_GetAWT(pEnv, &awt);
        if(result != JNI_FALSE)
        {
            ds = awt.GetDrawingSurface(pEnv, comp);
            if(ds != NULL)
            {
                lock = ds->Lock(ds);
    
                if((lock & JAWT_LOCK_ERROR) == 0)
                {
                    dsi = ds->GetDrawingSurfaceInfo(ds);
                    if(dsi)
                    {
                        dsi_win = (JAWT_Win32DrawingSurfaceInfo*)dsi->platformInfo;
                        if(dsi_win)
                        {
                          hWnd = dsi_win->hwnd;
                        }
                        else 
                        {
                          hWnd = (HWND) -1;
                        }
                        ds->FreeDrawingSurfaceInfo(dsi);
                    }
                    else 
                    {
                      hWnd = (HWND) -2;
                    }
                    ds->Unlock(ds);
                }
                else 
                {
                  hWnd = (HWND) -3;
                }
                awt.FreeDrawingSurface(ds);
            }
            else 
            {
              hWnd = (HWND) -4;
            }
        }
        else 
        {
          hWnd = (HWND) -5;
        }
      }
      else 
      {
        hWnd = (HWND) -6;
      }
    }
    else 
    {
      hWnd = (HWND) -7;
    }
    return (jint)hWnd;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeAcquireOfflineImages(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jstring fullPathFileName, jboolean forceTrigger)
{
   try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
   
     // Reset trigger mode flag	
     _isTriggerModeReady = FALSE;
   
     int result = 0;
     const char *nativeFullPathFileName = NULL;
     BOOL success = TRUE;          
     _isTriggered = FALSE;
    
     if (fullPathFileName != NULL)
       nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
	 
	 StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);
     StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);
     StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	 
	 // Setting up trigger timeout
	 StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);
      
     transferEndCallback = new funcTransferEndCallback(nativeAcquireOfflineImagesCallback);
	 
     //Set Callback Functions	 
     success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, *transferEndCallback, (PVOID)nativeFullPathFileName) ;
 
     success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);
	 
	 // Set trigger mode flag to true
	 _isTriggerModeReady = TRUE;
	 
	 int waitForTriggerModeReadyTime = 0;
	 while ((_isTriggered == FALSE) && (waitForTriggerModeReadyTime < _waitForTriggerModeReadyTimeout))
	 {
	   Sleep(100);
	   waitForTriggerModeReadyTime += 100;
	 }
	 
	 if (_isTriggered == FALSE)
	 {
	   return -1;
	 }
	 
	 // Reset trigger mode flag
	 _isTriggerModeReady = FALSE;
     
     StTrg_StopTransfer(_pCameraInfo->m_hCamera);  		
     if(!success)
     {
	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
     }
     
     if(fullPathFileName != NULL)                                         	   
	   pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);  
 
	 return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetNumberOfCameras(JNIEnv *pEnv, jobject javaObject)
{
  try
  {
    DWORD nNumCam = 0;
  
    nNumCam = StCam_CameraCount();
   
    return nNumCam;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeEnableAutoGain(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jboolean enable)
{
  try
  {
    BOOL success = TRUE;
    
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    if (enable == false)
      success = StTrg_SetWhiteBalanceMode(_pCameraInfo->m_hCamera, STCAM_WB_OFF);
    else
      success = StTrg_SetWhiteBalanceMode(_pCameraInfo->m_hCamera, STCAM_WB_FULLAUTO);
       
    if (!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      return -1;
    }  
  
    return 0;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Jack Hwee
 */  
JNIEXPORT void JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeInitializePspAlgorithm(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
   setDeviceIdToInstanceMap(cameraDeviceId);
   _pCameraInfo = getCameraInfo(cameraDeviceId);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
} 

/**
 * @author Jack Hwee
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeLoadCalibrationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath); 
    
    return isSuccess;    
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeLoadNewCalibrationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    // Make sure New PSP Algorithm is initialized
    //if (!_pCameraInfo->newPspAlgorithm->isInitialized())
    //{
      _pCameraInfo->newPspAlgorithm->Initialize(_nMaxWidth, _nMaxHeight, _pCameraInfo->numberOfProjector);
    //}
  
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    isSuccess = _pCameraInfo->newPspAlgorithm->LoadCalibrationAndReferenceData(nativeFullPath);
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath); 
    
    return isSuccess;    
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetNewCoordinateXInPixel(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    jfloatArray resultArray;
    
    if(_pCameraInfo->pNewCoordinateXInPixel != NULL)
    {
      jfloat temp[256] = {0.0};
      int newCoordinateXSize = _pCameraInfo->roiCount;
     
      // Initialize int new Coordinate X array
      resultArray = pEnv->NewFloatArray(newCoordinateXSize);
    
      for(unsigned int i=0; i < newCoordinateXSize; ++i)
        temp[i] = _pCameraInfo->pNewCoordinateXInPixel[i];
     
      // move from the temp structure to the java structure
      pEnv->SetFloatArrayRegion(resultArray, 0, newCoordinateXSize, temp);
      return resultArray; 
    }
    else
    {
      resultArray = pEnv->NewFloatArray(0);
      return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetNewCoordinateYInPixel(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    jfloatArray resultArray;
    
    if(_pCameraInfo->pNewCoordinateYInPixel != NULL)
    {
      jfloat temp[256] = {0.0};
      int newCoordinateYSize = _pCameraInfo->roiCount;
     
      // Initialize int new Coordinate Y array
      resultArray = pEnv->NewFloatArray(newCoordinateYSize);
    
      for(unsigned int i=0; i < newCoordinateYSize; ++i)
        temp[i] = _pCameraInfo->pNewCoordinateYInPixel[i];
     
      // move from the temp structure to the java structure
      pEnv->SetFloatArrayRegion(resultArray, 0, newCoordinateYSize, temp);
      return resultArray; 
    }
    else
    {
      resultArray = pEnv->NewFloatArray(0);
      return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Jack Hwee
 */ 
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativePerformCalibration(JNIEnv* pEnv, jclass object, jint cameraDeviceId, 
                                                                      jdouble referencePlaneKnownHeight, 
                                                                      jdouble objectPlanePlusThreeKnownHeight, 
                                                                      jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
           
   
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
  
    //isSuccess = _pCameraInfo->pspAlgorithm->performCalibration(knownHeight, nativeFullPath);
                                                               
    // memory free?
  /*  if (_pCameraInfo->pcImageMemory != NULL)
    {
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 2);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 3);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 4);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 5);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 6);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 7);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 8);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 9);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 10);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 11);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 12);
      is_FreeImageMem (_pCameraInfo->m_hCamera, _pCameraInfo->pcImageMemory, 13);
    }     */   
    
       // memory free?
    if(_pCameraInfo->pRefImg1 != NULL)
		  free(_pCameraInfo->pRefImg1);
		    
		if(_pCameraInfo->pRefImg2 != NULL)
		  free(_pCameraInfo->pRefImg2);
		    
		if(_pCameraInfo->pRefImg3 != NULL)
		  free(_pCameraInfo->pRefImg3);
		    
		if(_pCameraInfo->pRefImg4 != NULL)
		  free(_pCameraInfo->pRefImg4);
		    
		if(_pCameraInfo->pObjImg1 != NULL)
		  free(_pCameraInfo->pObjImg1);
		
		if(_pCameraInfo->pObjImg2 != NULL)
		  free(_pCameraInfo->pObjImg2);
		    
		if(_pCameraInfo->pObjImg3 != NULL)
	    free(_pCameraInfo->pObjImg3);
		    
		if(_pCameraInfo->pObjImg4 != NULL)
  		 free(_pCameraInfo->pObjImg4);
	 	    
		if(_pCameraInfo->pObjPlusThreeImg1 != NULL)
		  free(_pCameraInfo->pObjPlusThreeImg1);
		
		if(_pCameraInfo->pObjPlusThreeImg2 != NULL)
		    free(_pCameraInfo->pObjPlusThreeImg2);
		    
		if(_pCameraInfo->pObjPlusThreeImg3 != NULL)
		    free(_pCameraInfo->pObjPlusThreeImg3);
		    
		if(_pCameraInfo->pObjPlusThreeImg4 != NULL)
		   free(_pCameraInfo->pObjPlusThreeImg4);
  
    if(_pCameraInfo->pcImageMemory != NULL)
		    delete[] _pCameraInfo->pcImageMemory;
		    
		if(transferEndCallback != NULL)
		    delete[] transferEndCallback;
    
    StTrg_ClearBuffer(_pCameraInfo->m_hCamera);

    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath);     
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativePerformNewCalibration(JNIEnv* pEnv, jclass object, jint cameraDeviceId, 
																			jdouble calibrationHeight, 
																			jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    // Make sure New PSP Algorithm is initialized
    if (!_pCameraInfo->newPspAlgorithm->isInitialized())
    {
      _pCameraInfo->newPspAlgorithm->Initialize(_nMaxWidth, _nMaxHeight, _pCameraInfo->numberOfProjector);
    }
    
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    // Setup vector list
    std::vector<unsigned char*> calImageVector;
    calImageVector.clear();
    calImageVector.push_back(_pCameraInfo->pNewObjImg1);
    calImageVector.push_back(_pCameraInfo->pNewObjImg2);
    calImageVector.push_back(_pCameraInfo->pNewObjImg3);
    calImageVector.push_back(_pCameraInfo->pNewObjImg4);
    calImageVector.push_back(_pCameraInfo->pNewObjImg5);
    calImageVector.push_back(_pCameraInfo->pNewObjImg6);
    calImageVector.push_back(_pCameraInfo->pNewObjImg7);
    calImageVector.push_back(_pCameraInfo->pNewObjImg8);
    
    std::vector<unsigned char*> refImageVector;
    refImageVector.clear();
    refImageVector.push_back(_pCameraInfo->pNewRefImg1);
    refImageVector.push_back(_pCameraInfo->pNewRefImg2);
    refImageVector.push_back(_pCameraInfo->pNewRefImg3);
    refImageVector.push_back(_pCameraInfo->pNewRefImg4);
    refImageVector.push_back(_pCameraInfo->pNewRefImg5);
    refImageVector.push_back(_pCameraInfo->pNewRefImg6);
    refImageVector.push_back(_pCameraInfo->pNewRefImg7);
    refImageVector.push_back(_pCameraInfo->pNewRefImg8);
    
    isSuccess = _pCameraInfo->newPspAlgorithm->oneStepCalibrateThruImageMultiFringe_12X(calibrationHeight, 
                                                                                        calImageVector, 
                                                                                        refImageVector, 
                                                                                        nativeFullPath);
                                                                                        
    // Free object image
    if(_pCameraInfo->pNewObjImg1 != NULL)
		  free(_pCameraInfo->pNewObjImg1);
		  
		if(_pCameraInfo->pNewObjImg2 != NULL)
		  free(_pCameraInfo->pNewObjImg2);
		  
	  if(_pCameraInfo->pNewObjImg3 != NULL)
		  free(_pCameraInfo->pNewObjImg3);
		  
	  if(_pCameraInfo->pNewObjImg4 != NULL)
		  free(_pCameraInfo->pNewObjImg4);
		  
		if(_pCameraInfo->pNewObjImg5 != NULL)
		  free(_pCameraInfo->pNewObjImg5);
		  
		if(_pCameraInfo->pNewObjImg6 != NULL)
		  free(_pCameraInfo->pNewObjImg6);
		  
		if(_pCameraInfo->pNewObjImg7 != NULL)
		  free(_pCameraInfo->pNewObjImg7);
		  
		if(_pCameraInfo->pNewObjImg8 != NULL)
		  free(_pCameraInfo->pNewObjImg8);
		
    // Free reference image  
		if(_pCameraInfo->pNewRefImg1 != NULL)
		  free(_pCameraInfo->pNewRefImg1);
		  
		if(_pCameraInfo->pNewRefImg2 != NULL)
		  free(_pCameraInfo->pNewRefImg2);
		  
	  if(_pCameraInfo->pNewRefImg3 != NULL)
		  free(_pCameraInfo->pNewRefImg3);
		  
	  if(_pCameraInfo->pNewRefImg4 != NULL)
		  free(_pCameraInfo->pNewRefImg4);
		  
		if(_pCameraInfo->pNewRefImg5 != NULL)
		  free(_pCameraInfo->pNewRefImg5);
		  
		if(_pCameraInfo->pNewRefImg6 != NULL)
		  free(_pCameraInfo->pNewRefImg6);
		  
		if(_pCameraInfo->pNewRefImg7 != NULL)
		  free(_pCameraInfo->pNewRefImg7);
		  
		if(_pCameraInfo->pNewRefImg8 != NULL)
		  free(_pCameraInfo->pNewRefImg8);
		  
		if(_pCameraInfo->pcImageMemory != NULL)
		    delete[] _pCameraInfo->pcImageMemory;
		    
		if(transferEndCallback != NULL)
		    delete[] transferEndCallback;
    
    StTrg_ClearBuffer(_pCameraInfo->m_hCamera);

    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath);     
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}


/**
 * @author Jack Hwee
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetInspectionROI(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint roiCount, jintArray centerX, jintArray centerY, jintArray width, jintArray height)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;
    jboolean isCopy;
    
    // Get pointer to int array
    jint *centerXArray = pEnv->GetIntArrayElements(centerX, &isCopy);
    jint *centerYArray = pEnv->GetIntArrayElements(centerY, &isCopy);
    jint *widthArray = pEnv->GetIntArrayElements(width, &isCopy);
    jint *heightArray = pEnv->GetIntArrayElements(height, &isCopy);
    
    // Get the size of each input array
    jsize centerXArrayLength = pEnv->GetArrayLength(centerX);
    jsize centerYArrayLength = pEnv->GetArrayLength(centerY);
    jsize widthArrayLength = pEnv->GetArrayLength(width);
    jsize heightArrayLength = pEnv->GetArrayLength(height);
    
    // Populate CenterX array
    for(unsigned int i=0; i < centerXArrayLength; ++i)
        _pCameraInfo->centerX[i] = centerXArray[i];
        
    // Populate CenterY array
    for(unsigned int i=0; i < centerYArrayLength; ++i)
        _pCameraInfo->centerY[i] = centerYArray[i];
        
    // Populate Width array
    for(unsigned int i=0; i < widthArrayLength; ++i)
        _pCameraInfo->width[i] = widthArray[i];
        
    // Populate Height array
    for(unsigned int i=0; i < heightArrayLength; ++i)
        _pCameraInfo->height[i] = heightArray[i];
    
    _pCameraInfo->roiCount = (unsigned int) roiCount;                                                             

    // Release pointer
    pEnv->ReleaseIntArrayElements(centerX, centerXArray, 0);
    pEnv->ReleaseIntArrayElements(centerY, centerYArray, 0);
    pEnv->ReleaseIntArrayElements(width, widthArray, 0);
    pEnv->ReleaseIntArrayElements(height, heightArray, 0);
    
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetNewInspectionROI(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint roiCount, jintArray centerX, jintArray centerY, jintArray width, jintArray height)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    jboolean isCopy;
    
    // Get pointer to int array
    jint *centerXArray = pEnv->GetIntArrayElements(centerX, &isCopy);
    jint *centerYArray = pEnv->GetIntArrayElements(centerY, &isCopy);
    jint *widthArray = pEnv->GetIntArrayElements(width, &isCopy);
    jint *heightArray = pEnv->GetIntArrayElements(height, &isCopy);
    
    // Get the size of each input array
    jsize centerXArrayLength = pEnv->GetArrayLength(centerX);
    jsize centerYArrayLength = pEnv->GetArrayLength(centerY);
    jsize widthArrayLength = pEnv->GetArrayLength(width);
    jsize heightArrayLength = pEnv->GetArrayLength(height);
    
    // Populate CenterX array
    for(unsigned int i=0; i < centerXArrayLength; ++i)
        _pCameraInfo->centerX[i] = centerXArray[i];
        
    // Populate CenterY array
    for(unsigned int i=0; i < centerYArrayLength; ++i)
        _pCameraInfo->centerY[i] = centerYArray[i];
        
    // Populate Width array
    for(unsigned int i=0; i < widthArrayLength; ++i)
        _pCameraInfo->width[i] = widthArray[i];
        
    // Populate Height array
    for(unsigned int i=0; i < heightArrayLength; ++i)
        _pCameraInfo->height[i] = heightArray[i];
    
    _pCameraInfo->roiCount = (unsigned int) roiCount;
  
    // Setup ROI vector
    _pCameraInfo->inspectionRoiVector.clear();
    for(int i=0; i<roiCount; ++i)
    {
      Roi3DStruct inspectionRoi = {};
      inspectionRoi.x  = _pCameraInfo->centerX[i];
      inspectionRoi.y  = _pCameraInfo->centerY[i];
      inspectionRoi.dX = _pCameraInfo->width[i];
      inspectionRoi.dY = _pCameraInfo->height[i];
      
      int* pHeightMap = new int[inspectionRoi.dX * inspectionRoi.dY];
      int* pQualityMap = new int[inspectionRoi.dX * inspectionRoi.dY];
  
      inspectionRoi.pHeightMap = pHeightMap;
      inspectionRoi.pQualityMap = pQualityMap;
      
      // TODO: We may need new PSP 3D Algo to compensate based on camera magnification
      // For now, we just have to use back the original coordinate X and Y
      _pCameraInfo->pNewCoordinateXInPixel[i] = inspectionRoi.x;
      _pCameraInfo->pNewCoordinateYInPixel[i] = inspectionRoi.y;
      
      _pCameraInfo->inspectionRoiVector.push_back(inspectionRoi);
    }
                                                             
    // Release pointer
    pEnv->ReleaseIntArrayElements(centerX, centerXArray, 0);
    pEnv->ReleaseIntArrayElements(centerY, centerYArray, 0);
    pEnv->ReleaseIntArrayElements(width, widthArray, 0);
    pEnv->ReleaseIntArrayElements(height, heightArray, 0);
    
    return true;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGenerateHeightMap(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jfloat adjustment, jstring fullPathFileName, jint minusPlusSet, jboolean forceTrigger)
{
  try
  {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
     _isTriggered = FALSE; 
     
    // Reset member variables before proceed
    if (_pCameraInfo->imageCount > 4)
    {
      _pCameraInfo->reset();
    }

     const char *nativeFullPathFileName = NULL; 
     BOOL success = TRUE;
     jfloatArray resultArray;   
    
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
 
     if (_pCameraInfo->imageCount == 0)
     {
       StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);
       
       StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);
      
       StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	   
	   // Setting up trigger timeout	  
	   StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);
   
       transferEndCallback = new funcTransferEndCallback(nativeGenerateHeightMapCallback); 
     }
     //Set Callback Functions	 
     success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, *transferEndCallback, (PVOID)nativeFullPathFileName) ;
 
     success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);

	 int waitForTriggerModeReadyTime = 0;
	 while ((_isTriggered == FALSE) && (waitForTriggerModeReadyTime < _waitForTriggerModeReadyTimeout))
	 {
	   Sleep(100);
	   waitForTriggerModeReadyTime += 100;
	 }
	 
	 if (_isTriggered == FALSE)
	 {
	   resultArray = pEnv->NewFloatArray(0);
       return resultArray;
	 }
  	    	    
     StTrg_StopTransfer(_pCameraInfo->m_hCamera);  		
     if(!success)
     {
   	   resultArray = pEnv->NewFloatArray(0);
       return resultArray;
     }
     
     int iBpp(1);
	
     if (_pCameraInfo->imageCount == 4)
     {           
	     StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)STCAM_TRIGGER_MODE_TYPE_FREE_RUN);   
  
       StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_OUT_PIN_MODE_DISABLE);          
   
      if (success == false)
      {
        resultArray = pEnv->NewFloatArray(0);
        return resultArray;
      }
      else
      {
        // Free Image Memory 
        StTrg_ClearBuffer(_pCameraInfo->m_hCamera);
        	    
		    if(_pCameraInfo->pObjImg1)
		       free(_pCameraInfo->pObjImg1);
		  
		    if(_pCameraInfo->pObjImg2)
		      free(_pCameraInfo->pObjImg2);
		       
		    if(_pCameraInfo->pObjImg3)
		      free(_pCameraInfo->pObjImg3);
		       
		    if(_pCameraInfo->pObjImg4)
		      free(_pCameraInfo->pObjImg4);  
          
        if(transferEndCallback != NULL)
		      delete[] transferEndCallback;
      }
      _pCameraInfo->imageCount = 0;
    }
 
    if(fullPathFileName != NULL) 
       pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
   
    if(_pCameraInfo->pHeightMap != NULL && success)
    {    
     jfloat temp[256] = {0.0};
     int heightMapSize = _pCameraInfo->roiCount;
     
     // Initialize int Height Map array
     resultArray = pEnv->NewFloatArray(heightMapSize);
    
     for(unsigned int i=0; i < heightMapSize; ++i) 
        temp[i] = _pCameraInfo->pHeightMap[i];
   
     // move from the temp structure to the java structure
     pEnv->SetFloatArrayRegion(resultArray, 0, heightMapSize, temp);
     return resultArray;  
    }
    else
    {
     resultArray = pEnv->NewFloatArray(0);
     return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @return returnActualHeightMapValue Boolean flag to determine if we want to return the average height map or actual height map directly from PSP 3D library
 * @author Jack Hwee
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGenerateNewHeightMap(JNIEnv* pEnv, 
																		   jclass object, 
																		   jint cameraDeviceId, 
																		   jstring fullPathFileName, 
																		   jint inspectionQualityThreshold, 
																		   jint refLowerPhaseLimit, 
																		   jboolean forceTrigger,
																		   jboolean returnActualHeightMapValue)
{
  try
  {
    char msg[1024];

    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    _isTriggered = FALSE;
	
    // Reset trigger mode flag
    _isTriggerModeReady = FALSE;
	
    const char *nativeFullPathFileName = NULL; 
    BOOL success = TRUE;
    jfloatArray resultArray;
    BYTE *pbyteImageBuffer = NULL;
    BYTE *cropPbyteImageBuffer = NULL;
	     
    if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
 
    if (_pCameraInfo->imageCount == 0)
    {
      StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);
      StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);
      StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	  
      // Setting up trigger timeout	   
      StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);
   
      // Setting callback function
      transferEndCallback = new funcTransferEndCallback(nativeGenerateNewHeightMapCallback);
    }

    //Set scan mode
    success = StTrg_SetScanMode(_pCameraInfo->m_hCamera,
                                (WORD)STCAM_SCAN_MODE_PARTIAL_2,
                                0,0,0,0);    
	 
    //Set Callback Functions	 
    success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, *transferEndCallback, (PVOID)nativeFullPathFileName);	
    success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);
   
    // Set trigger mode flag to true
    _isTriggerModeReady = TRUE;
    
    int waitForTriggerModeReadyTime = 0;
    while ((_isTriggered == FALSE) && (waitForTriggerModeReadyTime < _waitForTriggerModeReadyTimeout))
    {
      Sleep(_sleepWaitInMiliseconds);
      waitForTriggerModeReadyTime += _sleepWaitInMiliseconds;
    }
	 
    if (_isTriggered == FALSE)
    {
      return NULL;
    }
	
    // Reset trigger mode flag
    _isTriggerModeReady = FALSE;
	
    StTrg_StopTransfer(_pCameraInfo->m_hCamera);
  
    // Release path string
    if(fullPathFileName != NULL) 
      pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);
	
    if(!success)
    {
   	  resultArray = pEnv->NewFloatArray(0);
      return resultArray;
    }
     
    if (_pCameraInfo->imageCount == 8)
    {      
      // Setup image vector
      std::vector<FRINGE_BUFFER3D> objImageVector;
      objImageVector.clear();
      
      FRINGE_BUFFER3D bufferObjImage1 = {};
      FRINGE_BUFFER3D bufferObjImage2 = {};
      FRINGE_BUFFER3D bufferObjImage3 = {};
      FRINGE_BUFFER3D bufferObjImage4 = {};
      FRINGE_BUFFER3D bufferObjImage5 = {};
      FRINGE_BUFFER3D bufferObjImage6 = {};
      FRINGE_BUFFER3D bufferObjImage7 = {};
      FRINGE_BUFFER3D bufferObjImage8 = {};
      
      bufferObjImage1.bufferId = 0;
      bufferObjImage2.bufferId = 1;
      bufferObjImage3.bufferId = 2;
      bufferObjImage4.bufferId = 3;
      bufferObjImage5.bufferId = 4;
      bufferObjImage6.bufferId = 5;
      bufferObjImage7.bufferId = 6;
      bufferObjImage8.bufferId = 7;
  
      bufferObjImage1.pDst = _pCameraInfo->pNewObjImg1;
      bufferObjImage2.pDst = _pCameraInfo->pNewObjImg2;
      bufferObjImage3.pDst = _pCameraInfo->pNewObjImg3;
      bufferObjImage4.pDst = _pCameraInfo->pNewObjImg4;
      bufferObjImage5.pDst = _pCameraInfo->pNewObjImg5;
      bufferObjImage6.pDst = _pCameraInfo->pNewObjImg6;
      bufferObjImage7.pDst = _pCameraInfo->pNewObjImg7;
      bufferObjImage8.pDst = _pCameraInfo->pNewObjImg8;
      
      objImageVector.push_back(bufferObjImage1);
      objImageVector.push_back(bufferObjImage2);
      objImageVector.push_back(bufferObjImage3);
      objImageVector.push_back(bufferObjImage4);
      objImageVector.push_back(bufferObjImage5);
      objImageVector.push_back(bufferObjImage6);
      objImageVector.push_back(bufferObjImage7);
      objImageVector.push_back(bufferObjImage8);
      
      // Make sure New PSP Algorithm is initialized
      if (!_pCameraInfo->newPspAlgorithm->isInitialized())
      {
        _pCameraInfo->newPspAlgorithm->Initialize(_nMaxWidth, _nMaxHeight, _pCameraInfo->numberOfProjector);
      }
 
      // Call New PSP Algorithm to perform actual height map calculation
      success = _pCameraInfo->newPspAlgorithm->CalculateHeightWithReferenceInputMultiFringe_MultiThread(objImageVector, 
                                                                                                        _pCameraInfo->inspectionRoiVector, 
                                                                                                        inspectionQualityThreshold, 
                                                                                                        refLowerPhaseLimit,
                                                                                                        "C:\\temp", // This is temporary folder
                                                                                                        false);
																										
      // Free Image Memory 
      StTrg_ClearBuffer(_pCameraInfo->m_hCamera);	  
	  
      if(_pCameraInfo->pNewObjImg1) free(_pCameraInfo->pNewObjImg1);
      if(_pCameraInfo->pNewObjImg2) free(_pCameraInfo->pNewObjImg2);
      if(_pCameraInfo->pNewObjImg3) free(_pCameraInfo->pNewObjImg3);
      if(_pCameraInfo->pNewObjImg4) free(_pCameraInfo->pNewObjImg4);
      if(_pCameraInfo->pNewObjImg5) free(_pCameraInfo->pNewObjImg5);
      if(_pCameraInfo->pNewObjImg6) free(_pCameraInfo->pNewObjImg6);
      if(_pCameraInfo->pNewObjImg7) free(_pCameraInfo->pNewObjImg7);
      if(_pCameraInfo->pNewObjImg8) free(_pCameraInfo->pNewObjImg8);

      // Reset image count
      _pCameraInfo->imageCount = 0;
   
      if (success == false)
      {
        resultArray = pEnv->NewFloatArray(0);
        return resultArray;
      }
      else
      {	  
        // Clear Callback pointer function
        if(transferEndCallback != NULL)
          delete[] transferEndCallback;
		  
        // Variable returnActualHeightMapValue is to control whether we want to return 
        // average height map value or whole height map defined by ROI
        if (returnActualHeightMapValue)
        {
          //_pCameraInfo->roiCount = (unsigned int) totalRoiSize;
          // Get total ROI size
          int heightMapSize = _pCameraInfo->roiCount;
          
          // Since we are returning the whole actual height map value,
          // we always get the last ROI
          Roi3DStruct roi = _pCameraInfo->inspectionRoiVector.at(heightMapSize - 1);
          int* pHeightMap = roi.pHeightMap;
          int totalRoiSize = roi.dX * roi.dY;
            
          // Initialize int Height Map array
          resultArray = pEnv->NewFloatArray(totalRoiSize);
          
          // Initialize temp float array to maximum image size
          int nImagePixel = _nMaxWidth * _nMaxHeight;
          jfloat* temp;
          temp = new jfloat[nImagePixel];
          
          for(unsigned int j=0; j < totalRoiSize; ++j)
          {
            float heightValueInMicron = pHeightMap[j];
            float heightValueInMilimeter = heightValueInMicron * 0.001f;
            temp[j] = heightValueInMilimeter;
          }
          
          // move from the temp structure to the java structure
          pEnv->SetFloatArrayRegion(resultArray, 0, totalRoiSize, temp);
          delete[] temp;
          return resultArray;
        }
        else
        {
          char msg[1024];
          jfloat temp[256] = {0.0};
          int heightMapSize = _pCameraInfo->roiCount;
          
          // Initialize int Height Map array
          resultArray = pEnv->NewFloatArray(heightMapSize);
            
          // Debug data for troubleshooting purpose
          if (_pCameraInfo->debugMode)
          {
            sprintf(msg, "Total Height Map size = %d", heightMapSize);
            WriteLog(msg);
          }
          for(unsigned int i=0; i < heightMapSize; ++i) 
          {
            Roi3DStruct roi = _pCameraInfo->inspectionRoiVector.at(i);
            int* pHeightMap = roi.pHeightMap;
            int totalRoiSize = roi.dX * roi.dY;
            float totalHeightValueInMilimeter = 0.0f;
            float avgHeightValue = 0.0f;
              
            // Debug data for troubleshooting purpose
            if (_pCameraInfo->debugMode)
            {
              sprintf(msg, "Total ROI size = %d", totalRoiSize);
              WriteLog(msg);
              sprintf(msg, "ROI X = %d", roi.x);
              WriteLog(msg);
              sprintf(msg, "ROI Y = %d", roi.y);
              WriteLog(msg);
              sprintf(msg, "ROI dX = %d", roi.dX);
              WriteLog(msg);
              sprintf(msg, "ROI dY = %d", roi.dY);
              WriteLog(msg);
            }
             
            for(unsigned int j=0; j < totalRoiSize; ++j)
            {
              float heightValueInMicron = pHeightMap[j];
              float heightValueInMilimeter = heightValueInMicron / 1000.0f;
              totalHeightValueInMilimeter += heightValueInMilimeter;
               
              if (_pCameraInfo->debugMode)
              {
              sprintf(msg, "ROI %d, j = %d, height value (um) = %f", i, j, heightValueInMicron);
              WriteLog(msg);
              }
            }
             
            if (totalRoiSize > 0)
            {
              avgHeightValue = totalHeightValueInMilimeter / totalRoiSize;
            }
            else
              avgHeightValue = 0.0f;
              
            // Debug data for troubleshooting purpose
            if (_pCameraInfo->debugMode)
            {
              sprintf(msg, "totalHeightValue (mm) = %f", totalHeightValueInMilimeter);
              WriteLog(msg);  
              sprintf(msg, "avgHeightValue (mm) = %f", avgHeightValue);
              WriteLog(msg);
            }
             
            // Finally assign the computed avg value into temporary array 
            temp[i] = avgHeightValue;
          }
          // move from the temp structure to the java structure
          pEnv->SetFloatArrayRegion(resultArray, 0, heightMapSize, temp);
          return resultArray;
        }
      }
    }
     
    // By default, return empty float array
    resultArray = pEnv->NewFloatArray(0);
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/*
 * Class:     com_axi_v810_hardware_SentechOpticalCamera_nativeEnableHardwareTrigger
 * Method:    nativeEnableHardwareTrigger
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeEnableHardwareTrigger(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    int result = 0;
    BOOL success = TRUE;  
    HANDLE	m_aCamera1;
    HANDLE	m_aCamera2;
    DWORD pdwCameraID = -1;
    TCHAR	cameraName[200];
	memset(cameraName, 0, sizeof(TCHAR) * 200);

    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);

    if(!_pCameraInfo->m_hCamera)
    {
       result = reInitializeOpticalCamera(cameraDeviceId);
       
       if (result != 0) 
         return result;
    }
 
    success = StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);   
    
    if(!success)
    {
   	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
    }
  
    StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);
    
    StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	
	// Setting up trigger timeout
	StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);

    if(!success)
    {
   	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
    }

    return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   } 
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeAcquireObjectFringeImages(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint planeId,  jstring fullPathFileName, jboolean forceTrigger)
{
  try
   {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    int result = 0;  
    const char *nativeFullPathFileName = NULL;
    BOOL success = TRUE;
    _isTriggered = FALSE;
     
    // Reset member variables before proceed
    if (_pCameraInfo->imageCount > 11)
    {
      _pCameraInfo->reset();
    }

    if (fullPathFileName != NULL)
	  nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);

    if (_pCameraInfo->imageCount == 0)
    {
	  StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);
      StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);      
      StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	   
	  // Setting up trigger timeout	  
	  StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);
      
	  // Setting callback function
      transferEndCallback = new funcTransferEndCallback(nativeAcquireObjectFringeImagesCallback); 
    }
 
    //Set Callback Functions	 
    success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, *transferEndCallback, (PVOID)nativeFullPathFileName) ;
    
    success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);	
	
	int waitForTriggerModeReadyTime = 0;
	while ((_isTriggered == FALSE) && (waitForTriggerModeReadyTime < _waitForTriggerModeReadyTimeout))
	{
	  Sleep(100);
	  waitForTriggerModeReadyTime += 100;
	}
	
	if (_isTriggered == FALSE)
	{
	  return -1;
	}
    
   	StTrg_StopTransfer(_pCameraInfo->m_hCamera);  		
    if(!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      return -1;
    }
    
    if(fullPathFileName != NULL)                                         	   
	  pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
 
	return result;	  
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/**
 * This is a new acquire calibration image function 
 * 
 * @author Cheah Lee Herng
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeAcquireCalibrationImages(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint planeId, jstring fullPathFileName, jboolean forceTrigger)
{
  try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
	 
	 // Reset trigger mode flag	
    _isTriggerModeReady = FALSE;
   
     int result = 0;  
     const char *nativeFullPathFileName = NULL;
     BOOL success = TRUE;
     _isTriggered = FALSE;
     
     // Reset member variables before proceed
     if (_pCameraInfo->imageCount > 15)
     {
       _pCameraInfo->reset();
     }
     
     // Set plane Id
     _pCameraInfo->planeId = planeId;

     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);

     if (_pCameraInfo->imageCount == 0)
     {
       // Setting up hardware trigger
       StTrg_SetTriggerMode(_pCameraInfo->m_hCamera, (DWORD)TRIGGERMODE);
       StTrg_SetIOPinMode(_pCameraInfo->m_hCamera, (BYTE)0, STCAM_IN_PIN_MODE_TRIGGER_INPUT);
       StTrg_SetIOPinPolarity(_pCameraInfo->m_hCamera, (DWORD)1);
	   
	   // Setting up trigger timeout
	   StTrg_SetTimeOut(_pCameraInfo->m_hCamera, STCAM_TIMEOUT_TE2EE, (DWORD)_triggerTimeoutInMS);
   
       transferEndCallback = new funcTransferEndCallback(nativeAcquireCalibrationImagesCallback); 
     }
     
     //Set Callback Functions	 
     success = StTrg_SetTransferEndCallback(_pCameraInfo->m_hCamera, *transferEndCallback, (PVOID)nativeFullPathFileName) ;
    
     success = StTrg_StartTransfer(_pCameraInfo->m_hCamera);
	 
	 // Set trigger mode flag to true
     _isTriggerModeReady = TRUE;
    
	 int waitForTriggerModeReadyTime = 0;
	 while ((_isTriggered == FALSE) && (waitForTriggerModeReadyTime < _waitForTriggerModeReadyTimeout))
	 {
	   Sleep(100);
	   waitForTriggerModeReadyTime += 100;
	 }
	 
	 if (_isTriggered == FALSE)
	 {
	   return -1;
	 }
	 
	 // Reset trigger mode flag
	 _isTriggerModeReady = FALSE;
    
   	 StTrg_StopTransfer(_pCameraInfo->m_hCamera);  		
     if(!success)
     {
       StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
     }
    
     if(fullPathFileName != NULL)                                         	   
	     pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
 
	   return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/*
 * Class:     com_axi_v810_hardware_SentechOpticalCamera_nativeGetMasterGainFactor()
 * Method:    nativeGetMasterGainFactor
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetMasterGainFactor(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    BOOL success = TRUE;
    PWORD  pwGain = 0;
    
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    success = StTrg_GetGain(_pCameraInfo->m_hCamera, pwGain);
    
    if(!success)
    {
   	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
    }
    
    return (int)pwGain;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_SentechOpticalCamera_nativeSetMasterGainFactor()
 * Method:    nativeSetMasterGainFactor
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetMasterGainFactor(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jint gainFactor)
{
  try
  {
    BOOL success = TRUE;
    WORD  wGain = (WORD)gainFactor;
    
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    success = StTrg_SetGain(_pCameraInfo->m_hCamera, wGain); 
    
    if(!success)
    {
   	   StTrg_Close(_pCameraInfo->m_hCamera);
       return -1;
    }

    return 0;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * 
 * 
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetDebugMode(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jboolean status)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;

    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }    
}

/**
 * 
 * 
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeIsTriggerModeReady(JNIEnv* pEnv, jclass object)
{
  try
  {
    return _isTriggerModeReady;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }    
} 

/*
 * Class:     com_axi_v810_hardware_SentechOpticalCamera_nativeSetMagnificationData
 * Method:    nativeSetMagnificationData
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetMagnificationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jdoubleArray magnificationData)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    boolean isSuccess = false;
    jboolean isCopy;
    
    // Get pointer to double array
    jdouble *magnificationDataArray = pEnv->GetDoubleArrayElements(magnificationData, &isCopy);
    
    // Get the size of each input array
    jsize magnificationDataArrayLength = pEnv->GetArrayLength(magnificationData);
    
    // Populate magnification data array
    for(unsigned int i=0; i < magnificationDataArrayLength; ++i)
        _pCameraInfo->magnificationData[i] = magnificationDataArray[i];
        
    isSuccess = true;
    
    // Release pointer
    pEnv->ReleaseDoubleArrayElements(magnificationData, magnificationDataArray, 0);
    
    if (isSuccess)
      return 0;
    else
      return -1;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGenerateOfflineHeightMap(JNIEnv* pEnv, 
                                                                            jclass object, 
                                                                            jint cameraDeviceId,                                                          
                                                                            jfloat adjustment, 
                                                                            jint minusPlusSet,
                                                                            jintArray jImageDef,
                                                                            jint roiMinX, 
                                                                            jint roiMinY, 
                                                                            jint roiWidth, 
                                                                            jint roiHeight)
{
  try
  {  
    BOOL success = TRUE;
    jfloatArray heightMapArray;
  
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    // Reset member variables before proceed
    if (_pCameraInfo->imageCount > 3)
    {
      _pCameraInfo->reset();
    }
  
    JNIImageData *pImage = getImageDataFromJava(pEnv, jImageDef); 
    
    // the result array is the size of the image.
    jfloatArray resultArray = pEnv->NewFloatArray(roiWidth * roiHeight);
    Ipp32f *pResult = reinterpret_cast<Ipp32f*>(pEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiCopy_32f_C1R(pImage->addressAt(roiMinX, roiMinY), 
                                           pImage->stride,
                                           pResult, 
                                           roiWidth * sizeof(Ipp32f), 
                                           resultSize);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    pEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(pEnv, jImageDef, pImage);
                                                
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    boolean isCopy = false;
    jfloat *imageArray = pEnv->GetFloatArrayElements(resultArray, &isCopy);
        
    if(_pCameraInfo->imageCount == 0) 
    {
      _pCameraInfo->pObjImg1 = (unsigned char*)imageArray; 
    }
    else if(_pCameraInfo->imageCount == 1) 
    {
      _pCameraInfo->pObjImg2 = (unsigned char*)imageArray;
    }
    else if(_pCameraInfo->imageCount == 2) 
    {
      _pCameraInfo->pObjImg3 = (unsigned char*)imageArray;
    }
    else if(_pCameraInfo->imageCount == 3) 
    {
      _pCameraInfo->pObjImg4 = (unsigned char*)imageArray;
    }
    
    if (_pCameraInfo->imageCount == 3)
    {                                                             
      if (success == false)
      {
        // Release any left over memory
        pEnv->ReleaseFloatArrayElements(resultArray, imageArray, 0);
                 
        heightMapArray = pEnv->NewFloatArray(0);
        return heightMapArray;
      }
    }
    
    _pCameraInfo->imageCount++;

    // Release any left over memory
    pEnv->ReleaseFloatArrayElements(resultArray, imageArray, 0);
    
    if(_pCameraInfo->pHeightMap != NULL && success)
    {    
     jfloat temp[256] = {0.0};
     int heightMapSize = _pCameraInfo->roiCount;
     
     // Initialize int Height Map array
     heightMapArray = pEnv->NewFloatArray(heightMapSize);
    
     for(unsigned int i=0; i < heightMapSize; ++i) 
        temp[i] = _pCameraInfo->pHeightMap[i];
     
     // move from the temp structure to the java structure
     pEnv->SetFloatArrayRegion(heightMapArray, 0, heightMapSize, temp);
     return heightMapArray;  
    }
    else
    {
     heightMapArray = pEnv->NewFloatArray(0);
     return heightMapArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetCropImageOffset(JNIEnv* pEnv, 
                                                                         jclass object, 
																		 jint cameraDeviceId, 
																		 jint frontModuleOffsetXInPixel, 
																		 jint rearModuleOffsetXInPixel)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
	
	_pCameraInfo->frontModuleCropImageOffsetXInPixel = frontModuleOffsetXInPixel;
	_pCameraInfo->rearModuleCropImageOffsetXInPixel  = rearModuleOffsetXInPixel;
	
	return true;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
	return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
	return false;
  }
}

/**
 * @author Ying-Huan.Chu
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_SentechOpticalCamera_nativeSetImageSizeInPixel(JNIEnv* pEnv, 
                                                                          jclass object,
																		  jint imageWidthInPixel, 
																		  jint imageHeightInPixel)
{
  try
  {
    _nMaxWidth = imageWidthInPixel;
	_nMaxHeight = imageHeightInPixel;
	
	return true;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
	return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
	return false;
  }
}

/**
 * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
 * Sentech camera provides USBProductID which is the camera model/serial number in Hex, use this to convert to string
 * Need to add more model according to the SDK whenever a newer SDK version is used
 */
static string getCameraModelString(WORD pwUSBProductID)
{
  string cameraSerialNumberString = "";
  
  switch (pwUSBProductID)
  {
    case (STCAM_USBPID_STC_B33USB):
      cameraSerialNumberString = "STC-B33USB";
      break;
    case (STCAM_USBPID_STC_C33USB):
      cameraSerialNumberString = "STC-C33USB";
      break;
    case (STCAM_USBPID_STC_B83USB):
      cameraSerialNumberString = "STC-B83USB";
      break;
    case (STCAM_USBPID_STC_C83USB):
      cameraSerialNumberString = "STC-C83USB";
      break;
    case (STCAM_USBPID_STC_TB33USB):
      cameraSerialNumberString = "STC-TB33USB";
      break;
    case (STCAM_USBPID_STC_TC33USB):
      cameraSerialNumberString = "STC-TC33USB";
      break;
    case (STCAM_USBPID_STC_TB83USB):
      cameraSerialNumberString = "STC-TB83USB";
      break;
    case (STCAM_USBPID_STC_TC83USB):
      cameraSerialNumberString = "STC-TC83USB";
      break;
    case (STCAM_USBPID_STC_TB133USB):
      cameraSerialNumberString = "STC-TB133USB";
      break;
    case (STCAM_USBPID_STC_TC133USB):
      cameraSerialNumberString = "STC-TC133USB";
      break;
    case (STCAM_USBPID_STC_TB152USB):
      cameraSerialNumberString = "STC-TB152USB";
      break;
    case (STCAM_USBPID_STC_TC152USB):
      cameraSerialNumberString = "STC-TC152USB";
      break;
    case (STCAM_USBPID_STC_TB202USB):
      cameraSerialNumberString = "STC-TB202USB";
      break;
    case (STCAM_USBPID_STC_TC202USB):
      cameraSerialNumberString = "STC-TC202USB";
      break;
    case (STCAM_USBPID_STC_MB33USB):
      cameraSerialNumberString = "STC-MB33USB";
      break;
    case (STCAM_USBPID_STC_MC33USB):
      cameraSerialNumberString = "STC-MC33USB";
      break;
    case (STCAM_USBPID_STC_MB83USB):
      cameraSerialNumberString = "STC-MB83USB";
      break;
    case (STCAM_USBPID_STC_MC83USB):
      cameraSerialNumberString = "STC-MC83USB";
      break;
    case (STCAM_USBPID_STC_MB133USB):
      cameraSerialNumberString = "STC-MB133USB";
      break;
    case (STCAM_USBPID_STC_MC133USB):
      cameraSerialNumberString = "STC-MC133USB";
      break;
    case (STCAM_USBPID_STC_MB152USB):
      cameraSerialNumberString = "STC-MB152USB";
      break;
    case (STCAM_USBPID_STC_MC152USB):
      cameraSerialNumberString = "STC-MC152USB";
      break;
    case (STCAM_USBPID_STC_MB202USB):
      cameraSerialNumberString = "STC-MB202USB";
      break;
    case (STCAM_USBPID_STC_MC202USB):
      cameraSerialNumberString = "STC-MC202USB";
      break;
    default:
      cameraSerialNumberString = "N/A";
      break;
  }
  
  return cameraSerialNumberString;
}

/**
 * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetFirmwareVersion(JNIEnv *pEnv, 
                                                                                                jclass object, 
                                                                                                jint cameraDeviceId)
{
  try
  {
    BOOL success = TRUE;
    WORD pwFirmVersion = 0;
    WORD pwUSBVendorID = 0;
    WORD pwUSBProductID = 0;
    WORD pwFPGAVersion = 0;

    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    success = StTrg_GetCameraVersion(_pCameraInfo->m_hCamera, &pwUSBVendorID, &pwUSBProductID, &pwFPGAVersion, &pwFirmVersion);
    
    if (!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      return -1;
    }
    
    return (int)pwFirmVersion;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetFpgaVersion(JNIEnv *pEnv, 
                                                                                            jclass object, 
                                                                                            jint cameraDeviceId)
{
  try
  {
    BOOL success = TRUE;
    WORD pwFirmVersion = 0; //Camera firmware version
    WORD pwUSBVendorID = 0; //USB Vendor ID(manufacturer) - 0X1421 for Sentech 
    WORD pwUSBProductID = 0; //USB Product ID (camera model/serial number)
    WORD pwFPGAVersion = 0; //Camera FPGA version

    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    success = StTrg_GetCameraVersion(_pCameraInfo->m_hCamera, &pwUSBVendorID, &pwUSBProductID, &pwFPGAVersion, &pwFirmVersion);
    
    if (!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      return -1;
    }
    
    return (int)pwFPGAVersion;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/**
 * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
 */
JNIEXPORT jstring JNICALL Java_com_axi_v810_hardware_SentechOpticalCamera_nativeGetModel(JNIEnv *pEnv, 
                                                                                         jclass object, 
                                                                                         jint cameraDeviceId)
{
  try
  {
    BOOL success = TRUE;
    WORD pwFirmVersion = 0;
    WORD pwUSBVendorID = 0;
    WORD pwUSBProductID = 0;
    WORD pwFPGAVersion = 0;

    string cameraModelString;
    
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    success = StTrg_GetCameraVersion(_pCameraInfo->m_hCamera, &pwUSBVendorID, &pwUSBProductID, &pwFPGAVersion, &pwFirmVersion);
    
    if (!success)
    {
      StTrg_Close(_pCameraInfo->m_hCamera);
      cameraModelString = "N/A";
    }
    else
    {
      cameraModelString = getCameraModelString(pwUSBProductID);   
    }
    
    return pEnv->NewStringUTF (cameraModelString.c_str());
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}
