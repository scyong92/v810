package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraEventEnum extends HardwareEventEnum
{
    private static int _index = -1;

    public static OpticalCameraEventEnum INITIALIZE = new OpticalCameraEventEnum(++_index, "Optical Camera, Initialize");    

    private String _name;

    /**
    * @author Cheah Lee Herng
    */
    private OpticalCameraEventEnum(int id, String name)
    {
        super(id);
        Assert.expect(name != null);

        _name = name;
    }

    /**
    * @author Cheah Lee Herng
    */
    public String toString()
    {
        return _name;
    }
}
