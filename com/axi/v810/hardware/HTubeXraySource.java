package com.axi.v810.hardware;

import java.util.regex.*;

import com.axi.util.*;
//import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class will provide all xray source functionality including things like turning
 * x-rays on and off.  The Hamamatsu L9181-18 X-ray Source (HTube) has the power supply
 * and control electronics built in so there will no longer be a separate XrayTube and
 * XrayPowerSupply class. </p>
 *
 * This gross representation of the tube itself will hopefully help make sense out the
 * values that can be read and set.<br/>
 * <pre>
 *                   The Hamamatsu X-ray Source
 *         ____________________________________________
 *        |                                            |
 *        |                  X-ray Tube    Control     |
 *        |  High              ,___,       Electronics |
 *        |  Voltage        __ |   |__     and         |
 *        |  Power         /   |   |  \    RS-232      |
 *        |  Supply       /    |   |   \   Interface   |
 *        |              |     |   |    |              |
 *        |_____________ |     |   |    |______________|
 *                       |     |   |    |
 *                       |     |   |<---------------- anode
 *                       |     |  /     |________    130 kV
 *                       |     | /      |        |
 *                       |     |/. . . . . . .]] |  cathode
 *                       |       .      |________|   300 uA
 *                       |       .      |
 *                       |=======.======| <------ Beryllium
 *                               .                   window
 * </pre>                      da Rays
 * <br/>
 *
 * <b>Description:</b> The Hamamatsu x-ray source is a reflective type source rather than a typical
 *   transmissive type tube with focus coils.  The cathode generateds electrons which are
 *   attracted to the anode.  The anode tip is coated with tungsten which generates x-rays
 *   when hit by the electron beam. The angle of the anode tip causes the x-rays be be
 *   directed outward. </p>
 *
 * <b>cathode:</b> This is the free electron source for the tube. The filament is part of the
 *   cathode.  The cathode current can be programmed over a range of 0 uA to 300 uA.<p/>
 *
 * <b>anode:</b> A rod in the center of the tube with an angled tungsten tip.  The anode is lomg and
 *  skinny to dissipate heat. The anode voltage can be programmed over a range of 0 to 160 kV.<p/>
 *
 *
 * HARDWARE REPORTED ERRORS: The controller reports on a myriad of error conditions
 *   that can occur on the hardware (see getHardwareReportedErrors()). Methods that
 *   attempt to change the state of the hardware use EROR responses from the
 *   getOperationalStatus() and getSelfTestStatus() methods to queue up calls to
 *   getHardwareReportedErrors().  This approach should probably be verified.<p/>
 *
 * NOTE ON abort(): Methods that spin block (i.e., sleep) check the abort flag.
 *   The only methods that pay attention to the abort flag are on() and performSelfTest().
 *   Please see those methods for more information.<p/>
 *
 * THREAD SAFETY: This class is re-entrant (synchronizes access to USB link).<p/>
 *
 * TEST NOTE: From a unit-test / code-coverage perspective, almost the entire class
 *   is testable without any hardware, because the simulation check happens just
 *   before interacting with the RS-232 port.  I.e., the code can be tested against
 *   sunny day responses from a simulated USB link.
 *
 * @author Bill Darbie
 * @author George A. David
 * @author Greg Loring
 * @author George Loring Booth
 */

public class HTubeXraySource extends AbstractXraySource implements HardwareStartupShutdownInt, Simulatable
{
  private boolean _debug = false; // false;
  private static String _me = "HTubeXraySource";
  // provide feedback to the user
  private static ProgressObservable _progressObservable = ProgressObservable.getInstance();

  // see javadoc on simulateTiming()
  private boolean _isThisObjectInSimMode = false;
  private static boolean _simulateTiming = false;

  // object which handles the low-level commanding protocol
  private HTubeXraySourceCommandingLink _commandingLink;

  // "command response"
  private Pattern _commandResponsePattern = Pattern.compile("^(\\w+)\\s+(.+)");
  private Pattern _commandTwoResponsePattern = Pattern.compile("^(\\w+)\\s+(.+)\\s+(.+)");
  private Pattern _commandNineResponsePattern = Pattern.compile("^(\\w+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)\\s+(.+)");

  // measurement state
//  private StatisticalTimer _onCallTimer = new StatisticalTimer("total");
//  private StatisticalTimer _onKVOnTimer = new StatisticalTimer("kV-on");
//  private StatisticalTimer _onUAOnTimer = new StatisticalTimer("uA-on");
//  private StatisticalTimer _turnBackOnCallTimer = new StatisticalTimer("turnBackOn");
//  private StatisticalTimer _offCallTimer = new StatisticalTimer("off()");
//  private StatisticalTimer _powerOffCallTimer = new StatisticalTimer("powerOff");
//  private StatisticalTimer _powerOffUAOffTimer = new StatisticalTimer("uA-off");
//  private StatisticalTimer _powerOffKVOffTimer = new StatisticalTimer("kV-off");
//  private StatisticalTimer _areXraysOnStatisticalTimer = new StatisticalTimer("areXraysOn-ck");
  private StatisticalTimer _htubeXrayOnStatisticalTimer = new StatisticalTimer("on()");
  private StatisticalTimer _htubeSelfTestStatisticalTimer = new StatisticalTimer("self-test");

  // optimization state
  private int _lastVoltageProgrammed = 0;
  private int _lastCurrentProgrammed = 0;
//  private int _lastFocalSpotModeProgrammed = 0;

  // x-ray test state
  private int _highVoltageProgramed = 130;
  private int _highCurrentProgramed = 300;
  private int _lowVoltageProgramed = 130;
  private int _lowCurrentProgramed = 150;

  private int _openOuterBarrierVoltage = 100;
  // htube xray source does not use the inner barrier
  private InnerBarrier _innerBarrier;

//  private HardwareTaskEngine _hardwareTaskEngine;

  private static boolean _SERVICE_MODE_ON = true;
  private static boolean _SERVICE_MODE_OFF = false;
  private boolean _safetyTestMode = false;
  private String _selfTestResultsString = "X-ray Source self-test has not been run";

  // current state of the x-ray source
  private HTubeXraySourceStatusEnum _operationalStatus = HTubeXraySourceStatusEnum.NOT_READY;
  private HTubeXraySourceStatusEnum _htubeSelfTestStatus = HTubeXraySourceStatusEnum.SELF_TEST_NOT_RUN;

  // thread used to make Renderers flash
  private WorkerThread _keepAliveWorkerThread = new WorkerThread("HTube X-ray source keep alive thread");
  private boolean _enableKeepAlive = false;
  private final Object _keepAliveLock = new Object();

  private FileLoggerAxi _hTubelogger = null;
  private int _maxLogFileSizeInBytes =
      _config.getIntValue(SoftwareConfigEnum.XRAY_SOURCE_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);

    //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  private boolean  _xrayAutoShortTermShutdownTimerEnabled;
  private double _xrayAutoShortTermShutdownTimeoutMinutes;

  public static final int SLOW_WARMUP_MODE = 2;
  public static final int FAST_WARMUP_MODE = 0;
  
  private int _currentFocalSpotMode = -1;
  private double _currentAnodeVoltage = -0.1;
  private double _currentCathodeCurrent = -0.1;
  
  // XCR-3728 Send keep-alive command to x-ray tube during system start-up
  private boolean _isKeepAliveModeSet = false;
  
  /**
   * @author Greg Loring
   * @author George Booth
   */
  HTubeXraySource()
  {
    //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
    _xrayAutoShortTermShutdownTimerEnabled = _config.getBooleanValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED);
    _xrayAutoShortTermShutdownTimeoutMinutes = _config.getDoubleValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES);

//    debug("HTubeXraySource(s) constructor");
    // guarantee that all of the objects on which i depend are in place
    _innerBarrier = InnerBarrier.getInstance();
//    _hardwareTaskEngine = HardwareTaskEngine.getInstance();

    // the _commandingLink object handles the low-level communication protocol
    _commandingLink = new HTubeXraySourceCommandingLink(getSerialPortName());

    // for debugging and off-line
    if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
    {
      setSimulationMode();
      _commandingLink.setSimulationMode();
//      simulateTiming();
    }
    
    try
    {
      setupHubeLog();
    }
    catch (DatastoreException ex)
    {
      System.out.print("DatastoreException :" + ex.getLocalizedMessage());
    }
  }

  /**
   * Simulatable implementation
   *
   * @author George Booth
   */
  @Override
  public void setSimulationMode()
  {
    _isThisObjectInSimMode = true;
  }

  /**
   * Simulatable implementation
   *
   * @author George Booth
   */
  @Override
  public void clearSimulationMode()
  {
    _isThisObjectInSimMode = false;
  }

  /**
   * Simulatable implementation
   *
   * sim mode may be on for this object because it was turned on explicitly via
   * setSimulationMode() or because there is some indication in the overall
   * system configuration that all hardware related objects should be operating
   * in sim mode.  In either case, this object will not interact with hardware.
   *
   * @author George Booth
   */
  @Override
  public boolean isSimulationModeOn()
  {
    return _isThisObjectInSimMode || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
  }

  /**
   * the standard sim mode just blazes through without any sleep calls; call this method
   *  to get a realistic feel for how long things will actually take on the hardware
   * @author Greg Loring
   * @author George Booth
   */
  public void simulateTiming()
  {
    _simulateTiming = true;
//    debug("simulateTiming()");
    HTubeXraySourceCommandingLink.setSimulateLinkTiming();
  }

  /**
   * there is no convienient set of hardware measures that is useful for implementing this
   *  method across all of the rapidly evolving hardware configurations. so, just use this
   *  to ask that startup() get called at least once.
   *
   * @return true if startup() has been called more recently than shutdown(), false otherwise
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#isStartupRequired()
   *
   * @author Greg Loring
   * @author Rex Shang
   * @author George Booth
   */
  @Override
  public boolean isStartupRequired() throws XrayTesterException
  {
//    debug("isStartupRequired()");

    // make sure comm link is established
    _commandingLink.configureSerialPortOnce();
    return _startupRequired;
  }

  /**
   * prepare the X-ray source hardware for imminent use.<p>
   *
   *   WARNING: this command will cause the X-ray source to begin emmitting X-rays<p>
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#startup()
   *
   * @author Greg Loring
   */
  @Override
  public void startup() throws XrayTesterException
  {
//    debug("startup()");
    _commandingLink.checkHTubeModelResult("TYP");
    
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.STARTUP);
    _hardwareObservable.setEnabled(false);
    try
    {
      // this overrides AbstractXraySource to avoid the crudeness; see AbstractXraySource.startup()

      // Clear the latched hardware error code before continuing.
      resetHardwareReportedErrors();

       // lock front door when x-ray start up
      _digitalIo.turnFrontPanelLeftDoorLockOn();
      _digitalIo.turnFrontPanelRightDoorLockOn();

      // simulate warmup
      _commandingLink.setSimulatedResponseForSTS("STS 0");

      // do whatever is needed to get the tube in the STANDBY state
      //ProgressReporterEnum progressReporterEnum = makeHTubeReadyToEmit();
      if (isHTubeIsReadyToEmit() == false)
      {
        makeHTubeReadyToEmit();
      }
      
      // XCR-3728 Send keep-alive command to x-ray tube during system start-up
      if (_isKeepAliveModeSet == false)
      {
        if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
        {
          _hTubelogger.append("   Start-up, Set AST command");
        }
        
        try
        {
          // Set keepalive timeout value here
          _commandingLink.sendAndReceive("AST " + Integer.toString(getKeepAliveTimeoutInSeconds()));
          _isKeepAliveModeSet = true;
        }
        catch(XrayTesterException xtx)
        {
          // Encounter error during AST command set. Ignore it
          // until we try again in next run.
          _isKeepAliveModeSet = false;
          
          if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
          {
            _hTubelogger.append("   Start-up, FAIL to set AST command");
          }
        }
      }
      
      // XCR-3696 Additional X-ray Tube Status Log
      if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        String satResult = _commandingLink.sendAndReceive("SAT");
        _hTubelogger.append("   Start-up, SAT = " + satResult);
      }

      // make sure xrays or off
//      _progressObservable.reportProgress(progressReporterEnum, 99);
      off();

      // make sure voltage, current and focal spot size are set up for imaging
//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 95);
      setupImagingXrayParameters();

      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.STANDBY))
      {
        // htube source finishes warmup with x-rays off
        _commandingLink.setSimulatedResponseForSHV("SHV 0");
        _commandingLink.setSimulatedResponseForSCU("SCU 0");
        _commandingLink.setSimulatedResponseForSFC("SFC 2");
        _startupRequired = false;
      }
      else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
      {
          _startupRequired = false;
      }
      else
      {
        _startupRequired = true;
      }
    }
    catch (XrayTesterException xtx)
    {
      _startupRequired = true; // reset when call is not successfull
      throw xtx;
    }
    catch (RuntimeException rx)
    {
      _startupRequired = true; // reset when call is not successfull
      throw rx;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_CLOSE, 100);

    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.STARTUP);
  }

  /**
   * @author George Booth
   */
  public void setupImagingXrayParameters() throws XrayTesterException
  {
//    debug("setupImagingXrayParameters()");

    setFocalSpotMode(getImagingFocalSpotMode());

    setAnodeVoltageInKiloVolts(getImagingVoltage());

    setCathodeCurrentInMicroAmps(getImagingCurrent());
  }

  // Convenience methods for getting config and parameterized values:
  // Many of these are being hard coded for now.  However, the getters are here for when
  // they are added to hardware.config in the future.
  // Another nice thing about these is that JBuilder shows you which values have not been used.

  /**
   * @author George Booth
   */
  private String getSerialPortName() // typically COMn
  {
//    debug("getSerialPortName()");
    String result = _config.getStringValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_SERIAL_PORT_NAME);
    return result;
  }

  /**
   * @author George Booth
   */
  public int getImagingVoltage() // typically 130 kV
  {
//    debug("getImagingVoltage()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_IMAGING_VOLTAGE_KILOVOLTS);
    return result;
  }

  /**
   * @author George Booth
   */
  public int getImagingCurrent() // typically 300 uA
  {
//    debug("getImagingCurrent()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_IMAGING_CURRENT_MICROAMPS);
    return result;
  }

  /**
   * @author George Booth
   */
  public int getImagingFocalSpotMode() // large = 2
  {
//    debug("getImagingFocalSpotMode()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_IMAGING_FOCAL_SPOT_MODE);
    return result;
  }
  
  /**
   * @author Siew Yeng
   */
  public long getXraySourceTurnOnDelayTimeInMiliSeconds()
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_TURN_ON_DELAY_SECONDS);
    return (long)(result * 1000);
  }
  
  /**
   * @author Siew Yeng
   */
  public long getXraySourceTurnOffDelayTimeInMiliSeconds()
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_TURN_OFF_DELAY_SECONDS);
    return (long)(result * 1000);
  }

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  public int getHighMagImagingVoltage() // typically 130 kV
  {
//    debug("getHighMagImagingVoltage()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_VOLTAGE_KILOVOLTS);
    return result;
  }

  /**
   * @author  Anthony Fong
   */
  public int getHighMagImagingCurrent() // typically 300 uA
  {
//    debug("getHighMagImagingCurrent()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_CURRENT_MICROAMPS);
    return result;
  }

  /**
   * @author  Anthony Fong
   */
  public int getHighMagImagingFocalSpotMode() // large = 2
  {
//    debug("getHighMagImagingFocalSpotMode()");
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_HIGH_MAG_IMAGING_FOCAL_SPOT_MODE);
    return result;
  }

    /**
   * @author Anthony Fong
   */
  public void setupHighMagImagingXrayParameters() throws XrayTesterException
  {
//    debug("setupImagingXrayParameters()");
    setFocalSpotMode(getHighMagImagingFocalSpotMode());

    setAnodeVoltageInKiloVolts(getHighMagImagingVoltage());

    setCathodeCurrentInMicroAmps(getHighMagImagingCurrent());
  }

  /**
   * @author Anthony Fong
   */
  public void setupLowMagImagingXrayParameters() throws XrayTesterException
  {
//    debug("setupImagingXrayParameters()");
    setFocalSpotMode(getImagingFocalSpotMode());

    setAnodeVoltageInKiloVolts(getImagingVoltage());

    setCathodeCurrentInMicroAmps(getImagingCurrent());
  }
  
  /**
   * @author Wei Chin
   */
  public void setupOpenOuterBarrierXrayParameters() throws XrayTesterException
  {
//    debug("setupImagingXrayParameters()");

    setFocalSpotMode(getImagingFocalSpotMode());

    setAnodeVoltageInKiloVolts(_openOuterBarrierVoltage);

    setCathodeCurrentInMicroAmps(getImagingCurrent());
  }

  /**
   * @author George Booth
   */
  private static double getTurnOffAnodeKiloVolts()
  {
    return 0.0;
  }

  /**
   * @author George Booth
   */
  private static double getMaxAnodeOffKiloVolts()
  {
    /** @todo GLB get max anode off volts */
    return 5.0; // kV
  }

  /**
   * @author Lim, Seng Yew
   */
  public int getKeepAliveTimeoutInSeconds()
  {
    int result = _config.getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_KEEPALIVE_TIMEOUT_SECONDS);
    return result;
  }

  /**
   * Is the X-ray source hardware producing X-rays?
   * @author Rex Shang
   * @author George Booth
   */
  public boolean areXraysOn() throws XrayTesterException
  {
    return areXraysOnInternal(_SERVICE_MODE_OFF);
  }

  /**
   * Is the X-ray source hardware producing X-rays?
   * @author Rex Shang
   * @author George Booth
   */
  public boolean areXraysOnForServiceMode() throws XrayTesterException
  {
    return areXraysOnInternal(_SERVICE_MODE_ON);
  }

  /**
   * Is the X-ray source hardware producing X-rays suitable for imaging?<p/>
   *
   * This method returns false if the X-ray source is operating in either the
   *  low-power or high-power survey/safety-test modes.
   *
   * @author George Booth
   */
  public boolean areXraysOnInternal(boolean serviceMode) throws XrayTesterException
  {
//    debug("areXraysOn()");

    String status = _commandingLink.sendAndReceive("STS");
    checkForErrors();

    if (status.equals("STS 3"))
    {
      // tube is emitting
      if (_safetyTestMode)
      {
        // not suitable for imaging
//        System.out.println("    X-rays are on but safety test mode is active");
        return false;
      }
      else
      {
          if (InnerBarrier.isInstalled())
          {
              if (_innerBarrier.isOpen() == false)
                  return false;
              else
                  return true;
          }
          else
              return true;
        // suitable for imaging
//        System.out.println("    X-rays are ON");
      }
    }
    else
    {
//      System.out.println("    X-rays are OFF");
      return false;
    }
  }

  /**
   * Could the X-ray source hardware be producing X-rays from a health/safety perspective?<p>
   *
   * Please note that it is normal and expected for an X-ray source to be producing X-rays,
   * but still not be ready for imaging to take place.<p>
   *
   * This method has not been pushed into the base class for fear it would cause clutter.
   * However, it is very useful for testing purposes and because it clarifies and encodes
   * vital knowledge.
   *
   * @author Greg Loring
   * @author George Booth
   */
  public boolean couldUnsafeXraysBeEmitted() throws XrayTesterException
  {
    // if the anode voltage is high enough, then the leakage current will cause some
    //  amount of electrons to be liberated and cause events (X-rays)
    boolean result = (getAnodeVoltageInKiloVolts() > getMaxAnodeOffKiloVolts());
    return result;
  }

  /**
   * Sets the voltage, current ans spot size for Service Mode
   *
   * @author George Booth
   */
  public void setXraySourceParametersForServiceMode(double voltage, double current, int focalSpotMode) throws XrayTesterException
  {
     setAnodeVoltageInKiloVolts(voltage);
     setCathodeCurrentInMicroAmps(current);
     setFocalSpotMode(focalSpotMode);
  }
  
  /*
   * Chin-Seong, Kee - Set Virtual Live XrayTube Voltage And Current
   */
  public void setXraySourceParametersForServiceMode(double voltage, double current) throws XrayTesterException
  {
     setAnodeVoltageInKiloVolts(voltage);
     setCathodeCurrentInMicroAmps(current);
  }
  
  /**
   * Get the X-ray source hardware ready for imaging, i.e., start emitting imaging quality X-rays.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author George Booth
   */
  public void on() throws XrayTesterException
  {
//    System.out.println("on()");
    _htubeXrayOnStatisticalTimer.start();
    Assert.expect(super.isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ON);
    _hardwareObservable.setEnabled(false);
    try
    {
//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 50);
      turnOnImagingXRays();
    }
    finally
    {
      _htubeXrayOnStatisticalTimer.stop();
//      debug(_htubeXrayOnStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    // 100% done
    _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_OPEN, 100);

    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ON);
  }

   //Variable Mag Anthony August 2011
   /**
   * Get the X-ray source hardware ready for imaging regardless of Inner Barrier open/close status  i.e., start emitting imaging quality X-rays.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author George Booth
   */
  public void turnOnImagingXRaysForHighMag() throws XrayTesterException
  {
//    debug("turnOnImagingXRays()");

    boolean needToWaitXrayOn = false;
    if (InnerBarrier.isInstalled())
    {
        // Do not call _innerBarrier.open() this will cause infinite looping
        //if (_innerBarrier.isOpen() == false)
        //{
        //_innerBarrier.open();
        //}
      _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_OPEN, 90);
    }
    else
    {
      needToWaitXrayOn = true;
    }

    if (isHTubeIsReadyToEmit() == false)
    {
      makeHTubeReadyToEmit();
    }

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
        return;

    if (isHTubeIsReadyToEmit())
    {
      // turn off xrays to reprogram
//      turnOffXRays(_SERVICE_MODE_OFF);

      String onCommand = "XON";
      String onStatus = _commandingLink.sendAndReceive(onCommand);

      trackPowerOnProgress();

      setSurveyModeOn(false);

      // enable x-ray keep alive loop
      _commandingLink.setSimulatedResponseForSTS("STS 3");
      setKeepAlive(true);

      updateHTubeStatus();
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 80);
      if(needToWaitXrayOn)
        waitForHardwareInMilliSeconds(20000);
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 90);
    }
  }

  /**
   * Get the X-ray source hardware ready for imaging, i.e., start emitting imaging quality X-rays.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author George Booth
   */
  public void turnOnImagingXRays() throws XrayTesterException
  {
//    debug("turnOnImagingXRays()");

    boolean needToWaitXrayOn = false;
    if (InnerBarrier.isInstalled())
    {
      if (_innerBarrier.isOpen() == false)
      {
        _innerBarrier.open();
      }
      _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_OPEN, 90);
    }
    else
    {
      needToWaitXrayOn = true;
    }

    if (isHTubeIsReadyToEmit() == false)
    {
      makeHTubeReadyToEmit();
    }

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
        return;

    if (isHTubeIsReadyToEmit())
    {
      // turn off xrays to reprogram
//      turnOffXRays(_SERVICE_MODE_OFF);

      String onCommand = "XON";
      String onStatus = _commandingLink.sendAndReceive(onCommand);

      trackPowerOnProgress();

      setSurveyModeOn(false);

      // enable x-ray keep alive loop
      _commandingLink.setSimulatedResponseForSTS("STS 3");
      setKeepAlive(true);

      updateHTubeStatus();
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 80);
      if(needToWaitXrayOn)
      {
        //waitForHardwareInMilliSeconds(20000);
        //Siew Yeng - add delay time when inner barrier is not installed
        TimerUtil xrayTurnOnDelayTimer = new TimerUtil();
        xrayTurnOnDelayTimer.start();
        
        waitForHardwareInMilliSeconds(getXraySourceTurnOnDelayTimeInMiliSeconds());
        
        xrayTurnOnDelayTimer.stop();
        debug("XrayTurnOnDelayTime " + xrayTurnOnDelayTimer.getElapsedTimeInMillis() + "(ms)");
      }
       
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 90);
    }
  }

  /**
   * @author George Booth
   */
  private void trackPowerOnProgress() throws XrayTesterException
  {
//    System.out.println("trackPowerOnProgress()");
    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 0);

    int targetVoltage = _lastVoltageProgrammed;
    int targetCurrent = _lastCurrentProgrammed;
    int voltageStep = targetVoltage / 3;
    int currentStep = targetCurrent / 3;

//    Pair<Integer, Integer> warmupState = getWarmupState();
    Pair<Integer, Integer> power = getVoltageAndCurrent();
    Integer voltage = power.getFirst();
    Integer current = power.getSecond();
//    System.out.println("  powerUp: V = " + voltage + ", I = " + current);
    while (voltage < targetVoltage && current < targetCurrent)
    {
      sleep(200);
      if(isAborting())
        return;
      
      checkForErrors();
      
      if (isSimulationModeOn())
      {
        if (voltage < voltageStep)
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 30);
          _commandingLink.setSimulatedResponseForSHV("SHV " + voltageStep);
          _commandingLink.setSimulatedResponseForSCU("SCU " + currentStep);
        }
        else if (voltage < 2 * voltageStep)
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 60);
          _commandingLink.setSimulatedResponseForSHV("SHV " + 2 * voltageStep);
          _commandingLink.setSimulatedResponseForSCU("SCU " + 2 * currentStep);
        }
        else
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 90);
          _commandingLink.setSimulatedResponseForSHV("SHV " + targetVoltage);
          _commandingLink.setSimulatedResponseForSCU("SCU " + targetCurrent);
          _commandingLink.setSimulatedResponseForSTS("STS 2");
        }
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerUp: V = " + voltage + ", I = " + current);
      }
      else
      {
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerUp: V = " + voltage + ", I = " + current);
        int percentDone = 100 * (int)(((float)current / targetCurrent));
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, Math.min(99, percentDone));
      }
    }
    _commandingLink.setSimulatedResponseForSTS("STS 3");
    checkForErrors();
  }


  /**
   * @author George Booth
   */
  private void trackPowerOnProgress(int voltageProgramed, int currentProgramed) throws XrayTesterException
  {
//    System.out.println("trackPowerOnProgress()");
    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 0);

    int targetVoltage = voltageProgramed;
    int targetCurrent = currentProgramed;
    int voltageStep = targetVoltage / 3;
    int currentStep = targetCurrent / 3;

//    Pair<Integer, Integer> warmupState = getWarmupState();
    Pair<Integer, Integer> power = getVoltageAndCurrent();
    Integer voltage = power.getFirst();
    Integer current = power.getSecond();
//    System.out.println("  powerUp: V = " + voltage + ", I = " + current);
    
    while (voltage < targetVoltage && current < targetCurrent)
    {
      sleep(200);
      
      if(isAborting())
        return;
      checkForErrors();
      
      if (isSimulationModeOn())
      {
        if (voltage < voltageStep)
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 30);
          _commandingLink.setSimulatedResponseForSHV("SHV " + voltageStep);
          _commandingLink.setSimulatedResponseForSCU("SCU " + currentStep);
        }
        else if (voltage < 2 * voltageStep)
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 60);
          _commandingLink.setSimulatedResponseForSHV("SHV " + 2 * voltageStep);
          _commandingLink.setSimulatedResponseForSCU("SCU " + 2 * currentStep);
        }
        else
        {
          _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 90);
          _commandingLink.setSimulatedResponseForSHV("SHV " + targetVoltage);
          _commandingLink.setSimulatedResponseForSCU("SCU " + targetCurrent);
          _commandingLink.setSimulatedResponseForSTS("STS 2");
        }
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerUp: V = " + voltage + ", I = " + current);
      }
      else
      {
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerUp: V = " + voltage + ", I = " + current);
        int percentDone = 100 * (int)(((float)current / targetCurrent));
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, Math.min(99, percentDone));
      }
    }
    _commandingLink.setSimulatedResponseForSTS("STS 3");
    checkForErrors();
  }

  /**
   * The HTube X-ray source requires commands to be sent continuously when the x-rays are on as a safety
   * measure in case the communication link is accidentally lost.  If a command is no seen within 3 seconds,
   * the x-rays are automatically turned off.
   * @param enabled should be true to start flashing, false to stop
   * @author Bill Darbie
   * @author George Booth
   */
  public void setKeepAlive(boolean enabled) throws XrayTesterException
  {
    // XCR-3696 Additional X-ray Tube Status Log
    if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      _hTubelogger.append("Start calling setKeepAlive, enabled = " + enabled);
    }
    
    if (enabled == false)
    {
      // disable x-ray keep alive
      disableKeepAlive();
    }
    else
    {
      // OK, now start the keep alive loop
      _keepAliveWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          enableKeepAlive();

          while (isKeepAliveEnabled())
          {
            try
            {
            // send a simple command
//            System.out.print("--- keepAlive ----------");
              updateHTubeStatus();
              
              // XCR-3696 Additional X-ray Tube Status Log
              if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
              {
                boolean isInnerBarrierInstalled = false;
                boolean isInnerBarrierOpen = false;

                if (InnerBarrier.isInstalled())
                {
                  isInnerBarrierInstalled = true;
                  
                  // XCR-3740 Software crashed when shutdown xray with inner barrier open 
                  if (_digitalIo.isStartupRequired() == false)
                  {
                    if (_innerBarrier.isOpen())
                      isInnerBarrierOpen = true;
                    else
                      isInnerBarrierOpen = false;
                  }
                }

                // Check AST command
                String satResult = _commandingLink.sendAndReceive("SAT");

                _hTubelogger.append("   Keep Alive Worker Thread, _operationalStatus = " + _operationalStatus.getName() + 
                  ", isInnerBarrierInstalled = " + isInnerBarrierInstalled + ", isInnerBarrierOpen = " + isInnerBarrierOpen +
                  ", SAT = " + satResult);
              }
            }
            catch(XrayTesterException xte)
            {
              /** @todo GLB handle this exception */
            }

            // sleep
            synchronized (_keepAliveLock)
            {
              try
              {
                _keepAliveLock.wait(2000);
              }
              catch (InterruptedException ie)
              {
                // do nothing
              }
            }
          }
        }
      });
    }
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private boolean isKeepAliveEnabled()
  {
    return _enableKeepAlive;
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void enableKeepAlive()
  {
    synchronized (_keepAliveLock)
    {
      _enableKeepAlive = true;
    }
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void disableKeepAlive()
  {
    synchronized (_keepAliveLock)
    {
      _enableKeepAlive = false;
      _keepAliveLock.notifyAll();
    }
  }

  /**
   * Stops the X-ray source X-ray emissions.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author George Booth
   */
  public void turnOffXRays(boolean serviceMode) throws XrayTesterException
  {
//    System.out.println("turnOffXRays()");

    boolean needToWaitXrayOff = false;
    if (InnerBarrier.isInstalled())
    {
      if (_innerBarrier.isOpen())
        _innerBarrier.close();

      _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_CLOSE, 90);
    }
    else
    {
      needToWaitXrayOff = true;
    }

    if(serviceMode || needToWaitXrayOff)
    {
        // disable x-ray keep alive loop
        setKeepAlive(false);

        // just started
        String onCommand = "XOF";
        String onStatus = _commandingLink.sendAndReceive(onCommand);

        trackPowerOffProgress();

        _commandingLink.setSimulatedResponseForSTS("STS 2");
        setSurveyModeOn(false);
        updateHTubeStatus();
        checkForErrors();
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 80);
        if(needToWaitXrayOff)
        {
          //waitForHardwareInMilliSeconds(2000);
          //Siew Yeng - add delay time when inner barrier is not installed
          TimerUtil xrayTurnOffDelayTimer = new TimerUtil();
          xrayTurnOffDelayTimer.start();
          
          waitForHardwareInMilliSeconds(getXraySourceTurnOffDelayTimeInMiliSeconds());
          
          xrayTurnOffDelayTimer.stop();
          debug("XrayTurnOffDelayTime " + xrayTurnOffDelayTimer.getElapsedTimeInMillis() + "(ms)");
        }
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 90);
    }

  }

  /**
   * @author George Booth
   */
  private void trackPowerOffProgress() throws XrayTesterException
  {
//    System.out.println("trackPowerOffProgress()");
//    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 0);

    int voltageStep = _lastVoltageProgrammed / 2;
    int currentStep = _lastCurrentProgrammed / 2;
    Pair<Integer, Integer> power = getVoltageAndCurrent();
    Integer voltage = power.getFirst();
    Integer current = power.getSecond();
//    System.out.println("  powerDown: V = " + voltage + ", I = " + current);
    while (voltage > 0 && current > 0)
    {
      sleep(200);
      if(isAborting())
        return;
      checkForErrors();
      if (isSimulationModeOn())
      {
        if (current > currentStep)
        {
//            _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 30);
            _commandingLink.setSimulatedResponseForSHV("SHV " + voltageStep);
            _commandingLink.setSimulatedResponseForSCU("SCU " + currentStep);
        }
        else
        {
//            _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 60);
            _commandingLink.setSimulatedResponseForSHV("SHV 0");
            _commandingLink.setSimulatedResponseForSCU("SCU 0");
            _commandingLink.setSimulatedResponseForCFS("STS 2");
        }
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerDown: V = " + voltage + ", I = " + current);
      }
      else
      {
        power = getVoltageAndCurrent();
        voltage = power.getFirst();
        current = power.getSecond();
//        System.out.println("  powerDown: V = " + voltage + ", I = " + current);
//        int percentDone = 100 * (int)(1.0f - ((float)voltage / _lastCurrentProgrammed));
//        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, percentDone);
      }
    }
    _commandingLink.setSimulatedResponseForSTS("STS 2");
    checkForErrors();
  }

  /**
   * Checks the current status to determine state.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author George Booth
   */
  private boolean isHTubeIsReadyToEmit() throws XrayTesterException
  {
//    debug("isHTubeIsReadyToEmit()");
    updateHTubeStatus();
//    System.out.println("    operationalStatus = " + _operationalStatus.getName());
    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.WARMUP_YET) ||
        _operationalStatus.equals(HTubeXraySourceStatusEnum.WARMUP) ||
        _operationalStatus.equals(HTubeXraySourceStatusEnum.OVER) ||
        _operationalStatus.equals(HTubeXraySourceStatusEnum.NOT_READY))
    {
//      System.out.println("    readyToEmit = " + false);
      return false;
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.STANDBY) ||
             _operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
    {
//      System.out.println("    readyToEmit = " + true);
      return true;
    }
    else
    {
      //should not get here
      Assert.expect(false, "Unknown HTube operational status = " + _operationalStatus.getName());
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private ProgressReporterEnum makeHTubeReadyToEmit() throws XrayTesterException
  {
//    debug("makeHTubeReadyToEmit()");

    updateHTubeStatus();

    // check status
    // if STS 0 (WARMUP_YET), needs warm-up
    // if STS 1 (WARMUP), warm-up is in progress
    // if STS 2 (STANDBY), it's ready to go and this should not have been called
    // if STS 3 (XON), it's already emitting and this should not have been called
    // if STS 4, (OVER) overload protection is active and it needs resetting
    // if STS 5, (NOT_READY) it is not ready due to an error
    // if STS 6, (SELF_TEST) it's in self-test

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.WARMUP_YET))
    {
      // start the warm-up and update progress
//      _commandingLink.sendAndReceive("WUP");
      _commandingLink.sendAndReceive("XON");
//      System.out.println("  Waiting for warm-up to complete");
      _commandingLink.setSimulatedResponseForSTS("STS 1");
      _commandingLink.setSimulatedResponseForSWS("SWS 1 0");
      return trackWarmupProgress();
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.WARMUP))
    {
      // wait for warm-up to finish
//      System.out.println("    Simulated waiting for warm-up to complete sequence");
      return trackWarmupProgress();
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.STANDBY))
    {
      // good to go
//      System.out.println("    Ready to emit");
      return ProgressReporterEnum.XRAY_SOURCE_ON;
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
    {
      // already on, WTF?
//      System.out.println("    The tube is already emitting!!");
      Assert.expect(false, "Trying to make tube ready to emit and it is already on!!");
      return null;
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.OVER))
    {
      _commandingLink.sendAndReceive("RST");
//      System.out.println("    Simulated overload protection reset sequence");
      return ProgressReporterEnum.XRAY_SOURCE_POWER_OFF;
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.NOT_READY))
    {
//      System.out.println("    HTube is NOT READY!!");
      checkForErrors();
      return ProgressReporterEnum.XRAY_SOURCE_ON;
    }
    else if (_operationalStatus.equals(HTubeXraySourceStatusEnum.SELF_TEST))
    {
      // doing self-test, WTF?
//      System.out.println("    HTube is doing self-test!!");
      Assert.expect(false, "Trying to make tube ready to emit and it is in self-test!!");
      return null;
    }
    Assert.expect(false, "Unexpected operational status = " + _operationalStatus.getName());
    return null;
  }

  // for simulation
  private int[][] warmupUpModeStepMinutes = {
       {1, 1, 3, 3, 3, 4},       // SWS 1 - 15 minutes
       {5, 5, 6, 7, 7, 10},      // SWS 2 - 40 minutess
       {10, 30, 20, 30, 20, 10}  // SWS 3 - 120 minutes
   };

  /**
   * @author George Booth
   */
  private ProgressReporterEnum trackWarmupProgress() throws XrayTesterException
  {
//    System.out.println("trackWarmupProgress()");

    Pair<Integer, Integer> statusAndVoltage = getStatusAndVoltage();
    Integer status = statusAndVoltage.getFirst();
    Integer voltage = statusAndVoltage.getSecond();

    Pair<Integer, Integer> warmupState = getWarmupState();
    Integer mode = warmupState.getFirst();
    Integer step = warmupState.getSecond();

    // set up progressReported message and sim mode
    ProgressReporterEnum progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_ON;
    String simulatedResponseString = "SWS 0 0";
    long expectedTimeInSeconds = 0;
    switch (mode)
    {
      case 1:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_SHORT;
        simulatedResponseString = "SWS 1 ";
        expectedTimeInSeconds = 15 * 60;  // 15 minutes
        break;
      case 2:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_MEDIUM;
        simulatedResponseString = "SWS 2 ";
        expectedTimeInSeconds = 40 * 60;  // 40 minutes;
        break;
      case 3:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_LONG;
        simulatedResponseString = "SWS 3 ";
        expectedTimeInSeconds = 120 * 60;  // 120 minutes;
        break;
      default:
        Assert.expect(false, "Unexpected warmup mode = " + mode);
        break;
    }
    _progressObservable.reportProgress(progressReporterEnum, 0);

    // set up timer for progress report
    long initialTime = System.currentTimeMillis();
    int timeInSeconds = (int)((System.currentTimeMillis() - initialTime) / 1000L);
    int minutes = timeInSeconds / 60;
    int seconds = timeInSeconds - (minutes * 60);
//    String timeString;
//    if (seconds < 10)
//      timeString = minutes + ":0" + seconds;
//    else
//      timeString = minutes + ":" + seconds;
//    System.out.println("  (" + timeString + ") mode " + mode + " step " + step + ": voltage = " + voltage + " status = " + status);

    // track progress every second
    while (status.intValue() == 1)
    {
      int percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
//      System.out.println("    percentDone = " + percentDone);
      _progressObservable.reportProgress(progressReporterEnum, Math.min(99, percentDone));

      if (this.isSimulationModeOn()) // minutess of time has elasped and step, voltage and current change
      {
        switch (step)
        {
          case 0:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS(simulatedResponseString + "1");
            _commandingLink.setSimulatedResponseForSHV("SHV 27");
            _commandingLink.setSimulatedResponseForSCU("SCU 0");
            break;
          case 1:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS(simulatedResponseString + "2");
            _commandingLink.setSimulatedResponseForSHV("SHV 54");
            _commandingLink.setSimulatedResponseForSCU("SCU 30");
            break;
          case 2:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS(simulatedResponseString + "3");
            _commandingLink.setSimulatedResponseForSHV("SHV 81");
            _commandingLink.setSimulatedResponseForSCU("SCU 90");
            break;
          case 3:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS(simulatedResponseString + "4");
            _commandingLink.setSimulatedResponseForSHV("SHV 108");
            _commandingLink.setSimulatedResponseForSCU("SCU 150");
            break;
          case 4:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS(simulatedResponseString + "5");
            _commandingLink.setSimulatedResponseForSHV("SHV 121");
            _commandingLink.setSimulatedResponseForSCU("SCU 230");
            break;
          case 5:
            initialTime -= (long)(warmupUpModeStepMinutes[mode - 1][step] * 60 * 1000); // move initial time back minutes in milliseconds
            _commandingLink.setSimulatedResponseForSWS("SWS 0 0");
            _commandingLink.setSimulatedResponseForSTS("STS 2");
            _commandingLink.setSimulatedResponseForSHV("SHV 130");
            _commandingLink.setSimulatedResponseForSCU("SCU 300");
            break;
          default:
            Assert.expect(false, "Unexpected SWS step = " + step);
            break;
        }
      }

      sleep(1000);
      warmupState = getWarmupState();
      mode = warmupState.getFirst();
      step = warmupState.getSecond();
      statusAndVoltage = getStatusAndVoltage();
      status = statusAndVoltage.getFirst();
      voltage = statusAndVoltage.getSecond();
      timeInSeconds = (int)((System.currentTimeMillis() - initialTime) / 1000L);
      minutes = timeInSeconds / 60;
      seconds = timeInSeconds - (minutes * 60);
//      if (seconds < 10)
//        timeString = minutes + ":0" + seconds;
//      else
//        timeString = minutes + ":" + seconds;
//      System.out.println("  (" + timeString + ") mode " + mode + " step " + step + ": voltage = " + voltage + " status = " + status);
      checkForErrors();
    }

    int percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
//    System.out.println("    percentDone = " + percentDone);
    _progressObservable.reportProgress(progressReporterEnum, Math.min(99, percentDone));

    _commandingLink.setSimulatedResponseForSTS("STS 2");
    checkForErrors();
    return progressReporterEnum;
  }

/**
 * @return
 * @throws XrayTesterException
 */
  private Pair<Integer,Integer> getWarmupState() throws XrayTesterException
  {
    String result = _commandingLink.sendAndReceive("SWS");
    // "SWS 0 0"
    Matcher matcher = _commandTwoResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String mode = matcher.group(2);
      String step = matcher.group(3);
      return new Pair<Integer, Integer>(new Integer(mode), new Integer(step));
    }
   return null;
  }

  /**
   * @return
   * @throws XrayTesterException
   */
  private Pair<Integer,Integer> getStatusAndVoltage() throws XrayTesterException
  {
    String state = "0";
    String voltage = "0";
    String result = _commandingLink.sendAndReceive("STS");
    // "STS 0"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      state = matcher.group(2);
    }
    result = _commandingLink.sendAndReceive("SHV");
    // "SHV 0"
    matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      voltage = matcher.group(2);
    }
    return new Pair<Integer, Integer>(new Integer(state), new Integer(voltage));
  }

  /**
   * @return
   * @throws XrayTesterException
   */
  private Pair<Integer,Integer> getVoltageAndCurrent() throws XrayTesterException
  {
    String voltage = "0";
    String current = "0";
    String result = _commandingLink.sendAndReceive("SHV");
    // "SHV 0"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      voltage = matcher.group(2);
    }
    result = _commandingLink.sendAndReceive("SCU");
    // "SCU 0"
    matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      current  = matcher.group(2);
    }
    return new Pair<Integer, Integer>(new Integer(voltage), new Integer(current));
  }

  /**
   * @author George Booth
   */
  private void updateHTubeStatus() throws XrayTesterException
  {
    // if the tube is in the STANDBY state, it is ready to emit
    // read status
    _operationalStatus = _commandingLink.getOperationStatus();
  }

  /**
   * turn on for low-power survey/safety-test mode operation.
   *
   *  NOTE: derived classes MUST call setSurveyMode(true) after X-rays suitable
   *   for low-power survey/safety-test mode are being emitted.
   *
   * @see #isSurveyModeOn()
   *
   * @author George Booth
   */
  public void lowPowerOn() throws XrayTesterException
  {
    // turnOffXRays(_SERVICE_MODE_ON);

    setXraySourceParametersForServiceMode(_lowVoltageProgramed, _lowCurrentProgramed, getFocalSpotMode());

    if (isHTubeIsReadyToEmit() == false)
    {
      makeHTubeReadyToEmit();
    }

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
        return;

    if (isHTubeIsReadyToEmit())
    {
//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 60);
      String onCommand = "XON";
      String onStatus = _commandingLink.sendAndReceive(onCommand);

      trackPowerOnProgress(_lowVoltageProgramed, _lowCurrentProgramed);

      setSurveyModeOn(false);

//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 80);
      // enable x-ray keep alive loop
      _commandingLink.setSimulatedResponseForSTS("STS 3");
      setKeepAlive(true);

      updateHTubeStatus();
    }
    debug("lowPowerOn()");
  }

  /**
   * turn on for high-power survey/safety-test mode operation.

   *  NOTE: derived classes MUST call setSurveyMode(true) after X-rays suitable
   *   for high-power survey/safety-test mode are being emitted.
   *
   * @see #isSurveyModeOn()
   *
   * @author George Booth
   */
  public void highPowerOn() throws XrayTesterException
  {
    // turnOffXRays(_SERVICE_MODE_ON);

    setXraySourceParametersForServiceMode(_highVoltageProgramed, _highCurrentProgramed, getFocalSpotMode());


    if (isHTubeIsReadyToEmit() == false)
    {
      makeHTubeReadyToEmit();
    }

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
      return;

    if (isHTubeIsReadyToEmit())
    {
//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 60);
      String onCommand = "XON";
      String onStatus = _commandingLink.sendAndReceive(onCommand);

      trackPowerOnProgress(_highVoltageProgramed, _highCurrentProgramed);

      setSurveyModeOn(false);

//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 80);
      // enable x-ray keep alive loop
      _commandingLink.setSimulatedResponseForSTS("STS 3");
      setKeepAlive(true);

      updateHTubeStatus();
    }
    debug("highPowerOn()");
  }

  /**
   * Stop the X-ray source hardware from emitting imaging quality X-rays.  Use this method
   *  imaging is being stopped for a short time (i.e., the system is not being powered off).<p>
   *
   *   WARNING: Calling this method does NOT guarantee that no X-rays will be emitted from the
   *     X-ray source hardware.  Depending on the particular hardware, X-rays may NOT be off
   *     from a health/safety perspective.  The only guarantee is that any X-rays that may be
   *     emitted will not be adequate for imaging purposes.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see HTubeXraySource#off()
   * @see XraySource#off()
   *
   * @author George Booth
   */
  public void off() throws XrayTesterException
  {
    debug("off()");

    _htubeXrayOnStatisticalTimer.start();
//    Assert.expect(super.isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.OFF);
    _hardwareObservable.setEnabled(false);
    try
    {
      turnOffXRays(_SERVICE_MODE_OFF);
    }
    finally
    {
      _htubeXrayOnStatisticalTimer.stop();
//      debug(_htubeXrayOnStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    // 100% done
    _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_CLOSE, 100);

    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.OFF);
  }

  /**
   * @author George Booth
   */
  public void offForServiceMode() throws XrayTesterException
  {
//    System.out.println("offForServiceMode()");
    _htubeXrayOnStatisticalTimer.start();
    Assert.expect(super.isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.OFF);
    _hardwareObservable.setEnabled(false);
    try
    {
      turnOffXRays(_SERVICE_MODE_ON);
    }
    finally
    {
      // 100% done
      _progressObservable.reportProgress(ProgressReporterEnum.INNER_BARRIER_CLOSE, 100);
      _htubeXrayOnStatisticalTimer.stop();
//      debug(_htubeXrayOnStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.OFF);
  }

  /**
   * Stop the X-ray source hardware from emitting any X-rays and get it ready for all power
   *  to be turned off.
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @author George Booth
   */
  public void powerOff() throws XrayTesterException
  {
//    System.out.println("powerOff()");

    _htubeXrayOnStatisticalTimer.start();
//    Assert.expect(super.isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.POWER_OFF);
    _hardwareObservable.setEnabled(false);
    try
    {
      turnOffXRays(_SERVICE_MODE_OFF);
    }
    finally
    {
      // 100% done
//      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 100);
      _htubeXrayOnStatisticalTimer.stop();
//      debug(_htubeXrayOnStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.POWER_OFF);
  }

  /**
   * ...to the hardware
   *
   * @author George Booth
   */
  public void performSelfTest() throws XrayTesterException
  {
    debug("perfromSelfTest()");
    Assert.expect(_operationalStatus.equals(HTubeXraySourceStatusEnum.STANDBY));

    _htubeSelfTestStatisticalTimer.start();

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.SELF_TEST);
    _hardwareObservable.setEnabled(false);
    try
    {
      _commandingLink.sendAndReceive("TSF");
      _commandingLink.setSimulatedResponseForZTE("ZTE 0");
      _commandingLink.setSimulatedResponseForSTS("STS 6");

      trackSelfTestProgress();
    }
    finally
    {
      _htubeSelfTestStatisticalTimer.stop();
      debug(_htubeSelfTestStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    // 100% done
    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_SELF_TEST, 100);

    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.SELF_TEST);
  }

  /**
   * @author George Booth
   */
  private void trackSelfTestProgress() throws XrayTesterException
  {
//    System.out.println("trackSelfTestProgress()");

    long expectedTimeInSeconds = 90;

    // set up timer for progress report
    long initialTime = System.currentTimeMillis();
    int timeInSeconds = 0;

    // track progress every second
    String selfTestStatus = _commandingLink.sendAndReceive("ZTE");
    while (selfTestStatus.equals("ZTE 0"))
    {
      timeInSeconds = (int)((System.currentTimeMillis() - initialTime) / 1000L);
      int minutes = timeInSeconds / 60;
      int seconds = timeInSeconds - (minutes * 60);
//      String timeString;
//      if (seconds < 10)
//        timeString = minutes + ":0" + seconds;
//      else
//        timeString = minutes + ":" + seconds;
//      System.out.println("  (" + timeString + ") status = " + selfTestStatus);

      int percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
//      System.out.println("    percentDone = " + percentDone);
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_SELF_TEST, Math.min(99, percentDone));

      if (this.isSimulationModeOn())
      {
        initialTime -= (long)(10000); // move initial time back 10 seconds
        if (timeInSeconds > 99)
        {
          _commandingLink.setSimulatedResponseForZTE("ZTE 1");
        }
      }

      sleep(1000);
      selfTestStatus = _commandingLink.sendAndReceive("ZTE");
    }

    // final progress
    int percentDone = (int)(100.0f*((float)timeInSeconds / (float)expectedTimeInSeconds));
//    System.out.println("    percentDone = " + percentDone);
    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_SELF_TEST, Math.min(99, percentDone));

    // self test complete
    _commandingLink.setSimulatedResponseForZTE("ZTE 1");
    _commandingLink.setSimulatedResponseForZTB("ZTB 1");
    _commandingLink.setSimulatedResponseForZTR("ZTR 5 23.9 22.9 23.9 0 0 0 30 26");
    _commandingLink.setSimulatedResponseForSTS("STS 2");
    checkForErrors();
  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  public HTubeXraySourceStatusEnum getSelfTestStatus()  throws XrayTesterException
  {
    // return latest result
    return _htubeSelfTestStatus;

  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  public HTubeXraySourceStatusEnum logSelfTestResults() throws XrayTesterException
  {
//    debug("logSelfTestResults()");

    // check that self test is done
    String selfTestRunStatusString = _commandingLink.sendAndReceive("ZTE");
    HTubeXraySourceStatusEnum selfTestRunStatus = HTubeXraySourceStatusEnum.getEnum(selfTestRunStatusString);
    Assert.expect(selfTestRunStatus.equals(HTubeXraySourceStatusEnum.SELF_TEST_DONE), "Cannot get self-test status while it is still running");

/*  bug in Hamamatsu software - ZTB return value is not correct
    for the time being, use detailed results to determine pass/fail

    // get status
    String selfTestStatusString = _commandingLink.sendAndReceive("ZTB");
    System.out.println("selfTestStatusString = " + selfTestStatusString);
    HTubeXraySourceStatusEnum selfTestStatus = HTubeXraySourceStatusEnum.getEnum(selfTestStatusString);
    Assert.expect(selfTestStatus.equals(HTubeXraySourceStatusEnum.SELF_TEST_RUNNING) == false, "Cannot get self-test status while it is still running");
    _htubeSelfTestStatus = selfTestStatus;
    if (selfTestStatus.equals(HTubeXraySourceStatusEnum.SELF_TEST_FAILED))
    {
      // failed
      _selfTestResultsString = "X-ray Source self-test FAILED\n" + logSelfTestDetails();
      System.out.println(_selfTestResultsString);
      return selfTestStatus;
    }
    else if (selfTestStatus.equals(HTubeXraySourceStatusEnum.SELF_TEST_PASSED))
    {
      // passed
      _selfTestResultsString = "X-ray Source self-test Passed\n" + logSelfTestDetails();
      System.out.println(_selfTestResultsString);
      return selfTestStatus;
    }
    else
    {
      Assert.expect(false, "Unexpected selfTestStatus = " + selfTestStatus);
    }

    // shouldn't get here
    return HTubeXraySourceStatusEnum.SELF_TEST;
*/
    _selfTestResultsString = logSelfTestDetails();
    if (_selfTestResultsString.contains("X-ray Source self-test Passed"))
    {
      _htubeSelfTestStatus = HTubeXraySourceStatusEnum.SELF_TEST_PASSED;
    }
    else
    {
      _htubeSelfTestStatus = HTubeXraySourceStatusEnum.SELF_TEST_FAILED;
    }
    return _htubeSelfTestStatus;
  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  private String logSelfTestDetails() throws XrayTesterException
  {
    String result = _commandingLink.sendAndReceive("ZTR");
    String status = "X-ray Source self-test Passed\n";
    String details = "  Self-test details are not available";
    // "ZTR 4 24.5 24.2 24.3 0 0 0 72 74"
    Matcher matcher = _commandNineResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String cathodeLevel = matcher.group(2);
      String inputVoltageMax = matcher.group(3);
      String inputVoltageMin = matcher.group(4);
      String inputVoltageAve = matcher.group(5);
      String highVoltageBlock =  matcher.group(6);
      String highVoltageCondition = highVoltageBlock.equals("0")?"Normal":"Abnormal";
      String controlCkt1 =  matcher.group(7);
      String controlCkt1Condition = controlCkt1.equals("0")?"Normal":"Abnormal";
      String controlCkt2 =  matcher.group(8);
      String controlCkt2Condition = controlCkt2.equals("0")?"Normal":"Abnormal";
      String pcbTemp1 =  matcher.group(9);
      String pcbTemp2 =  matcher.group(10);

      // determine pass/fail from details until Hamamatsu fixes the self test bug
      if (Integer.parseInt(cathodeLevel) < 2 ||
          Double.parseDouble(inputVoltageMax) >= 28.0 ||
          Double.parseDouble(inputVoltageMin) <= 21.0 ||
          Double.parseDouble(inputVoltageAve) <= 21.9 ||
          Double.parseDouble(inputVoltageAve) >= 27.0 ||
          highVoltageCondition.equals("Abnormal") ||
          controlCkt1Condition.equals("Abnormal") ||
          controlCkt2Condition.equals("Abnormal") ||
          Integer.parseInt(pcbTemp1) > 71 ||
          Integer.parseInt(pcbTemp2) > 71)
      {
        status = "X-ray Source self-test FAILED\n";
      }

      details = "  Self-test details:" +
                "\n    Cathode Level = " + cathodeLevel +
                "\n    Input Supply Maximum Voltage = " + inputVoltageMax +
                "\n    Input Supply Minimum Voltage = " + inputVoltageMin +
                "\n    Input Supply Average Voltage = " + inputVoltageAve +
                "\n    High Voltage Block = " + highVoltageCondition +
                "\n    Control Circuit 1 = " + controlCkt1Condition +
                "\n    Control Circuit 2 = " + controlCkt2Condition +
                "\n    PCB Temp 1 = " + pcbTemp1 + " degrees C" +
                "\n    PCB Temp 2 = " + pcbTemp2 + " degrees C";
    }
    else
    {
      status = "X-ray Source self-test returned unmatched ZTR response: " + result;
    }
//    System.out.println(status + details);
    return status + details;
  }

  /**
   * @author George Booth
   */
  public String getSelfTestResultsString()
  {
    return _selfTestResultsString;
  }

  /**
   * warm starts are allowed only if the X-ray source was previously
   *  powered off less than a configurable number of minutes ago
   * @author Greg Loring
   * @author George Booth
   */
  public boolean isWarmStartAllowed()
  {
    /** @todo GLB figure out warm start procedure */
//    long msDown = System.currentTimeMillis() - getLastShutdownTimeInMillis();
//    int minutesDown = (int) (msDown / 60L / 1000L);
//    boolean result = (minutesDown <= getMaxMinutesDownForWarmStart());
//    trace("isWarmStartAllowed() returning " + result);
    boolean result = true;
    return result;
  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  public String getFirmwareRevisionCode() throws XrayTesterException
  {
//    debug("getFirmwareRevisionCode()");
    String result = _commandingLink.sendAndReceive("TYP");
    // Comment by LeeHerng 21-May-2010
    // In first demo system, we use HTube model "L9181-18".
    // But starting from second system onwards, we use model HTube "L9181-05".
    // So we should allow to accept either response "TYP L9181-18" or "TYP L9181-05".
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);

      return response;
    }
    return "";
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   * @author George Booth
   */
  public void setAnodeVoltageInKiloVolts(double value) throws XrayTesterException
  {
    if (_currentAnodeVoltage == value)
    {
      //skip setAnodeVoltageInKiloVolts
    }
    else
    {
      _currentAnodeVoltage = value;
//    System.out.println("setAnodeVoltageInKiloVolts(" + value + ")");
      _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ANODE_VOLTAGE);
      _hardwareObservable.setEnabled(false);
      try
      {
        _commandingLink.setSimulatedResponseForHIV("HIV " + (int) value);
        if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
        {
          _commandingLink.setSimulatedResponseForSHV("SHV " + (int) value);
        }
        else
        {
          _commandingLink.setSimulatedResponseForSHV("SHV 0");
        }
        _commandingLink.sendAndReceive("HIV " + (int) value);
        checkForHardwareReportedErrors();
        _lastVoltageProgrammed = (int) value;
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ANODE_VOLTAGE);
    }
  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  public double getAnodeVoltageInKiloVolts() throws XrayTesterException
  {
//    debug("getAnodeVoltageInKiloVolts()");
    String result = _commandingLink.sendAndReceive("SHV");
    // "SHV 130"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);
      double voltage = Double.parseDouble(response);
      return voltage;
    }
    return 0.0f;
  }

  /**
   * @return boolean true if anode is off.
   * @author Rex Shang
   * @author George Booth
   */
  public boolean isAnodeVoltageOff() throws XrayTesterException
  {
    double kV = getAnodeVoltageInKiloVolts();
    double expected = getTurnOffAnodeKiloVolts();
    double tolerance = getMaxAnodeOffKiloVolts();
    boolean result = MathUtil.fuzzyEquals(kV, expected, tolerance);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   * @author George Booth
   */
  public void setCathodeCurrentInMicroAmps(double value) throws XrayTesterException
  {
    if (_currentCathodeCurrent == value)
    {
      //skip setAnodeVoltageInKiloVolts
    }
    else
    {
      _currentCathodeCurrent = value;
//    System.out.println("setCathodeCurrentInMicroAmps(" + value + ")");
      _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.CATHODE_CURRENT);
      _hardwareObservable.setEnabled(false);
      try
      {
        _commandingLink.setSimulatedResponseForCUR("CUR " + (int) value);
        if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
        {
          _commandingLink.setSimulatedResponseForSCU("SCU " + (int) value);
        }
        else
        {
          _commandingLink.setSimulatedResponseForSCU("SCU 0");
        }
        _commandingLink.sendAndReceive("CUR " + (int) value);
        checkForHardwareReportedErrors();
        _lastCurrentProgrammed = (int) value;
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.CATHODE_CURRENT);
    }
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   * @author George Booth
   */
  public void setCathodeCurrentInMicroAmpsAndTrack(double value) throws XrayTesterException
  {
//    System.out.println("setCathodeCurrentInMicroAmpsAndTrack(" + value + ")");
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.CATHODE_CURRENT);
    _hardwareObservable.setEnabled(false);
    try
    {
      _commandingLink.setSimulatedResponseForCUR("CUR " + (int)value);
      _commandingLink.setSimulatedResponseForSCU("SCU " + (int)(value / 2.0));
      _commandingLink.sendAndReceive("CUR " + (int)value);
      trackCurrentUpdateProgress(value);
      checkForHardwareReportedErrors();
      _lastCurrentProgrammed = (int)value;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.CATHODE_CURRENT);
  }

  /**
   * @author George Booth
   */
  private void trackCurrentUpdateProgress(double desiredCurrent) throws XrayTesterException
  {
//    System.out.println("trackCurrentUpdateProgress(" + desiredCurrent + ")");

    double current = getCathodeCurrentInMicroAmps();
//    System.out.println("  current = " + current);
    while (current != desiredCurrent)
    {
      sleep(200);
      if (isSimulationModeOn())
      {
        if (current != desiredCurrent)
        {
          _commandingLink.setSimulatedResponseForSCU("SCU " + (int)desiredCurrent);
        }
      }
      current = getCathodeCurrentInMicroAmps();
//      System.out.println("  current = " + current);
    }
    checkForErrors();
  }

  /**
   * ...from the hardware
   *
   * @author George Booth
   */
  public double getCathodeCurrentInMicroAmps() throws XrayTesterException
  {
//    debug("getCathodeCurrentInMicroAmps()");
    String result = _commandingLink.sendAndReceive("SCU");
    // "SCU 300"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);
      double current = Double.parseDouble(response);
      return current;
    }
    return 0.0f;
  }

  /**
   * ...from the hardware; the input power supply supplies all the power to the HTube x-ray source
   *
   * @author George Booth
   */
  public double getInputSupplyVoltageInVolts() throws XrayTesterException
  {
//    debug("getInputSupplyVoltageInVolts() -- STUB");
    return  24.3f;
  }

  /**
   * ...from the hardware; return the focal spot mode
   *
   * @author George Booth
   */
  public void setFocalSpotMode(int mode) throws XrayTesterException
  {
    if (_currentFocalSpotMode == mode)
    {
      //skip setFocalSpotMode
    }
    else
    {
      _currentFocalSpotMode = mode;
//    System.out.println("setFocalSpotMode(" + mode + ")");
      _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.FOCAL_SPOT_MODE);
      _hardwareObservable.setEnabled(false);
      try
      {
        _commandingLink.setSimulatedResponseForCFS("CFS " + (int) mode);
        _commandingLink.setSimulatedResponseForSFC("SFC " + (int) mode);
        _commandingLink.sendAndReceive("CFS " + (int) mode);
        checkForHardwareReportedErrors();
//      _lastFocalSpotModeProgrammed = mode;
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.FOCAL_SPOT_MODE);
    }
  }

  /**
   * ...from the hardware; return the focal spot mode
   *
   * @author George Booth
   */
  public int getFocalSpotMode() throws XrayTesterException
  {
//    debug("getFocalSpotMode()");
    String result = _commandingLink.sendAndReceive("SFC");
    // "SFC 0"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);
      int mode = Integer.parseInt(response);
      return mode;
    }
    return 0;
  }

  /**
   * ...from the hardware; return the total hours power has been on
   *
   * @author George Booth
   */
  public int getPowerOnTime() throws XrayTesterException
  {
//    debug("getPowerOnTime()");
    String result = _commandingLink.sendAndReceive("STM");
    // "STM 12"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);
      int powerOnTime = Integer.parseInt(response);
      return powerOnTime;
    }
    return 0;
  }

  /**
   * ...from the hardware; return the total hours of x-ray emission
   *
   * @author George Booth
   */
  public int getXrayEmissionTime() throws XrayTesterException
  {
//    debug("getXrayEmissionTime");
    String result = _commandingLink.sendAndReceive("SXT");
    // "SXT 4"
    Matcher matcher = _commandResponsePattern.matcher(result);
    if (matcher.matches())
    {
      String command = matcher.group(1);
      String response = matcher.group(2);
      int emissionTime = Integer.parseInt(response);
      return emissionTime;
    }
    return 0;
  }

  /**
   * ...from the hardware; return the operational status code
   *
   * @author George Booth
   */
  public String getOperationalStatusString() throws XrayTesterException
  {
//    debug("getOperationalStatusString");
    String result = _commandingLink.sendAndReceive("STS");
    // "STS 2"
    return result;
  }

  /**
   * ...from the hardware; return the warmup mode code
   *
   * @author George Booth
   */
  public String getWarmupModeString() throws XrayTesterException
  {
//    debug("getOperationalStatusString");
    String result = _commandingLink.sendAndReceive("SWS");
    //  "SWS 0 0";
    return result;
  }

  /**
   * ...from the hardware; return the preheat monitor code
   *
   * @author George Booth
   */
  public String getPreheatMonitorString() throws XrayTesterException
  {
//    debug("getPreheatMonitorString");
    String result = _commandingLink.sendAndReceive("SPH");
    //  "SPH 0";
    return result;
  }

  /**
   * check for any hardware errors. If any are found, throw an exception
   * @throws XrayTesterException
   * @author George Booth
   */
  private void checkForErrors() throws XrayTesterException
  {
//    debug("checkForErrors()");
    XraySourceHardwareReportedErrorsException xshre = getHardwareReportedErrors();
    if (xshre != null)
      throw xshre;
  }

  /**
   * ask the X-ray source hardware if it has experienced any errors<p/>
   *
   *   NOTE: the errors reported have been "latched" by the hardware.  this means the hardware
   *     is remembering them even if the actual condition has been resolved.  after all errors
   *     have been resolved (by hardware service staff, for example), call the related
   *     resetHardwareReportedErrors() method<p/>
   *
   * returns an XraySourceHardwareReportedErrorsException 'cuz that class has all the localized
   *   message handling: use getLocalizedMessage() or getMessage() to see the errors
   *
   * NOTE: methods that attempt to change the state of the hardware are "wrapped" in checks
   *   using this method.  methods that simply read the state of the hardware are not so wrapped
   *
   * @return XraySourceHardwareReportedErrorsException or null if no errors are being reported
   *
   * @see #resetHardwareReportedErrors()
   *
   * @author George Booth
   */
  public XraySourceHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException
  {
//    debug("getHardwareReportedErrors()");
    XraySourceHardwareReportedErrorsException result = null;

    // read latched status
    _operationalStatus = _commandingLink.getOperationStatus();

    if (_operationalStatus.equals(HTubeXraySourceStatusEnum.NOT_READY))
    {
      //XCR-2994 - Kok Chun, Tan - set to default(hardware.config) if have any error
      _lastVoltageProgrammed = getImagingVoltage();
      _lastCurrentProgrammed = getImagingCurrent();
//      _lastFocalSpotModeProgrammed = Integer.MIN_VALUE;

      // is there an interlock error?
      result = checkHTubeInterlockStatus();
      if (result != null)
        return result;

      // is there a hardware error?
      result = checkHTubeHardwareStatus();
      if (result != null)
        return result;
    }
    else if(_operationalStatus.equals(HTubeXraySourceStatusEnum.OVER))
    {
      //XCR-2994 - Kok Chun, Tan - set to default(hardware.config) if have any error
      _lastVoltageProgrammed = getImagingVoltage();
      _lastCurrentProgrammed = getImagingCurrent();
//      _lastFocalSpotModeProgrammed = Integer.MIN_VALUE;

      // try to reset overload
      String overloadStatus = _commandingLink.sendAndReceive("RST");

      // is there still a hardware error?
      result = checkHTubeHardwareStatus();

      // make the Htube to be standby mode
      if (isHTubeIsReadyToEmit() == false)
      {
        makeHTubeReadyToEmit();
      }
      // log the selftest result to track the overload status
      HardwareWorkerThread.getInstance().invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            logSelfTestResultsIntoFile();
          }
          catch (final XrayTesterException xte)
          {
            // do nothing
            System.out.println("Unknown Xray Selftest Error : " + xte.getMessage());
          }
        }
      });      
    }
    return result;
  }

  /**
   * ask the X-ray source hardware if it has an interlock error
   *
   * @return XraySourceHardwareReportedErrorsException or null if no errors are being reported
   *
   * @author George Booth
   */
  private XraySourceHardwareReportedErrorsException checkHTubeInterlockStatus() throws XrayTesterException
  {
//    debug("checkHTubeInterlockStatus()");
    // is there an interlock error?
    String interlockStatus = _commandingLink.sendAndReceive("SIN");
    //simulate unprintable unicode response
    //interlockStatus = "\u000E\u0007\u0007SIN 0";  
    if (interlockStatus.equals("SIN 0"))
    {
      // interlocks are OK, continue
    }
    else if (interlockStatus.equals("SIN 1"))
    {
      // interlocks are open
      return new XraySourceHardwareReportedErrorsException(14);
    }
    else
    {
      //XCR-3529: Proper handle Xraytube Exception intead of assert in SIN 0 Error
      return new XraySourceHardwareReportedErrorsException(16384, 
              new Object[] 
              {interlockStatus, interlockStatus.length()});
    }
    return null;
  }

  /**
   * ask the X-ray source hardware if it has a hardware error
   *
   * @return XraySourceHardwareReportedErrorsException or null if no errors are being reported
   *
   * @author George Booth
   */
  private XraySourceHardwareReportedErrorsException checkHTubeHardwareStatus() throws XrayTesterException
  {
//    debug("checkHTubeHardwareStatus()");
    // is there a hardware error?
    String hardwareStatus = _commandingLink.sendAndReceive("SER");
    //simulate unprintable unicode response
    //hardwareStatus = "\u000E\u0007\u0007SIN 0";   
    if (hardwareStatus.equals("SER 3") || hardwareStatus.equals("SER 4") ||
        hardwareStatus.equals("SER 203") || hardwareStatus.equals("SER 204") ||
        hardwareStatus.equals("SER 206") || hardwareStatus.equals("SER 207"))
    {
      // control board failure
      return new XraySourceHardwareReportedErrorsException(1024);
    }
    else if (hardwareStatus.equals("SER 201") || hardwareStatus.equals("SER 202") ||
             hardwareStatus.equals("SER 208"))
    {
      // input supply voltage error
      return new XraySourceHardwareReportedErrorsException(2048);
    }
    else if (hardwareStatus.equals("SER 200"))
    {
      // cooling fan error
      return new XraySourceHardwareReportedErrorsException(4096);
    }
    else if (hardwareStatus.equals("SER 209"))
    {
      // temperature alarm
      return new XraySourceHardwareReportedErrorsException(8192);
    }
    else if(hardwareStatus.equals("SER 0"))
    {
      // no error!
      return null;
    }
    else
    {
      //XCR-3529: Proper handle Xraytube Exception intead of assert in SIN 0 Error
      return new XraySourceHardwareReportedErrorsException(32768, 
              new Object[] 
              {hardwareStatus, hardwareStatus.length()});
    }
    //return null;
  }

  /**
   * for simulations
   *
   * @author George Booth
   */
  private void sleep(int milliseconds)
  {
    try
    {
      Thread.sleep(milliseconds);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  /**
   * ask the X-ray source hardware to clear its error stack<p/>
   *
   * any errors detected by the hardware are saved in a priority queue.
   *
   * NOTE: doesn't check for hardware reported errors before clearing them, but
   * does check immediately after
   *
   * @see #getHardwareReportedErrors()
   *
   * @author Greg Loring
   * @author George Booth
   */
  public void resetHardwareReportedErrors() throws XrayTesterException
  {
//    debug("resetHardwareReportedErrors()");
    // don't send RST unless status = OVER (STS 4)
//    _commandingLink.sendAndReceive("RST"); // ignore response (caller presumably already knows)
    checkForHardwareReportedErrors();
  }

  /**
   * @author Wei Chin, Chong
   */
  public HTubeXraySourceStatusEnum getXraySourceStatusEnum()
  {
    return _operationalStatus;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setupHubeLog() throws DatastoreException
  {
    if(_hTubelogger == null)
    {
      createLogFileAndHeader();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void createLogFileAndHeader() throws DatastoreException
  {
    _hTubelogger = new FileLoggerAxi(FileName.getConfirmXraySourceFullPath() + ".htube" +
                                FileName.getLogFileExtension(),
                                _maxLogFileSizeInBytes);

    StringBuffer stringBuff = new StringBuffer();
    stringBuff.append("--Hube Xray Status--");
    stringBuff.append(System.getProperty("line.separator"));

    _hTubelogger.append(stringBuff.toString());
  }

  /**
   * @author Wei Chin, Chong
   */
  public void logSelfTestResultsIntoFile() throws XrayTesterException
  {
    updateHTubeStatus();
    if(_operationalStatus.equals(HTubeXraySourceStatusEnum.STANDBY))
    {
      performSelfTest();
      logSelfTestResults();
      _hTubelogger.append(_selfTestResultsString);
    }
  }

  /**
   * "logging" that is "easy" to turn on and off
   *
   * @author Greg Loring
   * @author George Booth
   */
  private void debug(Object o)
  {
    // the debug output includes timing information which changes from run to run;
    //  furthermore, this class is used all over, so debug output would show up in oodles
    //  of Test_....out files; therefore, no debug output during unit testing
    if (_debug || _simulateTiming || _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      // turn debug logging off (on) by commenting (uncommenting) out the next line
      System.out.println(_me + ": " + o);
    }
  }
  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public boolean getUsesXrayAutoShortTermShutdownEnabled()
  {
    return _xrayAutoShortTermShutdownTimerEnabled;
  }

  /**
  * @author AnthonyFong
  */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public double getXrayAutoShortTermShutdownTimeoutMinutes()
  {
    Assert.expect(_xrayAutoShortTermShutdownTimeoutMinutes >= 0);
    return _xrayAutoShortTermShutdownTimeoutMinutes;
  }

  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public synchronized void setUsesXrayAutoShortTermShutdownEnabled(boolean flag) throws XrayTesterException
  {
    _xrayAutoShortTermShutdownTimerEnabled = flag;
    _config.setValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED, flag);
  }

  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public synchronized void setXrayAutoShortTermShutdownTimeoutMinutes(double newMax) throws XrayTesterException
  {
    Assert.expect(newMax >= 0);
    _xrayAutoShortTermShutdownTimeoutMinutes = newMax;
    _config.setValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES, newMax);
  }

  /**
   * Have the current thread wait for the hardware to finish its action.
   * @param timeOutInMilliSeconds long time out of 0 will return right away.
   * @author Rex Shang
   */
  void waitForHardwareInMilliSeconds(long timeOutInMilliSeconds)
  {
    Assert.expect(timeOutInMilliSeconds >= 0, "Time out " + timeOutInMilliSeconds + " is not greater than or equal to 0.");

    long deadLineInMilliSeconds = timeOutInMilliSeconds + System.currentTimeMillis();
    long timeToWait = timeOutInMilliSeconds;

    while (isAborting() == false && timeToWait > 0)
    {
      try
      {
        Thread.sleep(timeToWait);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
      finally
      {
        // Calculate how much more time we need to wait.
        timeToWait = deadLineInMilliSeconds - System.currentTimeMillis();
      }
    }
  }

/**
   * Stops the X-ray source X-ray emissions.<p>
   *
   * Cut off the power only nothing do with the inner Barrier
   *
   * @see #areXraysOn()
   *
   * @author Wei Chin
   */
  public void turnXRaysPowerOffOnly() throws XrayTesterException
  {
    //    System.out.println("turnOffXRays()");
    // disable x-ray keep alive loop
    setKeepAlive(false);

    // just started
    String onCommand = "XOF";
    String onStatus = _commandingLink.sendAndReceive(onCommand);

    trackPowerOffProgress();

    _commandingLink.setSimulatedResponseForSTS("STS 2");
    setSurveyModeOn(false);
    updateHTubeStatus();
  }
  
  /**
   * Warmup Xray Quickly
   *
   * @author Wei Chin
   */
  public void manualWarmupXRays(int warmUpMode) throws XrayTesterException
  {
    clearAbort();
    _commandingLink.checkHTubeModelResult("TYP");
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ON);
    _hardwareObservable.setEnabled(false);    
    try
    {
      _hTubelogger.append("Update HTube Is Over?");
      if(_operationalStatus.equals(HTubeXraySourceStatusEnum.OVER))
      {
        _hTubelogger.append("Yes, Sending Reset");
        String onStatus = _commandingLink.sendAndReceive("RST");
        _hTubelogger.append("onStatus : " + onStatus);
      }
      else
      {
        _hTubelogger.append("No.");
      }

      if (isHTubeIsReadyToEmit() == false)
      {
        makeHTubeReadyToEmit();    
      }
      
      if(isAborting())
        return;

      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
      {
        _hTubelogger.append("Trying to Off Xray");
        // disable x-ray keep alive loop
        setKeepAlive(false);

        // just started
        String onCommand = "XOF";
        String onStatus = _commandingLink.sendAndReceive(onCommand);

        trackPowerOffProgress();

        _commandingLink.setSimulatedResponseForSTS("STS 2");
        setSurveyModeOn(false);
        updateHTubeStatus();
      }

      setFocalSpotMode(0);
      setAnodeVoltageInKiloVolts(30);
      setCathodeCurrentInMicroAmps(60);
      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON) == false)
      {
        _hTubelogger.append("Trying to On Xray");
        String onCommand = "XON";
        String onStatus = _commandingLink.sendAndReceive(onCommand);
      }
  //    String onCommand = "XON";
  //    String onStatus = _commandingLink.sendAndReceive(onCommand);
      trackPowerOnProgress();
      trackManualWarmupProgress(warmUpMode);
      
      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
      {
        // disable x-ray keep alive loop
        setKeepAlive(false);

        // just started
        String onCommand = "XOF";
        String onStatus = _commandingLink.sendAndReceive(onCommand);

        trackPowerOffProgress();

        _commandingLink.setSimulatedResponseForSTS("STS 2");
        setSurveyModeOn(false);
        updateHTubeStatus();
      }
      
      if(isAborting())
        return;
      
      setFocalSpotMode(1);
      setAnodeVoltageInKiloVolts(80);
      setCathodeCurrentInMicroAmps(120);

      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON) == false)
      {
        String onCommand = "XON";
        String onStatus = _commandingLink.sendAndReceive(onCommand);      
      }
      trackPowerOnProgress();
      trackManualWarmupProgress(warmUpMode);
      
      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON))
      {
        // disable x-ray keep alive loop
        setKeepAlive(false);

        // just started
        String onCommand = "XOF";
        String onStatus = _commandingLink.sendAndReceive(onCommand);

        trackPowerOffProgress();

        _commandingLink.setSimulatedResponseForSTS("STS 2");
        setSurveyModeOn(false);
        updateHTubeStatus();
      }
      
      if(isAborting())
        return;

      setFocalSpotMode(2);
      setAnodeVoltageInKiloVolts(130);
      setCathodeCurrentInMicroAmps(200);
      if (_operationalStatus.equals(HTubeXraySourceStatusEnum.XON) == false)
      {
        String onCommand = "XON";
        String onStatus = _commandingLink.sendAndReceive(onCommand);
        _commandingLink.setSimulatedResponseForSTS("STS 3");
        setKeepAlive(true);
      }
      trackPowerOnProgress();
      trackManualWarmupProgress(warmUpMode);
      
      if(isAborting())
        return;

      updateHTubeStatus();      
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 100);
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ON);    
  }
    
  /**
   * @author Wei Chin
   */
  private ProgressReporterEnum trackManualWarmupProgress(int mode) throws XrayTesterException
  {
//    System.out.println("trackManualWarmupProgress()");

    // set up progressReported message and sim mode
    ProgressReporterEnum progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_ON;
    String simulatedResponseString = "SWS 0 0";
    long expectedTimeInSeconds = 0;
    switch (mode)
    {
      case 0:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_SHORT;
        expectedTimeInSeconds = 5 * 60;  // 5 minutes
        break;
      case 1:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_MEDIUM;
        expectedTimeInSeconds = 15 * 60;  // 15 minutes
        break;
      case 2:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_LONG;
        expectedTimeInSeconds = 40 * 60;  // 40 minutes;
        break;
      case 3:
        progressReporterEnum = ProgressReporterEnum.XRAY_SOURCE_WARM_UP_LONG;
        expectedTimeInSeconds = 120 * 60;  // 120 minutes;
        break;
      default:
        Assert.expect(false, "Unexpected warmup mode = " + mode);
        break;
    }
    _progressObservable.reportProgress(progressReporterEnum, 0);

    // set up timer for progress report
    long initialTime = System.currentTimeMillis();
    int timeInSeconds = (int)((System.currentTimeMillis() - initialTime) / 1000L);
    int minutes = timeInSeconds / 60;
    int seconds = timeInSeconds - (minutes * 60);
//    String timeString;
//    if (seconds < 10)
//      timeString = minutes + ":0" + seconds;
//    else
//      timeString = minutes + ":" + seconds;
//    System.out.println("  (" + timeString + ") mode " + mode + " step " + step + ": voltage = " + voltage + " status = " + status);
    int percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
    // track progress every second
    while (percentDone <= 99)
    {
      _progressObservable.reportProgress(progressReporterEnum, Math.min(99, percentDone));      

      sleep(1000);
      timeInSeconds = (int)((System.currentTimeMillis() - initialTime) / 1000L);
      minutes = timeInSeconds / 60;
      seconds = timeInSeconds - (minutes * 60);
      percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
      
      if(isAborting())
        return progressReporterEnum;
    }

    percentDone = (int)(100.0f * ((float)timeInSeconds / (float)expectedTimeInSeconds));
    _progressObservable.reportProgress(progressReporterEnum, Math.min(99, percentDone));

    return progressReporterEnum;
  }
}
