package com.axi.v810.hardware;

import java.rmi.*;
import java.rmi.registry.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * This class is used to "push" hardware configuration information onto
 * the Native side.
 * @author Rex Shang
 */
class NativeHardwareConfiguration
{
  private Config _config;
  private static NativeHardwareConfiguration _instance = null;

  private static RemoteNativeHardwareConfigurationInterface _rnhci;
  private static HardwareTaskEngine _hardwareTaskEngine;
  
  /**
   * @author Rex Shang
   */
  private NativeHardwareConfiguration()
  {
    _config = Config.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @author Rex Shang
   */
  static NativeHardwareConfiguration getInstance()
  {
    if (_instance == null)
      _instance = new NativeHardwareConfiguration();

    return _instance;
  }

  /**
   * @throws XrayTesterException
   */
  public static synchronized void connectToHardwareConfigurationServer() throws XrayTesterException
  {
    // Check to see if we are already connected to a server
    if (_rnhci != null) 
    {
      return;
    }

    // If not attempt to connected to the server specified by the hardware config
    String rmc_ip_address = RemoteMotionServerExecution.getInstance().getServerAddress();

    if(Config.isDeveloperDebugModeOn())
      System.out.println("NativeHardwareConfiguration: Expecting Remote Native Hardware Configuration at IP: " + rmc_ip_address);

    try 
    {
      Registry registry = LocateRegistry.getRegistry(rmc_ip_address);
      _rnhci = (RemoteNativeHardwareConfigurationInterface) registry.lookup("RemoteNativeHardwareConfiguration");
    }
    catch (NotBoundException nbe) 
    {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nbe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re) 
    {
        MotionControlHardwareException mche = new MotionControlHardwareException(re);
        _hardwareTaskEngine.throwHardwareException(mche);
    }
    if(Config.isDeveloperDebugModeOn())
      System.out.println("NativeHardwareConfiguration: Connected to Remote Native Hardware Configuration at IP: " + rmc_ip_address);
  }

  /**
   * @throws XrayTesterException
   */
  void setConfigurationParameters() throws XrayTesterException, RemoteException
  {
    connectToHardwareConfigurationServer();

    // hardware simulation mode
    _rnhci.remoteSetConfigurationParameters(_config.getKey(SoftwareConfigEnum.HARDWARE_SIMULATION),
      String.valueOf(_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)));
    // motion controller model
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MOTION_CONTROLLER_0_MODEL),
      _config.getStringValue(HardwareConfigEnum.MOTION_CONTROLLER_0_MODEL));

    // motion controller driver files
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MOTION_CONTROLLER_0_DRIVER_FILES),
      _config.getStringValue(HardwareConfigEnum.MOTION_CONTROLLER_0_DRIVER_FILES));

    // SynqNet node type assignments
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_TYPE));

    // SynqNet node model assignments
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_MODEL),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_MODEL));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_MODEL),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_MODEL));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_MODEL),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_MODEL));

    // kollmorgen_cd model option
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_MODEL_OPTION),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_MODEL_OPTION));

    // SynqNet motor type assignments
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_0_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_0_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_1_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_1_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_2_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_2_TYPE));

    // SynqNet axis settings
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_X_AXIS_SETTINGS),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_X_AXIS_SETTINGS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_Y_AXIS_SETTINGS),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_Y_AXIS_SETTINGS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_RAIL_WIDTH_AXIS_SETTINGS),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_RAIL_WIDTH_AXIS_SETTINGS));

    // motion controller firmware full path file names
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MOTION_CONTROLLER_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.MOTION_CONTROLLER_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MOTION_CONTROLLER_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.MOTION_CONTROLLER_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MOTION_CONTROLLER_0_CONFIGURATION_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.MOTION_CONTROLLER_0_CONFIGURATION_FILE_NAME_FULL_PATH));

    // kollmorgen_cd firmware full path file names
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_VENDOR_FIRMWARE_FILE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_VENDOR_FIRMWARE_FILE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_NETWORK_FIRMWARE_FILE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_NETWORK_FIRMWARE_FILE));

    // kollmorgen_cd firmware versions
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_NETWORK_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_NETWORK_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_VENDOR_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_VENDOR_FIRMWARE_VERSION));

    // SynqNet node 0 settings
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_VENDOR_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_VENDOR_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_NETWORK_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_NETWORK_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_CONFIGURATION_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_CONFIGURATION_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_0_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH));

    // SynqNet node 1 settings
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_VENDOR_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_VENDOR_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_NETWORK_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_NETWORK_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_CONFIGURATION_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_CONFIGURATION_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_1_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_1_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH));

    // SynqNet node 2 settings
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_VENDOR_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_VENDOR_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_NETWORK_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_NETWORK_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_CONFIGURATION_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_CONFIGURATION_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_VENDOR_FIRMWARE_FILE_NAME_FULL_PATH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_NODE_2_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_NODE_2_NETWORK_FIRMWARE_FILE_NAME_FULL_PATH));

    // Set kollmorgen_cd motion drive RailWidth-axis parameters
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPITCH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPITCH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MOTOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MOTOR_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MIPEAK),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MIPEAK));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MSPEED),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MSPEED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MKT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MKT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCRES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCRES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCOFF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MENCOFF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLMIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLMIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPHASE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPHASE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPOLES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MPOLES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MBEMFCOMP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MBEMFCOMP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MLGAINP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MTANGLP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MVANGLH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVA),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVA));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVB),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVB));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MHINVC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VBUS),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VBUS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ILIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ILIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VLIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_VLIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MFBDIR),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MFBDIR));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF1),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF1));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF2),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_ANOFF2));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_INITGAIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_INITGAIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IENCSTART),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IENCSTART));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVTIME),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVTIME));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVRECOVER),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_UVRECOVER));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_THERMTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IZERO),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_IZERO));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_RAIL_WIDTH_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED));

    // Set kollmorgen_cd motion drive X-axis parameters
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPITCH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPITCH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MOTOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MOTOR_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MIPEAK),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MIPEAK));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MSPEED),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MSPEED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MKT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MKT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCRES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCRES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCOFF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MENCOFF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLMIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLMIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPHASE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPHASE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPOLES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MPOLES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MBEMFCOMP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MBEMFCOMP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLGAINC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLGAINC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLGAINP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MLGAINP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MTANGLC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MTANGLC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MTANGLP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MTANGLP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MVANGLF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MVANGLF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MVANGLH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MVANGLH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVA),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVA));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVB),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVB));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MHINVC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_VBUS),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_VBUS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ILIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ILIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_VLIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_VLIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MFBDIR),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MFBDIR));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ANOFF1),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ANOFF1));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ANOFF2),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_ANOFF2));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_INITGAIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_INITGAIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_IENCSTART),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_IENCSTART));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVTIME),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVTIME));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVRECOVER),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_UVRECOVER));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_THERMMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_THERMMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_THERMTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_THERMTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_IZERO),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_IZERO));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_X_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED));


    // Set kollmorgen_cd motion drive Y-axis parameters
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPITCH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPITCH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MOTOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MOTOR_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MIPEAK),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MIPEAK));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MSPEED),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MSPEED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MKT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MKT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCRES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCRES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCOFF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MENCOFF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLMIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLMIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPHASE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPHASE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPOLES),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MPOLES));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MBEMFCOMP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MBEMFCOMP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLGAINC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLGAINC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLGAINP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MLGAINP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MTANGLC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MTANGLC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MTANGLP),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MTANGLP));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MVANGLF),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MVANGLF));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MVANGLH),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MVANGLH));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVA),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVA));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVB),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVB));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVC),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MHINVC));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_VBUS),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_VBUS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ILIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ILIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ICONT),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ICONT));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_VLIM),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_VLIM));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MFBDIR),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MFBDIR));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ANOFF1),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ANOFF1));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ANOFF2),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_ANOFF2));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_INITGAIN),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_INITGAIN));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_IENCSTART),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_IENCSTART));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVTIME),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVTIME));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVRECOVER),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_UVRECOVER));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_THERMMODE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_THERMMODE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_THERMTYPE),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_THERMTYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_IZERO),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_IZERO));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_VELOCITY_LIMIT_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(SystemConfigEnum.KOLLMORGEN_CD_Y_AXIS_MAXIMUM_MOTOR_ACCELERATION_LIMIT_IN_NANOMETERS_PER_SECOND_SQUARED));

    // point-to-point motion profile 0
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_SCURVE_SMOOTHING_PERCENTAGE));

    // point-to-point motion profile 1
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_SCURVE_SMOOTHING_PERCENTAGE));

    // point-to-point motion profile 2
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_2_SCURVE_SMOOTHING_PERCENTAGE));

    // point-to-point motion profile 3
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_DECELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_VELOCITY_IN_NANOMETERS_PER_SECOND));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_POINT_TO_POINT_MOTION_PROFILE_3_SCURVE_SMOOTHING_PERCENTAGE));

    // SynqNet motor counts per revolution
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_0_COUNTS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_0_COUNTS_PER_REVOLUTION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_1_COUNTS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_1_COUNTS_PER_REVOLUTION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_2_COUNTS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_2_COUNTS_PER_REVOLUTION));

    // X, Y and rail width axis offset from home sensor
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.X_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.X_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.Y_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.Y_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_OFFSET_FROM_HOME_SENSOR_TO_HARD_STOP_IN_NANOMETERS)));

    // X and Y axis ball screw travel
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION));

    // X and Y axis position limits
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS)));

    // scan motion profile 0
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND));
    
    // scan motion profile 1 (High Magnification) - Wei Chin
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND));
    
    // scan motion profile 2 (Low Magnification, M23) - Khang Wah
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND));

    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_2_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_2_VELOCITY_IN_NANOMETERS_PER_SECOND));
    
    //XCR-3449, Invalid move velocity if all subtype is set higher stage speed
    // add in divide factor for x profile too.
    // scan motion profile 3 (log mag, slow speed, /1.5)
    String x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    long x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),1.5);
    String x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    long x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),1.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile3AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile3VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    String y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    long y_accelerate_long     = performValueRounding(Long.parseLong(y_accelerate_string),1.5);
    String y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    long y_velocity_long       = performValueRounding(Long.parseLong(y_velocity_string),1.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile3AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile3VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 4 (log mag, slow speed, /2)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),2.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),2.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile4AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile4VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long     = performValueRounding(Long.parseLong(y_accelerate_string),2.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long       = performValueRounding(Long.parseLong(y_velocity_string),2.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile4AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile4VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    //scan motion profile 5 (log mag, slow speed, /2.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),2.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),2.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile5AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile5VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),2.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),2.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile5AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile5VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 6 (log mag, slow speed, /3)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),3.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),3.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile6AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile6VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),3.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),3.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile6AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile6VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
//     scan motion profile 7 (high mag, slow speed, /3.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),3.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),3.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile7AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile7VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),3.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),3.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile7AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile7VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 8 (log mag, slow speed, /4)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),4.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),4.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile8AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile8VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),4.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),4.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile8AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile8VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 9 (high mag, slow speed, /4.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),4.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),4.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile9AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile9VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),4.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),4.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile9AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile9VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 10 (log mag, slow speed, /5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),5.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),5.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile10AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile10VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),5.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),5.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile10AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile10VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 11 (high mag, slow speed, /5.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),5.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),5.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile11AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile11VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),5.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),5.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile11AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile11VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 12 (log mag, slow speed, /6)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),6.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),6.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile12AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile12VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),6.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),6.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile12AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile12VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 13 (high mag, slow speed, /6.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),6.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),6.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile13AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile13VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),6.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),6.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile13AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile13VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 14 (log mag, slow speed, /7)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),7.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),7.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile14AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile14VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),7.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),7.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile14AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile14VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 15 (high mag, slow speed, /7.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),7.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),7.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile15AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile15VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),7.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),7.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile15AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile15VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 16 (log mag, slow speed, /8)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),8.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),8.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile16AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile16VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),8.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),8.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile16AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile16VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 17 (high mag, slow speed, /8.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),8.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),8.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile17AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile17VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),8.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),8.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile17AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile17VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 18 (log mag, slow speed, /9)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),9.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),9.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile18AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile18VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),9.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),9.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile18AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile18VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 19 (high mag, slow speed, /1.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),1.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),1.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile19AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile19VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),1.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),1.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile19AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile19VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 20 (high mag, slow speed, /2)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),2.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),2.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile20AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile20VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),2.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),2.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile20AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile20VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 21 (high mag, slow speed, /2.5)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),2.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),2.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile21AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile21VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),2.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),2.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile21AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile21VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 22 (high mag, slow speed, /3)
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),3.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),3.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile22AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile22VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),3.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),3.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile22AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile22VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 23 (high mag, slow speed, /3.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),3.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),3.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile23AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile23VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),3.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),3.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile23AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile23VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 24 (high mag, slow speed, /4), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),4.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),4.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile24AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile24VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),4.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),4.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile24AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile24VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 25 (high mag, slow speed, /4.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),4.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),4.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile25AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile25VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),4.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),4.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile25AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile25VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 26 (high mag, slow speed, /5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),5.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),5.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile26AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile26VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),5.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),5.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile26AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile26VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 27 (high mag, slow speed, /5.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),5.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),5.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile27AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile27VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),5.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),5.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile27AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile27VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 28 (high mag, slow speed, /6), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),6.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),6.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile28AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile28VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),6.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),6.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile28AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile28VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 29 (high mag, slow speed, /6.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),6.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),6.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile29AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile29VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),6.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),6.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile29AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile29VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 30 (high mag, slow speed, /7), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),7.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),7.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile30AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile30VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),7.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),7.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile30AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile30VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 31 (high mag, slow speed, /7.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),7.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),7.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile31AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile31VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),7.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),7.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile31AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile31VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 32 (high mag, slow speed, /8), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),8.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),8.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile32AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile32VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),8.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),8.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile32AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile32VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 33 (high mag, slow speed, /8.5), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),8.5);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),8.5);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile33AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile33VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),8.5);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),8.5);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile33AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile33VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // scan motion profile 34 (high mag, slow speed, /9), remain acceleration
    x_accelerate_string = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    x_accelerate_long     = performValueRounding(Long.parseLong(x_accelerate_string),9.0);
    x_velocity_string   = _config.getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    x_velocity_long       = performValueRounding(Long.parseLong(x_velocity_string),9.0);
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile34AccelerationInNanometersPerSecondSquared", Long.toString(x_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("xAxisScanMotionProfile34VelocityInNanometersPerSecond", Long.toString(x_velocity_long));
    
    y_accelerate_string = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_ACCELERATION_IN_NANOMETERS_PER_SECOND_SQUARED);
    y_accelerate_long   = performValueRounding(Long.parseLong(y_accelerate_string),9.0);
    y_velocity_string   = _config.getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
    y_velocity_long     = performValueRounding(Long.parseLong(y_velocity_string),9.0);
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile34AccelerationInNanometersPerSecondSquared", Long.toString(y_accelerate_long));
    _rnhci.remoteSetConfigurationParameters("yAxisScanMotionProfile34VelocityInNanometersPerSecond", Long.toString(y_velocity_long));
    
    // digital I/O support
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.DIGITAL_IO_HOME_DIR),
      _config.getStringValue(HardwareConfigEnum.DIGITAL_IO_HOME_DIR));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.DIGITAL_IO_CONNECTION_TYPE),
      _config.getStringValue(HardwareConfigEnum.DIGITAL_IO_CONNECTION_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.DIGITAL_IO_CONNECTION_ID),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.DIGITAL_IO_CONNECTION_ID)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.DIGITAL_IO_LOCATION_TYPE),
      _config.getStringValue(HardwareConfigEnum.DIGITAL_IO_LOCATION_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.DIGITAL_IO_LOCATION_ID),
      String.valueOf(_config.getIntValue(HardwareConfigEnum.DIGITAL_IO_LOCATION_ID)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_DIGITAL_IO_HOME_DIR),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_DIGITAL_IO_HOME_DIR));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_MODEL_OPTION),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_MODEL_OPTION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_FILE),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_FILE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_FILE),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_FILE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_VENDOR_FIRMWARE_VERSION));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_VERSION),
      _config.getStringValue(HardwareConfigEnum.MEI_SLICE_IO_TSIO_1001_NETWORK_FIRMWARE_VERSION));

    // logging options
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.ENABLE_MOTION_CONTROL_PVT_LOG),
      _config.getStringValue(HardwareConfigEnum.ENABLE_MOTION_CONTROL_PVT_LOG));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.ENABLE_MOTION_CONTROL_TIMING_LOG),
      _config.getStringValue(HardwareConfigEnum.ENABLE_MOTION_CONTROL_TIMING_LOG));

    //  panel positioner hysteresis offest adjustment
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS)));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS),
      String.valueOf(_config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS)));

    // axis home sensor types
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.X_AXIS_HOME_SENSOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.X_AXIS_HOME_SENSOR_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.Y_AXIS_HOME_SENSOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.Y_AXIS_HOME_SENSOR_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.RAIL_WIDTH_AXIS_HOME_SENSOR_TYPE),
      _config.getStringValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_HOME_SENSOR_TYPE));

    // motor encoder feedback types
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_0_ENCODER_FEEDBACK_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_0_ENCODER_FEEDBACK_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_1_ENCODER_FEEDBACK_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_1_ENCODER_FEEDBACK_TYPE));
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.SYNQNET_0_MOTOR_2_ENCODER_FEEDBACK_TYPE),
      _config.getStringValue(HardwareConfigEnum.SYNQNET_0_MOTOR_2_ENCODER_FEEDBACK_TYPE));
    
    // Swee Yee Wong - Insufficient trigger fix, scan path settings
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    double meanPositionErrorInEncoderCounts = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_MEAN_POSITION_ERROR_IN_ENCODER_COUNTS);
    double sigmaPositionErrorInEncoderCounts = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_SIGMA_POSITION_ERROR_IN_ENCODER_COUNTS);
    double multiplierPositionErrorInEncoderCounts = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_MULTIPLIER_POSITION_ERROR_IN_ENCODER_COUNTS);
    int positionErrorBufferInEncoderCounts = (int) Math.round(meanPositionErrorInEncoderCounts + sigmaPositionErrorInEncoderCounts*multiplierPositionErrorInEncoderCounts);
    
    _rnhci.remoteSetConfigurationParameters("panelScanPassPositionBufferInEncoderCounts", Integer.toString(positionErrorBufferInEncoderCounts));
    
    double minSilentPeriodInMilliseconds = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_MINIMUM_SILENT_PERIOD_IN_MS);
    double sigmaSilentPeriodInMilliseconds = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_SIGMA_SILENT_PERIOD_IN_MS);
    double multiplierSilentPeriodInMilliseconds = Config.getInstance().getDoubleValue(HardwareConfigEnum.PANEL_MULTIPLIER_SILENT_PERIOD_IN_MS);
    int silentPeriodBufferInMilliseconds = (int) Math.round(minSilentPeriodInMilliseconds + sigmaSilentPeriodInMilliseconds*multiplierSilentPeriodInMilliseconds);
    
    _rnhci.remoteSetConfigurationParameters("panelScanPassSilentPeriodBufferInMilliseconds", Integer.toString(silentPeriodBufferInMilliseconds));
    
    String enableMotionControlLog = "false";
    if(Config.isMotionControlLogEnabled())
    {
      enableMotionControlLog = "true";
    }
    _rnhci.remoteSetConfigurationParameters(_config.getKey(HardwareConfigEnum.ENABLE_MOTION_CONTROL_LOG),
      enableMotionControlLog);

  }
  
  /**
   * to minimize the precision error before passing to motion profile
   * @param originalVelocity
   * @param speedFactor
   * @return 
   * sheng chuan 
   */
  public long performValueRounding(long originalValue, double speedFactor)
  {
    double dividedValue = originalValue;
    dividedValue /= speedFactor;
    
    return (long)MathUtil.roundDoubleToInt(dividedValue); 
  }

  /**
   * @author Chong, Wei Chin
   */
  public static void resetRemoteNativeHardwareConfiguration()
  {
    _rnhci = null;
  }
}
