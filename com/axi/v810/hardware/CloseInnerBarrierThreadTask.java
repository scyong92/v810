package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class CloseInnerBarrierThreadTask extends ThreadTask<Object>
{
  private InnerBarrier _innerBarrier;

  /**
   * @author Bill Darbie
   */
  CloseInnerBarrierThreadTask()
  {
    super("Start Panel Positioner And Panel Handler Thead Task");

    _innerBarrier = InnerBarrier.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws XrayTesterException
  {
    if (_innerBarrier.isInstalled() && _innerBarrier.isOpen())
    {
      _innerBarrier.close();
    }

    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _innerBarrier.abort();
  }
}
