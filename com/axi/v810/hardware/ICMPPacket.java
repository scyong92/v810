package com.axi.v810.hardware;

import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * In order to increase network efficiency, we need ICMP MTU (Maximum Transmission
 * Unit) size much greater than the default provided by Operating System which is
 * slightly under 1500.  We are using 9014.  This class is used to help
 * validate whether a networked hardware can handle the large size.  Otherwise,
 * it will be the bottlenecking piece to limit the whole network.
 * @author Rex Shang
 */
public class ICMPPacket
{
  private final static int REGULAR_PACKET_SIZE = 1500;
  private final static int SMALL_JUMBO_PACKET_SIZE = 4088;
  private final static int BIG_JUMBO_PACKET_SIZE = 9014;

  //Swee Yee Wong - XCR-2630 - Support New X-Ray Camera
  //bring the value to hardware.config
  private static int AXI_JUMBO_PACKET_SIZE;

  private static List<Integer> _packetSizes = null;
  private static List<String> _checkedLocalIpAddress = null;
  private static List <InetAddress> _localPrivateAddresses = null;

  static
  {
    _packetSizes = new ArrayList<Integer>();
    //Swee Yee Wong - XCR-2630 - Support New X-Ray Camera
    //bring the value to hardware.config
    AXI_JUMBO_PACKET_SIZE = Config.getInstance().getIntValue(HardwareConfigEnum.XRAY_CAMERA_JUMBO_PACKET_SIZE);
    _packetSizes.add(new Integer(BIG_JUMBO_PACKET_SIZE));
    _packetSizes.add(new Integer(SMALL_JUMBO_PACKET_SIZE));
    _packetSizes.add(new Integer(REGULAR_PACKET_SIZE));
  }

  private static ICMPPacket _instance = null;

  private ICMPPacketUtil _packetUtil;
  private HardwareTaskEngine _hardwareTaskEngine;

  /**
   * @author Rex Shang
   */
  private ICMPPacket()
  {
    _localPrivateAddresses = new ArrayList<InetAddress>();
    _checkedLocalIpAddress = new ArrayList<String>();
    _packetUtil = ICMPPacketUtil.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @author Rex Shang
   */
  public static synchronized ICMPPacket getInstance()
  {
    if (_instance == null)
      _instance = new ICMPPacket();

    Assert.expect(_instance != null, "instance is null.");
    return _instance;
  }

  /**
   * @author Rex Shang
   */
  private boolean isAgilentJumboPacket(int packetSize)
  {
    if (packetSize >= AXI_JUMBO_PACKET_SIZE)
      return true;

    return false;
  }

  /**
   * Make sure we check all available private network addresses.
   * @author Rex Shang
   */
  private synchronized void checkLocalhostIsUsingAgilentJumboPacket(String ipAddress) throws XrayTesterException
  {
    if (_localPrivateAddresses.size() == 0)
      _localPrivateAddresses = IPaddressUtil.getHostInetAddressesOnPrivateNetwork();
    Assert.expect(_localPrivateAddresses.size() != 0);

    for (InetAddress address : _localPrivateAddresses)
    {
      if (_checkedLocalIpAddress.contains(address.getHostAddress()) == false)
      {
        int localPacketSize = _packetUtil.getMTUSize(address.getHostAddress(), ipAddress);

        if (isAgilentJumboPacket(localPacketSize) == false)
        {
          ICMPPacketHardwareException exception =
              ICMPPacketHardwareException.getICMPPacketSizeTooSmallException(
                  "System Controller", address.getHostAddress(), AXI_JUMBO_PACKET_SIZE, localPacketSize);
          _hardwareTaskEngine.throwHardwareException(exception);
        }
        else
          _checkedLocalIpAddress.add(address.getHostAddress());
      }
    }
  }

  /**
   * @author Rex Shang
   */
  void checkRemoteHostIsUsingAgilentJumboPacket(String name, String ipAddress) throws XrayTesterException
  {
    // Check local host first since it can be the limiting factor.
    checkLocalhostIsUsingAgilentJumboPacket(ipAddress);

    boolean isUsingJumboPacket = _packetUtil.canHostTransmitPacketSize(ipAddress, AXI_JUMBO_PACKET_SIZE);

    if (isUsingJumboPacket == false)
    {
      int remotePacketSize = getRemoteMTUSize(ipAddress);
      ICMPPacketHardwareException exception =
          ICMPPacketHardwareException.getICMPPacketSizeTooSmallException(
              name, ipAddress, AXI_JUMBO_PACKET_SIZE, remotePacketSize);
      _hardwareTaskEngine.throwHardwareException(exception);
    }
  }

  /**
   * Find the remote MTU size closest to one of our predefined size.
   * @author Rex Shang
   */
  private int getRemoteMTUSize(String ipAddress)
  {
    int remotePacketSize = 0;

    for (Integer packetSize : _packetSizes)
    {
      if (_packetUtil.canHostTransmitPacketSize(ipAddress, packetSize.intValue()))
      {
        remotePacketSize = packetSize.intValue();
        break;
      }
    }

    return remotePacketSize;
  }

}
