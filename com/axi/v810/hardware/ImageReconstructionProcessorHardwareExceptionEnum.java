package com.axi.v810.hardware;

/**
 * Used by ImageReconstructionProcessorException to help populate the exception message.
 * @author Rex Shang
 */
public class ImageReconstructionProcessorHardwareExceptionEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ImageReconstructionProcessorHardwareExceptionEnum FAILED_TO_REBOOT_IMAGE_RECONSTRUCTION_HARDWARE = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);
  public static ImageReconstructionProcessorHardwareExceptionEnum FAILED_TO_CONNECT_TO_IMAGE_RECONSTRUCTION_SERVICE = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);
  public static ImageReconstructionProcessorHardwareExceptionEnum FAILED_TO_COMMUNICATE_WITH_IMAGE_RECONSTRUCTION_SERVICE = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);
  public static ImageReconstructionProcessorHardwareExceptionEnum FAILED_TO_START_IMAGE_RECONSTRUCTION_APPLICATION = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);
  public static ImageReconstructionProcessorHardwareExceptionEnum FAILED_TO_STOP_IMAGE_RECONSTRUCTION_APPLICATION = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);
  public static ImageReconstructionProcessorHardwareExceptionEnum IMAGE_RECONSTRUCTION_APPLICATION_VERSION_MISMATCH = new ImageReconstructionProcessorHardwareExceptionEnum(++_index);

  /**
   * @author Rex Shang
   */
  private ImageReconstructionProcessorHardwareExceptionEnum(int id)
  {
    super(id);
  }

}
