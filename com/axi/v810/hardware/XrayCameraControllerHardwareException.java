package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraControllerHardwareException extends HardwareException
{
  /**
   * @author Greg Esparza
   */
  public XrayCameraControllerHardwareException(String key, List<String> parameters)
  {
    super(new LocalizedString(key, parameters.toArray()));
  }

  /**
   * @author Greg Esparza
   */
  public String getLocalizedMessage()
  {
    String finalMessage = null;
    LocalizedString header = new LocalizedString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY", null);
    finalMessage = StringLocalizer.keyToString(header);
    String mainMessageBody = StringLocalizer.keyToString(getLocalizedString());
    finalMessage += mainMessageBody;
    LocalizedString companyContactInformation = new LocalizedString("GUI_COMPANY_CONTACT_INFORMATION_KEY", null);
    finalMessage += StringLocalizer.keyToString(companyContactInformation);

    return finalMessage;
  }
}
