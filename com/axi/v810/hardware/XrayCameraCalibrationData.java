package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraCalibrationData implements Serializable
{
  private static final String _LOG_SEGMENT_NUMBER_NAME = "Segment Number";
  private static final String _LOG_PIXEL_NUMBER_NAME = "Pixel Number";
  private static final String _LOG_FORWARD_GAIN_NAME = "Forward Gain";
  private static final String _LOG_FORWARD_OFFSET_NAME = "Forward Offset";
  private static final String _LOG_REVERSE_GAIN_NAME = "Reverse Gain";
  private static final String _LOG_REVERSE_OFFSET_NAME = "Reverse Offset";
  private static final String _LOG_CAMERA_NUMBER_NAME = "Camera Number";
  private static final String _LOG_CAMERA_TEMPERATURE_IN_FARENHEIT_NAME = "Camera Temperature (F)";
  private static final String _LOG_CSV_FORMAT_DELIMITER = ",";
  private static final String _LOG_END_OF_LINE_DELIMITER = "\n";
  private static final int _MAX_LOG_FILES_IN_DIRECTORY = Config.getInstance().getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_MAX_LOG_FILES_IN_DIRECTORY);

  private List<XrayCameraSegmentCalibrationData> _xRayCameraSegmentCalibrationData;
  private List<XrayCameraPixelCalibrationData> _xRayCameraPixelCalibrationData;
  private XrayCameraIdEnum _cameraId;
  private double _cameraTemperatureInFarenheit;

  // Segment summary data
  private double _minimumSegmentForwardGain;
  private double _maximumSegmentForwardGain;
  private double _minimumSegmentReverseGain;
  private double _maximumSegmentReverseGain;
  private double _minimumSegmentForwardOffset;
  private double _maximumSegmentForwardOffset;
  private double _minimumSegmentReverseOffset;
  private double _maximumSegmentReverseOffset;

  // Pixel summary data
  private double _minimumPixelForwardGain;
  private double _maximumPixelForwardGain;
  private double _minimumPixelReverseGain;
  private double _maximumPixelReverseGain;
  private double _minimumPixelForwardOffset;
  private double _maximumPixelForwardOffset;
  private double _minimumPixelReverseOffset;
  private double _maximumPixelReverseOffset;
  
  //Swee Yee Wong
  private List<Integer> _pixelNumberIgnoredList = new ArrayList<Integer>();

  /**
   * @author Greg Esparza
   */
  public XrayCameraCalibrationData(XrayCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);

    _cameraId = cameraId;
    _xRayCameraSegmentCalibrationData = null;
    _xRayCameraPixelCalibrationData = null;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getSegmentForwardGainData()
  {
    double[] segmentGainData = new double[_xRayCameraSegmentCalibrationData.size()];

    int i = 0;
    for (XrayCameraSegmentCalibrationData segment :  _xRayCameraSegmentCalibrationData)
    {
      segmentGainData[i] = segment.getForwardGain();
      ++i;
    }

    return segmentGainData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getSegmentReverseGainData()
  {
    double[] segmentGainData = new double[_xRayCameraSegmentCalibrationData.size()];

    int i = 0;
    for (XrayCameraSegmentCalibrationData segment :  _xRayCameraSegmentCalibrationData)
    {
      segmentGainData[i] = segment.getReverseGain();
      ++i;
    }

    return segmentGainData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getSegmentForwardOffsetData()
  {
    double[] segmentOffsetData = new double[_xRayCameraSegmentCalibrationData.size()];

    int i = 0;
    for (XrayCameraSegmentCalibrationData segment :  _xRayCameraSegmentCalibrationData)
    {
      segmentOffsetData[i] = segment.getForwardOffset();
      ++i;
    }

    return segmentOffsetData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getSegmentReverseOffsetData()
  {
    double[] segmentOffsetData = new double[_xRayCameraSegmentCalibrationData.size()];

    int i = 0;
    for (XrayCameraSegmentCalibrationData segment :  _xRayCameraSegmentCalibrationData)
    {
      segmentOffsetData[i] = segment.getReverseGain();
      ++i;
    }
    return segmentOffsetData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getPixelForwardGainData()
  {
    //Swee Yee Wong - XCR-3483 Failed Grayscale adjustment in S2ex 23um
    double[] pixelGainData = new double[_xRayCameraPixelCalibrationData.size() - getPixelNumberIgnored().size()];

    int i = 0;
    for (XrayCameraPixelCalibrationData pixel :  _xRayCameraPixelCalibrationData)
    {
      if(getPixelNumberIgnored().contains(pixel.getPixelNumber()) == false)
      {
        pixelGainData[i] = pixel.getForwardGain();
        ++i;
      }
    }

    return pixelGainData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getPixelReverseGainData()
  {
    //Swee Yee Wong - XCR-3483 Failed Grayscale adjustment in S2ex 23um
    double[] pixelGainData = new double[_xRayCameraPixelCalibrationData.size() - getPixelNumberIgnored().size()];

    int i = 0;
    for (XrayCameraPixelCalibrationData pixel : _xRayCameraPixelCalibrationData)
    {
      if(getPixelNumberIgnored().contains(pixel.getPixelNumber()) == false)
      {
        pixelGainData[i] = pixel.getReverseGain();
        ++i;
      }
    }

    return pixelGainData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getPixelForwardOffsetData()
  {
    //Swee Yee Wong - XCR-3483 Failed Grayscale adjustment in S2ex 23um
    double[] pixelGainData = new double[_xRayCameraPixelCalibrationData.size() - getPixelNumberIgnored().size()];

    int i = 0;
    for (XrayCameraPixelCalibrationData pixel :  _xRayCameraPixelCalibrationData)
    {
      if(getPixelNumberIgnored().contains(pixel.getPixelNumber()) == false)
      {
        pixelGainData[i] = pixel.getForwardOffset();
        ++i;
      }
    }

    return pixelGainData;
  }

  /**
   * @author Greg Esparza
   */
  private double[] getPixelReverseOffsetData()
  {
    //Swee Yee Wong - XCR-3483 Failed Grayscale adjustment in S2ex 23um
    double[] pixelGainData = new double[_xRayCameraPixelCalibrationData.size() - getPixelNumberIgnored().size()];

    int i = 0;
    for (XrayCameraPixelCalibrationData pixel : _xRayCameraPixelCalibrationData)
    {
      if(getPixelNumberIgnored().contains(pixel.getPixelNumber()) == false)
      {
        pixelGainData[i] = pixel.getReverseOffset();
        ++i;
      }
    }

    return pixelGainData;
  }

  /**
   * @author Greg Esparza
   */
  private void calculateMinimumAndMaximumSegmentGain()
  {
    double[] forwardGain = getSegmentForwardGainData();
    double[] reverseGain = getSegmentReverseGainData();

    _minimumSegmentForwardGain = ArrayUtil.min(forwardGain);
    _maximumSegmentForwardGain = ArrayUtil.max(forwardGain);
    _minimumSegmentReverseGain = ArrayUtil.min(reverseGain);
    _maximumSegmentReverseGain = ArrayUtil.max(reverseGain);
  }

  /**
   * @author Greg Esparza
   */
  private void calculateMinimumAndMaximumSegmentOffset()
  {
    double[] forwardOffset = getSegmentForwardOffsetData();
    double[] reverseOffset = getSegmentReverseOffsetData();

    _minimumSegmentForwardOffset = ArrayUtil.min(forwardOffset);
    _maximumSegmentForwardOffset = ArrayUtil.max(forwardOffset);
    _minimumSegmentReverseOffset = ArrayUtil.min(reverseOffset);
    _maximumSegmentReverseOffset = ArrayUtil.max(reverseOffset);
   }

  /**
   * @author Greg Esparza
   */
  private void calculateMinimumAndMaximumPixelGain()
  {
    double[] forwardGain = getPixelForwardGainData();
    double[] reverseGain = getPixelReverseGainData();

    _minimumPixelForwardGain = ArrayUtil.min(forwardGain);
    _maximumPixelForwardGain = ArrayUtil.max(forwardGain);
    _minimumPixelReverseGain = ArrayUtil.min(reverseGain);
    _maximumPixelReverseGain = ArrayUtil.max(reverseGain);
  }

  /**
   * @author Greg Esparza
   */
  private void calculateMinimumAndMaximumPixelOffset()
  {
    double[] forwardOffset = getPixelForwardOffsetData();
    double[] reverseOffset = getPixelReverseOffsetData();

    _minimumPixelForwardOffset = ArrayUtil.min(forwardOffset);
    _maximumPixelForwardOffset = ArrayUtil.max(forwardOffset);
    _minimumPixelReverseOffset = ArrayUtil.min(reverseOffset);
    _maximumPixelReverseOffset = ArrayUtil.max(reverseOffset);
  }

  /**
   * @author Greg Esparza
   */
  void set(double cameraTemperatureInFarenheit,
           List<XrayCameraSegmentCalibrationData> xRayCameraSegmentCalibrationData,
           List<XrayCameraPixelCalibrationData> xRayCameraPixelCalibrationData)
  {
    Assert.expect(xRayCameraSegmentCalibrationData != null);
    Assert.expect(xRayCameraPixelCalibrationData != null);

    _cameraTemperatureInFarenheit = cameraTemperatureInFarenheit;
    _xRayCameraSegmentCalibrationData = xRayCameraSegmentCalibrationData;
    _xRayCameraPixelCalibrationData = xRayCameraPixelCalibrationData;

    calculateMinimumAndMaximumSegmentGain();
    calculateMinimumAndMaximumSegmentOffset();
    calculateMinimumAndMaximumPixelGain();
    calculateMinimumAndMaximumPixelOffset();
  }

  /**
   * @author Greg Esparza
   */
  List<XrayCameraSegmentCalibrationData> getSegmentData()
  {
    Assert.expect(_xRayCameraSegmentCalibrationData != null);
    return _xRayCameraSegmentCalibrationData;
  }

  /**
   * @author Greg Esparza
   */
  List<XrayCameraPixelCalibrationData> getPixelData()
  {
    Assert.expect(_xRayCameraPixelCalibrationData != null);
    return _xRayCameraPixelCalibrationData;
  }

  /**
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  private String getPixelLogData()
  {
    Assert.expect(_xRayCameraPixelCalibrationData != null);
    StringBuilder pixelLogDataBuffer = new StringBuilder();

    // Set up the title header for each column
    pixelLogDataBuffer.append(_LOG_PIXEL_NUMBER_NAME);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_FORWARD_GAIN_NAME);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_FORWARD_OFFSET_NAME);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_REVERSE_GAIN_NAME);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_REVERSE_OFFSET_NAME);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    for (XrayCameraPixelCalibrationData pixel : _xRayCameraPixelCalibrationData)
    {
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(String.valueOf(pixel.getPixelNumber()));
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(String.valueOf(pixel.getForwardGain()));
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(String.valueOf(pixel.getForwardOffset()));
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(String.valueOf(pixel.getReverseGain()));
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      pixelLogDataBuffer.append(String.valueOf(pixel.getReverseOffset()));
      pixelLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);
    }

    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);
    pixelLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    pixelLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    return pixelLogDataBuffer.toString();
  }

  /**
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  private String getSegmentLogData()
  {
    Assert.expect(_xRayCameraSegmentCalibrationData != null);

    StringBuilder segmentLogDataBuffer = new StringBuilder();

    // Set up the title header for each column
    segmentLogDataBuffer.append(_LOG_SEGMENT_NUMBER_NAME);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_FORWARD_GAIN_NAME);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_FORWARD_OFFSET_NAME);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_REVERSE_GAIN_NAME);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_REVERSE_OFFSET_NAME);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    for (XrayCameraSegmentCalibrationData segment : _xRayCameraSegmentCalibrationData)
    {
      segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      segmentLogDataBuffer.append(String.valueOf(segment.getSegmentNumber()));
      segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      segmentLogDataBuffer.append(String.valueOf(segment.getForwardGain()));
      segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      segmentLogDataBuffer.append(String.valueOf(segment.getForwardOffset()));
      segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      segmentLogDataBuffer.append(String.valueOf(segment.getReverseGain()));
      segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
      segmentLogDataBuffer.append(String.valueOf(segment.getReverseOffset()));
      segmentLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);
    }

    // Allow a couple of empty rowss
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);
    segmentLogDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    segmentLogDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    return segmentLogDataBuffer.toString();
  }

  /**
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  private String getCameraNumberLogData()
  {
    StringBuilder logDataBuffer = new StringBuilder();

    logDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    logDataBuffer.append(_LOG_CAMERA_NUMBER_NAME);
    logDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    logDataBuffer.append(String.valueOf(_cameraId.getId()));
    logDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    return logDataBuffer.toString();
  }

  /**
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  private String getCameraTemperatureInFarenheitLogData()
  {
    StringBuilder logDataBuffer = new StringBuilder();

    logDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    logDataBuffer.append(_LOG_CAMERA_NUMBER_NAME);
    logDataBuffer.append(_LOG_CSV_FORMAT_DELIMITER);
    logDataBuffer.append(String.valueOf(_cameraTemperatureInFarenheit));
    logDataBuffer.append(_LOG_END_OF_LINE_DELIMITER);

    return logDataBuffer.toString();
  }

  /**
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  private String getLogData()
  {
    StringBuilder logDataBuffer = new StringBuilder();

    logDataBuffer.append(getCameraNumberLogData());
    logDataBuffer.append(getCameraTemperatureInFarenheitLogData());
    logDataBuffer.append(getSegmentLogData());
    logDataBuffer.append(getPixelLogData());

    return logDataBuffer.toString();
  }

  /**
   * Currently, the data will log in the CSV format that will make it easy to import into an Excel spreadsheet.
   *
   * @param targetLogDirectory is the desired directory to log the data
   * @param frequencyTimeIntervalInMilliseconds defines how often the client would like to log the data.
   * @author Greg Esparza
   * @author Reid Hayhow
   */
  public void log(String targetLogDirectory, int frequencyTimeIntervalInMilliseconds) throws DatastoreException
  {
    Assert.expect(_xRayCameraSegmentCalibrationData != null);
    Assert.expect(_xRayCameraPixelCalibrationData != null);
    Assert.expect(targetLogDirectory != null);
    Assert.expect(frequencyTimeIntervalInMilliseconds >= 0);

    DirectoryLoggerAxi logUtility = new DirectoryLoggerAxi(targetLogDirectory,
                                                           FileName.getGreyscaleLogFileBasename() + "_camera_" + String.valueOf(_cameraId.getId()) + "_",
                                                           FileName.getLogFileExtension(),
                                                           _MAX_LOG_FILES_IN_DIRECTORY);

    logUtility.logIfTimedOut(getLogData(), frequencyTimeIntervalInMilliseconds);
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumSegmentForwardGain()
  {
    return _minimumSegmentForwardGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumSegmentForwardGain()
  {
    return _maximumSegmentForwardGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumSegmentReverseGain()
  {
    return _minimumSegmentReverseGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumSegmentReverseGain()
  {
    return _maximumSegmentReverseGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumSegmentForwardOffset()
  {
    return _minimumSegmentForwardOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumSegmentForwardOffset()
  {
    return _maximumSegmentForwardOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumSegmentReverseOffset()
  {
    return _minimumSegmentReverseOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumSegmentReverseOffset()
  {
    return _maximumSegmentReverseOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumPixelForwardGain()
  {
    return _minimumPixelForwardGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumPixelForwardGain()
  {
    return _maximumPixelForwardGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumPixelReverseGain()
  {
    return _minimumPixelReverseGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumPixelReverseGain()
  {
    return _maximumPixelReverseGain;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumPixelForwardOffset()
  {
    return _minimumPixelForwardOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumPixelForwardOffset()
  {
    return _maximumPixelForwardOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMinimumPixelReverseOffset()
  {
    return _minimumPixelReverseOffset;
  }

  /**
   * @author Greg Esparza
   */
  public double getMaximumPixelReverseOffset()
  {
    return _maximumPixelReverseOffset;
  }
  
  /**
   * @author Wei Chin
   */
  public boolean hasCalibrationData()
  {
    return (_xRayCameraSegmentCalibrationData != null) && (_xRayCameraPixelCalibrationData != null);
  }
  
  /**
   * @author Swee Yee
   * XCR-3483 Failed Grayscale adjustment in S2ex 23um
   */
  public void setPixelNumberIgnored(List<Integer> listOfPixelNumberIgnored)
  {
    Assert.expect(_pixelNumberIgnoredList != null);
    Assert.expect(listOfPixelNumberIgnored != null);
    
    for (int pixelNumber : listOfPixelNumberIgnored)
    {
      if(_pixelNumberIgnoredList.contains(pixelNumber) == false)
        _pixelNumberIgnoredList.add(pixelNumber);
    }
  }
  /**
   * @author Swee Yee
   * XCR-3483 Failed Grayscale adjustment in S2ex 23um
   */
  private List<Integer> getPixelNumberIgnored()
  {
    Assert.expect(_pixelNumberIgnoredList != null);
    return _pixelNumberIgnoredList;
  }
  
  /**
   * @author Swee Yee
   * XCR-3483 Failed Grayscale adjustment in S2ex 23um
   */
  public void clearPixelNumberIgnored()
  {
    Assert.expect(_pixelNumberIgnoredList != null);
    _pixelNumberIgnoredList.clear();
  }
}
