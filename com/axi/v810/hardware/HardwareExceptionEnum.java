package com.axi.v810.hardware;

/**
 * @author Greg Esparza
 */
public class HardwareExceptionEnum extends com.axi.util.Enum
{
  // Note: DO NOT define any enumerations here.
  //
  // This class is only used to enforce a common interface for the HardwareTaskEngine or other objects that need
  // to evaluate an exception type.  Some, but not all hardware exception classes contain their specific enumerated
  // exception type as part of their exception object.  The client, like HardwareTaskEngine, can examine which type of
  // exception it is a determine if a corresponding confirmation, diagnostic or adjustment task should run.

  /**
   * @author Greg Esparza
   */
  protected HardwareExceptionEnum(int id)
  {
    super(id);
  }
}
