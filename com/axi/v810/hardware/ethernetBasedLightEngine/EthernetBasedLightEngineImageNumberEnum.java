package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineImageNumberEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private byte _value = 0;
  
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_0 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x00);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_1 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x10);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_2 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x20);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_3 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x30);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_4 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x40);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_5 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x50);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_6 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x60);
  public static EthernetBasedLightEngineImageNumberEnum NUMBER_7 = new EthernetBasedLightEngineImageNumberEnum(++_index, (byte)0x70);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineImageNumberEnum(int id, byte value)
  {
    super(id);
    _value = value;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public byte getValue()
  {
    return _value;
  }
}
