package com.axi.v810.hardware.ethernetBasedLightEngine;

import java.io.*;
import java.nio.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngine extends HardwareObject implements Observer
{
  private static final int _SOCKET_COMMUNICATION_TIMEOUT_IN_MILLISECONDS        = 15000;
  private static final int WRITE_BYTE_BUFFER_LENGTH                             = 6;
  private static final int RESPONSE_WRITE_BYTE_BUFFER_LENGTH                    = 7;
  private static final int READ_BYTE_BUFFER_LENGTH                              = 5;
  private static final int BLOCK_WRITE_BYTE_BUFFER_LENGTH                       = 12;
  private static final int BLOCK_WRITE_PRELOAD_IMAGE_BYTE_BUFFER_LENGTH         = 1032;
  private static final int BLOCK_WRITE_LINE_DATA_BYTE_BUFFER_LENGTH             = 598;
  private static final byte KEEP_ALIVE_BYTE                                     = (byte)0x06;  
  private static final int WAIT_FOR_IMAGE_READY_IN_MILISECONDS                  = 50;
  
  private EthernetBasedLightEngineEnum _ethernetBasedLightEngineEnum;
  private InetAddress _hostInetAddress = null;
  private InetSocketAddress _inetSocketAddress = null;

  private SocketClient _socketClient;
  private ByteOrder _byteOrder = ByteOrder.LITTLE_ENDIAN;
  
  private String _ipAddress = "";
  private int _portNumber;
  
  private int _currentSetting;
  private int _formulaPeriod;
  
  private boolean _displayWhiteLight; // Special handling for white light because we need to switch between line data and pre-defined data
  
  private boolean _unitTestModeOn;
  
  private static Map<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> _ethernetBasedLightEngines = new HashMap<EthernetBasedLightEngineEnum, EthernetBasedLightEngine>();
  
  private static Config _config;
  
  private static HardwareObservable _hardwareObservable;
  
  /**
   * @author Cheah Lee Herng
   */
  static
  {
    _config = Config.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }
  
  /**
   * Default Constructor
   * 
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngine(EthernetBasedLightEngineEnum id)
  {
    _ethernetBasedLightEngineEnum = id;

    // We need to listen to certain hardware events.
    _hardwareObservable.addObserver(this);
    
    String hostIpAddress = _config.getStringValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_HOST_IP_ADDRESS);
    
    if (id.equals(EthernetBasedLightEngineEnum.ENGINE_0))
    {
      _ipAddress = _config.getStringValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_0_IP_ADDRESS);
      _portNumber = _config.getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_0_PORT);
    }
    else if (id.equals(EthernetBasedLightEngineEnum.ENGINE_1))
    {
      _ipAddress = _config.getStringValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_1_IP_ADDRESS);
      _portNumber = _config.getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_1_PORT);
    }
    else
      Assert.expect(false, "Invalid EthernetBasedLightEngineEnum");
    
    _currentSetting = _config.getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_CURRENT_SETTING_MICRO_AMPS);
    _formulaPeriod = _config.getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_PERIOD);
    
    try
    {
      _hostInetAddress = InetAddress.getByName(hostIpAddress);
      _inetSocketAddress = getBindInetAddress();
    }
    catch (UnknownHostException e)
    {
      Assert.expect(false, "Undefined EthernetBasedLightEngineProcessor host address: " + e.getMessage());
    }
    
    _unitTestModeOn = false;
    setCommunication();
    
    _displayWhiteLight = false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  static public synchronized EthernetBasedLightEngine getInstance(EthernetBasedLightEngineEnum id)
  {
    Assert.expect(id != null, "id is null.");

    EthernetBasedLightEngine ible = _ethernetBasedLightEngines.get(id);

    if (ible == null)
    {
      ible = new EthernetBasedLightEngine(id);
      _ethernetBasedLightEngines.put(id, ible);
    }

    Assert.expect(ible!= null, "IRE is null.");
    return ible;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public InetSocketAddress getBindInetAddress()
  {
    Assert.expect(_hostInetAddress != null);
    
    if (_inetSocketAddress == null)
    {
      _inetSocketAddress = new InetSocketAddress(_hostInetAddress, 0);
    }
    
    return _inetSocketAddress;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setCommunication()
  {
    _socketClient = new SocketClient(_ipAddress, _portNumber);
    _socketClient.setDefaultByteOrderForSendAndRecieveBuffers(_byteOrder);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void initializeSystem() throws XrayTesterException
  {
    // Default switch mode to 0
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Set EBLE current setting
    sendAdjustCurrentMessage(EthernetBasedLightEngineCurrentSettingEnum.getEnum(_currentSetting));
    
    // Perform health check before start doing anything
    sendHealthCheckMessage();
    
    // Send Image Setting
    sendImageSettingMessage();
    
    // Enable projector and turn off projector LED by default
    sendEnableProjectorLEDMessage(true, false);
    
    // Initialize variable
    _displayWhiteLight = false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void closeConnections() throws XrayTesterException
  {
    if (_socketClient != null && _socketClient.isConnected())
    {
      disableProjector();

      try
      {
        _socketClient.close();
      }
      catch (IOException ioe)
      {
        // Do nothing.
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private ByteBuffer sendRequestAndReceiveReplies(ByteBuffer buffer, int replySizeInBytes, boolean bypassKeepAliveByteChecking) throws XrayTesterException
  {
    Assert.expect(buffer != null);
    
    ByteBuffer replyBuffer = null;
    try
    {
      connectIfNecessary();      
      replyBuffer = _socketClient.sendRequestAndReceiveReplies(buffer, replySizeInBytes, bypassKeepAliveByteChecking, KEEP_ALIVE_BYTE);
    }
    catch (IOException ex)
    {
      //ok, we failed, try reconnecting and sending again.
      try
      {
        if (_socketClient.isConnected())
          _socketClient.close();
      }
      catch (IOException ioe)
      {
        // Do nothing.
      }
      
      try
      {
        connectIfNecessary();
        replyBuffer = _socketClient.sendRequestAndReceiveReplies(buffer, replySizeInBytes, bypassKeepAliveByteChecking, KEEP_ALIVE_BYTE);
      }
      catch (IOException ioe)
      {
        EthernetBasedLightEngineHardwareException eblehe = EthernetBasedLightEngineHardwareException.getFailedToCommunicateWithEthernetBasedLightEngineException(
                                                                                                                                              _ethernetBasedLightEngineEnum.toString(), 
                                                                                                                                              _ipAddress, 
                                                                                                                                              _portNumber);
        eblehe.initCause(ioe);
        throw eblehe;
      }
    }

    Assert.expect(replyBuffer != null);
    return replyBuffer;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void connectIfNecessary() throws XrayTesterException
  {
    if (isSimulationModeOn() == false || isUnitTestModeOn())
    {
      try
      {
        if (_socketClient.isConnected() == false)
        {
          _socketClient.connect(_SOCKET_COMMUNICATION_TIMEOUT_IN_MILLISECONDS);
        }
      }
      catch (SocketConnectionFailedException scfe)
      {
        EthernetBasedLightEngineHardwareException eblehe = EthernetBasedLightEngineHardwareException.getFailedToCommunicateWithEthernetBasedLightEngineException(
                                                                                                                                              _ethernetBasedLightEngineEnum.toString(), 
                                                                                                                                              _ipAddress, 
                                                                                                                                              _portNumber);
        eblehe.initCause(scfe);
        throw eblehe;
      }
    }
  }
   
  /**
   * This function sends switch mode message to Ethernet-Based Light Engine.
   * It supports 3 types of switch mode:
   *   Mode 0 : Default Mode (allows setting of formula)
   *   Mode 1 : Display Mode (when we want to project certain fringe pattern)
   *   Mode 2 : This is still under development
   * 
   * @author Cheah Lee Herng
   */
  public void sendSwitchModeMessage(SwitchModeEnum mode) throws XrayTesterException
  {
    Assert.expect(mode != null);
    
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.SETTING_MODE_SELECTION.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    
    if (mode.equals(SwitchModeEnum.MODE_0))
      byteBuffer.put((byte)0x00); // Data
    else if (mode.equals(SwitchModeEnum.MODE_1))
      byteBuffer.put((byte)0x01); // Data
    else if (mode.equals(SwitchModeEnum.MODE_2))
      byteBuffer.put((byte)0x02); // Data
    else
      Assert.expect(false, "Invalid SwitchModeEnum");
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.SETTING_MODE_SELECTION.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function checks the status of Ethernet-Based Light Engine.
   * 
   * @author Cheah Lee Herng
   */
  public void sendHealthCheckMessage() throws XrayTesterException
  {
    ByteBuffer byteBuffer = getByteBuffer(READ_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x52); // 'R'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.STATUS_HEALTH_CHECK.getId());  // Start Address
    byteBuffer.put((byte)0x05); // Length
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, READ_BYTE_BUFFER_LENGTH + 5, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'R') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.STATUS_HEALTH_CHECK.getId()) &&
         (readByteArray[3] == (byte)0x05) &&
         (readByteArray[9] == calculateCRC8(byteBuffer))))
    {
      verifyHealthCheckResponse(readByteArray[4]);
    }
    else
      handleResponseErrorMessageException(byteBuffer);
  }
  
  /**
   * This function checks if image is ready in the memory block.
   * 
   * @author Cheah Lee Herng 
   */
  public boolean isImageReady() throws XrayTesterException
  {    
    boolean isImageReady = false;
    
    ByteBuffer byteBuffer = getByteBuffer(READ_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x52); // 'R'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.STATUS_HEALTH_CHECK.getId());  // Start Address
    byteBuffer.put((byte)0x05); // Length
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, READ_BYTE_BUFFER_LENGTH + 5, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'R') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.STATUS_HEALTH_CHECK.getId()) &&
         (readByteArray[3] == (byte)0x05) &&
         (readByteArray[9] == calculateCRC8(byteBuffer))))
    {
      byte cameraStatusByte = readByteArray[7];
      
      if ((cameraStatusByte & 0x01) == 0x01) // Camera Ready Check
      {
        isImageReady = true;
      }
    }
    
    return isImageReady;
  }
  
  /**
   * This function checks if image is ready in memory block. It will block until image is ready.
   * 
   * @throws XrayTesterException 
   */
  public void waitUntilImageReady() throws XrayTesterException
  {    
    try
    {
      Thread.sleep(WAIT_FOR_IMAGE_READY_IN_MILISECONDS);
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }
  }
  
  /**
   * This function enable/disable projector LED.
   * The default behaviour is to enable projector and enable/disable LED.
   * 
   * Below are the command we need to send over for the enable/disable projector 
   * and on/off operation.
   * 
   * OPERATION          COMMAND
   * --------------------------
   * ON LED             0x03
   * OFF LED            0x01
   * ENABLE PROJECTOR   0x01
   * ENABLE PROJECTOR   0x03
   * DISABLE PROJECTOR  0x00
   * DISABLE PROJECTOR  0x02
   * 
   * @author Cheah Lee Herng 
   */
  public void sendEnableProjectorLEDMessage(boolean enableProjector, boolean turnOnLED) throws XrayTesterException
  {
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.SETTING_PROJ_EN_DIS_LED_ON_OFF.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    
    byte command = 0;
//    if (enableProjector)
//    {
//      if (turnOnLED)
//        command |= 0x01;  // Enable projector and Off LED
//      else
//        command |= 0x03;  // Enable projector and On LED
//    }
//    else
//      command |= 0x00;  // Disable projector
//    
//    byteBuffer.put(command);  
    
    if (turnOnLED)
    {
      command |= 0x01;  // Enable projector
      command |= 0x02;  // Turn on LED
      
      byteBuffer.put(command);
    }
    else
    {
      command |= 0x01;  // Enable projector
      command |= 0x00;  // Turn off LED
      
      byteBuffer.put(command);
    }
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.SETTING_PROJ_EN_DIS_LED_ON_OFF.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function decide if projector will always stay on or will close after certain amount of timeout.
   * 
   * @author Cheah Lee Herng 
   */
  public void sendEnableProjectorLEDTimeoutMessage(boolean enable) throws XrayTesterException
  {
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.SETTING_LED_TIMEOUT_EN_DIS.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    
    if (enable)
      byteBuffer.put((byte)0x01);
    else
      byteBuffer.put((byte)0x00);
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.SETTING_LED_TIMEOUT_EN_DIS.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function sends the required command to light engine for initialization.
   * 
   * @author Cheah Lee Herng
   */
  public void sendImageSettingMessage() throws XrayTesterException
  {
    setupLineDataSetting();
  }
  
  /**
   * Currently we do not use formula setting. But we keep the function below
   * for future reference.
   * 
   * @author Cheah Lee Herng
   */
  private void setupFormulaSetting() throws XrayTesterException
  {
    // Block Image 0 - Fringe Image 1
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_0_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.FORMULA,
                            EthernetBasedLightEngineImageDisplayTypeEnum.HORIZONTAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendFormulaMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_0,
                       EthernetBasedLightEngineFringePeriodEnum.getEnum(_formulaPeriod),
                       EthernetBasedLightEngineFringePhaseEnum.PHASE_1,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_0,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_255);
    
    // Block Image 1 - Fringe Image 2
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_1_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.FORMULA,
                            EthernetBasedLightEngineImageDisplayTypeEnum.HORIZONTAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendFormulaMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_1,
                       EthernetBasedLightEngineFringePeriodEnum.getEnum(_formulaPeriod),
                       EthernetBasedLightEngineFringePhaseEnum.PHASE_2,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_0,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_255);
    
    // Block Image 2 - Fringe Image 3
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_2_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.FORMULA,
                            EthernetBasedLightEngineImageDisplayTypeEnum.HORIZONTAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendFormulaMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_2,
                       EthernetBasedLightEngineFringePeriodEnum.getEnum(_formulaPeriod),
                       EthernetBasedLightEngineFringePhaseEnum.PHASE_3,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_0,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_255);
    
    // Block Image 3 - Fringe Image 4
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_3_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.FORMULA,
                            EthernetBasedLightEngineImageDisplayTypeEnum.HORIZONTAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendFormulaMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_3,
                       EthernetBasedLightEngineFringePeriodEnum.getEnum(_formulaPeriod),
                       EthernetBasedLightEngineFringePhaseEnum.PHASE_4,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_0,
                       EthernetBasedLightEngineFringeIntensityEnum.INTENSITY_255);
    
    // Block Image 4 - White Image
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_4_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLoadPredefinedImageMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_4,
                                   EthernetBasedLightEnginePredefinedImageEnum.IMAGE_2);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setupLineDataSetting() throws XrayTesterException
  {
    setupLineDataBlock0();
    setupLineDataBlock1();
    setupLineDataBlock2();
    setupLineDataBlock3();
    setupLineDataBlock4();
    setupLineDataBlock5();
    setupLineDataBlock6();
    setupLineDataBlock7();
    
//    // Block Image 9 - White Image
//    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_4_SETTING,
//                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
//                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
//                            EthernetBasedLightEngineImageColorEnum.WHITE);
//    
//    sendLoadPredefinedImageMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_9,
//                                   EthernetBasedLightEnginePredefinedImageEnum.IMAGE_2);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock0() throws XrayTesterException
  {
    // Block Image 0 - Fringe Image 1
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_0_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_0,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_1,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock1() throws XrayTesterException
  {
    // Block Image 1 - Fringe Image 2
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_1_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_1,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_2,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock2() throws XrayTesterException
  {
    // Block Image 2 - Fringe Image 3
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_2_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_2,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_3,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock3() throws XrayTesterException
  {
    // Block Image 3 - Fringe Image 4
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_3_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_3,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_4,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock4() throws XrayTesterException
  {
    // Block Image 4 - Fringe Image 5
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_4_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_4,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_5,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock5() throws XrayTesterException
  {
    // Block Image 5 - Fringe Image 6
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_5_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_5,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_6,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock6() throws XrayTesterException
  {
    // Block Image 6 - Fringe Image 7
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_6_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_6,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_7,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupLineDataBlock7() throws XrayTesterException
  {
    // Block Image 7 - Fringe Image 8
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_7_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_7,
                        EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_8,
                        EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setupWhiteLightLineDataBlock0() throws XrayTesterException
  {
    // Block Image 0 - White Light Image
    sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_0_SETTING,
                            EthernetBasedLightEngineImagePopulationMethodEnum.LINE,
                            EthernetBasedLightEngineImageDisplayTypeEnum.VERTICAL,
                            EthernetBasedLightEngineImageColorEnum.WHITE);
    
    sendWhiteLightLineDataMessage(EthernetBasedLightEngineImageLocationEnum.BLOCK_0);
  }
  
  /**
   * This function setup the image setting needed for image processing.
   * 
   * @author Cheah Lee Herng 
   */
  private void sendImageSettingMessage(EthernetBasedLightEngineRegisterEnum imageSettingEnum, 
                                       EthernetBasedLightEngineImagePopulationMethodEnum methodEnum,
                                       EthernetBasedLightEngineImageDisplayTypeEnum displayTypeEnum,
                                       EthernetBasedLightEngineImageColorEnum colorEnum) throws XrayTesterException
  {
    Assert.expect(imageSettingEnum != null && 
                  (imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_0_SETTING) || 
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_1_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_2_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_3_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_4_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_5_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_6_SETTING) ||
                   imageSettingEnum.equals(EthernetBasedLightEngineRegisterEnum.MODE0_IMAGE_7_SETTING)));
    Assert.expect(methodEnum != null);
    Assert.expect(displayTypeEnum != null);
    Assert.expect(colorEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)imageSettingEnum.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    
    byte command = 0;
    if (methodEnum.equals(EthernetBasedLightEngineImagePopulationMethodEnum.FORMULA))
      command |= 0x08;
    else if (methodEnum.equals(EthernetBasedLightEngineImagePopulationMethodEnum.FULL))
      command |= 0x10;
    else
      command |= 0x00;
    
    if (displayTypeEnum.equals(EthernetBasedLightEngineImageDisplayTypeEnum.HORIZONTAL))
      command |= 0x00;
    else
      command |= 0x20;
    
    command |= getImageColorByte(colorEnum);
    
    byteBuffer.put(command);
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)imageSettingEnum.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void sendFormulaMessage(EthernetBasedLightEngineImageLocationEnum blockEnum,
                                  EthernetBasedLightEngineFringePeriodEnum fringePeriodEnum,
                                  EthernetBasedLightEngineFringePhaseEnum fringePhaseEnum,
                                  EthernetBasedLightEngineFringeIntensityEnum fringeMinIntensityEnum,
                                  EthernetBasedLightEngineFringeIntensityEnum fringeMaxIntensityEnum) throws XrayTesterException
  {
    Assert.expect(blockEnum != null);
    Assert.expect(fringePeriodEnum != null);
    Assert.expect(fringePhaseEnum != null);
    Assert.expect(fringeMinIntensityEnum != null);
    Assert.expect(fringeMaxIntensityEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(BLOCK_WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x42); // 'B'
    byteBuffer.put(getImageLocationByte(blockEnum));
    byteBuffer.put((byte)0x00); // Block Address High Byte
    byteBuffer.put((byte)0x00); // Block Address Low Byte
    byteBuffer.put((byte)0x00); // Block Length High Byte
    byteBuffer.put((byte)0x04); // Block Length Low Byte
    
    byteBuffer.put((byte)fringePeriodEnum.getValue());       // Fringe Period
    byteBuffer.put((byte)fringePhaseEnum.getValue());        // Fringe Phase
    byteBuffer.put((byte)fringeMaxIntensityEnum.getValue()); // Min Fringe Intensity
    byteBuffer.put((byte)fringeMinIntensityEnum.getValue()); // Min Fringe Intensity
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH + 3, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'B') && 
         (readByteArray[2] == getImageLocationByte(blockEnum)) &&
         (readByteArray[3] == (byte)0x00) &&
         (readByteArray[4] == (byte)0x00) &&
         (readByteArray[5] == (byte)0x00) &&
         (readByteArray[6] == (byte)0x04) &&
         (readByteArray[7] == 'O') &&
         (readByteArray[8] == 'K') &&
         (readByteArray[9] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function sends the command to upload pre-defined image into image block.
   * Currently there are 2 images predefined: near-black and near-white image.
   * 
   * @author Cheah Lee Herng 
   */
  public void sendLoadPredefinedImageMessage(EthernetBasedLightEngineImageLocationEnum blockEnum,
                                             EthernetBasedLightEnginePredefinedImageEnum predefinedImageEnum) throws XrayTesterException
  {
    Assert.expect(blockEnum != null);
    Assert.expect(predefinedImageEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(BLOCK_WRITE_PRELOAD_IMAGE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x42); // 'B'
    byteBuffer.put(getImageLocationByte(blockEnum));
    byteBuffer.put((byte)0x00); // Block Address High Byte
    byteBuffer.put((byte)0x00); // Block Address Low Byte 
    byteBuffer.put((byte)0x04); // Block Length High Byte
    byteBuffer.put((byte)0x00); // Block Length Low Byte
    
    int dataCount = 0;
    for (dataCount = 0; dataCount < 1024; dataCount++)
    {
      byteBuffer.put(predefinedImageEnum.getValue());
    }
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH + 3, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'B') && 
         (readByteArray[2] == getImageLocationByte(blockEnum)) &&
         (readByteArray[3] == (byte)0x00) &&
         (readByteArray[4] == (byte)0x00) &&
         (readByteArray[5] == (byte)0x04) &&
         (readByteArray[6] == (byte)0x00) &&
         (readByteArray[7] == 'O') &&
         (readByteArray[8] == 'K') &&
         (readByteArray[9] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function sends the command to load customized line data from file into projector memory block.
   * The initial reason why we want to do this is because the first version of Light Engine firmware
   * has customized 
   * 
   * @author Cheah Lee Herng
   */
  public void sendLineDataMessage(EthernetBasedLightEngineImageLocationEnum blockEnum,
                                  EthernetBasedLightEngineLineDataEnum lineDataEnum,
                                  EthernetBasedLightEngineLineDataTypeEnum lineDataTypeEnum) throws XrayTesterException
  {
    Assert.expect(blockEnum != null);
    Assert.expect(lineDataEnum != null);
    Assert.expect(lineDataTypeEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(BLOCK_WRITE_LINE_DATA_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x42); // 'B'
    byteBuffer.put(getImageLocationByte(blockEnum));
    byteBuffer.put((byte)0x00); // Block Address High Byte
    byteBuffer.put((byte)0x00); // Block Address Low Byte
    byteBuffer.put((byte)0x02); // Block Length High Byte 
    byteBuffer.put((byte)0x4E); // Block Length Low Byte
    
    byte[] lineData = EthernetBasedLightEngineLineDataGenerator.getInstance().generateLineData(lineDataEnum, lineDataTypeEnum);
    if (lineData != null)
    {
      for(int i=0; i < lineData.length; i++)
      {
        byteBuffer.put(lineData[i]);
      }
    }
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH + 3, true);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'B') && 
         (readByteArray[2] == getImageLocationByte(blockEnum)) &&
         (readByteArray[3] == (byte)0x00) &&
         (readByteArray[4] == (byte)0x00) &&
         (readByteArray[5] == (byte)0x02) &&
         (readByteArray[6] == (byte)0x4E) &&
         (readByteArray[7] == 'O') &&
         (readByteArray[8] == 'K') &&
         (readByteArray[9] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void sendWhiteLightLineDataMessage(EthernetBasedLightEngineImageLocationEnum blockEnum) throws XrayTesterException
  {
    Assert.expect(blockEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(BLOCK_WRITE_LINE_DATA_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x42); // 'B'
    byteBuffer.put(getImageLocationByte(blockEnum));
    byteBuffer.put((byte)0x00); // Block Address High Byte
    byteBuffer.put((byte)0x00); // Block Address Low Byte
    byteBuffer.put((byte)0x02); // Block Length High Byte 
    byteBuffer.put((byte)0x4E); // Block Length Low Byte
    
    for(int i=0; i < EthernetBasedLightEngineLineDataGenerator.TOTAL_IMAGE_HEIGHT; i++)
    {
      byteBuffer.put((byte)0xFF);
    }
    
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH + 3, true);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'B') && 
         (readByteArray[2] == getImageLocationByte(blockEnum)) &&
         (readByteArray[3] == (byte)0x00) &&
         (readByteArray[4] == (byte)0x00) &&
         (readByteArray[5] == (byte)0x02) &&
         (readByteArray[6] == (byte)0x4E) &&
         (readByteArray[7] == 'O') &&
         (readByteArray[8] == 'K') &&
         (readByteArray[9] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * This function sends adust current setting message.
   * The available current settings are: 350mA, 495mA, 635mA and 770mA.
   * These settings are defined in EthernetBasedLightEngineCurrentSettingEnum class.
   * 
   * @author Cheah Lee Herng 
   */
  public void sendAdjustCurrentMessage(EthernetBasedLightEngineCurrentSettingEnum currentSettingEnum) throws XrayTesterException
  {
    Assert.expect(currentSettingEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.SETTING_PROJ_LED_CURRENT.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    byteBuffer.put(currentSettingEnum.getValue());
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.SETTING_PROJ_LED_CURRENT.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void turnOnLED() throws XrayTesterException
  {    
    // Enable projector and turn on projector LED
    sendEnableProjectorLEDMessage(true, true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void turnOffLED() throws XrayTesterException
  {
    // Enable projector and turn off projector LED
    sendEnableProjectorLEDMessage(true, false);
    
    // Special handling of white light
    // Reason is that EBLE only has 8 memory slot.
    // We need to reload the line image back into memory for later use.
    if (_displayWhiteLight)
    {
      // Default switch mode to 0
      sendSwitchModeMessage(SwitchModeEnum.MODE_0);
      
      // Re-setup line data block 0
      setupLineDataBlock0();
      
      _displayWhiteLight = false;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void disableProjector() throws XrayTesterException
  {
    // Disable projector and turn off projector LED
    sendEnableProjectorLEDMessage(false, false);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage1() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 0
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage2() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 1
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_1, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage3() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 2    
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_2, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage4() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 3
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_3, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * This function displays coarse fringe pattern
   * 
   * @author Cheah Lee Herng
   */
  public void displayImage5() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 4
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_4, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage6() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 5
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_5, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage7() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 6
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_6, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayImage8() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // Fringe display for image number 7
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_7, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
  }
  
  /**
   * T
   * 
   * @author Cheah Lee Herng
   */
  public void displayWhiteLight() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Setup white light line data
    setupWhiteLightLineDataBlock0();
    
    // Switch to Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // White Image display for image number 0
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    _displayWhiteLight = true;
  }
  
  /**
   * This function will display fringe pattern one and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage1() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display fringe pattern two and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage2() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
        
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_1, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_1, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display fringe pattern three and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage3() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_2, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_2, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display fringe pattern four and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage4() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_3, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_3, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display coarse fringe pattern one and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage5() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_4, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_4, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display coarse fringe pattern one and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage6() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_5, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_5, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display coarse fringe pattern one and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage7() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_6, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_6, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display coarse fringe pattern one and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerImage8() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(true);
    
    // Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_7, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_7, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
  }
  
  /**
   * This function will display white light and send trigger for image capturing.
   * 
   * @author Cheah Lee Herng
   */
  public void triggerWhiteLight() throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Disable projector LED timeout
    sendEnableProjectorLEDTimeoutMessage(false);
    
    // Setup white light line data
    setupWhiteLightLineDataBlock0();
    
    // Switch to Mode 1 for display
    sendSwitchModeMessage(SwitchModeEnum.MODE_1);
    
    // We want to make sure we display fringe pattern image
    // prior to sending trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.DISABLE);        
    
    // Enable projector LED
    turnOnLED();
    
    // Wait for image ready before turning on LED
    waitUntilImageReady();
    
    // Once image is ready, we send the display image again, this time
    // with trigger signal
    sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum.NUMBER_0, 
                            EthernetBasedLightEngineImageColorEnum.WHITE, 
                            EthernetBasedLightEngineCameraTriggerEnum.ENABLE);
    
    _displayWhiteLight = true;
  }
  
  /**
   * This function will adjust the current of the projector.
   * 
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(EthernetBasedLightEngineCurrentSettingEnum currentSettingEnum) throws XrayTesterException
  {
    // Mode 0 for setting image properties
    sendSwitchModeMessage(SwitchModeEnum.MODE_0);
    
    // Set EBLE current setting
    sendAdjustCurrentMessage(currentSettingEnum);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void sendDisplayImageMessage(EthernetBasedLightEngineImageNumberEnum imageNumberEnum,
                                       EthernetBasedLightEngineImageColorEnum colorEnum,
                                       EthernetBasedLightEngineCameraTriggerEnum cameraTriggerEnum) throws XrayTesterException
  {
    Assert.expect(imageNumberEnum != null);
    Assert.expect(colorEnum != null);
    Assert.expect(cameraTriggerEnum != null);
    
    ByteBuffer byteBuffer = getByteBuffer(WRITE_BYTE_BUFFER_LENGTH);
    byteBuffer.put((byte)0x40); // '@'
    byteBuffer.put((byte)0x57); // 'W'
    byteBuffer.put((byte)EthernetBasedLightEngineRegisterEnum.MODE1_IMAGE_SEL_COLOR_OVERRIDE_SEL.getId());  // Start Address
    byteBuffer.put((byte)0x01); // Length
    
    byte command = 0;
    command |= imageNumberEnum.getValue();    // Image Number
    command |= 0x08;                          // Enable Override
    command |= getImageColorByte(colorEnum);  // Color Code
    command |= cameraTriggerEnum.getValue();  // Camera Trigger
    
    byteBuffer.put(command);
    byteBuffer.put(calculateCRC8(byteBuffer));
    byteBuffer.flip();
    
    byteBuffer = sendRequestAndReceiveReplies(byteBuffer, RESPONSE_WRITE_BYTE_BUFFER_LENGTH, false);
    
    // Verify response
    byte[] readByteArray = byteBuffer.array();
    if (((readByteArray[0] == '*') && 
         (readByteArray[1] == 'W') && 
         (readByteArray[2] == (byte)EthernetBasedLightEngineRegisterEnum.MODE1_IMAGE_SEL_COLOR_OVERRIDE_SEL.getId()) &&
         (readByteArray[3] == (byte)0x01) &&
         (readByteArray[4] == 'O') &&
         (readByteArray[5] == 'K') &&
         (readByteArray[6] == calculateCRC8(byteBuffer))) == false)
    {
      handleResponseErrorMessageException(byteBuffer);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private byte getImageLocationByte(EthernetBasedLightEngineImageLocationEnum blockEnum)
  {
    Assert.expect(blockEnum != null);
    
    byte imageBlockByte = 0;
    if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_0))
      imageBlockByte = 0x00;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_1))
      imageBlockByte = 0x01;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_2))
      imageBlockByte = 0x02;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_3))
      imageBlockByte = 0x03;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_4))
      imageBlockByte = 0x04;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_5))
      imageBlockByte = 0x05;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_6))
      imageBlockByte = 0x06;
    else if (blockEnum.equals(EthernetBasedLightEngineImageLocationEnum.BLOCK_7))
      imageBlockByte = 0x07;
    else
      Assert.expect(false, "invalid image location enum");
    
    return imageBlockByte;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private byte getImageColorByte(EthernetBasedLightEngineImageColorEnum colorEnum)
  {
    Assert.expect(colorEnum != null);
    
    byte imageColorByte = 0;
    if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.RED))
      imageColorByte = 0x01;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.GREEN))
      imageColorByte = 0x02;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.DARK_BLUE))
      imageColorByte = 0x03;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.LIGHT_BLUE))
      imageColorByte = 0x04;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.YELLOW))
      imageColorByte = 0x05;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.PINK))
      imageColorByte = 0x06;
    else if (colorEnum.equals(EthernetBasedLightEngineImageColorEnum.BLACK))
      imageColorByte = 0x07;
    else
      imageColorByte = 0x00;
    
    return imageColorByte;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void handleResponseErrorMessageException(ByteBuffer byteBuffer) throws XrayTesterException
  {
    Assert.expect(byteBuffer != null);
    
    EthernetBasedLightEngineHardwareException eblehe = null;
    
    byte[] readByteArray = byteBuffer.array();
    if ((readByteArray[0] == '*') &&
        (readByteArray[1] == 'E') &&
        (readByteArray[3] == calculateCRC8(byteBuffer, 3)))
    {
      int errorCode = (readByteArray[2] & 0xFF);
      if (errorCode == EthernetBasedLightEngineResponseErrorCodeEnum.UNKNOWN_HEADER.getId())
      {
        eblehe = EthernetBasedLightEngineHardwareException.getReceivedUnknownHeaderException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                             _ipAddress, 
                                                                                             _portNumber);
      }
      else if (errorCode == EthernetBasedLightEngineResponseErrorCodeEnum.UNKNOWN_COMMAND.getId())
      {
        eblehe = EthernetBasedLightEngineHardwareException.getReceivedUnknownCommandException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                              _ipAddress, 
                                                                                              _portNumber);
      }
      else if (errorCode == EthernetBasedLightEngineResponseErrorCodeEnum.OUT_OF_REG_BOUNDARY.getId())
      {
        eblehe = EthernetBasedLightEngineHardwareException.getReceivedOutOfRegBoundaryException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                                _ipAddress, 
                                                                                                _portNumber);
      }
      else if (errorCode == EthernetBasedLightEngineResponseErrorCodeEnum.CRC_FAIL.getId())
      {
        eblehe = EthernetBasedLightEngineHardwareException.getReceivedCrcFailException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                       _ipAddress, 
                                                                                       _portNumber);
      }
      else
        eblehe = EthernetBasedLightEngineHardwareException.getReceivedInvalidResponseException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                               _ipAddress, 
                                                                                               _portNumber);
    }
    else
    {
      eblehe = EthernetBasedLightEngineHardwareException.getReceivedInvalidResponseException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                               _ipAddress, 
                                                                                               _portNumber);
    }
    
    Assert.expect(eblehe != null);
    throw eblehe;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void verifyHealthCheckResponse(byte healthCheckByte) throws XrayTesterException
  {        
    EthernetBasedLightEngineHardwareException eblehe = null;
    
    if ((healthCheckByte & 0x08) != 0x08) // SRAM Health Check
    {
      eblehe = EthernetBasedLightEngineHardwareException.getFailedSDRamHealthCheckException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                            _ipAddress, 
                                                                                            _portNumber);
    }
    else if ((healthCheckByte & 0x02) != 0x02) // Projector Health Check
    {
      eblehe = EthernetBasedLightEngineHardwareException.getFailedProjectorHealthCheckException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                                _ipAddress, 
                                                                                                _portNumber);
    }
    else if ((healthCheckByte & 0x04) != 0x04) // EEPROM Health Check
    {
      eblehe = EthernetBasedLightEngineHardwareException.getFailedEepromHealthCheckException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                             _ipAddress, 
                                                                                             _portNumber);
    }
    else if ((healthCheckByte & 0x01) != 0x01) // Health Check Status
    {
      eblehe = EthernetBasedLightEngineHardwareException.getFailedHealthCheckException(_ethernetBasedLightEngineEnum.toString(), 
                                                                                       _ipAddress, 
                                                                                       _portNumber);
    }
    
    if (eblehe != null)
      throw eblehe;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private ByteBuffer getByteBuffer(int sizeInBytes)
  {
    Assert.expect(sizeInBytes > 0);
    ByteBuffer buffer = ByteBuffer.allocate(sizeInBytes);
    buffer.order(_byteOrder);

    return buffer;
  }
  
  /**
   * This function calculates CRC8 checksum needed for the data transportation.
   * Note that Java uses signed byte while .Net uses unsigned byte.
   * 
   * The steps are below:
   *   1) Get the raw byte value
   *   2) AND the raw byte value with 0xFF
   *   3) Assign the AND value into integer
   * 
   * @author Cheah Lee Herng 
   */
  protected byte calculateCRC8(ByteBuffer buffer)
  {
    Assert.expect(buffer != null);
    return calculateCRC8(buffer, buffer.array().length - 1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected byte calculateCRC8(ByteBuffer buffer, int byteArrayLength)
  {
    Assert.expect(buffer != null);
    
    byte[] byteArray = buffer.array();
    
    int temp1 = 0;
    int feedbackBit = 0;
    int feedbackValue = 0;
    int crc8Result = 0;
    
    final int BYTE_MULTIPLIER_1   = (0x01 & 0xFF);
    final int BYTE_MULTIPLIER_2   = (0x8c & 0xFF);
    
    for (int i = 0; i < byteArrayLength; i++)
    {
      temp1 = (byteArray[i] & 0xFF);  // Converting signed byte to unsigned byte
      
      for (int bitCounter = 8; bitCounter > 0; bitCounter--)
      {
        feedbackBit = crc8Result & BYTE_MULTIPLIER_1;
        crc8Result >>= 1;
        feedbackValue = feedbackBit ^ (temp1 & BYTE_MULTIPLIER_1);
        if (feedbackValue != 0)
        {
          crc8Result ^= BYTE_MULTIPLIER_2; 
        }
        temp1 >>= 1;
      }
    }
    
    return (byte)(crc8Result & 0xFF);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  protected boolean isUnitTestModeOn()
  {
    return _unitTestModeOn;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void update(Observable o, Object arg) 
  {
    // Do nothing
  }
  
  public EthernetBasedLightEngineEnum getEthernetBasedLightEngineEnum()
  {
    return _ethernetBasedLightEngineEnum;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getIpAddress()
  {
    return _ipAddress;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getPortNumber()
  {
    return _portNumber;
  }
}
