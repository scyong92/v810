package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineLineDataTypeEnum extends com.axi.util.Enum  
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineLineDataTypeEnum CORRECTED      = new EthernetBasedLightEngineLineDataTypeEnum(++_index);
  public static EthernetBasedLightEngineLineDataTypeEnum NON_CORRECTED  = new EthernetBasedLightEngineLineDataTypeEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineLineDataTypeEnum(int id)
  {
    super(id);
  }
}
