package com.axi.v810.hardware.ethernetBasedLightEngine;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineHardwareException extends HardwareException 
{
  private EthernetBasedLightEngineHardwareExceptionEnum _exceptionType;
  
  /**
   * @author Cheah Lee Herng 
   */
  private EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum exceptionType, String key, List<String> parameters)
  {
    super(new LocalizedString(key, parameters.toArray()));

    Assert.expect(exceptionType != null);
    _exceptionType = exceptionType;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public EthernetBasedLightEngineHardwareExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToCommunicateWithEthernetBasedLightEngineException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_TO_COMMUNICATE_WITH_ETHERNET_BASED_LIGHT_ENGINE,
                                                         "HW_FAILED_TO_COMMUNICATE_WITH_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getReceivedUnknownHeaderException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.RECEIVED_UNKNOWN_HEADER_ERROR,
                                                         "HW_RECEIVED_UNKNOWN_HEADER_FROM_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getReceivedUnknownCommandException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.RECEIVED_UNKNOWN_COMMAND_ERROR,
                                                         "HW_RECEIVED_UNKNOWN_COMMAND_FROM_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getReceivedOutOfRegBoundaryException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.RECEIVED_OUT_OF_REGISTRY_BOUND_ERROR,
                                                         "HW_RECEIVED_OUT_OF_REG_BOUNDARY_FROM_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getReceivedCrcFailException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.RECEIVED_CRC_FAIL_ERROR,
                                                         "HW_RECEIVED_CRC_FAIL_FROM_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getReceivedInvalidResponseException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.RECEIVED_INVALID_RESPONSE_ERROR,
                                                         "HW_RECEIVED_INVALID_RESPONSE_FROM_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedSDRamHealthCheckException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_SDRAM_HEALTH_CHECK,
                                                         "HW_FAILED_SDRAM_HEALTH_CHECK_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedProjectorHealthCheckException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_PROJECTOR_HEALTH_CHECK,
                                                         "HW_FAILED_PROJECTOR_HEALTH_CHECK_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedEepromHealthCheckException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_EEPROM_HEALTH_CHECK,
                                                         "HW_FAILED_EEPROM_HEALTH_CHECK_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedHealthCheckException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));
    
    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_HEALTH_CHECK,
                                                         "HW_FAILED_HEALTH_CHECK_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToTriggerEthernetBasedLightEngineException(PspModuleEnum pspModuleEnum)
  {
    EthernetBasedLightEngine ethernetBasedLightEngine = null;
    if (pspModuleEnum.equals(PspModuleEnum.FRONT))
    {
      ethernetBasedLightEngine = EthernetBasedLightEngineProcessor.getInstance().getEthernetBasedLightEngine(EthernetBasedLightEngineEnum.ENGINE_0);
    }
    else
    {
      ethernetBasedLightEngine = EthernetBasedLightEngineProcessor.getInstance().getEthernetBasedLightEngine(EthernetBasedLightEngineEnum.ENGINE_1);
    }

    String ebleID = ethernetBasedLightEngine.getEthernetBasedLightEngineEnum().toString();
    String ipAddress = ethernetBasedLightEngine.getIpAddress();
    int portNumber = ethernetBasedLightEngine.getPortNumber();
    
    return getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToTriggerEthernetBasedLightEngineException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_TO_TRIGGER_ETHERNET_BASED_LIGHT_ENGINE,
                                                         "HW_FAILED_TO_TRIGGER_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToRetrieveHeightMapException(PspModuleEnum pspModuleEnum)
  {
    EthernetBasedLightEngine ethernetBasedLightEngine = null;
    if (pspModuleEnum.equals(PspModuleEnum.FRONT))
    {
      ethernetBasedLightEngine = EthernetBasedLightEngineProcessor.getInstance().getEthernetBasedLightEngine(EthernetBasedLightEngineEnum.ENGINE_0);
    }
    else
    {
      ethernetBasedLightEngine = EthernetBasedLightEngineProcessor.getInstance().getEthernetBasedLightEngine(EthernetBasedLightEngineEnum.ENGINE_1);
    }

    String ebleID = ethernetBasedLightEngine.getEthernetBasedLightEngineEnum().toString();
    String ipAddress = ethernetBasedLightEngine.getIpAddress();
    int portNumber = ethernetBasedLightEngine.getPortNumber();
    
    return getFailedToRetrieveHeightMapException(ebleID, ipAddress, portNumber);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToRetrieveHeightMapException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_TO_RETRIEVE_HEIGHT_MAP,
                                                         "HW_FAILED_TO_RETRIEVE_HEIGHT_MAP_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static synchronized EthernetBasedLightEngineHardwareException getFailedToAdjustCurrentEthernetBasedLightEngineException(String ebleId, String ipAddress, int portNumber)
  {
    Assert.expect(ebleId != null);
    Assert.expect(ipAddress != null);
    
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(ebleId));
    exceptionParameters.add(String.valueOf(ipAddress));
    exceptionParameters.add(String.valueOf(portNumber));

    return new EthernetBasedLightEngineHardwareException(EthernetBasedLightEngineHardwareExceptionEnum.FAILED_TO_ADJUST_CURRENT,
                                                         "HW_FAILED_TO_ADJUST_CURRENT_ETHERNET_BASED_LIGHT_ENGINE_EXCEPTION_KEY",
                                                         exceptionParameters);
  }
}
