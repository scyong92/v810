package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineImageDisplayTypeEnum extends com.axi.util.Enum  
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineImageDisplayTypeEnum VERTICAL = new EthernetBasedLightEngineImageDisplayTypeEnum(++_index);
  public static EthernetBasedLightEngineImageDisplayTypeEnum HORIZONTAL = new EthernetBasedLightEngineImageDisplayTypeEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineImageDisplayTypeEnum(int id)
  {
    super(id);
  }
}
