package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineImageLocationEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_0 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_1 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_2 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_3 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_4 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_5 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_6 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  public static EthernetBasedLightEngineImageLocationEnum BLOCK_7 = new EthernetBasedLightEngineImageLocationEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineImageLocationEnum(int id)
  {
    super(id);
  }
}
