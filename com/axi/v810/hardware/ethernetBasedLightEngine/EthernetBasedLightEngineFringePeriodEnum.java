package com.axi.v810.hardware.ethernetBasedLightEngine;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineFringePeriodEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  private int _value;
  private static Map<Integer, EthernetBasedLightEngineFringePeriodEnum> _periodToPeriodEnumMap;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    _periodToPeriodEnumMap = new HashMap<Integer, EthernetBasedLightEngineFringePeriodEnum>();
  }
  
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_20 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 20);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_30 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 30);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_40 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 40);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_50 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 50);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_60 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 60);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_70 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 70);
  public static EthernetBasedLightEngineFringePeriodEnum PERIOD_80 = new EthernetBasedLightEngineFringePeriodEnum(++_index, 80);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineFringePeriodEnum(int id, int value)
  {
    super(id);
    _value = value;
    
    _periodToPeriodEnumMap.put(value, this);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static EthernetBasedLightEngineFringePeriodEnum getEnum(int period)
  {
    EthernetBasedLightEngineFringePeriodEnum periodEnum = _periodToPeriodEnumMap.get(period);
    Assert.expect(periodEnum != null);

    return periodEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getValue()
  {
    return _value;
  }
}
