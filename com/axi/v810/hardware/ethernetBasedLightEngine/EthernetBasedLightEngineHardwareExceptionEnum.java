package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.v810.hardware.HardwareExceptionEnum;
import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineHardwareExceptionEnum extends HardwareExceptionEnum 
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_TO_COMMUNICATE_WITH_ETHERNET_BASED_LIGHT_ENGINE = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum RECEIVED_UNKNOWN_HEADER_ERROR = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum RECEIVED_UNKNOWN_COMMAND_ERROR = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum RECEIVED_OUT_OF_REGISTRY_BOUND_ERROR = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum RECEIVED_CRC_FAIL_ERROR = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum RECEIVED_INVALID_RESPONSE_ERROR = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_SDRAM_HEALTH_CHECK = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_PROJECTOR_HEALTH_CHECK = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_EEPROM_HEALTH_CHECK = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_HEALTH_CHECK = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_TO_TRIGGER_ETHERNET_BASED_LIGHT_ENGINE = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_TO_RETRIEVE_HEIGHT_MAP = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  public static EthernetBasedLightEngineHardwareExceptionEnum FAILED_TO_ADJUST_CURRENT = new EthernetBasedLightEngineHardwareExceptionEnum(++_index);
  
  /**
    * @author Cheah Lee Herng
    */
  private EthernetBasedLightEngineHardwareExceptionEnum(int id)
  {
      super(id);
  }
}
