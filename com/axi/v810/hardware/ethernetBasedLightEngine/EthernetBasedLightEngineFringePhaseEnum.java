package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineFringePhaseEnum extends com.axi.util.Enum  
{
  private static int _index = -1;
  private int _value;
  
  public static EthernetBasedLightEngineFringePhaseEnum PHASE_1 = new EthernetBasedLightEngineFringePhaseEnum(++_index, 1);
  public static EthernetBasedLightEngineFringePhaseEnum PHASE_2 = new EthernetBasedLightEngineFringePhaseEnum(++_index, 2);
  public static EthernetBasedLightEngineFringePhaseEnum PHASE_3 = new EthernetBasedLightEngineFringePhaseEnum(++_index, 3);
  public static EthernetBasedLightEngineFringePhaseEnum PHASE_4 = new EthernetBasedLightEngineFringePhaseEnum(++_index, 4);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineFringePhaseEnum(int id, int value)
  {
    super(id);
    _value = value;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getValue()
  {
    return _value;
  }
}
