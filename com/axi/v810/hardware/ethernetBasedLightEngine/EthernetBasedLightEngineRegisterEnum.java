package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineRegisterEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static EthernetBasedLightEngineRegisterEnum FW_VER_L_BYTE = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum FW_VER_H_BYTE = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_1 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_2 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum STATUS_HEALTH_CHECK = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum STATUS_SRAM_HEALTH = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum STATUS_PROJECTOR_STATUS = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum STATUS_SYSTEM_STATUS = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum STATUS_CAMERA_STATUS = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_3 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_4 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_5 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_6 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_7 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_8 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_9 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_MODE_SELECTION = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_PROJ_LED_CURRENT = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_PROJ_EN_DIS_LED_ON_OFF = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_CAM_ACK_EN_DIS = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_CAM_TRIG_PULSE_LATCH_DURATION = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_CAM_ACK_TIMEOUT = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_NO_ACK_RES_TIMEOUT = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_10 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_11 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_12 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_13 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum SETTING_LED_TIMEOUT_EN_DIS = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_15 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_16 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_17 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum WEB_CONTROL = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_0_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_1_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_2_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_3_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_4_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_5_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_6_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE0_IMAGE_7_SETTING = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_18 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_19 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_20 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_21 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_22 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_23 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_24 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum RESERVE_25 = new EthernetBasedLightEngineRegisterEnum(++_index);
  public static EthernetBasedLightEngineRegisterEnum MODE1_IMAGE_SEL_COLOR_OVERRIDE_SEL = new EthernetBasedLightEngineRegisterEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineRegisterEnum(int id)
  {
    super(id);
  }
}
