package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineImagePopulationMethodEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineImagePopulationMethodEnum LINE = new EthernetBasedLightEngineImagePopulationMethodEnum(++_index);
  public static EthernetBasedLightEngineImagePopulationMethodEnum FORMULA = new EthernetBasedLightEngineImagePopulationMethodEnum(++_index);
  public static EthernetBasedLightEngineImagePopulationMethodEnum FULL = new EthernetBasedLightEngineImagePopulationMethodEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineImagePopulationMethodEnum(int id)
  {
    super(id);
  }
}
