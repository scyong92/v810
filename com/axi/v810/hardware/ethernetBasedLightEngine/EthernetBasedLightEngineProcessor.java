package com.axi.v810.hardware.ethernetBasedLightEngine;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineProcessor extends HardwareObject implements HardwareStartupShutdownInt
{
  private static EthernetBasedLightEngineProcessor _instance;
  private Map<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> _ethernetBasedLightEngineMap;

  private HardwareObservable _hardwareObservable;
  private ProgressObservable _progressObservable;
  
  protected boolean _initialized;
  
  // Expect the initialization will take up to 1 minutes.
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 60000;
  
  private static PspSettingEnum _settingEnum;
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineProcessor()
  {
    _hardwareObservable = HardwareObservable.getInstance();
    _progressObservable = ProgressObservable.getInstance();
    
    buildEthernetBasedLightEngineList();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public synchronized static EthernetBasedLightEngineProcessor getInstance()
  {
    if (_instance == null)
    {
      _instance = new EthernetBasedLightEngineProcessor();
    }
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void buildEthernetBasedLightEngineList()
  {
    if (_ethernetBasedLightEngineMap == null)
      _ethernetBasedLightEngineMap = new HashMap<EthernetBasedLightEngineEnum, EthernetBasedLightEngine>();
    
    _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));
    if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH))
    {
      _ethernetBasedLightEngineMap.put(EthernetBasedLightEngineEnum.ENGINE_0, EthernetBasedLightEngine.getInstance(EthernetBasedLightEngineEnum.ENGINE_0));
      _ethernetBasedLightEngineMap.put(EthernetBasedLightEngineEnum.ENGINE_1, EthernetBasedLightEngine.getInstance(EthernetBasedLightEngineEnum.ENGINE_1));
    }
    else if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
    {
      _ethernetBasedLightEngineMap.put(EthernetBasedLightEngineEnum.ENGINE_0, EthernetBasedLightEngine.getInstance(EthernetBasedLightEngineEnum.ENGINE_0));
    }
    else if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
    {
      _ethernetBasedLightEngineMap.put(EthernetBasedLightEngineEnum.ENGINE_1, EthernetBasedLightEngine.getInstance(EthernetBasedLightEngineEnum.ENGINE_1));
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public EthernetBasedLightEngine getEthernetBasedLightEngine(EthernetBasedLightEngineEnum ebleEnum)
  {
    Assert.expect(ebleEnum != null);
    
    EthernetBasedLightEngine requiredEble = _ethernetBasedLightEngineMap.get(ebleEnum);
    if (requiredEble == null)
    {
      _ethernetBasedLightEngineMap.put(ebleEnum, EthernetBasedLightEngine.getInstance(ebleEnum));
      requiredEble = _ethernetBasedLightEngineMap.get(ebleEnum);
    }
    return requiredEble;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void startup() throws XrayTesterException 
  {
    initialize();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void shutdown() throws XrayTesterException 
  {
    stop();
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isStartupRequired() throws XrayTesterException 
  {   
    if (_initialized == false)
      return true;
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void initialize() throws XrayTesterException
  {    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.INITIALIZING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.EBLE_INITIALIZE, _STARTUP_TIME_IN_MILLISECONDS);
    
    try
    {
      int tryCount = 1;
      // Try 2 times (1, 2).
      final int maxTryCount = 2;
      boolean initializationOk = false;
      
      while (tryCount <= maxTryCount && initializationOk == false)
      {
        try
        {
          if (isStartupRequired())
          {
            stopEthernetBasedLightEngineProcessors();
            startEthernetBasedLightEngineProcessors();
          }
          
          initializationOk = true;
        }
        catch(XrayTesterException ex)
        {
          // XCR1733 - No error message prompt out if PSP is disconnected
          // We need to throw the error when we have completed trying and still
          // have error
          if (tryCount >= maxTryCount)
            throw ex;
        }

        tryCount++;
      }
      
      _initialized = initializationOk;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.EBLE_INITIALIZE);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.INITIALIZING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void startEthernetBasedLightEngineProcessors() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.STARTING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        for (Map.Entry<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> entry : _ethernetBasedLightEngineMap.entrySet())
        {
          entry.getValue().initializeSystem();
        }
        _initialized = true;
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.STARTING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void stopEthernetBasedLightEngineProcessors() throws XrayTesterException
  {
    _initialized = false;
    
    if (isSimulationModeOn() == false)
    {
      for (Map.Entry<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> entry : _ethernetBasedLightEngineMap.entrySet())
      {
        entry.getValue().closeConnections();
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void restart() throws XrayTesterException
  {
    stopEthernetBasedLightEngineProcessors();
    startEthernetBasedLightEngineProcessors();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void stop() throws XrayTesterException
  {
    stopEthernetBasedLightEngineProcessors();
  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public void turnOnAllLED() throws XrayTesterException
//  {
//    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_ON_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
//    _hardwareObservable.setEnabled(false);
//    
//    try
//    {
//      if (isSimulationModeOn() == false)
//      {
//        for (Map.Entry<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> entry : _ethernetBasedLightEngineMap.entrySet())
//        {
//          entry.getValue().turnOnLED();
//        }
//      }
//    }
//    finally
//    {
//      _hardwareObservable.setEnabled(true);
//    }
//    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_ON_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
//  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public void turnOffAllLED() throws XrayTesterException
//  {
//    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_OFF_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
//    _hardwareObservable.setEnabled(false);
//    
//    try
//    {
//      if (isSimulationModeOn() == false)
//      {
//        for (Map.Entry<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> entry : _ethernetBasedLightEngineMap.entrySet())
//        {
//          entry.getValue().turnOffLED();
//        }
//      }
//    }
//    finally
//    {
//      _hardwareObservable.setEnabled(true);
//    }
//    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_OFF_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
//  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void turnOnLED(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_ON_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.turnOnLED();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_ON_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void turnOffLED(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_OFF_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.turnOffLED();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TURNING_OFF_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern1(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage1();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern2(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage2();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern3(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage3();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern4(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage4();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern5(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage5();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern6(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage6();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern7(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage7();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayFringePattern8(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayImage8();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void displayWhiteLight(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      if (isSimulationModeOn() == false)
      {
        EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
        eble.displayWhiteLight();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.DISPLAYING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern1(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage1();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern2(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage2();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern3(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage3();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern4(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage4();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern5(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage5();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern6(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage6();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern7(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage7();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerFringePattern8(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerImage8();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void triggerWhiteLight(EthernetBasedLightEngineEnum ebleEnum) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.triggerWhiteLight();
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(EthernetBasedLightEngineEnum ebleEnum, int current) throws XrayTesterException
  {
    Assert.expect(ebleEnum != null);
    
    _hardwareObservable.stateChangedBegin(this, EthernetBasedLightEngineProcessorEventEnum.ADJUST_CURRENT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
    _hardwareObservable.setEnabled(false);
    
    EthernetBasedLightEngine eble = getEthernetBasedLightEngine(ebleEnum);
    try
    {
      if (isSimulationModeOn() == false)
      {
        eble.adjustCurrent(EthernetBasedLightEngineCurrentSettingEnum.getEnum(current));
      }
    }
    catch (XrayTesterException xte)
    {
      String ebleID = eble.getEthernetBasedLightEngineEnum().toString();
      String ipAddress = eble.getIpAddress();
      int portNumber = eble.getPortNumber();

      throw EthernetBasedLightEngineHardwareException.getFailedToAdjustCurrentEthernetBasedLightEngineException(ebleID, ipAddress, portNumber);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, EthernetBasedLightEngineProcessorEventEnum.TRIGGERING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void isConnected() throws XrayTesterException
  {
    try
    {
      //XCR-2616, XCR-2595 - 24/3/2015
      // try to turn off LED, projector is disconnect if cannot turn off.
      if (isSimulationModeOn() == false)
      {
        for (Map.Entry<EthernetBasedLightEngineEnum, EthernetBasedLightEngine> entry : _ethernetBasedLightEngineMap.entrySet())
        {
          entry.getValue().turnOffLED();
        }
      }
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }
    
  }
}
