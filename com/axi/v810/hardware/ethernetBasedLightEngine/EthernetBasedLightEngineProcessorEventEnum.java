package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;
import com.axi.v810.hardware.HardwareEventEnum;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineProcessorEventEnum extends HardwareEventEnum 
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineProcessorEventEnum INITIALIZING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Initialize");
  public static EthernetBasedLightEngineProcessorEventEnum STARTING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Start");
  public static EthernetBasedLightEngineProcessorEventEnum STOPPING_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Stop");
  public static EthernetBasedLightEngineProcessorEventEnum TURNING_ON_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Turn-On ALL LED");
  public static EthernetBasedLightEngineProcessorEventEnum TURNING_OFF_ALL_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Turn-Off ALL LED");
  public static EthernetBasedLightEngineProcessorEventEnum TURNING_ON_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Turn-On Single LED");
  public static EthernetBasedLightEngineProcessorEventEnum TURNING_OFF_SINGLE_PROJECTOR_LED_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Turn-Off Single LED");
  
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern One");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Two");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Three");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Four");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Five");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Six");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Seven");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display Fringe Pattern Eight");
  public static EthernetBasedLightEngineProcessorEventEnum DISPLAYING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Display White Light");
  
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_ONE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern One");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_TWO_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Two");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_THREE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Three");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_FOUR_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Four");  
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_FIVE_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Five");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_SIX_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Six");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_SEVEN_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Seven");
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_FRINGE_PATTERN_EIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger Fringe Pattern Eight");  
  public static EthernetBasedLightEngineProcessorEventEnum TRIGGERING_WHITE_LIGHT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Trigger White Light");
  public static EthernetBasedLightEngineProcessorEventEnum ADJUST_CURRENT_ETHERNET_BASED_LIGHT_ENGINE_PROCESSOR = new EthernetBasedLightEngineProcessorEventEnum(++_index, "Ethernet-Based Light Engine Processor, Adjust Current");
  
  private String _name;

  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineProcessorEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Cheah Lee Herng
   */
  public String toString()
  {
    return _name;
  }
}
