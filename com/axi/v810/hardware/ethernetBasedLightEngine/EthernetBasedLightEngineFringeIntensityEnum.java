package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineFringeIntensityEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  private int _value;
  
  public static EthernetBasedLightEngineFringeIntensityEnum INTENSITY_0 = new EthernetBasedLightEngineFringeIntensityEnum(++_index, 0);
  public static EthernetBasedLightEngineFringeIntensityEnum INTENSITY_255 = new EthernetBasedLightEngineFringeIntensityEnum(++_index, 255);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineFringeIntensityEnum(int id, int value)
  {
    super(id);
    _value = value;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getValue()
  {
    return _value;
  }
}
