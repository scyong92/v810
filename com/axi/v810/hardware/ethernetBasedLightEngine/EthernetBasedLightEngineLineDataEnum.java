package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineLineDataEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  private String _fullPath;
  
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_1 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine1FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_2 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine2FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_3 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine3FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_4 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine4FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_5 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine5FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_6 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine6FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_7 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine7FullPath());
  public static EthernetBasedLightEngineLineDataEnum NON_CORRECTED_LINE_8 = new EthernetBasedLightEngineLineDataEnum(++_index, FileName.getNonCorrectedFringePatternLine8FullPath());
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineLineDataEnum(int id, String fullPath)
  {
    super(id);
    _fullPath = fullPath;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getFullPath()
  {
    Assert.expect(_fullPath != null);
    return _fullPath;
  }
}
