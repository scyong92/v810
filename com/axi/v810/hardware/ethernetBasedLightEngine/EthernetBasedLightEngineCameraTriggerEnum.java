package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineCameraTriggerEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  private byte _value = 0;
  
  public static EthernetBasedLightEngineCameraTriggerEnum DISABLE = new EthernetBasedLightEngineCameraTriggerEnum(++_index, (byte)0x00);
  public static EthernetBasedLightEngineCameraTriggerEnum ENABLE = new EthernetBasedLightEngineCameraTriggerEnum(++_index, (byte)0x80);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineCameraTriggerEnum(int id, byte value)
  {
    super(id);
    _value = value;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public byte getValue()
  {
    return _value;
  }
}
