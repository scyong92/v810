package com.axi.v810.hardware.ethernetBasedLightEngine;


import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineCurrentSettingEnum extends com.axi.util.Enum  
{
  private static int _index = -1;
  private int _value;
  private static Map<Integer, EthernetBasedLightEngineCurrentSettingEnum> _currentToCurrentSettingEnumMap;
  private String _name;
  private int _current;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    _currentToCurrentSettingEnumMap = new HashMap<Integer, EthernetBasedLightEngineCurrentSettingEnum>();
  }
  
  public static EthernetBasedLightEngineCurrentSettingEnum CURRENT_350mA = new EthernetBasedLightEngineCurrentSettingEnum(++_index, 350, (byte)0x00, StringLocalizer.keyToString("PSP_ADJUSTING_BRIGHTNESS_LOW_KEY"));
  public static EthernetBasedLightEngineCurrentSettingEnum CURRENT_495mA = new EthernetBasedLightEngineCurrentSettingEnum(++_index, 495, (byte)0x01, StringLocalizer.keyToString("PSP_ADJUSTING_BRIGHTNESS_MEDIUM_KEY"));
  public static EthernetBasedLightEngineCurrentSettingEnum CURRENT_635mA = new EthernetBasedLightEngineCurrentSettingEnum(++_index, 635, (byte)0x02, StringLocalizer.keyToString("PSP_ADJUSTING_BRIGHTNESS_HIGH_KEY"));
  //public static EthernetBasedLightEngineCurrentSettingEnum CURRENT_770mA = new EthernetBasedLightEngineCurrentSettingEnum(++_index, 770, (byte)0x03, StringLocalizer.keyToString("PSP_ADJUSTING_BRIGHTNESS_SUPER_HIGH_KEY"));
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineCurrentSettingEnum(int id, int current, byte value, String name)
  {
    super(id);
    _value = (value & 0xFF);
    _name = name;
    _current = current;
    _currentToCurrentSettingEnumMap.put(current, this);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public static EthernetBasedLightEngineCurrentSettingEnum getEnum(int current)
  {
    EthernetBasedLightEngineCurrentSettingEnum currentSettingEnum = _currentToCurrentSettingEnumMap.get(current);
    Assert.expect(currentSettingEnum != null);

    return currentSettingEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public byte getValue()
  {
    return (byte)(_value & 0xFF);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String toString()
  {
    return _name;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getCurrent()
  {
    return _current;
  }
}
