package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class handles the generation of customized fringe data line.
 * The line data is a 1D array of bytes. This will be put into projector
 * for customized line data loading.
 * 
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineLineDataGenerator 
{
  public static final int TOTAL_IMAGE_HEIGHT                  = 590;
  private static final int CORRECTED_FINE_IMAGE_COUNTER       = 4;
  private static final int CORRECTED_COARSE_IMAGE_COUNTER     = 16;
  private static final int NON_CORRECTED_FINE_IMAGE_COUNTER   = 16;
  private static final int NON_CORRECTED_COARSE_IMAGE_COUNTER = 48;
  private static final byte GRAY_LEVEL_0        = (byte)0x00;
  private static final byte GRAY_LEVEL_1        = (byte)0x01;
  private static final byte GRAY_LEVEL_4        = (byte)0x04;
  private static final byte GRAY_LEVEL_10       = (byte)0x0A;
  private static final byte GRAY_LEVEL_17       = (byte)0x11;
  private static final byte GRAY_LEVEL_26       = (byte)0x1A;
  private static final byte GRAY_LEVEL_37       = (byte)0x25;
  private static final byte GRAY_LEVEL_50       = (byte)0x32;
  private static final byte GRAY_LEVEL_64       = (byte)0x40;
  private static final byte GRAY_LEVEL_79       = (byte)0x4F;
  private static final byte GRAY_LEVEL_94       = (byte)0x5E;
  private static final byte GRAY_LEVEL_95       = (byte)0x5F;
  private static final byte GRAY_LEVEL_107      = (byte)0x6B;
  private static final byte GRAY_LEVEL_111      = (byte)0x6F;
  private static final byte GRAY_LEVEL_127      = (byte)0x7F;
  private static final byte GRAY_LEVEL_128      = (byte)0x80;
  private static final byte GRAY_LEVEL_139      = (byte)0x8B;
  private static final byte GRAY_LEVEL_144      = (byte)0x90;
  private static final byte GRAY_LEVEL_160      = (byte)0xA0;
  private static final byte GRAY_LEVEL_161      = (byte)0xA1;
  private static final byte GRAY_LEVEL_167      = (byte)0xA7;
  private static final byte GRAY_LEVEL_176      = (byte)0xB0;
  private static final byte GRAY_LEVEL_191      = (byte)0xBF;
  private static final byte GRAY_LEVEL_205      = (byte)0xCD;
  private static final byte GRAY_LEVEL_212      = (byte)0xD4;
  private static final byte GRAY_LEVEL_218      = (byte)0xD0;
  private static final byte GRAY_LEVEL_229      = (byte)0xE5;
  private static final byte GRAY_LEVEL_238      = (byte)0xEE;
  private static final byte GRAY_LEVEL_240      = (byte)0xF0; 
  private static final byte GRAY_LEVEL_245      = (byte)0xF5;
  private static final byte GRAY_LEVEL_249      = (byte)0xF9;
  private static final byte GRAY_LEVEL_251      = (byte)0xFB;
  private static final byte GRAY_LEVEL_254      = (byte)0xFE;
  private static final byte GRAY_LEVEL_255      = (byte)0xFF;
  
  private static EthernetBasedLightEngineLineDataGenerator _instance;
  private FringeLineDataFileReader _fringeLineDataFileReader;
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineLineDataGenerator()
  {
    _fringeLineDataFileReader = FringeLineDataFileReader.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static EthernetBasedLightEngineLineDataGenerator getInstance()
  {
    if (_instance == null)
    {
      _instance = new EthernetBasedLightEngineLineDataGenerator();
    }
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public byte[] generateLineData(EthernetBasedLightEngineLineDataEnum lineDataEnum, 
                                 EthernetBasedLightEngineLineDataTypeEnum lineDataTypeEnum) throws XrayTesterException
  {
    Assert.expect(lineDataEnum != null);
    Assert.expect(lineDataTypeEnum != null);
    
    if (lineDataTypeEnum.equals(EthernetBasedLightEngineLineDataTypeEnum.CORRECTED))
    {
      if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_1))
        return getCorrectedLineOneData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_2))
        return getCorrectedLineTwoData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_3))
        return getCorrectedLineThreeData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_4))
        return getCorrectedLineFourData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_5))
        return getCorrectedLineFiveData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_6))
        return getCorrectedLineSixData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_7))
        return getCorrectedLineSevenData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_8))
        return getCorrectedLineEightData();
      else
        Assert.expect(false, "Invalid line data enum");
    }
    else if (lineDataTypeEnum.equals(EthernetBasedLightEngineLineDataTypeEnum.NON_CORRECTED))
    {
      if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_1))
        return getNonCorrectedLineOneData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_2))
        return getNonCorrectedLineTwoData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_3))
        return getNonCorrectedLineThreeData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_4))
        return getNonCorrectedLineFourData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_5))
        return getNonCorrectedLineFiveData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_6))
        return getNonCorrectedLineSixData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_7))
        return getNonCorrectedLineSevenData();
      else if (lineDataEnum.equals(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_8))
        return getNonCorrectedLineEightData();
      else
        Assert.expect(false, "Invalid line data enum");
    }
    else
      Assert.expect(false, "Invalid line data type enum");
    
    return null;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineOneData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_167;
      lineData[counter+1]   = GRAY_LEVEL_249;
      lineData[counter+2]   = GRAY_LEVEL_167;
      lineData[counter+3]   = GRAY_LEVEL_0;
      
      counter += CORRECTED_FINE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineTwoData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_249;
      lineData[counter+1]   = GRAY_LEVEL_167;
      lineData[counter+2]   = GRAY_LEVEL_0;
      lineData[counter+3]   = GRAY_LEVEL_167;
      
      counter += CORRECTED_FINE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineThreeData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_167;
      lineData[counter+1]   = GRAY_LEVEL_0;
      lineData[counter+2]   = GRAY_LEVEL_167;
      lineData[counter+3]   = GRAY_LEVEL_249;
      
      counter += CORRECTED_FINE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineFourData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_0;
      lineData[counter+1]   = GRAY_LEVEL_167;
      lineData[counter+2]   = GRAY_LEVEL_249;
      lineData[counter+3]   = GRAY_LEVEL_167;
      
      counter += CORRECTED_FINE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineFiveData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_167;
      lineData[counter+1]   = GRAY_LEVEL_191;
      lineData[counter+2]   = GRAY_LEVEL_212;
      lineData[counter+3]   = GRAY_LEVEL_240;
      lineData[counter+4]   = GRAY_LEVEL_249;
      lineData[counter+5]   = GRAY_LEVEL_240;
      lineData[counter+6]   = GRAY_LEVEL_212;
      lineData[counter+7]   = GRAY_LEVEL_191;
      lineData[counter+8]   = GRAY_LEVEL_167;
      lineData[counter+9]   = GRAY_LEVEL_139;
      lineData[counter+10]  = GRAY_LEVEL_107;
      lineData[counter+11]  = GRAY_LEVEL_64;
      lineData[counter+12]  = GRAY_LEVEL_0;
      lineData[counter+13]  = GRAY_LEVEL_64;
      lineData[counter+14]  = GRAY_LEVEL_107;
      lineData[counter+15]  = GRAY_LEVEL_139;
      
      counter += CORRECTED_COARSE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineSixData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_249;
      lineData[counter+1]   = GRAY_LEVEL_240;
      lineData[counter+2]   = GRAY_LEVEL_212;
      lineData[counter+3]   = GRAY_LEVEL_191;
      lineData[counter+4]   = GRAY_LEVEL_167;
      lineData[counter+5]   = GRAY_LEVEL_139;
      lineData[counter+6]   = GRAY_LEVEL_107;
      lineData[counter+7]   = GRAY_LEVEL_64;
      lineData[counter+8]   = GRAY_LEVEL_0;
      lineData[counter+9]   = GRAY_LEVEL_64;
      lineData[counter+10]  = GRAY_LEVEL_107;
      lineData[counter+11]  = GRAY_LEVEL_139;
      lineData[counter+12]  = GRAY_LEVEL_167;
      lineData[counter+13]  = GRAY_LEVEL_191;
      lineData[counter+14]  = GRAY_LEVEL_212;
      lineData[counter+15]  = GRAY_LEVEL_240;
      
      counter += CORRECTED_COARSE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineSevenData()
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_167;
      lineData[counter+1]   = GRAY_LEVEL_139;
      lineData[counter+2]   = GRAY_LEVEL_107;
      lineData[counter+3]   = GRAY_LEVEL_64;
      lineData[counter+4]   = GRAY_LEVEL_0;
      lineData[counter+5]   = GRAY_LEVEL_64;
      lineData[counter+6]   = GRAY_LEVEL_107;
      lineData[counter+7]   = GRAY_LEVEL_139;
      lineData[counter+8]   = GRAY_LEVEL_167;
      lineData[counter+9]   = GRAY_LEVEL_191;
      lineData[counter+10]  = GRAY_LEVEL_212;
      lineData[counter+11]  = GRAY_LEVEL_240;
      lineData[counter+12]  = GRAY_LEVEL_249;
      lineData[counter+13]  = GRAY_LEVEL_240;
      lineData[counter+14]  = GRAY_LEVEL_212;
      lineData[counter+15]  = GRAY_LEVEL_191;
      
      counter += CORRECTED_COARSE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getCorrectedLineEightData() throws XrayTesterException
  {
    byte[] lineData = new byte[TOTAL_IMAGE_HEIGHT];
    int counter = 0;
    
    while(counter < TOTAL_IMAGE_HEIGHT)
    {
      lineData[counter+0]   = GRAY_LEVEL_0;
      lineData[counter+1]   = GRAY_LEVEL_64;
      lineData[counter+2]   = GRAY_LEVEL_107;
      lineData[counter+3]   = GRAY_LEVEL_139;
      lineData[counter+4]   = GRAY_LEVEL_167;
      lineData[counter+5]   = GRAY_LEVEL_191;
      lineData[counter+6]   = GRAY_LEVEL_212;
      lineData[counter+7]   = GRAY_LEVEL_240;
      lineData[counter+8]   = GRAY_LEVEL_249;
      lineData[counter+9]   = GRAY_LEVEL_240;
      lineData[counter+10]  = GRAY_LEVEL_212;
      lineData[counter+11]  = GRAY_LEVEL_191;
      lineData[counter+12]  = GRAY_LEVEL_167;
      lineData[counter+13]  = GRAY_LEVEL_139;
      lineData[counter+14]  = GRAY_LEVEL_107;
      lineData[counter+15]  = GRAY_LEVEL_64;
      
      counter += CORRECTED_COARSE_IMAGE_COUNTER;
    }
    return lineData;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineOneData() throws XrayTesterException
  {    
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_1, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineTwoData() throws XrayTesterException
  {
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_2, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineThreeData() throws XrayTesterException
  {    
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_3, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineFourData() throws XrayTesterException
  {    
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_4, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineFiveData() throws XrayTesterException
  {
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_5, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineSixData() throws XrayTesterException
  {
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_6, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineSevenData() throws XrayTesterException
  {
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_7, TOTAL_IMAGE_HEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] getNonCorrectedLineEightData() throws XrayTesterException
  {
    return _fringeLineDataFileReader.readDataFile(EthernetBasedLightEngineLineDataEnum.NON_CORRECTED_LINE_8, TOTAL_IMAGE_HEIGHT);
  }
}
