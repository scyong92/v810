package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineResponseErrorCodeEnum extends com.axi.util.Enum 
{
  private static int _index = -1;

  public static EthernetBasedLightEngineResponseErrorCodeEnum UNKNOWN_HEADER = new EthernetBasedLightEngineResponseErrorCodeEnum(++_index);
  public static EthernetBasedLightEngineResponseErrorCodeEnum UNKNOWN_COMMAND = new EthernetBasedLightEngineResponseErrorCodeEnum(++_index);
  public static EthernetBasedLightEngineResponseErrorCodeEnum OUT_OF_REG_BOUNDARY = new EthernetBasedLightEngineResponseErrorCodeEnum(++_index);
  public static EthernetBasedLightEngineResponseErrorCodeEnum CRC_FAIL = new EthernetBasedLightEngineResponseErrorCodeEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineResponseErrorCodeEnum(int id)
  {
    super(id);
  }
}
