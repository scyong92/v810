package com.axi.v810.hardware.ethernetBasedLightEngine;

import com.axi.util.*;

import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineEnum extends com.axi.util.Enum
{
  private static List<EthernetBasedLightEngineEnum> _listOfEnums = new ArrayList<EthernetBasedLightEngineEnum>();

  private static int _index = -1;

  public static EthernetBasedLightEngineEnum ENGINE_0 = new EthernetBasedLightEngineEnum(++_index);
  public static EthernetBasedLightEngineEnum ENGINE_1 = new EthernetBasedLightEngineEnum(++_index);
  
  /**
   * @author Cheah Lee Herng 
   */
  private EthernetBasedLightEngineEnum(int id)
  {
    super(id);

    _listOfEnums.add(this);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String toString()
  {
    return "" + super.getId();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static List<EthernetBasedLightEngineEnum> getEnumList()
  {
    return _listOfEnums;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getId()
  {
    Assert.expect(false, "getId() is used!");
    return super.getId();
  }
}
