package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEngineImageColorEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  
  public static EthernetBasedLightEngineImageColorEnum WHITE = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum RED = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum GREEN = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum DARK_BLUE = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum LIGHT_BLUE = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum YELLOW = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum PINK = new EthernetBasedLightEngineImageColorEnum(++_index);
  public static EthernetBasedLightEngineImageColorEnum BLACK = new EthernetBasedLightEngineImageColorEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEngineImageColorEnum(int id)
  {
    super(id);
  }
}
