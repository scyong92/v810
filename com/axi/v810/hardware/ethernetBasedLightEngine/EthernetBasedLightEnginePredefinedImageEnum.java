package com.axi.v810.hardware.ethernetBasedLightEngine;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEnginePredefinedImageEnum extends com.axi.util.Enum  
{
  private static int _index = -1;
  private int _value;
  
  public static EthernetBasedLightEnginePredefinedImageEnum IMAGE_1 = new EthernetBasedLightEnginePredefinedImageEnum(++_index, (byte)0x11);
  public static EthernetBasedLightEnginePredefinedImageEnum IMAGE_2 = new EthernetBasedLightEnginePredefinedImageEnum(++_index, (byte)0xEE);
  
  /**
   * @author Cheah Lee Herng
   */
  private EthernetBasedLightEnginePredefinedImageEnum(int id, byte value)
  {
    super(id);
    _value = (value & 0xFF);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public byte getValue()
  {
    return (byte)(_value & 0xFF);
  }
}