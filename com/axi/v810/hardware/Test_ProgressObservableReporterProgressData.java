package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Greg Loring
 */
public class Test_ProgressObservableReporterProgressData extends UnitTest
{

  /**
   * @author Greg Loring
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProgressObservableReporterProgressData());
  }

  /**
   * @author Greg Loring
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      ProgressObservableReporterProgressData progressData
          = new ProgressObservableReporterProgressData();

      Expect.expect(progressData.getEstimatedTimeOfCompletion() == Long.MAX_VALUE);
      Expect.expect(progressData.getPercentDone() == 0);

      progressData.reportProgress(10);
      Expect.expect(progressData.getEstimatedTimeOfCompletion() == Long.MAX_VALUE);
      Expect.expect(progressData.getPercentDone() == 10);

      // "dwell" and "recent" are same
      Thread.sleep(10000); // 10s: want enuf time to overwhelm swapping times
      Expect.expect(progressData.getEstimatedTimeOfCompletion() == Long.MAX_VALUE);
      Expect.expect(progressData.getPercentDone() == 10);
      progressData.reportProgress(20);
      double expectedTime = System.currentTimeMillis() + (80. / (10.0 / 10000.0));
      long actualTime = progressData.getEstimatedTimeOfCompletion();
      Expect.expect(MathUtil.fuzzyEquals(expectedTime, actualTime, 1000.0));
      Expect.expect(progressData.getPercentDone() == 20);
      long lastActualTime = actualTime;

      // "dwell" and "recent" differ
      Thread.sleep(10000); // 10s: want enuf time to overwhelm swapping times
      Expect.expect(progressData.getEstimatedTimeOfCompletion() == lastActualTime);
      Expect.expect(progressData.getPercentDone() == 20);
      progressData.reportProgress(80);
      expectedTime = System.currentTimeMillis() + (20. / (((60.0/10000.0) + (70.0/20000.0))/2.0));
      actualTime = progressData.getEstimatedTimeOfCompletion();
      Expect.expect(MathUtil.fuzzyEquals(expectedTime, actualTime, 1000.0));
      Expect.expect(progressData.getPercentDone() == 80);

      // regress
      progressData.reportProgress(70);
      Expect.expect(progressData.getEstimatedTimeOfCompletion() == Long.MAX_VALUE);
      Expect.expect(progressData.getPercentDone() == 70);

      // done: reporting 100% done should cause the estimated time of completion to set
      //  to now, even if there is no time delta from preceding progress report(s)
      progressData.reportProgress(100);
      long now = System.currentTimeMillis();
      actualTime = progressData.getEstimatedTimeOfCompletion();
      Expect.expect(MathUtil.fuzzyEquals(now, actualTime, 1000.0), "now=" + now + ", but actualTime=" + actualTime);
      lastActualTime = actualTime;

      // ... and done again
      progressData.reportProgress(100);
      actualTime = progressData.getEstimatedTimeOfCompletion();
      Expect.expect(actualTime == lastActualTime);
    }
    catch(Exception x)
    {
      x.printStackTrace();
    }
  }

}
