package com.axi.v810.hardware;

import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class MotionControl extends HardwareObject implements HardwareStartupShutdownInt
{
  private static RemoteMotionControlInterface _rmci;
  private RemoteMotionServerExecution _remoteMotionServer;
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 30000;
  private static final int _REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS = 500;
  private NativeHardwareConfiguration _nativeHardwareConfiguration;
  private MotionControllerTypeEnum _motionControllerModel;
  private MotionControllerIdEnum _motionControllerId;
  private static Map<MotionControllerIdEnum, MotionControl> _motionControllerIdToInstanceMap;
  private static Map<MotionControllerIdEnum, HardwareConfigEnum> _motionControllerIdToHardwareConfigEnumControllerModelMap;
  private static HardwareTaskEngine _hardwareTaskEngine;
  private List<HardwareConfigurationDescriptionInt> _configDescription;
  private static ProgressObservable _progressObservable;
  private static HardwareObservable _hardwareObservable;
  private boolean _debug = Config.isDeveloperDebugModeOn();
  //Xray tube crash preventive action
  private static boolean _checkForPointToPointMoveCompletion = false;

  /**
   * @author Greg Esparza
   */
  static
  {
    _motionControllerIdToInstanceMap = new HashMap<MotionControllerIdEnum, MotionControl>();
    setMotionControllerIdToHardwareConfigEnumModelMap();
  }

  /**
   * @author Greg Esparza
   */
  private static void setMotionControllerIdToHardwareConfigEnumModelMap()
  {
    _motionControllerIdToHardwareConfigEnumControllerModelMap = new HashMap<MotionControllerIdEnum, HardwareConfigEnum>();
    _motionControllerIdToHardwareConfigEnumControllerModelMap.put(MotionControllerIdEnum.MOTION_CONTROLLER_0,
            HardwareConfigEnum.MOTION_CONTROLLER_0_MODEL);
  }

  /**
   * @author Greg Esparza
   */
  private void setMotionControllerModelType(MotionControllerIdEnum motionControllerId)
  {
    HardwareConfigEnum configEnum = _motionControllerIdToHardwareConfigEnumControllerModelMap.get(motionControllerId);
    Assert.expect(configEnum != null);

    Config config = Config.getInstance();
    _motionControllerModel = MotionControllerTypeEnum.getEnum(config.getStringValue(configEnum));
  }

  /**
   * @author Greg Esparza
   */
  public static synchronized MotionControl getInstance(MotionControllerIdEnum motionControllerId)
  {
    Assert.expect(motionControllerId != null);

    MotionControl instance = null;

    if (_motionControllerIdToInstanceMap.containsKey(motionControllerId))
    {
      instance = _motionControllerIdToInstanceMap.get(motionControllerId);
    }
    else
    {
      instance = new MotionControl(motionControllerId);
      _motionControllerIdToInstanceMap.put(motionControllerId, instance);
    }

    Assert.expect(instance != null);
    return instance;
  }

  /**
   * @author Greg Esparza
   */
  private MotionControl(MotionControllerIdEnum motionControllerId)
  {
    _nativeHardwareConfiguration = NativeHardwareConfiguration.getInstance();
    _motionControllerId = motionControllerId;
    setMotionControllerModelType(motionControllerId);
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _configDescription = new ArrayList<HardwareConfigurationDescriptionInt>();
    _progressObservable = ProgressObservable.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();

    _remoteMotionServer = RemoteMotionServerExecution.getInstance();
  }

  /**
   * @author Greg Esparza
   */
  private void setConfigurationDescription() throws XrayTesterException
  {
    ArrayList<String> keys = new ArrayList<String>();
    ArrayList<String> parameters = new ArrayList<String>();
    final String PARAMETER_GROUP_SEPARATOR = "paramSeparator";

    try
    {
      _configDescription = _rmci.remoteGetConfigurationDescription(_motionControllerId.getId(), PARAMETER_GROUP_SEPARATOR, keys, parameters);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _configDescription = _rmci.remoteGetConfigurationDescription(_motionControllerId.getId(), PARAMETER_GROUP_SEPARATOR, keys, parameters);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setConfigurationDescription re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  int getStartupTimeInMilliseconds()
  {
    return _STARTUP_TIME_IN_MILLISECONDS;
  }

  /**
   * @author Greg Esparza
   */
  private synchronized void initialize() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, MotionControlEventEnum.INITIALIZE);
    _hardwareObservable.setEnabled(false);
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.MOTION_CONTROL_INITIALIZE, _STARTUP_TIME_IN_MILLISECONDS);

    // XCR-3604 Unit Test Phase 2
    try
    {
      if (_remoteMotionServer.isStartupRequired())
      {
        _remoteMotionServer.startup();
      }
      
      if (UnitTest.unitTesting() == false)
      {
        try
        {
          _nativeHardwareConfiguration.setConfigurationParameters();
        }
        catch (RemoteException re)
        {
          MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(re);
          _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
        }

        try
        {
          _rmci.remoteInitialize(_motionControllerModel.getId(), _motionControllerId.getId());
          setConfigurationDescription();
        }
        catch (NativeHardwareException nhe)
        {
          MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
          _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
        }
        catch (RemoteException re)
        {
          // try again 1 times
          // wei chin
          try
          {
            waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
            _rmci.remoteInitialize(_motionControllerModel.getId(), _motionControllerId.getId());
            setConfigurationDescription();
          }
          catch (NativeHardwareException nhe)
          {
            MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
            _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
          }
          catch (RemoteException re2)
          {
            MotionControlHardwareException mche = new MotionControlHardwareException(re);
            if (_debug)
            {
              System.out.println("initialize re2:" + re2.getMessage());
            }
            _hardwareTaskEngine.throwHardwareException(mche);
          }
        }
        catch (Exception e)
        {
          if (_debug)
          {
            System.out.println("e:" + e.getMessage());
          }
          MotionControlHardwareException mche = new MotionControlHardwareException(e);
          _hardwareTaskEngine.throwHardwareException(mche);
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.MOTION_CONTROL_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, MotionControlEventEnum.INITIALIZE);
  }

  /**
   * @author Greg Esparza
   */
  void enable(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remoteEnable(_motionControllerId.getId(), motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteEnable(_motionControllerId.getId(), motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("enable re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void disable(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remoteDisable(_motionControllerId.getId(), motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteDisable(_motionControllerId.getId(), motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("disable re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void home(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      setPointToPointMotionProfile(motionAxisToMove, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
      _rmci.remoteHome(_motionControllerId.getId(), motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      
      String[] exceptionParameters = new String[]
        {motionControlHardwareException.getMessage()};

      Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_STG_HWL_HOME_EXCEPTION, exceptionParameters);
      
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        setPointToPointMotionProfile(motionAxisToMove, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
        _rmci.remoteHome(_motionControllerId.getId(), motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        
        String[] exceptionParameters = new String[]
        {motionControlHardwareException.getMessage()};

        Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_STG_HWL_HOME_EXCEPTION, exceptionParameters);
        
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("home re2:" + re2.getMessage());
        }
        
        String[] exceptionParameters = new String[]
        {re2.getMessage()};

        Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_STG_HWL_HOME_EXCEPTION, exceptionParameters);
        
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      
      String[] exceptionParameters = new String[]
        {e.getMessage()};

      Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_STG_HWL_HOME_EXCEPTION, exceptionParameters);
      
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void setPointToPointMotionProfile(MotionAxisToMoveEnum motionAxisToMove, MotionProfile motionProfile) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);
    Assert.expect(motionProfile != null);
    Assert.expect(motionProfile.getMotionProfileType().equals(MotionProfileTypeEnum.POINT_TO_POINT));

    try
    {
      if (motionAxisToMove.equals(MotionAxisToMoveEnum.XY_COORDINATED_AXIS))
      {
        _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                MotionAxisToMoveEnum.X_INDEPENDENT_AXIS.getId(),
                motionProfile.getId());

        _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS.getId(),
                motionProfile.getId());
      }
      else
      {
        _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                motionAxisToMove.getId(),
                motionProfile.getId());
      }
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        if (motionAxisToMove.equals(MotionAxisToMoveEnum.XY_COORDINATED_AXIS))
        {
          _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                  MotionAxisToMoveEnum.X_INDEPENDENT_AXIS.getId(),
                  motionProfile.getId());

          _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                  MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS.getId(),
                  motionProfile.getId());
        }
        else
        {
          _rmci.remoteSetPointToPointMotionProfile(_motionControllerId.getId(),
                  motionAxisToMove.getId(),
                  motionProfile.getId());
        }
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setPointToPointMotionProfile re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  void setScanMotionProfile(MotionProfile motionProfile) throws XrayTesterException
  {
    Assert.expect(motionProfile != null);
    Assert.expect(motionProfile.getMotionProfileType().equals(MotionProfileTypeEnum.SCAN));

    try
    {
      _rmci.remoteSetScanMotionProfile(_motionControllerId.getId(),
              MotionAxisToMoveEnum.XY_COORDINATED_AXIS.getId(),
              motionProfile.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteSetScanMotionProfile(_motionControllerId.getId(),
                MotionAxisToMoveEnum.XY_COORDINATED_AXIS.getId(),
                motionProfile.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setScanMotionProfile re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Anthony Fong
   */
  void checkPointToPointMoveCompletion(int targetX, int targetY) throws XrayTesterException
  {
    //Check point to poin motion completion
    int actualX = 0;
    int actualY = 0;

    try
    {
      // Check to ensure motion is completed before proceeding.
      long timeToWaitInMilliSeconds = 5000;
      long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
      boolean continueCheck = true;
      boolean timeUpdate = false;
      while (continueCheck && (deadLineInMilliSeconds > System.currentTimeMillis()))
      {
        actualX = Math.abs(targetX - PanelPositioner.getInstance().getXaxisActualPositionInNanometers());
        actualY = Math.abs(targetY - PanelPositioner.getInstance().getYaxisActualPositionInNanometers());

        if (timeUpdate == false)
        {
          // adjust timeout to 1s if distance within 5 mm for optimization
          if (actualX < 5000000 && actualY < 5000000)
          {
            timeUpdate = true;
            deadLineInMilliSeconds = 1000 + System.currentTimeMillis();
          }
        }

        // Normally when the stage move, it will deviate around a few um,
        // we assume it is in move position when the reading of stage position is less than 20um.
        if (actualX < 20000 && actualY < 20000)
        {
          continueCheck = false;
        }
        Thread.sleep(100);
      }

      if (continueCheck == true)
      {
        if (timeUpdate == false)
        {
          System.out.println("checkPointToPointMoveCompletion timeout after 5 s ");
        }
        else
        {
          System.out.println("checkPointToPointMoveCompletion timeout after 1 s ");
        }
      }
    }
    catch (XrayTesterException xte)
    {
      // do nothing
    }
    catch (InterruptedException xte)
    {
      // do nothing
    }
  }

  /**
   * Xray tube crash preventive action
   * @author Anthony Fong
   */
  public boolean getCheckForPointToPointMoveCompletion()
  {
    return _checkForPointToPointMoveCompletion;
  }

  /**
   * Xray tube crash preventive action
   * @author Anthony Fong
   */
  public void setCheckForPointToPointMoveCompletion(boolean checkForPointToPointMoveCompletion)
  {
    _checkForPointToPointMoveCompletion = checkForPointToPointMoveCompletion;
  }

  /**
   * @author Greg Esparza
   */
  void pointToPointMove(MotionAxisPositionInt motionAxisPosition, MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisPosition != null);
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remotePointToPointMove(_motionControllerId.getId(),
              motionAxisToMove.getId(),
              motionAxisPosition.getXaxisPositionInNanometers(),
              motionAxisPosition.getYaxisPositionInNanometers(),
              motionAxisPosition.getRailWidthAxisPositionInNanometers());

      //Xray tube crash preventive action
      if (_checkForPointToPointMoveCompletion == true)
      {
        checkPointToPointMoveCompletion(motionAxisPosition.getXaxisPositionInNanometers(), motionAxisPosition.getYaxisPositionInNanometers());
      }
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remotePointToPointMove(_motionControllerId.getId(),
                motionAxisToMove.getId(),
                motionAxisPosition.getXaxisPositionInNanometers(),
                motionAxisPosition.getYaxisPositionInNanometers(),
                motionAxisPosition.getRailWidthAxisPositionInNanometers());

        //Xray tube crash preventive action
        if (_checkForPointToPointMoveCompletion == true)
        {
          checkPointToPointMoveCompletion(motionAxisPosition.getXaxisPositionInNanometers(), motionAxisPosition.getYaxisPositionInNanometers());
        }
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("pointToPointMove re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * Set up the entire scan path
   * Swee Yee Wong - include starting pulse for scanpass adjustment
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @param scanPath is a List of ScanPass objects; each describing one scan pass and corresponding scan step.
   */
  void enableScanPath(List<ScanPass> scanPath, int distanceInNanometersPerPulse, int numberOfStartPulses) throws XrayTesterException
  {
    Assert.expect(scanPath != null);

    // We need the array to be 4x the scan path list size because each scan path will contain
    // a start and end x,y pair.
    int[] scanPathArray = new int[4 * scanPath.size()];
    int arrayIndex = -1;
    for (ScanPass scanPass : scanPath)
    {
      scanPathArray[++arrayIndex] = scanPass.getStartPointInNanoMeters().getXInNanometers();
      scanPathArray[++arrayIndex] = scanPass.getStartPointInNanoMeters().getYInNanometers();
      scanPathArray[++arrayIndex] = scanPass.getEndPointInNanoMeters().getXInNanometers();
      scanPathArray[++arrayIndex] = scanPass.getEndPointInNanoMeters().getYInNanometers();
    }

    try
    {
      _rmci.remoteEnableScanPath(_motionControllerId.getId(), scanPathArray, distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteEnableScanPath(_motionControllerId.getId(), scanPathArray, distanceInNanometersPerPulse, numberOfStartPulses);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("enableScanPath re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void runNextScanPass() throws XrayTesterException
  {
    try
    {
      _rmci.remoteRunNextScanPass(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteRunNextScanPass(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re);
        if (_debug)
        {
          System.out.println("runNextScanPass re:" + re.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void pauseScanPath() throws XrayTesterException
  {
    try
    {
      _rmci.remotePauseScanPath(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remotePauseScanPath(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("pauseScanPath re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  void disableScanPath() throws XrayTesterException
  {
    try
    {
      _rmci.remoteDisableScanPath(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteDisableScanPath(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("disableScanPath re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  int getScanPathExecutionTimeInMilliseconds() throws XrayTesterException
  {
    int timeInMilliseconds = 0;

    try
    {
      timeInMilliseconds = _rmci.remoteGetScanPathExecutionTimeInMilliseconds(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        timeInMilliseconds = _rmci.remoteGetScanPathExecutionTimeInMilliseconds(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getScanPathExecutionTimeInMilliseconds re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return timeInMilliseconds;
  }

  /**
   * @author Greg Esparza
   */
  void setPositionPulse(int distanceInNanometersPerPulse, int numberOfStartPulses) throws XrayTesterException
  {
    try
    {
      _rmci.remoteSetPositionPulse(_motionControllerId.getId(),
              distanceInNanometersPerPulse,
              numberOfStartPulses);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteSetPositionPulse(_motionControllerId.getId(),
                distanceInNanometersPerPulse,
                numberOfStartPulses);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setPositionPulse re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  int getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int positionLimitInNanometers = 0;

    try
    {
      positionLimitInNanometers = _rmci.remoteGetMinimumPositionLimitInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        positionLimitInNanometers = _rmci.remoteGetMinimumPositionLimitInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMinimumPositionLimitInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return positionLimitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  int getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int positionLimitInNanometers = 0;

    try
    {
      positionLimitInNanometers = _rmci.remoteGetMaximumPositionLimitInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        positionLimitInNanometers = _rmci.remoteGetMaximumPositionLimitInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMaximumPositionLimitInNanometers re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return positionLimitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  void stop(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remoteStop(_motionControllerId.getId(), motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteStop(_motionControllerId.getId(), motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("stop re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  int getMinimumScanPassLengthInNanometers() throws XrayTesterException
  {
    int lengthInNanometers = 0;

    try
    {
      lengthInNanometers = _rmci.remoteGetMinimumScanPassLengthInNanometers(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        lengthInNanometers = _rmci.remoteGetMinimumScanPassLengthInNanometers(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMinimumScanPassLengthInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return lengthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  int getMaximumScanPassLengthInNanometers(int yAxisScanPassStartPositionInNanometers,
          ScanPassDirectionEnum scanPassDirection) throws XrayTesterException
  {
    Assert.expect(scanPassDirection != null);

    int lengthInNanometers = 0;

    try
    {
      lengthInNanometers = _rmci.remoteGetMaximumScanPassLengthInNanometers(_motionControllerId.getId(),
              yAxisScanPassStartPositionInNanometers,
              scanPassDirection.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        lengthInNanometers = _rmci.remoteGetMaximumScanPassLengthInNanometers(_motionControllerId.getId(),
                yAxisScanPassStartPositionInNanometers,
                scanPassDirection.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMaximumScanPassLengthInNanometers re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return lengthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  int getActualPositionInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int actualPositionInNanometers = 0;

    try
    {
      actualPositionInNanometers = _rmci.remoteGetActualPositionInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        actualPositionInNanometers = _rmci.remoteGetActualPositionInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getActualPositionInNanometers re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }

    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return actualPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  boolean isScanPathDone() throws XrayTesterException
  {
    boolean scanPathDone = false;

    try
    {
      scanPathDone = _rmci.remoteIsScanPathDone(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        scanPathDone = _rmci.remoteIsScanPathDone(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("isScanPathDone re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return scanPathDone;
  }

  /**
   * @author Greg Esparza
   */
  boolean isScanPassDone() throws XrayTesterException
  {
    boolean scanPassDone = false;

    try
    {
      scanPassDone = _rmci.remoteIsScanPassDone(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        scanPassDone = _rmci.remoteIsScanPassDone(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("isScanPassDone re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return scanPassDone;
  }

  /**
   * @author Greg Esparza
   */
  boolean isHomeSensorClear(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    boolean homeSensorIsClear = false;

    try
    {
      homeSensorIsClear = _rmci.remoteIsHomeSensorClear(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        homeSensorIsClear = _rmci.remoteIsHomeSensorClear(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("isHomeSensorClear re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return homeSensorIsClear;
  }

  /**
   * @author Greg Esparza
   */
  public List<HardwareConfigurationDescriptionInt> getConfigurationDescription()
  {
    Assert.expect(_configDescription != null);

    return _configDescription;
  }

  /**
   * @author Greg Esparza
   */
  boolean isAxisEnabled(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    boolean axisEnabled = false;

    try
    {
      axisEnabled = _rmci.remoteIsAxisEnabled(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        axisEnabled = _rmci.remoteIsAxisEnabled(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("isAxisEnabled re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
    return axisEnabled;
  }

  /**
   * @author Greg Esparza
   */
  void setHysteresisOffsetInNanometers(MotionAxisToMoveEnum motionAxisToMove, int hysteresisOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remoteSetHysteresisOffsetInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId(),
              hysteresisOffsetInNanometers);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteSetHysteresisOffsetInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId(),
                hysteresisOffsetInNanometers);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setHysteresisOffsetInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  int getForwardDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int hysteresisOffsetInNanometers = 0;

    try
    {
      hysteresisOffsetInNanometers = _rmci.remoteGetForwardDirectionHysteresisOffsetInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        hysteresisOffsetInNanometers = _rmci.remoteGetForwardDirectionHysteresisOffsetInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re);
        if (_debug)
        {
          System.out.println("getForwardDirectionHysteresisOffsetInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return hysteresisOffsetInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  int getReverseDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int hysteresisOffsetInNanometers = 0;

    try
    {
      hysteresisOffsetInNanometers = _rmci.remoteGetReverseDirectionHysteresisOffsetInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        hysteresisOffsetInNanometers = _rmci.remoteGetReverseDirectionHysteresisOffsetInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getReverseDirectionHysteresisOffsetInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return hysteresisOffsetInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  int getValidPositionPulseRateInNanometersPerPulse(int requestedNanometersPerPulse) throws XrayTesterException
  {
    int validPositionPulseRateInNanometers = 0;

    try
    {
      validPositionPulseRateInNanometers = _rmci.remoteGetValidPositionPulseRateInNanometersPerPulse(_motionControllerId.getId(),
              requestedNanometersPerPulse);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        validPositionPulseRateInNanometers = _rmci.remoteGetValidPositionPulseRateInNanometersPerPulse(_motionControllerId.getId(),
                requestedNanometersPerPulse);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getValidPositionPulseRateInNanometersPerPulse re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return validPositionPulseRateInNanometers;
  }

  /**
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  private boolean isInitialized() throws XrayTesterException
  {
    boolean initialized = false;

    if (_rmci == null)
    {
      return initialized;
    }

    try
    {
      initialized = _rmci.remoteIsInitialized(_motionControllerModel.getId(), _motionControllerId.getId());
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        initialized = _rmci.remoteIsInitialized(_motionControllerModel.getId(), _motionControllerId.getId());
      }
      catch (RemoteException re2)
      {
        return initialized;
//        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
//        if(_debug)
//          System.out.println("isInitialized re2:" + re2.getMessage());
//        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return initialized;
  }

  /**
   * @author Greg Esparza
   */
  public int getMinimumScanStepWidthInNanometers() throws XrayTesterException
  {
    int widthInNanometers = 0;

    try
    {
      widthInNanometers = _rmci.remoteGetMinimumScanStepWidthInNanometers(_motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        widthInNanometers = _rmci.remoteGetMinimumScanStepWidthInNanometers(_motionControllerId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMinimumScanStepWidthInNanometers re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return widthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getMaximumScanStepWidthInNanometers(int xAxisScanStepStartPositionInNanometers, ScanStepDirectionEnum scanStepDirection) throws XrayTesterException
  {
    Assert.expect(scanStepDirection != null);

    int widthInNanometers = 0;

    try
    {
      widthInNanometers = _rmci.remoteGetMaximumScanStepWidthInNanometers(_motionControllerId.getId(),
              xAxisScanStepStartPositionInNanometers,
              scanStepDirection.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        widthInNanometers = _rmci.remoteGetMaximumScanStepWidthInNanometers(_motionControllerId.getId(),
                xAxisScanStepStartPositionInNanometers,
                scanStepDirection.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getMaximumScanStepWidthInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return widthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public void setServiceModeConfiguration() throws XrayTesterException
  {
    try
    {
      _nativeHardwareConfiguration.setConfigurationParameters();
      waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
      _rmci.remoteSetServiceModeConfiguration(_motionControllerModel.getId(),
              _motionControllerId.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
//      try
//      {
//        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS * 2);
//        _rmci.remoteSetServiceModeConfiguration(_motionControllerModel.getId(),
//            _motionControllerId.getId());
//      }
//      catch (NativeHardwareException nhe)
//      {
//        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
//        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
//      }
//      catch (RemoteException re2)
//      {
//        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
//        if(_debug)
//          System.out.println("setServiceModeConfiguration re2:" + re2.getMessage());
//        _hardwareTaskEngine.throwHardwareException(mche);
//      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  public MotionControlServiceModeConfigurationStatus getServiceModeConfigurationStatus()
  {
    StringBuffer key = new StringBuffer();
    ArrayList<String> parameters = new ArrayList<String>();
    MotionControlServiceModeConfigurationStatus servicesStatus = null;

    try
    {
      if (_remoteMotionServer.isStartupRequired())
      {
        _remoteMotionServer.startup();
      }
    }
    catch (XrayTesterException ex)
    {
      // do nothing
    }

    try
    {

      servicesStatus = (MotionControlServiceModeConfigurationStatus) _rmci.remoteGetServiceModeConfigurationStatus(_motionControllerModel.getId(),
              _motionControllerId.getId(),
              key,
              parameters);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        servicesStatus = (MotionControlServiceModeConfigurationStatus) _rmci.remoteGetServiceModeConfigurationStatus(_motionControllerModel.getId(),
                _motionControllerId.getId(),
                key,
                parameters);
      }
      catch (RemoteException re2)
      {
        System.out.println("Remote Exception caught in getServiceModeConfigurationStatus");
        System.out.println(re.getMessage());
      }
    }

    return servicesStatus;
  }

  /**
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  public void abortServiceModeConfiguration()
  {
    try
    {
      _rmci.remoteAbortServiceModeConfiguration(_motionControllerModel.getId(), _motionControllerId.getId());
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteAbortServiceModeConfiguration(_motionControllerModel.getId(), _motionControllerId.getId());
      }
      catch (RemoteException re2)
      {
        System.out.println("Remote Exception caught in getServiceModeConfigurationStatus");
        System.out.println(re2.getMessage());
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  int getYaxisScanStartPositionLimitInNanometers(ScanPassDirectionEnum scanPassDirection) throws XrayTesterException
  {
    Assert.expect(scanPassDirection != null);

    int startPositionLimitInNanometers = 0;

    try
    {
      startPositionLimitInNanometers = _rmci.remoteGetYaxisScanStartPositionLimitInNanometers(_motionControllerId.getId(),
              scanPassDirection.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        startPositionLimitInNanometers = _rmci.remoteGetYaxisScanStartPositionLimitInNanometers(_motionControllerId.getId(),
                scanPassDirection.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getYaxisScanStartPositionLimitInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return startPositionLimitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  static int getNumberOfControllersInstalled()
  {
    Config config = Config.getInstance();
    return config.getIntValue(HardwareConfigEnum.NUMBER_OF_MOTION_CONTROLLERS_INSTALLED);
  }

  /**
   * @author Greg Esparza
   */
  void setActualPositionInNanometers(MotionAxisToMoveEnum motionAxisToMove, int actualPositionInNanometers) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    try
    {
      _rmci.remoteSetActualPositionInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId(),
              actualPositionInNanometers);
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteSetActualPositionInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId(),
                actualPositionInNanometers);
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setActualPositionInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * @author Greg Esparza
   */
  public void startup() throws XrayTesterException
  {
    initialize();
  }

  /**
   * @author Greg Esparza
   */
  public void shutdown() throws XrayTesterException
  {
    try
    {
      if (_rmci != null)
      {
        _rmci.remoteShutdown(_motionControllerId.getId(), _motionControllerModel.getId());
      }
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteShutdown(_motionControllerId.getId(), _motionControllerModel.getId());
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("shutdown re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    finally
    {
      if (_rmci != null)
      {
        _remoteMotionServer.forceRestart();
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    return (isInitialized() == false);
  }

  /**
   * @throws XrayTesterException
   */
  public static synchronized void connectToMotionControlServer() throws XrayTesterException
  {
    // Check to see if we are already connected to a server
    if (_rmci != null)
    {
      return;
    }

    // If not attempt to connected to the server specified by the hardware config
    String rmc_ip_address = RemoteMotionServerExecution.getInstance().getServerAddress();

    if (Config.isDeveloperDebugModeOn())
    {
      System.out.println("MotionControl: Expecting Remote Motion Control at IP: " + rmc_ip_address);
    }

    Registry registry;
    try
    {
      registry = LocateRegistry.getRegistry(rmc_ip_address);
      _rmci = (RemoteMotionControlInterface) registry.lookup("RemoteMotionControl");
    }
    catch (NotBoundException nbe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nbe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      MotionControlHardwareException mche = new MotionControlHardwareException(re);
      if (Config.isDeveloperDebugModeOn())
      {
        System.out.println("connectToMotionControlServer re:" + re.getMessage());
      }
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    if (Config.isDeveloperDebugModeOn())
    {
      System.out.println("MotionControl: Connected to Remote Motion Control at IP: " + rmc_ip_address);
    }
  }

  /**
   * Simulate a specific axis
   * 
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  public void setSimulationMode(MotionAxisToMoveEnum motionAxisToMove)
  {
    Assert.expect(motionAxisToMove != null);
    try
    {
      _rmci.remoteSetSimulationMode(_motionControllerId.getId(),
              _motionControllerModel.getId(),
              motionAxisToMove.getId());
    }
    catch (RemoteException re)
    {
      System.out.println("Remote Exception caught in setSimulationMode");
      System.out.println(re.getMessage());
    }

    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * Simulate a specific axis
   * 
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  public void clearSimulationMode(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);
    try
    {
      _rmci.remoteClearSimulationMode(_motionControllerId.getId(),
              _motionControllerModel.getId(),
              motionAxisToMove.getId());
    }
    catch (RemoteException re)
    {
      MotionControlHardwareException mche = new MotionControlHardwareException(re);
      if (_debug)
      {
        System.out.println("clearSimulationMode re:" + re.getMessage());
      }
      _hardwareTaskEngine.throwHardwareException(mche);
    }
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * Simulate a specific axis
   *
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  public boolean isSimulationModeOn(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    boolean simulation_mode_on = false;

    if (_rmci == null)
    {
      return super.isSimulationModeOn();
    }

    try
    {
//      connectToMotionControlServer();
      simulation_mode_on = _rmci.remoteIsSimulationModeOn(_motionControllerId.getId(),
              _motionControllerModel.getId(),
              motionAxisToMove.getId());
    }
    catch (RemoteException re)
    {
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        simulation_mode_on = _rmci.remoteIsSimulationModeOn(_motionControllerId.getId(), _motionControllerModel.getId());
      }
      catch (RemoteException re2)
      {
        return super.isSimulationModeOn();
//        MotionControlHardwareException mche = new MotionControlHardwareException(re);
//        if(_debug)
//          System.out.println("isSimulationModeOn re2:" + re2.getMessage());
//        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    return simulation_mode_on;
  }

  /*
   * Simulate the entire motion control system
   *
   * @author Greg Esparza
   */
  public void setSimulationMode() throws XrayTesterException
  {
    if (isInitialized())
    {
      super.setSimulationMode();
      try
      {
        _rmci.remoteSetSimulationMode(_motionControllerId.getId(), _motionControllerModel.getId());
      }
      catch (RemoteException re)
      {
//        MotionControlHardwareException mche = new MotionControlHardwareException(re);
//        if(_debug)
//          System.out.println("setSimulationMode re:" + re.getMessage());
//        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
  }

  /**
   * Simulate the entire motion control system
   *
   * @author Greg Esparza
   */
  public void clearSimulationMode() throws XrayTesterException
  {
    super.clearSimulationMode();

    if (_rmci == null)
    {
      return;
    }

    try
    {
      _rmci.remoteClearSimulationMode(_motionControllerId.getId(), _motionControllerModel.getId());
    }
    catch (RemoteException re)
    {
//      MotionControlHardwareException mche = new MotionControlHardwareException(re);
//      if(_debug)
//        System.out.println("clearSimulationMode re:" + re.getMessage());
//      _hardwareTaskEngine.throwHardwareException(mche);
    }
  }

  /**
   * Simulate the entire motion control system
   *
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  public boolean isSimulationModeOn()
  {
    boolean simulation_mode_on = false;

    if (_rmci == null)
    {
      return false;
    }

    try
    {
      if (_remoteMotionServer.isStartupRequired())
      {
        //_remoteMotionServer.startup();
        // Siew Yeng - XCR1426
        // Remove auto start up to fix hardwareWorkerThread assert
        return super.isSimulationModeOn();
      }

      simulation_mode_on = _rmci.remoteIsSimulationModeOn(_motionControllerId.getId(), _motionControllerModel.getId());
    }
    catch (XrayTesterException xte)
    {
      // FIXME What to do about this, isSimulationMode isn't able to throw an exception but exceptions can be generated
      // when checking if the hardware is in simulation mode
      System.out.println("XRayTesterException caught in isSimualtionModeOn");
      System.out.println(xte.getMessage());
    }
    catch (RemoteException re)
    {
      return super.isSimulationModeOn();
//      try
//      {
//        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
//        simulation_mode_on = _rmci.remoteIsSimulationModeOn(_motionControllerId.getId(), _motionControllerModel.getId());
//      }
//      catch (RemoteException re2)
//      {
//        System.out.println("RemoteException re2 caught in isSimualtionModeOn");
//        System.out.println(re2.getMessage());
//      }
    }

    if (UnitTest.unitTesting() == false)
    {
      Assert.expect(super.isSimulationModeOn() == simulation_mode_on);
    }

    return simulation_mode_on;
  }

  /**
   * @author Greg Esparza
   */
  MotionProfile getActiveMotionProfile(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    MotionProfile motionProfile = null;
    IntegerRef motionProfileTypeId = new IntegerRef();
    IntegerRef motionProfileId = new IntegerRef();

    String motionProfileTypeAndId = null;

    try
    {
      motionProfileTypeAndId = (String) _rmci.remoteGetActiveMotionProfile(
              _motionControllerId.getId(), motionAxisToMove.getId(), motionProfileTypeId, motionProfileId);

      StringTokenizer stringTokenizer = new StringTokenizer(motionProfileTypeAndId, ",");
      MotionProfileTypeEnum motionProfileType = MotionProfileTypeEnum.getEnum(Integer.parseInt(stringTokenizer.nextToken()));
      motionProfile = new MotionProfile(motionProfileType, Integer.parseInt(stringTokenizer.nextToken()));
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        motionProfileTypeAndId = (String) _rmci.remoteGetActiveMotionProfile(
                _motionControllerId.getId(), motionAxisToMove.getId(), motionProfileTypeId, motionProfileId);

        StringTokenizer stringTokenizer = new StringTokenizer(motionProfileTypeAndId, ",");
        MotionProfileTypeEnum motionProfileType = MotionProfileTypeEnum.getEnum(Integer.parseInt(stringTokenizer.nextToken()));
        motionProfile = new MotionProfile(motionProfileType, Integer.parseInt(stringTokenizer.nextToken()));
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getActiveMotionProfile (motionAxisToMove:" + motionAxisToMove + " ) re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return motionProfile;
  }

  /**
   * @author Greg Esparza
   * @throws XrayTesterException
   */
  void setHardwareResetMode(boolean hardwareResetModeOn) throws XrayTesterException
  {
    try
    {
      _rmci.remoteSetHardwareResetMode(_motionControllerId.getId(), _motionControllerModel.getId(), hardwareResetModeOn);
    }
    catch (RemoteException re)
    {
      // try again 1 time
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        _rmci.remoteSetHardwareResetMode(_motionControllerId.getId(), _motionControllerModel.getId(), hardwareResetModeOn);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("setHardwareResetMode re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  int getPositionAccuracyToleranceInNanometers(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    int toleranceInNanometers = 0;

    try
    {
      toleranceInNanometers = _rmci.remoteGetPositionAccuracyToleranceInNanometers(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        toleranceInNanometers = _rmci.remoteGetPositionAccuracyToleranceInNanometers(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re2);
        if (_debug)
        {
          System.out.println("getPositionAccuracyToleranceInNanometers re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return toleranceInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  boolean isHomingRequired(MotionAxisToMoveEnum motionAxisToMove) throws XrayTesterException
  {
    Assert.expect(motionAxisToMove != null);

    boolean homingRequired = false;

    try
    {
      homingRequired = _rmci.remoteIsHomingRequired(_motionControllerId.getId(),
              motionAxisToMove.getId());
    }
    catch (NativeHardwareException nhe)
    {
      MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
    }
    catch (RemoteException re)
    {
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT__RETRY_IN_MILLISECONDS);
        homingRequired = _rmci.remoteIsHomingRequired(_motionControllerId.getId(),
                motionAxisToMove.getId());
      }
      catch (NativeHardwareException nhe)
      {
        MotionControlHardwareException motionControlHardwareException = new MotionControlHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(motionControlHardwareException);
      }
      catch (RemoteException re2)
      {
        MotionControlHardwareException mche = new MotionControlHardwareException(re);
        if (_debug)
        {
          System.out.println("isHomingRequired re2:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(mche);
      }
    }
    catch (Exception e)
    {
      if (_debug)
      {
        System.out.println("e:" + e.getMessage());
      }
      MotionControlHardwareException mche = new MotionControlHardwareException(e);
      _hardwareTaskEngine.throwHardwareException(mche);
    }

    return homingRequired;
  }

  /**
   * Have the current thread wait for the hardware to finish its action.
   * @param timeOutInMilliSeconds long time out of 0 will return right away.
   * @author Rex Shang
   */
  void waitForHardwareInMilliSeconds(long timeOutInMilliSeconds)
  {
    Assert.expect(timeOutInMilliSeconds >= 0, "Time out " + timeOutInMilliSeconds + " is not greater than or equal to 0.");

    long deadLineInMilliSeconds = timeOutInMilliSeconds + System.currentTimeMillis();
    long timeToWait = timeOutInMilliSeconds;

    while (isAborting() == false && timeToWait > 0)
    {
      try
      {
        Thread.sleep(timeToWait);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
      finally
      {
        // Calculate how much more time we need to wait.
        timeToWait = deadLineInMilliSeconds - System.currentTimeMillis();
      }
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public static void resetRemoteMotionControl()
  {
    _rmci = null;
  }
}
