package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the movement of the stage.  Specifically, the
 * stage can move to an X,Y coordinate with point-to-point or scan
 * control.
 *
 * @author Greg Esparza
 */
public class PanelPositioner extends HardwareObject implements HardwareStartupShutdownInt
{
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 10000;
  private HardwareTaskEngine _hardwareTaskEngine;
  private static HardwareObservable _hardwareObservable;
  private static ProgressObservable _progressObservable;
  private static PanelPositioner _panelPositioner;
  private static final double _AVERAGE_VELOCITY_NANOMETERS_PER_SECOND = MathUtil.convertMilsToNanoMetersInteger(13.1 * 1000);
  private static final double _AVERAGE_TURNAROUND_TIME_SECONDS = .2; // 200 milliseconds

  private MotionControl _motionControl;
  private Config _config;
  private boolean _initialized;
  private MotionAxisToMoveEnum _activeMotionAxis;
  // XCR1144, Chnee Khang Wah, 21-Dec-2010
  private LeftPanelClearSensor _leftPanelClearSensor;
  private RightPanelClearSensor _rightPanelClearSensor;
  private LeftPanelInPlaceSensor _leftPanelInPlaceSensor;
  private RightPanelInPlaceSensor _rightPanelInPlaceSensor;
  // XCR1144 - end

  private Integer _motionControllerId = null;
  private String _yAxisMotorCountPerRevolutionConfigKey = null;
  private DigitalIo _digitalIo;
  public static final int _STAGE_READY_OFFSET_POSITION = 24465000;

  /**
   * @author Greg Esparza
   */
  private PanelPositioner()
  {
    _config = Config.getInstance();

    _motionControllerId = (Integer) _config.getValue(HardwareConfigEnum.PANEL_POSITIONER_MOTION_CONTROLLER_ID);
    MotionControllerIdEnum motionControllerIdEnum = MotionControllerIdEnum.getEnum(_motionControllerId);
    _motionControl = MotionControl.getInstance(motionControllerIdEnum);
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _progressObservable = ProgressObservable.getInstance();
    _initialized = false;
    _activeMotionAxis = MotionAxisToMoveEnum.XY_COORDINATED_AXIS;

    // XCR1144, Chnee Khang Wah, 21-Dec-2010
    _leftPanelClearSensor = LeftPanelClearSensor.getInstance();
    _rightPanelClearSensor = RightPanelClearSensor.getInstance();
    // XCR1144 - end
    
    _digitalIo = DigitalIo.getInstance();
    _leftPanelInPlaceSensor = LeftPanelInPlaceSensor.getInstance();
    _rightPanelInPlaceSensor = RightPanelInPlaceSensor.getInstance();

    setupYaxisSettings();
  }

  /**
   * @author Greg Esparza
   */
  public static synchronized PanelPositioner getInstance()
  {
    if (_panelPositioner == null)
      _panelPositioner = new PanelPositioner();

    return _panelPositioner;
  }

  /**
   * @author Rex Shang
   */
  public void setSimulationMode() throws XrayTesterException
  {
    super.setSimulationMode();
    _motionControl.setSimulationMode();
  }

  /**
   * @author Rex Shang
   */
  public void clearSimulationMode() throws XrayTesterException
  {
    super.clearSimulationMode();
    _motionControl.clearSimulationMode();
  }

  /**
   * @author Rex Shang
   */
  public boolean isSimulationModeOn()
  {
    if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) || super.isSimulationModeOn())
      return true;
    else
      return false;
  }

  /**
   * @author Roy Williams
   */
  public void startup() throws XrayTesterException
  {
    // XCR1144, Chnee Khang Wah, 21-Dec-2010
    if(isSafeToMove()==false)
    {
        throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
    }
    // XCR1144 - end
    
    PanelHandler.getInstance().checkPanelIsLoading();

    initialize();

    // XCR1144, Chnee Khang Wah, 11-Jan-2011, turn on the checking after complete the startup if it was turned off previously
    _digitalIo.setByPassPanelClearSensor(false);
  }

  /**
   * @author Roy Williams
   */
  public void shutdown() throws XrayTesterException
  {
    if (_motionControl.isStartupRequired() == false)
    {
      _motionControl.stop(MotionAxisToMoveEnum.XY_COORDINATED_AXIS);
      _motionControl.disable(MotionAxisToMoveEnum.XY_COORDINATED_AXIS);
      _motionControl.shutdown();
      _initialized = false;
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    if ((_initialized == false) || (_motionControl.isStartupRequired()) || doesXaxisRequireHoming() || doesYaxisRequireHoming())
      return true;
    else
      return false;
  }

  /**
   * @author Greg Esparza
   */
  private boolean doesXaxisRequireHoming() throws XrayTesterException
  {
    return _motionControl.isHomingRequired(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  private boolean doesYaxisRequireHoming() throws XrayTesterException
  {
    return _motionControl.isHomingRequired(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
  }

  /**
   * Initialize the Panel Positioner
   *
   * This will run a full initialization of motion controller and stage hardware.
   * The stage will end up in the home position when the sequence completes.
   *
   * @author Greg Esparza
   */
  private void initialize() throws XrayTesterException
  {
    _initialized = false;

    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.INITIALIZE);
    _hardwareObservable.setEnabled(false);
    int startupTimeInMilliseconds = _STARTUP_TIME_IN_MILLISECONDS + _motionControl.getStartupTimeInMilliseconds();
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, startupTimeInMilliseconds);

    clearAbort();

    try
    {
      //XCR1400 - Siew Yeng
//      DigitalIo.getInstance().shutdown();
//      DigitalIo.getInstance().startup();
      //XCR1400 - end
      //need to startup interlock again after shutdown digitalIO to enable interlock thread again
//      Interlock.getInstance().startup();
      
      if (_digitalIo.isInterlockChain1Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      else if (_digitalIo.isInterlockChain2Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      
      _motionControl.startup();
      
      // XCR-3604 Unit Test Phase 2
      if (UnitTest.unitTesting() == false)
      {
        enableAllAxes();
        homeAllAxes();
      }

      if (isAborting() == false)
        _initialized = true;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.INITIALIZE);
  }

  /**
   * @author Greg Esparza
   */
  public void enableAllAxes() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.ENABLE_ALL_AXES);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.enable(MotionAxisToMoveEnum.XY_COORDINATED_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.ENABLE_ALL_AXES);
  }

  /**
   * @author Greg Esparza
   */
  public void enableXaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.ENABLE_X_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.enable(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.ENABLE_X_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public void enableYaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.ENABLE_Y_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.enable(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.ENABLE_Y_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public void disableAllAxes() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.DISABLE_ALL_AXES);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.disable(MotionAxisToMoveEnum.XY_COORDINATED_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.DISABLE_ALL_AXES);
  }

  /**
   * @author Greg Esparza
   */
  public void disableXaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.DISABLE_X_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.disable(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.DISABLE_X_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public void disableYaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.DISABLE_Y_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.disable(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.DISABLE_Y_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public void homeAllAxes() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.HOME_ALL_AXES);
    _hardwareObservable.setEnabled(false);

    try
    {
      homeXaxis();

      if (isAborting())
        return;

      homeYaxis();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.HOME_ALL_AXES);
  }

  /**
   * @author Greg Esparza
   */
  public void homeXaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.HOME_X_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      if (_digitalIo.isInterlockChain1Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      else if (_digitalIo.isInterlockChain2Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      _activeMotionAxis = MotionAxisToMoveEnum.X_INDEPENDENT_AXIS;
      _motionControl.home(_activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.HOME_X_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public void homeYaxis() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.HOME_Y_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      if (_digitalIo.isInterlockChain1Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      else if (_digitalIo.isInterlockChain2Open())
      {
        _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockChainIsOpenException());
      }
      _activeMotionAxis = MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS;
      _motionControl.home(_activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.HOME_Y_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public synchronized void pointToPointMoveAllAxes(StagePosition stagePosition) throws XrayTesterException
  {
    Assert.expect(stagePosition != null);

    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_ALL_AXES);
    _hardwareObservable.setEnabled(false);


    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      MotionAxisPosition motionAxisPosition = new MotionAxisPosition();
      motionAxisPosition.setXaxisPositionInNanometers(stagePosition.getXInNanometers());
      motionAxisPosition.setYaxisPositionInNanometers(stagePosition.getYInNanometers());

      _activeMotionAxis = MotionAxisToMoveEnum.XY_COORDINATED_AXIS;
      _motionControl.pointToPointMove(motionAxisPosition, _activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_ALL_AXES);
  }

  /**
   * @author Greg Esparza
   */
  public synchronized void pointToPointMoveXaxis(StagePosition stagePosition) throws XrayTesterException
  {
    Assert.expect(stagePosition != null);

    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_X_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      MotionAxisPosition motionAxisPosition = new MotionAxisPosition();
      motionAxisPosition.setXaxisPositionInNanometers(stagePosition.getXInNanometers());

      _activeMotionAxis = MotionAxisToMoveEnum.X_INDEPENDENT_AXIS;
      _motionControl.pointToPointMove(motionAxisPosition, _activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_X_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public synchronized void pointToPointMoveYaxis(StagePosition stagePosition) throws XrayTesterException
  {
    Assert.expect(stagePosition != null);

    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_Y_AXIS);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      MotionAxisPosition motionAxisPosition = new MotionAxisPosition();
      motionAxisPosition.setYaxisPositionInNanometers(stagePosition.getYInNanometers());

      _activeMotionAxis = MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS;
      _motionControl.pointToPointMove(motionAxisPosition, _activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.POINT_TO_POINT_MOVE_Y_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  public synchronized void setMotionProfile(MotionProfile motionProfile) throws XrayTesterException
  {
    Assert.expect(motionProfile != null);
    if (motionProfile.getMotionProfileType().equals(MotionProfileTypeEnum.POINT_TO_POINT))
      _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.XY_COORDINATED_AXIS, motionProfile);
    else if (motionProfile.getMotionProfileType().equals(MotionProfileTypeEnum.SCAN))
      _motionControl.setScanMotionProfile(motionProfile);
    else
      Assert.expect(false);
  }

  /**
   * Set up the position pulse information for a scan move
   *
   * This method will determine the stage position necessary to start the output of
   * of scan pulses defined by "numberOfStartingPulses".  The pulses provide a
   * signal to a device that will operate on these pulses.
   *
   * A scan pulse is provided by the client's definition of "distanceInNanometersPerPulse".
   * For example, a client can  request a pulse be generated after "N" nanometers of relative
   * distance.  The relative distance means, for example,  once every 1000 nanometers.
   *
   * @author Greg Esparza
   */
  public void setPositionPulse(int distanceInNanometersPerPulse, int numberOfStartPulses) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.SET_POSITION_PULSE);
    _hardwareObservable.setEnabled(false);

    try
    {
      distanceInNanometersPerPulse = getValidPositionPulseRateInNanometersPerPulse(distanceInNanometersPerPulse);
  
      _motionControl.setPositionPulse(distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.SET_POSITION_PULSE);
  }

  /**
   * Set up and enable the scan path.
   *
   * The scan path will get stored and ready for use.  No motion will occur until
   * the client calls "runNextScanPass()" to run one scan pass of the entire path.
   *
   * author Greg Esparza
   * author Swee Yee Wong - Include starting pulse for scanpass adjustment
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   */
  public void enableScanPath(List<ScanPass> scanPath, int distanceInNanometersPerPulse, int numberOfStartPulses) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.ENABLE_SCAN_PATH);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.enableScanPath(scanPath, distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.ENABLE_SCAN_PATH);
  }

  /**
   *  Disable the previously loaded scan path.
   *  This must be called if the client is needs to set up a different scan path.
   *
   * @author Greg Esparza
   */
  public void disableScanPath() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.DISABLE_SCAN_PATH);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.disableScanPath();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.DISABLE_SCAN_PATH);
  }

  /**
   * @author Greg Esparza
   */
  public int getScanPathExecutionTimeInMilliseconds() throws XrayTesterException
  {
    return _motionControl.getScanPathExecutionTimeInMilliseconds();
  }

  /**
   * Run the next scan pass asynchronously.
   *
   * @author Greg Esparza
   */
  public void runNextScanPass() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, PanelPositionerEventEnum.RUN_NEXT_SCAN_PASS);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(XrayTester.isS2EXEnabled())
      {
        if (Interlock.getInstance().getServicePanelActivation() || _digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          //do nothing
        }
        else
        {
          if(_digitalIo.arePanelClampsOpened())
          {
            if (PanelHandler.getInstance().isPanelLoaded())
            {
              PanelClamps.getInstance().close();
            }
          }
        }
      }
      _activeMotionAxis = MotionAxisToMoveEnum.XY_COORDINATED_AXIS;
      _motionControl.runNextScanPass();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelPositionerEventEnum.RUN_NEXT_SCAN_PASS);
  }

  /**
   * @author Greg Esparza
   */
  public void pauseScanPath() throws XrayTesterException
  {
    _motionControl.pauseScanPath();
  }

  /**
   * @author Greg Esparza
   */
  public boolean isScanPathDone() throws XrayTesterException
  {
    boolean scanPathDone = false;

    try
    {
      scanPathDone = _motionControl.isScanPathDone();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return scanPathDone;

  }

  /**
   * @author Greg Esparza
   */
  public boolean isScanPassDone() throws XrayTesterException
  {
    boolean scanPassDone = false;

    try
    {
      scanPassDone = _motionControl.isScanPassDone();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return scanPassDone;
  }

  /**
   * Fix for CR31712
   * It will check and wait till scan pass status is done or time out
   * it use for prevent multiple motion interuption which will cause runtime error
   *
   * @author Wei Chin, Chong
   */
  public void waitTillScanPassDone() throws XrayTesterException
  {
    waitTillScanPassDone(1);
  }
  
  /**
   * Fix for CR31712
   * It will check and wait till scan pass status is done or time out
   * it use for prevent multiple motion interuption which will cause runtime error
   *
   * @author Wei Chin, Chong
   */
  public void waitTillScanPassDone(double multiplyFactor) throws XrayTesterException
  {
    TimerUtil scanPassTimer = new TimerUtil();
    scanPassTimer.start();
    double timeOut = 10000 * multiplyFactor;

    while (isScanPassDone() == false)
    {
      try
      {
        Thread.sleep(100);
        if(scanPassTimer.getElapsedTimeInMillis() > timeOut)
        {
          Assert.expect(false,"Failed to waitScanPassDone()");
        }
      }
      catch (InterruptedException e)
      {
        //do nothing
      }
    }
  }
  /**
   * For Speed Up
   * @author Anthony Fong
   */
  public void waitTillScanPathDone() throws XrayTesterException
  {
    waitTillScanPathDone(1);
  }
  
  /**
   * For Speed Up
   * @author Anthony Fong
   */
  public void waitTillScanPathDone(double multiplyFactor) throws XrayTesterException
  {
    TimerUtil scanPassTimer = new TimerUtil();
    scanPassTimer.start();

    double timeOut = 10000 * multiplyFactor;
    while (isScanPathDone() == false)
    {
      try
      {
        Thread.sleep(100);
        if(scanPassTimer.getElapsedTimeInMillis() > timeOut)
        {
          Assert.expect(false,"Failed to waitTillScanPathDone()");
        }
      }
      catch (InterruptedException e)
      {
        //do nothing
      }
    }
  }
  /**
   * @author Greg Esparza
   */
  public void abort() throws XrayTesterException
  {
    try
    {
      super.abort();
      if (_motionControl.isStartupRequired() == false)
        _motionControl.stop(_activeMotionAxis);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  public int getXaxisMinimumPositionLimitInNanometers() throws XrayTesterException
  {
    int limitInNanometers = 0;

    try
    {
      limitInNanometers =_motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return limitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getXaxisMaximumPositionLimitInNanometers() throws XrayTesterException
  {
    int limitInNanometers = 0;

    try
    {
      limitInNanometers = _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return limitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getYaxisMinimumPositionLimitInNanometers() throws XrayTesterException
  {
    int limitInNanometers = 0;

    try
    {
      limitInNanometers = _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return limitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getYaxisMaximumPositionLimitInNanometers() throws XrayTesterException
  {
    int limitInNanometers = 0;

    try
    {
      limitInNanometers = _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return limitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getMinimumScanPassLengthInNanometers() throws XrayTesterException
  {
    int lengthInNanometers = 0;

    try
    {
      lengthInNanometers =_motionControl.getMinimumScanPassLengthInNanometers();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return lengthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getMaximumScanPassLengthInNanometers(int yAxisScanPassStartPositionInNanometers, ScanPassDirectionEnum scanPassDirection) throws XrayTesterException
  {
    int lengthInNanometers = 0;

    try
    {
      lengthInNanometers = _motionControl.getMaximumScanPassLengthInNanometers(yAxisScanPassStartPositionInNanometers, scanPassDirection);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return lengthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public synchronized int getXaxisActualPositionInNanometers() throws XrayTesterException
  {
    int positionInNanometers = 0;

    try
    {
      positionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return positionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public synchronized int getYaxisActualPositionInNanometers() throws XrayTesterException
  {
    int positionInNanometers = 0;

    try
    {
      positionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return positionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public synchronized boolean isXaxisHomeSensorClear() throws XrayTesterException
  {
    boolean homeSensorClear = false;

    try
    {
      homeSensorClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return homeSensorClear;
  }

  /**
   * @author Greg Esparza
   */
  public synchronized boolean isYaxisHomeSensorClear() throws XrayTesterException
  {
    boolean homeSensorClear = false;

    try
    {
      homeSensorClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return homeSensorClear;
  }

  /**
   * XCR1144, Chnee Khang Wah, 21-Dec-2010
   * Check if a panel is in the machine
   * @return boolean whether a panel is blocking panel clear sensor or not
   * @author Khang-Wah, Chnee
   */
  public boolean isSafeToMove() throws XrayTesterException
  {
    boolean isSafeToMove = false; //assume system is not safe to move in first place

    if(UnitTest.unitTesting() || XrayTester.getInstance().isSimulationModeOn())
        return true;

    if(_digitalIo.isByPassPanelClearSensor()==true)
        isSafeToMove = true;
    else if(isNearHomePosition()==true)
    {
        if(XrayTester.isS2EXEnabled())
        {
          isSafeToMove = _leftPanelClearSensor.isBlocked()==false &&  _rightPanelClearSensor.isBlocked()==false;
        }
        else
        {
          isSafeToMove = _leftPanelClearSensor.isBlocked()==true &&  _rightPanelClearSensor.isBlocked()==false;
        }
    }
    else
        isSafeToMove = _leftPanelClearSensor.isBlocked()==false &&  _rightPanelClearSensor.isBlocked()==false;

    return isSafeToMove;
  }

   /**
   * XCR1144, Chnee Khang Wah, 22-Dec-2010
   * check wheater the board is near to home position? this is keep out zone!
   * @author Khang-Wah, Chnee
   */
  public boolean isNearHomePosition()
  {
    int X = 0;
    int Y = 0;

    if(_initialized==false) // return false when we unknow about the position.
        return false;

    try
    {
        X = getXaxisActualPositionInNanometers();
        Y = getYaxisActualPositionInNanometers();

        // Normally when the stage home, it will deviate around a few um,
        // we assume it is in home position when the reading of stage position is less than 20um.
        if(X<20000 && Y<20000)
            return true;
    }
    catch(XrayTesterException xte)
    {
        // do nothing
    }

    return false;
  }

  /**
   * CR34097, Chnee Khang Wah, 22-Dec-2010
   * check wheater the board is at loading/unloading position?
   * @author Khang-Wah, Chnee
   */
  public boolean isInLoadUnLoadPosition()
  {
      int X = 0;
      int Y = 0;
      int left_X = 0;
      int left_Y = 0;
      int right_X = 0;
      int right_Y = 0;

      if(_initialized==false) // return false when we unknow about the position.
            return false;
      
      try
      {
          X = getXaxisActualPositionInNanometers();
          Y = getYaxisActualPositionInNanometers();
          left_X = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
          left_Y = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
          right_X = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
          right_Y = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);

          int tol =20000;

          boolean left = MathUtil.fuzzyEquals(X, left_X, tol) && MathUtil.fuzzyEquals(Y, left_Y,tol);
          boolean right = MathUtil.fuzzyEquals(X, right_X, tol) && MathUtil.fuzzyEquals(Y, right_Y, tol);

          if(left || right)
              return true;
      }
      catch(XrayTesterException xte)
      {
          // do nothing
      }
      return false;
  }

  /**
   * @author Greg Esparza
   */
  boolean areAllAxesEnabled() throws XrayTesterException
  {
    return (isXaxisEnabled() && isYaxisEnabled());
  }

  /**
   * @author Greg Esparza
   */
  public boolean isXaxisEnabled() throws XrayTesterException
  {
    boolean axisEnabled = false;

    try
    {
      axisEnabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return axisEnabled;
  }

  /**
   * @author Greg Esparza
   */
  public boolean isYaxisEnabled() throws XrayTesterException
  {
    boolean axisEnabled = false;

    try
    {
      axisEnabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return axisEnabled;
  }

  /**
   * @author Greg Esparza
   * @author Rex Shang
   */
  public void setXaxisHysteresisOffsetInNanometers(int hysteresisOffsetInNanometers) throws XrayTesterException
  {
    _config.setValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS, hysteresisOffsetInNanometers);

    try
    {
      _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, hysteresisOffsetInNanometers);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   * @author Rex Shang
   * @author Wei Chin
   */
  public void setYaxisHysteresisOffsetInNanometers(int hysteresisOffsetInNanometers) throws XrayTesterException
  {
    if(XrayTester.isLowMagnification())
    {
      _config.setValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS, hysteresisOffsetInNanometers);

      try
      {
        _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, hysteresisOffsetInNanometers);
      }
      catch (MotionControlHardwareException he)
      {
        _initialized = false;
        throw he;
      }
    }
    else
    {
      _config.setValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG, hysteresisOffsetInNanometers);

      try
      {
        _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, hysteresisOffsetInNanometers);
      }
      catch (MotionControlHardwareException he)
      {
        _initialized = false;
        throw he;
      }
    }
  }

  /**
   * @author Wei Chin
   */
  public void resetYaxisHysteresisOffsetForHighMag() throws XrayTesterException
  {
    try
    {
      _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, getYaxisHystersisOffsetInNanometersForHighMag());
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
  }

  /**
   * @author Wei Chin
   */
  public void resetYaxisHysteresisOffsetForLowMag() throws XrayTesterException
  {
    try
    {
      _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, getYaxisHystersisOffsetInNanometers());
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }
  }
  
  /**
   * @author Rex Shang
   */
  public int getXaxisHystersisOffsetInNanometers()
  {
    return _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
  }

  /**
   * @author Rex Shang
   */
  public int getYaxisHystersisOffsetInNanometers()
  {
    return _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
  }
  
  /**
   * @author Wei Chin
   */
  public int getYaxisHystersisOffsetInNanometersForHighMag()
  {
    return _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG);
  }

  /**
   * @author Greg Esparza
   * @author Rex Shang
   */
  int getYaxisForwardDirectionHysteresisOffsetInNanometers() throws XrayTesterException
  {
    if(XrayTester.isLowMagnification())
      return getYaxisHystersisOffsetInNanometers();
    else
      return getYaxisHystersisOffsetInNanometersForHighMag();
  }

  /**
   * @author Greg Esparza
   * @author Rex Shang
   */
  int getYaxisReverseDirectionHysteresisOffsetInNanometers() throws XrayTesterException
  {
    if(XrayTester.isLowMagnification())
      return -1 * getYaxisHystersisOffsetInNanometers();
    else
      return -1 * getYaxisHystersisOffsetInNanometersForHighMag();
  }

  /**
   * This method is duplicating some functionalities in
   * cpp/hardware/motionControl/SingleMotionAxis::setSynqNetAxisSettings().
   * It is done because we needed some parameters before we initialize the
   * motion controller which in-turn builds the SingleMotionAxis.
   * @author Rex Shang
   */
  private void setupYaxisSettings()
  {
    String yAxisSettingsKey = "synqNet" + _motionControllerId + "yAxisSettings";
    String yAxisSetttings = _config.getStringValue(HardwareConfigEnum.getConfigEnum(yAxisSettingsKey));

    String[] axisSettingsList = yAxisSetttings.split(",");
    Assert.expect(axisSettingsList.length > 0, "There should be at least 1 yAxisSettings");

    // If we have more than one node-drive-motor setting, just use the first one since the motor
    // count per rev should be the same.
    String[] nodeDriveMotor = axisSettingsList[0].split(":");
    Assert.expect(nodeDriveMotor.length == 3, "There should be 3 paramters in nodeDriveMotor information");

    int yAxisMotorId = Integer.valueOf(nodeDriveMotor[2]);

    _yAxisMotorCountPerRevolutionConfigKey = "synqNet" + _motionControllerId + "Motor" + yAxisMotorId + "CountsPerRevolution";
  }

  /**
   * This method is duplicating some functionalities in
   * cpp/hardware/motionControl/MultipleMotionAxis::getValidPositionPulseRateInNanometersPerPulse().
   * It is done because we needed some parameters before we initialize the
   * motion controller which in-turn builds the MultipleMotionAxis.
   * @author Rex Shang
   */
  public int getValidPositionPulseRateInNanometersPerPulse(int requestedNanometersPerPulse)
  {
    double yAxisTravelInNanometersPerRev = Double.valueOf(
        _config.getStringValue(HardwareConfigEnum.Y_AXIS_BALL_SCREW_TRAVEL_IN_NANOMETERS_PER_REVOLUTION));

    double yAxisMotorCountPerRevolution = Double.valueOf(
        _config.getStringValue(HardwareConfigEnum.getConfigEnum(_yAxisMotorCountPerRevolutionConfigKey)));

    double yAxisMotorCountPerNanometers = yAxisMotorCountPerRevolution / yAxisTravelInNanometersPerRev;

    int rateInNanometersPerPulse = (int)(Math.round(requestedNanometersPerPulse * yAxisMotorCountPerNanometers)
                                         / yAxisMotorCountPerNanometers);

    return rateInNanometersPerPulse;
  }

  /**
   * @author Greg Esparza
   */
  int getMinimumScanStepWidthInNanometers() throws XrayTesterException
  {
    int widthInNanometers = 0;

    try
    {
      widthInNanometers = _motionControl.getMinimumScanStepWidthInNanometers();
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return widthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getMaximumScanStepWidthInNanometers(int xAxisScanStepStartPositionInNanometers, ScanStepDirectionEnum scanStepDirection) throws XrayTesterException
  {
    int widthInNanometers = 0;

    try
    {
      widthInNanometers = _motionControl.getMaximumScanStepWidthInNanometers(xAxisScanStepStartPositionInNanometers, scanStepDirection);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return widthInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getYaxisScanStartPositionLimitInNanometers(ScanPassDirectionEnum scanPassDirection) throws XrayTesterException
  {
    int limitInNanometers = 0;

    try
    {
      limitInNanometers = _motionControl.getYaxisScanStartPositionLimitInNanometers(scanPassDirection);
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    return limitInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public MotionProfile getActiveMotionProfile() throws XrayTesterException
  {
    MotionProfile xAxisMotionProfile = null;
    MotionProfile yAxisMotionProfile = null;
    try
    {
      xAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
      yAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);

      //Make sure both profiles are same
      if(xAxisMotionProfile.getId() != yAxisMotionProfile.getId())
      {
        setMotionProfile(yAxisMotionProfile);
        xAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
        yAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
      }
      if((xAxisMotionProfile.getMotionProfileType().equals(yAxisMotionProfile.getMotionProfileType())) == false)
      {
        setMotionProfile(yAxisMotionProfile);
        xAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
        yAxisMotionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
      }
      
      Assert.expect(xAxisMotionProfile.getId() == yAxisMotionProfile.getId());
      Assert.expect(xAxisMotionProfile.getMotionProfileType().equals(yAxisMotionProfile.getMotionProfileType()));
    }
    catch (MotionControlHardwareException he)
    {
      _initialized = false;
      throw he;
    }

    // The X-axis and Y-axis have to be the same so just return one of them
    return xAxisMotionProfile;
  }

  /**
   * @author Roy Williams
   */
  public static final double getVelocityForScanMotionProfileInNanometersPerSecond()
  {
    return _AVERAGE_VELOCITY_NANOMETERS_PER_SECOND;
  }


  /**
   * @author Roy Williams
   */
  public static final double getAverageTurnaroundTimeInSeconds()
  {
    return _AVERAGE_TURNAROUND_TIME_SECONDS;
  }
}
