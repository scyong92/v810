package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class InnerBarrierEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static InnerBarrierEventEnum BARRIER_OPEN = new InnerBarrierEventEnum(++_index, "Inner Barrier, Open");
  public static InnerBarrierEventEnum BARRIER_CLOSE = new InnerBarrierEventEnum(++_index, "Inner Barrier, Close");

  private String _name;

  /**
   * @author Rex Shang
   */
  private InnerBarrierEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
