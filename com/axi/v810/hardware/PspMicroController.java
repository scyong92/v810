package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cheah Lee Herng
 */
public class PspMicroController extends AbstractMicroController implements HardwareStartupShutdownInt, Simulatable
{
    private static final long _PROJECTOR_INITIALIZED_WAIT_MS  = 13000;
    private static final long _RESET_MICROCONROLLER_WAIT_MS   = 1300;
    
    private static ProgressObservable _progressObservable = ProgressObservable.getInstance();

    private boolean _isThisObjectInSimMode = false;
    private static boolean _simulateTiming = false;

    private FileLoggerAxi _pspMicroControllerlogger = null;
    private int _maxLogFileSizeInBytes =
      _config.getIntValue(SoftwareConfigEnum.MICROCONTROLLER_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);

    // object which handles the low-level commanding protocol
    private PspMicroControllerCommandingLink _commandingLink;

    private StatisticalTimer _pspMicroControllerOnStatisticalTimer = new StatisticalTimer("on()");

    // current state of the x-ray source
    private PspMicroControllerStatusEnum _operationalStatus = PspMicroControllerStatusEnum.NOT_READY;
    
    private String _firmwareVersion;

    /**
     * @author Cheah Lee Herng
     */
    public PspMicroController()
    {
        // the _commandingLink object handles the low-level communication protocol
        _commandingLink = new PspMicroControllerCommandingLink(getSerialPortName());

        // for debugging and off-line
        if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
        {
          setSimulationMode();
          _commandingLink.setSimulationMode();
        }

        try
        {
          setupPspMicroControllerLog();
        }
        catch (DatastoreException ex)
        {
          System.out.print("DatastoreException :" + ex.getLocalizedMessage());
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    public void setSimulationMode()
    {
        _isThisObjectInSimMode = true;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void clearSimulationMode()
    {
        _isThisObjectInSimMode = false;
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isSimulationModeOn()
    {
        return _isThisObjectInSimMode || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void simulateTiming()
    {
        _simulateTiming = true;
        PspMicroControllerCommandingLink.setSimulateLinkTiming();
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        // make sure comm link is established
        _commandingLink.configureSerialPortOnce();
        return _startupRequired;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void startup() throws XrayTesterException
    {
        _hardwareObservable.stateChangedBegin(this, MicroControllerEventEnum.ON);
        _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_INITIALIZE, 0);
        _hardwareObservable.setEnabled(false);
        try
        {
          // Clear the latched hardware error code before continuing.
          resetHardwareReportedErrors();

          // simulate warmup
          _commandingLink.setSimulatedResponseForReady("a0");
          
          // do whatever is needed to get the micro controller in the READY state
          if (isPspMicroControllerReady() == false)
          {
              makePspMicroControllerReady();
          }
                    
          // Regardless of the projector state, we need to make sure it is turn on.
          on();
          
          if (_operationalStatus.equals(PspMicroControllerStatusEnum.READY))
          {
              _startupRequired = false;
          }
          else
          {
            _startupRequired = true;
          }
        }
        catch (XrayTesterException xtx)
        {
          _startupRequired = true; // reset when call is not successfull
          throw xtx;
        }
        catch (RuntimeException rx)
        {
          _startupRequired = true; // reset when call is not successfull
          throw rx;
        }
        finally
        {
          _hardwareObservable.setEnabled(true);
        }
        _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_INITIALIZE, 100);

        _hardwareObservable.stateChangedEnd(this, MicroControllerEventEnum.ON);
    }

    /**
     * @author Cheah Lee Herng
     */
    private String getSerialPortName()
    {
        String result = _config.getStringValue(HardwareConfigEnum.PSP_MICROCONTROLLER_SERIAL_PORT_NAME);
        return result;
    }

    /**
     * @author Cheah Lee Herng
    */
    public void setupPspMicroControllerLog() throws DatastoreException
    {
        if(_pspMicroControllerlogger == null)
        {
          createLogFileAndHeader();
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createLogFileAndHeader() throws DatastoreException
    {
        _pspMicroControllerlogger = new FileLoggerAxi(FileName.getConfirmMicroControllerFullPath() + ".pspMicronController" +
                                                        FileName.getLogFileExtension(),
                                                        _maxLogFileSizeInBytes);

        StringBuffer stringBuff = new StringBuffer();
        stringBuff.append("--PSP MicroController Status--");
        stringBuff.append(System.getProperty("line.separator"));

        _pspMicroControllerlogger.append(stringBuff.toString());
    }

    /**
     * @author Cheah Lee Herng
     */
    private boolean isPspMicroControllerReady() throws XrayTesterException
    {
        updatePspMicroControllerStatus();
        if (_operationalStatus.equals(PspMicroControllerStatusEnum.READY))
            return true;
        else
            return false;
    }

    /**
     * @author Cheah Lee Herng
     */
    private ProgressReporterEnum makePspMicroControllerReady() throws XrayTesterException
    {
        updatePspMicroControllerStatus();
        if (_operationalStatus.equals(PspMicroControllerStatusEnum.NOT_READY))
        {
            _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_INITIALIZE, 10);

            String status;
            int numberOfRetries = 0;
            int maximumRetries = 3;
            while (numberOfRetries < maximumRetries)
            {
                status = _commandingLink.sendAndReceive("a0");
                if (status == PspMicroControllerStatusEnum.READY.getStatus())
                    break;
                ++numberOfRetries;
            }
            _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_INITIALIZE, 99);
            checkForErrors();
            return ProgressReporterEnum.MICROCONTROLLER_INITIALIZE;
        }
        else if (_operationalStatus.equals(PspMicroControllerStatusEnum.READY))
        {
            return ProgressReporterEnum.MICROCONTROLLER_INITIALIZE;
        }
        Assert.expect(false, "Unexpected operational status");
        return null;
    }

    /**
     * @author Cheah Lee Herng
     */
    private void updatePspMicroControllerStatus() throws XrayTesterException
    {
        _operationalStatus = _commandingLink.getOperationStatus();
    }

    /**
     * @author Cheah Lee Herng
     */
    private void checkForErrors() throws XrayTesterException
    {
        MicroControllerHardwareReportedErrorsException pspMChre = getHardwareReportedErrors();
        if (pspMChre != null)
          throw pspMChre;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void shutdown() throws XrayTesterException
    {
        _startupRequired = true;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void powerAllMicroControllerOn() throws XrayTesterException
    {
        if (isPspMicroControllerReady() == false)
            makePspMicroControllerReady();
                        
        // Turn all projector on
        _commandingLink.sendAndReceive("b0");
        
        // Check for available errors, just in case
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
        
        // Here, we put a sleep here to make sure projector
        // have enough time to initialize itself.
        try
        {
            Thread.sleep(_PROJECTOR_INITIALIZED_WAIT_MS);
        }
        catch(InterruptedException ex)
        {
            // Do nothing
        }
                 
        //_progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_ON, 90);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void powerAllMicroControllerOff() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("b5");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
        
        _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_OFF, 90);
    }        
    
    /**
     * @author Cheah Lee Herng
     */
    public void initializeProjectorOne() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("10");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void initializeProjectorTwo() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("20");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void enableAllProjectorLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c0");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void enableProjectorOneLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c1");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void enableProjectorTwoLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c2");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void disableAllProjectorLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c5");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void disableProjectorOneLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c6");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void disableProjectorTwoLED() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("c7");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void startProjectorOneSequence() throws XrayTesterException
    {        
        _commandingLink.sendAndReceive("11");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void startProjectorTwoSequence() throws XrayTesterException
    {
        _commandingLink.sendAndReceive("21");
        
        checkForErrors();
        
        // Get MicroController updated status
        updatePspMicroControllerStatus();
    }
    
    /**
     * Get microcontroller firmware version
     * 
     * @author Cheah Lee Herng
     */
    private String getMicroControllerFirmwareVersion() throws XrayTesterException
    {
      String firmwareVersion = _commandingLink.sendAndReceive("f0");
      checkForErrors();
      return firmwareVersion;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void setupMicroControllerInfo() throws XrayTesterException
    {
      // Retrieve MicroController firmware version
      _firmwareVersion = getMicroControllerFirmwareVersion();

      // Setup configuration
      setConfigurationDescription();
    }

    /**
     * @author Cheah Lee Herng
     */
    public void on() throws XrayTesterException
    {
      _pspMicroControllerOnStatisticalTimer.start();

      _hardwareObservable.stateChangedBegin(this, MicroControllerEventEnum.ON);
      _hardwareObservable.setEnabled(false);
      _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_ON, 0);

      try
      {
        int tryCount = 1;
        final int maxTryCount = 2;
        boolean startupOk = false;

        while (tryCount <= maxTryCount && startupOk == false)
        {
          try
          {
            // XCR1579 - Get microcontroller firmware here first
            // because starting from version 1.1 (new firmware),
            // it supports RESET command (rs)
            setupMicroControllerInfo();
            resetMicroControllerIfNecessary();

            powerAllMicroControllerOn();
            enableProjectorOneLED();
            enableProjectorTwoLED();            
            initializeProjectorOne();
            initializeProjectorTwo();

            startupOk = true;
          }
          catch(XrayTesterException xte)
          {
            if (tryCount >= maxTryCount)
              throw xte;
          }
          tryCount++;
        }
      }
      finally
      {
          _pspMicroControllerOnStatisticalTimer.stop();
          _hardwareObservable.setEnabled(true);
      }
      _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_ON, 100);
      _hardwareObservable.stateChangedEnd(this, MicroControllerEventEnum.ON);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void off() throws XrayTesterException
    {
        _pspMicroControllerOnStatisticalTimer.start();       
        
        _hardwareObservable.stateChangedBegin(this, MicroControllerEventEnum.OFF);
        _hardwareObservable.setEnabled(false);
        _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_OFF, 0);
        try
        {
            powerAllMicroControllerOff();
        }
        finally
        {
            _pspMicroControllerOnStatisticalTimer.stop();
            _hardwareObservable.setEnabled(true);
        }
        _progressObservable.reportProgress(ProgressReporterEnum.MICROCONTROLLER_OFF, 100);
        _hardwareObservable.stateChangedEnd(this, MicroControllerEventEnum.OFF);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void resetHardwareReportedErrors() throws XrayTesterException
    {
        checkForHardwareReportedErrors();
    }

    /**
     * @author Cheah Lee Herng
     */
    public MicroControllerHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException
    {
        MicroControllerHardwareReportedErrorsException result = null;

        // read latched status
        _operationalStatus = _commandingLink.getOperationStatus();

        return result;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void resetMicroController() throws XrayTesterException
    {
      _commandingLink.sendAndNoReceive("rs");

      try
      {
        Thread.sleep(_RESET_MICROCONROLLER_WAIT_MS);
      }
      catch(InterruptedException ex)
      {
          // Do nothing
      }

      checkForErrors();

      // Get MicroController updated status
      updatePspMicroControllerStatus();                
    }
    
    /**
     * Only firmware with version 1.1 and above supports RS command.
     * Current firmware version available: 1.0 (old firmware which does not support RS command), 1.1 and 1.2
     * 
     * @author Cheah Lee Herng
     */
    private void resetMicroControllerIfNecessary() throws XrayTesterException
    {
      if (_firmwareVersion.equals("11") || _firmwareVersion.equals("12"))
        resetMicroController();
    }
    
    /**
     * Return MicroController information, eg: firmware version.
     * 
     * @author Cheah Lee Herng
     */
    private void setConfigurationDescription()
    {
      List<String> parameters = new ArrayList<String>();

      parameters.add(_firmwareVersion);

      _configDescription.clear();
      _configDescription.add(new HardwareConfigurationDescription("HW_MICROCONTROLLER_CONFIGURATION_KEY", parameters));
    }
}
