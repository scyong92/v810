package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class PanelClampsEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static PanelClampsEventEnum CLAMPS_CLOSE = new PanelClampsEventEnum(++_index, "Panel Clamps, Close");
  public static PanelClampsEventEnum CLAMPS_OPEN = new PanelClampsEventEnum(++_index, "Panel Clamps, Open");

  private String _name;

  /**
   * @author Rex Shang
   */
  private PanelClampsEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
