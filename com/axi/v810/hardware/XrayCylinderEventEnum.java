package com.axi.v810.hardware;

import com.axi.util.*;

//Variable Mag Anthony August 2011
/**
 * @author Anthony Fong
 */
public class XrayCylinderEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static XrayCylinderEventEnum XRAY_CYLINDER_UP = new XrayCylinderEventEnum(++_index, "XrayCylinder, Up");
  public static XrayCylinderEventEnum XRAY_CYLINDER_DOWN = new XrayCylinderEventEnum(++_index, "XrayCylinder, Down");

  private String _name;

  /**
   * @author Anthony Fong
   */
  private XrayCylinderEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Anthony Fong
   */
  public String toString()
  {
    return _name;
  }

}
