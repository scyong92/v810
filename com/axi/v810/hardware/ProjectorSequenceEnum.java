package com.axi.v810.hardware;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class ProjectorSequenceEnum extends Enum 
{
    private static int _index = 0;
    public static ProjectorSequenceEnum PROJECTOR_ONE_SEQUENCE = new ProjectorSequenceEnum(++_index);
    public static ProjectorSequenceEnum PROJECTOR_TWO_SEQUENCE = new ProjectorSequenceEnum(++_index);    
    
    /**
     * @author Cheah Lee Herng 
     */
    protected ProjectorSequenceEnum(int id)
    {
        super(id);
    }
}
