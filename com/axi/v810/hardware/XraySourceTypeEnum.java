package com.axi.v810.hardware;

/**
 * Enumeration of supported X-ray sources
 *
 * @author George Booth
 */
public class XraySourceTypeEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  public static final XraySourceTypeEnum LEGACY = new XraySourceTypeEnum(_index++);    // 5DX X-ray Source
  public static final XraySourceTypeEnum GENESIS = new XraySourceTypeEnum(_index++);   // Genesis X-ray Source
  public static final XraySourceTypeEnum HTUBE = new XraySourceTypeEnum(_index++);     // Hamamatsu L9818-18 X-ray Source

  /**
   * @author George Booth
   */
  private XraySourceTypeEnum(int id)
  {
    super(id);
  }

}
