package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port specified does not exist on the computer, then throw this
 * exception.
 *
 * @author Jeff Ryer
 */
class NoSuchPortHardwareException extends HardwareException
{
  /**
   * @author Jeff Ryer
   */
  NoSuchPortHardwareException(String portName)
  {
    super(new LocalizedString("HW_NO_SUCH_PORT_KEY", new Object[]{portName}));
    Assert.expect(portName != null);
  }
}