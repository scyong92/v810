package com.axi.v810.hardware;

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Rick
 */
public class RemoteMotionControl extends UnicastRemoteObject implements RemoteMotionControlInterface
{ 
  protected static boolean _debug = false;

  /**
   * @throws RemoteException
   */
  public RemoteMotionControl() throws RemoteException 
  {
    // do nothing
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   * @throws NativeHardwareException
   */
  public void remoteInitialize(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteInitialize");
    try
    {
      nativeInitialize(motionControllerModel, motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteInitialize): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteHome(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteHome");
    try
    {
      nativeHome(motionControllerModel, motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteHome): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteEnable(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteEnable");
    try
    {
      nativeEnable(motionControllerModel, motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteEnable): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteDisable(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteDisable");
    try
    {
      nativeDisable(motionControllerModel, motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteDisable): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param motionProfile
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetPointToPointMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetPointToPointMotionProfile");
    try
    {
      nativeSetPointToPointMotionProfile(motionControllerId, motionAxisToMove, motionProfile);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetPointToPointMotionProfile): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param xInNanometers
   * @param yInNanometers
   * @param railWidthInNanometers
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remotePointToPointMove(int motionControllerId, int motionAxisToMove, int xInNanometers, int yInNanometers, int railWidthInNanometers)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remotePointToPointMove");
    try
    {
      nativePointToPointMove(motionControllerId, motionAxisToMove, xInNanometers, yInNanometers, railWidthInNanometers);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remotePointToPointMove): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param motionProfile
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetScanMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetScanMotionProfile");
    
    try
    {
      nativeSetScanMotionProfile(motionControllerId, motionAxisToMove, motionProfile);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetScanMotionProfile): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public boolean remoteIsScanPathDone(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteIsScanPathDone");
    try
    {
      return nativeIsScanPathDone(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteIsScanPathDone): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public boolean remoteIsScanPassDone(int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteIsScanPassDone");
    try
    {
      return nativeIsScanPassDone(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteIsScanPassDone): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public boolean remoteIsHomeSensorClear(int motionControllerId, int motionAxisToMove)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteIsHomeSensorClear");
    try
    {
      return nativeIsHomeSensorClear(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteIsHomeSensorClear): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param parameterGroupSeparator
   * @param keys
   * @param parameters
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public List<HardwareConfigurationDescriptionInt> remoteGetConfigurationDescription(int motionControllerId, String parameterGroupSeparator, ArrayList<String> keys, ArrayList<String> parameters)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetConfigurationDescription");
    List<HardwareConfigurationDescriptionInt> configDescription = new ArrayList<HardwareConfigurationDescriptionInt>();
    try
    {
      nativeGetConfigurationDescription(motionControllerId, parameterGroupSeparator, keys, parameters);
      ArrayList<String> finalParameters = new ArrayList<String>();

      int keyIndex = 0;
      for (String parameterElement : parameters)
      {
        if (parameterElement.equalsIgnoreCase(parameterGroupSeparator) == false)
          finalParameters.add(parameterElement);
        else
        {
          HardwareConfigurationDescription config = new HardwareConfigurationDescription(keys.get(keyIndex), finalParameters);
          configDescription.add(config);
          finalParameters = new ArrayList<String>();
          ++keyIndex;
        }
      }
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetConfigurationDescription): " + ne.getMessage());
      throw ne;
    }
    return configDescription;
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public boolean remoteIsAxisEnabled(int motionControllerId, int motionAxisToMove)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteIsAxisEnabled");
    try
    {
      return nativeIsAxisEnabled(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteIsAxisEnabled): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param hysteresisOffsetInNanometers
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove, int hysteresisOffsetInNanometers)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetHysteresisOffsetInNanometers");
    try
    {
      nativeSetHysteresisOffsetInNanometers(motionControllerId, motionAxisToMove, hysteresisOffsetInNanometers);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetHysteresisOffsetInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetForwardDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetForwardDirectionHysteresisOffsetInNanometers");
    try
    {
      return nativeGetForwardDirectionHysteresisOffsetInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetForwardDirectionHysteresisOffsetInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetReverseDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetReverseDirectionHysteresisOffsetInNanometers");
    try
    {
      return nativeGetReverseDirectionHysteresisOffsetInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetReverseDirectionHysteresisOffsetInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param requestedNanometersPerPulse
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetValidPositionPulseRateInNanometersPerPulse(int motionControllerId, int requestedNanometersPerPulse) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetValidPositionPulseRateInNanometersPerPulse");
    try
    {
      return nativeGetValidPositionPulseRateInNanometersPerPulse(motionControllerId, requestedNanometersPerPulse);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetValidPositionPulseRateInNanometersPerPulse): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @return
   * @throws RemoteException
   */
  public boolean remoteIsInitialized(int motionControllerModel, int motionControllerId) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteIsInitialized");
    return nativeIsInitialized(motionControllerModel, motionControllerId);    
  }

  /**
   * @param motionControllerId
   * @param xAxisScanStepStartPositionInNanometers
   * @param scanStepDirection
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetMaximumScanStepWidthInNanometers(int motionControllerId, int xAxisScanStepStartPositionInNanometers, int scanStepDirection) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMaximumScanStepWidthInNanometers");
    try
    {
      return nativeGetMaximumScanStepWidthInNanometers(motionControllerId, xAxisScanStepStartPositionInNanometers, scanStepDirection);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMaximumScanStepWidthInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetMinimumScanStepWidthInNanometers(int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMinimumScanStepWidthInNanometers");
    try
    {
      return nativeGetMinimumScanStepWidthInNanometers(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMinimumScanStepWidthInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetServiceModeConfiguration(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetServiceModeConfiguration");
    try
    {
      nativeSetServiceModeConfiguration(motionControllerModel, motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetServiceModeConfiguration): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param _motionControllerId
   * @param scanPassDirection
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetYaxisScanStartPositionLimitInNanometers(int _motionControllerId, int scanPassDirection) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetYaxisScanStartPositionLimitInNanometers");
    try
    {
      return nativeGetYaxisScanStartPositionLimitInNanometers(_motionControllerId, scanPassDirection);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetYaxisScanStartPositionLimitInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @param key
   * @param parameters
   * @throws RemoteException
   */
  public Serializable remoteGetServiceModeConfigurationStatus(int motionControllerModel, int motionControllerId, Object key,  ArrayList<String> parameters) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteGetServiceModeConfigurationStatus");

    nativeGetServiceModeConfigurationStatus(motionControllerModel, motionControllerId, key, parameters);

    return new MotionControlServiceModeConfigurationStatus(key.toString(),parameters);
  }

  /**
   * @param motionControllerModel
   * @param motionControllerId
   * @throws RemoteException
   */
  public void remoteAbortServiceModeConfiguration(int motionControllerModel, int motionControllerId) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteAbortServiceModeConfiguration");
    nativeAbortServiceModeConfiguration(motionControllerModel, motionControllerId);
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param actualPositionInNanometers
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetActualPositionInNanometers(int motionControllerId,  int motionAxisToMove, int actualPositionInNanometers) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetActualPositionInNanometers");
    try
    {
      nativeSetActualPositionInNanometers(motionControllerId,  motionAxisToMove, actualPositionInNanometers);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetActualPositionInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @throws RemoteException
   */
  public void remoteShutdown(int motionControllerId, int motionControllerModel) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteShutdown");
    nativeShutdown(motionControllerId, motionControllerModel);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @throws RemoteException
   */
  public void remoteSetSimulationMode(int motionControllerId, int motionControllerModel) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteSetSimulationMode");
    nativeSetSimulationMode(motionControllerId, motionControllerModel);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @param motionAxisToMove
   * @throws RemoteException
   */
  public void remoteSetSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteSetSimulationMode");
    nativeSetSimulationMode(motionControllerId, motionControllerModel, motionAxisToMove);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @throws RemoteException
   */
  public void remoteClearSimulationMode(int motionControllerId, int motionControllerModel) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteSetSimulationMode");
    nativeClearSimulationMode(motionControllerId, motionControllerModel);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @param motionAxisToMove
   * @throws RemoteException
   */
  public void remoteClearSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteClearSimulationMode");
    nativeClearSimulationMode(motionControllerId, motionControllerModel, motionAxisToMove);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @return
   * @throws RemoteException
   */
  public boolean remoteIsSimulationModeOn(int motionControllerId, int motionControllerModel) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteIsSimulationModeOn");
    return nativeIsSimulationModeOn(motionControllerId, motionControllerModel);
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   */
  public boolean remoteIsSimulationModeOn(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteIsSimulationModeOn");
    return nativeIsSimulationModeOn(motionControllerId, motionControllerModel, motionAxisToMove);
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @param motionProfileTypeId
   * @param motionProfile
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public String remoteGetActiveMotionProfile(int motionControllerId, int motionAxisToMove, IntegerRef motionProfileTypeId, IntegerRef motionProfile) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetActiveMotionProfile");
    try
    {
      nativeGetActiveMotionProfile(motionControllerId, motionAxisToMove, motionProfileTypeId, motionProfile);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetActiveMotionProfile): " + ne.getMessage());
      throw ne;
    }

    return motionProfileTypeId.getValue() + "," + motionProfile.getValue();
  }

  /**
   * @param motionControllerId
   * @param motionControllerModel
   * @param hardwareResetModeOn
   * @throws RemoteException
   */
  public void remoteSetHardwareResetMode(int motionControllerId, int motionControllerModel, boolean hardwareResetModeOn) throws RemoteException
  {
    vprint("RemoteMotionControl.remoteSetHardwareResetMode");
    nativeSetHardwareResetMode(motionControllerId, motionControllerModel, hardwareResetModeOn);    
  }

  /**
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remotePauseScanPath(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remotePauseScanPath");

    try
    {
      nativePauseScanPath(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remotePauseScanPath): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetPositionAccuracyToleranceInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetPositionAccuracyToleranceInNanometers");
    try
    {
      return nativeGetPositionAccuracyToleranceInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetPositionAccuracyToleranceInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public boolean remoteIsHomingRequired(int motionControllerId, int motionAxisToMove)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteIsHomingRequired");
    try
    {
      return nativeIsHomingRequired(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteIsHomingRequired): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteDisableScanPath(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteDisableScanPath");
    try
    {
      nativeDisableScanPath(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteDisableScanPath): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param scanPath
   * @throws RemoteException
   * @throws NativeHardwareException
   * Swee Yee Wong - Include starting pulse information for scan pass adjustment
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   */
  public void remoteEnableScanPath(int motionControllerId, int[] scanPath, int distanceInNanometersPerPulse, int numberOfStartPulses)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteEnableScanPath");
    try
    {
      nativeEnableScanPath(motionControllerId, scanPath, distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteEnableScanPath): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetActualPositionInNanometers(int motionControllerId, int motionAxisToMove) 
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetActualPositionInNanometers");
    try
    {
      return nativeGetActualPositionInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetActualPositionInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetMaximumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMaximumPositionLimitInNanometers");
    try
    {
      return nativeGetMaximumPositionLimitInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMaximumPositionLimitInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  public int remoteGetMaximumScanPassLengthInNanometers(int motionControllerId,
    int yAxisScanPassStartPositionInNanometers, int scanPassDirection)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMaximumScanPassLengthInNanometers");

    try
    {
      return nativeGetMaximumScanPassLengthInNanometers(motionControllerId,    yAxisScanPassStartPositionInNanometers, scanPassDirection);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMaximumScanPassLengthInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetMinimumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMinimumPositionLimitInNanometers");
    try
    {
      return nativeGetMinimumPositionLimitInNanometers(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMinimumPositionLimitInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetMinimumScanPassLengthInNanometers(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetMinimumScanPassLengthInNanometers");
    try
    {
      return nativeGetMinimumScanPassLengthInNanometers(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetMinimumScanPassLengthInNanometers): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetScanPathExecutionTimeInMilliseconds(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteGetScanPathExecutionTimeInMilliseconds");
    try
    {
      return nativeGetScanPathExecutionTimeInMilliseconds(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteGetScanPathExecutionTimeInMilliseconds): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteRunNextScanPass(int motionControllerId)
    throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteRunNextScanPass");
    try
    {
      nativeRunNextScanPass(motionControllerId);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteRunNextScanPass): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param distanceInNanometersPerPulse
   * @param numberOfStartPulses
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetPositionPulse(int motionControllerId, int distanceInNanometersPerPulse, int numberOfStartPulses)
      throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteSetPositionPulse");
    try
    {
      nativeSetPositionPulse(motionControllerId, distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteSetPositionPulse): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param motionControllerId
   * @param motionAxisToMove
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteStop(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteMotionControl.remoteStop");
    try
    {
      nativeStop(motionControllerId, motionAxisToMove);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native ERROR (remoteStop): " + ne.getMessage());
      throw ne;
    }
  }

  //-------------------------------------------------------------------------------------
  //                            Native methods
  //-------------------------------------------------------------------------------------
  private native void nativeInitialize(int motionControllerModel, int motionControllerId) throws NativeHardwareException;
  private native void nativeHome(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native void nativeEnable(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native void nativeDisable(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native void nativeSetPointToPointMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile) throws NativeHardwareException;
  private native void nativePointToPointMove(int motionControllerId, int motionAxisToMove, int xInNanometers, int yInNanometers, int railWidthInNanometers) throws NativeHardwareException;
  private native void nativeSetScanMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile) throws NativeHardwareException;
  private native void nativeEnableScanPath(int motionControllerId, int[] scanPath, int distanceInNanometersPerPulse, int numberOfStartPulses) throws NativeHardwareException;
  private native void nativeDisableScanPath(int motionControllerId) throws NativeHardwareException;
  private native void nativeRunNextScanPass(int motionControllerId) throws NativeHardwareException;
  private native int nativeGetScanPathExecutionTimeInMilliseconds(int motionControllerId) throws NativeHardwareException;
  private native void nativeSetPositionPulse(int motionControllerId, int distanceInNanometersPerPulse, int numberOfStartPulses) throws NativeHardwareException;
  private native void nativeStop(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native int nativeGetMinimumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native int nativeGetMaximumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native int nativeGetMinimumScanPassLengthInNanometers(int motionControllerId) throws NativeHardwareException;
  private native int nativeGetMaximumScanPassLengthInNanometers(int motionControllerId, int yAxisScanPassStartPositionInNanometers, int scanPassDirection) throws NativeHardwareException;
  private native int nativeGetActualPositionInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;

  private native boolean nativeIsScanPathDone(int motionControllerId) throws NativeHardwareException;
  private native boolean nativeIsScanPassDone(int motionControllerId) throws NativeHardwareException;
  private native boolean nativeIsHomeSensorClear(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native void nativeGetConfigurationDescription(int motionControllerId, String parameterGroupSeparator, ArrayList<String> keys, ArrayList<String> parameters) throws NativeHardwareException;
  private native boolean nativeIsAxisEnabled(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native void nativeSetHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove, int hysteresisOffsetInNanometers) throws NativeHardwareException;
  private native int nativeGetForwardDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native int nativeGetReverseDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native int nativeGetValidPositionPulseRateInNanometersPerPulse(int motionControllerId, int requestedNanometersPerPulse) throws NativeHardwareException;
  private native boolean nativeIsInitialized(int motionControllerModel, int motionControllerId);
  private native int nativeGetMinimumScanStepWidthInNanometers(int motionControllerId) throws NativeHardwareException;
  private native int nativeGetMaximumScanStepWidthInNanometers(int motionControllerId, int xAxisScanStepStartPositionInNanometers, int scanStepDirection) throws NativeHardwareException;
  private native void nativeSetServiceModeConfiguration(int motionControllerModel, int motionControllerId) throws NativeHardwareException;
  private native int nativeGetYaxisScanStartPositionLimitInNanometers(int _motionControllerId, int scanPassDirection) throws NativeHardwareException;
  private native void nativeGetServiceModeConfigurationStatus(int motionControllerModel, int motionControllerId, Object key,  ArrayList<String> parameters);
  private native void nativeAbortServiceModeConfiguration(int motionControllerModel, int motionControllerId);
  private native void nativeSetActualPositionInNanometers(int motionControllerId,  int motionAxisToMove, int actualPositionInNanometers) throws NativeHardwareException;
  private native void nativeShutdown(int motionControllerId, int motionControllerModel);
  private native void nativeSetSimulationMode(int motionControllerId, int motionControllerModel);
  private native void nativeSetSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove);
  private native void nativeClearSimulationMode(int motionControllerId, int motionControllerModel);
  private native void nativeClearSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove);
  private native boolean nativeIsSimulationModeOn(int motionControllerId, int motionControllerModel);
  private native boolean nativeIsSimulationModeOn(int motionControllerId, int motionControllerModel, int motionAxisToMove);
  private native void nativeGetActiveMotionProfile(int motionControllerId, int motionAxisToMove, IntegerRef motionProfileTypeId, IntegerRef motionProfile) throws NativeHardwareException;
  private native void nativeSetHardwareResetMode(int motionControllerId, int motionControllerModel, boolean hardwareResetModeOn);
  private native void nativePauseScanPath(int motionControllerId) throws NativeHardwareException;
  private native int nativeGetPositionAccuracyToleranceInNanometers(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;
  private native boolean nativeIsHomingRequired(int motionControllerId, int motionAxisToMove) throws NativeHardwareException;

  /**
   * @param message
   */
  private void vprint(String message)
  {
    if(_debug)
      System.err.println(message);
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */  
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
  
}
