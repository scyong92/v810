package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * This class handles control of a single barcode reader (scanner).
 * @author Bob Balliew
 */
class BarcodeReader extends HardwareObject
{
  private final int _END_OF_STREAM = -1;
  private static final Integer[] _EMPTY =
  {
    (int)'*', (int)'*', (int)'*',
    (int)'E', (int)'M', (int)'P', (int)'T', (int)'Y',
    (int)'*', (int)'*', (int)'*',
  };
  private static final List<Integer> _empty = Arrays.asList(_EMPTY);
  private BarcodeReaderEnum _bcrId;
  private Config _config;
  private Queue<String> _simulationModeTriggersCaptured;
  private Queue<IOException> _simulationModeTriggerExceptions;
  private Queue<Integer> _simulationModeReadingToReturn;
  private Queue<Queue<Integer>> _simulationModeReadingsToReturn;
  private Queue<IOException> _simulationModeCurrentReadingExceptions;
  private Queue<Queue<IOException>> _simulationModeReadingExceptions;
  
  /**
   * Construct a BarcodeReader object.
   * @author Bob Balliew
   */
  BarcodeReader(BarcodeReaderEnum bcrId)
  {
    Assert.expect(bcrId != null);
    _bcrId = bcrId;
    _config = Config.getInstance();
    Assert.expect(_config != null);
    _simulationModeTriggersCaptured = new LinkedList<String>();
    _simulationModeTriggerExceptions = new LinkedList<IOException>();
    _simulationModeReadingToReturn = new LinkedList<Integer>();
    _simulationModeReadingsToReturn = new LinkedList<Queue<Integer>>();
    _simulationModeCurrentReadingExceptions = new LinkedList<IOException>();
    _simulationModeReadingExceptions = new LinkedList<Queue<IOException>>();
  }

  /**
   * The set of methods that start with 'simulationMode' are provided to unit
   * (regression) test the majority of the code without any hardware.
   * The unit test may extablish the exact return for one or more readings.
   * The unit test may force an exception during a trigger or at a specific
   * character read.  The unit test may observe the trigger codes that would be
   * sent to the hardware.
   *
   * If the unit test does NOT set up the simulation mode data the mission mode
   * methods of this class will have default values provided as follows:
   *   all triggers sent are captured (they do not need to be read)
   *   no exception will be thrown during a trigger send
   *   no exception will be thrown during a read
   *   every read will return the string "***EMPTY***"
   *
   * If the unit test attempts to read more trigger codes than were actually
   * send "***NO TRIGGER CODES***" is returned.
   *
   * @author Bob Balliew
   */

  /**
   * Get the current size of the queue of trigger codes captured in hardware
   * simulation mode.
   * @return the number of trigger codes captured and not yet gotten.
   * @author Bob Balliew
   */
  int simulationModeNumberOfTriggerCodesCaptured()
  {
    return _simulationModeTriggersCaptured.size();
  }

  /**
   * Add another trigger to queue of trigger codes captured in hardware
   * simulation mode.
   * @param triggerCode to be added to the queue of tiggerCodes.
   * @author Bob Balliew
   */
  private void simulationModeAddTriggerCodeCaptured(String triggerCode)
  {
    _simulationModeTriggersCaptured.add(triggerCode);
  }

  /**
   * Get (and remove) the next trigger code from the queue of trigger codes
   * captured in hardware simulation mode.
   * @return the next trigger code captured, if none left return "***NO TRIGGER CODES***"
   * @author Bob Balliew
   */
  String simulationModeGetNextTriggerCodeCaptured()
  {
    try
    {
      return _simulationModeTriggersCaptured.remove();
    }
    catch (NoSuchElementException nsee)
    {
      return "***NO TRIGGER CODES***";
    }
  }

  /**
   * Get the current size of the queue of trigger exceptions be be thrown in
   * hardware simulation mode.
   * @return the number of trigger codes captured and not yet gotten.
   * @author Bob Balliew
   */
  int simulationModeNumberOfTriggerExceptions()
  {
    return _simulationModeTriggerExceptions.size();
  }

  /**
   * Add another trigger exception to queue of trigger codes captured in hardware
   * simulation mode.
   * @param exception to be added to the queue of exceptions thrown when
   * sending trigger codess.
   * @author Bob Balliew
   */
  void simulationModeAddTriggerException(IOException exception)
  {
    _simulationModeTriggerExceptions.add(exception);
  }

  /**
   * Get (and remove) the next trigger exception from the queue of trigger
   * exceptions in hardware simulation mode.
   * @return the next trigger code captured, if none left return "***EMPTY***"
   * @author Bob Balliew
   */
  private IOException simulationModeGetNextTriggerException()
  {
    try
    {
      return _simulationModeTriggerExceptions.remove();
    }
    catch (NoSuchElementException nsee)
    {
      return null;
    }
  }

  /**
   * Get the current size of the current of reading, those characters that have
   * not yet been read in hardware simulation mode.
   * @return the number of characters currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadCharactersInCurrentReading()
  {
    return _simulationModeReadingToReturn.size();
  }

  /**
   * Add a character (actually an int) to the current reading.
   * @author Bob Balliew
   */
  void simulationModeAddCharacterToCurrentReading(int nextCharacter)
  {
    _simulationModeReadingToReturn.add(nextCharacter);
  }

  /**
   * Get (and remove) the next character from the current reading in hardware
   * simulation mode.
   * @return the next character to return, if none left return "***EMPTY***"
   * @author Bob Balliew
   */
  private int simulationModeGetNextCharacterOfCurrentReading()
  {
    try
    {
      return _simulationModeReadingToReturn.remove();
    }
    catch (NoSuchElementException nsee)
    {
      return _END_OF_STREAM;
    }
  }

  /**
   * Get the current size of the queue of readings that have not yet been read
   * in hardware simulation mode.
   * @return the number of readings currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadReadings()
  {
    return _simulationModeReadingsToReturn.size();
  }

  /**
   * Add the current reading to the queue of readings to be returned in hardware
   * simulation mode.  The current reading is build (character by character)
   * using the 'simulationModeAddCharacterToReading()' method.
   * @author Bob Balliew
   */
  void simulationModeAddReadingToReadings()
  {
    _simulationModeReadingsToReturn.add(_simulationModeReadingToReturn);
    _simulationModeReadingToReturn = new LinkedList<Integer>();
  }

  /**
   * Get (and remove) the next reading from the queue of readings in hardware
   * simulation mode.
   * @return the next reading to return, if none left return "***EMPTY***"
   * @author Bob Balliew
   */
  private void simulationModeSetCurrentReadingToNextReading()
  {
    try
    {
      _simulationModeReadingToReturn = _simulationModeReadingsToReturn.remove();
    }
    catch (NoSuchElementException nsee)
    {
      _simulationModeReadingToReturn = new LinkedList<Integer>(_empty);
    }
  }

  /**
   * Get the current size of the current of reading exception queue, those
   * exceptions that have not yet been thrown in hardware simulation mode.
   * @return the number of exceptions currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue()
  {
    return _simulationModeCurrentReadingExceptions.size();
  }

  /**
   * Add a character (actually an int) to the current reading.
   * @author Bob Balliew
   */
  void simulationModeAddExceptionToCurrentExceptionQueue(IOException e)
  {
    _simulationModeCurrentReadingExceptions.add(e);
  }

  /**
   * Get (and remove) the next exception from the current exception queue in
   * hardware simulation mode.
   * @return the next exception, if none left return null
   * @author Bob Balliew
   */
  private IOException simulationModeGetNextExceptionFromCurrentExceptionQueue()
  {
    try
    {
      return _simulationModeCurrentReadingExceptions.remove();
    }
    catch (NoSuchElementException nsee)
    {
      return null;
    }
  }

  /**
   * Get the current size of the queue of exception queues that have not yet
   * been processed in hardware simulation mode.
   * @return the number of readings currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadExceptions()
  {
    return _simulationModeReadingExceptions.size();
  }

  /**
   * Add the current exception queue to the queue of exception queues to be
   * processed in hardware simulation mode.  The current exception queue is
   * build (one exception for every character of each reading) using the
   * 'simulationModeAddExceptionToCurrentExceptionQueue()' method.
   * @author Bob Balliew
   */
  void simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues()
  {
    _simulationModeReadingExceptions.add(_simulationModeCurrentReadingExceptions);
    _simulationModeCurrentReadingExceptions = new LinkedList<IOException>();
  }

  /**
   * Get (and remove) the next reading from the queue of readings in hardware
   * simulation mode.
   * @return the next reading to return, if none left return "***EMPTY***"
   * @author Bob Balliew
   */
  private void simulationModeSetCurrentExceptionQueueToNextExceptionQueue()
  {
    try
    {
      _simulationModeCurrentReadingExceptions = _simulationModeReadingExceptions.remove();
    }
    catch (NoSuchElementException nsee)
    {
      _simulationModeCurrentReadingExceptions = new LinkedList<IOException>();
    }
  }

  /**
   * Send trigger and read addressing if multiple barcode readers present.
   * @param writer is the output stream writer connected to the barcode reader.
   * @author Bob Balliew
   */
  private void triggerBcr(OutputStreamWriter writer) throws IOException, HardwareException
  {
    Assert.expect(writer != null);
    String tiggerCode = _config.getStringValue(_bcrId.getTrigger());

    if (isSimulationModeOn())
    {
      simulationModeAddTriggerCodeCaptured(tiggerCode);
      IOException e = simulationModeGetNextTriggerException();
      if (e != null) throw e;
    }
    else
    {
      try
      {
        writer.write(tiggerCode);
        Thread.sleep(3000);
        writer.flush();
      }
      catch(InterruptedException ex)
      {
        //do nothing 
      }
      catch(IOException ex)
      { 
         String portName = BarcodeReaderManager.getInstance().getPortName();
         String message = ex.getMessage();

         if (message != null)
           throw new IOHardwareException(portName, message);
         else
           throw new IOHardwareException(portName);
      }
    }
  }

  /**
   * Read the barcode.
   * @param reader is the input stream reader connected to the barcode reader.
   * @return the barcode read.
   * @author Bob Balliew
   */
  private String readBcr(InputStreamReader reader) throws IOException
  {
    Assert.expect(reader != null);
    clearAbort();
    final int BARCODE_BUFFER_INITIAL_CAPACITY = 128;
    StringBuffer codeRead = new StringBuffer(BARCODE_BUFFER_INITIAL_CAPACITY);
    final String postamble = _config.getStringValue(_bcrId.getPostamble());

    if (isSimulationModeOn())
    {
      simulationModeSetCurrentReadingToNextReading();
      simulationModeSetCurrentExceptionQueueToNextExceptionQueue();
    }

    while (true)
    {
      int x = _END_OF_STREAM;

      try
      {
        if (isSimulationModeOn())
        {
          x = simulationModeGetNextCharacterOfCurrentReading();
          IOException e = simulationModeGetNextExceptionFromCurrentExceptionQueue();

          if (e != null) throw e;
        }
        else
        {
          x = reader.read();
        }
      }
      catch (EOFException eof)
      {
        x = _END_OF_STREAM;
      }

      if (x == _END_OF_STREAM)  break;

      if (isAborting())
      {
        codeRead.append("Abort detected during readBcr");
        break;
      }

      Assert.expect(x >= Character.MIN_VALUE);
      Assert.expect(x <= Character.MAX_VALUE);
      codeRead.append((char)x);

      if ((postamble.length() > 0) && codeRead.toString().endsWith(postamble))  break;
      if(reader.ready() == false) break;
    }

    return codeRead.toString();
  }

  /**
   * Remove the preamble from the specified barcode.
   * @param barcode is the string of characters returned from the barcode reader.
   * @return the barcode without the preamble.
   * @author Bob Balliew
   */
  private String removePreamble(String barcode) throws PreambleNotFoundHardwareException
  {
    final String preamble = _config.getStringValue(_bcrId.getPreamble());

    if (barcode.startsWith(preamble))
      return barcode.substring(preamble.length());
    else
      throw new PreambleNotFoundHardwareException(preamble, barcode, _bcrId.getOrder());
  }

  /**
   * Remove the preamble from the specified barcode.
   * @param barcode is the string of characters returned from the barcode reader.
   * @return the barcode without the preamble.
   * @author Bob Balliew
   */
  private String removePostamble(String barcode) throws PostambleNotFoundHardwareException
  {
    final String postamble = _config.getStringValue(_bcrId.getPostamble());

    if (barcode.endsWith(postamble))
      return barcode.substring(0, (barcode.length() - postamble.length()));
    else
      throw new PostambleNotFoundHardwareException(postamble, barcode, _bcrId.getOrder());
  }

  /**
   * @return the barcode reader id.
   * @author Bob Balliew
   */
  BarcodeReaderEnum getId()
  {
    return _bcrId;
  }

  /**
   * Send trigger and read addressing if multiple barcode readers present.
   * Convert any exceptions into a HardwareException subclass.
   * @param writer is the output stream writer connected to the barcode reader.
   * @author Bob Balliew
   */
  void trigger(OutputStreamWriter writer) throws HardwareException
  {
    try
    {
      triggerBcr(writer);
    }
    catch (InterruptedIOException iioe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = iioe.getMessage();

      if (message != null)
        throw new InterruptedIOHardwareException(portName, iioe.bytesTransferred, message);
      else
        throw new InterruptedIOHardwareException(portName, iioe.bytesTransferred);
    }
    catch (SyncFailedException sfe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = sfe.getMessage();

      if (message != null)
        throw new SyncFailedHardwareException(portName, message);
      else
        throw new SyncFailedHardwareException(portName);
    }
    catch (UnsupportedEncodingException uee)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = uee.getMessage();

      if (message != null)
        throw new UnsupportedEncodingHardwareException(portName, message);
      else
        throw new UnsupportedEncodingHardwareException(portName);
    }
    catch (IOException ioe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = ioe.getMessage();

      if (message != null)
        throw new IOHardwareException(portName, message);
      else
        throw new IOHardwareException(portName);
    }
  }

  /**
   * Send trigger and read addressing if multiple barcode readers present.
   * Convert any exceptions into a string to return.
   * @todo REB make deprecated This method should not be used.
   * @param writer is the output stream writer connected to the barcode reader.
   * @return the short description of any Throwable, if any exception encounterd
   * otherwise the empty string.
   * @author Bob Balliew
   */
  String triggerReturnErrors(OutputStreamWriter writer)
  {
    try
    {
      trigger(writer);
      return "";
    }
    catch (Exception e)
    {
      return e.toString();
    }
  }

  /**
   * Read the barcode.  Ccheck for error returns and throw an exception.
   * Remove preamble (if applicable). Remove the postamble (if applicable).
   * @param reader is the input stream reader connected to the barcode reader.
   * @return the barcode read.
   * @author Bob Balliew
   */
  String read(InputStreamReader reader) throws HardwareException
  {
    try
    {
      return removePostamble(removePreamble(readBcr(reader)));
    }
    catch (InterruptedIOException iioe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = iioe.getMessage();

      if (message != null)
        throw new InterruptedIOHardwareException(portName, iioe.bytesTransferred, message);
      else
        throw new InterruptedIOHardwareException(portName, iioe.bytesTransferred);
    }
    catch (SyncFailedException sfe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = sfe.getMessage();

      if (message != null)
        throw new SyncFailedHardwareException(portName, message);
      else
        throw new SyncFailedHardwareException(portName);
    }
    catch (UnsupportedEncodingException uee)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = uee.getMessage();

      if (message != null)
        throw new UnsupportedEncodingHardwareException(portName, message);
      else
        throw new UnsupportedEncodingHardwareException(portName);
    }
    catch (IOException ioe)
    {
      String portName = BarcodeReaderManager.getInstance().getPortName();
      String message = ioe.getMessage();

      if (message != null)
        throw new IOHardwareException(portName, message);
      else
        throw new IOHardwareException(portName);
    }
  }

  /**
   * Read the barcode.  Remove preamble (if applicable). Remove the postamble
   * (if applicable).  The purpose of this routine is to return the maximum
   * amount of diagnostic information to aid the user when troubleshooting
   * problems with a barcode reader configuration.
   * @todo REB make deprecated This method should not be used.
   * @param reader is the input stream reader connected to the barcode reader.
   * @return the barcode read.
   * @author Bob Balliew
   */
  String readReturnErrors(InputStreamReader reader)
  {
    try
    {
      return read(reader);
    }
    catch (Exception e)
    {
      return e.toString();
    }
  }
  
  /*
   * End Barcode reader read cycle
   * @author Phang Siew Yeng
   */
  void stopReadCycle(OutputStreamWriter writer)
  {
    try
    {
      String stopCode = _config.getStringValue(_bcrId.getStop());
      
      //Siew Yeng - do not send if it is null
      if(stopCode.isEmpty())
        return;
      
      writer.write(stopCode);
      writer.flush();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
}
