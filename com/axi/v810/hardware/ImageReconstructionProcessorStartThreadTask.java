package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class ImageReconstructionProcessorStartThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionProcessor _imageReconstructionProcessor;
  private boolean _cancelled;

  /**
   * @author Rex Shang
   */
  public ImageReconstructionProcessorStartThreadTask(ImageReconstructionProcessor imageReconstructionProcessor)
  {
    super("ImageReconstructionEngineStartupThreadTask: " + imageReconstructionProcessor.getId());

    Assert.expect(imageReconstructionProcessor != null);

    _imageReconstructionProcessor = imageReconstructionProcessor;
    _cancelled = false;
  }

  /**
   * @author Rex Shang
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    _imageReconstructionProcessor.startImageReconstructionProcessor();

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    _cancelled = false;
  }

  /**
   * @author Rex Shang
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
