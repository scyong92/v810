package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Chong, Wei Chin
 */
public class RemoteMotionServerHardwareException extends HardwareException
{
  private RemoteMotionServerHardwareExceptionEnum _type;

  /**
   * Construct a new exception based on an enum.
   * @author Chong, Wei Chin
   */
  private RemoteMotionServerHardwareException(RemoteMotionServerHardwareExceptionEnum exceptionEnum, LocalizedString message)
  {
    super(message);
    Assert.expect(message != null);

    _type = exceptionEnum;
  }

  /**
   * Construct a new exception based on an enum.
   * @author Chong, Wei Chin
   */
  private RemoteMotionServerHardwareException(RemoteMotionServerHardwareExceptionEnum exceptionEnum, String key, Object[] parameters)
  {
    this(exceptionEnum, new LocalizedString(key, parameters));

    Assert.expect(exceptionEnum != null, "exceptionEnum is null.");
    Assert.expect(key != null, "key string is null.");
  }

  /**
   * @author Chong, Wei Chin
   */
  public RemoteMotionServerHardwareExceptionEnum getType()
  {
    return _type;
  }

  /**
   * @author Chong, Wei Chin
   */
  static RemoteMotionServerHardwareException getApplicationVersionMismatchException()
  {
    RemoteMotionServerHardwareException exception =
      new RemoteMotionServerHardwareException(RemoteMotionServerHardwareExceptionEnum.RMS_APPLICATION_VERSION_MISMATCH,
                                                        new LocalizedString("HW_REMOTE_MOTION_SERVER_APPLICATION_VERSION_MISMATCH_EXCEPTION_KEY", null));
    return exception;
  }

  /**
   * @author Chong, Wei Chin
   */
  static RemoteMotionServerHardwareException getFailedToRebootRMSHardwareException(String serverIpAddress)
  {
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {serverIpAddress};

    RemoteMotionServerHardwareException exception = new RemoteMotionServerHardwareException(
        RemoteMotionServerHardwareExceptionEnum.FAILED_TO_REBOOT_RMS_HARDWARE,
        "HW_FAILED_REBOOTING_REMOTE_MOTION_SERVER_HARDWARE_KEY",
        exceptionParameters);
    return exception;
  }

  /**
   * @author Chong Wei Chin
   */
  static RemoteMotionServerHardwareException getFailedToStartRMSApplicationException(String serverIpAddress)
  {
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {serverIpAddress};

    RemoteMotionServerHardwareException exception = new RemoteMotionServerHardwareException(
        RemoteMotionServerHardwareExceptionEnum.FAILED_TO_START_RMS_APPLICATION,
        "HW_FAILED_STARTING_REMOTE_MOTION_SERVER_KEY",
        exceptionParameters);
    return exception;
  }

  /**
   * @author Chong Wei Chin
   */
  static RemoteMotionServerHardwareException getFailedToStopRMSApplicationException(String serverIpAddress)
  {
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {serverIpAddress};

    RemoteMotionServerHardwareException exception = new RemoteMotionServerHardwareException(
        RemoteMotionServerHardwareExceptionEnum.FAILED_TO_STOP_RMS_APPLICATION,
        "HW_FAILED_STOPPING_REMOTE_MOTION_SERVER_KEY",
        exceptionParameters);
    return exception;
  }

  /**
   * @author Chong, Wei Chin
   */
  static RemoteMotionServerHardwareException getFailedToCommunicateWithRMSException(String serverIpAddress, Exception causedBy)
  {
    Assert.expect(serverIpAddress != null, "serverIpAddress is null.");

    String[] exceptionParameters = new String[]
                                   {serverIpAddress};

    RemoteMotionServerHardwareException exception = new RemoteMotionServerHardwareException(
        RemoteMotionServerHardwareExceptionEnum.FAILED_TO_COMMUNICATE_RMS,
        "HW_FAILED_COMMUNICATION_REMOTE_MOTION_SERVER_KEY",
        exceptionParameters);

    exception.initCause(causedBy);
    return exception;
  }
}
