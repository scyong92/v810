package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class Test_XrayCameraArray extends UnitTest
{
  private static final String _EMPTY_STRING = "";
  private XrayCameraArray _xRayCameraArray;
  private Config _config;
  private boolean _debugMode;
  private HardwareWorkerThread _hardwareWorkerThread;
  private String[] _args;

  /**
   * @author Greg Esparza
   */
  Test_XrayCameraArray(String[] args)
  {
    _args = args;
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();

    try
    {
      _config = Config.getInstance();
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void getKeyboardCommand(String commandMessage) throws IOException
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String keyboardString = "";

    do
    {
      System.out.println();
      System.out.println(commandMessage);

      keyboardString = br.readLine();
      if (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false)
      {
        System.out.println("Invalid key!");
      }
    }
    while (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false);
 }

  /**
    * @author Greg Esparza
    */
  private void checkArgs()
  {
    if (_args != null)
    {
      try
      {
        setDebugMode();
      }
      catch (HardwareException he)
      {
        System.out.println(he);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setDebugMode() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-debugMode"))
      {
        argFound = true;
        _debugMode = true;
        break;
      }
    }

    if (argFound == false)
      _debugMode = false;
  }

  /**
   * @author Greg Esparza
   */
  private void testStartup() throws XrayTesterException
  {
    TimerUtil timerUtil = new TimerUtil();

    timerUtil.reset();
    timerUtil.start();
    _xRayCameraArray.startup();
    timerUtil.stop();

    long startUpTime = timerUtil.getElapsedTimeInMillis();
    //System.out.println("Start up time = " + String.valueOf(startUpTime) + " milliseconds");
  }

  /**
   * @author Greg Esparza
   */
  private void testEnableUnitTestMode() throws XrayTesterException
  {
    String baseDirectory = getTestDataDir();
    String testDirectory = baseDirectory + Directory.getXrayCameraDir();
    _xRayCameraArray.enableUnitTestMode(testDirectory);
  }

  /**
   * @author Greg Esparza
   */
  private void testDisableUnitTestMode() throws XrayTesterException
  {
    _xRayCameraArray.disableUnitTestMode();
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException, IOException, BadFormatException
        {
          checkArgs();

           if (_debugMode)
             getKeyboardCommand("DEBUG MODE - Press <ENTER> when ready to start debug session ...");

          _xRayCameraArray = XrayCameraArray.getInstance();
          testEnableUnitTestMode();
          testStartup();
          testDisableUnitTestMode();
        }
      });
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    finally
    {
      try
      {
        testDisableUnitTestMode();
      }
      catch (XrayTesterException xte)
      {
        xte.printStackTrace();
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_XrayCameraArray(args));
  }
}
