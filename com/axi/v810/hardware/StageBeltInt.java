package com.axi.v810.hardware;

import com.axi.v810.util.XrayTesterException;

/**
 * @author Rex Shang
 */
interface StageBeltInt
{
  void on() throws XrayTesterException;

  void off() throws XrayTesterException;
}
