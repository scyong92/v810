package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;

/**
 * The resolution controls how many pixels are combined into a single pixel.  The
 * xray camera can do this by averaging pixels together and making a single pixel from the average.
 * This results in higher resolution.
 *
 * <pre>
 * The XrayCamera supports two modes.
 * 1024 pixels per image (2 pixels by 2 pixels are averaged into 1 pixel)
 * 512  pixels per image (4 pixels by 4 pixels are averaged into 1 pixel)
 * </pre>
 *
 * @author Bill Darbie
 */
public class XrayCameraResolutionEnum extends com.axi.util.Enum implements Serializable
{
  public static final XrayCameraResolutionEnum PIXELS_512x512 = new XrayCameraResolutionEnum(512);
  public static final XrayCameraResolutionEnum PIXELS_1024x1024 = new XrayCameraResolutionEnum(1024);

  /**
   * @author Bill Darbie
   */
  private XrayCameraResolutionEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  public int getPixels()
  {
    return getId();
  }
}
