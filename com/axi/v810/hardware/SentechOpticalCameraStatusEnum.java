package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Jack Hwee
 */
public class SentechOpticalCameraStatusEnum extends com.axi.util.Enum
{
    private static Map<Integer, String> _statusStringToOpticalCameraStatusEnumMap = new HashMap<Integer, String>();
    
    public static final SentechOpticalCameraStatusEnum IS_NO_SUCCESS                               = new SentechOpticalCameraStatusEnum(-1, "IS_NO_SUCCESS");
    public static final SentechOpticalCameraStatusEnum IS_SUCCESS                                  = new SentechOpticalCameraStatusEnum(0, "IS_SUCCESS");
    public static final SentechOpticalCameraStatusEnum IS_INVALID_CAMERA_HANDLE                    = new SentechOpticalCameraStatusEnum(1, "IS_INVALID_CAMERA_HANDLE");
    public static final SentechOpticalCameraStatusEnum IS_CANT_OPEN_DEVICE                         = new SentechOpticalCameraStatusEnum(3, "IS_CANT_OPEN_DEVICE");
    public static final SentechOpticalCameraStatusEnum IS_CANT_CLOSE_DEVICE                        = new SentechOpticalCameraStatusEnum(4, "IS_CANT_CLOSE_DEVICE");
    
    /**
     * @author Cheah Lee Herng
    */
    private SentechOpticalCameraStatusEnum(int id, String statusString)
    {
        super(id);
        Object previous = _statusStringToOpticalCameraStatusEnumMap.put(id, statusString);
        Assert.expect(previous == null);                
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public static String getStatusName(int id)
    {
        Assert.expect(_statusStringToOpticalCameraStatusEnumMap != null);
        String statusName = _statusStringToOpticalCameraStatusEnumMap.get(id);
        Assert.expect(statusName != null);
        
        return statusName;
    }
}
