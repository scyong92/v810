package com.axi.v810.hardware;

/**
 * Used by InterlockHardwareException to help populate the exception message.
 * @author Rex Shang
 */
public class InterlockHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static InterlockHardwareExceptionEnum INTERLOCK_IS_NOT_ON = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INTERLOCK_CHAIN_IS_OPEN = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INTERLOCK_CHAIN_1_IS_OPEN = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INTERLOCK_CHAIN_2_IS_OPEN = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INTERLOCK_IS_IN_SERVICE_MODE = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INVALID_HARDWARE_REVISION = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum HARDWARE_REVISION_MISMATCH = new InterlockHardwareExceptionEnum(++_index);
  public static InterlockHardwareExceptionEnum INVALID_DIAGNOSTICS_CODE = new InterlockHardwareExceptionEnum(++_index);

  /**
   * @author Rex Shang
   */
  private InterlockHardwareExceptionEnum(int id)
  {
    super(id);
  }

}
