package com.axi.v810.hardware;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;

/**
 * @author lee-herng.cheah
 */
public abstract class AbstractProjector  extends HardwareObject
    implements Simulatable, HardwareStartupShutdownInt
{
    private static AbstractProjector _instance;
    protected static ProgressObservable _progressObservable;
    protected static HardwareObservable _hardwareObservable;
    protected static FringePatternObservable _fringePatternObservable;

    protected boolean _initialized;
    protected HardwareTaskEngine _hardwareTaskEngine;

    private static IntegerRef _startupProgressStateCount;
    protected BooleanLock _finishDisplayFringePatternLock = new BooleanLock(false);

    static
    {
        _progressObservable = ProgressObservable.getInstance();
        _startupProgressStateCount = new IntegerRef();
        _fringePatternObservable = FringePatternObservable.getInstance();
    }

    protected boolean _startupRequired = true;

    /**
     * @author Cheah Lee Herng
     */
    protected AbstractProjector()
    {
        _hardwareObservable = HardwareObservable.getInstance();
        _hardwareTaskEngine = HardwareTaskEngine.getInstance();        
    }

    /**
     * @author Cheah Lee Herng
     */
    public static synchronized AbstractProjector getInstance()
    {
        if (_instance == null)
        {
            _instance = new DlpProjector();
        }
        return _instance;
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        return _startupRequired;
    }

    /**
     * @author Cheah Lee Herng
    */
    protected static void setStartupBeginProgressState()
    {
        synchronized (_startupProgressStateCount)
        {
          _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() + 1);
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    protected static void setStartupEndProgressState()
    {
        synchronized(_startupProgressStateCount)
        {
          _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() - 1);

          try
          {
            if (_startupProgressStateCount.getValue() > 0)
            {
              _startupProgressStateCount.wait();
            }
            else
            {
              _startupProgressStateCount.notifyAll();
            }
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public static FringePatternObservable getFringePatternObservable()
    {
        Assert.expect(_fringePatternObservable != null);
        return _fringePatternObservable;
    }
    
    /**
     * Tell projector to display white light from projector source.
     * 
     * @author Cheah Lee Herng
     */
    public abstract void projectWhiteLight(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector to display white light from projector source.
     * This is a non-blocking function.
     * 
     * @author Cheah Lee Herng
     */
    public abstract void projectWhiteLightWithoutBlocking(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector to display Fringe Pattern Shift One from projector source.
     * 
     * @author Cheah Lee Herng 
     */
    public abstract void projectFringePatternShiftOne(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector to display Fringe Pattern Shift Two from projector source.
     * 
     * @author Cheah Lee Herng 
     */
    public abstract void projectFringePatternShiftTwo(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector to display Fringe Pattern Shift Three from projector source.
     * 
     * @author Cheah Lee Herng 
     */
    public abstract void projectFringePatternShiftThree(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector to display Fringe Pattern Shift Four from projector source.
     * 
     * @author Cheah Lee Herng 
     */
    public abstract void projectFringePatternShiftFour(OpticalCameraIdEnum cameraId) throws XrayTesterException;
    
    /**
     * Tell projector that we are done with displaying Fringe Pattern. This means that
     * the fringe pattern is completely displayed on screen.
     * 
     * @author Cheah Lee Herng
     */
    public abstract void finishDisplayFringePattern();
    
    /**
     * Perform projector initialization.
     * 
     * @author Cheah Lee Herng 
     */
    public abstract void initialize() throws XrayTesterException;
}
