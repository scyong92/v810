package com.axi.v810.hardware;

import com.axi.v810.util.*;

//Variable Mag Anthony August 2011
/**
 * This interface will be implemented by all the Safety Level objects.
 * The Safety Level Sensor on the machine are there to prevent tall component from knocking on the X-rays Tube.
 * @author Anthony Fong
 */
public interface SafetyLevelSensorInt
{ 
  int _SAFETY_SENSOR_POLLING_INTERVAL_IN_MILLI_SECONDS = 500;
  /**
   * @return true if the Safety Level is OK.
   * @author Anthony Fong
   */
  abstract boolean isOK() throws XrayTesterException;
  
  /**
   * @return 
   * @author Wei Chin
   */
  abstract boolean getLastSafetyStatus();
  
  /**
   * @author Wei Chin
   */
  abstract public void startMonitorStatus();
  
  /**
   * @author Wei Chin
   */
  abstract public void stopMonitorStatus();
}
