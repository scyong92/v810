package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port does not support receive framing, then throw this exception.
 *
 * @author Bob Balliew
 */
class NoReceiveFramingSupportHardwareException extends HardwareException
{
  /**
   * This is for receive framing failure on the serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Bob Balliew
   */
  public NoReceiveFramingSupportHardwareException(String portName)
  {
    super(new LocalizedString("HW_NO_RECEIVE_FRAMING_SUPPORT_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }
}
