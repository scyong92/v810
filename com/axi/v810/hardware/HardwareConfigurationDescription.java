package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class is used to capture native hardware information such as
 * revision id, manufacture information, etc.
 * The key and parameters are usually used to localize the information.
 * @author Greg Esparza
 */
class HardwareConfigurationDescription implements HardwareConfigurationDescriptionInt, Serializable
{
  private String _key;
  private List<String> _parameters;

  /**
   * @author Greg Esparza
   */
  HardwareConfigurationDescription(String key, List<String> parameters)
  {
    Assert.expect(key != null);
    Assert.expect(parameters != null);

    _key = key;
    _parameters = parameters;
  }

  /**
   * @author Greg Esparza
   */
  public String getKey()
  {
    return _key;
  }

  /**
   * @author Greg Esparza
   */
  public List<String> getParameters()
  {
    return _parameters;
  }
}
