package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Greg Loring
 */
public class Test_LegacyXraySource extends UnitTest
{
  private Config _config = Config.getInstance();

  /**
   * @author Greg Loring
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LegacyXraySource());
  }

  /**
   * @author Greg Loring
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      try
      {
        _config.loadIfNecessary();
      }
      catch (DatastoreException de)
      {
        de.printStackTrace();
        Assert.expect(false);
      }

      // is the factory method configured to produce an object that i can test?
      String type = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE);
      if (!type.equals("legacy"))
      {
        out.println("can't test a " + type + " XraySource, dropping out");
        return;
      }

      AbstractXraySource source = AbstractXraySource.getInstance();
      Expect.expect(source != null);
      Expect.expect(source == AbstractXraySource.getInstance());
      Expect.expect(source instanceof LegacyXraySource);
      LegacyXraySource legacySource = (LegacyXraySource) source;

      // must do the
      if (   XrayTester.isHardwareAvailable()
          && (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == false))
      {
        doNormalUsageTests(legacySource, in, out);
      }

      legacySource.setSimulationMode();
      Expect.expect(legacySource.isSimulationModeOn());
      doSimModeTests(legacySource, in, out);
      legacySource.clearSimulationMode();

      //In order to get an accurate reading of legacySource sim mode OFF, we need
      //to remove the confounder of HARDWARE_SIMULATION mode because it is a logical OR
      //of the two values
      boolean wasInHardwareSimMode = _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
      try
      {
        //If hardware sim is on, turn it off
        if(wasInHardwareSimMode)
        {
          _config.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, false);
        }
        //Now we should see that the legacy source is not in sim mode
        Expect.expect(legacySource.isSimulationModeOn() == false);
      }
      //No matter what happened, restore Hardware sim to the previous state
      finally
      {
        if(wasInHardwareSimMode)
        {
          _config.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, true);
        }
      }
    }
    catch(Exception x)
    {
      x.printStackTrace();
    }
  }

  /**
   * @author Greg Loring
   */
  private void doNormalUsageTests(
    LegacyXraySource legacySource, BufferedReader in, PrintWriter out
  ) throws Exception
  {
    Expect.expect(!legacySource.areXraysOn());
    Expect.expect(!legacySource.couldUnsafeXraysBeEmitted());

    // test setters, startup(), and on() - all of the setters are used as part of startup()
    legacySource.startup();
    legacySource.on();
    Expect.expect(legacySource.areXraysOn());
    Expect.expect(legacySource.couldUnsafeXraysBeEmitted());
    //
    // run all of the getters looking very coarsely for some expected nominal values
    /** @todo check this stuff with the hardware guys */
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getAnodeVoltageInKiloVolts(), 160.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(), 100.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFilamentVoltageInVolts(), 5.5, 1.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFocusVoltageInVolts(), 2200.0, 100.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGridVoltageInVolts(), -250.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGunTemperatureInDegreesCelcius(), 100.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getTotalCurrentInMicroAmps(), 149.7, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getIsolatedSupplyVoltageInVolts(), 1.5, 1.0));

    // test off()
    legacySource.off();
    Expect.expect(!legacySource.areXraysOn());
    Expect.expect(legacySource.couldUnsafeXraysBeEmitted());
    //
    // run all of the getters looking very coarsely for some expected nominal values
    /** @todo check this stuff with the hardware guys */
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getAnodeVoltageInKiloVolts(), 160.0, 10.0));
//    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(), 0.0, 11.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(), 100.0, 11.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFilamentVoltageInVolts(), 5.5, 1.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFocusVoltageInVolts(), 2200.0, 100.0));
//    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGridVoltageInVolts(), -500.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGridVoltageInVolts(), -250.0, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGunTemperatureInDegreesCelcius(), 100.0, 10.0));
//    Expect.expect(MathUtil.fuzzyEquals(legacySource.getTotalCurrentInMicroAmps(), 49.7, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getTotalCurrentInMicroAmps(), 149.7, 10.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getIsolatedSupplyVoltageInVolts(), 1.5, 1.0));

    // just for code coverage, corner cases, etc.
    legacySource.on();
    legacySource.off();

    // test powerOff()
    legacySource.powerOff();
    Expect.expect(!legacySource.areXraysOn());
    Expect.expect(!legacySource.couldUnsafeXraysBeEmitted());
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getAnodeVoltageInKiloVolts(), 0.0, 5.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(), 0.0, 11.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFilamentVoltageInVolts(), 0.1, 0.1));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFocusVoltageInVolts(), 500.0, 10.0));
  }

  private void doSimModeTests(LegacyXraySource legacySource,
                              BufferedReader in,
                              PrintWriter out) throws Exception
  {
    Expect.expect(legacySource.getFirmwareRevisionCode().equals("1000"));

    // test hardware reported errors methods (must clear them out before any more tests!)
    XraySourceHardwareReportedErrorsException xshrex = legacySource.getHardwareReportedErrors();
//    Expect.expect(xshrex != null);
//    String errors = xshrex.getMessage();
//    // wow! this is a highly improbable set of errors, btw
//    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   == -1);
//    Expect.expect(errors.indexOf("Interlock 1 Off")  != -1);
//    Expect.expect(errors.indexOf("Interlock 2 Off")  != -1);
//    Expect.expect(errors.indexOf("Interlock 3 Off")  != -1);
//    Expect.expect(errors.indexOf("Regulation Error") == -1);
//    Expect.expect(errors.indexOf("Arc Error")        == -1);
//    Expect.expect(errors.indexOf("Over Voltage")     == -1);
//    Expect.expect(errors.indexOf("Over Current")     == -1);
//    Expect.expect(errors.indexOf("Temperature Limit") == -1);
//    Expect.expect(errors.indexOf("Communication error on fiber link") == -1);
//    legacySource.resetHardwareReportedErrors();
    Expect.expect(legacySource.getHardwareReportedErrors() == null);

    // test getters (rounding error is .5 * unitScale / cmdScale[read])
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getAnodeVoltageInKiloVolts(),         3.75, 0.02));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(),       16.6, 0.1));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFilamentVoltageInVolts(),         0.758, 0.005));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFocusVoltageInVolts(),            243.0, 2.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGridVoltageInVolts(),             -40.1, 0.3));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGunTemperatureInDegreesCelcius(), 100.0, 1.0));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getTotalCurrentInMicroAmps(),         4.49, 0.03));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getIsolatedSupplyVoltageInVolts(),    1.58, 0.01));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMinimumAnodeVoltageInKiloVolts(),  0.00, 0.02));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMaximumAnodeVoltageInKiloVolts(),  3.83, 0.02));

    // test min/max anode voltage resetter
    legacySource.resetMinimumAndMaximumAnodeVoltage();
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMinimumAnodeVoltageInKiloVolts(),  3.75, 0.02));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMaximumAnodeVoltageInKiloVolts(),  3.75, 0.02));

    // test setters (rounding error is .5 * unitScale / cmdScale[set])
    legacySource.setAnodeVoltageInKiloVolts(3.00);
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getAnodeVoltageInKiloVolts(), 3.00, 0.02));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMinimumAnodeVoltageInKiloVolts(), 3.00, 0.02));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getMaximumAnodeVoltageInKiloVolts(), 3.75, 0.02));
    legacySource.setCathodeCurrentInMicroAmps(15.0); // affects HVPS Current and Grid Voltage too
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getCathodeCurrentInMicroAmps(), 15.0, 0.1));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getGridVoltageInVolts(), (200.-15.)/200.*-500., .3*500./200.));
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getTotalCurrentInMicroAmps(), 15.0+49.7, 0.03*4095/1023));
    legacySource.setFilamentVoltageInVolts(1.0);
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFilamentVoltageInVolts(), 1.0, 0.005));
    // focus voltage has a different rounding error now 'cuz set's command scale is 1/4 as big as read's
    legacySource.setFocusVoltageInVolts(200.0);
    Expect.expect(MathUtil.fuzzyEquals(legacySource.getFocusVoltageInVolts(), 200.0, 6.0));

    doNormalUsageTests(legacySource, in, out);
  }

}
