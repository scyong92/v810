package com.axi.v810.hardware;

/**
 * Used by DigitalIoException to help populate the exception message.
 * @author Rex Shang
 */
public class PanelHandlerExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static PanelHandlerExceptionEnum BOTH_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum LEFT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum RIGHT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum FAILED_TO_RECAPTURE_PANEL = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum INVALID_PANEL_LOADING_MODE = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum INVALID_PANEL_FLOW_DIRECTION = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum FAILED_TO_DETECT_PANEL_CLEAR_SENSOR_STATE_DURING_STARTUP = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum PANEL_IN_PLACE_BUT_PRIMARY_SENSOR_NOT_DETECT_PANEL = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum PANEL_IN_PLACE_NOT_ENGAGED = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum PANEL_WIDTH_PLUS_PANEL_WIDTH_CLEARANCE_EXCEED_MAX_RAIL_WIDTH = new PanelHandlerExceptionEnum(++_index);
  
  //Swee Yee - Reset Optical PIP 2014
  public static PanelHandlerExceptionEnum HW_OPTICAL_PANEL_IN_PLACE_TUNING_REQUIRED = new PanelHandlerExceptionEnum(++_index);

  //Variable Mag Anthony August 2011
  public static PanelHandlerExceptionEnum PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL = new PanelHandlerExceptionEnum(++_index);
  public static PanelHandlerExceptionEnum PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL_MISALIGN_OR_MALFUNCTION = new PanelHandlerExceptionEnum(++_index);
 
  /**
   * @author Rex Shang
   */
  private PanelHandlerExceptionEnum(int id)
  {
    super(id);
  }

}
