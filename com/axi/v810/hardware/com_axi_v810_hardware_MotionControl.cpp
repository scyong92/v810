#include <list>
#include <map>
#include <string>


#include "com/axi/util/jniUtil.h"
#include "com_axi_v810_hardware_MotionControl.h"
#include "cpp/hardware/motionControl/MotionAxisPosition.h"
#include "cpp/hardware/motionControl/MotionAxisProperties.h"
#include "cpp/hardware/motionControl/MotionController.h"
#include "cpp/hardware/motionControl/MotionControlServiceModeConfigurationStatus.h"
#include "cpp/hardware/motionControl/MotionProfile.h"
#include "cpp/hardware/motionControl/MotionProfileManager.h"
#include "cpp/hardware/motionControl/ScanPass.h"
#include "cpp/hardware/motionControl/ScanStep.h"
#include "cpp/hardware/motionControl/MotionControlHardwareException.h"
#include "cpp/hardware/util/HardwareConfigurationDescription.h"
#include "cpp/util/src32/sptAssert.h"


using namespace std;

static map<int, MotionController*> _motionControllerIdToInstanceMap;
static MotionController* _pMotionController = 0;
static MotionProfileManager* _pMotionProfileManager = MotionProfileManager::getInstance();
static string const MOTION_CONTROL_HARDWARE_EXCEPTION = "com/axi/v810/hardware/NativeHardwareException";

///////////////////////////////////////////////////////////////////////////////
//
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
static void setMotionControllerIdToInstanceMap(int motionControllerId, int motionControllerModel)
{
  map<int, MotionController*>::iterator it = _motionControllerIdToInstanceMap.find(motionControllerId);
  MotionController* pMotionController = 0;

  if (it == _motionControllerIdToInstanceMap.end())
  {
    MotionController::Model modelEnum = MotionController::getModelEnumFromInt(motionControllerModel);
    MotionController::MotionControllerId idEnum = MotionController::getIdEnumFromInt(motionControllerId);

    pMotionController = MotionController::getInstance(modelEnum, idEnum);
    _motionControllerIdToInstanceMap.insert(make_pair(motionControllerId, pMotionController));


    pMotionController = 0;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Greg Esparza
//////////////////////////////////////////////////////////////////////////////
static MotionController* getMotionController(int motionControllerId)
{
  map<int, MotionController*>::iterator it = _motionControllerIdToInstanceMap.find(motionControllerId);
  sptAssert(it != _motionControllerIdToInstanceMap.end());

  return it->second;  
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    navtiveInitialize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeInitialize(JNIEnv *pEnv, 
                                                                                         jobject javaObject, 
                                                                                         jint motionControllerModel,
                                                                                         jint motionControllerId)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModel);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->initialize();  
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeHome
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeHome(JNIEnv* pEnv, 
                                                                                   jobject javaObject,
                                                                                   jint motionControllerId, 
                                                                                   jint motionAxisToMove)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
    _pMotionController->home(MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMove));
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                      MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeEnable
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeEnable(JNIEnv* pEnv, 
                                                                                     jobject javaObject, 
                                                                                     jint motionControllerId, 
                                                                                     jint motionAxisToMove)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->enable(MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMove));
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                      he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeDisable
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeDisable(JNIEnv* pEnv, 
                                                                                     jobject javaObject,
                                                                                     jint motionControllerId, 
                                                                                     jint motionAxisToMove)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->disable(MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMove));
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetPointToPointMotionProfile
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetPointToPointMotionProfile(JNIEnv *pEnv, 
                                                                                                           jobject javaObject,
                                                                                                           jint motionControllerId, 
                                                                                                           jint motionAxisToMoveId, 
                                                                                                           jint motionProfileId)
{
  try
  {
    sptAssert(_pMotionProfileManager);
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->setPointToPointMotionProfile(motionAxisToMove, motionProfileId);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                      he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativePointToPointMove
 * Signature: (IIII)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativePointToPointMove(JNIEnv* pEnv, 
                                                                                               jobject javaObject, 
                                                                                               jint motionControllerId, 
                                                                                               jint motionAxisToMove, 
                                                                                               jint xPositionInNanometers, 
                                                                                               jint yPositionInNanometers,
                                                                                               jint railWidthPositionInNanometers)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      MotionAxisPosition motionAxisPosition;
      motionAxisPosition.setXpositionInNanometers(xPositionInNanometers);
      motionAxisPosition.setYpositionInNanometers(yPositionInNanometers);
      motionAxisPosition.setRailWidthPositionInNanometers(railWidthPositionInNanometers);
      motionAxisPosition.setAxisToMove(MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMove));
      _pMotionController->pointToPointMove(motionAxisPosition);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetScanPath
 * Signature: ([I)V
 * Swee Yee Wong - Include starting pulse information for scan pass adjustment
 * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeEnableScanPath(JNIEnv* pEnv, 
                                                                                             jobject javaObject, 
                                                                                             jint motionControllerId, 
                                                                                             jintArray scanPathIntArray, 
																							 jint distanceInNanometersPerPulse, 
																							 jint numberOfStartPulses)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      vector<int> scanPath;
      
      loadSTLIntVectorFromJavaIntArray(pEnv, scanPathIntArray, scanPath);
      
      _pMotionController->enableScanPath(scanPath, distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeRunNextScanPassAndWait
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeRunNextScanPass(JNIEnv* pEnv, 
                                                                                              jobject javaObject,
                                                                                              jint motionControllerId)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->runNextScanPass();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeDisableScanPath
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeDisableScanPath(JNIEnv* pEnv, 
                                                                                              jobject javaObject,
                                                                                              jint motionControllerId)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->disableScanPath();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetScanMotionProfile
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetScanMotionProfile(JNIEnv* pEnv, 
                                                                                                   jobject javaObject,
                                                                                                   jint motionControllerId, 
                                                                                                   jint motionAxisToMoveId,
                                                                                                   jint motionProfileId)
{
  try
  {
    sptAssert(_pMotionProfileManager);
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->setScanMotionProfile(motionAxisToMove, motionProfileId);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetScanPathExecutionTimeInMilliseconds
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetScanPathExecutionTimeInMilliseconds(JNIEnv* pEnv, 
                                                                                                                     jobject javaObject,
                                                                                                                     jint motionControllerId)
{
  try
  {
    int scanPathExecutionTimeInMilliseconds = 0;
    
    _pMotionController = getMotionController(motionControllerId);  
    try
    {
      scanPathExecutionTimeInMilliseconds = _pMotionController->getScanPathExecutionTimeInMilliseconds();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return scanPathExecutionTimeInMilliseconds;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetPositionPulse
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetPositionPulse(JNIEnv* pEnv, 
                                                                                               jobject javaObject, 
                                                                                               jint motionControllerId, 
                                                                                               jint distanceInNanometersPerPulse, 
                                                                                               jint numberOfStartPulses)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);  
    try
    {
      _pMotionController->setPositionPulse(distanceInNanometersPerPulse, numberOfStartPulses);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeStop
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeStop(JNIEnv* pEnv, 
                                                                                   jobject javaObject, 
                                                                                   jint motionControllerId, 
                                                                                   jint motionAxisToMoveId)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->stop(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMinimumPositionLimitInNanometers
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMinimumPositionLimitInNanometers(JNIEnv* pEnv, 
                                                                                                                  jobject javaObject, 
                                                                                                                  jint motionControllerId, 
                                                                                                                  jint motionAxisToMoveId)
{
  try
  {
    int minimumPositionLimitInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      minimumPositionLimitInNanometers = _pMotionController->getMinimumPositionLimitInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return minimumPositionLimitInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMaximumPositionLimitInNanometers
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMaximumPositionLimitInNanometers(JNIEnv* pEnv, 
                                                                                                                  jobject javaObject, 
                                                                                                                  jint motionControllerId, 
                                                                                                                  jint motionAxisToMoveId)
{
  try
  {
    int maximumPositionLimitInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);  
    try
    {
      maximumPositionLimitInNanometers = _pMotionController->getMaximumPositionLimitInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return maximumPositionLimitInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMinimumScanPassLengthInNanometers
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMinimumScanPassLengthInNanometers(JNIEnv* pEnv, 
                                                                                                                   jobject javaObject,
                                                                                                                   jint motionControllerId)
{
  try
  {
    int minimumScanPassLengthInNanometers = 0;
    
    _pMotionController = getMotionController(motionControllerId);  
    try
    {
      minimumScanPassLengthInNanometers = _pMotionController->getMinimumScanPassLengthInNanometers();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return minimumScanPassLengthInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}


/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMaximumScanPassLengthInNanometers
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMaximumScanPassLengthInNanometers(JNIEnv* pEnv, 
                                                                                                                   jobject javaObject,
                                                                                                                   jint motionControllerId, 
                                                                                                                   jint yAxisScanPassStartPositionInNanometers,
                                                                                                                   jint scanPassDirectionId)
{
  try
  {
    int maximumScanPassLengthInNanometers = 0;
    
    ScanPass::Direction scanPassDirection = ScanPass::getDirectionEnumFromInt(scanPassDirectionId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      maximumScanPassLengthInNanometers = _pMotionController->getMaximumScanPassLengthInNanometers(yAxisScanPassStartPositionInNanometers, scanPassDirection);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return maximumScanPassLengthInNanometers; 
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetActualPositionInNanometers
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetActualPositionInNanometers(JNIEnv* pEnv, 
                                                                                                            jobject javObject, 
                                                                                                            jint motionControllerId, 
                                                                                                            jint motionAxisToMoveId)
{
  try
  {
    int actualPositionInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      actualPositionInNanometers = _pMotionController->getActualPositionInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return actualPositionInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsScanPathDone
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsScanPathDone(JNIEnv* pEnv, 
                                                                                                 jobject javaObject,
                                                                                                 jint motionControllerId)
{
  try
  {
    jboolean scanPathDone = true;

    _pMotionController = getMotionController(motionControllerId);
    try
    {
      scanPathDone = _pMotionController->isScanPathDone();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return scanPathDone;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsScanPassDone
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsScanPassDone(JNIEnv* pEnv, 
                                                                                                 jobject javaObject, 
                                                                                                 jint motionControllerId)
{
  try
  {
    jboolean scanPassDone = true;
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      scanPassDone = _pMotionController->isScanPassDone();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return scanPassDone;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsHomeSensorClear
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsHomeSensorClear(JNIEnv* pEnv, 
                                                                                                    jobject javaObject, 
                                                                                                    jint motionControllerId, 
                                                                                                    jint motionAxisToMoveId)
{
  try
  {
    jboolean homeSensorIsClear = true;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      homeSensorIsClear = _pMotionController->isHomeSensorClear(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return homeSensorIsClear;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetConfigurationDescription
 * Signature: (Ljava/util/ArrayList;Ljava/util/ArrayList;)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetConfigurationDescription(JNIEnv* pEnv, 
                                                                                                          jobject javaObject,
                                                                                                          jint motionControllerId,
                                                                                                          jstring parameterGroupSeparator, 
                                                                                                          jobject javaKeys, 
                                                                                                          jobject javaParameters)
{
  try
  {
    bool exceptionThrown = false;
    
    list<HardwareConfigurationDescription*> configDescriptions; 
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      configDescriptions = _pMotionController->getConfigurationDescription();
    }
    catch (MotionControlHardwareException& he)
    {
      exceptionThrown = true;
      
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    
    list<string> stlKeys;
    list<string> stlParameters;
    string paramGroupSeparator = getSTLString(pEnv, parameterGroupSeparator);
    
    list<HardwareConfigurationDescription*>::const_iterator configDescriptionIt;
    for (configDescriptionIt = configDescriptions.begin(); configDescriptionIt != configDescriptions.end(); ++configDescriptionIt)
    {
      stlKeys.push_back((*configDescriptionIt)->getKey());
      list<string> tempParameters = (*configDescriptionIt)->getParameters();
      
      // append parameters into the final STL parameters
      list<string>::const_iterator parametersIt;
      for (parametersIt = tempParameters.begin(); parametersIt != tempParameters.end(); ++parametersIt)
        stlParameters.push_back(*parametersIt);
      
      // separate each group of parameters with the separator token
      stlParameters.push_back(paramGroupSeparator);    
    }
    
    
    if (exceptionThrown == false)
    {
      loadJavaStringArrayFromStlStringList(pEnv, javaKeys, stlKeys);
      loadJavaStringArrayFromStlStringList(pEnv, javaParameters, stlParameters);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsAxisEnabled
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsAxisEnabled(JNIEnv* pEnv, 
                                                                                                jobject javaObject, 
                                                                                                jint motionControllerId, 
                                                                                                jint motionAxisToMoveId)
{
  try
  {
    jboolean axisEnabled = false;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      axisEnabled = _pMotionController->isAxisEnabled(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return axisEnabled;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetForwardHysteresisOffsetInNanometers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetForwardDirectionHysteresisOffsetInNanometers(JNIEnv* pEnv, 
                                                                                                                              jobject javaObject, 
                                                                                                                              jint motionControllerId, 
                                                                                                                              jint motionAxisToMoveId)
{
  try
  {
    int hysteresisOffsetInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      hysteresisOffsetInNanometers = _pMotionController->getForwardDirectionHysteresisOffsetInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return hysteresisOffsetInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetHysteresisOffsetInNanometers
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetHysteresisOffsetInNanometers(JNIEnv* pEnv, 
                                                                                                              jobject javaObject, 
                                                                                                              jint motionControllerId, 
                                                                                                              jint motionAxisToMoveId, 
                                                                                                              jint hysteresisOffsetInNanometers)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
    _pMotionController->setHysteresisOffsetInNanometers(motionAxisToMove, hysteresisOffsetInNanometers);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetReverseHysteresisOffsetInNanometers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetReverseDirectionHysteresisOffsetInNanometers(JNIEnv* pEnv, 
                                                                                                                              jobject javaObject, 
                                                                                                                              jint motionControllerId, 
                                                                                                                              jint motionAxisToMoveId)
{
  try
  {
    int hysteresisOffsetInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      hysteresisOffsetInNanometers = _pMotionController->getReverseDirectionHysteresisOffsetInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return hysteresisOffsetInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetValidPositionPulseRateInNanometers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetValidPositionPulseRateInNanometersPerPulse(JNIEnv* pEnv, 
                                                                                                                            jobject javaObject, 
                                                                                                                            jint motionControllerId, 
                                                                                                                            jint requestedNanometersPerPulse)
{
  try
  {
    int validPositionPulseRate = 0;
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      validPositionPulseRate = _pMotionController->getValidPositionPulseRateInNanometersPerPulse(requestedNanometersPerPulse);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return validPositionPulseRate;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsInitialized
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsInitialized(JNIEnv* pEnv, 
                                                                                                jobject javaObject, 
                                                                                                jint motionControllerModelId, 
                                                                                                jint motionControllerId)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    
    _pMotionController = getMotionController(motionControllerId);
    
    return _pMotionController->isInitialized();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMinimumScanStepWidthInNanometers
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMinimumScanStepWidthInNanometers(JNIEnv* pEnv, 
                                                                                                                  jobject javaObject, 
                                                                                                                  jint motionControllerId)
{
  try
  {
    int minimumScanStepLengthInNanometers = 0;
    
    _pMotionController = getMotionController(motionControllerId);  
    try
    {
      minimumScanStepLengthInNanometers = _pMotionController->getMinimumScanStepWidthInNanometers();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return minimumScanStepLengthInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}


/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetMaximumScanStepWidthInNanometers
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetMaximumScanStepWidthInNanometers(JNIEnv* pEnv, 
                                                                                                                  jobject javaObject, 
                                                                                                                  jint motionControllerId, 
                                                                                                                  jint xAxisScanStepStartPositionInNanometers, 
                                                                                                                  jint scanStepDirectionId)
{
  try
  {
    int maximumScanStepLengthInNanometers = 0;
    
    ScanStep::Direction scanStepDirection = ScanStep::getDirectionEnumFromInt(scanStepDirectionId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      maximumScanStepLengthInNanometers = _pMotionController->getMaximumScanStepWidthInNanometers(xAxisScanStepStartPositionInNanometers, scanStepDirection);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return maximumScanStepLengthInNanometers; 
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetServiceModeConfiguration
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetServiceModeConfiguration(JNIEnv* pEnv, 
                                                                                                          jobject javaObject, 
                                                                                                          jint motionControllerModelId,
                                                                                                          jint motionControllerId)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      _pMotionController->setServiceModeConfiguration();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetYaxisScanStartPositionLimitInNanometers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetYaxisScanStartPositionLimitInNanometers(JNIEnv* pEnv, 
                                                                                                                         jobject javaObject, 
                                                                                                                         jint motionControllerId, 
                                                                                                                         jint scanPassDirectionId)
{
  try
  {
    jint positionLimitInNanometers = 0;
    
    ScanPass::Direction scanPassDirection = ScanPass::getDirectionEnumFromInt(scanPassDirectionId);
    
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      positionLimitInNanometers = _pMotionController->getYaxisScanStartPositionLimitInNanometers(scanPassDirection);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return positionLimitInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetServiceModeConfigurationStatus
 * Signature: (IILjava/lang/Object;Ljava/util/ArrayList;)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetServiceModeConfigurationStatus(JNIEnv* pEnv, 
                                                                                                                jobject javaObject,
                                                                                                                jint motionControllerModelId,
                                                                                                                jint motionControllerId, 
                                                                                                                jobject javaKey, 
                                                                                                                jobject javaParameters)
{
  try
  {
    string stlMessage;
    string stlKey;
    list<string> stlParameters;
    
    MotionControlServiceModeConfigurationStatus configStatus(stlMessage, stlKey, stlParameters);
    
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    
    _pMotionController = getMotionController(motionControllerId);
    
    configStatus = _pMotionController->getServiceModeConfigurationStatus();
    
    stlKey = configStatus.getKey();
    stlParameters = configStatus.getParameters();
    
    setJavaStringBuffer(pEnv, javaKey, stlKey);
    loadJavaStringArrayFromStlStringList(pEnv, javaParameters, stlParameters);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeAbortServiceModeConfiguration
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeAbortServiceModeConfiguration(JNIEnv* pEnv, 
                                                                                                            jobject javaObject, 
                                                                                                            jint motionControllerModelId, 
                                                                                                            jint motionControllerId)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    
    _pMotionController = getMotionController(motionControllerId);
    
    _pMotionController->abortServiceModeConfiguration();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetActualPositionInNanometers
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetActualPositionInNanometers(JNIEnv* pEnv, 
                                                                                                            jobject javaObject, 
                                                                                                            jint motionControllerId, 
                                                                                                            jint motionAxisToMoveId, 
                                                                                                            jint actualPositionInNanometers)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
    _pMotionController->setActualPositionInNanometers(motionAxisToMove, actualPositionInNanometers);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeShutdown
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeShutdown(JNIEnv* pEnv, 
                                                                                       jobject javaObject, 
                                                                                       jint motionControllerId,
                                                                                       jint motionControllerModelId)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    _pMotionController->shutdown();
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetSimulationMode
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetSimulationMode__II(JNIEnv* pEnv, 
                                                                                                    jobject javaObject,
                                                                                                    jint motionControllerId, 
                                                                                                    jint motionControllerModelId)

{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      _pMotionController->setSimulationMode();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetSimulationMode
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetSimulationMode__III(JNIEnv* pEnv, 
                                                                                                     jobject javaObject,
                                                                                                     jint motionControllerId, 
                                                                                                     jint motionControllerModelId, 
                                                                                                     jint motionAxisToMoveId)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      _pMotionController->setSimulationMode(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeClearSimulationMode
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeClearSimulationMode__II(JNIEnv* pEnv, 
                                                                                                      jobject javaObject,
                                                                                                      jint motionControllerId, 
                                                                                                      jint motionControllerModelId)

{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      _pMotionController->clearSimulationMode();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeClearSimulationMode
 * Signature: (III)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeClearSimulationMode__III(JNIEnv* pEnv, 
                                                                                                       jobject javaObject,
                                                                                                       jint motionControllerId, 
                                                                                                       jint motionControllerModelId, 
                                                                                                       jint motionAxisToMoveId)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
        
    try
    {
      _pMotionController->clearSimulationMode(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsSimulationModeOn
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsSimulationModeOn__III(JNIEnv* pEnv, 
                                                                                                          jobject javaObject,
                                                                                                          jint motionControllerId, 
                                                                                                          jint motionControllerModelId, 
                                                                                                          jint motionAxisToMoveId)
{
  try
  {
    bool simulationModeOn = false;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      simulationModeOn = _pMotionController->isSimulationModeOn(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }

    return simulationModeOn;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsSimulationModeOn
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsSimulationModeOn__II(JNIEnv* pEnv, 
                                                                                                         jobject javaObject, 
                                                                                                         jint motionControllerId, 
                                                                                                         jint motionControllerModelId)
{
  try
  {
    bool simulationModeOn = false;
    
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    
    try
    {
      simulationModeOn = _pMotionController->isSimulationModeOn();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                      MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return simulationModeOn;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetActiveMotionProfile
 * Signature: (IILcom/axi/util/IntegerRef;Lcom/axi/util/IntegerRef;)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetActiveMotionProfile(JNIEnv* pEnv, 
                                                                                                     jobject javaObject, 
                                                                                                     jint motionControllerId, 
                                                                                                     jint motionAxisToMoveId, 
                                                                                                     jobject motionProfileTypeId, 
                                                                                                     jobject motionProfileId)
{
  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    MotionProfile motionProfile(MotionProfile::pointToPointMotionProfile);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      motionProfile = _pMotionController->getActiveMotionProfile(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    int motionProfileTypeIntValue =  MotionProfile::getIntFromMotionProfileTypeEnum(motionProfile.getMotionProfileType());
    integerRefSetValue(pEnv, javaObject, motionProfileTypeId, motionProfileTypeIntValue);
    integerRefSetValue(pEnv, javaObject, motionProfileId, motionProfile.getId());
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeSetHardwareResetMode
 * Signature: (IIZ)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativeSetHardwareResetMode(JNIEnv* pEnv, 
                                                                                                   jobject javaObject, 
                                                                                                   jint motionControllerId, 
                                                                                                   jint motionControllerModelId, 
                                                                                                   jboolean hardwareResetModeOn)
{
  try
  {
    setMotionControllerIdToInstanceMap(motionControllerId, motionControllerModelId);
    _pMotionController = getMotionController(motionControllerId);
    _pMotionController->setHardwareResetMode(hardwareResetModeOn);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativePauseScanPath
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_MotionControl_nativePauseScanPath(JNIEnv* pEnv, 
                                                                                            jobject javaObject, 
                                                                                            jint motionControllerId)
{
  try
  {
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      _pMotionController->pauseScanPath();
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeGetPositionAccuracyToleranceInNanometers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_MotionControl_nativeGetPositionAccuracyToleranceInNanometers(JNIEnv* pEnv, 
                                                                                                                       jobject javaObject, 
                                                                                                                       jint motionControllerId, 
                                                                                                                       jint motionAxisToMoveId)
{
  try
  {
    int toleranceInNanometers = 0;
    
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);
    
    _pMotionController = getMotionController(motionControllerId);
    try
    {
      toleranceInNanometers = _pMotionController->getPositionAccuracyToleranceInNanometers(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return toleranceInNanometers;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_MotionControl
 * Method:    nativeIsHomingRequired
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_MotionControl_nativeIsHomingRequired(JNIEnv* pEnv, 
                                                                                                   jobject javaObject, 
                                                                                                   jint motionControllerId, 
                                                                                                   jint motionAxisToMoveId)
{
  bool homingRequired = false;

  try
  {
    MotionAxisProperties::MotionAxisToMove motionAxisToMove =  MotionAxisProperties::getMotionAxisToMoveEnumFromInt(motionAxisToMoveId);    

    _pMotionController = getMotionController(motionControllerId);
    try
    {
      homingRequired = _pMotionController->isHomingRequired(motionAxisToMove);
    }
    catch (MotionControlHardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        MOTION_CONTROL_HARDWARE_EXCEPTION);
    }
    
    return homingRequired;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return homingRequired;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return homingRequired;
  }
}
