package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ICMPPacketHardwareException extends HardwareException
{
  private ICMPPacketHardwareExceptionEnum _exceptionType;

  /**
   * @author Rex Shang
   */
  private ICMPPacketHardwareException(ICMPPacketHardwareExceptionEnum exceptionEnum, String key, Object[] parameters)
  {
    super(new LocalizedString(key, parameters));
    Assert.expect(exceptionEnum != null, "ICMPPacketHardwareExceptionEnum is null.");

    _exceptionType = exceptionEnum;
  }

  /**
   * @author Rex Shang
   */
  public static ICMPPacketHardwareException getICMPPacketSizeTooSmallException(String name, String ipAddress, int expectedPacketSize, int packetSize)
  {
    Object[] exceptionParameters = new Object[]
                                   {name, ipAddress, new Integer(expectedPacketSize), new Integer(packetSize)};
    ICMPPacketHardwareException exception = new ICMPPacketHardwareException(ICMPPacketHardwareExceptionEnum.MTU_SIZE_TOO_SMALL_EXCEPTION,
                                                                            "HW_ICMP_MTU_SIZE_TOO_SMALL_EXCEPTION_KEY",
                                                                            exceptionParameters);

    return exception;
  }

  /**
   * @author Rex Shang
   */
  public ICMPPacketHardwareExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }

}
