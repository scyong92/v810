package com.axi.v810.hardware;

/**
 * @author Rex Shang
 */
public class RightPanelClearSensorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static RightPanelClearSensorEventEnum SENSOR_BLOCKED = new RightPanelClearSensorEventEnum(++_index);
  public static RightPanelClearSensorEventEnum SENSOR_CLEARED = new RightPanelClearSensorEventEnum(++_index);

  /**
   * @author Rex Shang
   */
  private RightPanelClearSensorEventEnum(int id)
  {
    super(id);
  }

}

