package com.axi.v810.hardware;

import com.axi.util.*;

/**
* This exception gets thrown if code asks for some object or method related
* to some hardware that does not exist with the current system.
*
* @author Bill Darbie
*/
public class HardwareDoesNotExistException extends HardwareException
{
  public HardwareDoesNotExistException()
  {
    super(new LocalizedString("HW_DOES_NOT_EXIST_KEY", null));
  }
}
