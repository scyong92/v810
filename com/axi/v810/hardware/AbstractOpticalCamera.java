package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public abstract class AbstractOpticalCamera extends HardwareObject implements HardwareStartupShutdownInt, Comparable<AbstractOpticalCamera>
{
  private static final String _OPTICAL_CAMERA_LOG_NAME_PREFIX = "Optical";
  private static Map<OpticalCameraIdEnum, AbstractOpticalCamera> _idToInstanceMap;
  private String _name;

  protected static ProgressObservable _progressObservable;
  protected static Config _config;
  protected static OpticalCameraModelTypeEnum _modelType;
  protected HardwareObservable _hardwareObservable;
  protected boolean _initialized;
  protected OpticalCameraIdEnum _cameraId;
  protected HardwareTaskEngine _hardwareTaskEngine;
  protected OpticalCameraLogUtil _opticalCameraLogUtil;
  protected String _opticalCameraLogName;
  protected List<HardwareConfigurationDescriptionInt> _configDescription;

  private static IntegerRef _startupProgressStateCount;

  protected static PspModuleVersionEnum _versionEnum;
  protected static boolean _isLargerFov = false;
  protected static OpticalCameraFovEnum _opticalCameraFovEnum;

  /**
   * @author Cheah Lee Herng
   */
  public int getId()
  {
    return _cameraId.getId();
  }

  /**
   * @author Cheah Lee Herng
   */
  static
  {
    _progressObservable = ProgressObservable.getInstance();
    _idToInstanceMap = new HashMap<OpticalCameraIdEnum, AbstractOpticalCamera>();
    _config = Config.getInstance();
    _startupProgressStateCount = new IntegerRef();
  }

  /**
   * @author Cheah Lee Herng
   */
  public synchronized static AbstractOpticalCamera getInstance(OpticalCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);

    AbstractOpticalCamera instance = null;

    _modelType = getModelType();

    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      if (_isLargerFov)
        _opticalCameraFovEnum = OpticalCameraFovEnum.UEYE_LARGE_FOV;
      else
        _opticalCameraFovEnum = OpticalCameraFovEnum.UEYE_SMALL_FOV;
    }
    else
    {
      if (_isLargerFov)
        _opticalCameraFovEnum = OpticalCameraFovEnum.SENTECH_LARGE_FOV;
      else
        _opticalCameraFovEnum = OpticalCameraFovEnum.SENTECH_SMALL_FOV;
    }
    
    if (_idToInstanceMap.containsKey(cameraId))
    {
      instance = _idToInstanceMap.get(cameraId);
    }
    else
    {
      if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
      {
        instance = uEyeOpticalCamera.getAnInstance(cameraId);
      }
      else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
      {
        instance = SentechOpticalCamera.getAnInstance(cameraId);
      }        
      else
        Assert.expect(false);
    }

    Assert.expect(instance != null);
    _idToInstanceMap.put(cameraId, instance);

    _versionEnum = getPspModuleVersionEnum();

    return instance;
  }

  
  /**
   * @author Cheah Lee Herng
   */
  public int compareTo(AbstractOpticalCamera rhs)
  {
    return getId() - rhs.getId();
  }

  /**
   * @author Cheah Lee Herng
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Cheah Lee Herng
   */
  protected AbstractOpticalCamera(OpticalCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);

    _cameraId = cameraId;
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();        
    _opticalCameraLogUtil = new OpticalCameraLogUtil(_cameraId);
    _opticalCameraLogName = _OPTICAL_CAMERA_LOG_NAME_PREFIX + " " + String.valueOf(_cameraId.getId());
    _configDescription = new ArrayList<HardwareConfigurationDescriptionInt>();
    _name = StringLocalizer.keyToString(new LocalizedString("HW_OPTICAL_CAMERA_NAME_KEY", null)) + " " + _cameraId.getId();;
  }

  /**
   * @author Cheah Lee Herng
   */
  private static OpticalCameraModelTypeEnum getModelType()
  {
    String modelTypeStr = _config.getStringValue(HardwareConfigEnum.OPTICAL_CAMERA_MODEL);
    return OpticalCameraModelTypeEnum.getEnum(modelTypeStr);
  }

  /**
   * @author Cheah Lee Herng
   */
  protected static void setStartupBeginProgressState()
  {
    synchronized (_startupProgressStateCount)
    {
      _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() + 1);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  protected static void setStartupEndProgressState()
  {
    synchronized(_startupProgressStateCount)
    {
      _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() - 1);

      try
      {
        if (_startupProgressStateCount.getValue() > 0)
        {
          _startupProgressStateCount.wait();
        }
        else
        {
          _startupProgressStateCount.notifyAll();
        }
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<HardwareConfigurationDescriptionInt> getConfigurationDescription()
  {
    Assert.expect(_configDescription != null);

    return _configDescription;
  }

  /**
   * @author Jack Hwee
   */
  public OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
    Assert.expect(_cameraId != null);
    return _cameraId;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static int getRoiCenterX()
  {
    int roiCenterX = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      roiCenterX = uEyeOpticalCamera.OPTICAL_IMAGE_CENTER_X;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      roiCenterX = SentechOpticalCamera.OPTICAL_IMAGE_CENTER_X;
    }
    else
      Assert.expect(false);
    return roiCenterX;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static int getRoiCenterY()
  {
    int roiCenterY = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      roiCenterY = uEyeOpticalCamera.OPTICAL_IMAGE_CENTER_Y;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      roiCenterY = SentechOpticalCamera.OPTICAL_IMAGE_CENTER_Y;
    }
    else
      Assert.expect(false);
    return roiCenterY;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static int getRoiWidth()
  {
    int roiWidth = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      roiWidth = uEyeOpticalCamera.OPTICAL_IMAGE_ROI_WIDTH;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      roiWidth = SentechOpticalCamera.OPTICAL_IMAGE_ROI_WIDTH;
    }
    else
      Assert.expect(false);
    return roiWidth;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static int getRoiHeight()
  {
    int roiHeight = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      roiHeight = uEyeOpticalCamera.OPTICAL_IMAGE_ROI_HEIGHT;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      roiHeight = SentechOpticalCamera.OPTICAL_IMAGE_ROI_HEIGHT;
    }
    else
      Assert.expect(false);
    return roiHeight;
  }
  
  /**
   * @author Cheah Lee Herng 
   * @author Ying-Huan.Chu
   * @return current OpticalCamera's image width.
   */
  public static int getImageWidth()
  {
    int imageWidth = _opticalCameraFovEnum.getImageWidth();
    return imageWidth;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   * @return current OpticalCamera's image height.
   */
  public static int getImageHeight()
  {
    int imageHeight = _opticalCameraFovEnum.getImageHeight();
    return imageHeight;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static int getMaxImageWidth()
  {
    int maxImageWidth = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      maxImageWidth = uEyeOpticalCamera.OPTICAL_MAX_IMAGE_WIDTH;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      maxImageWidth = SentechOpticalCamera.OPTICAL_MAX_IMAGE_WIDTH;
    }
    else
      Assert.expect(false);
    return maxImageWidth;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static int getMaxImageHeight()
  {
    int maxImageHeight = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      maxImageHeight = uEyeOpticalCamera.OPTICAL_MAX_IMAGE_HEIGHT;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      maxImageHeight = SentechOpticalCamera.OPTICAL_MAX_IMAGE_HEIGHT;
    }
    else
      Assert.expect(false);
    return maxImageHeight;
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public static PspModuleVersionEnum getPspModuleVersionEnum()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_MODULE_VERSION);
    return PspModuleVersionEnum.getEnum(version);
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public String getOpticalCalibrationDirectoryFullPath()
  {
    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      return Directory.getOpticalCameraCalibProfileDir(_cameraId.getId(), OpticalCalibrationProfileEnum.PROFILE_1.getId());
    else
    {
      if (_isLargerFov)
      {
        if (_cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
          return Directory.getLargerFovOpticalCalibrationProfileFrontModuleDir();
        else
          return Directory.getLargerFovOpticalCalibrationProfileRearModuleDir();
      }
      else
      {
        if (_cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
          return Directory.getOpticalCalibrationProfileFrontModuleDir();
        else
          return Directory.getOpticalCalibrationProfileRearModuleDir();
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static int getActualImageWidthInNanometer()
  {
    int actualImageWidthInNanometer = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      actualImageWidthInNanometer = uEyeOpticalCamera.OPTICAL_ACTUAL_IMAGE_WIDTH_IN_NANOMETER;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      SystemTypeEnum systemType = XrayTester.getSystemType();
      if (systemType.equals(SystemTypeEnum.STANDARD) || systemType.equals(SystemTypeEnum.XXL) || systemType.equals(SystemTypeEnum.THROUGHPUT))
        actualImageWidthInNanometer = SentechOpticalCamera.OPTICAL_ACTUAL_IMAGE_WIDTH_STD_XXL_IN_NANOMETER;
      else if (systemType.equals(SystemTypeEnum.S2EX))
        actualImageWidthInNanometer = SentechOpticalCamera.OPTICAL_ACTUAL_IMAGE_WIDTH_S2EX_IN_NANOMETER;
      else
        Assert.expect(false, "AbstractOpticalCamera: getActualImageWidthInNanometer(): Unknown System Type");
    }
    else
      Assert.expect(false);
    return actualImageWidthInNanometer;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static int getActualImageHeightInNanometer()
  {
    int actualImageHeightInNanometer = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      actualImageHeightInNanometer = uEyeOpticalCamera.OPTICAL_ACTUAL_IMAGE_HEIGHT_IN_NANOMETER;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      SystemTypeEnum systemType = XrayTester.getSystemType();
      if (systemType.equals(SystemTypeEnum.STANDARD) || systemType.equals(SystemTypeEnum.XXL) || systemType.equals(SystemTypeEnum.THROUGHPUT))
        actualImageHeightInNanometer = SentechOpticalCamera.OPTICAL_ACTUAL_IMAGE_HEIGHT_STD_XXL_IN_NANOMETER;
      else if (systemType.equals(SystemTypeEnum.S2EX))
        actualImageHeightInNanometer = SentechOpticalCamera.OPTICAL_ACTUAL_IMAGE_HEIGHT_S2EX_IN_NANOMETER;
      else
        Assert.expect(false, "AbstractOpticalCamera: getActualImageHeightInNanometer(): Unknown System Type");
    }
    else
      Assert.expect(false);
    return actualImageHeightInNanometer;
  }

  /**
   * @author Ying-Huan.Chu
   * @return Larger FOV's actual image width in nanometer.
   */
  public static int getLargerFovActualImageWidthInNanometer()
  {
    int largerFovActualImageWidthInNanometer = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      largerFovActualImageWidthInNanometer = uEyeOpticalCamera.OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_WIDTH_IN_NANOMETER;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      largerFovActualImageWidthInNanometer = SentechOpticalCamera.OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_WIDTH_IN_NANOMETER;
    }
    else
      Assert.expect(false);
    return largerFovActualImageWidthInNanometer;
  }
  
  /**
   * @author Ying-Huan.Chu.
   * @return Larger FOV's actual image height in nanometer.
   */
  public static int getLargerFovActualImageHeightInNanometer()
  {
    int largerFovActualImageHeightInNanometer = -1;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      largerFovActualImageHeightInNanometer = uEyeOpticalCamera.OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_HEIGHT_IN_NANOMETER;
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      largerFovActualImageHeightInNanometer = SentechOpticalCamera.OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_HEIGHT_IN_NANOMETER;
    }
    else
      Assert.expect(false);
    return largerFovActualImageHeightInNanometer;
  }
  
  /**
   * Set to use Larger FOV or Small FOV.
   * 
   * @author Ying-Huan.Chu
   * @param useLargerFov - pass in True to use Larger FOV and false to use small FOV.
   * @throws com.axi.v810.util.XrayTesterException
   */
  public void setUseLargerFov(boolean useLargerFov) throws XrayTesterException
  {
    _isLargerFov = useLargerFov;
    if (_modelType.equals(OpticalCameraModelTypeEnum.EdmundOptic_uEye))
    {
      if (_isLargerFov)
      {
        _opticalCameraFovEnum = OpticalCameraFovEnum.UEYE_LARGE_FOV;
      }
      else
      {
        _opticalCameraFovEnum = OpticalCameraFovEnum.UEYE_SMALL_FOV;
      }
      if (XrayTester.getInstance().isSimulationModeOn() == false)
        setImageSizeInPixel(_opticalCameraFovEnum.getImageWidth(), _opticalCameraFovEnum.getImageHeight());
    }
    else if (_modelType.equals(OpticalCameraModelTypeEnum.Sentech_usbCam))
    {
      if (_isLargerFov)
      {
        _opticalCameraFovEnum = OpticalCameraFovEnum.SENTECH_LARGE_FOV;
      }
      else
      {
        _opticalCameraFovEnum = OpticalCameraFovEnum.SENTECH_SMALL_FOV;
      }
      // Set image size to update JNI side. Need to reload calibration data and reinitialize camera for the changes to take effect.
      if (XrayTester.getInstance().isSimulationModeOn() == false)
      {
        setImageSizeInPixel(_opticalCameraFovEnum.getImageWidth(), _opticalCameraFovEnum.getImageHeight());
        loadCalibrationData(getOpticalCalibrationDirectoryFullPath());
        initialize();
      }
    }
    else
    {
      Assert.expect(false);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return true if camera is set to use Larger FOV now, return false otherwise.
   */
  public boolean isLargerFov()
  {
    return _isLargerFov;
  }
    
  public abstract int getWindowHandle(java.awt.Component comp) throws XrayTesterException;
  public abstract void initialize() throws XrayTesterException;
  public abstract void shutdown() throws XrayTesterException;
  public abstract void enableHardwareTrigger() throws XrayTesterException;
  public abstract void displayLiveVideo() throws XrayTesterException;
  public abstract void generateHeightMap(OpticalInspectionData opticalInspectionData, String fullPathFileName, boolean returnActualHeightMap) throws XrayTesterException;
  public abstract void acquireObjectFringeImages(String fullPathFileName, int planeId, boolean forceTrigger) throws XrayTesterException;
  public abstract void acquireOfflineImages(String fullPathFileName, boolean forceTrigger) throws XrayTesterException;
  public abstract void setInspectionROI(int roiCount, int[] centerX, int[] centerY, int[] width, int[] height);
  public abstract void loadCalibrationData(String fullPath);
  public abstract void setMagnificationData(double[] magnificationData) throws XrayTesterException;
  public abstract void enableAutoGain(boolean enable) throws XrayTesterException;
  public abstract void enableAutoSensorGain(boolean enable) throws XrayTesterException;
  public abstract void enableGainBoost(boolean enable) throws XrayTesterException;
  public abstract void setFramesPerSecond(double framesPerSecond) throws XrayTesterException;
  public abstract void setMasterGainFactor(int gainFactor) throws XrayTesterException;
  public abstract boolean isHeightMapValueAvailable();
  public abstract void setHeightMapValueAvailable(boolean isHeightMapValueAvaible);
  public abstract float[] getNewCoordinateXInPixel() throws XrayTesterException;
  public abstract float[] getNewCoordinateYInPixel() throws XrayTesterException;
  public abstract float[] getHeightMapValue();
  public abstract void exitCamera() throws XrayTesterException;
  public abstract void initializePspAlgorithm();
  public abstract void setStopLiveVideoBoolean(boolean bool);
  public abstract void performCalibration(OpticalCalibrationData opticalCalibrationData) throws XrayTesterException;
  public abstract void generateOfflineHeightMap(com.axi.util.image.Image image, com.axi.util.image.RegionOfInterest roi, float floatAdjustment, int minusPlusSet) throws XrayTesterException;
  public abstract void setDebugMode(boolean enableDebug);
  public abstract boolean isTriggerModeReady();
  public abstract void setCropImageOffset(int frontModuleOffsetXInPixel, int rearModuleOffsetXInPixel);
  public abstract void setImageSizeInPixel(int imageWidthInPixel, int imageHeightInPixel);
}
