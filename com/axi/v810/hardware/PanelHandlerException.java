package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class PanelHandlerException extends HardwareException
{
  /**
   * Used to determine whether to add header & footer
   */
  private boolean _appendHeaderFooter;

  /**
   * @author Rex Shang
   */
  private PanelHandlerException(PanelHandlerExceptionEnum panelHandlerHardwareExceptionEnum, LocalizedString message)
  {
    super(message);

    _appendHeaderFooter = true;
    _exceptionType = panelHandlerHardwareExceptionEnum;
    Assert.expect(panelHandlerHardwareExceptionEnum != null);
  }

  /**
   * Added a boolean to determine whether to add header & footer
   * @author Seng-Yew Lim
   */
  private PanelHandlerException(PanelHandlerExceptionEnum panelHandlerHardwareExceptionEnum, LocalizedString message, boolean appendHeaderFooter)
  {
    super(message);

    _appendHeaderFooter = appendHeaderFooter;
    _exceptionType = panelHandlerHardwareExceptionEnum;
    Assert.expect(panelHandlerHardwareExceptionEnum != null);
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getBothPanelClearSensorsUnexpectedBlockedException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.BOTH_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED,
                                     new LocalizedString("HW_BOTH_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getLeftPanelClearSensorUnexpectedBlockedException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.LEFT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED,
                                     new LocalizedString("HW_LEFT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getRightPanelClearSensorUnexpectedBlockedException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.RIGHT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED,
                                     new LocalizedString("HW_RIGHT_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getFailedToRecapturePanelException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.FAILED_TO_RECAPTURE_PANEL,
                                     new LocalizedString("HW_FAILED_TO_RECAPTURE_PANEL_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getFailedToRecapturePanelException(XrayTesterException xte)
  {
    Object[] exceptionParameters = new Object[] {xte.getLocalizedMessage()};
    PanelHandlerException newException = new PanelHandlerException(PanelHandlerExceptionEnum.FAILED_TO_RECAPTURE_PANEL,
                                                                   new LocalizedString("HW_FAILED_TO_RECAPTURE_PANEL_WITH_DETAILS_EXCEPTION_KEY", exceptionParameters));
    newException.initCause(xte);
    return newException;
  }


  /**
   * @author Rex Shang
   */
  static PanelHandlerException getInvalidPanelLoadingModeException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.INVALID_PANEL_LOADING_MODE,
                                     new LocalizedString("HW_INVALID_PANEL_LOADING_MODE_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getInvalidPanelFlowDirectionException()
  {
    return new PanelHandlerException(PanelHandlerExceptionEnum.INVALID_PANEL_FLOW_DIRECTION,
                                     new LocalizedString("HW_INVALID_PANEL_FLOW_DIRECTION_EXCEPTION_KEY", null));
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerException getFailedToDetectPanelClearSensorStateDuringStartupException(XrayTesterException xte)
  {
    Object[] exceptionParameters = new Object[] {xte.getLocalizedMessage()};
    PanelHandlerException newException =
        new PanelHandlerException(PanelHandlerExceptionEnum.FAILED_TO_DETECT_PANEL_CLEAR_SENSOR_STATE_DURING_STARTUP,
                                  new LocalizedString("HW_PANEL_HANDLER_FAILED_TO_DETECT_PANEL_CLEAR_SENSOR_STATE_DURING_STARTUP_EXCEPTION_KEY", exceptionParameters));
    newException.initCause(xte);
    return newException;
  }

  /**
 * @author YingKat Choor
 */
static PanelHandlerException getPanelInPlaceButPrimarySensorNotDetectPanelException()
{
  return new PanelHandlerException(PanelHandlerExceptionEnum.PANEL_IN_PLACE_BUT_PRIMARY_SENSOR_NOT_DETECT_PANEL,
                                   new LocalizedString("HW_PANEL_IN_PLACE_BUT_PRIMARY_SENSOR_NOT_DETECT_PANEL_EXCEPTION_KEY", null));
}

/**
 * @author Swee-Yee.Wong
 */
static PanelHandlerException getOpticalPanelInPlaceTuningRequiredException()
{
  return new PanelHandlerException(PanelHandlerExceptionEnum.HW_OPTICAL_PANEL_IN_PLACE_TUNING_REQUIRED,
                                   new LocalizedString("HW_OPTICAL_PANEL_IN_PLACE_TUNING_REQUIRED_EXCEPTION_KEY", null));
}


static PanelHandlerException getPanelInPlaceNotEngaged()
{
  return new PanelHandlerException(PanelHandlerExceptionEnum.PANEL_IN_PLACE_NOT_ENGAGED,
                                   new LocalizedString("HW_PANEL_IN_PLACE_NOT_ENGAGED_KEY", null));
}

  /**
     * @author YingKat Choor
     */
  static PanelHandlerException getPanelComponentHeightExceedMaxSafetyLevelException()
  {
      return new PanelHandlerException(PanelHandlerExceptionEnum.PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL,
                                       new LocalizedString("HW_PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL_EXCEPTION_KEY", null));
  }
  
  /**
     * @author Anthony Fong
     */
  static PanelHandlerException getPanelComponentHeightExceedMaxSafetyLevelMisalignOrMalfunctionException()
  {
      return new PanelHandlerException(PanelHandlerExceptionEnum.PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL_MISALIGN_OR_MALFUNCTION,
                                       new LocalizedString("HW_PANEL_COMPONENT_HEIGHT_EXCEED_MAX_SAFETY_LEVEL_MISALIGN_OR_MALFUNCTION_EXCEPTION_KEY", null));
  }
  /**
   * @author Seng-Yew Lim
   */
  static PanelHandlerException getPanelWidthPlusPanelWidthClearanceExceedMaxRailWidthExceptionKey (Object args[])
  {
    PanelHandlerException newException =
        new PanelHandlerException(PanelHandlerExceptionEnum.PANEL_WIDTH_PLUS_PANEL_WIDTH_CLEARANCE_EXCEED_MAX_RAIL_WIDTH,
                                  new LocalizedString("HW_PANEL_WIDTH_PLUS_PANEL_WIDTH_CLEARANCE_EXCEED_MAX_RAIL_WIDTH_EXCEPTION_KEY", args),
                                  false);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  public String getLocalizedMessage()
  {
    String header = StringLocalizer.keyToString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY");
    String messageBody = StringLocalizer.keyToString(getLocalizedString());
    String footer = StringLocalizer.keyToString("HW_PANEL_HANDLER_HARDWARE_EXCEPTION_FOOTER_KEY");
    String companyContactInformation = StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY");

    String finalMessage;
    if (_appendHeaderFooter)
      finalMessage = header + messageBody + footer + companyContactInformation;
    else
      finalMessage = messageBody;
    Assert.expect(finalMessage != null, "Exception message is null.");
    return finalMessage;
  }
}
