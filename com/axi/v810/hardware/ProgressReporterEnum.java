package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Identifies which hardware object is reporting progress.
 *
 * @author Greg Loring
 */
public class ProgressReporterEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private final LocalizedString _localizedString;

  public static ProgressReporterEnum MOTION_CONTROL_INITIALIZE  = new ProgressReporterEnum(++_index, "HW_MOTION_CONTROL_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum PANEL_POSITIONER_INITIALIZE  = new ProgressReporterEnum(++_index, "HW_PANEL_POSITIONER_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_ON = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_ON_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_POWER_OFF = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_POWER_OFF_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_OFF_COMPLETE = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_POWER_OFF_COMPLETE_KEY");
  public static ProgressReporterEnum PANEL_HANDLER_INITIALIZE  = new ProgressReporterEnum(++_index, "HW_PANEL_HANDLER_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum DIGITAL_IO_INITIALIZE = new ProgressReporterEnum(++_index, "HW_DIGITAL_IO_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum IMAGE_RECONSTRUCTION_PROCESSOR_INITIALIZE = new ProgressReporterEnum(++_index, "HW_IMAGE_RECONSTRUCTION_PROCESSOR_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_CAMERA_INITIALIZE = new ProgressReporterEnum(++_index, "HW_XRAY_CAMERA_INITIALIZE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_WARM_UP_SHORT = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_WARM_UP_SHORT_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_WARM_UP_MEDIUM = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_WARM_UP_MEDIUM_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_WARM_UP_LONG = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_WARM_UP_LONG_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum XRAY_SOURCE_SELF_TEST = new ProgressReporterEnum(++_index, "HW_XRAY_SOURCE_SELF_TEST_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum REMOTE_MOTION_SERVER_INITIALIZE = new ProgressReporterEnum(++_index, "HW_REMOTE_MOTION_SERVER_INITIALIZE_PROGRESS_REPORTER_KEY");

  // Statics for confirmations and diagnostics
  public static ProgressReporterEnum CONFIRM_CAMERA_COMMUNICATION = new ProgressReporterEnum(++_index, "HW_CONFIRM_CAMERA_COMMUNICATION_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_IRP_COMMUNICATION = new ProgressReporterEnum(++_index, "HW_CONFIRM_IRP_COMMUNICATION_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_RMS_COMMUNICATION = new ProgressReporterEnum(++_index, "HW_CONFIRM_RMS_COMMUNICATION_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_CAMERA_ADJUSTMENT = new ProgressReporterEnum(++_index, "HW_CONFIRM_CAMERA_ADJUSTMENT_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_ONE = new ProgressReporterEnum(++_index, "HW_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_ONE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_TWO = new ProgressReporterEnum(++_index, "HW_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_TWO_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_THREE = new ProgressReporterEnum(++_index, "HW_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_THREE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum LIGHT_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_LIGHT_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum DARK_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_DARK_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum DARK_OPTICAL_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_DARK_OPTICAL_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum LIGHT_OPTICAL_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_LIGHT_OPTICAL_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_ONE = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_ONE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_TWO = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_TWO_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_THREE = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_THREE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_LIGHT_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum HIGH_MAG_DARK_CAMERA_IMAGE_QUALITY_TASK = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_DARK_CAMERA_IMAGE_QUALITY_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_SYSTEM_MTF_TASK = new ProgressReporterEnum(++_index, "HW_CONFIRM_SYSTEM_MTF_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_MAGNIFICATION_COUPON_TASK = new ProgressReporterEnum(++_index, "HW_CONFIRM_MAGNIFICATION_COUPON_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_SYSTEM_FIDUCIAL_TASK = new ProgressReporterEnum(++_index, "HW_CONFIRM_SYSTEM_FIDUCIAL_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_SYSTEM_FIDUCIAL_B_TASK = new ProgressReporterEnum(++_index, "HW_CONFIRM_SYSTEM_FIDUCIAL_B_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum DIAGNOSE_SHARPNESS_TASK = new ProgressReporterEnum(++_index, "HW_DIAGNOSE_SHARPNESS_TASK_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_LEGACY_XRAY_SOURCE = new ProgressReporterEnum(++_index, "HW_CONFIRM_LEGACY_XRAY_SOURCE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_PANEL_HANDLING = new ProgressReporterEnum(++_index, "HW_CONFIRM_PANEL_HANDLING_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_XRAY_CYLINDER_HANDLING = new ProgressReporterEnum(++_index, "HW_CONFIRM_XRAY_CYLINDER_HANDLING_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_XRAY_CPMOTOR_HANDLING = new ProgressReporterEnum(++_index, "HW_CONFIRM_XRAY_CPMOTOR_HANDLING_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_MOTION_REPEATABILITY = new ProgressReporterEnum(++_index, "HW_CONFIRM_MOTION_REPEATABILITY_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_AREA_MODE_IMAGE = new ProgressReporterEnum(++_index, "HW_CONFIRM_AREA_MODE_IMAGE_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_FRONT_OPTICAL_MEASUREMENT_REPEATABILITY = new ProgressReporterEnum(++_index, "HW_CONFIRM_FRONT_OPTICAL_MEASUREMENT_REPEATABILITY_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_REAR_OPTICAL_MEASUREMENT_REPEATABILITY = new ProgressReporterEnum(++_index, "HW_CONFIRM_REAR_OPTICAL_MEASUREMENT_REPEATABILITY_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_BENCHMARK_TASK = new ProgressReporterEnum(++_index, "HW_CONFIRM_BENCHMARK_PROGRESS_REPORTER_KEY");
  
  //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  public static ProgressReporterEnum CONFIRM_CAMERA_CALIBRATION_IMAGES = new ProgressReporterEnum(++_index, "HW_CONFIRM_CAMERA_CALIBRATION_IMAGES_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum CONFIRM_HIGH_MAG_CAMERA_CALIBRATION_IMAGES = new ProgressReporterEnum(++_index, "HW_HIGH_MAG_CONFIRM_CAMERA_CALIBRATION_IMAGES_PROGRESS_REPORTER_KEY");

  // Statics for calibration/adjustments
  public static ProgressReporterEnum CAMERA_GRAYSCALE_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_CAMERA_GRAYSCALE_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum XRAY_SPOT_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_XRAY_SPOT_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum Y_HYSTERESIS_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_Y_HYSTERESIS_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum SYSTEM_OFFSETS_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_SYSTEM_OFFSETS_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum MAGNIFICATION_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_MAGNIFICATION_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_MAGNIFICATION_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_MAGNIFICATION_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_FRINGE_PATTERN_UNIFORMITY_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_FRINGE_PATTERN_UNIFORMITY_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_HEIGHT_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_HEIGHT_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum XRAY_SPOT_DEFLECTION_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_XRAY_SPOT_DEFLECTION_TASK_KEY");
  public static ProgressReporterEnum XRAY_CAMERA_ENABLE_GRAYSCALE_TO_RUN_TASK = new ProgressReporterEnum(++_index, "HW_XRAY_CAMERA_ENABLE_GRAYSCALE_TO_RUN_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_REFERENCE_PLANE_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_REFERENCE_PLANE_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_SYSTEM_FIDUCIAL_ROTATION_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_SYSTEM_FIDUCIAL_ROTATION_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_CALIBRATION_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_CALIBRATION_ADJUSTMENT_TASK_KEY");
  public static ProgressReporterEnum OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_TASK = new ProgressReporterEnum(++_index, "HW_OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_TASK_KEY");

  // inner barrier
  public static ProgressReporterEnum INNER_BARRIER_OPEN = new ProgressReporterEnum(++_index, "HW_INNER_BARRIER_OPEN_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum INNER_BARRIER_CLOSE = new ProgressReporterEnum(++_index, "HW_INNER_BARRIER_CLOSE_PROGRESS_REPORTER_KEY");

  // Optical camera
  public static ProgressReporterEnum OPTICAL_CAMERA_INITIALIZE = new ProgressReporterEnum(++_index, "HW_OPTICAL_CAMERA_INITIALIZE_PROGRESS_REPORTER_KEY");

  // Micro-Controller
  public static ProgressReporterEnum MICROCONTROLLER_INITIALIZE= new ProgressReporterEnum(++_index, "HW_MICROCONTROLLER_ON_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum MICROCONTROLLER_ON= new ProgressReporterEnum(++_index, "HW_MICROCONTROLLER_ON_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum MICROCONTROLLER_OFF= new ProgressReporterEnum(++_index, "HW_MICROCONTROLLER_OFF_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum MICROCONTROLLER_ALL_PROJECTOR_ON = new ProgressReporterEnum(++_index, "HW_MICROCONTROLLER_ALL_PROJECTOR_ON_PROGRESS_REPORTER_KEY");
  public static ProgressReporterEnum MICROCONTROLLER_ALL_PROJECTOR_OFF = new ProgressReporterEnum(++_index, "HW_MICROCONTROLLER_ALL_PROJECTOR_OFF_PROGRESS_REPORTER_KEY");

  // Projector
  public static ProgressReporterEnum PROJECTOR_INITIALIZE= new ProgressReporterEnum(++_index, "HW_PROJECTOR_INITIALIZE_PROGRESS_REPORTER_KEY");
  
  // EBLE
  public static ProgressReporterEnum EBLE_INITIALIZE = new ProgressReporterEnum(++_index, "HW_ETHERNET_BASED_LIGHT_ENGINE_INITIALIZE_PROGRESS_REPORTER_KEY");

  /**
   * @author Greg Loring
   */
  private ProgressReporterEnum(int id, String localizedStringKey)
  {
    super (id);
    _localizedString = new LocalizedString(localizedStringKey, null);
  }

  /**
   * @author Anthony Fong - For Performance Benchmarking
   */
  public ProgressReporterEnum(int id, String localizedStringKey, Object[] arguments)
  {
    super (id);
    _localizedString = new LocalizedString(localizedStringKey, arguments);
  }
  /**
   * @author Greg Loring
   */
  public LocalizedString getLocalizedString()
  {
    return _localizedString;
  }

  /**
   * @author Greg Loring
   */
  public String toString()
  {
    String result = StringLocalizer.keyToString(_localizedString);
    return result;
  }

}
