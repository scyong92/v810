package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.Config;

/**
 * @author Bob Balliew
 */
  class RunstringOptions
{
  private final String _usage;
  private String _showUsage;
  private String _error;
  private int _returnCode;
  private String _overridenConfig;
  private int _max_loop_count;

  /**
   * @author Bob Balliew
   */
  private String getParameter(String[] args, int argi, int missingReturnCode, String badValue)
  {
    Assert.expect(args != null);
    Assert.expect(argi >= 0);
    String parameter = badValue;

    if (argi < args.length)
    {
      parameter = args[argi];
    }
    else
    {
      _showUsage = _usage;
      _error = "missing arg[" + Integer.toString(argi) + "]";
      _returnCode = missingReturnCode;
    }

    return parameter;
  }

  /**
   * @author Bob Balliew
   */
  private int getParameter(String[] args, int argi, int missingReturnCode, int notIntReturnCode, int badValue)
  {
    Assert.expect(args != null);
    Assert.expect(argi >= 0);
    int parameter = badValue;

    if (argi < args.length)
    {
      try
      {
        parameter = Integer.parseInt(args[argi]);
      }
      catch (NumberFormatException nfe)
      {
        _showUsage = _usage;
        _error = "bad integer arg[" + Integer.toString(argi) + "]";
        _returnCode = notIntReturnCode;
      }
    }
    else
    {
      _showUsage = _usage;
      _error = "missing arg[" + Integer.toString(argi) + "]";
      _returnCode = missingReturnCode;
    }

    return parameter;
  }

  /**
   * @author Bob Balliew
   */
  private void unknownKeyword(String[] args, int argi, int unknownKeywordReturnCode)
  {
    _showUsage = _usage;
    _error = "unknown arg[" + Integer.toString(argi) + "]: '" + args[argi] + "'";
    _returnCode = unknownKeywordReturnCode;
  }

  /**
   * @author Bob Balliew
   */
  RunstringOptions(String[] args)
  {
    final int NO_ERROR_RETURN_CODE = 0;
    final int UNKOWN_ARG_RETURN_CODE = -1;
    final String QUESTION_KEYWORD = "-?";
    final String HELP_KEYWORD = "-help";
    final String CONFIG_KEYWORD = "-config";
    final String DEFAULT_CONFIG = null;
    final int MISSING_CONFIG_NAME_RETURN_CODE = -2;
    final String BAD_CONFIG_NAME = null;
    final String LOOPS_KEYWORD = "-loops";
    final int DEFAULT_LOOP_COUNT = 100;
    final int MISSING_MAX_LOOP_COUNT_RETURN_CODE = -3;
    final int MAX_LOOP_COUNT_NOT_AN_INTEGER_RETURN_CODE = -4;
    final int BAD_LOOP_COUNT = -1;
    _usage = "USAGE: runBarcodeReader [" + HELP_KEYWORD + "] [" + CONFIG_KEYWORD + " <config name>] [" + LOOPS_KEYWORD + " <max times>]";
    _showUsage = null;
    _error = null;
    _returnCode = NO_ERROR_RETURN_CODE;
    _overridenConfig = DEFAULT_CONFIG;
    _max_loop_count = DEFAULT_LOOP_COUNT;

    for (int i = 0; (i < args.length) && (_error == null); ++i)
    {
      if      (args[i].equals(QUESTION_KEYWORD))  { _showUsage = _usage; }
      else if (args[i].equals(HELP_KEYWORD))      { _showUsage = _usage; }
      else if (args[i].equals(CONFIG_KEYWORD))    { _overridenConfig = getParameter(args, ++i, MISSING_CONFIG_NAME_RETURN_CODE, BAD_CONFIG_NAME); }
      else if (args[i].equals(LOOPS_KEYWORD))     { _max_loop_count  = getParameter(args, ++i, MISSING_MAX_LOOP_COUNT_RETURN_CODE, MAX_LOOP_COUNT_NOT_AN_INTEGER_RETURN_CODE, BAD_LOOP_COUNT); }
      else                                        {  unknownKeyword(args, i, UNKOWN_ARG_RETURN_CODE); }
    }
  }

  /**
   * @author Bob Balliew
   */
  boolean showUsage()
  {
    return (_showUsage != null);
  }

  /**
   * @author Bob Balliew
   */
  String getUsage()
  {
    return showUsage() ? _showUsage : "";
  }

  /**
   * @author Bob Balliew
   */
  boolean badArgs()
  {
    return (_error != null);
  }

  /**
   * @author Bob Balliew
   */
  String getErrorMessage()
  {
    return badArgs() ? _error : "";
  }

  /**
   * @author Bob Balliew
   */
  int getReturnCode()
  {
    return _returnCode;
  }

  /**
   * @author Bob Balliew
   */
  boolean configOverriden()
  {
    return (_overridenConfig != null);
  }

  /**
   * @author Bob Balliew
   */
  String getConfigName()
  {
    return configOverriden() ? _overridenConfig : "";
  }

  /**
   * @author Bob Balliew
   */
  int getMaxLoopCount()
  {
    return _max_loop_count;
  }
}

/**
 * @author Bob Balliew
 */
public class BarcodeReaderManagerMain
{
  /**
   * @deprecated This method uses deprecated methods.
   * @author Bob Balliew
   */
  public static void main(String[] args)
  {
    final int CONFIG_NOT_FOUND_RETURN_CODE = -1000;
    Assert.setLogFile(BarcodeReaderManagerMain.class.getClass().getName(), "");
    RunstringOptions opts = new RunstringOptions(args);
    BarcodeReaderManager bcrm = null;

    try
    {
      PrintWriter printWriter = new PrintWriter(System.out, true);
      Config _config = Config.getInstance();
      _config.loadIfNecessary();
      bcrm = BarcodeReaderManager.getInstance();
      Assert.expect(bcrm != null);

      if (opts.showUsage())
      {
        printWriter.printf(Locale.US, "%s%n", opts.getUsage());
        System.exit(opts.getReturnCode());
      }

      if (opts.badArgs())
      {
        printWriter.printf(Locale.US, "%s%n", opts.getErrorMessage());
        System.exit(opts.getReturnCode());
      }

      if (opts.configOverriden())
      {
        if (! bcrm.getExistingBarcodeReaderConfigurationNames().contains(opts.getConfigName()))
        {
          printWriter.printf(Locale.US, "config name: '%s' not found%n", opts.getConfigName());
          System.exit(CONFIG_NOT_FOUND_RETURN_CODE);
        }

        bcrm.load(opts.getConfigName());
      }

      for (int readingCount = 1; readingCount <= opts.getMaxLoopCount(); ++readingCount)
      {
        List<String> barcodesRead = bcrm.getSerialNumbersReturnErrors();
        printWriter.printf(Locale.US, "barcodes read loop count: %d%n", readingCount);

        for (int scannerNumber = 1; scannerNumber <= barcodesRead.size(); ++scannerNumber)
        {
          printWriter.printf(Locale.US, "scanner %d barcode: '%s'%n", scannerNumber, barcodesRead.get(scannerNumber - 1));
        }

        Assert.expect(barcodesRead.size() == bcrm.getBarcodeReaderNumberOfScanners());
      }

      if (opts.configOverriden())
      {
        bcrm.load(bcrm.getBarcodeReaderConfigurationNameToUse());
      }
    }
    catch(Exception e)
    {
      try
      {
        if ((bcrm != null) && opts.configOverriden())
        {
          bcrm.load(bcrm.getBarcodeReaderConfigurationNameToUse());
          bcrm.deleteConfig(opts.getConfigName());
        }
      }
      catch (Exception ignored)
      {
      }
      e.printStackTrace();
      System.exit(1);
    }

    System.exit(0);
  }
}
