package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for SMEMA
 * @author Rex Shang
 */
class Test_ManufacturingEquipmentInterface extends UnitTest
{
  private ManufacturingEquipmentInterface _manufacturingequipmentinterface = null;

  private Test_ManufacturingEquipmentInterface()
  {
    _manufacturingequipmentinterface = ManufacturingEquipmentInterface.getInstance();
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Set the machine is ready to accept panel bit to false and verify.
      _manufacturingequipmentinterface.setXrayTesterIsReadyToAcceptPanel(false);
      Expect.expect(_manufacturingequipmentinterface.isXrayTesterReadyToAcceptPanel() == false);

      // Set the machine is ready to accept panel bit to true and verify.
      _manufacturingequipmentinterface.setXrayTesterIsReadyToAcceptPanel(true);
      Expect.expect(_manufacturingequipmentinterface.isXrayTesterReadyToAcceptPanel() == true);

      // Set the machine has panel to output bit to false and verify.
      _manufacturingequipmentinterface.setPanelFromXrayTesterAvailable(false);
      Expect.expect(_manufacturingequipmentinterface.isPanelFromXrayTesterAvailable() == false);

      // Set the machine has panel to output bit to true and verify.
      _manufacturingequipmentinterface.setPanelFromXrayTesterAvailable(true);
      Expect.expect(_manufacturingequipmentinterface.isPanelFromXrayTesterAvailable() == true);

      // Set the output panel is tested bit to false and verify.
      _manufacturingequipmentinterface.setPanelOutUntested();
      Expect.expect(_manufacturingequipmentinterface.isPanelOutUntested() == true);

      // Set the output panel is good bit to false and verify.
      _manufacturingequipmentinterface.setPanelOutFailed();
      Expect.expect(_manufacturingequipmentinterface.isPanelOutFail() == true);

      // Set the output panel is tested bit to true and verify.
      _manufacturingequipmentinterface.setPanelOutPassed();
      Expect.expect(_manufacturingequipmentinterface.isPanelOutPass() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    Test_ManufacturingEquipmentInterface test_manufacturingequipmentinterface = new
        Test_ManufacturingEquipmentInterface();
    UnitTest.execute(test_manufacturingequipmentinterface);
  }
}
