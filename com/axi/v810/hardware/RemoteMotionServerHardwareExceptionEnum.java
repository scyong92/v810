package com.axi.v810.hardware;

/**
 *
 * @author Chong, Wei Chin
 */
public class RemoteMotionServerHardwareExceptionEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static RemoteMotionServerHardwareExceptionEnum FAILED_TO_REBOOT_RMS_HARDWARE = new RemoteMotionServerHardwareExceptionEnum(++_index);
  public static RemoteMotionServerHardwareExceptionEnum FAILED_TO_COMMUNICATE_RMS = new RemoteMotionServerHardwareExceptionEnum(++_index);
  public static RemoteMotionServerHardwareExceptionEnum FAILED_TO_START_RMS_APPLICATION = new RemoteMotionServerHardwareExceptionEnum(++_index);
  public static RemoteMotionServerHardwareExceptionEnum FAILED_TO_STOP_RMS_APPLICATION = new RemoteMotionServerHardwareExceptionEnum(++_index);
  public static RemoteMotionServerHardwareExceptionEnum RMS_APPLICATION_VERSION_MISMATCH = new RemoteMotionServerHardwareExceptionEnum(++_index);

  /**
   * @author Chong, Wei Chin
   */
  private RemoteMotionServerHardwareExceptionEnum(int id)
  {
    super(id);
  }

}
