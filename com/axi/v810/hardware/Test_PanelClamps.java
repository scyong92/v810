package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * This is a unit test class for PanelClamps.
 * @author Rex Shang
 */
class Test_PanelClamps extends UnitTest
{
  private PanelClamps _panelClamps = null;

  private Test_PanelClamps()
  {
    _panelClamps = PanelClamps.getInstance();
  }

  public static void main(String[] args)
  {
    Test_PanelClamps testPanelClamps = new Test_PanelClamps();
    UnitTest.execute(testPanelClamps);
  }

  public void test(BufferedReader parm1, PrintWriter parm2)
  {
    try
    {
      // Close the clamps.
      _panelClamps.close();
      // Test to see if the clamps are indeed closed.
      Expect.expect(_panelClamps.areClampsClosed() == true);

      // Open the clamps.
      _panelClamps.open();
      // test to see that the _lightStack thinks that the red light is on
      Expect.expect(_panelClamps.areClampsClosed() == false);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
