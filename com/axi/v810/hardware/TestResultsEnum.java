package com.axi.v810.hardware;

/**
 * @author Bill Darbie
 */
public class TestResultsEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static TestResultsEnum PASSED = new TestResultsEnum(++_index);
  public static TestResultsEnum FAILED = new TestResultsEnum(++_index);
  public static TestResultsEnum NOT_TESTED = new TestResultsEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private TestResultsEnum(int id)
  {
    super(id);
  }
}
