package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalInspectionData 
{
  private static final int INSPECTION_QUALITY_THRESHOLD   = 1;
  private static final int REF_LOWER_PHASE_LIMIT          = -350000;
  
  private PspModuleEnum _pspModuleEnum;
  private OpticalCalibrationProfileEnum _opticalCalibrationProfileEnum;
  private Map<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> _stagePositionToImageFullPath;
  private boolean _forceTrigger;
  
  // Applicable to Psp Module Version 2
  private int _inspectionQualityThreshold;
  private int _refLowerPhaseLimit;
  
  // Boolean flag to determine if need to return height map value
  private boolean _returnActualHeightMapValue;
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalInspectionData()
  {
    _stagePositionToImageFullPath = new HashMap<StagePosition, List<Pair<OpticalShiftNameEnum, String>>>();
    _forceTrigger = false;
    _inspectionQualityThreshold = INSPECTION_QUALITY_THRESHOLD;
    _refLowerPhaseLimit = REF_LOWER_PHASE_LIMIT; 
    _returnActualHeightMapValue = false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addImageFullPath(StagePosition stagePosition, OpticalShiftNameEnum opticalShiftNameEnum, String imageFullPath)
  {
    Assert.expect(stagePosition != null);
    Assert.expect(opticalShiftNameEnum != null);
    Assert.expect(imageFullPath != null);
    
    boolean hasOpticalShiftNameEnum = false;
    if (_stagePositionToImageFullPath.containsKey(stagePosition) == false)
    {
      _stagePositionToImageFullPath.put(stagePosition, new ArrayList<Pair<OpticalShiftNameEnum, String>>());
    }
    
    List<Pair<OpticalShiftNameEnum, String>> opticalShiftNameEnumToImageFullPathList = _stagePositionToImageFullPath.get(stagePosition);
    for(Pair<OpticalShiftNameEnum, String> entry : opticalShiftNameEnumToImageFullPathList)
    {
      if (entry.getFirst().equals(opticalShiftNameEnum))
      {
        hasOpticalShiftNameEnum = true;
        break;
      }
    }
    
    if (hasOpticalShiftNameEnum == false)
    {
      opticalShiftNameEnumToImageFullPathList.add(new Pair<OpticalShiftNameEnum, String>(opticalShiftNameEnum, imageFullPath));
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> getStagePositionMap()
  {
    return _stagePositionToImageFullPath;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPspModuleEnum(PspModuleEnum pspModuleEnum)
  {
    _pspModuleEnum = pspModuleEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleEnum getPspModuleEnum()
  {
    return _pspModuleEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum opticalCalibrationProfileEnum)
  {
    _opticalCalibrationProfileEnum = opticalCalibrationProfileEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalCalibrationProfileEnum getOpticalCalibrationProfileEnum()
  {
    return _opticalCalibrationProfileEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setForceTrigger(boolean forceTrigger)
  {
    _forceTrigger = forceTrigger;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean getForceTrigger()
  {
    return _forceTrigger;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setInspectionQualityThreshold(int inspectionQualityThreshold)
  {
    _inspectionQualityThreshold = inspectionQualityThreshold;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getInspectionQualityThreshold()
  {
    return _inspectionQualityThreshold;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setRefLowerPhaseLimit(int refLowerPhaseLimit)
  {
    _refLowerPhaseLimit = refLowerPhaseLimit;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRefLowerPhaseLimit()
  {
    return _refLowerPhaseLimit;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_stagePositionToImageFullPath != null)
    {
      _stagePositionToImageFullPath.clear();
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setReturnActualHeightMap(boolean returnActualHeightMapValue)
  {
    _returnActualHeightMapValue = returnActualHeightMapValue;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean getReturnActualHeightMap()
  {
    return _returnActualHeightMapValue;
  }
}
