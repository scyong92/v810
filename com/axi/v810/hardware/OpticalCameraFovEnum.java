/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.hardware;

import java.util.*;

/**
 * OpticalCameraFovEnum class - defines the types of FOV we have now.
 * @author Ying-Huan.Chu
 */
public class OpticalCameraFovEnum extends com.axi.util.Enum
{
  private static Map<String, OpticalCameraFovEnum> _stringToEnumMap = new HashMap<>();
  private String _name;
  private int _imageWidth;
  private int _imageHeight;
  private static int _index = -1;
  
  public static OpticalCameraFovEnum UEYE_SMALL_FOV = new OpticalCameraFovEnum(++_index, "UEye Small FOV", 640, 470);
  public static OpticalCameraFovEnum UEYE_LARGE_FOV = new OpticalCameraFovEnum(++_index, "UEye Large FOV", 640, 470);
  public static OpticalCameraFovEnum SENTECH_SMALL_FOV = new OpticalCameraFovEnum(++_index, "Sentech Small FOV", 640, 470);
  public static OpticalCameraFovEnum SENTECH_LARGE_FOV = new OpticalCameraFovEnum(++_index, "Sentech Large FOV", 900, 800);
  
  /**
   * @author Ying-Huan.Chu
   */
  private OpticalCameraFovEnum(int id, String name, int imageWidth, int imageHeight)
  {
    super(id);
    _name = name;
    _imageWidth = imageWidth;
    _imageHeight = imageHeight;
    _stringToEnumMap.put(name, this);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public String getName()
  {
    return _name;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getImageWidth()
  {
    return _imageWidth;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getImageHeight()
  {
    return _imageHeight;
  }
}
