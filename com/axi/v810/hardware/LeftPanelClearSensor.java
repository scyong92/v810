package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * This class controls the left panel clear sensor (AKA panel crunch sensor).
 * @author Rex Shang
 */
public class LeftPanelClearSensor extends PanelClearSensor
{
  private static LeftPanelClearSensor _instance;

  /**
   * @author Rex Shang
   */
  private LeftPanelClearSensor()
  {
    super();
    _instance = null;
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized LeftPanelClearSensor getInstance()
  {
    if (_instance == null)
      _instance = new LeftPanelClearSensor();

    return _instance;
  }

  /**
   * @return true if the panel clear sensor is blocked.
   * @author Rex Shang
   */
  public boolean isBlocked() throws XrayTesterException
  {
    return _digitalIo.isLeftPanelClearSensorBlocked();
  }

}
