package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalPointPixelCoordinate 
{
    private int _coordinateX;
    private int _coordinateY;
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalPointPixelCoordinate()
    {
        _coordinateX = -1;
        _coordinateY = -1;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public OpticalPointPixelCoordinate(int coordinateX, int coordinateY)
    {
        _coordinateX = coordinateX;
        _coordinateY = coordinateY;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getX()
    {
        return _coordinateX;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getY()
    {
        return _coordinateY;
    }
}
