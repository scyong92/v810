package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class OpenOuterBarrierThreadTask extends ThreadTask<Object>
{
  private PanelHandler _panelHandler;
  private BarrierInt _outerBarrier;

  /**
   * @author Bill Darbie
   */
  OpenOuterBarrierThreadTask(BarrierInt outerBarrier)
  {
    super("Open Outer Barrier Thead Task");
    Assert.expect(outerBarrier != null);

    _panelHandler = PanelHandler.getInstance();
    _outerBarrier = outerBarrier;
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws XrayTesterException
  {
    _panelHandler.openOuterBarrierAsNecessary(_outerBarrier);
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // Do Nothing.
  }
}
