package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Unit test for right panel clear sensor
 * @author Rex Shang
 */
class Test_RightPanelClearSensor extends UnitTest
{
  private RightPanelClearSensor _rightPanelClearSensor = null;

  private Test_RightPanelClearSensor()
  {
    _rightPanelClearSensor = RightPanelClearSensor.getInstance();
  }

  public static void main(String[] args)
  {
    Test_RightPanelClearSensor _test_RightPanelClearSensor = new Test_RightPanelClearSensor();
    UnitTest.execute(_test_RightPanelClearSensor);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // XCR-3604 Unit Test Phase 2
      if (DigitalIo.getInstance().isStartupRequired())
        DigitalIo.getInstance().startup();
      
      _rightPanelClearSensor.isBlocked();
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
  }
}
