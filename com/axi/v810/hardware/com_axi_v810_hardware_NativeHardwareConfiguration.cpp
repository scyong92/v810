#include <iostream>

#include "com/axi/util/jniUtil.h"
#include "com_axi_v810_hardware_NativeHardwareConfiguration.h"
#include "cpp/hardware/util/HardwareConfiguration.h"
#include "cpp/util/src32/sptAssert.h"


using namespace std;

static HardwareConfiguration* _pConfigurationProperties = HardwareConfiguration::getInstance();

/*
 * Class:     com_axi_v810_hardware_NativeHardwareConfiguration
 * Method:    nativeSetConfigurationParameters
 * Signature: (Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_NativeHardwareConfiguration_nativeSetConfigurationParameters(JNIEnv *pEnv, 
                                                                                                                       jobject javaObject, 
                                                                                                                       jstring javaKeyStr, 
                                                                                                                       jstring javaValueStr)
{
  try
  {
    sptAssert(_pConfigurationProperties);
    
    string key = getSTLString(pEnv, javaKeyStr);
    string value = getSTLString(pEnv, javaValueStr);
    
    _pConfigurationProperties->setConfigurationParameter(key, value);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}
