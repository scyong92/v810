package com.axi.v810.hardware;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternEnum extends Enum
{
    private static int _index = 0;
    public static FringePatternEnum INITIALIZE = new FringePatternEnum(++_index);    
    public static FringePatternEnum PROJECT_WHITE_LIGHT = new FringePatternEnum(++_index);
    public static FringePatternEnum PROJECT_NO_LIGHT = new FringePatternEnum(++_index);
    public static FringePatternEnum PROJECT_FRINGE_PATTERN_SHIFT_ONE = new FringePatternEnum(++_index);    
    public static FringePatternEnum PROJECT_FRINGE_PATTERN_SHIFT_TWO = new FringePatternEnum(++_index);    
    public static FringePatternEnum PROJECT_FRINGE_PATTERN_SHIFT_THREE = new FringePatternEnum(++_index);    
    public static FringePatternEnum PROJECT_FRINGE_PATTERN_SHIFT_FOUR = new FringePatternEnum(++_index);
    public static FringePatternEnum FINISH_DISPLAY_FRINGE_PATTERN = new FringePatternEnum(++_index);
    
   /**
     * FringePatternEnum
     *
     * @author Cheah Lee Herng
    */
    protected FringePatternEnum(int id)
    {
        super(id);
    }
}
