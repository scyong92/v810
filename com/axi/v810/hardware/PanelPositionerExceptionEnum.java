package com.axi.v810.hardware;

import com.axi.util.*;
import java.util.HashMap;

/**
 * Used by DigitalIoException to help populate the exception message.
 * @author Chnee Khang Wah
 */
public class PanelPositionerExceptionEnum extends HardwareExceptionEnum
{
  private static HashMap<PanelPositionerExceptionEnum, LocalizedString> _enumToStringMap;
  private static int _index = -1;

  public static PanelPositionerExceptionEnum POSSIBLE_PANEL_INSIDE_MACHINE = new PanelPositionerExceptionEnum(++_index);
  public static PanelPositionerExceptionEnum PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD = new PanelPositionerExceptionEnum(++_index);
  public static PanelPositionerExceptionEnum HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE = new PanelPositionerExceptionEnum(++_index);
  public static PanelPositionerExceptionEnum POSSIBLE_PANEL_LOAD_UNSUCCESFUL = new PanelPositionerExceptionEnum(++_index);

  /**
   * @author Rex Shang
   */
  static
  {
    _enumToStringMap = new HashMap<PanelPositionerExceptionEnum, LocalizedString>(_index + 1);
    Assert.expect(_enumToStringMap.put(POSSIBLE_PANEL_INSIDE_MACHINE, new LocalizedString(
        "HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD, new LocalizedString(
        "HW_PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_EXCEPTION_DURING_LOAD_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE, new LocalizedString(
        "HW_HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(POSSIBLE_PANEL_LOAD_UNSUCCESFUL, new LocalizedString(
        "HW_PANEL_LOAD_PANEL_NOT_SUCCESS_EXCEPTION_KEY", null)) == null);
  
    Assert.expect(_enumToStringMap.size() == _index + 1);
  }

  /**
   * @author Chnee Khang Wah
   */
  private PanelPositionerExceptionEnum(int id)
  {
    super(id);
  }

  /**
   * @author Rex Shang
   */
  public static LocalizedString getLocalizedString(PanelPositionerExceptionEnum panelPositionerExceptionEnum)
  {
    LocalizedString myString = _enumToStringMap.get(panelPositionerExceptionEnum);

    Assert.expect(myString != null);
    return myString;
  }

}
