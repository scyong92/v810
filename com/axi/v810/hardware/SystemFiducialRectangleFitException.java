package com.axi.v810.hardware;

/**
 * Copyright: Copyright (c) 2005
 * Company: Agilent Technogies
 * @author Roy Williams
 */
/** @todo wpd Roy - all exceptions need to extend XrayTesterException */
public class SystemFiducialRectangleFitException extends Exception
{
  public SystemFiducialRectangleFitException()
  {
  }
}
