package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraEnableUnitTestModeThreadTask extends ThreadTask<Object>
{
  private AbstractXrayCamera _xRayCamera;
  private String _imageDataTestDirectory;

  /**
   * @author Greg Esparza
   */
  public XrayCameraEnableUnitTestModeThreadTask(AbstractXrayCamera xRayCamera, String imageDataTestDirectory)
  {
    super("Xray Camera enable unit test mode thread task.");

    Assert.expect(xRayCamera != null);
    Assert.expect(imageDataTestDirectory != null);

    _xRayCamera = xRayCamera;
    _imageDataTestDirectory = imageDataTestDirectory;
  }

  /**
   * @author Greg Esparza
   */
  public void cancel() throws Exception
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public Object executeTask() throws Exception
  {
    _xRayCamera.enableUnitTestMode(_imageDataTestDirectory);
    return null;
  }
}
