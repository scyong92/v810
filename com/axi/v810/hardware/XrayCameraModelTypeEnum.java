package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;


/**
 * @author Greg Esparza
 */
public class XrayCameraModelTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private String _name;
  private static Map<String, XrayCameraModelTypeEnum> _xRayCameraModelNameToModelTypeEnumMap;

  /**
   * @author Greg Esparza
   */
  static
  {
    _xRayCameraModelNameToModelTypeEnumMap = new HashMap<String, XrayCameraModelTypeEnum>();
  }

  public static final XrayCameraModelTypeEnum  AXI_TDI = new XrayCameraModelTypeEnum(++_index, HardwareConfigEnum.getAxiTdiXrayCameraModelName());

  /**
   * @author Greg Esparza
   */
  private XrayCameraModelTypeEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
    _xRayCameraModelNameToModelTypeEnumMap.put(name, this);
  }

  /**
   * @author Greg Esparza
   */
  static XrayCameraModelTypeEnum getEnum(String xRayCameraModelName)
  {
    Assert.expect(xRayCameraModelName != null);

    XrayCameraModelTypeEnum xRayCameraModelTypeEnum = _xRayCameraModelNameToModelTypeEnumMap.get(xRayCameraModelName);
    Assert.expect(xRayCameraModelTypeEnum != null);

    return xRayCameraModelTypeEnum;
  }

  /**
   * @author Greg Esparza
   */
  String getName()
  {
    return _name;
  }
}
