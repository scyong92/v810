package com.axi.v810.hardware;

/**
 * @author Cheah Lee Herng
 */
public class PspMicroControllerCommandEnum extends com.axi.util.Enum
{
    static private int _index = -1;
    private final byte _commandCode;
    
    public static final PspMicroControllerCommandEnum AXI_USAGE                     = new PspMicroControllerCommandEnum(++_index, (byte)0x0e);
    public static final PspMicroControllerCommandEnum INITIALIZE_PROJECTOR_1        = new PspMicroControllerCommandEnum(++_index, (byte)0x10);
    public static final PspMicroControllerCommandEnum START_PROJECTOR_1_SEQUENCE    = new PspMicroControllerCommandEnum(++_index, (byte)0x11);
    public static final PspMicroControllerCommandEnum INITIALIZE_PROJECTOR_2        = new PspMicroControllerCommandEnum(++_index, (byte)0x20);
    public static final PspMicroControllerCommandEnum START_PROJECTOR_2_SEQUENCE    = new PspMicroControllerCommandEnum(++_index, (byte)0x21);
    public static final PspMicroControllerCommandEnum INITIALIZE_PROJECTOR_3        = new PspMicroControllerCommandEnum(++_index, (byte)0x40);
    public static final PspMicroControllerCommandEnum START_PROJECTOR_3_SEQUENCE    = new PspMicroControllerCommandEnum(++_index, (byte)0x41);
    public static final PspMicroControllerCommandEnum INITIALIZE_PROJECTOR_4        = new PspMicroControllerCommandEnum(++_index, (byte)0x80);
    public static final PspMicroControllerCommandEnum START_PROJECTOR_4_SEQUENCE    = new PspMicroControllerCommandEnum(++_index, (byte)0x81);
    public static final PspMicroControllerCommandEnum CONTROLLER_STATUS             = new PspMicroControllerCommandEnum(++_index, (byte)0xa0);
    public static final PspMicroControllerCommandEnum ALL_PROJECTOR_ON              = new PspMicroControllerCommandEnum(++_index, (byte)0xb0);
    public static final PspMicroControllerCommandEnum PROJECTOR_1_ON                = new PspMicroControllerCommandEnum(++_index, (byte)0xb1);
    public static final PspMicroControllerCommandEnum PROJECTOR_2_ON                = new PspMicroControllerCommandEnum(++_index, (byte)0xb2);
    public static final PspMicroControllerCommandEnum PROJECTOR_3_ON                = new PspMicroControllerCommandEnum(++_index, (byte)0xb3);
    public static final PspMicroControllerCommandEnum PROJECTOR_4_ON                = new PspMicroControllerCommandEnum(++_index, (byte)0xb4);
    public static final PspMicroControllerCommandEnum ALL_PROJECTOR_OFF             = new PspMicroControllerCommandEnum(++_index, (byte)0xb5);
    public static final PspMicroControllerCommandEnum PROJECTOR_1_OFF               = new PspMicroControllerCommandEnum(++_index, (byte)0xb6);
    public static final PspMicroControllerCommandEnum PROJECTOR_2_OFF               = new PspMicroControllerCommandEnum(++_index, (byte)0xb7);
    public static final PspMicroControllerCommandEnum PROJECTOR_3_OFF               = new PspMicroControllerCommandEnum(++_index, (byte)0xb8);
    public static final PspMicroControllerCommandEnum PROJECTOR_4_OFF               = new PspMicroControllerCommandEnum(++_index, (byte)0xb9);
    public static final PspMicroControllerCommandEnum ALL_PROJECTOR_LED_ENABLE      = new PspMicroControllerCommandEnum(++_index, (byte)0xc0);
    public static final PspMicroControllerCommandEnum PROJECTOR_1_LED_ENABLE        = new PspMicroControllerCommandEnum(++_index, (byte)0xc1);
    public static final PspMicroControllerCommandEnum PROJECTOR_2_LED_ENABLE        = new PspMicroControllerCommandEnum(++_index, (byte)0xc2);
    public static final PspMicroControllerCommandEnum PROJECTOR_3_LED_ENABLE        = new PspMicroControllerCommandEnum(++_index, (byte)0xc3);
    public static final PspMicroControllerCommandEnum PROJECTOR_4_LED_ENABLE        = new PspMicroControllerCommandEnum(++_index, (byte)0xc4);
    public static final PspMicroControllerCommandEnum ALL_PROJECTOR_LED_DISABLE     = new PspMicroControllerCommandEnum(++_index, (byte)0xc5);
    public static final PspMicroControllerCommandEnum PROJECTOR_1_LED_DISABLE       = new PspMicroControllerCommandEnum(++_index, (byte)0xc6);
    public static final PspMicroControllerCommandEnum PROJECTOR_2_LED_DISABLE       = new PspMicroControllerCommandEnum(++_index, (byte)0xc7);
    public static final PspMicroControllerCommandEnum PROJECTOR_3_LED_DISABLE       = new PspMicroControllerCommandEnum(++_index, (byte)0xc8);
    public static final PspMicroControllerCommandEnum PROJECTOR_4_LED_DISABLE       = new PspMicroControllerCommandEnum(++_index, (byte)0xc9);
    public static final PspMicroControllerCommandEnum FIRMWARE_VERSION_REQUEST      = new PspMicroControllerCommandEnum(++_index, (byte)0xf0);
    public static final PspMicroControllerCommandEnum PROJECTOR_1_FIRMWARE_VERSION  = new PspMicroControllerCommandEnum(++_index, (byte)0xf1);
    public static final PspMicroControllerCommandEnum PROJECTOR_2_FIRMWARE_VERSION  = new PspMicroControllerCommandEnum(++_index, (byte)0xf2);
    public static final PspMicroControllerCommandEnum PROJECTOR_3_FIRMWARE_VERSION  = new PspMicroControllerCommandEnum(++_index, (byte)0xf3);
    public static final PspMicroControllerCommandEnum PROJECTOR_4_FIRMWARE_VERSION  = new PspMicroControllerCommandEnum(++_index, (byte)0xf4);
    
    /**     
     * @author Cheah Lee Herng
    */
    private PspMicroControllerCommandEnum(int ndx, byte commandCode)
    {
        super(ndx);
        _commandCode = commandCode;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public byte getCommandCode()
    {
        return _commandCode;
    }
}
