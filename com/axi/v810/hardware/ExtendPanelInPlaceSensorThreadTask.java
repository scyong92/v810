package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class ExtendPanelInPlaceSensorThreadTask extends ThreadTask<Object>
{
  private PanelInPlaceSensor _panelInPlaceSensor;

  /**
   * @author Rex Shang
   */
  ExtendPanelInPlaceSensorThreadTask(PanelInPlaceSensor panelInPlaceSensor)
  {
    super("Extend Panel In Place Sensor Thead Task");
    Assert.expect(panelInPlaceSensor != null);
    _panelInPlaceSensor = panelInPlaceSensor;
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    if (_panelInPlaceSensor.isExtended() == false)
    {
      _panelInPlaceSensor.extend();
    }

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Rex Shang
   */
  protected void cancel() throws XrayTesterException
  {
    _panelInPlaceSensor.abort();
  }
}