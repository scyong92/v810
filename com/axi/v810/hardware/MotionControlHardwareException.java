package com.axi.v810.hardware;

import java.rmi.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;


/**
 * @author Greg Esparza
 */
public class MotionControlHardwareException extends HardwareException
{
  /**
   * @author Greg Esparza
   */
  public MotionControlHardwareException(String key, List<String> parameters)
  {
    super(new LocalizedString(key, parameters.toArray()));
  }

  /**
   * @author Greg Esparza
   */
  public MotionControlHardwareException(NativeHardwareException nhe)
  {
    super(new LocalizedString(nhe.getKey(), nhe.getParameters().toArray()));
    initCause(nhe);
  }

  /**
   * @param re
   * @author Chong, Wei Chin
   */
  public MotionControlHardwareException(RemoteException re)
  {
    super(new LocalizedString("HW_REMOTE_MOTION_CONTROL_REMOTE_EXCEPTION_KEY", null));
    initCause(re);
  }

  /**
   * @param nbe
   * @author Chong, Wei Chin
   */
  public MotionControlHardwareException(NotBoundException nbe)
  {
    super(new LocalizedString("HW_REMOTE_MOTION_CONTROL_NOT_BOUND_EXCEPTION_KEY", null));
    initCause(nbe);
  }

  /**
   * @param nbe
   * @author Chong, Wei Chin
   */
  public MotionControlHardwareException(Exception e)
  {
    super(new LocalizedString("HW_REMOTE_MOTION_CONTROL_REMOTE_EXCEPTION_KEY", null));
    initCause(e);
  }

  /**
   * @author Greg Esparza
   */
  public String getLocalizedMessage()
  {
    String finalMessage = null;
    LocalizedString header = new LocalizedString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY", null);
    finalMessage = StringLocalizer.keyToString(header);
    String mainMessageBody = StringLocalizer.keyToString(getLocalizedString());
    finalMessage += mainMessageBody;
    LocalizedString companyContactInformation = new LocalizedString("GUI_COMPANY_CONTACT_INFORMATION_KEY", null);
    finalMessage += StringLocalizer.keyToString(companyContactInformation);

    return finalMessage;
  }
}
