package com.axi.v810.hardware;

import java.io.*;
import java.rmi.*;
import java.util.*;

/**
 * @author rick
 */
public interface RemoteDigitalIoInterface extends Remote
{
  public void remoteInitialize()
    throws RemoteException, NativeHardwareException;

  public void remoteShutdown()
    throws RemoteException, NativeHardwareException;

  public void remoteSetHardwareResetMode(boolean hardwareResetModeOn)
    throws RemoteException, NativeHardwareException;

  public void remoteSetOutputBitState(int bitPosition, int bitState)
    throws RemoteException, NativeHardwareException;

  public int remoteGetInputBitState(int bitPosition)
    throws RemoteException, NativeHardwareException;

  public Serializable remoteGetConfigurationDescription(StringBuffer key, List<String> parameters)
    throws RemoteException, NativeHardwareException;

  public String remoteGetApplicationVersion() throws RemoteException;
}
