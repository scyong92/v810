package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class StageBeltsEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static StageBeltsEventEnum BELTS_ON = new StageBeltsEventEnum(++_index, "Stage Belts, On");
  public static StageBeltsEventEnum BELTS_OFF = new StageBeltsEventEnum(++_index, "Stage Belts, Off");
  public static StageBeltsEventEnum BELTS_DIRECTION_LEFT_TO_RIGHT = new StageBeltsEventEnum(++_index, "Stage Belts, Set Direction Left To Right");
  public static StageBeltsEventEnum BELTS_DIRECTION_RIGHT_TO_LEFT = new StageBeltsEventEnum(++_index, "Stage Belts, Set Direction Right To Left");

  private String _name;

  /**
   * @author Rex Shang
   */
  private StageBeltsEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
