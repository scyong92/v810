package com.axi.v810.hardware;

/**
 * @author Cheah Lee Herng
 */
public class SwitchModeEnum extends com.axi.util.Enum 
{
  private static int _index = -1;

  public static SwitchModeEnum MODE_0 = new SwitchModeEnum(++_index);
  public static SwitchModeEnum MODE_1 = new SwitchModeEnum(++_index);
  public static SwitchModeEnum MODE_2 = new SwitchModeEnum(++_index);
  
  /**
   * @author Cheah Lee Herng
   */
  private SwitchModeEnum(int id)
  {
    super(id);
  }
}
