package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraIdEnum extends com.axi.util.Enum implements Serializable
{
  private static HashMap<Integer, XrayCameraIdEnum> _integerToEnumMap;
  private static int _index = -1;

  /**
   * @author Greg Esparza
   */
  static
  {
    _integerToEnumMap = new HashMap<Integer, XrayCameraIdEnum>();
  }

  public static final XrayCameraIdEnum XRAY_CAMERA_0 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_1 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_2 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_3 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_4 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_5 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_6 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_7 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_8 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_9 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_10 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_11 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_12 = new XrayCameraIdEnum(++_index);
  public static final XrayCameraIdEnum XRAY_CAMERA_13 = new XrayCameraIdEnum(++_index);

  /**
   * @author Greg Esparza
   */
  private XrayCameraIdEnum(int id)
  {
    super(id);

    Assert.expect(_integerToEnumMap.put(new Integer(id), this) == null);
  }

  /**
   * @author Greg Esparza
   */
  static public XrayCameraIdEnum getEnum(Integer id)
  {
    Assert.expect(id != null);
    XrayCameraIdEnum enumValue = _integerToEnumMap.get(id);

    Assert.expect(enumValue != null);
    return enumValue;
  }

  /**
   * @author Greg Esparza
   */
  static public ArrayList<XrayCameraIdEnum> getAllEnums()
  {
    return new ArrayList<XrayCameraIdEnum>(_integerToEnumMap.values());
  }
}
