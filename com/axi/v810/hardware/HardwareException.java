package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
* If the hardware has any kind of problem this class or a subclass of this
* class will be thrown.
*
* @author Bill Darbie
*/
public class HardwareException extends XrayTesterException
{
  protected HardwareExceptionEnum _exceptionType;

  /**
  * Create a HardwareException.
  *
  * @author Bill Darbie
  */
  public HardwareException(LocalizedString localizedString)
  {
    super(localizedString);
    _exceptionType = EventDoesNotExistHardwareExceptionEnum.EXCEPTION_EVENT_DOES_NOT_EXIST;
  }

  /**
   * @author Greg Esparza
   */
  public HardwareExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }

  /**
  * Given the error code returned from the cxobj layer and a mapping of
  * error codes to localization keys, return the correct key.
  *
  * @param errorNumber is the number returned from a cxobj server.
  * @param errorMap is the mapping of error numbers to localization keys.
  * @return the localization key associated with the errorNumber passed in.
  * @author Bill Darbie
  */
  protected static String mapErrorNumberToKey(Map<Long, String>errorMap, Long errorNumber)
  {
    String key = errorMap.get(errorNumber);

    // if no value is found - put the error code in the message instead
    if (key == null)
      key = "Unrecognized error code: " + errorNumber;

    return key;
  }
}
