package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class HardwareEvent
{
  private Object _source;
  private HardwareEventEnum _hardwareEventEnum;
  private boolean _isStart;
  private boolean _isPrimary;

  /**
   * @author Bill Darbie
   */
  HardwareEvent(Object source, HardwareEventEnum hardwareEventEnum, boolean isStart, boolean isPrimary)
  {
    Assert.expect(source != null);
    Assert.expect(hardwareEventEnum != null);

    _isStart = isStart;
    _isPrimary = isPrimary;
    _source = source;
    _hardwareEventEnum = hardwareEventEnum;
  }

  /**
   * @author Bill Darbie
   */
  public Object getSource()
  {
    Assert.expect(_source != null);

    return _source;
  }

  /**
   * @author Bill Darbie
   */
  public HardwareEventEnum getEventEnum()
  {
    Assert.expect(_hardwareEventEnum != null);

    return _hardwareEventEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPrimary()
  {
    return _isPrimary;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isStart()
  {
    return _isStart;
  }
}
