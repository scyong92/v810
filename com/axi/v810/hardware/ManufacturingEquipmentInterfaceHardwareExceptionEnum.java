package com.axi.v810.hardware;

/**
 * Enum class used by ManufacturingEquipmentInterfaceHardwareException
 * @author Rex Shang
 */
public class ManufacturingEquipmentInterfaceHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static ManufacturingEquipmentInterfaceHardwareExceptionEnum UPSTREAM_SYSTEM_UNEXPECTEDLY_HAS_PANEL_EXCEPTION = new ManufacturingEquipmentInterfaceHardwareExceptionEnum(++_index);

  private ManufacturingEquipmentInterfaceHardwareExceptionEnum(int id)
  {
    super(id);
  }
}
