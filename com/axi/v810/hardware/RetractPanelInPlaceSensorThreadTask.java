package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class RetractPanelInPlaceSensorThreadTask extends ThreadTask<Object>
{
  private PanelInPlaceSensor _panelInPlaceSensor;

  /**
   * @author Bill Darbie
   */
  RetractPanelInPlaceSensorThreadTask(PanelInPlaceSensor panelInPlaceSensor)
  {
    super("Retract panel in place sensor thread task");
    Assert.expect(panelInPlaceSensor != null);

    _panelInPlaceSensor = panelInPlaceSensor;
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws Exception
  {
    if (_panelInPlaceSensor.isExtended())
    {
      _panelInPlaceSensor.retract();
    }
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
