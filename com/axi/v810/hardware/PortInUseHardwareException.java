package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port specified is in use by another resource, then throw this exception.
 *
 * @author Jeff Ryer
 */
class PortInUseHardwareException extends HardwareException
{
  /**
   * @author Jeff Ryer
   */
  PortInUseHardwareException(String portName,
                             String currentOwner)
  {
    super(new LocalizedString("HW_PORT_IN_USE_KEY",
                              new Object[]{portName,
                                           currentOwner}));
    Assert.expect(portName != null);
    Assert.expect(currentOwner != null);
  }
}
