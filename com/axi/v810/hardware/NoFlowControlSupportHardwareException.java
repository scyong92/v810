package com.axi.v810.hardware;

import gnu.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * If the serial port does not support the requested flow control, then throw this exception.
 *
 * @author Bob Balliew
 */
class NoFlowControlSupportHardwareException extends HardwareException
{
  static final String _ENABLED = StringLocalizer.keyToString(new LocalizedString("GUI_ENABLED_KEY", null));
  static final String _DISABLED = StringLocalizer.keyToString(new LocalizedString("ATGUI_DISABLED_KEY", null));

  static private String getEnabledOrDisabled(int flowControl, int mask)
  {
    return ((flowControl & mask) != 0) ? _ENABLED : _DISABLED;
  }
  /**
   * This is for flow control setup failure on the serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param flowControl is the encoded value specified to the driver
   * @author Bob Balliew
   */
  public NoFlowControlSupportHardwareException(String portName, int flowControl)
  {
    super(new LocalizedString("HW_NO_FLOW_CONTROL_SUPPORT_EXCEPTION_KEY",
                              new Object[]{
                                            portName,
                                            getEnabledOrDisabled(flowControl, SerialPort.FLOWCONTROL_RTSCTS_IN),
                                            getEnabledOrDisabled(flowControl, SerialPort.FLOWCONTROL_RTSCTS_OUT),
                                            getEnabledOrDisabled(flowControl, SerialPort.FLOWCONTROL_XONXOFF_IN),
                                            getEnabledOrDisabled(flowControl, SerialPort.FLOWCONTROL_XONXOFF_OUT),
                                          }));

    Assert.expect(portName != null);
    Assert.expect(flowControl >= 0);
  }
}
