package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalShiftNameEnum extends com.axi.util.Enum implements Comparable
{
    private static int _index = -1;
    
    public static final OpticalShiftNameEnum SHIFT_ONE = new OpticalShiftNameEnum(++_index, "Optical Shift One");
    public static final OpticalShiftNameEnum SHIFT_TWO = new OpticalShiftNameEnum(++_index, "Optical Shift Two");
    public static final OpticalShiftNameEnum SHIFT_THREE = new OpticalShiftNameEnum(++_index, "Optical Shift Three");
    public static final OpticalShiftNameEnum SHIFT_FOUR = new OpticalShiftNameEnum(++_index, "Optical Shift Four");
    public static final OpticalShiftNameEnum WHITE_LIGHT = new OpticalShiftNameEnum(++_index, "Optical White Light");    
    public static final OpticalShiftNameEnum SHIFT_FIVE = new OpticalShiftNameEnum(++_index, "Optical Shift Five");
    public static final OpticalShiftNameEnum SHIFT_SIX = new OpticalShiftNameEnum(++_index, "Optical Shift Six");
    public static final OpticalShiftNameEnum SHIFT_SEVEN = new OpticalShiftNameEnum(++_index, "Optical Shift Seven");
    public static final OpticalShiftNameEnum SHIFT_EIGHT = new OpticalShiftNameEnum(++_index, "Optical Shift Eight");
    
    private String _name;
    
    /**
     * @author Cheah Lee Herng
     */
    private OpticalShiftNameEnum(int id, String name)
    {
        super(id);
        Assert.expect(name != null);

        _name = name;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public String getName()
    {
        Assert.expect(_name != null);
        return _name;
    }
    
    /**
    * @author Cheah Lee Herng
    */
    public int compareTo(Object object)
    {
      final OpticalShiftNameEnum opticalShiftNameEnum = (OpticalShiftNameEnum)object;

      if (getId() < opticalShiftNameEnum.getId())
      {
        return -1;
      }
      else if (getId() > opticalShiftNameEnum.getId())
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }
}
