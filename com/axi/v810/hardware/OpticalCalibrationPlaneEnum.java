package com.axi.v810.hardware;

import com.axi.util.Enum;

/**
 * Enum class to simplify evaluating Camera Image data values
 * Please do not modify the sequence of index below as this may
 * impact the operation of calibration.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalCalibrationPlaneEnum extends Enum
{
    private static int index = 0;
    public static final OpticalCalibrationPlaneEnum REFERENCE_PLANE = new OpticalCalibrationPlaneEnum(index++);
    public static final OpticalCalibrationPlaneEnum OBJECT_PLANE_MINUS_TWO = new OpticalCalibrationPlaneEnum(index++);
    public static final OpticalCalibrationPlaneEnum OBJECT_PLANE_PLUS_THREE = new OpticalCalibrationPlaneEnum(index++);
    public static final OpticalCalibrationPlaneEnum OBJECT_PLANE_MINUS_ONE = new OpticalCalibrationPlaneEnum(index++);
    public static final OpticalCalibrationPlaneEnum OBJECT_PLANE_PLUS_FOUR = new OpticalCalibrationPlaneEnum(index++);
    
    /**
     * CameraImageDataTypeEnum
     *
     * @author Reid Hayhow
    */
    protected OpticalCalibrationPlaneEnum(int id)
    {
        super(id);
    }
}
