package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left outer barrier
 * @author Rex Shang
 */
class Test_XrayCylinder extends UnitTest
{
  private XrayCylinder _xrayCylinder = null;

  private Test_XrayCylinder()
  {
    _xrayCylinder = XrayCylinder.getInstance();
  }

  public static void main(String[] args)
  {
    Test_XrayCylinder _test_XrayCylinder = new Test_XrayCylinder();
    UnitTest.execute(_test_XrayCylinder);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Move the xray cylinder up and verify 
      _xrayCylinder.up(false); //false = without loading camera config
      Expect.expect(_xrayCylinder.isUp() == true);

      // Move the  xray cylinder down and verify.
      _xrayCylinder.down(false); //false = without loading camera config
      Expect.expect(_xrayCylinder.isDown() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
