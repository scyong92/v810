package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Greg Esparza
 */
public class MotionControllerIdEnum extends com.axi.util.Enum implements Serializable
{
  private static HashMap<Integer, MotionControllerIdEnum> _integerToEnumMap;

  /**
   * @author Rex Shang
   */
  static
  {
    _integerToEnumMap = new HashMap<Integer, MotionControllerIdEnum>();
  }

  public static final MotionControllerIdEnum MOTION_CONTROLLER_0 = new MotionControllerIdEnum(HardwareConfigEnum.getMotionControllerId0());

  /**
   * @author Greg Esparza
   */
  private MotionControllerIdEnum(Integer id)
  {
    super(id.intValue());

    Assert.expect(_integerToEnumMap.put(id, this) == null);
  }

  /**
   * @author Rex Shang
   */
  static public MotionControllerIdEnum getEnum(Integer integerKey)
  {
    Assert.expect(integerKey != null);
    MotionControllerIdEnum myEnum = _integerToEnumMap.get(integerKey);

    Assert.expect(myEnum != null);
    return myEnum;
  }

  /**
   * @author Greg Esparza
   */
  static public ArrayList<MotionControllerIdEnum> getAllEnums()
  {
    return new ArrayList<MotionControllerIdEnum>(_integerToEnumMap.values());
  }
}
