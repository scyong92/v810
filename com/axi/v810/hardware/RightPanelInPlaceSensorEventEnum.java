package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class RightPanelInPlaceSensorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static RightPanelInPlaceSensorEventEnum SENSOR_EXTEND = new RightPanelInPlaceSensorEventEnum(++_index, "Right Panel In Place Sensor, Extend");
  public static RightPanelInPlaceSensorEventEnum SENSOR_RETRACT = new RightPanelInPlaceSensorEventEnum(++_index, "Right Panel In Place Sensor, Retract");

  private String _name;

  /**
   * @author Rex Shang
   */
  private RightPanelInPlaceSensorEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
