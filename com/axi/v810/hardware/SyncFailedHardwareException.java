package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If a SyncFailedException occurs, then throw this exception.
 *
 * @author Bob Balliew
 */
class SyncFailedHardwareException extends HardwareException
{
  /**
   * This is for InterruptedIOExceptions.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Bob Balliew
   */
  public SyncFailedHardwareException(String portName)
  {
    super(new LocalizedString("HW_SYNC_FAILED_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }

  /**
   * This is for SyncFailedExceptions with a detailed message.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param detailedMessage is the (non null) string returned by 'getMessage()'
   * @author Bob Balliew
   */
  public SyncFailedHardwareException(String portName, String detailedMessage)
  {
    super(new LocalizedString("HW_SYNC_FAILED_WITH_MESSAGE_EXCEPTION_KEY",
                              new Object[]{portName, detailedMessage}));

    Assert.expect(portName != null);
    Assert.expect(detailedMessage != null);
  }
}
