package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If there is a timeout waiting for data on the serial port, then throw this exception.
 *
 * @author Jeff Ryer
 */
class SerialCommunicationTimeoutHardwareException extends HardwareException
{
  /**
   * This is for a timeout on the serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Jeff Ryer
   */
  public SerialCommunicationTimeoutHardwareException(String portName)
  {
    super(new LocalizedString("HW_SERIAL_TIMEOUT_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }

}

