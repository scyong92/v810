package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * As users instruct the hardware to make measurements, specific subsystems will
 * be made ready to begin inspections.   There is a very specific order
 * controlled by the XrayTester object.
 *
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Agilent Technogies</p>
 * @author Roy Williams
 */
public interface HardwareStartupShutdownInt
{
  /**
   * Tell this hardware to perform any actions required to startup on behalf
   * of the XrayTester object.
   *
   * @author Roy Williams
   */
  void startup() throws XrayTesterException;

  /**
   * Tell this hardware to perform any actions required to shutdown on behalf
   * of the XrayTester object.
   *
   * @author Roy Williams
   */
  void shutdown() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  boolean isStartupRequired() throws XrayTesterException;
}
