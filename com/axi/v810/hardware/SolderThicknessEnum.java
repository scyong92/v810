package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import java.util.*;

/**
 * Defines the various solder thickness tables that can be selected
 * @author Eddie Williamson
 */
public class SolderThicknessEnum extends com.axi.util.Enum
{
  // Enum support and definitions
  private static int index = 0;
  
  public static List <SolderThicknessEnum> _solderThicknessEnumFullList = new ArrayList<SolderThicknessEnum>();

  public static SolderThicknessEnum LEAD_SOLDER_SHADED_BY_COPPER = new SolderThicknessEnum(index++,
      FileName.getSolderThicknessTable1FileName(), "LEAD_SOLDER_SHADED_BY_COPPER");

//  public static SolderThicknessEnum LEAD_SOLDER_SHADED_BY_COPPER_WITH_HIG_MAG = new SolderThicknessEnum(index++,
//      FileName.getSolderThicknessTableInHighMagFileName(), "LEAD_SOLDER_SHADED_BY_COPPER_WITH_HIG_MAG");
//  
//  public static SolderThicknessEnum LOWMAG_GAIN1 = new SolderThicknessEnum(index++,
//      FileName.getSolderThicknessTableLowMagGain1FileName(), "LOWMAG_GAIN1");
//
//  public static SolderThicknessEnum HIGHMAG_GAIN1 = new SolderThicknessEnum(index++,
//      FileName.getSolderThicknessTableHighMagGain1FileName(), "HIGHMAG_GAIN1");

// Other table possibilities:
//   LEAD_SOLDER_SHADED_BY_SOLDER
//   NOLEAD_SOLDER_SHADED_BY_COPPER
//   NOLEAD_SOLDER_SHADED_BY_SOLDER


  // object members
  private String _fileName = null;
  private String _solderThicknessName = null;

  /**
   * @author Eddie Williamson
   * @param id
   * @param key Full path to the underlying data file.
   */
  private SolderThicknessEnum(int id, String fileName, String solderThicknessName)
  {
    super(id);
    Assert.expect(fileName != null);
    Assert.expect(solderThicknessName != null);

    _fileName = fileName;
    _solderThicknessName = solderThicknessName;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public String getFileName()
  {
    Assert.expect(_fileName != null);
    return _fileName;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public String getSolderThicknessName()
  {
    Assert.expect(_solderThicknessName != null);
    return _solderThicknessName;
  }
  
  /**
   * @author Swee-Yee.Wong
   * @return Get the full enum list.
   */
  public static List <SolderThicknessEnum> getFullEnumList()
  {
    _solderThicknessEnumFullList.add(LEAD_SOLDER_SHADED_BY_COPPER);
    return _solderThicknessEnumFullList;
  }
}
