:: -----------------------------------------------------------------------------
:: (C) Copyright 2010 ViTrox Technologies. All rights reserved
:: -----------------------------------------------------------------------------
:: @author Lee Herng, Cheah
:: compile JNI and package it into nativeUEye.dll
::
@echo off

SET MODE=Debug
IF "%1"=="Debug" set MODE=Debug
IF "%1"=="Release" set MODE=Release

set OLDPATH=%PATH%
set VSTUDIO_DIR=C:/apps/vStudio2012
set VSTUDIO2008_DIR=C:/apps/vStudio2012
set JDK_PATH=C:/apps/java/jdk1.7.0
set WINSDK_PATH=../../../../../thirdParty/microsoft/WINSDK/v8.1
set IPPROOT=C:/apps/Intel/IPP/5.1/em64t
set QT_PATH=C:/apps/Qt/5.0.2/5.0.2/msvc2012_64
set VCVARS_PATH=%VSTUDIO_DIR%/VC/bin/amd64
set LIB_CLASSPATH=../../../../jre/1.7.0/lib/ext/comm.jar;../../../../jre/1.7.0/lib/ext/j3daudio.jar;../../../../jre/1.7.0/lib/ext/j3dcore.jar;../../../../jre/1.7.0/lib/ext/j3dutils.jar;../../../../jre/1.7.0/lib/ext/vecmath.jar;../../../../jre/1.7.0/lib/ext/Jama-1.0.2.jar;../../../../jre/1.6.0_21/lib/ext/jcommon-1.0.0.jar;../../../../jre/1.7.0/lib/ext/jfreechart-1.0.1.jar
set INCLUDE_VC=-I%VSTUDIO_DIR%/VC/include -I"%WINSDK_PATH%/include" -I%JDK_PATH%/include -I%JDK_PATH%/include/win32 -I../../../.. -I../../../../.. -I../../../../../cpp -I../../../../../cpp/util/src -I%QT_PATH%/include -I%IPPROOT%/include
IF "%MODE%"=="Release" set VC_FLAG=-D_WINDOWS -DWIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -EHsc -O2 -MD
IF "%MODE%"=="Debug" set VC_FLAG=-D_WINDOWS -DWIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -D_Debug -EHsc -O2 -MD
set DEBUG_FLAG=-DEBUG
IF "%MODE%"=="Release" set DEBUG_FLAG= 
set OBJ_PATH=../../../../buildFiles/jniMscpp/jniLoadHandler.obj ../../../../buildFiles/jniMscpp/com_axi_v810_hardware_uEyeOpticalCamera.obj ../../../../buildFiles/jniMscpp/jniImageUtil.obj
set PATH=%PATH%;%JDK_PATH%\bin;%VCVARS_PATH%
set TMP=%TEMP%

echo "#### generating JNI header file from com.axi.v810.hardware.uEyeOpticalCamera ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.v810.hardware.uEyeOpticalCamera
@echo off

echo "#### building nativeUEye.dll ####"
@echo on
%VCVARS_PATH%\cl -c -D_NATIVE_UEYE_DLL %VC_FLAG% %INCLUDE_VC% com_axi_v810_hardware_uEyeOpticalCamera.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_v810_hardware_uEyeOpticalCamera.obj

%VCVARS_PATH%\link %OBJ_PATH% -ignore:4089 -out:../../../../jniRelease/bin/nativeUEye.dll -LIBPATH:../../../../../cpp/release/bin -LIBPATH:%VSTUDIO_DIR%/VC/lib/amd64 -LIBPATH:"%WINSDK_PATH%/Lib/x64" -DLL %DEBUG_FLAG% ../../../../../cpp/release/bin/AgtAxiIPP.lib ../../../../../cpp/release/bin/axiUtil.lib ../../../../../cpp/release/bin/uEye_api_64.lib ../../../../../cpp/release/bin/Psp3DAlgo.lib ../../../../../cpp/release/bin/Qt5Core.lib ../../../../../cpp/release/bin/Qt5Gui.lib

"%WINSDK_PATH%\Bin\x64\mt" -manifest ../../../../jniRelease/bin/nativeUEye.dll.manifest -outputresource:../../../../jniRelease/bin/nativeUEye.dll;2

@echo off
:: clean up environment space
set PATH= %OLDPATH%
set OLDPATH=
set VCVARS_PATH=
set JDK_PATH=