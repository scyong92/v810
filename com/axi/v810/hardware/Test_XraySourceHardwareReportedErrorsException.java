package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * This class tests the XraySourceHardwareReportedErrorsException class.
 * @author Greg Loring
 */
public class Test_XraySourceHardwareReportedErrorsException extends UnitTest
{

  /**
   * @author Greg Loring
   */
  public static void main(String[] argsIgnored)
  {
    UnitTest.execute(new Test_XraySourceHardwareReportedErrorsException());
  }

  /**
   * @author Greg Loring
   */
  public void test(BufferedReader inIgnored, PrintWriter outIgnored)
  {
    XraySourceHardwareReportedErrorsException xshrex;
    String errors;

    // this is the case that is expected to occur most often
    xshrex = new XraySourceHardwareReportedErrorsException(1 + 2);
    errors = xshrex.getMessage();
    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   != -1);
    Expect.expect(errors.indexOf("Interlock 1 Off")  != -1);
    Expect.expect(errors.indexOf("Interlock 2 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 3 Off")  == -1);
    Expect.expect(errors.indexOf("Regulation Error") == -1);
    Expect.expect(errors.indexOf("Arc Error")        == -1);
    Expect.expect(errors.indexOf("Over Voltage")     == -1);
    Expect.expect(errors.indexOf("Over Current")     == -1);
    Expect.expect(errors.indexOf("Temperature Limit") == -1);
    Expect.expect(errors.indexOf("Communication error on fiber link") == -1);

    // read (1<<x) as 2 to the x
    xshrex = new XraySourceHardwareReportedErrorsException((1<<2) + (1<<3) + (1<<4));
    errors = xshrex.getMessage();
    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   == -1);
    Expect.expect(errors.indexOf("Interlock 1 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 2 Off")  != -1);
    Expect.expect(errors.indexOf("Interlock 3 Off")  != -1);
    Expect.expect(errors.indexOf("Regulation Error") != -1);
    Expect.expect(errors.indexOf("Arc Error")        == -1);
    Expect.expect(errors.indexOf("Over Voltage")     == -1);
    Expect.expect(errors.indexOf("Over Current")     == -1);
    Expect.expect(errors.indexOf("Temperature Limit") == -1);
    Expect.expect(errors.indexOf("Communication error on fiber link") == -1);

    // read (1<<x) as 2 to the x
    xshrex = new XraySourceHardwareReportedErrorsException((1<<5) + (1<<6) + (1<<7));
    errors = xshrex.getMessage();
    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   == -1);
    Expect.expect(errors.indexOf("Interlock 1 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 2 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 3 Off")  == -1);
    Expect.expect(errors.indexOf("Regulation Error") == -1);
    Expect.expect(errors.indexOf("Arc Error")        != -1);
    Expect.expect(errors.indexOf("Over Voltage")     != -1);
    Expect.expect(errors.indexOf("Over Current")     != -1);
    Expect.expect(errors.indexOf("Temperature Limit") == -1);
    Expect.expect(errors.indexOf("Communication error on fiber link") == -1);

    // read (1<<x) as 2 to the x
    xshrex = new XraySourceHardwareReportedErrorsException((1<<8) + (1<<9));
    errors = xshrex.getMessage();
    Expect.expect(errors.indexOf("SHUTDOWN ERROR")   == -1);
    Expect.expect(errors.indexOf("Interlock 1 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 2 Off")  == -1);
    Expect.expect(errors.indexOf("Interlock 3 Off")  == -1);
    Expect.expect(errors.indexOf("Regulation Error") == -1);
    Expect.expect(errors.indexOf("Arc Error")        == -1);
    Expect.expect(errors.indexOf("Over Voltage")     == -1);
    Expect.expect(errors.indexOf("Over Current")     == -1);
    Expect.expect(errors.indexOf("Temperature Limit") != -1);
    Expect.expect(errors.indexOf("Communication error on fiber link") != -1);
  }

}
