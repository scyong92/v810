package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraModelTypeEnum extends com.axi.util.Enum implements Serializable
{
    private static int _index = -1;
    private String _name;
    private static Map<String, OpticalCameraModelTypeEnum> _opticalCameraModelNameToModelTypeEnumMap;

    /**
    * @author Greg Esparza
    */
    static
    {
        _opticalCameraModelNameToModelTypeEnumMap = new HashMap<String, OpticalCameraModelTypeEnum>();
    }

    public static final OpticalCameraModelTypeEnum  EdmundOptic_uEye = new OpticalCameraModelTypeEnum(++_index, HardwareConfigEnum.getAxiOpticalCameraModelName());
    public static final OpticalCameraModelTypeEnum  Sentech_usbCam = new OpticalCameraModelTypeEnum(++_index, HardwareConfigEnum.getAxiOpticalCameraModelNameTwo());

    /**
    * @author Cheah Lee Herng
    */
    private OpticalCameraModelTypeEnum(int id, String name)
    {
        super(id);
        Assert.expect(name != null);
        
        _name = name;
        _opticalCameraModelNameToModelTypeEnumMap.put(name, this);
    }

    /**
    * @author Cheah Lee Herng
    */
    static OpticalCameraModelTypeEnum getEnum(String opticalCameraModelName)
    {
        Assert.expect(opticalCameraModelName != null);

        OpticalCameraModelTypeEnum opticalCameraModelTypeEnum = _opticalCameraModelNameToModelTypeEnumMap.get(opticalCameraModelName);
        Assert.expect(opticalCameraModelTypeEnum != null);

        return opticalCameraModelTypeEnum;
    }

    /**
    * @author Cheah Lee Herng
    */
    String getName()
    {
        return _name;
    }
}
