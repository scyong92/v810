package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * This class represents the PSP calibration version.
 * 
 * Version 1:
 *  -> Manual-loaded calibration jig
 * 
 * Version 2:
 *  -> On-Stage Calibration Jig + Buy-Off Jig
 * 
 * @author Cheah Lee Herng
 */
public class PspCalibrationVersionEnum extends com.axi.util.Enum 
{
  private static int _index = 1;
  private int _version;
  private static Map<Integer, PspCalibrationVersionEnum> _pspCalibrationVersionToVersionEnumMap;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    _pspCalibrationVersionToVersionEnumMap = new HashMap<Integer, PspCalibrationVersionEnum>();
  }

  public static final PspCalibrationVersionEnum VERSION_1  = new PspCalibrationVersionEnum(++_index, 1);
  public static final PspCalibrationVersionEnum VERSION_2  = new PspCalibrationVersionEnum(++_index, 2);
  public static final PspCalibrationVersionEnum VERSION_3  = new PspCalibrationVersionEnum(++_index, 3);
  public static final PspCalibrationVersionEnum VERSION_4  = new PspCalibrationVersionEnum(++_index, 4);
  
  /**
   * @author Cheah Lee Herng 
   */
  private PspCalibrationVersionEnum(int id, int version)
  {
    super(id);
    _version = version;
    
    _pspCalibrationVersionToVersionEnumMap.put(version, this);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public static PspCalibrationVersionEnum getEnum(int version)
  {
    PspCalibrationVersionEnum pspCalibrationVersionEnum = _pspCalibrationVersionToVersionEnumMap.get(version);
    Assert.expect(pspCalibrationVersionEnum != null);

    return pspCalibrationVersionEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getVersion()
  {
    return _version;
  }
}
