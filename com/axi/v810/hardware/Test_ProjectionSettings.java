package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * Tests the ProjectionSettings class
 * @author George A. David
 */
public class Test_ProjectionSettings extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProjectionSettings());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ScanPass scanPass = new ScanPass();
    scanPass.setId(5);
    int startCaptureLine = 133;
    int numberOfCaptureLines = 1574;
    List<ProjectionRegion> projectionRegions = new ArrayList<ProjectionRegion>();


    try
    {
      new ProjectionSettings(scanPass,
                             -1,
                             numberOfCaptureLines,
                             projectionRegions);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    try
    {
      new ProjectionSettings(scanPass,
                             startCaptureLine,
                             -1,
                             projectionRegions);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    try
    {
      new ProjectionSettings(scanPass,
                             startCaptureLine,
                             numberOfCaptureLines,
                             null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    ProjectionSettings settings = new ProjectionSettings(scanPass,
                                                         startCaptureLine,
                                                         numberOfCaptureLines,
                                                         projectionRegions);

    Expect.expect(settings.getScanPassNumber() == scanPass.getId());
    Expect.expect(settings.getStartCaptureLine() == startCaptureLine);
    Expect.expect(settings.getNumberOfCaptureLines() == numberOfCaptureLines);
    Expect.expect(settings.getProjectionRegions() == projectionRegions);


  }
}
