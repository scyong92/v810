package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class MotionProfile
{
  private MotionProfileTypeEnum _motionProfileType;
  private int _motionProfileId;
  private PointToPointMotionProfileEnum _pointToPointMotionProfile;
  private ScanMotionProfileEnum _scanMotionProfile;

  /**
   * @author Greg Esparza
   */
  public MotionProfile(MotionProfileTypeEnum motionProfileType, int motionProfileId)
  {
    Assert.expect(motionProfileType != null);

    _motionProfileType = motionProfileType;
    _motionProfileId = motionProfileId;

    if (_motionProfileType.equals(MotionProfileTypeEnum.POINT_TO_POINT))
      _pointToPointMotionProfile = PointToPointMotionProfileEnum.getEnum(_motionProfileId);
    else if (_motionProfileType.equals(MotionProfileTypeEnum.SCAN))
      _scanMotionProfile = ScanMotionProfileEnum.getEnum(_motionProfileId);
    else
      Assert.expect(false);
  }

  /**
   * @author Greg Esparza
   */
  public MotionProfile(PointToPointMotionProfileEnum pointToPointMotionProfile)
  {
    Assert.expect(pointToPointMotionProfile != null);
    _motionProfileType = MotionProfileTypeEnum.POINT_TO_POINT;
    _pointToPointMotionProfile = pointToPointMotionProfile;
    _motionProfileId = pointToPointMotionProfile.getId();
  }

  /**
   * @author Greg Esparza
   */
  public MotionProfile(ScanMotionProfileEnum scanMotionProfile)
  {
    Assert.expect(scanMotionProfile != null);
    _motionProfileType = MotionProfileTypeEnum.SCAN;
    _scanMotionProfile = scanMotionProfile;
    _motionProfileId = scanMotionProfile.getId();
  }

  /**
   * @author Greg Esparza
   */
  MotionProfileTypeEnum getMotionProfileType()
  {
    return _motionProfileType;
  }

  /**
   * @author Greg Esparza
   */
  PointToPointMotionProfileEnum getPointToPointMotionProfile()
  {
    Assert.expect(_pointToPointMotionProfile != null);
    return _pointToPointMotionProfile;
  }

  /**
   * @author Greg Esparza
   */
  ScanMotionProfileEnum getScanMotionProfile()
  {
    Assert.expect(_scanMotionProfile != null);
    return _scanMotionProfile;
  }

  /**
   * @author Greg Esparza
   */
  int getId()
  {
    int id = 0;

    if (_motionProfileType.equals(MotionProfileTypeEnum.POINT_TO_POINT))
      id = _pointToPointMotionProfile.getId();
    else if (_motionProfileType.equals(MotionProfileTypeEnum.SCAN))
      id = _scanMotionProfile.getId();
    else
      Assert.expect(false);

    return id;
  }
}
