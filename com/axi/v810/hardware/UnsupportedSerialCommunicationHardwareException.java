package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port parameters are not supported, throw this exception.
 *
 * @author Jeff Ryer
 */
class UnsupportedSerialCommunicationHardwareException extends HardwareException
{
  /**
   * @author Jeff Ryer
   */
  UnsupportedSerialCommunicationHardwareException(String portName,
                                                  String baudRate,
                                                  String dataBits,
                                                  String stopBits,
                                                  String parity)
  {
    super(new LocalizedString("HW_UNSUPPORTED_SERIAL_COMMUNICATION_PARAMETERS_KEY",
                              new Object[]{portName, baudRate, dataBits, stopBits, parity}));
    Assert.expect(portName != null);
    Assert.expect(baudRate != null);
    Assert.expect(dataBits != null);
    Assert.expect(stopBits != null);
    Assert.expect(parity != null);
  }

  /**
 * @author Jeff Ryer
 */
  UnsupportedSerialCommunicationHardwareException(String portName,
                                                  int timeoutInMilliseconds)
  {
    super(new LocalizedString("HW_UNSUPPORTED_SERIAL_COMMUNICATION_RECEIVE_TIMEOUT_KEY",
                              new Object[]{portName,
                                           new Integer(timeoutInMilliseconds) }));
    Assert.expect(portName != null);
    Assert.expect(timeoutInMilliseconds > 0);
  }
}
