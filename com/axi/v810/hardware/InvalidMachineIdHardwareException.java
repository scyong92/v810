package com.axi.v810.hardware;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class InvalidMachineIdHardwareException extends HardwareException
{
  public InvalidMachineIdHardwareException(String configFile, String configFileSetting, String registrySetting)
  {
    super(new LocalizedString("HW_INVALID_MACHINE_ID_KEY", new Object[]{configFile, configFileSetting, registrySetting}));
  }
}
