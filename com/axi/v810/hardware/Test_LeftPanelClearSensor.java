package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Unit test for left panel clear sensor
 * @author Rex Shang
 */
class Test_LeftPanelClearSensor extends UnitTest
{
  private LeftPanelClearSensor _leftPanelClearSensor = null;

  private Test_LeftPanelClearSensor()
  {
    _leftPanelClearSensor = LeftPanelClearSensor.getInstance();
  }

  public static void main(String[] args)
  {
    Test_LeftPanelClearSensor _test_LeftPanelClearSensor = new Test_LeftPanelClearSensor();
    UnitTest.execute(_test_LeftPanelClearSensor);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // XCR-3604 Unit Test Phase 2
      if (DigitalIo.getInstance().isStartupRequired())
        DigitalIo.getInstance().startup();
      
      _leftPanelClearSensor.isBlocked();
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
  }
}
