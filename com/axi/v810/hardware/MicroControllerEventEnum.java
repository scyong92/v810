package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MicroControllerEventEnum extends HardwareEventEnum
{
    private static int _index = -1;

    public static MicroControllerEventEnum STARTUP = new MicroControllerEventEnum(++_index, "MicroController, Startup");
    public static MicroControllerEventEnum SHUTDOWN = new MicroControllerEventEnum(++_index, "MicroController, Shutdown");
    public static MicroControllerEventEnum ON = new MicroControllerEventEnum(++_index, "MicroController, On");
    public static MicroControllerEventEnum OFF = new MicroControllerEventEnum(++_index, "MicroController, Off");

    private String _name;

    /**
     * @author Cheah Lee Herng
    */
    private MicroControllerEventEnum(int id, String name)
    {
        super(id);
        Assert.expect(name != null);

        _name = name;
    }

    /**
    * @author Cheah Lee Herng
    */
    public String toString()
    {
        return _name;
    }
}
