package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * This class gets an image whenever the SnapInspectionImageCommand is executed.
 * Any observers of this class will get an update call for each image snapped.
 *
 * @author Bill Darbie
 */
public class XrayImageObservable extends Observable
{
  private static XrayImageObservable _instance;

  /**
   * Do not allow the use of this objects constuctor, use getInstance instead.
   *
   * @author Bill Darbie
   */
  private XrayImageObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized XrayImageObservable getInstance()
  {
    if (_instance == null)
      _instance = new XrayImageObservable();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  void addImage(XrayCameraImage image)
  {
    Assert.expect(image != null);

    setChanged();
    notifyObservers(image);
  }
}
