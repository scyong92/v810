package com.axi.v810.hardware;

import com.axi.util.*;


/**
 *
 * @author Kee Chin Seong
 */
public class SerialPortCommEventEnum extends HardwareEventEnum
{
  private static int _index = -1;
  
  public static SerialPortCommEventEnum INITIALIZE = new SerialPortCommEventEnum(++_index, "SeiralPortCommunication, Initialize");
  public static SerialPortCommEventEnum CONNECT = new SerialPortCommEventEnum(++_index, "SeiralPortCommunication, Connect");
  public static SerialPortCommEventEnum DISCONNECT = new SerialPortCommEventEnum(++_index, "SeiralPortCommunication, Disconnect");

  private String _name;

  /**
   * @author Kee Chin Seong
   */
  private SerialPortCommEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Kee Chin Seong
   */
  public String toString()
  {
    return _name;
  }  
}
