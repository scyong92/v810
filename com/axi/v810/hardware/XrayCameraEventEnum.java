package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static XrayCameraEventEnum INITIALIZE = new XrayCameraEventEnum(++_index, "Xray Camera, Initialize");
  public static XrayCameraEventEnum SET_SCAN_PROGRAM = new XrayCameraEventEnum(++_index, "Xray Camera, Set Scan Program");
  public static XrayCameraEventEnum ENABLE_IMAGE_CORRECTION = new XrayCameraEventEnum(++_index, "Xray Camera, Enable Image Correction");
  public static XrayCameraEventEnum DISABLE_IMAGE_CORRECTION = new XrayCameraEventEnum(++_index, "Xray Camera, Disable Image Correction");
  public static XrayCameraEventEnum ENABLE_TRIGGER_DETECTION = new XrayCameraEventEnum(++_index, "Xray Camera, Enable Trigger Detection");
  public static XrayCameraEventEnum DISABLE_TRIGGER_DETECTION = new XrayCameraEventEnum(++_index, "Xray Camera, Disable Trigger Detection");
  public static XrayCameraEventEnum INITIALIZE_ACQUISITION = new XrayCameraEventEnum(++_index, "Xray Camera, Initialize Acquisition");
  public static XrayCameraEventEnum RUN_SENSOR_PIXEL_GAIN_CALIBRATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Sensor Pixel Gain Calibration");
  public static XrayCameraEventEnum RUN_SENSOR_PIXEL_OFFSET_CALIBRATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Sensor Pixel Offset Calibration");
  public static XrayCameraEventEnum RUN_SENSOR_SEGMMENT_GAIN_CALIBRATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Sensor Segment Gain Calibration");
  public static XrayCameraEventEnum RUN_SENSOR_SEGMENT_OFFSET_CALIBRATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Sensor Segment Offset Calibration");
  //Variable Mag Anthony August 2011
  public static XrayCameraEventEnum RUN_SAVE_VARIABLE_MAG_CALIBRATION_CONFIGURATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Save Variable Mag Calibration Configuration");
  public static XrayCameraEventEnum RUN_LOAD_VARIABLE_MAG_CALIBRATION_CONFIGURATION = new XrayCameraEventEnum(++_index, "Xray Camera, Run Load Variable Mag Calibration Configuration");
  
  //swee-yee.wong - XCR-3125 Support new camera upgrade firmware function
  public static XrayCameraEventEnum VERIFY_FILE_VERSION = new XrayCameraEventEnum(++_index, "Verifying X-Ray Camera Firmware Version");
  public static XrayCameraEventEnum UPDATE_FIRMWARE = new XrayCameraEventEnum(++_index, "Upgrading X-Ray Camera Firmware");
  public static XrayCameraEventEnum UPLOAD_CAMERA_FIRMFARE_FILE = new XrayCameraEventEnum(++_index, "Uploading X-Ray Camera Firmware File");

  private String _name;

  /**
   * @author Rex Shang
   */
  private XrayCameraEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
