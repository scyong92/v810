package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalPointToPointScan implements Comparable 
{
  private int _id = -1;
  private StagePosition _pointToPointStagePositionInNanoMeters = null;
  private OpticalCameraIdEnum _cameraId = null;
  private String _regionPositionName = null;
  private String _pointPositionName = null;
  private Map<String,DoubleRef> _pointPanelPositionNameToZHeightInNanometersMap;
  private Map<String,BooleanRef> _pointPanelPositionNameToZHeightValidMap;
  private Map<String,Pair<Integer,Integer>> _pointPanelPositionNameToPointPositionInPixelMap;
  private Map<String,Pair<Integer,Integer>> _pointPanelPositionNameToNewPointPositionInPixelMap;    
  private String _alignmentGroupName = null;
  private OpticalCalibrationProfileEnum _opticalCalibrationProfileEnum = null;

  private double _averageZHeightInNanometer;
  private boolean _validAverageZHeight;

  // Name to used to link to OpticalCameraRectangle
  private String _opticalCameraRectangleName;

  // ROI width and height
  private int _roiWidth;
  private int _roiHeight;

  // Boolean flag to determine if need to return height map value
  private boolean _returnActualHeightMapValue;

  /**
   * @author Cheah Lee Herng
   */
  public OpticalPointToPointScan()
  {
      _id = 0;
      _pointPositionName = null;
      _regionPositionName = null;
      _pointToPointStagePositionInNanoMeters = new StagePositionMutable();
      _cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
      _pointPanelPositionNameToZHeightInNanometersMap = new LinkedHashMap<String,DoubleRef>();
      _pointPanelPositionNameToZHeightValidMap = new LinkedHashMap<String,BooleanRef>();
      _pointPanelPositionNameToPointPositionInPixelMap = new LinkedHashMap<String,Pair<Integer,Integer>>();	    
      _pointPanelPositionNameToNewPointPositionInPixelMap = new LinkedHashMap<String,Pair<Integer,Integer>>();        
      _alignmentGroupName = null;
      _opticalCalibrationProfileEnum = null;

      // Default ROI width and height to at least 1x1 pixel resolution
      _roiWidth = 1;
      _roiHeight = 1;

      _returnActualHeightMapValue = false;
      _opticalCameraRectangleName = null;

      _averageZHeightInNanometer = -1;
      _validAverageZHeight = false;
  }

  /**
   * @author Cheah Lee Herng
  */
  public int getId()
  {
      Assert.expect(_id >= 0);

      return _id;
  }

  /**
   * @author Cheah Lee Herng
  */
  public void setId(int id)
  {
      Assert.expect(id >= 0);
      _id = id;        
  }

  /**
   * Returns the StagePosition representing the start point of the scan pass.
   *
   * @author Cheah Lee Herng
  */
  public StagePosition getPointToPointStagePositionInNanoMeters()
  {
      Assert.expect(_pointToPointStagePositionInNanoMeters != null);

      return _pointToPointStagePositionInNanoMeters;
  }

  /**
   * @author Cheah Lee Herng
  */
  public void setPointToPointStagePositionInNanoMeters(StagePosition pointToPointStagePositionInNanometers)
  {
      Assert.expect(pointToPointStagePositionInNanometers != null);

      _pointToPointStagePositionInNanoMeters = pointToPointStagePositionInNanometers;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCameraIdEnum(OpticalCameraIdEnum cameraId)
  {
      Assert.expect(cameraId != null);
      _cameraId = cameraId;
  }

  /**
   * @author Cheah Lee Herng
   */
  public OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
      Assert.expect(_cameraId != null);
      return _cameraId;
  }

  /**
   * Provides for 'natural' ordering based on pont to point scan id number.
   *
   * @author Cheah Lee Herng
  */
  public int compareTo(Object rhs) 
  {
      Assert.expect(rhs instanceof OpticalPointToPointScan, "OpticalPointToPointScan should only be compared to other OpticalPointToPointScan!");

      OpticalPointToPointScan rhsPointToPointScan = (OpticalPointToPointScan)rhs;

      if (_id < rhsPointToPointScan._id)
      {
        return -1;
      }
      else if (_id > rhsPointToPointScan._id)
      {
        return 1;
      }
      else
      {
        return 0;
      }
  }

  /**
   * @author Cheah Lee Herng     
   */
  public String getPointPositionName()
  {
      Assert.expect(_pointPositionName != null);
      return _pointPositionName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setPointPositionName(String pointPositionName)
  {
      Assert.expect(pointPositionName != null);
      _pointPositionName = pointPositionName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getRegionPositionName()
  {
      Assert.expect(_regionPositionName != null);
      return _regionPositionName;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setRegionPositionName(String regionPositionName)
  {
      Assert.expect(regionPositionName != null);
      _regionPositionName = regionPositionName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getAlignmentGroupName()
  {
      Assert.expect(_alignmentGroupName != null);
      return _alignmentGroupName;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setAlignmentGroupName(String alignmentGroupName)
  {
      Assert.expect(alignmentGroupName != null);
      _alignmentGroupName = alignmentGroupName;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean isAlignmentOpticalPointToPointScan()
  {
      if (_alignmentGroupName == null)
          return false;
      else
          return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String,DoubleRef> getPointPanelPositionNameToZHeightInNanometers()
  {
      Assert.expect(_pointPanelPositionNameToZHeightInNanometersMap != null);
      return _pointPanelPositionNameToZHeightInNanometersMap;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public Map<String,BooleanRef> getPointPanelPositionNameToZHeightValidMap()
  {
    Assert.expect(_pointPanelPositionNameToZHeightValidMap != null);
    return _pointPanelPositionNameToZHeightValidMap; 
  }

  /**
   * @author Cheah Lee Herng 
   */
  public double getZHeightInNanometers(String pointPanelPositionName)
  {
      Assert.expect(pointPanelPositionName != null);
      Assert.expect(_pointPanelPositionNameToZHeightInNanometersMap != null);

      double zHeightInNanometers = 0.0;
      if (_pointPanelPositionNameToZHeightInNanometersMap.containsKey(pointPanelPositionName))
      {
          DoubleRef zHeightInNanometersDoubleRef = _pointPanelPositionNameToZHeightInNanometersMap.get(pointPanelPositionName);
          zHeightInNanometers = zHeightInNanometersDoubleRef.getValue();
      }

      return zHeightInNanometers;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean getZHeightValid(String pointPanelPositionName)
  {
    Assert.expect(pointPanelPositionName != null);

    boolean zHeightValid = false;
    if (_pointPanelPositionNameToZHeightValidMap.containsKey(pointPanelPositionName))
    {
      zHeightValid = _pointPanelPositionNameToZHeightValidMap.get(pointPanelPositionName).getValue();
    }
    return zHeightValid;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addPointPanelPositionNameToPointPositionInPixel(Map<String,Pair<Integer,Integer>> pointPanelPositionNameToPointPositionInPixelMap)
  {
    Assert.expect(pointPanelPositionNameToPointPositionInPixelMap != null);
    Assert.expect(_pointPanelPositionNameToPointPositionInPixelMap != null);
    Assert.expect(_pointPanelPositionNameToZHeightInNanometersMap != null);
    Assert.expect(_pointPanelPositionNameToZHeightValidMap != null);

    _pointPanelPositionNameToPointPositionInPixelMap = pointPanelPositionNameToPointPositionInPixelMap;
    _pointPanelPositionNameToZHeightInNanometersMap.clear();
    _pointPanelPositionNameToZHeightValidMap.clear();

    for(Map.Entry<String,Pair<Integer,Integer>> entry : _pointPanelPositionNameToPointPositionInPixelMap.entrySet())
    {
      String pointPanelPositionName = entry.getKey();
      _pointPanelPositionNameToZHeightInNanometersMap.put(pointPanelPositionName, new DoubleRef(0));
      _pointPanelPositionNameToZHeightValidMap.put(pointPanelPositionName, new BooleanRef(false));
    }
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void addPointPanelPositionName(com.axi.v810.business.testProgram.TestSubProgram testSubProgram,
                                        List<PanelCoordinate> pointPanelCoordinateList,
                                        java.awt.geom.AffineTransform aggregateRunTimeAlignmentAffineTransform,
                                        int opticalFiducialSystemCoordinateLocationXInNanometers,
                                        int opticalFiducialSystemCoordinateLocationYInNanometers,
                                        int opticalFiducialStageCoordinateLocationXInNanometers,
                                        int opticalFiducialStageCoordinateLocationYInNanometers)
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(pointPanelCoordinateList != null);
    Assert.expect(aggregateRunTimeAlignmentAffineTransform != null);
    Assert.expect(_pointPanelPositionNameToPointPositionInPixelMap != null);
    Assert.expect(_pointPanelPositionNameToZHeightInNanometersMap != null);
    Assert.expect(_pointPanelPositionNameToZHeightValidMap != null);

    for (PanelCoordinate panelCoordinate : pointPanelCoordinateList)
    {
      String pointPanelCoordinateName = String.valueOf(panelCoordinate.getX()) + "_" + String.valueOf(panelCoordinate.getY());
      java.awt.Point pointInPixel = OpticalMechanicalConversions.convertPanelCoordinateInNanometerToAdjustedPixel(testSubProgram,
                                                                                                                  _opticalCameraRectangleName, 
                                                                                                                  panelCoordinate,
                                                                                                                  aggregateRunTimeAlignmentAffineTransform,
                                                                                                                  opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                                                                  opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                                                                  opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                                                  opticalFiducialStageCoordinateLocationYInNanometers);
      _pointPanelPositionNameToPointPositionInPixelMap.put(pointPanelCoordinateName, new Pair(pointInPixel.x, pointInPixel.y));
    }
    _pointPanelPositionNameToZHeightInNanometersMap.clear();
    _pointPanelPositionNameToZHeightValidMap.clear();

    for(Map.Entry<String,Pair<Integer,Integer>> entry : _pointPanelPositionNameToPointPositionInPixelMap.entrySet())
    {
      String pointPanelPositionName = entry.getKey();
      _pointPanelPositionNameToZHeightInNanometersMap.put(pointPanelPositionName, new DoubleRef(0));
      _pointPanelPositionNameToZHeightValidMap.put(pointPanelPositionName, new BooleanRef(false));
    }
  }

  /**
   * @author Jack Hwee
   */
  public Pair<Integer,Integer> getPointPositionInPixel(String panelPositionName)
  {
      Assert.expect(_pointPanelPositionNameToPointPositionInPixelMap != null);
      Assert.expect(panelPositionName != null);

      return _pointPanelPositionNameToPointPositionInPixelMap.get(panelPositionName);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addPointPanelPositionNameToNewPointPositionInPixel(String panelPositionName, int newCoordinateXInPixel, int newCoordinateYInPixel)
  {
      Assert.expect(_pointPanelPositionNameToNewPointPositionInPixelMap != null);

      if (_pointPanelPositionNameToNewPointPositionInPixelMap.containsKey(panelPositionName))
      {
         Pair<Integer,Integer> newPointPositionInPixel = _pointPanelPositionNameToNewPointPositionInPixelMap.get(panelPositionName);
         newPointPositionInPixel.setFirst(newCoordinateXInPixel);
         newPointPositionInPixel.setSecond(newCoordinateYInPixel);
      }
      else
      {
          _pointPanelPositionNameToNewPointPositionInPixelMap.put(panelPositionName, new Pair<Integer,Integer>(newCoordinateXInPixel, newCoordinateYInPixel));
      }
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String,Pair<Integer,Integer>> getPointPanelPositionNameToPointPositionInPixel()
  {
      Assert.expect(_pointPanelPositionNameToPointPositionInPixelMap != null);
      return _pointPanelPositionNameToPointPositionInPixelMap;
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String,Pair<Integer,Integer>> getPointPanelPositionNameToNewPointPositionInPixel()
  {
      Assert.expect(_pointPanelPositionNameToNewPointPositionInPixelMap != null);
      return _pointPanelPositionNameToNewPointPositionInPixelMap;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setPointPanelPositionZHeight(String panelPositionName, double zHeightInNanometer)
  {
    Assert.expect(panelPositionName != null);

    if (_pointPanelPositionNameToZHeightInNanometersMap.containsKey(panelPositionName))
      _pointPanelPositionNameToZHeightInNanometersMap.get(panelPositionName).setValue(zHeightInNanometer);
    else
      _pointPanelPositionNameToZHeightInNanometersMap.put(panelPositionName, new DoubleRef(zHeightInNanometer));
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setPointPanelPositionZHeightValid(String panelPositionName, boolean zHeightValid)
  {
    Assert.expect(panelPositionName != null);

    if (_pointPanelPositionNameToZHeightValidMap.containsKey(panelPositionName))
      _pointPanelPositionNameToZHeightValidMap.get(panelPositionName).setValue(zHeightValid);
    else
      _pointPanelPositionNameToZHeightValidMap.put(panelPositionName, new BooleanRef(zHeightValid));
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
      if (_pointPanelPositionNameToZHeightInNanometersMap != null)
          _pointPanelPositionNameToZHeightInNanometersMap.clear();
      if (_pointPanelPositionNameToZHeightValidMap != null)
          _pointPanelPositionNameToZHeightValidMap.clear();
      if (_pointPanelPositionNameToPointPositionInPixelMap != null)
          _pointPanelPositionNameToPointPositionInPixelMap.clear();
      if (_pointPanelPositionNameToNewPointPositionInPixelMap != null)
          _pointPanelPositionNameToNewPointPositionInPixelMap.clear();
  }

  /**
   * @author Cheah Lee Herng
   */
  public OpticalCalibrationProfileEnum getOpticalCalibrationProfileEnum()
  {
      Assert.expect(_opticalCalibrationProfileEnum != null);
      return _opticalCalibrationProfileEnum;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum opticalCalibrationProfileEnum)
  {
      Assert.expect(opticalCalibrationProfileEnum != null);
      _opticalCalibrationProfileEnum = opticalCalibrationProfileEnum;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setRoiWidth(int roiWidth)
  {
    _roiWidth = roiWidth;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getRoiWidth()
  {
    return _roiWidth;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setRoiHeight(int roiHeight)
  {
    _roiHeight = roiHeight;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getRoiHeight()
  {
    return _roiHeight;
  }

  /**
   * @author Cheah Lee Herng 
  */
  public void setReturnActualHeightMap(boolean returnActualHeightMapValue)
  {
    _returnActualHeightMapValue = returnActualHeightMapValue;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean getReturnActualHeightMap()
  {
    return _returnActualHeightMapValue;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setOpticalCameraRectangleName(String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);

    _opticalCameraRectangleName = opticalCameraRectangleName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getOpticalCameraRectangleName()
  {
    return _opticalCameraRectangleName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void calculateAverageZHeightInNanometer()
  {
    if (_averageZHeightInNanometer == -1)
    {
      int counter = 0;
      for(Map.Entry<String,BooleanRef> entry : _pointPanelPositionNameToZHeightValidMap.entrySet())
      {
        String pointPanelPositionName = entry.getKey();
        boolean zHeightValid = entry.getValue().getValue();

        if (zHeightValid)
        {
          if (_pointPanelPositionNameToZHeightInNanometersMap.containsKey(pointPanelPositionName))
          {
            double zHeightInNanometer = _pointPanelPositionNameToZHeightInNanometersMap.get(pointPanelPositionName).getValue();
            _averageZHeightInNanometer += zHeightInNanometer;

            counter++;
          }
        }
      }

      if (counter > 0)
      {
        _averageZHeightInNanometer = _averageZHeightInNanometer / counter;
        _validAverageZHeight = true;
      }
      else
        _validAverageZHeight = false;

      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        if (_validAverageZHeight)
          System.out.println("OpticalPointToPointScan: Camera " + getOpticalCameraIdEnum().getId() + ": Average Z-height (nm) = " + _averageZHeightInNanometer);
        else
          System.out.println("OpticalPointToPointScan: Camera " + getOpticalCameraIdEnum().getId() + ": Invalid Z-height");
      }
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  public double getAverageZHeightInNanometer()
  {
    return _averageZHeightInNanometer;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean isValidAverageZHeight()
  {
    return _validAverageZHeight;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void reset()
  {
    _averageZHeightInNanometer = -1;
    _validAverageZHeight = false;
  }
}
