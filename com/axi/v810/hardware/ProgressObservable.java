package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * Accepts "percent done" reports from multiple hardware objects (or activities),
 *  projects the estimated time of completion for each of those hardware objects,
 *  and reports to observers the percent done for the slowest hardware object.<p/>
 *
 * The "observable state" of the ProgressObservable is the identity and reported progress
 *  of the activity that is projected to finish last.  If this "state" does not change as
 *  the result of a progress report from some other activity, then no update calls are made
 *  (per the general observer/observable contract).<p/>
 *
 * The ProgressObservable is a ThreadSafeObservable, so it notifies on its own thread.
 *  The run method for the notification thread is from ThreadSafeObservable.  The estimation
 *  thread was added because it was impossible to modify the notification thread's run method
 *  to also do the estimation thread's work without rewriting it and making it very complicated:
 *  the notification thread waits on locks, but the estimation thread wakes up once a second
 *  and polls to see if it has work to do.  Since the existing notification run method is so
 *  widely used, it was deemed too reliable NOT to use.<p/>
 *
 * The calls from the observable and workers can individually come from any thread.</p>
 *
 * Workers are not required to count all the way to 100% done.  The operation may be aborted,
 *  for example.  So, turn off the observable when the hardware operation completes.<p/>
 *
 * An attempt was made to keep the external calls on the ProgressObservable and subsequent
 *  activities on internal threads grouped together in the following sequence diagram:<ul>
 *  <li>reportProgress calls cause the notification thread to act, and </li>
 *  <li>reportAtomicTaskStarted calls cause both the estimation and notification threads to act.</li>
 *  </ul><p/>
 *
 * <pre>
 *                        :Progress       :Progress       :Progress      w-1:     w-2:     w-3:
 * :Observer              Observable      Observable      Observable    Worker   Worker   Worker
 *     |                   (caller       (notification   (estimation      |        |        |
 *     |                    thread)        thread)         thread)        |        |        |
 *     |                      |               |               |           |        |        |
 *     |    addObserver(this) |               |               |           |        |        |
 *     |--------------------->|               |               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-1,3%)        |           |        |        |
 *     |                      |<--------------+---------------+-----------|        |        |
 *     | notify(w-1,3%)       |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-2,7%)        |           |        |        |
 *     |                      |<--------------+---------------+-----------+--------|        |
 *     |                      |               |               |           |        |        |
 *     |  still w-1,3%, so no notify(...) ----|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     |                      |               |               |           |        |        |
 *     |                      | reportAtomicTaskStarted(w-3,2.5s)         |        |        |
 *     |                      |<--------------+---------------+-----------+--------+--------|
 *     | notify(w-3,0%)       |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-3,40%)       |           |        |        |
 *     |                      |<--------------+---------------|           |        |        |
 *     | notify(w-1,3%)       |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-3,80%)       |           |        |        |
 *     |                      |<--------------+---------------|           |        |        |
 *     |                      |               |               |           |        |        |
 *     |  still w-1,3%, so no notify(...) ----|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-1,100%)      |           |        |        |
 *     |                      |<--------------+---------------+-----------|        |        |
 *     | notify(w-2,7%)       |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     |                      |               |               |           |        |        |
 *     |                      | reportProgress(w-2,100%)      |           |        |        |
 *     |                      |<--------------+---------------+-----------+--------|        |
 *     | notify(w-3,80%)      |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     |                      |               |               |           |        |        |
 *     |                      | reportAtomicTaskComplete(w-3) |           |        |        |
 *     |                      |<--------------+---------------+-----------+--------+--------|
 *     | notify(w-3,100%)     |               |               |           |        |        |
 *     |<---------------------+---------------|               |           |        |        |
 *     |                      |               |               |           |        |        |
 *     :                      :               :               :           :        :        :
 *     .                      .               .               .           .        .        .
 * </pre>
 *
 * @todo design and code reviews
 *
 * @see ProgressReporterEnum
 *
 * @author Greg Loring
 */
public class ProgressObservable extends Observable
{
  private static ProgressObservable _instance;

  private Map<ProgressReporterEnum, ProgressObservableReporterProgressData> _progressDataMap;
  private Map<ProgressReporterEnum, Integer> _atomicTaskRateMap; // reporter -> percentDonePerSecond
  private ProgressReporterEnum _slowestReporter;
  private Thread _atomicTaskEstimatorThread;

  /**
   * @author Greg Loring
   */
  public static synchronized ProgressObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new ProgressObservable();
    }
    return _instance;
  }

  /**
   * @author Greg Loring
   */
  private ProgressObservable()
  {
    _progressDataMap = new LinkedHashMap<ProgressReporterEnum, ProgressObservableReporterProgressData>();
    _atomicTaskRateMap = new LinkedHashMap<ProgressReporterEnum, Integer>();
    _slowestReporter = null;

    /** @todo may be an "official" place to put this */
    _atomicTaskEstimatorThread = new Thread("ProgressObservable estimation thread")
    {
      public void run()
      {
        runAtomicTaskEstimatorThread();
      }
    };
    _atomicTaskEstimatorThread.setDaemon(true);
    _atomicTaskEstimatorThread.start();
  }

  /**
   * get the identity of the progress reporter who is projected to finish last
   *   (the slowest reporter)<p/>
   *
   * @return ProgressReporterEnum of slowest reporter, or null if no progress
   *   reports have been received
   *
   * @author Greg Loring
   */
  public ProgressReporterEnum getReporter()
  {
    return _slowestReporter;
  }

  /**
   * get the relative progress of the reporter who is projected to finish last
   *   (the slowest reporter)<p/>
   *
   * @return int percent done of slowest reporter, or 0 if no progress reports
   *   have been received
   *
   * @author Greg Loring
   */
  public int getPercentDone()
  {
    // even though this is not an atomic operation (the identity of the slowest
    //  reporter might change between the non-null test and map lookup), it will
    //  not throw, and it will return the same answer that it would have if the
    //  method had been synchronized.
    int result = 0;
    if (_slowestReporter != null)
    {
      result = _progressDataMap.get(_slowestReporter).getPercentDone();
    }
    return result;
  }

  /**
   * The preferred method of reporting progress for a slow task: Use this method whenever you
   *  can break up a task into more than start/done AND the task has a good chance of taking
   *  more than a few seconds.
   *
   * @author Greg Loring
   */
  public synchronized void reportProgress(ProgressReporterEnum reporter, int percentDone)
  {
    Assert.expect(reporter != null);
    Assert.expect((percentDone >= 0) && (percentDone <= 100));
    Assert.expect(_atomicTaskRateMap.containsKey(reporter) == false);

    privateReportProgress(reporter, percentDone);
  }

  /**
   * The "method of last resort" for reporting progress on a slow task: Use this method only
   *  when a task is essentially made up of a single blocking call that typically lasts more
   *  than a few seconds.  E.g., homing the rails of the panel handling subsystem.
   *
   * @author Greg Loring
   */
  public void reportAtomicTaskStarted(ProgressReporterEnum reporter, int estimatedTaskDurationMillis)
  {
    Assert.expect(reporter != null);
    Assert.expect(estimatedTaskDurationMillis > 0);

    privateReportProgress(reporter, 0);

    // set up a rate for the atomic task estimation thread (if it makes sense to do so)
    int estimatedTaskDurationSeconds = estimatedTaskDurationMillis / 1000;
    if (estimatedTaskDurationSeconds > 0) // at least one second in length
    {
      int percentPerSecond = 100 / estimatedTaskDurationSeconds;
      if (percentPerSecond < 100) // the atom task estimation thread is allowed to report 100
      {
        if (percentPerSecond == 0)
        {
          percentPerSecond = 1; // stuck with ints for now and want progress to move
        }
        synchronized (this) // avoid fast fails
        {
          _atomicTaskRateMap.put(reporter, percentPerSecond); // don't care about re-puts
        }
      }
    }
  }

  /**
   * @see #reportAtomicTaskStarted(ProgressReporterEnum, int)
   *
   * @author Greg Loring
   */
  public void reportAtomicTaskComplete(ProgressReporterEnum reporter)
  {
    Assert.expect(reporter != null);

    // stop automated progress estimates
    synchronized (this) // avoid fast fails
    {
      _atomicTaskRateMap.remove(reporter); // don't care about un-removes
    }

    // report done
    privateReportProgress(reporter, 100);
  }

  /**
   * private version of reportProgress for use by "atomic task" methods (different asserts)
   *
   * @author Greg Loring
   */
  private synchronized void privateReportProgress(ProgressReporterEnum theReporter, int thePercentDone)
  {
    // my state just before this report
    ProgressReporterEnum previousReporter = getReporter();
    int previousPercentDone = getPercentDone();

    // record this progress report
    ProgressObservableReporterProgressData progressData = _progressDataMap.get(theReporter);
    if (progressData == null)
    {
      progressData = new ProgressObservableReporterProgressData();
      _progressDataMap.put(theReporter, progressData);
    }
    progressData.reportProgress(thePercentDone);

    // select the slowest reporter
    _slowestReporter = theReporter;
    long slowestEstimatedTimeOfCompletion = progressData.getEstimatedTimeOfCompletion();
    int slowestPercentDone = progressData.getPercentDone();
    for (ProgressReporterEnum reporter : _progressDataMap.keySet())
    {
      progressData = _progressDataMap.get(reporter);
      //XCR-3797, Failed to cancel CDNA task after start up
      if (progressData.getEstimatedTimeOfCompletion() > slowestEstimatedTimeOfCompletion && progressData.getEstimatedTimeOfCompletion() != Long.MAX_VALUE)
      {
        _slowestReporter = reporter;
        slowestEstimatedTimeOfCompletion = progressData.getEstimatedTimeOfCompletion();
        slowestPercentDone = progressData.getPercentDone();
      }
      else if (progressData.getEstimatedTimeOfCompletion() == slowestEstimatedTimeOfCompletion)
      {
        if (progressData.getPercentDone() < slowestPercentDone)
        {
          _slowestReporter = reporter;
          slowestEstimatedTimeOfCompletion = progressData.getEstimatedTimeOfCompletion();
          slowestPercentDone = progressData.getPercentDone();
        }
      }
    }

    // clear out activities/reporters that are done (and are not the slowest reporter)
    Iterator<Map.Entry<ProgressReporterEnum, ProgressObservableReporterProgressData>> iter;
    for (iter = _progressDataMap.entrySet().iterator(); iter.hasNext(); )
    {
      Map.Entry<ProgressReporterEnum, ProgressObservableReporterProgressData> entry = iter.next();
      ProgressReporterEnum reporter = entry.getKey();
      if (reporter != _slowestReporter)
      {
        progressData = entry.getValue();
        if (progressData.getPercentDone() == 100)
        {
          iter.remove();
        }
      }
    }

    // notify observers if the observable's observable state has changed
    int percentDone = getPercentDone();
    if (  (_slowestReporter != previousReporter)
       || (percentDone != previousPercentDone))
    {
      super.setChanged();
      ProgressReport report = new ProgressReport(_slowestReporter, percentDone);
      super.notifyObservers(report);
    }
    else //XCR1466 Siew Yeng - Quick fix for X-ray warm up report display
    {
      if(theReporter == ProgressReporterEnum.XRAY_SOURCE_WARM_UP_SHORT || 
         theReporter == ProgressReporterEnum.XRAY_SOURCE_WARM_UP_MEDIUM||
         theReporter == ProgressReporterEnum.XRAY_SOURCE_WARM_UP_LONG)
      {
        super.setChanged();
        ProgressReport report = new ProgressReport(theReporter, thePercentDone);
      
        super.notifyObservers(report);
      }
    }
  }


  /**
   * for those clients who cannot bust up their tasks into pieces, report estimated progress
   *  on their behalf on one second intervals
   *
   * @author Greg Loring
   */
  private void runAtomicTaskEstimatorThread()
  {
    while (true)
    {
      // sleep for about a second
      try
      {
        Thread.sleep(1000);
      }
      catch (InterruptedException ix)
      {
        // don't expect this, but don't really care how sleep returns either
      }

      synchronized (this)
      {
        // for each reporter of an atomic task that has not yet completed...
        Iterator<Map.Entry<ProgressReporterEnum, Integer>> iter; // need for possible remove
        for (iter = _atomicTaskRateMap.entrySet().iterator(); iter.hasNext(); )
        {
          Map.Entry<ProgressReporterEnum, Integer> entry = iter.next();
          ProgressReporterEnum reporter = entry.getKey();
          int percentDonePerSecond = entry.getValue();
          Assert.expect(percentDonePerSecond > 0);

          ProgressObservableReporterProgressData progressData = _progressDataMap.get(reporter);
          Assert.expect(progressData != null);

          // estimate a percent done to give to the "preferred" reportProgress method
          int newPercentDone = progressData.getPercentDone() + percentDonePerSecond;
          if (newPercentDone < 100) // don't want this thread to report 100 (and especially not > 100)
          {
            privateReportProgress(reporter, newPercentDone);
          }
          else
          {
            iter.remove(); // estimated duration of atomic task has passed or has almost passed
          }
        }
      }
    }
  }

}
