package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left outer barrier
 * @author Rex Shang
 */
class Test_LeftOuterBarrier extends UnitTest
{
  private LeftOuterBarrier _leftOuterBarrier = null;

  private Test_LeftOuterBarrier()
  {
    _leftOuterBarrier = LeftOuterBarrier.getInstance();
  }

  public static void main(String[] args)
  {
    Test_LeftOuterBarrier _test_LeftOuterBarrier = new Test_LeftOuterBarrier();
    UnitTest.execute(_test_LeftOuterBarrier);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Close the barrier and verify
      _leftOuterBarrier.close();
      Expect.expect(_leftOuterBarrier.isOpen() == false);

      // Open the barrier and verify.
      _leftOuterBarrier.open();
      Expect.expect(_leftOuterBarrier.isOpen() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
