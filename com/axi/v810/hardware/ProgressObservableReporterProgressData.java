package com.axi.v810.hardware;

import com.axi.util.Assert;

/**
 * This is an implementation class of ProgressObserver.  If you are not maintaining
 *  the ProgressObserver class, then stop reading now!<p/>
 *
 * This class's responsibility is to use progress report history to estimate the time
 *  when a reporter's activity will be done.  It attempts to reconcile two conflicting
 *  desires regarding completion time estimates:<ul>
 *   <li>react to changes in the rate of progress</li>
 *   <li>dampen any bursty-ness in the rate of progress</li>
 * </ul>
 *
 * @author Greg Loring
 */
/*package*/ class ProgressObservableReporterProgressData
{
  private Integer _firstPercentDone;
  private Long    _firstTimeInMillis;
  private Integer _lastPercentDone;
  private Long    _lastTimeInMillis;
  private long    _estimatedTimeOfCompletion;

  /**
   * @author Greg Loring
   */
  /*package*/ ProgressObservableReporterProgressData()
  {
    _firstPercentDone = null;
    _firstTimeInMillis = null;
    _lastPercentDone = null;
    _lastTimeInMillis = null;
    _estimatedTimeOfCompletion = Long.MAX_VALUE;
  }

  /**
   * accept a progress report and update completion time estimate<p/>
   *  NOTE: MUST allow at least one System.currentTimeMillis() tick between calls
   *
   * @author Greg Loring
   */
  /*package*/ void reportProgress(int percentDone)
  {
    Assert.expect((percentDone >= 0) && (percentDone <= 100));
    long now = System.currentTimeMillis();

    // is this the first report?
    if (_firstPercentDone == null)
    {
      Assert.expect(_firstTimeInMillis == null);
      Assert.expect(_lastPercentDone == null);
      Assert.expect(_lastTimeInMillis == null);
      _firstPercentDone = Integer.valueOf(percentDone);
      _firstTimeInMillis = Long.valueOf(now);
      Assert.expect(_estimatedTimeOfCompletion == Long.MAX_VALUE);
    }

    // is the reporter's progress negative?  (i.e., something must have
    //  happenned which caused it to rethink it's progress estimates)
    else if (percentDone < _lastPercentDone.intValue()) // regress
    {
      // when a reporter regresses, act like this is the very first report
      _firstPercentDone = Integer.valueOf(percentDone);
      _firstTimeInMillis = Long.valueOf(now);
      _estimatedTimeOfCompletion = Long.MAX_VALUE;
    }

    // all done?
    else if (percentDone == 100)
    {
       if (_lastPercentDone.intValue() < 100)
       {
         _estimatedTimeOfCompletion = now;
       }
    }

    // need at least two reports separated in time to make a completion time estimate
    else
    {
      Assert.expect(_firstPercentDone != null);
      Assert.expect(_firstTimeInMillis != null);
      Assert.expect(_lastPercentDone != null);
      Assert.expect(_lastTimeInMillis != null);
      Assert.expect(_firstPercentDone.intValue() <= _lastPercentDone.intValue());
      Assert.expect(_lastPercentDone.intValue() <= percentDone);
      // Reid removed this assert. If the system time was set back (for example by adjusting
      // the clock) the software would crash if the progress reporter was updating. Eventually
      //  a better fix may be required, but for not I am just removing the assert
      //Assert.expect(_firstTimeInMillis.longValue() <= _lastTimeInMillis.longValue());
      if (_lastTimeInMillis.longValue() < now)
      {
        // the "dwell" rate is between the current report and the first report
        //  (i.e., the entire reporting time interval)
        double deltaPct = percentDone - _firstPercentDone.intValue(); // pct
        double deltaT = now - _firstTimeInMillis.longValue(); // ms
        double dwellRate = deltaPct / deltaT; // pct / ms

        // the "recent" rate is between the current report and the last report
        //  (i.e., the most recent time interval)
        double recentRate;
        if (_lastTimeInMillis.longValue() > _firstTimeInMillis.longValue())
        {
          // "recent" rate will be different from the "dwell" rate
          deltaPct = percentDone - _lastPercentDone.intValue(); // pct
          deltaT = now - _lastTimeInMillis.longValue(); // ms
          recentRate = deltaPct / deltaT; // pct / ms
        }
        else
        {
          // when there are only two reports, the "dwell" and "recent" values
          //  and intervals are the same, so the rates are the same
          recentRate = dwellRate; // pct / ms
        }

        // use the average of the "dwell" and "recent" rates to estimate completion time
        //  (balances rate change responsiveness with burst immunity)
        double rate = (dwellRate + recentRate) / 2.0; // pct / ms
        double pctToGo = 100 - percentDone; // pct
        long millisRemaining = Math.round(pctToGo / rate); // ms
        _estimatedTimeOfCompletion = now + millisRemaining;
      }
    }

    _lastPercentDone = Integer.valueOf(percentDone);
    _lastTimeInMillis = Long.valueOf(now);
  }

  /**
   * The unit of measure is the same as System.currentTimeMillis(), but what is
   *  really important is that the reported values are greater for later times.
   *  In other words, the reporter whose activity is projected to complete last
   *  will have the greatest return value from this method.
   *
   * @author Greg Loring
   */
  /*package*/ long getEstimatedTimeOfCompletion()
  {
    return _estimatedTimeOfCompletion;
  }

  /**
   * Use this method to break ties from equal getEstimatedTimeOfCompletion() return
   *  values.  Lesser percentDone() values imply later estimated completion times.
   *
   * @author Greg Loring
   */
  /*package*/ int getPercentDone()
  {
    int result = 0;
    if (_lastPercentDone != null)
    {
      result = _lastPercentDone.intValue();
    }
    return result;
  }

}
