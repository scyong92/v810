package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * The stage light is the light that allows the customer to view the inside
 * of the system.
 * @author Rex Shang
 */
public class StageViewingLight extends HardwareObject implements LightInt
{
  private static StageViewingLight _instance;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;

  /**
   * @author Rex Shang
   */
  private StageViewingLight()
  {
    _instance = null;
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized StageViewingLight getInstance()
  {
    if (_instance == null)
      _instance = new StageViewingLight();

    return _instance;
  }

  /**
   * Turn on the light.
   * @author Rex Shang
   */
  public void on() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageViewingLightEventEnum.LIGHT_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnStageViewingLightOn();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageViewingLightEventEnum.LIGHT_ON);
  }

  /**
   * Turn off the light.
   * @author Rex Shang
   */
  public void off() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, StageViewingLightEventEnum.LIGHT_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnStageViewingLightOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, StageViewingLightEventEnum.LIGHT_OFF);
  }

  /**
   * @return true if the stage lights are on.
   * @author Rex Shang
   */
  public boolean isOn() throws XrayTesterException
  {
    return _digitalIo.isStageViewingLightOn();
  }
}
