package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 * @author Rex Shang
 */
public class ImageReconstructionProcessorManager
    extends HardwareObject
    implements HardwareStartupShutdownInt
{
  private static ImageReconstructionProcessorManager _instance;

  private boolean _isInitialized = false;
  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;
  private ProgressObservable _progressObservable;

  private ExecuteParallelThreadTasks<Object> _concurrentTasks;
  private List<ImageReconstructionProcessor> _imageReconstructionProcessors;

  // Expect the initialization will take up to 6 minutes.
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 360000;

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private ImageReconstructionProcessorManager()
  {
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _progressObservable = ProgressObservable.getInstance();

    _concurrentTasks = new ExecuteParallelThreadTasks<Object>();

    // Get ImageReconstructionProcessor instances.
    _imageReconstructionProcessors = new ArrayList<ImageReconstructionProcessor>();
    int numberOfIreToUse = getNumberOfImageReconstructionEngines();
    List<ImageReconstructionEngineEnum> ireIdList = ImageReconstructionEngineEnum.getEnumList();

    // Check validity of number of irp's.
    Assert.expect(numberOfIreToUse > 0, "Number of ire needs to be greater than 0.");
    Assert.expect(numberOfIreToUse <= ireIdList.size(), "Number of ire needs to no more than " + ireIdList.size() + ".");

    int ireCount = 0;
    for (ImageReconstructionEngineEnum ireId : ireIdList)
    {
      ImageReconstructionProcessor imageReconstructionProcessor = ImageReconstructionProcessor.getInstance(ireId);
      if (_imageReconstructionProcessors.contains(imageReconstructionProcessor) == false)
        _imageReconstructionProcessors.add(imageReconstructionProcessor);
      ireCount++;

      if (ireCount >= numberOfIreToUse)
        break;
    }
  }

  /**
   * @author George A. David
   */
  public static synchronized ImageReconstructionProcessorManager getInstance()
  {
    if (_instance == null)
      _instance = new ImageReconstructionProcessorManager();

    return _instance;
  }

  /**
   * @author Chnee Khang Wah, 2011-12-02, Low Cost AXI
   */
  public static int getNumberOfImageReconstructionProcessors()
  {
    return getConfig().getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSORS);
  }
  
  /**
   * @author George A. David
   */
  public static int getNumberOfImageReconstructionEngines()
  {
    return getConfig().getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES);
  }

  /**
   * @author George A. David
   */
  private static boolean isImageReconstructionProcessorsStartupDisabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.DISABLE_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP);
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private void startImageReconstructionProcessors() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.STARTING_IMAGE_RECONSTRUCTION_PROCESSORS);
    _hardwareObservable.setEnabled(false);
    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        // Ok, let's start up the irp software
        for (ImageReconstructionProcessor irp : _imageReconstructionProcessors)
        {
          _concurrentTasks.submitThreadTask(new ImageReconstructionProcessorStartThreadTask(irp));
        }

        try
        {
          _concurrentTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {
          _hardwareTaskEngine.throwHardwareException(xte);
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.STARTING_IMAGE_RECONSTRUCTION_PROCESSORS);
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  public void stopImageReconstructionProcessors() throws XrayTesterException
  {
    _isInitialized = false;

    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.STOPPING_IMAGE_RECONSTRUCTION_PROCESSORS);
    _hardwareObservable.setEnabled(false);
    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        // Ok, let's shutdown up the irp software
        for (ImageReconstructionProcessor irp : _imageReconstructionProcessors)
        {
          _concurrentTasks.submitThreadTask(new ImageReconstructionProcessorStopThreadTask(irp));
        }

        try
        {
          _concurrentTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {
          _hardwareTaskEngine.throwHardwareException(xte);
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.STOPPING_IMAGE_RECONSTRUCTION_PROCESSORS);
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    initialize();
  }

  /**
   * @author Rex Shang
   */
  public void initialize() throws XrayTesterException
  {
    if (isStartupRequired() == false)
      return;

    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSORS);
    _hardwareObservable.setEnabled(false);
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.IMAGE_RECONSTRUCTION_PROCESSOR_INITIALIZE, _STARTUP_TIME_IN_MILLISECONDS);

    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        // Initialize the image reconstruction processor and establish first contact.
        for (ImageReconstructionProcessor irp : _imageReconstructionProcessors)
        {
          _concurrentTasks.submitThreadTask(new ImageReconstructionProcessorStartupThreadTask(irp));
        }

        try
        {
          _concurrentTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {
          _hardwareTaskEngine.throwHardwareException(xte);
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
      }

      _isInitialized = true;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.IMAGE_RECONSTRUCTION_PROCESSOR_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSORS);
  }

  /**
   * @author George A. David
   */
  public void shutdown() throws XrayTesterException
  {
    stopImageReconstructionProcessors();
    _isInitialized = false;
  }

  /**
   * @author George A. David
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    boolean isStartupRequired = false;
    //XCR-3336, Assert when run CDNA after production error
    if (_isInitialized == false || areAllImageReconstructionProcessorsRunning() == false)
      isStartupRequired = true;

    return isStartupRequired;
  }

  /**
   * @author George A. David
   */
  private boolean areAllImageReconstructionProcessorsRunning() throws XrayTesterException
  {
    boolean areAllRunning = false;
    if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
    {
      areAllRunning = true;
      for (ImageReconstructionProcessor irp : _imageReconstructionProcessors)
      {
        try
        {
          areAllRunning &= irp.isImageReconstructionProcessorRunning();
        }
        catch (HardwareException ex)
        {
          areAllRunning = false;
        }

        if (areAllRunning == false)
          break;
      }
    }
    else
      areAllRunning = _isInitialized;

    return areAllRunning;
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  public void rebootImageReconstructionProcessors() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.REBOOTING_IMAGE_RECONSTRUCTION_PROCESSORS);
    _hardwareObservable.setEnabled(false);
    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        // Initialize the image reconstruction processor and establish first contact.
        for (ImageReconstructionProcessor irp : _imageReconstructionProcessors)
        {
          _concurrentTasks.submitThreadTask(new ImageReconstructionProcessorRebootThreadTask(irp));
        }

        try
        {
          _concurrentTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {
          _hardwareTaskEngine.throwHardwareException(xte);
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
      }

      _isInitialized = false;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.REBOOTING_IMAGE_RECONSTRUCTION_PROCESSORS);
  }

  /**
   * @author George A. David
   */
  public void restartImageReconstructionProcessors() throws XrayTesterException
  {
    stopImageReconstructionProcessors();
    // Disable saving projections
    ImageReconstructionProcessor.setSaveImageReconstructionProjections(false);
    startImageReconstructionProcessors();
  }

  /**
   * @author Dave Ferguson
   */
  public void restartImageReconstructionProcessorsWithSaveProjections() throws XrayTesterException
  {
    stopImageReconstructionProcessors();
    // Enable saving projections
    ImageReconstructionProcessor.setSaveImageReconstructionProjections(true);
    startImageReconstructionProcessors();
  }

  /**
   * @author George A. David
   */
  public void setStartupRequired()
  {
    _isInitialized = false;
  }

}
