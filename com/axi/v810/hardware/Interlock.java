package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Communicates with the HW Interlock controller.   This is a read only interface
 * because (for safety reasons) the software may not instruct an interlock to be
 * opened or closed.  The HW Interlock controller keeps this privledge to itself.
 * @author Rex Shang
 */
public class Interlock extends HardwareObject implements HardwareStartupShutdownInt, Observer
{
  private static Interlock _instance;
  private DigitalIo _digitalIo;
  private boolean _initialized;
  private Map<Integer, Pair<String, Boolean>> _diagnosticCodeToDescriptionMap;
  private int _hardwareRevisionId = -1;
  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;
  private boolean _monitorOn = false;

  private boolean _servicePanelActivated = false;

  // Variables used to calculate Infinite Impulse Response (IIR)
  private double _infiniteImpulseResponseSumOfPowerOk;
  private static final double _infiniteImpulseResponsePreviousResponseWeight = 0.75;
  private static final double _infiniteImpulseResponseLimit = 0.6;

  private static Map<Integer, String> _hardwareRevisionList;
  private static WorkerThread _interlockPollingThread;
  // The polling interval needs to be fast enough so we will report interlock
  // event ASAP.  Yet it needs to be slow enough we would waste precious CPU cycle.
  // So we should pick a number between 100 and 1500.
  private static final int _INTERLOCK_POLLING_INTERVAL_IN_MILLI_SECONDS = 888;
  private static final int _DIAGNOSTICS_INTERLOCK_IS_CLOSED = 0;

  // When panel is being loaded/unloaded, the interlock will report open event.
  // Ideally, the hardware should be able to detect that there is no xray leak
  // and no motion going on and not report erroneous event.
  private static boolean _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = false;

  private static boolean _interlockTroubleshootModeOn = false;

  //sheng chuan, used to update light tower realtime
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();  // used for light stack calls
  private boolean _isInterlockBreak = false;
  private int _previousLight;
  private final int _MACHINE_STANDBY_MODE = 1;
  private final int _HARDWARE_INUSED_MODE = 2;
  private final int _SYSTEM_ERROR_MODE = 3;
  private final int _ALL_LIGHT_OFF_MODE = 4;
  
  private RemoteMotionServerExecution _remoteMotionServer;
  /**
   * @author Rex Shang
   */
  static
  {
    _interlockPollingThread = new WorkerThread("Interlock Polling Thread");

    // Hardware revision id.
    _hardwareRevisionList = new HashMap<Integer, String>();
    _hardwareRevisionList.put(new Integer(3),
                              StringLocalizer.keyToString("HW_INTERLOCK_REVISION_3_KEY"));
    _hardwareRevisionList.put(new Integer(5),
                              StringLocalizer.keyToString("HW_INTERLOCK_REVISION_5_KEY"));

    _hardwareRevisionList.put(new Integer(6),
                              StringLocalizer.keyToString("HW_INTERLOCK_REVISION_5_KEY"));
    _hardwareRevisionList.put(new Integer(7),
                              StringLocalizer.keyToString("HW_INTERLOCK_REVISION_5_KEY"));
  }

  /**
   * @author Rex Shang
   */
  private Interlock()
  {
    _initialized = false;
    _infiniteImpulseResponseSumOfPowerOk = 1.0;
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _digitalIo = DigitalIo.getInstance();
    _remoteMotionServer = RemoteMotionServerExecution.getInstance();
    setupDiagnosticCodeDescriptions();
  }

  /**
   * @author Rex Shang
   */
  public static synchronized Interlock getInstance()
  {
    if (_instance == null)
      _instance = new Interlock();

    return _instance;
  }

  /**
   * Tell this hardware to perform any actions required to startup on behalf
   * of the XrayTester object.
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    if (isInstalled() == false)
    {
      _initialized = true;
      _digitalIo.turnFrontPanelLeftDoorLockOff();
      _digitalIo.turnFrontPanelRightDoorLockOff();
      return;
    }

    try
    {
      // XCR-3604 Unit Test Phase 2
      if (isSimulationModeOn() == false)
        checkInterlockStatus();
    }
    catch (InterlockHardwareException ihe)
    {
//      if (_interlockTroubleshootModeOn && ihe.getExceptionType().equals(InterlockHardwareExceptionEnum.INTERLOCK_IS_IN_SERVICE_MODE))
//      {
//        // If the hardware is in service mode while we do start up in service UI,
//        // we should allow initialization to finish.
//      }
//      else
        throw ihe;
    }

    _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = false;
    // Start the thread to check our status constantly.
    if (_monitorOn == false)
    {
      _monitorOn = true;
      _interlockPollingThread.invokeLater(new Runnable()
      {
        public void run()
        {
          pollingInterlockStatus();
        }
      });
    }

    _hardwareObservable.addObserver(this);

    _initialized = true;
  }

  /**
   * Use Infinite Impulse Response (IIR) to tune out the glitch which system is able to handle.
   * The value calculated in this method is then used to determine isInterlockPoweredOn().
   * @author Rex Shang
   */
  private void calculateInfiniteImpulseResponse() throws XrayTesterException
  {
    double newVal = 1.0;

    if (_digitalIo.isInterlockPoweredOn() == false)
      newVal = 0.0;

    _infiniteImpulseResponseSumOfPowerOk = _infiniteImpulseResponsePreviousResponseWeight * _infiniteImpulseResponseSumOfPowerOk + ( 1.0 - _infiniteImpulseResponsePreviousResponseWeight) * newVal;
  }

  /**
   * Private utility to check interlock board status for start up and polling.
   * @author Rex Shang
   */
  private void checkInterlockStatus() throws XrayTesterException
  {
    calculateInfiniteImpulseResponse();

    if (isInterlockPoweredOn())
    {
      if (isServiceModeOn() == false)
      {
        // Now store the hardware revision id.
        int expectedId = getHardwareRevisionIdFromConfiguration();
        int actualId = getHardwareRevisionId();
        if (expectedId != actualId)
        {
          _hardwareTaskEngine.throwHardwareException(
              InterlockHardwareException.getInterlockHardwareRevisionMismatchException(expectedId, actualId));
        }
        
        // When panel is loading/unloading, interlock chain will open as a
        // result.  We need not report chain open.
        if (_BYPASS_CHECKING_FOR_OPEN_INTERLOCK == false && isInterlockOpen())
        {
          _digitalIo.turnFrontPanelLeftDoorLockOff();
          _digitalIo.turnFrontPanelRightDoorLockOff();

          if(_isInterlockBreak == false)
          {
            if (_digitalIo.isInterlockChain1Open())
            {
              _hardwareTaskEngine.throwHardwareException(
                InterlockHardwareException.getInterlockChainIsOpenException());
            }
            else if (_digitalIo.isInterlockChain2Open())
            {
              _hardwareTaskEngine.throwHardwareException(
                InterlockHardwareException.getInterlockChainIsOpenException());
            }
            else
            {
              _hardwareTaskEngine.throwHardwareException(
                InterlockHardwareException.getInterlockChainIsOpenException());
            }
          }
        }
        else
        {
          //  no longer chain break no exception catched, so set to false
          //use key to control the update of light tower so that it will only record the previos light before break
          if (_isInterlockBreak)
          {
            _isInterlockBreak = false;
            //restore the light status to original before chain break detected
            trackingLightStackStatusBasedOnInterlockBreakingStatus(false);
          }
        }
           
        if(_servicePanelActivated == false)
        {
            _digitalIo.turnFrontPanelLeftDoorLockOn();
            _digitalIo.turnFrontPanelRightDoorLockOn();
        }

      }
      else
      {
        _hardwareTaskEngine.throwHardwareException(
            InterlockHardwareException.getInterlockIsInServiceModeException());
      }
    }
    else
    {
      _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockIsNotOnException());
    }
  }

  /**
   * Tell this hardware to perform any actions required to shutdown on behalf
   * of the XrayTester object.
   * @author Rex Shang
   */
  public void shutdown() throws HardwareException
  {
    try
    {
        _digitalIo.turnFrontPanelLeftDoorLockOff();
        _digitalIo.turnFrontPanelRightDoorLockOff();
    }
    catch (XrayTesterException xte)
    {
        _hardwareObservable.notifyObserversOfException(xte);
    }

    if (_hardwareObservable != null)
      _hardwareObservable.deleteObserver(this);
    _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = false;
    _initialized = false;
    _monitorOn = false;
    _infiniteImpulseResponseSumOfPowerOk = 1.0;
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws HardwareException
  {
    if (_initialized == false)
      return true;
    else
      return false;
  }

  /**
   * To be run on a thread to poll for interlock status.  In case of chain
   * open, we will generate event to notify user.
   * @author Rex Shang
   */
  private void pollingInterlockStatus()
  {
    while (_monitorOn)
    {
      try
      {
        Thread.sleep(_INTERLOCK_POLLING_INTERVAL_IN_MILLI_SECONDS);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }

      if (isSimulationModeOn() == false)
      {
        try
        {
          if (_digitalIo.isStartupRequired() || _remoteMotionServer.isStartupRequired())
          {
            _monitorOn = false;
          }
          if (_monitorOn == false)
            return;

          checkInterlockStatus();
          
        }
        catch (XrayTesterException xte)
        {
          //since this thread will be keep on running as long as interlock is initialized
          //therefore it will only notify error when chain break detected for the first time to prevent
          //error message prompt out continueosly
          if(_isInterlockBreak == false)
          {
             //set interlock break tag to true
             _isInterlockBreak = true;
              try 
              {
                recordPreviousLightStatus();
                trackingLightStackStatusBasedOnInterlockBreakingStatus(true);
              } 
              catch (XrayTesterException ex) 
              {
                //do nothing
              }
             _hardwareObservable.notifyObserversOfException(xte);
          }
          
          // When in trouble shoot mode, keep running.
          if (_interlockTroubleshootModeOn == false)
          {
            //allow the thread to continue monitor even interlock break
//            _monitorOn = false;
            _initialized = false;
            _infiniteImpulseResponseSumOfPowerOk = 1.0;
          }
        }
      }
    }
  }

  /**
   * Check to see if the interlock board is powered on.
   * @author Rex Shang
   */
  public boolean isInterlockPoweredOn() throws XrayTesterException
  {
    boolean powerOk = _infiniteImpulseResponseSumOfPowerOk > _infiniteImpulseResponseLimit;
    return powerOk;
  }

  /**
   * Check to see if the interlock chain is open (broken).
   * @author Rex Shang
   */
  public boolean isInterlockOpen() throws XrayTesterException
  {
    boolean isOpen = false;

    if (isServiceModeOn())
    {
      if (getDiagnosticsCode() != _DIAGNOSTICS_INTERLOCK_IS_CLOSED)
        isOpen = true;
    }
    else
    {
      if (_digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        isOpen = true;
    }

    return isOpen;
  }

  /**
   * Check to see if the interlock chain 1 is open (broken) in normal mode
   * @author Poh Kheng
   */
  public boolean isInterlockChain1Open() throws XrayTesterException
  {
    boolean isOpen = false;

      if (_digitalIo.isInterlockChain1Open())
        isOpen = true;

    return isOpen;
  }

  /**
   * Check to see if the interlock chain 2 is open (broken) in normal mode
   * @author Poh Kheng
   */
  public boolean isInterlockChain2Open() throws XrayTesterException
  {
    boolean isOpen = false;

      if (_digitalIo.isInterlockChain2Open())
        isOpen = true;

    return isOpen;
  }

  /**
   * Check to see if the service switch is on.
   * @author Rex Shang
   */
  public boolean isServiceModeOn() throws XrayTesterException
  {
    // Make sure the interlock board is powered on first.
    if (isInterlockPoweredOn() == false)
    {
      _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockIsNotOnException());
    }

    return _digitalIo.isInterlockServiceModeOn();
  }

  /**
   * In non-service mode, we can query the hardware for its revision id.
   * @author Rex Shang
   */
  public int getHardwareRevisionId() throws XrayTesterException
  {
    if (_hardwareRevisionId == -1)
    {
      if (isServiceModeOn())
      {
        _hardwareTaskEngine.throwHardwareException(
            InterlockHardwareException.getInterlockIsInServiceModeException());
      }

      int bit3 = _digitalIo.getInterlockHardwareIdBit3();
      int bit2 = _digitalIo.getInterlockHardwareIdBit2();
      int bit1 = _digitalIo.getInterlockHardwareIdBit1();
      int bit0 = _digitalIo.getInterlockHardwareIdBit0();

      int id = bit3 << 3 | bit2 << 2 | bit1 << 1 | bit0;

      if (_hardwareRevisionList.containsKey(new Integer(id)) == false)
      {
        // Invalide hardware revision id.
        _hardwareTaskEngine.throwHardwareException(
            InterlockHardwareException.getInterlockInvalidHardwareRevisionException(id));
      }

      _hardwareRevisionId = id;
    }

    return _hardwareRevisionId;
  }

  /**
   * @return hardware id from configuration file.
   * @author Rex Shang
   */
  static int getHardwareRevisionIdFromConfiguration()
  {
    return getConfig().getIntValue(HardwareConfigEnum.INTERLOCK_LOGIC_BOARD_REVISION_ID);
  }

  /**
   * @return keylock features from configuration file.
   * @author Anthony Fong
   */
  static public boolean getInterLockLogicBoardWithKeylock()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.INTERLOCK_LOGIC_BOARD_WITH_KEYLOCK);
  }

  /**
   * In service mode, we can indict the switch that cause the interlock to open.
   * @author Rex Shang
   */
  private int getDiagnosticsCode() throws XrayTesterException
  {
    Assert.expect(isServiceModeOn(), "Service mode is not on.");

    int bit5 = _digitalIo.getInterlockSwitchIdBit5();
    int bit4 = _digitalIo.getInterlockSwitchIdBit4();
    int bit3 = _digitalIo.getInterlockSwitchIdBit3();
    int bit2 = _digitalIo.getInterlockSwitchIdBit2();
    int bit1 = _digitalIo.getInterlockSwitchIdBit1();
    int bit0 = _digitalIo.getInterlockSwitchIdBit0();

    int id = bit5 << 5 | bit4 << 4 | bit3 << 3 | bit2 << 2 | bit1 << 1 | bit0;
    return id;
  }

  /**
   * The _diagnosticCodeToDescriptionMap is used to decipher the interlock error status.
   * The values stored in the map are pairs consist of a string and a boolean.
   * The string reprents the localized description of the diagnostics code and
   * the boolean represents whether the description is customer visible.
   * @author Rex Shang
   */
  protected void setupDiagnosticCodeDescriptions()
  {
    _diagnosticCodeToDescriptionMap = new TreeMap<Integer, Pair<String, Boolean>>();

    _diagnosticCodeToDescriptionMap.put(new Integer(0), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_NO_PROBLEM_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(1), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_1_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(2), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_1_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(3), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_2_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(4), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_2_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(5), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_CAMERA_ENCLOSURE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(6), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(7), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_CAMERA_ENCLOSURE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(8), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(9), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_3_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(10), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_3_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(11), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_4_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(12), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_4_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(13), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_LEFT_OUTER_BARRIER_PRESENCE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(14), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_LEFT_OUTER_BARRIER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(15), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_LEFT_OUTER_BARRIER_PRESENCE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(16), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_LEFT_OUTER_BARRIER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(17), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_RIGHT_OUTER_BARRIER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(18), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_RIGHT_OUTER_BARRIER_PRESENCE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(19), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_RIGHT_OUTER_BARRIER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(20), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_RIGHT_OUTER_BARRIER_PRESENCE_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(21), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_TOWER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(22), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(23), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_TOWER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(24), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(25), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_5_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(26), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_5_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(27), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_ACCESS_PANEL_6_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(28), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_ACCESS_PANEL_6_KEY"), new Boolean(true)));
    if(getInterLockLogicBoardWithKeylock()==false)_diagnosticCodeToDescriptionMap.put(new Integer(29), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_LEFT_SLIDER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(30), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    if(getInterLockLogicBoardWithKeylock()==false)_diagnosticCodeToDescriptionMap.put(new Integer(31), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_LEFT_SLIDER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(32), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    if(getInterLockLogicBoardWithKeylock()==false)_diagnosticCodeToDescriptionMap.put(new Integer(33), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_RIGHT_SLIDER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(34), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    if(getInterLockLogicBoardWithKeylock()==false)_diagnosticCodeToDescriptionMap.put(new Integer(35), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_RIGHT_SLIDER_KEY"), new Boolean(true)));
    _diagnosticCodeToDescriptionMap.put(new Integer(36), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(37), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_UNUSED_KEY"), new Boolean(false)));
    // The next 4 code is somewhat useless since it will get superceded isInterlockPoweredOn().  We will never report the following condition.
    _diagnosticCodeToDescriptionMap.put(new Integer(38), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_24V_POWER_SUPPLY_TOO_HIGH_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(39), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_24V_POWER_SUPPLY_TOO_LOW_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(40), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_48V_POWER_SUPPLY_TOO_HIGH_KEY"), new Boolean(false)));
    _diagnosticCodeToDescriptionMap.put(new Integer(41), new Pair<String, Boolean>(StringLocalizer.keyToString("HW_INTERLOCK_48V_POWER_SUPPLY_TOO_LOW_KEY"), new Boolean(false)));
  }

  /**
   * Return the list of switch names that are part of the interlock system.
   * @author Rex Shang
   */
  public List<String> getDiagnosticsDescriptionNames()
  {
    List<String> switchNames = new ArrayList<String>();
    List<Pair<String, Boolean>> diagnosticsDescriptionPairs = new ArrayList<Pair<String,Boolean>>(_diagnosticCodeToDescriptionMap.values());
    for (Pair<String, Boolean> diagnosticsDescriptionPair : diagnosticsDescriptionPairs)
    {
      if (diagnosticsDescriptionPair.getSecond().booleanValue())
        switchNames.add(diagnosticsDescriptionPair.getFirst());
    }

    return switchNames;
  }

  /**
   * Return the diagnostics description indicated by the diagnostics code.
   * @author Rex Shang
   */
  public String getDiagnosticsDescription() throws XrayTesterException
  {
    int diagnosticsCode = getDiagnosticsCode();
    Pair<String, Boolean> diagnosticsDescriptionPair = _diagnosticCodeToDescriptionMap.get(diagnosticsCode);
    if (diagnosticsDescriptionPair == null)
    {
      _hardwareTaskEngine.throwHardwareException(
          InterlockHardwareException.getInterlockHardwareInvalidDiagnosticsCodeException(diagnosticsCode));
    }

    return diagnosticsDescriptionPair.getFirst();
  }

  /**
   * Return whether the Interlock hardware is installed.
   * @author Rex Shang
   */
  public static boolean isInstalled()
  {
    // If the id store in config file is 0, there is no acture hardware.
    if (getHardwareRevisionIdFromConfiguration() == 0)
      return false;
    else
      return true;
  }

  /**
   * @author Rex Shang
   */
  public void setTroubleShootModeOn()
  {
    _interlockTroubleshootModeOn = true;
  }

  /**
   * @author Rex Shang
   */
  public void setTroubleShootModeOff()
  {
    _interlockTroubleshootModeOn = false;
  }

   /**
   * @author Anthony Fong
   */
  public void setServicePanelActivation(boolean activate)
  {
    _servicePanelActivated = activate;
  }
  
  /**
   * @author Anthony Fong
   */
  public boolean getServicePanelActivation()
  {
    return _servicePanelActivated;
  }

  /**
   * Listen to PanelHandler load unload event.  When event starts, we need to
   * turn off the notification of interlock open.
   * @author Rex Shang
   */
  public synchronized void update(Observable observable, Object object)
  {
    if (observable instanceof HardwareObservable)
    {
      if (object instanceof HardwareEvent)
      {
        HardwareEvent event = (HardwareEvent)object;
        HardwareEventEnum eventEnum = event.getEventEnum();

        if (eventEnum instanceof PanelHandlerEventEnum)
        {
          PanelHandlerEventEnum panelHandlerEvent = (PanelHandlerEventEnum)eventEnum;
          if (panelHandlerEvent.equals(PanelHandlerEventEnum.LOAD_PANEL)
              || panelHandlerEvent.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
          {
            if (event.isStart())
            {
              _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = true;
            }
            else
            {
              _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = false;
            }
          }
        }
      }
    }
    
  }
  
  public void setIntelockByPassCheckingStatus(boolean bypass)
  {
    _BYPASS_CHECKING_FOR_OPEN_INTERLOCK = bypass;
  }
  
  /*
   * author sheng chuan
   * to record the existing light tower status before update to error mode due to interlock break
   */
  private void recordPreviousLightStatus() throws XrayTesterException
  {
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if(LightStack.getInstance().isYellowLightOn())
            _previousLight = _MACHINE_STANDBY_MODE;
          else if(LightStack.getInstance().isGreenLightOn())
            _previousLight = _HARDWARE_INUSED_MODE;
          else if(LightStack.getInstance().isRedLightOn())
            _previousLight = _SYSTEM_ERROR_MODE;
          else
            _previousLight = _ALL_LIGHT_OFF_MODE; 
        }
        catch(XrayTesterException ex)
        {
          //do nothing
        }
      }
    });
  }
  
  /*
   * author sheng chuan
   * to update the light tower from time to time
   * if errorMode = false, it will restore the light to previous mode when interlock no longer break
   * else it will turn to errormode when interlock break detected
   */
  private void trackingLightStackStatusBasedOnInterlockBreakingStatus(final boolean errorMode) throws XrayTesterException
  {
    //put it on a worker thread else it will got problem like "statechanged is not called in hardware worker thread"
    //Normally, this will work right away
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if(errorMode)
            LightStack.getInstance().errorMode();
          else
          {
            if(_previousLight == _MACHINE_STANDBY_MODE)
              LightStack.getInstance().standyMode();
            else if(_previousLight == _HARDWARE_INUSED_MODE)
              LightStack.getInstance().hardwareInUseMode();
            else if(_previousLight == _SYSTEM_ERROR_MODE)
              LightStack.getInstance().errorMode();
            else
              LightStack.getInstance().turnOffAllLights();
          }      
        }
        catch(XrayTesterException ex)
        {
          //do nothing
        }
      }
    });
  }

}
