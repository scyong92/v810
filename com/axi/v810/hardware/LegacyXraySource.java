package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the 5DX X-ray source.  This source is made up of a high voltage
 *  power supply (HVPS) with integral high voltage cable and an X-ray tube assembly.
 *  The X-ray tube assembly is composed of the X-ray tube itself, a resistor chain
 *  that connects the 4 anodes, top gun electronics (the XR controller), focusing coils,
 *  and a surrounding pressure vessel.  The X-ray tube and electronics are "bathed" in
 *  Sulfur hexa-Fluoride (SF<sub>6</sub>) gas which acts as an insulator to prevent arcing
 *  shorts between the various high voltage components that are not in vacuum.<p/>
 *
 * This gross representation of the tube itself will hopefully help make sense out the
 *  myriad of values that can be read and set.<br/>
 * <pre>
 *                   The 5DX X-ray Tube
 *                                                                     nominal working voltages
 *              |    |---------------|    |
 *              |    |     |   |<----+----+--- filament           5.5V WRTC
 *              |    |    /     \    |    |
 *              |    |    \~~~~~/    |<---+--- cathode            0V WRTC            -160kV WRTG
 *      gun --->|    |---------------|    |                        (by definition)
 *              |            :            |
 *              |    ------- : ------- <--+--- control grid       0 to -500V WRTC
 *              |            :            |
 *              |   -------- : -------- <-+--- focus grid         +2200V WRTC
 *       -------|            :            |-------
 *            \              :              /   |
 *             |             :             |    /
 *             |             :             |    \
 *             |<-- glass    :             |    / <--- resistor
 *             |             :             |    \
 *             |             :             |    /
 *            /              :              \   |
 *       ------------------- : -------------------  <--- anode     +40kV WRTC        -120kV WRTG
 *            \              :              /   |       (stage 1)
 *             |             :             |    /
 *             |             :             |    \
 *             |<-- glass    :             |    / <--- resistor
 *             |             :             |    \
 *             |             :             |    /
 *            /              :              \   |
 *       ------------------- : -------------------  <--- anode     +80kV WRTC         -80kV WRTG
 *            \              :              /   |       (stage 2)
 *             |             :             |    /
 *             |             :             |    \
 *             |<-- glass    :             |    / <--- resistor
 *             |             :             |    \
 *             |             :             |    /
 *            /              :              \   |
 *       ------------------- : -------------------  <--- anode     +120kV WRTC        -40kV WRTG
 *            \              :              /   |       (stage 3)
 *             |             :             |    /
 *             |             :             |    \
 *             |<-- glass    :             |    / <--- resistor
 *             |             :             |    \
 *             |             :             |    /
 *            /              :              \   |
 *       ------------------- : -------------------  <--- anode     +160kV WRTC          0kV WRTG
 *              |            :            |             (stage 4)
 *              |            :            |
 *              |            :            |
 *       oo     |            :            |     oo
 *      oooo    |            :            |    oooo <--- deflection coils (not part of tube)
 *       oo     |            :            |     oo
 *              |            :            |
 *              |            :            |
 *              |            :            |
 *       oo     |            :            |     oo
 *      oooo    |            :            |    oooo <--- deflection coils (not part of tube)
 *       oo     |            :            |     oo
 *              |            :            |
 *              |            :            |
 *              |            :            |
 *              |            :            |  <-----------------    +160kV WRTC          0kV WRTG
 *             /             :             \
 *            /              :              \
 *           /               :               \         _______
 *          /                :                \________|      | <--- molecular ion-pump
 *         /      2000 deg C :                  _______       |
 *        /              \   :                  \      |______|
 *       /                \  :                   \
 *      /                  \ :                    \
 *     /                    v:                     \
 *     |-------------------------------------------| <--- 0.07?thk Tungsten target
 *     |===========================================| <--- Beryllium window
 *     |===========================================| <--- collimator
 *                           ^
 *                           |
 *                       100 deg C
 * </pre>
 * <br/>
 *
 * <b>gun:</b> The entire gun is the cathode part of a tube as apposed to the anode.  For school,
 *   an X-ray tube is described technically as having an anode and a cathode and a window. The
 *   cathode is what supplies the electrons.  Once one begins describing the cathode in detail
 *   then the assembly becomes a gun because it is made up of: grids, bias grids, accelerator grids,
 *   false anodes, first anode, focus assembly, what ever type of gun it is?and an actual cathode
 *   which is a very tiny part of the cathode or gun. (Courtesy of Dave Reynolds)<p/>
 *
 * <b>cathode:</b> this is the free electron source for the tube. the filament is part of the cathode<p/>
 *
 * <b>WRTC:</b> with respect to cathode -- because electrons are pulled toward positive voltages and repelled
 *   from negative voltages, it is very helpful to think about the voltages of the various tube parts
 *   in comparison to the voltage of the cathode<p/>
 *
 * <b>WRTG:</b> with respect to ground (Greg made this up just for this diagram)<p/>
 *
 * <b>filament:</b> (think light bulb) you apply a voltage, it heats up and causes the cathode
 *   to "boil off" electrons. the command protocol allows for values from 0 to 9.7V,
 *   but only 0 to 6 actually occur<p/>
 *
 * <b>control grid (aka grid):</b> by adjusting the amount of negative voltage WRTC, one can adjust
 *   how many electrons escape from the cathode (the cathode current). -500V WRTC turns the cathode off
 *   (actually can be more negative, but this is the largest (most negative) value the firmware can report).
 *   0V WRTC is presumably wide open or completely on. this code is not allowed to set the grid voltage,
 *   that is the prerogative of the top gun electrons. however, its value can be read<p/>
 *
 * <b>focus grid (aka focus):</b> by adjusting the amount of positive voltage WRTC, one can form the
 *   electrons that escape the cathode and pass the control grid into a beam heading towards the first
 *   anode stage<p/>
 *
 * <b>anode (stage n):</b> the "anode" is actually 4 anode stages. each stage is separated by 1/4 of what will
 *   be called the anode voltage.  E.g., at nominal operating settings, each stage adds +40kV of "boost"
 *   to the electron stream. the stages together represent an �accelerator?very analogous to SLAC (the
 *   Stanford Linear Accelerator), see <a href="http://www.slac.stanford.edu/">www.slac.stanford.edu</a>
 *   <p/>
 *
 * <b>resistor:</b> these resistors keep the anode stages at there relative voltages. in order to maintain
 *   the voltage differential, a slight bit of current is used by the resistor (dissipated as heat).
 *   AKA divider resistor, or bleeder resistor<p/>
 *
 * <b>bleeder current:</b> the current used by the whole chain of divider resistors (there are actually
 *   more than one per stage) is called the bleeder current<p/>
 *
 * <b>leakage current:</b> current traveling through ionized gas in the tube's vacuum chamber.
 *   failing cable or connector, bad grease in connector, failed caps or resistors, failing pcb, ...
 *   will also show as leakage. i.e., in a perfect system, the cathode plus bleeder current would
 *   be the only current supplied by the HVPS. any other current is leakage<p/>
 *
 * <b>total current:</b> this the total current supplied by the HVPS and is equal to the sum of the
 *   cathode, bleeder, and leakage currents<p/>
 *
 * <b>vacuum:</b> the interior of the tube is kept at a very good vacuum, but there are is always
 *   some amount of gas present in the tube.  there are also various ongoing processes that add
 *   small amounts of gas to the tube over time<p/>
 *
 * <b>molecular ion-pump:</b> any gas molecules that wander in are trapped. molecules wander by thermal motion,
 *   so this pump really sucks and sucking. however, since the vacuum starts out excellent, it tends
 *   to get even better over time because of this pump (in contrast, CRTs have a "getter" right in
 *   the tube cavity, so gas molecules have a much higher probability of hitting the getter)<p/>
 *
 * <b>arcing:</b> any gas inside the tube is eventually ionized and pushed to the end of the cavity.
 *   if there is enough gas and it is ionized so quickly that it doesn't have a chance to bunch up
 *   at one end of the cavity (remaining distributed throughout the cavity), then it become a good
 *   conductor and arcs form across the affected cavity. think of these as little lightning bolts;
 *   they are "not good" for the tube. arcs can be detected because they cause a spike in current
 *   and drop in voltage (which halts X-ray production). if the power supply is not properly current
 *   limited, then arcing will destroy a tube very quickly. one of the overriding design
 *   considerations of the 5DX tube was, "it shall not arc"<p/>
 *
 * <b>deflection coils:</b> these coils create magnetic fields which focus and point (steer) the beam of
 *   160kV electrons so they hit the target where they are supposed to hit with the proper �spot size?
 *   which relates directly to 5DX image quality<p/>
 *
 * <b>steering:</b> or deflection coils, the 5DX X-ray source has this really cool active steering
 *   mechanism. when used in the Genesis product, however, no active steering is necessary.  the
 *   steering will be provided by two simple Helmholtz coils that are driven by 2 manually adjusted
 *   power supplies.  it will be just good enough to keep the beam on the collimator<p/>
 *
 * <b>collimator:</b> more correctly called �dump ring?for a normal 5DX, collimator is more correct
 *   in the Genesis use. collimators are generally just tube-like things that let light to go in roughly
 *   one direction (rather like putting a flashlight up to an empty toilet paper roll). this specific
 *   collimator's purpose is to block the "4 pi steradians" (sphere-like distribution) of X-rays and only allow them onto the  detector area<p/>
 *
 * <b>heat dissipation:</b> 2000 &deg;C is pretty hot, but there is still only 16W of heat
 *   dissipation (16kV * 100uA = 16W). compare that to over 100W of heat dissipation for a Pentium 4,
 *   60W for common incandescent light bulb, and 16W for those "replaces a 60W" fluorescent bulbs.
 *   this thing can kill you, but it sure won't keep you warm...<p/>
 *
 * <b>For more information:</b> Search for Cathode Ray Tube at
 *   <a href="http://en.wikipedia.org/wiki/Cathode_ray_tube">www.wikipedia.org</a>
 *   - many of the workings and issues are similar, not mention the vocabulary.
 *   You could also talk to the hardware guys and ask to look at their toys...<p/>
 *
 * <b>HARDWARE REPORTED ERRORS:</b> The HVPS controller reports on a myriad of error
 *   conditions that can occur on the hardware (see getHardwareReportedErrors()).
 *   Methods that attempt to change the state of the hardware are "wrapped" in checks
 *   for these hardware reported errors.  Methods that simply read the state of the
 *   hardware are not so wrapped.<p/>
 *
 * <b>NOTE ON abort():</b> Methods that spin block (i.e., sleep) check the abort flag.
 *  abort() is taken to mean, "stop doing anything that can't be done quickly."
 *  Specifically, a call to abort() is interpreted as a request to "break out of
 *  spin block loops if the abort flag is set."  E.g., <pre>while (true)</pre>
 *  becomes <pre>while (isAborting() == false)</pre>.  Exceptions are made where
 *  continuing to issue "quick" commands on the hardware might cause damage.<p/>
 *
 * <b>THREAD SAFETY:</b> This class is re-entrant (see LegacyXraySourceCommandingLink).<p/>
 *
 * <b>TEST NOTE:</b> From a unit-test / code-coverage perspective, this entire class is
 *   testable without any hardware.  I.e., the code can be tested against sunny day
 *   responses from a simulated serial link.<p/>
 *
 * @author Greg Loring
 */
public class LegacyXraySource extends AbstractXraySource
{
  private static String _me = "LegacyXraySource";
  // provide feedback to the user
  private static ProgressObservable _progressObservable = ProgressObservable.getInstance();

  // see javadoc on simulateTiming()
  private static boolean _simulateTiming = false;

  // object which handles the low-level commanding protocol
  private LegacyXraySourceCommandingLink _commandingLink;

  // measurement state
  private StatisticalTimer _onCallTimer = new StatisticalTimer("total");
  private StatisticalTimer _onKVOnTimer = new StatisticalTimer("kV-on");
  private StatisticalTimer _onUAOnTimer = new StatisticalTimer("uA-on");
  private StatisticalTimer _turnBackOnCallTimer = new StatisticalTimer("turnBackOn");
  private StatisticalTimer _offCallTimer = new StatisticalTimer("off()");
  private StatisticalTimer _powerOffCallTimer = new StatisticalTimer("powerOff");
  private StatisticalTimer _powerOffUAOffTimer = new StatisticalTimer("uA-off");
  private StatisticalTimer _powerOffKVOffTimer = new StatisticalTimer("kV-off");
  private StatisticalTimer _areXraysOnStatisticalTimer = new StatisticalTimer("areXraysOn-ck");
  private StatisticalTimer _legacyXrayOnStatisticalTimer = new StatisticalTimer("on()");

  // optimization state
  private double _lastSetAnodeKiloVolts = Double.NaN;
  private double _lastSetFilamentVolts = Double.NaN;
  private double _lastSetFocusVolts = Double.NaN;

  // legacy xray source controls inner barrier.
  private InnerBarrier _innerBarrier;

  private HardwareTaskEngine _hardwareTaskEngine;

  private static boolean _SERVICE_MODE_ON = true;
  private static boolean _SERVICE_MODE_OFF = false;

  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  private boolean  _xrayAutoShortTermShutdownTimerEnabled;
  private double _xrayAutoShortTermShutdownTimeoutMinutes;


  /**
   * the standard sim mode just blazes through without any sleep calls; call this method
   *  to get a realistic feel for how long things will actually take on the hardware
   * @author Greg Loring
   */
  public static void simulateTiming()
  {
    _simulateTiming = true;
    LegacyXraySourceCommandingLink.setSimulateLinkTiming();
  }

  // convenience methods for getting config and parameterized values:
  //  after pushing back on David Reynolds (tube designer) and Jim Behnke (deployment),
  //  many of these are being hard coded.  however, let's leave the getters here in case
  //  more flexibility is needed in the future.
  // another nice thing about these is that JBuilder shows you which values have not been used.

  /** @author Greg Loring */
  private static String getSerialPortName() // typically COMn
  {
    String result = _config.getStringValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_SERIAL_PORT_NAME);
    return result;
  }

  /** @author Greg Loring */
  private static double getTurnOnFocusVolts() // typically 2200V
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_TURN_ON_FOCUS_VOLTS);
    return result;
  }

  /** @author Greg Loring */
  private static double getTurnOnFilamentVolts() // typically 5.5V
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_TURN_ON_FILAMENT_VOLTS);
    return result;
  }

  /** @author Greg Loring */
  private static int getMaxMinutesDownForWarmStart() // typcially 2 hours
  {
    int result = _config.getIntValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_MAX_MINUTES_DOWN_FOR_WARM_START);
    return result;
  }

  /** @author Greg Loring */
  private static long getLastShutdownTimeInMillis() // written back by app; start with 0
  {
    // int not big enuf, long not available, so use double
    long result = (long) _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_LAST_SHUTDOWN_TIME_IN_MILLIS);
    return result;
  }

  /** @author Greg Loring */
  private static double getColdStartAnodeVoltageRampTimeInMinutes() // typically 15 min
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_COLD_START_ANODE_VOLTAGE_RAMP_TIME_IN_MINUTES);
    return result;
  }

  /** @author Greg Loring */
  private static double getWarmStartAnodeVoltageRampTimeInMinutes() // typically 1 min
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_WARM_START_ANODE_VOLTAGE_RAMP_TIME_IN_MINUTES);
    return result;
  }

  /**
   * actually just a lower-bound: don't try to account for the other time spent in the loop
   * @author Greg Loring
   */
  private static long getAnodeVoltageRampCommandSeparationDelayInMillis()
  {
    return 100L; // ms
  }

  /**
   * @author Rex Shang
   */
  private static double getTurnOffAnodeKiloVolts()
  {
    return 0.0;
  }

  /** @author Greg Loring */
  private static double getTurnOnAnodeKiloVolts() // typcially 160kV
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_TURN_ON_ANODE_KILOVOLTS);
    return result;
  }

  /** @author Greg Loring */
  private static double getAnodeKiloVoltTolerance()
  {
    // it seemed odd that the anode voltage ramp spends about 25% of it's time ramping up the
    //  last 4% of the turn-on voltage, and then to use 4% as the tolerance for checking that
    //  the final voltage is correct.  but, Dave Reynolds sez not to worry about it...
    return 6.4; // kV; 4% of nominal turn-on value, per standard 5DX config file
  }

  /**
   * when waiting for a measure to stabilize, we do NOT want read as fast as we can for fear
   *  that we will just see the measure crossing the final value rather than settling onto it;
   *  actually just a lower-bound: don't try to account for the other time spent in the loop
   * @author Greg Loring
   */
  private static long getStabilizationReadSeparationDelayInMillis()
  {
    return 500L; // ms
  }

  /** @author Greg Loring */
  private static double getAnodeVoltageStabilizationTimeoutSeconds()
  {
    return 60.0; // s
  }

  /** @author Greg Loring */
  public static double getExpectedAnodeOnBleederMicroAmps() // typically 49.7uA
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_EXPECTED_ANODE_ON_BLEEDER_MICROAMPS);
    return result;
  }

  /** @author Greg Loring */
  private static double getAllowableLeakageCurrentInMicroAmps()
  {
    return 3.0; // uA
  }

  /**
   * @author Rex Shang
   */
  private static double getTurnOffCathodeMicroAmps()
  {
    return 0.0;
  }

  /** @author Greg Loring */
  private static double getTurnOnCathodeMicroAmps() // typcially 100uA
  {
    double result = _config.getDoubleValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_TURN_ON_CATHODE_MICROAMPS);
    return result;
  }

  /** @author Greg Loring */
  private static double getConfigCathodeMicroAmpTolerance()
  {
    return 3.0; // uA; 3% of nominal turn-on value, per standard 5DX config file
  }

  /**
   * Cathode ramp happens pretty quickly (~1-2 secs) if it is only thing that needs ramp.
   * However, it is found that when we ramp cathode along with filament voltage
   * and focus voltage, it gets as many as 10 secs...
   * @author Greg Loring
   * @author Rex Shang
   */
  private static double getCathodeCurrentStabilizationTimeoutSeconds()
  {
    return 12.0; // s
  }

  /** @author Greg Loring */
  private static double getMaxAnodeOffKiloVolts()
  {
    return 5.0; // kV
  }

  /** @author Greg Loring */
  private static double getMaxCathodeOffMicroAmps()
  {
    return 11.0; // uA
  }

  /** @author Greg Loring */
  private static double getShutdownFilamentVolts()
  {
    return 0.1; // V
  }

  /** @author Greg Loring */
  private static double getShutdownFocusVolts()
  {
    return 500.0; // V
  }

  /**
   * @author Greg Loring
   */
  LegacyXraySource()
  {
    // guarantee that all of the objects on which i depend are in place
    _innerBarrier = InnerBarrier.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();

    // the _commandingLink object handles the low-level communication protocol
    _commandingLink = new LegacyXraySourceCommandingLink(getSerialPortName());

    //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
    _xrayAutoShortTermShutdownTimerEnabled = _config.getBooleanValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED);
    _xrayAutoShortTermShutdownTimeoutMinutes = _config.getDoubleValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES);

  }

  /**
   * there is no convienient set of hardware measures that is useful for implementing this
   *  method across all of the rapidly evolving hardware configurations. so, just use this
   *  to ask that startup() get called at least once.
   *
   * @return true if startup() has been called more recently than shutdown(), false otherwise
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#isStartupRequired()
   *
   * @author Greg Loring
   * @author Rex Shang
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    boolean startupRequired = true;

    if (super.isStartupRequired() == false)
    {
      // System that is using legacy xray source and come whithout the inner
      // barrier can only be a lab system.  We want to be more lenient because
      // loading/unloading panel will cause the voltage to drop and cause
      // the whole system needing to startup.
      if (_innerBarrier.isInstalled())
      {
        if (isAnodeOn())
          startupRequired = false;
      }
      else
        startupRequired = false;
    }

    return startupRequired;
  }

  /**
   * put this object into simulation mode
   *
   * @author Greg Loring
   */
  public void setSimulationMode() throws XrayTesterException
  {
    //make sure to call the setSimulationMode for the super class
    super.setSimulationMode();
    _commandingLink.setSimulationMode();
  }

  /**
   * take this object out of individual simulation mode (may stay in simulation
   *  mode based on overall system configuration
   *
   * @author Greg Loring
   */
  public void clearSimulationMode() throws XrayTesterException
  {
    //make sure to clear the setSimulationMode for the super class
    super.clearSimulationMode();
    _commandingLink.clearSimulationMode();
  }

  /**
   * sim mode may be on for this object because it was turned on explicitly via
   *  setSimulationMode(), or because there is some indication in the overall
   *  system configuration that all hardware related objects should be operating
   *  in sim mode.  In either case, this object will not interact with hardware.
   *
   * @author Greg Loring
   */
  public boolean isSimulationModeOn()
  {
    boolean result = _commandingLink.isSimulationModeOn();

    //Assure that the various sim mode flags are in agreement. They must be or
    //something is badly wrong
    Assert.expect(result == super.isSimulationModeOn());

    return result;
  }

  /**
   * if you get an Assert ... 9999 thrown at you, try this
   * @author Greg Loring
   */
  public void resetCommandingLink() throws HardwareException
  {
    _commandingLink.reset();
  }

  /**
   * Is the X-ray source hardware producing X-rays suitable for imaging?<p/>
   *
   * This method returns false if the X-ray source is operating in either the
   *  low-power or high-power survey/safety-test modes.
   *
   * @author Greg Loring
   * @author Rex Shang
   */
  public boolean areXraysOn() throws XrayTesterException
  {
    return areXraysOnInternal(_SERVICE_MODE_OFF);
  }

  /**
   * Is the X-ray source hardware producing X-rays?
   * @author Rex Shang
   */
  public boolean areXraysOnForServiceMode() throws XrayTesterException
  {
    return areXraysOnInternal(_SERVICE_MODE_ON);
  }

  /**
   * Used by service mode and normal mode to determine if X-rays are emitting.
   * In service mode, xrays can be on regardless whether the inner barrier is
   * closed or not.
   * @author Greg Loring
   * @author Rex Shang
   */
  private boolean areXraysOnInternal(boolean serviceMode) throws XrayTesterException
  {
    try
    {
      _areXraysOnStatisticalTimer.start();

      if (isSurveyModeOn())
        return false;

      // check the inner barrier first, since this call is much faster than calling the tube for voltage or current
      if (serviceMode == false && _innerBarrier.isInstalled())
      {
        if (_innerBarrier.isOpen() == false)
          return false;
      }

      if (isAnodeOn())
      {
        if (isCathodeOn())
          return true;
      }

      return false;
    }
    finally
    {
      _areXraysOnStatisticalTimer.stop();
      debug(_areXraysOnStatisticalTimer);
    }
  }

  /**
   * Could the X-ray source hardware be producing X-rays from a health/safety perspective?<p>
   *
   *  Please note that it is normal and expected for an X-ray source to be producing X-rays,
   *  but still not be ready for imaging to take place.<p>
   *
   *  This method has not been pushed into the base class for fear it would cause clutter.
   *  However, it is very useful for testing purposes and because it clarifies and encodes
   *  vital knowledge.
   *
   * @author Greg Loring
   */
  public boolean couldUnsafeXraysBeEmitted() throws XrayTesterException
  {
    // if the anode voltage is high enough, then the leakage current will cause some
    //  amount of electrons to be liberated and cause events (X-rays)
    boolean result = (getAnodeVoltageInKiloVolts() > getMaxAnodeOffKiloVolts());
    return result;
  }

  /**
   * Get the X-ray source hardware ready for imaging, i.e., start emitting imaging quality X-rays.<p>
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 160kV</li>
   *  <li>nominal cathode current is 100uA</li>
   *  <li>areXraysOn() returns true</li>
   *  <li>couldUnsafeXraysBeEmitted() returns true</li>
   * </ul>
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @see #areXraysOn()
   * @see #couldUnsafeXraysBeEmitted()
   *
   * @author Greg Loring
   * @author Rex Shang
   */
  public void on() throws XrayTesterException
  {
    _legacyXrayOnStatisticalTimer.start();
    //Assert.expect(super.isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ON);
    _hardwareObservable.setEnabled(false);
    try
    {
      TurnOnXRayAndOpenInnerBarriersInParallel();
    }
    finally
    {
      _legacyXrayOnStatisticalTimer.stop();
      debug(_legacyXrayOnStatisticalTimer);
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ON);
  }

  /**
   * @author Rex Shang
   */
  private void TurnOnXRayAndOpenInnerBarriersInParallel() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> parallelTasks = new ExecuteParallelThreadTasks<Object>();

    ThreadTask<Object> task1 = new TurnOnLegacyXraySourceThreadTask(this);
    ThreadTask<Object> task2 = new OpenInnerBarrierThreadTask();

    // Start the tasks and wait.
    parallelTasks.submitThreadTask(task1);
    parallelTasks.submitThreadTask(task2);
    try
    {
      parallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Rex Shang
   */
  void ensureXraysAreOn() throws XrayTesterException
  {
    if (isCathodeOn() == false)
    {
      if (isAnodeOn())
        turnBackOn();
      else
        on(getTurnOnAnodeKiloVolts(), getTurnOnCathodeMicroAmps(), getTurnOnFocusVolts());
    }
  }

  /**
   * turn on for low-power survey/safety-test mode operation.
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @author Greg Loring
   */
  public void lowPowerOn() throws XrayTesterException
  {
    on(getTurnOnAnodeKiloVolts(), 20.0, getTurnOnFocusVolts());
    setSurveyModeOn(true);
  }

  /**
   * turn on for normal-power survey/safety-test mode operation.
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @author George Booth
   */
  public void normalPowerOn() throws XrayTesterException
  {
    on(getTurnOnAnodeKiloVolts(), 100.0, getTurnOnFocusVolts());
    setSurveyModeOn(true);
  }

  /**
   * turn on for high-power survey/safety-test mode operation.<p/>
   *
   * WARNING: The parameters for anode kV, cathod uA, and focus V have not been finalized.
   *   There is some concern that using this method with the current parameters will burn
   *   out X-ray tubes.  The typically operation of the 5DX tube has the beam moving in a
   *   circle around the target, so there is little concern about burning through the target.
   *   In the Genesis product, the beam always encounters the target at the same spot.
   *   There was discussion about defocussing the beam (hence focus voltage is parameterized),
   *   but the small size of the collimator opening caused greater concerns regarding not
   *   hitting the opening of the collimator at all if intentional defocussing was done.
   *   Simply changing to 165.0 kV from getTurnOnAnodeKiloVolts() should cause some amount
   *   of defocussing.  This was deemed good enough until system test determines the proper
   *   set of parameters.  I (Greg Loring) have some angst about whether testing results will
   *   be effectively communicated (or even done ...not trying to cast blame, it just seems
   *   like these things happen, and I will certainly forget to about it).
   *   So, if 5DX tubes start burning out due to highPowerOn() being called, then send
   *   a copy of these comments to me, Kris Kanack, Dave Reynolds, and John Siefers (or our
   *   corporate heirs).
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @author Greg Loring
   */
  public void highPowerOn() throws XrayTesterException
  {
    on(165.0, 125.0, getTurnOnFocusVolts());
    setSurveyModeOn(true); // call after on(...) so that isSurveyMode() can be used
  }

  /**
   * implements turn-on for normal, low-power, and high-power modes
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @author Greg Loring
   */
  private void on(double anodeKiloVolts, double cathodeMicroAmps, double focusVolts) throws XrayTesterException
  {
    try
    {
      // call this at the front of abort-handling methods because abort() may have
      //  been called during a call to a non-abort-handling method and not cleared
      clearAbort();

      _onCallTimer.start();

      // just started
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 0);

      // turn the focus on
      setFocusVoltageInVolts(focusVolts);
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 5);

      // turn the filament on
      setFilamentVoltageInVolts(getTurnOnFilamentVolts());
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 10);

      // see rampUpAnodeVoltage() comments (returns early on abort)
      _onKVOnTimer.start();
      rampUpAnodeVoltage(anodeKiloVolts); // abort()-able
      waitForAnodeVoltageToStabilize(anodeKiloVolts); // at full turn-on value; abort()-able
      _onKVOnTimer.stop();

      // following calls are problematic if the anode voltage ramp did not complete (see NOTE:s)
      if (isAborting())
        return; // also prevents _hardwareObservable.stateChangedEnd(... call

      // turn cathode on
      //  NOTE: may cause hardware degradation if anode voltage is not all the way on
      _onUAOnTimer.start();
      setCathodeCurrentInMicroAmps(cathodeMicroAmps);
      waitForCathodeCurrentToStabilize(cathodeMicroAmps); // abort()-able
      _onUAOnTimer.stop();

      _onCallTimer.stop();
      debug("LegacyXraySource.on() timing: " + _onKVOnTimer + ", " + _onUAOnTimer + ", " + _onCallTimer);
    }
    finally
    {
      // Report 100% done even if we have exception.
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 100);
    }
  }

  /**
   * turn *back* on to normal power mode when anode voltage is already OK; skips leakage current check
   *
   * NOTE: pays attention to the abort flag<p/>
   *
   * @author Greg Loring
   */
  private void turnBackOn() throws XrayTesterException
  {
    try
    {
      // call this at the front of abort-handling methods because abort() may have
      //  been called during a call to a non-abort-handling method and not cleared
      clearAbort();

      // expect a total time budget of 2.8s:
      //  - every command/response interaction with the hardware takes almost exactly .2s
      //     (measured 204 +/- 3 ms on more than 100 commands)
      //  - call to waitFor...ToStabilize... methods take at least 1.4 seconds (two reads and 1 sec sleep)
      //  - use the unguarded versions of setters to avoid the .4 seconds of error checking overhead
      //     (wrap the entire method in checkForHardwareReportedErrors() calls instead)

      _turnBackOnCallTimer.start();

      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 0);

      // this call could send 3 commands to hardware, but only if the hardware is reporting errors,
      //  in which case it will throw; the innocuous case of interlocks would cause anode voltage
      //  to be turned off, thus this method wouldn't be called (not assert-ing that to save .2s)
      checkForHardwareReportedErrors(); // .2s
      // report aggressively at first in attempt to be a short pole (& stay off screen)
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 14);

      if (_lastSetFocusVolts != getTurnOnFocusVolts())
        unguardedSetFocusVoltageInVolts(getTurnOnFocusVolts()); // .2s
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 21);

      if (_lastSetFilamentVolts != getTurnOnFilamentVolts())
        unguardedSetFilamentVoltageInVolts(getTurnOnFilamentVolts()); // .2s
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 28);

      if (_lastSetAnodeKiloVolts != getTurnOnAnodeKiloVolts())
        unguardedSetAnodeVoltageInKiloVolts(getTurnOnAnodeKiloVolts()); // .2s
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 35);

      unguardedSetCathodeCurrentInMicroAmps(getTurnOnCathodeMicroAmps()); // .2s
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 42);

      waitForCathodeCurrentToStabilize(getTurnOnCathodeMicroAmps()); // abort()-able; 1.6s (measured, indirectly)
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 93);

      checkForHardwareReportedErrors(); // .2s
    }
    finally
    {
      _turnBackOnCallTimer.stop();
      debug("LegacyXraySource.turnBackOn() timing: " + _turnBackOnCallTimer);

      // Report done even exception occurs.
      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 100);
    }
  }

  /**
   * Stop the X-ray source hardware from emitting imaging quality X-rays.  Use this method
   *  imaging is being stopped for a short time (i.e., the system is not being powered off).<p>
   *
   *   WARNING: Calling this method does NOT stop the hardware from emitting X-rays.
   *     X-rays will NOT be off from a health/safety perspective.  The only guarantee
   *     is that any X-rays that are emitted will not be adequate for imaging purposes.<p>
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 160kV</li>
   *  <li>nominal cathode current is 0uA</li>
   *  <li>areXraysOn() returns false</li>
   *  <li>couldUnsafeXraysBeEmitted() returns true</li>
   * </ul>
   *
   * @see #areXraysOn()
   * @see #couldUnsafeXraysBeEmitted()
   *
   * @author Greg Loring
   * @author Rex Shang
   */
  public void off() throws XrayTesterException
  {
    offInternal(_SERVICE_MODE_OFF);
  }

  /**
   * @author Rex Shang
   */
  public void offForServiceMode() throws XrayTesterException
  {
    offInternal(_SERVICE_MODE_ON);
  }

  /**
   * Used by service and normal mode to stop the X-ray source hardware from
   * emitting imaging quality X-rays.
   * In normal mode, for speed purpose, we are closing the inner barrier and
   * leave the x-ray souce full on.
   * But for service mode, we turn the current off but still allow a very fast
   * on later.
   * @author Greg Loring
   * @author Rex Shang
   */
  private void offInternal(boolean serviceMode) throws XrayTesterException
  {
    // call this at the front of abort-handling methods because abort() may have
    //  been called during a call to a non-abort-handling method and not cleared
    clearAbort();

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.OFF);
    _hardwareObservable.setEnabled(false);
    try
    {
      _offCallTimer.start();
      if (serviceMode == false && _innerBarrier.isInstalled())
      {
        if (_innerBarrier.isOpen())
          _innerBarrier.close();
      }
      else
      {
        // turn cathode off
        setCathodeCurrentInMicroAmps(0.0);
        waitForCathodeCurrentOff(); // abort()-able

        // NOTE: anode voltage may still be up, so may still be producing enuf X-rays to be unsafe
      }
    }
    catch (XraySourceHardwareReportedErrorsException xshrex)
    {
      // suppress hardware errors if hardware is in the off() state
      if (getCathodeCurrentInMicroAmps() > getMaxCathodeOffMicroAmps())
      {
        throw xshrex;
      }
    }
    finally
    {
      _offCallTimer.stop();
      debug(_offCallTimer);
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.OFF);
  }

  /**
   * Stop the X-ray source hardware from emitting any X-rays and get it ready for all power
   *  to be turned off.
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 0kV</li>
   *  <li>nominal cathode current is 0uA</li>
   *  <li>areXraysOn() returns false</li>
   *  <li>couldUnsafeXraysBeEmitted() returns false</li>
   * </ul>
   *
   * @see #areXraysOn()
   * @see #couldUnsafeXraysBeEmitted()

   * @author Greg Loring
   */
  public void powerOff() throws XrayTesterException
  {
    // call this at the front of abort-handling methods because abort() may have
    //  been called during a call to a non-abort-handling method and not cleared
    clearAbort();

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.POWER_OFF);
    _hardwareObservable.setEnabled(false);
    try
    {
      _powerOffCallTimer.start();

      _powerOffUAOffTimer.start();

      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 0);

      // turn the cathode off
      setCathodeCurrentInMicroAmps(0.0);

      // make sure the cathode is off.  if the anode voltage is not turned up,
      //  then the focus voltage will "slingshot" electrons back into the grid
      //  (which is a very special material that degrades under such treatment)
      waitForCathodeCurrentOff(); // abort()-able

      _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 20);

      _powerOffUAOffTimer.stop();

      // leave the filament a bit warm
      setFilamentVoltageInVolts(getShutdownFilamentVolts());

      // turn the focus down
      setFocusVoltageInVolts(getShutdownFocusVolts());

      // turn the anode off
      _powerOffKVOffTimer.start();
      setAnodeVoltageInKiloVolts(0.0); // make sure anode voltage is off
      waitForAnodeVoltageOff(); // abort()-able
      _powerOffKVOffTimer.stop();

      // unit test script is now checking that config files don't change (or are put back);
      //  since so many unit tests depend on this class and there is no one place to
      //  put things back just for unit testing, just don't make the change...
      if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == false)
      {
        // record the time when the X-ray source was powered down so we can use it to
        //  determine whether a warm start is possible during the next call to on()
        double millis = System.currentTimeMillis(); // double cuz Config can't do long
        _config.setValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_LAST_SHUTDOWN_TIME_IN_MILLIS, millis);
      }

      _powerOffCallTimer.stop();
      debug("LegacyXraySource.powerOff() timing: " + _powerOffUAOffTimer + ", " + _powerOffKVOffTimer
            + ", " + _powerOffCallTimer);
    }
    catch (XraySourceHardwareReportedErrorsException xshrex)
    {
      // suppress hardware errors if hardware is in the powerOff() state
      if (getCathodeCurrentInMicroAmps() > getMaxCathodeOffMicroAmps()) {
        throw xshrex;
      }
      if (getAnodeVoltageInKiloVolts() > getMaxAnodeOffKiloVolts()) {
        throw xshrex;
      }
    }
    finally
    {
      // 100% done
      // _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_POWER_OFF, 100);
	  // XCR1144, Chnee Khang Wah, 09-Dec-2010, Jabil board drop issue
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_OFF_COMPLETE, 100);

      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.POWER_OFF);
  }

  /**
   * NOTE: the top gun (XR) electronics do not have a revision code. if you spend
   *  a few focussed hours reviewing the 5DX codebase, you may think that the
   *  command 8V0 will return the "XR_REVCODE_R" measure.  however, at some point
   *  you will notice some confusing comments and a debug print statement that sez,
   *  "Xray controller firmware revision command timed out as it should!"  the 5DX
   *  codebase sends this command, but expects it to be ignored.  this code will,
   *  therefore, ignore it completely (except to say why it is being ignored).<p/>
   *
   * NOTE: doesn't check for hardware reported errors because diagnostic in nature
   *
   * @return the rev code returned from the high voltage power supply (HVPS)
   *
   * @author Greg Loring
   */
  public String getFirmwareRevisionCode() throws XrayTesterException
  {
    String result = _commandingLink.sendAndReceive("9V0");
    return result;
  }

  /**
   * ask the X-ray source hardware if it has experienced any errors<p/>
   *
   *   NOTE: the errors reported have been "latched" by the hardware.  this means the hardware
   *     is remembering them even if the actual condition has been resolved.  after all errors
   *     have been resolved (by hardware service staff, for example), call the related
   *     resetHardwareReportedErrors() method<p/>
   *
   * returns an XraySourceHardwareReportedErrorsException 'cuz that class has all the localized
   *   message handling: use getLocalizedMessage() or getMessage() to see the errors
   *
   * NOTE: methods that attempt to change the state of the hardware are "wrapped" in checks
   *   using this method.  methods that simply read the state of the hardware are not so wrapped
   *
   * @return XraySourceHardwareReportedErrorsException or null if no errors are being reported
   *
   * @see #resetHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public XraySourceHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException
  {
    XraySourceHardwareReportedErrorsException result = null;

    // read latched status
    int status = sendCommandAndReceiveResponseAsInteger("9S2");

    if (status != 0)
    {
      // reset optimization state
      _lastSetAnodeKiloVolts = Double.NaN;
      _lastSetFilamentVolts = Double.NaN;
      _lastSetFocusVolts = Double.NaN;

      // barrier interlocks are triggered all over the place and can be safely reset
      //  once the barrier(s) are closed again; if barrier(s) remain open, then resetting
      //  the latched error register won't clear reported interlock errors
      if ((status & ~0x0F) == 0) // is hardware only reporting interlock errors?
      {
        // only interlocks have been tripped, try resetting
        _commandingLink.sendAndReceive("9S3"); // reset latched status
        status = sendCommandAndReceiveResponseAsInteger("9S2");
      }

      if (status != 0)
      {
        result = new XraySourceHardwareReportedErrorsException(status);
      }
    }

    return result;
  }

  /**
   * ask the X-ray source hardware to clear its latched error register<p/>
   *
   * any errors detected by the hardware are "latched."  this means the hardware
   *   is remembering them even if the actual condition has been resolved.
   *   after all errors have been resolved (by hardware service staff, for example),
   *   call the related resetHardwareReportedErrors() method<p/>
   *
   * NOTE: doesn't check for hardware reported errors before clearing them, but
   *   does check immediately after
   *
   * @see #getHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public void resetHardwareReportedErrors() throws XrayTesterException
  {
    _commandingLink.sendAndReceive("9S3"); // ignore response (caller presumably already knows)
    checkForHardwareReportedErrors();
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("9I3", 4095, 165.0); // kV
    trace("getAnodeVoltageInKiloVolts() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public void setAnodeVoltageInKiloVolts(double value) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ANODE_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      checkForHardwareReportedErrors();
      unguardedSetAnodeVoltageInKiloVolts(value);
      checkForHardwareReportedErrors();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ANODE_VOLTAGE);
  }

  /**
   * @author Greg Loring
   */
  private void unguardedSetAnodeVoltageInKiloVolts(double value) throws XrayTesterException
  {
    standardSetHardwareValue(value, "9O0", 4095, 165.0); // kV; 9 'oh' 'zero'
    _lastSetAnodeKiloVolts = value;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getCathodeCurrentInMicroAmps() throws XrayTesterException
  {
    double result = standardReadHardwareValue("8I5", 1023, 200.0); // uA
    trace("getCathodeCurrentInMicroAmps() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public void setCathodeCurrentInMicroAmps(double value) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.CATHODE_CURRENT);
    _hardwareObservable.setEnabled(false);
    try
    {
      checkForHardwareReportedErrors();
      unguardedSetCathodeCurrentInMicroAmps(value);
      checkForHardwareReportedErrors();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.CATHODE_CURRENT);
  }

  /**
   * @author Greg Loring
   */
  private void unguardedSetCathodeCurrentInMicroAmps(double value) throws XrayTesterException
  {
    standardSetHardwareValue(value, "8A0", 1023, 200.0); // uA
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getFilamentVoltageInVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("8I0", 1023, 9.7); // V; > 6 won't happen
    trace("getFilamentVoltageInVolts() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public void setFilamentVoltageInVolts(double value) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.FILAMENT_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      checkForHardwareReportedErrors();
      unguardedSetFilamentVoltageInVolts(value);
      checkForHardwareReportedErrors();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.FILAMENT_VOLTAGE);
  }

  /**
   * @author Greg Loring
   */
  private void unguardedSetFilamentVoltageInVolts(double value) throws XrayTesterException
  {
    standardSetHardwareValue(value, "8P2", 1023, 9.7); // V; don't try > 6
    _lastSetFilamentVolts = value;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getFocusVoltageInVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("8I3", 1023, 3000.0); // V
    trace("getFocusVoltageInVolts() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public void setFocusVoltageInVolts(double value) throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.FOCUS_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      checkForHardwareReportedErrors();
      unguardedSetFocusVoltageInVolts(value);
      checkForHardwareReportedErrors();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.FOCUS_VOLTAGE);
  }

  /**
   * @author Greg Loring
   */
  private void unguardedSetFocusVoltageInVolts(double value) throws XrayTesterException
  {
    standardSetHardwareValue(value, "8P0", 255, 3000.0); // V
    _lastSetFocusVolts = value;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getGridVoltageInVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("8I2", 1023, -500.0); // V
    trace("getGridVoltageInVolts() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getGunTemperatureInDegreesCelcius() throws XrayTesterException
  {
    // the standard read doesn't work here because of the different int to double
    //  conversion (scale and offset vs. just plain scale)

    int cmdResult = sendCommandAndReceiveResponseAsInteger("8I6");

    // because the constants are doubles, cmdResult will be automatically promoted
    double result = (cmdResult * 2.33) - 275.0; // degrees C

    trace("getGunTemperatureInDegreesCelcius() returning " + result);
    return result;
  }

  /**
   * ...from the hardware, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getTotalCurrentInMicroAmps() throws XrayTesterException
  {
    double result = standardReadHardwareValue("9I2", 4095, 200.0); // uA
    trace("getTotalCurrentInMicroAmps() returning " + result);
    return result;
  }

  /**
   * ...from the hardware; the isolated power supply is in the top gun electronics
   *  and is used for the filament, focus, and grid voltages, see javadoc for class
   *
   * @author Greg Loring
   */
  public double getIsolatedSupplyVoltageInVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("8I1", 1023, 20.0); // V
    trace("getIsolatedSupplyVoltageInVolts() returning " + result);
    return result;
  }

  /**
   * the hardware actively monitors anode voltage keeping track of its min and max<p/>
   *
   * @author Greg Loring
   */
  double getMinimumAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("9M1", 4095, 165.0); // kV
    trace("getMinimumAnodeVoltageInKiloVolts() returning " + result);
    return result;
  }

  /**
   * the hardware actively monitors anode voltage keeping track of its min and max<p/>
   *
   * @author Greg Loring
   */
  double getMaximumAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    double result = standardReadHardwareValue("9M2", 4095, 165.0); // kV
    trace("getMaximumAnodeVoltageInKiloVolts() returning " + result);
    return result;
  }

  /**
   * @author Rex Shang
   */
  public static double getMaximumAllowableAnodeVoltageInKiloVolts()
  {
    return 165.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMinimumAllowableAnodeVoltageInKiloVolts()
  {
    return 0.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMaximumAllowableCathodeCurrentInMicroAmps()
  {
    return 125.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMinimumAllowableCathodeCurrentInMicroAmps()
  {
    return 0.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMaximumAllowableFilamentVoltageInVolts()
  {
    return 6.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMinimumAllowableFilamentVoltageInVolts()
  {
    return 0.0;
  }

  /**
   * The focus voltage has 2 ranges, we call them high end focus voltage
   * and low end focus voltage.  The next four methods will return the 2 ranges.
   * @author Rex Shang
   */
  public static double getMaximumAllowableHighRangeFocusVoltageInVolts()
  {
    return 2980.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMinimumAllowableHighRangeFocusVoltageInVolts()
  {
    return 2000.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMaximumAllowableLowRangeFocusVoltageInVolts()
  {
    return 1100.0;
  }

  /**
   * @author Rex Shang
   */
  public static double getMinimumAllowableLowRangeFocusVoltageInVolts()
  {
    return 600.0;
  }

  /**
   * the hardware actively monitors anode voltage keeping track of its min and max;
   *  this method causes the hardware to reset each to the current anode voltage;<p/>
   *
   * @author Greg Loring
   */
  void resetMinimumAndMaximumAnodeVoltage() throws XrayTesterException
  {
    checkForHardwareReportedErrors();
    standardReadHardwareValue("9M0", 4095, 165.0); // kV
    checkForHardwareReportedErrors();
  }

  /**
   * warm starts are allowed only if the X-ray source was previously
   *  powered off less than a configurable number of minutes ago
   * @author Greg Loring
   */
  public boolean isWarmStartAllowed()
  {
    long msDown = System.currentTimeMillis() - getLastShutdownTimeInMillis();
    int minutesDown = (int) (msDown / 60L / 1000L);
    boolean result = (minutesDown <= getMaxMinutesDownForWarmStart());
    trace("isWarmStartAllowed() returning " + result);
    return result;
  }

  /**
   * @author Greg Loring
   */
  private long getAnodeVoltageRampTimeInMilliSeconds() throws XrayTesterException
  {
    double minutes;
    if (isWarmStartAllowed() || isAnodeOn())
    {
      minutes = getWarmStartAnodeVoltageRampTimeInMinutes();
    }
    else
    {
      minutes = getColdStartAnodeVoltageRampTimeInMinutes();
    }

    double millis = minutes * 60.0 * 1000.0;

    Assert.expect(millis < Long.MAX_VALUE);
    long result = (long) millis;
    if (result < 1L)
    {
      result = 1L;
    }
    return result;
  }

  /**
   * the common problem with turning on X-ray tubes is arcing (see class comments).
   *  to avoid this, one has to slowly turn up the anode voltage on X-ray tubes.
   *  this method does the job of turning up the voltage slowly...<p/>
   *
   * methods that spin block (i.e., call sleep(...)) check the abort flag
   *
   * @author Greg Loring
   */
  private void rampUpAnodeVoltage(double anodeKiloVolts) throws XrayTesterException
  {
    long msRampTime = getAnodeVoltageRampTimeInMilliSeconds();

    // build a schedule of "how many kV to command when"
    LegacyXraySourceAnodeVoltageSchedule schedule
        = new LegacyXraySourceAnodeVoltageSchedule(anodeKiloVolts, msRampTime);

    // don't go back to 0kV if anode voltage is already partially up, go to next step
    double startingKV = getAnodeVoltageInKiloVolts();
    long startingMSTimeOffset = schedule.getTimeOffsetInMillis(startingKV)
                                + getAnodeVoltageRampCommandSeparationDelayInMillis();

    long startTime = System.currentTimeMillis() - startingMSTimeOffset;
    long lastProgressReportTime = 0;
    while (isAborting() == false)
    {
      long now = System.currentTimeMillis();

      // how long into ramp?
      long msTimeOffset = now - startTime;

      // time to quit?
      if ((msTimeOffset > msRampTime) || ((_simulateTiming == false) && isSimulationModeOn()))
      {
        // send one last "set anode voltage" command just be sure the final voltage is set
        setAnodeVoltageInKiloVolts(anodeKiloVolts); // only if NOT aborting

        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 90);

        return; // quit
      }

      // report progress once a second or so
      if ((now - lastProgressReportTime) > 1000)
      {
        int percentDone = 10 + (int) ((80 * msTimeOffset) / msRampTime);
        _progressObservable.reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, percentDone);
        lastProgressReportTime = now;
      }

      // set the voltage as appropriate to the current time
      double kVToCommand = schedule.getKiloVolts(msTimeOffset);
      setAnodeVoltageInKiloVolts(kVToCommand);

      // no sense in bothering the hardware all the time; let someone else use the machine
      sleep(getAnodeVoltageRampCommandSeparationDelayInMillis());
    }
  }

  /**
   * @author Greg Loring
   */
  private void checkForExcessiveLeakageCurrent() throws XrayTesterException
  {
    Assert.expect(isAnodeOn());

    double totalCurrent = getTotalCurrentInMicroAmps();
    double expectedTotalCurrent = getCathodeCurrentInMicroAmps() + getExpectedAnodeOnBleederMicroAmps();
    double leakageCurrent = totalCurrent - expectedTotalCurrent;
    double allowableLeakageCurrent = getAllowableLeakageCurrentInMicroAmps();

    // now check leakage current - allow either positive or negative, because finer grained
    //  negative check has consistently caused ignorable errors (false positives)
    if (Math.abs(leakageCurrent) > allowableLeakageCurrent)
    {
      _hardwareTaskEngine.throwHardwareException(XraySourceHardwareException.getLeakageCurrentExceededLimitException(allowableLeakageCurrent, leakageCurrent));
    }
  }

  /**
   * @return boolean true if anode is at full voltage (within tolerance)
   *
   * @author Greg Loring
   */
  private boolean isAnodeOn() throws XrayTesterException
  {
    double kV = getAnodeVoltageInKiloVolts();
    double expected = getTurnOnAnodeKiloVolts();
    double tolerance = getAnodeKiloVoltTolerance();
    boolean result = MathUtil.fuzzyEquals(kV, expected, tolerance);
    return result;
  }

  /**
   * @return boolean true if anode is off.
   * @author Rex Shang
   */
  public boolean isAnodeVoltageOff() throws XrayTesterException
  {
    double kV = getAnodeVoltageInKiloVolts();
    double expected = getTurnOffAnodeKiloVolts();
    double tolerance = getMaxAnodeOffKiloVolts();
    boolean result = MathUtil.fuzzyEquals(kV, expected, tolerance);
    return result;
  }

  /**
   * Check to see if cathode is at full current.
   * @author Rex Shang
   */
  private boolean isCathodeOn() throws XrayTesterException
  {
    double uA = getCathodeCurrentInMicroAmps();
    double expected = getTurnOnCathodeMicroAmps();
    double tolerance = getConfigCathodeMicroAmpTolerance();
    boolean result = MathUtil.fuzzyEquals(uA, expected, tolerance);

    return result;
  }

  /**
   * @author Greg Loring
   */
  private void waitForAnodeVoltageToStabilize(double anodeKiloVolts) throws XrayTesterException
  {
    waitForAnodeVoltageToStabilizeNear(anodeKiloVolts,
                                       getAnodeKiloVoltTolerance(),
                                       getAnodeVoltageStabilizationTimeoutSeconds(),
                                       0.0,
                                       ProgressReporterEnum.XRAY_SOURCE_ON);
  }

  /**
   * @author Greg Loring
   */
  private void waitForCathodeCurrentToStabilize(double cathodeMicroAmps) throws XrayTesterException
  {
    waitForCathodeCurrentToStabilizeNear(cathodeMicroAmps,
                                         getConfigCathodeMicroAmpTolerance(),
                                         getCathodeCurrentStabilizationTimeoutSeconds());
  }

  /**
   * @author Greg Loring
   */
  private void waitForCathodeCurrentOff() throws XrayTesterException
  {
    waitForCathodeCurrentToStabilizeNear(getTurnOffCathodeMicroAmps(),
                                         getMaxCathodeOffMicroAmps(),
                                         getCathodeCurrentStabilizationTimeoutSeconds());
  }

  /**
   * @author Greg Loring
   */
  private void waitForAnodeVoltageOff() throws XrayTesterException
  {
    waitForAnodeVoltageToStabilizeNear(getTurnOffAnodeKiloVolts(),
                                       getMaxAnodeOffKiloVolts(),
                                       getAnodeVoltageStabilizationTimeoutSeconds(),
                                       getTurnOnAnodeKiloVolts(),
                                       ProgressReporterEnum.XRAY_SOURCE_POWER_OFF);
  }

  /**
   * methods that spin block (i.e., call sleep(...)) check the abort flag
   *
   * @author Greg Loring
   */
  private void waitForAnodeVoltageToStabilizeNear(double requiredVoltage,
                                                  double tolerance,
                                                  double timeoutSeconds,
                                                  double reportProgressFrom,
                                                  ProgressReporterEnum activity) throws XrayTesterException
  {
    if ((_simulateTiming == false) && isSimulationModeOn())
      return;  // avoid sleep for getStabilizationReadSeparationDelayInMillis() (currently 500ms)

    // don't want to exit the loop with only one reading: if the voltage is bouncing
    //  around, it might get caught at the required value for one reading. therefore,
    //  initialize lastVoltage to any value that won't let first check pass
    double lastVoltage = requiredVoltage - (2.0 * tolerance);

    // while there is still time left...
    long counter = 0;
    long timeoutCount = (long) Math.ceil(timeoutSeconds * 1000.0 / getStabilizationReadSeparationDelayInMillis());
    while (isAborting() == false)
    {
      // check the elapsed time
      if (counter >= timeoutCount)
        _hardwareTaskEngine.throwHardwareException(new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_XRAY_SOURCE));

      // read the current voltage from the hardware
      double currentVoltage = getAnodeVoltageInKiloVolts();

      // is the current voltage tolerably equal to both the required and most recent voltages?
      if (MathUtil.fuzzyEquals(currentVoltage, requiredVoltage, tolerance)
          && MathUtil.fuzzyEquals(currentVoltage, lastVoltage, tolerance))
      {
        _progressObservable.reportProgress(activity, 95);
        return; // it's done equalized!
      }
      // otherwise...

      int pctDone = 90 + (int)(5.0 * Math.abs((currentVoltage - reportProgressFrom) / (requiredVoltage - reportProgressFrom)));
      Assert.expect(pctDone >= 0);
      if (pctDone <= 95)
      {
        _progressObservable.reportProgress(activity, pctDone);
      }

      // wait a while before trying again: want to be sure the measure has settled down,
      //  not find out that measurements can be taken so close together that the measure
      //  has no chance to move
      sleep(getStabilizationReadSeparationDelayInMillis());

      // don't forget to "slide" the voltage
      lastVoltage = currentVoltage;

      counter++;
    }
  }

  /**
   * methods that spin block (i.e., call sleep(...)) check the abort flag
   *
   * @author Greg Loring
   */
  private void waitForCathodeCurrentToStabilizeNear(double requiredCurrent,
                                                    double tolerance,
                                                    double timeoutSeconds) throws XrayTesterException
  {
    if ((_simulateTiming == false) && isSimulationModeOn())
      return;  // avoid sleep for getStabilizationReadSeparationDelayInMillis() (currently 500ms)

    // don't want to exit the loop with only one reading: if the current is bouncing
    //  around, it might get caught at the required value for one reading. therefore,
    //  initialize lastCurrent to any value that won't let first check pass
    double lastCurrent = requiredCurrent - (2.0 * tolerance);

    // while there is still time left...
    long counter = 0;
    long timeoutCount = (long) Math.ceil(timeoutSeconds * 1000.0 / getStabilizationReadSeparationDelayInMillis());
    while (isAborting() == false)
    {
      // check the elapsed time
      if (counter >= timeoutCount)
        _hardwareTaskEngine.throwHardwareException(new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_XRAY_SOURCE));

      // read the current current (the current as of now) from the hardware
      double currentCurrent = getCathodeCurrentInMicroAmps();

      // is the current current tolerably equal to both the required and most recent currents?
      if (MathUtil.fuzzyEquals(currentCurrent, requiredCurrent, tolerance)
          && MathUtil.fuzzyEquals(currentCurrent, lastCurrent, tolerance))
        return; // it's done equalized!
      // otherwise...

      // wait a while before trying again: want to be sure the current has settled down,
      //  not find out that measurements can be taken so close together that the current
      //  has no chance to move
      sleep(getStabilizationReadSeparationDelayInMillis());

      // don't forget to "slide" the current
      lastCurrent = currentCurrent;

      counter++;
    }
  }

  /**
   * "logging" that is "easy" to turn on and off
   *
   * @author Greg Loring
   */
  private void trace(Object o)
  {
    // turn trace logging off (on) by commenting (uncommenting) out the next line
    //debug(o);
  }

  /**
   * "logging" that is "easy" to turn on and off
   *
   * @author Greg Loring
   */
  private void debug(Object o)
  {
    // the debug output includes timing information which changes from run to run;
    //  furthermore, this class is used all over, so debug output would show up in oodles
    //  of Test_....out files; therefore, no debug output during unit testing
    if (_simulateTiming || _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      // turn debug logging off (on) by commenting (uncommenting) out the next line
      System.out.println(_me + ": " + o);
    }
  }

  /**
   * at various points, the code needs to sleep for a bit.  wrap up exception handling here
   * @author Greg Loring
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  /**
   * separated out for use by the odd-ball read command(s) that don't follow the standard pattern
   *
   * @author Greg Loring
   */
  private int sendCommandAndReceiveResponseAsInteger(String command) throws XrayTesterException
  {
    String responseString = _commandingLink.sendAndReceive(command);
    int response = -1;
    try
    {
      response = Integer.parseInt(responseString);
    }
    catch (NumberFormatException nfx)
    {
      try
      {
        responseString = _commandingLink.sendAndReceive(command);
        response = Integer.parseInt(responseString);
      }
      catch (NumberFormatException nfx2)
      {
        _hardwareTaskEngine.throwHardwareException(new SerialCommunicationProtocolHardwareException(getSerialPortName(),
                                                                                                    "{4-digits}", // expected
                                                                                                    responseString)); // actual
      }
    }
    return response;
  }

  /**
   * most read commands differ only by command string and scale
   *
   * @author Greg Loring
   */
  private double standardReadHardwareValue(String command,
                                           int cmdScale,
                                           double unitScale) throws XrayTesterException
  {
    Assert.expect((cmdScale >= 1) && (cmdScale <= 9999));
    Assert.expect((unitScale != 0.0));

    int cmdResult = sendCommandAndReceiveResponseAsInteger(command);

    // because unitScale is a double, cmdScale and then cmdResult will be automatically promoted
    double result = cmdResult * (unitScale / cmdScale);
    return result;
  }

  /**
   * currently, set commands differ only by command string and scale
   *
   * @author Greg Loring
   */
  private void standardSetHardwareValue(double unitValue,
                                        String command,
                                        int cmdScale,
                                        double unitScale) throws XrayTesterException
  {
    Assert.expect((cmdScale >= 1) && (cmdScale <= 9999));
    Assert.expect((unitScale != 0.0));
    Assert.expect((unitValue / unitScale) >= 0.0); // same sign
    Assert.expect(Math.abs(unitValue) <= Math.abs(unitScale)); // in range [0, unitScale)

    // compute and sanity check scaled value
    double scaledValue = unitValue * (cmdScale / unitScale);
    Assert.expect(scaledValue >= 0);
    Assert.expect(scaledValue <= cmdScale);

    // convert scaled double value into an int by rounding
    int cmdValue = (int) Math.round(scaledValue);

    // convert cmdValue into a 4-digit, 0-padded string
    String cmdValueString = Integer.toString(10000 + cmdValue).substring(1);

    // send complete command chunk and receive response
    String request = command + "," + cmdValueString;
    String response = _commandingLink.sendAndReceive(request);

    // verify response: hardware should echo commanded value
    if (!response.equals(cmdValueString))
    {
      _hardwareTaskEngine.throwHardwareException(new SerialCommunicationProtocolHardwareException(getSerialPortName(),
                                                                                                  cmdValueString, // expected
                                                                                                  response)); // actual
    }
  }
  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public boolean getUsesXrayAutoShortTermShutdownEnabled()
  {
    return _xrayAutoShortTermShutdownTimerEnabled;
  }

  /**
  * @author AnthonyFong
  */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public double getXrayAutoShortTermShutdownTimeoutMinutes()
  {
    Assert.expect(_xrayAutoShortTermShutdownTimeoutMinutes >= 0);
    return _xrayAutoShortTermShutdownTimeoutMinutes;
  }

  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public synchronized void setUsesXrayAutoShortTermShutdownEnabled(boolean flag) throws XrayTesterException
  {
    _xrayAutoShortTermShutdownTimerEnabled = flag;
    _config.setValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED, flag);
  }

  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray-AnthonyFong 19-March-2009
  public synchronized void setXrayAutoShortTermShutdownTimeoutMinutes(double newMax) throws XrayTesterException
  {
    Assert.expect(newMax >= 0);
    _xrayAutoShortTermShutdownTimeoutMinutes = newMax;
    _config.setValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES, newMax);
  }
  
  /*
   * Chin-Seong, Kee - Set Virtual Live XrayTube Voltage And Current
   */
  public void setXraySourceParametersForServiceMode(double voltage, double current) throws XrayTesterException
  {
     setAnodeVoltageInKiloVolts(voltage);
     setCathodeCurrentInMicroAmps(current);
  }
}
