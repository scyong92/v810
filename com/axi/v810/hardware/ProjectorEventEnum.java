package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ProjectorEventEnum extends HardwareEventEnum
{
    private static int _index = -1;

    public static ProjectorEventEnum STARTUP = new ProjectorEventEnum(++_index, "Projector, Startup");
    public static ProjectorEventEnum INITIALIZE = new ProjectorEventEnum(++_index, "Projector, Initialize");

    private String _name;

    /**
     * @author Cheah Lee Herng
    */
    private ProjectorEventEnum(int id, String name)
    {
        super(id);
        Assert.expect(name != null);

        _name = name;
    }

    /**
    * @author Cheah Lee Herng
    */
    public String toString()
    {
        return _name;
    }
}