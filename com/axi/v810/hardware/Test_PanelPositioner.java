package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
class Test_PanelPositioner extends UnitTest
{
  private static final String _EMPTY_STRING = "";
  private PanelPositioner _panelPositioner;
  private HardwareWorkerThread _hardwareWorkerThread;
  private String[] _args;
  private boolean _debugMode;
  private Config _config;


  /**
   * @author Greg Esparza
   */
  public Test_PanelPositioner(String[] args)
  {
    _args = args;
    _panelPositioner = PanelPositioner.getInstance();
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _debugMode = false;
    _config = Config.getInstance();

    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

 /**
  * @author Greg Esparza
  */
 private void getKeyboardCommand(String commandMessage) throws IOException
 {
   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
   String keyboardString = "";

   do
   {
     System.out.println();
     System.out.println(commandMessage);

     keyboardString = br.readLine();
     if (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false)
     {
       System.out.println("Invalid key!");
     }
   }
   while (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false);
 }

  /**
   * @author Greg Esparza
   */
  private void setDebugMode() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-debugMode"))
      {
        argFound = true;
        _debugMode = true;
        break;
      }
    }

    if (argFound == false)
      _debugMode = false;
  }

  /**
   * @author Greg Esparza
   */
  private void checkArgs()
  {
    if (_args != null)
    {
      try
      {
         setDebugMode();
      }
      catch (HardwareException he)
      {
        System.out.println(he);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  private void testStartup() throws XrayTesterException
  {
    _panelPositioner.startup();
    Expect.expect(_panelPositioner.isStartupRequired() == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testShutdown() throws XrayTesterException
  {
    _panelPositioner.shutdown();
    Expect.expect(_panelPositioner.isStartupRequired());
  }

  /**
   * @author Greg Esparza
   */
  private void testEnableAxis() throws XrayTesterException
  {
    _panelPositioner.enableXaxis();
    Expect.expect(_panelPositioner.isXaxisEnabled());

    _panelPositioner.enableYaxis();
    Expect.expect(_panelPositioner.isYaxisEnabled());

    _panelPositioner.disableAllAxes();
    Expect.expect(_panelPositioner.areAllAxesEnabled() == false);

    _panelPositioner.enableAllAxes();
    Expect.expect(_panelPositioner.areAllAxesEnabled());
  }

  /**
   * @author Greg Esparza
   */
  private void testDisableAxis() throws XrayTesterException
  {
    _panelPositioner.disableXaxis();
    Expect.expect(_panelPositioner.isXaxisEnabled() == false);

    _panelPositioner.disableYaxis();
    Expect.expect(_panelPositioner.isYaxisEnabled() == false);

    _panelPositioner.enableAllAxes();
    Expect.expect(_panelPositioner.areAllAxesEnabled());

    _panelPositioner.disableAllAxes();
    Expect.expect(_panelPositioner.areAllAxesEnabled() == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testHomeAxis() throws XrayTesterException
  {
    _panelPositioner.homeXaxis();
    _panelPositioner.homeYaxis();
    _panelPositioner.homeAllAxes();
  }

  /**
   * @author Greg Esparza
   */
  private void testGetAxisPositionLimitsInNanometers() throws XrayTesterException, BadFormatException
  {
    int limit = _panelPositioner.getXaxisMinimumPositionLimitInNanometers();
    int configValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.X_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configValue);

    limit = _panelPositioner.getXaxisMaximumPositionLimitInNanometers();
    configValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configValue);

    limit = _panelPositioner.getYaxisMinimumPositionLimitInNanometers();
    configValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.Y_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configValue);

    limit = _panelPositioner.getYaxisMaximumPositionLimitInNanometers();
    configValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.Y_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configValue);
  }

  /**
   * @author Greg Esparza
   */
  private void testPointToPointMove() throws XrayTesterException
  {
    int xAxisPositionInNanometers = 25400000;
    int yAxisPositionInNanometers = 25400000;
    StagePositionMutable stagePosition = new StagePositionMutable();
    stagePosition.setXInNanometers(xAxisPositionInNanometers);
    stagePosition.setYInNanometers(yAxisPositionInNanometers);
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);
    Expect.expect(_panelPositioner.getXaxisActualPositionInNanometers() == xAxisPositionInNanometers);
    Expect.expect(_panelPositioner.getYaxisActualPositionInNanometers() == yAxisPositionInNanometers);

    // Try and move the X-axis out of bounds
    try
    {
      stagePosition.setXInNanometers(_panelPositioner.getXaxisMaximumPositionLimitInNanometers() + 1);
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // An exception should be thrown because we tried to move out of bounds
    }

    try
    {
      stagePosition.setXInNanometers(_panelPositioner.getXaxisMinimumPositionLimitInNanometers() - 1);
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // An exception should be thrown because we tried to move out of bounds
    }

    // Try and move the Y-axis out of bounds
    try
    {
      stagePosition.setYInNanometers(_panelPositioner.getYaxisMaximumPositionLimitInNanometers() + 1);
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // An exception should be thrown because we tried to move out of bounds
    }

    try
    {
      stagePosition.setYInNanometers(_panelPositioner.getYaxisMinimumPositionLimitInNanometers() - 1);
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // An exception should be thrown because we tried to move out of bounds
    }
  }

  /**
   * @author Greg Esparza
   */
  private void enableScanPath(int xAxisStartPositionInNanometers,
                              int yAxisStartPositionInNanometers,
                              int scanPassLengthInNanometers,
                              int scanStepWidthInNanometers,
                              int numberOfScanPasses,
                              ScanPassDirectionEnum firstScanPassDirection,
                              ScanStepDirectionEnum scanStepDirection) throws XrayTesterException
  {
    // We need to set the profile before we can calculate what the scan pass length limits are
    _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));

    Assert.expect(xAxisStartPositionInNanometers >= _panelPositioner.getXaxisMinimumPositionLimitInNanometers());
    Assert.expect(xAxisStartPositionInNanometers <= _panelPositioner.getXaxisMaximumPositionLimitInNanometers());
    Assert.expect(yAxisStartPositionInNanometers >= _panelPositioner.getYaxisMinimumPositionLimitInNanometers());
    Assert.expect(yAxisStartPositionInNanometers <= _panelPositioner.getYaxisMaximumPositionLimitInNanometers());
    Assert.expect(numberOfScanPasses > 0);
    Assert.expect(firstScanPassDirection != null);
    Assert.expect(scanStepDirection != null);
    Assert.expect(scanPassLengthInNanometers >= _panelPositioner.getMinimumScanPassLengthInNanometers());
    Assert.expect(scanPassLengthInNanometers <= _panelPositioner.getMaximumScanPassLengthInNanometers(yAxisStartPositionInNanometers, firstScanPassDirection));
    Assert.expect(scanStepWidthInNanometers >= _panelPositioner.getMinimumScanStepWidthInNanometers());
    Assert.expect(scanStepWidthInNanometers <= _panelPositioner.getMaximumScanStepWidthInNanometers(xAxisStartPositionInNanometers, scanStepDirection));


    StagePositionMutable startPosition = null;
    StagePositionMutable endPosition = null;
    ScanPass currentScanPass = null;
    List<ScanPass> scanPath = new LinkedList<ScanPass>();

    int xStartScanPassInNanometers = xAxisStartPositionInNanometers;
    int yStartScanPassInNanometers = yAxisStartPositionInNanometers;
    int xEndScanPassInNanometers = xAxisStartPositionInNanometers;
    int yEndScanPassInNanometers = yAxisStartPositionInNanometers + (firstScanPassDirection.getId() *  scanPassLengthInNanometers);


    for (int i = 0; i < numberOfScanPasses; ++i)
    {
      startPosition = new StagePositionMutable();
      endPosition = new StagePositionMutable();

      if ((i % 2) == 0)
      {
        startPosition.setXInNanometers(xStartScanPassInNanometers + (scanStepDirection.getId() *  (i * scanStepWidthInNanometers)));
        startPosition.setYInNanometers(yStartScanPassInNanometers);
        endPosition.setXInNanometers(xEndScanPassInNanometers + (scanStepDirection.getId() * (i * scanStepWidthInNanometers)));
        endPosition.setYInNanometers(yEndScanPassInNanometers);
      }
      else
      {
        startPosition.setXInNanometers(xStartScanPassInNanometers + (scanStepDirection.getId() * (i * scanStepWidthInNanometers)));
        startPosition.setYInNanometers(yEndScanPassInNanometers);
        endPosition.setXInNanometers(xEndScanPassInNanometers + (scanStepDirection.getId() * (i * scanStepWidthInNanometers)));
        endPosition.setYInNanometers(yStartScanPassInNanometers);
      }

      currentScanPass = new ScanPass(i, startPosition, endPosition);
      scanPath.add(currentScanPass);

      currentScanPass = null;
      startPosition = null;
      endPosition = null;
    }

    // Swee Yee Wong - Include starting pulse information for scan pass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    _panelPositioner.enableScanPath(scanPath, 19000, 1250);
  }

  /**
   * @author Greg Esparza
   */
  private void runAllPausedScanPasses(int numberOfScanPasses) throws XrayTesterException
  {
    for (int i = 0; i < numberOfScanPasses; ++i)
    {
      _panelPositioner.runNextScanPass();
      _panelPositioner.pauseScanPath();
      while (_panelPositioner.isScanPassDone() == false);
    }

    while (_panelPositioner.isScanPathDone() == false);
  }

  /**
   * @author Greg Esparza
   */
  private void runAllScanPasses(int numberOfScanPasses) throws XrayTesterException
  {
    for (int i = 0; i < numberOfScanPasses; ++i)
    {
      _panelPositioner.runNextScanPass();
      while (_panelPositioner.isScanPassDone() == false);
    }

    while (_panelPositioner.isScanPathDone() == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testScanMove() throws XrayTesterException
  {
    _panelPositioner.startup();

    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 1 inch
    // First scan pass direction = forward
    // Scan step direction = forward
    int xAxisStartPositionInNanometers = 25400000;
    int yAxisStartPositionInNanometers = 25400000;
    int scanPassLengthInNanometers = 50800000;
    int scanStepWidthInNanometers = 25400000;
    int numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    runAllPausedScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 1 inch
    // Y start = 3 inch
    // Length = 2 inch
    // Step = 1 inch
    // First scan pass direction = forward
    // Scan step direction = reverse
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 76200000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 25400000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.REVERSE,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 4 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 1 inch
    // First scan pass direction = forward
    // Scan step direction = reverse
    xAxisStartPositionInNanometers = 101600000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 25400000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.REVERSE);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 4 inch
    // Y start = 3 inch
    // Length = 2 inch
    // Step = 1 inch
    // First scan pass direction = forward
    // Scan step direction = reverse
    xAxisStartPositionInNanometers = 101600000;
    yAxisStartPositionInNanometers = 76200000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 25400000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.REVERSE);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();


    // ------------------------------------------------------
    //  Test the following special step width cases:
    //  1) step <= 25 mm
    //  2) step > 25 mm AND step <= 40 mm
    //  3) step > 40 mm
    //
    // Note: these step sizes will require special calculations
    // internal to the native motion control code.
    // ------------------------------------------------------
    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 0.94488188 inch = 24 mm = 24000000 nm
    // First scan pass direction = forward
    // Scan step direction = forward
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 2400000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 0.98425196 inch = 25 mm = 25000000 nm
    // First scan pass direction = forward
    // Scan step direction = forward
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 2500000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 1.37795275 inch = 35 mm = 35000000 nm
    // First scan pass direction = forward
    // Scan step direction = forward
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 3500000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 1.57480314 inch = 40 mm = 40000000 nm
    // First scan pass direction = forward
    // Scan step direction = forward
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 4000000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();

    // X start = 1 inch
    // Y start = 1 inch
    // Length = 2 inch
    // Step = 1.968503937 inch = 50 mm = 50000000 nm
    // First scan pass direction = forward
    // Scan step direction = forward
    xAxisStartPositionInNanometers = 25400000;
    yAxisStartPositionInNanometers = 25400000;
    scanPassLengthInNanometers = 50800000;
    scanStepWidthInNanometers = 5000000;
    numberOfScanPasses = 4;

    enableScanPath(xAxisStartPositionInNanometers,
                   yAxisStartPositionInNanometers,
                   scanPassLengthInNanometers,
                   scanStepWidthInNanometers,
                   numberOfScanPasses,
                   ScanPassDirectionEnum.FORWARD,
                   ScanStepDirectionEnum.FORWARD);

    Expect.expect(_panelPositioner.getScanPathExecutionTimeInMilliseconds() > 0);
    runAllScanPasses(numberOfScanPasses);
    _panelPositioner.disableScanPath();
  }

  /**
   * @author Greg Esparza
   */
  private void testGetActiveMotionProfile() throws XrayTesterException
  {
    // Set axes for point-to-point profile 0
    MotionProfile setMotionProfile = new MotionProfile(PointToPointMotionProfileEnum.PROFILE0);
    _panelPositioner.setMotionProfile(setMotionProfile);
    MotionProfile activeMotionProfile = _panelPositioner.getActiveMotionProfile();
    Expect.expect(activeMotionProfile.getPointToPointMotionProfile().equals(setMotionProfile.getPointToPointMotionProfile()));

    // Set axes for point-to-point profile 1
    setMotionProfile = new MotionProfile(PointToPointMotionProfileEnum.PROFILE1);
    _panelPositioner.setMotionProfile(setMotionProfile);
    activeMotionProfile = _panelPositioner.getActiveMotionProfile();
    Expect.expect(activeMotionProfile.getPointToPointMotionProfile().equals(setMotionProfile.getPointToPointMotionProfile()));

    // Set axes for point-to-point profile 2
    setMotionProfile = new MotionProfile(PointToPointMotionProfileEnum.PROFILE2);
    _panelPositioner.setMotionProfile(setMotionProfile);
    activeMotionProfile = _panelPositioner.getActiveMotionProfile();
    Expect.expect(activeMotionProfile.getPointToPointMotionProfile().equals(setMotionProfile.getPointToPointMotionProfile()));

    // Set axes for point-to-point profile 3
    setMotionProfile = new MotionProfile(PointToPointMotionProfileEnum.PROFILE3);
    _panelPositioner.setMotionProfile(setMotionProfile);
    activeMotionProfile = _panelPositioner.getActiveMotionProfile();
    Expect.expect(activeMotionProfile.getPointToPointMotionProfile().equals(setMotionProfile.getPointToPointMotionProfile()));

    // Set axes for scan profile 0
    setMotionProfile = new MotionProfile(ScanMotionProfileEnum.PROFILE0);
    _panelPositioner.setMotionProfile(setMotionProfile);
    activeMotionProfile = _panelPositioner.getActiveMotionProfile();
    Expect.expect(activeMotionProfile.getScanMotionProfile().equals(setMotionProfile.getScanMotionProfile()));
  }

  /**
   * @author Greg Esparza
   */
  private void testSetHysteresisOffsetInNanometers() throws XrayTesterException
  {
    int xAxisHysteresisOffsetInNanometers = 16000;
    int yAxisHysteresisOffsetInNanometers = 16000;

    _panelPositioner.setXaxisHysteresisOffsetInNanometers(xAxisHysteresisOffsetInNanometers);
    _panelPositioner.setYaxisHysteresisOffsetInNanometers(yAxisHysteresisOffsetInNanometers);
    Expect.expect(yAxisHysteresisOffsetInNanometers == _panelPositioner.getYaxisForwardDirectionHysteresisOffsetInNanometers());
    Expect.expect((-1 * yAxisHysteresisOffsetInNanometers) == _panelPositioner.getYaxisReverseDirectionHysteresisOffsetInNanometers());

    // Home all axes first
    _panelPositioner.homeAllAxes();

    // Move out 2 inches = 50 800 000 nm
    int xAxisPositionInNanometers = 50800000;
    int yAxisPositionInNanometers = 50800000;
    StagePositionMutable stagePosition = new StagePositionMutable();
    stagePosition.setXInNanometers(xAxisPositionInNanometers);
    stagePosition.setYInNanometers(yAxisPositionInNanometers);
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);

    // When we move each axis in the forward direction (i.e. target position > current position)
    // the actual position == commanded position
    Expect.expect(_panelPositioner.getXaxisActualPositionInNanometers() == xAxisPositionInNanometers);
    Expect.expect(_panelPositioner.getYaxisActualPositionInNanometers() == yAxisPositionInNanometers);

    // Move back to 1 inch = 24 400 000 nm
    xAxisPositionInNanometers = 25400000;
    yAxisPositionInNanometers = 25400000;
    stagePosition = new StagePositionMutable();
    stagePosition.setXInNanometers(xAxisPositionInNanometers);
    stagePosition.setYInNanometers(yAxisPositionInNanometers);
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);

    // When we move each axis in the forward direction (i.e. target position > current position)
    // the actual position == commanded position
    Expect.expect(_panelPositioner.getXaxisActualPositionInNanometers() == (xAxisPositionInNanometers - xAxisHysteresisOffsetInNanometers));
    Expect.expect(_panelPositioner.getYaxisActualPositionInNanometers() == (yAxisPositionInNanometers - yAxisHysteresisOffsetInNanometers));
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMinimumScanPassLengthInNanometers() throws XrayTesterException
  {
    _panelPositioner.startup();
    _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
    Expect.expect(_panelPositioner.getMinimumScanPassLengthInNanometers() > 0);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMaximumScanPassLengthInNanometers() throws XrayTesterException
  {
    _panelPositioner.startup();
    _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
    int yAxisScanStartPositionInNanometers = _panelPositioner.getMinimumScanPassLengthInNanometers();
    ScanPassDirectionEnum direction = ScanPassDirectionEnum.FORWARD;
    Expect.expect(_panelPositioner.getMaximumScanPassLengthInNanometers(yAxisScanStartPositionInNanometers, direction) > 0);

    yAxisScanStartPositionInNanometers = _panelPositioner.getYaxisMaximumPositionLimitInNanometers();
    direction = ScanPassDirectionEnum.FORWARD;
    Expect.expect(_panelPositioner.getMaximumScanPassLengthInNanometers(yAxisScanStartPositionInNanometers, direction) == 0);

    yAxisScanStartPositionInNanometers = _panelPositioner.getYaxisMinimumPositionLimitInNanometers();
    direction = ScanPassDirectionEnum.REVERSE;
    Expect.expect(_panelPositioner.getMaximumScanPassLengthInNanometers(yAxisScanStartPositionInNanometers, direction) == 0);

    yAxisScanStartPositionInNanometers = _panelPositioner.getYaxisMaximumPositionLimitInNanometers();
    direction = ScanPassDirectionEnum.REVERSE;
    Expect.expect(_panelPositioner.getMaximumScanPassLengthInNanometers(yAxisScanStartPositionInNanometers, direction) > 0);
 }

  /**
   * @author Greg Esparza
   */
  private void testGetValidPositionPulseRateInNanometersPerPulse() throws XrayTesterException
  {
    _panelPositioner.startup();

    int nanometersPerPulse = 16000;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == nanometersPerPulse);

    nanometersPerPulse = 16250;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == 16500);

    nanometersPerPulse = 16500;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == nanometersPerPulse);

    nanometersPerPulse = 16750;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == 17000);

    nanometersPerPulse = 17000;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == nanometersPerPulse);

    nanometersPerPulse = 17250;
    Expect.expect(_panelPositioner.getValidPositionPulseRateInNanometersPerPulse(nanometersPerPulse) == 17500);
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException, IOException, BadFormatException
        {
          checkArgs();

          if (_debugMode)
            getKeyboardCommand("DEBUG MODE - Press <ENTER> when ready to start debug session ...");

          // We will change configuration file during test...
          TestUtils.initConfigForRegressionTest();

          testStartup();
          testShutdown();
          testStartup();
          testEnableAxis();
          testDisableAxis();
          testHomeAxis();
          testGetAxisPositionLimitsInNanometers();
          testGetActiveMotionProfile();
          testPointToPointMove();
          testScanMove();
          testSetHysteresisOffsetInNanometers();
          testGetMinimumScanPassLengthInNanometers();
          testGetMaximumScanPassLengthInNanometers();
          testGetValidPositionPulseRateInNanometersPerPulse();
        }
      });
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
    finally
    {
      TestUtils.restoreConfigAfterRegressionTest();
    }
  }

  /**
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PanelPositioner(args));
  }
}
