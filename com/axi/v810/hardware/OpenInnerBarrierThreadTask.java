package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class OpenInnerBarrierThreadTask extends ThreadTask<Object>
{
  private InnerBarrier _innerBarrier;

  /**
   * @author Rex Shang
   */
  OpenInnerBarrierThreadTask()
  {
    super("Open Inner Barrier Thead Task");

    _innerBarrier = InnerBarrier.getInstance();
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    if (_innerBarrier.isInstalled() && _innerBarrier.isOpen() == false)
    {
      _innerBarrier.open();
    }

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
