package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.StringLocalizer;

/**
 * @author Bob Balliew
 */
public class Test_BarcodeReader extends UnitTest
{
  /**
   * @author Bob Balliew
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_BarcodeReader());
  }

  /**
   * @author Bob Balliew
   */
  private String getRs232DataBits()
  {
    return Config.getInstance().getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS);
  }

  /**
   * @author Bob Balliew
   */
  private void setRs232DataBits(String dataBits)
  {
    try
    {
      Config.getInstance().setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS, dataBits);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }

  /**
   * @author Bob Balliew
   */
  private String getTriggerCode(BarcodeReaderEnum bcr)
  {
    return Config.getInstance().getStringValue(bcr.getTrigger());
  }

  /**
   * @author Bob Balliew
   */
  private void setTriggerCode(BarcodeReaderEnum bcr, String triggerCode)
  {
    try
    {
      Config.getInstance().setValueInMemoryOnly(bcr.getTrigger(), triggerCode);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }
  }

  /**
   * @author Bob Balliew
   */
  private String getPreamble(BarcodeReaderEnum bcr)
  {
    return Config.getInstance().getStringValue(bcr.getPreamble());
  }

  /**
   * @author Bob Balliew
   */
  private void setPreamble(BarcodeReaderEnum bcr, String preamble)
  {
    try
    {
      Config.getInstance().setValueInMemoryOnly(bcr.getPreamble(), preamble);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }
  }

  /**
   * @author Bob Balliew
   */
  private String getPostamble(BarcodeReaderEnum bcr)
  {
    return Config.getInstance().getStringValue(bcr.getPostamble());
  }

  /**
   * @author Bob Balliew
   */
  private void setPostamble(BarcodeReaderEnum bcr, String postamble)
  {
    try
    {
      Config.getInstance().setValueInMemoryOnly(bcr.getPostamble(), postamble);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }
  }

  /**
   * @author Bob Balliew
   */
  private void test_trigger(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {

    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);

    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    try
    {
      scanner.trigger(null);
      Expect.expect(false, "Expected an assert" + " " + bcr.toString());
    }
    catch (AssertException ae)
    {
      // Expect control to flow through here.
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    OutputStreamWriter w = new OutputStreamWriter(out);

    try
    {
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending first trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(null);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending first trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(null);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(null);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending second trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending second trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(null);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.trigger(w);
      Expect.expect(out.size() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending second trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending first trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 3);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
        Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while sending second trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      scanner.simulationModeAddTriggerException(new InterruptedIOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
      }
      catch (InterruptedIOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new SyncFailedException("Simulate SyncFailedException thrown while sending trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an SyncFailedHardwareException" + " " + bcr.toString());
      }
      catch (SyncFailedHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_SYNC_FAILED_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate SyncFailedException thrown while sending trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new SyncFailedException(null));  // SyncFailedException() NOT in API?
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an SyncFailedHardwareException" + " " + bcr.toString());
      }
      catch (SyncFailedHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_SYNC_FAILED_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 1);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new UnsupportedEncodingException("Simulate UnsupportedEncodingException thrown while sending trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an UnsupportedEncodingHardwareException" + " " + bcr.toString());
      }
      catch (UnsupportedEncodingHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_UNSUPPORTED_ENCODING_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate UnsupportedEncodingException thrown while sending trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new UnsupportedEncodingException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an UnsupportedEncodingHardwareException" + " " + bcr.toString());
      }
      catch (UnsupportedEncodingHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_UNSUPPORTED_ENCODING_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 1);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new IOException("Simulate IOException thrown while sending trigger."));
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an IOHardwareException" + " " + bcr.toString());
      }
      catch (IOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_IO_WITH_MESSAGE_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 2);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
        Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate IOException thrown while sending trigger."));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    try
    {
      scanner.simulationModeAddTriggerException(new IOException());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      try
      {
        scanner.trigger(w);
        Expect.expect(false, "Expected an IOHardwareException" + " " + bcr.toString());
      }
      catch (IOHardwareException ee)
      {
        Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_IO_EXCEPTION_KEY"));
        Expect.expect(ee.getLocalizedString().getArguments().length == 1);
        Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      }
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }

    String cacheDataBits = getRs232DataBits();
    setRs232DataBits("8");

    for (int c = 0x00; c <= 0xFF; ++c)
    {
      try
      {
        final String triggerCode = "" + (char)c;
        setTriggerCode(bcr, triggerCode);
        scanner.trigger(w);
        Expect.expect(out.size() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.getId().equals(bcr), bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      }
      catch (HardwareException e)
      {
        Expect.expect(false, e.getMessage() + " " + bcr.toString());
      }
    }

    {
      StringBuffer buf = new StringBuffer();

      for (int i = 0; i < 32; ++i)
        for (int c = 0x00; c <= 0xFF; ++c)
          buf.append((char)c);

      try
      {
        setTriggerCode(bcr, buf.toString());
        scanner.trigger(w);
        Expect.expect(out.size() == 0, bcr.toString());
        Expect.expect(scanner.getId().equals(bcr), bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      }
      catch (HardwareException e)
      {
        Expect.expect(false, e.getMessage() + " " + bcr.toString());
      }
    }

    setRs232DataBits(cacheDataBits);

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_triggerReturnErrors(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String triggerErrors;

    triggerErrors = "NO RETURN";
    triggerErrors = scanner.triggerReturnErrors(null);
    Expect.expect(triggerErrors.equals("com.axi.util.AssertException: "), triggerErrors + " " + bcr.toString());

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    OutputStreamWriter w = new OutputStreamWriter(out);

    triggerErrors = "NO RETURN";
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());

    triggerErrors = "NO RETURN";
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors += scanner.triggerReturnErrors(w);
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending second trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending second trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(new InterruptedIOException("Simulate InterruptedIOException thrown while sending second trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending first trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(triggerErrors.contains("Simulate InterruptedIOException thrown while sending second trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddTriggerException(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("0"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new SyncFailedException("Simulate SyncFailedException thrown while sending trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("Simulate SyncFailedException thrown while sending trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new SyncFailedException(null));  // SyncFailedException() NOT in API?
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new UnsupportedEncodingException("Simulate UnsupportedEncodingException thrown while sending trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("Simulate UnsupportedEncodingException thrown while sending trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    triggerErrors = "NO RETURN";
    scanner.simulationModeAddTriggerException(new UnsupportedEncodingException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    scanner.simulationModeAddTriggerException(new IOException("Simulate IOException thrown while sending trigger."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(triggerErrors.contains("Simulate IOException thrown while sending trigger."));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    scanner.simulationModeAddTriggerException(new IOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    triggerErrors = scanner.triggerReturnErrors(w);
    Expect.expect(triggerErrors.contains("???"));
    Expect.expect(out.size() == 0, bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    String cacheDataBits = getRs232DataBits();
    setRs232DataBits("8");

    for (int c = 0x00; c <= 0xFF; ++c)
    {
      final String triggerCode = "" + (char)c;
      setTriggerCode(bcr, triggerCode);
      triggerErrors = "NO RETURN";
      triggerErrors = scanner.triggerReturnErrors(w);
      Expect.expect(out.size() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.getId().equals(bcr), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString() + " " + c);
    }

    {
      StringBuffer buf = new StringBuffer();

      for (int i = 0; i < 32; ++i)
        for (int c = 0x00; c <= 0xFF; ++c)
          buf.append((char)c);

      setTriggerCode(bcr, buf.toString());
      triggerErrors = "NO RETURN";
      triggerErrors = scanner.triggerReturnErrors(w);
      Expect.expect(out.size() == 0, bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals(scanner.getId().getTrigger().getValue()), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      Expect.expect(triggerErrors.equals(""), triggerErrors + " " + bcr.toString());
    }

    setRs232DataBits(cacheDataBits);

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @author Bob Balliew
   */
  private void test_read_1(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(null);
      Expect.expect(false, "Expected an assert" + " " + bcr.toString());
    }
    catch (AssertException ae)
    {
      // Expect control flow through here.
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
    }
    Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      if (getPreamble(bcr).equals(""))
        Expect.expect(false, "Should not throw PreambleNotFoundHardwareException if preamble is ''." + " " + bcr.toString());

      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (PostambleNotFoundHardwareException post)
    {
      if (getPostamble(bcr).equals(""))
        Expect.expect(false, "Should not throw PostambleNotFoundHardwareException if postamble is ''." + " " + bcr.toString());

      Expect.expect(post.getLocalizedString().getMessageKey().equals("HW_POSTAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(post.getLocalizedMessage().contains(getPostamble(bcr)));
      Expect.expect(post.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PostambleNotFoundHardwareException post)
    {
      Expect.expect(post.getLocalizedString().getMessageKey().equals("HW_POSTAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(post.getLocalizedMessage().contains(getPostamble(bcr)));
      Expect.expect(post.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('X');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('X');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 12, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 13, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 14, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('x');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("xyzBAR CODE"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('a');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because posamble is not present." + " " + bcr.toString());
    }
    catch (PostambleNotFoundHardwareException post)
    {
      Expect.expect(post.getLocalizedString().getMessageKey().equals("HW_POSTAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(post.getLocalizedMessage().contains(getPostamble(bcr)));
      Expect.expect(post.getLocalizedMessage().contains("BAR CODEcba"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @author Bob Balliew
   */
  private void test_read_2(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 3);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while reading first character."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading last character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 3);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while reading last character."));
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown passed last character read."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading first character of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 3);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while reading first character of second reading."));
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading last character of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 3);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown while reading last character of second reading."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown passed last character read of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 3);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(ee.getLocalizedString().getArguments()[2].equals("Simulate InterruptedIOException thrown passed last character read of second reading."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (InterruptedIOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_INTERRUPTED_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals(0));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown two charactes passed last character read of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @author Bob Balliew
   */
  private void test_read_3(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new SyncFailedException("Simulate SyncFailedException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (SyncFailedHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_SYNC_FAILED_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate SyncFailedException thrown while reading first character."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new SyncFailedException(null));  // SyncFailedException() NOT in API?
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (SyncFailedHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_SYNC_FAILED_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 1);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new UnsupportedEncodingException("Simulate UnsupportedEncodingException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (UnsupportedEncodingHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_UNSUPPORTED_ENCODING_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate UnsupportedEncodingException thrown while reading first character."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new UnsupportedEncodingException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (UnsupportedEncodingHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_UNSUPPORTED_ENCODING_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 1);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new IOException("Simulate IOException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (IOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_IO_WITH_MESSAGE_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 2);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(ee.getLocalizedString().getArguments()[1].equals("Simulate IOException thrown while reading first character."));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new IOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expected an InterruptedIOHardwareException" + " " + bcr.toString());
    }
    catch (IOHardwareException ee)
    {
      Expect.expect(ee.getLocalizedString().getMessageKey().equals("HW_IO_EXCEPTION_KEY"));
      Expect.expect(ee.getLocalizedString().getArguments().length == 1);
      Expect.expect(ee.getLocalizedString().getArguments()[0].equals("???"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    try
    {
      barcode = scanner.read(r);
      Expect.expect(false, "Expect failure because preamble is not present." + " " + bcr.toString());
    }
    catch (PreambleNotFoundHardwareException pre)
    {
      Expect.expect(pre.getLocalizedString().getMessageKey().equals("HW_PREAMBLE_NOT_FOUND_EXCEPTION_KEY"));
      Expect.expect(pre.getLocalizedMessage().contains(getPreamble(bcr)));
      Expect.expect(pre.getLocalizedMessage().contains("***EMPTY***"));
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    catch (HardwareException e)
    {
      Expect.expect(false, e.getMessage() + " " + bcr.toString());
      Expect.expect(barcode.equals("NOTHING READ"), barcode + " " + bcr.toString());
    }
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @author Bob Balliew
   */
  private void test_read_4(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcodeRead;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    String cacheDataBits = getRs232DataBits();
    setRs232DataBits("8");
    final int MAX = 0xFF;

    for (int c = 0x00; c <= MAX; ++c)
    {
      char preambleChar = (char)c;
      char barcodeChar1 =  (char)((c + 1) % MAX);
      char barcodeChar2 =  (char)((c + 2) % MAX);
      char postambleChar = (char)((c + 3) % MAX);
      final String preamble = "" + preambleChar;
      final String barcode = "" + barcodeChar1 + barcodeChar2;
      final String postamble = "" + postambleChar;
      setPreamble(bcr, preamble);
      setPostamble(bcr, postamble);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(preambleChar);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(barcodeChar1);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(barcodeChar2);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(postambleChar);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddReadingToReadings();
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      barcodeRead = "NOTHING READ";
      try
      {
        barcodeRead = scanner.read(r);
        Expect.expect(barcodeRead.equals(barcode), barcodeRead + " " + bcr.toString() + " " + c);
      }
      catch (HardwareException e)
      {
        Expect.expect(false, e.getMessage() + " " + bcr.toString() + " " + c);
        Expect.expect(barcodeRead.equals("NOTHING READ"), barcodeRead + " " + bcr.toString() + " " + c);
      }
      Expect.expect(scanner.getId().equals(bcr), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
    }

    {
      StringBuffer preambleBuf = new StringBuffer();
      StringBuffer barcodeBuf = new StringBuffer();
      StringBuffer postambleBuf = new StringBuffer();

      for (int i = 0, c= 0; i < 8192; ++i, ++c)
      {
        char preambleChar  = (char)((c + 0) & MAX);
        char barcodeChar   = (char)((c + 1) % MAX);
        char postambleChar = (char)((c + 2) % MAX);
        preambleBuf.append(preambleChar);
        barcodeBuf.append(barcodeChar);
        postambleBuf.append(postambleChar);
      }

      final String preamble = preambleBuf.toString();
      final String barcode = barcodeBuf.toString();
      final String postamble = postambleBuf.toString();

      setPreamble(bcr, preamble);
      setPostamble(bcr, postamble);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      int count = 0;

      for (int i = 0; i < preamble.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(preamble.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      for (int i = 0; i < barcode.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(barcode.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      for (int i = 0; i < postamble.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(postamble.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      scanner.simulationModeAddReadingToReadings();
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      barcodeRead = "NOTHING READ";
      try
      {
        barcodeRead = scanner.read(r);
        Expect.expect(barcodeRead.equals(barcode), barcodeRead + " " + bcr.toString());
      }
      catch (HardwareException e)
      {
        Expect.expect(false, e.getMessage() + " " + bcr.toString());
        Expect.expect(barcodeRead.equals("NOTHING READ"), barcodeRead + " " + bcr.toString());
      }
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }

    setRs232DataBits(cacheDataBits);

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @author Bob Balliew
   */
  private void test_read(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    test_read_1(bcr, is, os);
    test_read_2(bcr, is, os);
    test_read_3(bcr, is, os);
    test_read_4(bcr, is, os);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_readReturnErrors_1(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {

    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(null);
    Expect.expect(barcode.equals("com.axi.util.AssertException: "), barcode + " " + bcr.toString());

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPostamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('X');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('X');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 12, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 13, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 14, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("BAR CODE"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "XYZ");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('x');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('y');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("xyzBAR CODE"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "CBA");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('B');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('R');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading(' ');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('C');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('O');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 6, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('D');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 7, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('E');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 8, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 9, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 10, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('a');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 11, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPostamble(bcr)));
    Expect.expect(barcode.contains("BAR CODEcba"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_readReturnErrors_2(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(barcode.contains("Simulate InterruptedIOException thrown while reading first character."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading last character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(barcode.contains("Simulate InterruptedIOException thrown while reading last character."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown passed last character read."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading first character of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(barcode.contains("Simulate InterruptedIOException thrown while reading first character of second reading."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown while reading last character of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(barcode.contains("Simulate InterruptedIOException thrown while reading last character of second reading."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown passed last character read of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(barcode.contains("Simulate InterruptedIOException thrown passed last character read of second reading."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("0"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException("Simulate InterruptedIOException thrown two charactes passed last character read of second reading."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "");
    setPostamble(bcr, "");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('1');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('2');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(null);
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new InterruptedIOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 5, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 2, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc1"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("bc2"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.equals("***EMPTY***"), barcode + " " + bcr.toString());
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_readReturnErrors_3(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcode;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new SyncFailedException("Simulate SyncFailedException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("Simulate SyncFailedException thrown while reading first character."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new SyncFailedException(null));  // SyncFailedException() NOT in API?
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new UnsupportedEncodingException("Simulate UnsupportedEncodingException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("Simulate UnsupportedEncodingException thrown while reading first character."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new UnsupportedEncodingException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new IOException("Simulate IOException thrown while reading first character."));
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(barcode.contains("Simulate IOException thrown while reading first character."));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setPreamble(bcr, "A");
    setPostamble(bcr, "Z");
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('A');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('b');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('c');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCharacterToCurrentReading('Z');
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddReadingToReadings();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddExceptionToCurrentExceptionQueue(new IOException());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    scanner.simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 1, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains("???"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    barcode = "NOTHING READ";
    barcode = scanner.readReturnErrors(r);
    Expect.expect(barcode.contains(getPreamble(bcr)));
    Expect.expect(barcode.contains("***EMPTY***"));
    Expect.expect(scanner.getId().equals(bcr), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
    Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_readReturnErrors_4(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    BarcodeReader scanner = new BarcodeReader(bcr);
    final String scannerNumber = (new Integer(bcr.getOrder() + 1)).toString();
    String cacheTriggerCode = getTriggerCode(bcr);
    String cachePreamble = getPreamble(bcr);
    String cachePostamble = getPostamble(bcr);
    String barcodeRead;

    final int BUF_SIZE = 10;
    byte[] buf = new byte[BUF_SIZE];
    ByteArrayInputStream in = new ByteArrayInputStream(buf);
    InputStreamReader r = new InputStreamReader(in);

    String cacheDataBits = getRs232DataBits();
    setRs232DataBits("8");
    final int MAX = 0xFF;

    for (int c = 0x00; c <= MAX; ++c)
    {
      char preambleChar = (char)c;
      char barcodeChar1 =  (char)((c + 1) % MAX);
      char barcodeChar2 =  (char)((c + 2) % MAX);
      char postambleChar = (char)((c + 3) % MAX);
      final String preamble = "" + preambleChar;
      final String barcode = "" + barcodeChar1 + barcodeChar2;
      final String postamble = "" + postambleChar;
      setPreamble(bcr, preamble);
      setPostamble(bcr, postamble);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(preambleChar);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 1, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(barcodeChar1);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 2, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(barcodeChar2);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 3, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddCharacterToCurrentReading(postambleChar);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 4, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      scanner.simulationModeAddReadingToReadings();
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
      barcodeRead = "NOTHING READ";
      barcodeRead = scanner.readReturnErrors(r);
      Expect.expect(barcodeRead.equals(barcode), barcodeRead + " " + bcr.toString() + " " + c);
      Expect.expect(scanner.getId().equals(bcr), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + c);
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + c);
    }

    {
      StringBuffer preambleBuf = new StringBuffer();
      StringBuffer barcodeBuf = new StringBuffer();
      StringBuffer postambleBuf = new StringBuffer();

      for (int i = 0, c= 0; i < 8192; ++i, ++c)
      {
        char preambleChar  = (char)((c + 0) & MAX);
        char barcodeChar   = (char)((c + 1) % MAX);
        char postambleChar = (char)((c + 2) % MAX);
        preambleBuf.append(preambleChar);
        barcodeBuf.append(barcodeChar);
        postambleBuf.append(postambleChar);
      }

      final String preamble = preambleBuf.toString();
      final String barcode = barcodeBuf.toString();
      final String postamble = postambleBuf.toString();

      setPreamble(bcr, preamble);
      setPostamble(bcr, postamble);
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      int count = 0;

      for (int i = 0; i < preamble.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(preamble.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      for (int i = 0; i < barcode.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(barcode.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      for (int i = 0; i < postamble.length() ; ++i)
      {
        scanner.simulationModeAddCharacterToCurrentReading(postamble.charAt(i));
        ++count;
        Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == count, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString() + " " + i);
        Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString() + " " + i);
      }

      scanner.simulationModeAddReadingToReadings();
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 1, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
      barcodeRead = "NOTHING READ";
      barcodeRead = scanner.readReturnErrors(r);
      Expect.expect(barcodeRead.equals(barcode), barcodeRead + " " + bcr.toString());
      Expect.expect(scanner.getId().equals(bcr), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerCodesCaptured() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeGetNextTriggerCodeCaptured().equals("***NO TRIGGER CODES***"), bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfTriggerExceptions() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadCharactersInCurrentReading() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadReadings() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue() == 0, bcr.toString());
      Expect.expect(scanner.simulationModeNumberOfUnreadExceptions() == 0, bcr.toString());
    }

    setRs232DataBits(cacheDataBits);

    setTriggerCode(bcr, cacheTriggerCode);
    setPreamble(bcr, cachePreamble);
    setPostamble(bcr, cachePostamble);
  }

  /**
   * @deprecated This method tests deprecated methods.
   * @author Bob Balliew
   */
  private void test_readReturnErrors(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    test_readReturnErrors_1(bcr, is, os);
    test_readReturnErrors_2(bcr, is, os);
    test_readReturnErrors_3(bcr, is, os);
    test_readReturnErrors_4(bcr, is, os);
  }

  /**
   * @author Bob Balliew
   */
  private void test_BCR(BarcodeReaderEnum bcr, BufferedReader is, PrintWriter os)
  {
    test_trigger(bcr, is, os);
    test_triggerReturnErrors(bcr, is, os);
    test_read(bcr, is, os);
    test_readReturnErrors(bcr, is, os);
  }

  /**
   * @author Bob Balliew
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Config.getInstance().loadIfNecessary();
      test_BCR(BarcodeReaderEnum.BCR1, is, os);
      test_BCR(BarcodeReaderEnum.BCR2, is, os);
      test_BCR(BarcodeReaderEnum.BCR3, is, os);
      test_BCR(BarcodeReaderEnum.BCR4, is, os);
      test_BCR(BarcodeReaderEnum.BCR5, is, os);
      test_BCR(BarcodeReaderEnum.BCR6, is, os);
      test_BCR(BarcodeReaderEnum.BCR7, is, os);
      test_BCR(BarcodeReaderEnum.BCR8, is, os);
      test_BCR(BarcodeReaderEnum.BCR9, is, os);
      test_BCR(BarcodeReaderEnum.BCR10, is, os);
      test_BCR(BarcodeReaderEnum.BCR11, is, os);
      test_BCR(BarcodeReaderEnum.BCR12, is, os);
      test_BCR(BarcodeReaderEnum.BCR13, is, os);
      test_BCR(BarcodeReaderEnum.BCR14, is, os);
      test_BCR(BarcodeReaderEnum.BCR15, is, os);
      test_BCR(BarcodeReaderEnum.BCR16, is, os);
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }
}
