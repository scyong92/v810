package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspModuleEnum extends com.axi.util.Enum 
{
  private static HashMap<Integer, PspModuleEnum> _integerToEnumMap;
  
  private static int _index = -1;
  private String _name;

  /**
    * @author Cheah Lee Herng
    */
  static
  {
    _integerToEnumMap = new HashMap<Integer, PspModuleEnum>();
  }

  public static PspModuleEnum FRONT = new PspModuleEnum(++_index, "Front");
  public static PspModuleEnum REAR  = new PspModuleEnum(++_index, "Rear");
  
  /**
   * @author Cheah Lee Herng 
   */
  private PspModuleEnum(int id, String name)
  {
    super(id);
    _name = name;
    
    Assert.expect(_integerToEnumMap.put(new Integer(id), this) == null);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String toString()
  {
    return _name;
  }
  
  /**
  * @author Cheah Lee Herng
  */
  static public ArrayList<PspModuleEnum> getAllEnums()
  {
    return new ArrayList<PspModuleEnum>(_integerToEnumMap.values());
  }
}
