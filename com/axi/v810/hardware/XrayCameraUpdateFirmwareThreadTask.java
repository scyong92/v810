package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Ronald Lim
 * @edited By Kee Chin Seong - Force update firmware
 */
public class XrayCameraUpdateFirmwareThreadTask extends ThreadTask<Object>
{
  private AbstractXrayCamera _xRayCamera;
  private boolean _isForceUpdate;
  
  /**
   * @author Ronald Lim
   * @edited By Kee Chin Seong - Force update firmware
   */
  public XrayCameraUpdateFirmwareThreadTask(AbstractXrayCamera xRayCamera, boolean isForceUpdate)
  {
    super("Xray Camera Update Firmware task.");

    Assert.expect(xRayCamera != null);
    _xRayCamera = xRayCamera;
    _isForceUpdate = isForceUpdate;
  }

  /**
   * @author Ronald Lim
   */
  public void cancel() throws Exception
  {
    // do nothing
  }

  /**
   * @author Ronald Lim
   */
  public void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Ronald Lim
   * @edited By Kee Chin Seong - Force update firmware
   */
  public Object executeTask() throws Exception
  {
    _xRayCamera.updateCameraFirmware(_isForceUpdate);
    _xRayCamera.reboot();
    return null;
  }
}
