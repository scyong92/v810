package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
abstract public class ScanPassBreakPoint
{
  private ScanPass _scanPass;
  private boolean _isEnabled = true;
  protected double _scanMotionTimeOutMultiplyFactor = 1;
  protected static boolean _debug = Config.isDeveloperDebugModeOn();

  /**
   * @author Roy Williams
   */
  public ScanPassBreakPoint(ScanPass scanPass, double speedFactor)
  {
    Assert.expect(scanPass != null);
    _scanPass = scanPass;
    _scanMotionTimeOutMultiplyFactor = speedFactor;
    scanPass.addBreakPoint(this);
  }
  
  /**
   * @author Roy Williams
   */
  public boolean isEnabled()
  {
    return _isEnabled;
  }

  /**
   * @author Roy Williams
   */
  public void setEnabled(boolean state)
  {
    _isEnabled = state;
  }

  /**
   * @author Roy Williams
   */
  public int getScanPassNumber()
  {
    return _scanPass.getId();
  }

  /**
   * @author Roy Williams
   */
  abstract public void executeAction() throws XrayTesterException;

  /**
   * @author Roy Williams
   */
  abstract public int getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction();

  abstract public void clearBreakPoint();

  /**
   * @author Yong Sheng Chuan
   */
  public void setScanMotionTimeOutMultipleFactor(double speedFactor)
  {
    _scanMotionTimeOutMultiplyFactor = speedFactor;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public double getScanMotionTimeOutMultiplyFactor()
  {
    return _scanMotionTimeOutMultiplyFactor;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  abstract public ScanPassBreakPointEnum getBreakPointEnum();
}
