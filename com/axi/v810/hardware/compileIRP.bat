:: -----------------------------------------------------------------------------
:: (C) Copyright 2010 ViTrox Technologies. All rights reserved
:: -----------------------------------------------------------------------------
:: @author Wei Chin, Chong
:: compile JNI and package it into nativeHardwareUtil.dll
::
@echo off

SET MODE=Debug
IF "%1"=="Debug" set MODE=Debug
IF "%1"=="Release" set MODE=Release

set OLDPATH=%PATH%
set VSTUDIO_DIR=C:/apps/vStudio2008
set VSTUDIO2008_DIR=C:/apps/vStudio2008
set JDK_PATH=C:/apps/java/jdk1.6.0_21
set WINSDK_PATH=../../../../../thirdParty/microsoft/WINSDK/v7.1
set VCVARS_PATH=%VSTUDIO_DIR%/VC/bin/amd64
set LIB_CLASSPATH=../../../../jre/1.6.0_21/lib/ext/comm.jar;../../../../jre/1.6.0_21/lib/ext/j3daudio.jar;../../../../jre/1.6.0_21/lib/ext/j3dcore.jar;../../../../jre/1.6.0_21/lib/ext/j3dutils.jar;../../../../jre/1.6.0_21/lib/ext/vecmath.jar;../../../../jre/1.6.0_21/lib/ext/Jama-1.0.2.jar;../../../../jre/1.6.0_21/lib/ext/jcommon-1.0.0.jar;../../../../jre/1.6.0_21/lib/ext/jfreechart-1.0.1.jar
set INCLUDE_VC=-I%VSTUDIO_DIR%/VC/include -I"%WINSDK_PATH%/include" -I%JDK_PATH%/include -I%JDK_PATH%/include/win32 -I../../../.. -I../../../../.. -I../../../../../cpp -I../../../../../cpp/util/src
IF "%MODE%"=="Release" set VC_FLAG=-D_WINDOWS -DWIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -EHsc -O2 -MD
IF "%MODE%"=="Debug" set VC_FLAG=-D_WINDOWS -DWIN64 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -D_Debug -EHsc -O2 -MD
set DEBUG_FLAG=-DEBUG
IF "%MODE%"=="Release" set DEBUG_FLAG= 
set OBJ_PATH=../../../../buildFiles/jniMscpp/jniLoadHandler.obj ../../../../buildFiles/jniMscpp/com_axi_v810_hardware_ImageReconstructionProcessor.obj
set PATH=%PATH%;%JDK_PATH%\bin;%VCVARS_PATH%
set TMP=%TEMP%

echo "#### generating JNI header file from com.axi.v810.hardware.ImageReconstructionProcessor ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.v810.hardware.ImageReconstructionProcessor
@echo off

echo "#### building nativeIRP.dll ####"
@echo on
%VCVARS_PATH%\cl -c -D_NATIVE_IRP_DLL %VC_FLAG% %INCLUDE_VC% com_axi_v810_hardware_ImageReconstructionProcessor.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_v810_hardware_ImageReconstructionProcessor.obj

%VCVARS_PATH%\link %OBJ_PATH% -ignore:4089 -out:../../../../jniRelease/bin/nativeIRP.dll -LIBPATH:../../../../../cpp/release/bin -LIBPATH:%VSTUDIO_DIR%/VC/lib/amd64 -LIBPATH:"%WINSDK_PATH%/Lib/x64" -DLL %DEBUG_FLAG% ../../../../../cpp/release/bin/axiUtil.lib

"%WINSDK_PATH%\Bin\x64\mt" -manifest ../../../../jniRelease/bin/nativeIRP.dll.manifest -outputresource:../../../../jniRelease/bin/nativeIRP.dll;2

@echo off
:: clean up environment space
set PATH= %OLDPATH%
set OLDPATH=
set VCVARS_PATH=
set JDK_PATH=