package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ImageReconstructionProcessorEnum extends com.axi.util.Enum
{
  private static List<ImageReconstructionProcessorEnum> _listOfEnums = new ArrayList<ImageReconstructionProcessorEnum>();

  private static int _index = -1;

  public static ImageReconstructionProcessorEnum IRP0 = new ImageReconstructionProcessorEnum(++_index);
  public static ImageReconstructionProcessorEnum IRP1 = new ImageReconstructionProcessorEnum(++_index);
  public static ImageReconstructionProcessorEnum IRP2 = new ImageReconstructionProcessorEnum(++_index);
  public static ImageReconstructionProcessorEnum IRP3 = new ImageReconstructionProcessorEnum(++_index);

  /**
   * @author Rex Shang
   */
  private ImageReconstructionProcessorEnum(int id)
  {
    super(id);

    _listOfEnums.add(this);
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return "" + super.getId();
  }

  /**
   * @author Rex Shang
   */
  public static List<ImageReconstructionProcessorEnum> getEnumList()
  {
    return _listOfEnums;
  }

  /**
   * This method should not be used.
   * @author Rex Shang
   */
  public int getId()
  {
    Assert.expect(false, "getId() is used!");
    return super.getId();
  }

}
