package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspMicroControllerStatusEnum extends com.axi.util.Enum
{
    private static Map<String, PspMicroControllerStatusEnum> _statusByteToPspMicroControllerStatusEnumMap = new HashMap<String, PspMicroControllerStatusEnum>();

    private static int _index = 0;
    private String _status;
    public static final PspMicroControllerStatusEnum READY = new PspMicroControllerStatusEnum(_index++, "a0");
    public static final PspMicroControllerStatusEnum NOT_READY = new PspMicroControllerStatusEnum(_index++, "ff");

  /**
   * @author Cheah Lee Herng
   */
  private PspMicroControllerStatusEnum(int id, String status)
  {
    super(id);
    Object previous = _statusByteToPspMicroControllerStatusEnumMap.put(status, this);
    Assert.expect(previous == null);
    _status = status;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static PspMicroControllerStatusEnum getEnum(String status)
  {
    PspMicroControllerStatusEnum enumeration = _statusByteToPspMicroControllerStatusEnumMap.get(status);

    Assert.expect(enumeration != null);
    return enumeration;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getStatus()
  {
      return _status;
  }
}
