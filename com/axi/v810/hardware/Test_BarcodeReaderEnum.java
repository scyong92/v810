package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bob Balliew
 */
public class Test_BarcodeReaderEnum extends UnitTest
{
  /**
   * @author Bob Balliew
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_BarcodeReaderEnum());
  }

  /**
   * @author Bob Balliew
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    Set<Integer> hashsSeen = new TreeSet<Integer>();

    Expect.expect(BarcodeReaderEnum.BCR1.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1));
    Expect.expect(BarcodeReaderEnum.BCR1.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1));
    Expect.expect(BarcodeReaderEnum.BCR1.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1));
    Expect.expect(BarcodeReaderEnum.BCR1.getOrder() == 0);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR1.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR1.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR1.equals(BarcodeReaderEnum.BCR1));
    Expect.expect(! BarcodeReaderEnum.BCR1.equals(BarcodeReaderEnum.BCR2));
    Expect.expect(BarcodeReaderEnum.BCR1.compareTo(BarcodeReaderEnum.BCR2) < 0);
    Expect.expect(BarcodeReaderEnum.BCR1.compareTo(BarcodeReaderEnum.BCR1) == 0);
    Expect.expect(BarcodeReaderEnum.BCR1.getId() == 0);

    Expect.expect(BarcodeReaderEnum.BCR2.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2));
    Expect.expect(BarcodeReaderEnum.BCR2.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2));
    Expect.expect(BarcodeReaderEnum.BCR2.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2));
    Expect.expect(BarcodeReaderEnum.BCR2.getOrder() == 1);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR2.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR2.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR2.equals(BarcodeReaderEnum.BCR2));
    Expect.expect(! BarcodeReaderEnum.BCR2.equals(BarcodeReaderEnum.BCR3));
    Expect.expect(BarcodeReaderEnum.BCR2.compareTo(BarcodeReaderEnum.BCR3) < 0);
    Expect.expect(BarcodeReaderEnum.BCR2.compareTo(BarcodeReaderEnum.BCR2) == 0);
    Expect.expect(BarcodeReaderEnum.BCR2.compareTo(BarcodeReaderEnum.BCR1) > 0);
    Expect.expect(BarcodeReaderEnum.BCR2.getId() == 1);

    Expect.expect(BarcodeReaderEnum.BCR3.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3));
    Expect.expect(BarcodeReaderEnum.BCR3.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3));
    Expect.expect(BarcodeReaderEnum.BCR3.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3));
    Expect.expect(BarcodeReaderEnum.BCR3.getOrder() == 2);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR3.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR3.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR3.equals(BarcodeReaderEnum.BCR3));
    Expect.expect(! BarcodeReaderEnum.BCR3.equals(BarcodeReaderEnum.BCR4));
    Expect.expect(BarcodeReaderEnum.BCR3.compareTo(BarcodeReaderEnum.BCR4) < 0);
    Expect.expect(BarcodeReaderEnum.BCR3.compareTo(BarcodeReaderEnum.BCR3) == 0);
    Expect.expect(BarcodeReaderEnum.BCR3.compareTo(BarcodeReaderEnum.BCR2) > 0);
    Expect.expect(BarcodeReaderEnum.BCR3.getId() == 2);

    Expect.expect(BarcodeReaderEnum.BCR4.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4));
    Expect.expect(BarcodeReaderEnum.BCR4.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4));
    Expect.expect(BarcodeReaderEnum.BCR4.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4));
    Expect.expect(BarcodeReaderEnum.BCR4.getOrder() == 3);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR4.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR4.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR4.equals(BarcodeReaderEnum.BCR4));
    Expect.expect(! BarcodeReaderEnum.BCR4.equals(BarcodeReaderEnum.BCR5));
    Expect.expect(BarcodeReaderEnum.BCR4.compareTo(BarcodeReaderEnum.BCR5) < 0);
    Expect.expect(BarcodeReaderEnum.BCR4.compareTo(BarcodeReaderEnum.BCR4) == 0);
    Expect.expect(BarcodeReaderEnum.BCR4.compareTo(BarcodeReaderEnum.BCR3) > 0);
    Expect.expect(BarcodeReaderEnum.BCR4.getId() == 3);

    Expect.expect(BarcodeReaderEnum.BCR5.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5));
    Expect.expect(BarcodeReaderEnum.BCR5.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5));
    Expect.expect(BarcodeReaderEnum.BCR5.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5));
    Expect.expect(BarcodeReaderEnum.BCR5.getOrder() == 4);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR5.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR5.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR5.equals(BarcodeReaderEnum.BCR5));
    Expect.expect(! BarcodeReaderEnum.BCR5.equals(BarcodeReaderEnum.BCR6));
    Expect.expect(BarcodeReaderEnum.BCR5.compareTo(BarcodeReaderEnum.BCR6) < 0);
    Expect.expect(BarcodeReaderEnum.BCR5.compareTo(BarcodeReaderEnum.BCR5) == 0);
    Expect.expect(BarcodeReaderEnum.BCR5.compareTo(BarcodeReaderEnum.BCR4) > 0);
    Expect.expect(BarcodeReaderEnum.BCR5.getId() == 4);

    Expect.expect(BarcodeReaderEnum.BCR6.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6));
    Expect.expect(BarcodeReaderEnum.BCR6.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6));
    Expect.expect(BarcodeReaderEnum.BCR6.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6));
    Expect.expect(BarcodeReaderEnum.BCR6.getOrder() == 5);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR6.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR6.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR6.equals(BarcodeReaderEnum.BCR6));
    Expect.expect(! BarcodeReaderEnum.BCR6.equals(BarcodeReaderEnum.BCR7));
    Expect.expect(BarcodeReaderEnum.BCR6.compareTo(BarcodeReaderEnum.BCR7) < 0);
    Expect.expect(BarcodeReaderEnum.BCR6.compareTo(BarcodeReaderEnum.BCR6) == 0);
    Expect.expect(BarcodeReaderEnum.BCR6.compareTo(BarcodeReaderEnum.BCR5) > 0);
    Expect.expect(BarcodeReaderEnum.BCR6.getId() == 5);

    Expect.expect(BarcodeReaderEnum.BCR7.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7));
    Expect.expect(BarcodeReaderEnum.BCR7.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7));
    Expect.expect(BarcodeReaderEnum.BCR7.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7));
    Expect.expect(BarcodeReaderEnum.BCR7.getOrder() == 6);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR7.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR7.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR7.equals(BarcodeReaderEnum.BCR7));
    Expect.expect(! BarcodeReaderEnum.BCR7.equals(BarcodeReaderEnum.BCR8));
    Expect.expect(BarcodeReaderEnum.BCR7.compareTo(BarcodeReaderEnum.BCR8) < 0);
    Expect.expect(BarcodeReaderEnum.BCR7.compareTo(BarcodeReaderEnum.BCR7) == 0);
    Expect.expect(BarcodeReaderEnum.BCR7.compareTo(BarcodeReaderEnum.BCR6) > 0);
    Expect.expect(BarcodeReaderEnum.BCR7.getId() == 6);

    Expect.expect(BarcodeReaderEnum.BCR8.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8));
    Expect.expect(BarcodeReaderEnum.BCR8.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8));
    Expect.expect(BarcodeReaderEnum.BCR8.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8));
    Expect.expect(BarcodeReaderEnum.BCR8.getOrder() == 7);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR8.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR8.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR8.equals(BarcodeReaderEnum.BCR8));
    Expect.expect(! BarcodeReaderEnum.BCR8.equals(BarcodeReaderEnum.BCR9));
    Expect.expect(BarcodeReaderEnum.BCR8.compareTo(BarcodeReaderEnum.BCR9) < 0);
    Expect.expect(BarcodeReaderEnum.BCR8.compareTo(BarcodeReaderEnum.BCR8) == 0);
    Expect.expect(BarcodeReaderEnum.BCR8.compareTo(BarcodeReaderEnum.BCR7) > 0);
    Expect.expect(BarcodeReaderEnum.BCR8.getId() == 7);

    Expect.expect(BarcodeReaderEnum.BCR9.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9));
    Expect.expect(BarcodeReaderEnum.BCR9.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9));
    Expect.expect(BarcodeReaderEnum.BCR9.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9));
    Expect.expect(BarcodeReaderEnum.BCR9.getOrder() == 8);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR9.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR9.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR9.equals(BarcodeReaderEnum.BCR9));
    Expect.expect(! BarcodeReaderEnum.BCR9.equals(BarcodeReaderEnum.BCR10));
    Expect.expect(BarcodeReaderEnum.BCR9.compareTo(BarcodeReaderEnum.BCR10) < 0);
    Expect.expect(BarcodeReaderEnum.BCR9.compareTo(BarcodeReaderEnum.BCR9) == 0);
    Expect.expect(BarcodeReaderEnum.BCR9.compareTo(BarcodeReaderEnum.BCR8) > 0);
    Expect.expect(BarcodeReaderEnum.BCR9.getId() == 8);

    Expect.expect(BarcodeReaderEnum.BCR10.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10));
    Expect.expect(BarcodeReaderEnum.BCR10.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10));
    Expect.expect(BarcodeReaderEnum.BCR10.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10));
    Expect.expect(BarcodeReaderEnum.BCR10.getOrder() == 9);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR10.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR10.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR10.equals(BarcodeReaderEnum.BCR10));
    Expect.expect(! BarcodeReaderEnum.BCR10.equals(BarcodeReaderEnum.BCR11));
    Expect.expect(BarcodeReaderEnum.BCR10.compareTo(BarcodeReaderEnum.BCR11) < 0);
    Expect.expect(BarcodeReaderEnum.BCR10.compareTo(BarcodeReaderEnum.BCR10) == 0);
    Expect.expect(BarcodeReaderEnum.BCR10.compareTo(BarcodeReaderEnum.BCR9) > 0);
    Expect.expect(BarcodeReaderEnum.BCR10.getId() == 9);

    Expect.expect(BarcodeReaderEnum.BCR11.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11));
    Expect.expect(BarcodeReaderEnum.BCR11.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11));
    Expect.expect(BarcodeReaderEnum.BCR11.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11));
    Expect.expect(BarcodeReaderEnum.BCR11.getOrder() == 10);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR11.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR11.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR11.equals(BarcodeReaderEnum.BCR11));
    Expect.expect(! BarcodeReaderEnum.BCR11.equals(BarcodeReaderEnum.BCR12));
    Expect.expect(BarcodeReaderEnum.BCR11.compareTo(BarcodeReaderEnum.BCR12) < 0);
    Expect.expect(BarcodeReaderEnum.BCR11.compareTo(BarcodeReaderEnum.BCR11) == 0);
    Expect.expect(BarcodeReaderEnum.BCR11.compareTo(BarcodeReaderEnum.BCR10) > 0);
    Expect.expect(BarcodeReaderEnum.BCR11.getId() == 10);

    Expect.expect(BarcodeReaderEnum.BCR12.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12));
    Expect.expect(BarcodeReaderEnum.BCR12.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12));
    Expect.expect(BarcodeReaderEnum.BCR12.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12));
    Expect.expect(BarcodeReaderEnum.BCR12.getOrder() == 11);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR12.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR12.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR12.equals(BarcodeReaderEnum.BCR12));
    Expect.expect(! BarcodeReaderEnum.BCR12.equals(BarcodeReaderEnum.BCR13));
    Expect.expect(BarcodeReaderEnum.BCR12.compareTo(BarcodeReaderEnum.BCR13) < 0);
    Expect.expect(BarcodeReaderEnum.BCR12.compareTo(BarcodeReaderEnum.BCR12) == 0);
    Expect.expect(BarcodeReaderEnum.BCR12.compareTo(BarcodeReaderEnum.BCR11) > 0);
    Expect.expect(BarcodeReaderEnum.BCR12.getId() == 11);

    Expect.expect(BarcodeReaderEnum.BCR13.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13));
    Expect.expect(BarcodeReaderEnum.BCR13.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13));
    Expect.expect(BarcodeReaderEnum.BCR13.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13));
    Expect.expect(BarcodeReaderEnum.BCR13.getOrder() == 12);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR13.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR13.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR13.equals(BarcodeReaderEnum.BCR13));
    Expect.expect(! BarcodeReaderEnum.BCR13.equals(BarcodeReaderEnum.BCR14));
    Expect.expect(BarcodeReaderEnum.BCR13.compareTo(BarcodeReaderEnum.BCR14) < 0);
    Expect.expect(BarcodeReaderEnum.BCR13.compareTo(BarcodeReaderEnum.BCR13) == 0);
    Expect.expect(BarcodeReaderEnum.BCR13.compareTo(BarcodeReaderEnum.BCR12) > 0);
    Expect.expect(BarcodeReaderEnum.BCR13.getId() == 12);

    Expect.expect(BarcodeReaderEnum.BCR14.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14));
    Expect.expect(BarcodeReaderEnum.BCR14.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14));
    Expect.expect(BarcodeReaderEnum.BCR14.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14));
    Expect.expect(BarcodeReaderEnum.BCR14.getOrder() == 13);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR14.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR14.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR14.equals(BarcodeReaderEnum.BCR14));
    Expect.expect(! BarcodeReaderEnum.BCR14.equals(BarcodeReaderEnum.BCR15));
    Expect.expect(BarcodeReaderEnum.BCR14.compareTo(BarcodeReaderEnum.BCR15) < 0);
    Expect.expect(BarcodeReaderEnum.BCR14.compareTo(BarcodeReaderEnum.BCR14) == 0);
    Expect.expect(BarcodeReaderEnum.BCR14.compareTo(BarcodeReaderEnum.BCR13) > 0);
    Expect.expect(BarcodeReaderEnum.BCR14.getId() == 13);

    Expect.expect(BarcodeReaderEnum.BCR15.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15));
    Expect.expect(BarcodeReaderEnum.BCR15.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15));
    Expect.expect(BarcodeReaderEnum.BCR15.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15));
    Expect.expect(BarcodeReaderEnum.BCR15.getOrder() == 14);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR15.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR15.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR15.equals(BarcodeReaderEnum.BCR15));
    Expect.expect(! BarcodeReaderEnum.BCR15.equals(BarcodeReaderEnum.BCR16));
    Expect.expect(BarcodeReaderEnum.BCR15.compareTo(BarcodeReaderEnum.BCR16) < 0);
    Expect.expect(BarcodeReaderEnum.BCR15.compareTo(BarcodeReaderEnum.BCR15) == 0);
    Expect.expect(BarcodeReaderEnum.BCR15.compareTo(BarcodeReaderEnum.BCR14) > 0);
    Expect.expect(BarcodeReaderEnum.BCR15.getId() == 14);

    Expect.expect(BarcodeReaderEnum.BCR16.getTrigger().equals(BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16));
    Expect.expect(BarcodeReaderEnum.BCR16.getPreamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16));
    Expect.expect(BarcodeReaderEnum.BCR16.getPostamble().equals(BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16));
    Expect.expect(BarcodeReaderEnum.BCR16.getOrder() == 15);
    Expect.expect(! hashsSeen.contains(BarcodeReaderEnum.BCR16.hashCode()));
    hashsSeen.add(BarcodeReaderEnum.BCR16.hashCode());
    Expect.expect(BarcodeReaderEnum.BCR16.equals(BarcodeReaderEnum.BCR16));
    Expect.expect(! BarcodeReaderEnum.BCR16.equals(BarcodeReaderEnum.BCR1));
    Expect.expect(BarcodeReaderEnum.BCR16.compareTo(BarcodeReaderEnum.BCR16) == 0);
    Expect.expect(BarcodeReaderEnum.BCR16.compareTo(BarcodeReaderEnum.BCR15) > 0);
    Expect.expect(BarcodeReaderEnum.BCR16.getId() == 15);

    Collection<BarcodeReaderEnum> allBCRs = BarcodeReaderEnum.values();
    Expect.expect(allBCRs.size() == 16);
    Collection<BarcodeReaderEnum> expectBCRs = new HashSet<BarcodeReaderEnum>();
    expectBCRs.add(BarcodeReaderEnum.BCR1);
    expectBCRs.add(BarcodeReaderEnum.BCR2);
    expectBCRs.add(BarcodeReaderEnum.BCR3);
    expectBCRs.add(BarcodeReaderEnum.BCR4);
    expectBCRs.add(BarcodeReaderEnum.BCR5);
    expectBCRs.add(BarcodeReaderEnum.BCR6);
    expectBCRs.add(BarcodeReaderEnum.BCR7);
    expectBCRs.add(BarcodeReaderEnum.BCR8);
    expectBCRs.add(BarcodeReaderEnum.BCR9);
    expectBCRs.add(BarcodeReaderEnum.BCR10);
    expectBCRs.add(BarcodeReaderEnum.BCR11);
    expectBCRs.add(BarcodeReaderEnum.BCR12);
    expectBCRs.add(BarcodeReaderEnum.BCR13);
    expectBCRs.add(BarcodeReaderEnum.BCR14);
    expectBCRs.add(BarcodeReaderEnum.BCR15);
    expectBCRs.add(BarcodeReaderEnum.BCR16);
    Expect.expect(allBCRs.containsAll(expectBCRs));
    Expect.expect(expectBCRs.containsAll(allBCRs));
    Expect.expect(BarcodeReaderEnum.getNumEnums(BarcodeReaderEnum.class) == 16);
  }
}
