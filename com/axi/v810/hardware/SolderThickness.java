package com.axi.v810.hardware;


import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import java.nio.*;
import java.util.*;
//import java.io.*;

/**
 * Reads a data file mapping background and delta gray levels
 * to the thickness. Provides access methods.
 * @author Eddie Williamson
 */
public class SolderThickness
{

  // ------------------------------------------------------------------------
  //
  // The data for the solder thickness table forms a triangular matrix. To
  // save memory only enough memory is allocated for each row. During
  // lookups into the table this can cause a problem during interpolation.
  // Normally the integer values just below and above the floating point
  // value for background or deltagray are used to interpolate between
  // the 4 ppints in the table (bg1,dg1) (bg1,dg2) (bg2,dg1) (bg2,dg2).
  // Where bg=background and dg=deltagray which are the indexes into the table.
  // Since the parameters are passed in as floats and the table is indexed by
  // integers the integers both just before and just after the float parameter
  // are determined. Example: bg1<=background<=bg2, dg1<=deltagray<=dg2.
  // If dg1 = bg1 then we are at the triangular edge of the table and the
  // value at (bg1,dg2) doesn't exist. You would get an array index out
  // of bound exception. To address this two values for the deltagray index
  // just above the passed in parameter are used -- dg2a and dg2b. If we are at
  // the edge of the matrix then dg2b is limited to bg1. This diagram attempts
  // to show how this works for two cases. Case 1 is in the middle of the table
  // where all 4 values exist and case 2 is where we are at the edge.
  //
  //       +------+------+          +------+
  //       |      |      |          |      |
  //  bg1  | dg1  | dg2b |          | dg1  |
  //       |      |      |          | dg2b |
  //       +------+------+          +------|------+
  //       |      |      |          |      |      |
  //  bg2  | dg1  | dg2a |          | dg1  | dg2a |
  //       |      |      |          |      |      |
  //       +------+------+          +------+------+
  //
  //
  //------------------------------------------------------------------------
  //


  private static final int NUMGRAYLEVELS = 256;
  private static final int MAXGRAYLEVEL = NUMGRAYLEVELS - 1;
  private float[][] _datatable = null;
  private Image _dataImage = null;
  private float _maxThickness = 0.0F;
  private static Map<String, SolderThickness> _solderThicknessFilePathToSolderThicknessMap = new HashMap<String, SolderThickness>();
  private static String _prevSolderThicknessFilePath = null;
  
  /**
   * Return the named SolderThickness table.
   * There can be only one instance of each enumerated table. This
   * conserves memory and makes updates easier if we add
   * an actual measured cal to this later.
   * @param solderThicknessEnum Identifies the solder table to load.
   * @return The single instance of the specified table.
   * @author Eddie Williamson
   */
  public static synchronized SolderThickness getInstance(String solderThicknessFilePath)
  {
    Assert.expect(solderThicknessFilePath != null);
    
    Assert.expect(FileUtil.exists(solderThicknessFilePath), "Thickness table does not exist: " + solderThicknessFilePath);

    SolderThickness solderThickness = _solderThicknessFilePathToSolderThicknessMap.get(solderThicknessFilePath);
    
    if (Config.isDeveloperDebugModeOn())
    {
      if (solderThicknessFilePath.equals(_prevSolderThicknessFilePath) == false)
      {
        System.out.println("Running " + solderThicknessFilePath);
        _prevSolderThicknessFilePath = solderThicknessFilePath;
      }
    }
    if (solderThickness == null)
    {
      // table not constructed yet, let's do it
      try
      {
        solderThickness = new SolderThickness(solderThicknessFilePath);
      }
      catch (DatastoreException ex)
      {
        Assert.expect(false, ex.getMessage());
      }
      SolderThickness prevEntry = _solderThicknessFilePathToSolderThicknessMap.put(solderThicknessFilePath, solderThickness);
      Assert.expect(prevEntry == null);
    }
    return solderThickness;
  }

  /**
   * @author Eddie Williamson
   */
  private SolderThickness(String solderThicknessFilePath) throws DatastoreException
  {
    Assert.expect(solderThicknessFilePath != null);

    SolderThicknessFileReader solderThicknessFileReader = SolderThicknessFileReader.getInstance();
    solderThicknessFileReader.readDataFile(solderThicknessFilePath, MAXGRAYLEVEL, NUMGRAYLEVELS);
    _datatable = solderThicknessFileReader.getDataTable();
    _maxThickness = solderThicknessFileReader.getMaxThickness();
    createSolderThicknessImage();
  }

  /**
   * Give access to the thickness table image.
   * This image contains the same data as the table, and is used for some special algorithm optimizations.
   *
   * @author Patrick Lacz
   */
  public Image getThicknessTableAsImage()
  {
    Assert.expect(_dataImage != null);
    return _dataImage;
  }

  /**
   * Convert the data table into an image that contains the same structure.
   * This image will be used as a table that the IPP can use to quickly transform images to thickness images.
   * @author Patrick Lacz
   */
  private void createSolderThicknessImage()
  {
    Assert.expect(_datatable != null);
    Assert.expect(_maxThickness > 0.f);

    _dataImage = new Image(256, 256);

    Paint.fillImage(_dataImage, 0.f);

    ByteBuffer byteBuffer = _dataImage.getByteBuffer();
    byteBuffer.position(0);

    float fillerData[] = new float[256];

    Arrays.fill(fillerData, _maxThickness);

    for (int backgroundGraylevel = 0 ; backgroundGraylevel < 256 ; ++backgroundGraylevel)
    {
      byteBuffer.position(backgroundGraylevel*_dataImage.getStride());
      int lineLength = _datatable[backgroundGraylevel].length;
      FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
      floatBuffer.put(_datatable[backgroundGraylevel], 0, lineLength);
      floatBuffer.put(fillerData, 0, 256 - lineLength);
      byteBuffer.rewind();
    }
  }


  /**
   * Returns the solder thickness in mils for a given background
   * and delta gray level. Background and delta gray must be in the
   * range 0 to MAXGRAYLEVEL. Additionally deltagray <= background.
   * @return The solder thickness in mils interpolated from the table.
   * @param background The background gray level of the image.
   * @param deltagray The difference in gray level background to foreground of the feature being measured.
   * @author Eddie Williamson
   */
  public float getThicknessInMils(float background, float deltagray)
  {
    // XCR1748 - not calling anymore except main function and unit test 
    // @author Wei Chin
    return getThicknessInMils(background, deltagray, 0);
  }

  /**
   * @param background The background gray level of the image.
   * @param deltagray The difference in gray level background to foreground of the feature being measured.
   * @param backgroundOffset Use for improve the repeatability of some dark background image
   * 
   * @author Wei Chin  
   */
  public float getThicknessInMils(float background, float deltagray, int backgroundOffset)
  {
    float value;
    float backgroundFraction;
    float deltagrayFraction;
    float tmpRightSide;
    float tmpLeftSide;
    int background2;
    int deltagray2a;
    int deltagray2b;

    // For both background and deltagray the 1&2 (ie background1) refer to the
    // lower and upper integers on either side of the float parameter passed in.

    // Need the integer indexes on both sides of the floating point number
    // First truncate to get the lower
    int background1 = (int)background;
    int deltagray1 = (int)deltagray;
    
    background1 += backgroundOffset;
    if(background1 >= MAXGRAYLEVEL)
      background1 = MAXGRAYLEVEL - 1;

    Assert.expect((background1 >= 0) && (background1 <= MAXGRAYLEVEL));
    Assert.expect(deltagray1 >= 0);
    if (deltagray1 > background1)
    {
      return 0;
    }// since background1<=MAXGRAYLEVEL, deltagray1 is also <=MAXGRAYLEVEL
    // Now determine the other index
    // The details are tricky, see big comment at end of file
    // It even has pictures :)
    if (background1 < MAXGRAYLEVEL)
    {
      background2 = background1 + 1;
    }
    else
    {
      background2 = MAXGRAYLEVEL;
    }
    backgroundFraction = background - background1;

    if (deltagray1 < background1)
    {
      deltagray2a = deltagray1 + 1;
      deltagray2b = deltagray2a;
    }
    else
    {
      // at edge of triangular matrix
      deltagray2a = (deltagray1 == MAXGRAYLEVEL ? MAXGRAYLEVEL : deltagray1 + 1);
      deltagray2b = deltagray1;
    }
    deltagrayFraction = deltagray - deltagray1;

    // Iinterpolate between the four points in the lookup table
    tmpRightSide = _datatable[background1][deltagray2b] + (_datatable[background2][deltagray2a]
        - _datatable[background1][deltagray2b]) * backgroundFraction;

    tmpLeftSide = _datatable[background1][deltagray1] + (_datatable[background2][deltagray1]
        - _datatable[background1][deltagray1]) * backgroundFraction;

    // Now interpolate left to right
    value = tmpLeftSide + (tmpRightSide - tmpLeftSide) * deltagrayFraction;
    
    // thickness should not lower than 0
    if (value < 0.0f)
      value = 0.0f;
    
    return value;
  }

  /**
   * Gets the thickness value in millimeters for the specified background and delta gray values.
   * Presently, this works by getting the thickness value in it's native mil units and then converting
   * that to millimeters.
   *
   * @author Matt Wharton
   */
  public float getThicknessInMillimeters(float background, float deltagray, int backgroundSensitivityOffset)
  {
    /** @todo: mdw - at some point it may make sense to return millimeters directly from the table. */
    float thicknessInMils = getThicknessInMils(background, deltagray, backgroundSensitivityOffset);
    float thicknessInMillimeters = MathUtil.convertMilsToMillimeters(thicknessInMils);

    return thicknessInMillimeters;
  }

  /**
   * Searches the solder thickness table and interpolates to return
   * the delta gray level for the given background and thickness.
   * @return The interpolated difference in gray level background to foreground.
   * @param background The background gray level of the image.
   * @param thicknessInMils The solder thickness in mils of the feature being measured.
   * @author Eddie Williamson
   */
  public float getDeltaGrayLevel(float background, float thicknessInMils)
  {
    Assert.expect(thicknessInMils >= 0.0F);
    if (thicknessInMils > _maxThickness)
    {
      return (float)MAXGRAYLEVEL;
    }

    int background1;
    int background2;
    int deltagrayLowPoint;
    int deltagrayMidPoint;
    int deltagrayHighPoint;
    float backgroundFraction;
    float tmpLeftSide;
    float tmpThickness;
    float tmpRightSide;

    background1 = (int)background; // truncate to lower int
    Assert.expect((background1 >= 0) && (background1 <= MAXGRAYLEVEL));

    background2 = background1 + 1;
    background2 = Math.min(255, background2);
    backgroundFraction = background - background1;

    // binary search
    // Using the picture as reference
    // +-----+----+-----+----+------+
    // | low |    | mid |    | high |
    // +-----+----+-----+----+------+
    // If the thickness we are searching for is above the mid point we reset the low to
    // equal the mid. Then recalculate a new mid point. Loop until low and high converge.
    // Same if the thickness is less than the mid but the high is reset to equal the mid.

    deltagrayLowPoint = 0; // set to lowest low
    deltagrayHighPoint = background2; // set to highest high
    deltagrayMidPoint = deltagrayHighPoint / 2; // calculate initial mid point

    // low and high have converged if they are equal or differ by 1
    while (deltagrayHighPoint - deltagrayLowPoint > 1)
    {
      tmpThickness = _datatable[background1][deltagrayMidPoint] + (_datatable[background2][deltagrayMidPoint]
          - _datatable[background1][deltagrayMidPoint]) * backgroundFraction;

      if (tmpThickness >= thicknessInMils)
      {
        // thickness at mid point is too high, reset high to equal mid
        deltagrayHighPoint = deltagrayMidPoint;
      }
      else
      {
        // thickness at mid point is too low, reset low to equal mid
        deltagrayLowPoint = deltagrayMidPoint;
      }
      // calculate new mid point
      deltagrayMidPoint = (deltagrayLowPoint + deltagrayHighPoint) / 2;
    }

    // Now we have a low and high that bound the answer, interpolate to get it
    tmpLeftSide = _datatable[background1][deltagrayLowPoint] + (_datatable[background2][deltagrayLowPoint]
        - _datatable[background1][deltagrayLowPoint]) * backgroundFraction;

    tmpRightSide = _datatable[background1][deltagrayHighPoint] + (_datatable[background2][deltagrayHighPoint]
        - _datatable[background1][deltagrayHighPoint]) * backgroundFraction;

    return ((float)deltagrayLowPoint + (thicknessInMils - tmpLeftSide) / (tmpRightSide - tmpLeftSide));

  }
  
  /**
   * XCR-3237 Intermittent software crash when generate precision tuning image
   * verify if the solder thickness file is workable
   * @author Swee Yee Wong
   */
  public static boolean isSolderThicknessFileWorkable(String solderThicknessFilePath)
  {
    boolean fileWorkable = true;
    try
    {
      SolderThicknessFileReader.getInstance().verifyDataFile(solderThicknessFilePath, MAXGRAYLEVEL, NUMGRAYLEVELS);
    }
    catch (DatastoreException ex)
    {
      fileWorkable = false;
    }
    return fileWorkable;
  }

  public static void main(String[] args)
  {
    if (args.length != 2)
    {
      System.out.println("\nUSAGE: get_thickness <background_gray> <delta_gray>\n");
      return;
    }

    try
    {
      Float backgroudGray = new Float(args[0]);
      Float delatGray = new Float(args[1]);
      String solderThicknessFilePath = FileName.getCalibThicknessTableFullPath("M19", "Gain 1", SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER.getFileName(), 0);
      SolderThickness st = new SolderThickness(solderThicknessFilePath);
      float thickness = st.getThicknessInMils(backgroudGray, delatGray);
      System.out.println("\nSolderThickness = " + thickness);
    }
    catch (Exception ex)
    {
      System.out.println(ex.toString());
      ex.printStackTrace();
    }
  }

}