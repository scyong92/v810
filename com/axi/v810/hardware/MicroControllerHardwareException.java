package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MicroControllerHardwareException extends HardwareException 
{
    /**
     * @author Cheah Lee Herng
     */
    private MicroControllerHardwareException(MicroControllerHardwareExceptionEnum microControllerHardwareExceptionEnum, LocalizedString message)
    {
        super(message);

        _exceptionType = microControllerHardwareExceptionEnum;
        Assert.expect(microControllerHardwareExceptionEnum != null, "MicroControllerHardwareExceptionEnum is null.");
        Assert.expect(message != null, "Localized string is null.");
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private MicroControllerHardwareException(MicroControllerHardwareExceptionEnum microControllerHardwareExceptionEnum, String key, Object[] parameters)
    {
        this(microControllerHardwareExceptionEnum, new LocalizedString(key, parameters));
    }
    
    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationTimeoutException(String portName)
    {
        Assert.expect(portName != null, "portName is null.");

        String[] exceptionParameters = new String[] {portName};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_TIMEOUT,
                                                   "HW_MICRO_CONTROLLER_SERIAL_COMMUNICATION_TIMEOUT_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationNoSuchPortException(String portName)
    {
        Assert.expect(portName != null, "portName is null.");

        String[] exceptionParameters = new String[] {portName};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_NO_SUCH_PORT,
                                                   "HW_MICRO_CONTROLLER_SERIAL_COMMUNICATION_NO_SUCH_PORT_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationPortInUseException(String portName, String currentPortOwner)
    {
        Assert.expect(portName != null, "portName is null.");
        Assert.expect(currentPortOwner != null, "currentPortOwner is null.");

        String[] exceptionParameters = new String[] {portName, currentPortOwner};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_PORT_IN_USE,
                                                   "HW_MICRO_CONTROLLER_SERIAL_COMMUNICATION_PORT_IN_USE_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationUnsupportedParametersException(String portName,
                                                                                                  int baudRate,
                                                                                                  int dataBits,
                                                                                                  int stopBits,
                                                                                                  String parity)
    {
        Assert.expect(portName != null, "portName is null.");
        Assert.expect(parity != null, "parity is null");
        Assert.expect(baudRate > 0, "baudRate is less than or equal to 0");
        Assert.expect(dataBits >= 5, "dataBits is less than 5");
        Assert.expect(dataBits <= 8, "dataBits is more than 8");
        Assert.expect(stopBits >= 1, "stopBits is less than 1");
        Assert.expect(stopBits <= 3, "stopBits is more than 3");

        Object[] exceptionParameters = new Object[] {portName, new Integer(baudRate), new Integer(dataBits), new Integer(stopBits), parity};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_UNSUPPORTED_PARAMETERS,
                                                   "HW_MICRO_CONTROLLER_UNSUPPORTED_SERIAL_COMMUNICATION_PARAMETERS_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationUnsupportedReceiveTimeoutException(String portName,
                                                                                              int timeoutInMilliSeconds)
    {
        Assert.expect(portName != null, "portName is null.");
        Assert.expect(timeoutInMilliSeconds > 0, "timeoutInMilliSeconds is less than or equal to 0");

        Object[] exceptionParameters = new Object[] {portName, new Integer(timeoutInMilliSeconds)};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_UNSUPPORTED_RECEIVE_TIMEOUT,
                                                   "HW_MICRO_CONTROLLER_UNSUPPORTED_SERIAL_COMMUNICATION_RECEIVE_TIMEOUT_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationErrorException(String portName)
    {
        Assert.expect(portName != null, "portName is null.");

        String[] exceptionParameters = new String[] {portName};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_ERROR,
                                                   "HW_MICRO_CONTROLLER_SERIAL_COMMUNICATION_ERROR_EXCEPTION_KEY",
                                                   exceptionParameters);
    }

    /**
     * @author Cheah Lee Herng
    */
    static MicroControllerHardwareException getSerialCommunicationProtocolHardwareException(String portName,
                                                                                     String expectedReply,
                                                                                     String actualReply)
    {
        Assert.expect(portName != null, "portName is null.");
        Assert.expect(expectedReply != null, "expectedReply is null");
        Assert.expect(actualReply != null, "actualReply is null");

        String[] exceptionParameters = new String[] {portName, expectedReply, actualReply};
        return new MicroControllerHardwareException(MicroControllerHardwareExceptionEnum.SERIAL_COMMUNICATION_PROTOCOL_ERROR,
                                                   "HW_MICRO_CONTROLLER_SERIAL_COMMUNICATION_PROTOCOL_ERROR_EXCEPTION_KEY",
                                                   exceptionParameters);
    }
}
