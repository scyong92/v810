package com.axi.v810.hardware;

import java.net.*;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 * This class is used by the XrayCamera.setScanPath() method
 * @author Bill Darbie
 */
public class ProjectionRegion
{
  private ImageRectangle _projectionRegionRectangle;
  // RMS: We need to explicitly use the long package name for ImageReconstructionEngineEnum
  // until it gets moved into hardware package.
  private ImageReconstructionEngineEnum _projectionDataDestinationId;
  private InetAddress _projectionDataDestinationAddress;
  private int _startPortNumber;

  /**
   * Hold data required to tell the camera how to break up a scan into separate images
   * Each image will be sent to a specific processing node.
   * @param projectionRegionRectangle is the Rectangle2D which encompasses this ProjectionRegion
   * @param projectionDataDestinationId is the InetAddress of the destination to where the
   *        data is send to
   *
   * @author Bill Darbie
   * @author Rex Shang
   * @author Matt Wharton
   */
  public ProjectionRegion(ImageRectangle projectionRegionRectangle,
                          ImageReconstructionEngineEnum projectionDataDestinationId,
                          InetAddress projectionDataDestinationAddress)
  {    
    this(projectionRegionRectangle, projectionDataDestinationId, projectionDataDestinationAddress, 0);
  }
  
 /**
   * @author Bee Hoon
   * 
   * Single IRP - Added ire in order to get the current start port number
   */
  public ProjectionRegion(ImageRectangle projectionRegionRectangle,
                          ImageReconstructionEngineEnum projectionDataDestinationId,
                          InetAddress projectionDataDestinationAddress,
                          int startPortNumber)
  {
    Assert.expect( projectionRegionRectangle != null );
    Assert.expect( projectionRegionRectangle.getMinX() >= 0 );
    Assert.expect( projectionRegionRectangle.getMinY() >= 0 );
    Assert.expect( projectionDataDestinationId != null );
    Assert.expect( projectionDataDestinationAddress != null );

    _projectionRegionRectangle = projectionRegionRectangle;
    _projectionDataDestinationId = projectionDataDestinationId;
    _projectionDataDestinationAddress = projectionDataDestinationAddress;
    _startPortNumber = startPortNumber;
  }

  /**
   * Returns the ImageRectangle which encompasses this ProjectionRegion.
   *
   * @author Matt Wharton
   */
  public ImageRectangle getProjectionRegionRectangle()
  {
    return _projectionRegionRectangle;
  }

  /**
  * @author Matt Wharton
  */
  public InetAddress getProjectionDataDestinationAddress()
  {
    return _projectionDataDestinationAddress;
  }

  /**
   * @author Rex Shang
   */
  public ImageReconstructionEngineEnum getProjectionDataDestinationId()
  {
    return _projectionDataDestinationId;
  }
  
  /**
   * @author Bee Hoon
   * Return zero when IRE = null (4 IRPs with Master Controller)
   * Return the start port according to the IRE (Single Server)
   */
  public int getStartPortNumber()
  {
    return _startPortNumber == 0 ? 0 : _startPortNumber;
  }
  
   /**
   * @author Kok Chun, Tan
   */
  public void setStartPortNumber(int startPortNumber)
  {
    Assert.expect(startPortNumber >= 0 && startPortNumber < 65536);
    _startPortNumber = startPortNumber;
  }

}
