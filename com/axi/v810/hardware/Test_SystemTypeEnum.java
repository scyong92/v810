package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class Test_SystemTypeEnum extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SystemTypeEnum());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    List<SystemTypeEnum> systemTypes = SystemTypeEnum.getAllSystemTypes();
    Expect.expect(systemTypes.size() == 4); // XCR-3604 Unit Test Phase 2

    for(SystemTypeEnum systemType : systemTypes)
    {
      if(systemType.equals(SystemTypeEnum.STANDARD))
      {
        Expect.expect(systemType.getName().equals("standard"));
        Expect.expect(systemType.equals(SystemTypeEnum.getSystemType(systemType.getName())));
      }
      else if(systemType.equals(SystemTypeEnum.THROUGHPUT))
      {
        Expect.expect(systemType.getName().equals("throughput"));
        Expect.expect(systemType.equals(SystemTypeEnum.getSystemType(systemType.getName())));
      }
      else if(systemType.equals(SystemTypeEnum.XXL))
      {
        Expect.expect(systemType.getName().equals("XXL"));
        Expect.expect(systemType.equals(SystemTypeEnum.getSystemType(systemType.getName())));
      }
      else if(systemType.equals(SystemTypeEnum.S2EX)) // XCR-3604 Unit Test Phase 2
      {
        Expect.expect(systemType.getName().equals("S2EX"));
        Expect.expect(systemType.equals(SystemTypeEnum.getSystemType(systemType.getName())));
      }
      else
        Assert.expect(false, "unkown system type: " + systemType.getName());
    }
  }
}
