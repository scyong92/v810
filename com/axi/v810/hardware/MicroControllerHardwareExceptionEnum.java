package com.axi.v810.hardware;

/**
 * @author Cheah Lee Herng
 */
public class MicroControllerHardwareExceptionEnum extends HardwareExceptionEnum
{
    private static int _index = -1;
    
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_ERROR = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_TIMEOUT = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_NO_SUCH_PORT = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_PORT_IN_USE = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_PROTOCOL_ERROR = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_UNSUPPORTED_PARAMETERS = new MicroControllerHardwareExceptionEnum(++_index);
    public static MicroControllerHardwareExceptionEnum SERIAL_COMMUNICATION_UNSUPPORTED_RECEIVE_TIMEOUT = new MicroControllerHardwareExceptionEnum(++_index);
    
    /**
     * @author Cheah Lee Herng
    */
    private MicroControllerHardwareExceptionEnum(int id)
    {
        super(id);
    }
}
