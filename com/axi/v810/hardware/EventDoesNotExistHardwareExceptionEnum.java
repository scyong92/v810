package com.axi.v810.hardware;

/**
 * @author Greg Esparza
 */
public class EventDoesNotExistHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static EventDoesNotExistHardwareExceptionEnum EXCEPTION_EVENT_DOES_NOT_EXIST = new EventDoesNotExistHardwareExceptionEnum(++_index);

  /**
   * @author Greg Esparza
   */
  private EventDoesNotExistHardwareExceptionEnum(int id)
  {
    super(id);
  }
}
