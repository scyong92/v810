package com.axi.v810.hardware;

import com.axi.util.*;
import java.util.HashMap;

/**
 * This class is used to help HardwareTimeOutException to identify which
 * specific timeout has happend.
 * @author Rex Shang
 */
public class HardwareTimeoutExceptionEnum extends com.axi.util.Enum
{
  private static HashMap<HardwareTimeoutExceptionEnum, LocalizedString> _enumToStringMap;
  private static int _index = -1;

  public static HardwareTimeoutExceptionEnum WAIT_FOR_PANEL_TO_BE_IN_PLACE = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_DOWNSTREAM_SYSTEM_TO_FINISH_ACCEPT_PANEL = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_XRAY_SOURCE = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_SYNTHETIC_CAMERA_TRIGGERS_OFF = new HardwareTimeoutExceptionEnum(++_index);
  public static HardwareTimeoutExceptionEnum WAIT_FOR_DOWNSTREAM_SENSOR_SIGNAL_CLEAR = new HardwareTimeoutExceptionEnum(++_index);

  /**
   * @author Rex Shang
   */
  static
  {
    _enumToStringMap = new HashMap<HardwareTimeoutExceptionEnum, LocalizedString>(_index + 1);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_PANEL_TO_BE_IN_PLACE, new LocalizedString(
        "HW_WAIT_FOR_PANEL_TO_BE_IN_PLACE_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR, new LocalizedString(
        "HW_WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK, new LocalizedString(
        "HW_WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM, new LocalizedString(
        "HW_WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL, new LocalizedString(
        "HW_WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_DOWNSTREAM_SYSTEM_TO_FINISH_ACCEPT_PANEL, new LocalizedString(
        "HW_WAIT_FOR_DOWNSTREAM_SYSTEM_TO_FINISH_ACCEPT_PANEL_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_XRAY_SOURCE, new LocalizedString(
        "HW_WAIT_FOR_XRAY_SOURCE_TIMEOUT_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_SYNTHETIC_CAMERA_TRIGGERS_OFF, new LocalizedString(
        "HW_WAIT_FOR_WAIT_FOR_SYNTHETIC_CAMERA_TRIGGERS_OFF_EXCEPTION_KEY", null)) == null);
    Assert.expect(_enumToStringMap.put(WAIT_FOR_DOWNSTREAM_SENSOR_SIGNAL_CLEAR, new LocalizedString(
        "HW_WAIT_FOR_SMEMA_CLEAR_EXCEPTION_KEY", null)) == null);

    Assert.expect(_enumToStringMap.size() == _index + 1);
  }

  /**
   * @author Rex Shang
   */
  private HardwareTimeoutExceptionEnum(int id)
  {
    super(id);
  }

  /**
   * @author Rex Shang
   */
  public static LocalizedString getLocalizedString(HardwareTimeoutExceptionEnum hardwareTimeoutExceptionEnum)
  {
    LocalizedString myString = _enumToStringMap.get(hardwareTimeoutExceptionEnum);

    Assert.expect(myString != null);
    return myString;
  }

}
