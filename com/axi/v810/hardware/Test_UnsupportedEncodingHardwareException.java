package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for UnsupportedEncodingHardwareException.
* @author Bob Balliew
*/
public class Test_UnsupportedEncodingHardwareException extends UnitTest
{

  /**
  * @author Bob Balliew
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_UnsupportedEncodingHardwareException());
  }

  /**
  * @author Bob Balliew
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    {
      HardwareException ex = new UnsupportedEncodingHardwareException("COM2");
      String localized = ex.getLocalizedMessage();
      os.println("                                                                                                   111111111111111111111");
      os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
      os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
      os.println("<-start localized------------------------------------------------------------------------------------------------------>");
      os.println(localized);
      os.println("<-end localized-------------------------------------------------------------------------------------------------------->");
      os.println("                                                                                                   111111111111111111111");
      os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
      os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
      int lineLength = 80;
      os.println("<-start format(" + lineLength + ")----------------------------------------------------------------------------------------------------->");
      os.println(StringUtil.format(localized, lineLength));
      os.println("<-end format(" + lineLength + ")------------------------------------------------------------------------------------------------------->");
    }
    {
      HardwareException ex = new UnsupportedEncodingHardwareException("COM1", "detailed message from UnsupportedEncodingExceptionException");
      String localized = ex.getLocalizedMessage();
      os.println("                                                                                                   111111111111111111111");
      os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
      os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
      os.println("<-start localized------------------------------------------------------------------------------------------------------>");
      os.println(localized);
      os.println("<-end localized-------------------------------------------------------------------------------------------------------->");
      os.println("                                                                                                   111111111111111111111");
      os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
      os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
      int lineLength = 80;
      os.println("<-start format(" + lineLength + ")----------------------------------------------------------------------------------------------------->");
      os.println(StringUtil.format(localized, lineLength));
      os.println("<-end format(" + lineLength + ")------------------------------------------------------------------------------------------------------->");
    }
  }
}
