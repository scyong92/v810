package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is a temporary internal tool that is used help with collecting projections through Genesis 1.00.
 * However, the long term goal is to remove this command line oriented program and replace with a real Projection Collector tool
 * that would be part the the Service UI.  Hopefully, we can get this in at Genesis 1.50.
 *
 * @author Greg Esparza
 */
class PanelPositionerUtil extends UnitTest
{
  private static final String _EMPTY_STRING = "";
  private static final int _SCAN_STEP_DIRECTION_LEFT_TO_RIGHT = 1;
  private static final int _SCAN_STEP_DIRECTION_RIGHT_TO_LEFT = -1;
  private static final int _FIRST_SCAN_PASS_DIRECTION_POSITIVE = 1;
  private static final int _FIRST_SCAN_PASS_DIRECTION_NEGATIVE = -1;
  private static final String _VARIABLE_SCAN_STEP_FILE_NAME = "variableScanStep.txt";
  private static final String _VARIABLE_SCAN_PASS_FILE_NAME = "variableScanPass.txt";

  private PanelPositioner _panelPositioner = null;
  private String[] _args = null;
  private boolean _runScanPath;
  private boolean _runPointToPointMove;
  private boolean _runHome;
  private boolean _consoleOutputEnabled;
  private boolean _runInitialize;
  private boolean _runScanWithKeyboard;
  private int _scanStepDirection;
  private int _firstScanPassDirection;
  private int _scanPathsToRun;
  private int _numberOfScanPasses;
  private int _xPositionInNanometers;
  private int _yPositionInNanometers;
  private int _distanceInNanometersPerPulse;
  private int _scanStepInNanometers;
  private int _scanPassLengthInNanometers;
  private int _scanPassDelayInMilliseconds;
  private int _xAxisScanPathOrigin;
  private int _yAxisScanPathOrigin;
  private Config _config = Config.getInstance();
  private ScanMotionProfileEnum _scanMotionProfileEnum;
  private boolean _runVariableScanStep;
  private boolean _runVariableScanPass;
  private BufferedReader _bufferedReader = null;
  private String _scanStepFileNameFullPath = null;
  private String _scanPassFileNameFullPath = null;
  private int _numberOfVariableScanSteps;
  private boolean _resetVariableScanStep;
  private boolean _resetVariableScanPass;
  private HardwareWorkerThread _hardwareWorkerThread;
  private int _firstScanPassDelayInMilliseconds;
  private boolean _debugMode;

  /**
   * @author Greg Esparza
   */
  public PanelPositionerUtil(String[] args)
  {
    _args = args;
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }


    _panelPositioner = PanelPositioner.getInstance();
    _runScanPath = false;
    _runPointToPointMove = false;
    _runHome = false;
    _runInitialize = true;
    _runScanWithKeyboard = false;
    _scanStepDirection = _SCAN_STEP_DIRECTION_LEFT_TO_RIGHT;
    _firstScanPassDirection = _FIRST_SCAN_PASS_DIRECTION_POSITIVE;
    _numberOfScanPasses = 0;
    _xPositionInNanometers = 0;
    _yPositionInNanometers = 0;
    _scanPathsToRun = 0;
    _distanceInNanometersPerPulse = 0;
    _scanStepInNanometers = 0;
    _scanPassLengthInNanometers = 0;
    _scanPassDelayInMilliseconds = 0;
    _xAxisScanPathOrigin = -1;
    _yAxisScanPathOrigin = -1;
    _consoleOutputEnabled = false;
    
      //Ngie Xing
      _scanMotionProfileEnum = ScanMotionProfileEnum.getScanMotionProfileEnum(_config.getSystemCurrentLowMagnification());
    
    _runVariableScanStep = false;
    _scanStepFileNameFullPath = Directory.getConfigDir() + File.separator + _VARIABLE_SCAN_STEP_FILE_NAME;
    _scanPassFileNameFullPath = Directory.getConfigDir() + File.separator + _VARIABLE_SCAN_PASS_FILE_NAME;
    _numberOfVariableScanSteps = 0;
    _resetVariableScanStep = false;
    _resetVariableScanPass = false;
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _firstScanPassDelayInMilliseconds = 0;
    _debugMode = false;
  }


  /**
   * @author Greg Esparza
   */
  private ArrayList<ScanPass> getVariableScanPassList() throws IOException, BadFormatException
  {
    String textLine = "";
    ArrayList<ScanPass> scanPassList = new ArrayList<ScanPass>();

    try
    {
      if (_resetVariableScanPass)
      {
        _bufferedReader.reset();
        _numberOfScanPasses = 0;
        _resetVariableScanPass = false;
      }

      if (_bufferedReader == null)
      {
        _bufferedReader = new BufferedReader(new FileReader(_scanPassFileNameFullPath));

        System.out.println("Using variable scan pass file: " + _scanPassFileNameFullPath);

        // mark the beginning of the file so the _buffered.reset() method will bring
        // us back to the beginning of the file any time.
        // Note that we must set the read ahead limit to be 1 larger than the file
        // so the _bufferedReader.reset() method is guaranteed to work
        File file = new File(_scanPassFileNameFullPath);
        long readAheadLimit = file.length() + 1;
        Assert.expect(readAheadLimit <= Integer.MAX_VALUE);
        _bufferedReader.mark((int)readAheadLimit);
      }
    }
    catch (FileNotFoundException fnfe)
    {
      System.out.println("The scan pass file: " + _scanPassFileNameFullPath + " was not found.");
      throw new IOException();
    }

    textLine = _bufferedReader.readLine();

    _numberOfScanPasses = 0;

    while (textLine != null)
    {
      String scanPassPositionsStr = textLine;
      String[] scanPassPositionArray = scanPassPositionsStr.split(",");
      StagePositionMutable startPosition = new StagePositionMutable();
      StagePositionMutable endPosition = new StagePositionMutable();

      startPosition.setXInNanometers(StringUtil.convertStringToInt(scanPassPositionArray[0]));
      startPosition.setYInNanometers(StringUtil.convertStringToInt(scanPassPositionArray[1]));
      endPosition.setXInNanometers(StringUtil.convertStringToInt(scanPassPositionArray[2]));
      endPosition.setYInNanometers(StringUtil.convertStringToInt(scanPassPositionArray[3]));

      ScanPass scanPass = new ScanPass(1, startPosition, endPosition);
      scanPassList.add(scanPass);

      ++_numberOfScanPasses;

      textLine = _bufferedReader.readLine();
    }

    if (_numberOfScanPasses == 0)
    {
      System.out.println("There are no scan passes defined in " + _scanPassFileNameFullPath);
      throw new IOException();
    }

    return scanPassList;
  }

  /**
   * @author Greg Esparza
   */
  private int getNextVariableScanStepInNanometers() throws IOException, BadFormatException
  {
    String textLine = "";
    int variableScanStepInNanometers = 0;

    try
    {
      if (_resetVariableScanStep)
      {
        _bufferedReader.reset();
        _numberOfVariableScanSteps = 0;
        _resetVariableScanStep = false;
      }

      if (_bufferedReader == null)
      {
        _bufferedReader = new BufferedReader(new FileReader(_scanStepFileNameFullPath));

        System.out.println("Using variable scan step file: " + _scanStepFileNameFullPath);

        // mark the beginning of the file so the _buffered.reset() method will bring
        // us back to the beginning of the file any time.
        // Note that we must set the read ahead limit to be 1 larger than the file
        // so the _bufferedReader.reset() method is guaranteed to work
        File file = new File(_scanStepFileNameFullPath);
        long readAheadLimit = file.length() + 1;
        Assert.expect(readAheadLimit <= Integer.MAX_VALUE);
        _bufferedReader.mark((int)readAheadLimit);
      }
    }
    catch (FileNotFoundException fnfe)
    {
      System.out.println("The scan step file: " + _scanStepFileNameFullPath + " was not found.");
      throw new IOException();
    }

    textLine = _bufferedReader.readLine();

    if (textLine != null)
    {
      variableScanStepInNanometers = StringUtil.convertStringToInt(textLine);
      ++_numberOfVariableScanSteps;
    }
    else if ((textLine == null) && (_numberOfScanPasses > _numberOfVariableScanSteps))
    {
      System.out.println("The number of scan passes exceeds the number of defined variable scan steps: " +
                         "\n\n" +
                         "Number of scan passes: " + String.valueOf(_numberOfScanPasses) +
                         "\n" +
                         "Number of defined variable scan steps: " + String.valueOf(_numberOfVariableScanSteps));

      throw new IOException();
    }

    return variableScanStepInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  private void setScanPath() throws XrayTesterException, IOException, BadFormatException
  {
    // First, set the scan motion profile
    _panelPositioner.setMotionProfile(new MotionProfile(_scanMotionProfileEnum));

    StagePositionMutable startPosition = null;
    StagePositionMutable endPosition = null;
    ScanPass currentScanPass = null;
    List<ScanPass> scanPath = new LinkedList<ScanPass>();
    ArrayList<ScanPass> variableScanPasses = null;

    int xStartScanPassInNanometers = _xAxisScanPathOrigin;
    int yStartScanPassInNanometers = _yAxisScanPathOrigin;
    int xEndScanPassInNanometers = _xAxisScanPathOrigin;
    int yEndScanPassInNanometers = _yAxisScanPathOrigin + (_firstScanPassDirection * _scanPassLengthInNanometers);

    int scanStepFactor = 0;

    if (_runVariableScanPass)
      variableScanPasses = getVariableScanPassList();

    for (int i = 0; i < _numberOfScanPasses; ++i)
    {
      startPosition = new StagePositionMutable();
      endPosition = new StagePositionMutable();

      if (_runVariableScanPass)
      {
        _scanStepDirection = 0;

         ScanPass scanPass = variableScanPasses.get(i);
         StagePosition scanPassStartPosition = scanPass.getStartPointInNanoMeters();
         StagePosition scanPassEndPosition = scanPass.getEndPointInNanoMeters();

         xStartScanPassInNanometers = scanPassStartPosition.getXInNanometers();
         yStartScanPassInNanometers = scanPassStartPosition.getYInNanometers();
         xEndScanPassInNanometers = scanPassEndPosition.getXInNanometers();
         yEndScanPassInNanometers = scanPassEndPosition.getYInNanometers();
      }

      if (_runVariableScanStep)
      {
        // The first scan pass does not require a scan step so we don't have to
        // get a step value out of the file.  Also set the factor to 0.
        // Otherwise, get the scan step value out of the file for all remaining passes
        // set the factor to be 1.
        if (i == 0)
        {
          _scanStepInNanometers = 0;
          scanStepFactor = i;
        }
        else
        {
          _scanStepInNanometers += getNextVariableScanStepInNanometers();
          scanStepFactor = 1;
        }
      }
      else
        // Always use the value of "i" as the multiplier for a fixed scan step.
        scanStepFactor = i;

      if ((i % 2) == 0)
      {
        // All the scan passes going in the positive Y-axis direction
        startPosition.setXInNanometers(xStartScanPassInNanometers + ((_scanStepDirection) * (scanStepFactor * _scanStepInNanometers)));
        startPosition.setYInNanometers(yStartScanPassInNanometers);
        endPosition.setXInNanometers(xEndScanPassInNanometers + ((_scanStepDirection) * (scanStepFactor * _scanStepInNanometers)));
        endPosition.setYInNanometers(yEndScanPassInNanometers);

      }
      else
      {
        // All the scan passes going in the negative Y-axis direction
        startPosition.setXInNanometers(xStartScanPassInNanometers + ((_scanStepDirection) * (scanStepFactor * _scanStepInNanometers)));
        startPosition.setYInNanometers(yEndScanPassInNanometers);
        endPosition.setXInNanometers(xEndScanPassInNanometers + ((_scanStepDirection) * (scanStepFactor * _scanStepInNanometers)));
        endPosition.setYInNanometers(yStartScanPassInNanometers);
      }

      // Create a new scan pass.
      currentScanPass = new ScanPass(i, startPosition, endPosition);
      // Update the scan path with this scan pass
      scanPath.add(currentScanPass);

      currentScanPass = null;
      startPosition = null;
      endPosition = null;
    }

    int numberOfRequiredSampleTriggers = 0;
    if(XrayTester.isS2EXEnabled() && XrayTester.isLowMagnification())
    {
      //Ngie Xing
      if (Config.getSystemCurrentLowMagnification().contains("M23"))
        numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS_FOR_M23);
      else
        numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS);
    }
    else
    {
        numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS);
    }
    System.out.println("numberOfRequiredSampleTriggers from PanelPositioner" + numberOfRequiredSampleTriggers);
    // Swee Yee Wong - Include starting pulse for scan pass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    _panelPositioner.enableScanPath(scanPath, _distanceInNanometersPerPulse, numberOfRequiredSampleTriggers);
    _panelPositioner.setPositionPulse(_distanceInNanometersPerPulse, numberOfRequiredSampleTriggers); //_config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS));
    
    _resetVariableScanStep = true;
    _resetVariableScanPass = true;
  }

  /**
   * @author Greg Esparza
   */
  private void getKeyboardCommand(String commandMessage) throws IOException
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String keyboardString = "";

    do
    {
      System.out.println();
      System.out.println(commandMessage);

      keyboardString = br.readLine();
      if (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false)
      {
        System.out.println("Invalid key!");
      }
    }
    while (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testScanPath() throws XrayTesterException, IOException, BadFormatException
  {
    for (int scanLoop = 0; scanLoop < _scanPathsToRun; ++scanLoop)
    {
      if (_consoleOutputEnabled)
      {
        System.out.println("");
        System.out.println("Running scan path #" + String.valueOf(scanLoop + 1) + " of " + String.valueOf(_scanPathsToRun) + " ...");
      }

      setScanPath();

      int scanPathExecutionTimeInMilliseconds = _panelPositioner.getScanPathExecutionTimeInMilliseconds();

      for(int i = 0; i < _numberOfScanPasses; ++i)
      {
        if (_runScanWithKeyboard)
          getKeyboardCommand("Press <ENTER> to run the next scan pass ...");

        _panelPositioner.runNextScanPass();

        if (_consoleOutputEnabled)
          System.out.print("*** Scan Pass #" + String.valueOf(i + 1) + " ...");

        try
        {
          // pretend that we are off checking the IRPs, Cameras, etc
          if (_runScanWithKeyboard == false)
          {
            // Wait twice as long on the first pass to allow the stage to
            // move to the start of the pass as well as run the first pass.
            if (i == 0)
              Thread.sleep(_firstScanPassDelayInMilliseconds);
            else
              Thread.sleep(_scanPassDelayInMilliseconds);
          }
        }
        catch (InterruptedException ie)
        {
          // do nothing
        }

        if (_consoleOutputEnabled)
          System.out.println(" done.");
      }

      long timeOutInMilliseconds = scanPathExecutionTimeInMilliseconds + 30000;
      long elapsedTimeInMilliseconds = 0;
      TimerUtil timerUtil = new TimerUtil();
      timerUtil.reset();

      boolean scanPathDone = false;
      do
      {
        timerUtil.start();
        scanPathDone = _panelPositioner.isScanPathDone();
        timerUtil.stop();
        elapsedTimeInMilliseconds = timerUtil.getElapsedTimeInMillis();
      }
      while((scanPathDone == false) && (elapsedTimeInMilliseconds <= timeOutInMilliseconds));

      if (elapsedTimeInMilliseconds > timeOutInMilliseconds)
      {
        System.out.println();
        System.out.println("Timeout waiting for scan path to complete.");
        System.out.println();
      }
      // We are all done with the scan path so disable the current scan path

      if (_debugMode)
        getKeyboardCommand("DEBUG MODE - Press <ENTER> to clean up scan information ...");

      _panelPositioner.disableScanPath();

      if (_consoleOutputEnabled)
        System.out.println("Scan Path #" + String.valueOf(scanLoop + 1) + " done.");
    }
  }

  /**
   * @author Greg Esparza
   */
  private void pointToPointMove() throws XrayTesterException
  {
    _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
    StagePositionMutable targetPosition = new StagePositionMutable();

    targetPosition.setXInNanometers(_xPositionInNanometers);
    targetPosition.setYInNanometers(_yPositionInNanometers);

    if (_consoleOutputEnabled)
    {
      System.out.println("");
      System.out.println("Moving to X = " + String.valueOf(_xPositionInNanometers) + " Y = " + String.valueOf(_yPositionInNanometers));
    }
    _panelPositioner.pointToPointMoveAllAxes(targetPosition);
  }

  /**
   * @author Greg Esparza
   */
  private void testInitialize() throws XrayTesterException
  {
    _panelPositioner.startup();
  }

  /**
   * @author Greg Esparza
   */
  private void commandLineUsage()
  {
    System.out.println("");
    System.out.println("Usage: ");
    System.out.println("[-benchTest | -benchmarkTest | -fullWorkspaceTest]");
    System.out.println("[-home]");
    System.out.println("[-scan]");
    System.out.println("[-move <X-axis in nanometers> <Y-axis in nanometers]");
    System.out.println("[-scanPathRepeat <number of scan paths>]");
    System.out.println("[-positionPulse <nanometers per pulse>]");
    System.out.println("[-scanPasses <number of scan passes>]");
    System.out.println("[-scanPassLength <length in nanometers>]");
    System.out.println("[-scanPassDelay <delay in milliseconds>]");
    System.out.println("[-scanStepSize <scan step in nanometers>]");
    System.out.println("[-scanPathOrigin <X-axis in nanometers> <Y-axis in nanometers>]");
    System.out.println("[-enableConsole]");
    System.out.println("[-scanWithKeyboard]");
    System.out.println("[-scanStepLeftToRight | -scanStepRightToLeft]");
    System.out.println("[-runVariableScanStep]");
    System.out.println("[-runVariableScanPass]");
    System.out.println("[-debugMode]");
    System.exit(1);
  }

  /**
   * @author Greg Esparza
   */
  private void setConsoleOutputEnable()
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-enableConsole"))
      {
        argFound = true;
        break;
      }
    }

    if (argFound == true)
      _consoleOutputEnabled = true;
    else
      _consoleOutputEnabled = false;
  }

  /**
   * @author Greg Esparza
   */
  private void setTestOption() throws HardwareException, BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scan"))
      {
        _runScanPath = true;
        argFound = true;
      }
      else if (_args[i].equalsIgnoreCase("-home"))
      {
        _runHome = true;
        argFound = true;
      }
      else if (_args[i].equalsIgnoreCase("-move"))
      {
        _xPositionInNanometers = StringUtil.convertStringToInt(_args[i+1]);
        _yPositionInNanometers = StringUtil.convertStringToInt(_args[i+2]);
        _runPointToPointMove = true;
        argFound = true;
      }

      if (argFound)
        break;
    }

    // We could not find the test option in the list
    if (argFound == false)
    {
      List<String> parameters = new ArrayList<String>();
      throw new MotionControlHardwareException("HW_DOES_NOT_EXIST_KEY", parameters);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setScanPathRepeat() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanPathRepeat"))
      {
        argFound = true;
        _scanPathsToRun = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _scanPathsToRun = 1;
  }

  /**
   * @author Greg Esparza
   */
  private void setPositionPulse() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-positionPulse"))
      {
        argFound = true;
        _distanceInNanometersPerPulse = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _distanceInNanometersPerPulse = 19000;

    // Make sure the motion profile is set for the correct magnification
    try
    {
      if(MagnificationEnum.getCurrentNorminal().equals(MagnificationEnum.NOMINAL))
      {
        String zAxisMotorPositionForLowMag = Config.getSystemCurrentLowMagnification();
        _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForLowMag)));
      }
      else if (MagnificationEnum.getCurrentNorminal().equals(MagnificationEnum.H_NOMINAL))
      {
        String zAxisMotorPositionForHighMag = Config.getSystemCurrentHighMagnification();
        _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForHighMag)));
      }
    }
    catch (XrayTesterException xte)
    {
      // do nothing
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setNumberOfScanPasses() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanPasses"))
      {
        argFound = true;
        _numberOfScanPasses = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _numberOfScanPasses = 1;
  }

  /**
   * @author Greg Esparza
   */
  private void setScanStepSizeInNanometers() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanStepSize"))
      {
        argFound = true;
        _scanStepInNanometers = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _scanStepInNanometers = 0;
  }

  /**
   * @author Greg Esparza
   */
  private void setScanPathOrigin() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanPathOrigin"))
      {
        argFound = true;
        _xAxisScanPathOrigin = StringUtil.convertStringToInt(_args[i+1]);
        _yAxisScanPathOrigin = StringUtil.convertStringToInt(_args[i+2]);
        break;
      }
    }

    if (argFound == false)
    {
      _xAxisScanPathOrigin = 25000000;
      _yAxisScanPathOrigin = 25000000;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setScanPassLengthInNanometers() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanPassLength"))
      {
        argFound = true;
        _scanPassLengthInNanometers = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _scanPassLengthInNanometers = 308610000;
  }

  /**
   * @author Greg Esparza
   */
  private void setScanPassDelayInMilliseconds() throws BadFormatException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanPassDelay"))
      {
        argFound = true;
        _scanPassDelayInMilliseconds = StringUtil.convertStringToInt(_args[i+1]);
        break;
      }
    }

    if (argFound == false)
      _scanPassDelayInMilliseconds = 100;
  }

  /**
 * @author Greg Esparza
 */
private void setFirstScanPassDelayInMilliseconds() throws BadFormatException
{
  boolean argFound = false;

  for (int i = 0; i < _args.length; ++i)
  {
    if (_args[i].equalsIgnoreCase("-firstScanPassDelay"))
    {
      argFound = true;
      _firstScanPassDelayInMilliseconds = StringUtil.convertStringToInt(_args[i+1]);
      break;
    }
  }

  if (argFound == false)
    _firstScanPassDelayInMilliseconds = _scanPassDelayInMilliseconds;
}


  /**
   * @author Greg Esparza
   */
  private void setScanKeyboardControl() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanWithKeyboard"))
      {
        argFound = true;
        _runScanWithKeyboard = true;
        break;
      }
    }

    if (argFound == false)
      _runScanWithKeyboard = false;
  }

  /**
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  private void setScanStepDirection() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-scanStepLeftToRight"))
      {
        argFound = true;
        _scanStepDirection = _SCAN_STEP_DIRECTION_LEFT_TO_RIGHT;
        break;
      }
      else if (_args[i].equalsIgnoreCase("-scanStepRightToLeft"))
      {
        argFound = true;
        _scanStepDirection = _SCAN_STEP_DIRECTION_RIGHT_TO_LEFT;
        break;
      }
    }

    if (argFound == false)
      _scanStepDirection = _SCAN_STEP_DIRECTION_LEFT_TO_RIGHT;
  }

  /**
 *
 * @throws HardwareException
 * @author Greg Esparza
 */
private void setFirstScanPassDirection() throws HardwareException
{
  boolean argFound = false;

  for (int i = 0; i < _args.length; ++i)
  {
    if (_args[i].equalsIgnoreCase("-firstScanPassPositive"))
    {
      argFound = true;
      _firstScanPassDirection = _SCAN_STEP_DIRECTION_LEFT_TO_RIGHT;
      break;
    }
    else if (_args[i].equalsIgnoreCase("-firstScanPassNegative"))
    {
      argFound = true;
      _firstScanPassDirection = _SCAN_STEP_DIRECTION_RIGHT_TO_LEFT;
      break;
    }
  }

  if (argFound == false)
    _firstScanPassDirection = _FIRST_SCAN_PASS_DIRECTION_POSITIVE;
}

  /**
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  private void setVariableScanStep() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-runVariableScanStep"))
      {
        argFound = true;
        _runVariableScanStep = true;
        break;
      }
    }

    if (argFound == false)
      _runVariableScanStep = false;
  }

  /**
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  private void setVariableScanPass() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-runVariableScanPass"))
      {
        argFound = true;
        _runVariableScanPass = true;
        _runVariableScanStep = false;
        break;
      }
    }

    if (argFound == false)
      _runVariableScanPass = false;
  }


  /**
   *
   * @throws HardwareException
   * @author Greg Esparza
   */
  private void setDebugMode() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-debugMode"))
      {
        argFound = true;
        _debugMode = true;
        break;
      }
    }

    if (argFound == false)
      _debugMode = false;
  }

  /**
   *
   * @return true if requesting usage, otherwise return false
   * @throws HardwareException
   * @author Greg Esparza
   */
  private boolean isUsageRequest() throws HardwareException
  {
    boolean usageRequest = false;


    for (int i = 0; i < _args.length; ++i)
    {
      if ((_args[i].equalsIgnoreCase("-help")) ||
          (_args[i].equalsIgnoreCase("-?") ) ||
          (_args[i].equalsIgnoreCase("/?")))
      {
        usageRequest = true;
        break;
      }
    }

    return usageRequest;
  }

  /**
   * @author Greg Esparza
   */
  private void checkArgs()
  {
    if (_args != null)
    {
      try
      {
        if (isUsageRequest() == false)
        {
          setTestOption();
          setScanPathRepeat();
          setPositionPulse();
          setNumberOfScanPasses();
          setScanPassLengthInNanometers();
          setScanPassDelayInMilliseconds();
          setFirstScanPassDelayInMilliseconds();
          setScanStepSizeInNanometers();
          setScanPathOrigin();
          setConsoleOutputEnable();
          setScanKeyboardControl();
          setScanStepDirection();
          setVariableScanStep();
          setVariableScanPass();
          setFirstScanPassDirection();
          setDebugMode();
        }
        else
          commandLineUsage();
      }
      catch (BadFormatException bfe)
      {
        System.out.println(bfe.getMessage());
        commandLineUsage();
      }
      catch (HardwareException he)
      {
        commandLineUsage();
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException
        {
          try
          {
            checkArgs();

            if (_debugMode)
              getKeyboardCommand("DEBUG MODE - Press <ENTER> when ready to start debug session ...");

            if ((_runInitialize) || (_runHome))
              testInitialize();

            if (_runPointToPointMove)
              pointToPointMove();
            else if (_runScanPath)
              testScanPath();
          }
          catch (IOException ie)
          {
            Expect.expect(false);
            ie.printStackTrace();
          }
          catch (BadFormatException bfe)
          {
            Expect.expect(false);
            bfe.printStackTrace();
          }
        }
      });
    }
    catch (XrayTesterException he)
    {
      Expect.expect(false);
      he.printStackTrace();
    }
  }

  /**
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new PanelPositionerUtil(args));
  }
}
