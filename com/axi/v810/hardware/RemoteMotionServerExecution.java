package com.axi.v810.hardware;

import java.net.*;
import java.rmi.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Controls the Remote Motion Server. This is a program
 * communicate on RMS.
 * The program will be able to do the following:
 * Shutdown the Server
 * Restart the Server
 * Start a Remote Motion Server
 * Stop/Kill a RMS
 * Determine if a program is running
 * Get the version of the program (encoded in the exe)
 * @author Chong, Wei Chin
 */
public class RemoteMotionServerExecution extends HardwareObject
    implements HardwareStartupShutdownInt
{
  private static boolean _debug = false;
  private static String _me = "RemoteMotionServerExecution";
  private static boolean _WAIT_ABORTABLE = true;
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 80000;
  private static final long _PROCESS_START_WAIT_MS = 200;

  private static boolean _FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK = false;

  private static String _localFilePath;
  private static String _localConfigFilePath;
  private static String _remoteFilePath;
  private static String _remoteFileSeperator = "\\";
  private static String _remoteFilePathRelativeToUploadRoot;

  private static Config _config;
  
  protected int _PING_TIMEOUT_IN_MILLISECONDS = 2000;

  private boolean _rebootNecessary = false;
  private boolean _initialised = false;
  private Process _rmi_registry_proc;
  private Process _remote_motion_proc;

  private PsExec _psExec;

  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;
  private ProgressObservable _progressObservable;


  List <String> remoteBindClassList = new ArrayList<String>();

  private static RemoteMotionServerExecution _instance = null;

  /**
   * @author Chong, Wei Chin
   */
  static
  {
    _config = Config.getInstance();
    _debug = _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);

    _localFilePath = Directory.getLocalRemoteMotionServerBinDir();
    _localConfigFilePath = Directory.getConfigDir();
    _remoteFilePath = Directory.getRemoteMotionServerBinDir();

    _remoteFilePathRelativeToUploadRoot = Directory.getRemoteMotionServerRelativeBinDir();
//    _FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK = getConfig().getBooleanValue(SoftwareConfigEnum.FORCE_UPLOAD_FOR_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP);
  }

  /**
   * @author Chong, Wei Chin
   */
  RemoteMotionServerExecution()
  {
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _progressObservable = ProgressObservable.getInstance();
    
    _psExec = PsExec.getInstance(Directory.getPsToolsDir());
    if(isSimulationModeOn())
      _psExec.setupConnection("127.0.0.1", "Administrator", "Please!");
    else
      _psExec.setupConnection(getServerAddress(), "Administrator", "Please!");
    remoteBindClassList.add("RemoteDigitalIo");
    remoteBindClassList.add("RemoteMotionControl");
    remoteBindClassList.add("RemoteNativeHardwareConfiguration");
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  static public RemoteMotionServerExecution getInstance()
  {
    if(_instance == null)
    {
      _instance = new RemoteMotionServerExecution();
    }
    return _instance;
  }

  /**
   * Determines whether the ip address of this service
   * is on the network and available.
   * @author Chong, Wei Chin
   */
  private boolean isHardwareReachable()
  {
    return addressIsOnNetwork(getServerAddress());
  }
  
  /**
   * @author Kee Chin Seong
   *   - Shut down Remote motion Server
   * @throws XrayTesterException 
   */
  public void shutdownRemoteMotionServer() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, RemoteMotionServerEventEnum.SHUTDOWN_REMOTE_MOTION_SERVER);
    _hardwareObservable.setEnabled(false);

    try
    {
      if (isSimulationModeOn() == false)
      {
        shutdownHardware();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, RemoteMotionServerEventEnum.SHUTDOWN_REMOTE_MOTION_SERVER);
  }

  /**
   * @author Chong, Wei Chin
   */
  private void restartHardware() throws XrayTesterException
  {
    String command = "shutdown";
    String arguments = "-r -t 1";

    ExitCodeEnum exitCode = ExitCodeEnum.NO_ERRORS;
    try
    {
      exitCode = _psExec.execProcess(command, arguments);
    }
    catch(IOException ioe)
    {
      throw RemoteMotionServerHardwareException.getFailedToRebootRMSHardwareException(getServerAddress());
    }

    if(exitCode != ExitCodeEnum.NO_ERRORS)
    {
      throw RemoteMotionServerHardwareException.getFailedToRebootRMSHardwareException(getServerAddress());
    }

    waitForRMSToReboot();
    startRMS();
  }
  
  /**
   * @author Kee Chin Seong
   * - Shut down the Atom PC
  */
  private void shutdownHardware() throws XrayTesterException
  {
    String command = "shutdown";
    String arguments = "-s -t 1";

    ExitCodeEnum exitCode = ExitCodeEnum.NO_ERRORS;
    try
    {
      exitCode = _psExec.execProcess(command, arguments);
    }
    catch(IOException ioe)
    {
      throw RemoteMotionServerHardwareException.getFailedToRebootRMSHardwareException(getServerAddress());
    }

    if(exitCode != ExitCodeEnum.NO_ERRORS)
    {
      throw RemoteMotionServerHardwareException.getFailedToRebootRMSHardwareException(getServerAddress());
    }
  }

  /**
   * @author Rick Gaudette
   */
  private String[] build_rms_command_array(String xray_root_path, String jre_bin_path)
  {
    // Use File objects to correctly build the path regardless of path convention
    File v810_jar_file = new File(xray_root_path + File.separator + "rmsBin" + File.separator + "v810.jar");
    File java_exec = new File(jre_bin_path + File.separator + "java.exe");
    File xray_root_file = new File(xray_root_path);

    String[] command_array = new String[9];
    command_array[0] = "\"" + java_exec.getAbsolutePath() + "\"";
    command_array[1] = "-Xms500M";
    command_array[2] = "-Xmx500M";
    command_array[3] = "-Djava.rmi.server.codebase=" + v810_jar_file.toURI();
    command_array[4] = "-DassertLogDir=\"" + xray_root_path + File.separator + "log\"";
    command_array[5] = "-classpath";
    command_array[6] = "\"" + v810_jar_file.getAbsolutePath() + File.pathSeparator+ xray_root_file.getAbsolutePath() + "\"";
    command_array[7] = "com.axi.v810.hardware.RemoteMotionServer";
    String current_time = String.valueOf(System.currentTimeMillis());
    command_array[8] = "\"" + xray_root_path + File.separator + "log" + File.separator + "remote_motion_server."
      + current_time + ".log" + "\"";
    return command_array;
  }

  /**
   * Ensure that the path is prepended with the xray home and jre home.  This is probably redundant as the full
   * path to the java executable is supplied above, but it won't hurt.
   * @author Rick Gaudette
   */
  private String[] build_rms_environment(String xray_root_path, String jre_bin_path)
  {
    Map<String, String> this_env = System.getenv();
    //  Create an array representation of the environment, modifying the path as it is copied

    String[] env_array = new String[this_env.size()];
    int i = 0;
    for (Map.Entry<String, String> element : this_env.entrySet()) 
    {
      if (element.getKey().equals("PATH")) 
      {
        env_array[i] = "Path=" + xray_root_path + File.separator + "rmsBin" + File.pathSeparator
//          + xray_root_path + File.separator + "bin" + File.pathSeparator
          + jre_bin_path + File.pathSeparator
          + element.getValue();
      }
      else 
      {
        env_array[i] = element.getKey() + "=" + element.getValue();
      }
      i++;
    }
    return env_array;
  }

  /**
   * @author Chong, Wei Chin
   */
  public synchronized void startRMS() throws XrayTesterException
  {
    if(UnitTest.unitTesting())
    {
      // start JRE rmiregistry
      String rmiFullPath = Directory.getJre32Bin() + File.separator + "rmiregistry.exe";
      try
      {
        _rmi_registry_proc = Runtime.getRuntime().exec(rmiFullPath);
      }
      catch(IOException ioe)
      {
      }

      // start Remote Java
      String rmcFullPath = Directory.getBinDir() + File.separator + "perl.exe";
      String argFullPath = "\"" + Directory.getBinDir() + File.separator + "runSimulationRemoteMotionServer.pl\"";
      try
      {
        Runtime.getRuntime().exec(rmcFullPath + " " + argFullPath);
      }
      catch(IOException ioe)
      {
      }
      waitForRMSIsConnected();
    }
    else if(isSimulationModeOn() == false)
    {
      if( isHardwareReachable() == false )
        throw RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), null);

      // start JRE rmiregistry
      String rmiFullPath = Directory.getRMSJreBin() + File.separator + "rmiregistry.exe";
      startProgram(rmiFullPath,"", false);

      // start Remote Java
      String rmcFullPath = Directory.getBinDir() + File.separator + "perl.exe";
      String argFullPath = "\"" + Directory.getBinDir() + File.separator + "runRemoteMotionServer.pl\"";

      startProgram(rmcFullPath, argFullPath, false);
      waitForRMSIsConnected();
    }
    else
    {
      // Start rmiregistry program for simulation mode
      String rmiFullPath = Directory.getJre32Bin() + File.separator + "rmiregistry.exe";
      try
      {
        _rmi_registry_proc = Runtime.getRuntime().exec(rmiFullPath);
      }
      catch(IOException ioe)
      {
        System.out.println("RemoteMotionServerExecution IOExecption " + rmiFullPath + ioe.getMessage());
      }
      // Let the RMI Registry program get started
      try {
        Thread.sleep(_PROCESS_START_WAIT_MS);
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }

      // start the remote motion server locally on this machine
      try
      {
        String[] command_array = build_rms_command_array(Directory.getXrayTesterReleaseDir(),
          Directory.getJre32Bin());
        String[] envp = build_rms_environment(Directory.getXrayTesterReleaseDir(), Directory.getJre32Bin());

        String commands = "";
        for (String val : command_array)
        {
          commands += val + " ";
          System.out.print(val + " ");
        }
        System.out.println("\n");
        
        File rmsRootBin = new File(Directory.getXrayTesterReleaseDir() + File.separator + "rmsBin");
        
        _remote_motion_proc = Runtime.getRuntime().exec(commands, envp, rmsRootBin);

        System.out.println("RemoteMotionServerExecution returned from exec ");        

      }
      catch(IOException ioe)
      {
        System.out.println("RemoteMotionServerExecution IOExecption " + ioe.getMessage());
      }

      waitForRMSIsConnected();
    }
    _initialised = true;
  }

  /**
   * @author Chong, Wei Chin
   */
  public synchronized void stopRMS() throws XrayTesterException
  {
    if(UnitTest.unitTesting())
    {
      // stop Remote Java
      String javaPath = "java.exe *32";
      stopProgram(javaPath);

      // stop JRE rmiregistry
      String rmiFullPath = "rmiregistry.exe *32";
      stopProgram(rmiFullPath);
      clearRemoteBinding();
    }
    else if(isSimulationModeOn() == false)
    {
      if( isHardwareReachable() == false )
        throw RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), null);

      // stop Remote Java
      String javaPath = "java.exe";
      stopProgram(javaPath);

      // stop JRE rmiregistry
      String rmiFullPath = "rmiregistry.exe";
      stopProgram(rmiFullPath);
      clearRemoteBinding();
    }
    else
    {
      // stop Remote Java
      String javaPath = "java.exe *32";
      stopProgram(javaPath);

      // stop JRE rmiregistry
      String rmiFullPath = "rmiregistry.exe *32";
      stopProgram(rmiFullPath);
      clearRemoteBinding();
    }

    _initialised = false;
  }

  /**
   * @author Chong, Wei Chin
   */
  private void startProgram(String fullPathFileName,
                           String arguments, boolean wait) throws XrayTesterException
  {
    Assert.expect(fullPathFileName != null);
    Assert.expect(arguments != null);

    ExitCodeEnum exitCode = ExitCodeEnum.NO_ERRORS;
    try
    {
      if(wait == false)
        exitCode = _psExec.execProcessNoWait(fullPathFileName, arguments);
      else
        exitCode = _psExec.execProcess(fullPathFileName, arguments);      
    }
    catch(IOException ioe)
    {
      RemoteMotionServerHardwareException exception = RemoteMotionServerHardwareException.getFailedToStartRMSApplicationException(getServerAddress());
      exception.initCause(ioe);
      throw exception;
    }

    if(exitCode != ExitCodeEnum.NO_ERRORS)
    {
      throw RemoteMotionServerHardwareException.getFailedToStartRMSApplicationException(getServerAddress());
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private void stopProgram(String programName) throws XrayTesterException
  {
    Assert.expect(programName != null);

    ExitCodeEnum exitCode = ExitCodeEnum.NO_ERRORS;

    if(isSimulationModeOn() == false)
    {
      String commands = Directory.getBinDir() + File.separator + "killProcess.exe";
      try
      {
        exitCode = _psExec.execProcess(commands, programName);
      }
      catch(IOException ioe)
      {
        RemoteMotionServerHardwareException exception = RemoteMotionServerHardwareException.getFailedToStopRMSApplicationException(getServerAddress());
        exception.initCause(ioe);
        throw exception;
      }
    }
    else
    {
      String commands = Directory.getLocalRemoteMotionServerBinDir() + File.separator + "killProcess.exe";

      try
      {
        Process p = Runtime.getRuntime().exec( commands + " " + programName);
        try
        {
          p.waitFor();
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      catch(IOException ioe)
      {
        RemoteMotionServerHardwareException exception = RemoteMotionServerHardwareException.getFailedToStopRMSApplicationException(getServerAddress());
        exception.initCause(ioe);
        throw exception;
      }
    }

    if(exitCode != ExitCodeEnum.NO_ERRORS &&
       exitCode != ExitCodeEnum.MISUSE_ERROR)
    {
      throw RemoteMotionServerHardwareException.getFailedToStopRMSApplicationException(getServerAddress());
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private void waitForRMSToReboot() throws XrayTesterException
  {
    // In case the computer is not completely down, do our wait of 1 1/2 minute.
    doMyWaitInMilliSeconds(90000);

    // Wait up to 4 more minutes.
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(4 * 60000, _WAIT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean isStarted = isHardwareReachable();
        return isStarted;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), null);
      }
    };

    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Chong, Wei Chin
   */
  private void waitForRMSIsConnected() throws XrayTesterException
  {
    int maximumTimeOut = 240000; //  4 minutes
    long deadline = System.currentTimeMillis() + maximumTimeOut;
    doMyWaitInMilliSeconds(2000);
    while(isConnectedToRMS() == false)
    {
      doMyWaitInMilliSeconds(1000);
      
      if(System.currentTimeMillis() > deadline)
        throw RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), null);

      if(isAborting())
        throw RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), null);
    }
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    setDebugIfNecessary();  
  }

  /**
   * @author Chong, Wei Chin
   */
  private void doMyWaitInMilliSeconds(int timeInMilliSeconds)
  {
    long deadline = System.currentTimeMillis() + timeInMilliSeconds;

    while (System.currentTimeMillis() < deadline)
    {
      try
      {
        // Wait half of the timeOut.  Do this twice if we don't get interrupted.
        Thread.sleep(timeInMilliSeconds / 2);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public String getServerAddress()
  {
    if(isSimulationModeOn())
      return "127.0.0.1";
    
    return _config.getStringValue(HardwareConfigEnum.REMOTE_MOTION_CONTROLLER_IP_ADDRESS);
  }

  /**
   * @author Chong, Wei Chin
   */
  private void debug(String string)
  {
    if (_debug)
    {
      System.out.println(_me + " " + getServerAddress() + " :" + string);
    }
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */
  static public void setDebugIfNecessary()
  {
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.REMOTE_MOTION_SERVER_DEBUG))
      RemoteDigitalIo.setDebug(true);
      RemoteMotionControl.setDebug(true);
      RemoteNativeHardwareConfiguration.setDebug(true);
  }

    /**
   * Method to actually 'ping' a remote system. It will wait for a set period of
   * time for the remote system to reply, and will return false if there is no
   * reply. It will return true of the remote system is available.
   *
   * @author Chong, Wei Chin
   */
  protected boolean addressIsOnNetwork(String addressToPing)
  {
    //Assume the connection is bad
    boolean isGoodConnection = false;

    try
    {
      //Get the InetAddress object for this address
      InetAddress addressToPingInet = InetAddress.getByName(addressToPing);

      //see if the system is reachable
      isGoodConnection = addressToPingInet.isReachable(_PING_TIMEOUT_IN_MILLISECONDS);
    }
    catch (IOException ex)
    {
      //IOException means network problems, which means connection is not good
      return false;
    }

    //Return the state of the connection
    return isGoodConnection;
  }

 /**
  * @throws XrayTesterException
  * @author Chong, Wei Chin
  */
  @Override
  public synchronized void startup() throws XrayTesterException
  {
    initialize();
  }
  
  /**
   * @author Chong, Wei Chin
   */
  private void initialize() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, RemoteMotionServerEventEnum.INITIALIZING_REMOTE_MOTION_SERVER);
    _hardwareObservable.setEnabled(false);
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.REMOTE_MOTION_SERVER_INITIALIZE, _STARTUP_TIME_IN_MILLISECONDS);

    try
    {
      // XCR-3604 Unit Test Phase 2
      if (UnitTest.unitTesting() == true)
        _initialised = true;
      else
      {
        int tryCount = 1;
        // Try 2 times (1, 2).
        final int maxTryCount = 2;
        boolean initializationOk = false;

        while (tryCount <= maxTryCount && initializationOk == false)
        {
          try
          {
            // Skip reboot as part of normal procedure because it is very slow.
            if (_rebootNecessary)
            {
              rebootRemoteMotionServer();
              _rebootNecessary = false;
            }

            // Stop the RMS if it is already running.
            stopRMS();

            if(isAborting())
              return;

            if (_FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK && isSimulationModeOn() == false)
              updateApplicationFiles();

            if(isAborting())
              return;

            if(isConnectedToRMS() == false)
              startRMS();


            if(isAborting())
              return;
            // Verify RMS, resolve any inconsistency.
            if(isSimulationModeOn() == false)
              verifyApplication();
            initializationOk = true;
            _initialised = true;
          }
          catch (RemoteMotionServerHardwareException rmshe)
          {
            debug(rmshe.getLocalizedMessage());
  //          _rebootNecessary = true;
            if (tryCount >= maxTryCount)
            {
              throw rmshe;
            }
            else if (rmshe.getType().equals(RemoteMotionServerHardwareExceptionEnum.RMS_APPLICATION_VERSION_MISMATCH))
            {
              // If we get version mismatch exception, it means the newly uploaded file
              // still mismatches.  We don't need to bother with retry.
              updateApplicationFiles();
              _rebootNecessary = false;
  //            throw rmshe;
            }
            else if (rmshe.getType().equals(RemoteMotionServerHardwareExceptionEnum.FAILED_TO_START_RMS_APPLICATION))
            {
              // Possible file corruption on the IRP, update the files.
              updateApplicationFiles();
              _rebootNecessary = false;
            }
          }
          catch (XrayTesterException xte)
          {
            debug(xte.getLocalizedMessage());
  //          _rebootNecessary = true;
            if (tryCount >= maxTryCount)
            {
              throw xte;
            }
          }
          tryCount++;
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.REMOTE_MOTION_SERVER_INITIALIZE);
    }
    _hardwareObservable.stateChangedEnd(this, RemoteMotionServerEventEnum.INITIALIZING_REMOTE_MOTION_SERVER);
  }  

  /**
   * @throws XrayTesterException
   * @author Chong, Wei Chin
   */
  @Override
  public void shutdown() throws XrayTesterException
  {
    stopRemoteMotionServer();
    clearRemoteBinding();
  }

  /**
   * @author Chong, Wei Chin
   */
  public void stopRemoteMotionServer() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, RemoteMotionServerEventEnum.STOPPING_REMOTE_MOTION_SERVER);
    _hardwareObservable.setEnabled(false);

    try
    {
      stopRMS();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, RemoteMotionServerEventEnum.STOPPING_REMOTE_MOTION_SERVER);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void restartRemoteMotionServer() throws XrayTesterException
  {
    stopRemoteMotionServer();
    startRemoteMotionServer();
  }

  /**
   * @author Chong, Wei Chin
   */
  public void startRemoteMotionServer() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, RemoteMotionServerEventEnum.STARTING_REMOTE_MOTION_SERVER);
    _hardwareObservable.setEnabled(false);

    try
    {
      // In case we don't have all of the irp software running, let's stop all of them first.
      if (isConnectedToRMS())
        stopRMS();

      // Ok, let's start up the rms software.
      if (isConnectedToRMS() == false)
        startRMS();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, RemoteMotionServerEventEnum.STARTING_REMOTE_MOTION_SERVER);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void rebootRemoteMotionServer() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, RemoteMotionServerEventEnum.REBOOTING_REMOTE_MOTION_SERVER);
    _hardwareObservable.setEnabled(false);

    try
    {
      if (isSimulationModeOn() == false)
      {
        restartHardware();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, RemoteMotionServerEventEnum.REBOOTING_REMOTE_MOTION_SERVER);
  }

  /**
   *
   * @return
   * @throws XrayTesterException
   * @author Chong, Wei Chin
   */
  @Override
  public boolean isStartupRequired() throws XrayTesterException
  {
    if(_initialised == false)
      return true;
    
    boolean isStartupRequired = (isConnectedToRMS() == false);
    return isStartupRequired;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public synchronized boolean isConnectedToRMS()
  {
    boolean bindingOk = true;
    
    // XCR-3604 Unit Test Phase 2
    if (UnitTest.unitTesting() == false)
    {
      try
      {
        DigitalIo.connectToDigitalIoServer();
        NativeHardwareConfiguration.connectToHardwareConfigurationServer();
        MotionControl.connectToMotionControlServer();
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    return bindingOk;
  }

  /**
   * @author Wei Chin
   */
  private void clearRemoteBinding()
  {
    DigitalIo.resetRemoteDigitalIO();
    NativeHardwareConfiguration.resetRemoteNativeHardwareConfiguration();
    MotionControl.resetRemoteMotionControl();
  } 
  
  /**
   * Copy the latest Application file to RMS
   * @author Chong, Wei Chin
   */
  public void updateApplicationFiles() throws XrayTesterException
  {
    if(isSimulationModeOn())
      return;

    stopRMS();

    TftpClientAxi client = new TftpClientAxi(getServerAddress(), TftpClientAxiCommunicationModeEnum.TFTP_BINARY_MODE);

    client.open();

    Collection<String> fileNames = getApplicationFileNames();
    Collection<String> configFileNames = getConfigFileNames();

    // Clean the destination directory.
    for (String fileName : fileNames)
    {
      if(fileName.equals("3CTftpSvc.exe") || fileName.equals("3CTftpSvc.ini") || fileName.equals("v810.jar"))
        continue;

      if(fileName.endsWith("dll") || fileName.endsWith("exe"))
        client.sendFile(_localFilePath + File.separator + FileName.getNearlyEmptyUploadFile(),
                        Directory.getRemoteMotionServerRelativeBinDir() + _remoteFileSeperator + fileName);
    }

    // Now upload the bin files.
    for (String fileName : fileNames)
    {
      if(fileName.equals("3CTftpSvc.exe") || fileName.equals("3CTftpSvc.ini") || fileName.equals("v810.jar"))
        continue;
      if(fileName.endsWith("dll") || fileName.endsWith("exe"))
        client.sendFile(_localFilePath + File.separator + fileName,
                        Directory.getRemoteMotionServerRelativeBinDir() + _remoteFileSeperator + fileName);
    }

    // Clean the destination directory.
    for (String fileName : configFileNames)
      client.sendFile(_localFilePath + File.separator + FileName.getNearlyEmptyUploadFile(),
                      Directory.getRemoteMotionServerRelativeConfigDir() + _remoteFileSeperator + fileName);

    // Now upload the config files.
    for (String fileName : configFileNames)
      client.sendFile(_localConfigFilePath + File.separator + fileName,
                      Directory.getRemoteMotionServerRelativeConfigDir() + _remoteFileSeperator + fileName);

    // Clean the Jar file
    client.sendFile(_localFilePath + File.separator + FileName.getNearlyEmptyUploadFile(),
                      Directory.getRemoteMotionServerRelativeBinDir() + _remoteFileSeperator + "v810.jar");
    // upload the Jar file
    client.sendFile( Directory.getBinDir() + File.separator + "v810.jar",
                     Directory.getRemoteMotionServerRelativeBinDir() + _remoteFileSeperator + "v810.jar");

    client.close();
  }

  /**
   * @author Chong, Wei Chin
   */
  private static Collection<String> getApplicationFileNames()
  {
    Collection<String> files = FileUtil.listAllFilesInDirectory(_localFilePath);
    return files;
  }

  /**
   * @author Chong, Wei Chin
   */
  private static Collection<String> getConfigFileNames()
  {
    Collection<String> files = FileUtil.listAllFilesInDirectory(_localConfigFilePath);
    return files;
  }

 /**
   * Verify that the application matches.  If not, try update.
   * @author Chong Wei Chin
   */
  private void verifyApplication() throws XrayTesterException
  {
    try
    {
      verifyApplicationVersion();
    }
    catch (RemoteMotionServerHardwareException rmshe)
    {
      if (rmshe.getType().equals(RemoteMotionServerHardwareExceptionEnum.RMS_APPLICATION_VERSION_MISMATCH))
      {
        // Version mismatch, try update the files then check again.
        updateApplicationFiles();
        startRMS();
        verifyApplicationVersion();
      }
      else
        throw rmshe;
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private void verifyApplicationVersion() throws XrayTesterException
  {
    try
    {
      if (DigitalIo.getRemoteMotionServerApplicationVersion().equals(Version.getVersion()) == false)
      {        
        if (_FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK == false)
        {
          HardwareException he = RemoteMotionServerHardwareException.getApplicationVersionMismatchException();
          throw he;
//          _hardwareTaskEngine.throwHardwareException(he);
        }
      }
    }
    catch(NotBoundException nbe)
    {
      HardwareException he = RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), nbe);
      _hardwareTaskEngine.throwHardwareException(he);
    }
    catch(RemoteException re)
    {
      HardwareException he = RemoteMotionServerHardwareException.getFailedToCommunicateWithRMSException(getServerAddress(), re);
      _hardwareTaskEngine.throwHardwareException(he);
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  public void forceRestart()
  {
    _initialised = false;
    //do nothing
  }

  /**
   * @throws XrayTesterException overriding class might throw exception due
   * to interacting with data store or hardware.
   * @author Rex Shang
   */
  public void setSimulationMode() throws XrayTesterException
  {
    super.setSimulationMode();
    _psExec.setupConnection("127.0.0.1", "Administrator", "Please!");
  }

  /**
   * @throws XrayTesterException overriding class might throw exception due
   * to interacting with data store or hardware.
   * @author Rex Shang
   */
  public void clearSimulationMode() throws XrayTesterException
  {
    super.clearSimulationMode();
    _psExec.setupConnection(getServerAddress(), "Administrator", "Please!");
  }
}
