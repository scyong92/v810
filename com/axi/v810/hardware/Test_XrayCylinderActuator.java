package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left outer barrier
 * @author Rex Shang
 */
class Test_XrayCPMotorActuator extends UnitTest
{
  private XrayCPMotorActuator _xrayCPMotorActuator = null;

  private Test_XrayCPMotorActuator()
  {
    _xrayCPMotorActuator = XrayCPMotorActuator.getInstance();
  }

  public static void main(String[] args)
  {
    Test_XrayCPMotorActuator _test_XrayCylinder = new Test_XrayCPMotorActuator();
    UnitTest.execute(_test_XrayCylinder);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Move the xray CP Motor or Z Axis up and verify 
      _xrayCPMotorActuator.home(false,false); //false = without loading camera config
      Expect.expect(_xrayCPMotorActuator.isHome() == true);
      
      // Move the xray CP Motor or Z Axis up and verify 
      _xrayCPMotorActuator.up(false); //false = without loading camera config
      Expect.expect(_xrayCPMotorActuator.isUp() == true);

      // Move the  xray CP Motor or Z Axis down and verify.
      _xrayCPMotorActuator.down(false); //false = without loading camera config
      Expect.expect(_xrayCPMotorActuator.isDown() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
