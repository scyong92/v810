package com.axi.v810.hardware;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class will provide all xray source functionality including
 * things like turning x-rays on and off.  The new tube has
 * the power supply built in so there will no longer be a separate
 * XrayTube and XrayPowerSupply class.
 *
 * This class controls the Genesis X-ray source.  This source is made up of a an
 *  integrated assembly that contains both the high voltage power supply (HVPS)
 *  and an X-ray tube along with deflection coils, controller electronics, etc.
 *  All of this is "bathed" in Sulfur hexa-Fluoride (SF<sub>6</sub>) gas which acts
 *  as an insulator to prevent arcing shorts between the various high voltage components
 *  that are not in vacuum.<p/>
 *
 * This gross representation of the tube itself will hopefully help make sense out the
 *  myriad of values that can be read and set.<br/>
 * <pre>
 *                   The Genesis X-ray Tube
 *            ________                       ________
 *           /        \                     /        \
 *          |          |                   |          |
 *          |          |                   |          | <--- glass vacuum vessel
 *          |          |                   |          |
 *          |          |                   |          |
 *          |          |                   |       <--+----- vacuum
 *          |          |                   |          |
 *          |          |                   |          |
 *          |          |                   |          |
 *          |          |                   |          |
 *         /           |                   |           \             nominal working
 *        /            |       |   |       |            \               voltages
 *       /                     |   |                     \
 *      /               |------+---|------|               \
 *     /                |      |   |<-----+---- filament   \          4.75 V WRTC (2 A)
 *    /                 |  +---+   |      |                 \
 *   /     __           |  |   |   |      |           ..     \
 *  |     |  |          |  |  /     \     |<--- cathode/gun   |       0 V WRTC           -160 kV WRTG
 *  |     |  |          |  |  \~~~~~/     |          .  .     |        (by definition)
 *  |     |  |          |  |     :        |          .  .     |
 *  |     |  |          |  +---- : ---- <-+---- control grid  |       0 V WRTC
 *  |     |  |          |        :        |          .  .     |        (grounded)
 *  |     |  |          |        :        |          .  .     |
 *  |     |  |          | ------ : ---- <-+---- focus grid    |       +2 kV WRTC
 *  |     |  |          |        :        |          .  .     |
 *  |     |  |                   :                   :  :     |
 *  |     |  |<----- anode       :       anode ----->|  |     |       +160 kV WRTC          0 V WRTG
 *  |     |  |        v          :        v          |  |     |
 *  | ---/    \----------------- : -----------------/    \--- |
 *  |                            :                            |
 *  |       |-----------------------------------------|       | <--- Tungsten target
 *  \______ |=========================================| ______/ <--- Beryllium window
 *          |=========================================|         <--- collimator
 *
 * </pre>
 * <br/>
 *
 * <b>gun:</b> The entire gun is the cathode part of a tube as apposed to the anode.  For school,
 *   an X-ray tube is described technically as having an anode and a cathode and a window. The
 *   cathode is what supplies the electrons.  Once one begins describing the cathode in detail
 *   then the assembly becomes a gun because it is made up of: grids, bias grids, accelerator grids,
 *   false anodes, first anode, focus assembly, what ever type of gun it is� and an actual cathode
 *   which is a very tiny part of the cathode or gun. (Courtesy of Dave Reynolds)<p/>
 *
 * <b>cathode:</b> this is the free electron source for the tube. the filament is part of the cathode<p/>
 *
 * <b>WRTC:</b> with respect to cathode -- because electrons are pulled toward positive voltages and repelled
 *   from negative voltages, it is very helpful to think about the voltages of the various tube parts
 *   in comparison to the voltage of the cathode<p/>
 *
 * <b>WRTG:</b> with respect to ground (Greg made this up just for this diagram)<p/>
 *
 * <b>filament:</b> (think light bulb) You apply a voltage, it heats up and causes the cathode
 *   to "boil off" electrons.  The filament current/voltage (TBD) output limit may be set
 *   to a value of up to 8.191 amps/volts (TBD).  However, the filament operating point is actually
 *   determined by a closed-loop control system that adjusts the beam current to be as close to 100uA
 *   (the "turn on" cathode current) as possible. The nominal values read back from the XRS should
 *   be near 4.75 volts and 2 amps. The specified maximum operating limits for the filament power
 *   supply (which is not the "isolated supply") are 7 volts and 4 amps.<p/>
 *
 * <b>control grid:</b> Not read-able or set-able!  The control grid runs at zero bias in Genesis.
 *   There is no control potential from the HVPS.  It is grounded to the more negative side of the
 *   filament (which is resistor like).  Therefore, it provides a control poential of roughly the
 *   average voltage across the filament: <pre>2.375 V ~= ((0 V + 4.75 V) / 2)</pre>.<p/>
 *
 * <b>focus grid (aka grid):</b> By adjusting the amount of positive voltage WRTC, one can form the
 *   electrons that escape the cathode and pass the control grid into a beam heading towards the first
 *   anode stage.  The focus grid may be programmed over a range of 1500 to 2500 volts.<p/>
 *
 * <b>anode:</b> A disc-like conductive plate with a hole in the middle for the electron beam to go thru.
 *   The voltage of the anode accelerates the electrons generated from the gun toward the target.  The
 *   anode in the Genesis tube has a cylindrical shape to catch "backscattered" electrons so they don't
 *   hit the glass, charging it, and creating breakdown (short circuits from anode to cathode along
 *   surface of glass).<p/>
 *
 * <b>resistor:</b> these resistors keep the anode stages at there relative voltages. in order to maintain
 *   the voltage differential, a slight bit of current is used by the resistor (dissipated as heat).
 *   AKA divider resistor, or bleeder resistor<p/>
 *
 * <b>bleeder current:</b> the current used by the whole chain of divider resistors (there are actually
 *   more than one per stage) is called the bleeder current<p/>
 *
 * <b>leakage current:</b> current traveling through ionized gas in the tube's vacuum chamber.
 *   failing cable or connector, bad grease in connector, failed caps or resistors, failing pcb, ...
 *   will also show as leakage. i.e., in a perfect system, the cathode plus bleeder current would
 *   be the only current supplied by the HVPS. any other current is leakage<p/>
 *
 * <b>total current:</b> this the total current supplied by the HVPS and is equal to the sum of the
 *   cathode, bleeder, and leakage currents<p/>
 *
 * <b>vacuum:</b> the interior of the tube is kept at a very good vacuum, but there are is always
 *   some amount of gas present in the tube.  there are also various ongoing processes that add
 *   small amounts of gas to the tube over time<p/>
 *
 * <b>glass vacuum vessel:</b> maintains the vacuum between the cathode, anode and target.  The
 *   Genesis X-ray tube has such a long "neck" to increase the surface path between the cathode
 *   and anode, helping to prevent breakdown (short circuits along surface of glass).<p/>
 *
 * <b>arcing:</b> any gas inside the tube is eventually ionized and pushed to the end of the cavity.
 *   if there is enough gas and it is ionized so quickly that it doesn't have a chance to bunch up
 *   at one end of the cavity (remaining distributed throughout the cavity), then it become a good
 *   conductor and arcs form across the affected cavity. think of these as little lightning bolts;
 *   they are "not good" for the tube. arcs can be detected because they cause a spike in current
 *   and drop in voltage (which halts X-ray production). if the power supply is not properly current
 *   limited, then arcing will destroy a tube very quickly.<p/>
 *
 * <b>collimator:</b> collimators are generally just tube-like things that let light to go in roughly
 *   one direction (rather like putting a flashlight up to an empty toilet paper roll). this specific
 *   collimator's purpose is to block the "4 pi steradians" (sphere-like distribution) of X-rays and only allow them onto the  detector area<p/>
 *
 * <b>heat dissipation:</b> there is only about 16W of heat dissipation (16kV * 100uA = 16W).
 *   compare that to over 100W of heat dissipation for a Pentium 4, 60W for common incandescent
 *   light bulb, and 16W for those "replaces a 60W" fluorescent bulbs. this thing can kill you,
 *   but it sure won't keep you warm...<p/>
 *
 * <b>For more information:</b> Search for Cathode Ray Tube at
 *   <a href="http://en.wikipedia.org/wiki/Cathode_ray_tube">www.wikipedia.org</a>
 *   - many of the workings and issues are similar, not mention the vocabulary.
 *   You could also talk to the hardware guys and ask to look at their toys...<p/>
 *
 * HARDWARE REPORTED ERRORS: The controller reports on a myriad of error conditions
 *   that can occur on the hardware (see getHardwareReportedErrors()). Methods that
 *   attempt to change the state of the hardware use EROR responses from the
 *   getOperationalStatus() and getSelfTestStatus() methods to queue up calls to
 *   getHardwareReportedErrors().  This approach should probably be verified.<p/>
 *
 * NOTE ON abort(): Methods that spin block (i.e., sleep) check the abort flag.
 *   The only methods that pay attention to the abort flag are on() and performSelfTest().
 *   Please see those methods for more information.<p/>
 *
 * THREAD SAFETY: This class is re-entrant (synchronizes access to USB link).<p/>
 *
 * TEST NOTE: From a unit-test / code-coverage perspective, almost the entire class
 *   is testable without any hardware, because the simulation check happens just
 *   before interacting with the USB port.  I.e., the code can be tested against
 *   sunny day responses from a simulated USB link.
 *
 * @todo second review of class javadoc with John Siefers and Dave Reynolds
 * @todo review and test
 * @todo review decision to ignore the RS0, RSA, RSE, RSU, and SG1 commands
 * @todo verify approach to HARDWARE REPORTED ERRORS (above)
 *
 * @author Bill Darbie
 * @author George A. David
 * @author Greg Loring
 */
public class XraySource extends AbstractXraySource implements HardwareStartupShutdownInt
{
  private static final Pattern _fourDigitStringRegex = Pattern.compile("^\\d{4}$");

  private static final double _MAX_ANODE_KILO_VOLTS = 165.0;
  private static final double _MAX_CATHODE_MICRO_AMPS = 204.7;
  private static final double _MAX_FILAMENT_VOLTS = 8.191;
  private static final double _MAX_FILAMENT_AMPS = 8.191;
  private static final double _MAX_GRID_VOLTS = 4095.0;
  private static final double _MAX_ISOLATED_SUPPLY_VOLTS = 40.95;
  /** @todo replace PRESSURE_UNITS with the real unit; e.g., _MAX_SF6_KILO_PASCALS */
  private static final double _MAX_SF6_PRESSURE_UNITS = 4095.0; /** @todo replace with real max value */
  private static final double _MAX_SF6_DEGREES_C = 150.0;
  private static final double _MAX_GUN_DEGREES_C = 4095.0; /** @todo replace with real max value */
//  private static final double _MAX_ = .0;

  // operational state
  private Config _config = null;
  private UsbDeviceDescriptionEnum _xraySourceDescription = UsbDeviceDescriptionEnum.XRAY_SOURCE;
  private UsbUtil5dx _usbUtil = UsbUtil5dx.getInstance(UsbDriverTypeEnum.FTDI);
  private int _percentDone = -1;

  // simulation state
  private boolean _isThisObjectInSimMode = false;
  private String _simOperationalStatus = "DOWN";
  private final int _simTurnOnDurationMillis = 5000;
  private long _simLastTurnOnStartTimeMillis = -1L;
  private String _simSelfTestStatus = "PASS";
  private final int _simSelfTestDurationMillis = 10000;
  private long _simLastSelfTestStartTimeMillis = -1L;
  private Map<Character, int[]> _simOperationalValueMap;
  private Map<String, Integer> _simAuxilliaryIntegerValueMap;
  private Map<String, String> _simAuxilliaryStringValueMap;

  /**
   * @author Matt Wharton
   * @author George David
   * @author Greg Loring
   */
  /*package*/ XraySource()
  {
    _config = Config.getInstance();
    initializeSimStateMaps();
  }

  /**
   * put this object into simulation mode
   *
   * @author Greg Loring
   */
  public synchronized void setSimulationMode() throws DatastoreException
  {
    _isThisObjectInSimMode = true;
  }

  /**
   * take this object out of individual simulation mode (may stay in simulation
   *  mode based on overall system configuration
   *
   * @author Greg Loring
   */
  public synchronized void clearSimulationMode() throws DatastoreException
  {
    _isThisObjectInSimMode = false;
  }

  /**
   * sim mode may be on for this object because it was turned on explicitly via
   *  setSimulationMode(), or because there is some indication in the overall
   *  system configuration that all hardware related objects should be operating
   *  in sim mode.  In either case, this object will not interact with hardware.
   *
   * @author Greg Loring
   */
  public boolean isSimulationModeOn()
  {
    boolean result = _isThisObjectInSimMode
                     || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    return result;
  }

  /**
   * Is the X-ray source hardware producing X-rays suitable for imaging?<p/>
   *
   * This method returns false if the X-ray source is operating in either the
   *  low-power or high-power survey/safety-test modes.
   *
   * @author George A. David
   * @author Greg Loring
   */
  public boolean areXraysOn() throws XrayTesterException
  {
    boolean result = false;
    if (isSurveyModeOn() == false)
    {
      XraySourceStatusEnum operStatus = getOperationalStatus();
      result = XraySourceStatusEnum.READY.equals(operStatus);
    }
    return result;
  }

  /**
   * Get the X-ray source hardware ready for imaging, i.e., start emitting imaging quality X-rays.<p>
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 160kV</li>
   *  <li>nominal cathode current is 100uA</li>
   *  <li>areXraysOn() returns true</li>
   * </ul>
   *
   * NOTE: This method pays attention to the abort flag.<p/>
   *
   * @see #areXraysOn()
   *
   * @author Greg Loring
   */
  public void on() throws XrayTesterException
  {
    Assert.expect(isStartupRequired() == false);

    setSurveyModeOn(false);

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ON);
    _hardwareObservable.setEnabled(false);
    try
    {
      on("XON");
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ON);
  }

  /**
   * turn on for low-power survey/safety-test mode operation.
   *
   * NOTE: This method pays attention to the abort flag.<p/>
   *
   * @author Greg Loring
   */
  public void lowPowerOn() throws XrayTesterException
  {
    on("XOL"); /** @todo remove todo when the hardware vendor implements this command */
    setSurveyModeOn(true);
  }

  /**
   * turn on for high-power survey/safety-test mode operation.
   *
   * NOTE: This method pays attention to the abort flag.<p/>
   *
   * @author Greg Loring
   */
  public void highPowerOn() throws XrayTesterException
  {
    on("XOH"); /** @todo remove todo when the hardware vendor implements this command */
    setSurveyModeOn(true);
  }

  /**
   * Send one of the turn-on commands and then wait for the status to change to READY.
   *
   * NOTE: This method pays attention to the abort flag.<p/>
   *
   * @todo implement abort functionality (call off() in response an abort event?)
   *
   * @author Greg Loring
   */
  private void on(String xoCommand) throws XrayTesterException
  {
    /** @todo gad assert that the string is not null */

    Assert.expect(xoCommand.startsWith("XO") && (xoCommand.length() == 3));

    clearInterlockFlags();
    checkForHardwareReportedErrors();

    // initiate turning on of X-rays
    processInitiateActionCommand(xoCommand);

    // wait 'til they're on

    XraySourceStatusEnum operStatus;
    while ((operStatus = getOperationalStatus()) != XraySourceStatusEnum.READY)
    {
      if (operStatus == XraySourceStatusEnum.STARTING_XRAYS)
      {
        /** @todo cache singleton */
        ProgressObservable.getInstance().reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, _percentDone);
      }

      if (operStatus == XraySourceStatusEnum.ERROR)
      {
        /** @todo resolve the "hopefully" */
        checkForHardwareReportedErrors(); // hopefully this will throw
      }

      sleep(1000);
    }

    // make sure 100% done is reported
    /** @todo cache singleton */
    ProgressObservable.getInstance().reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 100);
  }

  /**
   * Stop the X-ray source hardware from emitting imaging quality X-rays.  Use this method
   *  imaging is being stopped for a short time (i.e., the system is not being powered off).<p>
   *
   *   NOTE: A successful call to this method does guarantee that no X-rays will be emitted
   *     from the X-ray source hardware.<p>
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 0kV</li>
   *  <li>nominal cathode current is 0uA</li>
   *  <li>areXraysOn() returns false</li>
   * </ul>
   *
   * @see #areXraysOn()
   *
   * @author Greg Loring
   */
  public void off() throws XrayTesterException
  {
    setSurveyModeOn(false);

    if (getOperationalStatus() != XraySourceStatusEnum.STANDBY)
    {
      // initiate short-term shutdown of X-rays
      processInitiateActionCommand("XOS");

      // wait 'til they're off
      while (getOperationalStatus() != XraySourceStatusEnum.STANDBY)
      {
        if (getOperationalStatus() == XraySourceStatusEnum.ERROR)
        {
          /** @todo resolve the "hopefully" */
          checkForHardwareReportedErrors(); // hopefully this will throw
        }

        sleep(200);
      }
    }
  }

  /**
   * Stop the X-ray source hardware from emitting any X-rays and get it ready for all power
   *  to be turned off.
   *
   * post-conditions:<ul>
   *  <li>nominal anode voltage is 0kV</li>
   *  <li>nominal cathode current is 0uA</li>
   *  <li>areXraysOn() returns false</li>
   * </ul>
   *
   * @see #areXraysOn()
   *
   * @author Greg Loring
   */
  public void powerOff() throws XrayTesterException
  {
    setSurveyModeOn(false);

    if (getOperationalStatus() != XraySourceStatusEnum.LONG_TERM_SHUTDOWN)
    {
      // initiate long-term shutdown of X-rays
      processInitiateActionCommand("XOF");

      // wait 'til they're off
      while (getOperationalStatus() != XraySourceStatusEnum.LONG_TERM_SHUTDOWN)
      {
        if (getOperationalStatus() == XraySourceStatusEnum.ERROR)
        {
          /** @todo resolve the "hopefully" */
          checkForHardwareReportedErrors(); // hopefully this will throw
        }

        sleep(200);
      }
    }
  }

  /**
   * ...of the hardware
   *
   * @todo implement abort functionality (call off() in response an abort event?)
   *
   * @author Greg Loring
   */
  public String selfTest() throws XrayTesterException
  {
    setSurveyModeOn(false);

    // initiate hardware self-test
    processInitiateActionCommand("XST");

    // wait 'til the testing is done
    String status;
    while ((status = getSelfTestStatus()).equals("TEST"))
    {
      /** @todo cache singleton */
      ProgressObservable.getInstance().reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, _percentDone);

      sleep(1000);
    }

    // make sure 100% done is reported
    /** @todo cache singleton */
    ProgressObservable.getInstance().reportProgress(ProgressReporterEnum.XRAY_SOURCE_ON, 100);

    return status; // either PASS or FAIL#### (see N7280-66236-3 firmware spec)
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public String getFirmwareRevisionCode() throws XrayTesterException
  {
    String result = processReadStringValueCommand("RV0");
    return result;
  }

  /**
   * ask the X-ray source hardware if it has experienced any errors<p/>
   *
   *   NOTE: the errors reported have been "latched" by the hardware.  this means the hardware
   *     is remembering them even if the actual condition has been resolved.  after all errors
   *     have been resolved (by hardware service staff, for example), call the related
   *     resetHardwareReportedErrors() method<p/>
   *
   * @see #resetHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public XraySourceHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException
  {
    XraySourceHardwareReportedErrorsException result = null;

    int errorFlags = processReadIntegerValueCommand("RSL");
    if (errorFlags != 0)
    {
      result = new XraySourceHardwareReportedErrorsException(errorFlags);
    }

    /** @todo gad assert that result is not null */
    return result;
  }

  /**
   * ask the X-ray source hardware to clear its latched error register<p/>
   *
   * any errors detected by the hardware are "latched."  this means the hardware
   *   is remembering them even if the actual condition has been resolved.
   *   after all errors have been resolved (by hardware service staff, for example),
   *   call the related resetHardwareReportedErrors() method<p/>
   *
   * @see #getHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public void resetHardwareReportedErrors() throws XrayTesterException
  {
    processReadIntegerValueCommand("RSR"); // ignore response (caller presumably already knows)
  }

  /**
   * if only interlocks have been tripped, try to reset them.  this is very common on the 5DX
   *  because of the way the interlock system works: interlock errors were latched every time
   *  a barrier was openned.  assume that it will be the same on the Genesis platform.
   *
   * @todo confirm or deny assumption (above)
   * @todo make private?
   *
   * @author Greg Loring
   */
  public void clearInterlockFlags() throws XrayTesterException
  {
    int status = processReadIntegerValueCommand("RSL");
    if (status != 0)
    {
      if ((status & ~0x0F) == 0) // is hardware only reporting interlock errors?
      {
        // only interlocks have been tripped, try to reset them
        resetHardwareReportedErrors();

        // if the interlock errors refuse to be reset, then there must be an ongoing problem
        checkForHardwareReportedErrors(); // rechecks errorFlags, and throws if not clear
      }
    }
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RK0", _MAX_ANODE_KILO_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RK2", _MAX_ANODE_KILO_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RK3", _MAX_ANODE_KILO_VOLTS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingAnodeVoltageInKiloVolts() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_ANODE_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RK1", _MAX_ANODE_KILO_VOLTS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_ANODE_VOLTAGE);
  }

  /**
   * ...on the hardware: sets the anode voltage value that is used (set) during the
   *   next call to on(); the behavior is undefined if called after the X-ray source is
   *   already on (areXraysOn() == true)
   *
   * NOTE: John Siefers said he thought this would only be useful for causing hardware
   *        "malfunctions" in the lab.  However, may be needed for survey/safety-test as well.
   *
   * @author Greg Loring
   */
  public void setTurnOnAnodeVoltageInKiloVolts(double value) throws XrayTesterException
  {
    /** @todo gad assert that value is valid. such as >= 0 or <= 160kv whatever */

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.ANODE_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processSetTurnOnValueCommand("SK0", value, _MAX_ANODE_KILO_VOLTS);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.ANODE_VOLTAGE);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getCathodeCurrentInMicroAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RA0", _MAX_CATHODE_MICRO_AMPS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingCathodeCurrentInMicroAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RA2", _MAX_CATHODE_MICRO_AMPS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingCathodeCurrentInMicroAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RA3", _MAX_CATHODE_MICRO_AMPS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingCathodeCurrentInMicroAmps() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_CATHODE_CURRENT);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RA1", _MAX_CATHODE_MICRO_AMPS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_CATHODE_CURRENT);
  }

  /**
   * ...on the hardware: sets the cathode current value that is used (set) during the
   *   next call to on(); the behavior is undefined if called after the X-ray source is
   *   already on (areXraysOn() == true)<p/>
   *
   * NOTE: John Siefers said he thought this would only be useful for causing hardware
   *        "malfunctions" in the lab.  However, may be needed for survey/safety-test as well.
   *
   * @author Greg Loring
   */
  public void setTurnOnCathodeCurrentInMicroAmps(double value) throws XrayTesterException
  {
    /** @todo gad assert that value is valid */

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.CATHODE_CURRENT);
    _hardwareObservable.setEnabled(false);
    try
    {
      processSetTurnOnValueCommand("SA0", value, _MAX_CATHODE_MICRO_AMPS);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.CATHODE_CURRENT);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getFilamentVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RE0", _MAX_FILAMENT_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingFilamentVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RE2", _MAX_FILAMENT_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingFilamentVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RE3", _MAX_FILAMENT_VOLTS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingFilamentVoltageInVolts() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_FILAMENT_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RE1", _MAX_FILAMENT_VOLTS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_FILAMENT_VOLTAGE);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getFilamentCurrentInAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RF0", _MAX_FILAMENT_AMPS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingFilamentCurrentInAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RF2", _MAX_FILAMENT_AMPS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingFilamentCurrentInAmps() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RF3", _MAX_FILAMENT_AMPS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingFilamentCurrentInAmps() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_FILAMENT_CURRENT);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RF1", _MAX_FILAMENT_AMPS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_FILAMENT_CURRENT);
  }

  /**
   * ...on the hardware: sets an upper bound on the filament that is enforced during the
   *   next call to on(); the behavior is undefined if called after the X-ray source is
   *   already on (areXraysOn() == true)<p/>
   *
   * NOTE: John Siefers said he thought this would only be useful for causing hardware
   *        "malfunctions" in the lab.
   *
   * NOTE: whether the filament will be voltage or current limited is TBD (Greg Loring asked
   *   John Seifers to change the command to SEn if voltage limiting is chosen, just so it
   *   will be consistent with the REn series)
   *
   * NOTE: sim mode echoes the current limit when on() is called (for testing purposes),
   *   but that is NOT how the actual hardware will work!  (see the class javadoc)
   *
   * @author Greg Loring
   */
  public void setFilamentCurrentLimitInAmps(double value) throws XrayTesterException
  {
    /** @todo gad assert value is valid */

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.FILAMENT_CURRENT_LIMIT);
    _hardwareObservable.setEnabled(false);
    try
    {
      processSetTurnOnValueCommand("SF2", value, _MAX_FILAMENT_AMPS);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.FILAMENT_CURRENT_LIMIT);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getGridVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RG0", _MAX_GRID_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingGridVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RG2", _MAX_GRID_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingGridVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RG3", _MAX_GRID_VOLTS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingGridVoltageInVolts() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_GRID_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RG1", _MAX_GRID_VOLTS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_GRID_VOLTAGE);
  }

  /**
   * ...on the hardware: sets the anode voltage value that is used (set) during the
   *   next call to on(); the behavior is undefined if called after the X-ray source is
   *   already on (areXraysOn() == true)<p/>
   *
   * NOTE: John Siefers said he thought this would only be useful for causing hardware
   *        "malfunctions" in the lab.
   *
   * @author Greg Loring
   */
  public void setTurnOnGridVoltageInVolts(double value) throws XrayTesterException
  {
    /** @todo gad assert valid value */

    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.GRID_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processSetTurnOnValueCommand("SG0", value, _MAX_GRID_VOLTS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.GRID_VOLTAGE);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getIsolatedSupplyVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RH0", _MAX_ISOLATED_SUPPLY_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMinimumOperatingIsolatedSupplyVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RH2", _MAX_ISOLATED_SUPPLY_VOLTS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getMaximumOperatingIsolatedSupplyVoltageInVolts() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RH3", _MAX_ISOLATED_SUPPLY_VOLTS);
  }

  /**
   * ...on the hardware
   *
   * @author Greg Loring
   */
  public void resetMinMaxOperatingIsolatedSupplyVoltageInVolts() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.MIN_MAX_ISO_PS_VOLTAGE);
    _hardwareObservable.setEnabled(false);
    try
    {
      processReadDoubleValueCommand("RH1", _MAX_ISOLATED_SUPPLY_VOLTS); // don't care about result
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.MIN_MAX_ISO_PS_VOLTAGE);
  }

  /** @todo gets for auxillary values */

  /**
   * ...from the hardware
   *
   * @todo fix name: from Whats to ?
   *
   * @author Greg Loring
   */
  public double getSF6PressureInWhats() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RPI", _MAX_SF6_PRESSURE_UNITS);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getSF6TemperatureInDegreesC() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RTI", _MAX_SF6_DEGREES_C);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public double getGunTemperatureInDegreesC() throws XrayTesterException
  {
    return processReadDoubleValueCommand("RTG", _MAX_GUN_DEGREES_C);
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public int getCumulativeFilamentOnTimeInHours() throws XrayTesterException
  {
    return processReadIntegerValueCommand("RSF");
  }

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public String getSerialNumber() throws XrayTesterException
  {
    return processReadStringValueCommand("RSN");
  }

  /**
   * @author Greg Loring
   */
  private XraySourceStatusEnum getOperationalStatus() throws XrayTesterException
  {
    XraySourceStatusEnum result;

    String reply = processReadStringValueCommand("RXS");
    if (   (reply.length() == 4)
        && (reply.equals("REDY") || reply.equals("STBY") || reply.equals("DOWN") || reply.equals("EROR")))
    {
      result = XraySourceStatusEnum.getEnum(reply);
    }
    else if (   (reply.length() == 8)
             && reply.startsWith("RAMP")
             && _fourDigitStringRegex.matcher(reply.substring(4)).matches())
    {
      result = XraySourceStatusEnum.getEnum(reply.substring(0, 4));

      // keep track of the percent done, as well
      int percentToDo = fourDigitStringToInt(reply.substring(4));
      Assert.expect(percentToDo >= 0); // otherwise, regex wouldn't have matched
      if (percentToDo > 100) {
        throw new SerialCommunicationProtocolHardwareException("X-ray Source", "RAMP0100|RAMP00##", reply);
      }
      _percentDone = 100 - percentToDo;
    }
    else
    {
      throw new SerialCommunicationProtocolHardwareException("X-ray Source",
                                                             "REDY|STBY|DOWN|RAMP0100|RAMP00##|EROR",
                                                             reply);
    }

    return result;
  }

  /**
   * @author Greg Loring
   */
  private String getSelfTestStatus() throws XrayTesterException
  {
    String result;

    String reply = processReadStringValueCommand("RTR");
    if (reply.equals("PASS"))
    {
      result = reply;
    }
    else if (   (reply.length() == 8)
             && (reply.startsWith("TEST") || reply.startsWith("FAIL"))
             && _fourDigitStringRegex.matcher(reply.substring(4)).matches())
    {
      if (reply.startsWith("TEST"))
      {
        result = reply.substring(0, 4);

        // keep track of the percent done, as well
        int percentToDo = fourDigitStringToInt(reply.substring(4));
        Assert.expect(percentToDo >= 0); // otherwise, regex wouldn't have matched
        if (percentToDo > 100) {
          throw new SerialCommunicationProtocolHardwareException("X-ray Source", "TEST0100|TEST00##", reply);
        }
        _percentDone = 100 - percentToDo;
      }
      else // FAIL####
      {
        result = reply;
      }
    }
    else
    {
      throw new SerialCommunicationProtocolHardwareException("X-ray Source",
                                                             "PASS|TEST0100|TEST00##|FAIL####",
                                                             reply);
    }

    return result;
  }

  /**
   * @author Greg Loring
   */
  private void processInitiateActionCommand(String command) throws XrayTesterException
  {
    List<String> replies = sendCommandAndGetReplies(command);

    // check for hardware reported "EROR"
    if ((replies.size() == 2) && replies.get(0).equals("EROR") && replies.get(1).equals("OK"))
    {
      /** @todo resolve the "hopefully" */
      checkForHardwareReportedErrors(); // hopefully this will throw
    }

    checkForOK(replies, 1);
  }

  /**
   * @author Greg Loring
   */
  private void processSetTurnOnValueCommand(String commandPrefix,
                                            double value,
                                            double maxValue) throws XrayTesterException
  {
    /** @todo gad assert commandPrefix != null */

    long param = Math.round(value * ((double) 4095.0 / maxValue));
    Assert.expect((param >= 0) && (param <= 4095));

    // convert param to a zero-padded, 4-digit string
    String paramString = intToFourDigitString(param);

    String command = commandPrefix + ',' + paramString;

    List<String> replies = sendCommandAndGetReplies(command);
    checkForOK(replies, 1);
  }

  /**
   * @author Greg Loring
   */
  private double processReadDoubleValueCommand(String command, double maxValue) throws XrayTesterException
  {
    int reply = processReadIntegerValueCommand(command);
    if ((reply < 0) || (reply > 4095))
    {
      throw new SerialCommunicationProtocolHardwareException("X-ray Source",
                                                             "0<=n<=4095",
                                                             Integer.toString(reply));
    }
    double result = ((double) reply) * (maxValue / 4095.0);
    return result;
  }

  /**
   * @author Greg Loring
   */
  private int processReadIntegerValueCommand(String command) throws XrayTesterException
  {
    String reply = processReadStringValueCommand(command);
    if (_fourDigitStringRegex.matcher(reply).matches())
    {
      int result = fourDigitStringToInt(reply);
      Assert.expect(result >= 0); // otherwise, regex wouldn't have matched
      return result;
    }
    else
    {
      throw new SerialCommunicationProtocolHardwareException("X-ray Source", "####", reply);
    }
  }

  /**
   * @author Greg Loring
   */
  private String processReadStringValueCommand(String command) throws XrayTesterException
  {
    List<String> replies = sendCommandAndGetReplies(command);
    checkForOK(replies, 2);
    String result = replies.get(0);
    return result;
  }

  /**
   * the synchronization of the this method *should* make the whole class thread safe
   *
   * @author George A. David: USB link management
   * @author Greg Loring: added simulation mode switch and Asserts
   */
  private synchronized List<String> sendCommandAndGetReplies(String command) throws XrayTesterException
  {
    /** @todo gad assert command != null */

    List<String> replies;

    if (isSimulationModeOn())
    {
      replies = simulateSendCommandAndGetReplies(command);
    }
    else
    {
      String message = command + '\n';
      replies = _usbUtil.sendMessageAndGetReplies(_xraySourceDescription, message);
    }

    for (String reply : replies)
    {
      Assert.expect(reply.endsWith("\n") == false); // if this assert fails, then Greg Loring's guesses
                                                    //  about _usbUtil's behavior were wrong: change code
                                                    //  to expect / strip linefeeds
    }

    return replies;
  }

  /**
   * @author Greg Loring
   */
  private void checkForOK(List<String> replies, int expectedNumReplies) throws XrayTesterException
  {
    /** @todo gad assert replies != null */

    Assert.expect(expectedNumReplies >= 1);
    int okIndex = expectedNumReplies - 1;

    if (replies.size() != expectedNumReplies)
    {
      String reply = "<none>";
      if (replies.size() > 0)
      {
        reply = "";
        for (String s : replies)
        {
        reply += s + "\\n";
        }
      }
      throw new SerialCommunicationProtocolHardwareException("X-ray Source", ".*\\nOK\\n", reply);
    }
    else if (replies.get(okIndex) != "OK")
    {
      throw new SerialCommunicationProtocolHardwareException("X-ray Source", "OK", replies.get(1));
    }
  }

  /**
   * the command protocol requires a lot of 4-digit string to int conversion:
   *   put all of the safety checking in one spot
   *
   * @author Greg Loring
   */
  static private int fourDigitStringToInt(String fourDigits)
  {
    /** @todo gad assert four digits != null */

    // calling code is responsible for making certain that the input string matches
    //  the _fourDigitStringRegex, but go ahead and Assert this anyway
    Assert.expect(_fourDigitStringRegex.matcher(fourDigits).matches());

    try
    {
      int result = Integer.parseInt(fourDigits, 10);

      // double check the implementation
      Assert.expect((result >= 0) && (result <= 9999), "Integer.parseInt(\"" + fourDigits + "\") returned " + result);

      return result;
    }
    catch (NumberFormatException nfx)
    {
      Assert.expect(false, "four digits should ALWAYS convert cleanly to a radix-10 int: " + fourDigits);
      throw new IllegalStateException("won't get here, but the compiler doesn't know that");
    }
  }

  /**
   * the command protocol requires a lot of integer to 4-digit string conversion:
   *   put all of the safety checking in one spot
   *
   * @author Greg Loring
   */
  static private String intToFourDigitString(long value)
  {
    // calling code is responsible for making sure that input value is in the proper
    //  range, but go ahead and Assert this anyway
    Assert.expect((value >= 0) && (value <= 9999));

    // some people may not like this implementation, but at least it is only in one place
    String result = Long.toString(10000 + value, 10).substring(1);

    // double check the implementation
    Assert.expect(_fourDigitStringRegex.matcher(result).matches());

    return result;
  }

  /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Greg Loring
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  //
  // the rest of the class implements simulation mode;
  //  - a bit longer than expected, but it clearly documents the assumptions of the code;
  //  - similarly, the sim code represents a rigidly codified version of Greg Loring's
  //     interpretation of the "N7280-66236-3_update 27 Mar 06" firmware specification;
  //  - if you remove this sim code, don't forget to update the class javadoc comments
  //     regarding testability
  //

  /**
   * @author Greg Loring
   */
  private List<String> simulateSendCommandAndGetReplies(String command) throws HardwareException
  {
/** @todo gad assert command != null */

    Assert.expect((command.length() == 3) || (command.length() == 8), "illegal command: " + command);
    char firstChar = command.charAt(0);
    switch (firstChar)
    {
      case 'X':
        return simulateInitiateActionCommand(command);

      case 'S':
        return simulateSetTurnOnValueCommand(command);

      case 'R':
        return simulateReadValueCommand(command);

      default:
        Assert.expect(false, "illegal command: " + command);
        throw new IllegalStateException("Assert should have thrown");
    }
  }

  /**
   * @author Greg Loring
   */
  private List<String> simulateInitiateActionCommand(String command) throws HardwareException
  {
/** @todo gad assert command != null */
    Assert.expect(command.charAt(0) == 'X');

    if (command.equals("XON")) // on
    {
      _simOperationalStatus = "RAMPZZZZ";
      _simLastTurnOnStartTimeMillis = System.currentTimeMillis();
      _simSelfTestStatus = "<undefined>";
      _simLastSelfTestStartTimeMillis = -1L;
    }
    else if (command.equals("XOS")) // off (short-term shutdown)
    {
      _simOperationalStatus = "STBY";
      _simLastTurnOnStartTimeMillis = -1L;
      _simSelfTestStatus = "<undefined>";
      _simLastSelfTestStartTimeMillis = -1L;
    }
    else if (command.equals("XOF")) // power off (long-term shutdown)
    {
      _simOperationalStatus = "DOWN";
      _simLastTurnOnStartTimeMillis = -1L;
      _simSelfTestStatus = "<undefined>";
      _simLastSelfTestStartTimeMillis = -1L;
    }
    else if (command.equals("XST")) // self test
    {
      _simOperationalStatus = "STBY";
      _simLastTurnOnStartTimeMillis = -1L;
      _simSelfTestStatus = "TESTZZZZ";
      _simLastSelfTestStartTimeMillis = System.currentTimeMillis();
    }
    else
    {
      Assert.expect(false, "illegal command: " + command);
      throw new IllegalStateException("Assert should have thrown");
    }

    return successReplies();
  }

  /**
   * @author Greg Loring
   */
  private List<String> simulateSetTurnOnValueCommand(String command) throws HardwareException
  {
/** @todo gad assert command != null */
    Assert.expect(command.length() == 8);
    Assert.expect(command.charAt(0) == 'S');
    Assert.expect(command.charAt(3) == ',');
    Assert.expect(_fourDigitStringRegex.matcher(command.substring(4)).matches());
    int value = fourDigitStringToInt(command.substring(4));
    Assert.expect((value >= 0) && (value <= 4095));
    char operationalValueId = command.charAt(1);
    Assert.expect(command.charAt(2) == getTurnOnCommandSuffix(operationalValueId));
    setSimTurnOnValue(operationalValueId, value);
    return successReplies();
  }

  /**
   * @author Greg Loring
   */
  private List<String> simulateReadValueCommand(String command) throws HardwareException
  {
/** @todo gad assert command != null */
    Assert.expect(command.length() == 3);
    Assert.expect(command.charAt(0) == 'R');

    if (_simOperationalValueMap.containsKey(command.charAt(1)))
    {
      char operationalValueId = command.charAt(1);

      Assert.expect(Character.isDigit(command.charAt(2)));
      int commandSubCode = command.charAt(2) - '0';
      switch (commandSubCode)
      {
        case 0: // return current value
          return successReplies(getSimCurrentValue(operationalValueId));

        case 1: // reset min/max to current value, then return current value
          return successReplies(resetSimMinMaxValue(operationalValueId));

        case 2: // return minimum "on" value
          return successReplies(getSimMinOnValue(operationalValueId));

        case 3: // return minimum "on" value
          return successReplies(getSimMaxOnValue(operationalValueId));

        default:
          Assert.expect(false, "illegal command: " + command);
          throw new IllegalStateException("Assert should have thrown");
      }
    }
    else if (_simAuxilliaryIntegerValueMap.containsKey(command))
    {
      int value = _simAuxilliaryIntegerValueMap.get(command);
      return successReplies(value);
    }
    else if (_simAuxilliaryStringValueMap.containsKey(command))
    {
      String strValue = _simAuxilliaryStringValueMap.get(command);
      return successReplies(strValue);
    }
    else if (command.equals("RXS"))
    {
      return successReplies(getSimOperationalStatus());
    }
    else if (command.equals("RSR")) // reset latched error status
    {
      int status = _simAuxilliaryIntegerValueMap.get("RSL");
      _simAuxilliaryIntegerValueMap.put("RSL", 0);
      return successReplies(status);
    }
    else if (command.equals("RTR"))
    {
      return successReplies(getSimSelfTestStatus());
    }
    else
    {
      Assert.expect(false, "illegal command: " + command);
      throw new IllegalStateException("Assert should have thrown");
    }
  }

  /**
   * @author Greg Loring
   */
  /** @todo gad can you come up with a better name.  getSuccessfulReplies() or something*/

  private List<String> successReplies()
  {
    LinkedList<String> replies = new LinkedList<String>();
    Assert.expect(replies.offer("OK"));
    return replies;
  }

  /**
   * @author Greg Loring
   */
  private List<String> successReplies(int value)
  {
    Assert.expect((value >= 0) && (value <= 9998));
    String strValue = intToFourDigitString(value);
    return successReplies(strValue);
  }

  /**
   * @author Greg Loring
   */
  private List<String> successReplies(String strValue)
  {
    /** @todo gad assert strValue != null */

    LinkedList<String> replies = new LinkedList<String>();
    Assert.expect(replies.offer(strValue));
    Assert.expect(replies.offer("OK"));
    return replies;
  }

  //
  // from here down is primarily simulator state (as opposed to simulator interface)
  //

  /**
   * @author Greg Loring
   */
  private void initializeSimStateMaps()
  {
    _simOperationalValueMap = new LinkedHashMap<Character, int[]>();
    // which ->      turnOnCommandSuffix (-1 => n/a),    off,     on,  minOn,  maxOn
    //                          anode voltage [kV]         0,    160,    158,    161
    _simOperationalValueMap.put('K', new int[] { '0',      0,   3971,   3921,   3996 });
    //                          cathode current [uA]       0,    100,     97,    103
    _simOperationalValueMap.put('A', new int[] { '0',      0,   2000,   1940,   2061 });
    //                          filament voltage [V]      .2,   4.75,    4.5,      5
    _simOperationalValueMap.put('E', new int[] {  -1,    100,   2375,   2250,   2500 });
    //                          filament current [A]      .1,      2,   1.75,   2.25
    _simOperationalValueMap.put('F', new int[] { '2',     50,   1000,    875,   1125 });
    //                          grid voltage [V]        4095,   2000,   1900,   2100
    _simOperationalValueMap.put('G', new int[] { '0',   4095,   2000,   1900,   2100 });
    //                    isolated supply voltage [V]     20,     20,     19,     21
    _simOperationalValueMap.put('H', new int[] {  -1,   2000,   2000,   1900,   2100 });

    _simAuxilliaryIntegerValueMap = new LinkedHashMap<String, Integer>();
    _simAuxilliaryIntegerValueMap.put("RSL", 0x3); // latched error status
    _simAuxilliaryIntegerValueMap.put("RPI", 1234); // internal SF_6 pressure: 1234 whats
    _simAuxilliaryIntegerValueMap.put("RTI", 2730); // internal SF_6 temperature: 100 degrees C
    _simAuxilliaryIntegerValueMap.put("RTG", 4095); // gun temperature: 4095 degrees C (which is clearly wrong)
    _simAuxilliaryIntegerValueMap.put("RSF", 1); // cumulative filament on time

    _simAuxilliaryStringValueMap = new LinkedHashMap<String, String>();
    _simAuxilliaryStringValueMap.put("RV0", "0010"); // firmware revision
    _simAuxilliaryStringValueMap.put("RSN", "060100001"); // serial number
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private int getTurnOnCommandSuffix(char operationalValueId)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    int result = operationValueData[0];
    return result;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private int getSimTurnOffValue(char operationalValueId)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    int result = operationValueData[1];
    return result;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private int getSimTurnOnValue(char operationalValueId)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    int result = operationValueData[2];
    return result;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private void setSimTurnOnValue(char operationalValueId, int value)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    operationValueData[2] = value;

    int newMin = (9 * value) / 10;
    if (newMin < getSimMinOnValue(operationalValueId))
    {
      setSimMinOnValue(operationalValueId, newMin);
    }

    int newMax = (11 * value) / 10;
    newMax = (newMax > 4095) ? 4095 : newMax;
    if (newMax > getSimMaxOnValue(operationalValueId))
    {
      setSimMaxOnValue(operationalValueId, newMax);
    }
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private int getSimMinOnValue(char operationalValueId)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    int result = operationValueData[3];
    return result;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private void setSimMinOnValue(char operationalValueId, int value)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    operationValueData[3] = value;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private int getSimMaxOnValue(char operationalValueId)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    int result = operationValueData[4];
    return result;
  }

  /**
   * array interpreter
   *
   * @author Greg Loring
   */
  private void setSimMaxOnValue(char operationalValueId, int value)
  {
    int[] operationValueData = _simOperationalValueMap.get(operationalValueId);
    Assert.expect((operationValueData != null) && (operationValueData.length == 5));
    operationValueData[4] = value;
  }

  /**
   * @author Greg Loring
   */
  private int getSimCurrentValue(char operationalValueId)
  {
    if (_simLastTurnOnStartTimeMillis != -1L)
    {
      long now = System.currentTimeMillis();
      long t0 = _simLastTurnOnStartTimeMillis;
      if ((now - t0) > _simTurnOnDurationMillis)
      {
        return getSimTurnOnValue(operationalValueId);
      }
      else
      {
        long v0 = getSimTurnOffValue(operationalValueId);
        long vf = getSimTurnOnValue(operationalValueId);
        long result = v0 + (((now - t0) * (vf - v0)) / _simTurnOnDurationMillis);
        Assert.expect((result >= 0) && (result <= 4095));
        return (int)result;
      }
    }
    else
    {
      return getSimTurnOffValue(operationalValueId);
    }
  }

  /**
   * @author Greg Loring
   */
  private int resetSimMinMaxValue(char operationalValueId)
  {
    int currentValue = getSimCurrentValue(operationalValueId);
    setSimMinOnValue(operationalValueId, currentValue);
    setSimMaxOnValue(operationalValueId, currentValue);
    return currentValue;
  }

  /**
   * @author Greg Loring
   */
  private String getSimOperationalStatus()
  {
    if (_simLastTurnOnStartTimeMillis != -1L)
    {
      long elapsed = System.currentTimeMillis() - _simLastTurnOnStartTimeMillis;
      if (elapsed >= _simTurnOnDurationMillis)
      {
        _simOperationalStatus = "REDY";
      }
      else
      {
        long percentToGo = (100 * (_simTurnOnDurationMillis - elapsed)) / _simTurnOnDurationMillis;
        Assert.expect((percentToGo >= 0) && (percentToGo <= 100));
        _simOperationalStatus = "RAMP" + intToFourDigitString((int) percentToGo);
      }
    }

    return _simOperationalStatus;
  }

  /**
   * @author Greg Loring
   */
  private String getSimSelfTestStatus()
  {
    if (_simLastSelfTestStartTimeMillis != -1L)
    {
      long elapsed = System.currentTimeMillis() - _simLastSelfTestStartTimeMillis;
      if (elapsed >= _simSelfTestDurationMillis)
      {
        _simSelfTestStatus = "PASS";
        _simLastSelfTestStartTimeMillis = -1L;
      }
      else
      {
        long percentToGo = (100 * (_simSelfTestDurationMillis - elapsed)) / _simSelfTestDurationMillis;
        Assert.expect((percentToGo >= 0) && (percentToGo <= 100));
        _simSelfTestStatus = "TEST" + intToFourDigitString((int) percentToGo);
      }
    }

    return _simSelfTestStatus;
  }
  
  /*
   * Chin-Seong, Kee - Set Virtual Live XrayTube Voltage And Current
   */
  public void setXraySourceParametersForServiceMode(double voltage, double current) throws XrayTesterException
  {
     setTurnOnAnodeVoltageInKiloVolts(voltage);
     setTurnOnCathodeCurrentInMicroAmps(current);
  }
}

