package com.axi.v810.hardware;

/**
 * @author Rex Shang
 */
public class LeftPanelClearSensorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static LeftPanelClearSensorEventEnum SENSOR_BLOCKED = new LeftPanelClearSensorEventEnum(++_index);
  public static LeftPanelClearSensorEventEnum SENSOR_CLEARED = new LeftPanelClearSensorEventEnum(++_index);

  /**
   * @author Rex Shang
   */
  private LeftPanelClearSensorEventEnum(int id)
  {
    super(id);
  }

}
