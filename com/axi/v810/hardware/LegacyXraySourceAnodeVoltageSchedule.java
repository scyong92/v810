package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * The LegacyXraySourceAnodeVoltageSchedule is an implementation class for LegacyXraySource.
 *   If you are not doing maintenance programming on the LegacyXraySource class, then
 *   stop reading now.<p/>
 *
 * responsible for knowing what anode voltage to command when<p/>
 *
 * this class is completely testable without hardware<p/>
 *
 * not thread safe
 *
 * @author Greg Loring
 */
/*package*/class LegacyXraySourceAnodeVoltageSchedule
{

  // voltage ramp table provided by Dave Reynolds (a hardware guy)
  static private double[][] _anodeVoltageTable = { // % of time, % of voltage
    {   0.0,   0.0 },
    {  10.0,  50.0 },
    {  20.0,  70.0 },
    {  30.0,  80.0 },
    {  40.0,  85.0 },
    {  50.0,  89.0 },
    {  60.0,  92.0 },
    {  70.0,  95.0 },
    {  80.0,  97.0 },
    {  90.0,  98.5 },
    { 100.0, 100.0 }
  };

  // scale values used to turn percentages from table into actual voltages and times
  private double _kVScale;
  private double _msScale;

  /**
   * table provided by the hardware guys is in percentages, so we need to provide
   *  the scale values
   *
   * @author Greg Loring
   */
  LegacyXraySourceAnodeVoltageSchedule(double kVScale, double msScale)
  {
    Assert.expect((kVScale > 0.0) && (kVScale < 200.0)); // upper bound is reality, but not strictly necessary
    Assert.expect(msScale > 0.0);
    _kVScale = kVScale;
    _msScale = msScale;

    // quick sanity check on the table data: should go from 0 to 100, monotonically increasing
    Assert.expect(getPctTime(0) == 0.0); // literal, so no chance of rounding
    Assert.expect(getPctVoltage(0) == 0.0); // literal, so no chance of rounding
    for (int timeIndex = 1; timeIndex < tableSize(); timeIndex++)
    {
      Assert.expect(getPctTime(timeIndex) >= getPctTime(timeIndex - 1));
      Assert.expect(getPctVoltage(timeIndex) >= getPctVoltage(timeIndex - 1));
    }
    int lastTimeIndex = tableSize() - 1;
    Assert.expect(getPctTime(lastTimeIndex) == 100.0); // literal, so no chance of rounding
    Assert.expect(getPctVoltage(lastTimeIndex) == 100.0); // literal, so no chance of rounding
  }

  /**
   * @return double number of kV which should be commanded at the given time offset (ms)
   *
   * @author Greg Loring
   */
  double getKiloVolts(long msTimeOffset)
  {
    Assert.expect((msTimeOffset >= 0L));

    double pctVoltage;
    double pctTime = (msTimeOffset / _msScale) * 100.0;
    int lowerTimeIndex = findTimeIndexAtOrBefore(pctTime);
    if (lowerTimeIndex < (tableSize() - 1))
    {
      // linear interpolation (don't know what this is? google it)
      int upperTimeIndex = lowerTimeIndex + 1;
      double t0 = getPctTime(lowerTimeIndex);
      double v0 = getPctVoltage(lowerTimeIndex);
      double tf = getPctTime(upperTimeIndex);
      double vf = getPctVoltage(upperTimeIndex);
      double rate = (vf - v0) / (tf - t0);
      double deltaT = (pctTime - t0);
      pctVoltage = v0 + (rate * deltaT);
    }
    else
    {
      // upperTimeIndex would off the end of the table, so just return last entry
      Assert.expect(lowerTimeIndex == (tableSize() - 1));
      pctVoltage = getPctVoltage(lowerTimeIndex);
    }

    double kV = (pctVoltage / 100.0) * _kVScale;

    return kV;
  }

  /**
   * at how many milli-seconds into schedule does one find the given voltage
   * @param kV must be >= 0
   * @return # of ms (ceiling), or final time in ms if kV is greater than largest kV in the schedule
   *
   * @author Greg Loring
   */
  long getTimeOffsetInMillis(double kV)
  {
    Assert.expect(kV >= 0.0);

    double pctTime;
    double pctVoltage = (kV / _kVScale) * 100.0;
    int lowerTimeIndex = findTimeIndexForVoltageAtOrBelow(pctVoltage);
    if (lowerTimeIndex < (tableSize() - 1))
    {
      // linear interpolation (don't know what this is? google it)
      int upperTimeIndex = lowerTimeIndex + 1;
      double t0 = getPctTime(lowerTimeIndex);
      double v0 = getPctVoltage(lowerTimeIndex);
      double tf = getPctTime(upperTimeIndex);
      double vf = getPctVoltage(upperTimeIndex);
      double rate = (tf - t0) / (vf - v0);
      double deltaV = (pctVoltage - v0);
      pctTime = t0 + (rate * deltaV);
    }
    else
    {
      // upperTimeIndex would be off the end of the table, so just return last entry
      Assert.expect(lowerTimeIndex == (tableSize() - 1));
      pctTime = getPctTime(lowerTimeIndex);
    }

    // ceil worked, but then the cast was still truncating, hence round
    long ms = (long) Math.round(Math.ceil((pctTime / 100.0) * _msScale));

    return ms;
  }

  /** time bracketing method which finds lower bound
   * @author Greg Loring */
  private int findTimeIndexAtOrBefore(double pctTime)
  {
    Assert.expect((pctTime >= 0.0));

    int timeIndex;
    for (timeIndex = 0; timeIndex < (tableSize() - 1); timeIndex++)
    {
      if (getPctTime(timeIndex + 1) > pctTime)
      {
        break;
      }
    }
    return timeIndex;
  }

  /** voltage bracketing method which finds lower bound
   * @author Greg Loring */
  private int findTimeIndexForVoltageAtOrBelow(double pctVoltage)
  {
    Assert.expect((pctVoltage >= 0.0));

    int timeIndex;
    for (timeIndex = 0; timeIndex < (tableSize() - 1); timeIndex++)
    {
      if (getPctVoltage(timeIndex + 1) > pctVoltage)
      {
        break;
      }
    }
    return timeIndex;
  }

  /** multi-dimensional array interpreter
   * @author Greg Loring */
  private int tableSize()
  {
    return _anodeVoltageTable.length;
  }

  /** multi-dimensional array interpreter
   * @author Greg Loring */
  private double getPctTime(int timeIndex)
  {
    return _anodeVoltageTable[timeIndex][0];
  }

  /** multi-dimensional array interpreter
   * @author Greg Loring */
  private double getPctVoltage(int timeIndex)
  {
    return _anodeVoltageTable[timeIndex][1];
  }
}

