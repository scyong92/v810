package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This abstract class provides the common function for either the left or
 * right panel clear sensor (AKA panel crunch sensor).
 * @author Rex Shang
 */
public abstract class PanelClearSensor extends HardwareObject implements PanelClearSensorInt
{
  protected DigitalIo _digitalIo;
  private WaitForHardwareCondition _waitForHardwareCondition;

  // RMS: The following attribute should be in PanelHandler since it has
  // association with the current implementation of stage belts...
  // Stage belt moves 13 inches / second.  Anticipate "small" holes for about 1 inch.
  private static final long _SENSOR_BUFFER_TIME_TO_AVOID_HOLE_IN_PANEL_IN_MILLI_SECONDS = 200;

  private static final long _SETTLING_TIME_FOR_SENSOR_TO_CLEAR_IN_MILLI_SECONDS = _SENSOR_BUFFER_TIME_TO_AVOID_HOLE_IN_PANEL_IN_MILLI_SECONDS;
  private static final long _SETTLING_TIME_FOR_SENSOR_TO_BLOCK_IN_MILLI_SECONDS = 0;

  /**
   * This contructor is not public on purpose.  We should always use getInstance().
   * @author Rex Shang
   */
  protected PanelClearSensor()
  {
    _digitalIo = DigitalIo.getInstance();
  }

  /**
   * Wait for the sensor to be blocked.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param abortable boolean to indicator whether this wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilBlocked(long timeOutInMilliSeconds, boolean abortable) throws XrayTesterException
  {
    waitUntilBlocked(timeOutInMilliSeconds, _digitalIo.getHardwarePollingIntervalInMilliSeconds(), abortable);
  }

  /**
   * Wait for the sensor to be blocked.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param pollingIntervalInMilliSeconds how fast we should poll the hardware.
   * @param abortable boolean to indicator whether this wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilBlocked(long timeOutInMilliSeconds, long pollingIntervalInMilliSeconds, boolean abortable) throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);
    Assert.expect(pollingIntervalInMilliSeconds >= 0);

    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             _SETTLING_TIME_FOR_SENSOR_TO_BLOCK_IN_MILLI_SECONDS,
                                                             pollingIntervalInMilliSeconds,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        return isBlocked();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK);
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

  /**
   * Wait for the sensor to be cleared.
   * Due to sensor is not able to detect clear condition reliably, we have to
   * use debounce (check sensor during an interval) to make sure.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param abortable bool
   * @author Rex Shang
   */
  public void waitUntilClear(long timeOutInMilliSeconds, boolean abortable) throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0, "Invalid timeOutInMilliSeconds.");
    if (timeOutInMilliSeconds > 0)
      Assert.expect(timeOutInMilliSeconds > _SENSOR_BUFFER_TIME_TO_AVOID_HOLE_IN_PANEL_IN_MILLI_SECONDS, "Invalid timeOutInMilliSeconds.");

    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             _SETTLING_TIME_FOR_SENSOR_TO_CLEAR_IN_MILLI_SECONDS,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean panelClearSensorIsCleared = isBlocked() == false;
        return panelClearSensorIsCleared;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR);
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

  /**
   * Aborting the wait.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    super.abort();
    if (_waitForHardwareCondition != null)
      _waitForHardwareCondition.abort();
  }

  /**
   * Temporary method to return whether the active low type of PCS is installed.
   * This is done to ease the transition.
   * @todo RMS This method needs to be removed prior to release.
   * @author Rex Shang
   */
  public static boolean isActiveLowTypeInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_CLEAR_SENSOR_ACTIVE_LOW_TYPE_INSTALLED);
  }

  /**
   * Temporary mehtod to check if the PCS is installed more inward to the system.
   * @todo RMS This method needs to be removed prior to release.
   * @author Rex Shang
   */
  public static boolean isRecessedMountInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_CLEAR_SENSOR_RECESSED_MOUNT_INSTALLED);
  }

}
