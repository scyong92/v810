package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for left outer barrier
 * @author Rex Shang
 */
class Test_InnerBarrier extends UnitTest
{
  private InnerBarrier _innerBarrier = null;

  private Test_InnerBarrier()
  {
    _innerBarrier = InnerBarrier.getInstance();
  }

  public static void main(String[] args)
  {
    Test_InnerBarrier _test_InnerBarrier = new Test_InnerBarrier();
    UnitTest.execute(_test_InnerBarrier);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // XCR-3604 Unit Test Phase 2
      if (DigitalIo.getInstance().isStartupRequired())
        DigitalIo.getInstance().startup();
      
      // Close the barrier and verify
      _innerBarrier.close();
      Expect.expect(_innerBarrier.isOpen() == false);

      // Open the barrier and verify.
      _innerBarrier.open();
      Expect.expect(_innerBarrier.isOpen() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
