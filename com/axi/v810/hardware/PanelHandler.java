package com.axi.v810.hardware;

import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class provides all functionality related to loading and unloading panels
 * for the Xray machine.
 * @author Rex Shang
 */
public class PanelHandler extends HardwareObject implements HardwareStartupShutdownInt
{

  private static PanelHandler _instance;
  //Xray tube crash preventive action
  //flag to indicate when need to check
  // true = double check the panel when CD&A timeout
  // false = bypass checking.(be caution when set to false)
  private static boolean _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;
  private static boolean _passedHighMagSafetyLevelSensorScan = false;
  private static boolean _passedS2EXHighMagSafetyLevelSensorScan = false;
  private static boolean _passedS2EXLowMagSafetyLevelSensorScan = false;
  private static final boolean _overrideEjectPanelDuringStartup = true;
  private boolean _repeatUnloadLoadModeOn = false;
  private PanelPositioner _panelPositioner;
  private MotionControl _motionControl;
  private StageBelts _stageBelts;
  private PanelClamps _panelClamps;
  private LeftPanelClearSensor _leftPanelClearSensor;
  private RightPanelClearSensor _rightPanelClearSensor;
  private LeftPanelInPlaceSensor _leftPanelInPlaceSensor;
  private RightPanelInPlaceSensor _rightPanelInPlaceSensor;
  private LeftOuterBarrier _leftOuterBarrier;
  private RightOuterBarrier _rightOuterBarrier;
  private ManufacturingEquipmentInterface _manufacturingEquipmentInterface;
  private HardwareObservable _hardwareObservable;
  private HardwareTaskEngine _hardwareTaskEngine;
  private ProgressObservable _progressObservable;
  private ExecuteParallelThreadTasks<Object> _panelHandlerParallelTasks;
  private PanelHandlerPanelFlowDirectionEnum _flowDirection;
  private PanelHandlerPanelLoadingModeEnum _loadingMode;
  private boolean _initialized;
  private List<HardwareObject> _hardwareObjectList;
  // Panel information.
  private String _projectName;
  private int _panelWidthInNanoMeters;
  private int _panelLengthInNanoMeters;
  private boolean _panelLoaded;
  private PanelInPlaceSensorInt _panelIsAgainst;
  private boolean _panelIsLoading;
  private int _panelExtraClearDelayInMiliSeconds;
  private PanelHandlerPersistentData _panelHandlerPersistentData;
  // Variables used for panel flow.
  private BarrierInt _primaryOuterBarrier;
  private BarrierInt _secondaryOuterBarrier;
  private PanelClearSensor _primaryPanelClearSensor;
  private PanelClearSensor _secondaryPanelClearSensor;
  private PanelInPlaceSensor _primaryPanelInPlaceSensor;
  private PanelInPlaceSensor _secondaryPanelInPlaceSensor;
  //Variable Mag Anthony August 2011
  private SafetyLevelSensorInt _primarySafetyLevelSensor;
  private SafetyLevelSensorInt _secondarySafetyLevelSensor;
  private LeftSafetyLevelSensor _leftSafetyLevelSensor;
  private RightSafetyLevelSensor _rightSafetyLevelSensor;
  private boolean _twoStageInitializationInProgress = false;
  // Timeout constants.
  private static final long _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK_FOR_LOADING_TIME_OUT_IN_MILLI_SECONDS = 0;
  private static final long _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS = 10000;
  private static final long _WAIT_FOR_PANEL_TO_ENGAGE_PANEL_IN_PLACE_SENSOR_TIME_OUT_IN_MILLI_SECONDS = 21000;
  private static final long _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS = 0;
  private static final long _WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM_TIME_OUT_IN_MILLI_SECONDS = 0;
  private static final long _WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL_TIME_OUT_IN_MILLI_SECONDS = 0;
  private static final long _WAIT_FOR_DOWNSTREAM_SYSTEM_TO_FINISH_ACCEPT_PANEL_TIME_OUT_IN_MILLI_SECONDS = 20000;
  private static final long _WAIT_FOR_PANEL_TO_CLEAR_BARRIER_IN_MANUAL_MODE_TIME_OUT_IN_MILLI_SECONDS = 800;
  private static final long _WAIT_FOR_PANEL_TO_CLEAR_BARRIER_IN_IN_LINE_MODE_TIME_OUT_IN_MILLI_SECONDS = 20;//300;
  private static final long _WAIT_FOR_PANEL_TO_REACH_PANEL_CLEAR_SENSOR_TIME_OUT_IN_MILLI_SECONDS = 8000;
  private static final long _MINIMUM_INTERVAL_TIME_TO_DETECT_PANEL_IS_CLEAR = 3000; // XCR1161 - Hole on carrier causing outer barrier close without unload
  // XCR2008, Preserve old maxThickness value, Khang Wah
  private static final int _LEGACY_MAX_BOARD_THICKNESS = MathUtil.convertMilsToNanoMetersInteger(125);
  // Belt moves 13 inches / second.
  private static final long _TWO_INCH_MOVE_TIME_IN_MILLI_SECONDS = 250;
  private static final boolean _WAIT_ABORTABLE = true;
  private static final boolean _WAIT_NOT_ABORTABLE = false;
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 20000;
  private static final int _DEFAULT_EXTRA_CLEAR_DELAY_IN_MILLISECONDS = 5000;
  private static int _panelNotInPlaceCounter = 0;
  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  protected DigitalIo _digitalIo;
  
  // XCR-3116 Intermittent board chop during production looping using SMEMA
  private static SmemaLogUtil _smemaLogUtil = SmemaLogUtil.getInstance();

  //For Speed Up
  static boolean  _isProduction =false;
  
  protected static boolean _debug = Config.isDeveloperDebugModeOn();
   private static String _me = "PanelHandler";
   
  // log conveyor signal - debug sanmina board chop issue
  private boolean _logSMEMAConveyorSignal = false;
  private static WorkerThread _smemaSignalPollingThread = new WorkerThread("smemaSignalPollingThread");
  private static final int _SMEMA_SIGNAL_POLLING_INTERVAL_IN_MILLI_SECONDS = 20;
  
  private boolean _forceLoadPanelTORight = false;
  private boolean _keepOuterBarrierOpenAfterUnloadingPanel = false;
  private boolean _keepOuterBarrierOpenForLoadingPanel = false;

  //Khaw Chek Hau - XCR3774: Display load & unload time during production
  private long _productionPanelLoadingTimeInMilis = 0;
  private long _productionPanelUnloadingTimeInMilis = 0;
   
  /**
   * This contructor is not public on purpose.
   * @author Rex Shang
   */
  private PanelHandler()
  {
    _digitalIo = DigitalIo.getInstance();

    _repeatUnloadLoadModeOn = false;
    _initialized = false;
    _projectName = "";
    _panelWidthInNanoMeters = 0;
    _panelLengthInNanoMeters = 0;
    _panelLoaded = false;

    _hardwareObjectList = new ArrayList<HardwareObject>();

    _panelPositioner = PanelPositioner.getInstance();
    _hardwareObjectList.add(_panelPositioner);

    // Look up our motion controller through config file.
    Integer motionControllerId = (Integer) getConfig().getValue(HardwareConfigEnum.PANEL_HANDLER_MOTION_CONTROLLER_ID);
    MotionControllerIdEnum motionControllerIdEnum = MotionControllerIdEnum.getEnum(motionControllerId);
    _motionControl = MotionControl.getInstance(motionControllerIdEnum);
    _hardwareObjectList.add(_motionControl);

    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _progressObservable = ProgressObservable.getInstance();
    _panelHandlerParallelTasks = new ExecuteParallelThreadTasks<Object>();

    _stageBelts = StageBelts.getInstance();
    _hardwareObjectList.add(_stageBelts);
    _panelClamps = PanelClamps.getInstance();
    _hardwareObjectList.add(_panelClamps);
    _leftPanelClearSensor = LeftPanelClearSensor.getInstance();
    _hardwareObjectList.add(_leftPanelClearSensor);
    _rightPanelClearSensor = RightPanelClearSensor.getInstance();
    _hardwareObjectList.add(_rightPanelClearSensor);
    _leftPanelInPlaceSensor = LeftPanelInPlaceSensor.getInstance();
    _hardwareObjectList.add(_leftPanelInPlaceSensor);
    _rightPanelInPlaceSensor = RightPanelInPlaceSensor.getInstance();
    _hardwareObjectList.add(_rightPanelInPlaceSensor);
    _leftOuterBarrier = LeftOuterBarrier.getInstance();
    _hardwareObjectList.add(_leftOuterBarrier);
    _rightOuterBarrier = RightOuterBarrier.getInstance();
    _hardwareObjectList.add(_rightOuterBarrier);
    _manufacturingEquipmentInterface = ManufacturingEquipmentInterface.getInstance();
    _hardwareObjectList.add(_manufacturingEquipmentInterface);

    //Variable Mag Anthony August 2011
    _leftSafetyLevelSensor = LeftSafetyLevelSensor.getInstance();
    _hardwareObjectList.add(_leftSafetyLevelSensor);
    _rightSafetyLevelSensor = RightSafetyLevelSensor.getInstance();
    _hardwareObjectList.add(_rightSafetyLevelSensor);

    retrievePersistentPanelInformation();
  }

  /**
   * @return the one and only instance of this class.
   * @author Greg Esparza
   */
  public static synchronized PanelHandler getInstance()
  {
    if (_instance == null)
    {
      _instance = new PanelHandler();
    }

    return _instance;
  }

  /**
   * Initialize the panel handler.  This will run a full initialization
   * of the motion controller, related hardware, and data to a known state.
   * @author Rex Shang
   */
  private void initialize() throws XrayTesterException
  {
    Assert.expect(_panelLoaded == false, "Panel is loaded, special initialization is required.");
    
    checkPanelIsLoading();

    // Don't need to do anything if the panel handler is already initialized.
    if (_initialized)
    {
      return;
    }

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.INITIALIZE);
    _hardwareObservable.setEnabled(false);
    int startupTimeInMilliseconds = _STARTUP_TIME_IN_MILLISECONDS + _motionControl.getStartupTimeInMilliseconds();
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.PANEL_HANDLER_INITIALIZE, startupTimeInMilliseconds);

    try
    {
      initializePanelFlowDirectionAndLoadingMode();

      // Put hardware to known good state.
      _stageBelts.off();
      closeAllBarriersInParallel();
      //closeAllOuterBarriersInParallel();
      _panelClamps.open();
      _leftPanelInPlaceSensor.retract();
      _rightPanelInPlaceSensor.retract();

      // Initialize MotionControl for stage rails.  Then, Home the rails.
      if (isAborting())
      {
        return;
      }
      _motionControl.startup();
      if (isAborting())
      {
        return;
      }
      _motionControl.enable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      if (isAborting())
      {
        return;
      }
      homeStageRails();

      // Put SMEMA into good state.
      _smemaLogUtil.logSmemaMessage("Initialize Panel Handler");
      _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
      _manufacturingEquipmentInterface.setPanelFromXrayTesterAvailable(false);
      _manufacturingEquipmentInterface.setPanelOutUntested();

      _initialized = true;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.PANEL_HANDLER_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.INITIALIZE);
  }

  /**
   * This method is invoked when system startup detects a panel is left in the
   * system.
   * The panel handler initilization is broken up into two stages because we
   * need to initialize PanelPosition so it can be used to unload.  But since
   * PanelPositioner will home stage as part of initialization, we need to
   * capture the panel before that.
   * During this stage, we need to re-capture the panel.  We will unload the
   * panel during next stage so the rails can home.
   * @author Rex Shang
   */
  void startupWithPanelInSystemStageOneOfTwo() throws XrayTesterException
  {
    Assert.expect(_panelLoaded, "Panel is not loaded, normal initialization is required.");
    clearAbort();
    
    checkPanelIsLoading();

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_ONE_OF_TWO);
    _hardwareObservable.setEnabled(false);

    try
    {
      initializePanelFlowDirectionAndLoadingMode();

      // Put hardware to known good state.
      _stageBelts.off();
      closeAllOuterBarriersInParallel();

      // Capture the panel first during this stage.
      if (isAborting())
      {
        return;
      }
      _twoStageInitializationInProgress = true;
      recapturePanelDuringSystemStartUp();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_ONE_OF_TWO);
  }

  /**
   * This method is invoked when system startup detects a panel is left in the
   * system.
   * During this stage, we will unload the panel and then home the rails.
   * @author Rex Shang
   */
  void startupWithPanelInSytemStageTwoOfTwo() throws XrayTesterException
  {
    Assert.expect(_twoStageInitializationInProgress, "Second stage initializeWithPanelInSytem invoked without first.");
    clearAbort();
    _twoStageInitializationInProgress = false;

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_TWO_OF_TWO);
    _hardwareObservable.setEnabled(false);
    int startupTimeInMilliseconds = _STARTUP_TIME_IN_MILLISECONDS + _motionControl.getStartupTimeInMilliseconds();
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.PANEL_HANDLER_INITIALIZE, startupTimeInMilliseconds);

    try
    {
      // Initialize MotionControl for stage rails.
      if (isAborting())
      {
        return;
      }
      _motionControl.startup();
      _motionControl.enable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);

      // If system is being re-started, don't have to unload panel and home rail.
      // For system test, don't unload and home.
      if (_initialized == false && _overrideEjectPanelDuringStartup == false)
      {
        if (isAborting())
        {
          return;
        }
        unloadPanel();
        if (isAborting())
        {
          return;
        }
        homeStageRails();
      }
      else
      {
        // Since we could lose our rail width position during a start up (digital
        // io reset), and we don't home the rails, we need to set the last known
        // width.
        _motionControl.setActualPositionInNanometers(
                MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS,
                _panelWidthInNanoMeters + getPanelWidthClearanceInNanoMeters());
      }

      _smemaLogUtil.logSmemaMessage("Initialize Panel Handler with panel in system");
      _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
      _manufacturingEquipmentInterface.setPanelFromXrayTesterAvailable(false);
      _manufacturingEquipmentInterface.setPanelOutUntested();

      _initialized = true;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.PANEL_HANDLER_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_TWO_OF_TWO);
  }

  private boolean checkPIPSensorTuningRequiredDuringSystemStartUp(PanelInPlaceSensorInt panelInPlaceSensor) throws XrayTesterException
  {
    Assert.expect(panelInPlaceSensor != null);
    if (_digitalIo.isOpticalStopControllerInstalled() && _digitalIo.isOpticalPIPResetInstalled())
    {
      return panelInPlaceSensor.isPanelInPlace();
    }
    else
    {
      return false;
    }
  }
  /**
   * Used by initialization to re-capture a panel that is left in system from
   * previous run.
   * @author Rex Shang
   */
  private void recapturePanelDuringSystemStartUp() throws XrayTesterException
  {
    boolean recaptureSucceeded = false;
    XrayTesterException exceptionDuringRecapture = null;

    try
    {
      //XXL Optical Stop Sensor - Anthony (  May 2013)

      // Do not check panel location based on optical stop sensor sensor.
      // It can be triggered by any part of the stage
      if (DigitalIo.isOpticalStopControllerInstalled())
      {
        if (XrayTester.isS2EXEnabled())
        {
          //Swee Yee Wong
          if(_leftPanelInPlaceSensor.isPanelInPlace() && _rightPanelInPlaceSensor.isPanelInPlace())
          {
            //recapture for long panel
            recaptureSucceeded = recapturePanelWithInchMove();
          }
          // Check to see if panel is resting against a panel in place sensor.
          else if (_leftPanelInPlaceSensor.isPanelInPlace())
          {
            _panelIsAgainst = _leftPanelInPlaceSensor;
            recaptureSucceeded = recapturePanelWithInchMove();
          }
          else if (_rightPanelInPlaceSensor.isPanelInPlace())
          {
            _panelIsAgainst = _rightPanelInPlaceSensor;
            recaptureSucceeded = recapturePanelWithInchMove();
          }
          else
          {
            // DigitalIo have been resetted, all hardware returned to its
            // de-energized state.
            // For short panel, it is more reliable to move to the other
            // panel in place sensor.
            if (isPanelLongerThanPIPClearance() == false)
            {
              if (_panelIsAgainst == _leftPanelInPlaceSensor)
              {
                movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor, false);
                //Swee-Yee.Wong - check PIP Sensor
                if (checkPIPSensorTuningRequiredDuringSystemStartUp(_leftPanelInPlaceSensor))
                {
                  recaptureSucceeded = false;
                  //throw exception with warning message
                  throw PanelHandlerException.getOpticalPanelInPlaceTuningRequiredException();
                }
                else
                {
                  recaptureSucceeded = true;
                }
              }
              else if (_panelIsAgainst == _rightPanelInPlaceSensor)
              {
                movePanelToPanelInPlaceSensor(_leftPanelInPlaceSensor, false);
                //Swee-Yee.Wong - check PIP Sensor
                if (checkPIPSensorTuningRequiredDuringSystemStartUp(_rightPanelInPlaceSensor))
                {
                  recaptureSucceeded = false;
                  //throw exception with warning message
                  throw PanelHandlerException.getOpticalPanelInPlaceTuningRequiredException();
                }
                else
                {
                  recaptureSucceeded = true;
                }
              }
              else
              {
                recaptureSucceeded = false;
                // Don't know what to do to recapture.  Will throw exception.
              }
            }
            else
            {
              // Long panel.  Do inch move to recapture.
              recaptureSucceeded = recapturePanelWithInchMove();
            }
          }

        }
        else // if (XrayTester.isXXLBased())
        {
          // DigitalIo have been resetted, all hardware returned to its
          // de-energized state.
          // For short panel, it is more reliable to move to the other
          // panel in place sensor.
          if (isPanelLongerThanPIPClearance() == false)
          {
            if (_panelIsAgainst == _leftPanelInPlaceSensor)
            {
              movePanelToRightSide();
              recaptureSucceeded = true;
            }
            else if (_panelIsAgainst == _rightPanelInPlaceSensor)
            {
              movePanelToLeftSide();
              recaptureSucceeded = true;
            }
            else
            {
              // Don't know what to do to recapture.  Will throw exception.
            }
          }
          else
          {
            // Long panel.  Do inch move to recapture.
            recaptureSucceeded = recapturePanelWithInchMove();
          }
        }
      }
      else
      {

        // Check to see if panel is resting against a panel in place sensor.
        if (_leftPanelInPlaceSensor.isPanelInPlace())
        {
          _panelIsAgainst = _leftPanelInPlaceSensor;
          recaptureSucceeded = true;
        }
        else if (_rightPanelInPlaceSensor.isPanelInPlace())
        {
          _panelIsAgainst = _rightPanelInPlaceSensor;
          recaptureSucceeded = true;
        }
        else
        {
          // DigitalIo have been resetted, all hardware returned to its
          // de-energized state.
          // For short panel, it is more reliable to move to the other
          // panel in place sensor.
          if (isPanelLongerThanPIPClearance() == false)
          {
            if (_panelIsAgainst == _leftPanelInPlaceSensor)
            {
              movePanelToRightSide();
              recaptureSucceeded = true;
            }
            else if (_panelIsAgainst == _rightPanelInPlaceSensor)
            {
              movePanelToLeftSide();
              recaptureSucceeded = true;
            }
            else
            {
              // Don't know what to do to recapture.  Will throw exception.
            }
          }
          else
          {
            // Long panel.  Do inch move to recapture.
            recaptureSucceeded = recapturePanelWithInchMove();
          }
        }
      }

      // Panel recaptured, update information.
      if (recaptureSucceeded)
      {
        savePersistentPanelInformation();
      }
    }
    catch (XrayTesterException xte)
    {
      exceptionDuringRecapture = xte;
      recoverFromHardwareException();
    }
    finally
    {
      if (recaptureSucceeded == false)
      {
        _initialized = false;
        // Could not re-capture panel, ask user to clear it out.
        // Open up clamps and barriers to help user in retrieving the panel.
        prepareSystemForManualPanelRetrieval();

        // We will clean up our states by assuming user will do what's asked.
        manualPanelRetrivalSucceeded();

        PanelHandlerException panelHandlerException;
        if (exceptionDuringRecapture != null)
        {
          panelHandlerException = PanelHandlerException.getFailedToRecapturePanelException(exceptionDuringRecapture);
        }
        else
        {
          panelHandlerException = PanelHandlerException.getFailedToRecapturePanelException();
        }
        _hardwareTaskEngine.throwHardwareException(panelHandlerException);
      }
      else
      {
        _panelClamps.close();
      }
    }
  }

  /**
   * This method is used to recapture a panel after DigitalIo has been resetted.
   * @author Rex Shang
   */
  private boolean recapturePanelWithInchMove() throws XrayTesterException
  {
    boolean recaptureSucceeded = false;

    _panelClamps.open();
    // Just to be on the safe side, do these.
    _leftPanelInPlaceSensor.retract();
    _rightPanelInPlaceSensor.retract();

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled() == false || XrayTester.isS2EXEnabled() )
    {
      //For Standard or S2EX only
      if (_panelIsAgainst == _leftPanelInPlaceSensor)
      {
        _stageBelts.setDirectionLeftToRight();
        _stageBelts.on();
        waitForHardware(_TWO_INCH_MOVE_TIME_IN_MILLI_SECONDS);
        _stageBelts.off();
        
        //Swee-Yee.Wong - check PIP Sensor
        if(checkPIPSensorTuningRequiredDuringSystemStartUp(_leftPanelInPlaceSensor))
        {
          recaptureSucceeded = false;
          //throw exception with warning message
          throw PanelHandlerException.getOpticalPanelInPlaceTuningRequiredException();
        }
        if (XrayTester.isS2EXEnabled())
        {
          movePanelToPanelInPlaceSensor(_leftPanelInPlaceSensor, false);
        }
        else
        {
          movePanelToPanelInPlaceSensor(_leftPanelInPlaceSensor, true);
        }

        recaptureSucceeded = true;
      }
      else if (_panelIsAgainst == _rightPanelInPlaceSensor)
      {
        _stageBelts.setDirectionRightToLeft();
        _stageBelts.on();
        waitForHardware(_TWO_INCH_MOVE_TIME_IN_MILLI_SECONDS);
        _stageBelts.off();
        
        //Swee-Yee.Wong - check PIP Sensor
        if(checkPIPSensorTuningRequiredDuringSystemStartUp(_rightPanelInPlaceSensor))
        {
          recaptureSucceeded = false;
          //throw exception with warning message
          throw PanelHandlerException.getOpticalPanelInPlaceTuningRequiredException();
        }
        if (XrayTester.isS2EXEnabled())
        {
          movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor, false);
        }
        else
        {
          movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor, true);
        }

        recaptureSucceeded = true;
      }
    }

    return recaptureSucceeded;
  }

  /**
   * Wait for hardware for the spcified time.
   * @author Rex Shang
   */
  void waitForHardware(long timeToWaitInMilliSeconds)
  {
    Assert.expect(timeToWaitInMilliSeconds >= 0, "Time out " + timeToWaitInMilliSeconds + " is not greater than or equal to 0.");

    long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
    long timeToWait = timeToWaitInMilliSeconds;

    while (timeToWait > 0)
    {
      try
      {
        Thread.sleep(timeToWait);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
      finally
      {
        // Calculate how much more time we need to wait.
        timeToWait = deadLineInMilliSeconds - System.currentTimeMillis();
      }
    }
  }

  /**
   * Tell this hardware to perform any actions required to startup on behalf
   * of the XrayTester object.
   * @throws XrayTesterException
   * @author Roy Williams
   */
  public void startup() throws XrayTesterException
  {
    clearAbort();

    _initialized = false;

    // XCR1144, Chnee Khang Wah, 21-Dec-2010
    if (_panelPositioner.isSafeToMove() == false)
    {
      throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE); //?????
    }
    // XCR1144 - end

    initialize();

    // XCR1144, Chnee Khang Wah, 11-Jan-2011, turn on the checking after complete the startup if it was turned off previously
    _digitalIo.setByPassPanelClearSensor(false);
  }

  /**
   * Tell this hardware to perform any actions required to shutdown on behalf
   * of the XrayTester object.
   * @author Rex Shang
   */
  public void shutdown() throws XrayTesterException
  {
    if (_motionControl.isStartupRequired() == false)
    {
      _motionControl.stop(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      _motionControl.disable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      _motionControl.shutdown();
    }
    // CR31949-Board falling inside system during startup by Anthony Fong
    _initialized = false;
  }

  /**
   * Check to see if we need to run startup() before we can do anything.
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    if (_initialized == false || _motionControl.isStartupRequired() || _motionControl.isHomingRequired(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Sets abort flag to indicate an abort event is underway.
   * @throws XrayTesterException overriding class might throw exception due
   * to interaction with hardware.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    super.abort();
    
    //For Speed Up
    _isProduction=false;
    if (_repeatUnloadLoadModeOn == false)
    {
      for (HardwareObject hardwareObject : _hardwareObjectList)
      {
        hardwareObject.abort();
      }

      if (_motionControl.isStartupRequired() == false)
      {
        _motionControl.stop(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      }
    }
  }

  /**
   * Load the panel into the machine and wait until the sequence completes.
   * NOTE: The panel width and length used for loading are after rotations.
   *
   * @author Rex Shang
   */
  public void loadPanel(String projectName,
          int panelWidthAfterRotationsInNanoMeters,
          int panelLengthAfterRotationsInNanoMeters, 
          int panelExtraClearDelayInMiliSeconds) throws XrayTesterException
  {
    // needToEnableSafetyLevelSensorForPanelLoading Default should be set as true
    if (XrayActuator.isInstalled())
    {
      loadPanel(projectName, panelWidthAfterRotationsInNanoMeters, panelLengthAfterRotationsInNanoMeters, true, panelExtraClearDelayInMiliSeconds);
    }
    else
    {
      loadPanel(projectName, panelWidthAfterRotationsInNanoMeters, panelLengthAfterRotationsInNanoMeters, false, panelExtraClearDelayInMiliSeconds);
    }
  }

  /**
   * Load the panel into the machine and wait until the sequence completes.
   * NOTE: The panel width and length used for loading are after rotations.
   * @author Rex Shang
   * @author Wei Chin (added new needToEnableSafetyLevelSensorForPanelLoading to check the component height)
   */
  public void loadPanel(String projectName,
          int panelWidthAfterRotationsInNanoMeters,
          int panelLengthAfterRotationsInNanoMeters,
          boolean needToEnableSafetyLevelSensorForPanelLoading,
          int panelExtraClearDelayInMiliSeconds) throws XrayTesterException
  {
    Assert.expect(_panelLoaded == false, "Panel is already loaded.");
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(panelWidthAfterRotationsInNanoMeters >= getMinimumPanelWidthInNanoMeters(),
            "Panel is narrower than minimum width.");
    Assert.expect(panelWidthAfterRotationsInNanoMeters <= getMaximumPanelWidthInNanoMeters(),
            "Panel exceeds maximum width.");
    Assert.expect(panelLengthAfterRotationsInNanoMeters >= getMinimumPanelLengthInNanoMeters(),
            "Panel is shorter than minimum length");
    Assert.expect(panelLengthAfterRotationsInNanoMeters <= getMaximumPanelLengthInNanoMeters(),
            "Panel exceeds maximum length.");

    checkPanelIsLoading();
    
    //Xray tube crash preventive action
    _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _stageBelts.off();
      _digitalIo.setEnableXXLOpticalStopController(false);
    }

    boolean is_XXL_Or_S2EX_System = XrayTester.isXXLBased() || XrayTester.isS2EXEnabled();
    if ((panelWidthAfterRotationsInNanoMeters + getPanelWidthClearanceInNanoMeters()) <= getMinimumRailWidthInNanoMeters()
            || (panelWidthAfterRotationsInNanoMeters + getPanelWidthClearanceInNanoMeters()) >= getMaximumRailWidthInNanoMeters())
    {
      Object args[] = new Object[]
      {
        Float.toString(MathUtil.convertUnits(getMinimumRailWidthInNanoMeters(), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS)),
        Double.toString(getMinimumRailWidthInNanoMeters() / 1000000.0),
        Float.toString(MathUtil.convertUnits(getMaximumRailWidthInNanoMeters(), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS)),
        Double.toString(getMaximumRailWidthInNanoMeters() / 1000000.0),
        Float.toString(MathUtil.convertUnits(panelWidthAfterRotationsInNanoMeters, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS)),
        Double.toString(panelWidthAfterRotationsInNanoMeters / 1000000.0),
        Float.toString(MathUtil.convertUnits(getPanelWidthClearanceInNanoMeters(), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS)),
        Double.toString(getPanelWidthClearanceInNanoMeters() / 1000000.0),
        Float.toString(MathUtil.convertUnits(panelWidthAfterRotationsInNanoMeters + getPanelWidthClearanceInNanoMeters(), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS)),
        Double.toString((panelWidthAfterRotationsInNanoMeters + getPanelWidthClearanceInNanoMeters()) / 1000000.0),
      };
      throw PanelHandlerException.getPanelWidthPlusPanelWidthClearanceExceedMaxRailWidthExceptionKey(args);
    }

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_MOVE_STAGE_FOR_LOAD_PANEL);

    if (_repeatUnloadLoadModeOn == false)
    {
      clearAbort();
    }

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.LOAD_PANEL);
    _hardwareObservable.setEnabled(false);

    try
    {
      _projectName = projectName;
      _panelWidthInNanoMeters = panelWidthAfterRotationsInNanoMeters;
      _panelLengthInNanoMeters = panelLengthAfterRotationsInNanoMeters;
      _panelExtraClearDelayInMiliSeconds = panelExtraClearDelayInMiliSeconds;

      if (_repeatUnloadLoadModeOn == false && isAborting())
      {
        return;
      }

//      // Closing both outer barriers and inner barrier just to be sure.
//      closeAllBarriersInParallel();

      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      final TimerUtil panelLoadingTimerUtil = new TimerUtil(); 
      panelLoadingTimerUtil.start();
      
      //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
      {
        try
        {
          if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
              VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
          {
            VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_LOADING_START);
          }
        }
        catch (DatastoreException ex)
        {
          System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
        }
      }

      // Move stage
      // Size rail
      // Open panel clamps
      if (_keepOuterBarrierOpenForLoadingPanel == false || _primaryOuterBarrier.isOpen() == false)
      {
        moveStageAndSetRailWidthInParallelForLoadingPanel(is_XXL_Or_S2EX_System);
        _keepOuterBarrierOpenForLoadingPanel = false;
      }
      else
        _keepOuterBarrierOpenForLoadingPanel = false;

      if (_repeatUnloadLoadModeOn == false && isAborting())
      {
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        return;
      }

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_MOVE_STAGE_FOR_LOAD_PANEL);

      // reset performance log timer to 0.0
      _performanceLog.resetTimer();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.CYCLE_TIME_SEPARATOR);
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_LOAD_PANEL);

      if (isInLineModeEnabled())
      {
        // SMEMA sequence:
        // 1 or 2.  Upstream system signal panel available.
        // 2 or 1.  Downstream system signal ready to accept.
        // 3.  Upstream system signal panel not available.
        // 4.  Downstream system signal not ready to accept.
        
        //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
        panelLoadingTimerUtil.stop();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        
        // XCR-3116 Intermittent board chop during production looping using SMEMA
        _smemaLogUtil.logSmemaMessage("Start load panel " + projectName);
        
        _manufacturingEquipmentInterface.waitUntilPanelFromUpstreamSystemIsAvailable(
                _WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM_TIME_OUT_IN_MILLI_SECONDS, _WAIT_ABORTABLE);
        
        panelLoadingTimerUtil.start();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_LOADING_START);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
      }

      if (_repeatUnloadLoadModeOn == false && isAborting())
      {
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        return;
      }

      // If the panel z height safety check is enabled, we need to wait until panel z height sensor is in the Ok state,
      // before opening the outer barrier.  If we open it too early, the user might insert the panel while the sensor is
      // in the not Ok state, triggering an exception.
      // Rick Gaudette 2012-09-27
      if (needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false)
      {
        // TODO: have to throw error is wait too long, it might be cause by safety sensor not align properly
        long deadLineInMilliSeconds = 10000 + System.currentTimeMillis();
        while ((_primarySafetyLevelSensor.isOK() == false) && 
               (isAborting() == false) && 
               (deadLineInMilliSeconds > System.currentTimeMillis()))
        {
          try
          {
            Thread.sleep(50);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }
        }        
        if(deadLineInMilliSeconds <= System.currentTimeMillis())
        {                         
            _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelMisalignOrMalfunctionException());
        }
      }
      if (_repeatUnloadLoadModeOn == false && isAborting())
      {
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        return;
      }
          
      //XXL Optical Stop Sensor - Anthony (May 2013)
      resetXXLOpticalStopController();

      // Open the outer barrier.
      openOuterBarrierAsNecessary(_primaryOuterBarrier);

      if (isInLineModeEnabled())
      {
        // Turn on the stage belt to receive panel from upstream.
        if (_primaryPanelInPlaceSensor.equals(_leftPanelInPlaceSensor))
        {
          _stageBelts.setDirectionRightToLeft();
        }
        else
        {
          _stageBelts.setDirectionLeftToRight();
        }


        // For in-line mode, since the belt is turned on early in the process to help
        // transition the panel into our system, we want to extend the PIP in case
        // the panel clear sensor failed to detect the panel.
        _primaryPanelInPlaceSensor.extend();
        _secondaryPanelInPlaceSensor.retract();

        _panelIsLoading = true;
        savePersistentPanelInformation();
        
        _stageBelts.on();

        _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(true);

        if (_repeatUnloadLoadModeOn == false && isAborting())
        {
          if (isInLineModeEnabled())
          {
            // We need to turn off the belt and set the right SMEMA signal in case of abort.
            _stageBelts.off();
            _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
          }

          // Close barrier in event of abort.
          if (_primaryPanelClearSensor.isBlocked() == false)
          {
            _primaryOuterBarrier.close();
          }

          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
          {
            try
            {
              if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                  VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
              {
                VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
              }
            }
            catch (DatastoreException ex)
            {
              System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
            }
          }
          
          return;
        }

        //Variable Mag Anthony August 2011
        // Check for Max Safety Level
//        while (_primaryPanelClearSensor.isBlocked() == true && isAborting() == false)
//        {
//          if (needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false)
//          {
//            //Variable Mag Anthony August 2011
//            if (_primarySafetyLevelSensor.isOK() == false)
//            {
//              //Should NOT happen,
//              //_primarySafetyLevelSensor should not trigger or Panel Component Height should not Exceed Max Safety Level
//              _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
//            }
//          }
//
//          try
//          {
//            Thread.sleep(50);
//          }
//          catch (InterruptedException ie)
//          {
//            // Do nothing.
//          }
//
//        }

        // Not only need to wait for panel clear, but we also need to makesure
        // PIP should not be detected before PCS trigeered
        while (_primaryPanelClearSensor.isBlocked() == false && isAborting() == false)
        {
          if (_primaryPanelInPlaceSensor.isPanelInPlace() == true)
          {
            //Should NOT happen,
            //primaryPanelClearSensor should detect panel before primaryPanelInPlaceSensor
            _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelInPlaceButPrimarySensorNotDetectPanelException());
          }
          if(needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false)
          {
              //Variable Mag Anthony August 2011
              if (_primarySafetyLevelSensor.isOK() == false)
              {
                //Should NOT happen,
                //_primarySafetyLevelSensor should not trigger or Panel Component Height should not Exceed Max Safety Level
                _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
              }
          }

          try
          {
            Thread.sleep(50);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }

        }
      }
      else
      {
        //XCR 3085 - System hung if abort during loading panel
        if (_repeatUnloadLoadModeOn == false && isAborting())
        {
          _primaryOuterBarrier.close();
          
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
          {
            try
            {
              if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                  VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
              {
                VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
              }
            }
            catch (DatastoreException ex)
            {
              System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
            }
          }
          
          return;
        }
        
        //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
        panelLoadingTimerUtil.stop();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        
        // Wait until the panel clear sensor is blocked.
        _primaryPanelClearSensor.waitUntilBlocked(
                _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK_FOR_LOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_ABORTABLE);
        
        panelLoadingTimerUtil.start();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_LOADING_START);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }

        // Turn on the stage belt.
        if (_primaryPanelInPlaceSensor.equals(_leftPanelInPlaceSensor))
        {
          _stageBelts.setDirectionRightToLeft();
        }
        else
        {
          _stageBelts.setDirectionLeftToRight();
        }

        if (isAborting() == false)
        {
          _panelIsLoading = true;
          savePersistentPanelInformation();
          
          _stageBelts.on();
        }

        //Variable Mag Anthony August 2011
        // Check for Max Safety Level
        while (_primaryPanelClearSensor.isBlocked() == true && isAborting() == false)
        {
          if (needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false)
          {
            //Variable Mag Anthony August 2011
            if (_primarySafetyLevelSensor.isOK() == false)
            {
              _stageBelts.off();
              //Should NOT happen,
              //_primarySafetyLevelSensor should not trigger or Panel Component Height should not Exceed Max Safety Level
              _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
            }
          }

          try
          {
            Thread.sleep(50);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }

        }

      }

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.CONTINUE_LOAD_PANEL);

      // This is the last place where we need to handle abort for panel loading.
      if (_repeatUnloadLoadModeOn == false && isAborting())
      {
        if (isInLineModeEnabled())
        {
          // We need to turn off the belt and set the right SMEMA signal in case of abort.
          _stageBelts.off();

          // If user abort, we need to retract any PIP extended.
          _primaryPanelInPlaceSensor.retract();

          _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
        }
        else
        {
          // We need to turn off the belt.
          _stageBelts.off();
        }

        // Close barrier in event of abort.
        if (_primaryPanelClearSensor.isBlocked() == false)
        {
          _primaryOuterBarrier.close();
        }

        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.UPSTREAM_IDLING);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        
        return;
      }

      //Xray tube crash preventive action
      _checkPanelClearedAfterPanelUnloadedBeforeCDA = false;
      // Make information persistent in case of system power failure.
      _panelLoaded = true;
      _panelIsLoading = false;
      debug("Calling loadPanel(), setting _panelLoaded = true");
      savePersistentPanelInformation();

      _primaryPanelInPlaceSensor.setNeedToEnableSafetyLevelSensorForPanelLoading(needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false);

      if (isInLineModeEnabled() && isIgnorePIPModeEnabled() /*Chnee Khang Wah (12-Oct-2009)*/)
      {
        // Move panel to panel in place sensor.  When panel arrives at the sensor,
        // clamp down.
        // After the panel passes the panel clear sensor, close the outer barrier.
        ExecuteParallelThreadTasks<XrayTesterException> panelLoadThread = new ExecuteParallelThreadTasks<XrayTesterException>();
        ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("MovePanelToPanelInPlaceSensor")
        {

          protected XrayTesterException executeTask() throws XrayTesterException
          {
            XrayTesterException exception = null;
            try
            {
              movePanelToPanelInPlaceSensor(_primaryPanelInPlaceSensor,false);
            }
            catch (XrayTesterException xte)
            {
              //Xray tube crash preventive action
              _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;
              _panelLoaded = false;
              System.out.println("ERROR caught: movePanelToPanelInPlaceSensor() in loadPanel() function - InLineMode Enabled, setting _panelLoaded = false");
              savePersistentPanelInformation();
              exception = xte;
              throw xte;
            }

            return exception;
          }
        };

        ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("CloseBarrierAfterPanelPassesPanelClearSensor")
        {

          protected XrayTesterException executeTask() throws XrayTesterException
          {
            XrayTesterException exception = null;
            try
            {
              _primaryPanelClearSensor.waitUntilClear(_WAIT_FOR_PANEL_TO_ENGAGE_PANEL_IN_PLACE_SENSOR_TIME_OUT_IN_MILLI_SECONDS, false);
              
              // Chnee Khang Wah, 2014-07-07
              // Extra delay for carrier with long hole at side
              if(_panelExtraClearDelayInMiliSeconds>0)
              {
                TimerUtil timer = new TimerUtil();
                timer.restart();
                while((timer.getElapsedTimeInMillis()) < _panelExtraClearDelayInMiliSeconds)//_EXTRA_DELAY_TIME_TO_CONFIRM_PANEL_IS_CLEAR)
                {
                  if(_primaryPanelClearSensor.isBlocked() == true)
                  {
                    _primaryPanelClearSensor.waitUntilClear(
                          _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
                    timer.restart();
                    System.out.println("PANEL_CLEAR_SENSOR::Panel Clear Sensor have been ignore and re-wait.");
                  }
                }
                timer.stop();
                timer.reset();
              }
              ////// - Extra Delay end
                  
              closeAllOuterBarriersInParallel();
            }
            catch (XrayTesterException xte)
            {
              exception = xte;
              throw xte;
            }

            return exception;
          }
        };

        panelLoadThread.submitThreadTask(task1);
        panelLoadThread.submitThreadTask(task2);
        try
        {
          panelLoadThread.waitForTasksToComplete();
        }
        catch (XrayTesterException xex)
        {

          throw xex;

        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
        
        if (_primaryPanelClearSensor.equals(_leftPanelInPlaceSensor))
        {
          _forceLoadPanelTORight = true;
          movePanelToRightSide();
        }
      }
      else
      {
        // if in manual mode, for safety purpose, makesure the panel reach PIP
        // before close primary outer barrier
        try
        {
          movePanelToPanelInPlaceSensor(_primaryPanelInPlaceSensor,false);
        }
        catch (XrayTesterException xte)
        {
          //Xray tube crash preventive action
          _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;
          _panelLoaded = false;

          System.out.println("ERROR caught: movePanelToPanelInPlaceSensor() in loadPanel() function - InLineMode Disabled, setting _panelLoaded = false");
          savePersistentPanelInformation();
          throw xte;
        }
        // Close outer barrier
        // Clamp down panel
        closeAllOuterBarriersInParallel();
        if (_primaryPanelInPlaceSensor.equals(_leftPanelInPlaceSensor))
        {
          _forceLoadPanelTORight = true;
          movePanelToRightSide();
        }
      }
	  
      if (isInLineModeEnabled())
      // We can no longer accept panel.
      {
        _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
      }

      // Close outer barrier
      // Clamp down panel
      if( XrayTester.isS2EXEnabled())
      { 
        if(XrayMotorizedHeightLevelSensor.isInstalled())
        {
          if (Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION).equalsIgnoreCase("M23"))
          {
            if(needToEnableSafetyLevelSensorForPanelLoading)
            {
              //For M23, only need to check height safety for high mag
               closeAllOuterBarriersWithMotorizedHeightLevelSensorAdjustmentInParallel(needToEnableSafetyLevelSensorForPanelLoading);
            }
            else
            {
              //For M23, skip height safety check for low mag
              closeAllOuterBarriersInParallel();
            }
          }
          else
          {
            closeAllOuterBarriersWithMotorizedHeightLevelSensorAdjustmentInParallel(needToEnableSafetyLevelSensorForPanelLoading);
          }
        }
        else
        {
          closeAllOuterBarriersInParallel();
        }
      }
      else
      {
        closeAllOuterBarriersInParallel();
      }
      
      // Always Check Height Level Safety Sensor for S2EX machine
      if( XrayTester.isS2EXEnabled())
      {
        if(XrayMotorizedHeightLevelSensor.isInstalled())
        {
          if (Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION).equalsIgnoreCase("M23"))
          {
            if(needToEnableSafetyLevelSensorForPanelLoading)
            {
              //For M23, check height safety level for high-mag only
              //needToEnableSafetyLevelSensorForPanelLoading = true for high-mag
              safetyLevelSensorForS2EXSystem(needToEnableSafetyLevelSensorForPanelLoading);
            }
            else
            {
              //For M23, skip the low mag safety checking eventhough the xray tube is outside the cover 
              //because Motorized Height Level Sensor cannot reach the required height to check component height that over limit
              //Mechanical bar is installed to limit the components height in this case.
              _passedS2EXLowMagSafetyLevelSensorScan = true; 
            }
          }
          else
          {
            //Check for both high-mag and low-mag
            //needToEnableSafetyLevelSensorForPanelLoading = true for high-mag
            //needToEnableSafetyLevelSensorForPanelLoading = false for low-mag
            safetyLevelSensorForS2EXSystem(needToEnableSafetyLevelSensorForPanelLoading);
          }
        }
        else
        {
          //Check for high-mag only using XXL cylinder
          //needToEnableSafetyLevelSensorForPanelLoading = true for high-mag
          //needToEnableSafetyLevelSensorForPanelLoading = false for skip
          if (XrayCylinder.isInstalled() && needToEnableSafetyLevelSensorForPanelLoading)
          {
            safetyLevelSensorForXXLSystem();
          }
        }
      }
      // Check Safety Sensor for XXL machine 
      //(do not used is_XXL_Or_S2EX_System which is including isS2EXEnabled)
      if (XrayTester.isXXLBased() && needToEnableSafetyLevelSensorForPanelLoading)
      {
        safetyLevelSensorForXXLSystem();
      }
      
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_LOAD_PANEL);
      
      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      panelLoadingTimerUtil.stop();
      long panelLoadingTime = panelLoadingTimerUtil.getElapsedTimeInMillis();

      //Khaw Chek Hau - XCR3774: Display load & unload time during production
      setProductionPanelLoadingTimeInMilis(panelLoadingTime);

      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation     
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
      {
        try
        {
          if(MachineUtilizationReportSystem.getInstance().isProgramming() ==  false && MachineUtilizationReportSystem.getInstance().isServicing()==  false &&
              MachineUtilizationReportSystem.getInstance().isSetupRunning()==  false)
          {
            MachineUtilizationReportSystem.getInstance().initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.LOAD_PANEL, panelLoadingTime);
          }
        }
        catch (DatastoreException ex)
        {
          System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
        }
      }
      
      //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
      {
        try
        {
          if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
              VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
          {
            VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_LOADING_END);
          }
        }
        catch (DatastoreException ex)
        {
          System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
        }
      }
      
      // XCR-3116 Intermittent board chop during production looping using SMEMA
      if (isInLineModeEnabled())
        _smemaLogUtil.logSmemaMessage("Complete load panel " + projectName);

      // XCR-3630, No variable mag checking after change low mag to high mag setting and board is loaded
      // Skip Safety Sensor for standard machine 
      if (needToEnableSafetyLevelSensorForPanelLoading && is_XXL_Or_S2EX_System == false)
      {
        _passedHighMagSafetyLevelSensorScan = true;
        _passedS2EXHighMagSafetyLevelSensorScan = true;
        _passedS2EXLowMagSafetyLevelSensorScan = true;
      }
    }
    catch (XrayTesterException xte)
    {
      //Xray tube crash preventive action
      _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;

      recoverFromHardwareException();
      // Throw the original exception.
      throw xte;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _primaryPanelInPlaceSensor.setNeedToEnableSafetyLevelSensorForPanelLoading(false);
      _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.LOAD_PANEL);
    }
  }

  /**
   * User by service to check the reliability of panel handling.
   * This requires a panel pre-loaded by loadPanel().
   * @param repeatCount int 0 means loop forever.
   * @author Rex Shang
   */
  public void repeatUnloadLoadForServiceTest(int repeatCount) throws XrayTesterException
  {
    Assert.expect(repeatCount >= 0);
    Assert.expect(_panelLoaded == true);

    clearAbort();
    _repeatUnloadLoadModeOn = true;
    PanelHandlerPanelFlowDirectionEnum originalFlowDirection = _flowDirection;
    String projectName = _projectName;
    int panelWidth = _panelWidthInNanoMeters;
    int panelLength = _panelLengthInNanoMeters;
    int panelExtraClearDelayInMiliSeconds = _panelExtraClearDelayInMiliSeconds;

    // Need to make sure that if panel handler is in flow through mode, we can
    // change the flow direction to either direction on the fly.
    if (_loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      Assert.expect(isPanelFlowDirectionOk(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT));
      Assert.expect(isPanelFlowDirectionOk(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT));
    }

    try
    {
      int loopCounter = 0;

      // Repeat the unload/load as long as no abort, loop for ever, or not
      // yet reached repeatCount.
      while ((isAborting() == false)
              && ((repeatCount == 0) || (loopCounter < repeatCount)))
      {
        unloadPanel();

        if (_panelLoaded)
        {
          // If the panel handler detects that the panel is still loaded after
          // the unload(), the only way that this can occur is that abort() was
          // commanded at the begginning of the unload().
          break;
        }

        // Need to switch the direction of the flow for flow through mode since
        // the unloading position is the same as the next loading position.
        if (_loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
        {
          if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT))
          {
            _flowDirection = PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
          }
          else if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT))
          {
            _flowDirection = PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT;
          }
          else
          {
            Assert.expect(false, "Invalide panel flow direction.");
          }

          setupForPanelFlowDirectionAndLoadingMode();
        }

        // comment by Wei Chin
        // should not check the safety sensor (due to it don't lower down the xray tube)
        // Please be aware if it need to run this board in high mag! 
        loadPanel(projectName, panelWidth, panelLength, false, panelExtraClearDelayInMiliSeconds);

        loopCounter++;
      }
    }
    finally
    {
      if (_loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
      {
        _flowDirection = originalFlowDirection;
        setupForPanelFlowDirectionAndLoadingMode();
      }

      _repeatUnloadLoadModeOn = false;
    }
  }

  /**
   * @return String name of the project our panel belong to.
   * @author Rex Shang
   */
  public String getLoadedPanelProjectName()
  {
    return _projectName;
  }

  /**
   * @author Rex Shang
   */
  public int getLoadedPanelLengthInNanoMeters()
  {
    return _panelLengthInNanoMeters;
  }

  /**
   * @author Rex Shang
   */
  public int getLoadedPanelWidthInNanoMeters()
  {
    return _panelWidthInNanoMeters;
  }

  /**
   * Too speed up unload in inline mode, we break up the unload to two stages:
   * prepareToUnload and unloadPanel.
   * This method will get invoked right after scanning but before the
   * analysis is done.  Once the analysis is done, we then finish unloading
   * panel while communicate via SMEMA about test status.
   * @author Rex Shang
   */
  public void prepareToUnloadPanel() throws XrayTesterException
  {
    Assert.expect(_panelLoaded, "Panel is not loaded.");
    clearAbort();

    if (isAborting())
    {
      return;
    }
    closeAllOuterBarriersInParallel();

    if (isAborting())
    {
      return;
    }
    moveStageToOuterBarrier(_secondaryOuterBarrier);
  }

  /**
   * Xray tube crash preventive action
   * Check Panel Cleared After Panel Unloaded Before CDA to make sure panel is not inside.
   * @author Anthony Fong
   */
  public boolean checkPanelClearedAfterPanelUnloadedBeforeCDA() throws XrayTesterException
  {

    boolean bStopCDAIfPanelDetected = true; //Flag to determine need to proceed the CDA operation or not!
    boolean bCheckResult = true; //the final safety checking result to proceed or stop operation.
    if ((_checkPanelClearedAfterPanelUnloadedBeforeCDA == false) || (isPanelLoaded()))
    {
      return bCheckResult;
    }

    
    int panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds = Config.getInstance().getIntValue(SoftwareConfigEnum.PANEL_CLEARED_SCAN_PERIOD_AFTER_PANEL_UNLOADED_BEFORE_CDA_IN_MILLISECONDS);
    if ((panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds < 3000) || (panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds > 200000))
    {
      //Write to log file
      debug("Setting panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds = " + panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds + " ms, should be within 3000-20000 ms in software.config!");

      Config.getInstance().setValue(SoftwareConfigEnum.PANEL_CLEARED_SCAN_PERIOD_AFTER_PANEL_UNLOADED_BEFORE_CDA_IN_MILLISECONDS, (int) _WAIT_FOR_PANEL_TO_REACH_PANEL_CLEAR_SENSOR_TIME_OUT_IN_MILLI_SECONDS);
      panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds = (int) _WAIT_FOR_PANEL_TO_REACH_PANEL_CLEAR_SENSOR_TIME_OUT_IN_MILLI_SECONDS;
    }

    boolean bHardwareSimulation = Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    if (bHardwareSimulation == true)
    {
      return bCheckResult;
    }
    
    boolean isOverridePanelEjectForTestExec = Config.getInstance().getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC);
    boolean isSimulatePanelEjectForTestExec = Config.getInstance().getBooleanValue(SoftwareConfigEnum.SIMULATE_PANEL_EJECT_FOR_TEST_EXEC);
    if ((isOverridePanelEjectForTestExec == true)||(isSimulatePanelEjectForTestExec == true))
    {
        return bCheckResult;
    }
    
    boolean bBypassPanelClearedCheckingAfterPanelUnloadedBeforeCDA = Config.getInstance().getBooleanValue(SoftwareConfigEnum.BYPASS_PANEL_CLEARED_CHECKING_AFTER_PANEL_UNLOADED_BEFORE_CDA);
    boolean bDeveloperDebugMode = Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
    if (bDeveloperDebugMode == true)
    {
      if (bBypassPanelClearedCheckingAfterPanelUnloadedBeforeCDA == true)
      {
        if(XrayTester.isS2EXEnabled()) //for speed up the process at risk
        {
          boolean isPanelCleared = false;
          //swee yee wong - to be safe, move this open clamp action to the PIP sensor checking place.
//          if(_digitalIo.arePanelClampsClosed()) //for S2EX, need to open Panel Clampper before checking the PIP sensor
//          {
//            _digitalIo.openPanelClamps();
//          }
          isPanelCleared = _primaryPanelInPlaceSensor.isPanelInPlace()==false && _secondaryPanelInPlaceSensor.isPanelInPlace()==false;          
          if (isPanelCleared)
          {     
            //for speed up the proces, move stage to loadding position first
            moveStageToOuterBarrier(_primaryOuterBarrier);
          }
          return isPanelCleared;
        }
        else
        {
          return bCheckResult;
        }
      }
    }

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _digitalIo.setEnableXXLOpticalStopController(false);
    }

    try
    {
      // Move stage to unload position
      // Closing both outer barriers and inner barrier just to be sure.
      //closeAllOuterBarriersInParallel();
      closeAllBarriersInParallel();

      setupForPanelFlowDirectionAndLoadingMode();
      if (XrayTester.isXXLBased())
      {
        _motionControl.setCheckForPointToPointMoveCompletion(true);
        moveStageToOuterBarrier(_secondaryOuterBarrier);
        _motionControl.setCheckForPointToPointMoveCompletion(false);
      }
      else
      {
        moveStageToOuterBarrier(_secondaryOuterBarrier);
      }
      if(XrayTester.isS2EXEnabled()) //for speed up the process at risk
      {
        //skip for speed up
      }
      else
      {
        Thread.sleep(500); //is need to ensure motion is stop for safe.
      }
      _panelClamps.open();
      // Just to be on the safe side, retract the other PIP just in case.
      _leftPanelInPlaceSensor.retract();
      _rightPanelInPlaceSensor.retract();

      // Set up stage belt direction.
      if (_secondaryPanelClearSensor.equals(_rightPanelClearSensor))
      {
        _stageBelts.setDirectionLeftToRight();
      }
      else
      {
        _stageBelts.setDirectionRightToLeft();
      }

      // Turn on the belts
      _stageBelts.on();

      // Check to make sure nothing is blocking panel clear sensor
      // Check to ensure nothing is leftover from the previous run.
      long timeToWaitInMilliSeconds = panelClearedScanPeriodAfterPanelUnloadBeforeCDAInMilliseconds;
      long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
      boolean continueCheck = true;
      boolean throwExceptionAndProceed = false;
      boolean panelDetected = false;
      while (continueCheck && (deadLineInMilliSeconds > System.currentTimeMillis()))
      {
        try
        {

          if (_secondaryPanelClearSensor.isBlocked())
          {

            continueCheck = false;
            _stageBelts.off();
            panelDetected = true;
            if (bStopCDAIfPanelDetected == false)
            {
              // Need to proceed with automatic panel clear checking and door close operation after panel unloaded
              throwExceptionAndProceed = true;

              _secondaryOuterBarrier.open(); // panel should be at the secondary Outer Barrier
              //_primaryOuterBarrier.open(); // may need to open primary Outer Barrier for double check
            }
            else
            {
              _secondaryOuterBarrier.open(); // panel should be at the secondary Outer Barrier
              _primaryOuterBarrier.open(); // open primary Outer Barrier for double check
              abort();
            }

            bCheckResult = false;

            //Panel clear sensor not suppose to be blocked!!!
            throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
          }
          Thread.sleep(0);
        }
        catch (InterruptedException ie)
        {
          // Do nothing.
        }
        finally
        {
          if (throwExceptionAndProceed == true)
          {
            HardwareWorkerThread.getInstance().invokeLater(new Runnable()
            {

              public void run()
              {
                try
                {
                  // Wait until the panel is removed from the system.
                  // NOTE: the wait is not abortable because the panel is already at the
                  // outer barrier and we don't want to loose track of the panel.
                  _secondaryPanelClearSensor.waitUntilClear(
                          _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);

                  // Chnee Khang Wah, 2014-07-07
                  // Extra delay for carrier with long hole at side
                  if(_panelExtraClearDelayInMiliSeconds>0)
                  {
                    TimerUtil timer = new TimerUtil();
                    timer.restart();
                    while((timer.getElapsedTimeInMillis()) < _panelExtraClearDelayInMiliSeconds)//_EXTRA_DELAY_TIME_TO_CONFIRM_PANEL_IS_CLEAR)
                    {
                      if(_secondaryPanelClearSensor.isBlocked() == true)
                      {
                        _secondaryPanelClearSensor.waitUntilClear(
                              _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
                        timer.restart();
                        System.out.println("PANEL_CLEAR_SENSOR::Panel Clear Sensor have been ignore and re-wait.");
                      }
                    }
                    timer.stop();
                    timer.reset();
                  }
                  ////// - Extra Delay end
          
                  // The panel clear sensor is situated inside the system.  When panel
                  // is exiting the system, we need to wait for it to clear the door way.
                  // For manual mode, we need to wait longer.
                  waitForHardware(_WAIT_FOR_PANEL_TO_CLEAR_BARRIER_IN_MANUAL_MODE_TIME_OUT_IN_MILLI_SECONDS);


                  _secondaryOuterBarrier.close();
                  //_primaryOuterBarrier.close();
                }
                catch (final XrayTesterException xte)
                {
                  // do nothing
                }
              }
            });
          }
        }
      }

      _stageBelts.off();

      // Check to make sure nothing is blocking panel clear sensor
      if (_panelPositioner.isSafeToMove() == false)
      {
        throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
      }

      if (panelDetected == false)
      {
        _checkPanelClearedAfterPanelUnloadedBeforeCDA = false;
      }

    }
    catch (XrayTesterException xte)
    {
      recoverFromHardwareException();
      // Throw the original exception.
      throw xte;
    }
    catch (InterruptedException ie)
    {
      // Do nothing.
    }
    finally
    {
    }

    return bCheckResult;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void unloadPanel(int panelExtraClearDelayInMiliSeconds) throws XrayTesterException
  {
    Assert.expect(panelExtraClearDelayInMiliSeconds >= 0);
    
    _panelExtraClearDelayInMiliSeconds = panelExtraClearDelayInMiliSeconds;
    unloadPanel();
  }
  
  /**
   * Unload the panel and wait for the sequence to complete.
   * @author Rex Shang
   * @edited By Kee Chin Seong
   */
  public void unloadPanel() throws XrayTesterException
  {
    boolean is_XXL_Or_S2EX_System = XrayTester.isXXLBased() || XrayTester.isS2EXEnabled();
    //Xray tube crash preventive action
    _checkPanelClearedAfterPanelUnloadedBeforeCDA = true;

    debug("unloadPanel - start unload");
    
    // Kok Chun, Tan - XCR3535 - log SMEMA conveyor signal
    if ((XrayTester.isS2EXEnabled() || XrayTester.isXXLBased()) && Config.isDeveloperDebugModeOn() && isInLineModeEnabled())
    {
      if (_logSMEMAConveyorSignal == false)
      {
        _logSMEMAConveyorSignal = true;
        _smemaSignalPollingThread.invokeLater(new Runnable()
        {
          public void run()
          {
            pollingSmemaSignal();
          }
        });
      }
    }
    
//    Assert.expect(_panelLoaded, "Panel is not loaded.");
    if (_panelLoaded == false)
    {
      _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelInPlaceNotEngaged());
    }

    if (_repeatUnloadLoadModeOn == false)
    {
      clearAbort();
    }

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.UNLOAD_PANEL);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Move stage to unload position
      // Closing both outer barriers and inner barrier just to be sure.
      // closeAllOuterBarriersInParallel();
      // closeAllBarriersInParallel();
 
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_UNLOAD_PANEL);

      if (isAborting())
      {
        return;
      }
      debug("unloadPanel - start moveStageToOuterBarrier");
	  
      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      TimerUtil downstreamIdleTimerUtil = new TimerUtil();
      TimerUtil panelUnloadingTimerUtil = new TimerUtil();
      panelUnloadingTimerUtil.start();
      
      //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
      {
        try
        {
          if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
              VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
          {
            VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_UNLOADING_START);
          }
        }
        catch (DatastoreException ex)
        {
          System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
        }
      }
	  
      if(isStageAtUnloadPosition())
      {
        debug("StageAtUnloadPosition - Skip moveStageToOuterBarrier(_secondaryOuterBarrier) ");
      }
      else
      {
        debug("StageAtUnloadPosition - Calling moveStageToOuterBarrier(_secondaryOuterBarrier)");
        if(_digitalIo.isLeftOuterBarrierNotClosed() ||_digitalIo.isRightOuterBarrierNotClosed())
        {
          debug("StageAtUnloadPosition - closeAllBarriersInParallel start");
          closeAllBarriersInParallel();
          debug("StageAtUnloadPosition - closeAllBarriersInParallel end");
        }
        moveStageToOuterBarrier(_secondaryOuterBarrier);
      }

      if (isAborting())
      {
        return;
      }
      debug("unloadPanel - check SMEMA");

      if (isInLineModeEnabled())
      {
        // XCR-3116 Intermittent board chop during production looping using SMEMA
        _smemaLogUtil.logSmemaMessage("Start unload panel");
        
        // SMEMA sequence:
        // 1.  Downstream system signal ready to accept.
        // 2.  Upstream system signal panel available.
        // 3.  Upstream system signal panel not available.
        // 4.  Downstream system signal not ready to accept.
        //Kee Chin Seong - Set the sequence
        if(getConfig().getBooleanValue(SoftwareConfigEnum.ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL) == false)
           _manufacturingEquipmentInterface.setPanelFromXrayTesterAvailable(true);
        
        panelUnloadingTimerUtil.stop();
        downstreamIdleTimerUtil.start();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.DOWNSTREAM_BLOCK_START);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        
        _manufacturingEquipmentInterface.waitUntilDownstreamSystemReadyToAcceptPanel(
                _WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL_TIME_OUT_IN_MILLI_SECONDS, _WAIT_ABORTABLE);
        
        downstreamIdleTimerUtil.stop();
        panelUnloadingTimerUtil.start();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_UNLOADING_START);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }

        if (isAborting())
        {
          HardwareTaskEngine.getInstance().throwHardwareException(new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL));
          return;
        }
        
        //SMEMA sequence mode
        if(getConfig().getBooleanValue(SoftwareConfigEnum.ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL) == true)
           _manufacturingEquipmentInterface.setPanelFromXrayTesterAvailable(true);

      }
      debug("unloadPanel - start openOuterBarrierAndReleasePanelForUnloadingPanel");

      // Open Barrier
      // Retract panel in place sensor
      // Open panel clamps
      openOuterBarrierAndReleasePanelForUnloadingPanel(is_XXL_Or_S2EX_System);

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.CONTINUE_UNLOAD_PANEL_1);
      debug("unloadPanel - done openOuterBarrierAndReleasePanelForUnloadingPanel");

      TimerUtil timer = new TimerUtil(); // XCR1161 - Hole on carrier causing outer barrier close without unload

      try
      {
        // Turn on the belts
        // Wait until the Panel Clear sensor to block so we "know" the panel is exiting.
        // In case of failure, the exception will ask user to manually retrieve
        // panel.
        boolean leaveStageBeltsOn = isInLineModeEnabled();
        movePanelToPanelClearSensor(_secondaryPanelClearSensor, leaveStageBeltsOn);
		
        timer.start(); // XCR1161 - Hole on carrier causing outer barrier close without unload
      }
      finally
      {
        // Now we "know" the panel is out of the system.
        _panelLoaded = false;
        debug("Calling unloadPanel(), setting _panelLoaded = false");

        // Clear saved panel information.
        _projectName = "";
        _panelWidthInNanoMeters = 0;
        _panelLengthInNanoMeters = 0;
        _panelIsAgainst = null;

        // Update persistent panel information.
        savePersistentPanelInformation();
      }

      // For manual mode, the door is still closed to avoid dropping panel.
      // Let's open it.
      if (isInLineModeEnabled() == false && PanelClearSensor.isRecessedMountInstalled())
      {
        _secondaryOuterBarrier.open();
      }
      
      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      if (isInLineModeEnabled() == false)
      {
        panelUnloadingTimerUtil.stop();
        downstreamIdleTimerUtil.start();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.DOWNSTREAM_BLOCK_START);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
      }

      // In service mode we will pull the panel back into the system without
      // any delay and will controll the doors separately - Reid Hayhow
      if (_repeatUnloadLoadModeOn == false)
      {
        // XCR1161 - Hole on carrier causing outer barrier close without unload
        // Chnee Khang Wah, 10-Feb-2010
        // After the outbarrier open, if there is hole on the carrier, it may clear the PCS and causing the outerbarrier close before the board have been taken out
        // So we what we do here is if the PCS is clear within 3s, we will assume there is a hole on the carrier
        // We will ask the PCS to clear to wait for another block from board.
        // System will resume it unload process once it detect another block.
        // Modified by Seng Yew on 9-Jan-2013 as suggested by Anthony, this special handling should only applicable to manual loading.
        // If not, board might unload too fast (<3 seconds) in SMEMA mode, and still waiting for block.
        // On top of this, we will wait for "downstream not ready to accept panel" signal before closing outer barrier, meaning board chopping will not occur.
        // Modified by Lee Herng on 26-Feb-2013: We do not want to wait indefinately because it may cause outer barrier not closing
        if (_secondaryPanelClearSensor.isBlocked() == false && isInLineModeEnabled() == false)
        {
          timer.stop();

          if (timer.getElapsedTimeInMillis() < _MINIMUM_INTERVAL_TIME_TO_DETECT_PANEL_IS_CLEAR)
          {
            System.out.println("Panel Handler detected a hole on the carrier during unload, PCS clear signal have been ignored once.");
            _secondaryPanelClearSensor.waitUntilBlocked(
                    _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_BLOCK_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
          }

          timer.reset();
        }
        // XCR1161 - end

        // Wait until the panel is removed from the system.
        // NOTE: the wait is not abortable because the panel is already at the
        // outer barrier and we don't want to loose track of the panel.
        _secondaryPanelClearSensor.waitUntilClear(
                _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
        
        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.CONTINUE_UNLOAD_PANEL_2);

        // When user retrives a panel, even when PCS is clear, the panel might
        // still be in the way of the barrier.
        if (isInLineModeEnabled())
        {
          // The panel clear sensor is situated inside the system.  When panel
          // is exiting the system, we need to wait for it to clear the door way.
          waitForHardware(_WAIT_FOR_PANEL_TO_CLEAR_BARRIER_IN_IN_LINE_MODE_TIME_OUT_IN_MILLI_SECONDS);

          // Signal panel is no longer available.
          _smemaLogUtil.logSmemaMessage("Panel Clear Sensor Status: isBlocked = " + _secondaryPanelClearSensor.isBlocked());
          _manufacturingEquipmentInterface.setPanelFromXrayTesterAvailable(false);

          try
          {
              // Check downstream and make sure it is no longer ready to accept.
              _manufacturingEquipmentInterface.waitUntilDownstreamSystemNotReadyToAcceptPanel(
                      _WAIT_FOR_DOWNSTREAM_SYSTEM_TO_FINISH_ACCEPT_PANEL_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
          }
          catch (XrayTesterException ex)
          {
            if (isCheckSmemaSensorSignalDuringUnloadingModeEnabled())
            {
              if (ex instanceof HardwareTimeoutException == false)
              {
                throw ex;
              }
            }
            else
            {
              throw ex;
            }
          }

          _stageBelts.off();
          
          // Chnee Khang Wah, 2015-06-19
          // Extra delay for carrier with long hole at side
          if(_panelExtraClearDelayInMiliSeconds>0)
          {
            waitForHardware(_panelExtraClearDelayInMiliSeconds);
          }
        }
        else
        {
          // Chnee Khang Wah, 2014-07-07
          // Extra delay for carrier with long hole at side
          if(_panelExtraClearDelayInMiliSeconds>0)
          {
            timer.restart();
            while(timer.getElapsedTimeInMillis() < _panelExtraClearDelayInMiliSeconds) //_EXTRA_DELAY_TIME_TO_CONFIRM_PANEL_IS_CLEAR)
            {
              if(_secondaryPanelClearSensor.isBlocked() == true)
              {
                _secondaryPanelClearSensor.waitUntilClear(
                      _WAIT_FOR_PANEL_CLEAR_SENSOR_TO_CLEAR_FOR_UNLOADING_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);
                timer.restart();
                System.out.println("PANEL_CLEAR_SENSOR::Panel Clear Sensor have been ignore and re-wait.");
              }
            }
            timer.stop();
            timer.reset();
            
            // Update persistent panel information.
            savePersistentPanelInformation();
          }
          ////// - Extra Delay end
          
          // The panel clear sensor is situated inside the system.  When panel
          // is exiting the system, we need to wait for it to clear the door way.
          // For manual mode, we need to wait longer.
          waitForHardware(_WAIT_FOR_PANEL_TO_CLEAR_BARRIER_IN_MANUAL_MODE_TIME_OUT_IN_MILLI_SECONDS);
        }
        timer.stop();
        timer.reset();
        timer.start();
        if (_keepOuterBarrierOpenAfterUnloadingPanel && isAborting() == false)
        {
          _keepOuterBarrierOpenForLoadingPanel = true;
        }
        else
        {
          _keepOuterBarrierOpenForLoadingPanel = false;
          closeAllOuterBarriersInParallel();
        }
        //???
        timer.stop();
        debug("closeAllOuterBarriersInParallel = "+timer.getElapsedTimeInMillis());
        
        timer.reset();
		
        //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
        if (isInLineModeEnabled())
          panelUnloadingTimerUtil.stop();
        else
          downstreamIdleTimerUtil.stop();
        
        //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT))
        {
          try
          {
            if(VOneMachineStatusMonitoringSystem.getInstance().isProgramming() ==  false && VOneMachineStatusMonitoringSystem.getInstance().isServicing()==  false &&
                VOneMachineStatusMonitoringSystem.getInstance().isSetupRunning()==  false)
            {
              VOneMachineStatusMonitoringSystem.getInstance().writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PANEL_UNLOADING_END);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during load panel event");
          }
        }
        
        long panelUnloadingTime = panelUnloadingTimerUtil.getElapsedTimeInMillis();
        long downstreamIdleTime = downstreamIdleTimerUtil.getElapsedTimeInMillis();

        //Khaw Chek Hau - XCR3774: Display load & unload time during production
        setProductionPanelUnloadingTimeInMilis(panelUnloadingTime);
        
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT))
        {
          try
          {
            if(MachineUtilizationReportSystem.getInstance().isProgramming() ==  false && MachineUtilizationReportSystem.getInstance().isServicing()==  false &&
                MachineUtilizationReportSystem.getInstance().isSetupRunning()==  false)
            {
              MachineUtilizationReportSystem.getInstance().initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.UNLOAD_PANEL, downstreamIdleTime, panelUnloadingTime);
            }
          }
          catch (DatastoreException ex)
          {
            System.out.println("ERROR catched: DatastoreException in writting MachineUtilizationReport file during unload panel event");
          }
        }
		
        timer.start();
        // XCR1144, Chnee Khang Wah, 23-Dec-2010
        // Check to make sure nothing is blocking panel clear sensor
        if (_panelPositioner.isSafeToMove() == false)
        {
          throw new PanelPositionerException(PanelPositionerExceptionEnum.PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD);
        }
        timer.stop();
        debug("_panelPositioner.isSafeToMove() = "+timer.getElapsedTimeInMillis());

        // XCR1144 - end
        timer.reset();
        timer.start();
        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_UNLOAD_PANEL);
        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.CYCLE_TIME_SEPARATOR);
        timer.stop();
        debug("_performanceLog.logMilestone = "+timer.getElapsedTimeInMillis());

        if (isInLineModeEnabled())
          _smemaLogUtil.logSmemaMessage("Complete unload panel");
      }

      // XCR1161 - Hole on carrier causing outer barrier close without unload
      timer.stop();
      timer.reset();
    }
    catch (XrayTesterException xte)
    {
      recoverFromHardwareException();
      // Throw the original exception.
      throw xte;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _passedHighMagSafetyLevelSensorScan = false;
      _passedS2EXHighMagSafetyLevelSensorScan = false;
      _keepOuterBarrierOpenAfterUnloadingPanel = false;
      _passedS2EXLowMagSafetyLevelSensorScan = false;
      _logSMEMAConveyorSignal = false;
      _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.UNLOAD_PANEL);
    }
    TimerUtil timer= new TimerUtil();
    timer.reset();
    timer.start();
    if (XrayTester.isS2EXEnabled() && _digitalIo.isOpticalStopControllerEndStopperInstalled()) //For Speed up
    {    
      if (_digitalIo.arePanelClampsOpened())
      {
        if (_primaryOuterBarrier == _leftOuterBarrier)
        {
           _digitalIo.extendRightPanelInPlaceSensor();          
        }
        else
        {
           _digitalIo.extendLeftPanelInPlaceSensor();            
        }
      }
    }        
    timer.stop();
    debug("_performanceLog.logMilestone = "+timer.getElapsedTimeInMillis());
    if(_isProduction)
    {
        _isProduction=false;
        //moveStageToOuterBarrier(_primaryOuterBarrier);
    }
    
    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.UNLOAD_PANEL);
  }
  public boolean isProduction()
  {
    return _isProduction;
  }
  public void setIsProduction(boolean isProduction)
  {
    _isProduction = isProduction;
  }
  public void moveStageToUnloadPanelPosition() throws XrayTesterException
  {
        moveStageToOuterBarrier(_secondaryOuterBarrier);
  }
  /**
   * When panel is lost in the system (not able to recapture, etc.), we need
   * the user to retrive panel manually to return the system to known good
   * state.  Once the retrival is done, we need to invoke this mehtod to
   * synchronize the panel handler states.
   */
  public void manualPanelRetrivalSucceeded() throws XrayTesterException
  {
    _panelLoaded = false;
    debug("Calling manualPanelRetrivalSucceeded(), setting _panelLoaded = false");

    // Clear saved panel information.
    _projectName = "";
    _panelWidthInNanoMeters = 0;
    _panelLengthInNanoMeters = 0;
    _panelExtraClearDelayInMiliSeconds = 0;
    _panelIsAgainst = null;

    // Update persistent panel information.
    savePersistentPanelInformation();
  }

  /**
   * Move the panel to the left side of the system so that it is positioned
   * agasinst the left Panel In Place sensor.  This is used to test large
   * panels that reposition is need to test "2nd half".
   * @author Rex Shang
   */
  public void movePanelToLeftSide() throws XrayTesterException
  {
    Assert.expect(_panelLoaded, "Panel is not loaded.");
    clearAbort();
    if (_panelIsAgainst == _leftPanelInPlaceSensor)
    {
      return;
    }

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _digitalIo.setEnableXXLOpticalStopController(false);
    }

    int delay = getDelayTimeFromLeftToRightOrRightToLeft();
    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.MOVE_PANEL_TO_LEFT_SIDE);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Need to move panel.
      if (isPanelLongerThanPIPClearance() == false)
      {
        // For shorter panel, we can extend left PIP and move panel towards it.
        _panelClamps.open();
        movePanelToPanelInPlaceSensor(_leftPanelInPlaceSensor,true);
      }
      else
      {
        // For longer panel, we have to move towards right barrier first to
        // clear left PIP.
        moveStageToOuterBarrier(_rightOuterBarrier);

        _panelClamps.open();
        _rightPanelInPlaceSensor.retract();

        movePanelToPanelClearSensor(_rightPanelClearSensor, false);

        try
        {
          Thread.sleep(delay);
        }
        catch (InterruptedException ie)
        {
          // do nothing
        }

        movePanelToPanelInPlaceSensor(_leftPanelInPlaceSensor,true);
      }
    }
    catch (XrayTesterException xte)
    {
      recoverFromHardwareException();
      // Throw the original exception.
      throw xte;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.MOVE_PANEL_TO_LEFT_SIDE);
  }

  /**
   * Move the panel to the right side of the system so that it is positioned
   * agasinst the right Panel In Place sensor.  This is used to test large
   * panels that reposition is need to test "2nd half".
   * @author Rex Shang
   */
  public void movePanelToRightSide() throws XrayTesterException
  {
    Assert.expect(_panelLoaded, "Panel is not loaded.");
    clearAbort();
    if (_panelIsAgainst == _rightPanelInPlaceSensor)
    {
      return;
    }

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _digitalIo.setEnableXXLOpticalStopController(false);
    }

    int delay = getDelayTimeFromLeftToRightOrRightToLeft();

    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.MOVE_PANEL_TO_RIGHT_SIDE);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Need to move panel.
      if (_forceLoadPanelTORight)
      {
        moveStageToOuterBarrier(_leftOuterBarrier);
        _panelClamps.open();
        _leftPanelInPlaceSensor.retract();

        movePanelToPanelClearSensor(_leftPanelClearSensor, false);
        try
        {
          Thread.sleep(delay);
        }
        catch (InterruptedException ie)
        {
          // do nothing
        }

        movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor,true);
      }
      else
      {
        if (isPanelLongerThanPIPClearance() == false)
        {
          // For shorter panel, we can extend right PIP and move panel towards it.
          _panelClamps.open();
          movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor,true);
        }
        else
        {
          // For longer panel, we have to move towards left barrier first to
          // clear left PIP.
          moveStageToOuterBarrier(_leftOuterBarrier);
          _panelClamps.open();
          _leftPanelInPlaceSensor.retract();

          movePanelToPanelClearSensor(_leftPanelClearSensor, false);
          try
          {
            Thread.sleep(delay);
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }

          movePanelToPanelInPlaceSensor(_rightPanelInPlaceSensor,true);
        }
      }
    }
    catch (XrayTesterException xte)
    {
      recoverFromHardwareException();
      // Throw the original exception.
      throw xte;
    }
    finally
    {
      _forceLoadPanelTORight = false;
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.MOVE_PANEL_TO_RIGHT_SIDE);
  }

  /**
   * This method should remain private.  It is used by reposition to see
   * if the panel needs to be moved "out of the way of PIP" first.
   * @author Rex Shang
   */
  public boolean isPanelLongerThanPIPClearance()
  {
    return (_panelLengthInNanoMeters
            >= (getPanelInPlaceSensorSpacingInNanoMeters() - getPanelLengthClearanceInNanoMeters()));
  }

  /**
   * @return int the distance in between the 2 panel clear sensors
   * @author Rex Shang
   */
  private int getPanelInPlaceSensorSpacingInNanoMeters()
  {
    return (getConfig().getIntValue(HardwareConfigEnum.RIGHT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS)
            - getConfig().getIntValue(HardwareConfigEnum.LEFT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS));
  }

  /**
   * Home the stage rails.
   *
   * This method will move the stage rails to their physical, maximum width.
   * @author Rex Shang
   */
  public void homeStageRails() throws XrayTesterException
  {
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    //Assert.expect(_panelLoaded == false, "Panel is already loaded. Can not perform stage rail homing with panel loaded.");
    //Assert.expect(_primaryPanelInPlaceSensor.isPanelInPlace() == false, "Panel is already loaded. Can not perform stage rail homing with panel loaded.");
    //Assert.expect(_secondaryPanelInPlaceSensor.isPanelInPlace() == false, "Panel is already loaded. Can not perform stage rail homing with panel loaded.");

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (_panelLoaded || _primaryPanelInPlaceSensor.isPanelInPlace() || _secondaryPanelInPlaceSensor.isPanelInPlace())
    {
      if (DigitalIo.isOpticalStopControllerInstalled())
      {
        if (XrayTester.isS2EXEnabled())
        {
          // Check to see if panel is resting against a panel in place sensor.
          if(_leftPanelInPlaceSensor.isPanelInPlace() && _rightPanelInPlaceSensor.isPanelInPlace())
          {
            //recapture for long panel
            if(recapturePanelWithInchMove())
            {
              throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
            }
          }
          else if (_leftPanelInPlaceSensor.isPanelInPlace())
          {
            _panelIsAgainst = _leftPanelInPlaceSensor;
            if(recapturePanelWithInchMove())
            {
              throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
            }
          }
          else if (_rightPanelInPlaceSensor.isPanelInPlace())
          {
            _panelIsAgainst = _rightPanelInPlaceSensor;
            if(recapturePanelWithInchMove())
            {
              throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
            }
          }
          else
          {
            throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
          }
        }
        else
        {
          throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
        }
      }
      else
      {
        //Swee-Yee.Wong - BEST implementation
	    boolean panelDrop = (_panelLoaded && _primaryPanelInPlaceSensor.isPanelInPlace() == false) || (_panelLoaded  && _secondaryPanelInPlaceSensor.isPanelInPlace() == false);
	    boolean pipStucked = false;
	    String pipCondition = null;
	    if(_panelLoaded == false && _primaryPanelInPlaceSensor.isPanelInPlace() && _secondaryPanelInPlaceSensor.isPanelInPlace())
	    {
	      pipStucked = true;
	      pipCondition = "BOTH";
	    }
	    else if(_panelLoaded == false && _primaryPanelInPlaceSensor.isPanelInPlace())
	    {
	      pipStucked = true;
	      pipCondition = "RIGHT";
	    }
	    else if(_panelLoaded == false && _secondaryPanelInPlaceSensor.isPanelInPlace())
	    {
	      pipStucked = true;
	      pipCondition = "LEFT";
	    }
	    String[] exceptionParameters = new String[] {pipCondition};
	    
	    boolean panelInside = (_panelLoaded && _primaryPanelInPlaceSensor.isPanelInPlace()) || (_panelLoaded  && _secondaryPanelInPlaceSensor.isPanelInPlace());
	    
	    if (Assert.expectWithErrorPrompt(panelDrop == false, ErrorHandlerEnum.HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_DROP))
	    {
	      // Confirm board dropped
	      throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
	    }
	    else if (Assert.expectWithErrorPrompt(pipStucked == false, ErrorHandlerEnum.HW_COMM_SET_PIP_HWL_CONFIRM_PIP_STUCKED, exceptionParameters))
	    {
	      // Confirm PIP is stucked
	      throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
	    }
	    else if (Assert.expectWithErrorPrompt(panelInside == false, ErrorHandlerEnum.HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_INSIDE))
	    {
	      //Confirm board inside and PIP is working
	      throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
	    }
	     else if (_panelLoaded || _primaryPanelInPlaceSensor.isPanelInPlace() || _secondaryPanelInPlaceSensor.isPanelInPlace())
	    {
	      throw new PanelPositionerException(PanelPositionerExceptionEnum.HOME_STAGE_RAIL_WITH_PANEL_INSIDE_MACHINE);
	    }
      }
    }
    
    checkPanelIsLoading();
    
    // XCR1144, Chnee Khang Wah, 21-Dec-2010
    if (isSafeToMove() == false && (isNearHomePosition() == true || isInLoadUnLoadPosition() == true))
    {
      throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
    }
    // XCR1144 - end
    clearAbort();
    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.HOME_STAGE_RAILS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _motionControl.home(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }
    finally
    {
      // Swee-Yee.Wong - Trigger Optical PIP sensor to reset the signal strength
      if (XrayTester.isS2EXEnabled())
      {
        _digitalIo.resetOpticalPIPSensor();
      }
      
      _hardwareObservable.setEnabled(true);
      // XCR1144, Chnee Khang Wah, 11-Jan-2011, turn on the checking after complete the startup if it was turned off previously
      _digitalIo.setByPassPanelClearSensor(false);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.HOME_STAGE_RAILS);
  }

  /**
   * Check to see if our system can work with a given flow direction.
   * @author Rex Shang
   */
  public boolean isPanelFlowDirectionOk(PanelHandlerPanelFlowDirectionEnum flowDirection)
  {
    Assert.expect(flowDirection != null);

    if (flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT))
    {
      if (_leftOuterBarrier.isInstalled())
      {
        return true;
      }
    }
    else if (flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT))
    {
      if (_rightOuterBarrier.isInstalled())
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Check to see if our system can work with a given loading mode.
   * @author Rex Shang
   */
  public boolean isPanelLoadingModeOk(PanelHandlerPanelLoadingModeEnum loadingMode)
  {
    Assert.expect(loadingMode != null);

    if (loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      if (_leftOuterBarrier.isInstalled() && _rightOuterBarrier.isInstalled())
      {
        return true;
      }
    }
    else if (loadingMode.equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))
    {
      if (_leftOuterBarrier.isInstalled() || _rightOuterBarrier.isInstalled())
      {
        return true;
      }
    }

    return false;
  }

  /**
   * This method is used to set the flow direction of panel loading.
   * The new flow direction should be checked using isPanelFlowDirectionOk()
   * for validity before invoking this method.
   * @author Rex Shang
   */
  public void setPanelFlowDirection(PanelHandlerPanelFlowDirectionEnum flowDirection) throws XrayTesterException
  {
    Assert.expect(flowDirection != null, "flowDirection is null.");
    Assert.expect(isPanelFlowDirectionOk(flowDirection), "Flow direction is invalid.");
    clearAbort();
    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.SET_PANEL_FLOW_DIRECTION);
    _hardwareObservable.setEnabled(false);

    try
    {
      getConfig().setValue(SoftwareConfigEnum.PANEL_HANDLER_PANEL_FLOW_DIRECTION, flowDirection.toString());

      if (_initialized && flowDirection.equals(_flowDirection) == false)
      {
        _flowDirection = flowDirection;
        setupForPanelFlowDirectionAndLoadingMode();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.SET_PANEL_FLOW_DIRECTION);
  }

  /**
   * @author George Booth
   */
  public PanelHandlerPanelFlowDirectionEnum getPanelFlowDirection()
  {
    String flowDirection = getConfig().getStringValue(SoftwareConfigEnum.PANEL_HANDLER_PANEL_FLOW_DIRECTION);
    return PanelHandlerPanelFlowDirectionEnum.getEnum(flowDirection);
  }

  /**
   * @return the primary outer barrier
   * @author Farn Sern
   */
  public BarrierInt getPrimaryOuterBarrier()
  {
    return _primaryOuterBarrier;
  }

  /**
   * This method is used to set the loading mode of panel loading.
   * The new mode should be checked using isPanelLoadingModeOk()
   * for validity before invoking this method.
   * @author Rex Shang
   */
  public void setPanelLoadingMode(PanelHandlerPanelLoadingModeEnum loadingMode) throws XrayTesterException
  {
    Assert.expect(loadingMode != null, "loadingMode is null.");
    Assert.expect(isPanelLoadingModeOk(loadingMode), "Loading mode is invalid.");
    clearAbort();
    _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.SET_PANEL_LOADING_MODE);
    _hardwareObservable.setEnabled(false);

    try
    {
      getConfig().setValue(SoftwareConfigEnum.PANEL_HANDLER_LOADING_MODE, loadingMode.toString());

      if (_initialized && loadingMode.equals(_loadingMode) == false)
      {
        _loadingMode = loadingMode;
        setupForPanelFlowDirectionAndLoadingMode();
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.SET_PANEL_LOADING_MODE);
  }

  /**
   * @author George Booth
   */
  public PanelHandlerPanelLoadingModeEnum getPanelLoadingMode()
  {
    String loadingMode = getConfig().getStringValue(SoftwareConfigEnum.PANEL_HANDLER_LOADING_MODE);
    return PanelHandlerPanelLoadingModeEnum.getEnum(loadingMode);
  }

  /**
   * This method is used to set up the internal variables associated with
   * panel loading modes and flow directions.
   * @author Rex Shang
   */
  private void setupForPanelFlowDirectionAndLoadingMode()
  {
    if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT)
            && _loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      // Left to right flow through.
      _primaryOuterBarrier = _leftOuterBarrier;
      _secondaryOuterBarrier = _rightOuterBarrier;
      _primaryPanelClearSensor = _leftPanelClearSensor;
      _secondaryPanelClearSensor = _rightPanelClearSensor;
      _primaryPanelInPlaceSensor = _rightPanelInPlaceSensor;
      _secondaryPanelInPlaceSensor = _leftPanelInPlaceSensor;

      //Variable Mag Anthony August 2011
      _primarySafetyLevelSensor = _leftSafetyLevelSensor;
      _secondarySafetyLevelSensor = _rightSafetyLevelSensor;

    }
    else if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT)
            && _loadingMode.equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))
    {
      // Left to right pass back.
      _primaryOuterBarrier = _leftOuterBarrier;
      _secondaryOuterBarrier = _leftOuterBarrier;
      _primaryPanelClearSensor = _leftPanelClearSensor;
      _secondaryPanelClearSensor = _leftPanelClearSensor;
      _primaryPanelInPlaceSensor = _rightPanelInPlaceSensor;
      _secondaryPanelInPlaceSensor = _leftPanelInPlaceSensor;

      //Variable Mag Anthony August 2011
      _primarySafetyLevelSensor = _leftSafetyLevelSensor;
      _secondarySafetyLevelSensor = _leftSafetyLevelSensor;

    }
    else if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT)
            && _loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      // Right to left flow through.
      _primaryOuterBarrier = _rightOuterBarrier;
      _secondaryOuterBarrier = _leftOuterBarrier;
      _primaryPanelClearSensor = _rightPanelClearSensor;
      _secondaryPanelClearSensor = _leftPanelClearSensor;
      _primaryPanelInPlaceSensor = _leftPanelInPlaceSensor;
      _secondaryPanelInPlaceSensor = _rightPanelInPlaceSensor;

      //Variable Mag Anthony August 2011
      _primarySafetyLevelSensor = _rightSafetyLevelSensor;
      _secondarySafetyLevelSensor = _leftSafetyLevelSensor;
    }
    else if (_flowDirection.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT)
            && _loadingMode.equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))
    {
      // Right to left pass back.
      _primaryOuterBarrier = _rightOuterBarrier;
      _secondaryOuterBarrier = _rightOuterBarrier;
      _primaryPanelClearSensor = _rightPanelClearSensor;
      _secondaryPanelClearSensor = _rightPanelClearSensor;
      _primaryPanelInPlaceSensor = _leftPanelInPlaceSensor;
      _secondaryPanelInPlaceSensor = _rightPanelInPlaceSensor;

      //Variable Mag Anthony August 2011
      _primarySafetyLevelSensor = _rightSafetyLevelSensor;
      _secondarySafetyLevelSensor = _rightSafetyLevelSensor;
    }
  }

  /**
   * XCR1144, Chnee Khang Wah, 21-Dec-2010
   * Check if a panel is in the machine
   * @return boolean whether a panel is blocking panel clear sensor or not
   * @author Khang-Wah, Chnee
   */
  public boolean isSafeToMove() throws XrayTesterException
  {
    return _panelPositioner.isSafeToMove();
  }

  /**
   * XCR1144, Chnee Khang Wah, 22-Dec-2010
   * check wheater the board is near to home position? this is keep out zone!
   * @author Khang-Wah, Chnee
   */
  public boolean isNearHomePosition()
  {
    return _panelPositioner.isNearHomePosition();
  }

  /**
   * XCR1144, Chnee Khang Wah, 22-Dec-2010
   * check wheater the board is at loading/unloading position?
   * @author Khang-Wah, Chnee
   */
  public boolean isInLoadUnLoadPosition()
  {
    return _panelPositioner.isInLoadUnLoadPosition();
  }

  /**
   * @author Rex Shang
   */
  public boolean isRailWidthHomeSensorClear() throws XrayTesterException
  {
    return _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
  }

  /**
   * Set rail width for a given panel size.  The rail width is set to
   * panel width + clearance.
   * @param panelWidthInNanoMeters int
   * @throws XrayTesterException
   * @author Rex Shang
   */
  void setRailWidthForPanel(int panelWidthInNanoMeters) throws XrayTesterException
  {
    int railWidthInNanoMeters = panelWidthInNanoMeters + getPanelWidthClearanceInNanoMeters();

    setRailWidthInNanoMeters(railWidthInNanoMeters);
  }

  /**
   * Set the rail width in nanometers.
   * @author Rex Shang
   */
  public void setRailWidthInNanoMeters(int widthInNanometers) throws XrayTesterException
  {
    int toleranceInNanometers = _motionControl.getPositionAccuracyToleranceInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    
    if (MathUtil.fuzzyEquals(widthInNanometers, getRailWidthInNanoMeters(), toleranceInNanometers) && (Interlock.getInstance().getServicePanelActivation() == false))
    {
      //do nothing
      //System.out.println("Do nothing with rail width");
    }
    else
    {
      if (_digitalIo.arePanelClampsClosed())
      {
        _digitalIo.openPanelClamps();
      // Swee-Yee.Wong
        // This delay is used to compensate the respond time of optical stop sensor
        // This will be removed in the future.
        try
        {
          Thread.sleep(500);
        }
        catch (InterruptedException ex)
        {
          // Do nothing
        }
      }

      // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
      Assert.expect(_panelLoaded == false, "Panel is already loaded. Can not adjust stage rail width with panel loaded");
      Assert.expect(_primaryPanelInPlaceSensor.isPanelInPlace() == false, "Panel is already loaded. Can not adjust stage rail width with panel loaded.");
      Assert.expect(_secondaryPanelInPlaceSensor.isPanelInPlace() == false, "Panel is already loaded. Can not adjust stage rail width with panel loaded.");

      checkPanelIsLoading();

      // XCR1144, Chnee Khang Wah, 21-Dec-2010
      if (isSafeToMove() == false && (isNearHomePosition() == true || isInLoadUnLoadPosition() == true))
      {
        throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
      }
      // XCR1144 - end

      Assert.expect(widthInNanometers >= getMinimumRailWidthInNanoMeters());
      Assert.expect(widthInNanometers <= getMaximumRailWidthInNanoMeters());
      clearAbort();

      _hardwareObservable.stateChangedBegin(this, PanelHandlerEventEnum.SET_RAIL_WIDTH);
      _hardwareObservable.setEnabled(false);

      try
      {
        MotionAxisPosition position = new MotionAxisPosition();
        position.setRailWidthAxisPositionInNanometers(widthInNanometers);

        /**
         * @todo RMS need to change to a faster motion profile.
         */
        _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS,
          new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
        _motionControl.pointToPointMove(position, MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      }
      catch (XrayTesterException xte)
      {
        _initialized = false;
        throw xte;
      }
      finally
      {
        // Swee-Yee.Wong - Trigger Optical PIP sensor to reset the signal strength
        if (XrayTester.isS2EXEnabled())
        {
          _digitalIo.resetOpticalPIPSensor();
        }

        _hardwareObservable.setEnabled(true);
        // XCR1144, Chnee Khang Wah, 11-Jan-2011, turn on the checking after complete the startup if it was turned off previously
        _digitalIo.setByPassPanelClearSensor(false);
      }

      _hardwareObservable.stateChangedEnd(this, PanelHandlerEventEnum.SET_RAIL_WIDTH);
    }
  }

  /**
   * Get the rail width in nanometers.
   * @author Rex Shang
   */
  public int getRailWidthInNanoMeters() throws XrayTesterException
  {
    Assert.expect(_initialized, "Panel Handler is not initialized.");
    int widthInNanometers = -1;
    try
    {
      widthInNanometers =
              _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }

    int toleranceInNanometers = _motionControl.getPositionAccuracyToleranceInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(widthInNanometers, getMinimumRailWidthInNanoMeters(), toleranceInNanometers));
    Assert.expect(MathUtil.fuzzyLessThanOrEquals(widthInNanometers, getMaximumRailWidthInNanoMeters(), toleranceInNanometers));

    return widthInNanometers;
  }

  /**
   * @author Rex Shang
   */
  public boolean isStageRailHomeSensorClear() throws XrayTesterException
  {
    boolean isSensorClear = false;
    try
    {
      isSensorClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }

    return isSensorClear;
  }

  /**
   * Get the maximum rail width in nanometers.
   * @author Rex Shang
   */
  public int getMaximumRailWidthInNanoMeters() throws XrayTesterException
  {
    int widthInNanometers = -1;
    try
    {
      widthInNanometers =
              _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }

    return widthInNanometers;
  }

  /**
   * Get the minimum rail width in nanometers.
   * @author Rex Shang
   */
  public int getMinimumRailWidthInNanoMeters() throws XrayTesterException
  {
    int widthInNanometers = -1;
    try
    {
      widthInNanometers =
              _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      _initialized = false;
      throw xte;
    }

    return widthInNanometers;
  }

  /**
   * Get the maximum panel width in nanometers.
   * @author Rex Shang
   */
  public static int getMaximumPanelWidthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_WIDTH_IN_NANOMETERS);
  }

  /**
   * Get the minimum panel width in nanometers.
   * @author Rex Shang
   */
  public static int getMinimumPanelWidthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MINIMUM_PANEL_WIDTH_IN_NANOMETERS);
  }

  /**
   * Get the maximum panel length in nanometers.
   * @author Rex Shang
   */
  public static int getMaximumPanelLengthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_LENGTH_IN_NANOMETERS);
  }

  /**
   * Get the minimum panel length in nanometers.
   * @author Rex Shang
   */
  public static int getMinimumPanelLengthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MINIMUM_PANEL_LENGTH_IN_NANOMETERS);
  }

  /**
   * @author Rex Shang
   */
  public static int getPanelWidthClearanceInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_WIDTH_CLEARANCE_IN_NANOMETERS);
  }

  /**
   * Right now, use the width clearance as the length clearance
   * @author Rex Shang
   */
  private static int getPanelLengthClearanceInNanoMeters()
  {
    return getPanelWidthClearanceInNanoMeters();
  }

  /**
   * Get the hardware limit for minimum panel thickness.
   * @author Rex Shang
   */
  public static int getMinimumPanelThicknessInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MINIMUM_PANEL_THICKNESS_IN_NANOMETERS);
  }

  /**
   * Panel thicker than this should not be allowed to load.
   * @return maximum panel thickness allowed.
   * @author Rex Shang
   */
  public static int getMaximumPanelThicknessInNanoMeters()
  {
    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
    {
      return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_THICKNESS_IN_NANOMETERS) + XrayTester.getSystemPanelThicknessOffset();
    }
    else
    {
      return getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_THICKNESS_IN_NANOMETERS);
    }
  }
  
  /**
   * Panel thicker than this should not be allowed to load.
   * @return old maximum panel thickness allowed (125mil).
   * @author Chnee Khang Wah
   */
  public static int getLegacyMaximumPanelThicknessInNanoMeters()
  {
    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
    {
      return _LEGACY_MAX_BOARD_THICKNESS + XrayTester.getSystemPanelThicknessOffset();
    }
    else
    {
      return _LEGACY_MAX_BOARD_THICKNESS;
    }
  }
  
  /**
   * Default extra delay during unload (5000ms)
   * @author Chnee Khang Wah, 2015-08-05
   */
  public static int getDefaultExtraClearDelayInMiliseconds()
  {
      return _DEFAULT_EXTRA_CLEAR_DELAY_IN_MILLISECONDS;
  }

  /**
   * Update in-line mode information in configuration file.
   * @author Rex Shang
   */
  public static void setInLineModeEnabled(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.PANEL_HANDLER_SMEMA_ENABLE, new Boolean(enabled));
  }

  /**
   * @return boolean whether the panel handler is operating in SMEMA/"in-line" mode.
   * @author Rex Shang
   */
  public static boolean isInLineModeEnabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_SMEMA_ENABLE);
  }

  /**
   * Update turbo mode information in configuration file
   * @author Chnee Khang Wah (12-Oct-2009)
   */
  public void setIgnorePIPModeEnabled(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.PANEL_HANDLER_IGNORE_PIP_ENABLE, new Boolean(enabled));
  }

  /**
   * @return boolean whether the panel handler is operating in Turbo Mode if SMEMA is ON
   * Ignore PIP mode mean, system will close outer barrier once the clear sensor have been clearer, system will not wait until
   * the board hit PIP Sensor.
   * When this mode is ON, please be aware that the outer barrier may "chop" the board if the board is not handler
   * properly during loading.
   *@author Chnee Khang Wah (12-Oct-2009)
   */
  public static boolean isIgnorePIPModeEnabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_IGNORE_PIP_ENABLE);
  }

  /**
   * Check if a panel is in the machine
   * @return boolean whether a panel is currently in the system
   * @author Rex Shang
   */
  public boolean isPanelLoaded() throws XrayTesterException
  {
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    boolean isPanelLoaded = true; //assume panel is loadded by default
    if (XrayTester.isS2EXEnabled())
    {
      if (_initialized)
      {
        //this function is used to skip the PIP sensor checking whenever panel is loaded based on safety level checking
        if(isPanelLoadedBasedOnS2EXPassedSafetyLevelSensorScan())
        {
          isPanelLoaded = _panelLoaded;
        }
        else
        {
          if(Interlock.getInstance().getServicePanelActivation()||_digitalIo.isLeftOuterBarrierOpen()||_digitalIo.isRightOuterBarrierOpen()||_digitalIo.isInterlockChain1Open()||_digitalIo.isInterlockChain2Open())
          {
            isPanelLoaded = _panelLoaded;
          }
          else
          {
            isPanelLoaded = _panelLoaded || _primaryPanelInPlaceSensor.isPanelInPlace() || _secondaryPanelInPlaceSensor.isPanelInPlace();
          }
        }
      }
      else
      {
        isPanelLoaded = _panelLoaded;
      }
    }
    else
    {
      if (_initialized)
      {
        isPanelLoaded = _panelLoaded || _primaryPanelInPlaceSensor.isPanelInPlace() || _secondaryPanelInPlaceSensor.isPanelInPlace();
      }
      else
      {
        isPanelLoaded = _panelLoaded;
      }
    }
    
    return isPanelLoaded;
  }

  /**
   * @author Anthony Fong
   */
  public boolean getPassedSafetyLevelSensorScan()
  {
    if (XrayTester.isS2EXEnabled())
    {
      return _passedS2EXHighMagSafetyLevelSensorScan && _passedS2EXLowMagSafetyLevelSensorScan;
    }
    else
    {
      return _passedHighMagSafetyLevelSensorScan;
    }
  }
  
  /**
   * @author Anthony Fong
   */
  public boolean isPanelLoadedBasedOnS2EXPassedSafetyLevelSensorScan()
  {
      return _passedS2EXHighMagSafetyLevelSensorScan || _passedS2EXLowMagSafetyLevelSensorScan;
  }

  /**
   * Open the outer barrier specified as necessary.
   * @author Rex Shang
   */
  void openOuterBarrierAsNecessary(BarrierInt outerBarrier) throws XrayTesterException
  {
    Assert.expect(outerBarrier == _leftOuterBarrier || outerBarrier == _rightOuterBarrier);

    if (outerBarrier.isOpen() == false)
    {
      //XCR1775- Interlock safety checking- Anthony
      if (InnerBarrier.isInstalled())
      {
        if (_digitalIo.isInnerBarrierClosed() == false)
        {
          //logging for abnormality detection
          System.out.println("PanelHandler:Inner barrier was opened. Close it now before open outer barrier.");
          InnerBarrier.getInstance().close();
          if (_digitalIo.isInnerBarrierClosed() == true)
          {
            outerBarrier.open();
          }
          else
          {
            throw DigitalIoException.getInnerBarrierFailedToCloseException();
          }
        }
        else
        {
          outerBarrier.open();
        }
      }
      else
      {
        outerBarrier.open();
      }
    }
  }

  /**
   * Move stage to the outer barrier specified.
   * @author Rex Shang
   */
  void moveStageToOuterBarrier(BarrierInt barrier) throws XrayTesterException
  {
    Assert.expect(barrier != null);
    // The barrier can be only left or right outer barrier (NOT inner barrier).
    Assert.expect(barrier.equals(_leftOuterBarrier) || barrier.equals(_rightOuterBarrier));

    StagePositionMutable stagePosition = new StagePositionMutable();

    int xPosition = 0;
    int yPosition = 0;
    
    if (XrayTester.isS2EXEnabled() && _digitalIo.isOpticalStopControllerEndStopperInstalled()) //For Speed up
    {
      //Extend if loading
      if (barrier.equals(_primaryOuterBarrier))
      {
        if (_digitalIo.arePanelClampsOpened())
        {
          if (_primaryOuterBarrier == _leftOuterBarrier)
          {
            _digitalIo.extendRightPanelInPlaceSensor();          
          }
          else
          {
             _digitalIo.extendLeftPanelInPlaceSensor();            
          }
        }
      }
    }
    
    if (barrier.equals(_leftOuterBarrier))
    {
      xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
      yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
    }
    else
    {
      xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
      yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
    }

    stagePosition.setXInNanometers(xPosition);
    stagePosition.setYInNanometers(yPosition);

    // Save off the previously loaded motion profile
    MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
    // Set to the fastest motion profile.
    _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (XrayTester.isXXLBased() && DigitalIo.isOpticalStopControllerInstalled() && UnitTest.unitTesting() == false)
    {
      // For optical stop sensor, we have to move XY stage towards left outer barrier first
      // in order to algin to left PIP.
      _motionControl.setCheckForPointToPointMoveCompletion(true);
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
      _motionControl.setCheckForPointToPointMoveCompletion(false);
    }
    else
    {
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);
    }

    // Now that we are done with the move, restore the motion profile.
    _panelPositioner.setMotionProfile(originalMotionProfile);
  }
  
   /**
   * For Speed Up
   * @author Anthony Fong
   */
  public void openSecondaryOuterBarrier() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled()) //For Speed up
    {
      if (_secondaryOuterBarrier == _leftOuterBarrier)
      {
        _digitalIo.openLeftOuterBarrier();          
      }
      else
      {
         _digitalIo.openRightOuterBarrier();            
      }
    }
  }
  
  /**
   * to close unload side outer barrier
   * @author sheng chuan
   */
  public void closeSecondaryOuterBarrier() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled()) //For Speed up
    {
      if (_secondaryOuterBarrier == _leftOuterBarrier)
      {
        if (_digitalIo.isLeftOuterBarrierOpen())
        {
          LeftOuterBarrier.getInstance().close();
        }
      }
      else
      {
        if (_digitalIo.isRightOuterBarrierOpen())
        {
          RightOuterBarrier.getInstance().close();
        }
      }
    }
  }
  
  /**
   * For Speed Up
   * @author Anthony Fong
   */
  boolean isStageAtLoadPosition() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled()) //For Speed up
    {
      int xPosition=0;
      int yPosition=0;
      if (_primaryOuterBarrier == _leftOuterBarrier)
      {
         xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
         yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
      }
      else
      {
         xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
         yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
      }
      return isStageAtPosition(xPosition, yPosition);
    }   
    return false;
  }
  
    /**
   * For Speed Up
   * @author Anthony Fong
   */
  boolean isStageAtUnloadPosition() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled()) //For Speed up
    {
      long xPosition=0;
      long yPosition=0;
      if (_secondaryOuterBarrier == _leftOuterBarrier)
      {
         xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
         yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_LEFT_OUTER_BARRIER_IN_NANOMETERS);
      }
      else
      {
         xPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_X_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
         yPosition = getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_POSITIONER_Y_ALIGNMENT_WITH_RIGHT_OUTER_BARRIER_IN_NANOMETERS);
      }
      boolean isStageAtUnload = isStageAtPosition(xPosition, yPosition);
      debug("StageAtUnloadPosition - isStageAtUnloadPosition return = " + isStageAtUnload);
      return isStageAtUnload;
    }   
    

    return false;
  }
  
   /**
   * For Speed Up
   * @author Anthony Fong
   */
  boolean isStageAtPosition(long X, long Y) throws XrayTesterException
  {
    long xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
    long yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
    
    debug("StageAtUnloadPosition - isStageAtPosition X = " + X);
    debug("StageAtUnloadPosition - isStageAtPosition Y = " + Y);  
    debug("StageAtUnloadPosition - isStageAtPosition xPositionInNanometers = " + xPositionInNanometers);
    debug("StageAtUnloadPosition - isStageAtPosition yPositionInNanometers = " + yPositionInNanometers);

    //For position error of 20um. (20000)
    //126000
    //100000
    int error = 200000;
    long xMin = X - error; 
    long xMax = X + error; 
    long yMin = Y - error; 
    long yMax = Y + error; 
    if((xMin<xPositionInNanometers) && (xPositionInNanometers<xMax))
    {
      if((yMin<yPositionInNanometers) && (yPositionInNanometers<yMax))
      {
        debug("StageAtUnloadPosition - isStageAtPosition return true");
        return true;
      }
    }
    debug("StageAtUnloadPosition - isStageAtPosition return false");
    return false;
  }
  
  /**
   * Spawn threads to do hardware tasks in parallel.  In this method, we are
   * moving the stage and sizing the rail.
   * @author Rex Shang
   */
  private void moveStageAndSetRailWidthInParallelForLoadingPanel(boolean XXLSystem) throws XrayTesterException
  {
    // Closing both barrier just to be sure so we can move stage and rail safely.
    if (_repeatUnloadLoadModeOn == false)
    {
      closeAllOuterBarriersInParallel();
    }
    
    if (XXLSystem && _digitalIo.isOpticalStopControllerEndStopperInstalled()) //For Speed up
    {
      if (_digitalIo.arePanelClampsOpened())
      {
        if (_primaryOuterBarrier == _leftOuterBarrier)
        {
          _digitalIo.extendRightPanelInPlaceSensor();          
        }
        else
        {
           _digitalIo.extendLeftPanelInPlaceSensor();            
        }
      }
    }

    ThreadTask<Object> task1 = new MoveStageToOuterBarrierThreadTask(_primaryOuterBarrier);
    ThreadTask<Object> task2 = new SetRailWidthThreadTask(_panelWidthInNanoMeters);
    ThreadTask<Object> task3 = new OpenPanelClampsThreadTask();
    ThreadTask<Object> task4 = null;
    ThreadTask<Object> task5 = null;
    if (XXLSystem == false)
    {
      task4 = new RetractPanelInPlaceSensorThreadTask(_secondaryPanelInPlaceSensor);
      task5 = new ExtendPanelInPlaceSensorThreadTask(_primaryPanelInPlaceSensor);
    }
    else
    {
      _secondaryPanelInPlaceSensor.retract();
    }

    // Start the tasks and wait.
    if (_repeatUnloadLoadModeOn == false)
    {
      // In repeat unload load mode, don't close the barrier and change rail
      // width since the panel is still left on the rails.
      if (XXLSystem && _digitalIo.isOpticalStopControllerEndStopperInstalled()) //For Speed up
      {
        if(isStageAtLoadPosition())
        {
          // Skip MoveStageToOuterBarrierThreadTask
        }
        else
        {
          _panelHandlerParallelTasks.submitThreadTask(task1);
        }
      }
      else
      {
        _panelHandlerParallelTasks.submitThreadTask(task1);
      }
      _panelHandlerParallelTasks.submitThreadTask(task2);
    }

    _panelHandlerParallelTasks.submitThreadTask(task3);

    if (XXLSystem == false)
    {
      _panelHandlerParallelTasks.submitThreadTask(task4);
      _panelHandlerParallelTasks.submitThreadTask(task5);
    }

    try
    {
      _panelHandlerParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }

    if (XXLSystem && _digitalIo.isOpticalStopControllerEndStopperInstalled())
    {
      if (_digitalIo.arePanelClampsOpened())
      {
        if (_primaryOuterBarrier == _leftOuterBarrier)
        {
          _digitalIo.extendRightPanelInPlaceSensor();          
        }
        else
        {
           _digitalIo.extendLeftPanelInPlaceSensor();            
        }
      }    
    }
  }

  /**
   * Spawn threads to do hardware tasks in parallel.  In this method, we are
   * openning the outer barrier, panel clamps and retracting the panel in place
   * sensor.
   * @author Rex Shang
   */
  private void openOuterBarrierAndReleasePanelForUnloadingPanel(boolean is_XXL_Or_S2EX_System) throws XrayTesterException
  {
    ThreadTask<Object> task1 = new OpenOuterBarrierThreadTask(_secondaryOuterBarrier);
    ThreadTask<Object> task2 = new OpenPanelClampsThreadTask();
    ThreadTask<Object> task3 = new RetractPanelInPlaceSensorThreadTask(_primaryPanelInPlaceSensor);
    // When testing BIG panel, the panel might be moved to the secondary
    // panel in place sensor position.
    // Retract both panel in place sensor just to be sure.
    ThreadTask<Object> task4 = new RetractPanelInPlaceSensorThreadTask(_secondaryPanelInPlaceSensor);

    // Start the tasks and wait.
    // For manual mode unload, there is no loader to catch our panel in
    // case panel clear sensor missed.  Let's keep the door closed.
    if (isInLineModeEnabled() || PanelClearSensor.isRecessedMountInstalled() == false)
    {
      _panelHandlerParallelTasks.submitThreadTask(task1);
    }
    _panelHandlerParallelTasks.submitThreadTask(task2);
    if(is_XXL_Or_S2EX_System == false)
    {
      _panelHandlerParallelTasks.submitThreadTask(task3);
      _panelHandlerParallelTasks.submitThreadTask(task4);
    }
    try
    {
      _panelHandlerParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * Move panel to a panel in place sensor and capture it.
   * @author Rex Shang
   */
  private void movePanelToPanelInPlaceSensor(PanelInPlaceSensorInt panelInPlaceSensor, boolean needToClampPanelAndReposition) throws XrayTesterException
  {
    Assert.expect(panelInPlaceSensor != null);

    ExecuteParallelThreadTasks<Object> movePanelToPanelInPlaceSensorParallelTasks = new ExecuteParallelThreadTasks<Object>();
    ThreadTask<Object> task1 = null;
    ThreadTask<Object> task2 = null;

    // Set up stage belt direction and retract the other PIP just in case.
    if (panelInPlaceSensor.equals(_leftPanelInPlaceSensor))
    {
      //XXL Optical Stop Sensor - Anthony (May 2013)
      if (DigitalIo.isOpticalStopControllerInstalled() && UnitTest.unitTesting() == false)
      {
        // For optical stop sensor, we have to move XY stage towards left outer barrier first
        // in order to algin to left PIP.
        if(_digitalIo.areStageBeltsOn()==false)
        {
          resetXXLOpticalStopController();
        }
        if(needToClampPanelAndReposition && _twoStageInitializationInProgress == false) //No need to Clamp Panel And Reposition during LoadPanel
        {
          _panelClamps.close();
          if (XrayTester.isXXLBased())
          {
            _motionControl.setCheckForPointToPointMoveCompletion(true);
            moveXYStageToRightUnloadPosition(false);
            _motionControl.setCheckForPointToPointMoveCompletion(false);
          }
          else
          {
            moveXYStageToRightUnloadPosition(false);
          }
          _digitalIo.setEnableXXLOpticalStopController(true);
          _panelClamps.open();
        }
      }
      task1 = new RetractPanelInPlaceSensorThreadTask(_rightPanelInPlaceSensor);
      task2 = new ExtendPanelInPlaceSensorThreadTask(_leftPanelInPlaceSensor);
      _stageBelts.setDirectionRightToLeft();
    }
    else
    {
      //XXL Optical Stop Sensor - Anthony (May 2013)
      if (DigitalIo.isOpticalStopControllerInstalled() && UnitTest.unitTesting() == false)
      {
        // For optical stop sensor, we have to move XY stage towards right outer barrier first
        // in order to algin to right PIP.
        if(_digitalIo.areStageBeltsOn()==false)
        {
          resetXXLOpticalStopController();
        }
        if(needToClampPanelAndReposition && _twoStageInitializationInProgress == false) //Not need to Clamp Panel And Reposition during LoadPanel
        {
          _panelClamps.close();
          if (XrayTester.isXXLBased())
          {
            _motionControl.setCheckForPointToPointMoveCompletion(true);
            moveXYStageToLeftLoadPosition(false);
            _motionControl.setCheckForPointToPointMoveCompletion(false);
          }
          else
          {
             moveXYStageToLeftLoadPosition(false);                     
          }
          _digitalIo.setEnableXXLOpticalStopController(true);
          _panelClamps.open();
        }
      }
      task1 = new RetractPanelInPlaceSensorThreadTask(_leftPanelInPlaceSensor);
      task2 = new ExtendPanelInPlaceSensorThreadTask(_rightPanelInPlaceSensor);
      _stageBelts.setDirectionLeftToRight();
    }

    movePanelToPanelInPlaceSensorParallelTasks.submitThreadTask(task1);
    movePanelToPanelInPlaceSensorParallelTasks.submitThreadTask(task2);

    try
    {
      movePanelToPanelInPlaceSensorParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
    //XCR-3623, Motion error when try to unload board
    if (_digitalIo.isOpticalStopControllerInstalled() && 
        _digitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP() &&
        _twoStageInitializationInProgress)
    {
      _digitalIo.setEnableXXLOpticalStopController(false);
      _rightPanelInPlaceSensor.setNeedToBypassOpticalSensorSignal(true);
      _leftPanelInPlaceSensor.setNeedToBypassOpticalSensorSignal(true);
    }
    
    _panelIsLoading = true;
    savePersistentPanelInformation();
    
    _stageBelts.on();
    _panelIsAgainst = null;

    panelInPlaceSensor.waitUntilPanelInPlace(
            _WAIT_FOR_PANEL_TO_ENGAGE_PANEL_IN_PLACE_SENSOR_TIME_OUT_IN_MILLI_SECONDS, _WAIT_NOT_ABORTABLE);

    _stageBelts.off();
    
    _panelIsLoading = false;

    // XCR1144, Chnee Khang Wah, 23-Dec-2010
    // Check to make sure nothing is blocking panel clear sensor
    // We need to do this before panel clamps close to make sure user able to remove the panel if it is stuck.
    if (_panelPositioner.isSafeToMove() == false)
    {
      throw new PanelPositionerException(PanelPositionerExceptionEnum.PANEL_CLEAR_SENSOR_UNEXPCTEDLY_BLOCKED_DURING_LOAD);
    }
    // XCR1144 - end

    //Swee yee wong - for optical PIP module, panel clamp will be opened before check PIP sensor. So move this close panel action after the checking step
    //_panelClamps.close();

    if (panelInPlaceSensor.isPanelInPlace() == false && UnitTest.unitTesting() == false)
    {
      _panelNotInPlaceCounter++;
      debug("Panel NOT in place: " + _panelNotInPlaceCounter);
    }
    
    //Swee yee wong - for optical PIP module, panel clamp will be opened before check PIP sensor. So move this close panel action after the checking step
    _panelClamps.close();

    if (DigitalIo.isOpticalStopControllerInstalled() && UnitTest.unitTesting() == false)
    {
      checkForOpticalSensorControllerDoneSignal();
      if (_digitalIo.isOpticalStopControllerError() == true)
      {
        _digitalIo.setEnableXXLOpticalStopController(false);
        System.out.println("Optical Stop Controller fail to align panel: " + _panelNotInPlaceCounter);
        throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
      }
      else if (_digitalIo.isOpticalStopControllerDone() == false)
      {
        _digitalIo.setEnableXXLOpticalStopController(false);
        System.out.println("Optical Stop Controller not completed properly: " + _panelNotInPlaceCounter);
        throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
      }
      else
      {
        // Update panel position information.
        _panelIsAgainst = panelInPlaceSensor;
      }
      _digitalIo.setEnableXXLOpticalStopController(false);

    }
    else
    {
      // Update panel position information.
      _panelIsAgainst = panelInPlaceSensor;
    }
    //XCR-3623, Motion error when try to unload board
    _rightPanelInPlaceSensor.setNeedToBypassOpticalSensorSignal(false);
    _leftPanelInPlaceSensor.setNeedToBypassOpticalSensorSignal(false);
    savePersistentPanelInformation();
  }

  /**
   * @author Bill Darbie
   */
  void setPanelIsAgainst(PanelInPlaceSensorInt panelIsAgainst)
  {
    // null is okay
    _panelIsAgainst = panelIsAgainst;
  }

  /**
   * Spawn threads to do hardware tasks in parallel.  In this method, we are
   * waiting for the PCS to block while starting the stage belt.
   * @author Rex Shang
   */
  private void movePanelToPanelClearSensor(final PanelClearSensor panelClearSensor, boolean leaveStageBeltsOn) throws XrayTesterException
  {
    Assert.expect(panelClearSensor != null);
    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _digitalIo.setEnableXXLOpticalStopController(false);
    }
    BlockingQueue<Object> stageBeltLockingQueue = new ArrayBlockingQueue<Object>(1);

    ThreadTask<Object> task1 = new WaitForPanelClearSensorToBlockThreadTask(
            panelClearSensor,
            stageBeltLockingQueue,
            _WAIT_FOR_PANEL_TO_REACH_PANEL_CLEAR_SENSOR_TIME_OUT_IN_MILLI_SECONDS,
            leaveStageBeltsOn);
    ThreadTask<Object> task2 = new MoveStageBeltThreadTask(panelClearSensor, stageBeltLockingQueue);

    // Start the tasks and wait.
    _panelHandlerParallelTasks.submitThreadTask(task1);
    _panelHandlerParallelTasks.submitThreadTask(task2);
    try
    {
      _panelHandlerParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * While loading/unloading, moving panel, a HardwareException can occur,
   * the least we can do is to make sure the system is in a safe state.
   * @author Rex Shang
   */
  private void recoverFromHardwareException()
  {
    try
    {
      //XXL Optical Stop Sensor - Anthony (May 2013)
      if (DigitalIo.isOpticalStopControllerInstalled())
      {
        _digitalIo.setEnableXXLOpticalStopController(false);
      }
      _stageBelts.off();
      if (isInLineModeEnabled())
      {
        _manufacturingEquipmentInterface.setXrayTesterIsReadyToAcceptPanel(false);
      }
    }
    catch (XrayTesterException xte)
    {
      // Do nothing.
    }
  }

  /**
   * Make information persistent in case of system power failure.
   * @throws DatastoreException
   * @author Rex Shang
   */
  void savePersistentPanelInformation() throws DatastoreException
  {
    if (_panelHandlerPersistentData == null)
    {
      _panelHandlerPersistentData = new PanelHandlerPersistentData();
    }
    _panelHandlerPersistentData.setPanelName(_projectName);
    _panelHandlerPersistentData.setPanelLoaded(_panelLoaded);
    _panelHandlerPersistentData.setPanelIsLoading(_panelIsLoading);
    _panelHandlerPersistentData.setWidthInNanometers(_panelWidthInNanoMeters);
    _panelHandlerPersistentData.setLengthInNanometers(_panelLengthInNanoMeters);
    _panelHandlerPersistentData.setPanelExtraClearDelayInMiliSeconds(_panelExtraClearDelayInMiliSeconds);

    if (_panelIsAgainst == null)
    {
      _panelHandlerPersistentData.setPanelIsAgainstLeftPanelInPlaceSensor(false);
      _panelHandlerPersistentData.setPanelIsAgainstRightPanelInPlaceSensor(false);
    }
    else if (_panelIsAgainst == _leftPanelInPlaceSensor)
    {
      _panelHandlerPersistentData.setPanelIsAgainstLeftPanelInPlaceSensor(true);
      _panelHandlerPersistentData.setPanelIsAgainstRightPanelInPlaceSensor(false);
    }
    else if (_panelIsAgainst == _rightPanelInPlaceSensor)
    {
      _panelHandlerPersistentData.setPanelIsAgainstLeftPanelInPlaceSensor(false);
      _panelHandlerPersistentData.setPanelIsAgainstRightPanelInPlaceSensor(true);
    }
    else
    {
      Assert.expect(false, "Panel location is invalid.");
    }

//    FileUtilAxi.saveObjectToSerializedFile(_panelHandlerPersistentData,
//            FileName.getPanelHandlerLoadedPanelConfigFullPath());
    
    FileUtilAxi.saveObjectToSerializedFile(_panelHandlerPersistentData,
            FileName.getPanelHandlerLoadedPanelConfigFullPath2());
  }

  /**
   * Retrive persistent data so we know the state PanelHandler is in.
   * @author Rex Shang
   */
  public void retrievePersistentPanelInformation()
  {
    try
    {
      if(FileUtilAxi.exists(FileName.getPanelHandlerLoadedPanelConfigFullPath2()))
      {
        _panelHandlerPersistentData = (PanelHandlerPersistentData) FileUtilAxi.loadObjectFromSerializedFile(
                FileName.getPanelHandlerLoadedPanelConfigFullPath2());
        
        _panelIsLoading = _panelHandlerPersistentData.isPanelLoading();
        _panelExtraClearDelayInMiliSeconds = _panelHandlerPersistentData.getPanelExtraClearDelayInMiliSeconds();
      }
      else
      {
        _panelHandlerPersistentData = (PanelHandlerPersistentData) FileUtilAxi.loadObjectFromSerializedFile(
                FileName.getPanelHandlerLoadedPanelConfigFullPath());
        
        _panelIsLoading = false;
        _panelExtraClearDelayInMiliSeconds = 0;
      }

      _projectName = _panelHandlerPersistentData.getPanelName();
      _panelLoaded = _panelHandlerPersistentData.isPanelLoaded();
      _panelWidthInNanoMeters = _panelHandlerPersistentData.getWidthInNanometers();
      _panelLengthInNanoMeters = _panelHandlerPersistentData.getLengthInNanometers();

      if (_panelHandlerPersistentData.isPanelIsAgainstLeftPanelInPlaceSensor()
              && _panelHandlerPersistentData.isPanelIsAgainstRightPanelInPlaceSensor() == false)
      {
        _panelIsAgainst = _leftPanelInPlaceSensor;
      }
      else if (_panelHandlerPersistentData.isPanelIsAgainstLeftPanelInPlaceSensor() == false
              && _panelHandlerPersistentData.isPanelIsAgainstRightPanelInPlaceSensor())
      {
        _panelIsAgainst = _rightPanelInPlaceSensor;
      }
    }
    catch (CannotOpenFileDatastoreException cofde)
    {
      // File is missing.  Assume panel is loaded.
      if (UnitTest.unitTesting() == false)
      {
        _panelLoaded = true;
        _panelIsLoading = false;
        System.out.println("ERROR caught: CannotOpenFileDatastoreException in retrievePersistentPanelInformation() function, setting _panelLoaded = true");
        _panelIsAgainst = null;
      }
    }
    catch (DatastoreException de)
    {
      // In case of corrupt data, we have to assume panel is loaded.
      _panelLoaded = true;
      _panelIsLoading = false;
      System.out.println("ERROR caught: DatastoreException in retrievePersistentPanelInformation() function, setting _panelLoaded = true");
      _panelIsAgainst = null;

      // Added by LeeHerng - Only display datastore exception message if it is not a unit test
      if (UnitTest.unitTesting() == false)
      {
        System.out.println("ERROR caught: DatastoreException in retrievePersistentPanelInformation() function, setting _panelLoaded = true");
      }
    }
  }

  /**
   * Used by initialize() to set the panel loading modes.
   * @author Rex Shang
   */
  private void initializePanelFlowDirectionAndLoadingMode() throws XrayTesterException
  {
    _flowDirection = getPanelFlowDirection();
    _loadingMode = getPanelLoadingMode();

    if (isPanelLoadingModeOk(_loadingMode) == false)
    {
      _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getInvalidPanelLoadingModeException());
    }

    if (isPanelFlowDirectionOk(_flowDirection) == false)
    {
      _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getInvalidPanelFlowDirectionException());
    }

    setupForPanelFlowDirectionAndLoadingMode();
  }

  /**
   * Close all barriers (include inner barrier).  If there is blockage, exception will be thrown.
   * @author Rex Shang
   */
  void closeAllBarriersInParallel() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> closeBarrierParallelTasks = new ExecuteParallelThreadTasks<Object>();

    //Variable Mag Anthony August 2011
    if (XrayActuator.isInstalled())
    {
      XrayActuatorInt xrayActuator = XrayActuator.getInstance();
      if (XrayCylinderActuator.isInstalled() == true)
      {
        xrayActuator.up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        xrayActuator.home(false,false);
      }
    }

    ThreadTask<Object> task1 = new CloseOuterBarrierThreadTask(_leftOuterBarrier);
    ThreadTask<Object> task2 = new CloseOuterBarrierThreadTask(_rightOuterBarrier);
    ThreadTask<Object> task3 = new CloseInnerBarrierThreadTask();

    // Start the tasks and wait.
    closeBarrierParallelTasks.submitThreadTask(task1);
    closeBarrierParallelTasks.submitThreadTask(task2);
    closeBarrierParallelTasks.submitThreadTask(task3);
    try
    {
      closeBarrierParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }
  
  /**
   * Close all outer barriers.  If there is blockage, exception will be thrown.
   * @author Rex Shang
   */
  public void closeAllOuterBarriersInParallel() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> closeBarrierParallelTasks = new ExecuteParallelThreadTasks<Object>();

    ThreadTask<Object> task1 = new CloseOuterBarrierThreadTask(_leftOuterBarrier);
    ThreadTask<Object> task2 = new CloseOuterBarrierThreadTask(_rightOuterBarrier);

    // Start the tasks and wait.
    closeBarrierParallelTasks.submitThreadTask(task1);
    closeBarrierParallelTasks.submitThreadTask(task2);
    try
    {
      closeBarrierParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  
  /**
   * Close all outer barriers.  If there is blockage, exception will be thrown.
   * Together With Motorized Height Level Sensor Adjustment InParallel
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void closeAllOuterBarriersWithMotorizedHeightLevelSensorAdjustmentInParallel(boolean isHighMag) throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> closeBarrierWithMotorizedHeightLevelSensorAdjustmentInParallelTasks = new ExecuteParallelThreadTasks<Object>();

    ThreadTask<Object> task1 = new CloseOuterBarrierThreadTask(_leftOuterBarrier);
    ThreadTask<Object> task2 = new CloseOuterBarrierThreadTask(_rightOuterBarrier);
    ThreadTask<Object> task3 = new AdjustMotorizedHeightLevelSensorThreadTask(isHighMag);

    // Start the tasks and wait.
    closeBarrierWithMotorizedHeightLevelSensorAdjustmentInParallelTasks.submitThreadTask(task1);
    closeBarrierWithMotorizedHeightLevelSensorAdjustmentInParallelTasks.submitThreadTask(task2);
    closeBarrierWithMotorizedHeightLevelSensorAdjustmentInParallelTasks.submitThreadTask(task3);
    try
    {
      closeBarrierWithMotorizedHeightLevelSensorAdjustmentInParallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * Open all outer barriers.  If there is blockage, exception will be thrown.
   * @author Rex Shang
   */
  private void prepareSystemForManualPanelRetrieval() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> parallelTasks = new ExecuteParallelThreadTasks<Object>();

    ThreadTask<Object> task1 = new OpenOuterBarrierThreadTask(_leftOuterBarrier);
    ThreadTask<Object> task2 = new OpenOuterBarrierThreadTask(_rightOuterBarrier);
    ThreadTask<Object> task3 = new OpenPanelClampsThreadTask();
    ThreadTask<Object> task4 = new RetractPanelInPlaceSensorThreadTask(_primaryPanelInPlaceSensor);
    ThreadTask<Object> task5 = new RetractPanelInPlaceSensorThreadTask(_secondaryPanelInPlaceSensor);

    // Start the tasks and wait.
    parallelTasks.submitThreadTask(task1);
    parallelTasks.submitThreadTask(task2);
    parallelTasks.submitThreadTask(task3);
    parallelTasks.submitThreadTask(task4);
    parallelTasks.submitThreadTask(task5);
    try
    {
      parallelTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Wei Chin
   */
  private void safetyLevelSensorForXXLSystem() throws XrayTesterException
  {   
    debug("safetyLevelSensorForXXLSystem");
    _leftSafetyLevelSensor.startMonitorStatus();
    _rightSafetyLevelSensor.startMonitorStatus();

    if (_loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      moveStageToOuterBarrier(_secondaryOuterBarrier);
      moveStageToOuterBarrier(_primaryOuterBarrier);
    }
    else
    {
      if (_primaryOuterBarrier == _leftOuterBarrier)
      {
        moveStageToOuterBarrier(_rightOuterBarrier);
      }
      else
      {
        moveStageToOuterBarrier(_leftOuterBarrier);
      }
      moveStageToOuterBarrier(_primaryOuterBarrier);
    }

    _leftSafetyLevelSensor.stopMonitorStatus();
    _rightSafetyLevelSensor.stopMonitorStatus();

    if (_leftSafetyLevelSensor.getLastSafetyStatus() == false
            || _rightSafetyLevelSensor.getLastSafetyStatus() == false)
    {
      _passedHighMagSafetyLevelSensorScan = false;
      // wei chin : need to clarify either throw first or unload 1st?
      _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
    }
    else
    {
      _passedHighMagSafetyLevelSensorScan = true;
    }
  }
  /**
   * @author Anthony Fong
   */
  private void safetyLevelSensorForS2EXSystem( boolean isHighMag) throws XrayTesterException
  {   
    debug("safetyLevelSensorForXXLSystem");
    _leftSafetyLevelSensor.startMonitorStatus();
    _rightSafetyLevelSensor.startMonitorStatus();

    if (_loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
    {
      moveStageToOuterBarrier(_secondaryOuterBarrier);
      //moveStageToOuterBarrier(_primaryOuterBarrier);
    }
    else
    {
      if (_primaryOuterBarrier == _leftOuterBarrier)
      {
        moveStageToOuterBarrier(_rightOuterBarrier);
      }
      else
      {
        moveStageToOuterBarrier(_leftOuterBarrier);
      }
      //moveStageToOuterBarrier(_primaryOuterBarrier);
    }

    _leftSafetyLevelSensor.stopMonitorStatus();
    _rightSafetyLevelSensor.stopMonitorStatus();

    if (_leftSafetyLevelSensor.getLastSafetyStatus() == false
            || _rightSafetyLevelSensor.getLastSafetyStatus() == false)
    {
      if(isHighMag)
      {
        _passedS2EXHighMagSafetyLevelSensorScan  = false;
      }
      else
      {
        _passedS2EXLowMagSafetyLevelSensorScan = false;
        //for double ssfe set high-mag scan to false as well
        _passedS2EXHighMagSafetyLevelSensorScan  = false;
      }
      // wei chin : need to clarify either throw first or unload 1st?
      _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
    }
    else
    {
      if(isHighMag)
      {
        _passedS2EXHighMagSafetyLevelSensorScan  = true;
        // skip low mag check if high mag pass height safety check
        _passedS2EXLowMagSafetyLevelSensorScan = true;
      }
      else
      {
        _passedS2EXLowMagSafetyLevelSensorScan = true;       
      }
    }
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  private int getDelayTimeFromLeftToRightOrRightToLeft()
  {
    return getConfig().getIntValue(HardwareConfigEnum.LEFT_TO_RIGHT_DELAY_TIME_IN_MILISECONDS);
  }

  /**
   * XXL Optical Stop Sensor - Anthony (May 2013)
   * move XYStage To Unload/Right Position
   * @author Anthony Fong
   */
  public void moveXYStageToRightUnloadPosition(boolean useHardwareWorkerThread) throws XrayTesterException
  {
    clearAbort();

    if (isAborting())
    {
      return;
    }
    if (XrayActuator.isInstalled())
    {
      XrayActuatorInt xrayActuator = XrayActuator.getInstance();
      if (XrayCylinderActuator.isInstalled() == true)
      {
        xrayActuator.up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        xrayActuator.home(false,false);
      }
    }
    closeAllOuterBarriersInParallel();

    if (isAborting())
    {
      return;
    }
    if (useHardwareWorkerThread)
    {
      HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
      {

        public void run()
        {
          try
          {
            moveStageToOuterBarrier(RightOuterBarrier.getInstance());
          }
          catch (XrayTesterException xte)
          {
            // do nothing
          }
        }
      });
    }
    else
    {
      moveStageToOuterBarrier(RightOuterBarrier.getInstance());
    }
  }

  /**
   * XXL Optical Stop Sensor - Anthony (May 2013)
   * move XYStage To Load/Left Position
   * @author Anthony Fong
   */
  public void moveXYStageToLeftLoadPosition(boolean useHardwareWorkerThread) throws XrayTesterException
  {
    clearAbort();

    if (isAborting())
    {
      return;
    }
    if (XrayActuator.isInstalled())
    {
      XrayActuatorInt xrayActuator = XrayActuator.getInstance();
      if (XrayCylinderActuator.isInstalled() == true)
      {
        xrayActuator.up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        xrayActuator.home(false,false);
      }
    }
    if (useHardwareWorkerThread)
    {
      closeAllOuterBarriersInParallel();
    }
    if (isAborting())
    {
      return;
    }
    if (useHardwareWorkerThread)
    {
      HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
      {

        public void run()
        {
          try
          {
            moveStageToOuterBarrier(LeftOuterBarrier.getInstance());
          }
          catch (XrayTesterException xte)
          {
            // do nothing
          }
        }
      });
    }
    else
    {
      moveStageToOuterBarrier(LeftOuterBarrier.getInstance());
    }
  }

  /**
   * XXL Optical Stop Sensor - Anthony (May 2013)
   * Check For Optical Sensor Controller Done Signal.
   * @author Anthony Fong
   */
  public void resetXXLOpticalStopController() throws XrayTesterException
  {
    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      // To reset XXL Optical Stop Controller if Stage Belt change from OFF to ON
      if (_digitalIo.areStageBeltsOn() == false)
      {
        _digitalIo.setEnableXXLOpticalStopController(false);
        try
        {
          Thread.sleep(50);
        }
        catch (InterruptedException ie)
        {
          // Do nothing.
        }
        _digitalIo.setEnableXXLOpticalStopController(true);
      }
    }
  }

  /**
   * XXL Optical Stop Sensor - Anthony (May 2013)
   * Check For Optical Sensor Controller Done Signal.
   * @author Anthony Fong
   */
  public boolean checkForOpticalSensorControllerDoneSignal() throws XrayTesterException
  {
    boolean doneSignalDetected = false;
    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled() && UnitTest.unitTesting() == false)
    {
      try
      {
        long timeToWaitInMilliSeconds = 2000;
        long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
        boolean continueCheck = true;
        while (continueCheck && (deadLineInMilliSeconds > System.currentTimeMillis()))
        {
          try
          {
            if (_digitalIo.isOpticalStopControllerDone())
            {
              doneSignalDetected = true;
              continueCheck = false;
            }
            Thread.sleep(0);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }
          finally
          {
          }
        }
        if (doneSignalDetected == false)
        {
          _secondaryOuterBarrier.open(); // panel should be at the secondary Outer Barrier
          _primaryOuterBarrier.open(); // open primary Outer Barrier for double check
          abort();
          //Panel may left inside!!!
          throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_INSIDE_MACHINE);
        }
      }
      catch (XrayTesterException xte)
      {
        recoverFromHardwareException();
        // Throw the original exception.
        throw xte;
      }
      finally
      {
      }

    }
    return doneSignalDetected;
  }
  
  /**
   * @author Chnee Khang Wah
   * @throws XrayTesterException 
   */
  public void checkPanelIsLoading() throws XrayTesterException
  {
    if(_panelIsLoading==true && _panelLoaded==false)
    {
      throw new PanelPositionerException(PanelPositionerExceptionEnum.POSSIBLE_PANEL_LOAD_UNSUCCESFUL);
    }
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public void resetPanelLoadingStatus()
  {
    _panelIsLoading = false;
    
    try
    {
      savePersistentPanelInformation();
    }
    catch (XrayTesterException xte)
    {
      //do nothing..
    }
  }
  
  /**
   * @author Swee Yee Wong
   * @return true if success to move to camera clear position, false if panel is
   * loaded and fail to skip away
   */
  public boolean moveStageToCameraClearPosition() throws XrayTesterException
  {
    // CR32071-Pantec 2.4" panel customization ==> by AnthonyFong on 28 August,2008
    // To enhance X6000 with minimum hardware and software modifications,
    // in order to inspect small panel (2.40 inches) for Pantec
    // by extending the adjustable rail 1.60" towards fix rail,
    // in order to get the smaller rail width of 2.40"
    //
    // Setting changes in Hardware.config for inspecting panels less than 4.00" as follow:
    // the max is lowered form 18.00" to 16.40" because new customization lowered the min width by 1.60"
    // the min is lowered form  4.00" to  2.40"
    // (Notes: actual numbers may be slightly different from system to system)
    // panelHandlerPanelMaximumWidthInNanometers=416560000 (16,400 mils)     Old (18,000 mils) 457200000
    // panelHandlerPanelMinimumWidthInNanometers=60960000 (2,400 mils)       Old (4,000 mils) 101600000
    // railWidthAxisMaximumPositionLimitInNanometers=419735000 (16,525 mils) Old (18,125 mils) 460375000
    // railWidthAxisMinimumPositionLimitInNanometers=60960000 (2,400 mils)   Old (4,000 mils) 101600000
    //
    // Rail width hard stop limit in nanometers before customization is 4 inches (101,600,000 nm)
    int optimumRailWidthWithoutSkipAwayInNanometers = getConfig().getIntValue(HardwareCalibEnum.CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS);

    StagePositionMutable stagePosition;
    stagePosition = new StagePositionMutable();
    // Extended adjustable rail will block X-ray source during CD&A and
    // CD&A will fail if the stage is at the existing center position
    // System need to move the stage to a new position from blocking X-ray source during CD&A
    // Check to activate the new skip away motion using the actual rail width
    int actualRailWidthInNanometers = getRailWidthInNanoMeters();
    if (actualRailWidthInNanometers < optimumRailWidthWithoutSkipAwayInNanometers)
    {
      // Check for hardware limit
      Assert.expect(optimumRailWidthWithoutSkipAwayInNanometers > getMinimumRailWidthInNanoMeters());

        // Define XY Stage Skip Away Positiion for CD&A
      // Acceptable X Range = 750 mils -- 10,000 mils ---19,000 mils
      // Acceptable Y Range =   0 mils --  3,800 mils --- 6,500 mils
      // Optimum skip away position = P( 10000 mils , 3,800 mils ) = P( 254000000 nm , 96520000 nm)
      stagePosition.setXInNanometers(getConfig().getIntValue(HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS));
      stagePosition.setYInNanometers(getConfig().getIntValue(HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS));
    }
    else
    {
      if (isPanelLoaded())
      {
        return false; //return false if panel loaded and fail to skip away - camera position can't be cleared
      }

      // Move stage to existing center position before performing CD&A
      // LP3 and beyond have redesigned stage allowing us to get all cameras at once
      // Define stage position #1
      stagePosition.setXInNanometers(getConfig().getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS));
      stagePosition.setYInNanometers(getConfig().getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS));
      
    }
    _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);
    
    return true;
  }
  
  /**
   * @author Swee-Yee.Wong - To check machine's hardware functioning
   */
  public void initializeSafetyChecking() throws XrayTesterException
  {
    if(isPanelLoaded() == false)
    {
      if(_panelClamps.areClampsClosed())
      {
        _panelClamps.open();
      }
      
      //check for left and right PIP extend function
      ExecuteParallelThreadTasks<XrayTesterException> parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();

      ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("Extend left panel in place sensor")
      {
        protected XrayTesterException executeTask() throws XrayTesterException
        {
          XrayTesterException exception = null;
          try
          {
            _leftPanelInPlaceSensor.extend();
          }
          catch (XrayTesterException xte)
          {
            exception = xte;
            throw xte;
          }
          return exception;
        }
      };

      ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("Extend right panel in place sensor")
      {
        protected XrayTesterException executeTask() throws XrayTesterException
        {
          XrayTesterException exception = null;
          try
          {
            _rightPanelInPlaceSensor.extend();
          }
          catch (XrayTesterException xte)
          {
            exception = xte;
            throw xte;
          }
          return exception;
        }
      };

      parallelThread.submitThreadTask(task1);
      parallelThread.submitThreadTask(task2);
      try
      {
        parallelThread.waitForTasksToComplete();
      }
      catch (XrayTesterException xex)
      {
        throw xex;
      }
      catch (Exception e)
      {
        Assert.logException(e);
      }
      
      //now check for left and right PIP retract function
      parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();

      ThreadTask<XrayTesterException> task3 = new ThreadTask<XrayTesterException>("Retract left panel in place sensor")
      {
        protected XrayTesterException executeTask() throws XrayTesterException
        {
          XrayTesterException exception = null;
          try
          {
            _leftPanelInPlaceSensor.retract();
          }
          catch (XrayTesterException xte)
          {
            exception = xte;
            throw xte;
          }
          return exception;
        }
      };

      ThreadTask<XrayTesterException> task4 = new ThreadTask<XrayTesterException>("Retract right panel in place sensor")
      {
        protected XrayTesterException executeTask() throws XrayTesterException
        {
          XrayTesterException exception = null;
          try
          {
            _rightPanelInPlaceSensor.retract();
          }
          catch (XrayTesterException xte)
          {
            exception = xte;
            throw xte;
          }
          return exception;
        }
      };

      parallelThread.submitThreadTask(task3);
      parallelThread.submitThreadTask(task4);
      try
      {
        parallelThread.waitForTasksToComplete();
      }
      catch (XrayTesterException xex)
      {
        throw xex;
      }
      catch (Exception e)
      {
        Assert.logException(e);
      }
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
  
  /**
   * To be run on a thread to poll for digital IO status.  In case of
   * board drop, we will generate event to notify user.(Mini)
   * @author Kok Chun, Tan
   */
  private void pollingSmemaSignal()
  {
    if (isSimulationModeOn())
      return;
    
    while (_logSMEMAConveyorSignal)
    {
      try
      {
        Thread.sleep(_SMEMA_SIGNAL_POLLING_INTERVAL_IN_MILLI_SECONDS);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
      
      try
      {
        if (isStartupRequired())
          _logSMEMAConveyorSignal = false;

        if (_logSMEMAConveyorSignal == false)
          return;
        
        if (getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH) && getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT) ||
            getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK) && getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT))
        {
          _smemaLogUtil.logSmemaConveyorSignalMessage("Downstream ready to accept panel, SMEMA Sensor 1, SMEMA Sensor 2, right panel clear sensor, right outer barrier close sensor, right outer barrier open: "+ 
                                                      _digitalIo.isSmemaDownstreamSystemReadyToAcceptPanel() + ", " + 
                                                      _digitalIo.isSmemaSensorOneTrigger() + ", " + 
                                                      _digitalIo.isSmemaSensorTwoTrigger() + ", " + 
                                                      _digitalIo.isRightPanelClearSensorBlocked() + ", " + 
                                                      _digitalIo.isRightOuterBarrierClosed() + ", " +
                                                      _digitalIo.isRightOuterBarrierOpen());
        }
        else
        {
          _smemaLogUtil.logSmemaConveyorSignalMessage("Downstream ready to accept panel, SMEMA Sensor 1, SMEMA Sensor 2, left panel clear sensor, left outer barrier close sensor, left outer barrier open: "+ 
                                                      _digitalIo.isSmemaDownstreamSystemReadyToAcceptPanel() + ", " + 
                                                      _digitalIo.isSmemaSensorOneTrigger() + ", " + 
                                                      _digitalIo.isSmemaSensorTwoTrigger() + ", " + 
                                                      _digitalIo.isLeftPanelClearSensorBlocked() + ", " + 
                                                      _digitalIo.isLeftOuterBarrierClosed() + ", " +
                                                      _digitalIo.isLeftOuterBarrierOpen());
        }
        
      }
      catch (XrayTesterException xte)
      {
        _hardwareObservable.notifyObserversOfException(xte);
        _logSMEMAConveyorSignal = false;
        _initialized = false;
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static boolean isCheckSmemaSensorSignalDuringUnloadingModeEnabled()
  {
    if (XrayTester.isS2EXEnabled() || XrayTester.isXXLBased())
      return getConfig().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE);
    else
      return false;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setCheckSmemaSensorSignalDuringUnloadingModeEnabled(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE, new Boolean(enabled));
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static boolean isPanelAvailableAfterDownStreamAcceptPanelModeEnabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setPanelAvailableAfterDownStreamAcceptPanelModeEnabled(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL, new Boolean(enabled));
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isDownstreamSameSideWithClosingOuterBarrier(BarrierInt outerBarrier)
  {
    Assert.expect(outerBarrier != null);
    
    if (isInLineModeEnabled())
    {
      if (((getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT) && getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK)) ||
          (getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT) && getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))) &&
          outerBarrier.equals(LeftOuterBarrier.getInstance()))
      {
        return true;
      }
      else if (((getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT) && getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH)) ||
               (getPanelFlowDirection().equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT) && getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))) &&
               outerBarrier.equals(RightOuterBarrier.getInstance()))
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void setProductionPanelLoadingTimeInMilis(long productionPanelLoadingTimeInMilis)
  {
    _productionPanelLoadingTimeInMilis = productionPanelLoadingTimeInMilis;   
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public long getProductionPanelLoadingTimeInMilis()
  {
    return _productionPanelLoadingTimeInMilis;  
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void setProductionPanelUnloadingTimeInMilis(long productionPanelUnloadingTimeInMilis)
  {
    _productionPanelUnloadingTimeInMilis = productionPanelUnloadingTimeInMilis;   
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public long getProductionPanelUnloadingTimeInMilis()
  {
    return _productionPanelUnloadingTimeInMilis;  
  }
  
  /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * @param keepOuterBarrierOpenAfterUnloadingPanel 
   */
  public void setKeepOuterBarrierOpenAfterUnloadingPanel(boolean keepOuterBarrierOpenAfterUnloadingPanel)
  {
    _keepOuterBarrierOpenAfterUnloadingPanel = keepOuterBarrierOpenAfterUnloadingPanel;
  }
  
  /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * @return 
   */
  public boolean isKeepOuterBarrierOpenForLoadingPanel()
  {
    return _keepOuterBarrierOpenForLoadingPanel;
  }
  
  /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * 
   */
  public void setKeepOuterBarrierOpenForLoadingPanel(boolean keepOuterBarrierOpenForLoadingPanel)
  {
    _keepOuterBarrierOpenForLoadingPanel = keepOuterBarrierOpenForLoadingPanel;
  }
  
   /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * 
   */
  public boolean isEnabledKeepOuterBarrierOpenForLoadingAndUnloadingPanel()
  {
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.KEEP_OUTER_BARRIER_OPEN_FOR_LOADING_UNLOADING_PANEL);
  }
  
   /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * 
   */
  public void setKeepOuterBarrierOpenForLoadingAndUnloadingPanel(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.KEEP_OUTER_BARRIER_OPEN_FOR_LOADING_UNLOADING_PANEL, new Boolean(enabled));
  }
  
   /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * 
   */
  public boolean isEnabledOuterBarrierOpenAfterCompleteScanningForS2EX()
  {
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.KEEP_OUTER_BARRIER_OPEN_AFTER_SCANNING_COMPLETE_S2EX);
  }
  
   /**
   * @author weng-jian eoh
   * @XCR-3758: Allow user to control outer barrier closing / opening during panel unloading in SMEMA
   * 
   */
  public void setKeepOuterBarrierOpenAfterCompleteScanningForS2EX(boolean enabled) throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.KEEP_OUTER_BARRIER_OPEN_AFTER_SCANNING_COMPLETE_S2EX, new Boolean(enabled));
  }
  
  public int getToleranceInNanometersForRailWidth() throws XrayTesterException
  {
    int toleranceInNanometers = _motionControl.getPositionAccuracyToleranceInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    
    return toleranceInNanometers;
  }
}
