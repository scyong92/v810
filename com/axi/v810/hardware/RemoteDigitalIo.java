package com.axi.v810.hardware;

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;

public class RemoteDigitalIo extends UnicastRemoteObject implements RemoteDigitalIoInterface
{
  protected static boolean _debug = false;

  // Map the remote calls to the native calls, wrapping the result and any
  // exception
  //
  public RemoteDigitalIo() throws RemoteException 
  {
      // do nothing
  }

  /**
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteInitialize() throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteInitialize");
    try
    {
      nativeInitialize();
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteInitialize): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteShutdown() throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteShutdown");
    try
    {
      nativeShutdown();
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteShutdown): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param hardwareResetModeOn
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetHardwareResetMode(boolean hardwareResetModeOn) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteSetHardwareResetMode");

    try
    {
      nativeSetHardwareResetMode(hardwareResetModeOn);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteSetHardwareResetMode): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param bitPosition
   * @param bitState
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public void remoteSetOutputBitState(int bitPosition, int bitState) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteSetOutputBitState");

    try
    {
      nativeSetOutputBitState(bitPosition, bitState);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteSetOutputBitState): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param bitPosition
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public int remoteGetInputBitState(int bitPosition) throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteGetInputBitState");
    try
    {
      return nativeGetInputBitState(bitPosition);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteGetInputBitState): " + ne.getMessage());
      throw ne;
    }
  }

  /**
   * @param key
   * @param parameters
   * @return
   * @throws RemoteException
   * @throws NativeHardwareException
   */
  public Serializable remoteGetConfigurationDescription(StringBuffer key, List<String> parameters)
          throws RemoteException, NativeHardwareException
  {
    vprint("RemoteDigitalIo.remoteInitialize");
    try
    {
      nativeGetConfigurationDescription(key, parameters);
    }
    catch(NativeHardwareException ne)
    {
      System.err.println("Native Error (remoteGetConfigurationDescription): " + ne.getMessage());
      throw ne;
    }
    return new HardwareConfigurationDescription(key.toString(), parameters);
  }

  // ///////////////////////////////////////////////////////////////////////////
  // Define Native Interface
  // ///////////////////////////////////////////////////////////////////////////
  private native void nativeInitialize() throws NativeHardwareException;

  private native void nativeShutdown() throws NativeHardwareException;

  private native void nativeSetHardwareResetMode(boolean hardwareResetModeOn) throws NativeHardwareException;

  private native void nativeSetOutputBitState(int bitPosition, int bitState) throws NativeHardwareException;

  private native int nativeGetInputBitState(int bitPosition) throws NativeHardwareException;

  private native void nativeGetConfigurationDescription(StringBuffer key, List<String> parameters)
      throws NativeHardwareException;

  /**
   * @param message
   */
  private void vprint(String message)
  {
    if(_debug)
      System.err.println(message);
  }

  /**
   * @author Khaw Chek Hau
   * XCR2183: Standardize all print out when developer debug is true
   */  
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @return
   * @throws RemoteException
   * @author Chong, Wei Chin
   */
  public String remoteGetApplicationVersion() throws RemoteException
  {
    vprint("RemoteDigitalIo.remoteGetApplicationVersion");
    return com.axi.v810.util.Version.getVersion();
  }
}