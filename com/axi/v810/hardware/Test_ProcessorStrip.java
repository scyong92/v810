package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Matt Wharton
 */
class Test_ProcessorStrip extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  public Test_ProcessorStrip()
  {
    // do nothing
  }


  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ProcessorStrip processorStrip = new ProcessorStrip(0);

    try
    {
      processorStrip.getRegion();
      Expect.expect( false );
    }
    catch( AssertException ae )
    {
      // do nothing
    }

    try
    {
      processorStrip.getReconstructionEngineId();
      Expect.expect( false );
    }
    catch( AssertException ae )
    {
      // do nothing
    }

    try
    {
      processorStrip.setRegion( null );
      Expect.expect( false );
    }
    catch( AssertException ae )
    {
      // do nothing
    }

    try
    {
      processorStrip.setReconstructionEngineId(null);
      Expect.expect( false );
    }
    catch( AssertException ae )
    {
      // do nothing
    }

    PanelRectangle processorStripRectangle = new PanelRectangle( 0, 0, 1, 1 );
    processorStrip.setRegion( processorStripRectangle );
    Expect.expect( processorStrip.getRegion().equals( processorStripRectangle ) );

    processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE3);
    Expect.expect(processorStrip.getReconstructionEngineId().equals(ImageReconstructionEngineEnum.IRE3));
  }

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ProcessorStrip() );
  }
}
