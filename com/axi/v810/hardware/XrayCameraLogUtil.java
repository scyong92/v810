package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraLogUtil
{
  private static final String _HEADER_SEPARATOR = "---------------------------------------------";
  private static final String _DATA_BLOCK_SPACE = " ";
  private FileLoggerAxi _logUtility;
  private boolean _enabled;
  private String _logDirectory;
  private String _logFileNameFullPath;


  /**
   * @author Greg Esparza
   */
  XrayCameraLogUtil(XrayCameraIdEnum id)
  {
    Assert.expect(id != null);

    Config config = Config.getInstance();
    _enabled = config.getBooleanValue(HardwareConfigEnum.XRAY_CAMERA_LOGGING_ENABLED);
    _logDirectory = Directory.getXrayCameraLogDir();
    _logFileNameFullPath = FileName.getXrayCameraLogFileNameFullPath(id.getId());
  }

  /**
   * @author Greg Esparza
   */
  public void log(String headerDescription, String data) throws XrayTesterException
  {
    if (_enabled)
    {
      if (FileUtilAxi.existsDirectory(_logDirectory) == false)
        FileUtilAxi.mkdir(_logDirectory);

      _logUtility = new FileLoggerAxi(_logFileNameFullPath);
      _logUtility.append(_DATA_BLOCK_SPACE);
      _logUtility.append(_HEADER_SEPARATOR);
      _logUtility.append(headerDescription);
      _logUtility.append(_HEADER_SEPARATOR);
      _logUtility.append(data);
    }
  }
}
