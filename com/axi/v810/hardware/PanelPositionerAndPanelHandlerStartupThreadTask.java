package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class PanelPositionerAndPanelHandlerStartupThreadTask extends ThreadTask<Object>
{
  private PanelPositioner _panelPositioner;
  private PanelHandler _panelHandler;

  /**
   * @author Bill Darbie
   */
  PanelPositionerAndPanelHandlerStartupThreadTask()
  {
    super("Start Panel Positioner And Panel Handler Thread Task");

    _panelPositioner = PanelPositioner.getInstance();
    _panelHandler = PanelHandler.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws Exception
  {
    _panelPositioner.startup();
    _panelHandler.startup();
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelPositioner.abort();
    _panelHandler.abort();
  }
}
