package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for the LightStack class
* @author Tony Turner
*/
public class Test_Interlock extends UnitTest
{
  private Interlock _interlock;

  public static void main(String[] args)
  {
    Test_Interlock testInterlock = new Test_Interlock();
    UnitTest.execute (testInterlock);
  }

  public void test(BufferedReader parm1, PrintWriter parm2)
  {
    _interlock = Interlock.getInstance();

    try
    {
      _interlock.startup();

      Assert.expect(_interlock.isStartupRequired() == false);

      _interlock.shutdown();

      Assert.expect(_interlock.isStartupRequired() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
