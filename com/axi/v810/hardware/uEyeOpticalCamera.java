package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Jack Hwee
 */
public class uEyeOpticalCamera extends AbstractOpticalCamera
{
    public static final int OPTICAL_IMAGE_CENTER_X = 376;
    public static final int OPTICAL_IMAGE_CENTER_Y = 240;
    public static final int OPTICAL_IMAGE_ROI_WIDTH = 10;
    public static final int OPTICAL_IMAGE_ROI_HEIGHT = 10;
    public static final int OPTICAL_IMAGE_WIDTH = 752;
    public static final int OPTICAL_IMAGE_HEIGHT = 480;
    public static final int OPTICAL_MAX_IMAGE_WIDTH = 752;
    public static final int OPTICAL_MAX_IMAGE_HEIGHT = 480;
    public static final int OPTICAL_ACTUAL_IMAGE_WIDTH_IN_NANOMETER = 27000000;  
    public static final int OPTICAL_ACTUAL_IMAGE_HEIGHT_IN_NANOMETER = 19000000;
    public static final int OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_WIDTH_IN_NANOMETER = 37000000;
    public static final int OPTICAL_LARGER_FOV_ACTUAL_MAX_IMAGE_HEIGHT_IN_NANOMETER = 31000000;
    
    public static final double OPTICAL_CAMERA_FRAMES_PER_SECOND = 60.0;
    
    private static final int _IS_SUCCESS    = 0;
    private static final int _IS_NO_SUCCESS = 0;
    
    private static long _IS_SET_GAINBOOST_ON    = 0x0000;
    private static long _IS_SET_GAINBOOST_OFF   = 0x0002;
    
    private static final int _JNI_INITIALIZE_TIME_IN_MILLISECONDS = 650;

    private static Map<OpticalCameraIdEnum, uEyeOpticalCamera> _cameraIdToInstanceMap;    
    private int _windowHandle = 0;
    
    private boolean _isHeightMapValueAvailable = false;
    private float[] _heightMapValue;
    
    private String _serialNumber;
    private String _firmwareVersion;
    private String _manufacturer;
    private String _finalQualityCheckDate;

    /**
     * Build camera Id list for later retrieval.
     * 
     * @author Cheah Lee Herng
     */
    static void setCameraIdList()
    {
        _config = Config.getInstance();
        _cameraIdToInstanceMap = new HashMap<OpticalCameraIdEnum, uEyeOpticalCamera>();
    }

    /**
     * Load native library.
     * 
     * @author Jack Hwee
     */
    static
    {
        System.loadLibrary("nativeUEye");
        System.loadLibrary("jawt");
        setCameraIdList();
    }

    /**
     * Retrieve an instance of uEyeOpticalCamera based on camera Id.
     * 
     * @author Cheah Lee Herng
     */
    static uEyeOpticalCamera getAnInstance(OpticalCameraIdEnum cameraId)
    {
        uEyeOpticalCamera instance = null;

        if (_cameraIdToInstanceMap.containsKey(cameraId))
            instance = _cameraIdToInstanceMap.get(cameraId);
        else
        {
            instance = new uEyeOpticalCamera(cameraId);
            _cameraIdToInstanceMap.get(cameraId);
        }

        Assert.expect(instance != null);
        return instance;
    }

    /**
     * uEyeOptical Camera constructor.
     * 
     * @author Jack Hwee
     */
    protected uEyeOpticalCamera(OpticalCameraIdEnum cameraId)
    {
        super(cameraId);

        Assert.expect(cameraId != null);        
        _cameraId = cameraId;
    }

    /**
     * Initialize uEyeOpticalCamera for later usage.
     * In order to initialize successfully, we need to pass in device id and 
     * window handle.
     * 
     * @throws OpticalCameraInitializeException
     * @author Jack Hwee
     */
    public synchronized void initialize() throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        
        int result = _IS_NO_SUCCESS;
        try
        {
            result = nativeInitialize(_cameraId.getId(), _windowHandle);            
            if (result != _IS_SUCCESS)
                throw OpticalCameraHardwareException.getAxiOpticalFailedToInitialize(_cameraId.getId(),
                                                                                     _cameraId.getDeviceId(), 
                                                                                     result,
                                                                                     uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                     "Failed to initialize optical camera");

        }
        catch (NativeHardwareException nhe)
        {
            OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToInitialize(_cameraId.getId(),
                                                                                                                                            _cameraId.getDeviceId(), 
                                                                                                                                            result,
                                                                                                                                            uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                            "Failed to initialize optical camera");
            opticalCameraHardwareException.initCause(nhe);
            _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
        }
    }

    /**
     * Perform start-up operation to make sure uEyeOpticalCamera operates normally,
     * including connectivity.
     * 
     * @author Cheah Lee Herng
     */
    public void startup() throws XrayTesterException
    {
        int numberOfRetries = 2;
        int retryCount = 1;

        clearAbort();

        try
        {
            do
            {
                setStartupBeginProgressState();
                _hardwareObservable.stateChangedBegin(this, OpticalCameraEventEnum.INITIALIZE);
                _hardwareObservable.setEnabled(false);
                _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.OPTICAL_CAMERA_INITIALIZE, getStartupTimeInMilliseconds());

                try
                {
                    if ((UnitTest.unitTesting() == false) &&  isSimulationModeOn())
                    {
                        _initialized = true;
                        return;
                    }
                    
                    if (isAborting())
                        return;
                    
                    initialize();
                    if (isAborting())
                        return;
                    
                    // Check if calibration directory exists
                    String opticalCalibrationFullPath = getOpticalCalibrationDirectoryFullPath();
                    if (FileUtilAxi.existsDirectory(opticalCalibrationFullPath) == false)
                    {
                      FileUtilAxi.createDirectory(opticalCalibrationFullPath);
                    }
                    
                    loadCalibrationData(opticalCalibrationFullPath);
                    if (isAborting())
                        return;
                    
                    _serialNumber = getSerialNumber();
                    if (isAborting())
                        return;
                    
                    _firmwareVersion = getFirmwareVersion();
                    if (isAborting())
                        return;
                    
                    _manufacturer = getManufacturer();
                    if (isAborting())
                        return;
                    
                    _finalQualityCheckDate = getFinalQualityCheckDate();
                    if (isAborting())
                        return;
                        
                    setConfigurationDescription();
                    if (isAborting())
                        return;

                    _initialized = true;
                }
                catch (OpticalCameraHardwareException he)
                {
                  handleCriticalErrors(he);

                  if (retryCount < numberOfRetries)
                  {
                    setStartupEndProgressState();
                  }
                  else
                    _hardwareTaskEngine.throwHardwareException(he);
                }

                ++retryCount;
            }
            while (_initialized == false);            
        }
        finally
        {
          setStartupEndProgressState();
          _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.OPTICAL_CAMERA_INITIALIZE);
          _hardwareObservable.setEnabled(true);
          _hardwareObservable.stateChangedEnd(this, OpticalCameraEventEnum.INITIALIZE);
        }
    }

    /**
     * Handle errors returned from camera native.
     * 
     * @author Cheah Lee Herng
     */
    private void handleCriticalErrors(OpticalCameraHardwareException he) throws XrayTesterException
    {
        Assert.expect(he != null);

        OpticalCameraHardwareExceptionEnum exceptionType = he.getExceptionType();

        if ((exceptionType.equals(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_INITIALIZE)))
           _hardwareTaskEngine.throwHardwareException(he);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void shutdown() throws XrayTesterException
    {
        _initialized = false;
        
        if (isSimulationModeOn() == false)
            exitCamera();
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        if (_initialized == false)
            return true;
        else
            return false;
    }

    /**
     * @author Cheah Lee Herng
     */
    private int getStartupTimeInMilliseconds()
    {
        int startupTimeInMilliseconds = _JNI_INITIALIZE_TIME_IN_MILLISECONDS;
        return startupTimeInMilliseconds;
    }

    /**
     * @author Jack Hwee
     */
    public synchronized void exitCamera() throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        
        int result = _IS_NO_SUCCESS;
        try
        {
            result = nativeExitCamera(_cameraId.getId());
            if (result != _IS_SUCCESS)
                throw OpticalCameraHardwareException.getAxiOpticalFailedToStop(_cameraId.getId(), 
                                                                               _cameraId.getDeviceId(), 
                                                                               result,
                                                                               uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                               "Failed to stop optical camera");
            _windowHandle = 0;
        }
        catch (NativeHardwareException nhe)
        {
            OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToStop(_cameraId.getId(), 
                                                                                                                                     _cameraId.getDeviceId(), 
                                                                                                                                     result,
                                                                                                                                     uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                     "Failed to stop optical camera");
            opticalCameraHardwareException.initCause(nhe);
            _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
        }
    }    

      /**
     * @author Jack Hwee
     */
    public synchronized void displayLiveVideo() throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        
        int result = _IS_NO_SUCCESS;
        try
        {
            result = nativeFreezeVideo(_cameraId.getId());
            result = nativeLiveVideo(_cameraId.getId());
            if (result != _IS_SUCCESS)
                throw OpticalCameraHardwareException.getAxiOpticalFailedToShowLiveVideo(_cameraId.getId(), 
                                                                                        _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        uEyeOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to display live video");
        }
        catch (NativeHardwareException nhe)
        {
            OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToShowLiveVideo(_cameraId.getId(),
                                                                                            _cameraId.getDeviceId(), 
                                                                                            result,
                                                                                            uEyeOpticalCameraStatusEnum.getStatusName(result), 
                                                                                            "Failed to display live video");
            opticalCameraHardwareException.initCause(nhe);
            _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
        }
    }

    /**
     * Get the JAWT window handle which needs to be used in uEyeOpticalCamera
     * API. This window handle needs to be used to draw on the canvas.
     * 
     * @author Cheah Lee Herng
     */
    public synchronized int getWindowHandle(java.awt.Component window) throws XrayTesterException
    {
        Assert.expect(window != null);

        int windowHandle = 0;
        try
        {
            windowHandle = nativeGetWindowHandle(window);
            _windowHandle = windowHandle;
        }
        catch (NativeHardwareException nhe)
        {
            OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetWindowHandle(_cameraId.getId(),
                                                                                            _cameraId.getDeviceId(), "Failed to get optical camera handle");
            opticalCameraHardwareException.initCause(nhe);
            _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
        }
        return windowHandle;
    }

   /**
     * Make uEyeOpticalCamera in hardware external trigger mode.
     * 
   * @author Jack Hwee
   */
  public synchronized void enableHardwareTrigger() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);

    int result = _IS_NO_SUCCESS;
    try
    {
      result = nativeEnableHardwareTrigger(_cameraId.getId());
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToEnableHardwareTrigger(_cameraId.getId(), 
                                                                                        _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        uEyeOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to enable hardware trigger");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToEnableHardwareTrigger(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                                "Failed to enable hardware trigger");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }

  /**
   * This function can only be called after enableHardwareTrigger() function.
   * When calling acquireOfflineImages, uEyeOpticalCamera will wait until it has
   * been triggered to capture image.
   * 
   * @author Jack Hwee
   */
  public synchronized void acquireOfflineImages(String fullPathFileName, boolean forceTrigger) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    Assert.expect(fullPathFileName != null);
    
    int result = _IS_NO_SUCCESS;  
    try
    {
      result = nativeAcquireOfflineImages(_cameraId.getId(), fullPathFileName, forceTrigger);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToAcquireOfflineImages(_cameraId.getId(), 
                                                                                       _cameraId.getDeviceId(), 
                                                                                        result,
                                                                                        uEyeOpticalCameraStatusEnum.getStatusName(result), 
                                                                                        "Failed to acquire offline images");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToAcquireOfflineImages(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                uEyeOpticalCameraStatusEnum.getStatusName(result), 
                                                                                                                                                "Failed to acquire offline images");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }

  /**
   * Display a frozen image.
   * 
   * @author Jack Hwee
   */
  public synchronized void freezeVideo() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);  
    int result = _IS_NO_SUCCESS;    
    try
    {
      result = nativeFreezeVideo(_cameraId.getId());
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToFreezeVideo(_cameraId.getId(), 
                                                                              _cameraId.getDeviceId(), 
                                                                               result,
                                                                               uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                               "Failed to freeze video");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToFreezeVideo(_cameraId.getId(),
                                                                                                                                      _cameraId.getDeviceId(), 
                                                                                                                                      result,
                                                                                                                                      uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                      "Failed to freeze video");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getNumberOfConnectedCameras() throws XrayTesterException
  {
    int numberOfConnectedCameras = 0;  
    try
    {
        numberOfConnectedCameras = nativeGetNumberOfCameras();        
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetNumberOfConnectedCameras("Failed to freeze video");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
    return numberOfConnectedCameras;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
      Assert.expect(_cameraId != null);
      return _cameraId;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void enableAutoGain(boolean enable) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
      
    int result = _IS_NO_SUCCESS;  
    try
    {
      result = nativeEnableAutoGain(_cameraId.getId(), enable);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoGain(_cameraId.getId(), 
                                                                                 _cameraId.getDeviceId(), 
                                                                                 result,
                                                                                 uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                 "Failed to enable auto gain");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoGain(_cameraId.getId(),
                                                                                                                                         _cameraId.getDeviceId(), 
                                                                                                                                         result,
                                                                                                                                         uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                         "Failed to enable auto gain");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void enableAutoSensorGain(boolean enable) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);  
      
    int result = _IS_NO_SUCCESS;  
    try
    {
      result = nativeEnableAutoSensorGain(_cameraId.getId(), enable);
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoSensorGain(_cameraId.getId(), 
                                                                                       _cameraId.getDeviceId(), 
                                                                                       result,
                                                                                       uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                       "Failed to enable auto sensor gain");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToEnableAutoSensorGain(_cameraId.getId(),
                                                                                                                                               _cameraId.getDeviceId(), 
                                                                                                                                               result,
                                                                                                                                               uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                               "Failed to enable auto sensor gain");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isGainBoostSupported() throws XrayTesterException
  {
    Assert.expect(_cameraId != null);
    
    boolean isGainBoostSupported = false;  
    try
    {
      int result = nativeIsGainBoostSupported(_cameraId.getId());
      if (result == _IS_SET_GAINBOOST_ON)
          isGainBoostSupported = true;
      else if (result == _IS_SET_GAINBOOST_OFF)
          isGainBoostSupported = false;
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetIsGainBoostSupported(_cameraId.getId(),
                                                                                            _cameraId.getDeviceId(), "Failed to get if gain boost supported");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
    return isGainBoostSupported;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void enableGainBoost(boolean enable) throws XrayTesterException
  {
    Assert.expect(_cameraId != null);  
      
    int result = _IS_NO_SUCCESS;  
    try
    {    
      if (enable)
          result = nativeSetGainBoostOn(_cameraId.getId());
      else
          result = nativeSetGainBoostOff(_cameraId.getId());      
      if (result != _IS_SUCCESS)
        throw OpticalCameraHardwareException.getAxiOpticalFailedToSetGainBoost(_cameraId.getId(), 
                                                                               _cameraId.getDeviceId(), 
                                                                               result,
                                                                               uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                               "Failed to set gain boost");
    }
    catch (NativeHardwareException nhe)
    {
      OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetGainBoost(_cameraId.getId(),
                                                                                                                                       _cameraId.getDeviceId(), 
                                                                                                                                       result,
                                                                                                                                       uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                       "Failed to set gain boost");
      opticalCameraHardwareException.initCause(nhe);
      _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized int getMasterGainFactor() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      int result = _IS_NO_SUCCESS;
      try
      {
         result = nativeGetMasterGainFactor(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
        OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetMasterGainFactor(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                                "Failed to get master gain factor");
        opticalCameraHardwareException.initCause(nhe);
        _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return result;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void setMasterGainFactor(int gainFactor) throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      int result = _IS_NO_SUCCESS;
      try
      {
         result = nativeSetMasterGainFactor(_cameraId.getId(), gainFactor);
         if (result != _IS_SUCCESS)
            throw OpticalCameraHardwareException.getAxiOpticalFailedToSetMasterGainFactor(_cameraId.getId(), 
                                                                                          _cameraId.getDeviceId(), 
                                                                                          result,
                                                                                          uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                          "Failed to set master gain factor");
      }
      catch (NativeHardwareException nhe)
      {
        OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetMasterGainFactor(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                                "Failed to set master gain factor");
        opticalCameraHardwareException.initCause(nhe);
        _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized String getFirmwareVersion() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      String firmwareVersion = "";
      try
      {
          firmwareVersion = nativeGetFirmwareVersion(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
          OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetFirmwareVersion(_cameraId.getId(),
                                                                                                                                                 _cameraId.getDeviceId(),
                                                                                                                                                 "Failed to get firmware version");
          opticalCameraHardwareException.initCause(nhe);
          _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return firmwareVersion;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized String getSerialNumber() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      String serialNumber = "";
      try
      {
          serialNumber = nativeGetSerialNumber(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
          OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetSerialNumber(_cameraId.getId(),
                                                                                                                                              _cameraId.getDeviceId(),
                                                                                                                                              "Failed to get serial number");
          opticalCameraHardwareException.initCause(nhe);
          _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return serialNumber;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized String getManufacturer() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      String manufacturer = "";
      try
      {
          manufacturer = nativeGetManufacturer(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
          OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetManufacturer(_cameraId.getId(),
                                                                                                                                              _cameraId.getDeviceId(),
                                                                                                                                              "Failed to get manufacturer");
          opticalCameraHardwareException.initCause(nhe);
          _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return manufacturer;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized String getFinalQualityCheckDate() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      String finalQualityCheckDate = "";
      try
      {
          finalQualityCheckDate = nativeGetFinalQualityCheckDate(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
          OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetFinalQualityCheckDate(_cameraId.getId(),
                                                                                                                                                       _cameraId.getDeviceId(),
                                                                                                                                                       "Failed to get final quality check");
          opticalCameraHardwareException.initCause(nhe);
          _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return finalQualityCheckDate;
  }
  
  /*
   * @author Cheah Lee Herng
   */
  public synchronized double getFramesPerSecond() throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      double framesPerSecond = 0.0;
      try
      {
          framesPerSecond = nativeGetFramesPerSecond(_cameraId.getId());
      }
      catch (NativeHardwareException nhe)
      {
          OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToGetFramesPerSecond(_cameraId.getId(),
                                                                                                                                                 _cameraId.getDeviceId(),
                                                                                                                                                 "Failed to get frames per second");
          opticalCameraHardwareException.initCause(nhe);
          _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
      return framesPerSecond;
  }
  
  /*
   * @author Cheah Lee Herng
   */
  public synchronized void setFramesPerSecond(double framesPerSecond) throws XrayTesterException
  {
      Assert.expect(_cameraId != null);
      
      int result = _IS_NO_SUCCESS;
      try
      {
         result = nativeSetFramesPerSecond(_cameraId.getId(), framesPerSecond);
         if (result != _IS_SUCCESS)
            throw OpticalCameraHardwareException.getAxiOpticalFailedToSetFramesPerSecond(_cameraId.getId(), 
                                                                                         _cameraId.getDeviceId(), 
                                                                                         result,
                                                                                         uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                         "Failed to set frames per second");
      }
      catch (NativeHardwareException nhe)
      {
        OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetFramesPerSecond(_cameraId.getId(),
                                                                                                                                                _cameraId.getDeviceId(), 
                                                                                                                                                result,
                                                                                                                                                uEyeOpticalCameraStatusEnum.getStatusName(result),
                                                                                                                                                "Failed to set frames per second");
        opticalCameraHardwareException.initCause(nhe);
        _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setConfigurationDescription()
  {
      List<String> parameters = new ArrayList<String>();
      
      parameters.add(_serialNumber);
      parameters.add(_firmwareVersion);
      parameters.add(_manufacturer);
      parameters.add(_finalQualityCheckDate);
      
      _configDescription.clear();
      _configDescription.add(new HardwareConfigurationDescription("HW_OPTICAL_CAMERA_CONFIGURATION_KEY", parameters));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setDebugMode(boolean status)
  {
    Assert.expect(_cameraId != null);
    nativeSetDebugMode(_cameraId.getId(), status);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized void performCalibration(OpticalCalibrationData opticalCalibrationData) throws XrayTesterException
  {
    Assert.expect(opticalCalibrationData != null);
    
    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
    {
      nativePerformCalibration(_cameraId.getId(), 
                                opticalCalibrationData.getKnownHeight1(), 
                                opticalCalibrationData.getKnownHeight2(), 
                                opticalCalibrationData.getCalibrationDirectoryFullPath());
    }
    else
    {
      nativePerformNewCalibration(_cameraId.getId(), 
                                  opticalCalibrationData.getKnownHeight1(), 
                                  opticalCalibrationData.getCalibrationDirectoryFullPath());
    }
  }
    
     /**
     * By default fullPathFileName is NULL, means no images will be saved.
     * @author Jack Hwee
     */
    public synchronized void acquireObjectFringeImages(String fullPathFileName, int planeId, boolean forceTrigger) throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        
        if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
          nativeAcquireObjectFringeImages(_cameraId.getId(), planeId, fullPathFileName, forceTrigger);
        else
          nativeAcquireCalibrationImages(_cameraId.getId(), planeId, fullPathFileName, forceTrigger);
    }
  
    /**
    * @author Jack Hwee
    */
  public synchronized void loadCalibrationData(String fullPath)
  {
    Assert.expect(_cameraId != null);
    Assert.expect(fullPath != null);

    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      nativeLoadCalibrationData(_cameraId.getId(), fullPath);
    else
      nativeLoadNewCalibrationData(_cameraId.getId(), fullPath);
  }
    
  /**
   * This function sets the required ROI information for height map.
   * 
   * @author Cheah Lee Herng
   * @author Jack Hwee
   */
  public synchronized void setInspectionROI(int roiCount, int[] centerX, int[] centerY, int[] width, int[] height)
  {
    Assert.expect(_cameraId != null);

    if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))  
      nativeSetInspectionROI(_cameraId.getId(), roiCount, centerX, centerY, width, height);
    else
      nativeSetNewInspectionROI(_cameraId.getId(), roiCount, centerX, centerY, width, height);
  }
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized void generateHeightMap(OpticalInspectionData opticalInspectionData, String fullPathFileName, boolean returnActualHeightMapValue) throws XrayTesterException
    {
      Assert.expect(_cameraId != null);
      Assert.expect(opticalInspectionData != null);
      
      if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      {
        _heightMapValue = nativeGenerateHeightMap(_cameraId.getId(), 
                                                  opticalInspectionData.getOpticalCalibrationProfileEnum().getHeightAdjustment(), 
                                                  fullPathFileName, 
                                                  opticalInspectionData.getOpticalCalibrationProfileEnum().getId(), 
                                                  opticalInspectionData.getForceTrigger());
        
        if (_heightMapValue.length == 0)
            _isHeightMapValueAvailable = false;
        else
            _isHeightMapValueAvailable = true;
      }
      else
      {
        _heightMapValue = nativeGenerateNewHeightMap(_cameraId.getId(), 
                                                     fullPathFileName, 
                                                     opticalInspectionData.getInspectionQualityThreshold(), 
                                                     opticalInspectionData.getRefLowerPhaseLimit(),
                                                     opticalInspectionData.getForceTrigger(),
                                                     returnActualHeightMapValue);
        
        if (_heightMapValue.length == 0)
            _isHeightMapValueAvailable = false;
        else
            _isHeightMapValueAvailable = true;
      }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public synchronized void generateOfflineHeightMap(com.axi.util.image.Image image,
                                                      com.axi.util.image.RegionOfInterest roi,
                                                      float floatAdjustment, 
                                                      int minusPlusSet) throws XrayTesterException
    {
      Assert.expect(image != null);
      Assert.expect(roi != null);
      Assert.expect(_cameraId != null);
        
      _heightMapValue = nativeGenerateOfflineHeightMap(_cameraId.getId(), 
                                                       floatAdjustment, 
                                                       minusPlusSet, 
                                                       image.getNativeDefinition(), 
                                                       roi.getMinX(), 
                                                       roi.getMinY(), 
                                                       roi.getWidth(), 
                                                       roi.getHeight());
      if (_heightMapValue.length == 0)
          _isHeightMapValueAvailable = false;
      else
          _isHeightMapValueAvailable = true;
    }
    
  /**
   * @author Jack Hwee
   */
  public boolean isHeightMapValueAvailable()
  {
    return _isHeightMapValueAvailable;
  }

  /**
   * @author Jack Hwee
   */
  public void setHeightMapValueAvailable(boolean isHeightMapValueAvailable)
  {
    _isHeightMapValueAvailable = isHeightMapValueAvailable;
  }

  /**
   * @author Jack Hwee
   */
  public float[] getHeightMapValue()
  {
    Assert.expect(_heightMapValue.length > 0);
    return _heightMapValue;
  }
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized float[] getNewCoordinateXInPixel() throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        return nativeGetNewCoordinateXInPixel(_cameraId.getId());
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized float[] getNewCoordinateYInPixel() throws XrayTesterException
    {
        Assert.expect(_cameraId != null);
        return nativeGetNewCoordinateYInPixel(_cameraId.getId());
    }
    
     /**
     * @author Jack Hwee
     */
    public synchronized void initializePspAlgorithm()
    {
       Assert.expect(_cameraId != null);
       nativeInitializePspAlgorithm(_cameraId.getId());
    }
    
  /**
   *  Provide magnification data to PSP Algorithm library so that when
   *  performing new point adjustment, it knows how to compensate back to the 
   *  correct plane.
   * 
   * @author Cheah Lee Herng
   */
  public synchronized void setMagnificationData(double[] magnificationData) throws XrayTesterException
  {
      Assert.expect(magnificationData.length == 12);    // We expect array size of 12
      
      int result = _IS_NO_SUCCESS;
      try
      {
          result = nativeSetMagnificationData(_cameraId.getId(), magnificationData);
          if (result != _IS_SUCCESS)
              throw OpticalCameraHardwareException.getAxiOpticalFailedToSetMagnificationData(_cameraId.getId(), 
                                                                                             _cameraId.getDeviceId(),
                                                                                             "Failed to set magnification data");
      }
      catch (NativeHardwareException nhe)
      {
        OpticalCameraHardwareException opticalCameraHardwareException = OpticalCameraHardwareException.getAxiOpticalFailedToSetMagnificationData(_cameraId.getId(),
                                                                                                                                                 _cameraId.getDeviceId(),
                                                                                                                                                 "Failed to set magnification data");
        opticalCameraHardwareException.initCause(nhe);
        _hardwareTaskEngine.throwHardwareException(opticalCameraHardwareException);
      }
  }
  
  /**
   * @author Jack Hwee
  */
  public synchronized void setStopLiveVideoBoolean(boolean bool) 
  {
      //do nothing
  }    
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isTriggerModeReady()
  {
    return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setCropImageOffset(int frontModuleOffsetXInPixel, int rearModuleOffsetXInPixel)
  {
    // Do nothing
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setImageSizeInPixel(int imageWidthInPixel, int imageHeightInPixel)
  {
    // Do nothing
  }

  private native int nativeInitialize(int cameraDeviceId, int windowHandle) throws NativeHardwareException;  
  private native int nativeLiveVideo(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeGetWindowHandle(java.awt.Component window) throws NativeHardwareException;
  private native int nativeExitCamera(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeEnableHardwareTrigger(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeAcquireOfflineImages(int cameraDeviceId, String pathName, boolean forceTrigger) throws NativeHardwareException;
  private native int nativeFreezeVideo(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeGetNumberOfCameras() throws NativeHardwareException;
  private native int nativeEnableAutoGain(int cameraDeviceId, boolean enable) throws NativeHardwareException;
  private native int nativeEnableAutoSensorGain(int cameraDeviceId, boolean enable) throws NativeHardwareException;
  private native int nativeIsGainBoostSupported(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeSetGainBoostOn(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeSetGainBoostOff(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeGetMasterGainFactor(int cameraDeviceId) throws NativeHardwareException;
  private native int nativeSetMasterGainFactor(int cameraDeviceId, int gainFactor) throws NativeHardwareException;
  private native static boolean nativeSetDebugMode(int cameraDeviceId, boolean status);
  private native static int nativeAcquireObjectFringeImages(int cameraDeviceId, int planeId, String pathName, boolean forceTrigger);
  private native static int nativeAcquireCalibrationImages(int cameraDeviceId, int planeId, String pathName, boolean forceTrigger);
  private native static boolean nativePerformCalibration(int cameraDeviceId, double referencePlaneKnownHeight, double objectPlanePlusThreeKnownHeight, String fullPath);
  private native static boolean nativePerformNewCalibration(int cameraDeviceId, double calibrationKnownHeight, String fullPath);
  private native static boolean nativeLoadCalibrationData(int cameraDeviceId, String fullPath);
  private native static boolean nativeLoadNewCalibrationData(int cameraDeviceId, String fullPath);
  private native static boolean nativeSetInspectionROI(int cameraDeviceId, int roiCount, int[] centerX, int[] centerY, int[] width, int[] height);
  private native static boolean nativeSetNewInspectionROI(int cameraDeviceId, int roiCount, int[] centerX, int[] centerY, int[] width, int[] height);
  private native static float[] nativeGenerateHeightMap(int cameraDeviceId, float floatAdjustment, String fullPathFileName, int minusPlusSet, boolean forceTrigger);
  private native static float[] nativeGenerateNewHeightMap(int cameraDeviceId, String fullPathFileName, int inspQualityThreshold, int refLowerPhaseLimit, boolean forceTrigger, boolean returnActualHeightMapValue);
  private native static float[] nativeGenerateOfflineHeightMap(int cameraDeviceId, float floatAdjustment, int minusPlusSet, int imageDefinition[], int roiX, int roiY, int roiWidth, int roiHeight);
  private native static void nativeInitializePspAlgorithm(int cameraDeviceId);
  private native static String nativeGetFirmwareVersion(int cameraDeviceId) throws NativeHardwareException;
  private native static String nativeGetSerialNumber(int cameraDeviceId) throws NativeHardwareException;
  private native static String nativeGetManufacturer(int cameraDeviceId) throws NativeHardwareException;
  private native static String nativeGetFinalQualityCheckDate(int cameraDeviceId) throws NativeHardwareException;
  private native static float[] nativeGetNewCoordinateXInPixel(int cameraDeviceId);
  private native static float[] nativeGetNewCoordinateYInPixel(int cameraDeviceId);
  private native static double nativeGetFramesPerSecond(int cameraDeviceId) throws NativeHardwareException;
  private native static int nativeSetFramesPerSecond(int cameraDeviceId, double framesPerSecond) throws NativeHardwareException;
  private native static int nativeSetMagnificationData(int cameraDeviceId, double[] magnificationData) throws NativeHardwareException;  
}
