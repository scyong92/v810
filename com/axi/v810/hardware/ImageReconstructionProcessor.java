package com.axi.v810.hardware;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class ImageReconstructionProcessor
    extends HardwareObject
    implements HardwareStartupShutdownInt
{
  private static Map<ImageReconstructionEngineEnum, ImageReconstructionProcessor> _imageReconstructionProcessorsMap;
  private static boolean _debug;
  private static String _me = "ImageReconstructionProcessor";

  private static final String _imageReconstructionProcessorApplicationFile;
  private static final String _COMMAND_LINE_OPTION_TO_SAVE_PROJECTIONS = "-saveProjections";
  private static boolean _saveProjections = false;

  private ImageReconstructionProcessorEnum _id;

  private static String _remoteFileSeperator = "\\";
  private static String _localFilePath;
  private static String _remoteFilePath;
  // Remote server has different root path...
  private static String _remoteFilePathRelativeToUploadRoot;
  private static final boolean _FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK;

  private List<com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine> _imageReconstructionEngines;
  private static List<ImageReconstructionProcessor> _irps = null;
  private static Map<ImageReconstructionProcessorEnum, List<InetAddress>> _idToAddressMap = null;

  private ImageReconstructionService _imageReconstructionService;
  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;

  private boolean _rebootNecessary = false;

  private static int _numberOfImageReconstructionNetworkCards = -1;
  private List<InetAddress> _inetAddresses;

  private static boolean _isLocal = false;
    
  /**
   * @author Rex Shang
   */
  static
  {
    _debug = getConfig().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
    _FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK = getConfig().getBooleanValue(SoftwareConfigEnum.FORCE_UPLOAD_FOR_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP);
    _numberOfImageReconstructionNetworkCards = getConfig().getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSOR_NETWORK_CARDS);

    _irps = new ArrayList<ImageReconstructionProcessor>();

    _imageReconstructionProcessorsMap = new HashMap<ImageReconstructionEngineEnum, ImageReconstructionProcessor>();
    _idToAddressMap = new HashMap<ImageReconstructionProcessorEnum,List<InetAddress>>();

    _imageReconstructionProcessorApplicationFile = FileName.getImageReconstructionProcessorApplicationFile();

    _localFilePath = Directory.getLocalImageReconstructionProcessorBinDir();
    _remoteFilePath = Directory.getRemoteImageReconstructionProcessorBinDir();

    // Because remote server has different root path...
    _remoteFilePathRelativeToUploadRoot = Directory.getRemoteImageReconstructionProcessorRelativeBinDir();
    
    System.loadLibrary("nativeIRP");
  }

  /**
   * Constructor
   * @author Rex Shang
   */
  private ImageReconstructionProcessor(ImageReconstructionProcessorEnum id)
  {
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();

    _id = id;
    _inetAddresses = getHostIpAddresses(_id);
    _isLocal = isIrpInLocalServer(_inetAddresses);

    _imageReconstructionEngines = new ArrayList<com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine>();
    _imageReconstructionService = new ImageReconstructionService(this);
  }

  /**
   * @author Rex Shang
   */
  public synchronized static ImageReconstructionProcessor getInstance(ImageReconstructionEngineEnum ireId)
  {
    ImageReconstructionProcessor irp = _imageReconstructionProcessorsMap.get(ireId);

    if (irp == null)
    {
      // Not in the map.  That mean we don't have a direct association with
      // the IRE id.  But we need to go through all of the existing instance
      // to see whether it is just a matter of association.
      com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire
          = com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine.getInstance(ireId);

      for (ImageReconstructionProcessor irp2 : _irps)
      {
        if (ire.getHostIrpId().equals(irp2.getId()))
        {
          irp = irp2;
          break;
        }
      }

      if (irp == null)
      {
        // Not in the instance list.  Need to create...
        irp = new ImageReconstructionProcessor(ire.getHostIrpId());
        _irps.add(irp);
      }

      irp.associateImageReconstructionEngine(ire);
      _imageReconstructionProcessorsMap.put(ireId, irp);
    }

    Assert.expect(irp != null);
    return irp;
  }

  /**
   * @author Rex Shang
   */
  private void associateImageReconstructionEngine(com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire)
  {
    if (_imageReconstructionEngines.contains(ire) == false)
      _imageReconstructionEngines.add(ire);
  }

  /**
   * @author Rex Shang
   */
  ImageReconstructionProcessorEnum getId()
  {
    return _id;
  }

  /**
   * @author Rex Shang
   */
  public static List<InetAddress> getHostIpAddresses(ImageReconstructionProcessorEnum irpId)
  {
    List<InetAddress> addresses = _idToAddressMap.get(irpId);

    if (addresses == null)
    {
      addresses = new ArrayList<InetAddress>();
      _idToAddressMap.put(irpId, addresses);

      try
      {
        if (irpId.equals(ImageReconstructionProcessorEnum.IRP0))
        {
          addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_1)));
          if (_numberOfImageReconstructionNetworkCards > 1)
          {
            addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_2)));
          }
        }
        else if (irpId.equals(ImageReconstructionProcessorEnum.IRP1))
        {
          addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_1)));
          if (_numberOfImageReconstructionNetworkCards > 1)
          {
            addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_2)));
          }
        }
        else if (irpId.equals(ImageReconstructionProcessorEnum.IRP2))
        {
          addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_1)));
          if (_numberOfImageReconstructionNetworkCards > 1)
          {
            addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_2)));
          }
        }
        else if (irpId.equals(ImageReconstructionProcessorEnum.IRP3))
        {
          addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_1)));
          if (_numberOfImageReconstructionNetworkCards > 1)
          {
            addresses.add(InetAddress.getByName(getConfig().getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_2)));
          }
        }
        else
        {
          Assert.expect(false, "Invalid ImageReconstructionProcessorEnum");
        }
      }
      catch (UnknownHostException e)
      {
        Assert.expect(false, "Undefined ImageReconstructionProcessor host address: " + e.getMessage());
      }
    }

    Assert.expect(addresses.size() >= 1);
    return addresses;
  }

  /**
   * @author Rex Shang
   */
  List<InetAddress> getHostIpAddresses()
  {
    Assert.expect(_inetAddresses != null);
    return _inetAddresses;
  }

  /**
   * @author Rex Shang
   */
  InetAddress getHostIpAddress()
  {
    Assert.expect(_inetAddresses != null);
    Assert.expect(_inetAddresses.size() > 0);
    return _inetAddresses.get(0);
  }

  /**
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    initialize();
  }

  /**
   * @author Rex Shang
   */
  public void shutdown() throws XrayTesterException
  {
    stopImageReconstructionProcessor();
  }

  /**
   * @author Rex Shang
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    boolean isStartupRequired = isImageReconstructionProcessorRunning() == false;
    return isStartupRequired;
  }

  /**
   * @author Rex Shang
   */
  private void initialize() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSOR);
    _hardwareObservable.setEnabled(false);

    try
    {
      int tryCount = 1;
      // Try 2 times (1, 2).
      final int maxTryCount = 2;
      boolean initializationOk = false;

      while (tryCount <= maxTryCount && initializationOk == false)
      {
        try
        {
          // Skip reboot as part of normal procedure because it is very slow.
          if (_rebootNecessary)
          {
            rebootImageReconstructionProcessor();
            _rebootNecessary = false;
          }

          // Stop the IRP if it is already running.
          stopImageReconstructionProcessor();
          if (_FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK && _isLocal == false)
            updateApplicationFiles();
          startImageReconstructionProcessor();

          // Verify IRP, resolve any inconsistency.
          verifyImageReconstructionProcessor();
          initializationOk = true;
        }
        catch (com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionProcessorCommunicationException irpce)
        {
          //debug(irpce.getLocalizedMessage());
          _rebootNecessary = true;
          // No need to reboot the irp...
          if (tryCount >= maxTryCount)
            throw irpce;
          else
            _rebootNecessary = false;
        }
        catch (ImageReconstructionProcessorHardwareException irphe)
        {
          debug(irphe.getLocalizedMessage());
          _rebootNecessary = true;
          if (tryCount >= maxTryCount)
            throw irphe;
          else if (irphe.getType().equals(ImageReconstructionProcessorHardwareExceptionEnum.IMAGE_RECONSTRUCTION_APPLICATION_VERSION_MISMATCH))
          {
            // If we get version mismatch exception, it means the newly uploaded file
            // still mismatches.  We don't need to bother with retry.
            _rebootNecessary = false;
            throw irphe;
          }
          else if (irphe.getType().equals(ImageReconstructionProcessorHardwareExceptionEnum.FAILED_TO_START_IMAGE_RECONSTRUCTION_APPLICATION))
          {
            // Possible file corruption on the IRP, update the files.
            stopImageReconstructionProcessor();
            if (_isLocal == false)
              updateApplicationFiles();
            startImageReconstructionProcessor();
            _rebootNecessary = false;
          }
        }
        catch (ICMPPacketHardwareException ihe)
        {
          // MTU size prolem, no need to reboot, retry.
          _rebootNecessary = false;
          throw ihe;
        }
        catch (XrayTesterException xte)
        {
          debug(xte.getLocalizedMessage());
          _rebootNecessary = true;
          if (tryCount >= maxTryCount)
            throw xte;
        }

        tryCount++;
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSOR);
  }

  /**
   * Go through the various process to verify the IRP is what we are expecting.
   * @author Rex Shang
   */
  private void verifyImageReconstructionProcessor() throws XrayTesterException
  {
    verifyCommunication();
    verifyApplication();
    // Put other verification here.
  }

  /**
   * @author Rex Shang
   */
  private void verifyCommunication() throws XrayTesterException
  {
    for (InetAddress ipAddress : _inetAddresses)
      ICMPPacket.getInstance().checkRemoteHostIsUsingAgilentJumboPacket("IRP " + getId().toString(), ipAddress.getHostAddress());
  }

  /**
   * Verify that the application matches.  If not, try update.
   * @author Rex Shang
   */
  private void verifyApplication() throws XrayTesterException
  {
    try
    {
      verifyApplicationVersion();
    }
    catch (ImageReconstructionProcessorHardwareException irphe)
    {
      if (irphe.getType().equals(ImageReconstructionProcessorHardwareExceptionEnum.IMAGE_RECONSTRUCTION_APPLICATION_VERSION_MISMATCH))
      {
        // Version mismatch, try update the files then check again.
        stopImageReconstructionProcessor();
        if(_isLocal == false)
          updateApplicationFiles();
        startImageReconstructionProcessor();
        verifyApplicationVersion();
      }
      else
        throw irphe;
    }
  }

  /**
   * @author Rex Shang
   */
  private void verifyApplicationVersion() throws XrayTesterException
  {
    for (com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      ire.establishCommunicationPathsToImageReconstructionProcessor();

      ImageReconstructionProcessorApplicationVersion irpVersionInfo = ire.getImageReconstructionEngineVersionInfo();
      if (irpVersionInfo.getVersionId() != getExpectedApplicationVersion())
      {
        if (_FORCE_UPLOAD_AND_BYPASS_VERSION_CHECK == false)
        {
          HardwareException he = ImageReconstructionProcessorHardwareException.getApplicationVersionMismatchException();
          _hardwareTaskEngine.throwHardwareException(he);
        }
      }
    }
  }

  /**
   * change the version here
   * @author Rex Shang
   */
  private int getExpectedApplicationVersion()
  {
//    int version = 4;
    int version = nativeGetImageReconstructionProcessorApplicationVersion();
    return version;
  }

  /**
   * @author George A. David
   */
  private static boolean isImageReconstructionProcessorsStartupDisabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.DISABLE_IMAGE_RECONSTRUCTION_PROCESSORS_STARTUP);
  }

  /**
   * @author Dave Ferguson
   */
  public static void setSaveImageReconstructionProjections(boolean saveProjections)
  {
    _saveProjections = saveProjections;
  }

  /**
   * @author George A. David
   */
  public static boolean isSavingImageReconstructionProjections()
  {
    return _saveProjections;
  }

  /**
   * @author Rex Shang
   */
  void startImageReconstructionProcessor() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.STARTING_IMAGE_RECONSTRUCTION_PROCESSOR);
    _hardwareObservable.setEnabled(false);

    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        // In case we don't have all of the irp software running, let's stop all of them first.
        stopImageReconstructionProcessor();

        // Ok, let's start up the irp software.
        if (isImageReconstructionProcessorRunning() == false)
        {
          // BeeHoon- Get the bit size of IRP.exe once the IRP is going to start.
          Config.checkIs64BitsIRP(); 
          
          for (com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire : _imageReconstructionEngines)
          {
            // Amount of physical memory to reserve for non-IRP processes and the operating system.
            long megaByteToByte = 1024 * 1024;
            long reservedPhysicalMemory = ((long)Config.getInstance().getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_RESERVED_PHYSICAL_MEMORY_IN_MEGABYTES) * megaByteToByte);
            long processExtraVirtualMemory = ((long)Config.getInstance().getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_EXTRA_VIRTUAL_MEMORY_IN_MEGABYTES) * megaByteToByte);
            long processExtraWorkingSetMemory = ((long)Config.getInstance().getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_EXTRA_WORKING_SET_MEMORY_IN_MEGABYTES) * megaByteToByte);
            long processMinimumWorkingSetMemory = ((long)Config.getInstance().getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_PROCESS_MINIMUM_WORKING_SET_MEMORY_IN_MEGABYTES) * megaByteToByte);
            String arguments = "-port " + ire.getPort() +
                               " -reservedPhysicalMemory " + reservedPhysicalMemory +
                               " -processExtraVirtualMemory " + processExtraVirtualMemory +
                               " -processExtraWorkingSet " + processExtraWorkingSetMemory +
                               " -processMinimumWorkingSet " + processMinimumWorkingSetMemory;
            String additionalEnv = "";
            String pathExtension = "";
            if (isSavingImageReconstructionProjections())
              arguments += " " + _COMMAND_LINE_OPTION_TO_SAVE_PROJECTIONS;
            
            // Single Server - Start IRP program by using local file path
            if (_isLocal)
              _imageReconstructionService.startProgram(_localFilePath,  
                                                       _imageReconstructionProcessorApplicationFile,
                                                       arguments,
                                                       additionalEnv,
                                                       pathExtension);
            else
              _imageReconstructionService.startProgram(_remoteFilePath,  
                                                       _imageReconstructionProcessorApplicationFile,
                                                       arguments,
                                                       additionalEnv,
                                                       pathExtension);
          }
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.STARTING_IMAGE_RECONSTRUCTION_PROCESSOR);
  }

  /**
   * @author Rex Shang
   */
  void stopImageReconstructionProcessor() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.STOPPING_IMAGE_RECONSTRUCTION_PROCESSOR);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Give IRE a chance to close all its network connections.
      for (com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire : _imageReconstructionEngines)
      {
        ire.closeConnections();
      }

      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        //ok, let's shutdown up the irp software
        if (_imageReconstructionService.getNumberOfProgramRunning("", _imageReconstructionProcessorApplicationFile) > 0)
        {
          _imageReconstructionService.stopProgram("", _imageReconstructionProcessorApplicationFile);
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.STOPPING_IMAGE_RECONSTRUCTION_PROCESSOR);
  }

  /**
   * @author George A. David
   */
  void restartImageReconstructionProcessor() throws XrayTesterException
  {
    stopImageReconstructionProcessor();
    startImageReconstructionProcessor();
  }

  /**
   * @author George A. David
   */
  void rebootImageReconstructionProcessor() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ImageReconstructionProcessorEventEnum.REBOOTING_IMAGE_RECONSTRUCTION_PROCESSOR);
    _hardwareObservable.setEnabled(false);

    try
    {
      if (isSimulationModeOn() == false && isImageReconstructionProcessorsStartupDisabled() == false)
      {
        for (com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionEngine ire : _imageReconstructionEngines)
        {
          if (_isLocal == false)
            _imageReconstructionService.restartHardware();
        }      
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, ImageReconstructionProcessorEventEnum.REBOOTING_IMAGE_RECONSTRUCTION_PROCESSOR);
  }

  /**
   * @author Rex Shang
   */
  boolean isImageReconstructionProcessorRunning() throws XrayTesterException
  {
    int numberOfProgram = _imageReconstructionService.getNumberOfProgramRunning("", _imageReconstructionProcessorApplicationFile);

    return (numberOfProgram >= _imageReconstructionEngines.size());
  }

  /**
   * Stop IRP, update, then start IRP.
   * @author Rex Shang
   */
  private void updateApplicationFiles() throws XrayTesterException
  {
    TftpClientAxi client = new TftpClientAxi(getHostIpAddress().getHostAddress(), TftpClientAxiCommunicationModeEnum.TFTP_BINARY_MODE);
    Collection<String> fileNames = getApplicationFileNames();

    client.open();

    // Clean the destination directory.
    for (String fileName : fileNames)
      client.sendFile(_localFilePath + File.separator + FileName.getNearlyEmptyIrpUploadFile(),
                      _remoteFilePathRelativeToUploadRoot + _remoteFileSeperator + fileName);

    // Now upload the real files.
    for (String fileName : fileNames)
      client.sendFile(_localFilePath + File.separator + fileName,
                      _remoteFilePathRelativeToUploadRoot + _remoteFileSeperator + fileName);
    client.close();
  }

  /**
   * @author Rex Shang
   */
  private static Collection<String> getApplicationFileNames()
  {
    Collection<String> files = FileUtil.listAllFilesInDirectory(_localFilePath);
    return files;
  }

  /**
   * @author Rex Shang
   */
  private void debug(String string)
  {
    if (_debug)
      System.out.println(_me + " " + getHostIpAddress().getHostAddress() + ": " + string);
  }
  
  /**
   * @author Bee Hoon
   */
  public static boolean isIrpInLocalServer(List<InetAddress> irpInetAddresses)
  {
    try 
    {
      final Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();
      
      while (enumNI.hasMoreElements())
      {
        final NetworkInterface ifc = enumNI.nextElement();
        if(ifc.isUp())
        {
          final Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
          while (enumAdds.hasMoreElements())
          {
            final InetAddress address = enumAdds.nextElement();  
            if(irpInetAddresses.contains(address))
            {
              return true;
            }     
          }
        }
      }
    } 
    catch (SocketException ex) 
    {
      System.out.println("Warning: Single Server Network Interface: " + ex);
    }
    
    return false;
  }

  ////////////////////////////////////////////////////////////////////////////
  // Define Native Interface
  ////////////////////////////////////////////////////////////////////////////
  private native int nativeGetImageReconstructionProcessorApplicationVersion();
}
