package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class PanelPositionerEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static PanelPositionerEventEnum INITIALIZE = new PanelPositionerEventEnum(++_index, "Panel Positioner, Initialize");
  public static PanelPositionerEventEnum ENABLE_ALL_AXES = new PanelPositionerEventEnum(++_index, "Panel Positioner, Enable All Axes");
  public static PanelPositionerEventEnum ENABLE_X_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Enable X Axis");
  public static PanelPositionerEventEnum ENABLE_Y_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Enable Y Axis");
  public static PanelPositionerEventEnum DISABLE_ALL_AXES = new PanelPositionerEventEnum(++_index, "Panel Positioner, Disable All Axes");
  public static PanelPositionerEventEnum DISABLE_X_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Disable X Axis");
  public static PanelPositionerEventEnum DISABLE_Y_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Disable Y Axis");
  public static PanelPositionerEventEnum HOME_ALL_AXES = new PanelPositionerEventEnum(++_index, "Panel Positioner, Home All Axes");
  public static PanelPositionerEventEnum HOME_X_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Home X Axis");
  public static PanelPositionerEventEnum HOME_Y_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Home Y Axis");
  public static PanelPositionerEventEnum POINT_TO_POINT_MOVE_ALL_AXES = new PanelPositionerEventEnum(++_index, "Panel Positioner, Point To Point Move All Axes");
  public static PanelPositionerEventEnum POINT_TO_POINT_MOVE_X_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Point To Point Move X Axis");
  public static PanelPositionerEventEnum POINT_TO_POINT_MOVE_Y_AXIS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Point To Point Move Y Axis");
  public static PanelPositionerEventEnum SET_POINT_TO_POINT_MOTION_PROFILE = new PanelPositionerEventEnum(++_index, "Panel Positioner, Set Point To Point Motion Profile");
  public static PanelPositionerEventEnum SET_SCAN_MOTION_PROFILE = new PanelPositionerEventEnum(++_index, "Panel Positioner, Set Scan Motion Profile");
  public static PanelPositionerEventEnum SET_POSITION_PULSE = new PanelPositionerEventEnum(++_index, "Panel Positioner, Set Position Pulse");
  public static PanelPositionerEventEnum ENABLE_SCAN_PATH = new PanelPositionerEventEnum(++_index, "Panel Positioner, Enable Scan Path");
  public static PanelPositionerEventEnum DISABLE_SCAN_PATH = new PanelPositionerEventEnum(++_index, "Panel Positioner, Disable Scan Path");
  public static PanelPositionerEventEnum RUN_NEXT_SCAN_PASS = new PanelPositionerEventEnum(++_index, "Panel Positioner, Run Next Scan Pass");

  private String _name;

 /**
  * @author Greg Esparza
  */
 private PanelPositionerEventEnum(int id, String name)
 {
   super(id);
   Assert.expect(name != null);

   _name = name;
 }

 /**
  * @author Rex Shang
  */
 public String toString()
 {
   return _name;
  }
}
