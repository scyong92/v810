package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;

/**
 * Used to tell the camera what kind of client is connecting to it.
 * @author George A. David
 */
public class XrayCameraClientTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  public static final XrayCameraClientTypeEnum CAMERA_MANAGER = new XrayCameraClientTypeEnum(++_index, "CONTROLLER_CAMERA_MANAGER");
  public static final XrayCameraClientTypeEnum IMAGE_RECEIVER = new XrayCameraClientTypeEnum(++_index, "CONTROLLER_IMAGE_RECEIVER");


  private String _clientTypeString;

  /**
   * @author George A. David
   */
  private XrayCameraClientTypeEnum(int id, String clientTypeString)
  {
    super(id);
    Assert.expect(clientTypeString != null);

    _clientTypeString = clientTypeString;

  }

  /**
   * @author George A. David
   */
  public String getClientTypeString()
  {
    Assert.expect(_clientTypeString != null);

    return _clientTypeString;
  }
}
