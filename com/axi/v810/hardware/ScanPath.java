package com.axi.v810.hardware;

import java.util.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * A pair of objects are required to complete a ScanPath: MechanicalConversions
 * to govern the path and a set of ScanPass 'es to implement it.
 *
 * @author Roy Williams
 */
public class ScanPath
{
  private MechanicalConversions                 _mechanicalConversions;
  private List<ScanPass>                        _scanPasses;

  /**
   * @author Roy Williams
   */
  public ScanPath(MechanicalConversions mechanicalConversions, List<ScanPass> scanPasses)
  {
    Assert.expect(mechanicalConversions != null);
    Assert.expect(scanPasses != null);
    Assert.expect(scanPasses.size() > 0);

    _mechanicalConversions = mechanicalConversions;
    _scanPasses = scanPasses;
    for (ScanPass scanPass : _scanPasses)
    {
      scanPass.setScanPath(this);
    }
  }

  /**
   * @author Roy Williams
   */
  public List<ScanPass> getScanPasses()
  {
    return _scanPasses;
  }

  /**
   * @author Roy Williams
   */
  public void setScanPasses(List<ScanPass> scanPasses)
  {
    Assert.expect(scanPasses != null);
    _scanPasses = scanPasses;
    for (ScanPass scanPass : _scanPasses)
    {
      scanPass.setScanPath(this);
    }
  }

  /**
   * @author Roy Williams
   */
  public MechanicalConversions getMechanicalConversions()
  {
    Assert.expect(_mechanicalConversions != null);

    return _mechanicalConversions;
  }

  /**
   * @author Roy Williams
   */
  public void setMechanicalConversions(MechanicalConversions mechanicalConversions)
  {
    Assert.expect(mechanicalConversions != null);
    _mechanicalConversions = mechanicalConversions;
  }

  /**
   * @author Kay Lannen
   */
  public MachineRectangle getScanPathExtents()
  {
    Assert.expect( _scanPasses != null );
    Assert.expect( _scanPasses.size() > 0 );

    StagePosition pStart = _scanPasses.get(0).getStartPointInNanoMeters();
    MachineRectangle scanPathExtents = new MachineRectangle( pStart.getXInNanometers(), pStart.getYInNanometers(), 0, 0 );

    for (ScanPass scanPass: _scanPasses)
    {
      pStart = scanPass.getStartPointInNanoMeters();
      scanPathExtents.add( pStart.getXInNanometers(), pStart.getYInNanometers() );
      StagePosition pEnd = scanPass.getEndPointInNanoMeters();
      scanPathExtents.add( pEnd.getXInNanometers(), pEnd.getYInNanometers());
    }
    return( scanPathExtents );
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_mechanicalConversions != null)
      _mechanicalConversions.clear();
    
    if (_scanPasses != null)
    {
      for(ScanPass scanPass : _scanPasses)
        scanPass.clearBreakPoint();
      
      _scanPasses.clear();
    }
  }
}
