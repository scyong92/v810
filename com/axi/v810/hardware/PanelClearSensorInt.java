package com.axi.v810.hardware;

import com.axi.v810.util.*;

interface PanelClearSensorInt
{
  /**
   * @return true if the panel clear sensor is blocked.
   * @author Rex Shang
   */
  boolean isBlocked() throws XrayTesterException;

  /**
   * Wait for sensor to get blocked.
   * @param timeOut long
   * @author Rex Shang
   */
  void waitUntilBlocked(long timeOut, boolean abortable) throws XrayTesterException;

  /**
   * Wait for sensor to get cleared.
   * @param timeOut long
   * @param abortable bool
   * @author Rex Shang
   */
  void waitUntilClear(long timeOut, boolean abortable) throws XrayTesterException;
}
