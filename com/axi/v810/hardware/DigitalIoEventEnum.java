package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class DigitalIoEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static DigitalIoEventEnum INITIALIZE = new DigitalIoEventEnum(++_index, "Digital IO, Initialize");
  public static DigitalIoEventEnum RESET = new DigitalIoEventEnum(++_index, "Digital IO, Reset");

  private String _name;

  /**
   * @author Rex Shang
   */
  private DigitalIoEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
