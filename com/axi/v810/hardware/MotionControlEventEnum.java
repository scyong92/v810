package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class MotionControlEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static MotionControlEventEnum INITIALIZE = new MotionControlEventEnum(++_index, "Motion Control, Initialize");

  private String _name;

  /**
   * @author Rex Shang
   */
  private MotionControlEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
