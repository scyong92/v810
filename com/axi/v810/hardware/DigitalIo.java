package com.axi.v810.hardware;

import java.io.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class will support direct interfacing with the digital I/O hardware.
 *
 * All classes, excluding any low-level diagnostic classes, should not directly
 * interface with this class. For example, since the primary use for digital I/O
 * is to control panel handling hardware such as outer barriers, sensors, etc,
 * most classes should interface with the PanelHandler class to control the
 * hardware or get hardware status.
 *
 * @author Rex Shang
 * @author Rick Gaudette
 */
public class DigitalIo extends HardwareObject implements HardwareStartupShutdownInt
{

  private static DigitalIo _instance;
  private static boolean _initialized;
  private static RemoteDigitalIoInterface _rdioi;
  private static RemoteMotionServerExecution _remoteMotionServer;
  private static final int _STARTUP_TIME_IN_MILLISECONDS = 35000;
  private static final int _REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS = 1000;
  private static final long _FAST_HARDWARE_POLLING_INTERVAL_IN_MILLI_SECONDS = 20;
  private static final long _HARDWARE_POLLING_INTERVAL_IN_MILLI_SECONDS = 10;
  /**
   * @todo RMS need to check with actual hardware characterization
   */
  private static final int _PANEL_CLAMPS_OPENING_TIME_IN_MILLI_SECONDS = 20;//200;
  private static final int _PANEL_CLAMPS_CLOSING_TIME_IN_MILLI_SECONDS = 20;//200;
  private static final int _PANEL_IN_PLACE_SENSOR_EXTENDING_TIME_IN_MILLI_SECONDS = 20;//200;
  private static final int _PANEL_IN_PLACE_SENSOR_RETRACTING_TIME_IN_MILLI_SECONDS = 20;//200;
  private static final int _STAGE_BELTS_START_MOVING_TIME_IN_MILLI_SECONDS = 0;
  private static final int _STAGE_BELTS_STOP_MOVING_TIME_IN_MILLI_SECONDS = 0;
  private static final int _OUTER_BARRIER_SETTLING_TIME_IN_MILLI_SECONDS = 20;//100;
  private static final int _INNER_BARRIER_SETTLING_TIME_IN_MILLI_SECONDS = 20;//100;
  //Variable Mag Anthony August 2011
  private static final int _XRAY_CYLINDER_SETTLING_TIME_IN_MILLI_SECONDS = 20;//200;
  private static final int _PANEL_CLAMPS_SETTLING_TIME_IN_MILLI_SECONDS = 20;//100;
  // The opening time took 460 ?465 milliseconds.
  // The closing time took 225 milliseconds.
  // But we are setting these to 1 second to allow the hardware to settle.
  private static final int _OUTER_BARRIER_OPENING_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _OUTER_BARRIER_CLOSING_TIME_IN_MILLI_SECONDS = 2000;
  // 426 msec is the slowest inner barrier speed...
  private static final int _INNER_BARRIER_OPENING_TIME_IN_MILLI_SECONDS = 3000;
  private static final int _INNER_BARRIER_CLOSING_TIME_IN_MILLI_SECONDS = 3000;
  //S2EX Anthony 2014
  private static final int _XRAY_Z_AXIS_MOVE_HOME_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _XRAY_Z_AXIS_MOVE_LOW_MAG_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _XRAY_Z_AXIS_MOVE_HIGH_MAG_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _XRAY_Z_AXIS_SETTLING_TIME_IN_MILLI_SECONDS = 11;
     
  //S2EX Anthony 2014
  private static final int _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME_TIME_IN_MILLI_SECONDS = 20000;
  private static final int _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_LOW_MAG_TIME_IN_MILLI_SECONDS = 10000;
  private static final int _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HIGH_MAG_TIME_IN_MILLI_SECONDS = 10000;
  private static final int _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_SETTLING_TIME_IN_MILLI_SECONDS = 200;
  
  //Variable Mag Anthony August 2011
  private static final int _XRAY_CYLINDER_UP_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _XRAY_CYLINDER_DOWN_TIME_IN_MILLI_SECONDS = 5000;
  private static final int _PANEL_CLAMP_CYLINDER_TIMEOUT_IN_MILLI_SECONDS = 2000;
  private static final int _PANEL_IN_PLACE_CYLINDER_TIMEOUT_IN_MILLI_SECONDS = 2000;
  // Turning off synthetic triggers can take a few milliseconds.
  private static final int _SYNTHETIC_TRIGGERS_TURNOFF_TIME_IN_MILLI_SECONDS = 1000;
  // NOTE: The id's will be used as keys to retrive actual hardware address.
  // Output bit id assignment.
  private static final String _leftOuterBarrierOutputBit = StringLocalizer.keyToString("HW_LEFT_OUTER_BARRIER_OUTPUT_KEY");
  private static final String _rightOuterBarrierOutputBit = StringLocalizer.keyToString("HW_RIGHT_OUTER_BARRIER_OUTPUT_KEY");
  private static final String _panelClampsOutputBit = StringLocalizer.keyToString("HW_PANEL_CLAMPS_OUTPUT_KEY");
  //S2EX Based Anthony 2014
  private static final String _zAxisPositionBitAOutputBit = StringLocalizer.keyToString("HW_Z_AXIS_POSITION_BIT_A_OUTPUT_KEY");
  private static final String _zAxisPositionBitBOutputBit = StringLocalizer.keyToString("HW_Z_AXIS_POSITION_BIT_B_OUTPUT_KEY");

  //XXL Based Anthony Jan 2013
  private static final String _panelUnclampsOutputBit = StringLocalizer.keyToString("HW_PANEL_UNCLAMPS_OUTPUT_KEY");
  //XXL Optical Stop Sensor - Anthony (May 2013)
  private static final String _enableXXLOpticalStopControllerOutputBit = StringLocalizer.keyToString("HW_ENABLE_XXL_OPTICAL_STOP_CONTROLLER_OUTPUT_KEY");
  private static final String _enableXXLOpticalStopControllerInputBit = StringLocalizer.keyToString("HW_ENABLE_XXL_OPTICAL_STOP_CONTROLLER_INPUT_KEY");
  private static final String _leftPanelInPlaceSensorOutputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_SENSOR_OUTPUT_KEY");
  private static final String _rightPanelInPlaceSensorOutputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_SENSOR_OUTPUT_KEY");
  private static final String _runStageBeltsOutputBit = StringLocalizer.keyToString("HW_STAGE_BELTS_OUTPUT_KEY");
  private static final String _stageBeltDirectionOutputBit = StringLocalizer.keyToString("HW_STAGE_BELTS_DIRECTION_OUTPUT_KEY");
  private static final String _stageViewingLightOutputBit = StringLocalizer.keyToString("HW_STAGE_VIEWING_LIGHT_OUTPUT_KEY");
  //Variable Mag Anthony August 2011
  private static final String _xrayCylinderDownOutputBit = StringLocalizer.keyToString("HW_XRAY_CYLINDER_UPDOWN_OUTPUT_KEY");
  private static final String _xrayCylinderClampOutputBit = StringLocalizer.keyToString("HW_XRAY_CYLINDER_CLAMP_OUTPUT_KEY");
  
  //Swee-Yee.Wong - S2EX Optical PIP Reset output bit 2014
  private static final String _resetOpticalPIPSensorOutputBit = StringLocalizer.keyToString("HW_RESET_OPTICAL_PANEL_IN_PLACE_SENSOR_OUTPUT_KEY");
  private static final String _resetOpticalPIPSensorInputBit = StringLocalizer.keyToString("HW_RESET_OPTICAL_PANEL_IN_PLACE_SENSOR_INPUT_KEY");
  
  //S2EX Based Anthony 2014  
  private static final String _zAxisEnableOutputBit = StringLocalizer.keyToString("HW_Z_AXIS_ENABLE_OUTPUT_KEY");
  
  //S2EX Based Anthony 2014   
  private static final String _leftPanelInPlaceEndStopperOutputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_END_STOPPER_OUTPUT_KEY");
  private static final String _rightPanelInPlaceEndStopperOutputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_END_STOPPER_OUTPUT_KEY");

  
  private static final String _lightStackRedLightOutputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_RED_LIGHT_OUTPUT_KEY");
  private static final String _lightStackYellowLightOutputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_YELLOW_LIGHT_OUTPUT_KEY");
  private static final String _lightStackGreenLightOutputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_GREEN_LIGHT_OUTPUT_KEY");
  private static final String _lightStackBuzzerOutputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_BUZZER_OUTPUT_KEY");
  private static final String _panelFrontLeftDoorLockOutputBit = StringLocalizer.keyToString("HW_PANEL_FRONT_LEFT_DOOR_LOCK_KEY");
  private static final String _panelFrontRightDoorLockOutputBit = StringLocalizer.keyToString("HW_PANEL_FRONT_RIGHT_DOOR_LOCK_KEY");
  private static final String _xrayTesterReadyToAcceptPanelOutputBit = StringLocalizer.keyToString("HW_XRAY_TESTER_READY_TO_ACCEPT_PANEL_OUTPUT_KEY");
  private static final String _panelOutAvailableOutputBit = StringLocalizer.keyToString("HW_PANEL_OUT_AVAILABLE_OUTPUT_KEY");
  private static final String _panelOutGoodOutputBit = StringLocalizer.keyToString("HW_PANEL_OUT_GOOD_OUTPUT_KEY");
  private static final String _panelOutUntestedOutputBit = StringLocalizer.keyToString("HW_PANEL_OUT_UNTESTED_OUTPUT_KEY");
  private static final String _xrayCamerasSyntheticTriggerOutputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_SYNTHETIC_TRIGGER_OUTPUT_KEY");
  private static final String _innerBarrierOutputBit = StringLocalizer.keyToString("HW_INNER_BARRIER_OUTPUT_KEY");
  // Command value for the setting the corresponding output bits.
  private static final int _io_ON = 1;
  private static final int _io_OFF = 0;
  
  private static final int _io_NegativeON = 0;
  private static final int _io_NegativeOFF = 1;
  
  private static final int _openLeftOuterBarrier = 1;
  private static final int _closeLeftOuterBarrier = 0;
  private static final int _openRightOuterBarrier = 1;
  private static final int _closeRightOuterBarrier = 0;
  private static final int _turnStageBeltsOn = 1;
  private static final int _turnStageBeltsOff = 0;
  private static final int _extendLeftPanelInPlaceSensor = 1;
  private static final int _retractLeftPanelInPlaceSensor = 0;
  private static final int _extendRightPanelInPlaceSensor = 1;
  private static final int _retractRightPanelInPlaceSensor = 0;
  private static final int _openPanelClamps = 0;
  private static final int _closePanelClamps = 1;
  private static final int _stageBeltDirectionLeftToRight = 0;
  private static final int _stageBeltDirectionRightToLeft = 1;
  private static final int _turnStageViewingLightOn = 1;
  private static final int _turnStageViewingLightOff = 0;
  private static final int _turnLightStackIndicatorOn = 1;
  private static final int _turnLightStackIndicatorOff = 0;
  //XXL Based Anthony Jan 2013
  private static final int _turnONOutputSignal = 1;
  private static final int _turnOFFOutputSignal = 0;
  //Variable Mag Anthony August 2011
  //XRay Cylinder Default Status is go up when output is off(0),
  //XRay Cylinder will go down when output is triggered/energized(1)
  private static final int _turnXRayCylinderDownOff = 0;
  private static final int _turnXRayCylinderDownOn = 1;
  //XRay Cylinder Clamp Default Status is Clamp when output is off(0),
  //XRay Cylinder Clamp will unclamp when output is triggered/energized(1)
  private static final int _turnXRayCylinderClamp = 0;
  private static final int _turnXRayCylinderUnClamp = 1;
  private static final int _turnZAxisEnableON= 1;
  private static final int _turnZAxisEnableOFF = 0;
  private static final int _xrayCylinderLevelOK = 1;
  private static final int _xrayCylinderLevelNotOK = 0;
  private static final int _turnPanelFrontLeftDoorLockOn = 1;
  private static final int _turnPanelFrontLeftDoorLockOff = 0;
  private static final int _turnPanelFrontRightDoorLockOn = 1;
  private static final int _turnPanelFrontRightDoorLockOff = 0;
  private static final int _xrayTesterReadyToAcceptPanel = 1;
  private static final int _xrayTesterNotReadyToAcceptPanel = 0;
  private static final int _panelOutAvailable = 1;
  private static final int _panelOutNotAvailable = 0;
  private static final int _panelOutIsGood = 1;
  private static final int _panelOutIsBad = 0;
  private static final int _panelOutIsTested = 0;
  private static final int _panelOutIsNotTested = 1;
  private static final int _turnXrayCamerasSyntheticTriggerOn = 0;
  private static final int _turnXrayCamerasSyntheticTriggerOff = 1;
  private static final int _openInnerBarrier; //default for Standard = 0;
  private static final int _closeInnerBarrier; //default for standard = 1;
  // NOTE: The id's will be used as keys to retrive actual hardware address.
  // Input bit id assignment.
  private static final String _leftOuterBarrierClosedInputBit = StringLocalizer.keyToString("HW_LEFT_OUTER_BARRIER_CLOSED_INPUT_KEY");
  private static final String _leftOuterBarrierOpenInputBit = StringLocalizer.keyToString("HW_LEFT_OUTER_BARRIER_OPEN_INPUT_KEY");
  private static final String _rightOuterBarrierClosedInputBit = StringLocalizer.keyToString("HW_RIGHT_OUTER_BARRIER_CLOSED_INPUT_KEY");
  private static final String _rightOuterBarrierOpenInputBit = StringLocalizer.keyToString("HW_RIGHT_OUTER_BARRIER_OPEN_INPUT_KEY");
  private static final String _leftPanelClearSensorInputBit;
  private static final String _rightPanelClearSensorInputBit;
  private static final String _leftPanelInPlaceSensorExtendInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_SENSOR_EXTEND_INPUT_KEY");
  private static final String _rightPanelInPlaceSensorExtendInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_SENSOR_ENTEND_INPUT_KEY");
   
  //S2EX Based Anthony 2014
  private static final String _leftPanelInPlaceEndStopperExtendInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_END_STOPPER_EXTEND_INPUT_KEY");
  private static final String _rightPanelInPlaceEndStopperExtendInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_END_STOPPER_ENTEND_INPUT_KEY");
  private static final String _leftPanelInPlaceEndStopperRetractInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_END_STOPPER_RETRACT_INPUT_KEY");
  private static final String _rightPanelInPlaceEndStopperRetractInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_END_STOPPER_RETRACT_INPUT_KEY");

  //S2EX Based Anthony 2014
  private static final String _zAxisEnableInputBit = StringLocalizer.keyToString("HW_Z_AXIS_ENABLE_INPUT_KEY");
  
  private static final String _zAxisAtReadySafePositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_READY_SAFE_POSITION_INPUT_KEY");
  //private static final String _zAxisAtLowMagPositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_LOW_MAG_POSITION_INPUT_KEY");
  private static final String _zAxisAtMag1PositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_MAG_1_POSITION_INPUT_KEY");
  private static final String _zAxisAtMag2PositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_MAG_2_POSITION_INPUT_KEY");

  //private static final String _zAxisAtHighMagPositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_HIGH_MAG_POSITION_INPUT_KEY");
  private static final String _zAxisAtMag3PositionInputBit = StringLocalizer.keyToString("HW_Z_AXIS_AT_MAG_3_POSITION_INPUT_KEY");
  
  //S3 Based Anthony 2014
  private static final String _xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_AT_19_MICRON_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_AT_11_MICRON_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_ALARM_INPUT_KEY");
  
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmSimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_RESET_ALARM_SIMULIATED_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerHomeSimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_HOMING_SIMULIATED_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerStartSimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_START_SIMULIATED_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit0SimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT0_SIMULIATED_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit1SimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT1_SIMULIATED_INPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit2SimulatedIuputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT2_SIMULIATED_INPUT_KEY");
  
  private static final String _xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_RESET_ALARM_OUTPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorHomingOutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_HOMING_OUTPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_START_MOTION_OUTPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT0_OUTPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT1_OUTPUT_KEY");
  private static final String _xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit = StringLocalizer.keyToString("HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_POSITION_BIT2_OUTPUT_KEY");
  
  //XXL Based Anthony Jan 2013
  private static final String _leftPanelInPlaceSensorRetractInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_SENSOR_RETRACT_INPUT_KEY");
  private static final String _rightPanelInPlaceSensorRetractInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_SENSOR_RETRACT_INPUT_KEY");
  private static final String _panelUnclampsInputBit = StringLocalizer.keyToString("HW_PANEL_UNCLAMPS_INPUT_KEY");
  //XXL Optical Stop Sensor - Anthony (May 2013)
  private static final String _leftPanelInPlaceOpticalSensorEngagedInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_OPTICAL_SENSOR_ENGAGE_INPUT_KEY");
  private static final String _rightPanelInPlaceOpticalSensorEngagedInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_OPTICAL_SENSOR_ENGAGE_INPUT_KEY");
  private static final String _opticalStopControllerDoneInputBit = StringLocalizer.keyToString("HW_OPTICAL_STOP_CONTROLLER_DONE_INPUT_KEY");
  private static final String _opticalStopControllerErrorInputBit = StringLocalizer.keyToString("HW_OPTICAL_STOP_CONTROLLER_ERROR_INPUT_KEY");
  private static final String _opticalStopControllerInstalledInputBit = StringLocalizer.keyToString("HW_OPTICAL_STOP_CONTROLLER_INSTALLED_INPUT_KEY");
  private static final int _opticalStopControllerInstalled = 1;
  private static final int _opticalStopControllerIsDone = 1;
  private static final int _opticalStopControllerIsError = 1;
  private static final String _leftPanelInPlaceSensorEngagedInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_IN_PLACE_SENSOR_ENGAGE_INPUT_KEY");
  private static final String _rightPanelInPlaceSensorEngagedInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_IN_PLACE_SENSOR_ENGAGE_INPUT_KEY");
  private static final String _panelClampsInputBit = StringLocalizer.keyToString("HW_PANEL_CLAMPS_INPUT_KEY");
  private static final String _stageBeltsInputBit = StringLocalizer.keyToString("HW_STAGE_BELTS_INPUT_KEY");
  private static final String _interlockServiceModeOnInputBit = StringLocalizer.keyToString("HW_INTERLOCK_SERVICE_MODE_ON_INPUT_KEY");
  private static final String _interlockPowerOnInputBit = StringLocalizer.keyToString("HW_INTERLOCK_POWER_ON_INPUT_KEY");
  private static final String _interlockChain1OpenInputBit = StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_1_OPEN_INPUT_KEY");
  private static final String _interlockChain2OpenInputBit = StringLocalizer.keyToString("HW_INTERLOCK_CHAIN_2_OPEN_INPUT_KEY");
  private static final String _interlockHardwareIdBit3InputBit = StringLocalizer.keyToString("HW_INTERLOCK_HARDWARE_ID_BIT_3_INPUT_KEY");
  private static final String _interlockHardwareIdBit2InputBit = StringLocalizer.keyToString("HW_INTERLOCK_HARDWARE_ID_BIT_2_INPUT_KEY");
  private static final String _interlockHardwareIdBit1InputBit = StringLocalizer.keyToString("HW_INTERLOCK_HARDWARE_ID_BIT_1_INPUT_KEY");
  private static final String _interlockHardwareIdBit0InputBit = StringLocalizer.keyToString("HW_INTERLOCK_HARDWARE_ID_BIT_0_INPUT_KEY");
  private static final String _interlockSwitchIdBit5InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_5_INPUT_KEY");
  private static final String _interlockSwitchIdBit4InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_4_INPUT_KEY");
  private static final String _interlockSwitchIdBit3InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_3_INPUT_KEY");
  private static final String _interlockSwitchIdBit2InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_2_INPUT_KEY");
  private static final String _interlockSwitchIdBit1InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_1_INPUT_KEY");
  private static final String _interlockSwitchIdBit0InputBit = StringLocalizer.keyToString("HW_INTERLOCK_SWITCH_ID_BIT_0_INPUT_KEY");
  private static final String _downStreamSystemReadyToAcceptPanelInputBit = StringLocalizer.keyToString("HW_DOWNSTREAM_SYSTEM_READY_TO_ACCEPT_PANEL_INPUT_KEY");
  private static final String _panelInAvailableInputBit = StringLocalizer.keyToString("HW_PANEL_IN_AVAILABLE_INPUT_KEY");
  private static final String _stageBeltDirectionInputBit = StringLocalizer.keyToString("HW_STAGE_BELTS_DIRECTION_INPUT_KEY");
  private static final String _stageViewingLightInputBit = StringLocalizer.keyToString("HW_STAGE_VIEWING_LIGHT_INPUT_KEY");
  private static final String _lightStackRedLightInputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_RED_LIGHT_INPUT_KEY");
  private static final String _lightStackYellowLightInputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_YELLOW_LIGHT_INPUT_KEY");
  private static final String _lightStackGreenLightInputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_GREEN_LIGHT_INPUT_KEY");
  private static final String _lightStackBuzzerInputBit = StringLocalizer.keyToString("HW_LIGHT_STACK_BUZZER_INPUT_KEY");
  //Variable Mag Anthony August 2011
  private static final String _xrayCylinderUpInputBit = StringLocalizer.keyToString("HW_XRAY_CYLINDER_UP_INPUT_KEY");
  private static final String _xrayCylinderDownInputBit = StringLocalizer.keyToString("HW_XRAY_CYLINDER_DOWN_INPUT_KEY");
  private static final String _xrayCylinderClampInputBit = StringLocalizer.keyToString("HW_XRAY_CYLINDER_CLAMP_INPUT_KEY"); 
  private static final String _xrayLeftSafetyLevelSensorInputBit = StringLocalizer.keyToString("HW_XRAY_LEFT_SAFETY_LEVEL_SENSOR_INPUT_KEY");
  private static final String _xrayRightSafetyLevelSensorInputBit = StringLocalizer.keyToString("HW_XRAY_RIGHT_SAFETY_LEVEL_SENSOR_INPUT_KEY");
  private static final String _panelFrontLeftDoorLockInputBit = StringLocalizer.keyToString("HW_PANEL_FRONT_LEFT_DOOR_LOCK_KEY");
  private static final String _panelFrontRightDoorLockInputBit = StringLocalizer.keyToString("HW_PANEL_FRONT_RIGHT_DOOR_LOCK_KEY");
  private static final String _xrayTesterReadyToAcceptPanelInputBit = StringLocalizer.keyToString("HW_XRAY_TESTER_READY_TO_ACCEPT_PANEL_INPUT_KEY");
  private static final String _panelOutAvailableInputBit = StringLocalizer.keyToString("HW_PANEL_OUT_AVAILABLE_INPUT_KEY");
  private static final String _panelOutGoodInputBit = StringLocalizer.keyToString("HW_PANEL_OUT_GOOD_INPUT_KEY");
  private static final String _panelOutUntestedInputBit = StringLocalizer.keyToString("HW_PANEL_OUT_UNTESTED_INPUT_KEY");
  private static final String _xrayCamerasSyntheticTriggerInputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_SYNTHETIC_TRIGGER_INPUT_KEY");
  private static final String _xrayCamerasConnectedInputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_CONNECTED_INPUT_KEY");
  private static final String _xrayCamerasPowerOnInputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_POWER_ON_INPUT_KEY");
  private static final String _xrayCamerasUsingExcessivePowerInputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_USING_EXCESSIVE_POWER_INPUT_KEY");
  private static final String _xrayCamerasPhaseLockLoopOnInputBit = StringLocalizer.keyToString("HW_XRAY_CAMERAS_PHASE_LOCKED_INPUT_KEY");
  private static final String _innerBarrierClosedInputBit = StringLocalizer.keyToString("HW_INNER_BARRIER_CLOSED_INPUT_KEY");
  private static final String _innerBarrierOpenInputBit = StringLocalizer.keyToString("HW_INNER_BARRIER_OPEN_INPUT_KEY");
  
  private static final String _smemaSignalOneDebugInputBit = StringLocalizer.keyToString("HW_SMEMA_SENSOR_1_SIGNAL_DEBUG_INPUT_KEY");
  private static final String _smemaSignalTwoDebugInputBit = StringLocalizer.keyToString("HW_SMEMA_SENSOR_2_SIGNAL_DEBUG_INPUT_KEY");
  // Input bit status definitions.
  // For simulation to work, make sure to initialize the status if 0
  // is not the initial state. See the initializeInputBitField().
  private static final int _leftOuterBarrierIsOpen = 1;
  private static final int _leftOuterBarrierIsNotClosed = 0;
  private static final int _rightOuterBarrierIsOpen = 1;
  private static final int _rightOuterBarrierIsNotClosed = 0;
  private static final int _stageBeltsAreOn = 1;
  private static final int _xrayZAxisEnableIsOn = 1;
  private static final int _leftPanelInPlaceSensorIsExtended = 1;
  private static final int _rightPanelInPlaceSensorIsExtended = 1;
  //XXL Based Anthony Jan 2013
  private static final int _leftPanelInPlaceSensorIsRetracted = 1;
  private static final int _rightPanelInPlaceSensorIsRetracted = 1;
  private static final int _leftPanelInPlaceSensorIsEngaged = 1;
  private static final int _rightPanelInPlaceSensorIsEngaged = 1;
  private static final int _panelClampsAreClosed = 1;
  private boolean _bypassOpticalStopController = false; //set this flag to true in order to bypass Optical Stop Controller for development debug or troubleshooting only
  /**
   * @todo RMS The next few lines of code need to be clean up once we complete
   * the transition to use active low type PCS.
   */
  private static final int _leftPanelClearSensorIsBlocked;
  private static final int _rightPanelClearSensorIsBlocked;
  private boolean _debug = Config.isDeveloperDebugModeOn();
  //private boolean _opticalStopControllerWithEndStopper = true;

  private static final String _POSITION_NAME_23_MICRON = "M23";
  private static final String _POSITION_NAME_19_MICRON = "M19";
  private static final String _POSITION_NAME_11_MICRON = "M11";
  private static String _xrayZAxisMotorPositionLookupForLowMagnification = _POSITION_NAME_19_MICRON;
  private static String _xrayZAxisMotorPositionLookupForHighMagnification = _POSITION_NAME_11_MICRON;

  private boolean _isInnerBarrierCommandedClose = true;
  private boolean _isXrayTubeCommandedHome = true;
  private boolean _isByPassPanelClearSensor = false;
  
  static
  {
    if (PanelClearSensor.isActiveLowTypeInstalled())
    {
      _leftPanelClearSensorInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_CLEAR_SENSOR_INPUT_KEY");
      _rightPanelClearSensorInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_CLEAR_SENSOR_INPUT_KEY");

      _leftPanelClearSensorIsBlocked = 0;
      _rightPanelClearSensorIsBlocked = 0;
    }
    else
    {
      _leftPanelClearSensorInputBit = StringLocalizer.keyToString("HW_LEFT_PANEL_CLEAR_SENSOR_ACTIVE_HIGH_INPUT_KEY");
      _rightPanelClearSensorInputBit = StringLocalizer.keyToString("HW_RIGHT_PANEL_CLEAR_SENSOR_ACTIVE_HIGH_INPUT_KEY");

      _leftPanelClearSensorIsBlocked = 1;
      _rightPanelClearSensorIsBlocked = 1;
    }

    if (XrayCylinderActuator.isInstalled())
    {
      _openInnerBarrier = 0;
      _closeInnerBarrier = 1;
    }
    else
    {
      _openInnerBarrier = 0;
      _closeInnerBarrier = 1;
    }
    if(XrayTester.isS2EXEnabled())
    {
      _xrayZAxisMotorPositionLookupForLowMagnification = _POSITION_NAME_11_MICRON;
      _xrayZAxisMotorPositionLookupForHighMagnification = _POSITION_NAME_19_MICRON;
    }
  }
  private static final int _stageBeltDirectionIsLeftToRight = 0;
  private static final int _stageViewingLightIsOn = 1;
  private static final int _lightStackIndicatorIsOn = 1;
  private static final int _panelOutIsAvailable = 1;
  private static final int _panelInIsAvailable = 1;
  private static final int _downStreamSystemIsReadyToAcceptPanel = 1;
  private static final int _smemaSignalSensorTrigger = 1;
  private static final int _xrayCamerasSyntheticTriggerAreOn = 0;
  private static final int _xrayCamerasAreConnected = 1;
  private static final int _xrayCamerasAreOn = 1;
  private static final int _xrayCamerasAreUsingExcessivePower = 0;
  private static final int _xrayCameraArePhaseLocked = 1;
  private static final int _innerBarrierIsOpen = 1;
  private static final int _innerBarrierIsClosed = 1;
  private static final int _innerBarrierIsNotClosed = 0;
  private static final int _interlockServiceModeIsOn = 0;
  private static final int _interlockIsOn = 1;
  private static final int _intlockChainIsOpen = 0;
  //Variable Mag Anthony August 2011
  private static final int _xrayCylinderIsDown = 1;
  private static final int _xrayCylinderIsNotUp = 0;
  private static final int _xrayCylinderIsUp = 1;
  private static final int _xrayCylinderIsNotDown = 0;
  private static final int _xrayCylinderDownOutputBitNumber = 13;
  private static final int _xrayCylinderClampOutputBitNumber = 14;
  //S2EX Based  Anthony 2014
  private static final int _xrayZAxisIsDown = 1;
  private static final int _xrayZAxisIsNotUp = 0;
  private static final int _xrayZAxisIsUp = 1;
  private static final int _xrayZAxisIsNotDown = 0; 
  private static final int _xrayZAxisIsHome = 1;
  private static final int _xrayZAxisIsNotHome = 0;

  private static final int _zAxisEnableOutputBitNumber = 15;
  private static final int _zAxisPositionBitAOutputBitNumber = 19;
  private static final int _zAxisPositionBitBOutputBitNumber = 18;
  
  //Swee yee - 2014
  private static final int _runStageBeltsOutputBitNumber = 4;
  private static final int _stageBeltDirectionOutputBitNumber = 5;
  

  private static final int _xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBitNumber = 35;
  private static final int _xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBitNumber = 36;
  private static final int _xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBitNumber = 37;
  
  private static final int _xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBitNumber = 24;
  private static final int _xrayMotorizedHeightLevelSafetySensorHomingOutputBitNumber = 25;
  private static final int _xrayMotorizedHeightLevelSafetySensorStartMotionOutputBitNumber = 26;
  private static final int _xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBitNumber = 27;
  private static final int _xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBitNumber = 28;
  private static final int _xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBitNumber = 29;
   
  private static final int _InnerBarrierOutputBitNumber = 7;
  //XXL Based Anthony Jan 2013
  private static final int _panelClampOutputBitNumber = 6;
  private static final int _leftPanelInPlaceSensorOutputBitNumber = 2;
  private static final int _rightPanelInPlaceSensorOutputBitNumber = 3;
  //XXL Optical Stop Sensor - Anthony (May 2013)
  private static final int _enableXXLOpticalStopControllerOutputBitNumber = 2;
  private static final int _enableXXLOpticalStopControllerWithEndStopperOutputBitNumber = 17;
  private static final int _enableXXLOpticalStopControllerInputBitNumber = 43; //Simulated
  private static final int _enableS2EXOpticalStopControllerInputBitNumber = 74; //Simulated
  
  //S2EX Optical Stop Sensor Reset - Swee Yee 2014
  private static final int _resetOpticalPIPSensorOutputBitNumber = 30;
  private static final int _resetOpticalPIPSensorInputBitNumber = 75; //Simulated
  // Maps from the variable integer to the bit that needs to be set in the hardware
  // two keys cannot be identical, but two or more keys can map to the same hardware bit
  // because different versions of hardware use the same bit for different functions.
  private Map<String, Integer> _inputBitKeyToInputBitIdMap;
  private Map<String, Integer> _outputBitKeyToOutputBitIdMap;
  // This map is used to map the input to an output -- paired io.
  private Map<Integer, Integer> _outputBitIdToInputBitIdMap;
  // This map is used to map a pair of io that has inverted relationship.
  // So setting "1" to an output will cause an input to go to "0".
  private Map<Integer, Integer> _outputBitIdToInputBitIdMapForTheInverted;
  // The last bit number assignment for an input bit. Bits beyond that range
  // are for simulation only.
  private static int _maximumInputBitNumber;
  // The last bit number assignment for an output bit. Number beyond that
  // is not a valid hardware and writing to the designated bit will cause assert.
  private static int _maximumOutputBitNumber;
  // The number of bits required for _inputBitFieldForSimulation.
  private static int _numberOfInputBits;
  // The bit field is used for simulation. But it is also used to keep track
  // of the states of output bits in non-simulation mode.
  private List<Integer> _inputBitField;
  // Unique keys used to look up the bit designation.
  private List<String> _outputKeys;
  private List<String> _inputKeys;
  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;
  private ProgressObservable _progressObservable;
  private HardwareConfigurationDescriptionInt _configDescription;
 

  /**
   * Do not allow the client to call the constructor. Instead, they must call
   * "getInstance()" to get the one and only instance
   *
   * @author Rex Shang
   */
  private DigitalIo()
  {
    _initialized = false;
    
    // Initialize object
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _progressObservable = ProgressObservable.getInstance();

    _remoteMotionServer = RemoteMotionServerExecution.getInstance();
    
    if (XrayTester.isS2EXEnabled())
    {
      _xrayZAxisMotorPositionLookupForLowMagnification=getConfig().getSystemCurrentLowMagnification();   
      _xrayZAxisMotorPositionLookupForHighMagnification=getConfig().getSystemCurrentHighMagnification();
      
      if (XrayTester.isS2EXEnabled())
      {
         if ((isOpticalStopControllerInstalled()==false) || 
         (isOpticalStopControllerEndStopperInstalled() == false))
        {
            _initialized = false;
            return;
        }
      }
      boolean isXrayZAxisMotorPositionLookupForMagnificationValid=true;
      if(_xrayZAxisMotorPositionLookupForLowMagnification.contains(_POSITION_NAME_11_MICRON))
      {
        isXrayZAxisMotorPositionLookupForMagnificationValid=false;
      }
      else if(_xrayZAxisMotorPositionLookupForHighMagnification.contains(_POSITION_NAME_23_MICRON))
      {
        isXrayZAxisMotorPositionLookupForMagnificationValid=false;
      }      
      else if(_xrayZAxisMotorPositionLookupForHighMagnification.contains(_POSITION_NAME_19_MICRON))
      {
        isXrayZAxisMotorPositionLookupForMagnificationValid=false;
      }
      
      if(isXrayZAxisMotorPositionLookupForMagnificationValid==false)
      {      
        try
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoXrayZAxisMotorPositionLookupForMagnificationInvalidException();
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        catch (XrayTesterException xte)
        {
        }
      }
    }

    setInputBitIdToInputBitAddressMap();
    setOutputBitIdToOutputBitAddressMap();
    setOutputBitIdToInputBitIdMap();

    if (  isSimulationModeOn() == false)
    {
    initializeInputBitField();
    }

  }

  /**
   * @throws RemoteException
   * @throws NotBoundException
   */
  public static synchronized void connectToDigitalIoServer() throws RemoteException, NotBoundException
  {
    // Check to see if we are already connected to a server
    if (_rdioi != null)
    {
      return;
    }

    // If not attempt to connected to the server specified by the hardware config
    String rmc_ip_address = RemoteMotionServerExecution.getInstance().getServerAddress();

    if (Config.isDeveloperDebugModeOn())
    {
      System.out.println("DigitialIO: Expecting Remote Digitial IO at IP: " + rmc_ip_address);
    }

    Registry registry = LocateRegistry.getRegistry(rmc_ip_address);
    _rdioi = (RemoteDigitalIoInterface) registry.lookup("RemoteDigitalIo");

    if (Config.isDeveloperDebugModeOn())
    {
      System.out.println("Digitial IO: Connected to Remote Digitial IO at IP: " + rmc_ip_address);
    }
  }

  /**
   * @throws RemoteException
   * @throws NotBoundException
   * @author Chong, Wei Chin
   */
  public static synchronized String getRemoteMotionServerApplicationVersion() throws RemoteException, NotBoundException
  {
    connectToDigitalIoServer();

    String version;
    try
    {
      version = _rdioi.remoteGetApplicationVersion();
    }
    catch (RemoteException re)
    {
      try
      {
        version = _rdioi.remoteGetApplicationVersion();
      }
      catch (RemoteException re2)
      {
        throw re2;
      }
    }

    return version;
  }

  /**
   * Get the one and only instance of the DigtialIo object
   *
   * @return the only instance of the DigitalIo object
   * @author Greg Esparza
   */
  public static synchronized DigitalIo getInstance()
  {
    if (_instance == null)
    {
      _instance = new DigitalIo();
    }

    return _instance;
  }

  /**
   * Assign the actual hardware address to an output bit.
   *
   * @author Rex Shang
   */
  protected void setOutputBitIdToOutputBitAddressMap()
  {
    _outputBitKeyToOutputBitIdMap = new HashMap<String, Integer>();
    _outputKeys = new ArrayList<String>();

    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_leftOuterBarrierOutputBit,
      new Integer(0)) == null);
    if (LeftOuterBarrier.isInstalled())
    {
      _outputKeys.add(_leftOuterBarrierOutputBit);
    }
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_rightOuterBarrierOutputBit,
      new Integer(1)) == null);
    if (RightOuterBarrier.isInstalled())
    {
      _outputKeys.add(_rightOuterBarrierOutputBit);
    }

    if (isOpticalStopControllerInstalled())
    {
      if (isOpticalStopControllerEndStopperInstalled())
      {
        Assert.expect(_outputBitKeyToOutputBitIdMap.put(_enableXXLOpticalStopControllerOutputBit,
          new Integer(_enableXXLOpticalStopControllerWithEndStopperOutputBitNumber)) == null);
        _outputKeys.add(_enableXXLOpticalStopControllerOutputBit);
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          Assert.expect(_outputBitKeyToOutputBitIdMap.put(_leftPanelInPlaceSensorOutputBit,
            new Integer(_leftPanelInPlaceSensorOutputBitNumber)) == null);
          _outputKeys.add(_leftPanelInPlaceSensorOutputBit);
          Assert.expect(_outputBitKeyToOutputBitIdMap.put(_rightPanelInPlaceSensorOutputBit,
            new Integer(_rightPanelInPlaceSensorOutputBitNumber)) == null);
          _outputKeys.add(_rightPanelInPlaceSensorOutputBit);
        }
        else
        {
          Assert.expect(_outputBitKeyToOutputBitIdMap.put(_leftPanelInPlaceEndStopperOutputBit,
            new Integer(_leftPanelInPlaceSensorOutputBitNumber)) == null);
          _outputKeys.add(_leftPanelInPlaceEndStopperOutputBit);
          Assert.expect(_outputBitKeyToOutputBitIdMap.put(_rightPanelInPlaceEndStopperOutputBit,
            new Integer(_rightPanelInPlaceSensorOutputBitNumber)) == null);
          _outputKeys.add(_rightPanelInPlaceEndStopperOutputBit);
        }
 
      }
      else
      {
        Assert.expect(_outputBitKeyToOutputBitIdMap.put(_enableXXLOpticalStopControllerOutputBit,
          new Integer(_enableXXLOpticalStopControllerOutputBitNumber)) == null);
        _outputKeys.add(_enableXXLOpticalStopControllerOutputBit);
      }
      if (isOpticalPIPResetInstalled())
      {
        Assert.expect(_outputBitKeyToOutputBitIdMap.put(_resetOpticalPIPSensorOutputBit,
          new Integer(_resetOpticalPIPSensorOutputBitNumber)) == null);
        _outputKeys.add(_resetOpticalPIPSensorOutputBit);
      }
    }
    else
    {
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_leftPanelInPlaceSensorOutputBit,
        new Integer(_leftPanelInPlaceSensorOutputBitNumber)) == null);
      _outputKeys.add(_leftPanelInPlaceSensorOutputBit);
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_rightPanelInPlaceSensorOutputBit,
        new Integer(_rightPanelInPlaceSensorOutputBitNumber)) == null);
      _outputKeys.add(_rightPanelInPlaceSensorOutputBit);
    }
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_runStageBeltsOutputBit,
      new Integer(_runStageBeltsOutputBitNumber)) == null);
    _outputKeys.add(_runStageBeltsOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_stageBeltDirectionOutputBit,
      new Integer(_stageBeltDirectionOutputBitNumber)) == null);
    _outputKeys.add(_stageBeltDirectionOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelClampsOutputBit,
      new Integer(_panelClampOutputBitNumber)) == null);
    _outputKeys.add(_panelClampsOutputBit);

    if (XrayTester.isXXLBased()||XrayTester.isS2EXEnabled())
    {
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelUnclampsOutputBit,
        new Integer(16)) == null);

      _outputKeys.add(_panelUnclampsOutputBit);
    }
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_innerBarrierOutputBit,
      new Integer(_InnerBarrierOutputBitNumber)) == null);
    if (InnerBarrier.isInstalled())
    {
      _outputKeys.add(_innerBarrierOutputBit);
    }

    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_lightStackRedLightOutputBit,
      new Integer(8)) == null);
    _outputKeys.add(_lightStackRedLightOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_lightStackYellowLightOutputBit,
      new Integer(9)) == null);
    _outputKeys.add(_lightStackYellowLightOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_lightStackGreenLightOutputBit,
      new Integer(10)) == null);
    _outputKeys.add(_lightStackGreenLightOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_lightStackBuzzerOutputBit,
      new Integer(11)) == null);
    if (LightStack.isBuzzerEnabled())
    {
      _outputKeys.add(_lightStackBuzzerOutputBit);
    }
    if (XrayTester.isS2EXEnabled() == false)
    {
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_stageViewingLightOutputBit,
        new Integer(12)) == null);
      _outputKeys.add(_stageViewingLightOutputBit);
    }

    //Variable Mag Anthony August 2011
    //Output port:
    //Slice1-5  - For trigger cylinder solenoid valve
    //Slice1-6  - For trigger cylinder clamping mechanism
    //
    //Input Port:
    //Slice 5-2 - For cylinder positioning proximity sensor 1
    //Slice 5-3 - For cylinder positioning proximity sensor 2
    //Slice 5-4 - For height filtering sensing(left door)
    //Slice 5-5 - For height filtering sensing(right door)

    //XXL Based Anthony Jan 2013
    //Output port:
    //Slice 0-2 (2) Left PIP Extend (Existing output)
    //Slice 0-3 (3) Right PIP Extend (Existing output)
    //Slice 0-6 (6) Panel Clamper Up (for Clamping)(Existing output)
    //Slice 2-0 (16) Panel Clamper Down (for Open) (New output)
    //Input Port:
    //Slice 8-0 (24) Left PIP Extend
    //Slice 8-1 (25) Left PIP Retract
    //Slice 8-2 (26) Right PIP Extend
    //Slice 8-3 (27) Right PIP Retract
    //Slice 8-4 (28) Panel Clamper Down(Open or Unclamps)

    //XXL Optical Stop Sensor - Anthony (May 2013)
    //Output port:
    //*Slice 0-2 (2) Left PIP Extend (Existing output) -> Enable XXL Optical Stop Sensor
    //Slice 0-3 (3) Right PIP Extend (Existing output) -> Not in used
    //Slice 0-6 (6) Panel Clamper Up (for Clamping)(Existing output) -> Remain
    //Slice 2-0 (16) Panel Clamper Down (for Open) (New output) -> Remain
    //Input Port:
    //*Slice 3-6 (6) Left PIP Engaged  -> Left Panel Optical Stop Sensor
    //*Slice 3-7 (7) Right PIP Engaged -> Right Panel Optical Stop Sensor
    //*Slice 8-0 (24) Left PIP Extend  -> VIE Controller Done Signal
    //*Slice 8-1 (25) Left PIP Retract -> VIE Controller Error Signal
    //Slice 8-2 (26) Right PIP Extend -> Not in used
    //Slice 8-3 (27) Right PIP Retract -> Not in used
    //Slice 8-4 (28) Panel Clamper Down(Open or Unclamps) -> Remain
    //*Slice 8-5 (29) Optical Stop Sensor Installed -> New I/O

    // Input Optical Sensor Layout:
    //
    //        L_PIP (Alignment)                                                                         R_PIP (Alignment)
    //         |<--Slow---| <--Fast---------------  Converyor Speed  --------------- Fast--> | ---Slow-->|
    //         V          V                                                                  V           V
    //         o          o                                                                  o           0
    //        L_Stop    L_Slow  <-----------------  Optical Sensor   ----------------->   R_Slow        L_Stop
    //        (S2EX-6)   (hard ware1)--------------> to VIE Controller <-----------------  (hard ware2)    (S2EX-7)
    //
    //
    // Output Conveyor Relay Control Layout:
    //
    //         ------                ------                ------                ------         ------------
    //        |      |              |  /o  |              |  /o  |              |      |       | Conveyor   |-----
    //      --|-/ o--|--------------|-/ ---|--------------|-/ ---|--------------|-/ o--|-------| Motor      |     |
    //        | NC   |              | NO   |              | NO   |              | NC   |       |            |-----
    //         ------                ------                ------                ------         ------------
    //       Change Speed           Motor Run             Motor Dir          Immediate Stop
    //        FAST/SLOW              OFF/ON             Forward/Backward
    //       (hard ware3)            (S0-4)               (S0-5)             (hard ware4)
    //
    // VIE Optical Stop Controller Layout:
    //
    //                                          Enable (S0-2)
    //                                                |
    //                                                V
    //                                     -----------------------
    // Conveyor Motor Dir (S0-5)       -->|                       |
    // Conveyor Motor Run (S0-4)       -->|                       |--> Conveyor Change Slow Speed (h/w3)
    // Left Panel Slow Sensor(h/w1)    -->|    VIE Optical Stop   |--> Conveyor Immediate Stop (h/w4)
    // Left Panel Stop Sensor(S2EX-6)    -->|      Controller       |--> Controller Done Signal (S8-0)
    // Right Panel Slow Sensor(h/w2)   -->|                       |--> Controller Error Signal (S8-1)
    // Right Panel Stop Sensor(S2EX-7)   -->|                       |
    //                                     -----------------------


 

    if(XrayTester.isS2EXEnabled())
    {

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_zAxisEnableOutputBit,
        new Integer(_zAxisEnableOutputBitNumber)) == null);
      _outputKeys.add(_zAxisEnableOutputBit);

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_zAxisPositionBitAOutputBit,
        new Integer(_zAxisPositionBitAOutputBitNumber)) == null);
      _outputKeys.add(_zAxisPositionBitAOutputBit);

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_zAxisPositionBitBOutputBit,
        new Integer(_zAxisPositionBitBOutputBitNumber)) == null);
      _outputKeys.add(_zAxisPositionBitBOutputBit);
    }
    else
    {
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayCylinderDownOutputBit,
      new Integer(_xrayCylinderDownOutputBitNumber)) == null);
      if (XrayCylinderActuator.isInstalled())
      {
        _outputKeys.add(_xrayCylinderDownOutputBit);
      }
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayCylinderClampOutputBit,
        new Integer(_xrayCylinderClampOutputBitNumber)) == null);
      if (XrayCylinderActuator.isInstalled())
      {
        _outputKeys.add(_xrayCylinderClampOutputBit);
      }

      if (Interlock.getInterLockLogicBoardWithKeylock() == false)
      {
        Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelFrontLeftDoorLockOutputBit,
          new Integer(18)) == null);
        _outputKeys.add(_panelFrontLeftDoorLockOutputBit);

        Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelFrontRightDoorLockOutputBit,
          new Integer(19)) == null);
        _outputKeys.add(_panelFrontRightDoorLockOutputBit);
      }
    }

    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayTesterReadyToAcceptPanelOutputBit,
      new Integer(20)) == null);
    _outputKeys.add(_xrayTesterReadyToAcceptPanelOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelOutAvailableOutputBit,
      new Integer(21)) == null);
    _outputKeys.add(_panelOutAvailableOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelOutGoodOutputBit,
      new Integer(22)) == null);
    _outputKeys.add(_panelOutGoodOutputBit);
    Assert.expect(_outputBitKeyToOutputBitIdMap.put(_panelOutUntestedOutputBit,
      new Integer(23)) == null);
    _outputKeys.add(_panelOutUntestedOutputBit);

 
    
    if(isXrayMotorizedHeightLevelSafetySensorInstalled())
    {    
      //Input         Slice I/O          Description
      //19um          9-3                Sensor for 19um Magnification Position
      //11um          9-4                Sensor for 11um Magnification Position
      //Alarm         9-5                Alarm output from motor controller

      //Output        Slice I/O         Description
      //Alarm Reset   10-0              Reset motor if have any alarm/error
      //Home          10-1              Trigger motor to do home operation
      //Start         10-2              Trigger motor to move according to defined input bits
      //M0            10-3              Input bit 1
      //M1            10-4              Input bit 2
      //M2            10-5              Input bit 3

      
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit);
    
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorHomingOutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorHomingOutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorHomingOutputBit);

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit);

      
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit);

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit);

      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit,
        new Integer(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBitNumber)) == null);
      _outputKeys.add(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit);
      
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayCamerasSyntheticTriggerOutputBit,
      new Integer(32)) == null);
      _outputKeys.add(_xrayCamerasSyntheticTriggerOutputBit);
        
      // The value of _maximumOutputBitNumber should be the last output
      // bit assignment. Bit beyond this range should not exist.
      _maximumOutputBitNumber = 32;
    }
    else
    {    
      Assert.expect(_outputBitKeyToOutputBitIdMap.put(_xrayCamerasSyntheticTriggerOutputBit,
      new Integer(24)) == null);
      _outputKeys.add(_xrayCamerasSyntheticTriggerOutputBit);

      // The value of _maximumOutputBitNumber should be the last output
      // bit assignment. Bit beyond this range should not exist.
      _maximumOutputBitNumber = 24;
    }
  }

  /**
   * Assign the actual hardware address to an input bit.
   *
   * @author Rex Shang
   */
  protected void setInputBitIdToInputBitAddressMap()
  {
    _inputBitKeyToInputBitIdMap = new HashMap<String, Integer>();
    _inputKeys = new ArrayList<String>();

    // We expect put() to always reaturn null meaning the key can only get assigned once.
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftOuterBarrierClosedInputBit,
      new Integer(0)) == null);
    if (LeftOuterBarrier.isInstalled())
    {
      _inputKeys.add(_leftOuterBarrierClosedInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightOuterBarrierClosedInputBit,
      new Integer(1)) == null);
    if (RightOuterBarrier.isInstalled())
    {
      _inputKeys.add(_rightOuterBarrierClosedInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftOuterBarrierOpenInputBit,
      new Integer(2)) == null);
    if (LeftOuterBarrier.isInstalled())
    {
      _inputKeys.add(_leftOuterBarrierOpenInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightOuterBarrierOpenInputBit,
      new Integer(3)) == null);
    if (RightOuterBarrier.isInstalled())
    {
      _inputKeys.add(_rightOuterBarrierOpenInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelClearSensorInputBit,
      new Integer(4)) == null);
    _inputKeys.add(_leftPanelClearSensorInputBit);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelClearSensorInputBit,
      new Integer(5)) == null);
    _inputKeys.add(_rightPanelClearSensorInputBit);

    if (isOpticalStopControllerInstalled())
    {
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceOpticalSensorEngagedInputBit,
        new Integer(6)) == null);
      _inputKeys.add(_leftPanelInPlaceOpticalSensorEngagedInputBit);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceOpticalSensorEngagedInputBit,
        new Integer(7)) == null);
      _inputKeys.add(_rightPanelInPlaceOpticalSensorEngagedInputBit);

      Assert.expect(_inputBitKeyToInputBitIdMap.put(_opticalStopControllerInstalledInputBit,
        new Integer(29)) == null);
      _inputKeys.add(_opticalStopControllerInstalledInputBit);
    }
    else
    {
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorEngagedInputBit,
        new Integer(6)) == null);
      _inputKeys.add(_leftPanelInPlaceSensorEngagedInputBit);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorEngagedInputBit,
        new Integer(7)) == null);
      _inputKeys.add(_rightPanelInPlaceSensorEngagedInputBit);
    }

    // Depending on whether interlock service is in service mode, the next
    // 6 bits mean different things. So for _inputKeys, let's just put in
    // the more generic input label.
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockServiceModeOnInputBit,
      new Integer(15)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockServiceModeOnInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockPowerOnInputBit,
      new Integer(14)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockPowerOnInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit5InputBit,
      new Integer(13)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit5InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit4InputBit,
      new Integer(12)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit4InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit3InputBit,
      new Integer(11)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit3InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit2InputBit,
      new Integer(10)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit2InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit1InputBit,
      new Integer(9)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit1InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockSwitchIdBit0InputBit,
      new Integer(8)) == null);
    if (Interlock.isInstalled())
    {
      _inputKeys.add(_interlockSwitchIdBit0InputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockChain1OpenInputBit,
      new Integer(13)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockChain2OpenInputBit,
      new Integer(12)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockHardwareIdBit3InputBit,
      new Integer(11)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockHardwareIdBit2InputBit,
      new Integer(10)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockHardwareIdBit1InputBit,
      new Integer(9)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_interlockHardwareIdBit0InputBit,
      new Integer(8)) == null);

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_innerBarrierClosedInputBit,
      new Integer(16)) == null);
    if (InnerBarrier.isInstalled())
    {
      _inputKeys.add(_innerBarrierClosedInputBit);
    }
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_innerBarrierOpenInputBit,
      new Integer(17)) == null);
    if (InnerBarrier.isInstalled())
    {
      _inputKeys.add(_innerBarrierOpenInputBit);
    }

    if(XrayTester.isS2EXEnabled())
    {
       Assert.expect(_inputBitKeyToInputBitIdMap.put(_zAxisAtReadySafePositionInputBit,
        new Integer(18)) == null);
        _inputKeys.add(_zAxisAtReadySafePositionInputBit);

      Assert.expect(_inputBitKeyToInputBitIdMap.put(_zAxisEnableInputBit,
        new Integer(19)) == null);
        _inputKeys.add(_zAxisEnableInputBit);
    }
    else
    {
      //Variable Mag Anthony August 2011
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCylinderUpInputBit,
        new Integer(18)) == null);
      if (XrayCylinderActuator.isInstalled())
      {
        _inputKeys.add(_xrayCylinderUpInputBit);
      }

      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCylinderDownInputBit,
        new Integer(19)) == null);
      if (XrayCylinderActuator.isInstalled())
      {
        _inputKeys.add(_xrayCylinderDownInputBit);
      }
    }

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayLeftSafetyLevelSensorInputBit,
      new Integer(20)) == null);
    if (XrayActuator.isInstalled())
    {
      _inputKeys.add(_xrayLeftSafetyLevelSensorInputBit);
    }

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayRightSafetyLevelSensorInputBit,
      new Integer(21)) == null);
    if (XrayActuator.isInstalled())
    {
      _inputKeys.add(_xrayRightSafetyLevelSensorInputBit);
    }

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_downStreamSystemReadyToAcceptPanelInputBit,
      new Integer(22)) == null);
    _inputKeys.add(_downStreamSystemReadyToAcceptPanelInputBit);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelInAvailableInputBit,
      new Integer(23)) == null);
    _inputKeys.add(_panelInAvailableInputBit);

    //XXL Based Anthony Jan 2013
    // one slide has 8 input bits
    // below are slides number 6, so from slide number 5 jump to slides number 6,
    // we need to start from input bit 24 --> [ (6-3)*8 = 24 ]
    //S2EX based Anthony 2014
    // Add in Slice IO S9 input bit 32 --> [ (7-3)*8 = 32 ]
    if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      if (isOpticalStopControllerInstalled())
      {
        if (isOpticalStopControllerEndStopperInstalled())
        {
          //S8-6(30) and S8-7(31) use for Optical Stop Controller Done and Error signal
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_opticalStopControllerDoneInputBit,
            new Integer(30)) == null);
          _inputKeys.add(_opticalStopControllerDoneInputBit);

          Assert.expect(_inputBitKeyToInputBitIdMap.put(_opticalStopControllerErrorInputBit,
            new Integer(31)) == null);
          _inputKeys.add(_opticalStopControllerErrorInputBit);
          if (XrayTester.isS2EXEnabled())
          {
            //Simulated signal for S2Ex
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_enableXXLOpticalStopControllerInputBit,
              new Integer(_enableS2EXOpticalStopControllerInputBitNumber)) == null);
            _inputKeys.add(_enableXXLOpticalStopControllerInputBit);
          }
          else
          {
            //Simulated signal for XXL
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_enableXXLOpticalStopControllerInputBit,
              new Integer(_enableXXLOpticalStopControllerInputBitNumber)) == null);
            _inputKeys.add(_enableXXLOpticalStopControllerInputBit);            
          }
          if (isOpticalStopControllerEndStopperResumedAsNormalPIP())
          {
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorExtendInputBit,
              new Integer(24)) == null);
            _inputKeys.add(_leftPanelInPlaceSensorExtendInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorRetractInputBit,
              new Integer(25)) == null);
            _inputKeys.add(_leftPanelInPlaceSensorRetractInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorExtendInputBit,
              new Integer(26)) == null);
            _inputKeys.add(_rightPanelInPlaceSensorExtendInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorRetractInputBit,
              new Integer(27)) == null);
            _inputKeys.add(_rightPanelInPlaceSensorRetractInputBit);

          }
          else
          {
            //Restore back PIP cylinder as end stopper
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceEndStopperExtendInputBit,
              new Integer(24)) == null);
            _inputKeys.add(_leftPanelInPlaceEndStopperExtendInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceEndStopperRetractInputBit,
              new Integer(25)) == null);
            _inputKeys.add(_leftPanelInPlaceEndStopperRetractInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceEndStopperExtendInputBit,
              new Integer(26)) == null);
            _inputKeys.add(_rightPanelInPlaceEndStopperExtendInputBit);

            Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceEndStopperRetractInputBit,
              new Integer(27)) == null);
            _inputKeys.add(_rightPanelInPlaceEndStopperRetractInputBit);
          }
        }
        else
        {

          Assert.expect(_inputBitKeyToInputBitIdMap.put(_opticalStopControllerDoneInputBit,
            new Integer(24)) == null);
          _inputKeys.add(_opticalStopControllerDoneInputBit);

          Assert.expect(_inputBitKeyToInputBitIdMap.put(_opticalStopControllerErrorInputBit,
            new Integer(25)) == null);
          _inputKeys.add(_opticalStopControllerErrorInputBit);
          if (XrayTester.isS2EXEnabled())
          {
            //Simulated signal
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_enableXXLOpticalStopControllerInputBit,
              new Integer(_enableS2EXOpticalStopControllerInputBitNumber)) == null);
            _inputKeys.add(_enableXXLOpticalStopControllerInputBit);
          }
          else
          {
            //Simulated signal
            Assert.expect(_inputBitKeyToInputBitIdMap.put(_enableXXLOpticalStopControllerInputBit,
              new Integer(_enableXXLOpticalStopControllerInputBitNumber)) == null);
            _inputKeys.add(_enableXXLOpticalStopControllerInputBit);
          }
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorExtendInputBit,
            new Integer(44)) == null);
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorRetractInputBit,
            new Integer(45)) == null);
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorExtendInputBit,
            new Integer(46)) == null);
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorRetractInputBit,
            new Integer(47)) == null);
        }
        
        if (isOpticalPIPResetInstalled())
        {
          //Simulated signal for Optical PIP Reset - Swee Yee
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_resetOpticalPIPSensorInputBit,
            new Integer(_resetOpticalPIPSensorInputBitNumber)) == null);
          _inputKeys.add(_resetOpticalPIPSensorInputBit);
        }
      }
      else
      {
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorExtendInputBit,
          new Integer(24)) == null);
        _inputKeys.add(_leftPanelInPlaceSensorExtendInputBit);

        Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorRetractInputBit,
          new Integer(25)) == null);
        _inputKeys.add(_leftPanelInPlaceSensorRetractInputBit);

        Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorExtendInputBit,
          new Integer(26)) == null);
        _inputKeys.add(_rightPanelInPlaceSensorExtendInputBit);

        Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorRetractInputBit,
          new Integer(27)) == null);
        _inputKeys.add(_rightPanelInPlaceSensorRetractInputBit);
      }

      Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelUnclampsInputBit,
        new Integer(28)) == null);
      _inputKeys.add(_panelUnclampsInputBit);

     if(XrayTester.isS2EXEnabled())
     {
       Assert.expect(_inputBitKeyToInputBitIdMap.put(_zAxisAtMag1PositionInputBit,
          new Integer(32)) == null);
        _inputKeys.add(_zAxisAtMag1PositionInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_zAxisAtMag2PositionInputBit,
          new Integer(33)) == null);
        _inputKeys.add(_zAxisAtMag2PositionInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_zAxisAtMag3PositionInputBit,
          new Integer(34)) == null);
        _inputKeys.add(_zAxisAtMag3PositionInputBit);
        
        if(isXrayMotorizedHeightLevelSafetySensorInstalled())
        {            
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBit,
            new Integer(_xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBitNumber)) == null);
          _inputKeys.add(_xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBit);
          
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBit,
            new Integer(_xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBitNumber)) == null);
          _inputKeys.add(_xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBit);
          
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBit,
            new Integer(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBitNumber)) == null);
          _inputKeys.add(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBit);
        }  
        
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasConnectedInputBit,
          new Integer(40)) == null);
        _inputKeys.add(_xrayCamerasConnectedInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPowerOnInputBit,
          new Integer(41)) == null);
        _inputKeys.add(_xrayCamerasPowerOnInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasUsingExcessivePowerInputBit,
          new Integer(42)) == null);
        _inputKeys.add(_xrayCamerasUsingExcessivePowerInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPhaseLockLoopOnInputBit,
          new Integer(43)) == null);
        _inputKeys.add(_xrayCamerasPhaseLockLoopOnInputBit); 
        
        // Kok Chun, Tan - XCR-3535 - log smema conveyor sensor signal
        if (Config.isDeveloperDebugModeOn() || Config.getInstance().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE))
        {
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_smemaSignalOneDebugInputBit,
            new Integer(38)) == null);
          _inputKeys.add(_smemaSignalOneDebugInputBit); 
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_smemaSignalTwoDebugInputBit,
            new Integer(39)) == null);
          _inputKeys.add(_smemaSignalTwoDebugInputBit); 
        }
        
        // The value of _maximumInputBitNumber should be the last input
        // bit assignment. Bit beyond this range should only exist for simulation.
        _maximumInputBitNumber = 43;
     }
     else if(XrayTester.isXXLBased())
     {
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasConnectedInputBit,
          new Integer(32)) == null);
        _inputKeys.add(_xrayCamerasConnectedInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPowerOnInputBit,
          new Integer(33)) == null);
        _inputKeys.add(_xrayCamerasPowerOnInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasUsingExcessivePowerInputBit,
          new Integer(34)) == null);
        _inputKeys.add(_xrayCamerasUsingExcessivePowerInputBit);
        Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPhaseLockLoopOnInputBit,
          new Integer(35)) == null);
        _inputKeys.add(_xrayCamerasPhaseLockLoopOnInputBit);
        // Kok Chun, Tan - XCR-3535 - log smema conveyor sensor signal
        if (Config.isDeveloperDebugModeOn() || Config.getInstance().getBooleanValue(SoftwareConfigEnum.PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE))
        {
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_smemaSignalOneDebugInputBit,
            new Integer(29)) == null);
          _inputKeys.add(_smemaSignalOneDebugInputBit);
          Assert.expect(_inputBitKeyToInputBitIdMap.put(_smemaSignalTwoDebugInputBit,
            new Integer(30)) == null);
          _inputKeys.add(_smemaSignalTwoDebugInputBit);
        }
        // The value of _maximumInputBitNumber should be the last input
        // bit assignment. Bit beyond this range should only exist for simulation.
        _maximumInputBitNumber = 35;
     }

      Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelClampsInputBit,
        new Integer(49)) == null);
    }
    else
    {
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasConnectedInputBit,
        new Integer(24)) == null);
      _inputKeys.add(_xrayCamerasConnectedInputBit);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPowerOnInputBit,
        new Integer(25)) == null);
      _inputKeys.add(_xrayCamerasPowerOnInputBit);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasUsingExcessivePowerInputBit,
        new Integer(26)) == null);
      _inputKeys.add(_xrayCamerasUsingExcessivePowerInputBit);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasPhaseLockLoopOnInputBit,
        new Integer(27)) == null);
      _inputKeys.add(_xrayCamerasPhaseLockLoopOnInputBit);
      // The value of _maximumInputBitNumber should be the last input
      // bit assignment. Bit beyond this range should only exist for simulation.
      _maximumInputBitNumber = 27;

      // Input bits that we want to have but not availabe. Put the bit assignment
      // in a range where real hardware doesn't exist.
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_leftPanelInPlaceSensorExtendInputBit,
        new Integer(46)) == null);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_rightPanelInPlaceSensorExtendInputBit,
        new Integer(47)) == null);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelClampsInputBit,
        new Integer(49)) == null);
    }

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_stageBeltsInputBit,
      new Integer(50)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_stageBeltDirectionInputBit,
      new Integer(51)) == null);
    
    if (XrayTester.isS2EXEnabled() == false)
    {
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_stageViewingLightInputBit,
        new Integer(52)) == null);
    }

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_lightStackRedLightInputBit,
      new Integer(53)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_lightStackYellowLightInputBit,
      new Integer(54)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_lightStackGreenLightInputBit,
      new Integer(55)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_lightStackBuzzerInputBit,
      new Integer(56)) == null);

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayTesterReadyToAcceptPanelInputBit,
      new Integer(60)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelOutAvailableInputBit,
      new Integer(61)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelOutGoodInputBit,
      new Integer(62)) == null);
    Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelOutUntestedInputBit,
      new Integer(63)) == null);

    Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCamerasSyntheticTriggerInputBit,
      new Integer(64)) == null);
    if (Interlock.getInterLockLogicBoardWithKeylock() == false)
    {
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelFrontLeftDoorLockInputBit,
        new Integer(65)) == null);
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_panelFrontRightDoorLockInputBit,
        new Integer(66)) == null);
    }
    
    if(XrayTester.isS2EXEnabled())
    {
      
      if(isXrayMotorizedHeightLevelSafetySensorInstalled())
      { 

         // Input bits that we want to have but not availabe. Put the bit assignment
         // in a range where real hardware doesn't exist.
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmSimulatedIuputBit,
           new Integer(68)) == null);
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerHomeSimulatedIuputBit,
           new Integer(69)) == null);
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerStartSimulatedIuputBit,
           new Integer(70)) == null);
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit0SimulatedIuputBit,
           new Integer(71)) == null);
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit1SimulatedIuputBit,
           new Integer(72)) == null);
         Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit2SimulatedIuputBit,
           new Integer(73)) == null);

         // _numberOfInputBits needs to be greater than the last integer
         // assigned to an input bit.
         
        //Note*  _enableS2EXOpticalStopControllerInputBitNumber =74
        //Note*  _resetOpticalPIPSensorInputBitNumber = 75
         _numberOfInputBits = 76;
      }
      else
      {
         // _numberOfInputBits needs to be greater than the last integer
         // assigned to an input bit.
         _numberOfInputBits = 68;
      }
    }  
    else
    {
      //Variable Mag Anthony August 2011
      Assert.expect(_inputBitKeyToInputBitIdMap.put(_xrayCylinderClampInputBit,
        new Integer(67)) == null);

      // _numberOfInputBits needs to be greater than the last integer
      // assigned to an input bit.
      _numberOfInputBits = 68;
    }
  }

  /**
   * Associate the output bit with available input bit. This is used for
   * confirming output bit and for simulation.
   *
   * @author Rex Shang
   */
  protected void setOutputBitIdToInputBitIdMap()
  {
    _outputBitIdToInputBitIdMap = new HashMap<Integer, Integer>();

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_leftOuterBarrierOutputBit),
      getInputBitFromMap(_leftOuterBarrierOpenInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_rightOuterBarrierOutputBit),
      getInputBitFromMap(_rightOuterBarrierOpenInputBit)) == null);

    if (isOpticalStopControllerInstalled())
    {
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_enableXXLOpticalStopControllerOutputBit),
        getInputBitFromMap(_enableXXLOpticalStopControllerInputBit)) == null);
      
      if (isOpticalPIPResetInstalled())
      {
        //Simulated signal for Optical PIP Reset - Swee Yee
        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_resetOpticalPIPSensorOutputBit),
          getInputBitFromMap(_resetOpticalPIPSensorInputBit)) == null);
      }

      if (isOpticalStopControllerEndStopperInstalled())
      {
        if (isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          Assert.expect(_outputBitIdToInputBitIdMap.put(
            getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit),
            getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit)) == null);
          Assert.expect(_outputBitIdToInputBitIdMap.put(
            getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit),
            getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit)) == null);
        }
        else
        {
          Assert.expect(_outputBitIdToInputBitIdMap.put(
            getOutputBitFromMap(_leftPanelInPlaceEndStopperOutputBit),
            getInputBitFromMap(_leftPanelInPlaceEndStopperExtendInputBit)) == null);
          Assert.expect(_outputBitIdToInputBitIdMap.put(
            getOutputBitFromMap(_rightPanelInPlaceEndStopperOutputBit),
            getInputBitFromMap(_rightPanelInPlaceEndStopperExtendInputBit)) == null);
        }
      }
    }
    else
    {
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit),
        getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit)) == null);
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit),
        getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit)) == null);
    }

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_runStageBeltsOutputBit),
      getInputBitFromMap(_stageBeltsInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_stageBeltDirectionOutputBit),
      getInputBitFromMap(_stageBeltDirectionInputBit)) == null);
    
    if (XrayTester.isS2EXEnabled() == false)
    {
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_stageViewingLightOutputBit),
        getInputBitFromMap(_stageViewingLightInputBit)) == null);
    }

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_lightStackRedLightOutputBit),
      getInputBitFromMap(_lightStackRedLightInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_lightStackYellowLightOutputBit),
      getInputBitFromMap(_lightStackYellowLightInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_lightStackGreenLightOutputBit),
      getInputBitFromMap(_lightStackGreenLightInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_lightStackBuzzerOutputBit),
      getInputBitFromMap(_lightStackBuzzerInputBit)) == null);
 
    if(XrayTester.isS2EXEnabled())
    {

        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_zAxisEnableOutputBit),
          getInputBitFromMap(_zAxisEnableInputBit)) == null);
      
        //???
        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_zAxisPositionBitAOutputBit),
          getInputBitFromMap(_zAxisAtMag1PositionInputBit)) == null);
        
        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_zAxisPositionBitBOutputBit),
          getInputBitFromMap(_zAxisAtMag2PositionInputBit)) == null);
    } 
    else
    {
      //Variable Mag Anthony August 2011
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayCylinderDownOutputBit),
        getInputBitFromMap(_xrayCylinderDownInputBit)) == null);

      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayCylinderClampOutputBit),
        getInputBitFromMap(_xrayCylinderClampInputBit)) == null);
    
      if (Interlock.getInterLockLogicBoardWithKeylock() == false)
      {
        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_panelFrontLeftDoorLockOutputBit),
          getInputBitFromMap(_panelFrontLeftDoorLockInputBit)) == null);
        Assert.expect(_outputBitIdToInputBitIdMap.put(
          getOutputBitFromMap(_panelFrontRightDoorLockOutputBit),
          getInputBitFromMap(_panelFrontRightDoorLockInputBit)) == null);
      }
    }

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_xrayTesterReadyToAcceptPanelOutputBit),
      getInputBitFromMap(_xrayTesterReadyToAcceptPanelInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_panelOutAvailableOutputBit),
      getInputBitFromMap(_panelOutAvailableInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_panelOutGoodOutputBit),
      getInputBitFromMap(_panelOutGoodInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_panelOutUntestedOutputBit),
      getInputBitFromMap(_panelOutUntestedInputBit)) == null);

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_xrayCamerasSyntheticTriggerOutputBit),
      getInputBitFromMap(_xrayCamerasSyntheticTriggerInputBit)) == null);

    // Note the inner barrier is hooked up somewhat strangely...
    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_innerBarrierOutputBit),
      getInputBitFromMap(_innerBarrierClosedInputBit)) == null);

    _outputBitIdToInputBitIdMapForTheInverted = new HashMap<Integer, Integer>();

    Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
      getOutputBitFromMap(_leftOuterBarrierOutputBit),
      getInputBitFromMap(_leftOuterBarrierClosedInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
      getOutputBitFromMap(_rightOuterBarrierOutputBit),
      getInputBitFromMap(_rightOuterBarrierClosedInputBit)) == null);
    Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
      getOutputBitFromMap(_innerBarrierOutputBit),
      getInputBitFromMap(_innerBarrierOpenInputBit)) == null);

    //Variable Mag Anthony August 2011
    if (XrayCylinderActuator.isInstalled())
    {
      Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
        getOutputBitFromMap(_xrayCylinderDownOutputBit),
        getInputBitFromMap(_xrayCylinderUpInputBit)) == null);
    }

    Assert.expect(_outputBitIdToInputBitIdMap.put(
      getOutputBitFromMap(_panelClampsOutputBit),
      getInputBitFromMap(_panelClampsInputBit)) == null);

    //XXL Based Anthony Jan 2013
    if (XrayTester.isXXLBased() == true||XrayTester.isS2EXEnabled())
    {
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_panelUnclampsOutputBit),
        getInputBitFromMap(_panelUnclampsInputBit)) == null);
      if (isOpticalStopControllerInstalled())
      {
        if (isOpticalStopControllerEndStopperInstalled())
        {
          if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
          {
            Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
              getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit),
              getInputBitFromMap(_leftPanelInPlaceSensorRetractInputBit)) == null);
            Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
              getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit),
              getInputBitFromMap(_rightPanelInPlaceSensorRetractInputBit)) == null);

          }
          else
          {
            Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
              getOutputBitFromMap(_leftPanelInPlaceEndStopperOutputBit),
              getInputBitFromMap(_leftPanelInPlaceEndStopperRetractInputBit)) == null);
            Assert.expect(_outputBitIdToInputBitIdMapForTheInverted.put(
              getOutputBitFromMap(_rightPanelInPlaceEndStopperOutputBit),
              getInputBitFromMap(_rightPanelInPlaceEndStopperRetractInputBit)) == null);

          }
        }
      }
    }
    if(isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmSimulatedIuputBit)) == null);
      
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorHomingOutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerHomeSimulatedIuputBit)) == null);
      
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerStartSimulatedIuputBit)) == null);
      
      
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit0SimulatedIuputBit)) == null);
      
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit1SimulatedIuputBit)) == null);
        
      Assert.expect(_outputBitIdToInputBitIdMap.put(
        getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit),
        getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerPositionBit2SimulatedIuputBit)) == null);    
    }

    Assert.expect(_outputBitKeyToOutputBitIdMap.size() == _outputBitIdToInputBitIdMap.size(),
      "_outputBitKeyToOutputBitIdMap should contain the same number of elements as _outputBitIdToInputBitIdMap " + _outputBitKeyToOutputBitIdMap.size() + " vs " + _outputBitIdToInputBitIdMap.size());
  }

  /**
   * Used by Service GUI to get the available output bit keys. It will then make
   * a query to this class to get/set the current state information.
   *
   * @author Rex Shang
   */
  public List<String> getOutputBitKeys()
  {
    return _outputKeys;
  }

  /**
   * Used by Service GUI to get the available input bit keys. It will then make
   * a query to this class to get the current state information.
   *
   * @author Rex Shang
   */
  public List<String> getinputBitKeys()
  {
    return _inputKeys;
  }

  /**
   * Tell this hardware to perform any actions required to startup on behalf of
   * the XrayTester object.
   *
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    _initialized = false;
    // Pass configuration information and start hardware.
    initialize();
    
    //XXL may not have optical PIP handling, but S2EX always with optical PIP handling and without OpticalStopControllerInstalledBasedOnDigitalInput signal
    if (XrayTester.isXXLBased() && isSimulationModeOn() == false)
    {
      if (isOpticalStopControllerInstalled())
      {
        if (isOpticalStopControllerInstalledBasedOnDigitalInput() == false)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoOpticalStopSensorNotInitializedException();
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
      }
    }
  }

  /**
   * Tell this hardware to perform any actions required to shutdown on behalf of
   * the XrayTester object.
   *
   * @author Rex Shang
   */
  public void shutdown() throws XrayTesterException
  {
    try
    {
      // TODO have simulation mode actually exercise RMI layer
      if (_initialized && isSimulationModeOn() == false)
      {
        if (_remoteMotionServer.isStartupRequired())
        {
          _remoteMotionServer.startup();
        }

        _rdioi.remoteShutdown();
      }
    }
    catch (NativeHardwareException nhe)
    {
      _initialized = false;
      DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
    catch (RemoteException re)
    {
      if (_debug)
      {
        System.out.println("shutdown re:" + re.getMessage());
      }
      // try again 1 times
      // wei chin
      try
      {
        waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS);
        if (_initialized)
        {
          if (_remoteMotionServer.isStartupRequired())
          {
            _remoteMotionServer.startup();
          }
          _rdioi.remoteShutdown();
        }
      }
      catch (NativeHardwareException nhe)
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
      catch (RemoteException re2)
      {
        _initialized = false;
        if (_debug)
        {
          System.out.println("shutdown re:" + re2.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re2));
      }
    }
    finally
    {
      _initialized = false;
//      _remoteMotionServer.forceRestart();
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    if (_initialized == false)
    {
      return true;
    }
    else
    {
      if (_rdioi == null)
      {
        return true;
      }

      return false;
    }
  }

  /**
   * Make sure hardware is initialized before we call get and set bits.
   *
   * @author Rex Shang
   */
  private synchronized void initialize() throws XrayTesterException
  {
    if (_initialized)
    {
      return;
    }

    _hardwareObservable.stateChangedBegin(this, DigitalIoEventEnum.INITIALIZE);
    _hardwareObservable.setEnabled(false);
    _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.DIGITAL_IO_INITIALIZE, _STARTUP_TIME_IN_MILLISECONDS);

    try
    {
      // TODO have simulation mode actually exercise RMI layer
      if (_remoteMotionServer.isStartupRequired())
      {
        _remoteMotionServer.startup();
      }

      if (isAborting())
      {
        return;
      }

      initializeInputBitField();

      if (isAborting())
      {
        return;
      }

      if (isSimulationModeOn() == false)
      {
        // XCR-3604 Unit Test Phase 2
        try
        {
          NativeHardwareConfiguration.getInstance().setConfigurationParameters();
        }
        catch (RemoteException re)
        {
          _initialized = false;
          _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re));
        }

        try
        {
          if (isAborting())
          {
            return;
          }

          _rdioi.remoteInitialize();
        }
        catch (NativeHardwareException nhe)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        catch (RemoteException re)
        {
          if (_debug)
          {
            System.out.println("initialize re:" + re.getMessage());
          }
          // try again 1 times
          // wei chin
          try
          {
            waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS);
            _rdioi.remoteInitialize();
          }
          catch (NativeHardwareException nhe)
          {
            _initialized = false;
            DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
            _hardwareTaskEngine.throwHardwareException(dioe);
          }
          catch (RemoteException re2)
          {
            _initialized = false;
            if (_debug)
            {
              System.out.println("initialize re2:" + re2.getMessage());
            }
            _remoteMotionServer.stopRMS();
            _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re2));
          }
        }
        catch (Exception e)
        {
          _initialized = false;
          if (_debug)
          {
            System.out.println("e:" + e.getMessage());
          }
          DigitalIoException dioe = DigitalIoException.getGeneralException(e);
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
      }

      if (isAborting())
      {
        return;
      }

      setConfigurationDescription();
      _initialized = true;
      
      //Swee Yee - move inner barrier and xray cylinder initialize from startup to initialize
      // Initilize  Inner Barrier and XrayCylinder
      if (XrayCylinderActuator.isInstalled() || XrayCPMotorActuator.isInstalled())
      {
        try
        {
          setOutputBitState(getOutputBitFromMap(_innerBarrierOutputBit).intValue(), _openInnerBarrier);
          if (XrayTester.isS2EXEnabled() && XrayCPMotorActuator.isInstalled())
          {
            setOutputBitState(getOutputBitFromMap(_zAxisEnableOutputBit).intValue(), _turnZAxisEnableON);

            //if(XrayMotorizedHeightLevelSensor.isInstalled())
            if (isXrayMotorizedHeightLevelSafetySensorInstalled())
            {
              XrayMotorizedHeightLevelSensor.getInstance().startupHoming();
            }

            //Swee Yee - do one startup homing instead of two
            //S2EX Anthony 2014
            //Inner barrier is opened by default for VM safety purpose (when power OFF)
            //So, need to closed inner barrier after the ZAxis go home
            //Do not call this XrayCPMotorActuator.getInstance().home();
            //For safety purpose, we need to perform double home during startup only
            XrayCPMotorActuator.getInstance().startupHoming();

            closeInnerBarrier();
            setOutputBitState(getOutputBitFromMap(_innerBarrierOutputBit).intValue(), _closeInnerBarrier);

            setOutputBitState(getOutputBitFromMap(_zAxisEnableOutputBit).intValue(), _turnZAxisEnableON);
          }
          else
          {
            setOutputBitState(getOutputBitFromMap(_xrayCylinderClampOutputBit).intValue(), _turnXRayCylinderUnClamp);
            setOutputBitState(getOutputBitFromMap(_xrayCylinderDownOutputBit).intValue(), _turnXRayCylinderDownOff);

            //XCR1775- Interlock safety checking- Anthony 
            //Inner barrier is opened by default for VM safety purpose (when power OFF)
            //So, need to closed inner barrier after the XrayCylinder go up
            XrayCylinderActuator.getInstance().up(false);
            closeInnerBarrier();
            setOutputBitState(getOutputBitFromMap(_innerBarrierOutputBit).intValue(), _closeInnerBarrier);
          }

        }
        catch (XrayTesterException xte)
        {
          //do not set _initialized = false since z-axis homing is not part of digitalIO initialization.
          //_initialized = false;
          _hardwareTaskEngine.throwHardwareException(xte);
        }
      }
      else
      {
        //XCR1775- Interlock safety checking for no VM system- Anthony 
        if (InnerBarrier.isInstalled())
        {
          closeInnerBarrier();
          setOutputBitState(getOutputBitFromMap(_innerBarrierOutputBit).intValue(), _closeInnerBarrier);
        }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.DIGITAL_IO_INITIALIZE);
    }

    _hardwareObservable.stateChangedEnd(this, DigitalIoEventEnum.INITIALIZE);
  }

  /**
   * Change the reset behavior of DigitalIo while starting up. By default, reset
   * is true.
   *
   * @param hardwareResetModeOn true to have the DigitalIO hardware reset while
   * starting up.
   * @author Rex Shang
   */
  public void setHardwareResetMode(boolean hardwareResetModeOn) throws XrayTesterException
  {
    if (isSimulationModeOn() == false)
    {
      try
      {
        if (_remoteMotionServer.isStartupRequired())
        {
          _remoteMotionServer.startup();
        }
//        connectToDigitalIoServer();

        if (isAborting())
        {
          return;
        }

        _rdioi.remoteSetHardwareResetMode(hardwareResetModeOn);
      }
      catch (NativeHardwareException nhe)
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
      catch (RemoteException re)
      {
        _initialized = false;
        // try again 1 times
        // wei chin
        try
        {
          waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS);
          _rdioi.remoteSetHardwareResetMode(hardwareResetModeOn);
        }
        catch (NativeHardwareException nhe)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        catch (RemoteException re2)
        {
          _initialized = false;
          if (_debug)
          {
            System.out.println("setHardwareResetMode re2:" + re2.getMessage());
          }
          _remoteMotionServer.stopRMS();
          _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re2));
        }
//      }
//      catch (NotBoundException e)
//      {
//        _initialized = false;
//        _hardwareTaskEngine.throwHardwareException(DigitalIoException.getNotBoundException(e));
//      }
//      catch(Exception e)
//      {
//        _initialized = false;
//        if(_debug)
//          System.out.println("e:" + e.getMessage());
//        DigitalIoException dioe = DigitalIoException.getGeneralException(e);
//        _hardwareTaskEngine.throwHardwareException(dioe);
      }
    }
  }

  /**
   * This method is protected so different DigitalIo can override this. For
   * simulation to work, make sure to initialize the status if 0 is not the
   * initial state.
   *
   * @author Rex Shang
   */
  protected void initializeInputBitField()
  {
    _inputBitField = new ArrayList<Integer>(_numberOfInputBits);

    // Initialize default state.
    for (int i = 0; i < _numberOfInputBits; i++)
    {
      _inputBitField.add(i, new Integer(0));
    }

    // Set alternate initial state for the few.
    setAlternateInitialState(_rightOuterBarrierClosedInputBit);
    setAlternateInitialState(_leftOuterBarrierClosedInputBit);
    setAlternateInitialState(_innerBarrierClosedInputBit);
    
    //S2EX Anthony 2014
    if (XrayCPMotorActuator.isInstalled())
    {
      //setAlternateInitialState(_zAxisAtMag1PositionInputBit);
      setAlternateInitialState(_zAxisAtReadySafePositionInputBit);
    }
    
    //Variable Mag Anthony August 2011
    if (XrayCylinderActuator.isInstalled())
    {
      setAlternateInitialState(_xrayCylinderUpInputBit);
    }
    setAlternateInitialState(_xrayLeftSafetyLevelSensorInputBit);
    setAlternateInitialState(_xrayRightSafetyLevelSensorInputBit);

    if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      setAlternateInitialState(_panelUnclampsInputBit);
      
      if ((isOpticalStopControllerInstalled()==true) && 
         (isOpticalStopControllerEndStopperInstalled() == true))
      {
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          setAlternateInitialState(_leftPanelInPlaceSensorRetractInputBit);
          setAlternateInitialState(_rightPanelInPlaceSensorRetractInputBit);
        }
        else
        {
          setAlternateInitialState(_leftPanelInPlaceEndStopperRetractInputBit);
          setAlternateInitialState(_rightPanelInPlaceEndStopperRetractInputBit);
        }
      }
      else
      {
        setAlternateInitialState(_leftPanelInPlaceSensorRetractInputBit);
        setAlternateInitialState(_rightPanelInPlaceSensorRetractInputBit);       
      }

    }

    setAlternateInitialState(_interlockServiceModeOnInputBit);
    setAlternateInitialState(_interlockPowerOnInputBit);
    setAlternateInitialState(_interlockChain1OpenInputBit);
    setAlternateInitialState(_interlockChain2OpenInputBit);
    setAlternateInitialState(_xrayCamerasPowerOnInputBit);
    setAlternateInitialState(_xrayCamerasConnectedInputBit);

    /**
     * @todo RMS the next few lines of code need to be clean up after we
     * complete to transition to active low PCS.
     */
    if (PanelClearSensor.isActiveLowTypeInstalled())
    {
      setAlternateInitialState(_rightPanelClearSensorInputBit);
      setAlternateInitialState(_leftPanelClearSensorInputBit);
    }
    // Set revision code into simulation bit field based on hardware.config.
    int interlockRev = Interlock.getHardwareRevisionIdFromConfiguration();
    if (((interlockRev >> 0) & 1) != 0)
    {
      setAlternateInitialState(_interlockHardwareIdBit0InputBit);
    }
    if (((interlockRev >> 1) & 1) != 0)
    {
      setAlternateInitialState(_interlockHardwareIdBit1InputBit);
    }
    if (((interlockRev >> 2) & 1) != 0)
    {
      setAlternateInitialState(_interlockHardwareIdBit2InputBit);
    }
    if (((interlockRev >> 3) & 1) != 0)
    {
      setAlternateInitialState(_interlockHardwareIdBit3InputBit);
    }
  }

  /**
   * Used to set alternate initial state.
   *
   * @author Rex Shang
   */
  private void setAlternateInitialState(String bitKey)
  {
    Integer bitId;

    bitId = getInputBitFromMap(bitKey);
    _inputBitField.set(bitId.intValue(), new Integer(1));
  }

  /**
   * @return long the hardware polling interval in milli seconds
   * @author Rex Shang
   */
  static long getHardwarePollingIntervalInMilliSeconds()
  {
    return _HARDWARE_POLLING_INTERVAL_IN_MILLI_SECONDS;
  }

  /**
   * This one is used to special circumstances where the normal polling interval
   * (which is pretty fast already) is not fast enough. We need to push the
   * limit of our digital io hardware.
   *
   * @return long the hardware polling interval in milli seconds
   * @author Rex Shang
   */
  static long getFastHardwarePollingIntervalInMilliSeconds()
  {
    return _FAST_HARDWARE_POLLING_INTERVAL_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getOuterBarrierOpenTimeOutInMilliSeconds()
  {
    return _OUTER_BARRIER_OPENING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getOuterBarrierCloseTimeOutInMilliSeconds()
  {
    return _OUTER_BARRIER_CLOSING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getOuterBarrierSettlingTimeOutInMilliSeconds()
  {
    return _OUTER_BARRIER_SETTLING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getInnerBarrierOpenTimeOutInMilliSeconds()
  {
    return _INNER_BARRIER_OPENING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getInnerBarrierCloseTimeOutInMilliSeconds()
  {
    return _INNER_BARRIER_CLOSING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getInnerBarrierSettlingTimeOutInMilliSeconds()
  {
    return _INNER_BARRIER_SETTLING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Wei Chin
   */
  long getPanelClampSettlingTimeOutInMilliSeconds()
  {
    return _PANEL_CLAMPS_SETTLING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Anthony Fong
   */
  long getPanelClampCylinderTimeOutInMilliSeconds()
  {
    return _PANEL_CLAMP_CYLINDER_TIMEOUT_IN_MILLI_SECONDS;
  }

  /**
   * @author Anthony Fong
   */
  long getPanelInPlaceCylinderTimeOutInMilliSeconds()
  {
    return _PANEL_IN_PLACE_CYLINDER_TIMEOUT_IN_MILLI_SECONDS;
  }

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  long getXrayCylinderUpTimeOutInMilliSeconds()
  {
    return _XRAY_CYLINDER_UP_TIME_IN_MILLI_SECONDS;
  }
  //Variable Mag Anthony August 2011

  /**
   * @author Anthony Fong
   */
  long getXrayCylinderDownTimeOutInMilliSeconds()
  {
    return _XRAY_CYLINDER_DOWN_TIME_IN_MILLI_SECONDS;
  }
  //Variable Mag Anthony August 2011  

  /**
   * @author Anthony Fong
   */
  long getXrayCylinderSettlingTimeOutInMilliSeconds()
  {
    return _XRAY_CYLINDER_SETTLING_TIME_IN_MILLI_SECONDS;
  }
  
  //S2EX Anthony 2014
  /**
   * @author Anthony Fong
   */
  long getXrayZAxisMoveHomeTimeOutInMilliSeconds()
  {
    return _XRAY_Z_AXIS_MOVE_HOME_TIME_IN_MILLI_SECONDS;
  }
  /**
   * @author Anthony Fong
   */
  long getXrayZAxisMoveLowMagTimeOutInMilliSeconds()
  {
    return _XRAY_Z_AXIS_MOVE_LOW_MAG_TIME_IN_MILLI_SECONDS;
  } 
  /**
   * @author Anthony Fong
   */
  long getXrayZAxisMoveHighMagTimeOutInMilliSeconds()
  {
    return _XRAY_Z_AXIS_MOVE_HIGH_MAG_TIME_IN_MILLI_SECONDS;
  } 
  /**
   * @author Anthony Fong
   */
  long getXrayZAxisSettlingTimeOutInMilliSeconds()
  {
    return _XRAY_Z_AXIS_SETTLING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getStageBeltsStartMovingTimeInMilliSeconds()
  {
    return _STAGE_BELTS_START_MOVING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getStageBeltsStopMovingTimeInMilliSeconds()
  {
    return _STAGE_BELTS_STOP_MOVING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getPanelInPlaceSensorExtendingTimeInMilliSeconds()
  {
    return _PANEL_IN_PLACE_SENSOR_EXTENDING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getPanelInPlaceSensorRetractingTimeInMilliSeconds()
  {
    return _PANEL_IN_PLACE_SENSOR_RETRACTING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getPanelClampsOpeningTimeInMilliSeconds()
  {
    return _PANEL_CLAMPS_OPENING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Rex Shang
   */
  long getPanelClampsClosingTimeInMilliSeconds()
  {
    return _PANEL_CLAMPS_CLOSING_TIME_IN_MILLI_SECONDS;
  }

  /**
   * @author Roy Williams
   */
  long getSyntheticTriggersTurnoffTimeInMilliSeconds()
  {
    return _SYNTHETIC_TRIGGERS_TURNOFF_TIME_IN_MILLI_SECONDS;
  }

  /**
   * Re-initialize DigitalIo.
   *
   * @author Rex Shang
   */
  void reset() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, DigitalIoEventEnum.RESET);
    _hardwareObservable.setEnabled(false);

    try
    {
      _initialized = false;
      initialize();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, DigitalIoEventEnum.RESET);
  }

  /**
   * Open the inner barrier
   *
   * @author Rex Shang
   */
  public void openInnerBarrier() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
    boolean isPrevInnerBarrierCommandedClose = _isInnerBarrierCommandedClose;
    _isInnerBarrierCommandedClose = false;
    if (setOutputBitState(outputBit, _openInnerBarrier) == false)
    {
      _initialized = false;
      _isInnerBarrierCommandedClose = isPrevInnerBarrierCommandedClose;
      DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
    if(_debug)
      logHardwareStatus("Debug - DIO: openInnerBarrier - opening InnerBarrier");
  }

  /**
   * Open the left outer barrier
   *
   * @author Rex Shang
   */
  void openLeftOuterBarrier() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_leftOuterBarrierOutputBit).intValue();
    setOutputBitState(outputBit, _openLeftOuterBarrier);
  }

  /**
   * Open the right outer barrier
   *
   * @author Rex Shang
   */
  void openRightOuterBarrier() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_rightOuterBarrierOutputBit).intValue();
    setOutputBitState(outputBit, _openRightOuterBarrier);
  }

  /**
   * Close the inner barrier
   *
   * @author Rex Shang
   */
  public void closeInnerBarrier() throws XrayTesterException
  {
    boolean isPrevInnerBarrierCommandedClose = _isInnerBarrierCommandedClose;
    _isInnerBarrierCommandedClose = true;
    
    try
    {
      //Variable Mag Anthony August 2011
      if (XrayCylinderActuator.isInstalled() && XrayTester.isS2EXEnabled() == false)
      {
        if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - Using XrayCylinder, not S2EX machine");
        XrayCylinderActuator xrayCylinder = XrayCylinderActuator.getInstance();
        if (xrayCylinder.isHome()==false || _isXrayTubeCommandedHome == false)
        {
          if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - XrayCylinder is not at home position or commanded to move away from home position");
        //do not call _xraySource.offForServiceMode() here, this will cause infinite looping.
          //AbstractXraySource xraySource = AbstractXraySource.getInstance();
          //Assert.expect(xraySource instanceof HTubeXraySource);
          //HTubeXraySource _xraySource = (HTubeXraySource)xraySource;
          //if(_xraySource.areXraysOn())_xraySource.offForServiceMode();

          xrayCylinder.up(false);
          if (isXrayCylinderUp())
          {
            int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
            if (setOutputBitState(outputBit, _closeInnerBarrier) == false)
            {
              _initialized = false;
              DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
              _hardwareTaskEngine.throwHardwareException(dioe);
            }
            if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - closing InnerBarrier");
          }
          else
          {
            logHardwareStatus("ERROR - DIO: Inner barrier failed to close due to xray tube is not in home position.");
          }
        }
        else if (xrayCylinder.isUp())
        {
          if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - XrayCylinder is detected at home position");
          int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
          if (setOutputBitState(outputBit, _closeInnerBarrier) == false)
          {
            _initialized = false;
            DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
            _hardwareTaskEngine.throwHardwareException(dioe);
          }
          if(_debug)
            logHardwareStatus("Debug - DIO: closeInnerBarrier - closing InnerBarrier");
        }
        else
        {
          logHardwareStatus("ERROR - DIO: Inner barrier failed to close due to xray tube position unknown.");
        }
      }
      else if (XrayCPMotorActuator.isInstalled() && XrayTester.isS2EXEnabled() == true)
      {
        if(_debug)
          logHardwareStatus("Debug - DIO: closeInnerBarrier - Using XrayCPMotor, S2EX machine detected.");
        XrayCPMotorActuator xrayCPMotorActuator = XrayCPMotorActuator.getInstance();
        if (xrayCPMotorActuator.isHome()==false || _isXrayTubeCommandedHome == false)
        {
          if(_debug)
            logHardwareStatus("Debug - DIO: closeInnerBarrier - XrayCPMotor is not at home position or commanded to move away from home position");
        //do not call _xraySource.offForServiceMode() here, this will cause infinite looping.
          //AbstractXraySource xraySource = AbstractXraySource.getInstance();
          //Assert.expect(xraySource instanceof HTubeXraySource);
          //HTubeXraySource _xraySource = (HTubeXraySource)xraySource;
          //if(_xraySource.areXraysOn())_xraySource.offForServiceMode();

          xrayCPMotorActuator.home(false, false);
          if (isXrayZAxisHome())
          {
            int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
            if (setOutputBitState(outputBit, _closeInnerBarrier) == false)
            {
              _initialized = false;
              DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
              _hardwareTaskEngine.throwHardwareException(dioe);
            }
            if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - closing InnerBarrier");
          }
          else
          {
            logHardwareStatus("ERROR - DIO: Inner barrier failed to close due to xray tube is not in home position.");
          }
        }
        else
        {
          if(_debug)
              logHardwareStatus("Debug - DIO: closeInnerBarrier - XrayCPMotor is detected at home position");

          int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
          if (setOutputBitState(outputBit, _closeInnerBarrier) == false)
          {
            _initialized = false;
            DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
            _hardwareTaskEngine.throwHardwareException(dioe);
          }
          if(_debug)
            logHardwareStatus("Debug - DIO: closeInnerBarrier - closing InnerBarrier");

        }
      }
      else
      {
        int outputBit = getOutputBitFromMap(_innerBarrierOutputBit).intValue();
        if (setOutputBitState(outputBit, _closeInnerBarrier) == false)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetInnerBarrierBitException();
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        if(_debug)
          logHardwareStatus("Debug - DIO: closeInnerBarrier - closing InnerBarrier");
      }
    }
    catch (XrayTesterException dioe)
    {
      _isInnerBarrierCommandedClose = isPrevInnerBarrierCommandedClose;
      throw dioe;
    }
  }

  /**
   * Close the left outer barrier
   *
   * @author Rex Shang
   */
  public void closeLeftOuterBarrier() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_leftOuterBarrierOutputBit).intValue();
    setOutputBitState(outputBit, _closeLeftOuterBarrier);
  }

  /**
   * Close the right outer barrier
   *
   * @author Rex Shang
   */
  public void closeRightOuterBarrier() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_rightOuterBarrierOutputBit).intValue();
    setOutputBitState(outputBit, _closeRightOuterBarrier);
  }

  /**
   * Check the inner barrier open bit.
   *
   * @author Rex Shang
   */
  boolean isInnerBarrierOpen() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_innerBarrierOpenInputBit).intValue();
    int innerBarrierOpenState = getInputBitState(inputBit);

    boolean barrierIsOpen = innerBarrierOpenState == _innerBarrierIsOpen;
    return barrierIsOpen;
  }
 
  /**
   * Check the inner barrier closed bit.
   *
   * @author Anthony Fong
   */
  public boolean isInnerBarrierClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_innerBarrierClosedInputBit).intValue();
    int innerBarrierOpenState = getInputBitState(inputBit);

    boolean barrierIsOpen = innerBarrierOpenState == _innerBarrierIsClosed;
    return barrierIsOpen;
  }

  /**
   * Check the inner barrier closed bit.
   *
   * @author Rex Shang
   */
  boolean isInnerBarrierNotClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_innerBarrierClosedInputBit).intValue();
    int innerBarrierClosedState = getInputBitState(inputBit);

    boolean barrierIsNotClosed = innerBarrierClosedState == _innerBarrierIsNotClosed;
    return barrierIsNotClosed;
  }

  /**
   * Check if the left outer barrier is open.
   *
   * @author Rex Shang
   */
  public boolean isLeftOuterBarrierOpen() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_leftOuterBarrierOpenInputBit).intValue();
    int leftOuterBarrierOpenState = getInputBitState(inputBit);

    boolean barrierIsOpen = leftOuterBarrierOpenState == _leftOuterBarrierIsOpen;
    return barrierIsOpen;
  }

  /**
   * Check if the left outer barrier is open by the not closed bit.
   *
   * @author Rex Shang
   */
  boolean isLeftOuterBarrierNotClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_leftOuterBarrierClosedInputBit).intValue();
    int leftOuterBarrierClosedState = getInputBitState(inputBit);

    boolean barrierIsNotClosed = leftOuterBarrierClosedState == _leftOuterBarrierIsNotClosed;
    return barrierIsNotClosed;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  boolean isLeftOuterBarrierClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_leftOuterBarrierClosedInputBit).intValue();
    int leftOuterBarrierClosedState = getInputBitState(inputBit);

    boolean barrierIsClosed = leftOuterBarrierClosedState == 1;
    return barrierIsClosed;
  }

  /**
   * Check if the right outer barrier is open.
   *
   * @author Rex Shang
   */
  public boolean isRightOuterBarrierOpen() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_rightOuterBarrierOpenInputBit).intValue();
    int rightOuterBarrierOpenState = getInputBitState(inputBit);

    boolean barrierIsOpen = rightOuterBarrierOpenState == _rightOuterBarrierIsOpen;
    return barrierIsOpen;
  }

  /**
   * Check if the right outer barrier is open by not closed bit.
   *
   * @author Rex Shang
   */
  boolean isRightOuterBarrierNotClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_rightOuterBarrierClosedInputBit).intValue();
    int rightOuterBarrierClosedState = getInputBitState(inputBit);

    boolean barrierIsNotClosed = rightOuterBarrierClosedState == _rightOuterBarrierIsNotClosed;
    return barrierIsNotClosed;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  boolean isRightOuterBarrierClosed() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_rightOuterBarrierClosedInputBit).intValue();
    int rightOuterBarrierClosedState = getInputBitState(inputBit);

    boolean barrierIsClosed = rightOuterBarrierClosedState == 1;
    return barrierIsClosed;
  }

  /**
   * Turn the stage belts on
   *
   * @author Rex Shang
   */
  void turnStageBeltsOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_runStageBeltsOutputBit).intValue();
    setOutputBitState(outputBit, _turnStageBeltsOn);
  }

  /**
   * Turn the stage belts off
   *
   * @author Rex Shang
   */
  void turnStageBeltsOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_runStageBeltsOutputBit).intValue();
    setOutputBitState(outputBit, _turnStageBeltsOff);
  }

  /**
   * @return true if the belts are on and false if they are not
   * @author Rex Shang
   */
  public boolean areStageBeltsOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_stageBeltsInputBit).intValue();
    int stageBeltsState = getInputBitState(inputBit);

    if (stageBeltsState == _stageBeltsAreOn)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Extend the left Panel-In-Place sensor
   *
   * @author Rex Shang
   * @author Anthony Fong - For XXL optical stop Controller just Enable the VIE
   * Optical Stop Controller
   */
  void extendLeftPanelInPlaceSensor() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      if (isOpticalStopControllerEndStopperInstalled())
      {
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          int outputBit = getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit).intValue();
          setOutputBitState(outputBit, _extendLeftPanelInPlaceSensor);
        }
        else
        {
          int outputBit = getOutputBitFromMap(_leftPanelInPlaceEndStopperOutputBit).intValue();
          setOutputBitState(outputBit, _extendLeftPanelInPlaceSensor);
        }
      }
      else
      {
        //If End Stopper are not installed just simulate the signals
        int inputBitLeftExtend = getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit).intValue();
        int inputBitLeftRetract = getInputBitFromMap(_leftPanelInPlaceSensorRetractInputBit).intValue();
        _inputBitField.set(inputBitLeftExtend, new Integer(_extendLeftPanelInPlaceSensor));
        _inputBitField.set(inputBitLeftRetract, new Integer(_retractLeftPanelInPlaceSensor));
      }
    }
    else
    {
      int outputBit = getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit).intValue();
      setOutputBitState(outputBit, _extendLeftPanelInPlaceSensor);
    }
  }

  /**
   * Extend the right Panel-In-Place sensor
   *
   * @author Rex Shang
   * @author Anthony Fong - For XXL optical stop Controller just Enable the VIE
   * Optical Stop Controller
   */
  void extendRightPanelInPlaceSensor() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      if (isOpticalStopControllerEndStopperInstalled())
      {
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          int outputBit = getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit).intValue();
          setOutputBitState(outputBit, _extendRightPanelInPlaceSensor);
        }
        else
        {
          int outputBit = getOutputBitFromMap(_rightPanelInPlaceEndStopperOutputBit).intValue();
          setOutputBitState(outputBit, _extendRightPanelInPlaceSensor);
        }
      }
      else
      {
        //If End Stopper are not installed just simulate the signals
        int inputBitRightExtend = getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit).intValue();
        int inputBitRightRetract = getInputBitFromMap(_rightPanelInPlaceSensorRetractInputBit).intValue();
        _inputBitField.set(inputBitRightExtend, new Integer(_extendRightPanelInPlaceSensor));
        _inputBitField.set(inputBitRightRetract, new Integer(_retractRightPanelInPlaceSensor));
      }
    }
    else
    {
      int outputBit = getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit).intValue();
      setOutputBitState(outputBit, _extendRightPanelInPlaceSensor);
    }
  }

  /**
   * Retract the right Panel-In-Place sensor
   *
   * @author Rex Shang
   * @author Anthony Fong - For XXL optical stop Controller just Disable the VIE
   * Optical Stop Controller
   */
  void retractLeftPanelInPlaceSensor() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      if (isOpticalStopControllerEndStopperInstalled())
      {
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          int outputBit = getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit).intValue();
          setOutputBitState(outputBit, _retractLeftPanelInPlaceSensor);
        }
        else
        {
          int outputBit = getOutputBitFromMap(_leftPanelInPlaceEndStopperOutputBit).intValue();
          setOutputBitState(outputBit, _retractLeftPanelInPlaceSensor);
        }
       }
      else
      {
        //If End Stopper are not installed just simulate the signals
        int inputBitLeftExtend = getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit).intValue();
        int inputBitLeftRetract = getInputBitFromMap(_leftPanelInPlaceSensorRetractInputBit).intValue();
        _inputBitField.set(inputBitLeftExtend, new Integer(_retractLeftPanelInPlaceSensor));
        _inputBitField.set(inputBitLeftRetract, new Integer(_extendLeftPanelInPlaceSensor));
      }
    }
    else
    {
      int outputBit = getOutputBitFromMap(_leftPanelInPlaceSensorOutputBit).intValue();
      setOutputBitState(outputBit, _retractLeftPanelInPlaceSensor);
    }
  }

  /**
   * Retract the right Panel-In-Place sensor
   *
   * @author Rex Shang
   * @author Anthony Fong - For XXL optical stop Controller just Disable the VIE
   * Optical Stop Controller
   */
  void retractRightPanelInPlaceSensor() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      if (isOpticalStopControllerEndStopperInstalled())
      {
        if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
        {
          int outputBit = getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit).intValue();
          setOutputBitState(outputBit, _retractRightPanelInPlaceSensor);
        }
        else
        {
          int outputBit = getOutputBitFromMap(_rightPanelInPlaceEndStopperOutputBit).intValue();
          setOutputBitState(outputBit, _retractRightPanelInPlaceSensor);
        }
      }
      else
      {
        //If End Stopper are not installed just simulate the signals
        int inputBitRightExtend = getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit).intValue();
        int inputBitRightRetract = getInputBitFromMap(_rightPanelInPlaceSensorRetractInputBit).intValue();
        _inputBitField.set(inputBitRightExtend, new Integer(_retractRightPanelInPlaceSensor));
        _inputBitField.set(inputBitRightRetract, new Integer(_extendRightPanelInPlaceSensor));
      }
    }
    else
    {
      int outputBit = getOutputBitFromMap(_rightPanelInPlaceSensorOutputBit).intValue();
      setOutputBitState(outputBit, _retractRightPanelInPlaceSensor);
    }
  }

  /**
   * Check if the left Panel-In-Place sensor is extended
   *
   * @author Rex Shang
   */
  boolean isLeftPanelInPlaceSensorExtended() throws XrayTesterException
  {    
    if ((isOpticalStopControllerInstalled()==true) && 
       (isOpticalStopControllerEndStopperInstalled() == true))
    {
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        int inputBit = getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit).intValue();
        int leftPanelInPlaceSensorExtendState = getInputBitState(inputBit);

        if (leftPanelInPlaceSensorExtendState == _leftPanelInPlaceSensorIsExtended)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        int inputBit = getInputBitFromMap(_leftPanelInPlaceEndStopperExtendInputBit).intValue();
        int leftPanelInPlaceEndStopperExtendState = getInputBitState(inputBit);

        if (leftPanelInPlaceEndStopperExtendState == _leftPanelInPlaceSensorIsExtended)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_leftPanelInPlaceSensorExtendInputBit).intValue();
      int leftPanelInPlaceSensorExtendState = getInputBitState(inputBit);

      if (leftPanelInPlaceSensorExtendState == _leftPanelInPlaceSensorIsExtended)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }

  /**
   * Check if the right Panel-In-Place sensor is extended
   *
   * @author Rex Shang
   */
  boolean isRightPanelInPlaceSensorExtended() throws XrayTesterException
  {    
    if ((isOpticalStopControllerInstalled()==true) && 
       (isOpticalStopControllerEndStopperInstalled() == true))
    {
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        int inputBit = getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit).intValue();
        int rightPanelInPlaceSensorExtendState = getInputBitState(inputBit);

        if (rightPanelInPlaceSensorExtendState == _rightPanelInPlaceSensorIsExtended)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        int inputBit = getInputBitFromMap(_rightPanelInPlaceEndStopperExtendInputBit).intValue();
        int rightPanelInPlaceEndStopperExtendState = getInputBitState(inputBit);

        if (rightPanelInPlaceEndStopperExtendState == _rightPanelInPlaceSensorIsExtended)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_rightPanelInPlaceSensorExtendInputBit).intValue();
      int rightPanelInPlaceSensorExtendState = getInputBitState(inputBit);

      if (rightPanelInPlaceSensorExtendState == _rightPanelInPlaceSensorIsExtended)
      {
        return true;
      }
      else
      {
        return false;
      }      
    }

  }

  //XXL Based Anthony Jan 2013
  /**
   * Check if the left Panel-In-Place sensor is retracted
   *
   * @author Anthony Fong
   */
  boolean isLeftPanelInPlaceSensorRetracted() throws XrayTesterException
  {
    if ((isOpticalStopControllerInstalled()==true) && 
       (isOpticalStopControllerEndStopperInstalled() == true))
    {
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        int inputBit = getInputBitFromMap(_leftPanelInPlaceSensorRetractInputBit).intValue();
        int leftPanelInPlaceSensorRetractState = getInputBitState(inputBit);

        if (leftPanelInPlaceSensorRetractState == _leftPanelInPlaceSensorIsRetracted)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        int inputBit = getInputBitFromMap(_leftPanelInPlaceEndStopperRetractInputBit).intValue();
        int leftPanelInPlaceEndStopperRetractState = getInputBitState(inputBit);

        if (leftPanelInPlaceEndStopperRetractState == _leftPanelInPlaceSensorIsRetracted)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }
    else
    {    
      int inputBit = getInputBitFromMap(_leftPanelInPlaceSensorRetractInputBit).intValue();
      int leftPanelInPlaceSensorRetractState = getInputBitState(inputBit);

      if (leftPanelInPlaceSensorRetractState == _leftPanelInPlaceSensorIsRetracted)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }

  /**
   * Check if the right Panel-In-Place sensor is retracted
   *
   * @author Anthony Fong
   */
  boolean isRightPanelInPlaceSensorRetracted() throws XrayTesterException
  {    
    if ((isOpticalStopControllerInstalled()==true) && 
       (isOpticalStopControllerEndStopperInstalled() == true))
    {
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        int inputBit = getInputBitFromMap(_rightPanelInPlaceSensorRetractInputBit).intValue();
        int rightPanelInPlaceSensorRetractState = getInputBitState(inputBit);

        if (rightPanelInPlaceSensorRetractState == _rightPanelInPlaceSensorIsRetracted)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        int inputBit = getInputBitFromMap(_rightPanelInPlaceEndStopperRetractInputBit).intValue();
        int rightPanelInPlaceEndStopperRetractState = getInputBitState(inputBit);

        if (rightPanelInPlaceEndStopperRetractState == _rightPanelInPlaceSensorIsRetracted)
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_rightPanelInPlaceSensorRetractInputBit).intValue();
      int rightPanelInPlaceSensorRetractState = getInputBitState(inputBit);

      if (rightPanelInPlaceSensorRetractState == _rightPanelInPlaceSensorIsRetracted)
      {
        return true;
      }
      else
      {
        return false;
      }      
    }

  }

  /**
   * Check if the left Panel-In-Place sensor is engaged
   *
   * @author Rex Shang
   * @author Anthony Fong - This signal either used for optical stop sensor
   * signal for XXL or PIP engaged for Standard M/C
   */
  boolean isLeftPanelInPlaceSensorEngaged() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      //Right Optical Stop Sensor actually is an alternative for Left Panel In Place Sensor Engaged
      //_rightPanelInPlaceOpticalSensorEngagedInputBit <-> _leftPanelInPlaceSensorEngagedInputBit
      int inputBit = getInputBitFromMap(_rightPanelInPlaceOpticalSensorEngagedInputBit).intValue();
      int leftPanelInPlaceOpticalSensorEngagedState = getInputBitState(inputBit);

      if (leftPanelInPlaceOpticalSensorEngagedState == _leftPanelInPlaceSensorIsEngaged)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_leftPanelInPlaceSensorEngagedInputBit).intValue();
      int leftPanelInPlaceSensorEngagedState = getInputBitState(inputBit);

      if (leftPanelInPlaceSensorEngagedState == _leftPanelInPlaceSensorIsEngaged)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }

  /**
   * Check if the Optical Stop Controller is Installed
   *
   * @author Anthony Fong
   */
  boolean isOpticalStopControllerInstalledBasedOnDigitalInput() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_opticalStopControllerInstalledInputBit).intValue();
    int opticalStopControllerInstalledState = getInputBitState(inputBit);

    if (opticalStopControllerInstalledState == _opticalStopControllerInstalled)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Check if the right Panel-In-Place sensor is engaged
   *
   * @author Rex Shang
   */
  boolean isRightPanelInPlaceSensorEngaged() throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      //Left Optical Stop Sensor actually is an alternative for Right Panel In Place Sensor Engaged
      //_leftPanelInPlaceOpticalSensorEngagedInputBit <-> _rightPanelInPlaceSensorEngagedInputBit
      int inputBit = getInputBitFromMap(_leftPanelInPlaceOpticalSensorEngagedInputBit).intValue();
      int rightPanelInPlaceOpticalSensorEngagedState = getInputBitState(inputBit);

      if (rightPanelInPlaceOpticalSensorEngagedState == _rightPanelInPlaceSensorIsEngaged)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_rightPanelInPlaceSensorEngagedInputBit).intValue();
      int rightPanelInPlaceSensorEngagedState = getInputBitState(inputBit);

      if (rightPanelInPlaceSensorEngagedState == _rightPanelInPlaceSensorIsEngaged)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }

  /**
   * Open the panel clamps
   *
   * @author Rex Shang
   */
  public void openPanelClamps() throws XrayTesterException
  {
    //XXL Based Anthony Jan 2013
    if (XrayTester.isXXLBased()||XrayTester.isS2EXEnabled())
    {
      //For a double-acting cylinder we need to turn OFF the first valve
      //before turning ON the second valve
      int outputBit = getOutputBitFromMap(_panelClampsOutputBit).intValue();
      setOutputBitState(outputBit, _turnOFFOutputSignal);

      //Need to wait for least 300 ms of ON/OFF switch over time
      //waitForHardwareInMilliSeconds(50);

      outputBit = getOutputBitFromMap(_panelUnclampsOutputBit).intValue();
      setOutputBitState(outputBit, _turnONOutputSignal);
    }
    else
    {
      int outputBit = getOutputBitFromMap(_panelClampsOutputBit).intValue();
      setOutputBitState(outputBit, _openPanelClamps);
    }
  }

  /**
   * Close the panel clamps
   *
   * @author Rex Shang
   */
  public void closePanelClamps() throws XrayTesterException
  {    //XXL Based Anthony Jan 2013
    if (XrayTester.isXXLBased()||XrayTester.isS2EXEnabled())
    {
      //For a double-acting cylinder we need to turn OFF the first valve
      //before turning ON the second valve
      int outputBit = getOutputBitFromMap(_panelUnclampsOutputBit).intValue();
      setOutputBitState(outputBit, _turnOFFOutputSignal);

      //Need to wait for least 300 ms of ON/OFF switch over time
      waitForHardwareInMilliSeconds(50);

      outputBit = getOutputBitFromMap(_panelClampsOutputBit).intValue();
      setOutputBitState(outputBit, _turnONOutputSignal);
    }
    else
    {
      int outputBit = getOutputBitFromMap(_panelClampsOutputBit).intValue();
      setOutputBitState(outputBit, _closePanelClamps);
    }
  }

  /**
   * @return true if panel clamps are closed
   * @author Rex Shang
   */
  boolean arePanelClampsClosed() throws XrayTesterException
  {
    //XXL Based Anthony Jan 2013
    if (XrayTester.isXXLBased() == true || XrayTester.isS2EXEnabled())
    {
      int inputBit = getInputBitFromMap(_panelUnclampsInputBit).intValue();
      int panelClampsState = getInputBitState(inputBit);

      if (panelClampsState != _panelClampsAreClosed)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      int inputBit = getInputBitFromMap(_panelClampsInputBit).intValue();
      int panelClampsState = getInputBitState(inputBit);

      if (panelClampsState == _panelClampsAreClosed)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }

  //XXL Based Anthony Jan 2013
  /**
   * @return true if panel clamps are opened
   * @author Anthony Fong
   */
  public boolean arePanelClampsOpened() throws XrayTesterException
  {
    return arePanelClampsClosed() == false;
  }

  /**
   * @return true if it is blocked and false if it is not
   * @author Rex Shang
   */
  boolean isLeftPanelClearSensorBlocked() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_leftPanelClearSensorInputBit).intValue();
    int leftPanelClearSensorState = getInputBitState(inputBit);

    if (leftPanelClearSensorState == _leftPanelClearSensorIsBlocked)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Check if the right panel clear sensor is blocked
   *
   * @author Rex Shang
   */
  boolean isRightPanelClearSensorBlocked() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_rightPanelClearSensorInputBit).intValue();
    int rightPanelClearSensorState = getInputBitState(inputBit);

    if (rightPanelClearSensorState == _rightPanelClearSensorIsBlocked)
    {
      return true;
    }
    else
    {
      return false;
    }   
  }

  /**
   * Set the stage belt direction to left-to-right
   *
   * @author Rex Shang
   */
  void setStageBeltsDirectionLeftToRight() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_stageBeltDirectionOutputBit).intValue();
    setOutputBitState(outputBit, _stageBeltDirectionLeftToRight);
  }

  /**
   * Set the stage belt direction to right-to-left
   *
   * @author Rex Shang
   */
  void setStageBeltDirectionRightToLeft() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_stageBeltDirectionOutputBit).intValue();
    setOutputBitState(outputBit, _stageBeltDirectionRightToLeft);
  }

  /**
   * @return true if the direction is left-to-right and false if it is not
   * @author Rex Shang
   */
  boolean areStageBeltsDirectionLeftToRight() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_stageBeltDirectionInputBit).intValue();
    int stageBeltDirectionState = getInputBitState(inputBit);

    if (stageBeltDirectionState == _stageBeltDirectionIsLeftToRight)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return true if the interlock board is in service mode.
   * @author Rex Shang
   */
  boolean isInterlockServiceModeOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockServiceModeOnInputBit).intValue();
    int interlockServiceModeState = getInputBitState(inputBit);

    if (interlockServiceModeState == _interlockServiceModeIsOn)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return true if the interlock chain 1 is open.
   * @author Rex Shang
   */
  public boolean isInterlockChain1Open() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockChain1OpenInputBit).intValue();
    int interlockChain1OpenState = getInputBitState(inputBit);

    if (interlockChain1OpenState == _intlockChainIsOpen)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return true if the interlock chain 2 is open.
   * @author Rex Shang
   */
  public boolean isInterlockChain2Open() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockChain2OpenInputBit).intValue();
    int interlockChain2OpenState = getInputBitState(inputBit);

    if (interlockChain2OpenState == _intlockChainIsOpen)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return true if the interlock board is powered on.
   * @author Rex Shang
   */
  boolean isInterlockPoweredOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockPowerOnInputBit).intValue();
    int interlockPowerOnState = getInputBitState(inputBit);

    if (interlockPowerOnState == _interlockIsOn)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return interlock hardware id bit 3.
   * @author Rex Shang
   */
  int getInterlockHardwareIdBit3() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockHardwareIdBit3InputBit).intValue();
    int bit = getInputBitState(inputBit);

    return bit;
  }

  /**
   * @return interlock hardware id bit 2.
   * @author Rex Shang
   */
  int getInterlockHardwareIdBit2() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockHardwareIdBit2InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock hardware id bit 1.
   * @author Rex Shang
   */
  int getInterlockHardwareIdBit1() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockHardwareIdBit1InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock hardware id bit 0.
   * @author Rex Shang
   */
  int getInterlockHardwareIdBit0() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockHardwareIdBit0InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 5.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit5() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit5InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 4.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit4() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit4InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 3.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit3() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit3InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 2.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit2() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit2InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 1.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit1() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit1InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * @return interlock switch id bit 0.
   * @author Rex Shang
   */
  int getInterlockSwitchIdBit0() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_interlockSwitchIdBit0InputBit).intValue();
    int id = getInputBitState(inputBit);

    return id;
  }

  /**
   * Turn on the red light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackRedIndicatorOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackRedLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOn);
  }

  /**
   * Turn off the red light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackRedIndicatorOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackRedLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOff);
  }

  /**
   * Check to see if the red light is on on the light stack.
   *
   * @author Rex Shang
   */
  boolean isLightStackRedIndicatorOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_lightStackRedLightInputBit).intValue();
    return (getInputBitState(inputBit) == _lightStackIndicatorIsOn);
  }

  /**
   * Turn on the green light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackGreenIndicatorOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackGreenLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOn);
  }

  /**
   * Turn off the green light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackGreenIndicatorOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackGreenLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOff);
  }

  /**
   * Check to see if the green light is on on the light stack.
   *
   * @author Rex Shang
   */
  boolean isLightStackGreenIndicatorOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_lightStackGreenLightInputBit).intValue();
    return (getInputBitState(inputBit) == _lightStackIndicatorIsOn);
  }

  /**
   * Turn on the yellow light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackYellowIndicatorOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackYellowLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOn);
  }

  /**
   * Turn off the yellow light on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackYellowIndicatorOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackYellowLightOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOff);
  }

  /**
   * Check to see if the yellow light is on on the light stack.
   *
   * @author Rex Shang
   */
  boolean isLightStackYellowIndicatorOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_lightStackYellowLightInputBit).intValue();
    return (getInputBitState(inputBit) == _lightStackIndicatorIsOn);
  }

  /**
   * Turn on the buzzer on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackBuzzerOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackBuzzerOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOn);
  }

  /**
   * Turn off the buzzer on the light stack.
   *
   * @author Rex Shang
   */
  void turnLightStackBuzzerOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_lightStackBuzzerOutputBit).intValue();
    setOutputBitState(outputBit, _turnLightStackIndicatorOff);
  }

  /**
   * Check to see if the buzzer is on on the light stack.
   *
   * @author Rex Shang
   */
  boolean isLightStackBuzzerOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_lightStackBuzzerInputBit).intValue();
    return (getInputBitState(inputBit) == _lightStackIndicatorIsOn);
  }

  /**
   * Turn on the stage viewing light.
   *
   * @author Rex Shang
   */
  void turnStageViewingLightOn() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_stageViewingLightOutputBit).intValue();
      setOutputBitState(outputBit, _turnStageViewingLightOn);
    }
  }

  /**
   * Turn off the stage viewing light.
   *
   * @author Rex Shang
   */
  void turnStageViewingLightOff() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_stageViewingLightOutputBit).intValue();
      setOutputBitState(outputBit, _turnStageViewingLightOff);
    }
  }

  /**
   * Check to see if the stage viewing light is on.
   *
   * @author Rex Shang
   */
  boolean isStageViewingLightOn() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled() == false)
    {
      int inputBit = getInputBitFromMap(_stageViewingLightInputBit).intValue();
      return (getInputBitState(inputBit) == _stageViewingLightIsOn);
    }
    else
    {
      return false;
    }
  }

  //Variable Mag Anthony August 2011
  /**
   * Turn on the X-Ray Cylinder Down.
   *
   * @author Anthony Fong
   */
  void turnXrayCylinderDownOn() throws XrayTesterException
  {
    //InnerBarrier innerBarrier = InnerBarrier.getInstance();
    //innerBarrier.open();
    openInnerBarrier();
    if (isInnerBarrierOpen() && _isInnerBarrierCommandedClose == false)
    {
      turnXrayCylinderUnClamp();
      int outputBit = getOutputBitFromMap(_xrayCylinderDownOutputBit).intValue();
      setOutputBitState(outputBit, _turnXRayCylinderDownOn);
      _isXrayTubeCommandedHome = false;
    }
  }

  /**
   * Turn off the X-Ray Cylinder Down.
   *
   * @author Anthony Fong
   */
  void turnXrayCylinderDownOff() throws XrayTesterException
  {
    //openInnerBarrier();
    //if(isInnerBarrierOpen())
    {
      turnXrayCylinderUnClamp();
      int outputBit = getOutputBitFromMap(_xrayCylinderDownOutputBit).intValue();
      setOutputBitState(outputBit, _turnXRayCylinderDownOff);
      _isXrayTubeCommandedHome = true;
    }
  }

  /**
   * Check to see if the X-Ray Cylinder down bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayCylinderDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCylinderDownInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCylinderIsDown);
  }

  /**
   * Check to see if the X-Ray Cylinder up bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayCylinderNotUp() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCylinderUpInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCylinderIsNotUp);
  }

  /**
   * Check to see if the X-Ray Cylinder up bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayCylinderUp() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCylinderUpInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCylinderIsUp);
  }
  
    /**
   * Check to see if the X-Ray Cylinder down bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayCylinderNotDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCylinderDownInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCylinderIsNotDown);
  }
  
  /**
   * Check to see if the Z Axis Motor is in Up or Safe Loading position.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisHome() throws XrayTesterException
  { 
    int inputBit = getInputBitFromMap(_zAxisAtReadySafePositionInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayZAxisIsHome);
  }
 
  /**
   * Check to see if the Z Axis Motor is in Up or Safe Loading position.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisNotHome() throws XrayTesterException
  { 
    int inputBit = getInputBitFromMap(_zAxisAtReadySafePositionInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayZAxisIsNotHome);
  }
  
  
   /**
   * Check to see if the Z Axis Motor is in Up or Safe Loading position.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisNoDownAndUp() throws XrayTesterException
  { 
    int inputBitUp1 = getInputBitFromMap(_zAxisAtMag1PositionInputBit).intValue();
    int inputBitUp2 = getInputBitFromMap(_zAxisAtMag2PositionInputBit).intValue();
    int inputBitDown = getInputBitFromMap(_zAxisAtMag3PositionInputBit).intValue();
    
    return (getInputBitState(inputBitUp1) == _xrayZAxisIsNotUp) && 
           (getInputBitState(inputBitUp2) == _xrayZAxisIsNotUp) && 
           (getInputBitState(inputBitDown) == _xrayZAxisIsNotDown);
      
  }
  
  /**
   * Check to see if the X-Ray Z Axis down or High Mag bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_zAxisAtMag3PositionInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayZAxisIsDown);
  }

  /**
   * Check to see if the X-Ray Z Axis up bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisNotUp() throws XrayTesterException
  {
    int inputBitUp1 = getInputBitFromMap(_zAxisAtMag1PositionInputBit).intValue();
    int inputBitUp2 = getInputBitFromMap(_zAxisAtMag2PositionInputBit).intValue();
    
    return (getInputBitState(inputBitUp1) == _xrayZAxisIsNotUp) &&
           (getInputBitState(inputBitUp2) == _xrayZAxisIsNotUp);
  }

  /**
   * Check to see if the X-Ray Z Axis up bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisUp() throws XrayTesterException
  {
    int inputBitUp1 = getInputBitFromMap(_zAxisAtMag1PositionInputBit).intValue();
    int inputBitUp2 = getInputBitFromMap(_zAxisAtMag2PositionInputBit).intValue();
    
    if(_xrayZAxisMotorPositionLookupForLowMagnification.contains(_POSITION_NAME_23_MICRON))
    {
      return (getInputBitState(inputBitUp1) == _xrayZAxisIsUp);
    }
    else if(_xrayZAxisMotorPositionLookupForLowMagnification.contains(_POSITION_NAME_19_MICRON))
    {
      return (getInputBitState(inputBitUp2) == _xrayZAxisIsUp);    
    }
    else //default
    {
      return (getInputBitState(inputBitUp1) == _xrayZAxisIsUp);
    }
  }
  
  /**
   * Check to see if the X-Ray Z Axis up to position 1 bit.
   *
   * @author Swee Yee Wong
   */
  boolean isXrayZAxisUpToPosition1() throws XrayTesterException
  {
    int inputBitUp1 = getInputBitFromMap(_zAxisAtMag1PositionInputBit).intValue();
    return (getInputBitState(inputBitUp1) == _xrayZAxisIsUp);
  }
  
  /**
   * Check to see if the X-Ray Z Axis up to position 2 bit.
   *
   * @author Swee Yee Wong
   */
  boolean isXrayZAxisUpToPosition2() throws XrayTesterException
  {
    int inputBitUp2 = getInputBitFromMap(_zAxisAtMag2PositionInputBit).intValue();
    return (getInputBitState(inputBitUp2) == _xrayZAxisIsUp);
  }
  
  /**
   * Check to see if the X-Ray Z Axis up to position 1 bit.
   *
   * @author Swee Yee Wong
   */
  boolean isXrayZAxisNotUpToPosition1() throws XrayTesterException
  {
    int inputBitUp1 = getInputBitFromMap(_zAxisAtMag1PositionInputBit).intValue();
    
    return (getInputBitState(inputBitUp1) == _xrayZAxisIsNotUp);
  }
  
  /**
   * Check to see if the X-Ray Z Axis up to position 2 bit.
   *
   * @author Swee Yee Wong
   */
  boolean isXrayZAxisNotUpToPosition2() throws XrayTesterException
  {
    int inputBitUp2 = getInputBitFromMap(_zAxisAtMag2PositionInputBit).intValue();
    
    return (getInputBitState(inputBitUp2) == _xrayZAxisIsNotUp);
  }

  /**
   * Check to see if the X-Ray Z Axis down bit.
   *
   * @author Anthony Fong
   */
  boolean isXrayZAxisNotDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_zAxisAtMag3PositionInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayZAxisIsNotDown);
  }
  
  /**
   * Check to see if the X-Ray Z Axis Enable bit.
   *
   * @author Swee Yee Wong
   */
  public boolean isXrayZAxisEnabled() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_zAxisEnableInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayZAxisEnableIsOn);
  }

  /**
   * Turn on the X-Ray Cylinder Clamp.
   *
   * @author Anthony Fong
   */
  void turnXrayCylinderClamp() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayCylinderClampOutputBit).intValue();
    setOutputBitState(outputBit, _turnXRayCylinderClamp);
  }

  /**
   * Turn off the X-Ray Cylinder Clamp.
   *
   * @author Anthony Fong
   */
  public void turnXrayCylinderUnClamp() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayCylinderClampOutputBit).intValue();
    setOutputBitState(outputBit, _turnXRayCylinderUnClamp);
  }

  /**
   * Check to see if the X-Ray Cylinder is clamp.
   *
   * @author Anthony Fong
   */
  boolean isXrayCylinderClampOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCylinderClampInputBit).intValue();
    return (getInputBitState(inputBit) == _turnXRayCylinderClamp);
  }
   
   /**
   * Enable the Z-Axis.
   *
   * @author Anthony Fong
   */
  public void enableZAxis() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisEnableOutputBit).intValue();
    setOutputBitState(outputBit, _turnZAxisEnableON);
    sleep(50);
  }
  
  /**
   * Disable the Z-Axis.
   *
   * @author Anthony Fong
   */
  public void disableZAxis() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisEnableOutputBit).intValue();
    setOutputBitState(outputBit, _turnZAxisEnableOFF);
  }
    
  /**
   * Move Z-Axis to Home Position.
   * 
   * @author Anthony Fong
   */
  public void moveZAxisToHomePosition() throws XrayTesterException
  {
    if(isInnerBarrierOpen())
    {  
      if(_debug)
          logHardwareStatus("Debug - DIO: moveZAxisToHomePosition - Inner barrier is opened");
      boolean isPrevXrayTubeCommandedHome = _isXrayTubeCommandedHome;
      _isXrayTubeCommandedHome = true;
      try
      {
        //Always turn off A and B for Homing 
        enableZAxis();
        //swee-yee.wong - must enable z-axis before turn on/off z-axis bit A/B
        turnOffZAxisPositionBitA();
        turnOffZAxisPositionBitB();
        if(_debug)
          logHardwareStatus("Debug - DIO: moveZAxisToHomePosition - moving xray tube to home position");
      }
      catch(XrayTesterException dioe)
      {
        _isXrayTubeCommandedHome = isPrevXrayTubeCommandedHome;
        throw dioe;
      }
    }
    else
    {
      logHardwareStatus("ERROR - DIO: Xray tube failed to go home due to inner barrier is not open.");
    }
  }
  
    /**
   * Move Z-Axis to Position 1. - low mag 23um
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public void moveZAxisToPosition1() throws XrayTesterException
  {
    if(isInnerBarrierOpen() && _isInnerBarrierCommandedClose == false)
    { 
      if(_debug)
        logHardwareStatus("Debug - DIO: moveZAxisToPosition1 - Inner barrier is opened and not commanded to close.");
      boolean isPrevXrayTubeCommandedHome = _isXrayTubeCommandedHome;
      _isXrayTubeCommandedHome = false;
      try
      {
        enableZAxis();
        turnOffZAxisPositionBitB();      
        turnOnZAxisPositionBitA();
        if(_debug)
          logHardwareStatus("Debug - DIO: moveZAxisToPosition1 - moving xray tube to low mag position 1");
      }
      catch(XrayTesterException dioe)
      {
        _isXrayTubeCommandedHome = isPrevXrayTubeCommandedHome;
        throw dioe;
      }
    }
    else
    {
      if(isInnerBarrierOpen() == false)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position 1 due to inner barrier is not open.");
      if(_isInnerBarrierCommandedClose)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position 1 due to inner barrier is commanded close in other thread.");
    }
  }
  
  /**
   * Move Z-Axis to Position 2. - low mag 19um
   * this function only used in service panel
   * @author Swee Yee Wong
   */
  public void moveZAxisToPosition2() throws XrayTesterException
  {
    if (isInnerBarrierOpen() && _isInnerBarrierCommandedClose == false)
    {
      if(_debug)
        logHardwareStatus("Debug - DIO: moveZAxisToPosition2 - Inner barrier is opened and not commanded to close.");
      boolean isPrevXrayTubeCommandedHome = _isXrayTubeCommandedHome;
      _isXrayTubeCommandedHome = false;
      try
      {
        enableZAxis();
        turnOffZAxisPositionBitA();
        turnOnZAxisPositionBitB();
        if(_debug)
          logHardwareStatus("Debug - DIO: moveZAxisToPosition2 - moving xray tube to low mag position 2");
      }
      catch(XrayTesterException dioe)
      {
        _isXrayTubeCommandedHome = isPrevXrayTubeCommandedHome;
        throw dioe;
      }
    }
    else
    {
      if(isInnerBarrierOpen() == false)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position 2 due to inner barrier is not open.");
      if(_isInnerBarrierCommandedClose)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position 2 due to inner barrier is commanded close in other thread.");
    }
  }
  
     
  /**
   * Move Z-Axis to Low Mag Position.
   *
   * @author Anthony Fong
   */
  public void moveZAxisToLowMagPosition() throws XrayTesterException
  {
    if(isInnerBarrierOpen() && _isInnerBarrierCommandedClose == false)
    { 
      if(_debug)
        logHardwareStatus("Debug - DIO: moveZAxisToLowMagPosition - Inner barrier is opened and not commanded to close.");
      boolean isPrevXrayTubeCommandedHome = _isXrayTubeCommandedHome;
      _isXrayTubeCommandedHome = false;
      try
      {
        enableZAxis();

        if(_xrayZAxisMotorPositionLookupForLowMagnification.contains(_POSITION_NAME_23_MICRON))
        {
          if(_debug)
            logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - M32 detected");
          turnOffZAxisPositionBitB();      
          turnOnZAxisPositionBitA();
        }
        else if(_xrayZAxisMotorPositionLookupForLowMagnification.contains(_POSITION_NAME_19_MICRON))
        {
          if(_debug)
            logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - M19 detected");
          turnOffZAxisPositionBitA();
          turnOnZAxisPositionBitB();       
        }             
        else //default
        {
          logHardwareStatus("Error: Low mag position is not set properly in config");
          turnOffZAxisPositionBitB();      
          turnOnZAxisPositionBitA();
        }
        if(_debug)
          logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - moving xray tube to low mag position");
      }
      catch(XrayTesterException dioe)
      {
        _isXrayTubeCommandedHome = isPrevXrayTubeCommandedHome;
        throw dioe;
      }
    }
    else
    {
      if(isInnerBarrierOpen() == false)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position due to inner barrier is not open.");
      if(_isInnerBarrierCommandedClose)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go low mag position due to inner barrier is commanded close in other thread.");
    }
  }
  
  /**
   * Move Z-Axis to Low Mag Position.
   *
   * @author Anthony Fong
   */
  public void moveZAxisToHighMagPosition() throws XrayTesterException
  {
    if(isInnerBarrierOpen() && _isInnerBarrierCommandedClose == false)
    {
      if(_debug)
        logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - Inner barrier is opened and not commanded to close.");
      boolean isPrevXrayTubeCommandedHome = _isXrayTubeCommandedHome;
      _isXrayTubeCommandedHome = false;
      try
      {
        enableZAxis();

        if(_xrayZAxisMotorPositionLookupForHighMagnification.contains(_POSITION_NAME_11_MICRON))
        {
          if(_debug)
            logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - M11 is detected");
          turnOnZAxisPositionBitA();
          turnOnZAxisPositionBitB();      
        }      
        else //default
        {
          logHardwareStatus("Error: High mag position is not set properly in config");
          turnOffZAxisPositionBitA();
          turnOnZAxisPositionBitB();
        }
        if(_debug)
          logHardwareStatus("Debug - DIO:  moveZAxisToLowMagPosition - moving xray tube to high mag position");
      }
      catch(XrayTesterException dioe)
      {
        _isXrayTubeCommandedHome = isPrevXrayTubeCommandedHome;
        throw dioe;
      }
    }
    else
    {
      if(isInnerBarrierOpen() == false)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go high mag position due to inner barrier is not open.");
      if(_isInnerBarrierCommandedClose)
        logHardwareStatus("ERROR - DIO: Xray tube failed to go high mag position due to inner barrier is commanded close in other thread.");
    }
  }
  
  /**
   * Turn On Z-Axis Position Bit A.
   *
   * @author Anthony Fong
   */
  private void turnOnZAxisPositionBitA() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisPositionBitAOutputBit).intValue();
    if(setOutputBitState(outputBit, _turnZAxisEnableON)==false)
    {
      _initialized = false;
      DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetZAxisPositionBitException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
  }
  
  /**
   * Turn Off Z-Axis Position Bit A.
   *
   * @author Anthony Fong
   */
  private void turnOffZAxisPositionBitA() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisPositionBitAOutputBit).intValue();
    if(setOutputBitState(outputBit, _turnZAxisEnableOFF)==false)
    {
      _initialized = false;
      DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetZAxisPositionBitException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
  }
  
  
    /**
   * Turn On Z-Axis Position Bit B.
   *
   * @author Anthony Fong
   */
  private void turnOnZAxisPositionBitB() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisPositionBitBOutputBit).intValue();
    if(setOutputBitState(outputBit, _turnZAxisEnableON)==false)
    {
      _initialized = false;
      DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetZAxisPositionBitException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
  }
  
  /**
   * Turn Off Z-Axis Position Bit B.
   *
   * @author Anthony Fong
   */
  private void turnOffZAxisPositionBitB() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_zAxisPositionBitBOutputBit).intValue();
    if(setOutputBitState(outputBit, _turnZAxisEnableOFF)==false)
    {
      _initialized = false;
      DigitalIoException dioe = DigitalIoException.getDigitalIoFailedToSetZAxisPositionBitException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
  }

  /**
   * Check to see if the X-Ray Left Safety Level OK.
   *
   * @author Anthony Fong
   */
  public boolean isXrayLeftSafetyLevelOK() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayLeftSafetyLevelSensorInputBit).intValue();
    if(isSimulationModeOn())
      return true;
    else
      return (getInputBitState(inputBit) == _xrayCylinderLevelOK);
  }

  /**
   * Check to see if the X-Ray Right Safety Level OK.
   *
   * @author Anthony Fong
   */
  public boolean isXrayRightSafetyLevelOK() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayRightSafetyLevelSensorInputBit).intValue();
    if(isSimulationModeOn())
      return true;
    else
      return (getInputBitState(inputBit) == _xrayCylinderLevelOK);
  }

  /**
   * Turn on the Front Panel Left Door Lock.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public void turnFrontPanelLeftDoorLockOn() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false && XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_panelFrontLeftDoorLockOutputBit).intValue();
      setOutputBitState(outputBit, _turnPanelFrontLeftDoorLockOn);
    }
  }

  /**
   * Turn off the Front Panel Left Door Lock.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public void turnFrontPanelLeftDoorLockOff() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false && XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_panelFrontLeftDoorLockOutputBit).intValue();
      setOutputBitState(outputBit, _turnPanelFrontLeftDoorLockOff);
    }
  }

  /**
   * Check to see if the Front Panel Left Door Lock is on.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public boolean isFrontPanelLeftDoorLockOn() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false  && XrayTester.isS2EXEnabled() == false)
    {
      int inputBit = getInputBitFromMap(_panelFrontLeftDoorLockInputBit).intValue();
      return (getInputBitState(inputBit) == _turnPanelFrontLeftDoorLockOn);
    }
    else
    {
      return false;
    }
  }

  /**
   * Turn on the Front Panel Right Door Lock.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public void turnFrontPanelRightDoorLockOn() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false && XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_panelFrontRightDoorLockOutputBit).intValue();
      setOutputBitState(outputBit, _turnPanelFrontRightDoorLockOn);
    }
  }

  /**
   * Turn off the Front Panel Right Door Lock.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public void turnFrontPanelRightDoorLockOff() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false && XrayTester.isS2EXEnabled() == false)
    {
      int outputBit = getOutputBitFromMap(_panelFrontRightDoorLockOutputBit).intValue();
      setOutputBitState(outputBit, _turnPanelFrontRightDoorLockOff);
    }
  }

  /**
   * Check to see if the Front Panel Right Door Lock is on.
   *
   * @author Anthony Fong
   * @editedby Kee Chin Seong - S2ex is no longer use this BOB 8 and 9 !
   */
  public boolean isFrontPanelRightDoorLockOn() throws XrayTesterException
  {
    if (Interlock.getInterLockLogicBoardWithKeylock() == false && XrayTester.isS2EXEnabled() == false)
    {
      int inputBit = getInputBitFromMap(_panelFrontRightDoorLockInputBit).intValue();
      return (getInputBitState(inputBit) == _turnPanelFrontRightDoorLockOn);
    }
    else
    {
      return false;
    }
  }

  /**
   * SMEMA TRO Tell the upstream devices that we are ready to take in a new
   * panel.
   *
   * @author Rex Shang
   */
  void setSmemaXrayTesterIsReadyToAcceptPanel() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayTesterReadyToAcceptPanelOutputBit).intValue();
    setOutputBitState(outputBit, _xrayTesterReadyToAcceptPanel);
  }

  /**
   * SMEMA TRO Tell the upstream device that we are not ready to take in any
   * panel.
   *
   * @author Rex Shang
   */
  void setSmemaXrayTesterIsNotReadyToAcceptPanel() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayTesterReadyToAcceptPanelOutputBit).intValue();
    setOutputBitState(outputBit, _xrayTesterNotReadyToAcceptPanel);
  }

  /**
   * SMEMA TRO Check to see if we are ready to accept a new panel. This input
   * bit is not very useful for SMEMA operation but useful for double check
   * whether we can set or unset the bit.
   *
   * @author Rex Shang
   */
  boolean isSmemaXrayTesterReadyToAcceptPanel() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayTesterReadyToAcceptPanelInputBit).intValue();
    return (getInputBitState(inputBit) != _xrayTesterNotReadyToAcceptPanel);
  }

  /**
   * SMEMA SRO Tell the downstream device that we are ready to output a panel.
   *
   * @author Rex Shang
   */
  void setSmemaPanelFromXrayTesterAvailable() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutAvailableOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutAvailable);
  }

  /**
   * SMEMA SRO Tell the downstream device that we don't have a panel to output.
   *
   * @author Rex Shang
   */
  void setSmemaPanelFromXrayTesterNotAvailable() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutAvailableOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutNotAvailable);
  }

  /**
   * SMEMA SRO Check to see if we have a panel ready to be sent out. This input
   * bit is not very useful for SMEMA operation but useful to check whether we
   * can set/unset the bit.
   *
   * @author Rex Shang
   */
  boolean isSmemaPanelFromXrayTesterAvailable() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_panelOutAvailableInputBit).intValue();
    return (getInputBitState(inputBit) == _panelOutIsAvailable);
  }

  /**
   * Tell the downstream device that the panel we are sending out is good. This
   * is not part of SMEMA.
   *
   * @author Rex Shang
   */
  void setPanelTestResultPass() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutGoodOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutIsGood);
  }

  /**
   * Tell the downstream device that the panel we are sending out is bad. This
   * is not part of SMEMA.
   *
   * @author Rex Shang
   */
  void setPanelTestResultFail() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutGoodOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutIsBad);
  }

  /**
   * Check to see if the panel we are sending out is good. This input bit is not
   * very useful to "us" for SMEMA operation. It is used to check whether we can
   * set/unset the bit. This is not part of SMEMA.
   *
   * @author Rex Shang
   */
  boolean isPanelTestResultPass() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_panelOutGoodInputBit).intValue();
    return (getInputBitState(inputBit) == _panelOutIsGood);
  }

  /**
   * Tell the downstream device that the panel we are sending out is tested.
   * This is not part of SMEMA.
   *
   * @author Rex Shang
   */
  void setPanelTestResultTested() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutUntestedOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutIsTested);
  }

  /**
   * Tell the downstream device that the panel we are sending out is not tested.
   * This is not part of SMEMA.
   *
   * @author Rex Shang
   */
  void setPanelTestResultNotTested() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_panelOutUntestedOutputBit).intValue();
    setOutputBitState(outputBit, _panelOutIsNotTested);
  }

  /**
   * Check to see if the board we are sending out has been tested. This input
   * bit is not useful to "us" for SMEMA operation. It is used to check if we
   * can toggle the bit. This is not part of SMEMA.
   *
   * @author Rex Shang
   */
  boolean isPanelTestResultTested() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_panelOutUntestedInputBit).intValue();
    return (getInputBitState(inputBit) == _panelOutIsTested);
  }

  /**
   * SMEMA SRI Check to see if there is a panel coming our way.
   *
   * @author Rex Shang
   */
  boolean isSmemaPanelFromUpstreamSystemAvailable() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_panelInAvailableInputBit).intValue();
    return (getInputBitState(inputBit) == _panelInIsAvailable);
  }

  /**
   * SMEMA TRI Check to see if downstream device is ready to accept a panel.
   *
   * @author Rex Shang
   */
  boolean isSmemaDownstreamSystemReadyToAcceptPanel() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_downStreamSystemReadyToAcceptPanelInputBit).intValue();
    return (getInputBitState(inputBit) == _downStreamSystemIsReadyToAcceptPanel);
  }

  /**
   * This will enable Camera Trigger Board to continually output trigger to
   * cameras. This function allows bypass of stage motion which generates camera
   * triggers in normal operation.
   *
   * @author Rex Shang
   */
  void turnXrayCamerasSyntheticTriggerOn() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayCamerasSyntheticTriggerOutputBit).intValue();
    setOutputBitState(outputBit, _turnXrayCamerasSyntheticTriggerOn);
  }

  /**
   * This will disable Camera Trigger Board to continually output trigger to
   * cameras. This function returns the cameras to normal operation which uses
   * trigger generated by stage motion.
   *
   * @author Rex Shang
   */
  void turnXrayCamerasSyntheticTriggerOff() throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(_xrayCamerasSyntheticTriggerOutputBit).intValue();
    setOutputBitState(outputBit, _turnXrayCamerasSyntheticTriggerOff);
  }

  /**
   * Check to see if cameras are operating with synthetic trigger.
   *
   * @author Rex Shang
   */
  boolean areXrayCamerasSyntheticTriggerOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasSyntheticTriggerInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCamerasSyntheticTriggerAreOn);
  }

  /**
   * Check to see if all cameras are connected ok.
   *
   * @author Rex Shang
   */
  boolean areXrayCameraControlCablesConnected() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasConnectedInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCamerasAreConnected);
  }

  /**
   * Check to see if all cameras are powered on.
   *
   * @author Rex Shang
   */
  boolean areXrayCamerasPoweredOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasPowerOnInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCamerasAreOn);
  }

  /**
   * Check to see if camera controler is powered on. Note that this is tied with
   * the camera power...
   *
   * @author Rex Shang
   */
  boolean isXrayCameraControllerPoweredOn() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasPowerOnInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCamerasAreOn);
  }

  /**
   * Check to see if the cameras are pulling excessive power.
   *
   * @author Rex Shang
   */
  boolean areXrayCamerasUsingExcessivePower() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasUsingExcessivePowerInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCamerasAreUsingExcessivePower);
  }

  /**
   * Check to see if all cameras are phase locked which means all cameras are in
   * synch.
   *
   * @author Rex Shang
   */
  boolean areXrayCamerasPhaseLocked() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayCamerasPhaseLockLoopOnInputBit).intValue();
    return (getInputBitState(inputBit) == _xrayCameraArePhaseLocked);
  }

  /**
   * Used by service GUI to get input specfied by inputKeyString.
   *
   * @return boolean true if the slice io input is high
   * @author Rex Shang
   */
  public boolean getInputBitState(String inputKeyString) throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(inputKeyString).intValue();

    return (getInputBitState(inputBit) == 1);
  }

  /**
   * Used by service GUI to get the output bit state specified by
   * outputKeyString.
   *
   * @return boolean true if the slice io output is high
   * @author Rex Shang
   */
  public boolean getOutputBitState(String outputKeyString) throws XrayTesterException
  {
    Integer outputBit = getOutputBitFromMap(outputKeyString);
    int inputBit = _outputBitIdToInputBitIdMap.get(outputBit).intValue();
    
    if(getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED))
    {
      if(outputBit == _zAxisPositionBitAOutputBitNumber || outputBit == _zAxisPositionBitBOutputBitNumber)
        return (getInputBitState(inputBit) == 1 || getInputBitState(getInputBitFromMap(_zAxisAtMag3PositionInputBit)) == 1);
      else
        return (getInputBitState(inputBit) == 1);
    }
    else
      return (getInputBitState(inputBit) == 1);
  }

  /**
   * Used by service GUI to set an output bit state specified by
   * outputKeyString.
   *
   * @author Rex Shang
   */
  public boolean setOutputBitState(String outputKeyString, boolean state) throws XrayTesterException
  {
    int outputBit = getOutputBitFromMap(outputKeyString).intValue();

    int bitState = 0;
    if (state)
    {
      bitState = 1;
    }

    return setOutputBitState(outputBit, bitState);
  }

  /**
   * Used by service GUI to get the output bit number specified by
   * outputKeyString.
   *
   * @return output bit number
   * @author Anthony Fong
   */
  public int getOutputBitFromKeyString(String outputKeyString) throws XrayTesterException
  {
    return getOutputBitFromMap(outputKeyString).intValue();
  }

  public int getXrayCylinderDownOutputBitNumber() throws XrayTesterException
  {
    return _xrayCylinderDownOutputBitNumber;
  }
  
  public int getZAxisPositionBitAOutputBitNumber() throws XrayTesterException
  {
    return _zAxisPositionBitAOutputBitNumber;
  }
  
  public int getZAxisPositionBitBOutputBitNumber() throws XrayTesterException
  {
    return _zAxisPositionBitBOutputBitNumber;
  }  
  
  public int getZAxisEnableOutputBitNumber() throws XrayTesterException
  {
    return _zAxisEnableOutputBitNumber;
  } 

  public int getXrayCylinderClampOutputBitNumber() throws XrayTesterException
  {
    return _xrayCylinderClampOutputBitNumber;
  }

  public String getXrayCylinderClampOutputBitString() throws XrayTesterException
  {
    return _xrayCylinderClampOutputBit;
  }
  
  public String getInnerBarrierOutputBitString() throws XrayTesterException
  {
    return _innerBarrierOutputBit;
  }

  public int getInnerBarrierOutputBitNumber() throws XrayTesterException
  {
    return _InnerBarrierOutputBitNumber;
  }

  public int getPanelClampOutputBitNumber() throws XrayTesterException
  {
    return _panelClampOutputBitNumber;
  }

  public int getLeftPanelInPlaceSensorOutputBitNumber() throws XrayTesterException
  {
    return _leftPanelInPlaceSensorOutputBitNumber;
  }

  public int getRightPanelInPlaceSensorOutputBitNumber() throws XrayTesterException
  {
    return _rightPanelInPlaceSensorOutputBitNumber;
  }
  
  //Swee Yee
  public int getStageBeltsOutputBitNumber() throws XrayTesterException
  {
    return _runStageBeltsOutputBitNumber;
  }

  /**
   * Use the unique input bit key to look up hardware designation.
   *
   * @author Rex Shang
   */
  private Integer getInputBitFromMap(String inputBitKey)
  {
    Integer inputBit = _inputBitKeyToInputBitIdMap.get(inputBitKey);

    Assert.expect(inputBit != null, "Input id can not be found with key " + inputBitKey);
    return inputBit;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private boolean isInputBitExist(String inputBitKey)
  {
    Assert.expect(inputBitKey != null);
    return _inputBitKeyToInputBitIdMap.containsKey(inputBitKey);
  }

  /**
   * Use the unique output bit key to look up hardware designation.
   *
   * @author Rex Shang
   */
  private Integer getOutputBitFromMap(String outputBitKey)
  {
    Integer outputBit = _outputBitKeyToOutputBitIdMap.get(outputBitKey);

    Assert.expect(outputBit != null, "Output id can not be found with key " + outputBitKey);
    return outputBit;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private boolean isOutputBitExist(String outputBitKey)
  {
    Assert.expect(outputBitKey != null);
    return _outputBitKeyToOutputBitIdMap.containsKey(outputBitKey);
  }

  /**
   * Update our internal input bit state with new output state.
   */
  private void updateInternalInputBitFieldWithNewOutput(int outputBitNumber, int bitValue)
  {
    int correspondingInputBit = -1;
    Integer outputBitKey = new Integer(outputBitNumber);

    // Update output/input pair with non-inverted relationship. (set "1" to an
    // output will cause the corresponding input to go to "1").
    if (_outputBitIdToInputBitIdMap.containsKey(outputBitKey))
    {
      correspondingInputBit = _outputBitIdToInputBitIdMap.get(outputBitKey).intValue();
      _inputBitField.set(correspondingInputBit, new Integer(bitValue));
    }

    // Update output/input pair with inverted relationship. (set "1" to an
    // output will cause the corresponding input to go to "0").
    if (_outputBitIdToInputBitIdMapForTheInverted.containsKey(outputBitKey))
    {
      correspondingInputBit = _outputBitIdToInputBitIdMapForTheInverted.get(outputBitKey).intValue();
      if (bitValue == 0)
      {
        _inputBitField.set(correspondingInputBit, new Integer(1));
      }
      else
      {
        _inputBitField.set(correspondingInputBit, new Integer(0));
      }
    }
  }

  /**
   * Set an output bit to either a 1 or 0 Put pre-defined check here because
   * they are very critical, e.g. xray tube damage. Pre-defined requirements 1)
   * If xray tube is lowered down, not allow inner barrier to close. 2) If inner
   * barrier is closed, not allow xray tube to lower down.
   *
   * Please update the list above if you add in more pre-defined checks
   *
   * @return Return false only if pre-defined requirements are not met.
   * @author Rex Shang
   * @author Anthony Fong
   */
  private synchronized boolean setOutputBitState(int outputBitNumber, int bitValue) throws XrayTesterException
  {
    if (_initialized == false)
    {
      initialize();
      Assert.expect(_initialized, "Digital IO is not initialized.");
    }
    Assert.expect(outputBitNumber >= 0, "Output bit number " + outputBitNumber + "is not greater than or equal to 0.");
    Assert.expect(outputBitNumber <= _maximumOutputBitNumber, "Output bit number " + outputBitNumber + " is not less than " + _maximumOutputBitNumber + ".");
    Assert.expect(bitValue == 0 || bitValue == 1, "Digital IO bit value is out of range.  Value = " + bitValue);
    if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())// && isSimulationModeOn() == false)
    {
      if ((isOpticalStopControllerInstalled() == false) || (isOpticalStopControllerEndStopperInstalled() == true))
      {
        //Check if safe to trigger Panel Clamp Cylinder Move Up/Clamp
        if (outputBitNumber == _panelClampOutputBitNumber)
        {
          if (bitValue == _turnONOutputSignal)
          {
            if (isLeftPanelInPlaceSensorRetracted() && isRightPanelInPlaceSensorRetracted())
            {
              // OK to proceed
            }
            else
            {
              return false;  //It is not  safe to trigger panel clamp while PIP is not fully retracted.
            }
          }
        }
        //Check if safe to trigger left PIP Cylinder Extend/Rtract
        else if (outputBitNumber == _leftPanelInPlaceSensorOutputBitNumber)
        {
          if (arePanelClampsClosed())
          {
            return false;  //It is not safe to trigger left PIP cylinder while panel is clamped.
          }
        }
        //Check if safe to trigger right PIP Cylinder Extend/Rtract
        else if (outputBitNumber == _rightPanelInPlaceSensorOutputBitNumber)
        {
          if (arePanelClampsClosed())
          {
            return false;  //It is not safe to trigger right PIP cylinder while panel is clamped.
          }
        }
      }
    }
    
    if(XrayTester.isS2EXEnabled() && isSimulationModeOn() == false)
    {
      //Check if safe to trigger Z-Axis Up/Down
      if ((outputBitNumber == _zAxisPositionBitAOutputBitNumber)||
          (outputBitNumber == _zAxisPositionBitBOutputBitNumber)||
          (outputBitNumber == _zAxisEnableOutputBitNumber))
      {
        //if (bitValue == _turnXRayCylinderDownOn)
        {
          if (!isInnerBarrierOpen())
          {
            return false; // Not Safe to triffer Z-Axis Motor while Inner Barrier is Closed
          }
        }
      }
      //Check if safe to trigger Z-Axis Up/Down
      if ((outputBitNumber == _zAxisPositionBitAOutputBitNumber)||
          (outputBitNumber == _zAxisPositionBitBOutputBitNumber))
      {
        //if (bitValue == _turnXRayCylinderDownOn)
        {
          if (!isXrayZAxisEnabled())
          {
            return false; // Not Safe to triffer Z-Axis Motor while Inner Barrier is Closed
          }
        }
      }
      //Check if safe to trigger Inner Barrier Close
      if (outputBitNumber == _InnerBarrierOutputBitNumber)
      {
        if (bitValue == _closeInnerBarrier)
        {
          if (!isXrayZAxisHome())
          {
            return false; // Not Safe to triffer X-Ray Cylinder while  Inner Barrier is Closed
          }
        }
      }
    }
        
    if (XrayCylinderActuator.isInstalled() && isSimulationModeOn() == false)
    {
      //Check if safe to trigger Xray Cylinder Down
      if (outputBitNumber == _xrayCylinderDownOutputBitNumber)
      {
        if (bitValue == _turnXRayCylinderDownOn)
        {
          if (!isInnerBarrierOpen())
          {
            return false; // Not Safe to triffer X-Ray Cylinder while  Inner Barrier is Closed
          }
        }
      }
      //Check if safe to trigger Inner Barrier Close
      if (outputBitNumber == _InnerBarrierOutputBitNumber)
      {
        if (bitValue == _closeInnerBarrier)
        {
          if (!isXrayCylinderUp())
          {
            return false; // Not Safe to triffer X-Ray Cylinder while  Inner Barrier is Closed
          }
        }
      }
    }
    
    //Swee Yee - prevent belt turn on when panel is clamped
    if (outputBitNumber == _runStageBeltsOutputBitNumber)
    {
      if (bitValue == _turnStageBeltsOn)
      {
        if (arePanelClampsClosed())
        {
          return false;  //It will cause the belts wear out if turn on the stage when panel is clamped.
        }
      }
    }
    
    if (outputBitNumber == _panelClampOutputBitNumber)
    {
      if (bitValue == _turnONOutputSignal)
      {
        if (areStageBeltsOn())
        {
          return false;  //It will cause the belt wear out if clamp the panel when belts are on.
        }
      }
    }

    if (isSimulationModeOn() == false)
    {
      try
      {
        _rdioi.remoteSetOutputBitState(outputBitNumber, bitValue);
      }
      catch (NativeHardwareException nhe)
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
      catch (RemoteException re)
      {
        if (_debug)
        {
          System.out.println("setOutputBitState (outputBitNumber:" + outputBitNumber + ", bitValue: " + bitValue + " ) re:" + re.getMessage());
        }
        // try again 1 times
        try
        {
          waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS);
          _rdioi.remoteSetOutputBitState(outputBitNumber, bitValue);
        }
        catch (NativeHardwareException nhe)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        catch (RemoteException re2)
        {
          _initialized = false;
          if (_debug)
          {
            System.out.println("setOutputBitState (outputBitNumber:" + outputBitNumber + ", bitValue: " + bitValue + " ) re2:" + re2.getMessage());
          }
          _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re2));
        }
      }
      catch (Exception e)
      {
        _initialized = false;
        if (_debug)
        {
          System.out.println("e:" + e.getMessage());
        }
        DigitalIoException dioe = DigitalIoException.getGeneralException(e);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
    }

    updateInternalInputBitFieldWithNewOutput(outputBitNumber, bitValue);
    return true;
  }

  /**
   * Get the input bit state
   *
   * @author Rex Shang
   */
  private synchronized int getInputBitState(int inputBitNumber) throws XrayTesterException
  {
    if (_initialized == false)
    {
//      initialize();
//      Assert.expect(_initialized);
      // Siew Yeng - XCR1426
      // Remove auto initialize to avoid hardwareWorkerThread assert. Throw hardware exception if digitalIo is not initialized.
      DigitalIoException dioe = DigitalIoException.getDigitalIoNotInitializedException();
      _hardwareTaskEngine.throwHardwareException(dioe);
    }
    Assert.expect(inputBitNumber >= 0, "Input bit number " + inputBitNumber + "is not greater than or equal to 0.");
    Assert.expect(inputBitNumber < _numberOfInputBits, "Input bit number " + inputBitNumber + " is not less than " + _numberOfInputBits + ".");

    int returnValue = -1;
    if (isSimulationModeOn() || inputBitNumber > _maximumInputBitNumber)
    {
      // inputBitNumber > _maxInputBitNumber indicates a software only input
      // bit. It is used to record the output status of hardware such as
      // panel clamps that does not have paired io.
      returnValue = _inputBitField.get(inputBitNumber).intValue();
    }
    else
    {
      try
      {
        returnValue = _rdioi.remoteGetInputBitState(inputBitNumber);
      }
      catch (NativeHardwareException nhe)
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
      catch (RemoteException re)
      {
        if (_debug)
        {
          System.out.println("getInputBitState (inputBitNumber:" + inputBitNumber + " ) re:" + re.getMessage());
        }
        // try again 1 times
        // wei chin
        try
        {
          waitForHardwareInMilliSeconds(_REMOTE_TIME_OUT_RETRY_IN_MILLISECONDS);
          returnValue = _rdioi.remoteGetInputBitState(inputBitNumber);
        }
        catch (NativeHardwareException nhe)
        {
          _initialized = false;
          DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
          _hardwareTaskEngine.throwHardwareException(dioe);
        }
        catch (RemoteException re2)
        {
          _initialized = false;
          if (_debug)
          {
            System.out.println("getInputBitState (inputBitNumber:" + inputBitNumber + " ) re2:" + re.getMessage());
          }
          _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re2));
        }
      }
      catch (Exception e)
      {
        _initialized = false;
        if (_debug)
        {
          System.out.println("e:" + e.getMessage());
        }
        DigitalIoException dioe = DigitalIoException.getGeneralException(e);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
    }

    Assert.expect(returnValue == 0 || returnValue == 1, "Digital IO bit value is out of range.  Value = " + returnValue);
    return returnValue;
  }

  /**
   * Have the current thread wait for the hardware to finish its action.
   *
   * @param timeOutInMilliSeconds long time out of 0 will return right away.
   * @author Rex Shang
   */
  void waitForHardwareInMilliSeconds(long timeOutInMilliSeconds)
  {
    Assert.expect(timeOutInMilliSeconds >= 0, "Time out " + timeOutInMilliSeconds + " is not greater than or equal to 0.");

    if (isSimulationModeOn())
    {
      return;
    }

    long deadLineInMilliSeconds = timeOutInMilliSeconds + System.currentTimeMillis();
    long timeToWait = timeOutInMilliSeconds;

    while (isAborting() == false && timeToWait > 0)
    {
      try
      {
        Thread.sleep(timeToWait);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }
      finally
      {
        // Calculate how much more time we need to wait.
        timeToWait = deadLineInMilliSeconds - System.currentTimeMillis();
      }
    }
  }

  /**
   * @author Rex Shang
   */
  private void setConfigurationDescription() throws XrayTesterException
  {
    StringBuffer key = new StringBuffer();
    List<String> parameters = new ArrayList<String>();

    if (isSimulationModeOn())
    {
      key.append("HW_DIGITAL_IO_CONFIGURATION_DESCRIPTION_IN_SIMULATION_KEY");
      _configDescription = new HardwareConfigurationDescription(key.toString(), parameters);
    }
    else
    {
      try
      {
        Serializable configDescription = _rdioi.remoteGetConfigurationDescription(key, parameters);
        _configDescription = (HardwareConfigurationDescription) configDescription;
      }
      catch (NativeHardwareException nhe)
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoExceptionBasedOnNativeHardwareException(nhe);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
      catch (RemoteException re)
      {
        _initialized = false;
        if (_debug)
        {
          System.out.println("setConfigurationDescription re:" + re.getMessage());
        }
        _hardwareTaskEngine.throwHardwareException(DigitalIoException.getRMIException(re));
      }
      catch (Exception e)
      {
        _initialized = false;
        if (_debug)
        {
          System.out.println("e:" + e.getMessage());
        }
        DigitalIoException dioe = DigitalIoException.getGeneralException(e);
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public HardwareConfigurationDescriptionInt getConfigurationDescription()
  {
    Assert.expect(_configDescription != null);

    return _configDescription;
  }

  /**
   * @author Chong, Wei Chin
   */
  public static void resetRemoteDigitalIO()
  {
    _rdioi = null;
  }

  /**
   * @author Anthony Fong
   */
  public static boolean isOpticalStopControllerInstalled()
  {
    if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      return getConfig().getBooleanValue(HardwareConfigEnum.OPTICAL_STOP_CONTROLLER_INSTALLED);
    }
    else
    {
      return false;
    }
  }

  /**
   * @author Anthony Fong
   */
  public boolean isOpticalStopControllerEndStopperInstalled()
  {
    if (XrayTester.isXXLBased()|| XrayTester.isS2EXEnabled())
    {
      boolean opticalStopControllerEndStopperInstalled = getConfig().getBooleanValue(HardwareConfigEnum.OPTICAL_STOP_CONTROLLER_END_STOPPER_INSTALLED);
      if(isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        if(opticalStopControllerEndStopperInstalled==false)
        {
          try
          {
            DigitalIoException dioe = DigitalIoException.getDigitalIoOpticalStopControllerEndStopperResumedAsNormalPIPNotInitializedException();
            _hardwareTaskEngine.throwHardwareException(dioe);
          }
          catch (XrayTesterException ex)
          {
            // Do nothing
          }
        }
      }
      return opticalStopControllerEndStopperInstalled; 
    }
    else
    {
      return false;
    }
  }
  
  /**
   * @author Anthony Fong
   */
  public static boolean isOpticalStopControllerEndStopperResumedAsNormalPIP()
  {
    if (XrayTester.isXXLBased()|| XrayTester.isS2EXEnabled())
    {
      return getConfig().getBooleanValue(HardwareConfigEnum.OPTICAL_STOP_CONTROLLER_END_STOPPER_RESUMED_AS_NORMAL_PIP);
    }
    else
    {
      return false;
    }
  }
    /**
   * @author Swee-Yee.Wong
   */
  public static boolean isOpticalPIPResetInstalled()
  {
    if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      return getConfig().getBooleanValue(HardwareConfigEnum.OPTICAL_PIP_RESET_INSTALLED);
    }
    else
    {
      return false;
    }
  }
  
  /**
   * @author Swee-Yee.Wong 
   */
  public int getOpticalPIPResetSignalDuration()
  {
    return getConfig().getIntValue(HardwareConfigEnum.OPTICAL_PIP_RESET_SIGNAL_DURATION);
  }

  /**
   * Check if the Optical Stop Controller Error
   *
   * @author Anthony Fong
   */
  public boolean isOpticalStopControllerError() throws XrayTesterException
  {
    if (_bypassOpticalStopController || isSimulationModeOn())
    {
      return false;
    }
    int inputBit = getInputBitFromMap(_opticalStopControllerErrorInputBit).intValue();
    int opticalStopControllerErrorInputBitState = getInputBitState(inputBit);

    if (opticalStopControllerErrorInputBitState == _opticalStopControllerIsError)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Check if the Optical Stop Controller Done
   *
   * @author Anthony Fong
   */
  public boolean isOpticalStopControllerDone() throws XrayTesterException
  {
    if (_bypassOpticalStopController || isSimulationModeOn())
    {
      return true;
    }
    int inputBit = getInputBitFromMap(_opticalStopControllerDoneInputBit).intValue();
    int opticalStopControllerDoneInputBitState = getInputBitState(inputBit);

    if (opticalStopControllerDoneInputBitState == _opticalStopControllerIsDone)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Enable VIE Optical Stop Controller
   *
   * @author Anthony Fong - For XXL optical stop Controller
   */
  void setEnableXXLOpticalStopController(boolean enable) throws XrayTesterException
  {
    if (isOpticalStopControllerInstalled())
    {
      if (_bypassOpticalStopController || isSimulationModeOn())
      {
        int outputBit = getOutputBitFromMap(_enableXXLOpticalStopControllerOutputBit).intValue();
        setOutputBitState(outputBit, 0);
      }
      else
      {
        int outputBit = getOutputBitFromMap(_enableXXLOpticalStopControllerOutputBit).intValue();
        setOutputBitState(outputBit, enable ? 1 : 0);
      }

    }
  }
  
    /**
   * Set Optical PIP Sensor - Reset ON
   *
   * @author Swee Yee - For Optical PIP Sensor Signal Strength Stabilization
   */
  void setOpticalPIPSensorResetON() throws XrayTesterException
  {
    if(isOpticalStopControllerInstalled() && isOpticalPIPResetInstalled())
    {
       int outputBit = getOutputBitFromMap(_resetOpticalPIPSensorOutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set Optical PIP Sensor - Reset OFF
   *
   * @author Swee Yee - For Optical PIP Sensor Signal Strength Stabilization
   */
  void setOpticalPIPSensorResetOFF() throws XrayTesterException
  {
    if(isOpticalStopControllerInstalled() && isOpticalPIPResetInstalled())
    {
       int outputBit = getOutputBitFromMap(_resetOpticalPIPSensorOutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  }
  
  /**
   * @author Anthony Fong S2EX
   */
  public static boolean isS2EXEnabled()
  {
    return XrayTester.isS2EXEnabled();
  }
  
  /**
   * @author Anthony Fong S2EX
   */
  public static boolean isXrayMotorizedHeightLevelSafetySensorInstalled()
  {
    if (XrayTester.isS2EXEnabled())
    {
      return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_INSTALLED);
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Reset ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorResetON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Reset OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorResetOFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorResetAlarmOutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Reset ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorHomingON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorHomingOutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Homing OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorHomingOFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorHomingOutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  }

  /**
   * Set XRay Motorized Height Level Safety Sensor - Start Motion ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorStartMotionON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Start Motion OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorStartMotionOFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorStartMotionOutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  } 
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit0 ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit0ON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit0 OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit0OFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit0OutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  } 
  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit1 ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit1ON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit1 OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit1OFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit1OutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  }

  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit2 ON
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit2ON() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit).intValue();
       setOutputBitState(outputBit, _io_ON);
      
    }
  }
  
  /**
   * Set XRay Motorized Height Level Safety Sensor - Position Bit2 OFF
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  void setXrayMotorizedHeightLevelSafetySensorPositionBit2OFF() throws XrayTesterException
  {
    if (isXrayMotorizedHeightLevelSafetySensorInstalled())
    {
       int outputBit = getOutputBitFromMap(_xrayMotorizedHeightLevelSafetySensorPositionBit2OutputBit).intValue();
       setOutputBitState(outputBit, _io_OFF);
      
    }
  } 
  
 /**
   * Is XRay Motorized Height Level Safety Sensor - Motor Controller Alarm
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  boolean isXrayMotorizedHeightLevelSafetySensorMotorControllerAlarm() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorMotorControllerAlarmInputBit).intValue();
    int motorControllerAlarmState = getInputBitState(inputBit);
    return motorControllerAlarmState == _io_NegativeON;
  }
  
   /**
   * Is XRay Motorized Height Level Safety Sensor - Up (At Mag 19 Micron)
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  boolean isXrayMotorizedHeightLevelSafetySensorUp() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBit).intValue();
    int heightLevelSafetySensorAtMag19MicronState = getInputBitState(inputBit);
    return heightLevelSafetySensorAtMag19MicronState == _io_ON;
  }
   
  boolean isXrayMotorizedHeightLevelSafetySensorNotUp() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorAtMag19MicronInputBit).intValue();
    int heightLevelSafetySensorAtMag19MicronState = getInputBitState(inputBit);
    return heightLevelSafetySensorAtMag19MicronState == _io_OFF;
  }
  /**
   * Is XRay Motorized Height Level Safety Sensor - Down (At Mag 11 Micron)
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  boolean isXrayMotorizedHeightLevelSafetySensorDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBit).intValue();
    int heightLevelSafetySensorAtMag11MicronState = getInputBitState(inputBit);
    return heightLevelSafetySensorAtMag11MicronState == _io_ON;
  }
  boolean isXrayMotorizedHeightLevelSafetySensorNotDown() throws XrayTesterException
  {
    int inputBit = getInputBitFromMap(_xrayMotorizedHeightLevelSafetySensorAtMag11MicronInputBit).intValue();
    int heightLevelSafetySensorAtMag11MicronState = getInputBitState(inputBit);
    return heightLevelSafetySensorAtMag11MicronState == _io_OFF;
  }
    
  /**
   *XRay Motorized Height Level Safety Sensor - Reset
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  public void resetXrayMotorizedHeightLevelMotionController() throws XrayTesterException
  {    
  
    if(isXrayMotorizedHeightLevelSafetySensorMotorControllerAlarm())
    {
      setXrayMotorizedHeightLevelSafetySensorResetOFF();
      sleep(100);
      setXrayMotorizedHeightLevelSafetySensorResetON();
      sleep(200);
      setXrayMotorizedHeightLevelSafetySensorResetOFF();
    }
  }
  /**
   * XRay Motorized Height Level Safety Sensor - Move to Home Position
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  public void moveMotorizedHeightLevelToHomePosition() throws XrayTesterException
  {      

      resetXrayMotorizedHeightLevelMotionController();
      setXrayMotorizedHeightLevelSafetySensorStartMotionOFF();
       //Always turn of PositionBit0, PositionBit1 and PositionBit2 for Homing
      setXrayMotorizedHeightLevelSafetySensorPositionBit0OFF();
      setXrayMotorizedHeightLevelSafetySensorPositionBit1OFF();
      setXrayMotorizedHeightLevelSafetySensorPositionBit2OFF();
      setXrayMotorizedHeightLevelSafetySensorHomingOFF();
      sleep(50);
      setXrayMotorizedHeightLevelSafetySensorHomingON();
  }
  
  /**
   * XRay Motorized Height Level Safety Sensor - Move to Low Mag Position
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  public void moveMotorizedHeightLevelToLowMagPosition() throws XrayTesterException
  { 
      resetXrayMotorizedHeightLevelMotionController();
      //Turn of PositionBit0 only for Low Mag Position
      setXrayMotorizedHeightLevelSafetySensorPositionBit0ON();
      setXrayMotorizedHeightLevelSafetySensorPositionBit1OFF();
      setXrayMotorizedHeightLevelSafetySensorPositionBit2OFF();  
      setXrayMotorizedHeightLevelSafetySensorStartMotionOFF();
      setXrayMotorizedHeightLevelSafetySensorHomingOFF();
      sleep(50);
      setXrayMotorizedHeightLevelSafetySensorStartMotionON();    
  }
  
  /**
   * XRay Motorized Height Level Safety Sensor - Move to High Mag Position
   *
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  public void moveMotorizedHeightLevelToHighMagPosition() throws XrayTesterException
  {      
      resetXrayMotorizedHeightLevelMotionController();
      //Turn of PositionBit1 only for Low Mag Position
      setXrayMotorizedHeightLevelSafetySensorPositionBit0OFF();
      setXrayMotorizedHeightLevelSafetySensorPositionBit1ON();
      setXrayMotorizedHeightLevelSafetySensorPositionBit2OFF();       
      setXrayMotorizedHeightLevelSafetySensorStartMotionOFF();
      setXrayMotorizedHeightLevelSafetySensorHomingOFF();
      sleep(50);
      setXrayMotorizedHeightLevelSafetySensorStartMotionON();    
  }
      
  /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Anthony Fong- For XRay Motorized Height Level Safety Sensor
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  long getXrayMotorizedHeightLevelMoveHomeTimeOutInMilliSeconds()
  {
    return _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME_TIME_IN_MILLI_SECONDS;
  }
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  long getXrayMotorizedHeightLevelMoveLowMagTimeOutInMilliSeconds()
  {
    return _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_LOW_MAG_TIME_IN_MILLI_SECONDS;
  } 
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  long getXrayMotorizedHeightLevelMoveHighMagTimeOutInMilliSeconds()
  {
    return _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HIGH_MAG_TIME_IN_MILLI_SECONDS;
  } 
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  long getXrayMotorizedHeightLevelSettlingTimeOutInMilliSeconds()
  {
    return _XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_SETTLING_TIME_IN_MILLI_SECONDS;
  }
  
  /**
   *Optical PIP Sensor - Reset
   *
   * @author Swee-Yee.Wong - To stablelize Optical PIP Sensor Signal Strength
   */
  public void resetOpticalPIPSensor() throws XrayTesterException
  {    
    if(isOpticalStopControllerInstalled() && isOpticalPIPResetInstalled())
    {
      //Bypass reset Optical PIP Sensor
      if(isOpticalStopControllerEndStopperResumedAsNormalPIP())
      return;
      
      if (arePanelClampsClosed())
      {
        openPanelClamps();
        // Swee-Yee.Wong
        // This delay is used to compensate the respond time of optical stop sensor
        // Minimum 300ms is needed to prevent inaccurate Optical PIP sensor value
        sleep(500);
      }
      setOpticalPIPSensorResetOFF();
      sleep(20);
      setOpticalPIPSensorResetON();
      sleep(getOpticalPIPResetSignalDuration());
      setOpticalPIPSensorResetOFF();
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public boolean isXrayTubeCommandedHome()
  {
    return _isXrayTubeCommandedHome;
  }
    
  /**
   * @author Swee-Yee.Wong
   */
  public boolean isInnerBarrierCommandedClose()
  {
    return _isInnerBarrierCommandedClose;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public boolean isInnerBarrierInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_INNER_BARRIER_INSTALLED);
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private void logHardwareStatus(String message)
  {
    try
    {
      HardwareStatusLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to log hardwareStatus. \n" + e.getMessage());
    }
  }
  
  /**
   * XCR1144, Chnee Khang Wah, 22-Dec-2010
   * flag to by pass panel clear sensor checking
   * @author Khang-Wah, Chnee
   * move from panel positioner to digitalio by sheng chuan
   * XCR-3409, Failed to run CDNA although already select there are No board in the system
   */
  public void setByPassPanelClearSensor(boolean isByPass)
  {
    _isByPassPanelClearSensor = isByPass;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean isByPassPanelClearSensor()
  {
    return _isByPassPanelClearSensor;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  boolean isSmemaSensorOneTrigger() throws XrayTesterException
  {
    if (isInputBitExist(_smemaSignalOneDebugInputBit) == false)
      return false;
    
    int inputBit = getInputBitFromMap(_smemaSignalOneDebugInputBit).intValue();
    return (getInputBitState(inputBit) == _smemaSignalSensorTrigger);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  boolean isSmemaSensorTwoTrigger() throws XrayTesterException
  {
    if (isInputBitExist(_smemaSignalTwoDebugInputBit) == false)
      return false;
    
    int inputBit = getInputBitFromMap(_smemaSignalTwoDebugInputBit).intValue();
    return (getInputBitState(inputBit) == _smemaSignalSensorTrigger);
  }
  
  // XCR 3656: System crash when load S2EX V810 application if opticalStopControllerEndStopperInstalled=false
  //check the optical stop sensor settings in hanrdware config
  public void checkOpticalStopSensorSettings() throws XrayTesterException
  {
    if (XrayTester.isS2EXEnabled())
    {
      if ((isOpticalStopControllerInstalled() == false) || 
          (isOpticalStopControllerEndStopperInstalled() == false))
      {
        _initialized = false;
        DigitalIoException dioe = DigitalIoException.getDigitalIoOpticalStopSensorSettingsInvalidException();
        _hardwareTaskEngine.throwHardwareException(dioe);
      }
    }
  }
}
