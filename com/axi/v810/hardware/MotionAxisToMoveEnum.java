package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class MotionAxisToMoveEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;

  public static final MotionAxisToMoveEnum X_INDEPENDENT_AXIS = new MotionAxisToMoveEnum(++_index);
  public static final MotionAxisToMoveEnum Y_INDEPENDENT_AXIS = new MotionAxisToMoveEnum(++_index);
  public static final MotionAxisToMoveEnum RAILWIDTH_INDEPENDENT_AXIS = new MotionAxisToMoveEnum(++_index);
  public static final MotionAxisToMoveEnum XY_COORDINATED_AXIS = new MotionAxisToMoveEnum(++_index);


  /**
   * @author Bill Darbie
   */
  private MotionAxisToMoveEnum(int id)
  {
    super(id);
  }
}
