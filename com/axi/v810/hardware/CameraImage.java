package com.axi.v810.hardware;

/**
 * This is the base class that all image classes should inherit from.
 *
 * @author Bill Darbie
 */
public abstract class CameraImage
{
}