package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;


/**
 * @author Greg Esparza
 */
public class MotionControllerTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<String, MotionControllerTypeEnum> _motionControllerModelNameToModelTypeEnumMap;

  /**
   * @author Greg Esparza
   */
  static
  {
    _motionControllerModelNameToModelTypeEnumMap = new HashMap<String,MotionControllerTypeEnum>();
  }

  public static final MotionControllerTypeEnum  XMP_SYNQNET_PCI = new MotionControllerTypeEnum(++_index, HardwareConfigEnum.getXmpSynqPciMotionControllerModelName());

  /**
   * @author Greg Esparza
   */
  private MotionControllerTypeEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _motionControllerModelNameToModelTypeEnumMap.put(name, this);
  }

  /**
   * @author Greg Esparza
   */
  public static MotionControllerTypeEnum getEnum(String motionControllerName)
  {
    Assert.expect(motionControllerName != null);

    MotionControllerTypeEnum motionControllerTypeEnum = _motionControllerModelNameToModelTypeEnumMap.get(motionControllerName);
    Assert.expect(motionControllerTypeEnum != null);

    return motionControllerTypeEnum;
  }
}
