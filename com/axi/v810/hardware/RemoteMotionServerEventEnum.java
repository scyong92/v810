package com.axi.v810.hardware;

import com.axi.util.*;
/**
 * @author Chong, Wei Chin
 */
public class RemoteMotionServerEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static RemoteMotionServerEventEnum INITIALIZING_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Initialize");
  public static RemoteMotionServerEventEnum RESTARTING_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Restart");
  public static RemoteMotionServerEventEnum STARTING_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Start");
  public static RemoteMotionServerEventEnum STOPPING_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Stop");
  public static RemoteMotionServerEventEnum REBOOTING_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Reboot");
  public static RemoteMotionServerEventEnum SHUTDOWN_REMOTE_MOTION_SERVER = new RemoteMotionServerEventEnum(++_index, "Remote Motion Server, Shutdown");

  private String _name;

  /**
   * @author Chong, Wei Chin
   */
  private RemoteMotionServerEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Chong, Wei Chin
   */
  public String toString()
  {
    return _name;
  }

}
