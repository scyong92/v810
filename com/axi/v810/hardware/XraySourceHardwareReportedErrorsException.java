package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * If the hardware reports errors in response to a request for its status...
 *
 * @author Greg Loring
 * @author George Loring Booth
 */
public class XraySourceHardwareReportedErrorsException extends HardwareException
{
  // keys for individual error messages associated with each bit of the statusBitField
  private static final String[] _errorKeys = new String[] {
/** @todo wpd - Greg L - please combine some of these and make the messages more user actionable
        see Greg Esparza for formating and actionable message information
        combine interlock 1,2,and 3 into a single message
        also, in the Localization.properties file, do not put a newline in front of the messages - put
        that in the code instead when you append things.
 **/
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_SHUTDOWN_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_INTERLOCK_1_OFF_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_INTERLOCK_2_OFF_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_INTERLOCK_3_OFF_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_REGULATION_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_ARC_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_OVER_VOLTAGE_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_OVER_CURRENT_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_TEMPERATURE_LIMIT_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_FIBER_LINK_COMMUNICATION_ERROR_KEY",
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_HTUBE_CONTROL_BOARD_ERROR_KEY",        // 1024
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_HTUBE_INPUT_SUPPLY_ERROR_KEY",         // 2048
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_HTUBE_COOLING_FAN_ERROR_KEY",          // 4096
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_HTUBE_TEMPERATURE_ALARM_ERROR_KEY",     // 8192
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_UNKNOWN_INTERLOCK_STATUS_ERROR_KEY",     // 16384
    "HW_XRAY_SOURCE_HARDWARE_REPORTED_UNKNOWN_HARDWARE_STATUS_ERROR_KEY"       //32768
  };

  private int _statusBitField;
  private Object[] _arguments = null;

  /**
   * @author Greg Loring
   */
  public XraySourceHardwareReportedErrorsException(int statusBitField)
  {
    super (new LocalizedString("HW_XRAY_SOURCE_HARDWARE_REPORTED_ERRORS_KEY", null));
    Assert.expect(statusBitField != 1);
    _statusBitField = statusBitField;
  }
  
  //XCR-3529: Proper handle Xraytube Exception intead of assert in SIN 0 Error
  public XraySourceHardwareReportedErrorsException(int statusBitField, Object[] arguments)
  {
    super (new LocalizedString("HW_XRAY_SOURCE_HARDWARE_REPORTED_ERRORS_KEY", null));
    Assert.expect(statusBitField != 1);
    _statusBitField = statusBitField;
    _arguments = arguments;
  }

  /**
   * concatenates together all of the various individual error messages that may be
   *  represented by the statusBitField given to the ctor<p/>
   *
   * @author Greg Loring
   */
  public String getLocalizedMessage()
  {
    String error;
    StringBuffer sb = new StringBuffer();

    sb.append(super.getLocalizedMessage());

    for (int i = 0; i < _errorKeys.length; i++)
    {
      int mask = 0x1 << i;
      if ((_statusBitField & mask) != 0)
      {
        String errorKey = _errorKeys[i];
        //XCR-3529: Proper handle Xraytube Exception intead of assert in SIN 0 Error
        if (_arguments == null)
        {    
          error = StringLocalizer.keyToString(errorKey);
        }
        else
        {
          LocalizedString unknownError = new LocalizedString(errorKey,_arguments);
          error = StringLocalizer.keyToString(unknownError);
        }
        sb.append(" ");
        sb.append(error);
      }
    }

    String result = sb.toString();
    return result;
  }

}
