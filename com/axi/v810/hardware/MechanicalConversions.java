package com.axi.v810.hardware;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This is a set of utility functions used for unit conversion.
 *
 * @author Roy Williams
 */
public class MechanicalConversions
{
  private List<ProcessorStrip> _processorStrips;
  private List<SystemFiducialRectangle> _processorStripsSystemFiducicalRectangle;
  private int _panelWidthWithAlignmentUncertainty = 0;

  private int _alignmentUncertainty = -1;

  private SystemFiducialRectangle _rectangleToImage;
  private List<AbstractXrayCamera> _listOfCamerasToUseForImaging;
  private int _stageTravelStart = -1;
  private int _stageTravelEnd = -1;

  private double _zHeightDeltaUp = -1;
  private double _zHeightDeltaDown = -1;
  private int _maxYofCamerasInNanometers = -1;
  private int _minYofCamerasInNanometers = -1;
  private int _maxPositionalDelay = -1;
  private int _imagePointStartAtMinCamera = -1;
  private int _minYAssociatedWithLeadingEdgeDistance = 0;
  
  //swee-yee.wong - XCR-2630 Support New X-Ray Camera
  //gen3 camera backward compatible might have mix camera, 
  //so we should calculate these values for each camera
//  private int _cameraHalfHeightInNanometers = -1;
//  private int _cameraHalfHeightInNanometersAtReferencePlane = -1;
  
  private Map<AbstractXrayCamera, Map<ProcessorStrip, ImageRectangle>> _projectionRectanglesMapsForCameras = new HashMap<AbstractXrayCamera, Map<ProcessorStrip, ImageRectangle>>();

  private AffineTransform _affineTransform = null;

  static private Config _config = Config.getInstance();
  static private XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();
  static private AbstractXrayCamera _xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
 
  // Comment by Wei Chin
  //static private int _scanDirectionPixelSizeInNanometers = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();

  static private int _minimumStageTravelInNanometers = -1;
  static private PanelPositioner _panelPositioner = PanelPositioner.getInstance();

  private int _distanceBeforeLeadingEdgeFromFirstCamera = -1;
  private int _distanceAfterTrailingEdgeFromLastCamera = -1;
  private Map<AbstractXrayCamera, Integer> _cameraToDistanceBeforeLeadingEdgeMap = new HashMap<AbstractXrayCamera, Integer>();
  private Map<AbstractXrayCamera, Integer> _cameraToDistanceAfterTrailingEdgeMap = new HashMap<AbstractXrayCamera, Integer>();
  
  private MagnificationEnum _magnification = null;

  /**
   * These calculations need to be performed relative to the xraySpot location
   * which can change during calibration.
   *
   * @author Roy Williams
   */
  public MechanicalConversions(
      List<ProcessorStrip> processorStrips,
      AffineTransform affineTransform,
      int alignmentUncertainty,
      int largestUndivisiblePadInNanometers,
      double zHeightDeltaAboveNominalInNanometers,
      double zHeightDeltaBelowNominalInNanometers,
      List<AbstractXrayCamera> listOfCamerasToUseForImaging,
      SystemFiducialRectangle rectangleToImage) throws XrayTesterException
  {
    Assert.expect(processorStrips != null);
    Assert.expect(alignmentUncertainty >= 0); // zero needed for calibrations.
    Assert.expect(largestUndivisiblePadInNanometers >= 0); // zero needed for calibrations.
    Assert.expect(listOfCamerasToUseForImaging != null);
    Assert.expect(listOfCamerasToUseForImaging.size() > 0);
    Assert.expect(rectangleToImage != null);

    // Both zHeight numbers are expected to be positive in sign.   All equations
    // below add/subtract as needed expecting both to be positive.
    Assert.expect(zHeightDeltaAboveNominalInNanometers >= 0);
    Assert.expect(zHeightDeltaBelowNominalInNanometers >= 0);
    
    _magnification = MagnificationEnum.getCurrentNorminal();

    _processorStrips = processorStrips;
    _affineTransform = affineTransform;
    _alignmentUncertainty = alignmentUncertainty;

    // maximum upslice/downSlice from nominal.
    _zHeightDeltaUp = zHeightDeltaAboveNominalInNanometers; // focalRangeUpsliceInNanometers();
    _zHeightDeltaDown = zHeightDeltaBelowNominalInNanometers; // focalRangeDownsliceInNanometers();

    _listOfCamerasToUseForImaging = listOfCamerasToUseForImaging;
    _rectangleToImage = new SystemFiducialRectangle(
        rectangleToImage.getMinX() - _alignmentUncertainty,
        rectangleToImage.getMinY() - _alignmentUncertainty,
        rectangleToImage.getWidth() + 2 * _alignmentUncertainty,
        rectangleToImage.getHeight() + 2 * _alignmentUncertainty);
    _panelWidthWithAlignmentUncertainty = _rectangleToImage.getHeight(); // default (changed later).
    _cameraToDistanceBeforeLeadingEdgeMap.clear();
    _cameraToDistanceAfterTrailingEdgeMap.clear();

    //swee-yee.wong - XCR-2630 Support New X-Ray Camera
    //gen3 camera backward compatible might have mix camera, 
    //so we should calculate these values for each camera
    // Just get any camera so we can get the sensor properties
//    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
//    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
//    int cameraHalfHeight = xRayCamera.getNumberOfSensorRows() / 2;
//    _cameraHalfHeightInNanometers = cameraHalfHeight * xRayCamera.getSensorRowPitchInNanometers();
//    _cameraHalfHeightInNanometersAtReferencePlane = cameraHalfHeight * _magnification.getNanoMetersPerPixel();
    
    recomputePositionalLocations();
    initializeProjectionRectanglesMapsForCameras();
  }
  
  public void reInitialize(
      List<ProcessorStrip> processorStrips,
      SystemFiducialRectangle rectangleToImage) throws XrayTesterException
  {
    _processorStrips = processorStrips;

    _rectangleToImage = new SystemFiducialRectangle(
        rectangleToImage.getMinX() - _alignmentUncertainty,
        rectangleToImage.getMinY() - _alignmentUncertainty,
        rectangleToImage.getWidth() + 2 * _alignmentUncertainty,
        rectangleToImage.getHeight() + 2 * _alignmentUncertainty);
    _panelWidthWithAlignmentUncertainty = _rectangleToImage.getHeight(); // default (changed later).
    _cameraToDistanceBeforeLeadingEdgeMap.clear();
    _cameraToDistanceAfterTrailingEdgeMap.clear();

    //swee-yee.wong - XCR-2630 Support New X-Ray Camera
	//gen3 camera backward compatible might have mix camera, 
    //so we should calculate these values for each camera
    // Just get any camera so we can get the sensor properties
//    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
//    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
//    int cameraHalfHeight = xRayCamera.getNumberOfSensorRows() / 2;
//    _cameraHalfHeightInNanometers = cameraHalfHeight * xRayCamera.getSensorRowPitchInNanometers();
//    _cameraHalfHeightInNanometersAtReferencePlane = cameraHalfHeight * _magnification.getNanoMetersPerPixel();
    
    recomputePositionalLocations();
    initializeProjectionRectanglesMapsForCameras();
  }
  
  /**
   * @author Roy Williams
   */
  public static void startup() throws XrayTesterException
  {
    if (_minimumStageTravelInNanometers < 0)
    {
      Assert.expect(_panelPositioner.isStartupRequired() == false, "Panel positioner should be started already.");
 
        if(XrayTester.isLowMagnification())
        {
          String zAxisMotorPositionForLowMag = Config.getSystemCurrentLowMagnification();
          _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForLowMag)));
        }
        else
        {
          String zAxisMotorPositionForHighMag = Config.getSystemCurrentHighMagnification();
          _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForHighMag)));
        }
      
      _minimumStageTravelInNanometers = _panelPositioner.getMinimumScanPassLengthInNanometers();

      CameraTrigger cameraTrigger = CameraTrigger.getInstance();
      int nominalPixelSizeInNanometers = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      int xrayCameraMinimumTravel = cameraTrigger.getNumberOfRequiredSampleTriggers() * nominalPixelSizeInNanometers;
//      System.out.println("_minimumStageTravelInNanometers : " + _minimumStageTravelInNanometers);
      _minimumStageTravelInNanometers = Math.max(_minimumStageTravelInNanometers, xrayCameraMinimumTravel);
    }
  }

  /**
   * @author Roy Williams
   */
  private void recomputePositionalLocations() throws XrayTesterException
  {
    // Compute the min and max y's of all cameras at camera/hardware plane.
    computeMinAndMaxYBasedUponSelectedCameraSet();

    // positional delay computation for all cameras at camera/hardware plane.
    computePositionalDelayInPixelsForEachCamera();

    // Compute the additional distances before leading (or after trailing) edge
    // in order to allow for upslice and downslice on forward part on camera.
    computeAdditionalDistancesForUpsliceAndDownsliceOnForwardEdge();

    // Convert the ProcessorStrips into SystemFiducialRectangles.
    computeSystemFiducialRectangleToProcessorStrips();

    // This is the y location where leading point of board is imaged on first camera.
    computeImagePointStartAtMinCamera();

    // Compute the stage travel required.
    computeStageTravelStartAndEnd();
  }


  /**
   * @author Roy Williams
   */
  static public double zAtReferencePlane()
  {
    if(XrayTester.isLowMagnification())
    {
      int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
      double magnificationAtReferencePlane = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
      return (double)zHeightFromCameraArrayToXraySourceInNanometers /
        magnificationAtReferencePlane;      
    }
    else
    {
      int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
      double magnificationAtReferencePlane = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
      return (double)zHeightFromCameraArrayToXraySourceInNanometers /
        magnificationAtReferencePlane;
    }    
  }

  /**
   * @author Roy Williams
   */
  static public double zAtNominalPlane()
  {
    if(XrayTester.isLowMagnification())
    {
      int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
      double magnificationAtNominalPlane = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);
      return (double)zHeightFromCameraArrayToXraySourceInNanometers /
          magnificationAtNominalPlane;
    }
    else
    {
      int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
      double magnificationAtNominalPlane = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL);
      return (double)zHeightFromCameraArrayToXraySourceInNanometers /
        magnificationAtNominalPlane;
    }     
  }

  /**
   * @author Roy Williams
   */
  public double getFocalRangeUpsliceInNanometers()
  {
    return _zHeightDeltaUp;
  }

  /**
   * @author Roy Williams
   */
  public double getFocalRangeDownsliceInNanometers()
  {
    return _zHeightDeltaDown;
  }

  /**
   * Steps in the x direction are different because they are relative to the reference
   * plane.
   * @param nanometersAbove can be a negative number to denote below reference plane.
   * @author Roy Williams
   */
  public double getStepDirectionPixelSizeInNanometersAboveReferencePlane(double nanometersAbove)
  {
    int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
    
    if(XrayTester.isLowMagnification() == false)
      zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
    
    double magnificationAtOffset = zHeightFromCameraArrayToXraySourceInNanometers / (zAtReferencePlane() - nanometersAbove);

    // Just get any camera so we can get sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    return (double)xRayCamera.getSensorColumnPitchInNanometers() / magnificationAtOffset;
  }

  /**
   * Steps in the x direction are different because they are relative to the reference
   * plane.
   * @param nanometersAbove can be a negative number to denote below reference plane.
   * @author Roy Williams
   */
  static public double getStepDirectionPixelSizeInNanometersAboveNominalPlane(double nanometersAbove)
  {
    int zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
    
    if(XrayTester.isLowMagnification() == false)
      zHeightFromCameraArrayToXraySourceInNanometers = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
    
    double magnificationAtOffset = zHeightFromCameraArrayToXraySourceInNanometers / (zAtNominalPlane() - nanometersAbove);

    // Just get any camera so we can get sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    return (double)xRayCamera.getSensorColumnPitchInNanometers() / magnificationAtOffset;
  }

  /**
   * @author Roy Williams
   */
  private int maxYofCamerasInNanometers()
  {
    return _maxYofCamerasInNanometers;
  }

  /**
   * @author Roy Williams
   */
  private int minYofCamerasInNanometers()
  {
    return _minYofCamerasInNanometers;
  }

  /**
   * @author Roy Williams
   */
  private int maxPositionalDelay()
  {
    return _maxPositionalDelay;
  }

  /**
   * Compute the min and max y's for all cameras at the camera hardware plane.
   *
   * @author Roy Williams
   */
  private void computeMinAndMaxYBasedUponSelectedCameraSet()
  {
    boolean initialValueSet = false;
    for (AbstractXrayCamera xRayCamera : _listOfCamerasToUseForImaging)
    {
      MachineRectangle tdiRectangle = _xrayCameraArray.getCameraSensorRectangle(xRayCamera);
      
      if(XrayTester.isLowMagnification() == false)
        tdiRectangle = _xrayCameraArray.getCameraSensorRectangleForHighMag(xRayCamera);
      
      int centerYofCameraInNanometers = tdiRectangle.getCenterY();
      if (initialValueSet == false)
      {
        _minYofCamerasInNanometers = centerYofCameraInNanometers;
        _maxYofCamerasInNanometers = centerYofCameraInNanometers;
        initialValueSet = true;
      }
      else
      {
        _minYofCamerasInNanometers = Math.min(centerYofCameraInNanometers, _minYofCamerasInNanometers);
        _maxYofCamerasInNanometers = Math.max(centerYofCameraInNanometers, _maxYofCamerasInNanometers);
      }
    }
  }

  /**
   * Compute the forward and reverse delays requires we use the min and max
   * y values just computed.  Record the max<forwardDelay> == the max<reverseDelay>
   * to be used for stage travel by IAE.
   *
   * @author Roy Williams
   */
  private void computePositionalDelayInPixelsForEachCamera()
  {
    int maxForwardDelay = 0;
    int maxReverseDelay = 0;
    boolean initializedDelay = false;
    for (AbstractXrayCamera xRayCamera : _listOfCamerasToUseForImaging)
    {
      // Figure out the min
      if(XrayTester.isLowMagnification())
      {
        int forwardDelay = _xrayCameraArray.computeForwardDelayInPixels(xRayCamera, _minYofCamerasInNanometers);
        int reverseDelay = _xrayCameraArray.computeReverseDelayInPixels(xRayCamera, _maxYofCamerasInNanometers);
        if (initializedDelay == false)
        {
          maxForwardDelay = forwardDelay;
          maxReverseDelay = reverseDelay;
          initializedDelay = true;
        }
        else
        {
          maxForwardDelay = Math.max(maxForwardDelay, forwardDelay);
          maxReverseDelay = Math.max(maxReverseDelay, reverseDelay);
        }
      }
      else
      {
        int forwardDelay = _xrayCameraArray.computeForwardDelayInPixelsForHighMag(xRayCamera, _minYofCamerasInNanometers);
        int reverseDelay = _xrayCameraArray.computeReverseDelayInPixelsForHighMag(xRayCamera, _maxYofCamerasInNanometers);
        if (initializedDelay == false)
        {
          maxForwardDelay = forwardDelay;
          maxReverseDelay = reverseDelay;
          initializedDelay = true;
        }
        else
        {
          maxForwardDelay = Math.max(maxForwardDelay, forwardDelay);
          maxReverseDelay = Math.max(maxReverseDelay, reverseDelay);
        }
      }
    }
    Assert.expect(maxForwardDelay == maxReverseDelay, "An asymetric group of cameras has not been tested");
    _maxPositionalDelay = maxForwardDelay;
  }

  /**
   * Compute the additional distances before leading (or after trailing) edge
   * in order to allow for upslice and downslice on forward part on camera
   * @author Roy Williams
   */
  private void computeAdditionalDistancesForUpsliceAndDownsliceOnForwardEdge()
  {
    int xraySpotY = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      
    int minCenterY = 0;
    int maxCenterY = 0;
    boolean initialized = false;
    int zAtCameraArrayPlane = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
    
    if(XrayTester.isLowMagnification() == false)
    {
      xraySpotY = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      zAtCameraArrayPlane = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
    }
    
    for (AbstractXrayCamera xRayCamera : _listOfCamerasToUseForImaging)
    {
      int centerY = _xrayCameraArray.getCameraSensorRectangle(xRayCamera).getCenterY();
      
      if(XrayTester.isLowMagnification() == false)
        centerY = _xrayCameraArray.getCameraSensorRectangleForHighMag(xRayCamera).getCenterY();
      
      int camreaId = xRayCamera.getId();
      
	  //swee-yee.wong - XCR-2630 Support New X-Ray Camera
      double cameraHalfHeight = xRayCamera.getNumberOfSensorRowsUsed() / 2;
      int cameraHalfHeightInNanometers = (int)Math.floor((double)(cameraHalfHeight * xRayCamera.getSensorRowPitchInNanometers()));

      // Figure out the leading edge info.
      double zHeightAdjustForLeadingEdge = 0;
      int yna = centerY - cameraHalfHeightInNanometers - xraySpotY;
      if (yna < 0)
        zHeightAdjustForLeadingEdge = _zHeightDeltaDown;
      else
        zHeightAdjustForLeadingEdge = _zHeightDeltaUp;
      _cameraToDistanceBeforeLeadingEdgeMap.put(
          xRayCamera, Math.abs((int)Math.round(zHeightAdjustForLeadingEdge * yna /
                                               zAtCameraArrayPlane)));
      
      // Figure out the trailing edge info.
      int ync = centerY + cameraHalfHeightInNanometers - xraySpotY;
      double zHeightAdjustForTrailingEdge = 0;
      if (ync < 0)
        zHeightAdjustForTrailingEdge = _zHeightDeltaUp;
      else
        zHeightAdjustForTrailingEdge = _zHeightDeltaDown;
      _cameraToDistanceAfterTrailingEdgeMap.put(
          xRayCamera, Math.abs((int)Math.round(zHeightAdjustForTrailingEdge * ync /
                                               zAtCameraArrayPlane)));

      if (initialized == false)
      {
        minCenterY = centerY;
        maxCenterY = centerY;
        _distanceBeforeLeadingEdgeFromFirstCamera = _cameraToDistanceBeforeLeadingEdgeMap.get(xRayCamera);
        _distanceAfterTrailingEdgeFromLastCamera = _cameraToDistanceAfterTrailingEdgeMap.get(xRayCamera);
        _minYAssociatedWithLeadingEdgeDistance = yna;
        initialized = true;
      }
      else
      {
        if (centerY > maxCenterY)
        {
          maxCenterY = centerY;
          _distanceAfterTrailingEdgeFromLastCamera = _cameraToDistanceAfterTrailingEdgeMap.get(xRayCamera);
        }
        if (centerY < minCenterY)
        {
          minCenterY = centerY;
          _distanceBeforeLeadingEdgeFromFirstCamera = _cameraToDistanceBeforeLeadingEdgeMap.get(xRayCamera);
          _minYAssociatedWithLeadingEdgeDistance = yna;
        }
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private int getDistanceBeforeLeadingEdgeFromFirstCamera()
  {
    return _distanceBeforeLeadingEdgeFromFirstCamera;
  }

  /**
   * @author Roy Williams
   */
  private int getDistanceAfterTrailingEdgeFromLastCamera()
  {
    return _distanceAfterTrailingEdgeFromLastCamera;
  }

  /**
   * @author Roy Williams
   */
  private int panelRectangleWidthWithAlignmentUncertainty()
  {
    return _panelWidthWithAlignmentUncertainty;
  }

  /**
   * y1 for Tracy's equations
   *
   * @author Roy Williams
   */
  public int computeImagePointStartAtMinCamera() throws XrayTesterException
  {
    int distanceFromSystemFiducialToXraySpotWhenStageHomed =
        _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    double magNominal = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);
    
    if(XrayTester.isLowMagnification() == false)
    {
      distanceFromSystemFiducialToXraySpotWhenStageHomed =
        _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      magNominal = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION_NOMINAL);
    }
    _imagePointStartAtMinCamera =
        distanceFromSystemFiducialToXraySpotWhenStageHomed -
        _rectangleToImage.getMinY() -
        panelRectangleWidthWithAlignmentUncertainty() +
        (int)Math.round(_minYAssociatedWithLeadingEdgeDistance / magNominal);
    if (_imagePointStartAtMinCamera < 0)
      throw new MechanicalTravelLimitHardwareException();

    return _imagePointStartAtMinCamera;
  }
  
  /**
   * swee-yee.wong - XCR-2630 Support New X-Ray Camera
   * @author Roy Williams
   */
  static private int travelRequiredForExtraCameraRowsInNanometers(MagnificationEnum magnificationEnum, AbstractXrayCamera camera)
  {
    return camera.getNumberOfTriggersRequiredToCompleteAcquisition() * magnificationEnum.getNanoMetersPerPixel();
  }
  
  /**
   * @author Roy Williams
   */
  public int stageTravelStart()
  {
    Assert.expect(_stageTravelStart >= 0);
    return _stageTravelStart;
  }

  /**
   * @author Roy Williams
   */
  public int stageTravelEnd()
  {
    Assert.expect(_stageTravelEnd >= 0);
    return _stageTravelEnd;
  }

  /**
   * Page 5d of calculations.
   *
   * @author Roy Williams
   * swee-yee.wong - XCR-2630 Support New X-Ray Camera
   */
  public void computeStageTravelStartAndEnd() throws XrayTesterException
  {
    // Just get any camera so we can get the sensor properties
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // _stageTravelStart
    int hysteresis = _panelPositioner.getYaxisForwardDirectionHysteresisOffsetInNanometers();
    _stageTravelStart = _imagePointStartAtMinCamera -
                        _distanceBeforeLeadingEdgeFromFirstCamera -
                        travelRequiredForExtraCameraRowsInNanometers(_magnification, xRayCamera) -
                        hysteresis;

    // _stageTravelEnd
    int cameraRowsInNanometers = _xRayCamera.getCameraMaxPocketHeightInPixels() * _magnification.getNanoMetersPerPixel();
    _stageTravelEnd = _imagePointStartAtMinCamera +
                      _maxPositionalDelay * _magnification.getNanoMetersPerPixel() +
                      panelRectangleWidthWithAlignmentUncertainty() +
                      _distanceAfterTrailingEdgeFromLastCamera +
                      cameraRowsInNanometers +
                      travelRequiredForExtraCameraRowsInNanometers(_magnification, xRayCamera);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public int updateStageTravelStart(int yStart)
  {
    Assert.expect(_stageTravelStart >= 0);
    
    if (_stageTravelStart==yStart)
      return 0;
    
    int diff = _stageTravelStart-yStart;
    
    return diff;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public int updateStageTravelEnd(int yEnd)
  {
    Assert.expect(_stageTravelEnd >= 0);
 
    if (_stageTravelEnd==yEnd)
      return 0;
    
    int diff = yEnd-_stageTravelEnd;
    
    return diff;
  }
  
  /**
   * @author Roy Williams
   */
  public int numberOfLinesToCapture(AbstractXrayCamera camera)
  {
    Assert.expect(camera != null);

    int distanceInNanometers = Math.abs(_cameraToDistanceBeforeLeadingEdgeMap.get(camera)) +
                               _rectangleToImage.getHeight() +
                               Math.abs(_cameraToDistanceAfterTrailingEdgeMap.get(camera));
    int pixels = distanceInNanometers / _magnification.getNanoMetersPerPixel();
    return pixels;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfLinesToSkipDuringPositiveYScan(AbstractXrayCamera camera) throws XrayTesterException
  {
    Assert.expect(camera != null);

    //swee-yee.wong - XCR-2630 Support New X-Ray Camera
    int hysteresis = _panelPositioner.getYaxisForwardDirectionHysteresisOffsetInNanometers();
    int cameraRowsInNanometers = camera.getCameraReadingPositionFromMaxPocketHeightInPixels() * _magnification.getNanoMetersPerPixel();
    int distanceInNanometers = _distanceBeforeLeadingEdgeFromFirstCamera;
    distanceInNanometers -= Math.abs(_cameraToDistanceBeforeLeadingEdgeMap.get(camera));
    distanceInNanometers += cameraRowsInNanometers;
    distanceInNanometers += 2 * travelRequiredForExtraCameraRowsInNanometers(_magnification, camera);
    distanceInNanometers += hysteresis;    
    int distanceInPixels = (int)Math.round((double)(distanceInNanometers / _magnification.getNanoMetersPerPixel()));
    CameraTrigger cameraTrigger = CameraTrigger.getInstance();  
    int pixels = distanceInPixels + _xrayCameraArray.getForwardCaptureDelayInPixels(camera, _magnification) 
                  + cameraTrigger.getNumberOfRequiredSampleTriggers();
    return pixels;
  }
  
  /**
   * @author Roy Williams
   */
  public int getNumberOfLinesToSkipDuringNegativeYScan(AbstractXrayCamera camera) throws XrayTesterException
  {
    Assert.expect(camera != null);

    //swee-yee.wong - XCR-2630 Support New X-Ray Camera
    int hysteresis = _panelPositioner.getYaxisForwardDirectionHysteresisOffsetInNanometers();
    int cameraRowsInNanometers = camera.getCameraReadingPositionFromMaxPocketHeightInPixels() * _magnification.getNanoMetersPerPixel();
    int distanceInNanometers = _distanceAfterTrailingEdgeFromLastCamera;
    distanceInNanometers -= Math.abs(_cameraToDistanceAfterTrailingEdgeMap.get(camera));
    distanceInNanometers += cameraRowsInNanometers;
    distanceInNanometers += (2 * travelRequiredForExtraCameraRowsInNanometers(_magnification, camera));
    distanceInNanometers += hysteresis;
    distanceInNanometers -= camera.getCameraExtraDelayToMakeHysteresisMorePositiveInNanometers();
    int distanceInPixels = (int)Math.round((double)(distanceInNanometers / _magnification.getNanoMetersPerPixel()));
    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    int pixels = distanceInPixels + _xrayCameraArray.getReverseCaptureDelayInPixels(camera, _magnification) 
                  + cameraTrigger.getNumberOfRequiredSampleTriggers();
    return pixels;
  }  

  /**
   * @author Roy Williams
   * swee-yee.wong - XCR-2630 Support New X-Ray Camera
   */
  public int yLocationForReconstructionOnCamera(AbstractXrayCamera camera)
  {
    Assert.expect(camera != null);

    double cameraHalfHeight = camera.getNumberOfSensorRowsUsed() / 2;
    int cameraHalfHeightInNanometersAtReferencePlane = (int)Math.floor((double)(cameraHalfHeight * _magnification.getNanoMetersPerPixel()));
    int forwardCaptureDelayInNanometers = _xrayCameraArray.getForwardCaptureDelayInPixels(camera, _magnification) * _magnification.getNanoMetersPerPixel();
    return _imagePointStartAtMinCamera                       +
           cameraHalfHeightInNanometersAtReferencePlane     -
           _cameraToDistanceBeforeLeadingEdgeMap.get(camera) +
           forwardCaptureDelayInNanometers;
  }
  
  /**
   * Initializes a map of project rectangles for the process strips for each camera.
   *
   * @author Rex Shang
   * @author Roy Williams
   */
  private Map<ProcessorStrip, ImageRectangle> createProcessorStripToProjectionRectanglesMap(AbstractXrayCamera camera)
      throws XrayTesterException
  {
    Map<ProcessorStrip, ImageRectangle> projectionRectanglesMap; // = new HashMap<ProcessorStrip, ImageRectangle>(); // Accidentally found by Lim, Seng Yew on 7-Dec-2009

    int numberOfStrips = _processorStrips.size();
    //int widthOfSingleProcessorStrip = heightOfRectangleToImageWithoutAlignmentUncertainty() / numberOfStrips;
    int projectionTopLeftX = 0;
    //swee-yee.wong - XCR-2630 Support New X-Ray Camera
    int projectionWidth = camera.getSensorWidthInPixelsWithOverHeadByte();

    projectionRectanglesMap = new HashMap<ProcessorStrip, ImageRectangle>();
    int stripIndex = 0;
    //int lengthOfImage = heightOfRectangleToImageWithoutAlignmentUncertainty();
    int numberOfProcessorStrips = _processorStrips.size();
    Assert.expect(numberOfProcessorStrips > 0);
    int processorStripNumber = -1;
    for (ProcessorStrip processorStrip : _processorStrips)
    {
      processorStripNumber++;
      int startingPixel = getProcessorStripStartInPixels(processorStrip);

      int endingPixel = getProcessorStripEndInPixels(processorStrip, camera);

      ImageRectangle projectionRectangle = new ImageRectangle(projectionTopLeftX,
                                                              startingPixel,
                                                              projectionWidth,
                                                              endingPixel - startingPixel);
      projectionRectanglesMap.put(processorStrip, projectionRectangle);

      stripIndex++;
    }
    return projectionRectanglesMap;
  }

  /**
   * ProjectionRegions must begin capturing image on a given camera earlier than
   * the raw ProcessorStrip beginning due to focalRangeUpslice, alignment
   * uncertainty and max pad.
   *
   * @author Roy Williams
   * @author Dave Ferguson
   */
  int getProcessorStripStartInPixels(ProcessorStrip processorStrip)
      throws XrayTesterException
  {
    Assert.expect(processorStrip != null);

    if (processorStrip.isInitialized() == false)
      return 0;
    double start = 0;
    try
    {
      int yBeginStrip0 = _processorStripsSystemFiducicalRectangle.get(0).getMaxY();
      int location = processorStrip.getLocation();
      int yBeginStrip = _processorStripsSystemFiducicalRectangle.get(location).getMaxY();

      start = (yBeginStrip0 - yBeginStrip) / _magnification.getNanoMetersPerPixel();
    }
    catch (Exception ex)
    {
      int i = 1;
    }
    return (int)Math.round(start);
  }

  /**
   * ProjectionRegions must stop capturing image on a given camera later than
   * the raw ProcessorStrip beginning due to focalRangeDownslice, alignment
   * uncertainty and max pad.
   *
   * @author Roy Williams
   * @author Dave Ferguson
   */
  int getProcessorStripEndInPixels(ProcessorStrip processorStrip,
                                           AbstractXrayCamera camera) throws XrayTesterException
  {
    Assert.expect(processorStrip != null);
    Assert.expect(camera != null);

    if (processorStrip.isInitialized() == false)
      return 0;

    int yBeginStrip0 = _processorStripsSystemFiducicalRectangle.get(0).getMaxY();
    int location = processorStrip.getLocation();
    int yEndStrip    = _processorStripsSystemFiducicalRectangle.get(location).getMinY();
    double end = yBeginStrip0 - yEndStrip;

    end += _cameraToDistanceBeforeLeadingEdgeMap.get(camera);  // focal range up
    end += _alignmentUncertainty;                              // 1st for lead edge
    end += _alignmentUncertainty;                              // 2nd for trail edge
    end += _cameraToDistanceAfterTrailingEdgeMap.get(camera);  // focal range down

    // Convert to pixels now that we have stopInNanometers + all extensions.
    int rv = (int)Math.round(((float)end)/_magnification.getNanoMetersPerPixel());
    return rv;
  }

  /**
   * Convert the ProcessorStrips into SystemFiducialRectangles.   In doing so,
   * the value of _panelWidthWithAlignmentUncertainty will be updated to a
   * more accurate value based upon SFRs after affineTransform is applied.
   *
   * @author Roy Williams
   */
  public void computeSystemFiducialRectangleToProcessorStrips()
  {
    // _processorStripsSystemFiducicalRectangle is a sparse array.   That is, it
    // may have members at location 0, 1, 3, 4, 5.   Where 2 is intentionally missing.
    // We only want rectangles for those with real locations.
    _processorStripsSystemFiducicalRectangle = new ArrayList<SystemFiducialRectangle>();

    // Convert each ProcessorStrip panelRectangle to SystemFiducicalRectangle
    int counter = 0;
    int insertPosition = 0;
    boolean initialized = false;
    int minY = 0;
    int maxY = 0;
    for (ProcessorStrip processorStrip : _processorStrips)
    {
      // This should only be necessary for calibrations.
      if (processorStrip.isInitialized() == false)
        return;

      insertPosition = processorStrip.getLocation();
      for (; insertPosition > counter; counter++)
        _processorStripsSystemFiducicalRectangle.add(counter, null);
      SystemFiducialRectangle sfr =
          MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
              processorStrip.getRegion(),
              _affineTransform);
      _processorStripsSystemFiducicalRectangle.add(insertPosition, sfr);
      if (initialized == false)
      {
        initialized = true;
        minY = sfr.getMinY();
        maxY = sfr.getMaxY();
      }
      else
      {
        minY = Math.min(minY, sfr.getMinY());
        maxY = Math.max(maxY, sfr.getMaxY());
      }
      counter++;
//      printSystemFiducialRectangleRectangle(Integer.toString(processorStrip.getLocation()) + " SFR" , sfr);
    }
    _panelWidthWithAlignmentUncertainty = (maxY - minY) + 2 * _alignmentUncertainty;
  }

  /**
   * Create a set of maps of ProjectionRectangles for a ProcessorStrip from perspective
   * of a each camera.
   *
   * @author Roy Williams
   */
  private void initializeProjectionRectanglesMapsForCameras() throws XrayTesterException
  {
    for (AbstractXrayCamera camera : _listOfCamerasToUseForImaging)
    {
      _projectionRectanglesMapsForCameras.put(
          camera,
          createProcessorStripToProjectionRectanglesMap(camera));
    }
  }

  /**
   * Converts a PanelRectangle to a SystemFiducialRectangle.
   * PanelRectangle describes a region with respect to the lower left hand corner of the
   * board (when the board is is the system this is in the back left side of the system)
   *
   * NOTE:  This really only translates the rectangle.  It doesn't actually rotate
   * the rectangle itself, even though a true transform would do that.
   *
   * @author Dave Ferguson
   * @author Peter Esbensen
   */
  static public SystemFiducialRectangle convertPanelRectangleToSystemFiducialRectangle(
      PanelRectangle panelRectangleToConvert,
      AffineTransform alignmentMatrix)
  {
    Assert.expect(panelRectangleToConvert != null);

    int x0 = panelRectangleToConvert.getMinX();
    int y0 = panelRectangleToConvert.getMinY();
    int x1 = panelRectangleToConvert.getMaxX();
    int y1 = panelRectangleToConvert.getMaxY();

    int x, y, xmin, xmax, ymin, ymax;

    // Transform x0, y0
    IntCoordinate newRectangleCoordinate =
        transform(x0, y0, alignmentMatrix);

    xmin = xmax = newRectangleCoordinate.getX();
    ymin = ymax = newRectangleCoordinate.getY();

    // Transform x0, y1
    newRectangleCoordinate =
        transform(x0, y1, alignmentMatrix);

    x = newRectangleCoordinate.getX();
    y = newRectangleCoordinate.getY();

    xmin = Math.min(xmin, x);
    ymin = Math.min(ymin, y);
    xmax = Math.max(xmax, x);
    ymax = Math.max(ymax, y);

    // Transform x1, y1
    newRectangleCoordinate =
        transform(x1, y1, alignmentMatrix);

    x = newRectangleCoordinate.getX();
    y = newRectangleCoordinate.getY();

    xmin = Math.min(xmin, x);
    ymin = Math.min(ymin, y);
    xmax = Math.max(xmax, x);
    ymax = Math.max(ymax, y);

    // Transform x1, y0
    newRectangleCoordinate =
        transform(x1, y0, alignmentMatrix);

    x = newRectangleCoordinate.getX();
    y = newRectangleCoordinate.getY();

    xmin = Math.min(xmin, x);
    ymin = Math.min(ymin, y);
    xmax = Math.max(xmax, x);
    ymax = Math.max(ymax, y);

    SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(
        xmin,
        ymin,
        xmax - xmin + 1,
        ymax - ymin + 1);

    return systemFiducialRectangle;
  }

  /**
   * @author Peter Esbensen
   */
  static public IntCoordinate transform(int x, int y, AffineTransform affineTransform)
  {
    Assert.expect(affineTransform != null);

    Point2D fromCoord = new Point2D.Double(x, y);
    Point2D toCoord = new Point2D.Double();
    affineTransform.transform(fromCoord, toCoord);
    return new IntCoordinate((int)Math.round(toCoord.getX()), (int)Math.round(toCoord.getY()));
  }

  /**
   * Get the overall area we want to inspect relative to the system fiducial
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public SystemFiducialRectangle rectangleToImageWithAlignmentUncertaintyAddedToBorders()
  {
    Assert.expect(_rectangleToImage != null);

    return _rectangleToImage;
  }

  /**
   * @author Roy Williams
   */
  public int getAlignmentUncertainty()
  {
    return _alignmentUncertainty;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberProcessorStrips()
  {
    Assert.expect(_processorStrips != null);

    return _processorStrips.size();
  }

  /**
   * @author Roy Williams
   */
  public List<ProcessorStrip> getProcessorStrips()
  {
    Assert.expect(_processorStrips != null);

    return _processorStrips;
  }

  /**
   * There are two major contributors to acquire minimum stage travel: PanelPositioner
   * and XrayCameras.  This function was placed in XrayTester because it is the one
   * common point for determining the maximum of these minimum contributors.
   *
   * @author Roy Williams
   */
  static public int getMinimumStageTravelInNanometers()
  {
    Assert.expect(_minimumStageTravelInNanometers > 0);
    return _minimumStageTravelInNanometers;
  }

  /**
   * @author Roy Williams
   */
  static public MachineRectangle getCameraArrayRectangleAtMinSliceHeight()
  {
    return _xrayCameraArray.getBoundingRectangle(MagnificationEnum.getCurrentMinSliceHeight());
  }
  
  /**
   * @author Roy Williams
   */
  static public MachineRectangle getCameraArrayRectangleAtMinSliceHeightInHighMag()
  {
    return _xrayCameraArray.getBoundingRectangle(MagnificationEnum.H_MIN_SLICE_HEIGHT);
  }
  /**
   * @author Roy Williams
   * @author Dave Ferguson
   */
  public Map<ProcessorStrip, ImageRectangle> getProcessorStripToProjectionRectangleMap(AbstractXrayCamera camera)
  {
    return _projectionRectanglesMapsForCameras.get(camera);
  }

  /**
   * @author Roy Williams
   */
  public static void printSystemFiducialRectangleRectangle(String name, SystemFiducialRectangle sfr)
  {
    System.out.println(name + " " +
                       sfr.getMinX() + " " +
                       sfr.getMinY() + " " +
                       sfr.getWidth() + " " +
                       sfr.getHeight());
  }

  /**
   * @author Roy Williams
   */
  public SystemFiducialRectangle systemFiducicalRectangleForProcessorStrip(ProcessorStrip processorStrip)
  {
    return _processorStripsSystemFiducicalRectangle.get(processorStrip.getLocation());
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {    
    if (_projectionRectanglesMapsForCameras != null)
      _projectionRectanglesMapsForCameras.clear();
  }
  
  /**
   * @author Wei Chin
   */
  public static void reset()
  {
    try
    {
      if(_panelPositioner.isStartupRequired() == false)
      {
        Assert.expect(_panelPositioner.isStartupRequired() == false, "Panel positioner should be started already.");

          if (XrayTester.isLowMagnification())
          {
            String zAxisMotorPositionForLowMag = Config.getSystemCurrentLowMagnification();
            _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForLowMag)));
          }
          else
          {
            String zAxisMotorPositionForHighMag = Config.getSystemCurrentHighMagnification();
            _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForHighMag)));
        }
        
        _minimumStageTravelInNanometers = _panelPositioner.getMinimumScanPassLengthInNanometers();

        CameraTrigger cameraTrigger = CameraTrigger.getInstance();
        int nominalPixelSizeInNanometers = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
        int xrayCameraMinimumTravel = cameraTrigger.getNumberOfRequiredSampleTriggers() * nominalPixelSizeInNanometers;
        _minimumStageTravelInNanometers = Math.max(_minimumStageTravelInNanometers, xrayCameraMinimumTravel);
      }
      else
      {
        _minimumStageTravelInNanometers = -1;
      }
    }
    catch(XrayTesterException xe)
    {
      // do nothing
    }
  }
}
