package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 * @author Greg Esparza
 */
public class XrayCameraArray extends HardwareObject implements HardwareStartupShutdownInt
{

  private HardwareObservable _hardwareObservable;
  private HardwareTaskEngine _hardwareTaskEngine;
  private static Config _config;
  private static XrayCameraArray _instance;
  private static Map<XrayCameraIdEnum, AbstractXrayCamera> _idToCameraMap;
  private Map<AbstractXrayCamera, Map<MagnificationEnum, MachineRectangle>> _xRayCameraToMagnificationToMachineRectangleMapMap;
  private Map<AbstractXrayCamera, MachineRectangle> _xRayCameraToCameraSensorRectangleMap;
  private Map<AbstractXrayCamera, Integer> _xRayCameraToForwardDelayInPixelsMap;
  private Map<AbstractXrayCamera, Integer> _xRayCameraToReverseDelayInPixelsMap;
  private static Map<XrayCameraIdEnum, IntCoordinate> _idToSystemPositionInNanometersMap;

  private static AbstractXrayCamera _firstCameraInX;
  private static AbstractXrayCamera _lastCameraInX;

  private static int _numberOfCameras;

  // Cameras need to see stage travel far enough (1 inch) for TDI imaging.
  // This is rounded up based upon pixel size.
  private static int _oneInchInNanometersAlignedUpToPixelBoundary = 0;

  // Wei Chin
  private Map<AbstractXrayCamera, MachineRectangle> _xRayCameraToCameraSensorRectangleMapForHighMag;
  private Map<AbstractXrayCamera, Integer> _xRayCameraToForwardDelayInPixelsMapForHighMag;
  private Map<AbstractXrayCamera, Integer> _xRayCameraToReverseDelayInPixelsMapForHighMag;

  /**
   * @author George A. David
   * @author Greg Esparza
   */
  static
  {
    _config = Config.getInstance();
    _idToCameraMap = new HashMap<XrayCameraIdEnum, AbstractXrayCamera>();
    _instance = null;
    setIdToSystemPositionInNanometersMap();
    buildCamerasIfNecessary();
  }

  /**
   * @author Roy Williams
   * @author Greg Esparza
   */
  public static synchronized XrayCameraArray getInstance()
  {
    if (_instance == null)
      _instance = new XrayCameraArray();

    return _instance;
  }

  /**
   * @author Reid Hayhow
   * @author Greg Esparza
   */
  private XrayCameraArray()
  {
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _xRayCameraToMagnificationToMachineRectangleMapMap = new HashMap<AbstractXrayCamera,Map<MagnificationEnum,MachineRectangle>>();
    _xRayCameraToForwardDelayInPixelsMap = new HashMap<AbstractXrayCamera,Integer>();
    _xRayCameraToReverseDelayInPixelsMap = new HashMap<AbstractXrayCamera,Integer>();
    _xRayCameraToCameraSensorRectangleMap = new HashMap<AbstractXrayCamera,MachineRectangle>();
    _xRayCameraToCameraSensorRectangleMapForHighMag = new HashMap<AbstractXrayCamera,MachineRectangle>();
    _xRayCameraToForwardDelayInPixelsMapForHighMag = new HashMap<AbstractXrayCamera,Integer>();
    _xRayCameraToReverseDelayInPixelsMapForHighMag = new HashMap<AbstractXrayCamera,Integer>();
  }

  /**
   * @author Greg Esparza
   */
  private static void setIdToSystemPositionInNanometersMap()
  {
    _idToSystemPositionInNanometersMap = new HashMap<XrayCameraIdEnum,IntCoordinate>();

    int xPositionInNanometers = 0;
    int yPositionInNanometers = 0;
    IntCoordinate systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_0, systemPositionInNanometers);

    xPositionInNanometers = 140182600;
    yPositionInNanometers = 61188600;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_1, systemPositionInNanometers);

    xPositionInNanometers = 126898400;
    yPositionInNanometers = 132918200;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_2, systemPositionInNanometers);

    xPositionInNanometers = 24130000;
    yPositionInNanometers = 165531800;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_3, systemPositionInNanometers);

    xPositionInNanometers = -18948400;
    yPositionInNanometers = 100203000;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_4, systemPositionInNanometers);

    xPositionInNanometers = -123113800;
    yPositionInNanometers = 139242800;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_5, systemPositionInNanometers);

    xPositionInNanometers = -139395200;
    yPositionInNanometers = 76428600;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_6, systemPositionInNanometers);

    xPositionInNanometers = -83261200;
    yPositionInNanometers = 50038000;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_7, systemPositionInNanometers);

    xPositionInNanometers = -143840200;
    yPositionInNanometers = -53873400;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_8, systemPositionInNanometers);

    xPositionInNanometers = -96342200;
    yPositionInNanometers = -144602200;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_9, systemPositionInNanometers);

    xPositionInNanometers = -18516600;
    yPositionInNanometers = -103403400;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_10, systemPositionInNanometers);

    xPositionInNanometers = 113817400;
    yPositionInNanometers = -161569400;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_11, systemPositionInNanometers);

    xPositionInNanometers = 77495400;
    yPositionInNanometers = -77063600;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_12, systemPositionInNanometers);

    xPositionInNanometers = 95326200;
    yPositionInNanometers = -38354000;
    systemPositionInNanometers = new IntCoordinate(xPositionInNanometers, yPositionInNanometers);
    _idToSystemPositionInNanometersMap.put(XrayCameraIdEnum.XRAY_CAMERA_13, systemPositionInNanometers);
  }

  /**
   * @author Greg Esparza
   */
  private static IntCoordinate getSystemPositionInNanometersFromId(XrayCameraIdEnum id)
  {
    Assert.expect(id != null);

    IntCoordinate systemPositionInNanometers = _idToSystemPositionInNanometersMap.get(id);
    Assert.expect(systemPositionInNanometers != null);

    return systemPositionInNanometers;
  }

  /**
   * @author Roy Williams
   */
  private static synchronized void buildCamerasIfNecessary()
  {
    if (_idToCameraMap.isEmpty())
    {
      AbstractXrayCamera xRayCamera = null;
      List<XrayCameraIdEnum> idEnums = XrayCameraIdEnum.getAllEnums();
      _numberOfCameras = idEnums.size();

      for (XrayCameraIdEnum id : idEnums)
      {
        IntCoordinate currentCameraSystemPosition = getSystemPositionInNanometersFromId(id);
        xRayCamera = AbstractXrayCamera.getInstance(id, currentCameraSystemPosition);
        _idToCameraMap.put(id, xRayCamera);

        // Identify the leftmost camera (smallest X).
        if (_firstCameraInX == null)
          _firstCameraInX = xRayCamera;
        else
        {
          XrayCameraIdEnum firstCameraId = _firstCameraInX._cameraId;
          IntCoordinate firstCameraCoord = getSystemPositionInNanometersFromId(firstCameraId);
          if (currentCameraSystemPosition.getX() < firstCameraCoord.getX())
            _firstCameraInX = xRayCamera;
        }

        // Identify the rightmost camera (largest X).
        if (_lastCameraInX == null)
          _lastCameraInX = xRayCamera;
        else
        {
          XrayCameraIdEnum lastCameraId = _lastCameraInX._cameraId;
          IntCoordinate lastCameraCoord = getSystemPositionInNanometersFromId(lastCameraId);
          if (currentCameraSystemPosition.getX() > lastCameraCoord.getX())
            _lastCameraInX = xRayCamera;
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   * @author Greg Esparza
   */
  public static synchronized AbstractXrayCamera getCamera(XrayCameraIdEnum id)
  {
    Assert.expect(id != null);

    buildCamerasIfNecessary();
    AbstractXrayCamera xRayCamera = _idToCameraMap.get(id);
    Assert.expect(xRayCamera != null);

    return xRayCamera;
  }

  /**
   * @author Roy Williams
   */
  public static int getNumberOfCameras()
  {
    return _numberOfCameras;
  }

  /**
   * @author Bill Darbie
   * @author Greg Esparza
   */
  public static List<AbstractXrayCamera> getCameras()
  {
    buildCamerasIfNecessary();

    List<AbstractXrayCamera> xRayCameras = new ArrayList<AbstractXrayCamera>();
    for (Map.Entry<XrayCameraIdEnum, AbstractXrayCamera> entry : _idToCameraMap.entrySet())
      xRayCameras.add(entry.getValue());

    return xRayCameras;
  }

  /**
   * @author Greg Esparza
   */
  public void startup() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XrayCameraEventEnum.INITIALIZE);
    _hardwareObservable.setEnabled(false);

    refresh();

    ExecuteParallelThreadTasks<Object> xRayCameraStartupTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraStartupTasks.submitThreadTask(new XrayCameraStartupThreadTask(xRayCamera));

    try
    {
      xRayCameraStartupTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      // we need to clear the map to force a rebuild of all camera instances
      _idToCameraMap.clear();
      throw xte;
    }
    catch (Exception ex)
    {
      // we need to clear the map to force a rebuild of all camera instances
      _idToCameraMap.clear();
      Assert.logException(ex);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayCameraEventEnum.INITIALIZE);
  }

  /**
   * @author Greg Esparza
   */
  public void shutdown() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> xRayCameraShutdownTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraShutdownTasks.submitThreadTask(new XrayCameraShutdownThreadTask(xRayCamera));

    try
    {
      xRayCameraShutdownTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    return doCamerasRequireStartup();
  }

  /**
   * @author Greg Esparza
   */
  public void abort() throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> xRayCameraAbortTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraAbortTasks.submitThreadTask(new XrayCameraAbortThreadTask(xRayCamera));

    try
    {
      xRayCameraAbortTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Eric Littlefield
   */
  public int getProjectionCaptureBufferDepth() throws XrayTesterException
  {
    int projectionCaptureBufferDepth = Integer.MAX_VALUE; // how to a create a max int value???
    try
    {
      int temp;
      for (AbstractXrayCamera xRayCamera : getCameras())
      {
        temp = xRayCamera.getFreeImageBufferCount();
        projectionCaptureBufferDepth = Math.min(temp, projectionCaptureBufferDepth );
      }

    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
    return projectionCaptureBufferDepth;
  }


  /**
   * @author Greg Esparza
   */
  private boolean doCamerasRequireStartup() throws XrayTesterException
  {
    boolean camerasRequireStartup = false;

    for (AbstractXrayCamera xRayCamera : getCameras())
      camerasRequireStartup = camerasRequireStartup || xRayCamera.isStartupRequired();

    return camerasRequireStartup;
  }

  /**
   * A MachineRectangle is returned that is the union of imageable cameras.
   * 4 unused pixels on the rightmost camera must be deducted from the union to
   * be accurate.
   *
   * @author George A. David
   * @author Roy Williams
   */
  public MachineRectangle getBoundingRectangle(MagnificationEnum magnification)
  {
    Assert.expect(magnification != null);

    MachineRectangle rectangle = null;
    int unusedPixelTravelInX = 0;

    for (AbstractXrayCamera xRayCamera : getCameras())
    {
      if (rectangle == null)
        rectangle = new MachineRectangle(getCameraSensorRectangle(xRayCamera, magnification));

      rectangle.add(getCameraSensorRectangle(xRayCamera, magnification));

      // This value is the same for all cameras but we will just use the convenience of the loop and access to cameras here
      unusedPixelTravelInX = getNonImageableMarginInNanometers(xRayCamera, magnification);
    }

    return new MachineRectangle(rectangle.getMinX(),
                                rectangle.getMinY(),
                                rectangle.getWidth() - unusedPixelTravelInX,
                                rectangle.getHeight());
  }

  /**
   * @author Roy Williams
   */
  public int getImageableWidthInNanometers(MagnificationEnum magnification)
  {
    Assert.expect(magnification != null);

    AbstractXrayCamera xRayCamera = getCamera(XrayCameraIdEnum.XRAY_CAMERA_1);
    MachineRectangle machineRectangle = getCameraSensorRectangle(xRayCamera, magnification);

    int imageableWidthInNanometers = machineRectangle.getWidth() - (int)(xRayCamera.getNumberOfUnusedPixelsOnEndOfSensor() * magnification.getNanoMetersPerPixel());

    return imageableWidthInNanometers;
  }

  /**
   * @author Roy Williams
   * @author Greg Esparza
   */
  public int getNonImageableMarginInNanometers(AbstractXrayCamera xRayCamera, MagnificationEnum magnification)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(magnification != null);

    return (int)(xRayCamera.getNumberOfUnusedPixelsOnEndOfSensor() * magnification.getNanoMetersPerPixel());
  }

  /**
   * @author Roy Williams
   * @author Greg Esparza
   */
  public int getCameraMaxXWithoutNonImageableMarginInNanometers(AbstractXrayCamera xRayCamera, MagnificationEnum magnification)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(magnification != null);

    int unusedPixelsInNanometers = getNonImageableMarginInNanometers(xRayCamera, magnification);

    MachineRectangle machineRectangle = getCameraSensorRectangle(xRayCamera, magnification);
    int maxXwithoutNonImageableMarginInNanometers = machineRectangle.getMaxX() - unusedPixelsInNanometers;

    return maxXwithoutNonImageableMarginInNanometers;
  }

  /**
   * MechanicalConversions is setting up the camera array.  This camera will calculate
   * the distance from the minY (of the entire array) so we can compute the relative
   * positional delay across the array.
   *
   * @author Roy Williams
   */
  public int computeForwardDelayInPixels(AbstractXrayCamera xRayCamera, int minYofCamerasInNanometers)
  {
    Assert.expect(xRayCamera != null);

    MachineRectangle cameraSensorRectangle = getCameraSensorRectangle(xRayCamera);
    int centerYinNanometers = cameraSensorRectangle.getCenterY();
    int forwardDelayInPixels = (int)Math.round((double)(centerYinNanometers - minYofCamerasInNanometers) / xRayCamera.getSensorRowPitchInNanometers());

    _xRayCameraToForwardDelayInPixelsMap.put(xRayCamera, new Integer(forwardDelayInPixels));

    return forwardDelayInPixels;
  }

  /**
   * MechanicalConversions is setting up the camera array.  This camera will calculate
   * the distance from the minY (of the entire array) so we can compute the relative
   * positional delay across the array.
   *
   * @author Roy Williams
   */
  public int computeReverseDelayInPixels(AbstractXrayCamera xRayCamera, int maxYofCamerasInNanometers)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(maxYofCamerasInNanometers >= 0);

    MachineRectangle cameraSensorRectangle = getCameraSensorRectangle(xRayCamera);
    int centerYinNanometers = cameraSensorRectangle.getCenterY();
    int reverseDelayInPixels = (int)Math.round((double)(maxYofCamerasInNanometers - centerYinNanometers) / xRayCamera.getSensorRowPitchInNanometers());

    _xRayCameraToReverseDelayInPixelsMap.put(xRayCamera, new Integer(reverseDelayInPixels));

    return reverseDelayInPixels;
  }
  
  /**
   * MechanicalConversions is setting up the camera array.  This camera will calculate
   * the distance from the minY (of the entire array) so we can compute the relative
   * positional delay across the array.
   *
   * @author Wei Chin
   */
  public int computeForwardDelayInPixelsForHighMag(AbstractXrayCamera xRayCamera, int minYofCamerasInNanometers)
  {
    Assert.expect(xRayCamera != null);

    MachineRectangle cameraSensorRectangle = getCameraSensorRectangleForHighMag(xRayCamera);
    int centerYinNanometers = cameraSensorRectangle.getCenterY();
    int forwardDelayInPixels = (int)Math.round((double)(centerYinNanometers - minYofCamerasInNanometers) / xRayCamera.getSensorRowPitchInNanometers());

    _xRayCameraToForwardDelayInPixelsMapForHighMag.put(xRayCamera, new Integer(forwardDelayInPixels));

    return forwardDelayInPixels;
  }

  /**
   * MechanicalConversions is setting up the camera array.  This camera will calculate
   * the distance from the minY (of the entire array) so we can compute the relative
   * positional delay across the array.
   *
   * @author Wei Chin
   */
  public int computeReverseDelayInPixelsForHighMag(AbstractXrayCamera xRayCamera, int maxYofCamerasInNanometers)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(maxYofCamerasInNanometers >= 0);

    MachineRectangle cameraSensorRectangle = getCameraSensorRectangleForHighMag(xRayCamera);
    int centerYinNanometers = cameraSensorRectangle.getCenterY();
    int reverseDelayInPixels = (int)Math.round((double)(maxYofCamerasInNanometers - centerYinNanometers) / xRayCamera.getSensorRowPitchInNanometers());

    _xRayCameraToReverseDelayInPixelsMapForHighMag.put(xRayCamera, new Integer(reverseDelayInPixels));

    return reverseDelayInPixels;
  }

  /**
   * @author Roy Williams
   */
  public int getForwardCaptureDelayInPixels(AbstractXrayCamera xRayCamera, MagnificationEnum magnification)
  {
    Assert.expect(xRayCamera != null);

    if(magnification.equals(MagnificationEnum.NOMINAL))
    {
      Integer forwardDelayInPixels = _xRayCameraToForwardDelayInPixelsMap.get(xRayCamera);
      Assert.expect(forwardDelayInPixels != null);

      return forwardDelayInPixels.intValue();
    }
    else
    {
      Integer forwardDelayInPixels = _xRayCameraToForwardDelayInPixelsMapForHighMag.get(xRayCamera);
      Assert.expect(forwardDelayInPixels != null);
      
      return forwardDelayInPixels.intValue();
    }     
  }

  /**
   * @author Roy Williams
   */
  public int getReverseCaptureDelayInPixels(AbstractXrayCamera xRayCamera, MagnificationEnum magnification)
  {
    Assert.expect(xRayCamera != null);

    if(magnification.equals(MagnificationEnum.NOMINAL))
    {
      Integer reverseDelayInPixels = _xRayCameraToReverseDelayInPixelsMap.get(xRayCamera);
      Assert.expect(reverseDelayInPixels != null);

      return reverseDelayInPixels;
    }
    else
    {
      Integer reverseDelayInPixels = _xRayCameraToReverseDelayInPixelsMapForHighMag.get(xRayCamera);
      Assert.expect(reverseDelayInPixels != null);

      return reverseDelayInPixels;
    }
  }

  /**
   * @author George A. David
   * @author Greg Esparza
   */
  public synchronized MachineRectangle getCameraSensorRectangle(AbstractXrayCamera xRayCamera, MagnificationEnum magnification)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(magnification != null);

    if (_xRayCameraToMagnificationToMachineRectangleMapMap.size() == 0)
      refresh();

    Map<MagnificationEnum, MachineRectangle> magnificationToMachineRectangleMap = _xRayCameraToMagnificationToMachineRectangleMapMap.get(xRayCamera);
    Assert.expect(magnificationToMachineRectangleMap != null);

    MachineRectangle machineRectangle = magnificationToMachineRectangleMap.get(magnification);
    Assert.expect(machineRectangle != null);

    return machineRectangle;
  }

  /**
   * @return the camera tdi sensor rectangle with respect to the xray spot
   * @author Horst Mueller
   */
  public MachineRectangle getCameraSensorRectangle(AbstractXrayCamera xRayCamera)
  {
    Assert.expect(xRayCamera != null);

    refresh();
    
    MachineRectangle cameraSensorRectangle = _xRayCameraToCameraSensorRectangleMap.get(xRayCamera);
    Assert.expect(cameraSensorRectangle != null);

    return cameraSensorRectangle;      
  }
  
  /**
   * @return the camera tdi sensor rectangle with respect to the xray spot
   * @author Wei Chin
   */
  public MachineRectangle getCameraSensorRectangleForHighMag(AbstractXrayCamera xRayCamera)
  {
    Assert.expect(xRayCamera != null);

    refresh();
    
    MachineRectangle cameraSensorRectangle = _xRayCameraToCameraSensorRectangleMapForHighMag.get(xRayCamera);
    Assert.expect(cameraSensorRectangle != null);

    return cameraSensorRectangle;
  }

  /**
   * Enable unit test mode for a selective set of cameras
   *
   * @author Greg Esparza
   */
  public void enableUnitTestMode(List<AbstractXrayCamera> xRayCameras, String imageDataTestDirectory) throws XrayTesterException
  {
    Assert.expect(xRayCameras != null);
    Assert.expect(xRayCameras.isEmpty() == false);

    ExecuteParallelThreadTasks<Object> xRayCameraEnableUnitTestModeTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : xRayCameras)
      xRayCameraEnableUnitTestModeTasks.submitThreadTask(new XrayCameraEnableUnitTestModeThreadTask(xRayCamera, imageDataTestDirectory));

    try
    {
      xRayCameraEnableUnitTestModeTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Enable unit test mode for all cameras
   *
   * @author Greg Esparza
   */
  public void enableUnitTestMode(String imageDataTestDirectory) throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> xRayCameraEnableUnitTestModeTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraEnableUnitTestModeTasks.submitThreadTask(new XrayCameraEnableUnitTestModeThreadTask(xRayCamera, imageDataTestDirectory));

    try
    {
      xRayCameraEnableUnitTestModeTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Disable unit test mode for all cameras.  This will even work if the client to this class
   * only enabled a selective set of cameras.
   *
   * @author Greg Esparza
   */
  public void disableUnitTestMode()
  {
    ExecuteParallelThreadTasks<Object> xRayCameraDisableUnitTestModeTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraDisableUnitTestModeTasks.submitThreadTask(new XrayCameraDisableUnitTestModeThreadTask(xRayCamera));

    try
    {
      xRayCameraDisableUnitTestModeTasks.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Tells each XrayCamera to refresh the cached MachineRectangles stored within
   * the camera to represent the area of this camera at specialized magnifications.
   *
   * This function should be called by the calibration subsystem when a new X-ray spot is calculated.
   *
   * @author Roy Williams
   */
  public void refresh()
  {
    _oneInchInNanometersAlignedUpToPixelBoundary = 0;
    for (AbstractXrayCamera xRayCamera : getCameras())
      refreshCameraRectanglesAtAllMagnificationLevels(xRayCamera);
  }

  /**
   * Refresh the cached MachineRectangles stored within the camera to represent
   * the area of this camera at specialized magnification levels.   These cached
   * magnifications represent the area of the camera at the imaging plane,
   * reference plane, with MAX_SLICE_HEIGHT and MIN_SLICE_HEIGHT.
   *
   * This function will be called by the calibration subsystem when a new xraySpot
   * location has been calculated and stored into the hardware.calib.  Thus, the
   * resulting changes to these rectangles will be used in subsequent calibration
   * utilities for system fiducials, hysteresis, etc.
   *
   * @author Roy Williams
   */
  private void refreshCameraRectanglesAtAllMagnificationLevels(AbstractXrayCamera xRayCamera)
  {
    Assert.expect(xRayCamera != null);

    // The points define the center of the camera so subtract off the half-width and half-height to
    // define the rectangle with respect to the lower left corner.

    Map<MagnificationEnum, MachineRectangle> magnificationToMachineRectangle = new HashMap<MagnificationEnum, MachineRectangle>();
    int halfCameraTDIRectangleWidthInNanometers = Math.round(xRayCamera.getSensorWidthInNanometers() * 0.5f);
    int halfCameraTDIRectangleHeightInNanometers = Math.round(xRayCamera.getSensorHeightInNanometers() * 0.5f);

    int xRaySpotXLocation = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int xRaySpotYLocation = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    
    int highMagXRaySpotXLocation = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int highMagXRaySpotYLocation = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    
    XrayCameraIdEnum id = XrayCameraIdEnum.getEnum(xRayCamera.getId());
    IntCoordinate systemPositionInNanometers = getSystemPositionInNanometersFromId(id);
    MachineRectangle cameraTDISensorRectangle = new MachineRectangle(systemPositionInNanometers.getX() + xRaySpotXLocation - halfCameraTDIRectangleWidthInNanometers,
                                                                     systemPositionInNanometers.getY() + xRaySpotYLocation - halfCameraTDIRectangleHeightInNanometers,
                                                                     xRayCamera.getSensorWidthInNanometers(),
                                                                     xRayCamera.getSensorHeightInNanometers());
    
    MachineRectangle cameraTDISensorRectangleForHighMag = new MachineRectangle(systemPositionInNanometers.getX() + highMagXRaySpotXLocation - halfCameraTDIRectangleWidthInNanometers,
                                                                     systemPositionInNanometers.getY() + highMagXRaySpotYLocation - halfCameraTDIRectangleHeightInNanometers,
                                                                     xRayCamera.getSensorWidthInNanometers(),
                                                                     xRayCamera.getSensorHeightInNanometers());

    // In order for the math to work the camera location need to be in x-ray spot coordinates.
    // Subtract off the x-ray spot location
    // Nominal magnification
    double nominalMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers() / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    double x = (double)(cameraTDISensorRectangle.getMinX() - xRaySpotXLocation) / nominalMagnification;
    double y = (double)(cameraTDISensorRectangle.getMinY() - xRaySpotYLocation) / nominalMagnification;
    double dx = (double)cameraTDISensorRectangle.getWidth() / nominalMagnification;
    double dy = (double)cameraTDISensorRectangle.getHeight() / nominalMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    MachineRectangle nominalRectangle = new MachineRectangle((int)(x + xRaySpotXLocation),
                                                             (int)(y + xRaySpotYLocation),
                                                             (int)dx,
                                                             (int)dy);
//    // Nominal magnification
    double nominalHighMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers() / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    x = (double)(cameraTDISensorRectangleForHighMag.getMinX() - highMagXRaySpotXLocation) / nominalHighMagnification;
    y = (double)(cameraTDISensorRectangleForHighMag.getMinY() - highMagXRaySpotYLocation) / nominalHighMagnification;
    dx = (double)cameraTDISensorRectangleForHighMag.getWidth() / nominalHighMagnification;
    dy = (double)cameraTDISensorRectangleForHighMag.getHeight() / nominalHighMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    MachineRectangle hNominalRectangle = new MachineRectangle((int)(x + highMagXRaySpotXLocation),
                                                             (int)(y + highMagXRaySpotYLocation),
                                                             (int)dx,
                                                             (int)dy);

    magnificationToMachineRectangle.put(MagnificationEnum.NOMINAL, nominalRectangle);
    magnificationToMachineRectangle.put(MagnificationEnum.H_NOMINAL, hNominalRectangle);

    // Max slice height magnification
    double maximumMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers() / MagnificationEnum.MAX_SLICE_HEIGHT.getNanoMetersPerPixel();
    x = (double)(cameraTDISensorRectangle.getMinX() - xRaySpotXLocation) / maximumMagnification;
    y = (double)(cameraTDISensorRectangle.getMinY() - xRaySpotYLocation) / maximumMagnification;
    dx = (double)cameraTDISensorRectangle.getWidth() / maximumMagnification;
    dy = (double)cameraTDISensorRectangle.getHeight() / maximumMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    MachineRectangle machineRectangle = new MachineRectangle((int)(x + xRaySpotXLocation),
                                                             (int)(y + xRaySpotYLocation),
                                                             (int)dx,
                                                             (int)dy);
    
    double maximumHighMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers() / MagnificationEnum.H_MAX_SLICE_HEIGHT.getNanoMetersPerPixel();
    x = (double)(cameraTDISensorRectangleForHighMag.getMinX() - highMagXRaySpotXLocation) / maximumHighMagnification;
    y = (double)(cameraTDISensorRectangleForHighMag.getMinY() - highMagXRaySpotYLocation) / maximumHighMagnification;
    dx = (double)cameraTDISensorRectangleForHighMag.getWidth() / maximumHighMagnification;
    dy = (double)cameraTDISensorRectangleForHighMag.getHeight() / maximumHighMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    MachineRectangle hMachineRectangle = new MachineRectangle((int)(x + highMagXRaySpotXLocation),
                                                             (int)(y + highMagXRaySpotYLocation),
                                                             (int)dx,
                                                             (int)dy);

    magnificationToMachineRectangle.put(MagnificationEnum.MAX_SLICE_HEIGHT, machineRectangle);
    magnificationToMachineRectangle.put(MagnificationEnum.H_MAX_SLICE_HEIGHT, hMachineRectangle);

    // Min slice height magnification
    double minimumMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers()/ MagnificationEnum.MIN_SLICE_HEIGHT.getNanoMetersPerPixel();
    x = (double)(cameraTDISensorRectangle.getMinX() - xRaySpotXLocation) / minimumMagnification;
    y = (double)(cameraTDISensorRectangle.getMinY() - xRaySpotYLocation) / minimumMagnification;
    dx = (double)cameraTDISensorRectangle.getWidth() / minimumMagnification;
    dy = (double)cameraTDISensorRectangle.getHeight() / minimumMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    machineRectangle = new MachineRectangle((int)(x + xRaySpotXLocation),
                                            (int)(y + xRaySpotYLocation),
                                            (int)dx,
                                            (int)dy);
    
    // Min slice height magnification
    double minimumHighMagnification = (double)xRayCamera.getSensorColumnPitchInNanometers()/ MagnificationEnum.H_MIN_SLICE_HEIGHT.getNanoMetersPerPixel();
    x = (double)(cameraTDISensorRectangleForHighMag.getMinX() - highMagXRaySpotXLocation) / minimumHighMagnification;
    y = (double)(cameraTDISensorRectangleForHighMag.getMinY() - highMagXRaySpotYLocation) / minimumHighMagnification;
    dx = (double)cameraTDISensorRectangleForHighMag.getWidth() / minimumHighMagnification;
    dy = (double)cameraTDISensorRectangleForHighMag.getHeight() / minimumHighMagnification;

    // Add back the x-ray spot location to convert to machine coordinates
    hMachineRectangle = new MachineRectangle((int)(x + highMagXRaySpotXLocation),
                                            (int)(y + highMagXRaySpotYLocation),
                                            (int)dx,
                                            (int)dy);

    magnificationToMachineRectangle.put(MagnificationEnum.MIN_SLICE_HEIGHT, machineRectangle);
    magnificationToMachineRectangle.put(MagnificationEnum.H_MIN_SLICE_HEIGHT, hMachineRectangle);
    
    _xRayCameraToMagnificationToMachineRectangleMapMap.put(xRayCamera, magnificationToMachineRectangle);
    _xRayCameraToCameraSensorRectangleMap.put(xRayCamera, cameraTDISensorRectangle);
    _xRayCameraToCameraSensorRectangleMapForHighMag.put(xRayCamera, cameraTDISensorRectangleForHighMag);
  }

  /**
   * @author Reid Hayhow
   */
  public static int getCameraWidthInNanometersAtNominalMagnification()
  {
    // Get any camera so we can get sensor properties
    AbstractXrayCamera xRayCamera = getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    return xRayCamera.getNumberOfSensorPixelsUsed() * MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
  }
  
  /**
   * @author Reid Hayhow
   */
  public static int getCameraLengthInNanometersAtNominalMagnification()
  {
    // Get any camera so we can get sensor properties
    AbstractXrayCamera xRayCamera = getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

	//swee-yee.wong - XCR-2630 Support New X-Ray Camera
    return xRayCamera.getNumberOfSensorRowsUsed() * MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
  }

  /**
   * @author Greg Esparza
   */
  private boolean areCamerasReadyForAcquisition() throws XrayTesterException
  {
    boolean readyForAcquisition = true;

    for (AbstractXrayCamera xRayCamera : getCameras())
      readyForAcquisition = readyForAcquisition && xRayCamera.isReadyForAcquisition();

    return readyForAcquisition;
  }

  /**
   * @author Wei Chin
   */
  public void setDefaultCamerasLight() throws XrayTesterException
  {
    for (AbstractXrayCamera xRayCamera : getCameras())
    {
      xRayCamera.setDefaultLightSensitivity();
    }
  }

  /**
   * @author Wei Chin
   */
  public void setUserDefinedCamerasLight() throws XrayTesterException
  {
    for (AbstractXrayCamera xRayCamera : getCameras())
    {
      xRayCamera.setUserDefinedLightSensitivity();
    }
  }

  /**
   * @author Greg Esparza
   */
  public void waitUntilCamerasAreReadyForAcquisition() throws XrayTesterException
  {
    boolean readyForAcquisition = false;
    int retryCount = 1;
    int maximumNumberOfRetries = 50;

    clearAbort();

    do
    {
      if (isAborting())
        return;

      readyForAcquisition = areCamerasReadyForAcquisition();
      ++retryCount;
      try
      {
        if (readyForAcquisition == false)
          Thread.sleep(100);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
    while ((readyForAcquisition == false) && (retryCount <= maximumNumberOfRetries));

    if (retryCount > maximumNumberOfRetries)
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getCameraArrayCamerasNotReadyForAcquisitionTimeoutException());
  }

  /**
   * @author Ronald Lim
   * Kee Chin Seong - Forcing ugrade firmware.
   */
  public void updateCameraFirmware(boolean isForceUpgrade) throws XrayTesterException
  {
    ExecuteParallelThreadTasks<Object> xRayCameraUpdateFirmwareTasks = new ExecuteParallelThreadTasks<Object>();

    for (AbstractXrayCamera xRayCamera : getCameras())
      xRayCameraUpdateFirmwareTasks.submitThreadTask(new XrayCameraUpdateFirmwareThreadTask(xRayCamera, isForceUpgrade));

    try
    {
      xRayCameraUpdateFirmwareTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }

  }

  /**
   * @author Ronald Lim
   */
  public boolean updateCameraFirmwareRequired()throws XrayTesterException
  {
    boolean updateRequired = false;

    if (isStartupRequired() == false)
    {
      for (AbstractXrayCamera camera : getCameras())
      {
        updateRequired = camera.updateCameraFirmwareRequired();
        if (updateRequired)
        {
          break;
        }
      }
    }

    return updateRequired;
  }
  /**
   * @author Roy Williams
   * @author Reid Hayhow
   */
  public static int getZHeightFromCameraArrayToXraySourceInNanometers()
  {
    return XrayTester.getDistanceFromCameraArrayToXraySourceInNanometers();
  }
  
  /**
   * @author Wei Chin
   */
  public static int getZHeightFromCameraArrayTo2ndXraySourceInNanometers()
  {
    return XrayTester.getDistanceFromCameraArrayTo2ndXraySourceInNanometers();
  }
  
  /**
  /**
   * @author George A. David
   */
  public int getCameraArrayWidthProjectedOnMinimumSlicePlane()
  {
    return getBoundingRectangle(MagnificationEnum.getCurrentMinSliceHeight()).getWidth();
  }

  /**
   * @author Roy Williams
   */
  public static int getOneInchInNanometersAlignedUpToPixelBoundary()
  {
//    if (_oneInchInNanometersAlignedUpToPixelBoundary == 0)
//    {
      int nominalPixelSizeInNanometers = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      _oneInchInNanometersAlignedUpToPixelBoundary = (int)Math.ceil(25400000.0 / nominalPixelSizeInNanometers) *
                                                     nominalPixelSizeInNanometers;
//    }
    return _oneInchInNanometersAlignedUpToPixelBoundary;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getInchesInNanometersAlignedUpToPixelBoundary(double inches)
  {
    int nominalPixelSizeInNanometers = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    _oneInchInNanometersAlignedUpToPixelBoundary = (int)Math.ceil(inches / nominalPixelSizeInNanometers) *
                                                     nominalPixelSizeInNanometers;

    return _oneInchInNanometersAlignedUpToPixelBoundary;
  }

  /**
   * @author Roy Williams
   */
  public static AbstractXrayCamera getFirstCameraInX()
  {
    return _firstCameraInX;
  }

  /**
   * @author Roy Williams
   */
  public static AbstractXrayCamera getLastCameraInX()
  {
    return _lastCameraInX;
  }

  /**
   * @author sham
   */
  public void setUserDefinedCamerasLight(double userGain) throws XrayTesterException
  {
    Assert.expect(userGain > 0);
    for (AbstractXrayCamera xRayCamera : getCameras())
    {
      xRayCamera.setUserDefinedLightSensitivity(userGain);
    }
  }
  
  /**
   * @throws XrayTesterException 
   * 
   * @author Wei Chin
   */
  public void loadHighMagGrayScale() throws XrayTesterException
  {
      for (AbstractXrayCamera xRayCamera : getCameras())
      {
        xRayCamera.runLoadVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraHighMagnificationFileConfigFileName());
      }
  }

  
  /**
   * @throws XrayTesterException 
   * 
   * @author Wei Chin
   */
  public void loadLowMagGrayScale() throws XrayTesterException
  {
      for (AbstractXrayCamera xRayCamera : getCameras())
      {
        xRayCamera.runLoadVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraLowMagnificationFileConfigFileName());
      }
  }
  
  /**
   * @throws XrayTesterException 
   * @param magnificationTypeEnum HIGH/LOW
   * set camera calPoint values for different mag
   * @author Swee Yee
   */
  public void loadCalPointForVariableMag(boolean isLowMag) throws XrayTesterException
  {
    for (AbstractXrayCamera xRayCamera : getCameras())
    {
      xRayCamera.setCalPointValuesForVariableMag(isLowMag);
    }
  }
}

