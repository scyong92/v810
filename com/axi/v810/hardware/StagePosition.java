package com.axi.v810.hardware;

/**
 * @author Bill Darbie
 */
public interface StagePosition
{
  /**
   * @author Greg Esparza
   */
  public int getXInNanometers();

  /**
   * @author Roy Williams
   */
  public void setXInNanometers(int yPositionInNanometers);

  /**
   * @author Greg Esparza
   */
  public int getYInNanometers();

  /**
   * @author Roy Williams
   */
  public void setYInNanometers(int yPositionInNanometers);
}
