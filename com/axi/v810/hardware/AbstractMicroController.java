package com.axi.v810.hardware;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;

import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public abstract class AbstractMicroController extends HardwareObject
    implements Simulatable, HardwareStartupShutdownInt
{
    private static AbstractMicroController _instance;

    protected static Config _config;
    protected static HardwareObservable _hardwareObservable;
    protected static List<HardwareConfigurationDescriptionInt> _configDescription;

    static
    {
        _config = Config.getInstance();
        _hardwareObservable = HardwareObservable.getInstance();
        _configDescription = new ArrayList<>();
    }

    protected boolean _startupRequired = true;

    /**
     * @author Cheah Lee Herng
     */
    public static synchronized AbstractMicroController getInstance()
    {
        if (_instance == null)
        {
            _instance = new PspMicroController();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        return _startupRequired;
    }

    /**
     * @author Cheah Lee Herng
     */
    public void startup() throws XrayTesterException
    {
        _hardwareObservable.stateChangedBegin(this, MicroControllerEventEnum.STARTUP);
        _hardwareObservable.setEnabled(false);
        try
        {          
          _startupRequired = false;
          resetHardwareReportedErrors();

          on();
          off();
        }
        catch (XrayTesterException xtx)
        {
          _startupRequired = true; // reset when call is not successfull
          throw xtx;
        }
        catch (RuntimeException rx)
        {
          _startupRequired = true; // reset when call is not successfull
          throw rx;
        }
        finally
        {
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, MicroControllerEventEnum.STARTUP);
    }

    /**
     * @author Cheah Lee Herng
     */
    public void shutdown() throws XrayTesterException
    {
        _hardwareObservable.stateChangedBegin(this, MicroControllerEventEnum.SHUTDOWN);
        _hardwareObservable.setEnabled(false);
        try
        {
          _startupRequired = true; // reset flag no matter how much of powerOff() succeeds
          resetHardwareReportedErrors();
        }
        finally
        {
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, MicroControllerEventEnum.SHUTDOWN);
    }

    /**
     * @author Cheah Lee Herng
     */
    protected void checkForHardwareReportedErrors() throws XrayTesterException
    {
        MicroControllerHardwareReportedErrorsException mchrex = getHardwareReportedErrors();
        if (mchrex != null)
        {
          // Set start up required so user can reset the latched error code through
          // new start up.
          _startupRequired = true;
          throw mchrex;
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public List<HardwareConfigurationDescriptionInt> getConfigurationDescription()
    {
      Assert.expect(_configDescription != null);
      Assert.expect(_configDescription.isEmpty() == false);

      return _configDescription;
    }

    /**
     * @author Cheah Lee Herng
     */
    public abstract void on() throws XrayTesterException;

    /**
     * @author Cheah Lee Herng
     */
    public abstract void off() throws XrayTesterException;

    /**
     * @author Cheah Lee Herng
     */
    public abstract MicroControllerHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException;

    /**
     * @author Cheah Lee Herng
     */
    public abstract void resetHardwareReportedErrors() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void startProjectorOneSequence() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void enableProjectorOneLED() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void enableProjectorTwoLED() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void disableProjectorOneLED() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void disableProjectorTwoLED() throws XrayTesterException;
    
    /**
     * @author Cheah Lee Herng
     */
    public abstract void startProjectorTwoSequence() throws XrayTesterException;
}
