package com.axi.v810.hardware;

/**
 * Used by ICMPPacketHardwareException class.
 * @author Rex Shang
 */
public class ICMPPacketHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static final ICMPPacketHardwareExceptionEnum MTU_SIZE_TOO_SMALL_EXCEPTION = new ICMPPacketHardwareExceptionEnum(++_index);

  private ICMPPacketHardwareExceptionEnum(int id)
  {
    super(id);
  }
}
