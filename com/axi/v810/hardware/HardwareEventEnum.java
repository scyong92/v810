package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class HardwareEventEnum extends com.axi.util.Enum
{
  /**
   * @author Bill Darbie
   */
  protected HardwareEventEnum(int id)
  {
    super(id);
  }
}
