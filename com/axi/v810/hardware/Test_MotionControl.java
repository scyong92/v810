package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;


/**
 * @author Greg Esparza
 */
class Test_MotionControl extends UnitTest
{
  private static final int _X_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS = 10000;
  private static final int _Y_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS = 10000;
  private static final int _RAIL_WIDTH_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS = 102000000;
  private static final String _EMPTY_STRING = "";
  private MotionControl _motionControl;
  private HardwareWorkerThread _hardwareWorkerThread;
  private Config _config;
  private boolean _debugMode;
  private String[] _args;
  private ExecuteParallelThreadTasks<Object> _serviceModeConfigParallelTasks;
  private boolean _configDone;

  /**
    * @author Greg Esparza
    */
   private void getKeyboardCommand(String commandMessage) throws IOException
   {
     BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
     String keyboardString = "";

     do
     {
       System.out.println();
       System.out.println(commandMessage);

       keyboardString = br.readLine();
       if (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false)
       {
         System.out.println("Invalid key!");
       }
     }
     while (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false);
  }

  /**
   * @author Greg Esparza
   */
  public Test_MotionControl(String[] args)
  {
    try
    {
      _config = Config.getInstance();
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }

    _args = args;
    _motionControl = MotionControl.getInstance(MotionControllerIdEnum.MOTION_CONTROLLER_0);
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _debugMode = false;
    _serviceModeConfigParallelTasks  = new ExecuteParallelThreadTasks<Object>();
    _configDone = false;
  }


  /**
   * @author Greg Esparza
   */
  private void setDebugMode() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-debugMode"))
      {
        argFound = true;
        _debugMode = true;
        break;
      }
    }

    if (argFound == false)
      _debugMode = false;
  }

  /**
   * @author Greg Esparza
   */
  private void checkArgs()
  {
    if (_args != null)
    {
      try
      {
         setDebugMode();
      }
      catch (HardwareException he)
      {
        System.out.println(he);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  private void testGetYaxisScanStartPositionLimitInNanometers() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
    int positionLimitInNanometers = _motionControl.getYaxisScanStartPositionLimitInNanometers(ScanPassDirectionEnum.FORWARD);
    Expect.expect(positionLimitInNanometers > _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS));
    positionLimitInNanometers = _motionControl.getYaxisScanStartPositionLimitInNanometers(ScanPassDirectionEnum.REVERSE);
    Expect.expect(positionLimitInNanometers < _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS));
  }

  /**
   * @author Greg Esparza
   */
  private void testGetConfigurationDescription() throws XrayTesterException
  {
    List<HardwareConfigurationDescriptionInt> configDescription = _motionControl.getConfigurationDescription();
    Expect.expect(configDescription != null);

    for (HardwareConfigurationDescriptionInt config : configDescription)
    {
      LocalizedString localizedString = new LocalizedString(config.getKey(), config.getParameters().toArray());
      String message = StringLocalizer.keyToString(localizedString);
      //System.out.println(message);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMinimumPositionLimitInNanometers() throws XrayTesterException, BadFormatException
  {
    int limit = _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    int configEnumValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.X_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configEnumValue);

    limit = _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    configEnumValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.Y_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configEnumValue);

    limit = _motionControl.getMinimumPositionLimitInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    configEnumValue = _config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_MINIMUM_POSITION_LIMIT_IN_NANOMETERS);
    Expect.expect(limit == configEnumValue);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMaximumPositionLimitInNanometers() throws XrayTesterException, BadFormatException
  {
    int limit = _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    int configEnumValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configEnumValue);

    limit = _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    configEnumValue = StringUtil.convertStringToInt(_config.getStringValue(HardwareConfigEnum.Y_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    Expect.expect(limit == configEnumValue);

    limit = _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    configEnumValue = _config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS);
    Expect.expect(limit == configEnumValue);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetValidPositionPulseRateInNanometersPerPulse() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
    int validPositionPulseRate = _motionControl.getValidPositionPulseRateInNanometersPerPulse(16000);
    validPositionPulseRate = _motionControl.getValidPositionPulseRateInNanometersPerPulse(16250);
  }

  /**
   * @author Greg Esparza
   */
  private void testIsHomeSensorClear() throws XrayTesterException
  {
    boolean homeSensorIsClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    homeSensorIsClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    homeSensorIsClear = _motionControl.isHomeSensorClear(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMinimumScanPassLengthInNanometers() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));

    int limit = _motionControl.getMinimumScanPassLengthInNanometers();
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMaximumScanPassLengthInNanometers() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));

    int limit = _motionControl.getMaximumScanPassLengthInNanometers(25400000, ScanPassDirectionEnum.FORWARD);
    limit = _motionControl.getMaximumScanPassLengthInNanometers(508000000, ScanPassDirectionEnum.FORWARD);
    limit = _motionControl.getMaximumScanPassLengthInNanometers(584200000, ScanPassDirectionEnum.FORWARD);
    limit = _motionControl.getMaximumScanPassLengthInNanometers(508000000, ScanPassDirectionEnum.REVERSE);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMinimumScanStepWidthInNanometers() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));

    int limit = _motionControl.getMinimumScanStepWidthInNanometers();
  }

  /**
   * @author Greg Esparza
   */
  private void testGetMaximumScanStepWidthInNanometers() throws XrayTesterException
  {
    _motionControl.setScanMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));

    int limit = _motionControl.getMaximumScanStepWidthInNanometers(0, ScanStepDirectionEnum.FORWARD);
    Expect.expect(limit == _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS));
    limit = _motionControl.getMaximumScanStepWidthInNanometers(_motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS), ScanStepDirectionEnum.FORWARD);
    Expect.expect(limit == 0);
    limit = _motionControl.getMaximumScanStepWidthInNanometers(0, ScanStepDirectionEnum.REVERSE);
    Expect.expect(limit == 0);
    limit = _motionControl.getMaximumScanStepWidthInNanometers(_motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS), ScanStepDirectionEnum.REVERSE);
    Expect.expect(limit == _motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS));
    limit = _motionControl.getMaximumScanStepWidthInNanometers(482600000, ScanStepDirectionEnum.FORWARD);
    Expect.expect(limit == (_motionControl.getMaximumPositionLimitInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS) - 482600000));
    limit = _motionControl.getMaximumScanStepWidthInNanometers(482600000, ScanStepDirectionEnum.REVERSE);
    Expect.expect(limit == 482600000);
  }

  /**
   * @author Greg Esparza
   */
  private void testSetHysteresisOffsetInNanometers() throws XrayTesterException
  {
    int xAxisHysteresisOffsetInNanometers = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, xAxisHysteresisOffsetInNanometers);

    int yAxisHysteresisOffsetInNanometers = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, yAxisHysteresisOffsetInNanometers);

    int railWidthAxisHysteresisOffsetInNanometers = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    _motionControl.setHysteresisOffsetInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, railWidthAxisHysteresisOffsetInNanometers);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetHysteresisOffsetInNanometers() throws XrayTesterException
  {
    int hysteresisOffsetInNanometers = _motionControl.getForwardDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    int configEnumValue = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    Expect.expect(hysteresisOffsetInNanometers == configEnumValue);
    hysteresisOffsetInNanometers = _motionControl.getReverseDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(hysteresisOffsetInNanometers ==  (-1) * configEnumValue);

    configEnumValue = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    hysteresisOffsetInNanometers = _motionControl.getForwardDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(hysteresisOffsetInNanometers == configEnumValue);
    hysteresisOffsetInNanometers = _motionControl.getReverseDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(hysteresisOffsetInNanometers == (-1) * configEnumValue);

    configEnumValue = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
    hysteresisOffsetInNanometers = _motionControl.getForwardDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(hysteresisOffsetInNanometers == configEnumValue);
    hysteresisOffsetInNanometers = _motionControl.getReverseDirectionHysteresisOffsetInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(hysteresisOffsetInNanometers == (-1) * configEnumValue);
  }

  /**
   * @author Greg Esparza
   */
  private void testEnable() throws XrayTesterException
  {
    _motionControl.enable(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    boolean enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(enabled);

    _motionControl.enable(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(enabled);

    _motionControl.enable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(enabled);
  }

  /**
   * @author Greg Esparza
   */
  private void testDisable() throws XrayTesterException
  {
    _motionControl.disable(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    boolean enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(enabled == false);

    _motionControl.disable(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(enabled == false);

    _motionControl.disable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    enabled = _motionControl.isAxisEnabled(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(enabled == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testStop() throws XrayTesterException
  {
    _motionControl.stop(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    _motionControl.stop(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    _motionControl.stop(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
  }

  private void testRailWidthAxis() throws XrayTesterException
  {
    // Initialize the rail width axis
    try
    {
      _motionControl.enable(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      _motionControl.home(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    MotionAxisPosition motionAxisPosition = new MotionAxisPosition();
    int widthInNanometers = (int)(5000.0 * (1.0 / MathUtil.MILS_PER_NANOMETER));

    // Set the rail width to a valid position
    try
    {
      motionAxisPosition.setRailWidthAxisPositionInNanometers(widthInNanometers);
      _motionControl.pointToPointMove(motionAxisPosition, MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      int actualPositionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      Expect.expect(actualPositionInNanometers == motionAxisPosition.getRailWidthAxisPositionInNanometers());
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    try
    {
      widthInNanometers = (int)(10000.0 * (1.0 / MathUtil.MILS_PER_NANOMETER));
      motionAxisPosition.setRailWidthAxisPositionInNanometers(widthInNanometers);
      _motionControl.pointToPointMove(motionAxisPosition, MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    // Test the case with a position that requests a rail width that exceeds the minimum limit
    try
    {
      widthInNanometers = (int)(30000.0 * (1.0 / MathUtil.MILS_PER_NANOMETER));
      motionAxisPosition.setRailWidthAxisPositionInNanometers(widthInNanometers);
      _motionControl.pointToPointMove(motionAxisPosition, MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // we want an exception to be thrown and caught.
    }

    // Test the case with a position that requests a rail width that exceeds the maximum limit
    try
    {
      widthInNanometers = (int)(19000.0 * (1.0 / MathUtil.MILS_PER_NANOMETER));
      motionAxisPosition.setRailWidthAxisPositionInNanometers(widthInNanometers);
      _motionControl.pointToPointMove(motionAxisPosition, MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
      Expect.expect(false);
    }
    catch (XrayTesterException xte)
    {
      // we want an exception to be thrown and caught.
    }
  }

  /**
   * @author Greg Esparza
   */
  private synchronized void setConfigDone(boolean configDone)
  {
    _configDone = configDone;
  }

  private synchronized boolean isConfigDone()
  {
    return _configDone;
  }

  /**
   * @author Greg Esparza
   */
  private void testSetServiceModeConfiguration() throws XrayTesterException
  {

    ThreadTask<Object> task1 = new ThreadTask<Object>("Set configuration")
    {
      protected Object executeTask() throws Exception
      {
        _motionControl.setServiceModeConfiguration();
        Thread.sleep(15000);
        setConfigDone(true);
        return null;
      }

      protected void clearCancel()
      {
        // do nothing
      }

      protected void cancel()
      {
        // do nothing
      }
    };

    ThreadTask<Object> task2 = new ThreadTask<Object>("Get configuration status")
    {
      public Object executeTask() throws Exception
      {
        do
        {
          Thread.sleep(100);
          MotionControlServiceModeConfigurationStatus configStatus = _motionControl.getServiceModeConfigurationStatus();

          LocalizedString localizedString = new LocalizedString(configStatus.getKey(), configStatus.getParameters().toArray());
          String message = StringLocalizer.keyToString(localizedString);
          if (message.length() > 0)
          {
            //System.out.println();
            //System.out.println(message);
          }
        }
        while (isConfigDone() == false);

        return null;
      }

      protected void clearCancel()
      {
        // do nothing
      }

      protected void cancel()
      {
        // do nothing
      }
    };

    _serviceModeConfigParallelTasks.submitThreadTask(task1);
    _serviceModeConfigParallelTasks.submitThreadTask(task2);

    try
    {
      _serviceModeConfigParallelTasks.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void testGetNumberOfControllersInstalled()
  {
    int numberOfControllersInstalled = MotionControl.getNumberOfControllersInstalled();
  }

  /**
   * @author Greg Esparza
   */
  private void testStartup() throws XrayTesterException
  {
    _motionControl.shutdown();
    boolean startupRequired = _motionControl.isStartupRequired();
    Expect.expect(startupRequired);
    _motionControl.startup();
    startupRequired= _motionControl.isStartupRequired();
    Expect.expect(startupRequired == false);
  }

  /**
   * @author Greg Esparza
   */
  private void testPointToPointMove() throws XrayTesterException
  {
    MotionAxisPosition motionAxisPosition = new MotionAxisPosition();

    MotionAxisToMoveEnum motionAxisToMove = MotionAxisToMoveEnum.X_INDEPENDENT_AXIS;
    _motionControl.setPointToPointMotionProfile(motionAxisToMove, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
    motionAxisPosition.setXaxisPositionInNanometers(_X_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    _motionControl.enable(motionAxisToMove);
    _motionControl.pointToPointMove(motionAxisPosition, motionAxisToMove);

    motionAxisToMove = MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS;
    _motionControl.setPointToPointMotionProfile(motionAxisToMove, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
    motionAxisPosition.setYaxisPositionInNanometers(_Y_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    _motionControl.enable(motionAxisToMove);
    _motionControl.pointToPointMove(motionAxisPosition, motionAxisToMove);

    motionAxisToMove = MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS;
    _motionControl.setPointToPointMotionProfile(motionAxisToMove, new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
    motionAxisPosition.setRailWidthAxisPositionInNanometers(_RAIL_WIDTH_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    _motionControl.enable(motionAxisToMove);
    _motionControl.pointToPointMove(motionAxisPosition, motionAxisToMove);
  }

  /**
   * @author Greg Esparza
   */
  private void testGetActualPositionInNanometers() throws XrayTesterException
  {
    int positionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(positionInNanometers == _X_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);

    positionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(positionInNanometers == _Y_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);

    positionInNanometers = _motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(positionInNanometers == _RAIL_WIDTH_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
  }

  /**
   * @author Greg Esparza
   */
  private void testSetActualPositionInNanometers() throws XrayTesterException
  {
    _motionControl.setActualPositionInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, _X_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    Expect.expect(_motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS) == _X_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);

    _motionControl.setActualPositionInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, _Y_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    Expect.expect(_motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS) == _Y_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);

    _motionControl.setActualPositionInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, _RAIL_WIDTH_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
    Expect.expect(_motionControl.getActualPositionInNanometers(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS) == _RAIL_WIDTH_AXIS_POINT_TO_POINT_MOVE_POSITION_IN_NANOMETERS);
  }

  /**
   * @author Greg Esparza
   */
  private void testShutdown() throws XrayTesterException
  {
    _motionControl.shutdown();
  }

  /**
   * @author Greg Esparza
   */
  private void testFullStartup() throws XrayTesterException
  {
    _motionControl.startup();
    _motionControl.shutdown();
    _motionControl.startup();
  }

  /**
   * @author Greg Esparza
   */
  private void testSimulationModes() throws XrayTesterException
  {
    _motionControl.startup();

    // Simulate the entire motion control system
    _motionControl.setSimulationMode();
    Expect.expect(_motionControl.isSimulationModeOn());

    // Clear the simulation for the entire motion control system but we won't
    // command anything that will try and use the hardware in the context of this regression test
    _motionControl.clearSimulationMode();
    Expect.expect(_motionControl.isSimulationModeOn() == false);

    // Simulate the X-axis only
    MotionAxisToMoveEnum motionAxisToMove = MotionAxisToMoveEnum.X_INDEPENDENT_AXIS;
    _motionControl.setSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove));

    // Clear the simulation for the X-axis but we won't
    // command anything that will try and use the hardware in the context of this regression test
    _motionControl.clearSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove) == false);

    // Simulate the Y-axis only
    motionAxisToMove = MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS;
    _motionControl.setSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove));

    // Clear the simulation for the Y-axis but we won't
    // command anything that will try and use the hardware in the context of this regression test
    _motionControl.clearSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove) == false);

    // Simulate the Rail Width-axis only
    motionAxisToMove = MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS;
    _motionControl.setSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove));

    // Clear the simulation for the Rail Width-axis but we won't
    // command anything that will try and use the hardware in the context of this regression test
    _motionControl.clearSimulationMode(motionAxisToMove);
    Expect.expect(_motionControl.isSimulationModeOn(motionAxisToMove) == false);
  }

  private void testGetActiveMotionProfile() throws XrayTesterException
  {
    // Set axes for point-to-point profile 0
    PointToPointMotionProfileEnum pointToPointMotionProfile = PointToPointMotionProfileEnum.PROFILE0;

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    MotionProfile motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    // Set axes for point-to-point profile 1
    pointToPointMotionProfile = PointToPointMotionProfileEnum.PROFILE1;

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    // Set axes for point-to-point profile 2
    pointToPointMotionProfile = PointToPointMotionProfileEnum.PROFILE2;

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    // Set axes for point-to-point profile 3
    pointToPointMotionProfile = PointToPointMotionProfileEnum.PROFILE3;

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    _motionControl.setPointToPointMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS, new MotionProfile(pointToPointMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getPointToPointMotionProfile().equals(pointToPointMotionProfile));

    // Set axes for scan profile 0
    ScanMotionProfileEnum scanMotionProfile = ScanMotionProfileEnum.PROFILE0;
    _motionControl.setScanMotionProfile(new MotionProfile(scanMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getScanMotionProfile().equals(scanMotionProfile));
    motionProfile = _motionControl.getActiveMotionProfile(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
    Expect.expect(motionProfile.getScanMotionProfile().equals(scanMotionProfile));
  }

  /**
   * @author Greg Esparza
   */
  private void testSetHardwareResetMode() throws XrayTesterException
  {
    _motionControl.shutdown();
    boolean hardwareResetModeOn = true;
    _motionControl.setHardwareResetMode(hardwareResetModeOn);
    _motionControl.startup();

    _motionControl.shutdown();
    hardwareResetModeOn = false;
    _motionControl.setHardwareResetMode(hardwareResetModeOn);
    _motionControl.startup();
  }

  /**
   * @author Greg Esparza
   */
  private void testPauseScanPath() throws XrayTesterException
  {
    _motionControl.pauseScanPath();
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException, BadFormatException, IOException
        {
          checkArgs();

          if (_debugMode)
            getKeyboardCommand("DEBUG MODE - Press <ENTER> when ready to start debug session ...");

          testGetNumberOfControllersInstalled();
          testStartup();
          testSetActualPositionInNanometers();
          testSetServiceModeConfiguration();
          testGetYaxisScanStartPositionLimitInNanometers();
          testGetConfigurationDescription();
          testGetMinimumPositionLimitInNanometers();
          testGetMaximumPositionLimitInNanometers();
          testGetValidPositionPulseRateInNanometersPerPulse();
          testIsHomeSensorClear();
          testGetMinimumScanPassLengthInNanometers();
          testGetMaximumScanPassLengthInNanometers();
          testGetMinimumScanStepWidthInNanometers();
          testGetMaximumScanStepWidthInNanometers();
          testSetHysteresisOffsetInNanometers();
          testGetHysteresisOffsetInNanometers();
          testEnable();
          testDisable();
          testStop();
          testPointToPointMove();
          testGetActualPositionInNanometers();
          testRailWidthAxis();
          testShutdown();
          testFullStartup();
          testGetActiveMotionProfile();
          testSimulationModes();
          testSetHardwareResetMode();
          testPauseScanPath();
        }
      });
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
  }

  /**
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MotionControl(args));
  }
}
