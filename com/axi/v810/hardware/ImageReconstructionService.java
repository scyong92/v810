package com.axi.v810.hardware;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Controls the image reconstruction service. This is a program
 * running as a service on each image reconstruction processor.
 * The program will be able to do the following:
 * Shutdown the hardware
 * Restart the hardware
 * Start a program
 * Stop/Kill a program
 * Determine if a program is running
 * Get the version of the program (encoded in the exe)
 * Get the total ram installed on the computer in kilobytes
 * @author George A. David
 * @author Rex Shang
 */
public class ImageReconstructionService
{
  private static boolean _debug = false;
  private static String _me = "ImageReconstructionService";
  private static int _SOCKET_TIME_OUT_IN_MILLI_SECONDS = 30000;
  private static boolean _WAIT_ABORTABLE = true;
  private static Config _config;

  private SocketClient _socketClient;
  private InetSocketAddress _inetSocketAddress;
  private boolean _isConnected = false;
  private ByteOrder _byteOrder = ByteOrder.LITTLE_ENDIAN;
  private HardwareTaskEngine _hardwareTaskEngine;
  private ImageReconstructionProcessor _irp;
  private int _numberOfProgramRunning = 0;

  /**
   * @author Rex Shang
   */
  static
  {
    _config = Config.getInstance();
    _debug = _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
  }

  /**
   * @author Rex Shang
   */
  ImageReconstructionService(ImageReconstructionProcessor irp)
  {
    this(new InetSocketAddress(irp.getHostIpAddress(), getServerPortAddress()));
    _irp = irp;
  }

  /**
   * Constructor used only for testing.
   * @author George A. David
   */
  ImageReconstructionService(String serverIpAddress, int portNumber)
  {
    this(new InetSocketAddress(serverIpAddress, portNumber));
  }

  /**
   * @author George A. David
   */
  private ImageReconstructionService(InetSocketAddress inetSocketAddress)
  {
    Assert.expect(inetSocketAddress != null);

    _inetSocketAddress = inetSocketAddress;
    _socketClient = new SocketClient(_inetSocketAddress);
    _socketClient.setDefaultByteOrderForSendAndRecieveBuffers(_byteOrder);

    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @author George A. David
   */
  public boolean isHardwareRunning()
  {
    boolean isRunning = false;
    try
    {
      // try sending a message, if we get a response, then it's running
      getRamInKiloBytes();
      isRunning = true;
    }
    catch (XrayTesterException ex)
    {
      isRunning = false;
    }

    return isRunning;
  }

  /**
   * Determines whether the ip address of this service
   * is on the network and available.
   * @author George A. David
   */
  private boolean isHardwareReachable()
  {
    boolean isReachable = false;
    try
    {
      isReachable = _inetSocketAddress.getAddress().isReachable(1000);
    }
    catch (IOException ex)
    {
      isReachable = false;
    }

    return isReachable;
  }

  /**
   * @author George A. David
   */
  private void connectIfNecessary() throws XrayTesterException
  {
    Assert.expect(_socketClient != null);
    if(_isConnected == false)
    {
      try
      {
        _socketClient.connect(_SOCKET_TIME_OUT_IN_MILLI_SECONDS);
      }
      catch (SocketConnectionFailedException scfe)
      {
        ImageReconstructionProcessorHardwareException irpException =
            ImageReconstructionProcessorHardwareException.getFailedToConnectToImageReconstructionServiceException(
                _irp.getId().toString(),
                _inetSocketAddress.getAddress().getHostAddress(),
                _inetSocketAddress.getPort(),
                scfe);
        _hardwareTaskEngine.throwHardwareException(irpException);
      }
      _isConnected = true;
    }
  }

  /**
   * @author George A. David
   */
  private ByteBuffer getByteBuffer(int sizeInBytes)
  {
    Assert.expect(sizeInBytes > 0);
    ByteBuffer buffer = ByteBuffer.allocate(sizeInBytes);
    buffer.order(_byteOrder);

    return buffer;
  }

  /**
   * @author George A. David
   */
  private void disconnect() throws XrayTesterException
  {
    int totalCommandSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes();
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.DISCONNECT_COMMAND.getId());
    buffer.flip();

    try
    {
      _socketClient.sendRequest(buffer);
    }
    catch (IOException ioe)
    {
      ImageReconstructionProcessorHardwareException irpException =
          ImageReconstructionProcessorHardwareException.getFailedToCommunicateWithImageReconstructionServiceException(
              _irp.getId().toString(),
              _inetSocketAddress.getAddress().getHostAddress(),
              _inetSocketAddress.getPort(),
              ioe);
      _hardwareTaskEngine.throwHardwareException(irpException);
    }
  }

  /**
   * @author George A. David
   */
  private void shutdownHardware() throws XrayTesterException
  {
    int totalCommandSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes();
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.SHUTDOWN_HARDWARE_COMMAND.getId());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();
    debug("shutdownHardware(): " + reply);

    if (reply == 1)
    {
      try
      {
        _socketClient.close();
      }
      catch (IOException ioe)
      {
        // Do nothing.
      }
    }
  }

 /**
   * @author George A. David
   */
  public void restartHardware() throws XrayTesterException
  {
    int totalCommandSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes();
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.RESTART_HARDWARE_COMMAND.getId());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();
    if(reply == 1)
    {
      try
      {
        _socketClient.close();
      }
      catch (IOException ioe)
      {
        // Do nothing.
      }

      _isConnected = false;
    }

    debug("restartHardware(): " + (reply == 1));

    waitForImageReconstructionProcessorToReboot();
  }

  /**
   * @author George A. David
   */
  public int getRamInKiloBytes() throws XrayTesterException
  {
    int totalCommandSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes();
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.GET_RAM_COMMAND.getId());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int ramInKiloBytes = buffer.getInt();
    debug("getRamInKiloBytes(): " + ramInKiloBytes);

    return ramInKiloBytes;
  }

  /**
   * @author George A. David
   */
  public void startProgram(String path,
                           String name,
                           String arguments,
                           String additionalEnv,
                           String pathExtension) throws XrayTesterException
  {
    Assert.expect(path != null);
    Assert.expect(name != null);
    Assert.expect(arguments != null);
    Assert.expect(additionalEnv != null);
    Assert.expect(pathExtension != null);

    _numberOfProgramRunning = getNumberOfProgramRunning(path, name);

    // the header consists of the size of the command + 10 integers:
    // the path length
    // the offset to the path
    // the name length
    // the offset to the name
    // the arguments length
    // the offset to the arguments
    // the additionalEnv length
    // the offset to the additionalEnv
    // the pathExtension length
    // the offset to the path extension
    int headerSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes() + 10 * JavaTypeSizeEnum.INT.getSizeInBytes();
    int totalCommandSize = headerSize + path.length() + name.length() + arguments.length() + additionalEnv.length() + pathExtension.length();
    int offset = headerSize;
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.START_PROGRAM_COMMAND.getId());

    buffer.putInt(path.length());
    buffer.putInt(offset);

    buffer.putInt(name.length());
    offset += path.length();
    buffer.putInt(offset);

    buffer.putInt(arguments.length());
    offset += name.length();
    buffer.putInt(offset);

    buffer.putInt(additionalEnv.length());
    offset += arguments.length();
    buffer.putInt(offset);

    buffer.putInt(pathExtension.length());
    offset += additionalEnv.length();
    buffer.putInt(offset);

    buffer.put(path.getBytes());
    buffer.put(name.getBytes());
    buffer.put(arguments.getBytes());
    buffer.put(additionalEnv.getBytes());
    buffer.put(pathExtension.getBytes());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();
    /** @todo gad handle reply */
    debug("startProgram(): " + reply);

    waitForImageReconstructionProcessorToStart(path, name);
  }

  /**
   * @author George A. David
   */
  public void stopProgram(String path, String name) throws XrayTesterException
  {
    Assert.expect(path != null);
    Assert.expect(name != null);

    // the header consists of the size of the command + 4 integers:
    // the path length
    // the offset to the path
    // the name length
    // the offset to the name
    int headerSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes() + 4 * JavaTypeSizeEnum.INT.getSizeInBytes();
    int totalCommandSize = headerSize + path.length() + name.length();
    int offset = headerSize;
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.STOP_PROGRAM_COMMAND.getId());

    buffer.putInt(path.length());
    buffer.putInt(offset);

    buffer.putInt(name.length());
    offset += path.length();
    buffer.putInt(offset);

    buffer.put(path.getBytes());
    buffer.put(name.getBytes());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();
    /** @todo gad handle reply */
    debug("stopProgram(): " + reply);

    waitForImageReconstructionProcessorToStop(path, name);
  }

  /**
   * @author George A. David
   */
  public boolean isProgramRunning(String path, String name) throws XrayTesterException
  {
    int reply = getNumberOfProgramRunning(path, name);

    boolean programRunning = (reply > 0);

    debug("isProgramRunning(): " + programRunning);
    return programRunning;
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  public int getNumberOfProgramRunning(String path, String name) throws XrayTesterException
  {
    Assert.expect(path != null);
    Assert.expect(name != null);

    // the header consists of the size of the command + 4 integers:
    // the path length
    // the offset to the path
    // the name length
    // the offset to the name
    int headerSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes() + 4 * JavaTypeSizeEnum.INT.getSizeInBytes();
    int totalCommandSize = headerSize + path.length() + name.length();
    int offset = headerSize;
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.IS_PROGRAM_RUNNING_COMMAND.getId());

    buffer.putInt(path.length());
    buffer.putInt(offset);

    buffer.putInt(name.length());
    offset += path.length();
    buffer.putInt(offset);

    buffer.put(path.getBytes());
    buffer.put(name.getBytes());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();

    return reply;
  }

  /**
   * @author George A. David
   */
  public void getProgramVersion(String path, String name) throws XrayTesterException
  {
    Assert.expect(path != null);
    Assert.expect(name != null);

    // the header consists of the size of the command + 4 integers:
    // the path length
    // the offset to the path
    // the name length
    // the offset to the name
    int headerSize = ImageReconstructionServiceCommandEnum.getCommandSizeInBytes() + 4 * JavaTypeSizeEnum.INT.getSizeInBytes();
    int totalCommandSize = headerSize + path.length() + name.length();
    int offset = headerSize;
    ByteBuffer buffer = getByteBuffer(totalCommandSize);
    buffer.putInt(ImageReconstructionServiceCommandEnum.GET_PROGRAM_VERSION_COMMAND.getId());

    buffer.putInt(path.length());
    buffer.putInt(offset);

    buffer.putInt(name.length());
    offset += path.length();
    buffer.putInt(offset);

    buffer.put(path.getBytes());
    buffer.put(name.getBytes());
    buffer.flip();

    buffer = sendRequestAndReceiveReplies(buffer, 4);
    int reply = buffer.getInt();
    /** @todo gad handle reply */
    debug("getProgramVersion(): " + reply);
  }

  /**
   * @author Rex Shang
   */
  private void waitForImageReconstructionProcessorToStart(
      final String remoteFilePath, final String executableName) throws XrayTesterException
  {
    final int expectedNumberOfProgramRunning = _numberOfProgramRunning + 1;

    // Wait up to 5 seconds for application to start.
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(5000, _WAIT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        int numberOfProgramRunning = getNumberOfProgramRunning(remoteFilePath, executableName);
        boolean isStarted = (numberOfProgramRunning >= expectedNumberOfProgramRunning);
        return isStarted;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return ImageReconstructionProcessorHardwareException.getFailedToStartImageReconstructionApplicationException(_irp.getId().toString(), getIpAddress());
      }
    };

    myWait.waitUntilConditionIsMet();

    // Do an extra wait for irp to establish network.
    /** @todo wpd - remove this wait after Roy/Bob/Dave puts in a call to the IRP that says it is ready
     *              to be used.
     */
    doMyWaitInMilliSeconds(5000);
  }

  /**
   * @author Rex Shang
   */
  private void waitForImageReconstructionProcessorToStop(
      final String remoteFilePath, final String executableName) throws XrayTesterException
  {
    // Wait up to 5 seconds for application to stop.
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(5000, _WAIT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean isStopped = isProgramRunning(remoteFilePath, executableName) == false;
        return isStopped;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return ImageReconstructionProcessorHardwareException.getFailedToStopImageReconstructionApplicationException(_irp.getId().toString(), getIpAddress());
      }
    };

    myWait.waitUntilConditionIsMet();

    // Do an extra wait just to be sure the networks resources are reclaimed.
    doMyWaitInMilliSeconds(500);
  }

  /**
   * @author Rex Shang
   */
  private void waitForImageReconstructionProcessorToReboot() throws XrayTesterException
  {
    // In case the computer is not completely down, do our wait of 1 1/2 minute.
    doMyWaitInMilliSeconds(90000);

    // Wait up to 4 more minutes.
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(4 * 60000, _WAIT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean isStarted = isHardwareRunning();
        return isStarted;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return ImageReconstructionProcessorHardwareException.getFailedToRebootImageReconstructionHardwareException(_irp.getId().toString(), getIpAddress());
      }
    };

    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Rex Shang
   */
  private void doMyWaitInMilliSeconds(int timeInMilliSeconds)
  {
    long deadline = System.currentTimeMillis() + timeInMilliSeconds;

    while (System.currentTimeMillis() < deadline)
    {
      try
      {
        // Wait half of the timeOut.  Do this twice if we don't get interrupted.
        Thread.sleep(timeInMilliSeconds / 2);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private ByteBuffer sendRequestAndReceiveReplies(ByteBuffer buffer, int replySizeInBytes) throws XrayTesterException
  {
    connectIfNecessary();

    ByteBuffer replyBuffer = null;
    try
    {
      replyBuffer = _socketClient.sendRequestAndReceiveReplies(buffer, replySizeInBytes);
    }
    catch (IOException ex)
    {
      //ok, we failed, try reconnecting and sending again.
      try
      {
        _socketClient.close();
      }
      catch (IOException ioe)
      {
        // Do nothing.
      }

      _isConnected = false;
      connectIfNecessary();
      try
      {
        replyBuffer = _socketClient.sendRequestAndReceiveReplies(buffer, replySizeInBytes);
      }
      catch (IOException ioe)
      {
        ImageReconstructionProcessorHardwareException exception =
            ImageReconstructionProcessorHardwareException.getFailedToCommunicateWithImageReconstructionServiceException(
                _irp.getId().toString(),
                _inetSocketAddress.getAddress().getHostAddress(),
                _inetSocketAddress.getPort(),
                ioe);
        _hardwareTaskEngine.throwHardwareException(exception);
      }
    }

    try
    {
      _socketClient.close();
    }
    catch (IOException ioe)
    {
      // Do nothing.
    }
    _isConnected = false;

    Assert.expect(replyBuffer != null);
    return replyBuffer;
  }

  /**
   * @author George A. David
   */
  private String getIpAddress()
  {
    Assert.expect(_inetSocketAddress.getAddress().getHostAddress() != null);

    return _inetSocketAddress.getAddress().getHostAddress();
  }

  /**
   * @author Rex Shang
   */
  private static int getServerPortAddress()
  {
    return _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_SERVICE_PORT_NUMBER);
  }

  /**
   * @author Rex Shang
   */
  private void debug(String string)
  {
    if (_debug)
    {
      System.out.println(_me + " " + _inetSocketAddress.getAddress().getHostAddress() + " :" + string);
    }
  }

}
