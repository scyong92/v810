package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * Events for light stack indicators.
 * @author Rex Shang
 */
public class LightStackEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static LightStackEventEnum RED_LIGHT_ON = new LightStackEventEnum(++_index, "Light Stack, Red Light On");
  public static LightStackEventEnum RED_LIGHT_OFF = new LightStackEventEnum(++_index, "Light Stack, Red Light Off");
  public static LightStackEventEnum YELLOW_LIGHT_ON = new LightStackEventEnum(++_index, "Light Stack, Yellow Light On");
  public static LightStackEventEnum YELLOW_LIGHT_OFF = new LightStackEventEnum(++_index, "Light Stack, Yellow Light Off");
  public static LightStackEventEnum GREEN_LIGHT_ON = new LightStackEventEnum(++_index, "Light Stack, Green Light On");
  public static LightStackEventEnum GREEN_LIGHT_OFF = new LightStackEventEnum(++_index, "Light Stack, Green Light Off");
  public static LightStackEventEnum BUZZER_ON = new LightStackEventEnum(++_index, "Light Stack, Buzzer On");
  public static LightStackEventEnum BUZZER_OFF = new LightStackEventEnum(++_index, "Light Stack, Buzzer Off");

  private String _name;

  /**
   * @author Rex Shang
   */
  private LightStackEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }


}
