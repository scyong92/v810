package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.Assert;
import com.axi.v810.datastore.config.SoftwareConfigEnum;

/**
 * @author Rex Shang
 */
public class PanelHandlerPanelFlowDirectionEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static HashMap<String, PanelHandlerPanelFlowDirectionEnum> _stringToEnumMap;
  private String _name;

  /**
   * @author Rex Shang
   */
  static
  {
    _stringToEnumMap = new HashMap<String, PanelHandlerPanelFlowDirectionEnum>();
  }

  public static final PanelHandlerPanelFlowDirectionEnum LEFT_TO_RIGHT = new PanelHandlerPanelFlowDirectionEnum(
      ++_index, SoftwareConfigEnum.getPanelHandlerPanelFlowDirectionLeftToRightModeName());

  public static final PanelHandlerPanelFlowDirectionEnum RIGHT_TO_LEFT = new PanelHandlerPanelFlowDirectionEnum(
      ++_index, SoftwareConfigEnum.getPanelHandlerPanelFlowDirectionRightToLeftModeName());

  /**
   * @author Rex Shang
   */
  private PanelHandlerPanelFlowDirectionEnum(int id, String flowDirectionString)
  {
    super(id);

    Assert.expect(flowDirectionString != null);
    _name = flowDirectionString;
    Assert.expect(_stringToEnumMap.put(flowDirectionString, this) == null);
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerPanelFlowDirectionEnum getEnum(String keyString)
  {
    Assert.expect(keyString != null);
    PanelHandlerPanelFlowDirectionEnum modeEnum = _stringToEnumMap.get(keyString);

    Assert.expect(modeEnum != null);
    return modeEnum;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static List<PanelHandlerPanelFlowDirectionEnum> getAllValues()
  {
    List<PanelHandlerPanelFlowDirectionEnum> allValues = new ArrayList<PanelHandlerPanelFlowDirectionEnum>();
    allValues.add(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT);
    allValues.add(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT);
    return allValues;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
