package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCalibrationProfileEnum extends com.axi.util.Enum 
{
    private static HashMap<Integer, OpticalCalibrationProfileEnum> _integerToEnumMap;
    
    /**
     * @author Cheah Lee Herng
    */
    static
    {
      _integerToEnumMap = new HashMap<Integer, OpticalCalibrationProfileEnum>();
    }
  
    private static int index = 0;    
    public static final OpticalCalibrationProfileEnum PROFILE_1 = new OpticalCalibrationProfileEnum(index++, 
                                                                    Config.getInstance().getIntValue(PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET),
                                                                    -2,
                                                                    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET);
    public static final OpticalCalibrationProfileEnum PROFILE_2 = new OpticalCalibrationProfileEnum(index++, 
                                                                    Config.getInstance().getIntValue(PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET),
                                                                    -1,
                                                                    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET);
    
    private int _offset = -1;
    private int _heightAdjustment;
    private PspCalibEnum _calibEnum;
     
    
    /**
     * OpticalCalibrationProfileEnum
     *
     * @author Cheah Lee Herng
    */
    protected OpticalCalibrationProfileEnum(int id, int offset, int heightAdjustment, PspCalibEnum calibEnum)
    {
        super(id);        
        _offset = offset;
        _calibEnum = calibEnum;
        _heightAdjustment = heightAdjustment;
        
        Assert.expect(_integerToEnumMap.put(new Integer(id), this) == null);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getOffset()
    {
        return _offset;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void saveOffset(int offset) throws XrayTesterException
    {
        Config.getInstance().setValue(_calibEnum, offset);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getHeightAdjustment()
    {
        return _heightAdjustment;
    }
    
    /**
    * @author Cheah Lee Herng
    */
    static public ArrayList<OpticalCalibrationProfileEnum> getAllEnums()
    {
      return new ArrayList<OpticalCalibrationProfileEnum>(_integerToEnumMap.values());
    }
}
