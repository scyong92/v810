package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * This class represents the PSP module version.
 * This is the version control for PSP hardware module.
 * 
 * Version 1:
 *   -> Microcontroller
 *   -> Optical Camera
 *   -> Pico projector
 * 
 * Version 2:
 *   -> Ethernet-based Light Engine
 *   -> Optical Camera
 * 
 * @author Cheah Lee Herng
 */
public class PspModuleVersionEnum extends com.axi.util.Enum 
{
  private static int _index = 1;
  private int _version;
  private static Map<Integer, PspModuleVersionEnum> _pspVersionToVersionEnumMap;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    _pspVersionToVersionEnumMap = new HashMap<Integer, PspModuleVersionEnum>();
  }

  public static final PspModuleVersionEnum VERSION_1  = new PspModuleVersionEnum(++_index, 1);
  public static final PspModuleVersionEnum VERSION_2  = new PspModuleVersionEnum(++_index, 2);
  
  /**
   * @author Cheah Lee Herng 
   */
  private PspModuleVersionEnum(int id, int version)
  {
    super(id);
    _version = version;
    
    _pspVersionToVersionEnumMap.put(version, this);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public static PspModuleVersionEnum getEnum(int version)
  {
    PspModuleVersionEnum pspVersionEnum = _pspVersionToVersionEnumMap.get(version);
    Assert.expect(pspVersionEnum != null);

    return pspVersionEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getVersion()
  {
    return _version;
  }
}
