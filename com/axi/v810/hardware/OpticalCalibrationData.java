package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCalibrationData 
{
  private PspModuleEnum _pspModuleEnum;
  private OpticalCalibrationProfileEnum _profileEnum;
  private Map<OpticalCalibrationProfileEnum, Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>> _calibrationPositionToImageFullPath;
  private Map<OpticalCalibrationPlaneEnum, StagePositionMutable> _planeToStagePositionMap;
  private int _totalRun;
  
  private double _knownHeight1 = 0.0;
  private double _knownHeight2 = 0.0;
  private String _calibrationDirectoryFullPath;
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalCalibrationData()
  {
    _calibrationPositionToImageFullPath = new HashMap<OpticalCalibrationProfileEnum, Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>>();
    _planeToStagePositionMap = new HashMap<OpticalCalibrationPlaneEnum, StagePositionMutable>();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addImageFullPath(OpticalCalibrationProfileEnum profileEnum, 
                               OpticalCalibrationPlaneEnum planeEnum, 
                               StagePositionMutable stagePositionMutable, 
                               OpticalShiftNameEnum opticalShiftNameEnum, 
                               String imageFullPath)
  {
    Assert.expect(profileEnum != null);
    Assert.expect(planeEnum != null);
    Assert.expect(stagePositionMutable != null);
    Assert.expect(opticalShiftNameEnum != null);
    Assert.expect(imageFullPath != null);
    
    if (_calibrationPositionToImageFullPath.containsKey(profileEnum) == false)
    {
      _calibrationPositionToImageFullPath.put(profileEnum, new TreeMap<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>());
    }
    
    Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>> positionMapToImageFullPath = _calibrationPositionToImageFullPath.get(profileEnum);
    if (positionMapToImageFullPath.containsKey(planeEnum) == false)
    {
      positionMapToImageFullPath.put(planeEnum, new ArrayList<Pair<OpticalShiftNameEnum, String>>());
    }
    
    List<Pair<OpticalShiftNameEnum, String>> opticalShiftNamePairList = positionMapToImageFullPath.get(planeEnum);
    opticalShiftNamePairList.add(new Pair<OpticalShiftNameEnum, String>(opticalShiftNameEnum, imageFullPath));
    
    // Update plane to stage position map
    if (_planeToStagePositionMap.containsKey(planeEnum) == false)
      _planeToStagePositionMap.put(planeEnum, stagePositionMutable);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<OpticalCalibrationProfileEnum, Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>> getProfileStagePositionMap()
  {
    return _calibrationPositionToImageFullPath;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public StagePositionMutable getStagePosition(OpticalCalibrationPlaneEnum planeEnum)
  {
    Assert.expect(planeEnum != null);
    StagePositionMutable stagePosition = _planeToStagePositionMap.get(planeEnum);
    
    Assert.expect(stagePosition != null);
    return stagePosition;
  }
  
//  /**
//   * @author Cheah Lee Herng 
//   */
//  public Map<Pair<OpticalCalibrationPlaneEnum, StagePositionMutable>, List<Pair<OpticalShiftNameEnum, String>>> getStagePosition(OpticalCalibrationProfileEnum profileEnum)
//  {
//    Assert.expect(profileEnum != null);
//    
//    if (_calibrationPositionToImageFullPath.containsKey(profileEnum))
//    {
//      return new TreeMap<Pair<OpticalCalibrationPlaneEnum, StagePositionMutable>, List<Pair<OpticalShiftNameEnum, String>>>(_calibrationPositionToImageFullPath.get(profileEnum));
//    }
//    return new TreeMap<Pair<OpticalCalibrationPlaneEnum, StagePositionMutable>, List<Pair<OpticalShiftNameEnum, String>>>();
//  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPspModuleEnum(PspModuleEnum pspModuleEnum)
  {
    _pspModuleEnum = pspModuleEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleEnum getPspModuleEnum()
  {
    return _pspModuleEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setProfileEnum(OpticalCalibrationProfileEnum profileEnum)
  {
    _profileEnum = profileEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public OpticalCalibrationProfileEnum getProfileEnum()
  {
    return _profileEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setTotalRun(int totalRun)
  {
    _totalRun = totalRun;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getTotalRun()
  {
    return _totalRun;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setKnownHeight1(double knownHeight1)
  {
    _knownHeight1 = knownHeight1;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getKnownHeight1()
  {
    return _knownHeight1;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setKnownHeight2(double knownHeight1)
  {
    _knownHeight2 = knownHeight1;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getKnownHeight2()
  {
    return _knownHeight2;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setCalibrationDirectoryFullPath(String calibrationDirectoryFullPath)
  {
    _calibrationDirectoryFullPath = calibrationDirectoryFullPath;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getCalibrationDirectoryFullPath()
  {
    return _calibrationDirectoryFullPath;
  }
}
