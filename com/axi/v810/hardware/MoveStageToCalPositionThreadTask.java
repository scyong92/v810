package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;


/**
 * This class is designed to allow stage motion to camera light calibration position
 * while performing other tasks in parallel.
 * @author Farn Sern
 */

public class MoveStageToCalPositionThreadTask extends ThreadTask<Object>
{
  private PanelPositioner _panelPositioner;
  private StagePositionMutable _stagePosition;
  private static Config _config = Config.getInstance();

  /**
   * @author Farn Sern
   */
  public MoveStageToCalPositionThreadTask()
  {
    super("Move Stage To Cal Position Thread Task");
    _panelPositioner = PanelPositioner.getInstance();
    // Define stage position
    _stagePosition = new StagePositionMutable();
    _stagePosition.setXInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS));
    _stagePosition.setYInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS));

  }

  /**
   * @author Farn Sern
   */
  public Object executeTask() throws Exception
  {
    // Save off the previously loaded motion profile
    MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
    _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
    _panelPositioner.pointToPointMoveAllAxes(_stagePosition);
    // Now that we are done with the move, restore the motion profile.
    _panelPositioner.setMotionProfile(originalMotionProfile);
    return null;
  }

  /**
   * @author Farn Sern
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Farn Sern
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }

}
