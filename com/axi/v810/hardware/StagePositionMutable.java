package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class is used to describe a stage X, Y, and Z position for the
 * PanelPositioner. The PanelPositioner will either move to the position
 * defined in this class or return it's current position .
 *
 * @author Greg Esparza
 * @author Matt Wharton
 */
public class StagePositionMutable implements StagePosition
{
  private MachineCoordinate _xyPosition = null;

  /**
   * @author Greg Esparza
   * @author Matt Wharton
   */
  public StagePositionMutable()
  {
    _xyPosition = new MachineCoordinate();
  }

  /**
   * @author Greg Esparza
   * @author Matt Wharton
   */
  public void setXInNanometers(int xPositionInNanometers)
  {
    _xyPosition.setX( xPositionInNanometers );
  }

  /**
   * @author Greg Esparza
   * @author Matt Wharton
   */
  public void setYInNanometers(int yPositionInNanometers)
  {
    _xyPosition.setY( yPositionInNanometers );
  }

  /**
   * @author Greg Esparza
   * @author Matt Wharton
   */
  public int getXInNanometers()
  {
    return _xyPosition.getX();
  }

  /**
   * @author Greg Esparza
   * @author Matt Wharton
   */
  public int getYInNanometers()
  {
    return _xyPosition.getY();
  }
}
