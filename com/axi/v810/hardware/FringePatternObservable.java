package com.axi.v810.hardware;

import com.axi.util.*;

import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternObservable extends Observable 
{
    private static FringePatternObservable _instance = null;
    
    /**
     * @author Cheah Lee Herng 
     */
    public static synchronized FringePatternObservable getInstance()
    {
        if (_instance == null)
        {
          _instance = new FringePatternObservable();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized void sendEvent(FringePatternInfo fringePatternInfo)
    {
        Assert.expect(fringePatternInfo != null);
        
        setChanged();
        notifyObservers(fringePatternInfo);
    }
}
