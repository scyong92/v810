package com.axi.v810.hardware;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import gnu.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * The LegacyXraySourceCommandingLink is an implementation class for LegacyXraySource.
 *   If you are not doing maintenance programming on the LegacyXraySource class, then
 *   stop reading now.<p/>
 *
 * This class is responsible for the low-level communication protocol with the firmware
 *  that controls the legacy (5DX) X-ray source hardware (and simulating the same).<p/>
 *
 * Specifically,<ul>
 *   <li>configuring the proper serialPort as 9600,8,NONE,1 with no flow control</li>
 *   <li>resetting the serial link</li>
 *   <li>sending CR delimitted chunks of text to the serial port</li>
 *   <li>verifying the echo of CR delimitted chunks sent (bit buckets the echo)</li>
 *   <li>receiving CR delimitted chunks of text from serial port</li>
 * </ul>
 *
 * THREAD SAFETY: This class is synchronized at the level of request/response exchange
 *   on the serial port (assuming no programmer errors).  The LegacyXraySource class should,
 *   therefore, be re-entrant.<p/>
 *
 * NOTE: This class will blow up (throw checked Exceptions) if a serial port is not available
 *   and it is NOT run in simulation mode.  Checking XrayTester.isHardwareAvailable() is
 *   generally not allowed.<p/>
 *
 * "HARDWARE" DESCRIPTION: The 5DX X-ray source is actually controlled by two *separate*
 *   sets of electronics.  These two *separate* controllers are "daisy chained" (not my
 *   words) onto the same RS-232 port.  The high voltage power supply (HV) controller
 *   is connected to both the PC and the top gun electronics (XR) controller.  The HVPS
 *   controller relays data from the PC to the XR controller and vice versa.  Commands
 *   that start with a 9 are interpreted and executed by the HV controller.  Commands
 *   that start with an 8 are interpreted and executed by the HV controler.  The HV
 *   controller ignores commands that start with an 8 (except to relay them).  The XR
 *   controller ignores commands that start with a 9.  This sounds wacky, but consider
 *   the fact that ground for the XR controller is 160kV lower than ground for the HV
 *   controller.<p/>
 *
 * SEE: N7200-66236-3-a12.pdf<p/>
 *
 * This class does not implement HardwareObject because it was not clear to the original
 *   author what the intent and obligations of HardwareObject derivatives are.
 *     -- "Better to be reckoned a fool than to open your mouth and remove all doubt."
 *
 * @author Greg Loring
 */
class LegacyXraySourceCommandingLink implements Simulatable
{
  // RECEIVE_TIMEOUT serves as both the first character and inter-character timeout;
  //  raised from 1 second to 1.5 seconds based on observed hardware behavior
  private static final int RECEIVE_TIMEOUT = 1500; // ms

  // constants for implementing CR framing (CR serves as an end-of-message marker)
  private static final String CR_RE = "\r";
  private static final byte CR = '\r';

  private static Config _config = Config.getInstance();
  private static boolean _simulateLinkTiming = false;

  // simulation state
  private final Map<String, String> _setToReadCommandMap = new TreeMap<String,String>();
  private Map<String, Integer> _simMeasures = new TreeMap<String, Integer>();
  private boolean _isThisObjectInSimMode;

  // timings state
  StatisticalTimer _callTimer = new StatisticalTimer("LegacyXraySource:sendAndReceive");

  private final String _portName;
  private SerialPort _serialPort;
  private byte[] _inputBuffer;
  private Queue<String> _responseQueue;

  /**
   * the real hardware on a real serial port takes almost exactly .2 seconds
   *  with a very small standard deviation between samples (except for the
   *  first call after the JVM start up which takes a little over 5 seconds)
   * HOWEVER, calling this sleep is breaking regression tests that have
   *  timing dependencies, like Test_HardwareTaskEngine...
   * SO, call this method when you want it
   *
   * @author Greg Loring
   */
  static void setSimulateLinkTiming()
  {
    _simulateLinkTiming = true;
  }

  /**
   * @author Greg Loring
   */
  LegacyXraySourceCommandingLink(String portName)
  {
    Assert.expect(portName != null);

    // initialize simulation state (also used for syntax validation)
    _setToReadCommandMap.put("9O0", "9I3"); // AnodeVoltage
    _setToReadCommandMap.put("8A0", "8I5"); // CathodeCurrent
    _setToReadCommandMap.put("8P0", "8I3"); // FocusVoltage
    _setToReadCommandMap.put("8P2", "8I0"); // FilamentVoltage
    _simMeasures.put("9I3", 93); // Anode Voltage       -> 3.747 kV
    _simMeasures.put("9I2", 92); // Total Current       -> 4.493 uA
    _simMeasures.put("8I5", 85); // Cathode Current     -> 16.62 uA
    _simMeasures.put("8I2", 82); // Grid Voltage        -> -40.07 V
    _simMeasures.put("8I3", 83); // Focus Voltage       -> 243.4 V
    _simMeasures.put("8I0", 80); // Filament Voltage    -> .7586 V
    _simMeasures.put("8I1", 81); // Isolated PS Voltage -> 1.584 V
    _simMeasures.put("8I6", 161); // Gun Temperature    -> 100.1 C
    _simMeasures.put("9V0", 1000); // HVPS rev code
    _simMeasures.put("9S2", 14); // HVPS status
    _simMeasures.put("9M1", 0); // Minimum Anode Voltage -> 0.0 kV
    _simMeasures.put("9M2", 95); // Maximum Anode Voltage -> 3.828 kV

    // initialize hardware state
    _portName = portName;
    _serialPort = null; // already is, but more obvious if you see it
    _inputBuffer = new byte[1024]; // largest ever expected is 5
    _responseQueue = new LinkedList<String>();
  }

  /**
   * put this object into simulation mode
   *
   * @author Greg Loring
   */
  public synchronized void setSimulationMode() throws DatastoreException
  {
    _isThisObjectInSimMode = true;
  }

  /**
   * take this object out of individual simulation mode (may stay in simulation
   *  mode based on overall system configuration
   *
   * @author Greg Loring
   */
  public synchronized void clearSimulationMode() throws DatastoreException
  {
    _isThisObjectInSimMode = false;
  }

  /**
   * sim mode may be on for this object because it was turned on explicitly via
   *  setSimulationMode(), or because there is some indication in the overall
   *  system configuration that all hardware related objects should be operating
   *  in sim mode.  In either case, this object will not interact with hardware.
   *
   * @author Greg Loring
   */
  public boolean isSimulationModeOn()
  {
    boolean result = _isThisObjectInSimMode
                     || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    return result;
  }

  /**
   * reset the commanding link according to the established protocol
   *
   * @author Greg Loring
   */
  synchronized void reset() throws HardwareException
  {
    if (isSimulationModeOn() == false)
    {
      configureSerialPortOnce();
      serialPortReset();
    }
  }

  /**
   * send a chunk of text (adding CR delimitter) to the serial port, verify the echo,
   *  and receive a chunk of text (removing CR delimitter) from the serial port
   *
   * @author Greg Loring
   */
  synchronized String sendAndReceive(String request) throws HardwareException
  {
    // syntax validation
    Assert.expect(request != null);
    Assert.expect((request.length() == 3) || (request.length() == 8));
    Assert.expect(request.startsWith("8") || request.startsWith("9"));
    if (request.length() == 3)
    {
      Assert.expect(_simMeasures.containsKey(request)
                    || request.equals("9S3")
                    || request.equals("9M0"));
    }
    else // request.length() == 8, already Assert'ed
    {
      Assert.expect(_setToReadCommandMap.containsKey(request.substring(0, 3)));
      Assert.expect(request.indexOf(',') == 3);
      Assert.expect(Pattern.matches("[0-9]{4}", request.substring(4)));
    }

//    // find caller
//    String caller = "<<unknown>>";
//    StringWriter sout = new StringWriter();
//    PrintWriter out = new PrintWriter(sout);
//    Throwable isx = new Throwable(); // NOT for throwing...
//    isx.printStackTrace(out); // ...just want the stack trace (but just the first few lines)
//    out.flush();
//    BufferedReader in = new BufferedReader(new StringReader(sout.toString()));
//    try
//    {
//    String line = in.readLine(); // skip first line with meaningless Exception message
//    while ((line = in.readLine()) != null)
//    {
//      if (line.contains("com.axi.v810.hardware.LegacyXraySource."))
//      {
//        if (  (line.contains("sendCommandAndReceiveResponseAsInteger") == false)
//           && (line.contains("standardReadHardwareValue") == false)
//           && (line.contains("standardSetHardwareValue") == false))
//        {
//          caller = line.substring("	at com.axi.v810.hardware.LegacyXraySource.".length());
//          break;
//        }
//      }
//    }
//    }
//    catch (IOException iox)
//    {
//      throw new IllegalStateException(iox);
//    }
//    trace(caller);
//    trace("  called sendAndReceive(" + request + ") at " + new Date());

    // serial port or sim?
    String response = null;
    if (isSimulationModeOn())
    {
      response = simulatedSendAndReceive(request);
    }
    else
    {
      // this version of try 3 times just doesn't make sense, because 9999 responses mean that an
      //  invalid command was sent (the first one maybe, assuming that there was noise or something
      //  that put spurrious bits on the transmit line...); looping on exception catches might make
      //  some sense, but time on actual hardware indicates that even that is not necessary...
//      for (int i = 0; i < 3; i++)
//      {
        configureSerialPortOnce();
        response = serialPortSendAndReceive(request);
//        if (response.equals("9999"))
//        {
//          serialPortReset();
//          continue;
//        }
//        else
//        {
//          break;
//        }
//      }
    }

    // a response of 9999 means that an invalid command was sent: i.e., a syntax error
    Assert.expect(response.equals("9999") == false);

    return response;
  }

  /**
   * if the serial port isn't ready, then configure and reset it
   *
   * @author Greg Loring
   */
  private synchronized void configureSerialPortOnce() throws XraySourceHardwareException
  {
    if (_serialPort == null)
    {
      configureSerialPort();
      serialPortReset();
    }
  }

  /**
   * configure the proper serial port to 9600,8,1,NONE with no flow control
   *
   * @author Greg Loring
   */
  synchronized void configureSerialPort() throws XraySourceHardwareException
  {
    Assert.expect(_serialPort == null);

    // does portName even exist?
    CommPortIdentifier commPortId = null;
    try
    {
      commPortId = CommPortIdentifier.getPortIdentifier(_portName);
    }
    catch (NoSuchPortException nspx)
    {
      throw XraySourceHardwareException.getSerialCommunicationNoSuchPortException(_portName);
    }

    // does portName reference a serial port?
    if (commPortId.getPortType() != CommPortIdentifier.PORT_SERIAL)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }

    // grab the serial port
    CommPort commPort;
    try
    {
      commPort = commPortId.open("LegacyXraySource", 0); // may throw javax.comm.PortInUseException
    }
    catch (PortInUseException piux)
    {
      throw XraySourceHardwareException.getSerialCommunicationPortInUseException(_portName, piux.currentOwner);
    }

    // the javax.comm framing stuff is just not working out very well:
    //  it returns embedded CRs; it returns on a timeout with partial frames.
//    // let the javax.comm stuff do the CR framing
//    try
//    {
//      commPort.enableReceiveFraming(CR);
//      Assert.expect(commPort.isReceiveFramingEnabled());
//      Assert.expect(commPort.getReceiveFramingByte() == CR);
//    }
//    catch (UnsupportedCommOperationException ucox)
//    {
//      throw new IllegalStateException("code port problem?: these features were supported on WIN32");
//    }

    // set the receive timeout in millis
    try
    {
      commPort.enableReceiveTimeout(RECEIVE_TIMEOUT); // ms
    }
    catch (UnsupportedCommOperationException ucox)
    {
      throw XraySourceHardwareException.getSerialCommunicationUnsupportedReceiveTimeoutException(_portName, RECEIVE_TIMEOUT);
    }

    // set the baud rate, etc.
    try
    {
      _serialPort = (SerialPort)commPort;
      _serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
      _serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
    }
    catch (UnsupportedCommOperationException ucox)
    {
      _serialPort = null;
      throw XraySourceHardwareException.getSerialCommunicationUnsupportedParametersException(_portName, 9600, 8, 1, "NONE");
    }
  }

  /**
   * reset the commanding link according to the established protocol
   *
   * NOTE: any serial link that is set to full-duplex and whose far end ignores CRs will
   *  "pass" this reset method (complete without errors); therefore, reset is NOT a good
   *  method for asking "am I talking to what I think I am talking to."
   *
   * @author Greg Loring
   */
  private synchronized void serialPortReset() throws XraySourceHardwareException
  {
    // dump any read-ahead responses
    _responseQueue.clear();

    try
    {
      // do this bit a few times, because 2 CRs didn't seem to be enuf
      for (int i = 0; i < 3; i++)
      {
        // wait for any current activity to end
        while (_serialPort.getInputStream().read() != -1)
        {
          // just dumping unexpected input on the floor until the RECEIVE_TIMEOUT expires
        }

        // send one CR and receive any amount of unexpected input followed by a CR in reply
        _serialPort.getOutputStream().write((byte)CR);
      }
      receive(); // receive() removes the CR; throw the actual input on the floor

      // send another CR and expect just it to come back
      _serialPort.getOutputStream().write((byte) CR);
      String response = receive();
      if (false == response.equals(""))
      {
        throw XraySourceHardwareException.getSerialCommunicationProtocolHardwareException(_portName, "", response);
      }
    }
    catch (IOException iox)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }
  }

  /**
   * send a message (adding CR delimitter) to the serial port, verify the echo,
   *  and receive a message (removing CR delimitter) from the serial port
   *
   * @author Greg Loring
   */
  private synchronized String serialPortSendAndReceive(String request) throws XraySourceHardwareException
  {
    try
    {
      _callTimer.start();
      sendAndVerifyEcho(request);
      String response = receive();
      _callTimer.stop();
      trace(_callTimer);
      return response;
    }
    catch (IOException iox)
    {
      throw XraySourceHardwareException.getSerialCommunicationErrorException(_portName);
    }
  }

  /**
   * send a chunk of text (adding CR delimitter) to the serial port and verify the echo
   *
   * @author Greg Loring
   */
  private synchronized void sendAndVerifyEcho(String request) throws IOException,
                                                                     XraySourceHardwareException
  {
    trace("request", request);

    // assume that any pending input is caused by this class getting out of sync with the hardware:
    // actually, on at least one piece of hardware, each and every time the hardware interlocks are
    //  tripped, the top gun controller puts 8A0,0000<CR>0000 on the RS-232 link, as if it had received
    //  a "set cathode current to zero" command
    flushUnexpectedInput();

    // convert from String to bytes using US-ASCII (do not use the default charset!)
    _serialPort.getOutputStream().write(request.getBytes("US-ASCII"));
    _serialPort.getOutputStream().write((byte) CR);

    String response = receive();

    if (response.equals(request) == false)
    {
      throw XraySourceHardwareException.getSerialCommunicationProtocolHardwareException(_portName, request, response);
    }
  }

  /**
   * get CR framed input message from the serial port; removes the CR delimitter
   *
   * @author Greg Loring
   */
  private synchronized String receive() throws IOException, XraySourceHardwareException
  {
    // likely to get more than one frame (message + CR) in a read, so fill and drain a response queue
    if (_responseQueue.isEmpty())
    {
      // collect input until we have at least one complete frame (accumlated input ends with a CR)
      StringBuffer framesBuffer = new StringBuffer();
      do
      {
        // read with timeout (RECEIVE_TIMEOUT)
        int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
        if (numBytesRead < 1)
        {
          throw XraySourceHardwareException.getSerialCommunicationTimeoutException(_portName);
        }

        // convert from bytes to String using US-ASCII (do not use the default charset!)
        String chunk = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");
        trace("chunk", chunk.replaceAll(CR_RE, "<CR>"));

        framesBuffer.append(chunk);
      }
      while (framesBuffer.charAt(framesBuffer.length() - 1) != CR); // last character not a CR

      String frames = framesBuffer.toString();
      trace("frames", frames.replaceAll(CR_RE, "<CR>"));

      // break the data read in to response frames
      StringBuffer responseBuffer = new StringBuffer();
      for (int i = 0; i < frames.length(); i++)
      {
        char c = frames.charAt(i);
        if (c == CR)
        {
          String response = responseBuffer.toString();
          trace("response", response);

          boolean status = _responseQueue.offer(response);
          Assert.expect(status);

          responseBuffer.delete(0, responseBuffer.length());
        }
        else
        {
          responseBuffer.append(c);
        }
      }
    }
    Assert.expect(_responseQueue.isEmpty() == false);

    String result = _responseQueue.remove();
    trace("result", result);
    return result;
  }

  /**
   * flush whatever data are currently ready for input, printing them to a log for debug purposes
   *
   *   NOTE: some of the ugliest debug logging that I have ever seen!
   *
   * @author Greg Loring
   */
  private synchronized void flushUnexpectedInput() throws IOException
  {
    StringBuffer unexpectedInputBuffer = new StringBuffer();

    while (_responseQueue.isEmpty() == false)
    {
      unexpectedInputBuffer.append("queue=");
      unexpectedInputBuffer.append(_responseQueue.remove());
      unexpectedInputBuffer.append("<CR>");
    }

    int numBytesReady = _serialPort.getInputStream().available();
    if (numBytesReady > 0)
    {
      int numBytesRead = _serialPort.getInputStream().read(_inputBuffer);
      String input = new String(_inputBuffer, 0, numBytesRead, "US-ASCII");
      unexpectedInputBuffer.append("port=");
      unexpectedInputBuffer.append(input.replaceAll(CR_RE, "<CR>"));
      unexpectedInputBuffer.append(';');
    }

    String unexpectedInput = unexpectedInputBuffer.toString();
    if ((unexpectedInput.length() > 0) && (unexpectedInput.startsWith("port=8A0,0000") == false))
    {
      String eol = System.getProperty("line.separator");
      String msg = "unexpected input on " + _portName + ": " + unexpectedInput;

      StringBuffer stackTraceBuffer = new StringBuffer(msg);
      stackTraceBuffer.append(eol);
      StringWriter sout = new StringWriter();
      PrintWriter out = new PrintWriter(sout);
      Throwable isx = new Throwable(); // NOT for throwing...
      isx.printStackTrace(out); // ...just want the stack trace (but just the first few lines)
      out.flush();
      BufferedReader in = new BufferedReader(new StringReader(sout.toString()));
      String line = in.readLine(); // skip first line with meaningless Exception message
      while ((line = in.readLine()) != null)
      {
        if (line.contains("com.axi.v810.hardware.LegacyXraySource"))
        {
          stackTraceBuffer.append(line);
          stackTraceBuffer.append(eol);
        }
        else
        {
          break;
        }
      }
      debug(stackTraceBuffer);
    }
  }

  private void trace(String name, String value)
  {
    // symbol/value trace logging is turned off; to turn it on, uncomment out the next line
    trace(_portName + ": " + name + "=" + value + ';');
  }

  private void trace(Object o)
  {
    // object trace logging is turned off; to turn it on, uncomment out the next line
    debug(o);
  }

  private void debug(Object o)
  {
//    Date date = new Date(System.currentTimeMillis());
//    System.out.println(date.toString() + ":" + o.toString());
  }

  /** @author Greg Loring */
  private synchronized String simulatedSendAndReceive(String request)
  {
    Assert.expect(isSimulationModeOn());
    Assert.expect((request.length() == 3) || (request.length() == 8));

    String command = request.substring(0, 3);
    String response;
    if (request.length() == 3)
    {
      int cmdResult = simulatedRead(command);
      response = Integer.toString(10000 + cmdResult).substring(1);
    }
    else
    {
      Assert.expect(request.indexOf(',') == 3);
      String cmdValueString = request.substring(4);
      int cmdValue = Integer.parseInt(cmdValueString);
      simulatedSet(command, cmdValue);
      response = cmdValueString;
    }

    // the real hardware on a real serial port takes almost exactly .2 seconds
    //  with a very small standard deviation between samples (except for the
    //  first call after the JVM start up which takes a little over 5 seconds)
    // HOWEVER, calling this sleep is breaking regression tests that have
    //  timing dependencies, like Test_HardwareTaskEngine...
    if (_simulateLinkTiming)
      sleep(200);

    return response;
  }

  /** @author Greg Loring */
  private synchronized int simulatedRead(String command) // get resets too!
  {
    Assert.expect(isSimulationModeOn());

    // read and reset status is a special case
    boolean resetStatus = false;
    if (command.equals("9S3"))
    {
      command = "9S2";
      resetStatus = true;
    }

    // reset min/max Anode Voltage is also a special case
    boolean resetMinMaxAnodeVoltage = false;
    if (command.equals("9M0"))
    {
      command = "9I3";
      resetMinMaxAnodeVoltage = true;
    }

    Assert.expect(_simMeasures.containsKey(command));
    int cmdResult = _simMeasures.get(command);

    // handle resets
    if (resetStatus)
      _simMeasures.put("9S2", 0);
    else if (resetMinMaxAnodeVoltage)
    {
      _simMeasures.put("9M1", cmdResult);
      _simMeasures.put("9M2", cmdResult);
    }

    return cmdResult;
  }

  /** @author Greg Loring */
  private synchronized void simulatedSet(String command, int cmdValue)
  {
    Assert.expect(isSimulationModeOn());

    // there are a couple of commands that don't fit the standard set/get paradigm

    if (command.equals("8A0")) // CathodeCurrent
    {
      // CathodeCurrent has a roughly inverse relationship to GridVoltage (GridVoltage is
      //  the "lever" used to move CathodeCurrent; see LegacyXraySource class comments)
      int simGridVoltageCmdValue = 1023 - cmdValue;
      _simMeasures.put("8I2", simGridVoltageCmdValue);


      // TotalCurrent = CathodeCurrent + BleederCurrent + LeakageCurrent;
      //  see class comments for LegacyXraySource; leakage current is ignored here
      double simTotalCurrent = (cmdValue * 200.0 / 1023.0) // calculate unit-ized measure
                               + LegacyXraySource.getExpectedAnodeOnBleederMicroAmps();
      int simTotalCurrentCmdValue = (int) Math.round(simTotalCurrent * 4095.0 / 200.0);
      _simMeasures.put("9I2", simTotalCurrentCmdValue);
    }

    else if (command.equals("8P0")) // FocusVoltage
    {
      // FocusVoltage set and read cmdScales are different
      cmdValue *= 4; // doesn't fill output range, but it is predictable
    }

    // keep track of min/max anode voltage
    if (command.equals("9O0"))
    {
      int min = _simMeasures.get("9M1");
      if (cmdValue < min)
      {
        _simMeasures.put("9M1", cmdValue);
      }

      int max = _simMeasures.get("9M2");
      if (cmdValue > max)
      {
        _simMeasures.put("9M2", cmdValue);
      }
    }

    Assert.expect(_setToReadCommandMap.containsKey(command));
    String readCommand = _setToReadCommandMap.get(command);
    Assert.expect(_simMeasures.containsKey(readCommand));
    _simMeasures.put(readCommand, cmdValue);
  }

  /**
   * for simulating RS-232 communication and hardware processing delays
   *
   * @author Greg Loring
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

}
