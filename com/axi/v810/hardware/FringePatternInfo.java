package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class FringePatternInfo 
{
    private OpticalCameraIdEnum _cameraId;
    private FringePatternEnum _fringePatternEnum;
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePatternInfo(OpticalCameraIdEnum cameraId, FringePatternEnum fringePatternEnum)
    {
        Assert.expect(cameraId != null);
        Assert.expect(fringePatternEnum != null);
        
        _cameraId = cameraId;
        _fringePatternEnum = fringePatternEnum;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public OpticalCameraIdEnum getCameraId()
    {
        Assert.expect(_cameraId != null);
        return _cameraId;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePatternEnum getFringePatternEnum()
    {
        Assert.expect(_fringePatternEnum != null);
        return _fringePatternEnum;
    }
}
