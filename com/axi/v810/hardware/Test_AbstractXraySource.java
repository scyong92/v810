package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * tests that should pass for both the LegacyXraySource and standard XraySource
 *
 * @author Greg Loring
 */
public class Test_AbstractXraySource extends UnitTest
{
  private static final double ANODE_KILO_VOLT_TOLERANCE   = 6.4;
  private static final double CATHODE_MICRO_AMP_TOLERANCE = 3.0;

  private Config _config = Config.getInstance();

  /**
   * @author Greg Loring
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AbstractXraySource());
  }

  /**
   * tests that should pass for both the LegacyXraySource and standard XraySource
   *
   * @author Greg Loring
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      try
      {
        _config.loadIfNecessary();
      }
      catch (DatastoreException de)
      {
        de.printStackTrace();
        Assert.expect(false);
      }

      AbstractXraySource source = AbstractXraySource.getInstance();
      Expect.expect(source != null);
      Expect.expect(source == AbstractXraySource.getInstance());

      // these are the three magic state interrogators
      Expect.expect(source.areXraysOn() == false);
      Expect.expect(source.isStartupRequired());
      Expect.expect(source.isSurveyModeOn() == false);

      source.startup(); // ramps up kV, but leaves cathode current off: same final state as off()
      expectOffState(source);

      source.shutdown(); // just calls powerOff()
      expectPowerOffState(source);

      source.startup(); // on() requires previous startup()
      source.on();
      expectOnState(source);

      source.off();
      expectOffState(source);

      source.powerOff(); // synonymous with shutdown()
      expectPowerOffState(source);

      source.lowPowerOn(); // survey / safety-test mode
      expectLowPowerOnState(source);

      // any state changing method other than lowPowerOn & highPowerOn will clear survey mode
      source.shutdown();
      Expect.expect(source.isSurveyModeOn() == false);

      source.highPowerOn(); // survey / safety-test mode
      expectHighPowerOnState(source);

      // any state changing method other than lowPowerOn & highPowerOn will clear survey mode
      source.startup();
      source.on();
      Expect.expect(source.isSurveyModeOn() == false);

      source.lowPowerOn();
      Expect.expect(source.isSurveyModeOn());
      source.startup();
      Expect.expect(source.isSurveyModeOn() == false);

      source.lowPowerOn();
      Expect.expect(source.isSurveyModeOn());
      source.shutdown();
      Expect.expect(source.isSurveyModeOn() == false);

      source.lowPowerOn();
      Expect.expect(source.isSurveyModeOn());
      source.powerOff();
      Expect.expect(source.isSurveyModeOn() == false);

      // how about on to survey mode, and back again to on, and low to high, and ...?
      source.startup();
      source.on();
      expectOnState(source);
      source.lowPowerOn();
      expectLowPowerOnState(source);
      source.on();
      expectOnState(source);
      source.highPowerOn();
      expectHighPowerOnState(source);
      source.lowPowerOn();
      expectLowPowerOnState(source);
      source.highPowerOn();
      expectHighPowerOnState(source);
      source.on();
      expectOnState(source);

      source.shutdown();
    }
    catch(Exception x)
    {
      x.printStackTrace();
    }
  }

  private void expectOnState(AbstractXraySource source) throws Exception
  {
    Expect.expect(source.areXraysOn());
    Expect.expect(source.isStartupRequired() == false); // on() asserts that startup has been called
    Expect.expect(source.isSurveyModeOn() == false);
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),   160.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(), 100.0, CATHODE_MICRO_AMP_TOLERANCE);
  }

  private void expectOffState(AbstractXraySource source) throws Exception
  {
    Expect.expect(source.areXraysOn() == false); // even though MAY be some X-rays; do NOT image!
    // startup required is not affected by off()
    Expect.expect(source.isSurveyModeOn() == false);
    // anode voltage after off() is not consistent between two tubes
    if (source instanceof LegacyXraySource)
    {
      // With legacy source, off() just shuts the inner barrier.
      fuzzyExpect(source.getAnodeVoltageInKiloVolts(),   160.0, ANODE_KILO_VOLT_TOLERANCE);
      fuzzyExpect(source.getCathodeCurrentInMicroAmps(), 100.0, CATHODE_MICRO_AMP_TOLERANCE);
    }
    else
    {
      fuzzyExpect(source.getCathodeCurrentInMicroAmps(), 0.0, CATHODE_MICRO_AMP_TOLERANCE);
    }
  }

  private void expectPowerOffState(AbstractXraySource source) throws Exception
  {
    Expect.expect(source.areXraysOn() == false);
    // startup required is not affected by powerOff()
    Expect.expect(source.isSurveyModeOn() == false);
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),   0.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(), 0.0, CATHODE_MICRO_AMP_TOLERANCE);
  }

  private void expectLowPowerOnState(AbstractXraySource source) throws Exception
  {
    Expect.expect(source.areXraysOn() == false); // even though some X-rays; do NOT image!
    // startup required is not affected by lowPowerOn()
    Expect.expect(source.isSurveyModeOn());      // NOTICE that this is true
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),   160.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(),  20.0, CATHODE_MICRO_AMP_TOLERANCE);
  }

  private void expectHighPowerOnState(AbstractXraySource source) throws Exception
  {
    Expect.expect(source.areXraysOn() == false); // even though gobs of X-rays; do NOT image!
    // startup required is not affected by highPowerOn()
    Expect.expect(source.isSurveyModeOn());      // NOTICE that this is true
    fuzzyExpect(source.getAnodeVoltageInKiloVolts(),   165.0, ANODE_KILO_VOLT_TOLERANCE);
    fuzzyExpect(source.getCathodeCurrentInMicroAmps(), 125.0, CATHODE_MICRO_AMP_TOLERANCE);
  }

  private void fuzzyExpect(double actual, double expected, double tolerance)
  {
    Expect.expect(MathUtil.fuzzyEquals(actual, expected, tolerance));
  }

}
