package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.Map;

/**
 * @author Cheah Lee Herng
 */
public class PspHardwareModuleVersion1 extends PspHardwareEngine
{
  private OpticalCameraArray _opticalCameraArray;
  private AbstractMicroController _microController;
  private AbstractProjector _projector;
  
  /**
   * @author Cheah Lee Herng
   */
  public PspHardwareModuleVersion1()
  {
    super();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void buildHardwareList() 
  {
    if (_microController == null)
      _microController = AbstractMicroController.getInstance();
    
    if (_opticalCameraArray == null)
      _opticalCameraArray = OpticalCameraArray.getInstance();
    
    if (_projector == null)
      _projector = AbstractProjector.getInstance();
    
    _allHardwareList.add(_opticalCameraArray);
    _allHardwareList.add(_microController);
    _allHardwareList.add(_projector);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOnLED(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _projector.initialize();
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _microController.enableProjectorOneLED();
    else
      _microController.enableProjectorTwoLED();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOffLED(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _microController.disableProjectorOneLED();
    else
      _microController.disableProjectorTwoLED();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayWhiteLight(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _projector.projectWhiteLight(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      _projector.projectWhiteLight(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern1(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _projector.projectFringePatternShiftOne(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      _projector.projectFringePatternShiftOne(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern2(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _projector.projectFringePatternShiftTwo(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      _projector.projectFringePatternShiftTwo(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern3(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _projector.projectFringePatternShiftThree(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      _projector.projectFringePatternShiftThree(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void displayFringePattern4(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _projector.projectFringePatternShiftFour(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    else
      _projector.projectFringePatternShiftFour(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(PspModuleEnum moduleEnum, int current) throws XrayTesterException
  {
    // Do nothing. Does not support for PSP1.
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void sendTriggerSequence(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftNameEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftNameEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      _microController.startProjectorOneSequence();
    else
      _microController.startProjectorTwoSequence();
  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public int getImageRoiCenterX()
//  {
//    return AbstractOpticalCamera.getRoiCenterX();
//  }
//  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public int getImageRoiCenterY()
//  {
//    return AbstractOpticalCamera.getRoiCenterY();
//  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getImageRoiWidth()
  {
    return AbstractOpticalCamera.getRoiWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getImageRoiHeight()
  {
    return AbstractOpticalCamera.getRoiHeight();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void abort() throws XrayTesterException 
  {
    _opticalCameraArray.abort();
    _microController.abort();
    _projector.abort();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOnAllLED() throws XrayTesterException 
  {
    _projector.initialize();
    
    _microController.enableProjectorOneLED();
    _microController.enableProjectorTwoLED();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void turnOffAllLED() throws XrayTesterException 
  {
    _microController.disableProjectorOneLED();
    _microController.disableProjectorTwoLED();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void initializeMicroController(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    _microController.startup();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void initializeProjector(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    _projector.initialize();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void shutdownProjector(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    _projector.shutdown();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void finishDisplayFringePattern() throws XrayTesterException
  {
    _projector.finishDisplayFringePattern();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableOpticalCameraHardwareTrigger(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.enableHardwareTrigger();
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getImageWidth() 
  {
    return AbstractOpticalCamera.getImageWidth();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getImageHeight() 
  {
    return AbstractOpticalCamera.getImageHeight();
  }
}
