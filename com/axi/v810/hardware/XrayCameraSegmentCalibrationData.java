package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraSegmentCalibrationData implements Serializable
{
  private int _segmentNumber;
  private double _forwardGain;
  private double _forwardOffset;
  private double _reverseGain;
  private double _reverseOffset;

  /**
   * @author Greg Esparza
   */
  XrayCameraSegmentCalibrationData(int segmentNumber,
                                   double forwardGain,
                                   double forwardOffset,
                                   double reverseGain,
                                   double reverseOffset)
  {
    Assert.expect(segmentNumber >= 0);
    _segmentNumber = segmentNumber;
    Assert.expect(forwardGain >= 0.0);
    _forwardGain = forwardGain;
    // Offsets can be positive or negative
    _forwardOffset = forwardOffset;
    Assert.expect(reverseGain >= 0.0);
    _reverseGain = reverseGain;
    // Offsets can be positive or negative
    _reverseOffset = reverseOffset;
  }

  /**
   * @author Greg Esparza
   */
  int getSegmentNumber()
  {
    return _segmentNumber;
  }

  /**
   * @author Greg Esparza
   */
  double getForwardGain()
  {
    return _forwardGain;
  }

  /**
   * @author Greg Esparza
   */
  double getForwardOffset()
  {
    return _forwardOffset;
  }

  /**
   * @author Greg Esparza
   */
  double getReverseGain()
  {
    return _reverseGain;
  }

  /**
   * @author Greg Esparza
   */
  double getReverseOffset()
  {
    return _reverseOffset;
  }
}
