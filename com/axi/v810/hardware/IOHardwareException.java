package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If a IOException occurs, then throw this exception.
 *
 * @author Bob Balliew
 */
class IOHardwareException extends HardwareException
{
  /**
   * This is for IOExceptions.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Bob Balliew
   */
  public IOHardwareException(String portName)
  {
    super(new LocalizedString("HW_IO_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }

  /**
   * This is for IOExceptions with a detailed message.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param detailedMessage is the (non null) string returned by 'getMessage()'
   * @author Bob Balliew
   */
  public IOHardwareException(String portName, String detailedMessage)
  {
    super(new LocalizedString("HW_IO_WITH_MESSAGE_EXCEPTION_KEY",
                              new Object[]{portName, detailedMessage}));

    Assert.expect(portName != null);
    Assert.expect(detailedMessage != null);
  }
}
