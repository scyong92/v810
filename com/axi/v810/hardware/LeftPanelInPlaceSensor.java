package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class handles all functions of the left panel in place sensor.
 * @author Rex Shang
 */
public class LeftPanelInPlaceSensor extends PanelInPlaceSensor
{
  private static LeftPanelInPlaceSensor _instance = null;
  private WaitForHardwareCondition _waitForHardwareCondition;
  private static boolean _WAIT_NOT_ABORTABLE = false;

  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Rex Shang
   */
  private LeftPanelInPlaceSensor()
  {
    super();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized LeftPanelInPlaceSensor getInstance()
  {
    if (_instance == null)
      _instance = new LeftPanelInPlaceSensor();

    return _instance;
  }

  /**
   * Extend the sensor to the position where it can sense if the panel is in place or not.
   * @author Rex Shang
   */
  public void extend() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LeftPanelInPlaceSensorEventEnum.SENSOR_EXTEND);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.extendLeftPanelInPlaceSensor();

      //XXL Based Anthony Jan 2013
      if((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && isSimulationModeOn() == false)
      {
          waitUntilPIPExtended();
                    
          // Check current state.
          if (isExtended() == false || isRetracted())
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToExtendLeftPanelInPlaceSensorException());
          } 
      }
      else
      {
          // Wait for sensor to extend.
          _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getPanelInPlaceSensorExtendingTimeInMilliSeconds());
          
          // Check current state.
          if (isExtended() == false)
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToExtendLeftPanelInPlaceSensorException());
          }      
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, LeftPanelInPlaceSensorEventEnum.SENSOR_EXTEND);
  }

   //XXL Based Anthony Jan 2013
   /**
   * Wait for PIP to be extended.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long the time in milli seconds that we wait for panel to be
   * in place.  0 to indicate wait forever.
   * @param abortable used to indicate whether the wait can be aborted.
   * @author Anthony Fong
   */
  public void waitUntilPIPExtended() throws XrayTesterException
  {
     _waitForHardwareCondition = new WaitForHardwareCondition(_digitalIo.getPanelInPlaceCylinderTimeOutInMilliSeconds(),
                                                             _PANEL_DEBOUNCE_TIME_IN_MILLI_SECONDS,
                                                             _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        return isExtended();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getFailedToExtendLeftPanelInPlaceSensorException();
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

  //XXL Based Anthony Jan 2013
  /**
   * Wait for PIP to be retracted.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long the time in milli seconds that we wait for panel to be
   * in place.  0 to indicate wait forever.
   * @param abortable used to indicate whether the wait can be aborted.
   * @author Anthony Fong
   */
  public void waitUntilPIPRetracted() throws XrayTesterException
  {
     _waitForHardwareCondition = new WaitForHardwareCondition(_digitalIo.getPanelInPlaceCylinderTimeOutInMilliSeconds(),
                                                             _PANEL_DEBOUNCE_TIME_IN_MILLI_SECONDS,
                                                             _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        return isRetracted();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getFailedToRetractRightPanelInPlaceSensorException();
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
  }

  /**
   * @return true if this sensor is extended.
   * @author Rex Shang
   */
  public boolean isExtended() throws XrayTesterException
  {
    return _digitalIo.isLeftPanelInPlaceSensorExtended();
  }

  //XXL Based Anthony Jan 2013
  /**
   * @return true if this sensor is retracted.
   * @author Anthony Fong
   */
  public boolean isRetracted() throws XrayTesterException
  {
    return _digitalIo.isLeftPanelInPlaceSensorRetracted();
  }

  /**
   * Retract the sensor so it will not get in the way of the panel moving.  The panel cannot
   * be sensed when this sensor is retracted.
   * @author Rex Shang
   */
  public void retract() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LeftPanelInPlaceSensorEventEnum.SENSOR_RETRACT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.retractLeftPanelInPlaceSensor();
      //XXL Based Anthony Jan 2013
      if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
      {
          waitUntilPIPRetracted();

          // Check current state.
          if (isRetracted() == false || isExtended())
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToRetractLeftPanelInPlaceSensorException());
          }
      }
      else
      {
         // Wait for sensor to retract.
         _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getPanelInPlaceSensorRetractingTimeInMilliSeconds());

         // Check current state.
          if (isExtended())
          {
            HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getFailedToRetractLeftPanelInPlaceSensorException());
          }
      }
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, LeftPanelInPlaceSensorEventEnum.SENSOR_RETRACT);
  }

  /**
   * @return true if the sensor says that the panel is in postion
   * @author Rex Shang
   */
  public boolean isPanelInPlace() throws XrayTesterException
  {
    return isPanelInPlace(true);
  }
  
  /**
   * XCR-3360 - Clamp auto close when attempt to open via Panel Handling page (Anthony Fong)
   * @return true if the sensor says that the panel is in postion
   * @author Rex Shang
   */
  public boolean isPanelInPlace(boolean checkWithPanelClampOpened) throws XrayTesterException
  {
    // Swee-Yee.Wong - XCR-3074 Request to unload board when running high mag CDA task although there are no board loaded
    if (checkWithPanelClampOpened && _digitalIo.isOpticalStopControllerInstalled() && _digitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP()==false)
    {
      if (_digitalIo.arePanelClampsClosed())
      {
        if (HardwareWorkerThread.isHardwareWorkerThread() == false)
        {
          HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
          {
            public void run()
            {
              try
              {
                PanelClamps.getInstance().open();
              }
              catch (XrayTesterException xte)
              {
                // do nothing
              }
            }
          });
        }
        else
        {
          PanelClamps.getInstance().open();
        }
        // This delay is used to compensate the respond time of optical stop sensor
        // Minimum 300ms is needed to prevent inaccurate Optical PIP sensor value
        try
        {
          Thread.sleep(300);
        }
        catch (InterruptedException ix)
        {
          // don't expect this, but don't really care how sleep returns either
        }
      }
    }
    return _digitalIo.isLeftPanelInPlaceSensorEngaged();
  }
  
  /**
   * @return the x distance in nanometers from the system fiducial to the left PIP
   * @author Dave Ferguson
   */
  public int getXDistanceFromSystemFiducial()
  {
    return getConfig().getIntValue(HardwareConfigEnum.LEFT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS);
  }

  /**
   * @return the y distance in nanometers from the system fiducial to the left PIP
   * @author Dave Ferguson
   */
  public int getYDistanceFromSystemFiducial()
  {
    return getConfig().getIntValue(HardwareConfigEnum.LEFT_PIP_SENSOR_Y_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS);
  }
  
  /**
   * @author Kee, Chin Seong
   */
  public void checkXraySafetyLevel() throws XrayTesterException
  {
    //Variable Mag Chin Seong Jun 2012
    //Purpose i check right is because the Panel clear sensor is on the 
    //opposite of the machine.
    if (_digitalIo.isXrayRightSafetyLevelOK() == false)
    {
      //Should NOT happen,
      //_primarySafetyLevelSensor should not trigger or Panel Component Height should not Exceed Max Safety Level
      HardwareTaskEngine.getInstance().throwHardwareException(PanelHandlerException.getPanelComponentHeightExceedMaxSafetyLevelException());
    }
  }
}
