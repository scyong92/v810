:: -----------------------------------------------------------------------------
:: (C) Copyright 2010 ViTrox Technologies. All rights reserved
:: -----------------------------------------------------------------------------
:: @author Wei Chin, Chong
:: compile JNI and package it into nativeHardwareUtil.dll
::
@echo off

SET MODE=Debug
IF "%1"=="Debug" set MODE=Debug
IF "%1"=="Release" set MODE=Release

set OLDPATH=%PATH%
set VSTUDIO_DIR=C:/apps/vStudio2003
set VSTUDIO2008_DIR=C:/apps/vStudio2008
set JDK_PATH=C:/apps/java/jdk1.7.0
set WINSDK_PATH=../../../../../thirdParty/microsoft/WINSDK/v7.1
set IPPROOT=C:/apps/Intel/IPP/5.1/ia32
set VCVARS_PATH=%VSTUDIO_DIR%/VC7/bin
set LIB_CLASSPATH=../../../../jre/1.7.0/lib/ext/comm.jar;../../../../jre/1.7.0/lib/ext/j3daudio.jar;../../../../jre/1.7.0/lib/ext/j3dcore.jar;../../../../jre/1.7.0/lib/ext/j3dutils.jar;../../../../jre/1.7.0/lib/ext/vecmath.jar;../../../../jre/1.7.0/lib/ext/Jama-1.0.2.jar;../../../../jre/1.7.0/lib/ext/jcommon-1.0.0.jar;../../../../jre/1.7.0/lib/ext/jfreechart-1.0.1.jar
set INCLUDE_VC=-I%VSTUDIO_DIR%/VC7/include -I"%VSTUDIO_DIR%/VC7/include" -I%JDK_PATH%/include -I%JDK_PATH%/include/win32 -I../../../.. -I../../../../.. -I../../../../../cpp -I../../../../../cpp/util/src32 -I../../../../../cpp/hardware/digitalIo -I../../../../../cpp/hardware/motionControl -I../../../../../thirdParty/boost/boost_1_34_0 -I../../../../../thirdParty/mei/MPI/INCLUDE -I../../../../../thirdParty/mei/xmp/INCLUDE
IF "%MODE%"=="Release" set VC_FLAG=-D_WINDOWS -DWIN32 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -EHsc -O2 -MD
IF "%MODE%"=="Debug" set VC_FLAG=-D_WINDOWS -DWIN32 -D_USRDLL -D_WINDLL -D_CRT_SECURE_NO_DEPRECATE -D_AFXDLL -D_MBCS -DBOOST_THREAD_USE_DLL -D_Debug -EHsc -O2 -MD
set DEBUG_FLAG=-DEBUG
IF "%MODE%"=="Release" set DEBUG_FLAG= 
set OBJ_PATH=../../../../buildFiles/jniMscpp/jniLoadHandler32.obj ../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteDigitalIo.obj ../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteMotionControl.obj ../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteNativeHardwareConfiguration.obj
set PATH=%PATH%;%JDK_PATH%\bin;%VCVARS_PATH%
set TMP=%TEMP%

echo "#### generating JNI header file from com.axi.v810.hardware.RemoteDigitalIo ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.v810.hardware.RemoteDigitalIo
@echo off

echo "#### generating JNI header file from com.axi.v810.hardware.RemoteMotionControl ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.v810.hardware.RemoteMotionControl
@echo off

echo "#### generating JNI header file from com.axi.v810.hardware.RemoteNativeHardwareConfiguration ####"
@echo on
javah  -classpath ../../../..;../../../../classes;%LIB_CLASSPATH% com.axi.v810.hardware.RemoteNativeHardwareConfiguration
@echo off

echo "#### building nativeHardwareUtil.dll ####"
@echo on
%VCVARS_PATH%\cl -c -D_NATIVE_XRAY_TESTER_HARDWARE_DLL %VC_FLAG% %INCLUDE_VC% com_axi_v810_hardware_RemoteDigitalIo.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteDigitalIo.obj

%VCVARS_PATH%\cl -c -D_NATIVE_XRAY_TESTER_HARDWARE_DLL %VC_FLAG% %INCLUDE_VC% com_axi_v810_hardware_RemoteMotionControl.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteMotionControl.obj

%VCVARS_PATH%\cl -c -D_NATIVE_XRAY_TESTER_HARDWARE_DLL %VC_FLAG% %INCLUDE_VC% com_axi_v810_hardware_RemoteNativeHardwareConfiguration.cpp -Fo../../../../buildFiles/jniMscpp/com_axi_v810_hardware_RemoteNativeHardwareConfiguration.obj

%VCVARS_PATH%\link %OBJ_PATH% -ignore:4089 -out:../../../../jniRelease/bin/nativeXrayTesterHardware.dll -LIBPATH:../../../../../cpp/release/bin -LIBPATH:../../../../../thirdParty/boost/boost_1_34_0/lib -LIBPATH:%VSTUDIO_DIR%/VC7/lib -DLL %DEBUG_FLAG% ../../../../../cpp/release/bin/axiUtil32.lib ../../../../../cpp/release/bin/digitalIo.lib ../../../../../cpp/release/bin/motionControl.lib ../../../../../cpp/release/bin/axiHardwareUtil.lib ../../../../jniRelease/bin/nativeAxiUtil32.lib

@echo off
:: clean up environment space
set PATH= %OLDPATH%
set OLDPATH=
set VCVARS_PATH=
set JDK_PATH=
