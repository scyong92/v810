package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Unit test for Digital IO
 * @author Rex Shang
 */
class Test_DigitalIo extends UnitTest
{
  private DigitalIo _digitalIo;
  private static boolean _regressionTest;

  /**
   * @author Rex Shang
   */
  static
  {
    _regressionTest = true;
  }

  /**
   * @author Rex Shang
   */
  private Test_DigitalIo()
  {
    _digitalIo = DigitalIo.getInstance();
  }

  /**
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    boolean doAction = false;
    boolean doInnerBarrier = false;
    boolean openInnerBarrier = true;

    for (int i = 0; i < args.length; i++)
    {
      if (args[i].compareToIgnoreCase("-notRegression") == 0)
      {
        _regressionTest = false;
      }
      else if (args[i].compareToIgnoreCase("-openInnerBarrier") == 0)
      {
        doAction = true;
        doInnerBarrier = true;
        openInnerBarrier = Boolean.parseBoolean(args[++i]);
      }
    }

    Test_DigitalIo test_DigitalIo = new Test_DigitalIo();

    if (doAction == false)
    {
      UnitTest.execute(test_DigitalIo);
    }
    else
    {
      test_DigitalIo.clearSimulationMode();
      if (doInnerBarrier)
      {
        try
        {
          if (openInnerBarrier)
          {
            test_DigitalIo.doOpenInnerBarrier();
          }
          else
            test_DigitalIo.doCloseInnerBarrier();
        }
        catch (Exception xte)
        {
          xte.printStackTrace();
        }
      }
    }
  }

  /**
   * @author Rex Shang
   */
  private void clearSimulationMode()
  {
    try
    {
      _digitalIo.clearSimulationMode();
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
  }

  /**
   * @author Rex Shang
   */
  private void doOpenInnerBarrier() throws Exception
  {
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws Exception
      {
        _digitalIo.setHardwareResetMode(false);
        _digitalIo.startup();
        _digitalIo.openInnerBarrier();
      }
    });
  }

  /**
   * @author Rex Shang
   */
  private void doCloseInnerBarrier() throws Exception
  {
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws Exception
      {
        _digitalIo.setHardwareResetMode(false);
        _digitalIo.startup();
        _digitalIo.closeInnerBarrier();
      }
    });
  }

  /**
   * @author Rex Shang
   */
  private void testGetConfigurationDescription() throws XrayTesterException
  {
    HardwareConfigurationDescriptionInt configDescription = _digitalIo.getConfigurationDescription();
    Expect.expect(configDescription != null);

    LocalizedString localizedString = new LocalizedString(configDescription.getKey(), configDescription.getParameters().toArray());
    String message = StringLocalizer.keyToString(localizedString);
    Assert.expect(message != null);
//      System.out.println(message);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      if (_regressionTest == false)
        _digitalIo.clearSimulationMode();
      _digitalIo.startup();

      _digitalIo.openLeftOuterBarrier();
      Assert.expect(_digitalIo.isLeftOuterBarrierOpen());
      Assert.expect(_digitalIo.isLeftOuterBarrierNotClosed());
      _digitalIo.closeLeftOuterBarrier();
      Assert.expect(_digitalIo.isLeftOuterBarrierOpen() == false);
      Assert.expect(_digitalIo.isLeftOuterBarrierNotClosed() == false);

      _digitalIo.openRightOuterBarrier();
      Assert.expect(_digitalIo.isRightOuterBarrierOpen());
      Assert.expect(_digitalIo.isRightOuterBarrierNotClosed());
      _digitalIo.closeRightOuterBarrier();
      Assert.expect(_digitalIo.isRightOuterBarrierOpen() == false);
      Assert.expect(_digitalIo.isRightOuterBarrierNotClosed() == false);

      _digitalIo.closePanelClamps();
      Assert.expect(_digitalIo.arePanelClampsClosed());
      _digitalIo.openPanelClamps();
      Assert.expect(_digitalIo.arePanelClampsClosed() == false);

      _digitalIo.extendLeftPanelInPlaceSensor();
      Assert.expect(_digitalIo.isLeftPanelInPlaceSensorExtended());
      _digitalIo.retractLeftPanelInPlaceSensor();
      Assert.expect(_digitalIo.isLeftPanelInPlaceSensorExtended() == false);

      _digitalIo.extendRightPanelInPlaceSensor();
      Assert.expect(_digitalIo.isRightPanelInPlaceSensorExtended());
      _digitalIo.retractRightPanelInPlaceSensor();
      Assert.expect(_digitalIo.isRightPanelInPlaceSensorExtended() == false);

      _digitalIo.setStageBeltsDirectionLeftToRight();
      Assert.expect(_digitalIo.areStageBeltsDirectionLeftToRight());
      _digitalIo.turnStageBeltsOn();
      Assert.expect(_digitalIo.areStageBeltsOn());
      _digitalIo.turnStageBeltsOff();
      Assert.expect(_digitalIo.areStageBeltsOn() == false);

      _digitalIo.setStageBeltDirectionRightToLeft();
      Assert.expect(_digitalIo.areStageBeltsDirectionLeftToRight() == false);
      _digitalIo.turnStageBeltsOn();
      Assert.expect(_digitalIo.areStageBeltsOn());
      _digitalIo.turnStageBeltsOff();
      Assert.expect(_digitalIo.areStageBeltsOn() == false);

      _digitalIo.setStageBeltsDirectionLeftToRight();
      Assert.expect(_digitalIo.areStageBeltsDirectionLeftToRight());

      _digitalIo.turnStageViewingLightOn();
      Assert.expect(_digitalIo.isStageViewingLightOn());
      _digitalIo.turnStageViewingLightOff();
      Assert.expect(_digitalIo.isStageViewingLightOn() == false);

      _digitalIo.openInnerBarrier();
      Assert.expect(_digitalIo.isInnerBarrierOpen());
      Assert.expect(_digitalIo.isInnerBarrierNotClosed());
      _digitalIo.closeInnerBarrier();
      Assert.expect(_digitalIo.isInnerBarrierOpen() == false);
      Assert.expect(_digitalIo.isInnerBarrierNotClosed() == false);

      _digitalIo.turnXrayCamerasSyntheticTriggerOn();
      Assert.expect(_digitalIo.areXrayCamerasSyntheticTriggerOn());
      _digitalIo.turnXrayCamerasSyntheticTriggerOff();
      Assert.expect(_digitalIo.areXrayCamerasSyntheticTriggerOn() == false);

      testGetConfigurationDescription();
    }
    catch(XrayTesterException xte) {
      xte.printStackTrace();
    }
  }
}
