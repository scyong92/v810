package com.axi.v810.hardware;

import java.util.*;

import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Creates synthetic triggers that are sent to
 * the x-ray cameras. In normal uses, the x-ray cameras
 * receive triggers from the stage when it's in motion.
 * This tells the camera when to snap images. However, for
 * performing grayscale calibrations, the stage will not
 * be moving, hence it will not create triggers. This is
 * where this class comes in. It will create synthectic
 * triggers to tell the cameras to snap images.
 *
 * @author George A. David
 */
public class CameraTrigger extends HardwareObject implements HardwareStartupShutdownInt
{
  private static CameraTrigger _instance = null;
  private DigitalIo _digitalIo;
  private boolean _initialized;
  private int _numberOfRequiredSampleTriggers;
  private Config _config;

  /**
   * @author George A. David
   */
  private CameraTrigger()
  {
    _digitalIo = DigitalIo.getInstance();
    _initialized = false;
    _config = Config.getInstance();
    
    setNumberOfRequiredSampleTriggers();
  }

  /**
   * @author George A. David
   */
  public static synchronized CameraTrigger getInstance()
  {
    if(_instance == null)
      _instance = new CameraTrigger();

    return _instance;
  }
  
  /**
   * Set the number of required Sample Triggers according to the magnification.
   * The ideal way when changing magnification is to change the stage speed.
   * However, because of the limitation of the stage's maximum speed,
   * there are times when we will need to reduce the Sample Triggers required.
   * Sample Triggers are supposed to be 1000 to 1500.
   * 
   * @author Ngie Xing
   */
  public void setNumberOfRequiredSampleTriggers()
  {
    if(XrayTester.isS2EXEnabled() && XrayTester.isLowMagnification())
    {
      if (Config.getSystemCurrentLowMagnification().contains("M23"))
        _numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS_FOR_M23);
      else
        _numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS);
    }
    else
      _numberOfRequiredSampleTriggers = _config.getIntValue(HardwareConfigEnum.CAMERA_CONTROLLER_NUMBER_OF_REQUIRED_SAMPLE_TRIGGERS);    
  }

  /**
   * @author Greg Esparza
   */
  public int getNumberOfRequiredSampleTriggers()
  {
    return _numberOfRequiredSampleTriggers;
  }

  /**
   * turn on the synthetic triggers
   * @author George A. David
   */
  public void on() throws XrayTesterException
  {
    _digitalIo.turnXrayCamerasSyntheticTriggerOn();
  }

  /**
   * turn off the synthetic triggers
   * @author George A. David
   */
  public void off() throws XrayTesterException
  {
    // Tell Digital IO to do the job.
    _digitalIo.turnXrayCamerasSyntheticTriggerOff();

    // Wait till the input (detector) confirms they are off OR timeout as appropriate.
    long timeOutInMilliSeconds = _digitalIo.getSyntheticTriggersTurnoffTimeInMilliSeconds();
    long deadLineInMilliSeconds = timeOutInMilliSeconds + System.currentTimeMillis();
    while (_digitalIo.areXrayCamerasSyntheticTriggerOn())
    {
      _digitalIo.waitForHardwareInMilliSeconds(_digitalIo.getHardwarePollingIntervalInMilliSeconds());

      // Check to see if we timed out.
      if (timeOutInMilliSeconds != 0 && System.currentTimeMillis() > deadLineInMilliSeconds)
        HardwareTaskEngine.getInstance().throwHardwareException(new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_SYNTHETIC_CAMERA_TRIGGERS_OFF));
    }
  }

  /**
   * @author George A. David
   */
  public void shutdown() throws XrayTesterException
  {

    off();
    _initialized = false;
  }

  /**
   * @author George A. David
   */
  public void startup() throws XrayTesterException
  {
    if (isPoweredOn() == false)
      throw new XrayCameraControllerHardwareException("HW_XRAY_CAMERA_CONTROLLER_NOT_POWERED_ON_KEY", new ArrayList<String>());

    if (areControlCablesConnected() == false)
      throw new XrayCameraControllerHardwareException("HW_XRAY_CAMERA_CONTROLLER_CONTROL_CABLES_NOT_CONNECTED_KEY", new ArrayList<String>());

    on();
    _initialized = true;
  }

  /**
   * @author Greg Esparza
   */
  public boolean isStartupRequired() throws HardwareException
  {
    if (_initialized == false)
      return true;
    else
      return false;
  }

  /**
   * @author Rex Shang
   */
  public boolean isPoweredOn() throws XrayTesterException
  {
    boolean poweredOn = true;

    if (isSimulationModeOn() == false)
      poweredOn = _digitalIo.isXrayCameraControllerPoweredOn();

    return poweredOn;
  }

  /**
   * @author Rex Shang
   */
  public boolean areControlCablesConnected() throws XrayTesterException
  {
    boolean cablesConnected = true;

    if (isSimulationModeOn() == false)
      cablesConnected = _digitalIo.areXrayCameraControlCablesConnected();

    return cablesConnected;
  }
  
  /**
   * added by sheng chuan for temp fix slow stage insufficient problem
   * please remove this afterward
   * @return 
   */
  public void setSampleTriggerCount(int sampleTrigger)
  {
    _numberOfRequiredSampleTriggers = sampleTrigger;
  }
}
