#include "com/axi/util/jniUtil.h"
#include "com_axi_v810_hardware_ImageReconstructionProcessor.h"
#include "cpp/util/src/IrpVersion.h"
#include "cpp/util/src/sptAssert.h"
#include "cpp/util/src/HardwareException.h"

using namespace std;

static string const javaHardwareException = "com/axi/v810/hardware/NativeHardwareException";

/*
 * Class:     com_axi_v810_hardware_ImageReconstructionProcessor
 * Method:    nativeGetImageReconstructionProcessorApplicationVersion
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_ImageReconstructionProcessor_nativeGetImageReconstructionProcessorApplicationVersion(JNIEnv *pEnv, 
                                                                                                                                               jobject javaObject)
{
  try
  {
    jint version = IrpVersion::getVersion();
    return version;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}
