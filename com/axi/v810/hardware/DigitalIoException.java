package com.axi.v810.hardware;

import java.rmi.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * Any hardware (outer barrier, panel clamps...) that uses DigitaIo can
 * throw one of this.
 * @author Rex Shang
 */
public class DigitalIoException extends HardwareException
{
  private boolean _isNativeException = false;

  /**
   * Construct a new exception based on an enum.
   * @author Rex Shang
   */
  private DigitalIoException(DigitalIoExceptionEnum digitalIoExceptionEnum, LocalizedString message)
  {
    super(message);
    Assert.expect(message != null);

    _exceptionType = digitalIoExceptionEnum;
  }

  /**
   * Construct a new exception based on an enum.
   * @author Rex Shang
   */
  private DigitalIoException(DigitalIoExceptionEnum digitalIoExceptionEnum, String key, Object[] parameters)
  {
    this(digitalIoExceptionEnum, new LocalizedString(key, parameters));

    Assert.expect(digitalIoExceptionEnum != null, "digitalIoExceptionEnum is null.");
    Assert.expect(key != null, "key string is null.");

    _exceptionType = digitalIoExceptionEnum;
  }

  /**
   * Construct a new exception based on NativeHardwareException
   * @author Rex Shang
   */
  private DigitalIoException(NativeHardwareException nativeHardwareException)
  {
    super(new LocalizedString(nativeHardwareException.getKey(),
                              nativeHardwareException.getParameters().toArray()));
    initCause(nativeHardwareException);
    _exceptionType = DigitalIoExceptionEnum.NATIVE_HARDWARE_EXCEPTION;
    _isNativeException = true;
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getDigitalIoExceptionBasedOnNativeHardwareException(NativeHardwareException nativeHardwareException)
  {
    return new DigitalIoException(nativeHardwareException);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getLeftOuterBarrierFailedToOpenException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.LEFT_OUTER_BARRIER_FAILED_TO_OPEN,
                                  "HW_LEFT_OUTER_BARRIER_FAILED_TO_OPEN_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getLeftOuterBarrierFailedToCloseException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.LEFT_OUTER_BARRIER_FAILED_TO_CLOSE,
                                  "HW_LEFT_OUTER_BARRIER_FAILED_TO_CLOSE_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  public static DigitalIoException getLeftOuterBarrierIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.LEFT_OUTER_BARRIER_IS_STUCK,
                                  "HW_LEFT_OUTER_BARRIER_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getRightOuterBarrierFailedToOpenException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.RIGHT_OUTER_BARRIER_FAILED_TO_OPEN,
                                  "HW_RIGHT_OUTER_BARRIER_FAILED_TO_OPEN_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getRightOuterBarrierFailedToCloseException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.RIGHT_OUTER_BARRIER_FAILED_TO_CLOSE,
                                  "HW_RIGHT_OUTER_BARRIER_FAILED_TO_CLOSE_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  public static DigitalIoException getRightOuterBarrierIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.RIGHT_OUTER_BARRIER_IS_STUCK,
                                  "HW_RIGHT_OUTER_BARRIER_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getInnerBarrierFailedToOpenException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.INNER_BARRIER_FAILED_TO_OPEN,
                                  "HW_INNER_BARRIER_FAILED_TO_OPEN_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getInnerBarrierFailedToCloseException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.INNER_BARRIER_FAILED_TO_CLOSE,
                                  "HW_INNER_BARRIER_FAILED_TO_CLOSE_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getInnerBarrierIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.INNER_BARRIER_IS_STUCK,
                                  "HW_INNER_BARRIER_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayCylinderFailedToGoUpException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_CYLINDER_FAILED_TO_GO_UP,
                                  "HW_XRAY_CYLINDER_FAILED_TO_UP_EXCEPTION_KEY",
                                  null);
  }
  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayCylinderFailedToGoDownException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_CYLINDER_FAILED_TO_GO_DOWN,
                                  "HW_XRAY_CYLINDER_FAILED_TO_DOWN_EXCEPTION_KEY",
                                  null);
  }
  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayCylinderIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_CYLINDER_IS_STUCK,
                                  "HW_XRAY_CYLINDER_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }


  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayZAxisFailedToGoHomeException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_Z_AXIS_FAILED_TO_GO_HOME,
                                  "HW_XRAY_Z_AXIS_FAILED_TO_GO_HOME_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayZAxisFailedToGoLowMagException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_Z_AXIS_FAILED_TO_GO_LOW_MAG,
                                  "HW_XRAY_Z_AXIS_FAILED_TO_GO_LOW_MAG_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayZAxisFailedToGoHighMagException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_Z_AXIS_FAILED_TO_GO_HIGH_MAG,
                                  "HW_XRAY_Z_AXIS_FAILED_TO_GO_HIGH_MAG_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong
   */
  static DigitalIoException getXrayZAxisIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_Z_AXIS_IS_STUCK,
                                  "HW_XRAY_Z_AXIS_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }

  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  static DigitalIoException getXrayMotorizedHeightLevelSensorFailedToGoHomeException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HOME,
                                  "HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HOME_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  static DigitalIoException getXrayMotorizedHeightLevelSensorFailedToGoLowMagException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_LOW_MAG,
                                  "HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_LOW_MAG_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  static DigitalIoException getXrayMotorizedHeightLevelSensorFailedToGoHighMagException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HIGH_MAG,
                                  "HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_FAILED_TO_GO_HIGH_MAG_EXCEPTION_KEY",
                                  null);
  }
  //S2EX Anthony August 2014
  /**
   * @author Anthony Fong - For XRay Motorized Height Level Safety Sensor
   */
  static DigitalIoException getXrayMotorizedHeightLevelSensorIsStuckException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_IS_STUCK,
                                  "HW_XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_IS_STUCK_EXCEPTION_KEY",
                                  null);
  }
  
  
  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToTurnOnStageBeltsException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_TURN_ON_STAGE_BELTS,
                                  "HW_FAILED_TO_TURN_ON_STAGE_BELTS_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToTurnOffStageBeltsException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_TURN_OFF_STAGE_BELTS,
                                  "HW_FAILED_TO_TURN_OFF_STAGE_BELTS_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToExtendLeftPanelInPlaceSensorException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_EXTEND_LEFT_PANEL_IN_PLACE_SENSOR,
                                  "HW_FAILED_TO_EXTEND_LEFT_PANEL_IN_PLACE_SENSOR_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToRetractLeftPanelInPlaceSensorException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_RETRACT_LEFT_PANEL_IN_PLACE_SENSOR,
                                  "HW_FAILED_TO_RETRACT_LEFT_PANEL_IN_PLACE_SENSOR_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToExtendRightPanelInPlaceSensorException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_EXTEND_RIGHT_PANEL_IN_PLACE_SENSOR,
                                  "HW_FAILED_TO_EXTEND_RIGHT_PANEL_IN_PLACE_SENSOR_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToRetractRightPanelInPlaceSensorException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_RETRACT_RIGHT_PANEL_IN_PLACE_SENSOR,
                                  "HW_FAILED_TO_RETRACT_RIGHT_PANEL_IN_PLACE_SENSOR_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToOpenPanelClampsException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_OPEN_PANEL_CLAMPS,
                                  "HW_FAILED_TO_OPEN_PANEL_CLAMPS_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Rex Shang
   */
  static DigitalIoException getFailedToClosePanelClampsException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_CLOSE_PANEL_CLAMPS,
                                  "HW_FAILED_TO_CLOSE_PANEL_CLAMPS_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @author Siew Yeng
   */
  static DigitalIoException getDigitalIoNotInitializedException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_NOT_INITIALIZED,
                                  "HW_DIGITAL_IO_NOT_INITIALIZED_EXCEPTION_KEY",
                                  null);
  }


  /**
   * XXL Optical Stop Sensor - Anthony (May 2013)
   * @author Anthony Fong
   */
  static DigitalIoException getDigitalIoOpticalStopSensorNotInitializedException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_OPTICAL_STOP_SENSOR_NOT_INITIALIZED,
                                  "HW_DIGITAL_IO_OPTICAL_STOP_SENSOR_NOT_INITIALIZED_EXCEPTION_KEY",
                                  null);
  }
  
    /**
   * @author Yi Fong
   */
  static DigitalIoException getDigitalIoOpticalStopSensorSettingsInvalidException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_OPTICAL_STOP_SENSOR_SETTINGS_INVALID,
                                  "HW_DIGITAL_IO_OPTICAL_STOP_SENSOR_SETTINGS_INVALID_EXCEPTION_KEY",
                                  null);
  }
  
  /**
   * New Optical Stop Sensor - Anthony (2015)
   * @author Anthony Fong
   */
  static DigitalIoException getDigitalIoOpticalStopControllerEndStopperResumedAsNormalPIPNotInitializedException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_OPTICAL_STOP_CONTROLLER_END_STOPPER_RESUMED_AS_NORMAL_PIP_NOT_INITIALIZED,
                                  "HW_DIGITAL_IO_OPTICAL_STOP_CONTROLLER_END_STOPPER_RESUMED_AS_NORMAL_PIP_EXCEPTION_KEY",
                                  null);
  }
   
  
 /**
  * S2EX Based 2015
  * @author Swee Yee Wong
  */
  static DigitalIoException getDigitalIoFailedToSetZAxisPositionBitException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_FAILED_TO_SET_Z_AXIS_POSITION_BIT,
      "HW_DIGITAL_IO_FAILED_TO_SET_Z_AXIS_POSITION_BIT_EXCEPTION_KEY",
      null);
  }
  
  /**
  * @author Swee Yee Wong
  */
  static DigitalIoException getDigitalIoFailedToSetInnerBarrierBitException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.FAILED_TO_SET_INNER_BARRIER_BIT,
      "HW_DIGITAL_IO_FAILED_TO_SET_INNER_BARRIER_BIT_EXCEPTION_KEY",
      null);
  }
  
  /**
   * S2EX Based  Anthony 2014
   * @author Anthony Fong
   */
  static DigitalIoException getDigitalIoXrayZAxisMotorPositionLookupForMagnificationInvalidException()
  {
    return new DigitalIoException(DigitalIoExceptionEnum.DIGITAL_IO_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_MAGNIFICATION_INVALID,
                                  "HW_DIGITAL_IO_DIGITAL_IO_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_MAGNIFICATION_INVALID_EXCEPTION_KEY",
                                  null);
  }

  /**
   * @param re
   * @return
   */
  static DigitalIoException getRMIException(RemoteException re) 
  {
    String[] parameters = new String[1];
    parameters[0] = re.getMessage();
    return new DigitalIoException(DigitalIoExceptionEnum.RMI_EXCEPTION,
        "HW_REMOTE_DIGITAL_IO_REMOTE_EXCEPTION_KEY",
        parameters);
  }

  /**
   * @param re
   * @return
   */
  static DigitalIoException getNotBoundException(NotBoundException re) 
  {
    String[] parameters = new String[1];
    parameters[0] = re.getMessage();
    return new DigitalIoException(DigitalIoExceptionEnum.RMI_EXCEPTION,
        "HW_REMOTE_DIGITAL_IO_NOT_BOUND_EXCEPTION_KEY",
        parameters);
  }

  /**
   * @param re
   * @return
   */
  static DigitalIoException getGeneralException(Exception re)
  {
    String[] parameters = new String[1];
    parameters[0] = re.getMessage();
    return new DigitalIoException(DigitalIoExceptionEnum.RMI_EXCEPTION,
        "HW_REMOTE_DIGITAL_IO_REMOTE_EXCEPTION_KEY",
        parameters);
  }

  
  /**
   * Exceptions from the native side are all motion control exceptions masked
   * as digital io exception.  For building motion control exceptions, there
   * is a general header and footer that is required.  We are overriding this
   * method just to get the header and footer for the exceptions generated from
   * native side and leave the ones from Java side alone.
   * @author Rex Shang
   */
  public String getLocalizedMessage()
  {
    if (_isNativeException)
    {
      String finalMessage = null;
      LocalizedString header = new LocalizedString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY", null);
      finalMessage = StringLocalizer.keyToString(header);
      String mainMessageBody = StringLocalizer.keyToString(getLocalizedString());
      finalMessage += mainMessageBody;
      LocalizedString companyContactInformation = new LocalizedString("GUI_COMPANY_CONTACT_INFORMATION_KEY", null);
      finalMessage += StringLocalizer.keyToString(companyContactInformation);
      return finalMessage;
    }
    else
    {
      String header = StringLocalizer.keyToString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY");
      String messageBody = StringLocalizer.keyToString(getLocalizedString());
      String footer = StringLocalizer.keyToString("HW_DIGITAL_IO_HARDWARE_EXCEPTION_FOOTER_KEY");
      String companyContactInformation = StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY");

      String finalMessage = header + messageBody + footer + companyContactInformation;
      Assert.expect(finalMessage != null, "Excpetion message is null.");
      return finalMessage;
    }
//      return super.getLocalizedMessage();
  }

}
