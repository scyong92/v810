package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;



/**
 * @author Greg Esparza
 */
public class Test_AbstractXrayCamera extends UnitTest
{
  private static final String _EMPTY_STRING = "";
  private AbstractXrayCamera _xRayCamera;
  private Config _config;
  private boolean _debugMode;
  private HardwareWorkerThread _hardwareWorkerThread;
  private String[] _args;

  /**
   * @author Greg Esparza
   */
  Test_AbstractXrayCamera(String[] args)
  {
    _args = args;
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();

    try
    {
      _config = Config.getInstance();
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void getKeyboardCommand(String commandMessage) throws IOException
  {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String keyboardString = "";

    do
    {
      System.out.println();
      System.out.println(commandMessage);

      keyboardString = br.readLine();
      if (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false)
      {
        System.out.println("Invalid key!");
      }
    }
    while (keyboardString.equalsIgnoreCase(_EMPTY_STRING) == false);
 }

  /**
    * @author Greg Esparza
    */
  private void checkArgs()
  {
    if (_args != null)
    {
      try
      {
        setDebugMode();
      }
      catch (HardwareException he)
      {
        System.out.println(he);
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setDebugMode() throws HardwareException
  {
    boolean argFound = false;

    for (int i = 0; i < _args.length; ++i)
    {
      if (_args[i].equalsIgnoreCase("-debugMode"))
      {
        argFound = true;
        _debugMode = true;
        break;
      }
    }

    if (argFound == false)
      _debugMode = false;
  }

  /**
   * @author Greg Esparza
   */
  private void testStartup() throws XrayTesterException
  {
    _xRayCamera.startup();
    List<HardwareConfigurationDescriptionInt> configDescription = _xRayCamera.getConfigurationDescription();
    for (HardwareConfigurationDescriptionInt config : configDescription)
    {
      LocalizedString localizedString = new LocalizedString(config.getKey(), config.getParameters().toArray());
      String message = StringLocalizer.keyToString(localizedString);
      //System.out.println(message);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void waitForCommandToCompleteInMilliseconds(int waitTimeInMilliseconds)
  {
    Assert.expect(waitTimeInMilliseconds >= 0);

    try
    {
      Thread.sleep(waitTimeInMilliseconds);
    }
    catch (InterruptedException ie)
    {
      // do nothing
    }
  }

  /**
   * @author Greg Esparza
   */
  private void testEnableUnitTestMode() throws XrayTesterException
  {
    String baseDirectory = getTestDataDir();
    String testDirectory = baseDirectory + Directory.getXrayCameraDir();
    _xRayCamera.enableUnitTestMode(testDirectory);
  }

  /**
   * @author Greg Esparza
   */
  private void testDisableUnitTestMode() throws XrayTesterException
  {
    _xRayCamera.disableUnitTestMode();
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException, IOException, BadFormatException
        {
          checkArgs();

          if (_debugMode)
            getKeyboardCommand("DEBUG MODE - Press <ENTER> when ready to start debug session ...");

          List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();

          for (AbstractXrayCamera xRayCamera : xRayCameras)
          {
            _xRayCamera = xRayCamera;
            testEnableUnitTestMode();
            testStartup();
            testDisableUnitTestMode();
          }
        }
      });
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    finally
    {
      try
      {
        testDisableUnitTestMode();
      }
      catch (XrayTesterException xte)
      {
        xte.printStackTrace();
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AbstractXrayCamera(args));
  }
}
