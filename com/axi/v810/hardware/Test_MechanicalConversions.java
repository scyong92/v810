package com.axi.v810.hardware;

import java.lang.*;
import java.awt.geom.*;
import java.util.*;
import java.io.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class Test_MechanicalConversions extends UnitTest
{

  /**
   * @author Roy Williams
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      int processorStripNominalWidth  = 10 * 1000000; // 10 Million
      int processorStripNominalHeight = 20 * 1000000; // 20 Million

      // Create a first ProcessorStrip.
      PanelRectangle nominalPanelRectangle0 = new PanelRectangle(
          30, 40,
          processorStripNominalWidth,
          processorStripNominalHeight);
      ProcessorStrip processorStrip0 = new ProcessorStrip(0, nominalPanelRectangle0, 3);
      processorStrip0.setNominalStartInNanometers(0);
      processorStrip0.setNominalStopInNanometers(processorStripNominalWidth);

      // Create a second ProcessorStrip starts at end of first one.
      PanelRectangle nominalPanelRectangle1 = new PanelRectangle(
          nominalPanelRectangle0.getMaxX(),
          nominalPanelRectangle0.getMinY(),
          processorStripNominalWidth,
          processorStripNominalHeight);
      ProcessorStrip processorStrip1 = new ProcessorStrip(1, nominalPanelRectangle1, 3);
      processorStrip1.setNominalStartInNanometers(processorStripNominalWidth);
      processorStrip1.setNominalStopInNanometers(processorStripNominalWidth * 2);

      // Create a third ProcessorStrip starts at end of first one.
      PanelRectangle nominalPanelRectangle2 = new PanelRectangle(
          nominalPanelRectangle1.getMaxX(),
          nominalPanelRectangle1.getMinY(),
          processorStripNominalWidth,
          processorStripNominalHeight);
      ProcessorStrip processorStrip2 = new ProcessorStrip(2, nominalPanelRectangle2, 3);
      processorStrip2.setNominalStartInNanometers(processorStripNominalWidth * 2);
      processorStrip2.setNominalStopInNanometers(processorStripNominalWidth  * 3);

      // Put them in a list.
      List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
      processorStrips.add(processorStrip0);
      processorStrips.add(processorStrip1);
      processorStrips.add(processorStrip2);

      // Combine both PanelRectangles into a single/larger/combined area.
      PanelRectangle panelRectangleToConvert = new PanelRectangle(nominalPanelRectangle0);
      panelRectangleToConvert.add(nominalPanelRectangle1);
      panelRectangleToConvert.add(nominalPanelRectangle2);


      // Get a list of cameras.
      XrayCameraArray cameraArray = XrayCameraArray.getInstance();
      AbstractXrayCamera xRayCamera = cameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

      AffineTransform coreAlignmentMatrix = AffineTransform.getRotateInstance(Math.toRadians(-90), 2000, 3000);
      SystemFiducialRectangle sfr = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
          panelRectangleToConvert,
          coreAlignmentMatrix);

      AffineTransform manualAlignmentMatrix = AffineTransform.getRotateInstance(Math.toRadians(-90), 2000, 3000);
      MechanicalConversions mechanicalConversions =
          new MechanicalConversions(processorStrips,
                                    manualAlignmentMatrix,
                                    (int)ProcessorStrip.ALIGNMENT_VARIATION_BUFFER_IN_NANOMETERS,
                                    (int)ProcessorStrip.OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS,
                                    XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers(),
                                    Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()),
                                    cameraArray.getCameras(),
                                    sfr);

      // Run through the calls.
      int linesToSkipDuringNegativeYScan = mechanicalConversions.getNumberOfLinesToSkipDuringNegativeYScan(xRayCamera);
      int linesToSkipDuringPositiveYScan = mechanicalConversions.getNumberOfLinesToSkipDuringPositiveYScan(xRayCamera);

      // Compare SystemFiducialRectangle vs PanelRectangle for processorStrip0.
      SystemFiducialRectangle processorStripZeroSfr = mechanicalConversions.systemFiducicalRectangleForProcessorStrip(processorStrip0);
      Assert.expect(MathUtil.fuzzyEquals(processorStripZeroSfr.getWidth(), processorStrip0.getRegion().getHeight(), 1));
      Assert.expect(MathUtil.fuzzyEquals(processorStripZeroSfr.getHeight(), processorStrip0.getRegion().getWidth(), 1));

      int ps0Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip0);
      int ps0End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip0, xRayCamera);
      Assert.expect(ps0Start == 0);
      Assert.expect(ps0End   == 1104);

      // Compare SystemFiducialRectangle vs PanelRectangle for processorStrip1.
      SystemFiducialRectangle processorStripOneSfr = mechanicalConversions.systemFiducicalRectangleForProcessorStrip(processorStrip1);
      Assert.expect(MathUtil.fuzzyEquals(processorStripOneSfr.getWidth(), processorStrip1.getRegion().getHeight(), 1));
      Assert.expect(MathUtil.fuzzyEquals(processorStripOneSfr.getHeight(), processorStrip1.getRegion().getWidth(), 1));

      int ps1Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip1);
      int ps1End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip1, xRayCamera);
      Assert.expect(ps1Start == 307);
      Assert.expect(ps1End   == 1729);

      // Compare SystemFiducialRectangle vs PanelRectangle for processorStrip2.
      SystemFiducialRectangle processorStripTwoSfr = mechanicalConversions.systemFiducicalRectangleForProcessorStrip(processorStrip2);
      Assert.expect(MathUtil.fuzzyEquals(processorStripTwoSfr.getWidth(), processorStrip2.getRegion().getHeight(), 1));
      Assert.expect(MathUtil.fuzzyEquals(processorStripTwoSfr.getHeight(), processorStrip2.getRegion().getWidth(), 1));

      int ps2Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip2);
      int ps2End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip2, xRayCamera);
      Assert.expect(ps2Start == 932);
      Assert.expect(ps2End   == 2036);

      /**************************************************************************/

      manualAlignmentMatrix = AffineTransform.getRotateInstance(Math.toRadians(0), 2000, 3000);
      mechanicalConversions =
          new MechanicalConversions(processorStrips,
                                    manualAlignmentMatrix,
                                    (int)ProcessorStrip.ALIGNMENT_VARIATION_BUFFER_IN_NANOMETERS,
                                    (int)ProcessorStrip.OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS,
                                    XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers(),
                                    Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()),
                                    cameraArray.getCameras(),
                                    sfr);

      ps0Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip0);
      ps0End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip0, xRayCamera);
      Assert.expect(ps0Start == 0);
      Assert.expect(ps0End   == 1411);

      // Compare SystemFiducialRectangle vs PanelRectangle for processorStrip1.
      processorStripOneSfr = mechanicalConversions.systemFiducicalRectangleForProcessorStrip(processorStrip1);
      Assert.expect(MathUtil.fuzzyEquals(processorStripOneSfr.getWidth(), processorStrip1.getRegion().getWidth(), 1));
      Assert.expect(MathUtil.fuzzyEquals(processorStripOneSfr.getHeight(), processorStrip1.getRegion().getHeight(), 1));

      ps1Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip1);
      ps1End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip1, xRayCamera);
      Assert.expect(ps1Start == 0);
      Assert.expect(ps1End   == 1411);

      // Compare SystemFiducialRectangle vs PanelRectangle for processorStrip2.
      processorStripTwoSfr = mechanicalConversions.systemFiducicalRectangleForProcessorStrip(processorStrip2);
      Assert.expect(MathUtil.fuzzyEquals(processorStripTwoSfr.getWidth(), processorStrip2.getRegion().getWidth(), 1));
      Assert.expect(MathUtil.fuzzyEquals(processorStripTwoSfr.getHeight(), processorStrip2.getRegion().getHeight(), 1));

      ps2Start = mechanicalConversions.getProcessorStripStartInPixels(processorStrip2);
      ps2End   = mechanicalConversions.getProcessorStripEndInPixels(processorStrip2, xRayCamera);
      Assert.expect(ps2Start == 0);
      Assert.expect(ps2End   == 1411);


    }
    catch (XrayTesterException ex)
    {
      Assert.logException(ex);
    }
  }

   /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_MechanicalConversions() );
  }
}
