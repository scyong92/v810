package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class ImageReconstructionEngineEnum extends com.axi.util.Enum
{
  private static List<ImageReconstructionEngineEnum> _listOfEnums = new ArrayList<ImageReconstructionEngineEnum>();

  private static int _index = -1;

  public static ImageReconstructionEngineEnum IRE0 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE1 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE2 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE3 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE4 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE5 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE6 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE7 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE8 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE9 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE10 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE11 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE12 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE13 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE14 = new ImageReconstructionEngineEnum(++_index);
  public static ImageReconstructionEngineEnum IRE15 = new ImageReconstructionEngineEnum(++_index);

  /**
   * @author Rex Shang
   */
  private ImageReconstructionEngineEnum(int id)
  {
    super(id);

    _listOfEnums.add(this);
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return "" + super.getId();
  }

  /**
   * @author Rex Shang
   */
  public static List<ImageReconstructionEngineEnum> getEnumList()
  {
    return _listOfEnums;
  }

  /**
   * This method should not be used.
   * @author Rex Shang
   */
  public int getId()
  {
//    Assert.expect(false, "getId() is used!");
    return super.getId();
  }

}
