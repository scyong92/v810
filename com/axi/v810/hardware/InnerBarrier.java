package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the inner barrier.  The inner barrier is used to block
 * x-ray while loading/unloading panel.  For Genesis II, the x-ray tube is able
 * to turn on/off in an instant and therefore eliminate the need of the inner
 * barrier.
 * @author Rex Shang
 */
public class InnerBarrier extends HardwareObject implements BarrierInt
{
  private static InnerBarrier _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the barrier is open or closed.
  private boolean _barrierIsOpen;
  private boolean _barrierIsNotClosed;

  private static boolean _WAIT_NOT_ABORTABLE = false;

  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Rex Shang
   */
  private InnerBarrier()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized InnerBarrier getInstance()
  {
    if (_instance == null)
      _instance = new InnerBarrier();

    return _instance;
  }

  /**
   * Close the barrier.
   * @author Rex Shang
   */
  public void close() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, InnerBarrierEventEnum.BARRIER_CLOSE);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the close action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();

      if (XrayCylinderActuator.isInstalled() && XrayTester.isS2EXEnabled() == false)
      {
        XrayCylinderActuator xrayCylinder = XrayCylinderActuator.getInstance();
        if (xrayCylinder.isHome()==false)
        {
        //do not call _xraySource.offForServiceMode() here, this will cause infinite looping.
          //AbstractXraySource xraySource = AbstractXraySource.getInstance();
          //Assert.expect(xraySource instanceof HTubeXraySource);
          //HTubeXraySource _xraySource = (HTubeXraySource)xraySource;
          //if(_xraySource.areXraysOn())_xraySource.offForServiceMode();

          xrayCylinder.up(false);
        }
      }
      else if (XrayCPMotorActuator.isInstalled() && XrayTester.isS2EXEnabled() == true)
      {
        XrayCPMotorActuator xrayCPMotorActuator = XrayCPMotorActuator.getInstance();
        if (xrayCPMotorActuator.isHome()==false)
        {
        //do not call _xraySource.offForServiceMode() here, this will cause infinite looping.
          //AbstractXraySource xraySource = AbstractXraySource.getInstance();
          //Assert.expect(xraySource instanceof HTubeXraySource);
          //HTubeXraySource _xraySource = (HTubeXraySource)xraySource;
          //if(_xraySource.areXraysOn())_xraySource.offForServiceMode();

          xrayCPMotorActuator.home(false, false);
        }
      }
      _digitalIo.closeInnerBarrier();
      waitUntilClosed();
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, InnerBarrierEventEnum.BARRIER_CLOSE);
  }

  /**
   * Open the barrier.
   * @author Rex Shang
   */
  public void open() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, InnerBarrierEventEnum.BARRIER_OPEN);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();

      _digitalIo.openInnerBarrier();
      waitUntilOpen();
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, InnerBarrierEventEnum.BARRIER_OPEN);
  }

  /**
   * @return long returns how long last open/close action time.
   * @author Rex Shang
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Rex Shang
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    // XCR-3604 Unit Test Phase 2
    if (UnitTest.unitTesting() && _digitalIo.isStartupRequired())
      _digitalIo.startup();
    
    _barrierIsOpen = _digitalIo.isInnerBarrierOpen();
    _barrierIsNotClosed = _digitalIo.isInnerBarrierNotClosed();
  }

  /**
   * When the barrier is being commanded to open/close, there is a window of time
   * where the barrier is neither open nor closed.
   * @return true if the barrier sensor are giving consistent state information.
   * @author Rex Shang
   */
  private boolean isHardwareSettled() throws XrayTesterException
  {
    boolean isSettled = _barrierIsOpen == _barrierIsNotClosed;
    return isSettled;
  }

  /**
   * @return true if the barrier is for sure open.
   * @throws XrayTesterException exception will be thrown if the barrier is neither
   * open nor closed.
   * @author Rex Shang
   */
  synchronized public boolean isOpen() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the barrier is open.
    updateSensorStatus();

    if (isHardwareSettled() == false)    
    {
      //Sometimes isHardwareSettled() return false while Inner Barrieris is opening or closing
      //Return true if Hardware is Settled within 3 seconds or throw Exception when time up. 
      if (waitForHardwareSettled(3000)==false)
      {
        logHardwareStatus("ERROR - Inner barrier: Inner barrier open and close sensor signal shouldn't be both on or off. Please check the sensor or air pressure.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getInnerBarrierIsStuckException());
      }
    }
    return _barrierIsOpen;
  }

  /**
   * @author Rex Shang
   */
  private void waitUntilOpen() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getInnerBarrierOpenTimeOutInMilliSeconds(),
                                                                   _digitalIo.getInnerBarrierSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean barrierIsOpen = false;

        updateSensorStatus();
        if (isHardwareSettled())
          barrierIsOpen = _barrierIsOpen;

        return barrierIsOpen;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Inner barrier: Inner barrier open and close sensor signal shouldn't be both on or off. Please check the sensor or air pressure.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getInnerBarrierFailedToOpenException();
      }
    };
    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Rex Shang
   */
  private void waitUntilClosed() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getInnerBarrierCloseTimeOutInMilliSeconds(),
                                                                   _digitalIo.getInnerBarrierSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean barrierIsClosed = false;

        updateSensorStatus();
        if (isHardwareSettled())
          //barrierIsClosed = _barrierIsOpen == false; //this may have problem 
          //To prevent inner barrier neither close nor open - Anthony 
          barrierIsClosed = (_barrierIsNotClosed == false);


        return barrierIsClosed;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        logHardwareStatus("ERROR - Inner barrier: Inner barrier open and close sensor signal shouldn't be both on or off. Please check the sensor or air pressure.");
        printXrayTubePositionStatus();
        printInnerBarrierPositionStatus();
        return DigitalIoException.getInnerBarrierFailedToCloseException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
    /**
   * @author Anthony Fong
   */
  private boolean waitForHardwareSettled(long timeToWaitInMilliSeconds) throws XrayTesterException
  {
    boolean doneSignalDetected = false;
    //if (XrayTester.isS2EXEnabled())
    {
      try
      {
        long deadLineInMilliSeconds = timeToWaitInMilliSeconds + System.currentTimeMillis();
        boolean continueCheck = true;
        while (continueCheck && (deadLineInMilliSeconds > System.currentTimeMillis()))
        {
          try
          {
            updateSensorStatus();
            if (isHardwareSettled())
            {
              doneSignalDetected = true;
              continueCheck = false;
            }
            Thread.sleep(0);
          }
          catch (InterruptedException ie)
          {
            // Do nothing.
          }
          finally
          {
          }
        }
//        if (doneSignalDetected == false)
//        {
//          //Inner Barrier Is Stuck!!!
//          HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getInnerBarrierIsStuckException());
//        }
      }
      catch (XrayTesterException xte)
      {
        // Throw the original exception.
        throw xte;
      }
      finally
      {
      }
    }
    return doneSignalDetected;
  }

  /**
   * For Genesis II, the inner barrier is not needed since the x-ray tube can
   * be turned on/off in an instant.
   * @return boolean whether the barrier is installed.
   * @author Rex Shang
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_INNER_BARRIER_INSTALLED);
  }

  /**
   * @author Swee-Yee.Wong
   */
  public void printXrayTubePositionStatus()
  {
    try
    {
      if (XrayCPMotorActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayZAxisHome())
        {
          logHardwareStatus("X-ray tube at home position");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition1())
        {
          logHardwareStatus("X-ray tube at low mag position 1");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition2())
        {
          logHardwareStatus("X-ray tube at low mag position 2");
        }
        if (DigitalIo.getInstance().isXrayZAxisDown())
        {
          logHardwareStatus("X-ray tube at high mag position");
        }
        if (DigitalIo.getInstance().isXrayZAxisHome() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition1() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition2() == false && DigitalIo.getInstance().isXrayZAxisDown() == false)
        {
          logHardwareStatus("ERROR - Inner barrier: X-ray tube position status unknown");
        }
      }
      else if (XrayCylinderActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayCylinderUp())
        {
          logHardwareStatus("X-ray tube at home/low mag position");
        }
        if (DigitalIo.getInstance().isXrayCylinderDown())
        {
          logHardwareStatus("X-ray tube at high mag position 1");
        }
        if (DigitalIo.getInstance().isXrayCylinderUp() == false && DigitalIo.getInstance().isXrayCylinderDown() == false)
        {
          logHardwareStatus("ERROR - Inner barrier: X-ray tube position status unknown");
        }
      }
    }
    catch (XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      logHardwareStatus("ERROR - Inner barrier: Failed to get xray tube position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public void printInnerBarrierPositionStatus()
  {
    try
    {
      if(DigitalIo.getInstance().isInnerBarrierInstalled())
      {
        if (DigitalIo.getInstance().isInnerBarrierOpen())
        {
          logHardwareStatus("Inner barrier is opened");
        }
        if (DigitalIo.getInstance().isInnerBarrierClosed())
        {
          logHardwareStatus("Inner barrier is closed");
        }
        if (DigitalIo.getInstance().isInnerBarrierOpen() == false && DigitalIo.getInstance().isInnerBarrierClosed() == false)
        {
          logHardwareStatus("ERROR - Inner barrier: Inner barrier position status unknown");
        }
      }
    }
    catch(XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      logHardwareStatus("ERROR - Inner barrier: Failed to get inner barrier position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private void logHardwareStatus(String message)
  {
    try
    {
      HardwareStatusLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to log hardwareStatus. \n" + e.getMessage());
    }
  }
}
