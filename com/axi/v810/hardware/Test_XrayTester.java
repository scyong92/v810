package com.axi.v810.hardware;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class test the XrayTester class.
 * @author Roy Williams
 */
public class Test_XrayTester extends UnitTest
{
  private HardwareWorkerThread _hardwareWorkerThread;
  private XrayTester _tester;

  private Test_XrayTester()
  {
    try
    {
      Config config = Config.getInstance();
      config.loadIfNecessary();
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _tester = XrayTester.getInstance();
  }

  /**
   * @author Roy Williams
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_XrayTester());
  }

  /**
   * Simple test to starup and shutdown the XrayTester object.
   *
   * @author Roy Williams
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      testSystemType();

      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException
        {
          _tester.startup();
          _tester.shutdown();
        }
      });
    }
    catch (XrayTesterException e)
    {
      e.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public void testSystemType() throws XrayTesterException
  {
    Config config = Config.getInstance();
    // save the state of hardware available
    boolean isHardwareAvailable = XrayTester.isHardwareAvailable();
    SystemTypeEnum currentSystemType = SystemTypeEnum.getSystemType(config.getStringValue(HardwareConfigEnum.SYSTEM_TYPE));

    try
    {
      // change the config files so we are running on an online workstation
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, true);

      // test that the config files and the XrayTester class are in sync
      Expect.expect(currentSystemType.equals(XrayTester.getSystemType()));

      SystemTypeEnum otherType = null;
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        // XCR-3604 Unit Test Phase 2
        if (systemType.equals(SystemTypeEnum.STANDARD) == false && 
            systemType.equals(currentSystemType) == false)
        {
          otherType = systemType;
          break;
        }
      }

      Expect.expect(otherType.equals(XrayTester.getSystemType()) == false);

      // test that we cannot change the system type when running on hardware
      try
      {
        XrayTester.changeSystemTypeIfNecessary(otherType);
        Expect.expect(false);
      }
      catch (AssertException ex)
      {
        // do nothing this was expected
      }
      Expect.expect(otherType.equals(XrayTester.getSystemType()) == false);

      // change the config files so we are running on an offline workstationg
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false);

      // change the system type and test that we remain in sync
      // test that the config and the XrayTester class stay in sync
      XrayTester.changeSystemTypeIfNecessary(currentSystemType);
      Expect.expect(currentSystemType.equals(XrayTester.getSystemType()));

      // change the system type and test that we remain in sync
      // test that the config and the XrayTester class stay in sync
      XrayTester.changeSystemTypeIfNecessary(otherType);
      Expect.expect(otherType.equals(XrayTester.getSystemType()));

    }
    finally
    {
      // set config values to their original state
      XrayTester.changeSystemTypeIfNecessary(currentSystemType);
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, isHardwareAvailable);
    }
  }
}
