package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;


/**
 * @author Greg Esparza
 */
public class MotionProfileTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<Integer, MotionProfileTypeEnum> _idToEnumMap;

  /**
   * @author Greg Esparza
   */
  static
  {
    _idToEnumMap = new HashMap<Integer, MotionProfileTypeEnum>();
  }

  public static final MotionProfileTypeEnum POINT_TO_POINT = new MotionProfileTypeEnum(++_index);
  public static final MotionProfileTypeEnum SCAN = new MotionProfileTypeEnum(++_index);

  /**
   * @author Greg Esparza
   */
  private MotionProfileTypeEnum(int id)
  {
    super(id);

    _idToEnumMap.put(new Integer(id), this);
  }

  /**
   * @author Greg Esparza
   */
  public static MotionProfileTypeEnum getEnum(int id)
  {
    MotionProfileTypeEnum motionProfileType = _idToEnumMap.get(new Integer(id));
    Assert.expect(motionProfileType != null);

    return motionProfileType;
  }


}
