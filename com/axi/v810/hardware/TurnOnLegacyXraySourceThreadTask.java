package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class TurnOnLegacyXraySourceThreadTask extends ThreadTask<Object>
{
  private LegacyXraySource _xraySource;

  /**
   * @author Rex Shang
   */
  TurnOnLegacyXraySourceThreadTask(LegacyXraySource xraySource)
  {
    super("Open Inner Barrier Thead Task");

    _xraySource = xraySource;
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    _xraySource.ensureXraysAreOn();

    return null;
  }

  /**
   * @author Rex Shang
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
