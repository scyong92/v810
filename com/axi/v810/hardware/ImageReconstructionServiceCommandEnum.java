package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * Commands that are sent to the image reconstruction service
 *
 * @author George A. David
 */
public class ImageReconstructionServiceCommandEnum extends com.axi.util.Enum
{
  public static final ImageReconstructionServiceCommandEnum SHUTDOWN_HARDWARE_COMMAND = new ImageReconstructionServiceCommandEnum(10);
  public static final ImageReconstructionServiceCommandEnum RESTART_HARDWARE_COMMAND = new ImageReconstructionServiceCommandEnum(11);
  public static final ImageReconstructionServiceCommandEnum STOP_PROGRAM_COMMAND = new ImageReconstructionServiceCommandEnum(20);
  public static final ImageReconstructionServiceCommandEnum START_PROGRAM_COMMAND = new ImageReconstructionServiceCommandEnum(30);
  public static final ImageReconstructionServiceCommandEnum GET_PROGRAM_VERSION_COMMAND = new ImageReconstructionServiceCommandEnum(40);
  public static final ImageReconstructionServiceCommandEnum GET_RAM_COMMAND = new ImageReconstructionServiceCommandEnum(50);
  public static final ImageReconstructionServiceCommandEnum IS_PROGRAM_RUNNING_COMMAND = new ImageReconstructionServiceCommandEnum(60);
  public static final ImageReconstructionServiceCommandEnum DISCONNECT_COMMAND = new ImageReconstructionServiceCommandEnum(70);

  /**
   * @author George A. David
   */
  private ImageReconstructionServiceCommandEnum(int id)
  {
    super(id);
  }

  /**
   * @author George A. David
   */
  static int getCommandSizeInBytes()
  {
    return JavaTypeSizeEnum.INT.getSizeInBytes();
  }
}
