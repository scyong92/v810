package com.axi.v810.hardware;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.*;
import java.rmi.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rick
 */
public class RemoteMotionServer
{
  static
  {
//    AssertUtil.setUpAssert(null);
    Assert.setLogFile("remoteMotionControlErrors", Version.getVersion());
  }

  /**
   * @param args
   */
  public static void main(String[] args)
  {
    // log_file name is the argument, if it is not supplied write to System.out
    PrintStream log_file = System.out;
    File v810_jar_file = null; 
    if (args.length > 0)
    {
      try {
        log_file = new PrintStream(args[0]);
      }
      catch (FileNotFoundException e) {
        System.out.println("Could not open log file: " + args[0]);
        log_file = System.out;
      }
    }
    
    if (args.length > 1)
    {
      v810_jar_file = new File(args[1]);
    }

    log_file.println("java.home: " + System.getProperty("java.home", "UNDEFINED"));
    log_file.println("java.version: " + System.getProperty("java.version", "UNDEFINED"));
    log_file.println("java.class.path: " + System.getProperty("java.class.path", "UNDEFINED"));
    log_file.println("java.library.path: " + System.getProperty("java.library.path", "UNDEFINED"));
    log_file.println("java.rmi.server.hostname: " + System.getProperty("java.rmi.server.hostname", "Using IP Address"));
    log_file.println("user.dir: " + System.getProperty("user.dir", "UNDEFINED"));
    log_file.println("user.name: " + System.getProperty("user.name", "UNDEFINED"));
    log_file.println("user.home: " + System.getProperty("user.home", "UNDEFINED"));

    // Verify that the server codebase exists
    try
    {
      URI rmi_server_codebase_uri = new URI(System.getProperty("java.rmi.server.codebase", "UNDEFINED"));
      
      if(v810_jar_file == null)
      {      
        File rmi_server_codebase_file = new File(rmi_server_codebase_uri);
        if (rmi_server_codebase_file.exists())
        {
          log_file.println("java.rmi.server.codebase: "
          + System.getProperty("java.rmi.server.codebase", "UNDEFINED")
          + " exists");
        }
        else
        {
          log_file.println("java.rmi.server.codebase: "
          + System.getProperty("java.rmi.server.codebase", "UNDEFINED")
          + " does not exist");
          System.exit(-1);
        }
      }
      else
      {
        System.setProperty("java.rmi.server.codebase", v810_jar_file.toURI().toString());      
      }
    } 
    catch (URISyntaxException e) {
      e.printStackTrace();
    }

    // Verify that the security policy exists
    try {
      String security_policy_property = System.getProperty("java.security.policy");
      if (security_policy_property != null)
      {
        URI security_policy_uri = new URI(security_policy_property);
        File security_policy_file = new File(security_policy_uri);
        if (security_policy_file.exists())
        {
          log_file.println("java.security.policy: " + security_policy_property + " exists");
        }
        else
        {
          log_file.println("java.security.policy: " + security_policy_property + " does not exist");
          System.exit(-1);
        }
      }
      else
      {
        log_file.println("java.security.policy: " + "was not supplied");
      }
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    }

    try
    {
      System.loadLibrary("nativeXrayTesterHardware");  
    }
    catch(Exception e)
    {
      log_file.println("Exception");
      log_file.println(e.getMessage());
      e.printStackTrace();
    }

    try
    {
      Naming.rebind("RemoteNativeHardwareConfiguration", new RemoteNativeHardwareConfiguration());
    }
    catch (RemoteException e)
    {
      log_file.println("RemoteNativeHardwareConfiguration server RemoteException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    catch (MalformedURLException e)
    {
      log_file.println("RemoteNativeHardwareConfiguration server MalformedURLException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    log_file.println("RemoteNativeHardwareConfiguration Server is ready.");

//    try {
//      log_file.println("Verifying digital IO hardware");
//      RemoteDigitalIo local_rdio = new RemoteDigitalIo();
//      local_rdio.remoteInitialize();
//      int bit_state = local_rdio.remoteGetInputBitState(0);
//      log_file.println("Bit 0 value: " + String.valueOf(bit_state));
//    }
//    catch (RemoteException e1) {
//      log_file.println("RemoteException");
//      log_file.println(e1.getMessage());
//      e1.printStackTrace();
//    }
//    catch (NativeHardwareException e) {
//      log_file.println("NativeHardwareException");
//      log_file.println(e.getMessage());
//      e.printStackTrace();
//    }
    
    
    try
    {
      Naming.rebind("RemoteMotionControl", new RemoteMotionControl());
    }
    catch (RemoteException e)
    {
      log_file.println("RemoteMotionControl server RemoteException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    catch (MalformedURLException e)
    {
      log_file.println("RemoteMotionControl server MalformedURLException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    log_file.println("RemoteMotionControl Server is ready.");

    try
    {
      Naming.rebind("RemoteDigitalIo", new RemoteDigitalIo());
    }
    catch (RemoteException e)
    {
      log_file.println("RemoteDigitalIo server RemoteException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    catch (MalformedURLException e)
    {
      log_file.println("RemoteDigitalIo server MalformedURLException");
      log_file.println(e.getMessage());
      System.exit(-1);
    }
    log_file.println("RemoteDigitalIo Server is ready.");
  }
}
