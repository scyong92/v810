package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This DLP projector class is actually contains a FringePatternObservable instance.
 * This instance is necessary to inform the caller (observer) that we need to project patterns
 * from the GUI caller. This is because FringePattern is parked under com.axi.v810.guiUtil which
 * can only call from GUI layer.
 * 
 * @author Cheah Lee Herng
 */
public class DlpProjector extends AbstractProjector
{
    private static final int _INITIALIZE_TIME_IN_MILLISECONDS = 650;
    
    /**
     * @author Cheah Lee Herng
     */
    protected DlpProjector()
    {
        // Do nothing
    }
        
    /**
     * @author Cheah Lee Herng
     */
    public void startup() throws XrayTesterException
    {
        Assert.expect(_fringePatternObservable != null);

        clearAbort();

        try
        {
            _hardwareObservable.stateChangedBegin(this, ProjectorEventEnum.INITIALIZE);
            _hardwareObservable.setEnabled(false);
            _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.PROJECTOR_INITIALIZE, getStartupTimeInMilliseconds());

            if (isSimulationModeOn() == false)
            {
                // By default, start-up operation does not really care about which camera
                // is used for image capturing. So we default to Camera 1 in FringePatternInfo
                _fringePatternObservable.sendEvent(new FringePatternInfo(OpticalCameraIdEnum.OPTICAL_CAMERA_1, FringePatternEnum.INITIALIZE));
                
                projectWhiteLight(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            }
            _initialized = true;
        }
        finally
        {
          setStartupEndProgressState();
          _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.PROJECTOR_INITIALIZE);
          _hardwareObservable.setEnabled(true);
          _hardwareObservable.stateChangedEnd(this, ProjectorEventEnum.INITIALIZE);
        }        
    }

    /**
     * @author Cheah Lee Herng
     */
    public void shutdown() throws XrayTesterException
    {
        if (isSimulationModeOn() == false)
        {
            // By default, shut-down operation does not really care about which camera
            // is used for image capturing. So we default to Camera 1 in FringePatternInfo
            _fringePatternObservable.sendEvent(new FringePatternInfo(OpticalCameraIdEnum.OPTICAL_CAMERA_1, FringePatternEnum.PROJECT_NO_LIGHT));
        }
        //_initialized = false;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void initialize() throws XrayTesterException
    {
        Assert.expect(_fringePatternObservable != null);
        
        clearAbort();
        
        try
        {
            _hardwareObservable.stateChangedBegin(this, ProjectorEventEnum.INITIALIZE);
            _hardwareObservable.setEnabled(false);
            
            if (isSimulationModeOn() == false)
            {
                // By default, start-up operation does not really care about which camera
                // is used for image capturing. So we default to Camera 1 in FringePatternInfo
                _fringePatternObservable.sendEvent(new FringePatternInfo(OpticalCameraIdEnum.OPTICAL_CAMERA_1, FringePatternEnum.INITIALIZE));
            }
            _initialized = true;
        }
        finally
        {
            _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, ProjectorEventEnum.INITIALIZE);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private int getStartupTimeInMilliseconds()
    {
        int startupTimeInMilliseconds = _INITIALIZE_TIME_IN_MILLISECONDS;
        return startupTimeInMilliseconds;
    }

    /**
     * @author Cheah Lee Herng 
     */
    public void projectWhiteLight(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_WHITE_LIGHT));
            
            _finishDisplayFringePatternLock.setValue(false);
            try
            {
                _finishDisplayFringePatternLock.waitUntilTrue();
            }
            catch(InterruptedException ex)
            {
                // Do nothing
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void projectWhiteLightWithoutBlocking(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_WHITE_LIGHT));
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void projectFringePatternShiftOne(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_ONE));
            
            _finishDisplayFringePatternLock.setValue(false);
            try
            {
                _finishDisplayFringePatternLock.waitUntilTrue();
            }
            catch(InterruptedException ex)
            {
                // Do nothing
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void projectFringePatternShiftTwo(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_TWO));
            
            _finishDisplayFringePatternLock.setValue(false);
            try
            {
                _finishDisplayFringePatternLock.waitUntilTrue();
            }
            catch(InterruptedException ex)
            {
                // Do nothing
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void projectFringePatternShiftThree(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_THREE));
            
            _finishDisplayFringePatternLock.setValue(false);
            try
            {
                _finishDisplayFringePatternLock.waitUntilTrue();
            }
            catch(InterruptedException ex)
            {
                // Do nothing
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void projectFringePatternShiftFour(OpticalCameraIdEnum cameraId) throws XrayTesterException 
    {
        Assert.expect(cameraId != null);
        Assert.expect(_fringePatternObservable != null);
        
        if (isSimulationModeOn() == false)
        {
            _fringePatternObservable.sendEvent(new FringePatternInfo(cameraId, FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_FOUR));
            
            _finishDisplayFringePatternLock.setValue(false);
            try
            {
                _finishDisplayFringePatternLock.waitUntilTrue();
            }
            catch(InterruptedException ex)
            {
                // Do nothing
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public boolean isStartupRequired() throws XrayTesterException
    {
        if (_initialized == false)
          return true;
        else
          return false;        
    }
    
    /**
     * This is triggered by event in FringePattern when complete displaying FringePattern.
     * 
     * @author Cheah Lee Herng
     */
    public void finishDisplayFringePattern()
    {
        _finishDisplayFringePatternLock.setValue(true);
    }
}
