package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class PanelPositionerException extends HardwareException
{
  public PanelPositionerException(PanelPositionerExceptionEnum panelPositionerExceptionEnum)
  {
    super(PanelPositionerExceptionEnum.getLocalizedString(panelPositionerExceptionEnum));
    Assert.expect(panelPositionerExceptionEnum != null);
  }

  /**
   * @author Rex Shang
   */
  public String getLocalizedMessage()
  {
    String finalMessage = null;
    LocalizedString header = new LocalizedString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY", null);
    finalMessage = StringLocalizer.keyToString(header);
    String mainMessageBody = StringLocalizer.keyToString(getLocalizedString());
    finalMessage += mainMessageBody;
    LocalizedString companyContactInformation = new LocalizedString("GUI_COMPANY_CONTACT_INFORMATION_KEY", null);
    finalMessage += StringLocalizer.keyToString(companyContactInformation);

    return finalMessage;
  }
}
