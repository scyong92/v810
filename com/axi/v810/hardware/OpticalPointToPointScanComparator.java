package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalPointToPointScanComparator implements Comparator<OpticalPointToPointScan> 
{
    private boolean _ascending;
    private OpticalPointToPointScanComparatorEnum _opticalPointToPointScanComparatorEnum;
    private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalPointToPointScanComparator(boolean ascending, OpticalPointToPointScanComparatorEnum opticalPointToPointScanComparatorEnum)
    {
        Assert.expect(opticalPointToPointScanComparatorEnum != null);
        
        _ascending = ascending;
        _opticalPointToPointScanComparatorEnum = opticalPointToPointScanComparatorEnum;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalPointToPointScanComparator()
    {
        _ascending = true;
        _opticalPointToPointScanComparatorEnum = OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_X_IN_NANOMETERS;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int compare(OpticalPointToPointScan lhs, OpticalPointToPointScan rhs) 
    {
        Assert.expect( lhs != null );
        Assert.expect( rhs != null );
        
        StagePosition lhsStagePosition = lhs.getPointToPointStagePositionInNanoMeters();
        StagePosition rhsStagePosition = rhs.getPointToPointStagePositionInNanoMeters();
        
        if (_opticalPointToPointScanComparatorEnum.equals(OpticalPointToPointScanComparatorEnum.OPTICAL_CAMERA_POSITION))
        {
          if (lhs.getOpticalCameraIdEnum().getId() < rhs.getOpticalCameraIdEnum().getId())
            return -1;
          else if (lhs.getOpticalCameraIdEnum().getId() > rhs.getOpticalCameraIdEnum().getId())
            return 1;
          else
            return 0;
        }
        else if (_opticalPointToPointScanComparatorEnum.equals(OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_X_IN_NANOMETERS))
        {
            int lhsStagePositionXInNanometers = lhsStagePosition.getXInNanometers();
            int rhsStagePositionXInNanometers = rhsStagePosition.getXInNanometers();
            
            if (_ascending)            
                return _alphaNumericComparator.compare(lhsStagePositionXInNanometers, rhsStagePositionXInNanometers);            
            else
                return _alphaNumericComparator.compare(rhsStagePositionXInNanometers, lhsStagePositionXInNanometers); 
        }
        else if (_opticalPointToPointScanComparatorEnum.equals(OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_Y_IN_NANOMETERS))
        {
            int lhsStagePositionYInNanometers = lhsStagePosition.getYInNanometers();
            int rhsStagePositionYInNanometers = rhsStagePosition.getYInNanometers();
            
            if (_ascending)            
                return _alphaNumericComparator.compare(lhsStagePositionYInNanometers, rhsStagePositionYInNanometers);            
            else
                return _alphaNumericComparator.compare(rhsStagePositionYInNanometers, lhsStagePositionYInNanometers);
        }
        else
        {
            Assert.expect(false, "Invalid OpticalPointToPointScanComparatorEnum.");
            return 0;
        }       
    }
    
}
