package com.axi.v810.hardware;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * Governs the X-ray source hardware.  This abstract base
 * exists because the code has to deal with two significantly
 * different pieces of X-ray source hardware.  It provides
 * <ul>
 *   <li>a factory method (getInstance) which creates the appropriate
 *     concrete implementation object based on values from the
 *     Config,</li>
 *   <li>the common API for use by the X-ray testing application (on, off, powerOff),</li>
 *   <li>a small, common set of hardware value getters and setters, and</li>
 *   <li>the bit of implementation that will be common to both
 *     derived types (startup, shutdown, ...).</li>
 * </ul>
 *
 * @author Greg Loring
 * @author George Loring Booth (added support for HTube, the Hamamatsu L9181-18 X=ray Source)
 */
public abstract class AbstractXraySource extends HardwareObject
    implements Simulatable, HardwareStartupShutdownInt
{
  private static AbstractXraySource _instance; // singleton
  private static XraySourceTypeEnum _xraySourceTypeEnum;
  protected static DigitalIo _digitalIo = null;

  // XraySource classes have "uses" relationships to the following singletons
  //  - these are cached for access effeciency, because singletons are not swapped
  //   out and getInstance() methods are typically synchronized which will add up
  //   to a big performance hit
  protected static Config _config;
  protected static HardwareObservable _hardwareObservable;

  // NOTE: since these singleton reference caches are now static,
  //  watch for dependency loops during class loading
  static
  {
    // guarantee that the singletons on which I depend are in place
    _config = Config.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  protected boolean _startupRequired = true;
  private boolean _inSurveyMode = false; // normal vs. survey/safety-test operation

  /**
   * factory method which creates and stores the appropriate concrete singleton
   *  instance based on information in the Config
   *
   * @author Greg Loring
   * @author George Booth
   */
  public static synchronized AbstractXraySource getInstance()
  {
    _digitalIo = DigitalIo.getInstance();

    if (_instance == null)
    {
      String type = _config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE);
      if (type.equals("legacy"))
      {
        _instance = new LegacyXraySource();
        _xraySourceTypeEnum = XraySourceTypeEnum.LEGACY;
      }
      else if (type.equals("standard"))
      {
        _instance = new XraySource();
        _xraySourceTypeEnum = XraySourceTypeEnum.GENESIS;
      }
      else if (type.equals("htube"))
      {
        _instance = new HTubeXraySource();
        _xraySourceTypeEnum = XraySourceTypeEnum.HTUBE;
      }
      else
      {
        Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      }
    }
    return _instance;
  }

  /**
   * static method to return type of x-ray source
   *
   * @author George Booth
   */
  public static XraySourceTypeEnum getSourceTypeEnum()
  {
    return _xraySourceTypeEnum;
  }

  /**
   * Is the X-ray source hardware producing X-rays suitable for imaging?<p/>
   *
   * This method returns false if the X-ray source is operating in either the
   *  low-power or high-power survey/safety-test modes.
   *
   * @author Greg Loring
   */
  public abstract boolean areXraysOn() throws XrayTesterException;

  /**
   * Get the X-ray source hardware ready for imaging, i.e., start emitting imaging quality X-rays.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see #areXraysOn()
   *
   * @author Greg Loring
   */
  public abstract void on() throws XrayTesterException;

  /**
   * turn on for low-power survey/safety-test mode operation.
   *
   *  NOTE: derived classes MUST call setSurveyMode(true) after X-rays suitable
   *   for low-power survey/safety-test mode are being emitted.
   *
   * @see #isSurveyModeOn()
   *
   * @author Greg Loring
   */
  public abstract void lowPowerOn() throws XrayTesterException;

  /**
   * turn on for high-power survey/safety-test mode operation.

   *  NOTE: derived classes MUST call setSurveyMode(true) after X-rays suitable
   *   for high-power survey/safety-test mode are being emitted.
   *
   * @see #isSurveyModeOn()
   *
   * @author Greg Loring
   */
  public abstract void highPowerOn() throws XrayTesterException;

  /**
   * Stop the X-ray source hardware from emitting imaging quality X-rays.  Use this method
   *  imaging is being stopped for a short time (i.e., the system is not being powered off).<p>
   *
   *   WARNING: Calling this method does NOT guarantee that no X-rays will be emitted from the
   *     X-ray source hardware.  Depending on the particular hardware, X-rays may NOT be off
   *     from a health/safety perspective.  The only guarantee is that any X-rays that may be
   *     emitted will not be adequate for imaging purposes.<p>
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @see LegacyXraySource#off()
   * @see XraySource#off()
   *
   * @author Greg Loring
   */
  public abstract void off() throws XrayTesterException;

  /**
   * Stop the X-ray source hardware from emitting any X-rays and get it ready for all power
   *  to be turned off.
   *
   * Concrete implementations are expected to document anode voltages and cathode currents
   *  and call setSurveyMode(false).
   *
   * @author Greg Loring
   */
  public abstract void powerOff() throws XrayTesterException;

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public abstract String getFirmwareRevisionCode() throws XrayTesterException;

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public abstract double getAnodeVoltageInKiloVolts() throws XrayTesterException;

  /**
   * ...from the hardware
   *
   * @author Greg Loring
   */
  public abstract double getCathodeCurrentInMicroAmps() throws XrayTesterException;

  /**
   * ask the X-ray source hardware if it has experienced any errors<p/>
   *
   *   NOTE: the errors reported have been "latched" by the hardware.  this means the hardware
   *     is remembering them even if the actual condition has been resolved.  after all errors
   *     have been resolved (by hardware service staff, for example), call the related
   *     resetHardwareReportedErrors() method<p/>
   *
   * @see #resetHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public abstract XraySourceHardwareReportedErrorsException getHardwareReportedErrors() throws XrayTesterException;

  /**
   * ask the X-ray source hardware to clear its latched error register<p/>
   *
   * any errors detected by the hardware are "latched."  this means the hardware
   *   is remembering them even if the actual condition has been resolved.
   *   after all errors have been resolved (by hardware service staff, for example),
   *   call the related resetHardwareReportedErrors() method<p/>
   *
   * @see #getHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  public abstract void resetHardwareReportedErrors() throws XrayTesterException;

  /**
   * there is no convienient set of hardware measures that is useful for implementing this
   *  method across all of the rapidly evolving hardware configurations. so, just use this
   *  to ask that startup() get called at least once.
   *
   * @return true if startup() has been called more recently than shutdown(), false otherwise
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#isStartupRequired()
   *
   * @author Greg Loring
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    return _startupRequired;
  }
  
  /**
   * 
   */
  public abstract void setXraySourceParametersForServiceMode(double voltage, double current) throws XrayTesterException;

  /**
   * prepare the X-ray source hardware for imminent use.<p>
   *
   *   WARNING: this command will cause the X-ray source to begin emmitting X-rays<p>
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#startup()
   *
   * @author Greg Loring
   */
  public void startup() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.STARTUP);
    _hardwareObservable.setEnabled(false);
    try
    {
      // this is a little crude, but it is roughly correct for both XraySource and LegacyXraySource;
      //  it could be overridden to shave off a few seconds for LegacyXraySource, but...
      _startupRequired = false; // need to do this now because on() will Assert it
      // Clear the latched hardware error code before continuing.
      resetHardwareReportedErrors();

      // lock front door when x-ray start up
      _digitalIo.turnFrontPanelLeftDoorLockOn();
      _digitalIo.turnFrontPanelRightDoorLockOn();

      on();
      off();
    }
    catch (XrayTesterException xtx)
    {
      _startupRequired = true; // reset when call is not successfull
      throw xtx;
    }
    catch (RuntimeException rx)
    {
      _startupRequired = true; // reset when call is not successfull
      throw rx;
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.STARTUP);
  }

  /**
   * prepare the X-ray source hardware to be powered off<p>
   *
   * @see com.axi.v810.hardware.HardwareStartupShutdownInt#shutdown()
   *
   * @author Greg Loring
   */
  public void shutdown() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, XraySourceEventEnum.SHUTDOWN);
    _hardwareObservable.setEnabled(false);
    try
    {
      _startupRequired = true; // reset flag no matter how much of powerOff() succeeds
      resetHardwareReportedErrors();

      // if user need to unlock front panel, please go to service mode to manually trigger.
      // go to derive class to unlock the front door.
      powerOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XraySourceEventEnum.SHUTDOWN);
  }

  /**
   * are survey/safety-text mode X-rays being produced?<p/>
   *   the survey/safety-test equivalent of areXraysOn()
   *
   * @author Greg Loring
   */
  public boolean isSurveyModeOn()
  {
    return _inSurveyMode;
  }

  /**
   * @author Greg Loring
   */
  protected void setSurveyModeOn(boolean flag)
  {
    _inSurveyMode = flag;
  }

  /**
   * derived classes are expected to call this method whenever they attempt to modify the state
   *  of the hardware<p/>
   *
   *   NOTE: the errors reported have been "latched" by the hardware.  this means the hardware
   *     is remembering them even if the actual condition has been resolved.  after all errors
   *     have been resolved (by hardware service staff, for example), call the related
   *     resetHardwareReportedErrors() method<p/>
   *
   * @throws an XraySourceHardwareReportedErrorsException if the X-ray source hardware
   *  has experienced any errors<p/>
   *
   * @see #getHardwareReportedErrors()
   * @see #resetHardwareReportedErrors()
   *
   * @author Greg Loring
   */
  protected void checkForHardwareReportedErrors() throws XrayTesterException
  {
    XraySourceHardwareReportedErrorsException xshrex = getHardwareReportedErrors();
    if (xshrex != null)
    {
      // Set start up required so user can reset the latched error code through
      // new start up.
      _startupRequired = true;
      throw xshrex;
    }
  }
  
  /**
   * @author Wei Chin
   */
  public boolean isXrayFilterInstalled()
  {
    boolean isInstalled = _config.getBooleanValue(HardwareConfigEnum.XRAY_FILTER_INSTALLED);    
    return isInstalled;
  }

}
