package com.axi.v810.hardware;


import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;


/**
 * @author Eddie Williamson
 */
public class Test_SolderThickness extends UnitTest
{
  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SolderThickness());
  }

  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    float background;
    float deltagrey;
    float thickness;
    float returnedThickness;
    float returnedDeltagrey;
    SolderThickness solderThickness;

    // Using a corner case background value
    String solderThicknessFilePath = FileName.getCalibThicknessTableFullPath("M19", "Gain1", SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER.getFileName(), 0);
    solderThickness = SolderThickness.getInstance(solderThicknessFilePath);
    background = 255.0F;
    deltagrey = 100.9F;
    returnedThickness = solderThickness.getThicknessInMils(background, deltagrey);
    returnedDeltagrey = solderThickness.getDeltaGrayLevel(background, returnedThickness);
    Expect.expect(MathUtil.fuzzyEquals(deltagrey, returnedDeltagrey));


    // Round trip test starting with background and deltagrey
    solderThickness = SolderThickness.getInstance(solderThicknessFilePath);
    background = 210.5F;
    deltagrey = 100.9F;
    returnedThickness = solderThickness.getThicknessInMils(background, deltagrey);
    returnedDeltagrey = solderThickness.getDeltaGrayLevel(background, returnedThickness);
    Expect.expect(MathUtil.fuzzyEquals(deltagrey, returnedDeltagrey));

    // Round trip test starting with background and thickness
    solderThickness = SolderThickness.getInstance(solderThicknessFilePath);
    background = 180.3F;
    thickness = 5.01F;
    returnedDeltagrey = solderThickness.getDeltaGrayLevel(background, thickness);
    returnedThickness = solderThickness.getThicknessInMils(background, returnedDeltagrey);
    Expect.expect(MathUtil.fuzzyEquals(thickness, returnedThickness));

    // More deltagrey should give more thickness if in the middle of the data
    background = 210.0F;
    deltagrey = 50.0F;
    float deltagrey2 = 100.0F;
    returnedThickness = solderThickness.getThicknessInMils(background, deltagrey);
    float returnedThickness2 = solderThickness.getThicknessInMils(background, deltagrey2);
    Expect.expect(returnedThickness2 > returnedThickness);
  }
}
