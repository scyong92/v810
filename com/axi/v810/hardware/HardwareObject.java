package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class provides methods that all Objects that
 * represent hardware must provide.
 *
 * @author Bill Darbie
 * @author Rex Shang
 */
public abstract class HardwareObject implements Simulatable
{
  private static Config _config;
  private static List<HardwareObject> _hardwareObjects;

  private boolean _aborting;
  private boolean _simulationModeOn;

  /**
   * @author Rex Shang
   */
  static
  {
    _config = Config.getInstance();
    // When running application, we don't want to load config since error can't
    // be reported.
    if (UnitTest.unitTesting())
    {
      try
      {
        _config.loadIfNecessary();
      }
      catch (DatastoreException xte)
      {
        Assert.logException(xte);
      }
    }

    _hardwareObjects = new ArrayList<HardwareObject>();

    // Load the nativeXrayTesterHardware.dll now to be sure it is ready when needed
//    if (XrayTester.isHardwareAvailable())
//      System.loadLibrary("nativeXrayTesterHardware");

  }

  /**
   * @author Bill Darbie
   */
  protected HardwareObject()
  {
    _aborting = false;
    _simulationModeOn = false;

    // XCR-3604 Unit Test Phase 2
    if (_hardwareObjects == null)
      _hardwareObjects = new ArrayList<HardwareObject>();

    _hardwareObjects.add(this);
  }

  /**
   * @return all of the preexistent hardware objects.
   * @author Rex Shang
   */
  protected List<HardwareObject> getHardwareObjects()
  {
    return _hardwareObjects;
  }

  /**
   * @return static Config object.
   * @author Rex Shang
   */
  protected static Config getConfig()
  {
    return _config;
  }

  /**
   * Sets abort flag to indicate an abort event is underway.
   * @throws XrayTesterException overriding class might throw exception due
   * to interaction with hardware.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    _aborting = true;
  }

  /**
   * Clears abort flag.
   * @author Rex Shang
   */
  protected void clearAbort()
  {
    _aborting = false;
  }

  /**
   * Check to see if an abort event is underway.
   * @return boolean inidicating an abort event is happening.
   * @author Rex Shang
   */
  public boolean isAborting()
  {
    return (_aborting);
  }

  /**
   * @throws XrayTesterException overriding class might throw exception due
   * to interacting with data store or hardware.
   * @author Rex Shang
   */
  public void setSimulationMode() throws XrayTesterException
  {
    _simulationModeOn = true;
  }

  /**
   * @throws XrayTesterException overriding class might throw exception due
   * to interacting with data store or hardware.
   * @author Rex Shang
   */
  public void clearSimulationMode() throws XrayTesterException
  {
    _simulationModeOn = false;
  }

  /**
   * @return true if current object is or all hardware objects are in simulation mode.
   * @author Rex Shang
   */
  public boolean isSimulationModeOn()
  {
    if (_simulationModeOn || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
      return true;
    else
      return false;
  }

}
