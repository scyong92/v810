package com.axi.v810.hardware;

import java.io.*;
import java.util.HashMap;

import com.axi.util.Assert;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import java.util.List;
import java.util.ArrayList;

/**
 * @author Rex Shang
 */
public class PanelHandlerPanelLoadingModeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static HashMap<String, PanelHandlerPanelLoadingModeEnum> _stringToEnumMap;
  private String _name;

  /**
   * @author Rex Shang
   */
  static
  {
    _stringToEnumMap = new HashMap<String, PanelHandlerPanelLoadingModeEnum>();
  }

  public static final PanelHandlerPanelLoadingModeEnum FLOW_THROUGH = new PanelHandlerPanelLoadingModeEnum(
      ++_index, SoftwareConfigEnum.getPanelHandlerPanelLoadingFlowThroughModeName());

  public static final PanelHandlerPanelLoadingModeEnum PASS_BACK = new PanelHandlerPanelLoadingModeEnum(
      ++_index, SoftwareConfigEnum.getPanelHandlerPanelLoadingPassBackModeName());

  /**
   * @author Rex Shang
   */
  private PanelHandlerPanelLoadingModeEnum(int id, String loadingModeString)
  {
    super(id);

    Assert.expect(loadingModeString != null);
    _name = loadingModeString;

    Assert.expect(_stringToEnumMap.put(loadingModeString, this) == null);
  }

  /**
   * @author Rex Shang
   */
  static PanelHandlerPanelLoadingModeEnum getEnum(String keyString)
  {
    Assert.expect(keyString != null);

    PanelHandlerPanelLoadingModeEnum modeEnum = _stringToEnumMap.get(keyString);

    Assert.expect(modeEnum != null);
    return modeEnum;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static List<PanelHandlerPanelLoadingModeEnum> getAllValues()
  {
    List<PanelHandlerPanelLoadingModeEnum> allValues = new ArrayList<PanelHandlerPanelLoadingModeEnum>();
    allValues.add(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH);
    allValues.add(PanelHandlerPanelLoadingModeEnum.PASS_BACK);
    return allValues;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
