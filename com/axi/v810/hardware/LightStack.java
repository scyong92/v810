package com.axi.v810.hardware;

import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.shopFloorSystem.*;

/**
 * This class controls the LightStack lights.  This includes the buzzer, the
 * red, green and yellow indicator.
 * @author Rex Shang
 */
public class LightStack extends HardwareObject
{
  private static LightStack _lightStack;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;

  private static boolean _SERVICE_MODE_ON = true;
  private static boolean _SERVICE_MODE_OFF = false;
  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();

  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Rex Shang
   */
  private LightStack()
  {
    _lightStack = null;
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized LightStack getInstance()
  {
    if (_lightStack == null)
      _lightStack = new LightStack();

    return _lightStack;
  }

  /**
   * @return true if the buzzer hardware is installed.
   * @author Rex Shang
   */
  public static boolean isBuzzerEnabled()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.LIGHT_STACK_BUZZER_ENABLE);
  }

  /**
   * Invoked from configuration UI to set the buzzer installed flag to true.
   * @author Rex Shang
   */
  public void setBuzzerEnabled() throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.LIGHT_STACK_BUZZER_ENABLE, new Boolean(true));
  }

  /**
   * Invoked from configuration UI to set the buzzer installed flag to false.
   * @author Rex Shang
   */
  public void setBuzzerDisabled() throws XrayTesterException
  {
    getConfig().setValue(SoftwareConfigEnum.LIGHT_STACK_BUZZER_ENABLE, new Boolean(false));
  }

  /**
   * Turn on the red light on the light stack.
   * @author Rex Shang
   */
  public void turnOnRedLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.RED_LIGHT_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackRedIndicatorOn();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.RED_LIGHT_ON);
  }

  /**
   * Turn off the red light on the light stack.
   * @author Rex Shang
   */
  public void turnOffRedLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.RED_LIGHT_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      //Khaw Chek Hau - XCR2654: CAMX Integration
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
      {
        try
        {
          _shopFloorSystem.equipmentErrorCleared();
          _shopFloorSystem.equipmentAlarmCleared();
        }
        catch (DatastoreException ex)
        {
          ex.printStackTrace();
        }
      }
      
      //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT) && _machineUtilizationReportSystem.isFaultExist())
      {
        try
        { 
          _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.MACHINE_ERROR_CLEARED);  
        }
        catch (DatastoreException ex)
        {
          ex.printStackTrace();
        }
      }
      
      //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _vOneMachineStatusMonitoringSystem.isFaultExist())
      {
        try
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.MACHINE_ERROR_CLEARED);
        }
        catch (DatastoreException ex)
        {
          ex.printStackTrace();
        }
      }
      
      _digitalIo.turnLightStackRedIndicatorOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.RED_LIGHT_OFF);
  }

  /**
   * Get the status of the red light on the light stack.
   * @author Rex Shang
   */
  public boolean isRedLightOn() throws XrayTesterException
  {
    return _digitalIo.isLightStackRedIndicatorOn();
  }

  /**
   * Turn on the yellow light on the light stack.
   * @author Rex Shang
   */
  public void turnOnYellowLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.YELLOW_LIGHT_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackYellowIndicatorOn();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.YELLOW_LIGHT_ON);
  }

  /**
   * Turn off the yellow light on the light stack.
   * @author Rex Shang
   */
  public void turnOffYellowLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.YELLOW_LIGHT_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackYellowIndicatorOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.YELLOW_LIGHT_OFF);
  }

  /**
   * Get the status of the yellow light on the light stack.
   * @author Rex Shang
   */
  public boolean isYellowLightOn() throws XrayTesterException
  {
    return _digitalIo.isLightStackYellowIndicatorOn();
  }

  /**
   * Turn on the green light on the light stack.
   * @author Rex Shang
   */
  public void turnOnGreenLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.GREEN_LIGHT_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackGreenIndicatorOn();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.GREEN_LIGHT_ON);
  }

  /**
   * Turn off the green light on the light stack.
   * @author Rex Shang
   */
  public void turnOffGreenLight() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.GREEN_LIGHT_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackGreenIndicatorOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.GREEN_LIGHT_OFF);
  }

  /**
   * Get the status of the green light on the light stack.
   * @author Rex Shang
   */
  public boolean isGreenLightOn() throws XrayTesterException
  {
    return _digitalIo.isLightStackGreenIndicatorOn();
  }

  /**
   * In service mode, even if buzzer is disabled, we need to buzz.
   * @author Rex Shang
   */
  public void trunBuzzerOnForServiceMode() throws XrayTesterException
  {
    turnBuzzerOnInternal(_SERVICE_MODE_ON);
  }

  /**
   * @author Rex Shang
   */
  public void turnBuzzerOn() throws XrayTesterException
  {
    turnBuzzerOnInternal(_SERVICE_MODE_OFF);
  }

  /**
   * Turn on the buzzer on the light stack if it is enabled.
   * @author Rex Shang
   */
  private void turnBuzzerOnInternal(boolean serviceModeOn) throws XrayTesterException
  {
    if (isBuzzerEnabled() == false && serviceModeOn == false)
      return;

    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.BUZZER_ON);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackBuzzerOn();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.BUZZER_ON);
  }

  /**
   * Turn off the buzzer on the light stack if it exists.
   * @author Rex Shang
   */
  public void turnBuzzerOff() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, LightStackEventEnum.BUZZER_OFF);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.turnLightStackBuzzerOff();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, LightStackEventEnum.BUZZER_OFF);
  }

  /**
   * Get the status of the buzzer on the light stack.
   * @author Rex Shang
   */
  public boolean isBuzzerOn() throws XrayTesterException
  {
    return _digitalIo.isLightStackBuzzerOn();
  }

  /**
   * Used by test execution to indicate error state.
   * @author Bill Darbie
   */
  public void errorMode() throws XrayTesterException
  {
    turnOnRedLight();
    turnBuzzerOn();

    turnOffGreenLight();
    turnOffYellowLight();
  }

  /**
   * Used by test execution to indicate standby state.
   * @author Bill Darbie
   */
  public void standyMode() throws XrayTesterException
  {
    turnOnYellowLight();

    turnOffRedLight();
    turnOffGreenLight();
    turnBuzzerOff();
  }

  /**
   * Used by test execution to indicate running state.
   * @author Bill Darbie
   */
  public void hardwareInUseMode() throws XrayTesterException
  {
    // green
    turnOnGreenLight();

    turnOffRedLight();
    turnOffYellowLight();
    turnBuzzerOff();
  }

  /**
   * @author Bill Darbie
   */
  public void turnOffAllLights() throws XrayTesterException
  {
    turnOffGreenLight();
    turnOffRedLight();
    turnOffYellowLight();
    turnBuzzerOff();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    return _digitalIo.isStartupRequired();
  }
}
