package com.axi.v810.hardware;

import java.rmi.*;

/**
 * @author George A. David
 */
public interface RemoteLegacyXraySourceInt extends Remote
{
  /**
   * @author George A. David
   */
  public boolean on() throws RemoteException;

  /**
   * @author George A. David
   */
  public boolean off() throws RemoteException;
}
