package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;


/**
 * Test class for XrayCameraAcquisitionParameters.
 *
 * @author Matt Wharton
 */
public class Test_XrayCameraAcquisitionParameters extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  public Test_XrayCameraAcquisitionParameters()
  {
    // Do nothing...
  }


  /**
   * @author Matt Wharton
   */
  private void testCase1()
  {
    try
    {
      AbstractXrayCamera xRayCamera = null;
      XrayCameraAcquisitionParameters cameraParameters = new XrayCameraAcquisitionParameters(xRayCamera);
      Expect.expect( false );
    }
    catch ( AssertException aex )
    {
      // Do nothing...
    }
  }


  /**
   * @author Matt Wharton
   */
  private void testCase2()
  {
    // test copy constructor...
  }

  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader is, PrintWriter os )
  {
    testCase1();
    testCase2();
    // @todo flesh out some more cases!
  }

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_XrayCameraAcquisitionParameters() );
  }
}
