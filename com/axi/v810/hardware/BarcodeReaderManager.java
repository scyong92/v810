package com.axi.v810.hardware;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import gnu.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class handles control of a set (configuration) of barcode readers .
 * @author Bob Balliew
 */
public class BarcodeReaderManager extends HardwareObject
{
  private static final int BAD_OPEN_TIMEOUT = -1;      // Must not be any possible legal value.
  private static final int BAD_RECEIVE_FRAMING = -1;   // Must not be any possible legal value.
  private static final int BAD_RECEIVE_THRESHOLD = -1; // Must not be any possible legal value.
  private static final int BAD_RECEIVE_TIMEOUT = -1;   // Must not be any possible legal value.
  private static final int BAD_BITS_PER_SECOND = -1;   // Must not be any possible legal value.
  private static final int BAD_DATA_BITS = -1;         // Must not be any possible legal value.
  private static final int BAD_STOP_BITS = -1;         // Must not be any possible legal value.
  private static final int BAD_PARITY = -1;            // Must not be any possible legal value.
  private static final int BAD_FLOW_CONTROL = -1;      // Must not be any possible legal value.
  private static BarcodeReaderManager _barcodeReaderManager = null;
  private Config _config;
  private String _lastPortNameUsed;
  private boolean _lastPortNameUsedWasStale;
  private String _lastApplicationNameUsed;
  private boolean _lastApplicationNameUsedWasStale;
  private int _lastOpenTimeoutInMillisecondsUsed;
  private boolean _lastOpenTimeoutInMillisecondsUsedWasStale;
  private boolean _lastEnableReceiveFramingUsed;
  private int _lastReceiveFramingByteUsed;
  private boolean _lastReceiveFramingByteUsedWasStale;
  private boolean _lastEnableReceiveThresholdUsed;
  private int _lastReceiveThresholdCountUsed;
  private boolean _lastReceiveThresholdCountUsedWasStale;
  private boolean _lastEnableReceiveTimeoutUsed;
  private int _lastReceiveTimeoutInMillisecondsUsed;
  private boolean _lastReceiveTimeoutUsedWasStale;
  private int _lastBitsPerSecondUsed;
  private boolean _lastBitsPerSecondUsedWasStale;
  private int _lastDataBitsUsed;
  private boolean _lastDataBitsUsedWasStale;
  private int _lastStopBitsUsed;
  private boolean _lastStopBitsUsedWasStale;
  private int _lastParityUsed;
  private boolean _lastParityUsedWasStale;
  private int _lastFlowControlUsed;
  private boolean _lastFlowControlUsedWasStale;
  private int _lastOutputBufferSizeUsed;
  private boolean _lastOutputBufferSizeUsedWasStale;
  private int _lastInputBufferSizeUsed;
  private boolean _lastInputBufferSizeUsedWasStale;
  private String _lastCharsetUsed;
  private boolean _lastCharsetUsedWasStale;
  private InputStreamReader _inputSerialStreamReader;
  private OutputStreamWriter _outputSerialStreamWriter;
  private SerialPort _serialPort;
  private BarcodeReader[] _barcodeReaders;

  private ConfigObservable _configObservable;

  // Wei Chin
  static final private int _WAIT_TIME_FOR_BARCODE_BEEN_READ = 3500;
  
  //Siew Yeng
  private String _currentConfig;
  private List<Integer> _scannerIdNeeded = new ArrayList<Integer>();
  private static final String SEPARATOR = ",";

  SerialPortWatcher _serialPortWatcher = null;
  /**
   * The set of methods that start with 'simulationMode' are provided to unit
   * (regression) test the majority of the code without any hardware.
   * The unit test may extablish the exact return for one or more readings.
   * The unit test may force an exception during a trigger or at a specific
   * character read.  The unit test may observe the trigger codes that would be
   * sent to the hardware.
   *
   * If the unit test does NOT set up the simulation mode data the mission mode
   * methods of this class will have default values provided as follows:
   *   all triggers sent are captured (they do not need to be read)
   *   no exception will be thrown during a trigger send
   *   no exception will be thrown during a read
   *   every read will return the string "***EMPTY***"
   *
   * If the unit test attempts to read more trigger codes than were actually
   * send "***NO TRIGGER CODES***" is returned.
   *
   * @author Bob Balliew
   */

  /**
   * Get the current size of the queue of trigger codes captured in hardware
   * simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of trigger codes captured and not yet gotten.
   * @author Bob Balliew
   */
  int simulationModeNumberOfTriggerCodesCaptured(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfTriggerCodesCaptured();
  }

  /**
   * Get (and remove) the next trigger code from the queue of trigger codes
   * captured in hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the next trigger code captured, if none left return "***NO TRIGGER CODES***"
   * @author Bob Balliew
   */
  String simulationModeGetNextTriggerCodeCaptured(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeGetNextTriggerCodeCaptured();
  }

  /**
   * Get the current size of the queue of trigger exceptions be be thrown in
   * hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of trigger codes captured and not yet gotten.
   * @author Bob Balliew
   */
  int simulationModeNumberOfTriggerExceptions(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfTriggerExceptions();
  }

  /**
   * Add another trigger exception to queue of trigger codes captured in hardware
   * simulation mode.
   * @param order specify the barcode reader (scanner)
   * @param exception to be added to the queue of exceptions thrown when
   * sending trigger codess.
   * @author Bob Balliew
   */
  void simulationModeAddTriggerException(int order, IOException exception)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    _barcodeReaders[order].simulationModeAddTriggerException(exception);
  }

  /**
   * Get the current size of the current of reading, those characters that have
   * not yet been read in hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of characters currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadCharactersInCurrentReading(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfUnreadCharactersInCurrentReading();
  }

  /**
   * Add a character (actually an int) to the current reading.
   * @param order specify the barcode reader (scanner)
   * @param nextCharacter is the next character to simulate being sent from the
   * scanner
   * @author Bob Balliew
   */
  void simulationModeAddCharacterToCurrentReading(int order, int nextCharacter)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    _barcodeReaders[order].simulationModeAddCharacterToCurrentReading(nextCharacter);
  }

  /**
   * Get the current size of the queue of readings that have not yet been read
   * in hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of readings currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadReadings(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfUnreadReadings();
  }

  /**
   * Add the current reading to the queue of readings to be returned in hardware
   * simulation mode.  The current reading is build (character by character)
   * using the 'simulationModeAddCharacterToReading()' method.
   * @param order specify the barcode reader (scanner)
   * @author Bob Balliew
   */
  void simulationModeAddReadingToReadings(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    _barcodeReaders[order].simulationModeAddReadingToReadings();
  }

  /**
   * Get the current size of the current of reading exception queue, those
   * exceptions that have not yet been thrown in hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of exceptions currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfUnreadExceptionsInCurrentExceptionQueue();
  }

  /**
   * Add a character (actually an int) to the current reading.
   * @param order specify the barcode reader (scanner)
   * @author Bob Balliew
   */
  void simulationModeAddExceptionToCurrentExceptionQueue(int order, IOException e)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    _barcodeReaders[order].simulationModeAddExceptionToCurrentExceptionQueue(e);
  }

  /**
   * Get the current size of the queue of exception queues that have not yet
   * been processed in hardware simulation mode.
   * @param order specify the barcode reader (scanner)
   * @return the number of readings currently available to be read
   * @author Bob Balliew
   */
  int simulationModeNumberOfUnreadExceptions(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    return _barcodeReaders[order].simulationModeNumberOfUnreadExceptions();
  }

  /**
   * Add the current exception queue to the queue of exception queues to be
   * processed in hardware simulation mode.  The current exception queue is
   * build (one exception for every character of each reading) using the
   * 'simulationModeAddExceptionToCurrentExceptionQueue()' method.
   * @param order specify the barcode reader (scanner)
   * @author Bob Balliew
   */
  void simulationModeAddCurrentExceptionQueueToQueuesOfExceptionQueues(int order)
  {
    Assert.expect(order >=0);
    Assert.expect(order < _barcodeReaders.length);
    Assert.expect(_barcodeReaders[order].getId().getOrder() == order);
    _barcodeReaders[order].simulationModeAddCurrentExceptionQueueToQueusOfExceptionQueues();
  }

  /**
   * Get the barcode reader configuration full path from the configuration name.
   * Tolerate specifiing a configuration filename.
   * @param configName is a the barcode reader configuration name.
   * @return the filename for the specified configuration name.
   * @author Bob Balliew
   */
  static public String getBarcodeReaderConfigFullPath(String configName)
  {
    Assert.expect(configName != null);
    return configName.endsWith(FileName.getConfigFileExtension()) ? configName : FileName.getBarcodeReaderConfigurationFullPath(configName);
  }

  /**
   * Get the barcode reader configuration name from the configuration full path.
   * Tolerate specifiing a configuration name.
   * @param configFullPath is a the barcode reader configuration full path.
   * @return the name for the specified configuration name.
   * @author Bob Balliew
   */
  static private String getBarcodeReaderConfigName(String configFullPath)
  {
    Assert.expect(configFullPath != null);
    int end = configFullPath.lastIndexOf(FileName.getConfigFileExtension());
    int start = configFullPath.lastIndexOf(File.separatorChar) + 1;
    Assert.expect(start >= 0);
    return (end < 0) ? configFullPath.substring(start) : configFullPath.substring(start, end);
  }

  /**
   * @return true if the port name is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isPortNameStale()
  {
    final String portName = getBarcodeReaderSerialPortName();
    Assert.expect(portName != null);
    _lastPortNameUsedWasStale = (_lastPortNameUsed == null) || (! portName.equals(_lastPortNameUsed));
    return _lastPortNameUsedWasStale;
  }

  /**
   * @return true if the port name was stale (changed) on the last initialize
   * attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastPortNameWasStale()
  {
    return _lastPortNameUsedWasStale;
  }

  /**
   * @return the port name used during the last initialize attempt.
   * @author Bob Balliew
   */
  String getLastPortName()
  {
    return _lastPortNameUsed;
  }

  /**
   * @return true if the application name is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isApplicationNameStale()
  {
    final String applicationName = getBarcodeReaderApplicationName();
    Assert.expect(applicationName != null);
    _lastApplicationNameUsedWasStale = (_lastApplicationNameUsed == null) || (! applicationName.equals(_lastApplicationNameUsed));
    return _lastApplicationNameUsedWasStale;
  }

  /**
   * @return true if the application name was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastApplicationNameWasStale()
  {
    return _lastApplicationNameUsedWasStale;
  }

  /**
   * @return the application name used during the last initialize attempt.
   * @author Bob Balliew
   */
  String getLastApplicationName()
  {
    return _lastApplicationNameUsed;
  }

  /**
   * @return true if the open timeout is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isOpenTimeoutStale()
  {
    final int openTimeoutInMilliseconds = getBarcodeReaderOpenTimeoutInMilliseconds();
    Assert.expect(openTimeoutInMilliseconds != BAD_OPEN_TIMEOUT);
    _lastOpenTimeoutInMillisecondsUsedWasStale = false;  // Changes to open timeout NEVER considered stale, since a change to the open time is not relevant if the serial port is already opened.
    return _lastOpenTimeoutInMillisecondsUsedWasStale;
  }

  /**
   * @return true if the open timeout was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastOpenTimeoutWasStale()
  {
    return _lastOpenTimeoutInMillisecondsUsedWasStale;
  }

  /**
   * @return the open timeout (in milliseconds) used during the last initialize
   * attempt.
   * @author Bob Balliew
   */
  int getLastOpenTimeoutInMilliseconds()
  {
    return _lastOpenTimeoutInMillisecondsUsed;
  }

  /**
   * @return true if the receive framing is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isReceiveFramingByteStale()
  {
    final boolean receiveFramingEnabled = getBarcodeReaderEnableReceiveFraming();
    final int receiveFramingByte = getBarcodeReaderReceiveFramingByte();
    Assert.expect(receiveFramingByte != BAD_RECEIVE_FRAMING);
    _lastReceiveFramingByteUsedWasStale = (receiveFramingEnabled != _lastEnableReceiveFramingUsed) ||
                                          (receiveFramingEnabled && (receiveFramingByte != _lastReceiveFramingByteUsed)) ||
                                          (_lastReceiveFramingByteUsed == BAD_RECEIVE_FRAMING);
    return _lastReceiveFramingByteUsedWasStale;
  }

  /**
   * @return true if the framing byte was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastReceiveFramingByteUsedWasStale()
  {
    return _lastReceiveFramingByteUsedWasStale;
  }

  /**
   * @return the recieve framing enabled flag used during the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastEnableReceiveFraming()
  {
    return _lastEnableReceiveFramingUsed;
  }

  /**
   * @return framing byte used during the last initialize attempt (with the
   * receive count enabled).
   * @author Bob Balliew
   */
  int getLastReceiveFramingByte()
  {
    return _lastReceiveFramingByteUsed;
  }

  /**
   * @return true if the receive threshold is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isReceiveThresholdCountStale()
  {
    final boolean receiveThresholdEnabled = getBarcodeReaderEnableReceiveThreshold();
    final int receiveThresholdCount = getBarcodeReaderReceiveThresholdCount();
    Assert.expect(receiveThresholdCount != BAD_RECEIVE_THRESHOLD);
    _lastReceiveThresholdCountUsedWasStale = (receiveThresholdEnabled != _lastEnableReceiveThresholdUsed) ||
                                             (receiveThresholdEnabled && (receiveThresholdCount != _lastReceiveThresholdCountUsed)) ||
                                             (_lastReceiveThresholdCountUsed == BAD_RECEIVE_THRESHOLD);
    return _lastReceiveThresholdCountUsedWasStale;
  }

  /**
   * @return true if the threshold count was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastReceiveThresholdCountUsedWasStale()
  {
    return _lastReceiveThresholdCountUsedWasStale;
  }

  /**
   * @return the recieve threshold enabled flag used during the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastEnableReceiveThreshold()
  {
    return _lastEnableReceiveThresholdUsed;
  }

  /**
   * @return threshold count used during the last initialize attempt (with the
   * receive threshold enabled).
   * @author Bob Balliew
   */
  int getLastReceiveThresholdCount()
  {
    return _lastReceiveThresholdCountUsed;
  }

  /**
   * @return true if the receive timeout is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isReceiveTimeoutStale()
  {
    final boolean receiveTimeoutEnabled = getBarcodeReaderEnableReceiveTimeout();
    final int receiveTimeoutInMilliseconds = getBarcodeReaderReceiveTimeoutInMilliseconds();
    Assert.expect(receiveTimeoutInMilliseconds != BAD_RECEIVE_TIMEOUT);
    _lastReceiveTimeoutUsedWasStale = (receiveTimeoutEnabled != _lastEnableReceiveTimeoutUsed) ||
                                      (receiveTimeoutEnabled && (receiveTimeoutInMilliseconds != _lastReceiveTimeoutInMillisecondsUsed)) ||
                                      (_lastReceiveTimeoutInMillisecondsUsed == BAD_RECEIVE_TIMEOUT);
    return _lastReceiveTimeoutUsedWasStale;
  }

  /**
   * @return true if the threshold count was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastReceiveTimeoutUsedWasStale()
  {
    return _lastReceiveTimeoutUsedWasStale;
  }

  /**
   * @return the recieve timeout enabled flag used during the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastEnableReceiveTimeout()
  {
    return _lastEnableReceiveTimeoutUsed;
  }

  /**
   * @return timeout (is milliseconds) used during the last initialize attempt
   * (with the receive timeout enabled).
   * @author Bob Balliew
   */
  int getLastReceiveTimeoutInMilliseonds()
  {
    return _lastReceiveTimeoutInMillisecondsUsed;
  }

  /**
   * @return true if the bits per second is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isBitsPerSecondStale()
  {
    final int bitsPerSecond = convertBitsPerSecond(getBarcodeReaderRs232BitsPerSecond());
    Assert.expect(bitsPerSecond != BAD_BITS_PER_SECOND);
    _lastBitsPerSecondUsedWasStale = (bitsPerSecond != _lastBitsPerSecondUsed);
    return _lastBitsPerSecondUsedWasStale;
  }

  /**
   * @return true if the bits per second was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastBitsPerSecondUsedWasStale()
  {
    return _lastBitsPerSecondUsedWasStale;
  }

  /**
   * @return bits per second used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastBitsPerSecond()
  {
    return _lastBitsPerSecondUsed;
  }

  /**
   * @return true if the data bits is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isDataBitsStale()
  {
    final int dataBits = convertDataBits(getBarcodeReaderRs232DataBits());
    Assert.expect(dataBits != BAD_DATA_BITS);
    _lastDataBitsUsedWasStale = (dataBits != _lastDataBitsUsed);
    return _lastDataBitsUsedWasStale;
  }

  /**
   * @return true if the data bits was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastDataBitsUsedWasStale()
  {
    return _lastDataBitsUsedWasStale;
  }

  /**
   * @return data bits used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastDataBits()
  {
    return _lastDataBitsUsed;
  }

  /**
   * @return true if the stop bits is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isStopBitsStale()
  {
    final int stopBits = convertStopBits(getBarcodeReaderRs232StopBits());
    Assert.expect(stopBits != BAD_STOP_BITS);
    _lastStopBitsUsedWasStale = (stopBits != _lastStopBitsUsed);
    return _lastStopBitsUsedWasStale;
  }

  /**
   * @return true if the stop bits was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastStopBitsUsedWasStale()
  {
    return _lastStopBitsUsedWasStale;
  }

  /**
   * @return stop bits used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastStopBits()
  {
    return _lastStopBitsUsed;
  }

  /**
   * @return true if the parity is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isParityStale()
  {
    final int parity = convertParity(getBarcodeReaderRs232Parity());
    Assert.expect(parity != BAD_PARITY);
    _lastParityUsedWasStale = (parity != _lastParityUsed);
    return _lastParityUsedWasStale;
  }

  /**
   * @return true if the parity was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastParityUsedWasStale()
  {
    return _lastParityUsedWasStale;
  }

  /**
   * @return parity used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastParity()
  {
    return _lastParityUsed;
  }

  /**
   * @return flow control.
   * @author Bob Balliew
   */
  private int getFlowControl()
  {

    final boolean RTS_CTS_In = getBarcodeReaderRs232RtsCtsFlowControlIn();
    final boolean RTS_CTS_Out = getBarcodeReaderRs232RtsCtsFlowControlOut();
    final boolean XON_XOFF_In = getBarcodeReaderRs232XonXoffFlowControlIn();
    final boolean XON_XOFF_Out = getBarcodeReaderRs232XonXoffFlowControlOut();
    final int flowControl = convertFlowControl(RTS_CTS_In, RTS_CTS_Out, XON_XOFF_In, XON_XOFF_Out);
    Assert.expect(flowControl != BAD_FLOW_CONTROL);
    return flowControl;
  }

  /**
   * @return true if the flow control is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isFlowControlStale()
  {
    _lastFlowControlUsedWasStale = (getFlowControl() != _lastFlowControlUsed);
    return _lastFlowControlUsedWasStale;
  }

  /**
   * @return true if the flow control was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastFlowControlUsedWasStale()
  {
    return _lastFlowControlUsedWasStale;
  }

  /**
   * @return flow control used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastFlowControl()
  {
    return _lastFlowControlUsed;
  }

  /**
   * @return true if the output buffer size is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isOutputBufferSizeStale()
  {
    _lastOutputBufferSizeUsedWasStale = (getBarcodeReaderOutputBufferSize() != _lastOutputBufferSizeUsed);
    return _lastOutputBufferSizeUsedWasStale;
  }

  /**
   * @return true if the output buffer size was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastOutputBufferSizeUsedWasStale()
  {
    return _lastOutputBufferSizeUsedWasStale;
  }

  /**
   * @return output buffer size used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastOutputBufferSize()
  {
    return _lastOutputBufferSizeUsed;
  }

  /**
   * @return true if the input buffer size is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isInputBufferSizeStale()
  {
    _lastInputBufferSizeUsedWasStale = (getBarcodeReaderInputBufferSize() != _lastInputBufferSizeUsed);
    return _lastInputBufferSizeUsedWasStale;
  }

  /**
   * @return true if the input buffer size was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastInputBufferSizeUsedWasStale()
  {
    return _lastInputBufferSizeUsedWasStale;
  }

  /**
   * @return input buffer size used during the last initialize attempt.
   * @author Bob Balliew
   */
  int getLastInputBufferSize()
  {
    return _lastInputBufferSizeUsed;
  }

  /**
   * @return true if the character set is stale, false otherwise.
   * @author Bob Balliew
   */
  private boolean isCharsetStale()
  {
    final String charset = getBarcodeReaderCharsetName();
    Assert.expect(charset != null);
    _lastCharsetUsedWasStale = (_lastCharsetUsed == null) || (! charset.equals(_lastCharsetUsed));
    return _lastCharsetUsedWasStale;
  }

  /**
   * @return true if the charset was stale (changed) on the last
   * initialize attempt, false otherwise.
   * @author Bob Balliew
   */
  boolean getLastCharsetUsedWasStale()
  {
    return _lastCharsetUsedWasStale;
  }

  /**
   * @return charset used during the last initialize attempt.
   * @author Bob Balliew
   */
  String getLastCharset()
  {
    return _lastCharsetUsed;
  }

  /**
   * Verify that the 'Scanner' enumeration meets the assumptions of this class.
   * @author Bob Balliew
   */
  static private void checkScannerSanity()
  {
    Collection<BarcodeReaderEnum> allPossibleReaders = BarcodeReaderEnum.values();
    Set<Integer> ordersFound = new TreeSet<Integer>();

    // Check for duplicate order values.
    for (BarcodeReaderEnum barcodeReaderEnum : allPossibleReaders)
    {
      Integer order = barcodeReaderEnum.getOrder();
      Assert.expect(! ordersFound.contains(order), "Duplicate order: " + order + " for Scanner" + barcodeReaderEnum);
      ordersFound.add(order);
    }

    // Check order values are zero based without gaps.
    int expectOrder = 0;

    for (int order: ordersFound)
    {
      Assert.expect(order == expectOrder, "Found order: " + order + " expected: " + expectOrder);
      ++expectOrder;
    }
  }

  /**
   * Verify that the '_barcodeReaders[]' instance meets the assumptions of this class.
   * @author Bob Balliew
   */
  private void checkBarcodeReaderSanity()
  {
    // Check that '_barcodeReader[]' array index matches getOrder() return.
    for (int i = 0; i < _barcodeReaders.length; ++i)
    {
      Assert.expect(i == _barcodeReaders[i].getId().getOrder(), "_barcodeReaders[" + i + "].getId().getOrder(): " + _barcodeReaders[i].getId().getOrder() + " expected: " + i);
    }
  }

  /**
   * Initailze stale parameter detection.
   * @author Bob Balliew
   */
  private void initializeStaleDetection()
  {
    _lastPortNameUsed = null;
    _lastPortNameUsedWasStale = true;
    _lastApplicationNameUsed = null;
    _lastApplicationNameUsedWasStale = true;
    _lastOpenTimeoutInMillisecondsUsed = BAD_OPEN_TIMEOUT;
    _lastOpenTimeoutInMillisecondsUsedWasStale = true;
    _lastEnableReceiveFramingUsed = false;
    _lastReceiveFramingByteUsed = BAD_RECEIVE_FRAMING;
    _lastReceiveFramingByteUsedWasStale = true;
    _lastEnableReceiveThresholdUsed = false;
    _lastReceiveThresholdCountUsed = BAD_RECEIVE_THRESHOLD;
    _lastReceiveThresholdCountUsedWasStale = true;
    _lastEnableReceiveTimeoutUsed = false;
    _lastReceiveTimeoutInMillisecondsUsed = BAD_RECEIVE_TIMEOUT;
    _lastReceiveTimeoutUsedWasStale = true;
    _lastBitsPerSecondUsed = BAD_BITS_PER_SECOND;
    _lastBitsPerSecondUsedWasStale = true;
    _lastDataBitsUsed = BAD_DATA_BITS;
    _lastDataBitsUsedWasStale = true;
    _lastStopBitsUsed = BAD_STOP_BITS;
    _lastStopBitsUsedWasStale = true;
    _lastParityUsed = BAD_PARITY;
    _lastParityUsedWasStale = true;
    _lastFlowControlUsed = BAD_FLOW_CONTROL;
    _lastFlowControlUsedWasStale = true;
    _lastOutputBufferSizeUsed = -1;
    _lastOutputBufferSizeUsedWasStale = true;
    _lastInputBufferSizeUsed = -1;
    _lastInputBufferSizeUsedWasStale = true;
    _lastCharsetUsed = null;
    _lastCharsetUsedWasStale = true;
  }

  /**
   * Construct a BarcodeReaderManager object.
   * @author Bob Balliew
   */
  private BarcodeReaderManager()
  {
    _config = null;
    _serialPort = null;
    _inputSerialStreamReader = null;
    _outputSerialStreamWriter = null;
    initializeStaleDetection();
    checkScannerSanity();
    // The following MUST be inserted in ascending order by 'getOrder()' value.
    // The implementation requires the _barcodeReaders[] index be identical to
    // 'getOrder()'.
    _barcodeReaders = new BarcodeReader[]
     {
       new BarcodeReader(BarcodeReaderEnum.BCR1),
       new BarcodeReader(BarcodeReaderEnum.BCR2),
       new BarcodeReader(BarcodeReaderEnum.BCR3),
       new BarcodeReader(BarcodeReaderEnum.BCR4),
       new BarcodeReader(BarcodeReaderEnum.BCR5),
       new BarcodeReader(BarcodeReaderEnum.BCR6),
       new BarcodeReader(BarcodeReaderEnum.BCR7),
       new BarcodeReader(BarcodeReaderEnum.BCR8),
       new BarcodeReader(BarcodeReaderEnum.BCR9),
       new BarcodeReader(BarcodeReaderEnum.BCR10),
       new BarcodeReader(BarcodeReaderEnum.BCR11),
       new BarcodeReader(BarcodeReaderEnum.BCR12),
       new BarcodeReader(BarcodeReaderEnum.BCR13),
       new BarcodeReader(BarcodeReaderEnum.BCR14),
       new BarcodeReader(BarcodeReaderEnum.BCR15),
       new BarcodeReader(BarcodeReaderEnum.BCR16),
     };
     checkBarcodeReaderSanity();
  }
  
  /**
   * @author Kee Chin Seong 
   * @ 2 type of port issue
   *   - Connection Lost 
   *   - Force 
   * @param portName
   * @return 
   */
  public boolean isPortExist(String portName)
  {
    Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
    while (thePorts.hasMoreElements())
    {
       CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
       switch (com.getPortType())
       {
          case CommPortIdentifier.PORT_SERIAL:
          {
             if(com.getName().matches(portName))
               return true;
          }
       }
    }
    return false;
  }

  /**
   * Close serial port.
   * @author Bob Balliew
   */
  private void closeSerialPort()
  {
    if (_serialPort != null)
       _serialPort.close();  // Be sure to release any currently allocated serial port.
    
    _serialPort = null;
    _inputSerialStreamReader = null;
    _outputSerialStreamWriter = null;
    initializeStaleDetection();
  }

  /**
   * If any pertinent changes have occurred since the serial port was last
   * opened, reopen the serial port using current values.
   * @author Bob Balliew
   */
  private void getSerialPort() throws HardwareException
  {
    CommPortIdentifier commPortId = null;
    /*
     * Ensure all 'is...Stale' methods used are called so all '_last...Stale'
     * flags are up-to-date--calling these methods from within a boolean
     * expression will not ensure all the functions are called because of
     * partial evaluation (early out) of boolean expressions.
     */
    final boolean portNameIsStale = isPortNameStale();
    final boolean applicationNameIsStale = isApplicationNameStale();
    final boolean openTimeoutIsStale = isOpenTimeoutStale();
    
    if ((_serialPort == null) || portNameIsStale || applicationNameIsStale || openTimeoutIsStale)
    {
      if(isPortExist(getBarcodeReaderSerialPortName()))
         closeSerialPort();
      
      try
      {
        commPortId = CommPortIdentifier.getPortIdentifier(getBarcodeReaderSerialPortName());
      }
      catch (NoSuchPortException e)
      {
        throw new NoSuchPortHardwareException(getBarcodeReaderSerialPortName());
      }

      if (commPortId.getPortType() != CommPortIdentifier.PORT_SERIAL)
      {
        throw new NotASerialPortHardwareException(getBarcodeReaderSerialPortName());
      }

      // Attempt to open the serial port.
      try
      {
        //Siew Yeng - XCR-3172 - Java SE binary stop working if unplug barcode reader immediately
        if(isPortExist(getBarcodeReaderSerialPortName()))
        {
          _serialPort = (SerialPort)commPortId.open(getBarcodeReaderApplicationName(), getBarcodeReaderOpenTimeoutInMilliseconds());
          _lastPortNameUsed = getBarcodeReaderSerialPortName();
          _lastApplicationNameUsed = getBarcodeReaderApplicationName();
          _lastOpenTimeoutInMillisecondsUsed = getBarcodeReaderOpenTimeoutInMilliseconds();

          // Initialize to force latest serial port instance to be completely setup.
          _lastEnableReceiveFramingUsed = false;
          _lastReceiveFramingByteUsed = BAD_RECEIVE_FRAMING;
          _lastEnableReceiveThresholdUsed = false;
          _lastReceiveThresholdCountUsed = BAD_RECEIVE_THRESHOLD;
          _lastEnableReceiveTimeoutUsed = false;
          _lastReceiveTimeoutInMillisecondsUsed = BAD_RECEIVE_TIMEOUT;
          _lastBitsPerSecondUsed = BAD_BITS_PER_SECOND;
          _lastDataBitsUsed = BAD_DATA_BITS;
          _lastStopBitsUsed = BAD_STOP_BITS;
          _lastParityUsed = BAD_PARITY;
          _lastFlowControlUsed = BAD_FLOW_CONTROL;

          // Set to null to force new stream instances to be connected to lastest serial port instance.
          _inputSerialStreamReader = null;
          _outputSerialStreamWriter = null;
        }
        else
        {
          throw new IOHardwareException(getBarcodeReaderSerialPortName());
        }
      }
      catch (PortInUseException e)
      {
        closeSerialPort();
        throw new PortInUseHardwareException(getBarcodeReaderSerialPortName(), e.currentOwner);
      }
      
      if(_serialPortWatcher == null)
         _serialPortWatcher = new SerialPortWatcher();
      
      _serialPortWatcher.startThread();
    }

    Assert.expect(_serialPort != null);
  }

  /**
   * If any pertinent changes have occurred since the receive framing on the
   * serial port was last set, reset the serial port receive framing using
   * current values, otherwise do nothing.
   * @author Bob Balliew
   */
  private void setReceiveFraming() throws HardwareException
  {
    if (isReceiveFramingByteStale())
    {
      Assert.expect(_serialPort != null);

      if (getBarcodeReaderEnableReceiveFraming())
      {
        try
        {
          _serialPort.enableReceiveFraming(getBarcodeReaderReceiveFramingByte());
          Assert.expect(_serialPort.isReceiveFramingEnabled());
          Assert.expect(_serialPort.getReceiveFramingByte() == getBarcodeReaderReceiveFramingByte());
        }
        catch (UnsupportedCommOperationException e)
        {
          final String portName = getPortName();
          closeSerialPort();
          throw new NoReceiveFramingSupportHardwareException(portName);
        }
      }
      else
      {
        _serialPort.disableReceiveFraming();
        Assert.expect(!_serialPort.isReceiveFramingEnabled());
        // Do not expect receive framing byte to track when receive framing is disabled.
      }

      _lastEnableReceiveFramingUsed = getBarcodeReaderEnableReceiveFraming();
      _lastReceiveFramingByteUsed = getBarcodeReaderReceiveFramingByte();
    }
  }

  /**
   * If any pertinent changes have occurred since the receive threshold on the
   * serial port was last set, reset the serial port receive threshold using
   * current values, otherwise do nothing.
   * @author Bob Balliew
   */
  private void setReceiveThreshold() throws HardwareException
  {
    if (isReceiveThresholdCountStale())
    {
      Assert.expect(_serialPort != null);

      if (getBarcodeReaderEnableReceiveThreshold())
      {
        try
        {
          _serialPort.enableReceiveThreshold(getBarcodeReaderReceiveThresholdCount());
          Assert.expect(_serialPort.isReceiveThresholdEnabled());
          Assert.expect(_serialPort.getReceiveThreshold() == getBarcodeReaderReceiveThresholdCount());
        }
        catch (UnsupportedCommOperationException e)
        {
          final String portName = getPortName();
          closeSerialPort();
          throw new NoReceiveThresholdSupportHardwareException(portName);
        }
      }
      else
      {
        _serialPort.disableReceiveThreshold();
        Assert.expect(!_serialPort.isReceiveThresholdEnabled());
        // Do not expect receive threshold count to track when receive threshold is disabled.
      }

      _lastEnableReceiveThresholdUsed = getBarcodeReaderEnableReceiveThreshold();
      _lastReceiveThresholdCountUsed = getBarcodeReaderReceiveThresholdCount();
    }
   }

  /**
   * If any pertinent changes have occurred since the receive timeout on the
   * serial port was last set, reset the serial port receive timeout using
   * current values, otherwise do nothing.
   * @author Bob Balliew
   */
  private void setReceiveTimeout() throws HardwareException
  {
    if (isReceiveTimeoutStale())
    {
      Assert.expect(_serialPort != null);

      if (getBarcodeReaderEnableReceiveTimeout())
      {
        try
        {
          _serialPort.enableReceiveTimeout(getBarcodeReaderReceiveTimeoutInMilliseconds());
          Assert.expect(_serialPort.isReceiveTimeoutEnabled());
          Assert.expect(_serialPort.getReceiveTimeout() == getBarcodeReaderReceiveTimeoutInMilliseconds());
        }
        catch (UnsupportedCommOperationException e)
        {
          final String portName = getPortName();
          closeSerialPort();
          throw new NoReceiveTimeoutSupportHardwareException(portName, getBarcodeReaderReceiveTimeoutInMilliseconds());
        }
      }
      else
      {
        _serialPort.disableReceiveTimeout();
        Assert.expect(!_serialPort.isReceiveTimeoutEnabled());
        // Do not expect receive timeout to track when receive timeout is disabled.
      }

      _lastEnableReceiveTimeoutUsed = getBarcodeReaderEnableReceiveTimeout();
      _lastReceiveTimeoutInMillisecondsUsed = getBarcodeReaderReceiveTimeoutInMilliseconds();
    }
  }

  /**
   * Covert a 'Bits Per Second' string into the appropriate serial port code.
   * @param bps contains a string that represents bits per second.
   * @return a number that is the desired bits per second code.
   * @author Bob Balliew
   */
  static int convertBitsPerSecond(String bps)
  {
    Assert.expect(bps != null);

    if      (bps.equals("75"))      return 75;
    else if (bps.equals("110"))     return 110;
    else if (bps.equals("134"))     return 134;
    else if (bps.equals("150"))     return 150;
    else if (bps.equals("300"))     return 300;
    else if (bps.equals("600"))     return 600;
    else if (bps.equals("1200"))    return 1200;
    else if (bps.equals("1800"))    return 1800;
    else if (bps.equals("2400"))    return 2400;
    else if (bps.equals("4800"))    return 4800;
    else if (bps.equals("7200"))    return 7200;
    else if (bps.equals("9600"))    return 9600;
    else if (bps.equals("14400"))   return 14400;
    else if (bps.equals("19200"))   return 19200;
    else if (bps.equals("38400"))   return 38400;
    else if (bps.equals("57600"))   return 57600;
    else if (bps.equals("115200"))  return 115200;
    else if (bps.equals("128000"))  return 128000;
    else                            return BAD_BITS_PER_SECOND;
  }

  /**
   * Covert a 'Data Bits' string into the appropriate serial port code.
   * @param db contains a string that represents the number of data bits.
   * @return the serial port code for the desired number of data bits.
   * @author Bob Balliew
   */
  static int convertDataBits(String db)
  {
    Assert.expect(db != null);

    if      (db.equals("5"))  return SerialPort.DATABITS_5;
    else if (db.equals("6"))  return SerialPort.DATABITS_6;
    else if (db.equals("7"))  return SerialPort.DATABITS_7;
    else if (db.equals("8"))  return SerialPort.DATABITS_8;
    else                      return BAD_DATA_BITS;
  }

  /**
   * Covert a 'Parity' string into the appropriate serial port code.
   * @param p contains a string that represents the type of parity checking.
   * @return the serial port code for the desired type of parity checking.
   * @author Bob Balliew
   */
  static int convertParity(String p)
  {
    Assert.expect(p != null);

    if      (p.equals("EVEN"))   return SerialPort.PARITY_EVEN;
    else if (p.equals("MARK"))   return SerialPort.PARITY_MARK;
    else if (p.equals("NONE"))   return SerialPort.PARITY_NONE;
    else if (p.equals("ODD"))    return SerialPort.PARITY_ODD;
    else if (p.equals("SPACE"))  return SerialPort.PARITY_SPACE;
    else                         return BAD_PARITY;
  }

  /**
   * Covert a 'Stop Bits' string into the appropriate serial port code.
   * @param sb contains a string that represents the number of stop bits.
   * @return the serial port code for the desired number of stop bits.
   * @author Bob Balliew
   */
  static int convertStopBits(String sb)
  {
    Assert.expect(sb != null);

    if      (sb.equals("1"))    return SerialPort.STOPBITS_1;
    else if (sb.equals("1.5"))  return SerialPort.STOPBITS_1_5;
    else if (sb.equals("2"))    return SerialPort.STOPBITS_2;
    else                        return BAD_STOP_BITS;
  }

  /**
   * Covert set the 'FlowControl' flags into the appropriate serial port code.
   * @param RTS_CTS_In is true if RTS/CTS flow control is on for input.
   * @param RTS_CTS_Out is true if RTS/CTS flow control is on for output.
   * @param XON_XOFF_In is true if XON/XOFF flow control is on for input.
   * @param XON_XOFF_Out is true if XON/XOFF flow control is on for output.
   * @return the serial port code for the desired type of flow control.
   * @author Bob Balliew
   */
  static int convertFlowControl(boolean RTS_CTS_In,
                                boolean RTS_CTS_Out,
                                boolean XON_XOFF_In,
                                boolean XON_XOFF_Out)
  {
    int code = SerialPort.FLOWCONTROL_NONE;
    if (RTS_CTS_In)    code |= SerialPort.FLOWCONTROL_RTSCTS_IN;
    if (RTS_CTS_Out)   code |= SerialPort.FLOWCONTROL_RTSCTS_OUT;
    if (XON_XOFF_In)   code |= SerialPort.FLOWCONTROL_XONXOFF_IN;
    if (XON_XOFF_Out)  code |= SerialPort.FLOWCONTROL_XONXOFF_OUT;
    return code;
  }

  /**
   * Ensure '_config' contains a reference to the 'Config' class.
   * @author Bob Balliew
   */
  private void initializeConfig()
  {
    if (_config == null)
    {
      _config = Config.getInstance();
      _configObservable = ConfigObservable.getInstance();
    }

    Assert.expect(_config != null);
  }

  /**
   * If any pertinent changes have occurred since the properties on the
   * serial port was last set, reset the serial port properties using
   * current values, otherwise do nothing.
   * @author Bob Balliew
   */
  private void setSerialPortProperties() throws HardwareException
  {
    /*
     * Ensure all 'is...Stale' methods used are called so all '_last...Stale'
     * flags are up-to-date--calling these methods from within a boolean
     * expression will not ensure all the functions are called because of
     * partial evaluation (early out) of boolean expressions.
     */
    final boolean bitsPerSecondIsStale = isBitsPerSecondStale();
    final boolean dataBitsIsStale = isDataBitsStale();
    final boolean stopBitsIsStale = isStopBitsStale();
    final boolean parityIsStale = isParityStale();

    if (bitsPerSecondIsStale || dataBitsIsStale || stopBitsIsStale || parityIsStale)
    {
      Assert.expect(_serialPort != null);
      final int bitsPerSecond = convertBitsPerSecond(getBarcodeReaderRs232BitsPerSecond());
      final int dataBits = convertDataBits(getBarcodeReaderRs232DataBits());
      final int stopBits = convertStopBits(getBarcodeReaderRs232StopBits());
      final int parity = convertParity(getBarcodeReaderRs232Parity());

      try
      {
        _serialPort.setSerialPortParams(bitsPerSecond, dataBits, stopBits, parity);
        Assert.expect(_serialPort.getBaudRate() == bitsPerSecond);
        Assert.expect(_serialPort.getDataBits() == dataBits);
        Assert.expect(_serialPort.getStopBits() == stopBits);
        Assert.expect(_serialPort.getParity() == parity);
      }
      catch (UnsupportedCommOperationException e)
      {
        final String portName = getPortName();
        closeSerialPort();
        throw new UnsupportedSerialCommunicationHardwareException(portName, getBarcodeReaderRs232BitsPerSecond(), getBarcodeReaderRs232DataBits(), getBarcodeReaderRs232StopBits(), getBarcodeReaderRs232Parity());
      }

      _lastBitsPerSecondUsed = bitsPerSecond;
      _lastDataBitsUsed = dataBits;
      _lastStopBitsUsed = stopBits;
      _lastParityUsed = parity;
    }
  }

  /**
   * If any pertinent changes have occurred since the flow control on the
   * serial port was last set, reset the serial port flow control using
   * current values, otherwise do nothing.
   * @author Bob Balliew
   */
  private void setSerialPortFlowControl() throws HardwareException
  {
    if (isFlowControlStale())
    {
      Assert.expect(_serialPort != null);

      try
      {
        _serialPort.setFlowControlMode(getFlowControl());
        final int flowControlReturn = _serialPort.getFlowControlMode();
        Assert.expect(flowControlReturn == getFlowControl());
        Assert.expect(((flowControlReturn & SerialPort.FLOWCONTROL_RTSCTS_IN) != 0) == getBarcodeReaderRs232RtsCtsFlowControlIn());
        Assert.expect(((flowControlReturn & SerialPort.FLOWCONTROL_RTSCTS_OUT) != 0) == getBarcodeReaderRs232RtsCtsFlowControlOut());
        Assert.expect(((flowControlReturn & SerialPort.FLOWCONTROL_XONXOFF_IN) != 0) == getBarcodeReaderRs232XonXoffFlowControlIn());
        Assert.expect(((flowControlReturn & SerialPort.FLOWCONTROL_XONXOFF_OUT) != 0) == getBarcodeReaderRs232XonXoffFlowControlOut());
      }
      catch (UnsupportedCommOperationException e)
      {
        final String portName = getPortName();
        closeSerialPort();
        throw new NoFlowControlSupportHardwareException(portName, getFlowControl());
      }

      _lastFlowControlUsed = getFlowControl();
    }
  }

  /**
   * Get a serial port and set it out based on current values.
   * @author Bob Balliew
   */
  private void initializeSerialPort() throws HardwareException
  {
    getSerialPort();
    setReceiveFraming();
    setReceiveThreshold();
    setReceiveTimeout();
    setSerialPortProperties();
    setSerialPortFlowControl();
  }

  /**
   * If any pertinent changes have occurred since the serial port output
   * writer was instantiated, reopen the serial port output writer using
   * current values.
   * @author Bob Balliew
   */
  private void initializeSerialOutputWriter() throws HardwareException
  {
    /*
     * Ensure all 'is...Stale' methods used are called so all '_last...Stale'
     * flags are up-to-date--calling these methods from within a boolean
     * expression will not ensure all the functions are called because of
     * partial evaluation (early out) of boolean expressions.
     */
    final boolean outputBufferSizeIsStale = isOutputBufferSizeStale();
    final boolean charsetIsStale = isCharsetStale();

    if ((_outputSerialStreamWriter == null) || outputBufferSizeIsStale || charsetIsStale)
    {
      Assert.expect(_serialPort != null);

      try
      {
        OutputStream os = _serialPort.getOutputStream();
        BufferedOutputStream buf = new BufferedOutputStream(os, getBarcodeReaderOutputBufferSize());
        _outputSerialStreamWriter = new OutputStreamWriter(buf, getBarcodeReaderCharsetName());
      }
      catch (IOException e)
      {
        final String portName = getPortName();
        closeSerialPort();
        throw new SerialCommunicationErrorHardwareException(portName);
      }

      _lastOutputBufferSizeUsed = getBarcodeReaderOutputBufferSize();
      _lastCharsetUsed = getBarcodeReaderCharsetName();
    }

    Assert.expect(_serialPort != null);
    Assert.expect(_outputSerialStreamWriter != null);
  }

  /**
   * If any pertinent changes have occurred since the serial port inpuy
   * reader was instantiated, reopen the serial port input reader using
   * current values.
   * @author Bob Balliew
   */
  private void initializeSerialInputReader() throws HardwareException
  {
    /*
     * Ensure all 'is...Stale' methods used are called so all '_last...Stale'
     * flags are up-to-date--calling these methods from within a boolean
     * expression will not ensure all the functions are called because of
     * partial evaluation (early out) of boolean expressions.
     */
    final boolean inputBufferSizeIsStale = isInputBufferSizeStale();
    final boolean charsetIsStale = isCharsetStale();

    if ((_inputSerialStreamReader == null) || inputBufferSizeIsStale || charsetIsStale)
    {
      Assert.expect(_serialPort != null);

      try
      {
        InputStream is = _serialPort.getInputStream();
        BufferedInputStream buf = new BufferedInputStream(is, getBarcodeReaderInputBufferSize());
        _inputSerialStreamReader = new InputStreamReader(buf, getBarcodeReaderCharsetName());
      }
      catch (IOException e)
      {
        final String portName = getPortName();
        closeSerialPort();
        throw new SerialCommunicationErrorHardwareException(portName);
      }

      _lastInputBufferSizeUsed = getBarcodeReaderInputBufferSize();
      _lastCharsetUsed = getBarcodeReaderCharsetName();
    }

    Assert.expect(_serialPort != null);
    Assert.expect(_inputSerialStreamReader != null);
  }

  /**
   * Perform any initializations needed to ensure that all instance variables
   * represent the current values in their corresponding ConfigEnum.  In addition,
   * detect and update any driver setting based on values that have changed
   * since the driver was last set up.
   * @author Bob Balliew
   */
  void initialize() throws HardwareException
  {
    initializeConfig();
    initializeSerialPort();
    initializeSerialOutputWriter();
    initializeSerialInputReader();
    //Siew Yeng - XCR1727 - Clear barcode reader memory before start scan barcode in production
    initializeBarcodeReaders(); 
    Assert.expect(_config != null);
    Assert.expect(_lastPortNameUsed != null);
    Assert.expect(_lastPortNameUsed.equals(getBarcodeReaderSerialPortName()));
    Assert.expect(_lastApplicationNameUsed != null);
    Assert.expect(_lastApplicationNameUsed.equals(getBarcodeReaderApplicationName()));
    Assert.expect(_lastOpenTimeoutInMillisecondsUsed != BAD_OPEN_TIMEOUT);
    Assert.expect(_lastOpenTimeoutInMillisecondsUsed == getBarcodeReaderOpenTimeoutInMilliseconds());
    Assert.expect(_lastEnableReceiveFramingUsed == getBarcodeReaderEnableReceiveFraming());
    Assert.expect(_lastReceiveFramingByteUsed != BAD_RECEIVE_FRAMING);
    Assert.expect(_lastReceiveFramingByteUsed == getBarcodeReaderReceiveFramingByte());
    Assert.expect(_lastEnableReceiveThresholdUsed == getBarcodeReaderEnableReceiveThreshold());
    Assert.expect(_lastReceiveThresholdCountUsed != BAD_RECEIVE_THRESHOLD);
    Assert.expect(_lastReceiveThresholdCountUsed == getBarcodeReaderReceiveThresholdCount());
    Assert.expect(_lastEnableReceiveTimeoutUsed == getBarcodeReaderEnableReceiveTimeout());
    Assert.expect(_lastReceiveTimeoutInMillisecondsUsed != BAD_RECEIVE_TIMEOUT);
    Assert.expect(_lastReceiveTimeoutInMillisecondsUsed == getBarcodeReaderReceiveTimeoutInMilliseconds());
    Assert.expect(_lastBitsPerSecondUsed != BAD_BITS_PER_SECOND);
    Assert.expect(_lastDataBitsUsed != BAD_DATA_BITS);
    Assert.expect(_lastStopBitsUsed != BAD_STOP_BITS);
    Assert.expect(_lastParityUsed != BAD_PARITY);
    Assert.expect(_lastFlowControlUsed != BAD_FLOW_CONTROL);
    Assert.expect(_lastOutputBufferSizeUsed > 0);
    Assert.expect(_lastInputBufferSizeUsed > 0);
    Assert.expect(_lastCharsetUsed != null);
    Assert.expect(_inputSerialStreamReader != null);
    Assert.expect(_outputSerialStreamWriter != null);
    Assert.expect(_serialPort != null);
    Assert.expect(_barcodeReaders != null);
  }

  /**
   * Wrap 'StringUtil.convertHexCodedStringIntoUnicodeString' such that it
   * now throws 'XrayTesterException' subclasses.
   * @author Bob Balliew
   */
  static private String convertHexCodedStringIntoUnicodeString(String codesInHex) throws InvalidHexadecimalCodeDatastoreException, HexadecimalCodeValueOutOfRangeDatastoreException
  {
    Assert.expect(codesInHex != null);

    try
    {
      return StringUtil.convertHexCodedStringIntoUnicodeString(codesInHex);
    }
    catch (CharConversionException cce)
    {
      throw new HexadecimalCodeValueOutOfRangeDatastoreException(cce.getMessage());
    }
    catch (NumberFormatException nfe)
    {
      throw new InvalidHexadecimalCodeDatastoreException(nfe.getMessage());
    }
  }

  /**
   * Wrap 'StringUtil.convertUnicodeStringIntoHexCodedString' for symmetry with
   * 'convertHexCodedStringIntoUnicodeString' (see above).
   * @author Bob Balliew
   */
  static private String convertUnicodeStringIntoHexCodedString(String codePoints)
  {
    Assert.expect(codePoints != null);
    return StringUtil.convertUnicodeStringIntoHexCodedString(codePoints);
  }

  /**
   * @return the port name (last used).
   * @author Bob Balliew
   */
  synchronized String getPortName()
  {
    return (_lastPortNameUsed != null) ? _lastPortNameUsed : "???";
  }

  /**
   * @return the instance of BarcodeReaderManager (a singleton).
   * @author Bob Balliew
   */
  public synchronized static BarcodeReaderManager getInstance()
  {
    if (_barcodeReaderManager == null)
      _barcodeReaderManager = new BarcodeReaderManager();

    return _barcodeReaderManager;
  }

  /**
   * Release any allocated resources.  Should never happen since this class is
   * currently a singleton.
   * @author Bob Balliew
   */
  protected synchronized void finalize() throws Throwable
  {
    closeSerialPort();
    super.finalize();
  }
  
  /**
   * @author Kee Chin Seong 
   * - Chcking the output stream availability 
   * @return 
   */
  public boolean isOutputStreamWriterIsAvailable()
  {
     return (_outputSerialStreamWriter == null) ? false : true;
  }
  
  /**
   * @author Kee Chin Seong 
   * - Chcking the input stream availability
   * @return 
   */
  public boolean isInputStreamWriterIsAvailable()
  {
     return (_inputSerialStreamReader == null) ? false : true;
  }
  
  /**
   * Get the serial numbers.
   * @return a list of serial numbers read.  The order of the items is based on
   * the order field in the 'Scanner' enumeration.
   * @author Bob Balliew
   */
  public synchronized List<String> getSerialNumbers() throws HardwareException
  {
    initialize();
    Assert.expect(_config != null);
    Assert.expect(_serialPort != null);
    Assert.expect(_inputSerialStreamReader != null);
    Assert.expect(_outputSerialStreamWriter != null);
    clearAbort();
    int numberOrScanners = getBarcodeReaderNumberOfScanners();
    List<String> serialNumbers = new ArrayList<String>(numberOrScanners);

    for (int order = 0; order < numberOrScanners; ++ order)
    {
      if (isAborting())
      {
        serialNumbers.add("Abort detected during getSerialNumbers");
      }
      else
      {
        _barcodeReaders[order].trigger(_outputSerialStreamWriter);

        // Wei Chin
        if(getBarcodeReaderEnableResponseDelay())
        {
          try
          {
            wait(getBarcodeReaderResponseDelayInMilliseconds());
          }
          catch(InterruptedException ie)
          {
            // do nothing
          }
        }
        
        //Checking _inputSerialStreamReader availability
        if(_inputSerialStreamReader != null)
            serialNumbers.add(_barcodeReaders[order].read(_inputSerialStreamReader));
        else
            serialNumbers.add("");
      }
    }

    Assert.expect(serialNumbers.size() == numberOrScanners);
    return serialNumbers;
  }

  /**
   * Get the serial numbers.    The purpose of this routine is to return the
   * maximum amount of diagnostic information to aid the user when
   * troubleshooting problems with a barcode reader configuration.
   * @todo REB deprecated This method should not be used.
   * @return a list of serial numbers read.  The order of the items is based on
   * the order field in the 'Scanner' enumeration.
   * @author Bob Balliew
   */
  public synchronized List<String> getSerialNumbersReturnErrors()
  {
    final int NO_SCANNERS_SLEEP_TIME = 50;  // The number of milliseconds this routine will wait if there are no scanners, this is done so this routine does not consume the majority of the CPU cycles if is called in a loop (typical application).
    initializeConfig();
    Assert.expect(_config != null);
    clearAbort();
    int numberOfScanners = getBarcodeReaderNumberOfScanners();
    List<String> serialNumbers = new ArrayList<String>(numberOfScanners);

    try
    {
      initialize();
    }
    catch (HardwareException e)
    {
      if (numberOfScanners > 0)  // No place to put exception and still meet post condition if no barcode reader are in the configuration.
        serialNumbers.add("HW Exception (initialize): e.getMessage():" + e.getLocalizedMessage());

      // Pad remainder of 'serialNumbers' so it contains 'numberOfScanners' items.
      for (int i = 1; i < numberOfScanners; ++i)
        serialNumbers.add("EXCEPTION");

      Assert.expect(serialNumbers.size() == numberOfScanners);
      return serialNumbers;
    }

    Assert.expect(_serialPort != null);
    Assert.expect(_inputSerialStreamReader != null);
    Assert.expect(_outputSerialStreamWriter != null);

    for (int order = 0; order < numberOfScanners; ++order)
    {
      if (isAborting())
      {
        serialNumbers.add("Abort detected during getSerialNumbers");
      }
      else
      {
        _barcodeReaders[order].triggerReturnErrors(_outputSerialStreamWriter);

        stopBarcodeReaderReadCycle();
        serialNumbers.add(_barcodeReaders[order].readReturnErrors(_inputSerialStreamReader));
      }
    }

    try
    {
      if (numberOfScanners == 0)
        Thread.sleep(NO_SCANNERS_SLEEP_TIME);
    }
    catch (InterruptedException e)
    {
      // Do nothing.
    }

    Assert.expect(serialNumbers.size() == numberOfScanners);
    return serialNumbers;
  }

  /**
   * Check to see if the current (to edit) barcode reader configuration name is
   * protected.
   * @return true if the current (to edit) configuration name is protected
   * (unwritable), otherwise it returns false.
   * @author Bob Balliew
   */
  public synchronized boolean isProtected()
  {
    return isProtected(getBarcodeReaderConfigurationNameToEdit());
  }

  /**
   * Check to see if specified barcode reader configuration name is protected.
   * This routine tolerates specifying a configuration file name.
   * @param barcodeReaderConfigName the configuration name.
   * @return true if the specified configuration name is protected (unwritable),
   * otherwise it returns false.
   * @author Bob Balliew
   */
  public synchronized boolean isProtected(String barcodeReaderConfigName)
  {
    Assert.expect(barcodeReaderConfigName != null);
    initializeConfig();
    Assert.expect(_config != null);
    return Pattern.matches(_config.getStringValue(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES), getBarcodeReaderConfigName(barcodeReaderConfigName));
  }

  /**
   * @return the string which contains the protected indicator
   * @author Andy Mechtenberg
   */
  public String getProtectedIndicator()
  {
    initializeConfig();
    Assert.expect(_config != null);
    String protectedIndicator = Config.getInstance().getStringValue(SoftwareConfigEnum.BARCODE_READER_PROTECTED_CONFIGURATION_NAMES);
    protectedIndicator = protectedIndicator.substring(0, 1);
    return protectedIndicator;
  }

  /**
   * Create a new barcode reader config file, it becomes the current (to edit)
   * barcode reader config.
   * @param newBarcodeReaderConfigName is the name of the barcode reader configuration
   * to create
   * @author Bob Balliew
   */
  public synchronized void newConfig(String newBarcodeReaderConfigName) throws DatastoreException
  {
    saveAs(newBarcodeReaderConfigName);
  }

  /**
   * Delete the specified existing barcode reader config file.  The specified
   * config cannot be a protected config, the current config (to edit), or the
   * 'to use' barcode reader config.
   * @param existingBarcodeReaderConfigName is the name of the barcode reader
   * configuration to delete
   * @author Bob Balliew
   */
  public synchronized void deleteConfig(String existingBarcodeReaderConfigName) throws DatastoreException
  {
    String toUseConfig = getBarcodeReaderConfigurationNameToUse();
    Assert.expect(toUseConfig != null);
    Assert.expect(! toUseConfig.equalsIgnoreCase(getBarcodeReaderConfigName(existingBarcodeReaderConfigName)));
    String currentConfig = getBarcodeReaderConfigurationNameToEdit();
    Assert.expect(currentConfig != null);
    Assert.expect(! currentConfig.equalsIgnoreCase(getBarcodeReaderConfigName(existingBarcodeReaderConfigName)));
    Assert.expect(! isProtected(existingBarcodeReaderConfigName));
    Assert.expect(getExistingBarcodeReaderConfigurationNames().contains(getBarcodeReaderConfigName(existingBarcodeReaderConfigName)));
    FileUtilAxi.delete(getBarcodeReaderConfigFullPath(existingBarcodeReaderConfigName));
  }

  /**
   * Load the specified barcode reader config from disk.  This becomes the new
   * current (to edit) barcode reader config file
   * @param barcodeReaderConfigName is the name of the barcode reader configuration
   * to load
   * @author Bob Balliew
   */
  public synchronized void load(String barcodeReaderConfigName) throws DatastoreException
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(barcodeReaderConfigName != null);

    //XCR1713 - Barcode config used in production is not the one selected in options
    //Siew Yeng - keep track of current loaded config
    if(_currentConfig == null)
    {        
      _currentConfig = getBarcodeReaderConfigurationNameToEdit();
    }
    
    if(_currentConfig.equals(barcodeReaderConfigName))
      return;
    
    //Siew Yeng - XCR-2969 - set to default config if config to load does not exist
    if(FileUtil.exists(getBarcodeReaderConfigFullPath(barcodeReaderConfigName)) == false)
    {
      barcodeReaderConfigName = getBarcodeReaderConfigFullPath(FileName.getDefaultBarcodeReaderConfigurationFullPath());
      
      //create empty file if default config not exist
      if(FileUtil.exists(barcodeReaderConfigName) == false)
      {
        try 
        {
          FileUtil.createEmtpyFile(barcodeReaderConfigName);
        } 
        catch (IOException ex) 
        {
          CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(barcodeReaderConfigName);
          dex.initCause(ex);
          throw dex;
        }
      }
    }
    
    _config.load(getBarcodeReaderConfigFullPath(_currentConfig), getBarcodeReaderConfigFullPath(barcodeReaderConfigName));
    _currentConfig = barcodeReaderConfigName;
    setBarcodeReaderConfigurationNameToEdit(getBarcodeReaderConfigName(barcodeReaderConfigName));
  }

  /**
   * Save the current (to edit) barcode reader config to disk to persist any
   * changes.  This routine must NOT be called if the current barcode reader
   * configuration file is protected (updates are not allowed).
   * @author Bob Balliew
   */
  public synchronized void save() throws DatastoreException
  {
    String currentConfig = getBarcodeReaderConfigurationNameToEdit();
    Assert.expect(currentConfig != null);
    Assert.expect(! isProtected(currentConfig));
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_configObservable != null);
    // don't want to generate update events during a save
    _configObservable.setEnabled(false);
    _config.save(getBarcodeReaderConfigFullPath(currentConfig));
    _configObservable.setEnabled(true);
  }

  /**
   * Save the current (to edit) barcode reader config into the specified barcode
   * reader config on disk to persist any changes.  This routine must NOT be
   * called if the current barcode reader configuration file is protected
   * (updates are not allowed).
   * @param newBarcodeReaderConfigName is the name of the barcode reader configuration
   * to save as
   * @author Bob Balliew
   */
  public synchronized void saveAs(String newBarcodeReaderConfigName) throws DatastoreException
  {
    Assert.expect(newBarcodeReaderConfigName != null);
    String currentConfig = getBarcodeReaderConfigurationNameToEdit();
    Assert.expect(currentConfig != null);
    Assert.expect(! isProtected(newBarcodeReaderConfigName));
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_configObservable != null);
    // don't want to generate update events during a save
    _configObservable.setEnabled(false);
    _config.save(getBarcodeReaderConfigFullPath(currentConfig), getBarcodeReaderConfigFullPath(newBarcodeReaderConfigName));
    _configObservable.setEnabled(true);
    //Siew Yeng - Fixes for XCR1713 caused software crash when create new barcode config
    //The solution is to keep track of current loaded config after save config
    _currentConfig = newBarcodeReaderConfigName; 
    setBarcodeReaderConfigurationNameToEdit(getBarcodeReaderConfigName(newBarcodeReaderConfigName));
  }

  /**
   * Get a list of all the barcode reader configurations that currently exist.
   * A barcode reader configuration name is the same as the file name without
   * the configuration file extension.
   * @return a list of barcode reader configuration names.
   * @author Bob Balliew
   */
  public List<String> getExistingBarcodeReaderConfigurationNames()
  {
    return FileName.getBarcodeReaderConfigurationNames();
  }

  /**
   * Get automatic barcode reader enabled flag from configuration.
   * @return true if automatic barcode reader is enabled, otherwise false.
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderAutomaticReaderEnabled()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED);
  }

  /**
   * Sets the automatic barcode reader enabled flag in the configuration file.
   * @param setting of true means enable automatic barcode reader, false means
   * disable automatic barcode reader.
   * @author Bob Balliew
   */
  public void setBarcodeReaderAutomaticReaderEnabled(boolean setting) throws DatastoreException
  {
    initializeConfig();
    Assert.expect(_config != null);
    _config.setValue(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED, setting);
  }

  /**
   * Get barcode reader configuration name to edit from the configuration file.
   * The configuration name is the file name without the extension.  The file
   * name is what is persisted in the software configuration file.
   * @return barcode reader configuration name.
   * @author Bob Balliew
   */
  public String getBarcodeReaderConfigurationNameToEdit()
  {
    initializeConfig();
    Assert.expect(_config != null);
    String configurationFileName = _config.getStringValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT);
    Assert.expect(configurationFileName != null);
    return getBarcodeReaderConfigName(configurationFileName);
  }

  /**
   * Sets barcode reader configuration name to edit in the configuration file.
   * The configuration name is the file name without the extention.  The file
   * name is what is persisted in the configuration file.  This routine will
   * tolerate having either a configuration name or configuration file name
   * specified.
   * @param configurationName the name of the configuration to set (will also
   * accept a configuration file name).
   * @author Bob Balliew
   */
  private void setBarcodeReaderConfigurationNameToEdit(String configurationName) throws DatastoreException
  {
    Assert.expect(configurationName != null);
    initializeConfig();
    Assert.expect(_config != null);
    _config.setValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_EDIT, getBarcodeReaderConfigName(configurationName));
  }

  /**
   * Get barcode reader configuration name to use from the configuration file.
   * The configuration name is the file name without the extension.  The file
   * name is what is persisted in the software configuration file.
   * @return barcode reader configuration name.
   * @author Bob Balliew
   */
  public String getBarcodeReaderConfigurationNameToUse()
  {
    initializeConfig();
   Assert.expect(_config != null);
   String configurationFileName = _config.getStringValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE);
   Assert.expect(configurationFileName != null);
   return getBarcodeReaderConfigName(configurationFileName);
  }

  /**
   * Sets barcode reader configuration name to use in the configuration file.
   * The configuration name is the file name without the extention.  The file
   * name is what is persisted in the configuration file.  This routine will
   * tolerate having either a configuration name or configuration file name
   * specified.
   * @param configurationName the name of the configuration to set (will also
   * accept a configuration file name).
   * disable automatic barcode reader.
   * @author Bob Balliew
   */
  public void setBarcodeReaderConfigurationNameToUse(String configurationName) throws DatastoreException
  {
    Assert.expect(configurationName != null);
    initializeConfig();
    Assert.expect(_config != null);
    _config.setValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE, getBarcodeReaderConfigName(configurationName));
  }

  /**
   * Get the number of barcode scanners (readers) in the configuration.
   * @return the number of barcode scanners
   * @author Bob Balliew
   */
  public int getBarcodeReaderNumberOfScanners()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS);
  }

  /**
   * Set the number of barcode scanners (readers) in the barcode reader
   * configuration.  The set is only to the in-memory object.  This change is
   * NOT persisted until a save or saveAs (if any) is done.
   * @param numberOfScanners is the number of barcode scanners
   * @author Bob Balliew
   */
  public void setBarcodeReaderNumberOfScanners(int numberOfScanners) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS, numberOfScanners);
  }

  /**
   * @return the smallest legal number of barcode scanners
   * @author Bob Balliew
   */
  public int getMinimumBarcodeReaderNumberOfScanners()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS);
    Assert.expect(validValues.size() == 2);
    Object minimumValue = validValues.get(0);
    Assert.expect(minimumValue instanceof Integer);
    return (Integer)minimumValue;
  }

  /**
   * @return the largest legal number of barcode scanners
   * @author Bob Balliew
   */
  public int getMaximumBarcodeReaderNumberOfScanners()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS);
    Assert.expect(validValues.size() == 2);
    Object maximumValue = validValues.get(1);
    Assert.expect(maximumValue instanceof Integer);
    return (Integer)maximumValue;
  }

  /**
   * Get output buffer size.
   * @return the (initial) size of the output buffer.
   * @author Bob Balliew
   */
  private int getBarcodeReaderOutputBufferSize()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_OUTPUT_BUFFER_SIZE);
  }

  /**
   * Get intput buffer size.
   * @return the (initial) size of the input buffer.
   * @author Bob Balliew
   */
  private int getBarcodeReaderInputBufferSize()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_INPUT_BUFFER_SIZE);
  }

  /**
   * Get character set name used to translate bytes on RS232 (in to/out of)
   * Unicode strings..
   * @return the character set name.
   * @author Bob Balliew
   */
  private String getBarcodeReaderCharsetName()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_CHARSET_NAME);
  }

  /**
   * Get serial port name.
   * @return the name of the serial port
   * @author Bob Balliew
   */
  public String getBarcodeReaderSerialPortName()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME);
  }

  /**
   * Set serial port name in the barcode reader configuration.  The set is only
   * to the in-memory object.  This change is NOT persisted until a save or
   * saveAs (if any) is done.
   * @param portName is the name of the serial port
   * @author Bob Balliew
   */
  public void setBarcodeReaderSerialPortName(String portName) throws DatastoreException
  {
    Assert.expect(portName != null);
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME, portName);
  }

  /**
   * Get a list of valid serial port names.
   * @return a list of serial port name choices.
   * @author Bob Balliew
   */
  public List<String> getValidBarcodeReaderSerialPortNames()
  {
    initializeConfig();
    Assert.expect(_config != null);
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME);
    List<String> validStringNames = new ArrayList<String>();

    String xraySourcePortName;
    if (AbstractXraySource.getSourceTypeEnum().equals(XraySourceTypeEnum.LEGACY))
    {
      xraySourcePortName = _config.getStringValue(HardwareConfigEnum.LEGACY_XRAY_SOURCE_SERIAL_PORT_NAME);
    }
    else if (AbstractXraySource.getSourceTypeEnum().equals(XraySourceTypeEnum.HTUBE))
    {
      xraySourcePortName = _config.getStringValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_SERIAL_PORT_NAME);
    }
    else
    {
      Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      xraySourcePortName = null;
    }

    for (Object valid : validValues)
    {
      Assert.expect(valid instanceof String);
      String validName = (String)valid;

      // Do NOT allow com port name currently used to conrol Xray source to appear in the list of barcode reader choices.
      if (xraySourcePortName.equals(validName) == false)
        validStringNames.add(validName);
    }

    return validStringNames;
  }

  /**
   * Get appliation name (used to register owner of serial port with operating system).
   * @return the name of the application
   * @author Bob Balliew
   */
  private String getBarcodeReaderApplicationName()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_APPLICATION_NAME);
  }

  /**
   * Get open timeout (to wait for operating system to find serial port available to open).
   * @return the open timeout
   * @author Bob Balliew
   */
  private int getBarcodeReaderOpenTimeoutInMilliseconds()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS);
  }

  /**
   * Get string representation of the number of RS232 data bits.  A string is
   * used to limit the choices to a small set.
   * @return the number of RS232 data bits (as a string)
   * @author Bob Balliew
   */
  public String getBarcodeReaderRs232DataBits()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS);
  }

  /**
   * Set string representation of the number of RS232 data bits in the barcode
   * reader configuration.  The set is only to the in-memory object.  This
   * change is NOT persisted until a save or saveAs (if any) is done.  A string
   * is used to limit the choices to a small set.
   * @param dataBits is the number of RS232 data bits (as a string)
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232DataBits(String dataBits) throws DatastoreException
  {
    Assert.expect(dataBits != null);
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS, dataBits);
  }

  /**
   * Get a list of valid RS232 data bit choices.  Strings are used to express
   * the exact set of possible choices.
   * @return list of valid RS232 number of data bit choices.
   * @author Bob Balliew
   */
  public List<String> getValidBarcodeReaderRs232DataBits()
  {
    initializeConfig();
    Assert.expect(_config != null);
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS);
    List<String> validStringNames = new ArrayList<String>(validValues.size());

    for (Object valid : validValues)
    {
      Assert.expect(valid instanceof String);
      validStringNames.add((String)valid);
    }

    return validStringNames;
  }

  /**
   * Get RS232 hardware (RTS/CTS) flow control for input (reading from barcode
   * scanner).
   * @return true if RTS/CTS flow control on input is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderRs232RtsCtsFlowControlIn()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN);
  }

  /**
   * Set RS232 hardware (RTS/CTS) flow control for input (reading from barcode
   * scanner) in the barcode reader configuration.  The set is only to the
   * in-memory object.  This change is NOT persisted until a save or saveAs (if
   * any) is done.
   * @param enable of true if RTS/CTS flow control on input is on, false if it
   * is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232RtsCtsFlowControlIn(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN, enable);
  }

  /**
   * Get RS232 hardware (RTS/CTS) flow control for output (writing to barcode
   * scanner).
   * @return true if RTS/CTS flow control on output is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderRs232RtsCtsFlowControlOut()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT);
  }

  /**
   * Set RS232 hardware (RTS/CTS) flow control for output (writing to barcode
   * scanner) in the barcode reader configuration.  The set is only to the
   * in-memory object.  This change is NOT persisted until a save or saveAs (if
   * any) is done.
   * @param enable of true if RTS/CTS flow control on output is on, false if it
   * is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232RtsCtsFlowControlOut(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT, enable);
  }

  /**
   * Get RS232 software (XON/XOFF) flow control for input (reading from barcode
   * scanner).
   * @return true if XON/XOFF flow control on input is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderRs232XonXoffFlowControlIn()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN);
  }

  /**
   * Set RS232 software (XON/XOFF) flow control for input (reading from barcode
   * scanner) in the barcode reader configuration.  The set is only to the
   * in-memory object.  This change is NOT persisted until a save or saveAs (if
   * any) is done.
   * @param enable of true if RTS/CTS flow control on input is on, false if it
   * is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232XonXoffFlowControlIn(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN, enable);
  }

  /**
   * Get RS232 software (XON/XOFF) flow control for output (writing to barcode
   * scanner).
   * @return true if XON/XOFF flow control on output is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderRs232XonXoffFlowControlOut()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT);
  }

  /**
   * Set RS232 software (XON/XOFF) flow control for output (writing to barcode
   * scanner) in the barcode reader configuration.  The set is only to the
   * in-memory object.  This change is NOT persisted until a save or saveAs (if
   * any) is done.
   * @param enable of true if XON/XOFF flow control on output is on, false if it
   * is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232XonXoffFlowControlOut(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT, enable);
  }

  /**
   * Get the RS232 parity.
   * @return the RS232 parity
   * @author Bob Balliew
   */
  public String getBarcodeReaderRs232Parity()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY);
  }

  /**
   * Set the RS232 parity in the barcode reader configuration.  The set is only
   * to the in-memory object.  This change is NOT persisted until a save or
   * saveAs (if any) is done.
   * @param parity is the RS232 parity
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232Parity(String parity) throws DatastoreException
  {
    Assert.expect(parity != null);
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY, parity);
  }

  /**
   * Get a list of valid RS232 parity choices.
   * @return list of valid RS232 parity choices.
   * @author Bob Balliew
   */
  public List<String> getValidBarcodeReaderRs232Parity()
  {
    initializeConfig();
    Assert.expect(_config != null);
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY);
    List<String> validStringNames = new ArrayList<String>(validValues.size());

    for (Object valid : validValues)
    {
      Assert.expect(valid instanceof String);
      validStringNames.add((String)valid);
    }

    return validStringNames;
  }

  /**
   * Get string representation of the number of RS232 stop bits.  A string is
   * used to limit the choices to a small set.
   * @return the number of RS232 stop bits (as a string)
   * @author Bob Balliew
   */
  public String getBarcodeReaderRs232StopBits()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS);
  }

  /**
   * Set string representation of the number of RS232 stop bits in the barcode
   * reader configuration.  The set is only to the in-memory object.  This
   * change is NOT persisted until a save or saveAs (if any) is done.  A string
   * is used to limit the choices to a small set.
   * @param stopBits is the number of RS232 stop bits (as a string)
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232StopBits(String stopBits) throws DatastoreException
  {
    Assert.expect(stopBits != null);
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS, stopBits);
  }

  /**
   * Get a list of valid RS232 stop bit choices.  Strings are used to express
   * the exact set of possible choices.
   * @return list of valid RS232 number of stop bit choices.
   * @author Bob Balliew
   */
  public List<String> getValidBarcodeReaderRs232StopBits()
  {
    initializeConfig();
    Assert.expect(_config != null);
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS);
    List<String> validStringNames = new ArrayList<String>(validValues.size());

    for (Object valid : validValues)
    {
      Assert.expect(valid instanceof String);
      validStringNames.add((String)valid);
    }

    return validStringNames;
  }

  /**
   * Get string representation of the RS232 bits per second data rate.
   * A string is used to limit the choices to a small set.
   * @return the RS232 bit per second data rate (as a string)
   * @author Bob Balliew
   */
  public String getBarcodeReaderRs232BitsPerSecond()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND);
  }

  /**
   * Set string representation of the RS232 bits per second data rate in the
   * barcode reader configuration.  The set is only to the in-memory object.
   * This change is NOT persisted until a save or saveAs (if any) is done.  A
   * string is used to limit the choices to a small set.
   * @param bitsPerSecond is the RS232 bit per second data rate (as a string)
   * @author Bob Balliew
   */
  public void setBarcodeReaderRs232BitsPerSecond(String bitsPerSecond) throws DatastoreException
  {
    Assert.expect(bitsPerSecond != null);
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND, bitsPerSecond);
  }

  /**
   * Get a list of valid RS232 bits per second data rate choices.  Strings are
   * used to express the exact set of possible choices.
   * @return list of valid RS232 bits per second data rate choices.
   * @author Bob Balliew
   */
  public List<String> getValidBarcodeReaderRs232BitsPerSecond()
  {
    initializeConfig();
    Assert.expect(_config != null);
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND);
    List<String> validStringNames = new ArrayList<String>(validValues.size());

    for (Object valid : validValues)
    {
      Assert.expect(valid instanceof String);
      validStringNames.add((String)valid);
    }

    return validStringNames;
  }

  /**
   * Get receive framing enabled.
   * @return true if recieve framing is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderEnableReceiveFraming()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING);
  }

  /**
   * Set receive framing enable in the barcode reader configuration.  The set is
   * only to the in-memory object.  This change is NOT persisted until a save or
   * saveAs (if any) is done.
   * @param enable of true receive framing is on, false if it is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderEnableReceiveFraming(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_FRAMING, enable);
  }

  /**
   * Get the receive framing byte.
   * @return the receive framing byte
   * @author Bob Balliew
   */
  public int getBarcodeReaderReceiveFramingByte()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE);
  }

  /**
   * Set the receive framing byte in the barcode reader configuration.  The set
   * is only to the in-memory object.  This change is NOT persisted until a save
   * or saveAs (if any) is done.
   * @param receiveFramingByte is the receive framing byte
   * @author Bob Balliew
   */
  public void setBarcodeReaderReceiveFramingByte(int receiveFramingByte) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE, receiveFramingByte);
  }

  /**
   * @return the smallest legal receive framing byte
   * @author Bob Balliew
   */
  public int getMinimumBarcodeReaderReceiveFramingByte()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE);
    Assert.expect(validValues.size() == 2);
    Object minimumValue = validValues.get(0);
    Assert.expect(minimumValue instanceof Integer);
    return (Integer)minimumValue;
  }

  /**
   * @return the largest legal receive framing byte
   * @author Bob Balliew
   */
  public int getMaximumBarcodeReaderReceiveFramingByte()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_FRAMING_BYTE);
    Assert.expect(validValues.size() == 2);
    Object maximumValue = validValues.get(1);
    Assert.expect(maximumValue instanceof Integer);
    return (Integer)maximumValue;
  }

  /**
   * Get receive threshold enabled.
   * @return true if recieve threshold is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderEnableReceiveThreshold()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD);
  }

  /**
   * Set receive threshold enable in the barcode reader configuration.  The set
   * is only to the in-memory object.  This change is NOT persisted until a save
   * or saveAs (if any) is done.
   * @param enable of true receive threshold is on, false if it is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderEnableReceiveThreshold(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_THRESHOLD, enable);
  }

  /**
   * Get the receive threshold count.
   * @return the receive threshold count
   * @author Bob Balliew
   */
  public int getBarcodeReaderReceiveThresholdCount()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT);
  }

  /**
   * Set the receive threshold count in the barcode reader configuration.  The
   * set is only to the in-memory object.  This change is NOT persisted until a
   * or saveAs save (if any) is done.
   * @param receiveThresholdCount is the receive threshold count
   * @author Bob Balliew
   */
  public void setBarcodeReaderReceiveThresholdCount(int receiveThresholdCount) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT, receiveThresholdCount);
  }

  /**
   * @return the smallest legal receive threshold count
   * @author Bob Balliew
   */
  public int getMinimumBarcodeReaderThresholdCount()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT);
    Assert.expect(validValues.size() == 2);
    Object minimumValue = validValues.get(0);
    Assert.expect(minimumValue instanceof Integer);
    return (Integer)minimumValue;
  }

  /**
   * @return the largest legal receive threshold count
   * @author Bob Balliew
   */
  public int getMaximumBarcodeReaderThresholdCount()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_THRESHOLD_COUNT);
    Assert.expect(validValues.size() == 2);
    Object maximumValue = validValues.get(1);
    Assert.expect(maximumValue instanceof Integer);
    return (Integer)maximumValue;
  }

  /**
   * Get receive timeout enable.
   * @return true if recieve timeout is on, false if it is off
   * @author Bob Balliew
   */
  public boolean getBarcodeReaderEnableReceiveTimeout()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT);
  }

  /**
   * Set receive timeout enable in the barcode reader configuration.  The
   * set is only to the in-memory object.  This change is NOT persisted until a
   * or saveAs save (if any) is done.
   * @param enable of true receive timeout is on, false if it is off
   * @author Bob Balliew
   */
  public void setBarcodeReaderEnableReceiveTimeout(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RECEIVE_TIMEOUT, enable);
  }

  /**
   * Get the receive timeout (in milliseconds).
   * @return the receive timeout (in milliseconds)
   * @author Bob Balliew
   */
  public int getBarcodeReaderReceiveTimeoutInMilliseconds()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS);
  }

  /**
   * Set the receive timeout (in milliseconds) in the barcode reader
   * configuration.  The set is only to the in-memory object.  This change is
   * NOT persisted until a save or saveAs (if any) is done.
   * @param receiveTimeoutInMilliseconds is the receive timeout
   * @author Bob Balliew
   */
  public void setBarcodeReaderReceiveTimeoutInMilliseconds(int receiveTimeoutInMilliseconds) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS, receiveTimeoutInMilliseconds);
  }

  /**
   * @return the smallest legal receive timeout (in milliseconds)
   * @author Bob Balliew
   */
  public int getMinimumBarcodeReaderTimeoutInMilliseconds()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS);
    Assert.expect(validValues.size() == 2);
    Object minimumValue = validValues.get(0);
    Assert.expect(minimumValue instanceof Integer);
    return (Integer)minimumValue;
  }

  /**
   * @return the largest legal receive timeout (in milliseconds)
   * @author Bob Balliew
   */
  public int getMaximumBarcodeReaderTimeoutInMilliseconds()
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(_config.getValueType(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS).equals(TypeEnum.INTEGER));
    List<Object> validValues = _config.getValidValues(BarcodeReaderConfigEnum.BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS);
    Assert.expect(validValues.size() == 2);
    Object maximumValue = validValues.get(1);
    Assert.expect(maximumValue instanceof Integer);
    return (Integer)maximumValue;
  }

  /**
   * Get barcode reader 'no read' message from the configuration file.  This is
   * what the scanner returns if it detects a 'no read' condition.
   * @return 'no read' string
   * @author Bob Balliew
   */
  public String getBarcodeReaderNoReadMessage()
  {
    initializeConfig();
    Assert.expect(_config != null);

    if (_config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE))
      return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE);
    else
      return "";
  }

  /**
   * Set barcode reader 'no read' message in the barcode reader configuration.
   * The set is only to the in-memory object.  This change is NOT persisted
   * until a save or saveAs (if any) is done.  This is what the scanner returns
   * if it detects a 'no read' condition.
   * @param noReadMessage specifies the 'no read' message
   * @author Bob Balliew
   */
  public void setBarcodeReaderNoReadMessage(String noReadMessage) throws DatastoreException
  {
    Assert.expect(noReadMessage != null);

    if (noReadMessage.length() == 0)
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE, false);
    }
    else
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE, true);
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE, noReadMessage);
    }
  }

  /**
   * Get barcode reader 'bad symbol' message from the configuration file.  This
   * is what the scanner returns if it detects a 'bad symbol' condition.
   * @return 'no read' string
   * @author Bob Balliew
   */
  public String getBarcodeReaderBadSymbolMessage()
  {
    initializeConfig();
    Assert.expect(_config != null);

    if (_config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE))
      return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE);
    else
      return "";
  }

  /**
   * Set barcode reader 'bad symbol' message in the barcode reader
   * configuration.  The set is only to the in-memory object.  This change is
   * NOT persisted until a save or saveAs (if any) is done.  This is what the
   * scanner returns if it detects a 'bad symbol' condition.
   * @param badSymbolMessage specifies the 'bad symbol' message
   * @author Bob Balliew
   */
  public void setBarcodeReaderBadSymbolMessage(String badSymbolMessage) throws DatastoreException
  {
    Assert.expect(badSymbolMessage != null);

    if (badSymbolMessage.length() == 0)
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE, false);
    }
    else
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE, true);
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE, badSymbolMessage);
    }
  }

  /**
   * Get barcode reader 'no label' message from the configuration file.  This
   * is what the scanner returns if it detects a 'no label' condition.
   * @return 'no label' string
   * @author Bob Balliew
   */
  public String getBarcodeReaderNoLabelMessage()
  {
    initializeConfig();
    Assert.expect(_config != null);

    if (_config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE))
      return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE);
    else
      return "";
  }

  /**
   * Set barcode reader 'no label' message in the barcode reader configuration.
   * The set is only to the in-memory object.  This change is NOT persisted
   * until a save or saveAs (if any) is done.  This is what the scanner returns
   * if it detects a 'no label' condition.
   * @param noLabelMessage specifies the 'no label' message
   * @author Bob Balliew
   */
  public void setBarcodeReaderNoLabelMessage(String noLabelMessage) throws DatastoreException
  {
    Assert.expect(noLabelMessage != null);

    if (noLabelMessage.length() == 0)
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE, false);
    }
    else
    {
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE, true);
      _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE, noLabelMessage);
    }
  }

  /**
   * Get Scanner enum based on the scanner number.
   * @param scannerNumber is the scanner number (1 to n) based on the order that
   * the scanners are read
   * @return the Scanner enum for the specified order.
   * @author Bob Balliew
   */
  private BarcodeReaderEnum mapScannerNumberToScanner(int scannerNumber)
  {
    Assert.expect(scannerNumber >= 1);
    Assert.expect(scannerNumber <= _barcodeReaders.length);
    return _barcodeReaders[scannerNumber - 1].getId();
  }

  /**
   * Converts the specified code into what the GUI field should display.
   * @author Bob Balliew
   */
  static private String convertCodeToDisplay(String code)
  {
    return (code.length() == 0) ? code : convertUnicodeStringIntoHexCodedString(code);
  }

  /**
   * Get barcode reader trigger code for the specified scanner as a hexadecimal
   * string from the configuration file.  The trigger code is the byte sequence
   * sent to trigger a reading from the specified scanner.  The code is
   * presented as a comma delimited sequence of hexadecimal digits.  This
   * eliminates issues around displaying or entering unprintable codes.  The
   * string returned will contain zero or more commas (,) as a delimiter
   * character separating the hexadecimal codes.  Each hexadecimal code
   * corresponds to a single byte in the trigger code.  Each hexadecimal code
   * consists of one or two hexadecimal digits ('0', '1', '2', '3', '4', '5',
   * '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e' and 'f').
   *
   * E.g., a trigger code of "Thelma", for the specified scanner, would return
   * a hexadecimal string of "54,68,65,6c,6d,61".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @return barcode reader trigger code (comma delimited sequence of
   * hexadecimal digits.
   * @author Bob Balliew
   */
  public String getBarcodeReaderTriggerAsHexadecimalString(int scannerNumber)
  {
    initializeConfig();
   Assert.expect(_config != null);
   return convertCodeToDisplay(_config.getStringValue(mapScannerNumberToScanner(scannerNumber).getTrigger()));
  }

  /**
   * Sets the barcode reader trigger code for the specified scanner as a
   * hexadecimal string in the barcode reader configuration.  The set is only to
   * the in-memory object.  This change is NOT persisted until a save or saveAs
   * (if any) is done.  The trigger code is the byte sequence sent to trigger a
   * reading from the specified scanner.  The code is is expected to be a comma
   * delimited sequence of hexadecimal digits.  This eliminates issues around
   * displaying or entering unprintable codes.  The trigger string specified
   * must comma (,) delimit each hexadecimal code.  Each hexadecimal code
   * corresponds to a single byte in the trigger code.  Each hexadecimal code
   * consists of one or two hexadecimal digits ('0', '1', '2', '3', '4', '5',
   * '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E'
   * and 'F').
   *
   * E.g., specifing a hexadecimal string of "4c,6f,75,69,73,65", for the
   * specified scanner, is a trigger code of "Louise".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @param trigger is the trigger code (comma delimited sequence of hexadecimal
   * digits.
   * @author Bob Balliew
   */
  public void setBarcodeReaderTriggerAsHexadecimalString(int scannerNumber, String trigger) throws DatastoreException
  {
    Assert.expect(trigger != null);
    _config.setValueInMemoryOnly(mapScannerNumberToScanner(scannerNumber).getTrigger(), convertHexCodedStringIntoUnicodeString(trigger));
  }

  /**
   * Get barcode reader stop code for the specified scanner as a hexadecimal
   * string from the configuration file.  The stop code is the byte sequence
   * sent to stop read cycle from the specified scanner.  The code is
   * presented as a comma delimited sequence of hexadecimal digits.  This
   * eliminates issues around displaying or entering unprintable codes.  The
   * string returned will contain zero or more commas (,) as a delimiter
   * character separating the hexadecimal codes.  Each hexadecimal code
   * corresponds to a single byte in the stop code.  Each hexadecimal code
   * consists of one or two hexadecimal digits ('0', '1', '2', '3', '4', '5',
   * '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e' and 'f').
   *
   * E.g., a stop code of "Thelma", for the specified scanner, would return
   * a hexadecimal string of "54,68,65,6c,6d,61".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @return barcode reader stop code (comma delimited sequence of
   * hexadecimal digits.
   * @author Phang Siew Yeng
   */
  public String getBarcodeReaderStopAsHexadecimalString(int scannerNumber)
  {
    initializeConfig();
   Assert.expect(_config != null);
   return convertCodeToDisplay(_config.getStringValue(mapScannerNumberToScanner(scannerNumber).getStop()));
  }
  
  /**
   * Sets the barcode reader stop code for the specified scanner as a
   * hexadecimal string in the barcode reader configuration.  The set is only to
   * the in-memory object.  This change is NOT persisted until a save or saveAs
   * (if any) is done.  The stop code is the byte sequence sent to stop a
   * read cycle from the specified scanner.  The code is is expected to be a comma
   * delimited sequence of hexadecimal digits.  This eliminates issues around
   * displaying or entering unprintable codes.  The stop string specified
   * must comma (,) delimit each hexadecimal code.  Each hexadecimal code
   * corresponds to a single byte in the trigger code.  Each hexadecimal code
   * consists of one or two hexadecimal digits ('0', '1', '2', '3', '4', '5',
   * '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E'
   * and 'F').
   *
   * E.g., specifing a hexadecimal string of "4c,6f,75,69,73,65", for the
   * specified scanner, is a stop code of "Louise".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @param stop is the stop code (comma delimited sequence of hexadecimal
   * digits.
   * @author Phang Siew Yeng
   */
  public void setBarcodeReaderStopAsHexadecimalString(int scannerNumber, String stop) throws DatastoreException
  {
    Assert.expect(stop != null);
    _config.setValueInMemoryOnly(mapScannerNumberToScanner(scannerNumber).getStop(), convertHexCodedStringIntoUnicodeString(stop));
  }
  
  /**
   * Get barcode reader preamble code for the specified scanner as a
   * hexadecimal string from the configuration file.  The preamble code is the
   * byte sequence expected to be prepended to a reading from the specified
   * scanner.  The code is presented as a comma delimited sequence of
   * hexadecimal digits.  This eliminates issues around displaying or entering
   * unprintable codes.  The string returned will contain zero or more commas
   * (,) as a delimiter character separating the hexadecimal codes.  Each
   * hexadecimal code corresponds to a single byte in the preamble code.  Each
   * hexadecimal code consists of one or two hexadecimal digits ('0', '1', '2',
   * '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e' and 'f').
   *
   * E.g., a preamble code of "Thelma", for the specified scanner, would return
   * a hexadecimal string of "54,68,65,6c,6d,61".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @return barcode reader preamble code (comma delimited sequence of
   * hexadecimal digits.
   * @author Bob Balliew
   */
  public String getBarcodeReaderPreambleAsHexadecimalString(int scannerNumber)
  {
    initializeConfig();
   Assert.expect(_config != null);
   return convertCodeToDisplay(_config.getStringValue(mapScannerNumberToScanner(scannerNumber).getPreamble()));
  }

  /**
   * Sets the barcode reader preamble code for the specified scanner as a
   * hexadecimal string in the barcode reader configuration.  The set is only to
   * the in-memory object.  This change is NOT persisted until a save or saveAs
   * (if any) is done.  The preamble code is the byte sequence expected to be
   * prepended to a reading from the specified scanner.  The code is expected to
   * be a comma delimited sequence of hexadecimal digits.  This eliminates
   * issues around displaying or entering unprintable codes.  The preamble
   * string specified must comma (,) delimit each hexadecimal code.  Each
   * hexadecimal code corresponds to a single byte in the postamble code.  Each
   * hexadecimal code consists of one or two hexadecimal digits ('0', '1', '2',
   * '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B',
   * 'C', 'D', 'E' and 'F').
   *
   * E.g., specifing a hexadecimal string of "4c,6f,75,69,73,65", for the
   * specified scanner, is a preamble code of "Louise".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @param preamble is the preamble code (comma delimited sequence of
   * hexadecimal digits.
   * @author Bob Balliew
   */
  public void setBarcodeReaderPreambleAsHexadecimalString(int scannerNumber, String preamble) throws DatastoreException
  {
    Assert.expect(preamble != null);
    _config.setValueInMemoryOnly(mapScannerNumberToScanner(scannerNumber).getPreamble(), convertHexCodedStringIntoUnicodeString(preamble));
  }

  /**
   * Get barcode reader postamble code for the specified scanner as a
   * hexadecimal string from the configuration file.  The postamble code is the
   * byte sequence expected to be appended to a reading from the specified
   * scanner.  The code is presented as a comma delimited sequence of
   * hexadecimal digits.  This eliminates issues around displaying or entering
   * unprintable codes.  The string returned will contain zero or more commas
   * (,) as a delimiter character separating the hexadecimal codes.  Each
   * hexadecimal code corresponds to a single byte in the postamble code.  Each
   * hexadecimal code consists of one or two hexadecimal digits ('0', '1', '2',
   * '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e' and 'f').
   *
   * E.g., a postamble code of "Thelma", for the specified scanner, would return
   * a hexadecimal string of "54,68,65,6c,6d,61".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @return barcode reader trigger code (comma delimited sequence of
   * hexadecimal digits.
   * @author Bob Balliew
   */
  public String getBarcodeReaderPostambleAsHexadecimalString(int scannerNumber)
  {
    initializeConfig();
   Assert.expect(_config != null);
   return convertCodeToDisplay(_config.getStringValue(mapScannerNumberToScanner(scannerNumber).getPostamble()));
  }

  /**
   * Sets the barcode reader postamble code for the specified scanner as a
   * hexadecimal string in the barcode reader configuration.  The set is only to
   * the in-memory object.  This change is NOT persisted until a save or saveAs
   * (if any) is done.  The postamble code is the byte sequence expected to be
   * appended to a reading from the specified scanner.  The code is expected to
   * be a comma delimited sequence of hexadecimal digits.  This eliminates
   * issues around displaying or entering unprintable codes.  The postamble
   * string specified must comma (,) delimit each hexadecimal code.  Each
   * hexadecimal code corresponds to a single byte in the postamble code.  Each
   * hexadecimal code consists of one or two hexadecimal digits ('0', '1', '2',
   * '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B',
   * 'C', 'D', 'E' and 'F').
   *
   * E.g., specifing a hexadecimal string of "4c,6f,75,69,73,65", for the
   * specified scanner, is a postamble code of "Louise".
   *
   * @param scannerNumber specifies the barcode reader scanner
   * @param postamble is the postamble code (comma delimited sequence of
   * hexadecimal digits.
   * @author Bob Balliew
   */
  public void setBarcodeReaderPostambleAsHexadecimalString(int scannerNumber, String postamble) throws DatastoreException
  {
    Assert.expect(postamble != null);
    _config.setValueInMemoryOnly(mapScannerNumberToScanner(scannerNumber).getPostamble(), convertHexCodedStringIntoUnicodeString(postamble));
  }

   /**
   * Get response delay enable.
   * @author Wei Chin
   */
  public boolean getBarcodeReaderEnableResponseDelay()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getBooleanValue(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RESPONSE_DELAY);
  }

  /**
   * Set response delay enable in the barcode reader configuration.  The
   * set is only to the in-memory object.  This change is NOT persisted until a
   * or saveAs save (if any) is done.
   * @param enable of true response delay is on, false if it is off
   * @author Wei Chin
   */
  public void setBarcodeReaderEnableResponseDelay(boolean enable) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_RESPONSE_DELAY, enable);
  }

  /**
   * Get the response delay (in milliseconds).
   * @return the response delay (in milliseconds)
   * @author Wei Chin
   */
  public int getBarcodeReaderResponseDelayInMilliseconds()
  {
    initializeConfig();
    Assert.expect(_config != null);
    return _config.getIntValue(BarcodeReaderConfigEnum.BARCODE_READER_RESPONSE_DELAY_IN_MILLISECONDS);
  }

  /**
   * Set the response delay  (in milliseconds) in the barcode reader
   * configuration.  The set is only to the in-memory object.  This change is
   * NOT persisted until a save or saveAs (if any) is done.
   * @param responseDelayInMilliseconds is the response delay
   * @author Wei Chin
   */
  public void setBarcodeReaderResponseDelayInMilliseconds(int responseDelayInMilliseconds) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_RESPONSE_DELAY_IN_MILLISECONDS, responseDelayInMilliseconds);
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void connectSerialPort() throws HardwareException
  {
    initialize();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void disconnectSerialPort()
  {
    closeSerialPort();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public boolean isSerialPortOpened()
  {
    if(_serialPort != null)
      return true;
    
    return false;
  }
  
  /*
   * Clear data before start capture
   * @author Phang Siew Yeng
   */
  private void initializeBarcodeReaders() throws HardwareException
  {   
    try
    {
      stopBarcodeReaderReadCycle();
      for(int order = 0; order < getBarcodeReaderNumberOfScanners(); order++)
      {
        while(_inputSerialStreamReader.ready())
          _barcodeReaders[order].read(_inputSerialStreamReader); //clear data
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();   
    }
  }
  
  /*
   * This is use when scan method scan during board loading is selected
   * @author Phang Siew Yeng
   */
  public void triggerBarcodeReader() throws HardwareException
  {
    initialize();
    int numberOrScanners = getBarcodeReaderNumberOfScanners();

    for (int order = 0; order < numberOrScanners; ++ order)
    {
      _barcodeReaders[order].trigger(_outputSerialStreamWriter);
    }
  }
  
  /**
   * Get the serial numbers when board loading.
   * @author Siew Yeng
   */
  public synchronized List<String> getSerialNumbersWhenBoardLoading() throws HardwareException
  {
    Assert.expect(_config != null);
    Assert.expect(_serialPort != null);
    Assert.expect(_inputSerialStreamReader != null);
    Assert.expect(_outputSerialStreamWriter != null);
    clearAbort();
    List<String> serialNumbers = new ArrayList<String>();
    
    try
    {
      for(int order: _scannerIdNeeded)
      {
        if (isAborting())
        {
          serialNumbers.add("Abort detected during getSerialNumbers");
        }  
        else
        { 
           // Wei Chin
          if(getBarcodeReaderEnableResponseDelay())
          {
            try
            {
              wait(getBarcodeReaderResponseDelayInMilliseconds());
            }
            catch(InterruptedException ie)
            {
              // do nothing
            }
          }

          stopBarcodeReaderReadCycle();
          
          String barcodes = _barcodeReaders[order-1].read(_inputSerialStreamReader);
          
          if(barcodes.contains(SEPARATOR))
            serialNumbers.addAll(new ArrayList(Arrays.asList(barcodes.split(SEPARATOR))));
          else
            serialNumbers.add(barcodes);
        }
      }
    }
    catch(HardwareException he)
    {
      throw he;
    }
    
    return serialNumbers;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setBarcodeReaderScanMethod(BarcodeReaderScanMethodEnum scanMethod) throws DatastoreException
  {
    _config.setValueInMemoryOnly(BarcodeReaderConfigEnum.BARCODE_READER_SCAN_METHOD, scanMethod.toString());
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public String getBarcodeReaderScanMethod()
  {
    return _config.getStringValue(BarcodeReaderConfigEnum.BARCODE_READER_SCAN_METHOD);
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void stopBarcodeReaderReadCycle()
  {  
    int numberOrScanners = getBarcodeReaderNumberOfScanners();
  
    for (int order = 0; order < numberOrScanners; ++ order)
    {
      _barcodeReaders[order].stopReadCycle(_outputSerialStreamWriter);
    }
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setScannerIdsNeeded(List<Integer> scannerIdNeeded)
  {
    _scannerIdNeeded = scannerIdNeeded;
  }
}
