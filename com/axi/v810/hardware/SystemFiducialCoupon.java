package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * The following are locations of the features of the system coupon. A
 * quadrilateral is located in the upper part of the coupone where the smallest
 * vertical edge is on the right side. A small right triangle is located below
 * it where the right angle is facing the right side. There are 56 dots that
 * surround the triangle and the quadrilateral. Looking at the cad drawing of
 * the system coupon, the dot furthest down and furthest to the left is dot #1.
 * From there, we will number the dots in a clockwise fashion. This is an
 * abitrary naming convention chosen by me. If there is a better way feel free
 * to change it.
 *
 * The cad drawing is such that the right angle is facing to the right and the
 * smallest side of the quadrilateral is on the left. The triangle is also
 * below the quadrilateral. Below is simple asci art to illustrate the
 * orientation of the cad drawing.
 *
 *               ---------- _________
 *             /                      -------- _______
 *            /                                        \
 *           /                                          \
 *          /                                            \
 *         /                                          ____\
 *        /                            _______ ------
 *       /          _________ --------
 *       ----------
 *
 *                             ____
 *                            |   /
 *                            |  /
 *                            | /
 *                            |/
 *
 *
 * all values are from the lower left of the coupon when oriented in the
 * above fashion.
 *
 * All the descriptive names here are in respect to the above image rotated 180
 * degrees. The upper left quadrilateral is the lower right of the above image.
 *
 * The SystemFiducialCoordiante is based on the coupon is rotated 180 degrees.
 * When converting from a x or y value to a system fiducial coordinate, we must
 * subtract the value from the x or y fiducial point respectively.
 *
 * @author George A. David
 */
public class SystemFiducialCoupon
{
  private static SystemFiducialCoupon _instance;
  
  private Config _config;
  /**
   * @author George David
   */
  private SystemFiducialCoupon()
  {
    _config = Config.getInstance();
  }

  /**
   * @author George David
   */
  public static SystemFiducialCoupon getInstance()
  {
    if(_instance == null)
      _instance = new SystemFiducialCoupon();

    return _instance;
  }

  /**
   * currently located at 12.66 cm from the lower left of the system coupon
   * @author George David
   */
  public int getSystemFiducialPointXinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS);
  }

  /**
   * currently located at 11.05 cm from the lower left of the system coupon
   * @author George David
   */
  public int getSystemFiducialPointYinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS);
  }

  /**
   * currently located at 21.93 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralUpperLeftXinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS);
  }

  /**
   * currently located at 14.05 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralUpperLeftYinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS);
  }

  /**
   * location in x from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralUpperLeftXinNanometersInSystemFiducialCoordinates()
  {
    return convertXvalueToSystemFiducialCoordinate(getQuadrilateralUpperLeftXinNanoMeters());
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralUpperLeftYinNanometersInSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getQuadrilateralUpperLeftYinNanoMeters());
  }

  /**
   * currently located at 5.42 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralUpperRightXinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS);
  }

  /**
   * currently located at 12.89 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralUpperRightYinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS);
  }

  /**
   * location in x from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralUpperRightXinNanometersInSystemFiducialCoordinates()
  {
    return convertXvalueToSystemFiducialCoordinate(getQuadrilateralUpperRightXinNanoMeters());
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralUpperRightYinNanometersInSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getQuadrilateralUpperRightYinNanoMeters());
  }

  /**
   * currently located at 21.71 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralLowerLeftXinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS);
  }

  /**
   * currently located at 17.25 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralLowerLeftYinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS);
  }

  /**
   * location in x from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralLowerLeftXinNanometersInSystemFiducialCoordinates()
  {
    return convertXvalueToSystemFiducialCoordinate(getQuadrilateralLowerLeftXinNanoMeters());
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralLowerLeftYinNanometersInSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getQuadrilateralLowerLeftYinNanoMeters());
  }

  /**
   * currently located at 5.71 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralLowerRightXinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS);
  }

  /**
   * currently located at 18.36 cm from the lower left of the system coupon
   * @author George David
   */
  public int getQuadrilateralLowerRightYinNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS);
  }

  /**
   * location in x from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralLowerRightXinNanometersInSystemFiducialCoordinates()
  {
    return convertXvalueToSystemFiducialCoordinate(getQuadrilateralLowerRightXinNanoMeters());
  }

  /**
   * location in x from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getQuadrilateralLowerRightYinNanometersInSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getQuadrilateralLowerRightYinNanoMeters());
  }

  /**
   * @author George David
   */
  public int getDotWidthInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_DOT_WIDTH_NANOMETERS);
  }

  /**
   * this is the closest dot to the lower horizontal edge of the quadrilateral
   * that is within the x bounds of the quadrilateral.
   * @author George David
   */
  private int getDot31yInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_DOT_31_Y_NANOMETERS);
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getDot31yInNanometersSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getDot31yInNanoMeters());
  }

  /**
   * this is the closest dot to the upper horizontal edge of the quadrilateral
   * that is within the x bounds of the quadrilateral.
   * @author George David
   */
  private int getDot10yInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_DOT_10_Y_NANOMETERS);
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getDot10yInNanometersSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getDot10yInNanoMeters());
  }

  /**
   * this dot is lowest dot that is visible in the image when using a 16 micron
   * system. It was decided that this where we want to begin imaging the
   * system coupon. 
   *
   * @author George David
   */
  private int getDot19yInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS);
  }

  /**
   * location in y from the system fiducial point at 180 degrees rotation.
   * @author George David
   */
  public int getDot19YInNanometersSystemFiducialCoordinates()
  {
    return convertYvalueToSystemFiducialCoordinate(getDot19yInNanoMeters());
  }

  /**
   * @author Roy Williams
   * @author George A. David
   */
  public int getMaxWidthOfQuadrilateralInNanometers()
  {
    return Math.abs(getQuadrilateralUpperLeftXinNanoMeters() - getQuadrilateralUpperRightXinNanoMeters());
  }

  /**
   * @author George David
   */
  private int convertYvalueToSystemFiducialCoordinate(int yNanos)
  {
    return getSystemFiducialPointYinNanoMeters() - yNanos;
  }

  /**
   * @author George David
   */
  private int convertXvalueToSystemFiducialCoordinate(int xNanos)
  {
    return getSystemFiducialPointXinNanoMeters() - xNanos;
  }

  /**
   * @author George David
   */
  public int getTriangleWidthInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS);
  }

  /**
   * @author George David
   */
  public int getTriangleHeightInNanoMeters()
  {
    return _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS);
  }
}
