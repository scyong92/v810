package com.axi.v810.hardware;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Greg Esparza
 */
abstract class AxiTdiXrayCamera extends AbstractXrayCamera
{
  private static final int _UNIT_TEST_SOCKET_SERVER_PORT_FINDER_CREATE_SOCKET_ON_ANY_FREE_PORT = 0;
  private static final int _UNIT_TEST_STOP_CAMERA_SIMULATOR_WAIT_TIMEOUT_IN_MILLISECONDS = 500;
  private static final int _UNIT_TEST_STOP_CAMERA_SIMULATOR_WAIT_FOR_FLUSH_TIMEOUT_IN_MILLISECONDS = 0;
  private static final String _UNIT_TEST_CAMERA_SIMULATOR = "FalseCamera.exe";
  private static final String _UNIT_TEST_CAMERA_ID_KEY = "CAMERA_ID";
  private static final String _UNIT_TEST_PORT_NUMBER_KEY = "PORT";
  private static final String _UNIT_TEST_ENABLE_KEY = "UNIT_TEST";

  private static final int _TCPIP_ADDRESS_OCTETS[] = {192, 168, 128};
  private static final int _TCPIP_ADDRESS_BASE_FINAL_OCTET = 0;
  private static final int _TCPIP_PORT_NUMBER = 8080;
  private static final int _SOCKET_COMMUNICATION_TIMEOUT_IN_MILLISECONDS = 30000;
  private static final int _PING_TIMEOUT_IN_MILLISECONDS = 5000;
  private static final String _EMBEDDED_HARDWARE_ERROR_DATA_DELIMITER = ":";
  private static final String _EMBEDDED_HARDWARE_ERROR_INDICATOR = "ERROR" + _EMBEDDED_HARDWARE_ERROR_DATA_DELIMITER;
  private static final String _END_REPLY_INDICATOR = "\n";

  private static Map<XrayCameraIdEnum, HardwareConfigEnum> _cameraIdToBoardVersionMap;
  private static Map<String, AxiTdiXrayCamera> _boardVersionToInstanceMap;
  private static boolean _runtimeFileInformationReady;
  private static IntegerRef _runtimeFileInformationAccessCount;
  private static BooleanRef _runtimeFilesArchiveLock;
  private static IntegerRef _startupProgressStateCount;

  private CommunicationType _communicationType;
  private LaunchedProcess _cameraSimulatorProcess;
  private SocketClient _socketClient;
  private boolean _unitTestModeOn;
  private ServerSocket _unitTestSocketServerPortFinder;
  private Map<String, String> _unitTestSystemEnvironmentMap;

  protected int _portNumber;
  protected String _serverIpAddress;
  protected String _serverMacAddress="";
  protected String _boardVersion;
  protected List<String> _exceptionParameters;
  protected boolean _calibrationDataLoaded;
  protected String _runtimeInformationDirectory;
  protected String _runtimeInformationFileNameFullPath;
  protected Map<String, String> _runtimeFileNameToInformationMap;
  protected boolean _automaticRuntimeFileUpdateEnabled;
  protected boolean _byPassBoardVersionInHardwareConfigEnabled;
  
  //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
  private int _maxPocketHeightInNanometers = 0;
  private boolean _areAllCameraBoardSameType = true;
  private List<Integer> _listOfCamerasToDebug = null;

  /**
   * @author Greg Esparza
   */
  static void setCameraIdToBoardVersionMap()
  {
    _config = Config.getInstance();
    _cameraIdToBoardVersionMap = new HashMap<XrayCameraIdEnum, HardwareConfigEnum>();
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_0, HardwareConfigEnum.AXI_TDI_CAMERA_0_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_1, HardwareConfigEnum.AXI_TDI_CAMERA_1_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_2, HardwareConfigEnum.AXI_TDI_CAMERA_2_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_3, HardwareConfigEnum.AXI_TDI_CAMERA_3_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_4, HardwareConfigEnum.AXI_TDI_CAMERA_4_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_5, HardwareConfigEnum.AXI_TDI_CAMERA_5_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_6, HardwareConfigEnum.AXI_TDI_CAMERA_6_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_7, HardwareConfigEnum.AXI_TDI_CAMERA_7_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_8, HardwareConfigEnum.AXI_TDI_CAMERA_8_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_9, HardwareConfigEnum.AXI_TDI_CAMERA_9_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_10, HardwareConfigEnum.AXI_TDI_CAMERA_10_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_11, HardwareConfigEnum.AXI_TDI_CAMERA_11_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_12, HardwareConfigEnum.AXI_TDI_CAMERA_12_BOARD_VERSION);
    _cameraIdToBoardVersionMap.put(XrayCameraIdEnum.XRAY_CAMERA_13, HardwareConfigEnum.AXI_TDI_CAMERA_13_BOARD_VERSION);
  }

  /**
   * @author Greg Esparza
   */
  static
  {
    _boardVersionToInstanceMap = new HashMap<String, AxiTdiXrayCamera>();
    setCameraIdToBoardVersionMap();
    _runtimeFileInformationAccessCount = new IntegerRef();
    _runtimeFilesArchiveLock = new BooleanRef(false);
    _startupProgressStateCount = new IntegerRef();
  }

  /**
   * @author Greg Esparza
   */
  protected abstract void setLightSensitivity() throws XrayTesterException;

  /**
   * @author Greg Esparza
   */
  public void enableUnitTestMode(String imageDataTestDirectory) throws XrayTesterException
  {
    clearAbort();
    setUnitTestModeSocketClient();
    setUnitTestModeSystemEnvironment();
    runCameraSimulator(imageDataTestDirectory);
    _unitTestModeOn = true;
    assureCameraSimulatorIsConnected();
  }

  /**
   * Camera simulators may take some time to startup and be ready to accept connections.
   * This method tries to connect a set number of times with a sleep inbetween. If the
   * connection is still refused the last time it throws the connection exception.
   *
   * @author Reid Hayhow
   */
  private void assureCameraSimulatorIsConnected() throws XrayTesterException
  {
    Assert.expect(isUnitTestModeOn() == true);
    Assert.expect(_cameraSimulatorProcess != null);

    int numberOfConnectionAttempts = 10;
    while (numberOfConnectionAttempts > 0)
    {
      // Got in here, so we took a try
      numberOfConnectionAttempts--;

      try
      {
        // Attempt to connect.
        connect();

        // if the command doesn't cause an exception we are connected and done
        numberOfConnectionAttempts = 0;
      }
      // There was a problem with the connection/command so try again
      catch (XrayTesterException ex)
      {
        // But first give the simulator time to get further in the startup process
        try
        {
          Thread.currentThread().sleep(500);
        }
        catch (InterruptedException ex1)
        {
          // Do nothing
        }

        // If we saw an exception on the last attempt, go ahead and throw it
        if (numberOfConnectionAttempts == 0)
        {
          throw ex;
        }
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  public void disableUnitTestMode()
  {
    clearAbort();
    stopCameraSimulator();
    _unitTestModeOn = false;
  }

  /**
   * @author Greg Esparza
   */
  protected boolean isUnitTestModeOn()
  {
    return _unitTestModeOn;
  }

  /**
   * @author Greg Esparza
   */
  public CommunicationType getCommunicationType()
  {
    return _communicationType;
  }

  /**
   * @author Greg Esparza
   */
  static AxiTdiXrayCamera getAnInstance(XrayCameraIdEnum cameraId, IntCoordinate systemPositionInNanometers)
  {
    AxiTdiXrayCamera instance = null;

    String boardVersion = getBoardVersion(cameraId);
    String versionKey = boardVersion + "_" + String.valueOf(cameraId.getId());

    if (_boardVersionToInstanceMap.containsKey(versionKey))
    {
      instance = _boardVersionToInstanceMap.get(versionKey);
    }
    else
    {
      AxiTdiXrayCameraBoardVersionEnum boardVersionEnum = AxiTdiXrayCameraBoardVersionEnum.getEnum(boardVersion);
      if (boardVersionEnum.equals(AxiTdiXrayCameraBoardVersionEnum.BOARD_VERSION_A))
        instance = new AxiTdiVersionAXrayCamera(cameraId, boardVersion, systemPositionInNanometers);
      //Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	  else if (boardVersionEnum.equals(AxiTdiXrayCameraBoardVersionEnum.BOARD_VERSION_B))
        instance = new AxiTdiVersionBXrayCamera(cameraId, boardVersion, systemPositionInNanometers);
      else
        Assert.expect(false);

      _boardVersionToInstanceMap.put(versionKey, instance);
    }

    Assert.expect(instance != null);
    return instance;
  }
  
  /**
   * XCR-2630 Support New X-Ray Camera
   * @author Swee Yee Wong
   */
  public boolean areAllCameraBoardSameType()
  {
    //private static final String _VERSION_FIELD_DELIMITER = "\\.";
    String _VERSION_FIELD_DELIMITER = "\\.";
    String firstBoardType = "";
    for (Map.Entry<String, AxiTdiXrayCamera> entry : _boardVersionToInstanceMap.entrySet())
    {
      //System.out.println(entry.getKey() + "/" + entry.getValue());
      String actualVersion = entry.getKey();
      Assert.expect(actualVersion != null);
      String[] versionFields = actualVersion.split(_VERSION_FIELD_DELIMITER);
      Assert.expect(versionFields.length == 2);
      if(firstBoardType.equalsIgnoreCase(""))
        firstBoardType = versionFields[0];
      _areAllCameraBoardSameType = _areAllCameraBoardSameType && versionFields[0].equalsIgnoreCase(firstBoardType);
    }
    return _areAllCameraBoardSameType;
  }
  
  public int getCameraMaxPocketHeightInNanometers()
  {
    if(_maxPocketHeightInNanometers > 0)
      return _maxPocketHeightInNanometers;
    
    for (Map.Entry<String, AxiTdiXrayCamera> entry : _boardVersionToInstanceMap.entrySet())
    {
      AxiTdiXrayCamera xrayCamera = entry.getValue();
      if(xrayCamera.getSensorHeightInNanometers() > _maxPocketHeightInNanometers)
        _maxPocketHeightInNanometers = xrayCamera.getSensorHeightInNanometers();
    }
    return _maxPocketHeightInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  protected AxiTdiXrayCamera(XrayCameraIdEnum cameraId, String boardVersion, IntCoordinate systemPositionInNanometers)
  {
    super(cameraId, systemPositionInNanometers);

    Assert.expect(boardVersion != null);

    _boardVersion = boardVersion;
    _calibrationDataLoaded = false;
    _exceptionParameters = new ArrayList<String>();
    _unitTestModeOn = false;
    _unitTestSocketServerPortFinder = null;
    _unitTestSystemEnvironmentMap = null;
    _cameraSimulatorProcess = null;
    _automaticRuntimeFileUpdateEnabled = _config.getBooleanValue(HardwareConfigEnum.AXI_TDI_CAMERA_AUTOMATIC_RUNTIME_FILE_UPDATE);
    _byPassBoardVersionInHardwareConfigEnabled = _config.getBooleanValue(HardwareConfigEnum.AXI_TDI_CAMERA_BYPASS_BOARD_VERSION_IN_HARDWARE_CONFIG_UPDATE);
    setCommunication();
  }

  /**
   * @author Greg Esparza
   */
  protected HardwareConfigEnum getBoardVersionEnumFromId(XrayCameraIdEnum cameraId)
  {
    HardwareConfigEnum hardwareConfigEnum = (HardwareConfigEnum)_cameraIdToBoardVersionMap.get(cameraId);
    Assert.expect(hardwareConfigEnum != null);

    return hardwareConfigEnum;
  }
  
   /**
   * @author Anthony Fong
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   */
   private String getMacAddressFromIpAddress(String ipAddress) throws IOException
   {
	Runtime r = Runtime.getRuntime();
	Process m = r.exec("ping -n 1 -w 200 " + ipAddress);
	try
	{
		m.waitFor();
	}
	catch(InterruptedException ii)
	{
		return ii.toString();
	}
	Process p = r.exec("arp -a " +ipAddress);
        BufferedReader reader = new BufferedReader (new InputStreamReader(p.getInputStream()));
	String macAddress = "";
        String line;
        while ((line = reader.readLine()) != null)
        {
          if(Config.isDeveloperSystemEnabled())
            System.out.println ("Stdout: " + line);
          if(foundIpAddress(ipAddress + " ",line))
          {
              macAddress = extractMacAddress(line);
          }
        }
	return macAddress;
   }

   /**
   * @author Anthony Fong
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   */
  public boolean foundIpAddress(String ipAddress, String inputString)
  {
    Pattern macPattern = Pattern.compile( ipAddress);
    Matcher matcher = macPattern.matcher( inputString );
    return matcher.find();
  }
   /**
   * @author Anthony Fong
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   */
  public String extractMacAddress(String inputString)
  {
    String MAC_ADD_PATTERN = "((([0-9a-fA-F]){1,2}[-:]){5}([0-9a-fA-F]){1,2})";
    Pattern macPattern = Pattern.compile( MAC_ADD_PATTERN);
    Matcher matcher = macPattern.matcher( inputString );
    String macAddr="";
    if( matcher.find() )
    {
        macAddr = matcher.group().toUpperCase();
    }
    return macAddr;
 }

  /**
   * @author Greg Esparza
   */
  protected void connect() throws XrayTesterException
  {
    if (isSimulationModeOn() == false || isUnitTestModeOn())
    {
      try
      {
        if (_socketClient.isConnected() == false)
        {
          InetAddress address = IPaddressUtil.getHostInetAddressOnPrivateNetwork();
          if (isUnitTestModeOn())
            address = IPaddressUtil.getRemoteLoopbackAddress();
          _socketClient.connect(address, _SOCKET_COMMUNICATION_TIMEOUT_IN_MILLISECONDS);

          //get Mac Address
          _serverMacAddress = getMacAddressFromIpAddress(_serverIpAddress);
        }
      }
      catch (SocketConnectionFailedException scfe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToConnectException(_cameraId,
                                                                                                           _serverIpAddress,
                                                                                                           _portNumber,
                                                                                                           scfe.getLocalizedMessage());
        he.initCause(scfe);
        throw he;
      }
      catch(IOException xp)
      {
          
      }
    }
  }

  /**
   * @author George A. David
   */
  protected void disconnect() throws XrayTesterException
  {
    disconnectSocketClient();
  }

 /**
  * @author George A. David
  * @author Greg Esparza
  */
 protected synchronized List<String> sendCommandAndGetReplies(String command) throws XrayTesterException
 {
   List<String> replies = null;

   try
   {
     connect();
     boolean matchEndOfRepliesIndicatorExactly = false;
     boolean hideEndOfRepliesIndicatorFromReplies = true;
    
//     huge data will be saved into the log if uncomment the debug lines. It will append into the log.
//     Remember to clear the cameraCommandAndReplies.log whenever the file size larger than 25Mb.
     
     debug("Send >> Camera " + getId() + " - " + command);
     replies = _socketClient.sendRequestAndReceiveReplies(command + _END_REPLY_INDICATOR,
                                                          _END_REPLY_INDICATOR,
                                                          matchEndOfRepliesIndicatorExactly,
                                                          hideEndOfRepliesIndicatorFromReplies);
     debug("Received << Camera " + getId() + " - " + replies);

   }
   catch (IOException ioe)
   {
     exceptionDisconnect();
     try
     {
       connect();
       boolean matchEndOfRepliesIndicatorExactly = false;
       boolean hideEndOfRepliesIndicatorFromReplies = true;
       debug("Send >> Camera " + getId() + " - " + command);
       replies = _socketClient.sendRequestAndReceiveReplies(command + _END_REPLY_INDICATOR,
                                                            _END_REPLY_INDICATOR,
                                                            matchEndOfRepliesIndicatorExactly,
                                                            hideEndOfRepliesIndicatorFromReplies);
       debug("Received << Camera " + getId() + " - " + replies);
     }
     catch (IOException ioe2)
     {
       exceptionDisconnect();
       XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToExecuteCommandException(_cameraId,
                                                                                                                 _serverIpAddress,
                                                                                                                 _portNumber,
                                                                                                                 command,
                                                                                                                 ioe2.getLocalizedMessage());
       he.initCause(ioe2);
       throw he;
     }
   }

   if (doesErrorExist(replies))
   {
     exceptionDisconnect();
     throwEmbeddedHardwareException(replies.get(0));
   }

   return replies;
  }

  /**
   * @author Greg Esparza
   */
  protected ByteBuffer getBytesFromCamera(int numberOfBytesToReceive) throws XrayTesterException
  {
    Assert.expect(numberOfBytesToReceive > 0);

    ByteBuffer rawByteBuffer = null;

    if (isAborting())
      return null;

    try
    {
      connect();
      rawByteBuffer = _socketClient.receiveBuffer(numberOfBytesToReceive);
    }
    catch (IOException ioe)
    {
      exceptionDisconnect();
      try
      {
        connect();
        rawByteBuffer = _socketClient.receiveBuffer(numberOfBytesToReceive);
      }
      catch (IOException ioe2)
      {
        exceptionDisconnect();
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetByteDataException(_cameraId,
                                                                                                               _serverIpAddress,
                                                                                                               _portNumber,
                                                                                                               ioe.getLocalizedMessage());
        he.initCause(ioe);
        throw he;
      }
    }

    Assert.expect(rawByteBuffer != null);
    return rawByteBuffer;
  }

  /**
   * @author George A. David
   * @author Eddie Williamson
   * @author Greg Esparza
   */
  protected ByteBuffer getBytesFromCamera(String command, int numberOfBytesToReceive) throws XrayTesterException
  {
    Assert.expect(numberOfBytesToReceive > 0);

    ByteBuffer rawByteBuffer = null;
    boolean readAckCharAndReportErrors = true;

    try
    {
      if (isAborting())
        return null;

      connect();
      debug("Send >> Camera " + getId() + " - " + command + ", numberOfBytesToReceive - " + numberOfBytesToReceive);
      rawByteBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(command + _END_REPLY_INDICATOR,
                                                                      numberOfBytesToReceive,
                                                                      readAckCharAndReportErrors);
    }
    catch (Exception ex)
    {
//      exceptionDisconnect();
//      try
//      {
//        if (isAborting())
//          return null;
//
//        connect();
//        rawByteBuffer = _socketClient.sendRequestAndReturnRawByteBuffer(command + _END_REPLY_INDICATOR,
//                                                                        numberOfBytesToReceive,
//                                                                        readAckCharAndReportErrors);
//      }
//      catch (Exception ex2)
//      {
        exceptionDisconnect();
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToExecuteCommandException(_cameraId,
                                                                                                                  _serverIpAddress,
                                                                                                                  _portNumber,
                                                                                                                  command,
                                                                                                                  ex.getLocalizedMessage());
        he.initCause(ex);
        throw he;
//      }
    }
//    catch (SocketMessageFailedException smfe)
//    {
//      exceptionDisconnect();
//      XrayCameraHardwareException he = XrayCameraHardwareException.getAgilentTdiFailedToExecuteCommandException(_cameraId,
//                                                                                                                _serverIpAddress,
//                                                                                                                _portNumber,
//                                                                                                                command,
//                                                                                                                smfe.getLocalizedMessage());
//      he.initCause(smfe);
//      throw he;
//    }

    Assert.expect(rawByteBuffer != null);
    return rawByteBuffer;
  }

  /**
   * @author Greg Esparza
   */
  protected void waitForCommandToCompleteInMilliseconds(int waitTimeInMilliseconds)
  {
    try
    {
      Thread.sleep(waitTimeInMilliseconds);
    }
    catch (InterruptedException ie)
    {
      // do nothing
    }
  }

  /**
   * @author Greg Esparza
   */
  protected void verifyRuntimeFileInformationArchiveExists() throws XrayTesterException
  {
    String runtimeZipFileNameFullPath = FileName.getAgilentTdiXrayCameraRuntimeZipFileNameFullPath();

    if (FileUtilAxi.exists(runtimeZipFileNameFullPath) == false)
    {
      _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiLocalRuntimeFileDoesNotExistException(_cameraId,
                                                                                                                                _serverIpAddress,
                                                                                                                                _portNumber,
                                                                                                                                runtimeZipFileNameFullPath));
    }
  }

  /**
   * @author Greg Esparza
   */
  protected static synchronized void setRuntimeFileInformationReady(boolean runtimeFileInformationReady)
  {
    _runtimeFileInformationReady = runtimeFileInformationReady;
  }

  /**
   * @author Greg Esparza
   */
  protected static void setRuntimeFileInformationArchive() throws XrayTesterException
  {
    synchronized (_runtimeFileInformationAccessCount)
    {
      _runtimeFileInformationAccessCount.setValue(_runtimeFileInformationAccessCount.getValue() + 1);
    }

    // We only want one camera to extract the archive file containing the runtime file information
    synchronized(_runtimeFilesArchiveLock)
    {
      if (isRuntimeFileInformationReady() == false)
      {
        boolean retainOriginalFileTimeStamps = true;
        String runtimeZipFileNameFullPath = FileName.getAgilentTdiXrayCameraRuntimeZipFileNameFullPath();
        FileUtilAxi.unZip(runtimeZipFileNameFullPath, Directory.getXrayCameraConfigDir(), retainOriginalFileTimeStamps);
        setRuntimeFileInformationReady(true);
      }
    }

    synchronized(_runtimeFileInformationAccessCount)
    {
      _runtimeFileInformationAccessCount.setValue(_runtimeFileInformationAccessCount.getValue() - 1);

      // When all cameras have tried to extract the archive, we will mark the runtime file ready state to be false.
      // This will ensure that every we time we run "startup()" the archive is extracted.  We do not want to trust
      // the content in the existing extracted directories.
      if (_runtimeFileInformationAccessCount.getValue() == 0)
        setRuntimeFileInformationReady(false);
    }
  }

  /**
   * @author Greg Esparza
   */
  protected static void setStartupBeginProgressState()
  {
    synchronized (_startupProgressStateCount)
    {
      _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() + 1);
    }
  }

  /**
   * @author Greg Esparza
   */
  protected static void setStartupEndProgressState()
  {
    synchronized(_startupProgressStateCount)
    {
      _startupProgressStateCount.setValue(_startupProgressStateCount.getValue() - 1);

      try
      {
        if (_startupProgressStateCount.getValue() > 0)
        {
          _startupProgressStateCount.wait();
        }
        else
        {
          _startupProgressStateCount.notifyAll();
        }
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  protected void verifyCommunication() throws XrayTesterException
  {
    boolean cameraOnline = false;

    if (isSimulationModeOn() == false)
    {
      try
      {
        InetAddress addressToPingInet = InetAddress.getByName(_serverIpAddress);
        cameraOnline = addressToPingInet.isReachable(_PING_TIMEOUT_IN_MILLISECONDS);

        ICMPPacket.getInstance().checkRemoteHostIsUsingAgilentJumboPacket(toString(), _serverIpAddress);
      }
      catch (IOException ioe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetCommunicationPingStatusException(_cameraId,
                                                                                                                              _serverIpAddress,
                                                                                                                              _portNumber,
                                                                                                                              ioe.getLocalizedMessage());
        he.initCause(ioe);
        throw he;
      }
      catch (ICMPPacketHardwareException phe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToGetCommunicationPingStatusException(_cameraId,
                                                                                                                              _serverIpAddress,
                                                                                                                              _portNumber,
                                                                                                                              phe.getLocalizedMessageWithoutHeaderAndFooter());
        he.initCause(phe);
        throw he;
      }

      if (cameraOnline == false)
        throw XrayCameraHardwareException.getAxiTdiFailedCommunicationPingException(_cameraId, _serverIpAddress, _portNumber);
    }
  }

  /**
   * @author Greg Esparza
   */
  private static String getBoardVersion(XrayCameraIdEnum cameraId)
  {
    HardwareConfigEnum hardwareConfigEnum = (HardwareConfigEnum)_cameraIdToBoardVersionMap.get(cameraId);
    return _config.getStringValue(hardwareConfigEnum);
  }

  /**
   * Note: We should not couple the IP address to the camera ID.  Unfortunately, are using the camera ID to make
   *       the IP address unique.  This is undesriable but a functional approach.  However, since we are using zero-based
   *       camera IDs this becomes a problem because the last octet of (0) in an IP address is not allowed.  As a result,
   *       we need to make the last octet for camera (0) equal to the maximum number of cameras that are in the system.
   *       This causes the following problems:
   *
   *       1) This tends to break the relationship between XrayCameraArray and each individual camera.  Specifically,
   *          XrayCameraArray knows how many cameras are used for its array type; each individual camera should not
   *          know or care.  Unfortunately, we need to kludge this IP address scheme together due to a lack of
   *          software/hardware design review.
   *
   *       2) If this software is going to support systems with different camera array types, then the maximum
   *          number of cameras is the system will vary.  The kludge stated above will still work for zero-based IDs
   *          but the embedded camera code (bootloader) that generates the IP address will have to change.  Specifically,
   *          the bootloader determines which camera ID it is based on a hardware tag that the camera mounts on to.  This
   *          tag ID will start at (0).  This is consistent with zero-based software camera IDs.  However, when the bootloader
   *          detects an ID of (0), it makes the last octet of its IP address equal to the maximum number of cameras in
   *          the system.  Unfortunately, this is a hardcoded number that is based on the number of cameras for a specific
   *          version of a camera array type.  If another camera array type (more or less cameras) is used for future
   *          systems, then the bootloader has to change its hardcoded maximum value.
   *
   *          This breaks the design of the camera that states that the runtime files that exist on the camera such as
   *          the bootloader are only tied to the camera's board version; not the camera array version.  This will become
   *          a problem in the future.
   *
   * Other issues:
   *
   *       1) It was discovered that, to make the camera IDs one-based, would cause a problem in the Image Reconstruction Processor (IRP)
   *          C++ code.  Specifically, the code can only operate with a zero-based ID scheme that was done for optimmization.
   *
   *       2) For now, we have decided to continue with zero-based camera IDs and kludge the IP address scheme.  If the IRP
   *          code can de-couple the camera ID from its operation then we could make the camera IDs one-based and the IP
   *          address scheme will have a consistent set up.
   *
   *       3) A preferable IP address scheme would involve using DHCP. This would make the camera ID and IP address independent
   *          but will require new and different Java infrastructure to support this.
   *
   * @author Greg Esparza
   */
  private void setCommunication()
  {
    int baseFinalOctet = 0;
    if (getId() == _TCPIP_ADDRESS_BASE_FINAL_OCTET)
      baseFinalOctet = _TCPIP_ADDRESS_BASE_FINAL_OCTET + XrayCameraArray.getNumberOfCameras();
    else
      baseFinalOctet = _TCPIP_ADDRESS_BASE_FINAL_OCTET + getId();

    _serverIpAddress = _TCPIP_ADDRESS_OCTETS[0] +
                       TcpipProperties.getOctectDelimiter() +
                       _TCPIP_ADDRESS_OCTETS[1] +
                       TcpipProperties.getOctectDelimiter() +
                       _TCPIP_ADDRESS_OCTETS[2] +
                       TcpipProperties.getOctectDelimiter() +
                       String.valueOf(baseFinalOctet);

    _portNumber = _TCPIP_PORT_NUMBER;
    _communicationType = new CommunicationType(new TcpipProperties(_serverIpAddress, _portNumber));
    _socketClient = new SocketClient(_serverIpAddress, _portNumber);
  }
  
  /**
   * @author Anthony Fong
   */
  public String getServerIpAddress()
  {
      return _serverIpAddress;
  }

  /**
   * @author Anthony Fong
   */
  public String getServerMacAddress()
  {
      return _serverMacAddress;
  }

  /**
   * @author Greg Esparza
   */
  private boolean doesErrorExist(List<String> replies)
  {
    Assert.expect(replies != null);

    boolean errorExists = false;

    if (replies.isEmpty() == false)
    {
      String reply = replies.get(0).trim();
      reply = reply.toUpperCase();
      if (reply.startsWith(_EMBEDDED_HARDWARE_ERROR_INDICATOR))
        errorExists = true;
    }

    return errorExists;
  }

  /**
   * @author Greg Esparza
   */
  private String getLowLevelErrorMessageLocalizedKey(String[] lowLevelErrorData)
  {
    Assert.expect(lowLevelErrorData != null);
    Assert.expect(lowLevelErrorData.length >= 2);

    return lowLevelErrorData[1].trim();
  }

  /**
   * @author Greg Esparza
   */
  private List<String> getLowLevelErrorMessageLocalizedParameters(String[] lowLevelErrorData)
  {
    Assert.expect(lowLevelErrorData != null);

    List<String> finalParameters = new ArrayList<String>();
    int parametersStartIndex = 2;
    int parametersEndIndex = lowLevelErrorData.length - 1;

    for (int i = parametersStartIndex; i <= parametersEndIndex; i++)
      finalParameters.add(lowLevelErrorData[i]);

    return finalParameters;
  }

  /**
   * @author Greg Esparza
   */
  private void throwEmbeddedHardwareException(String reply) throws XrayTesterException
  {
    Assert.expect(reply != null);
    Assert.expect(reply.length() > 0);

    String[] errorData = reply.split(_EMBEDDED_HARDWARE_ERROR_DATA_DELIMITER);
    Assert.expect(errorData.length >= 2);

    String embeddedHardwareKey = getLowLevelErrorMessageLocalizedKey(errorData);
    List<String> embeddedHardwareParameters = getLowLevelErrorMessageLocalizedParameters(errorData);

    _hardwareTaskEngine.throwHardwareException(XrayCameraHardwareException.getAxiTdiExceptionFromEmbeddedHardwareException(_cameraId,
                                                                                                                               _serverIpAddress,
                                                                                                                               _portNumber,
                                                                                                                               embeddedHardwareKey,
                                                                                                                               embeddedHardwareParameters));
  }

  /**
   * @author Greg Esparza
   */
  private void releaseUnitTestModeAvailablePort() throws XrayTesterException
  {
    try
    {
      _unitTestSocketServerPortFinder.close();
    }
    catch (IOException ioe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToReleaseAvailableUnitTestPortException(_cameraId,
                                                                                                                              _serverIpAddress,
                                                                                                                              _portNumber,
                                                                                                                              ioe.getLocalizedMessage());
      he.initCause(ioe);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private int getUnitTestModeAvailablePort() throws XrayTesterException
  {
    try
    {
      _unitTestSocketServerPortFinder = new ServerSocket(_UNIT_TEST_SOCKET_SERVER_PORT_FINDER_CREATE_SOCKET_ON_ANY_FREE_PORT);
      _portNumber = _unitTestSocketServerPortFinder.getLocalPort();
    }
    catch (IOException ioe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToFindAvailableUnitTestPortException(_cameraId,
                                                                                                                   _serverIpAddress,
                                                                                                                   _portNumber,
                                                                                                                   ioe.getLocalizedMessage());
      he.initCause(ioe);
      throw he;
    }

    return _portNumber;
  }

  /**
   * @author Greg Esparza
   */
  private void setUnitTestModeSocketClient() throws XrayTesterException
  {
    _portNumber = getUnitTestModeAvailablePort();

    try
    {
      InetAddress ipAddress = IPaddressUtil.getRemoteLoopbackAddress();
      _serverIpAddress = ipAddress.getHostName();
      _socketClient = new SocketClient(_serverIpAddress, _portNumber);
    }
    catch (Exception ex)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToSetUnitTestSocketException(_cameraId,
                                                                                                                   _serverIpAddress,
                                                                                                                   _portNumber,
                                                                                                                   ex.getLocalizedMessage());
      he.initCause(ex);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void setUnitTestModeSystemEnvironment()
  {
    _unitTestSystemEnvironmentMap = new HashMap<String,String>(System.getenv());
    _unitTestSystemEnvironmentMap.put(_UNIT_TEST_CAMERA_ID_KEY, String.valueOf(getId()));
    _unitTestSystemEnvironmentMap.put(_UNIT_TEST_PORT_NUMBER_KEY, String.valueOf(_portNumber));
    _unitTestSystemEnvironmentMap.put(_UNIT_TEST_ENABLE_KEY, "true");
  }

  /**
   * @author Greg Esparza
   */
  private List<String> getUnitTestModeSystemEnvironment()
  {
    List<String> environment = new ArrayList<String>();
    for (Map.Entry<String, String> entry : _unitTestSystemEnvironmentMap.entrySet())
    {
      String key = entry.getKey();
      String value = entry.getValue();
      environment.add(key + "=" + value);
    }

    return environment;
  }

  /**
   * @author Greg Esparza
   */
  private void runCameraSimulator(String imageDataTestDirectory) throws XrayTesterException
  {
    Assert.expect(imageDataTestDirectory != null);

    String configFileName = "camera_" + String.valueOf(getId()) + ".cfg";
    String imageDataTestConfigFileNameFullPath = imageDataTestDirectory + File.separator + configFileName;

    if (FileUtil.exists(imageDataTestConfigFileNameFullPath) == false)
      throw XrayCameraHardwareException.getAxiTdiFailedToFindUnitTestSimulatorConfigurationFileException(_cameraId, _serverIpAddress, _portNumber, imageDataTestConfigFileNameFullPath);

    try
    {
      //System.out.println("Camera " + String.valueOf(getId()) + ":" + " Port number: " + String.valueOf(_portNumber));
      String command = _UNIT_TEST_CAMERA_SIMULATOR + " " + configFileName;
      _cameraSimulatorProcess = new LaunchedProcess(command, getUnitTestModeSystemEnvironment(), new File(imageDataTestDirectory));
      releaseUnitTestModeAvailablePort();
    }
    catch (IOException ioe)
    {
      XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToStartUnitTestSimulatorException(_cameraId, _serverIpAddress, _portNumber, ioe.getLocalizedMessage());
      he.initCause(ioe);
      throw he;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void stopCameraSimulator()
  {
    try
    {
      disconnectSocketClient();
    }
    catch (XrayTesterException xte)
    {
      // if we cannot disconnect gracefully, we will still continue to stop the simulator process
    }

    if (_cameraSimulatorProcess != null)
      _cameraSimulatorProcess.destroy(_UNIT_TEST_STOP_CAMERA_SIMULATOR_WAIT_TIMEOUT_IN_MILLISECONDS,
                                      _UNIT_TEST_STOP_CAMERA_SIMULATOR_WAIT_FOR_FLUSH_TIMEOUT_IN_MILLISECONDS);
    _cameraSimulatorProcess = null;
    _socketClient = null;
  }

  /**
   * @author Greg Esparza
   */
  private static synchronized boolean isRuntimeFileInformationReady()
  {
    return _runtimeFileInformationReady;
  }

  /**
   * @author Greg Esparza
   */
  private void disconnectSocketClient() throws XrayTesterException
  {
    if (isSimulationModeOn() == false)
    {
      try
      {
        if (_socketClient.isConnected())
          _socketClient.close();
      }
      catch (IOException ioe)
      {
        XrayCameraHardwareException he = XrayCameraHardwareException.getAxiTdiFailedToDisconnectException(_cameraId,
                                                                                                              _serverIpAddress,
                                                                                                              _portNumber,
                                                                                                              ioe.getLocalizedMessage());
        he.initCause(ioe);
        throw he;
      }
    }
  }

  /**
   * When we encounter an error sending a command to the remote camera we will try and disconnect the socket client.
   * This will ensure that subsequent commands to the remote camera will not return residual data from the previous,
   * error-related command.  If the disconnect fails we do not want to throw exception because we are ready to throw
   * an exception due to the initial error.
   *
   * @author Greg Esparza
   */
  private void exceptionDisconnect()
  {
    try
    {
      disconnectSocketClient();
    }
    catch (XrayTesterException xte)
    {
      // we will try and disconnect to clean up from an exception
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private void debugList(List<String> messageList)
  {
    for (String message : messageList)
    {
      debug(message);
    }
  }
  /**
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    if(_listOfCamerasToDebug == null)
    {
      _listOfCamerasToDebug = new ArrayList<Integer>();

      SoftwareConfigEnum softwareConfigCameraListEnum = SoftwareConfigEnum.CAMERA_DEBUG_COMMAND_LIST_OF_CAMERAS_TO_SHOW;
      //Get the value from the config file
      String cameraList = _config.getStringValue(softwareConfigCameraListEnum);

      //It is ',' delimited, so split it into pieces
      String[] splitCameraList = cameraList.split(",");

      //For every individual camera string, create an Integer and add it to the Set
      for (String cameraNumberString : splitCameraList)
      {
        // The string should be just a number but let's trim it to be
        // forgiving of extra spaces.
        try
        {
          //try to parse the string value to get the camera number
          int cameraId = StringUtil.convertStringToInt(cameraNumberString.trim());
          _listOfCamerasToDebug.add(cameraId);
        }
        //Handle a parse exception
        catch (BadFormatException ex)
        {
          //if bad format detected, set it to default camera 0
          _listOfCamerasToDebug.clear();
          _listOfCamerasToDebug.add(0);
          break;
        }
      }
    }
    if (_listOfCamerasToDebug.contains(getId()))
    {
      try
      {
        CameraCommandRepliesLogUtil.getInstance().log(message);
      }
      catch (XrayTesterException e)
      {
        System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
      }
    }
  }
}
