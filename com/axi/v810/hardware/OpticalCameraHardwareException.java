package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraHardwareException extends HardwareException
{
  private OpticalCameraHardwareExceptionEnum _exceptionType;

  /**
   * @author Cheah Lee Herng
   */
  private OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum exceptionType, String key, List<String> parameters)
  {
    super(new LocalizedString(key, parameters.toArray()));

    Assert.expect(exceptionType != null);
    _exceptionType = exceptionType;
  }

  /**
   * @author Cheah Lee Herng
   */
  public OpticalCameraHardwareExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }

  /**
   * @author Cheah Lee Herng
   */
  private static synchronized String getDetailMessage(String originalDetails)
  {
    String finalDetails = null;

    if (originalDetails == null)
      finalDetails = StringLocalizer.keyToString(new LocalizedString("HW_OPTICAL_CAMERA_LOW_LEVEL_MESSAGE_NOT_AVAILABLE_KEY", null));
    else
      finalDetails = originalDetails;

    return finalDetails;
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToInitialize(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_INITIALIZE,
                                              "HW_OPTICAL_CAMERA_INITIALIZE_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToStop(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_STOP,
                                              "HW_OPTICAL_CAMERA_STOP_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetWindowHandle(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_WINDOW_HANDLE,
                                              "HW_OPTICAL_CAMERA_GET_WINDOW_HANDLE_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToShowLiveVideo(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_SHOW_LIVE_VIDEO,
                                              "HW_OPTICAL_CAMERA_LIVE_VIDEO_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToDrawImage(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_DRAW_IMAGE,
                                              "HW_OPTICAL_CAMERA_DRAW_IMAGE_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToEnableHardwareTrigger(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_ENABLE_HARDWARE_TRIGGER,
                                              "HW_OPTICAL_CAMERA_ENABLE_HARDWARE_TRIGGER_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToAcquireOfflineImages(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_ACQUIRE_OFFLINE_IMAGES,
                                              "HW_OPTICAL_CAMERA_ACQUIRE_OFFLINE_IMAGES_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToFreezeVideo(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_FREEZE_VIDEO,
                                              "HW_OPTICAL_CAMERA_FREEZE_VIDEO_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetNumberOfConnectedCameras(String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_NUMBER_OF_CONNECTED_CAMERAS,
                                              "HW_OPTICAL_CAMERA_GET_NUMBER_OF_CONNECTED_CAMERAS_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToEnableAutoGain(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_ENABLE_AUTO_GAIN,
                                              "HW_OPTICAL_CAMERA_ENABLE_AUTO_GAIN_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToEnableAutoSensorGain(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_ENABLE_AUTO_SENSOR_GAIN,
                                              "HW_OPTICAL_CAMERA_ENABLE_AUTO_SENSOR_GAIN_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetIsGainBoostSupported(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_IS_GAIN_BOOST_SUPPORTED,
                                              "HW_OPTICAL_CAMERA_GET_IS_GAIN_BOOST_SUPPORTED_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToSetGainBoost(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_SET_GAIN_BOOST,
                                              "HW_OPTICAL_CAMERA_SET_GAIN_BOOST_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetMasterGainFactor(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_MASTER_GAIN_FACTOR,
                                              "HW_OPTICAL_CAMERA_GET_MASTER_GAIN_FACTOR_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToSetMasterGainFactor(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_SET_MASTER_GAIN_FACTOR,
                                              "HW_OPTICAL_CAMERA_SET_MASTER_GAIN_FACTOR_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetFirmwareVersion(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_FIRMWARE_VERSION,
                                              "HW_OPTICAL_CAMERA_GET_FIRMWARE_VERSION_ERROR_KEY",
                                              exceptionParameters);
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3813: Provide Sentech Optical Camera description details in Optical Camera tab
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetFpgaVersion(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_FPGA_VERSION,
                                              "HW_OPTICAL_CAMERA_GET_FPGA_VERSION_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetSerialNumber(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_SERIAL_NUMBER,
                                              "HW_OPTICAL_CAMERA_GET_SERIAL_NUMBER_ERROR_KEY",
                                              exceptionParameters);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetModel(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_MODEL,
                                              "HW_OPTICAL_CAMERA_GET_MODEL_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetManufacturer(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_MANUFACTURER,
                                              "HW_OPTICAL_CAMERA_GET_MANUFACTURER_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetFinalQualityCheckDate(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_FINAL_QUALITY_CHECK,
                                              "HW_OPTICAL_CAMERA_GET_FINAL_QUALITY_CHECK_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToGetFramesPerSecond(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_GET_FRAMES_PER_SECOND,
                                              "HW_OPTICAL_CAMERA_GET_FRAMES_PER_SECOND_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToSetFramesPerSecond(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_SET_FRAMES_PER_SECOND,
                                              "HW_OPTICAL_CAMERA_SET_FRAMES_PER_SECOND_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Cheah Lee Herng
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToSetMagnificationData(int cameraId, int deviceId, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_SET_MAGNIFICATION_DATA,
                                              "HW_OPTICAL_CAMERA_SET_MAGNIFICATION_DATA_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Ying-Huan.Chu
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToAcquireCalibrationImages(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_ACQUIRE_CALIBRATION_IMAGES,
                                              "HW_OPTICAL_CAMERA_ACQUIRE_CALIBRATION_IMAGES_ERROR_KEY",
                                              exceptionParameters);
  }

  /**
   * @author Ying-Huan.Chu
   */
  static synchronized OpticalCameraHardwareException getAxiOpticalFailedToPerformCalibration(int cameraId, int deviceId, int errorCode, String errorName, String details)
  {
    List<String> exceptionParameters = new ArrayList<String>();
    exceptionParameters.add(String.valueOf(cameraId));
    exceptionParameters.add(String.valueOf(deviceId));
    exceptionParameters.add(String.valueOf(errorCode));
    exceptionParameters.add(errorName);
    exceptionParameters.add(getDetailMessage(details));

    return new OpticalCameraHardwareException(OpticalCameraHardwareExceptionEnum.AXI_OPTICAL_FAILED_TO_PERFORM_CALIBRATION,
                                              "HW_OPTICAL_CAMERA_PERFORM_CALIBRATION_ERROR_KEY",
                                              exceptionParameters);
  }
}
