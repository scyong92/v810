package com.axi.v810.hardware;

import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
class WaitForPanelClearSensorToBlockThreadTask extends ThreadTask<Object>
{
  private static final boolean _WAIT_NOT_ABORTABLE = false;

  private PanelHandler _panelHandler;
  private PanelClearSensor _panelClearSensor;
  private StageBelts _stageBelts;
  private long _waitTimeoutInMilliSeconds;
  private BlockingQueue<Object> _stageBeltLockingQueue;
  private boolean _leaveStageBeltOn = false;

  /**
   * @author Rex Shang
   */
  WaitForPanelClearSensorToBlockThreadTask(PanelClearSensor panelClearSensor,
                                           BlockingQueue<Object> stageBeltLockingQueue,
                                           long waitTimeoutInMilliSeconds,
                                           boolean leaveStageBeltsOn)

  {
    super("Wait for panel clear sensor to block thread task");
    Assert.expect(panelClearSensor != null);

    _stageBeltLockingQueue = stageBeltLockingQueue;
    _panelHandler = PanelHandler.getInstance();
    _panelClearSensor = panelClearSensor;
    _stageBelts = StageBelts.getInstance();
    _waitTimeoutInMilliSeconds = waitTimeoutInMilliSeconds;
    _leaveStageBeltOn = leaveStageBeltsOn;
  }

  /**
   * @author Rex Shang
   */
  protected Object executeTask() throws XrayTesterException
  {
    try
    {
      // Set current thread to top priorty in case there are a lot of threads running
      // and we need to stop the stage belt.
      Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

      // Make sure we are waiting first before the belt is turned on on the other thread.
      boolean isOfferSuccessful = false;
      while (isOfferSuccessful == false)
      {
        isOfferSuccessful = _stageBeltLockingQueue.offer(_stageBelts);
      }

      // Wait until the Panel Clear sensor to block so we "know" the panel is exiting.
      // NOTE: the wait is not abort-able since we don't want to loose the panel.
      _panelClearSensor.waitUntilBlocked(_waitTimeoutInMilliSeconds,
                                         DigitalIo.getFastHardwarePollingIntervalInMilliSeconds(),
                                         _WAIT_NOT_ABORTABLE);

    }
    finally
    {
      // Stop belt
      if (_leaveStageBeltOn == false)
        _stageBelts.off();
    }

    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelClearSensor.abort();
    _panelHandler.abort();
    _stageBelts.abort();
  }
}
