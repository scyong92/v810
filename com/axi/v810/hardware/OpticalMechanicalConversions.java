package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is used for unit conversion related to optical images processing.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalMechanicalConversions 
{    
    private static Config _config = Config.getInstance();
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalMechanicalConversions()
    {
        // Do nothing
    }
    
    /**
     * This function will convert an input of PanelCoordinate into StagePosition
     * with respect to Optical Fiducial.
     * 
     * Some explanation on how we get the coordinate conversion working.
     * 
     * We need the following items in order to do the converrsion:
     * 1) SystemFiducialCoordinate for OpticalFiducial
     * 2) OpticalFiducial stage position (when calibration jig is positioned agains left PIP)
     * 3) SystemFiducialCoordinate for component/region
     * 4) Distance difference between OpticalFiducial SystemFiducialCoordinate and component SystemFiducialCoordinate
     * 
     * Overall diagram of Optical Fiducial in stage:
     * 
     * 
     * 
     *         ************************
     *         *    *   *    +  *  *  *
     *         *    *   *   /|  *  *  *
     *         *    *   * +--+  *  *  *
     *         *    *   *       *  *  *
     *         ************************
     *         *                      *
     *         *  *******    *******  *
     *         *  *     *    *     *  *                                              
     *         *  *******    *******  *
     *         *
     *         *  *******    *******  *                                              
     *   |----|*  *     *    *     *  *                                              |-----|
     *   |LEFT|*  *******    *******  *                                              |Right|
     *   |PIP |*                      *                                              |PIP  |
     *   |----|************************                                              |-----|
     * ------------------------------------------------------------------------------------------
     * |                                                            *                           |
     * |                                                           **                           |
     * |                                                          ***                           |
     * ------------------------------------------------------------------------------------------
     * 
     * @author Cheah Lee Herng
     */
    public static StagePosition convertPanelCoordinateToStageCoordinate(com.axi.v810.business.testProgram.TestSubProgram testSubProgram, 
                                                                        PanelCoordinate panelCoordinateInNanometers,
                                                                        java.awt.geom.AffineTransform aggregateRunTimeAlignmentAffineTransform,
                                                                        com.axi.v810.business.testProgram.PanelLocationInSystemEnum panelLocationInSystemEnum,
                                                                        int opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                        int opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                        int opticalFiducialStageCoordinateLocationXInNanometers,
                                                                        int opticalFiducialStageCoordinateLocationYInNanometers,
                                                                        boolean useCameraOne)
    {
        Assert.expect(testSubProgram != null);
        Assert.expect(panelCoordinateInNanometers != null);
        Assert.expect(aggregateRunTimeAlignmentAffineTransform != null);
        Assert.expect(panelLocationInSystemEnum != null);
        
        int opticalCameraSpotXStagePositionInNanometers = opticalFiducialStageCoordinateLocationXInNanometers;
        int opticalCameraSpotYStagePositionInNanometers = opticalFiducialStageCoordinateLocationYInNanometers;
        
        Assert.expect(opticalCameraSpotXStagePositionInNanometers > 0);
        Assert.expect(opticalCameraSpotYStagePositionInNanometers > 0);
        
        // Next, we figure out the SystemFiducialCoordinate of component, depending on PanelLocationInSystemEnum, 
        // which is defined in aggregateRunTimeAlignmentAffineTransform from TestSubProgram
        IntCoordinate componentSystemFiducialPosition = MechanicalConversions.transform((int)panelCoordinateInNanometers.getX(), 
                                                                                        (int)panelCoordinateInNanometers.getY(), 
                                                                                        aggregateRunTimeAlignmentAffineTransform);        

        int componentSystemFiducialLocationXInNanometers = componentSystemFiducialPosition.getX();
        int componentSystemFiducialLocationYInNanometers = componentSystemFiducialPosition.getY();
        
        // Calculate component OpticalFiducialCoordinate
        int componentXOpticalFiducialLocation = componentSystemFiducialLocationXInNanometers - opticalFiducialSystemCoordinateLocationXInNanometers;
        int componentYOpticalFiducialLocation = componentSystemFiducialLocationYInNanometers - opticalFiducialSystemCoordinateLocationYInNanometers;                        
        
        // Adjust OpticalFiducial System Fiducial coordinate and OpticalFiducial stage position with stage hysteresis value
        int xAxisHysteresisOffsetInNanometers = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
        int yAxisHysteresisOffsetInNanometers = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);
        
        // Calculate component StageCoordinate based on PSP Coupon location
        double stagePositionXInNanometers = opticalCameraSpotXStagePositionInNanometers - componentXOpticalFiducialLocation;
        double stagePositionYInNanometers = opticalCameraSpotYStagePositionInNanometers - componentYOpticalFiducialLocation;
        
        stagePositionXInNanometers -= xAxisHysteresisOffsetInNanometers;
        stagePositionYInNanometers -= yAxisHysteresisOffsetInNanometers;
        
        StagePosition stagePosition = new StagePositionMutable();
        stagePosition.setXInNanometers((int)stagePositionXInNanometers);
        stagePosition.setYInNanometers((int)stagePositionYInNanometers);
        
        return stagePosition;
    }
    
    /**
     * @param testSubProgram
     * @param panelCoordinateInNanometers
     * @param aggregateRunTimeAlignmentAffineTransform
     * @param panelLocationInSystemEnum
     * @param opticalFiducialSystemCoordinateLocationXInNanometers
     * @param opticalFiducialSystemCoordinateLocationYInNanometers
     * @param opticalFiducialStageCoordinateLocationXInNanometers
     * @param opticalFiducialStageCoordinateLocationYInNanometers
     * @param useCameraOne
     * @return StagePosition Converted StageCoordinate
     * 
     * @author Cheah Lee Herng
     */
    public static StagePosition convertPanelCoordinateToStageCoordinate(com.axi.v810.business.testProgram.TestSubProgram testSubProgram, 
                                                                            PanelCoordinate panelCoordinateInNanometers,
                                                                            java.awt.geom.AffineTransform aggregateRunTimeAlignmentAffineTransform,
                                                                            com.axi.v810.business.testProgram.PanelLocationInSystemEnum panelLocationInSystemEnum,
                                                                            int opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                            int opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                            int opticalFiducialStageCoordinateLocationXInNanometers,
                                                                            int opticalFiducialStageCoordinateLocationYInNanometers,
                                                                            BooleanRef useCameraOne)
    {
      Assert.expect(testSubProgram != null);
      Assert.expect(panelCoordinateInNanometers != null);
      Assert.expect(aggregateRunTimeAlignmentAffineTransform != null);
      Assert.expect(panelLocationInSystemEnum != null);
      Assert.expect(useCameraOne != null);
      
      // XCR-3192 System crash when generate fine tuning image with surface map point
      com.axi.v810.business.panelDesc.Panel panel = testSubProgram.getTestProgram().getProject().getPanel();
      com.axi.v810.business.panelSettings.PanelSettings panelSettings = panel.getPanelSettings();
      PanelRectangle bottomSectionOfOpticalLongPanelPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();

      // Get the stage position of OpticalFiducial under Camera 1 or Camera 2
      if (bottomSectionOfOpticalLongPanelPanelRectangle.contains((int)panelCoordinateInNanometers.getX(), (int)panelCoordinateInNanometers.getY()))
        useCameraOne.setValue(true);
      else    
        useCameraOne.setValue(false);
      
      return convertPanelCoordinateToStageCoordinate(testSubProgram, 
                                                     panelCoordinateInNanometers, 
                                                     aggregateRunTimeAlignmentAffineTransform, 
                                                     panelLocationInSystemEnum,
                                                     opticalFiducialSystemCoordinateLocationXInNanometers,
                                                     opticalFiducialSystemCoordinateLocationYInNanometers,
                                                     opticalFiducialStageCoordinateLocationXInNanometers,
                                                     opticalFiducialStageCoordinateLocationYInNanometers,
                                                     useCameraOne.getValue());
    }
    
  /**
   * Convert PanelCoordinate to Screen Pixel without considering Alignment AffineTransform, Stage Hysteresis.
   * This is direct translate from Point nanometer to Point Pixel.
   * 
   * @author Ying-Huan.Chu
   */
  public static java.awt.Point convertPanelCoordinateInNanometerToNonAdjustedPixel(String opticalCameraRectangleName, PanelCoordinate pointPanelCoordinate)
  {
    int xCoordinateInNanometer = pointPanelCoordinate.getX();
    int yCoordinateInNanometer = pointPanelCoordinate.getY();
    double nanometerToPixelXMultiplier = (double)PspHardwareEngine.getInstance().getImageWidth() / (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    double nanometerToPixelYMultiplier = (double)PspHardwareEngine.getInstance().getImageHeight() / (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
    int opticalCameraRectangleMinX = Integer.parseInt(opticalCameraRectangleName.split("_")[0]);
    int opticalCameraRectangleMinY = Integer.parseInt(opticalCameraRectangleName.split("_")[1]);

    int roundedXCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt((xCoordinateInNanometer - opticalCameraRectangleMinX) * nanometerToPixelXMultiplier),0);
    int roundedYCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt((yCoordinateInNanometer - opticalCameraRectangleMinY) * nanometerToPixelYMultiplier),0);

    return new java.awt.Point(roundedXCoordinateInPixel, roundedYCoordinateInPixel);
  }
  
  /**
   * Convert PanelCoordinate to Screen Pixel by considering Alignment AffineTransform, Stage Hysteresis.
   * 
   * @param testSubProgram
   * @param opticalCameraRectangleName
   * @param pointPanelCoordinate
   * @param aggregateRunTimeAlignmentAffineTransform
   * @param opticalFiducialSystemCoordinateLocationXInNanometers
   * @param opticalFiducialSystemCoordinateLocationYInNanometers
   * @param opticalFiducialStageCoordinateLocationXInNanometers
   * @param opticalFiducialStageCoordinateLocationYInNanometers
   * @return 
   */
  public static java.awt.Point convertPanelCoordinateInNanometerToAdjustedPixel(com.axi.v810.business.testProgram.TestSubProgram testSubProgram,
                                                                                String opticalCameraRectangleName,
                                                                                PanelCoordinate pointPanelCoordinate,
                                                                                java.awt.geom.AffineTransform aggregateRunTimeAlignmentAffineTransform,
                                                                                int opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                                int opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                                int opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                int opticalFiducialStageCoordinateLocationYInNanometers)
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(opticalCameraRectangleName != null);
    Assert.expect(pointPanelCoordinate != null);
    Assert.expect(aggregateRunTimeAlignmentAffineTransform != null);
    
    double nanometerToPixelXMultiplier = (double)PspHardwareEngine.getInstance().getImageWidth() / (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    double nanometerToPixelYMultiplier = (double)PspHardwareEngine.getInstance().getImageHeight() / (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
            
    // XCR-3192 System crash when generate fine tuning image with surface map point
    // Get stage coordinate for OpticalRectangle Min X, Min Y
    int opticalCameraRectangleMinX = Integer.parseInt(opticalCameraRectangleName.split("_")[0]);
    int opticalCameraRectangleMinY = Integer.parseInt(opticalCameraRectangleName.split("_")[1]);
    int opticalCameraRectangleWidth = Integer.parseInt(opticalCameraRectangleName.split("_")[2]);
    int opticalCameraRectangleHeight = Integer.parseInt(opticalCameraRectangleName.split("_")[3]);
    
    // Get OpticalRectangle Center X, Center Y
    int opticalCameraRectangleCenterX = MathUtil.roundDoubleToInt((opticalCameraRectangleMinX + opticalCameraRectangleWidth * 0.5));
    int opticalCameraRectangleCenterY = MathUtil.roundDoubleToInt((opticalCameraRectangleMinY + opticalCameraRectangleHeight * 0.5));
    
    com.axi.v810.business.panelDesc.Panel panel = testSubProgram.getTestProgram().getProject().getPanel();
    com.axi.v810.business.panelSettings.PanelSettings panelSettings = panel.getPanelSettings();
    PanelRectangle bottomSectionOfOpticalLongPanelPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();

    BooleanRef useCameraOne = new BooleanRef(false);
    
    // Get the stage position of OpticalFiducial under Camera 1 or Camera 2
    if (bottomSectionOfOpticalLongPanelPanelRectangle.contains(opticalCameraRectangleCenterX, opticalCameraRectangleCenterY))
      useCameraOne.setValue(true);
    else
      useCameraOne.setValue(false);
    
    // Convert Point PanelCoordinate to MachineCoordinate
    StagePosition pointStagePosition = convertPanelCoordinateToStageCoordinate(testSubProgram,
                                                                               pointPanelCoordinate,
                                                                               aggregateRunTimeAlignmentAffineTransform,
                                                                               testSubProgram.getPanelLocationInSystem(),
                                                                               opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                               opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                               opticalFiducialStageCoordinateLocationXInNanometers,
                                                                               opticalFiducialStageCoordinateLocationYInNanometers,
                                                                               useCameraOne.getValue());
    
    PanelCoordinate minXMinYOpticalRectPanelCoordinate = new PanelCoordinate(opticalCameraRectangleMinX, opticalCameraRectangleMinY);
    StagePosition minXMinYOpticalRectStagePosition = convertPanelCoordinateToStageCoordinate(testSubProgram,
                                                                                             minXMinYOpticalRectPanelCoordinate,
                                                                                             aggregateRunTimeAlignmentAffineTransform,
                                                                                             testSubProgram.getPanelLocationInSystem(),
                                                                                             opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                                             opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                                             opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                             opticalFiducialStageCoordinateLocationYInNanometers,
                                                                                             useCameraOne.getValue());
    
    // Get adjusted Optical Rectangle offset    
    int roundedXCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt(Math.abs(pointStagePosition.getYInNanometers() - minXMinYOpticalRectStagePosition.getYInNanometers()) * nanometerToPixelXMultiplier),0);
    int roundedYCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt(Math.abs(pointStagePosition.getXInNanometers() - minXMinYOpticalRectStagePosition.getXInNanometers()) * nanometerToPixelYMultiplier),0);
    
    return new java.awt.Point(roundedXCoordinateInPixel, roundedYCoordinateInPixel);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static java.awt.Point convertPanelCoordinateInNanometerToNonAdjustedPixel(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle, PanelCoordinate pointPanelCoordinate)
  {
    int xCoordinateInNanometer = pointPanelCoordinate.getX();
    int yCoordinateInNanometer = pointPanelCoordinate.getY();
    double nanometerToPixelXMultiplier = (double)PspHardwareEngine.getInstance().getImageWidth() / (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    double nanometerToPixelYMultiplier = (double)PspHardwareEngine.getInstance().getImageHeight() / (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
    int opticalCameraRectangleMinX = opticalCameraRectangle.getRegion().getMinX();
    int opticalCameraRectangleMinY = opticalCameraRectangle.getRegion().getMinY();

    int roundedXCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt((xCoordinateInNanometer - opticalCameraRectangleMinX) * nanometerToPixelXMultiplier),0);
    int roundedYCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt((yCoordinateInNanometer - opticalCameraRectangleMinY) * nanometerToPixelYMultiplier),0);

    return new java.awt.Point(roundedXCoordinateInPixel, roundedYCoordinateInPixel);
  }
  
  /**
   * Convert PanelCoordinate to Screen Pixel by considering Alignment AffineTransform, Stage Hystereris.
   * 
   * @param testSubProgram
   * @param opticalCameraRectangle
   * @param pointPanelCoordinate
   * @param aggregateRunTimeAlignmentAffineTransform
   * @param opticalFiducialSystemCoordinateLocationXInNanometers
   * @param opticalFiducialSystemCoordinateLocationYInNanometers
   * @param opticalFiducialStageCoordinateLocationXInNanometers
   * @param opticalFiducialStageCoordinateLocationYInNanometers
   * @return 
   */
  public static java.awt.Point convertPanelCoordinateInNanometerToAdjustedPixel(com.axi.v810.business.testProgram.TestSubProgram testSubProgram,
                                                                                com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle, 
                                                                                PanelCoordinate pointPanelCoordinate,
                                                                                java.awt.geom.AffineTransform aggregateRunTimeAlignmentAffineTransform,
                                                                                int opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                                int opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                                int opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                int opticalFiducialStageCoordinateLocationYInNanometers)
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(pointPanelCoordinate != null);
    Assert.expect(aggregateRunTimeAlignmentAffineTransform != null);
    
    double nanometerToPixelXMultiplier = (double)PspHardwareEngine.getInstance().getImageWidth() / (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    double nanometerToPixelYMultiplier = (double)PspHardwareEngine.getInstance().getImageHeight() / (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
    
    // XCR-3192 System crash when generate fine tuning image with surface map point
    com.axi.v810.business.panelDesc.Panel panel = testSubProgram.getTestProgram().getProject().getPanel();
    com.axi.v810.business.panelSettings.PanelSettings panelSettings = panel.getPanelSettings();
    PanelRectangle bottomSectionOfOpticalLongPanelPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();

    BooleanRef useCameraOne = new BooleanRef(false);
    
    // Get the stage position of OpticalFiducial under Camera 1 or Camera 2
    if (bottomSectionOfOpticalLongPanelPanelRectangle.contains(opticalCameraRectangle.getRegion().getCenterX(), opticalCameraRectangle.getRegion().getCenterY()))
      useCameraOne.setValue(true);
    else
      useCameraOne.setValue(false);
    
    // Convert Point PanelCoordinate to MachineCoordinate
    StagePosition pointStagePosition = convertPanelCoordinateToStageCoordinate(testSubProgram,
                                                                               pointPanelCoordinate,
                                                                               aggregateRunTimeAlignmentAffineTransform,
                                                                               testSubProgram.getPanelLocationInSystem(),
                                                                               opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                               opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                               opticalFiducialStageCoordinateLocationXInNanometers,
                                                                               opticalFiducialStageCoordinateLocationYInNanometers,
                                                                               useCameraOne.getValue());
    
    // Get stage coordinate for OpticalRectangle Min X, Min Y
    PanelCoordinate minXMinYOpticalRectPanelCoordinate = new PanelCoordinate(opticalCameraRectangle.getRegion().getMinX(), opticalCameraRectangle.getRegion().getMinY());
    StagePosition minXMinYOpticalRectStagePosition = convertPanelCoordinateToStageCoordinate(testSubProgram,
                                                                                             minXMinYOpticalRectPanelCoordinate,
                                                                                             aggregateRunTimeAlignmentAffineTransform,
                                                                                             testSubProgram.getPanelLocationInSystem(),
                                                                                             opticalFiducialSystemCoordinateLocationXInNanometers,
                                                                                             opticalFiducialSystemCoordinateLocationYInNanometers,
                                                                                             opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                             opticalFiducialStageCoordinateLocationYInNanometers,
                                                                                             useCameraOne.getValue());
        
    // Get adjusted Optical Rectangle offset    
    int roundedXCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt(Math.abs(pointStagePosition.getYInNanometers() - minXMinYOpticalRectStagePosition.getYInNanometers()) * nanometerToPixelXMultiplier),0);
    int roundedYCoordinateInPixel = Math.max(MathUtil.roundDoubleToInt(Math.abs(pointStagePosition.getXInNanometers() - minXMinYOpticalRectStagePosition.getXInNanometers()) * nanometerToPixelYMultiplier),0);
    
    return new java.awt.Point(roundedXCoordinateInPixel, roundedYCoordinateInPixel);
  }
  
  /**
   * This function is used to convert the pixel coordinates in PanelSurfaceMapSettings / BoardSurfaceMapSettings 
   * in earlier versions (1,2,3) to panel coordinates in nanometer.
   * 
   * @author Ying-Huan.Chu
   */
  public static java.awt.Point convertPixelCoordinateToPanelCoordinateInNanometer(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle, java.awt.Point pixelCoordinate)
  {
    Assert.expect(pixelCoordinate != null);
    
    int xCoordinateInPixel = (int) pixelCoordinate.getX();
    int yCoordinateInPixel = (int) pixelCoordinate.getY();
    
    double pixelToNanometerXMultiplier = (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer() / (double)PspHardwareEngine.getInstance().getImageWidth();
    double pixelToNanometerYMultiplier = (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer() / (double)PspHardwareEngine.getInstance().getImageHeight();

    int roundedXCoordinateInNanometer = Math.max(MathUtil.roundDoubleToInt(opticalCameraRectangle.getRegion().getMinX() + (xCoordinateInPixel * pixelToNanometerXMultiplier)),0);
    int roundedYCoordinateInNanometer = Math.max(MathUtil.roundDoubleToInt(opticalCameraRectangle.getRegion().getMinY() + (yCoordinateInPixel * pixelToNanometerYMultiplier)),0);
    
    return new java.awt.Point(roundedXCoordinateInNanometer, roundedYCoordinateInNanometer);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static java.awt.Point convertPixelCoordinateToPanelCoordinateInNanometer(double xCoordinateInPixel, double yCoordinateInPixel, com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle)
  {
    double pixelToNanometerXMultiplier = (double)PspHardwareEngine.getInstance().getActualImageWidthInNanometer() / (double)PspHardwareEngine.getInstance().getImageWidth();
    double pixelToNanometerYMultiplier = (double)PspHardwareEngine.getInstance().getActualImageHeightInNanometer() / (double)PspHardwareEngine.getInstance().getImageHeight();
    
    java.awt.Rectangle rectangleInNanometer = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    
    double xOffsetInNanometer = xCoordinateInPixel * pixelToNanometerXMultiplier;
    double yOffsetInNanometer = yCoordinateInPixel * pixelToNanometerYMultiplier;
    
    int roundedXCoordinateInNanometer = Math.max(MathUtil.roundDoubleToInt(rectangleInNanometer.getMinX() + xOffsetInNanometer),0);
    int roundedYCoordinateInNanometer = Math.max(MathUtil.roundDoubleToInt(rectangleInNanometer.getMinY() + yOffsetInNanometer),0);
    
    java.awt.Point coordinateInNanometers = new java.awt.Point(roundedXCoordinateInNanometer, roundedYCoordinateInNanometer);
    
    return coordinateInNanometers;
  }
  
  /**
   * This function is used to convert the OpticalCameraRectangle in PanelSurfaceMapSettings / BoardSurfaceMapSettings in earlier versions (1,2,3)
   * 
   * @author Ying-Huan.Chu
   */
  public static com.axi.v810.business.panelSettings.OpticalCameraRectangle convertOldOpticalCameraRectangleToNewOpticalCameraRectangle(com.axi.v810.business.panelSettings.OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    
    int opticalCameraRectangleWidthInNanometer = PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    int opticalCameraRectangleHeightInNanometer = PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
    
    int newXInNanometer = Math.max(opticalCameraRectangle.getRegion().getCenterX() - (opticalCameraRectangleWidthInNanometer / 2),0);
    int newYInNanometer = Math.max(opticalCameraRectangle.getRegion().getCenterY() - (opticalCameraRectangleHeightInNanometer / 2),0);
    
    opticalCameraRectangle.setRegion(newXInNanometer, newYInNanometer, opticalCameraRectangleWidthInNanometer, opticalCameraRectangleHeightInNanometer);
    return opticalCameraRectangle;
  }
}
