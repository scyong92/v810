package com.axi.v810.hardware;

import java.io.*;
import java.util.*;
import com.axi.util.*;

/**
 * NativeHardwareException is thrown from the native side and it has to be
 * caught in the hardware package.
 * This entire class is package scoped because it can never be referenced or
 * created outside of the hardware package.
 * @author Rex Shang
 */
public class NativeHardwareException extends Exception implements Serializable
{
  private String _key;
  private List<String> _parameters;

  /**
   * The constructor is package scoped so that it can only be used in hardware
   * package.
   * @param key unique string used to look up the localization message.
   * @param parameters List of string to be placed into localization message.
   * @author Rex Shang
   */
  NativeHardwareException(String key, List<String> parameters)
  {
    Assert.expect(key != null);
    Assert.expect(parameters != null);

    _key = key;
    _parameters = parameters;
  }

  /**
   * @author Rex Shang
   */
  String getKey()
  {
    return _key;
  }

  /**
   * @author Rex Shang
   */
  List<String> getParameters()
  {
    return _parameters;
  }

}
