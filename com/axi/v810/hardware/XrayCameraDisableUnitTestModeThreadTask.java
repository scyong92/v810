package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraDisableUnitTestModeThreadTask extends ThreadTask<Object>
{
  private AbstractXrayCamera _xRayCamera;

  /**
   * @author Greg Esparza
   */
  public XrayCameraDisableUnitTestModeThreadTask(AbstractXrayCamera xRayCamera)
  {
    super("Xray Camera disable unit test mode thread task.");

    Assert.expect(xRayCamera != null);

    _xRayCamera = xRayCamera;
  }

  /**
   * @author Greg Esparza
   */
  public void cancel() throws Exception
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public Object executeTask() throws Exception
  {
    _xRayCamera.disableUnitTestMode();
    return null;
  }
}
