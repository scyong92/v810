package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * If a timeout occurs for any reason, this exception is thrown.
 *
 * @author Rex Shang
 */
public class HardwareTimeoutException extends HardwareException
{
  HardwareTimeoutExceptionEnum _hardwareTimeoutExceptionEnum;
  
  public HardwareTimeoutException(HardwareTimeoutExceptionEnum hardwareTimeoutExceptionEnum)
  {
    super(HardwareTimeoutExceptionEnum.getLocalizedString(hardwareTimeoutExceptionEnum));
    Assert.expect(hardwareTimeoutExceptionEnum != null);
    
    _hardwareTimeoutExceptionEnum = hardwareTimeoutExceptionEnum;
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public HardwareTimeoutExceptionEnum getExceptionEnum()
  {
    return _hardwareTimeoutExceptionEnum;
  }

  /**
   * @author Rex Shang
   */
  public String getLocalizedMessage()
  {
    String finalMessage = null;
    LocalizedString header = new LocalizedString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY", null);
    finalMessage = StringLocalizer.keyToString(header);
    String mainMessageBody = StringLocalizer.keyToString(getLocalizedString());
    finalMessage += mainMessageBody;
    LocalizedString companyContactInformation = new LocalizedString("GUI_COMPANY_CONTACT_INFORMATION_KEY", null);
    finalMessage += StringLocalizer.keyToString(companyContactInformation);

    return finalMessage;
  }

}
