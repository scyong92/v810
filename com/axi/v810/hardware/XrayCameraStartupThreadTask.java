package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraStartupThreadTask extends ThreadTask<Object>
{
  private AbstractXrayCamera _xRayCamera;

  /**
   * @author Greg Esparza
   */
  public XrayCameraStartupThreadTask(AbstractXrayCamera xRayCamera)
  {
    super("Xray Camera startup task.");

    Assert.expect(xRayCamera != null);
    _xRayCamera = xRayCamera;
  }

  /**
   * @author Greg Esparza
   */
  public void cancel() throws Exception
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public Object executeTask() throws Exception
  {
    _xRayCamera.startup();
    return null;
  }
}
