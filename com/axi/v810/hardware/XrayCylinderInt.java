package com.axi.v810.hardware;

//Variable Mag Anthony August 2011
import com.axi.v810.util.*;

/**
 * This interface will be implemented by all the X-rays Cylinder objects.
 * The X-rays Cylinder on the machine are there to adjust the system magnification factor (Low Mag or High Mag).
 * @author Anthony Fong
 */
public interface XrayCylinderInt
{
  /**
   * Move up the cylinder.
   * @author Anthony Fong
   */
  abstract void up(boolean loadConfig) throws XrayTesterException;

  /**
   * Move the current cylinder down.
   * @author Anthony Fong
   */
  abstract void down(boolean loadConfig) throws XrayTesterException;

  /**
   * @return true if the cylinder is up.
   * @author Anthony Fong
   */
  abstract boolean isUp() throws XrayTesterException;

  /**
   * @return true if the cylinder is down.
   * @author Anthony Fong
   */
  abstract boolean isDown() throws XrayTesterException;

}
