package com.axi.v810.hardware;

import gnu.io.*;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for NoFlowControlSupportHardwareException.
* @author Bob Balliew
*/
public class Test_NoFlowControlSupportHardwareException extends UnitTest
{

  /**
  * @author Bob Balliew
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_NoFlowControlSupportHardwareException());
  }

  /**
  * @author Bob Balliew
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    final int[] fchi = {0, SerialPort.FLOWCONTROL_RTSCTS_IN};
    final int[] fch0 = {0, SerialPort.FLOWCONTROL_RTSCTS_OUT};
    final int[] fcsi = {0, SerialPort.FLOWCONTROL_XONXOFF_IN};
    final int[] fcs0 = {0, SerialPort.FLOWCONTROL_XONXOFF_OUT};

    for (int fchi_i = 0; fchi_i < fchi.length; ++fchi_i)
    {
      for (int fcho_i = 0; fcho_i < fch0.length; ++fcho_i)
      {
        for (int fcsi_i = 0; fcsi_i < fcsi.length; ++fcsi_i)
        {
          for (int fcso_i = 0; fcso_i < fcs0.length; ++fcso_i)
          {
            int fc = fchi[fchi_i] | fch0[fcho_i] | fcsi[fcsi_i] | fcs0[fcso_i];
            HardwareException ex = new NoFlowControlSupportHardwareException("COM1", fc);
            String localized = ex.getLocalizedMessage();
            os.println("                                                                                                   111111111111111111111");
            os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
            os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
            os.println("<-start localized------------------------------------------------------------------------------------------------------>");
            os.println(localized);
            os.println("<-end localized-------------------------------------------------------------------------------------------------------->");
            os.println("                                                                                                   111111111111111111111");
            os.println("         111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112");
            os.println("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
            int lineLength = 80;
            os.println("<-start format(" + lineLength + ")----------------------------------------------------------------------------------------------------->");
            os.println(StringUtil.format(localized, lineLength));
            os.println("<-end format(" + lineLength + ")------------------------------------------------------------------------------------------------------->");
          }
        }
      }
    }
  }
}
