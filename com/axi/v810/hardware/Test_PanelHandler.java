package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Unit test for Digital IO
 * @author Rex Shang
 */
class Test_PanelHandler extends UnitTest
{
  private XrayTester _xRayTester;
  private DigitalIo _digitalIo;
  private PanelPositioner _panelPositioner;
  private PanelHandler _panelHandler;
  private HardwareWorkerThread _hardwareWorkerThread;

  private int _numberOfExecution;
  private boolean _regressionTest;
  private int _panelWidth;
  private int _panelLength;
  private boolean _serviceModeOn;
  private boolean _testPIP;

  /**
   * @author Rex Shang
   */
  private Test_PanelHandler(String[] args)
  {
    // By default, only run loading unloading once.
    _numberOfExecution = 1;
    // Usually, we are in simulation mode.
    _regressionTest = true;

    // In service mode, unload/load will repeat without user interaction.
    _serviceModeOn = false;
    _testPIP = false;

    // CobraJet board is 5.28125 inch W, 10.5 inch L.
    // SledgeHammer board is 11.9375 inch W, 17 inch L.
    // Use 20 inch length to simulate large panel.
    _panelWidth = MathUtil.convertMilsToNanoMetersInteger(5.28125 * 1000);
    _panelLength = MathUtil.convertMilsToNanoMetersInteger(20 * 1000);

    // Parse arguments, overwrite default parameters.
    parseArguments(args);

    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    _xRayTester = XrayTester.getInstance();
    _digitalIo = DigitalIo.getInstance();
    _panelPositioner = PanelPositioner.getInstance();
    _panelHandler = PanelHandler.getInstance();

    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  }

  /**
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    Test_PanelHandler _test_DigitalIo = new Test_PanelHandler(args);
    UnitTest.execute(_test_DigitalIo);
  }

  /**
   * Parse command line arguments.  This enables non-simulation hardware
   * testing.
   * @author Rex Shang
   */
  private void parseArguments(String[] args)
  {
    for (int i = 0; i < args.length; i++)
    {
      if (args[i].compareToIgnoreCase("-loop") == 0)
      {
        try
        {
          _numberOfExecution = Integer.parseInt(args[++i]);
        }
        catch (Exception e)
        {
          // Do nothing.
        }
      }
      else if (args[i].compareToIgnoreCase("-notRegression") == 0)
      {
        _regressionTest = false;
      }
      else if (args[i].compareToIgnoreCase("-width") == 0)
      {
        _panelWidth = Integer.parseInt(args[++i]);
      }
      else if (args[i].compareToIgnoreCase("-length") == 0)
      {
        _panelLength = Integer.parseInt(args[++i]);
      }
      else if (args[i].compareToIgnoreCase("-service") == 0)
      {
        _serviceModeOn = true;
      }
      else if (args[i].compareToIgnoreCase("-testPIP") == 0)
      {
        _testPIP = true;
      }
    }
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException
        {
          int railWidth = 0;

          if (_regressionTest == false)
          {
            _digitalIo.clearSimulationMode();
            _panelHandler.clearSimulationMode();
            _panelPositioner.clearSimulationMode();
          }

          _xRayTester.startupMotionRelatedHardware();

          // Sometimes, the user would leave a panel in system, unload it first.
          if (_panelHandler.isPanelLoaded())
          {
            railWidth = _panelHandler.getRailWidthInNanoMeters();
            Assert.expect(railWidth ==
                          _panelHandler.getLoadedPanelWidthInNanoMeters()
                          + _panelHandler.getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_WIDTH_CLEARANCE_IN_NANOMETERS));

            _panelHandler.unloadPanel();
          }
          else
          {
            // By just getting the rail width, we are checking to see if it
            // exceeded the limits.
            railWidth = _panelHandler.getRailWidthInNanoMeters();
            Assert.expect(railWidth == _panelHandler.getMaximumRailWidthInNanoMeters());
          }

          if (_serviceModeOn)
          {
            _panelHandler.loadPanel("TestPanel", _panelWidth, _panelLength, false, 0);
            _panelHandler.repeatUnloadLoadForServiceTest(_numberOfExecution);
            _panelHandler.unloadPanel();
          }
          else if (_testPIP)
          {
            _panelHandler.loadPanel("TestPanel", _panelWidth, _panelLength, false, 0);

            for (int i = 0; i < _numberOfExecution; i++)
            {
              _panelHandler.movePanelToLeftSide();
              _panelHandler.movePanelToRightSide();
            }

            _panelHandler.unloadPanel();
          }
          else
          {
            for (int i = 0; i < _numberOfExecution; i++)
            {
              _panelHandler.loadPanel("TestPanel", _panelWidth, _panelLength, false, 0);

              Assert.expect(_panelWidth == _panelHandler.getLoadedPanelWidthInNanoMeters());
              Assert.expect(_panelLength == _panelHandler.getLoadedPanelLengthInNanoMeters());

              // Check rail width.
              railWidth = _panelHandler.getRailWidthInNanoMeters();
              Assert.expect(railWidth == _panelWidth + _panelHandler.getConfig().getIntValue(HardwareConfigEnum.PANEL_HANDLER_PANEL_WIDTH_CLEARANCE_IN_NANOMETERS));

              // First thing the test execution engine would do is to position
              // the panel against the right panel in place sensor.
              _panelHandler.movePanelToRightSide();
              // Simulate scan, it will move stage away from loading position.
              moveStageAwayFromLoadingPosition();

              // Reposition.
              _panelHandler.movePanelToLeftSide();
              moveStageAwayFromLoadingPosition();

              _panelHandler.unloadPanel();
            }
          }

          // In regression test mode, let's try to startup while panel in system.
          if (_regressionTest)
          {
            _panelHandler.loadPanel("TestPanel", _panelWidth, _panelLength, false, 0);
            _xRayTester.startupMotionRelatedHardware();
            _panelHandler.unloadPanel();
          }
        }
      });
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
  }

  /**
   * Move stage away to simulate start of scan.
   * @author Rex Shang
   */
  private void moveStageAwayFromLoadingPosition() throws XrayTesterException
  {
    StagePositionMutable stagePosition = new StagePositionMutable();
    // Try 10 inches from orgin.
    stagePosition.setXInNanometers(254000000);
    stagePosition.setYInNanometers(254000000);

    _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);
  }

}
