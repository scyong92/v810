package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Greg Loring
 */
public class Test_ProgressObservable extends UnitTest implements Observer
{
  private static Map<Long, ProgressReport> _hwSchedule; // time -> (reporter + %done)
  private static List<ProgressReport> _expectedUpdates;

  private ProgressObservable _progressObservable = ProgressObservable.getInstance();
  private Queue<ProgressReport> _updateQueue = new LinkedList<ProgressReport>();

  /**
   * @author Greg Loring
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProgressObservable());
  }

  /**
   * @author Greg Loring
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      Thread pseudoHardwareThread = new Thread(new Runnable() {
        public void run() { runPseudoHardware(); } });

      Expect.expect(_progressObservable.getReporter() == null);
      Expect.expect(_progressObservable.getPercentDone() == 0);

      _progressObservable.addObserver(this);

      Expect.expect(_progressObservable.getReporter() == null);
      Expect.expect(_progressObservable.getPercentDone() == 0);

      pseudoHardwareThread.start();
      pseudoHardwareThread.join();
      sleep(500); // leave plenty of time for events to make it thru

//      for (ProgressReport report : _updateQueue)
//      {
//        System.out.println("observer received " + report);
//      }

      if (_updateQueue.size() == _expectedUpdates.size())
      {
        Iterator<ProgressReport> actualsIter = _updateQueue.iterator();
        Iterator<ProgressReport> expectedsIter = _expectedUpdates.iterator();
        while (actualsIter.hasNext())
        {
          Assert.expect(expectedsIter.hasNext());
          ProgressReport actual = actualsIter.next();
          ProgressReport expected = expectedsIter.next();
          Expect.expect(actual.getReporter() == expected.getReporter());
          Expect.expect(actual.getPercentDone() == expected.getPercentDone());
        }
        Assert.expect(expectedsIter.hasNext() == false);
      }
      else
      {
         Expect.expect(false, "number of update calls different than expected");
      }
      _updateQueue.clear();

      _progressObservable.reportAtomicTaskStarted(ProgressReporterEnum.XRAY_SOURCE_ON, 5000);
      sleep(6000); // leave plenty of time for events to make it thru
      Expect.expect(_updateQueue.size() == 5);
      for (int i = 0; i < 5; i++)
      {
        ProgressReport actual = _updateQueue.remove();
        Expect.expect(actual.getPercentDone() == (20 * i));
      }
      _progressObservable.reportAtomicTaskComplete(ProgressReporterEnum.XRAY_SOURCE_ON);
      sleep(500); // leave plenty of time for events to make it thru
      Expect.expect(_updateQueue.size() == 1);
      Expect.expect(_updateQueue.remove().getPercentDone() == 100);

    }
    catch(Exception x)
    {
      x.printStackTrace();
    }
  }

  /**
   * @author Greg Loring
   */
  public synchronized void update(Observable observable, Object report)
  {
    Expect.expect(observable == _progressObservable);
    Expect.expect(report instanceof ProgressReport);
    Assert.expect(_updateQueue.offer((ProgressReport) report));
    System.out.println("observer received " + report);
  }

  /**
   * @author Greg Loring
   */
  private void runPseudoHardware()
  {
    long lastTime = -1L;
    for (Map.Entry<Long,ProgressReport> entry : _hwSchedule.entrySet())
    {
      long time = entry.getKey();
      Assert.expect(time > lastTime);
      sleep(time - lastTime);
      lastTime = time;

      ProgressReport report = entry.getValue();
      System.out.println(report.getReporter() + " reporting " + report.getPercentDone());
      _progressObservable.reportProgress(report.getReporter(), report.getPercentDone());
    }
  }

  /**
   * @author Greg Loring
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      Expect.expect(false, "unexpected InterruptException");
    }
  }

  /**
   * @author Greg Loring
   */
  static
  {
    _hwSchedule = new TreeMap<Long,ProgressReport>(); // sort on time

    // steady ramper
    long step = 500L;
    long time = 0L;
    for (int pct : new int[] { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 })
    {
       _hwSchedule.put(time, new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, pct));
       time += step;
    }

    // jumpy ramper
    time = step / 5L; // slight offset from start of steady ramp
    for (int pct : new int[] { 0,  5, 10, 15, 35, 55, 75, 85, 45, 65, 85, 100 })
    {
       _hwSchedule.put(time, new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON, pct));
       time += step;
    }

    _expectedUpdates = new LinkedList<ProgressReport>();
    // first (& therefore only) reporter
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 0));
    // ties go to the reporter who reported most recently (but this is not guaranteed!)
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    0));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    5));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    10));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    15));
    // even tho X-ray only 35; MO-CO looses 'cuz X-rays rate hike has brought in est. complete
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 40));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 50));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 60));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 70));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.PANEL_POSITIONER_INITIALIZE, 80));
    // X-ray went backward, so it was given an est. complete at "the end of time"
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    45));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    65));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    85));
    _expectedUpdates.add(new ProgressReport(ProgressReporterEnum.XRAY_SOURCE_ON,    100));
  }

}
