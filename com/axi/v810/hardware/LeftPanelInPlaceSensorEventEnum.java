package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class LeftPanelInPlaceSensorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static LeftPanelInPlaceSensorEventEnum SENSOR_EXTEND = new LeftPanelInPlaceSensorEventEnum(++_index, "Left Panel In Place Sensor, Extend");
  public static LeftPanelInPlaceSensorEventEnum SENSOR_RETRACT = new LeftPanelInPlaceSensorEventEnum(++_index, "Left Panel In Place Sensor, Retract");

  private String _name;

  /**
   * @author Rex Shang
   */
  private LeftPanelInPlaceSensorEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
