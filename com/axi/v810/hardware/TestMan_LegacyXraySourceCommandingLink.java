package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * debugging aid
 *
 * @author Greg Loring
 */
public class TestMan_LegacyXraySourceCommandingLink
{
  private static final Map<String, String> _readCommands;
  private static final Map<String, String> _setCommands;

  /**
   * dumb terminal style interface for rapid debugging
   *
   * @author Greg Loring
   */
  public static void main(String[] args) throws Exception
  {
    // parse command line args
    String portName = "COM1";
    if (args.length >= 1)
    {
      if ((args.length == 1) && args[0].startsWith("COM"))
      {
        portName = args[0];
      }
      else
      {
        System.err.println("usage: TestMan_LegacyXraySourceCommandingLink [COMn]");
        System.exit(1);
      }
    }
    System.out.println("using " + portName);

    Config.getInstance().loadIfNecessary();

    LegacyXraySourceCommandingLink link = new LegacyXraySourceCommandingLink(portName);
    link.configureSerialPort();
    if (link.isSimulationModeOn())
      link.setSimulateLinkTiming();

    // in/out for the "terminal"
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in, "US-ASCII"));
    PrintStream out = System.out;

    TimerUtil timer = new TimerUtil();
    timer.start();

    // "terminal" loop
    while (true)
    {
      out.print("? "); // prompt

      String cmd = in.readLine();

      if ((cmd == null) || cmd.equals("exit") || cmd.equals("quit") || cmd.equals("q"))
      {
        System.exit(0);
      }
      else if (cmd.equals("reset"))
      {
        link.reset();
      }
      else if (cmd.equals("reset-timer"))
      {
        timer.reset();
        out.println("timer reset: " + timer.getElapsedTimeInMillis() + " ms elapsed");
        timer.start();
      }
      else if (cmd.equals("look-timer"))
      {
        timer.stop();
        out.println("timer: " + timer.getElapsedTimeInMillis() + " ms elapsed");
        timer.start();
      }
      else if (cmd.startsWith("sleep ") && (cmd.length() > "sleep ".length()))
      {
        try
        {
          long msToSleep = Long.parseLong(cmd.substring("sleep ".length()).trim());
          out.println("sleeping for " + msToSleep + " ms");
          Thread.sleep(msToSleep);
        }
        catch (InterruptedException ix)
        {
          ix.printStackTrace();
        }
        catch (NumberFormatException nfx)
        {
          System.err.println("error parsing sleep command duration");
          nfx.printStackTrace();
        }
      }
      else if (cmd.startsWith("echo ") && (cmd.length() > "echo ".length()))
      {
        out.println(cmd.substring("echo ".length()));
      }
      else if (((cmd.length() == 3) && _readCommands.containsKey(cmd))
               || ((cmd.length() == 8) && _setCommands.containsKey(cmd.substring(0, 3))))
      {
        try
        {
          String response = link.sendAndReceive(cmd);
          out.println(response);
        }
        catch (HardwareException hx)
        {
          hx.printStackTrace(out);
        }
      }
      else
      {
        out.println("?|help -> print command list");
        out.println("exit|quit|q");
        out.println("reset");
        out.println("reset-timer");
        out.println("look-timer");
        out.println("sleep <millis>");
        out.println("echo <stuff and blather>");
        for (Map.Entry<String, String> entry : _readCommands.entrySet())
          out.println(entry.getKey() + " -> read " + entry.getValue());
        for (Map.Entry<String, String> entry : _setCommands.entrySet())
          out.println(entry.getKey() + ",#### -> set " + entry.getValue());
      }
    }
  }

  /**
   * namespace / main-only
   *
   * @author Greg Loring
   */
  private TestMan_LegacyXraySourceCommandingLink()
  {
  }

  static
  {
    _readCommands = new LinkedHashMap<String, String>();
    _readCommands.put("9I3", "Anode Voltage");
    _readCommands.put("9I2", "Total Current");
    _readCommands.put("8I5", "Cathode Current");
    _readCommands.put("8I2", "Grid Voltage");
    _readCommands.put("8I3", "Focus Voltage");
    _readCommands.put("8I0", "Filament Voltage");
    _readCommands.put("8I1", "Isolated PS Voltage");
    _readCommands.put("8I6", "Gun Temperature");
    _readCommands.put("9V0", "HVPS rev code");
    _readCommands.put("9S2", "HVPS status");
    _readCommands.put("9S3", "& reset! HVPS status");
    _readCommands.put("9M0", "& reset! Min/Max Anode Voltage");
    _readCommands.put("9M1", "Minimum Anode Voltage");
    _readCommands.put("9M2", "Maximum Anode Voltage");

    _setCommands = new LinkedHashMap<String, String>();
    _setCommands.put("9O0", "Anode Voltage");
    _setCommands.put("8A0", "Cathode Current");
    _setCommands.put("8P0", "Focus Voltage");
    _setCommands.put("8P2", "Filament Voltage");
  }

}
