package com.axi.v810.hardware;

import com.axi.v810.util.XrayTesterException;

/**
 * @author Bill Darbie
 */
public interface Simulatable
{
  /**
   *
   * @throws XrayTesterException because it will interact with datastore and
   * hardware and potentially throw DatastoreException and HardwareException.
   * @author Bill Darbie
   * @author Rex Shang
   */
  void setSimulationMode() throws XrayTesterException;

  /**
   *
   * @throws XrayTesterException because it will interact with datastore and
   * hardware and potentially throw DatastoreException and HardwareException.
   * @author Bill Darbie
   * @author Rex Shang
   */
  void clearSimulationMode() throws XrayTesterException;

  /**
   * @author Bill Darbie
   * @author Rex Shang
   */
  boolean isSimulationModeOn();
}
