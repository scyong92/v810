package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Matt Wharton
 */
public class ProcessorStrip implements Serializable
{
  public static final int OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS = 5080000; // 200 mils
  public static final int FOCUS_SEARCH_BUFFER_IN_NANOMETERS = 5080000;  // 200 mils
  public static final int ALIGNMENT_VARIATION_BUFFER_IN_NANOMETERS = 1270000;  // 50 mils

  private ImageReconstructionEngineEnum _reconstructionEngineId;
  private int _location = -1;
  private int _start = -1;
  private int _stop = -1;
  private int _numberOfReconstructionEnginesToUse;

  private PanelRectangle _processorStripRectangle;
  private PanelRectangle _nominalRectangle;
  
  //Siew Yeng - XCR-2218 - Increase inspectable pad size
  private static int _currentOverlapNeededForLargestPossibleJointInNanometers = OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS;

  /**
   * This constructor is used for calibrations.
   *
   * @author Roy Williams
   */
  public ProcessorStrip(int location)
  {
    Assert.expect(location >= 0);
    _location = location;
  }

  /**
   * @author Roy Williams
   */
  public boolean isInitialized()
  {
    return (_start >= 0 && _stop >= 0);
  }

  /**
   * @author Roy Williams
   */
  public ProcessorStrip(int location,
                        PanelRectangle nominalRegionToImage,
                        final int numberOfReconstructionEnginesToUse)
  {
    Assert.expect(location >= 0);
    Assert.expect(numberOfReconstructionEnginesToUse > 0);

    _location = location;
    _numberOfReconstructionEnginesToUse = numberOfReconstructionEnginesToUse;
    _nominalRectangle = new PanelRectangle(nominalRegionToImage);
    initializeDefaultProjectionRectangleFromNominalRectangle();
  }

  /**
   * @author Roy Williams
   */
  public void initializeDefaultProjectionRectangleFromNominalRectangle()
  {
    //Siew Yeng - XCR-2218 - Increase inspectable pad size
    //int largestJointOverlapInNanometers = ProcessorStrip.OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS;
    int largestJointOverlapInNanometers = getOverlapNeededForLargestPossibleJointInNanometers();

    int x = _nominalRectangle.getMinX();
    int y = _nominalRectangle.getMinY();
    int w = _nominalRectangle.getWidth();
    int h = _nominalRectangle.getHeight();

    if (_location == 0)
    {
      // Processor strip on the left end.  Enlarge by the largest joint only
      // towards the adjacent processor strip.  If we only have one
      // processor strip, no need to enlarge.

      if (_numberOfReconstructionEnginesToUse == 1)
      {
        // do nothing... defaults are okay.
      }
      else
      {
        w = w + largestJointOverlapInNanometers;
      }
    }
    else if (_location == (_numberOfReconstructionEnginesToUse - 1))
    {
      // Processor strip on the end.  Enlarge by the largest joint only
      // towards the adjacent processor strip.
      x = x - largestJointOverlapInNanometers;
      w = w + largestJointOverlapInNanometers;
    }
    else
    {
      // Middle processor strips.  Enlarge strip towards both adjacent
      // processor strips.
      x = x - largestJointOverlapInNanometers;
      w = w + (2 * largestJointOverlapInNanometers);
    }
    _processorStripRectangle = new PanelRectangle(x, y, w, h);
  }

  /**
   * @author Roy Williams
   */
  public int getLocation()
  {
    return _location;
  }

  /**
   * @author Roy Williams
   */
  public int getNominalWidthInNanometers()
  {
    return _nominalRectangle.getWidth();
  }

  /**
   * @author Roy Williams
   */
  public int getLeadingEdgeWidth()
  {
    int leadingEdgeWidth = _nominalRectangle.getMinX() - _processorStripRectangle.getMinX();
    Assert.expect(leadingEdgeWidth >= 0);
    return leadingEdgeWidth;
  }

  /**
   * @author Roy Williams
   */
  public void setLeadingEdgeWidth(int leadingEdgeWidth)
  {
    Assert.expect(leadingEdgeWidth >= 0);
    _processorStripRectangle.setRect(_nominalRectangle.getMinX() - leadingEdgeWidth,
                                     _nominalRectangle.getMinY(),
                                     leadingEdgeWidth + _nominalRectangle.getWidth() + getTrailingEdgeWidth(),
                                     _nominalRectangle.getHeight());
  }

  /**
   * @author Roy Williams
   */
  public int getTrailingEdgeWidth()
  {
    int trailingEdgeWidth = _processorStripRectangle.getMaxX() - _nominalRectangle.getMaxX();
    Assert.expect(trailingEdgeWidth >= 0);
    return trailingEdgeWidth;
  }

  /**
   * @author Roy Williams
   */
  public void setTrailingEdgeWidth(int trailingEdgeWidth, boolean startingPositionOnStrip)
  {
    Assert.expect(trailingEdgeWidth >= 0);
    int leadingEdgeWidth = getLeadingEdgeWidth();
    if (startingPositionOnStrip)
    {
      int start = _nominalRectangle.getMinX() + _processorStripRectangle.getWidth() - trailingEdgeWidth;
      _processorStripRectangle.setRect(start,
                                       _nominalRectangle.getMinY(),
                                       trailingEdgeWidth,
                                       _nominalRectangle.getHeight());
    }
    else
      _processorStripRectangle.setRect(_nominalRectangle.getMinX() - leadingEdgeWidth,
                                       _nominalRectangle.getMinY(),
                                       leadingEdgeWidth + _nominalRectangle.getWidth() + trailingEdgeWidth,
                                       _nominalRectangle.getHeight());

  }

  /**
   * Gets the PanelRectangle which encompasses this ProcessorStrip.
   *
   * @author Matt Wharton
   */
  public PanelRectangle getRegion()
  {
    Assert.expect( _processorStripRectangle != null );

    return _processorStripRectangle;
  }

  /**
   * Sets the PanelRectangle which encompasses the nominal rectangle of this ProcessorStrip.
   *
   * @author Matt Wharton
   */
  public void setRegion(PanelRectangle processorStripRectangle)
  {
    Assert.expect( processorStripRectangle != null );

    _processorStripRectangle = processorStripRectangle;
  }

  /**
   * Gets the PanelRectangle for the nominal area to image.
   *
   * @author Roy Williams
   */
  public PanelRectangle getNominalRegion()
  {
    Assert.expect( _nominalRectangle != null );

    return _nominalRectangle;
  }

  /**
   * @author Rex Shang
   */
  public ImageReconstructionEngineEnum getReconstructionEngineId()
  {
    Assert.expect(_reconstructionEngineId != null);

    return _reconstructionEngineId;
  }

  /**
   * @author Rex Shang
   */
  public void setReconstructionEngineId(ImageReconstructionEngineEnum reconstructionEngineId)
  {
    Assert.expect(reconstructionEngineId != null);

    _reconstructionEngineId = reconstructionEngineId;
  }

  /**
   * @author Roy Williams
   */
  public int getNominalStartInNanometers()
  {
    return _start;
  }

  /**
   * @author Roy Williams
   */
  public void setNominalStartInNanometers(int start)
  {
    _start = start;
  }

  /**
   * @author Roy Williams
   */
  public int getNominalStopInNanometers()
  {
    return _stop;
  }

  /**
   * @author Roy Williams
   */
  public void setNominalStopInNanometers(int stop)
  {
    _stop = stop;
  }
  
  /**
   * @author Siew Yeng
   */
  public static void setOverlapNeededForLargestPossibleJointInNanometers(int overlapInNanometers)
  {
    Assert.expect(overlapInNanometers >= OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS, "Processor Strip set incorrect overlap size.");
    
    _currentOverlapNeededForLargestPossibleJointInNanometers = overlapInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getOverlapNeededForLargestPossibleJointInNanometers()
  {
    return _currentOverlapNeededForLargestPossibleJointInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public static void resetOverlapNeededForLargestPossibleJoint()
  {
    _currentOverlapNeededForLargestPossibleJointInNanometers = OVERLAP_NEEDED_FOR_LARGEST_POSSIBLE_JOINT_IN_NANOMETERS;
  }
}
