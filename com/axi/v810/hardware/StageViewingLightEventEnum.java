package com.axi.v810.hardware;

/**
 * @author Rex Shang
 */
public class StageViewingLightEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static StageViewingLightEventEnum LIGHT_ON = new StageViewingLightEventEnum(++_index);
  public static StageViewingLightEventEnum LIGHT_OFF = new StageViewingLightEventEnum(++_index);

  /**
   * @author Rex Shang
   */
  private StageViewingLightEventEnum(int id)
  {
    super(id);
  }

}
