package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class LeftOuterBarrierEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static LeftOuterBarrierEventEnum BARRIER_OPEN = new LeftOuterBarrierEventEnum(++_index, "Left Outer Barrier, Open");
  public static LeftOuterBarrierEventEnum BARRIER_CLOSE = new LeftOuterBarrierEventEnum(++_index, "Left Outer Barrier, Close");

  private String _name;

  /**
   * @author Rex Shang
   */
  private LeftOuterBarrierEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
