package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the port is not a serial port, then throw this exception.
 *
 * @author Bob Balliew
 */
class NotASerialPortHardwareException extends HardwareException
{
  /**
   * This is for the requested port (name) not being a serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @author Bob Balliew
   */
  public NotASerialPortHardwareException(String portName)
  {
    super(new LocalizedString("HW_NOT_A_SERIAL_PORT_EXCEPTION_KEY",
                              new Object[]{portName}));

    Assert.expect(portName != null);
  }
}
