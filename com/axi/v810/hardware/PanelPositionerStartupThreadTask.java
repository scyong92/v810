package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class PanelPositionerStartupThreadTask extends ThreadTask<Object>
{
  private PanelPositioner _panelPositioner;

  /**
   * @author Bill Darbie
   */
  PanelPositionerStartupThreadTask()
  {
    super("Start panel positioner thread task");

    _panelPositioner = PanelPositioner.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws XrayTesterException
  {
    _panelPositioner.startup();
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelPositioner.abort();
  }
}
