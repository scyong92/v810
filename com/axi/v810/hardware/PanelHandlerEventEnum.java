package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class PanelHandlerEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static PanelHandlerEventEnum INITIALIZE = new PanelHandlerEventEnum(++_index, "Panel Handler, Initialize");
  public static PanelHandlerEventEnum INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_ONE_OF_TWO = new PanelHandlerEventEnum(++_index, "Panel Handler, Initialize With Panel In System Stage One of Two");
  public static PanelHandlerEventEnum INITIALIZE_WITH_PANEL_IN_SYSTEM_STAGE_TWO_OF_TWO = new PanelHandlerEventEnum(++_index, "Panel Handler, Initialize With Panel In System Stage Two of Two");
  public static PanelHandlerEventEnum RESET = new PanelHandlerEventEnum(++_index, "Panel Handler, Reset");
  public static PanelHandlerEventEnum ENABLE = new PanelHandlerEventEnum(++_index, "Panel Handler, Enable");
  public static PanelHandlerEventEnum DISABLE = new PanelHandlerEventEnum(++_index, "Panel Handler, Disable");
  public static PanelHandlerEventEnum LOAD_PANEL = new PanelHandlerEventEnum(++_index, "Panel Handler, Load Panel");
  public static PanelHandlerEventEnum CANCEL_LOAD_PANEL = new PanelHandlerEventEnum(++_index, "Panel Handler, Cancel Load Panel");
  public static PanelHandlerEventEnum UNLOAD_PANEL = new PanelHandlerEventEnum(++_index, "Panel Handler, Unload Panel");
  public static PanelHandlerEventEnum MOVE_PANEL_TO_LEFT_SIDE = new PanelHandlerEventEnum(++_index, "Panel Handler, Move Panel to Left Side");
  public static PanelHandlerEventEnum MOVE_PANEL_TO_RIGHT_SIDE = new PanelHandlerEventEnum(++_index, "Panel Handler, Move Panel to Right Side");
  public static PanelHandlerEventEnum HOME_STAGE_RAILS = new PanelHandlerEventEnum(++_index, "Panel Handler, Home Stage Rails");
  public static PanelHandlerEventEnum SET_PANEL_FLOW_DIRECTION = new PanelHandlerEventEnum(++_index, "Panel Handler, Set Panel Flow Direction");
  public static PanelHandlerEventEnum SET_PANEL_LOADING_MODE = new PanelHandlerEventEnum(++_index, "Panel Handler, Set Panel Loading Mode");
  public static PanelHandlerEventEnum SET_RAIL_WIDTH = new PanelHandlerEventEnum(++_index, "Panel Handler, Set Stage Rail Width");

  private String _name;

 /**
  * @author Rex Shang
  */
 private PanelHandlerEventEnum(int id, String name)
 {
   super(id);
   Assert.expect(name != null);

   _name = name;
 }

 /**
  * @author Rex Shang
  */
 public String toString()
 {
   return _name;
  }

}
