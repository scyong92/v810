package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for right outer barrier
 * @author Rex Shang
 */
class Test_RightOuterBarrier extends UnitTest
{
  private RightOuterBarrier _rightOuterBarrier = null;

  private Test_RightOuterBarrier()
  {
    _rightOuterBarrier = RightOuterBarrier.getInstance();
  }

  public static void main(String[] args)
  {
    Test_RightOuterBarrier _test_RightOuterBarrier = new Test_RightOuterBarrier();
    UnitTest.execute(_test_RightOuterBarrier);
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Close the barrier and verify
      _rightOuterBarrier.close();
      Expect.expect(_rightOuterBarrier.isOpen() == false);

      // Open the barrier and verify.
      _rightOuterBarrier.open();
      Expect.expect(_rightOuterBarrier.isOpen() == true);
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }
}
