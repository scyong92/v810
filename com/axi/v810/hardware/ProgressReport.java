package com.axi.v810.hardware;

/**
 * passed to update calls on Observers of the ProgressObservable
 *
 * @author Greg Loring
 */
public class ProgressReport
{
  ProgressReporterEnum _reporter;
  int _percentDone;

  /**
   * @author Greg Loring
   */
  public ProgressReport(ProgressReporterEnum reporter, int percentDone)
  {
    _reporter = reporter;
    _percentDone = percentDone;
  }

  /**
   * @author Greg Loring
   */
  public ProgressReporterEnum getReporter()
  {
    return _reporter;
  }

  /**
   * @author Greg Loring
   */
  public int getPercentDone()
  {
    return _percentDone;
  }

  /**
   * @author George Booth
   */
  public String getAction()
  {
      return _reporter.toString();
  }

  /**
   * @author Greg Loring
   */
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append(_reporter.toString());
    sb.append("\t");
    sb.append(_percentDone);
    return sb.toString();
  }

}
