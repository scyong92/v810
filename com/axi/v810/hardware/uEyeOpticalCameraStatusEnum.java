package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class uEyeOpticalCameraStatusEnum extends com.axi.util.Enum
{
    private static Map<Integer, String> _statusStringToOpticalCameraStatusEnumMap = new HashMap<Integer, String>();
    
    public static final uEyeOpticalCameraStatusEnum IS_NO_SUCCESS                               = new uEyeOpticalCameraStatusEnum(-1, "IS_NO_SUCCESS");
    public static final uEyeOpticalCameraStatusEnum IS_SUCCESS                                  = new uEyeOpticalCameraStatusEnum(0, "IS_SUCCESS");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_CAMERA_HANDLE                    = new uEyeOpticalCameraStatusEnum(1, "IS_INVALID_CAMERA_HANDLE");
    public static final uEyeOpticalCameraStatusEnum IS_IO_REQUEST_FAILED                        = new uEyeOpticalCameraStatusEnum(2, "IS_IO_REQUEST_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_OPEN_DEVICE                         = new uEyeOpticalCameraStatusEnum(3, "IS_CANT_OPEN_DEVICE");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_CLOSE_DEVICE                        = new uEyeOpticalCameraStatusEnum(4, "IS_CANT_CLOSE_DEVICE");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_SETUP_MEMORY                        = new uEyeOpticalCameraStatusEnum(5, "IS_CANT_SETUP_MEMORY");
    public static final uEyeOpticalCameraStatusEnum IS_NO_HWND_FOR_ERROR_REPORT                 = new uEyeOpticalCameraStatusEnum(6, "IS_NO_HWND_FOR_ERROR_REPORT");
    public static final uEyeOpticalCameraStatusEnum IS_ERROR_MESSAGE_NOT_CREATED                = new uEyeOpticalCameraStatusEnum(7, "IS_ERROR_MESSAGE_NOT_CREATED");
    public static final uEyeOpticalCameraStatusEnum IS_ERROR_STRING_NOT_FOUND                   = new uEyeOpticalCameraStatusEnum(8, "IS_ERROR_STRING_NOT_FOUND");
    public static final uEyeOpticalCameraStatusEnum IS_HOOK_NOT_CREATED                         = new uEyeOpticalCameraStatusEnum(9, "IS_HOOK_NOT_CREATED");
    public static final uEyeOpticalCameraStatusEnum IS_TIMER_NOT_CREATED                        = new uEyeOpticalCameraStatusEnum(10, "IS_TIMER_NOT_CREATED");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_OPEN_REGISTRY                       = new uEyeOpticalCameraStatusEnum(11, "IS_CANT_OPEN_REGISTRY");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_READ_REGISTRY                       = new uEyeOpticalCameraStatusEnum(12, "IS_CANT_READ_REGISTRY");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_VALIDATE_BOARD                      = new uEyeOpticalCameraStatusEnum(13, "IS_CANT_VALIDATE_BOARD");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_GIVE_BOARD_ACCESS                   = new uEyeOpticalCameraStatusEnum(14, "IS_CANT_GIVE_BOARD_ACCESS");
    public static final uEyeOpticalCameraStatusEnum IS_NO_IMAGE_MEM_ALLOCATED                   = new uEyeOpticalCameraStatusEnum(15, "IS_NO_IMAGE_MEM_ALLOCATED");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_CLEANUP_MEMORY                      = new uEyeOpticalCameraStatusEnum(16, "IS_CANT_CLEANUP_MEMORY");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_COMMUNICATE_WITH_DRIVER             = new uEyeOpticalCameraStatusEnum(17, "IS_CANT_COMMUNICATE_WITH_DRIVER");
    public static final uEyeOpticalCameraStatusEnum IS_FUNCTION_NOT_SUPPORTED_YET               = new uEyeOpticalCameraStatusEnum(18, "IS_FUNCTION_NOT_SUPPORTED_YET");
    public static final uEyeOpticalCameraStatusEnum IS_OPERATING_SYSTEM_NOT_SUPPORTED           = new uEyeOpticalCameraStatusEnum(19, "IS_OPERATING_SYSTEM_NOT_SUPPORTED");
    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_VIDEO_IN                         = new uEyeOpticalCameraStatusEnum(20, "IS_INVALID_VIDEO_IN");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_IMG_SIZE                         = new uEyeOpticalCameraStatusEnum(21, "IS_INVALID_IMG_SIZE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_ADDRESS                          = new uEyeOpticalCameraStatusEnum(22, "IS_INVALID_ADDRESS");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_VIDEO_MODE                       = new uEyeOpticalCameraStatusEnum(23, "IS_INVALID_VIDEO_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_AGC_MODE                         = new uEyeOpticalCameraStatusEnum(24, "IS_INVALID_AGC_MODE");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_GAMMA_MODE                       = new uEyeOpticalCameraStatusEnum(25, "IS_INVALID_GAMMA_MODE");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_SYNC_LEVEL                       = new uEyeOpticalCameraStatusEnum(26, "IS_INVALID_SYNC_LEVEL");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_CBARS_MODE                       = new uEyeOpticalCameraStatusEnum(27, "IS_INVALID_CBARS_MODE");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_COLOR_MODE                       = new uEyeOpticalCameraStatusEnum(28, "IS_INVALID_COLOR_MODE");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_SCALE_FACTOR                     = new uEyeOpticalCameraStatusEnum(29, "IS_INVALID_SCALE_FACTOR");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_IMAGE_SIZE                       = new uEyeOpticalCameraStatusEnum(30, "IS_INVALID_IMAGE_SIZE");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_IMAGE_POS                        = new uEyeOpticalCameraStatusEnum(31, "IS_INVALID_IMAGE_POS");    
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_CAPTURE_MODE                     = new uEyeOpticalCameraStatusEnum(32, "IS_INVALID_CAPTURE_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_RISC_PROGRAM                     = new uEyeOpticalCameraStatusEnum(33, "IS_INVALID_RISC_PROGRAM");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_BRIGHTNESS                       = new uEyeOpticalCameraStatusEnum(34, "IS_INVALID_BRIGHTNESS");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_CONTRAST                         = new uEyeOpticalCameraStatusEnum(35, "IS_INVALID_CONTRAST");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_SATURATION_U                     = new uEyeOpticalCameraStatusEnum(36, "IS_INVALID_SATURATION_U");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_SATURATION_V                     = new uEyeOpticalCameraStatusEnum(37, "IS_INVALID_SATURATION_V");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_HUE                              = new uEyeOpticalCameraStatusEnum(38, "IS_INVALID_HUE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_HOR_FILTER_STEP                  = new uEyeOpticalCameraStatusEnum(39, "IS_INVALID_HOR_FILTER_STEP");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_VERT_FILTER_STEP                 = new uEyeOpticalCameraStatusEnum(40, "IS_INVALID_VERT_FILTER_STEP");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_EEPROM_READ_ADDRESS              = new uEyeOpticalCameraStatusEnum(41, "IS_INVALID_EEPROM_READ_ADDRESS");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_EEPROM_WRITE_ADDRESS             = new uEyeOpticalCameraStatusEnum(42, "IS_INVALID_EEPROM_WRITE_ADDRESS");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_EEPROM_READ_LENGTH               = new uEyeOpticalCameraStatusEnum(43, "IS_INVALID_EEPROM_READ_LENGTH");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_EEPROM_WRITE_LENGTH              = new uEyeOpticalCameraStatusEnum(44, "IS_INVALID_EEPROM_WRITE_LENGTH");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_BOARD_INFO_POINTER               = new uEyeOpticalCameraStatusEnum(45, "IS_INVALID_BOARD_INFO_POINTER");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_DISPLAY_MODE                     = new uEyeOpticalCameraStatusEnum(46, "IS_INVALID_DISPLAY_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_ERR_REP_MODE                     = new uEyeOpticalCameraStatusEnum(47, "IS_INVALID_ERR_REP_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_BITS_PIXEL                       = new uEyeOpticalCameraStatusEnum(48, "IS_INVALID_BITS_PIXEL");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_MEMORY_POINTER                   = new uEyeOpticalCameraStatusEnum(49, "IS_INVALID_MEMORY_POINTER");
    
    public static final uEyeOpticalCameraStatusEnum IS_FILE_WRITE_OPEN_ERROR                    = new uEyeOpticalCameraStatusEnum(50, "IS_FILE_WRITE_OPEN_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_FILE_READ_OPEN_ERROR                     = new uEyeOpticalCameraStatusEnum(51, "IS_FILE_READ_OPEN_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_FILE_READ_INVALID_BMP_ID                 = new uEyeOpticalCameraStatusEnum(52, "IS_FILE_READ_INVALID_BMP_ID");
    public static final uEyeOpticalCameraStatusEnum IS_FILE_READ_INVALID_BMP_SIZE               = new uEyeOpticalCameraStatusEnum(53, "IS_FILE_READ_INVALID_BMP_SIZE");
    public static final uEyeOpticalCameraStatusEnum IS_FILE_READ_INVALID_BIT_COUNT              = new uEyeOpticalCameraStatusEnum(54, "IS_FILE_READ_INVALID_BIT_COUNT");
    public static final uEyeOpticalCameraStatusEnum IS_WRONG_KERNEL_VERSION                     = new uEyeOpticalCameraStatusEnum(55, "IS_WRONG_KERNEL_VERSION");
    
    public static final uEyeOpticalCameraStatusEnum IS_RISC_INVALID_XLENGTH                     = new uEyeOpticalCameraStatusEnum(60, "IS_RISC_INVALID_XLENGTH");
    public static final uEyeOpticalCameraStatusEnum IS_RISC_INVALID_YLENGTH                     = new uEyeOpticalCameraStatusEnum(61, "IS_RISC_INVALID_YLENGTH");
    public static final uEyeOpticalCameraStatusEnum IS_RISC_EXCEED_IMG_SIZE                     = new uEyeOpticalCameraStatusEnum(62, "IS_RISC_EXCEED_IMG_SIZE");
    
    public static final uEyeOpticalCameraStatusEnum IS_DD_MAIN_FAILED                           = new uEyeOpticalCameraStatusEnum(70, "IS_DD_MAIN_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_PRIMSURFACE_FAILED                    = new uEyeOpticalCameraStatusEnum(71, "IS_DD_PRIMSURFACE_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_SCRN_SIZE_NOT_SUPPORTED               = new uEyeOpticalCameraStatusEnum(72, "IS_DD_SCRN_SIZE_NOT_SUPPORTED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CLIPPER_FAILED                        = new uEyeOpticalCameraStatusEnum(73, "IS_DD_CLIPPER_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CLIPPER_HWND_FAILED                   = new uEyeOpticalCameraStatusEnum(74, "IS_DD_CLIPPER_HWND_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CLIPPER_CONNECT_FAILED                = new uEyeOpticalCameraStatusEnum(75, "IS_DD_CLIPPER_CONNECT_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_BACKSURFACE_FAILED                    = new uEyeOpticalCameraStatusEnum(76, "IS_DD_BACKSURFACE_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_BACKSURFACE_IN_SYSMEM                 = new uEyeOpticalCameraStatusEnum(77, "IS_DD_BACKSURFACE_IN_SYSMEM");
    public static final uEyeOpticalCameraStatusEnum IS_DD_MDL_MALLOC_ERR                        = new uEyeOpticalCameraStatusEnum(78, "IS_DD_MDL_MALLOC_ERR");
    public static final uEyeOpticalCameraStatusEnum IS_DD_MDL_SIZE_ERR                          = new uEyeOpticalCameraStatusEnum(79, "IS_DD_MDL_SIZE_ERR");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CLIP_NO_CHANGE                        = new uEyeOpticalCameraStatusEnum(80, "IS_DD_CLIP_NO_CHANGE");
    public static final uEyeOpticalCameraStatusEnum IS_DD_PRIMMEM_NULL                          = new uEyeOpticalCameraStatusEnum(81, "IS_DD_PRIMMEM_NULL");
    public static final uEyeOpticalCameraStatusEnum IS_DD_BACKMEM_NULL                          = new uEyeOpticalCameraStatusEnum(82, "IS_DD_BACKMEM_NULL");
    public static final uEyeOpticalCameraStatusEnum IS_DD_BACKOVLMEM_NULL                       = new uEyeOpticalCameraStatusEnum(83, "IS_DD_BACKOVLMEM_NULL");
    public static final uEyeOpticalCameraStatusEnum IS_DD_OVERLAYSURFACE_FAILED                 = new uEyeOpticalCameraStatusEnum(84, "IS_DD_OVERLAYSURFACE_FAILED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_OVERLAYSURFACE_IN_SYSMEM              = new uEyeOpticalCameraStatusEnum(85, "IS_DD_OVERLAYSURFACE_IN_SYSMEM");
    public static final uEyeOpticalCameraStatusEnum IS_DD_OVERLAY_NOT_ALLOWED                   = new uEyeOpticalCameraStatusEnum(86, "IS_DD_OVERLAY_NOT_ALLOWED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_OVERLAY_COLKEY_ERR                    = new uEyeOpticalCameraStatusEnum(87, "IS_DD_OVERLAY_COLKEY_ERR");
    public static final uEyeOpticalCameraStatusEnum IS_DD_OVERLAY_NOT_ENABLED                   = new uEyeOpticalCameraStatusEnum(88, "IS_DD_OVERLAY_NOT_ENABLED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_GET_DC_ERROR                          = new uEyeOpticalCameraStatusEnum(89, "IS_DD_GET_DC_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_DD_DDRAW_DLL_NOT_LOADED                  = new uEyeOpticalCameraStatusEnum(90, "IS_DD_DDRAW_DLL_NOT_LOADED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_THREAD_NOT_CREATED                    = new uEyeOpticalCameraStatusEnum(91, "IS_DD_THREAD_NOT_CREATED");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CANT_GET_CAPS                         = new uEyeOpticalCameraStatusEnum(92, "IS_DD_CANT_GET_CAPS");
    public static final uEyeOpticalCameraStatusEnum IS_DD_NO_OVERLAYSURFACE                     = new uEyeOpticalCameraStatusEnum(93, "IS_DD_NO_OVERLAYSURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DD_NO_OVERLAYSTRETCH                     = new uEyeOpticalCameraStatusEnum(94, "IS_DD_NO_OVERLAYSTRETCH");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CANT_CREATE_OVERLAYSURFACE            = new uEyeOpticalCameraStatusEnum(95, "IS_DD_CANT_CREATE_OVERLAYSURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DD_CANT_UPDATE_OVERLAYSURFACE            = new uEyeOpticalCameraStatusEnum(96, "IS_DD_CANT_UPDATE_OVERLAYSURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DD_INVALID_STRETCH                       = new uEyeOpticalCameraStatusEnum(97, "IS_DD_INVALID_STRETCH");
    
    public static final uEyeOpticalCameraStatusEnum IS_EV_INVALID_EVENT_NUMBER                  = new uEyeOpticalCameraStatusEnum(100, "IS_EV_INVALID_EVENT_NUMBER");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_MODE                             = new uEyeOpticalCameraStatusEnum(101, "IS_INVALID_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_FIND_FALCHOOK                       = new uEyeOpticalCameraStatusEnum(102, "IS_CANT_FIND_FALCHOOK");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_GET_HOOK_PROC_ADDR                  = new uEyeOpticalCameraStatusEnum(103, "IS_CANT_GET_HOOK_PROC_ADDR");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_CHAIN_HOOK_PROC                     = new uEyeOpticalCameraStatusEnum(104, "IS_CANT_CHAIN_HOOK_PROC");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_SETUP_WND_PROC                      = new uEyeOpticalCameraStatusEnum(105, "IS_CANT_SETUP_WND_PROC");
    public static final uEyeOpticalCameraStatusEnum IS_HWND_NULL                                = new uEyeOpticalCameraStatusEnum(106, "IS_HWND_NULL");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_UPDATE_MODE                      = new uEyeOpticalCameraStatusEnum(107, "IS_INVALID_UPDATE_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_NO_ACTIVE_IMG_MEM                        = new uEyeOpticalCameraStatusEnum(108, "IS_NO_ACTIVE_IMG_MEM");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_INIT_EVENT                          = new uEyeOpticalCameraStatusEnum(109, "IS_CANT_INIT_EVENT");
    public static final uEyeOpticalCameraStatusEnum IS_FUNC_NOT_AVAIL_IN_OS                     = new uEyeOpticalCameraStatusEnum(110, "IS_FUNC_NOT_AVAIL_IN_OS");
    public static final uEyeOpticalCameraStatusEnum IS_CAMERA_NOT_CONNECTED                     = new uEyeOpticalCameraStatusEnum(111, "IS_CAMERA_NOT_CONNECTED");
    public static final uEyeOpticalCameraStatusEnum IS_SEQUENCE_LIST_EMPTY                      = new uEyeOpticalCameraStatusEnum(112, "IS_SEQUENCE_LIST_EMPTY");
    public static final uEyeOpticalCameraStatusEnum IS_CANT_ADD_TO_SEQUENCE                     = new uEyeOpticalCameraStatusEnum(113, "IS_CANT_ADD_TO_SEQUENCE");
    public static final uEyeOpticalCameraStatusEnum IS_LOW_OF_SEQUENCE_RISC_MEM                 = new uEyeOpticalCameraStatusEnum(114, "IS_LOW_OF_SEQUENCE_RISC_MEM");
    public static final uEyeOpticalCameraStatusEnum IS_IMGMEM2FREE_USED_IN_SEQ                  = new uEyeOpticalCameraStatusEnum(115, "IS_IMGMEM2FREE_USED_IN_SEQ");
    public static final uEyeOpticalCameraStatusEnum IS_IMGMEM_NOT_IN_SEQUENCE_LIST              = new uEyeOpticalCameraStatusEnum(116, "IS_IMGMEM_NOT_IN_SEQUENCE_LIST");
    public static final uEyeOpticalCameraStatusEnum IS_SEQUENCE_BUF_ALREADY_LOCKED              = new uEyeOpticalCameraStatusEnum(117, "IS_SEQUENCE_BUF_ALREADY_LOCKED");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_DEVICE_ID                        = new uEyeOpticalCameraStatusEnum(118, "IS_INVALID_DEVICE_ID");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_BOARD_ID                         = new uEyeOpticalCameraStatusEnum(119, "IS_INVALID_BOARD_ID");
    public static final uEyeOpticalCameraStatusEnum IS_ALL_DEVICES_BUSY                         = new uEyeOpticalCameraStatusEnum(120, "IS_ALL_DEVICES_BUSY");
    public static final uEyeOpticalCameraStatusEnum IS_HOOK_BUSY                                = new uEyeOpticalCameraStatusEnum(121, "IS_HOOK_BUSY");
    public static final uEyeOpticalCameraStatusEnum IS_TIMED_OUT                                = new uEyeOpticalCameraStatusEnum(122, "IS_TIMED_OUT");
    public static final uEyeOpticalCameraStatusEnum IS_NULL_POINTER                             = new uEyeOpticalCameraStatusEnum(123, "IS_NULL_POINTER");
    public static final uEyeOpticalCameraStatusEnum IS_WRONG_HOOK_VERSION                       = new uEyeOpticalCameraStatusEnum(124, "IS_WRONG_HOOK_VERSION");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_PARAMETER                        = new uEyeOpticalCameraStatusEnum(125, "IS_INVALID_PARAMETER");
    public static final uEyeOpticalCameraStatusEnum IS_NOT_ALLOWED                              = new uEyeOpticalCameraStatusEnum(126, "IS_NOT_ALLOWED");
    public static final uEyeOpticalCameraStatusEnum IS_OUT_OF_MEMORY                            = new uEyeOpticalCameraStatusEnum(127, "IS_OUT_OF_MEMORY");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_WHILE_LIVE                       = new uEyeOpticalCameraStatusEnum(128, "IS_INVALID_WHILE_LIVE");
    public static final uEyeOpticalCameraStatusEnum IS_ACCESS_VIOLATION                         = new uEyeOpticalCameraStatusEnum(129, "IS_ACCESS_VIOLATION");
    public static final uEyeOpticalCameraStatusEnum IS_UNKNOWN_ROP_EFFECT                       = new uEyeOpticalCameraStatusEnum(130, "IS_UNKNOWN_ROP_EFFECT");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_RENDER_MODE                      = new uEyeOpticalCameraStatusEnum(131, "IS_INVALID_RENDER_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_THREAD_CONTEXT                   = new uEyeOpticalCameraStatusEnum(132, "IS_INVALID_THREAD_CONTEXT");
    public static final uEyeOpticalCameraStatusEnum IS_NO_HARDWARE_INSTALLED                    = new uEyeOpticalCameraStatusEnum(133, "IS_NO_HARDWARE_INSTALLED");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_WATCHDOG_TIME                    = new uEyeOpticalCameraStatusEnum(134, "IS_INVALID_WATCHDOG_TIME");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_WATCHDOG_MODE                    = new uEyeOpticalCameraStatusEnum(135, "IS_INVALID_WATCHDOG_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_PASSTHROUGH_IN                   = new uEyeOpticalCameraStatusEnum(136, "IS_INVALID_PASSTHROUGH_IN");
    public static final uEyeOpticalCameraStatusEnum IS_ERROR_SETTING_PASSTHROUGH_IN             = new uEyeOpticalCameraStatusEnum(137, "IS_ERROR_SETTING_PASSTHROUGH_IN");
    public static final uEyeOpticalCameraStatusEnum IS_FAILURE_ON_SETTING_WATCHDOG              = new uEyeOpticalCameraStatusEnum(138, "IS_FAILURE_ON_SETTING_WATCHDOG");
    public static final uEyeOpticalCameraStatusEnum IS_NO_USB20                                 = new uEyeOpticalCameraStatusEnum(139, "IS_NO_USB20");
    public static final uEyeOpticalCameraStatusEnum IS_CAPTURE_RUNNING                          = new uEyeOpticalCameraStatusEnum(140, "IS_CAPTURE_RUNNING");
    
    public static final uEyeOpticalCameraStatusEnum IS_MEMORY_BOARD_ACTIVATED                   = new uEyeOpticalCameraStatusEnum(141, "IS_MEMORY_BOARD_ACTIVATED");
    public static final uEyeOpticalCameraStatusEnum IS_MEMORY_BOARD_DEACTIVATED                 = new uEyeOpticalCameraStatusEnum(142, "IS_MEMORY_BOARD_DEACTIVATED");
    public static final uEyeOpticalCameraStatusEnum IS_NO_MEMORY_BOARD_CONNECTED                = new uEyeOpticalCameraStatusEnum(143, "IS_NO_MEMORY_BOARD_CONNECTED");
    public static final uEyeOpticalCameraStatusEnum IS_TOO_LESS_MEMORY                          = new uEyeOpticalCameraStatusEnum(144, "IS_TOO_LESS_MEMORY");
    public static final uEyeOpticalCameraStatusEnum IS_IMAGE_NOT_PRESENT                        = new uEyeOpticalCameraStatusEnum(145, "IS_IMAGE_NOT_PRESENT");
    public static final uEyeOpticalCameraStatusEnum IS_MEMORY_MODE_RUNNING                      = new uEyeOpticalCameraStatusEnum(146, "IS_MEMORY_MODE_RUNNING");
    public static final uEyeOpticalCameraStatusEnum IS_MEMORYBOARD_DISABLED                     = new uEyeOpticalCameraStatusEnum(147, "IS_MEMORYBOARD_DISABLED");
    
    public static final uEyeOpticalCameraStatusEnum IS_TRIGGER_ACTIVATED                        = new uEyeOpticalCameraStatusEnum(148, "IS_TRIGGER_ACTIVATED");
    public static final uEyeOpticalCameraStatusEnum IS_WRONG_KEY                                = new uEyeOpticalCameraStatusEnum(150, "IS_WRONG_KEY_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_CRC_ERROR                                = new uEyeOpticalCameraStatusEnum(151, "IS_CRC_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_NOT_YET_RELEASED                         = new uEyeOpticalCameraStatusEnum(152, "IS_NOT_YET_RELEASED");
    public static final uEyeOpticalCameraStatusEnum IS_NOT_CALIBRATED                           = new uEyeOpticalCameraStatusEnum(153, "IS_NOT_CALIBRATED");
    public static final uEyeOpticalCameraStatusEnum IS_WAITING_FOR_KERNEL                       = new uEyeOpticalCameraStatusEnum(154, "IS_WAITING_FOR_KERNEL");
    public static final uEyeOpticalCameraStatusEnum IS_NOT_SUPPORTED                            = new uEyeOpticalCameraStatusEnum(155, "IS_NOT_SUPPORTED");
    public static final uEyeOpticalCameraStatusEnum IS_TRIGGER_NOT_ACTIVATED                    = new uEyeOpticalCameraStatusEnum(156, "IS_TRIGGER_NOT_ACTIVATED");
    public static final uEyeOpticalCameraStatusEnum IS_OPERATION_ABORTED                        = new uEyeOpticalCameraStatusEnum(157, "IS_OPERATION_ABORTED");
    public static final uEyeOpticalCameraStatusEnum IS_BAD_STRUCTURE_SIZE                       = new uEyeOpticalCameraStatusEnum(158, "IS_BAD_STRUCTURE_SIZE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_BUFFER_SIZE                      = new uEyeOpticalCameraStatusEnum(159, "IS_INVALID_BUFFER_SIZE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_PIXEL_CLOCK                      = new uEyeOpticalCameraStatusEnum(160, "IS_INVALID_PIXEL_CLOCK");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_EXPOSURE_TIME                    = new uEyeOpticalCameraStatusEnum(161, "IS_INVALID_EXPOSURE_TIME");
    public static final uEyeOpticalCameraStatusEnum IS_AUTO_EXPOSURE_RUNNING                    = new uEyeOpticalCameraStatusEnum(162, "IS_AUTO_EXPOSURE_RUNNING");
    public static final uEyeOpticalCameraStatusEnum IS_CANNOT_CREATE_BB_SURF                    = new uEyeOpticalCameraStatusEnum(163, "IS_CANNOT_CREATE_BB_SURF");
    public static final uEyeOpticalCameraStatusEnum IS_CANNOT_CREATE_BB_MIX                     = new uEyeOpticalCameraStatusEnum(164, "IS_CANNOT_CREATE_BB_MIX");
    public static final uEyeOpticalCameraStatusEnum IS_BB_OVLMEM_NULL                           = new uEyeOpticalCameraStatusEnum(165, "IS_BB_OVLMEM_NULL");
    public static final uEyeOpticalCameraStatusEnum IS_CANNOT_CREATE_BB_OVL                     = new uEyeOpticalCameraStatusEnum(166, "IS_CANNOT_CREATE_BB_OVL");
    public static final uEyeOpticalCameraStatusEnum IS_NOT_SUPP_IN_OVL_SURF_MODE                = new uEyeOpticalCameraStatusEnum(167, "IS_NOT_SUPP_IN_OVL_SURF_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_SURFACE                          = new uEyeOpticalCameraStatusEnum(168, "IS_INVALID_SURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_SURFACE_LOST                             = new uEyeOpticalCameraStatusEnum(169, "IS_SURFACE_LOST");
    public static final uEyeOpticalCameraStatusEnum IS_RELEASE_BB_OVL_DC                        = new uEyeOpticalCameraStatusEnum(170, "IS_RELEASE_BB_OVL_DC");
    public static final uEyeOpticalCameraStatusEnum IS_BB_TIMER_NOT_CREATED                     = new uEyeOpticalCameraStatusEnum(171, "IS_BB_TIMER_NOT_CREATED");
    public static final uEyeOpticalCameraStatusEnum IS_BB_OVL_NOT_EN                            = new uEyeOpticalCameraStatusEnum(172, "IS_BB_OVL_NOT_EN");
    public static final uEyeOpticalCameraStatusEnum IS_ONLY_IN_BB_MODE                          = new uEyeOpticalCameraStatusEnum(173, "IS_ONLY_IN_BB_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_COLOR_FORMAT                     = new uEyeOpticalCameraStatusEnum(174, "IS_INVALID_COLOR_FORMAT");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_WB_BINNING_MODE                  = new uEyeOpticalCameraStatusEnum(175, "IS_INVALID_WB_BINNING_MODE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_I2C_DEVICE_ADDRESS               = new uEyeOpticalCameraStatusEnum(176, "IS_INVALID_I2C_DEVICE_ADDRESS");
    public static final uEyeOpticalCameraStatusEnum IS_COULD_NOT_CONVERT                        = new uEyeOpticalCameraStatusEnum(177, "IS_COULD_NOT_CONVERT");
    public static final uEyeOpticalCameraStatusEnum IS_TRANSFER_ERROR                           = new uEyeOpticalCameraStatusEnum(178, "IS_TRANSFER_ERROR");
    public static final uEyeOpticalCameraStatusEnum IS_PARAMETER_SET_NOT_PRESENT                = new uEyeOpticalCameraStatusEnum(179, "IS_PARAMETER_SET_NOT_PRESENT");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_CAMERA_TYPE                      = new uEyeOpticalCameraStatusEnum(180, "IS_INVALID_CAMERA_TYPE");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_HOST_IP_HIBYTE                   = new uEyeOpticalCameraStatusEnum(181, "IS_INVALID_HOST_IP_HIBYTE");
    public static final uEyeOpticalCameraStatusEnum IS_CM_NOT_SUPP_IN_CURR_DISPLAYMODE          = new uEyeOpticalCameraStatusEnum(182, "IS_CM_NOT_SUPP_IN_CURR_DISPLAYMODE");
    public static final uEyeOpticalCameraStatusEnum IS_NO_IR_FILTER                             = new uEyeOpticalCameraStatusEnum(183, "IS_NO_IR_FILTER");
    public static final uEyeOpticalCameraStatusEnum IS_STARTER_FW_UPLOAD_NEEDED                 = new uEyeOpticalCameraStatusEnum(184, "IS_STARTER_FW_UPLOAD_NEEDED");
    
    public static final uEyeOpticalCameraStatusEnum IS_DR_LIBRARY_NOT_FOUND                     = new uEyeOpticalCameraStatusEnum(185, "IS_DR_LIBRARY_NOT_FOUND");
    public static final uEyeOpticalCameraStatusEnum IS_DR_DEVICE_OUT_OF_MEMORY                  = new uEyeOpticalCameraStatusEnum(186, "IS_DR_DEVICE_OUT_OF_MEMORY");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_CREATE_SURFACE                 = new uEyeOpticalCameraStatusEnum(187, "IS_DR_CANNOT_CREATE_SURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_CREATE_VERTEX_BUFFER           = new uEyeOpticalCameraStatusEnum(188, "IS_DR_CANNOT_CREATE_VERTEX_BUFFER");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_CREATE_TEXTURE                 = new uEyeOpticalCameraStatusEnum(189, "IS_DR_CANNOT_CREATE_TEXTURE");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_LOCK_OVERLAY_SURFACE           = new uEyeOpticalCameraStatusEnum(190, "IS_DR_CANNOT_LOCK_OVERLAY_SURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_UNLOCK_OVERLAY_SURFACE         = new uEyeOpticalCameraStatusEnum(191, "IS_DR_CANNOT_UNLOCK_OVERLAY_SURFACE");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_GET_OVERLAY_DC                 = new uEyeOpticalCameraStatusEnum(192, "IS_DR_CANNOT_GET_OVERLAY_DC");
    public static final uEyeOpticalCameraStatusEnum IS_DR_CANNOT_RELEASE_OVERLAY_DC             = new uEyeOpticalCameraStatusEnum(193, "IS_DR_CANNOT_RELEASE_OVERLAY_DC");
    public static final uEyeOpticalCameraStatusEnum IS_DR_DEVICE_CAPS_INSUFFICIENT              = new uEyeOpticalCameraStatusEnum(194, "IS_DR_DEVICE_CAPS_INSUFFICIENT");
    public static final uEyeOpticalCameraStatusEnum IS_INCOMPATIBLE_SETTING                     = new uEyeOpticalCameraStatusEnum(195, "IS_INCOMPATIBLE_SETTING");
    public static final uEyeOpticalCameraStatusEnum IS_DR_NOT_ALLOWED_WHILE_DC_IS_ACTIVE        = new uEyeOpticalCameraStatusEnum(196, "IS_DR_NOT_ALLOWED_WHILE_DC_IS_ACTIVE");
    public static final uEyeOpticalCameraStatusEnum IS_DEVICE_ALREADY_PAIRED                    = new uEyeOpticalCameraStatusEnum(197, "IS_DEVICE_ALREADY_PAIRED");
    public static final uEyeOpticalCameraStatusEnum IS_SUBNETMASK_MISMATCH                      = new uEyeOpticalCameraStatusEnum(198, "IS_SUBNETMASK_MISMATCH");
    public static final uEyeOpticalCameraStatusEnum IS_SUBNET_MISMATCH                          = new uEyeOpticalCameraStatusEnum(199, "IS_SUBNET_MISMATCH");
    public static final uEyeOpticalCameraStatusEnum IS_INVALID_IP_CONFIGURATION                 = new uEyeOpticalCameraStatusEnum(200, "IS_INVALID_IP_CONFIGURATION");
    public static final uEyeOpticalCameraStatusEnum IS_DEVICE_NOT_COMPATIBLE                    = new uEyeOpticalCameraStatusEnum(201, "IS_DEVICE_NOT_COMPATIBLE");
    public static final uEyeOpticalCameraStatusEnum IS_NETWORK_FRAME_SIZE_INCOMPATIBLE          = new uEyeOpticalCameraStatusEnum(202, "IS_NETWORK_FRAME_SIZE_INCOMPATIBLE");
    public static final uEyeOpticalCameraStatusEnum IS_NETWORK_CONFIGURATION_INVALID            = new uEyeOpticalCameraStatusEnum(203, "IS_NETWORK_CONFIGURATION_INVALID");
    public static final uEyeOpticalCameraStatusEnum IS_ERROR_CPU_IDLE_STATES_CONFIGURATION      = new uEyeOpticalCameraStatusEnum(204, "IS_ERROR_CPU_IDLE_STATES_CONFIGURATION");
    
    /**
     * @author Cheah Lee Herng
    */
    private uEyeOpticalCameraStatusEnum(int id, String statusString)
    {
        super(id);
        Object previous = _statusStringToOpticalCameraStatusEnumMap.put(id, statusString);
        Assert.expect(previous == null);                
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public static String getStatusName(int id)
    {
        Assert.expect(_statusStringToOpticalCameraStatusEnumMap != null);
        String statusName = _statusStringToOpticalCameraStatusEnumMap.get(id);
        Assert.expect(statusName != null);
        
        return statusName;
    }
}
