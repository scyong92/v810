package com.axi.v810.hardware;

import com.axi.v810.util.StringLocalizer;
import com.axi.v810.util.XrayTesterException;

/**
 * @author Kee Chin Seong
*/
public class SerialPortWatcher extends Thread 
{
    boolean _threadStart = false;
    boolean _suspended = false;
    
    /**
     * @author Kee Chin Seong
     * @run() 
     */
    public void run() 
    {
       while(_threadStart)
       {
          try
          {
            waitWhileSuspended();

            if(BarcodeReaderManager.getInstance().isPortExist(BarcodeReaderManager.getInstance().getBarcodeReaderSerialPortName()) == false)
            {
               BarcodeReaderManager.getInstance().disconnectSerialPort();
               triggerObservable();
               this.pauseThread();
            }
          }
          catch (InterruptedException x) 
          {
              System.out.println("interrupted in SerialPortWatcher()");
          }
          catch (XrayTesterException e)
          {
              
          }
       }
    }
    
    /**
     * @author Kee Chin Seong
     * @throws XrayTesterException 
     */
    private void triggerObservable() throws XrayTesterException
    {
       HardwareObservable.getInstance().stateChangedBegin(this, SerialPortCommEventEnum.DISCONNECT); 
    }
    
    /**
     * @author Kee Chin Seong
     * @throws InterruptedException 
     */
    private void waitWhileSuspended() throws InterruptedException 
    {
       while(_suspended) 
       {
         Thread.sleep(200);
       }
    }
    
    /**
     * @author Kee Chin Seong
     * @Thread Start()
     */
    public void startThread()
    {
      _threadStart = true;
      
      if (this.getState() == Thread.State.NEW)
         this.start();
      else
         this.resumeThread();
    }
    
    /**
     * @author Kee Chin Seong
     * @Thread Stop 
     */
    public void stopThread()
    {
      _threadStart = false;
    }
    
    /**
     * @author Kee Chin Seong
     * @Thread Paused
     */
    public void pauseThread()
    {
      _suspended = true;
    }

    /**
     * @author Kee Chin Seong
     * @Thread Resumed
     */
    public void resumeThread()
    {
      _suspended = false; 
    }
}