package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import static com.axi.v810.hardware.HardwareObject.getConfig;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the Motorized Height Level Sensor.  The Motorized Height Level Sensor is used to control
 * Height Level Sensor up and down movement.  For S2Ex, the Height Level Sensor is able
 * to move up and down, to check the Height Level at different magnification.
 * @author Anthony Fong
 */
public class XrayMotorizedHeightLevelSensor extends HardwareObject implements XrayActuatorInt
{
  private static XrayMotorizedHeightLevelSensor _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the X-Ray MotorizedHeightLevelSensor is up or down.
  private boolean _xrayMotorizedHeightLevelSensorIsDown;
  private boolean _xrayMotorizedHeightLevelSensorIsNotUp;
  private boolean _xrayMotorizedHeightLevelSensorIsUp;
  private boolean _xrayMotorizedHeightLevelSensorIsNotDown;  
  private boolean _xrayMotorizedHeightLevelSensorIsHome;
  private boolean _xrayMotorizedHeightLevelSensorIsNotHome;
  private static boolean _WAIT_NOT_ABORTABLE = false;


 
  /**
   * This constructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private XrayMotorizedHeightLevelSensor()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();

  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized XrayMotorizedHeightLevelSensor getInstance()
  {
    if (_instance == null)
      _instance = new XrayMotorizedHeightLevelSensor();

    return _instance;
  }
  
  /**
   * Move the X-Ray MotorizedHeightLevelSensor down (High Mag).
   * @author Anthony Fong
   */
  public void down(boolean loadConfig) throws XrayTesterException
  {
    
//    System.out.println("MotorizedHeightLevelSensor Down");
    boolean initStateIsDown = isDown();
    if(initStateIsDown)
      return;
    

    _hardwareObservable.stateChangedBegin(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HIGH_MAG);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the close action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();

      _digitalIo.moveMotorizedHeightLevelToHighMagPosition();
      waitUntilHighMag();
      
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForDown();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HIGH_MAG);

  }

  /**
   * Move the X-Ray MotorizedHeightLevelSensor up.
   * @author Anthony Fong
   */
  public void up(boolean loadConfig) throws XrayTesterException
  {
   
//    System.out.println("MotorizedHeightLevelSensor Up");
    boolean initStateIsUp = isUp();

    if(initStateIsUp)
      return;
        
    _hardwareObservable.stateChangedBegin(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_LOW_MAG);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.moveMotorizedHeightLevelToLowMagPosition();
      waitUntilLowMag();
      
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForUp();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_LOW_MAG);

  }
  
  /**
   * Move the X-Ray MotorizedHeightLevelSensor up.
   * @author Swee Yee Wong
   */
  public void upToPosition1(boolean loadConfig) throws XrayTesterException
  {
    // 20 Jan 2015 - motorized height level sensor is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    up(loadConfig);
  }
  
  /**
   * Move the X-Ray MotorizedHeightLevelSensor up.
   * @author Swee Yee Wong
   */
  public void upToPosition2(boolean loadConfig) throws XrayTesterException
  {
    // 20 Jan 2015 - motorized height level sensor is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    up(loadConfig);
  }

  
  /**
   * Move the X-Ray MotorizedHeightLevelSensor Startup Homing is used during startup to double confirm that the CP Motor is properly home.
   * CP Motor may fall down if overloaded or tuning parameters are incorrect.
   * @author Anthony Fong
   */
  public void startupHoming() throws XrayTesterException
  {        
    _hardwareObservable.stateChangedBegin(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.moveMotorizedHeightLevelToHomePosition();
      waitUntilNotHome();
      sleep(200);
      waitUntilHome();
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForHome();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME);
  }  
  
  /**
   * Move the X-Ray MotorizedHeightLevelSensor home.
   * @author Anthony Fong
   */
  public void home(boolean loadConfig, boolean isHighMag) throws XrayTesterException
  {
 
//    System.out.println("MotorizedHeightLevelSensor Home");
    boolean initStateIsHome = isHome();

    if(initStateIsHome)
      return;
        

    _hardwareObservable.stateChangedBegin(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.moveMotorizedHeightLevelToHomePosition();
      waitUntilHome();
      
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForHome();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME);

  }
  
  /**
   * @return long returns how long last open/close action time.
   * @author Anthony Fong
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Anthony Fong
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _xrayMotorizedHeightLevelSensorIsDown = _digitalIo.isXrayMotorizedHeightLevelSafetySensorDown();
    _xrayMotorizedHeightLevelSensorIsNotUp = _digitalIo.isXrayMotorizedHeightLevelSafetySensorNotUp();

    _xrayMotorizedHeightLevelSensorIsUp = _digitalIo.isXrayMotorizedHeightLevelSafetySensorUp();
    _xrayMotorizedHeightLevelSensorIsNotDown = _digitalIo.isXrayMotorizedHeightLevelSafetySensorNotDown();
      
    _xrayMotorizedHeightLevelSensorIsHome = _digitalIo.isXrayMotorizedHeightLevelSafetySensorUp();
    _xrayMotorizedHeightLevelSensorIsNotHome = _digitalIo.isXrayMotorizedHeightLevelSafetySensorNotUp();
     
  }

  /**
   * When the X-Ray MotorizedHeightLevelSensor is being commanded to up/down, there is a window of time
   * where the MotorizedHeightLevelSensor is neither up nor down.
   * @return true if the MotorizedHeightLevelSensor sensor are giving consistent state information.
   * @author Anthony Fong
   */
  private boolean isHardwareDownSettled() throws XrayTesterException
  {
    boolean isSettled = (_xrayMotorizedHeightLevelSensorIsDown == _xrayMotorizedHeightLevelSensorIsNotUp);
    return isSettled;
  }
  private boolean isHardwareUpSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayMotorizedHeightLevelSensorIsUp == _xrayMotorizedHeightLevelSensorIsNotDown;
    return isSettled;
  }
  private boolean isHardwareHomeSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayMotorizedHeightLevelSensorIsHome == _xrayMotorizedHeightLevelSensorIsNotDown;
    return isSettled;
  }
  /**
   * @return true if the X-Ray MotorizedHeightLevelSensor is for sure home.
   * @throws XrayTesterException exception will be thrown if the X-Ray MotorizedHeightLevelSensor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isHome_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is home.
    updateSensorStatus();

    if (isHardwareHomeSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoHomeException());

    return _xrayMotorizedHeightLevelSensorIsHome;
  }
  
  public boolean isHome() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is home.
    if (isSimulationModeOn() == false)
    {
      updateSensorStatus();
      if (isHardwareHomeSettled() == false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoHomeException());
      }
    }

    return _xrayMotorizedHeightLevelSensorIsHome;
  }
  
  /**
   * @return true if the X-Ray MotorizedHeightLevelSensor is for sure down or low mag.
   * @throws XrayTesterException exception will be thrown if the X-Ray MotorizedHeightLevelSensor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isDown_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is down or low mag.
    updateSensorStatus();

    if (isHardwareDownSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayMotorizedHeightLevelSensorIsStuckException());

    return _xrayMotorizedHeightLevelSensorIsDown;
  }
  public boolean isDown() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is down or low mag.
    if (isSimulationModeOn() == false)
    {
      updateSensorStatus();
    }
    return _xrayMotorizedHeightLevelSensorIsDown;
  }

  /**
   * @return true if the X-Ray MotorizedHeightLevelSensor is for sure up or high mag.
   * @throws XrayTesterException exception will be thrown if the X-Ray MotorizedHeightLevelSensor is neither
   * up nor down.
   * @author Anthony Fong
   */
  private boolean isUp_ForInternalUse() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is up or high mag.
    updateSensorStatus();

    if (isHardwareUpSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayMotorizedHeightLevelSensorIsStuckException());

    return _xrayMotorizedHeightLevelSensorIsUp;
  }
  
  public boolean isUp() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray MotorizedHeightLevelSensor is up or high mag.
    if (isSimulationModeOn() == false)
    {
      updateSensorStatus();
    }
    return _xrayMotorizedHeightLevelSensorIsUp;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition1() throws XrayTesterException
  {
    // 20 Jan 2015 - motorized height level sensor is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    return isUp();
  }
  
  /**
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition2() throws XrayTesterException
  {
    // 20 Jan 2015 - motorized height level sensor is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    return isUp();
  }
    
  /**
   * @author Anthony Fong
   */
  private void waitUntilHighMag() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayMotorizedHeightLevelMoveHighMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayMotorizedHeightLevelSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayMotorizedHeightLevelSensorIsDown = false;
        updateSensorStatus();
        if (isHardwareDownSettled())
          xrayMotorizedHeightLevelSensorIsDown = _xrayMotorizedHeightLevelSensorIsDown;

        return xrayMotorizedHeightLevelSensorIsDown;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoHighMagException();
      }
    };
    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilLowMag() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayMotorizedHeightLevelMoveLowMagTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayMotorizedHeightLevelSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayMotorizedHeightLevelSensorIsUp = false;
        updateSensorStatus();
        if (isHardwareUpSettled())
          xrayMotorizedHeightLevelSensorIsUp = _xrayMotorizedHeightLevelSensorIsUp;

        return xrayMotorizedHeightLevelSensorIsUp;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoLowMagException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  /**
   * @author Anthony Fong
   */
  private void waitUntilHome() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayMotorizedHeightLevelMoveHomeTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayMotorizedHeightLevelSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayMotorizedHeightLevelSensorIsHome = false;
        updateSensorStatus();
        if (isHardwareHomeSettled())
          xrayMotorizedHeightLevelSensorIsHome = _xrayMotorizedHeightLevelSensorIsHome;

        return xrayMotorizedHeightLevelSensorIsHome;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoHomeException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
  /**
   * @author Anthony Fong
   */
  private void waitUntilNotHome() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayMotorizedHeightLevelMoveHomeTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayMotorizedHeightLevelSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayMotorizedHeightLevelSensorIsNotHome = false;
        updateSensorStatus();
        if (_xrayMotorizedHeightLevelSensorIsNotHome && _xrayMotorizedHeightLevelSensorIsNotDown &&  _xrayMotorizedHeightLevelSensorIsNotUp)
          xrayMotorizedHeightLevelSensorIsNotHome = _xrayMotorizedHeightLevelSensorIsNotHome;

        return xrayMotorizedHeightLevelSensorIsNotHome;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayMotorizedHeightLevelSensorFailedToGoHomeException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }
  
    /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Anthony Fong
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
   
//  /**
//   * @return boolean whether the X-Ray Actuator Interface is installed.
//   * @author Anthony Fong
//   */
//  public boolean isActuatorInstalled()
//  {
//    return isInstalled();
//  }

  /**
   * For Variable Magnification, the X-Ray MotorizedHeightLevelSensor is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray MotorizedHeightLevelSensor is installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    if (XrayTester.isS2EXEnabled())
    {
      return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SAFETY_SENSOR_INSTALLED);
    }
    else
    {
      return false;
    } 
  }
  
  /**
   * This method is used to update simulation sensor readings.
   * @author Swee Yee Wong
   */
  private void updateSimulationSensorStatusForUp() throws XrayTesterException
  {
    _xrayMotorizedHeightLevelSensorIsDown = false;
    _xrayMotorizedHeightLevelSensorIsNotDown = true;

    _xrayMotorizedHeightLevelSensorIsUp = true;
    _xrayMotorizedHeightLevelSensorIsNotUp = false;
      
    _xrayMotorizedHeightLevelSensorIsHome = false;
    _xrayMotorizedHeightLevelSensorIsNotHome = true;
  }
  
  /**
   * This method is used to update simulation sensor readings.
   * @author Swee Yee Wong
   */
  private void updateSimulationSensorStatusForDown() throws XrayTesterException
  {
    _xrayMotorizedHeightLevelSensorIsDown = true;
    _xrayMotorizedHeightLevelSensorIsNotDown = false;

    _xrayMotorizedHeightLevelSensorIsUp = false;
    _xrayMotorizedHeightLevelSensorIsNotUp = true;
      
    _xrayMotorizedHeightLevelSensorIsHome = false;
    _xrayMotorizedHeightLevelSensorIsNotHome = true;
  }
  
  /**
   * This method is used to update simulation sensor readings.
   * @author Swee Yee Wong
   */
  private void updateSimulationSensorStatusForHome() throws XrayTesterException
  {
    _xrayMotorizedHeightLevelSensorIsDown = false;
    _xrayMotorizedHeightLevelSensorIsNotDown = true;

    _xrayMotorizedHeightLevelSensorIsUp = false;
    _xrayMotorizedHeightLevelSensorIsNotUp = true;
      
    _xrayMotorizedHeightLevelSensorIsHome = true;
    _xrayMotorizedHeightLevelSensorIsNotHome = false;
  }
}
