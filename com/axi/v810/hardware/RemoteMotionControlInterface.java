package com.axi.v810.hardware;

import java.io.*;
import java.util.*;
import java.rmi.*;

import com.axi.util.*;

/**
 * 
 * @author Rick Gaudette
 */
public interface RemoteMotionControlInterface extends Remote
{
  public void remoteInitialize(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException, NativeHardwareException;

  public void remoteHome(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public void remoteEnable(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public void remoteDisable(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public void remoteSetPointToPointMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile) throws RemoteException, NativeHardwareException;

  public void remotePointToPointMove(int motionControllerId, int motionAxisToMove, int xInNanometers, int yInNanometers, int railWidthInNanometers) throws RemoteException, NativeHardwareException;

  public void remoteSetScanMotionProfile(int motionControllerId, int motionAxisToMove, int motionProfile) throws RemoteException, NativeHardwareException;

  // Swee Yee Wong - Include starting pulse information for scan pass adjustment
  // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
  public void remoteEnableScanPath(int motionControllerId, int[] scanPath, int distanceInNanometersPerPulse, int numberOfStartPulses) throws RemoteException, NativeHardwareException;

  public void remoteDisableScanPath(int motionControllerId) throws RemoteException, NativeHardwareException;

  public void remoteRunNextScanPass(int motionControllerId) throws RemoteException, NativeHardwareException;

  public int remoteGetScanPathExecutionTimeInMilliseconds(int motionControllerId) throws RemoteException, NativeHardwareException;

  public void remoteSetPositionPulse(int motionControllerId, int distanceInNanometersPerPulse, int numberOfStartPulses) throws RemoteException, NativeHardwareException;

  public void remoteStop(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public int remoteGetMinimumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public int remoteGetMaximumPositionLimitInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public int remoteGetMinimumScanPassLengthInNanometers(int motionControllerId) throws RemoteException, NativeHardwareException;

  public int remoteGetMaximumScanPassLengthInNanometers(int motionControllerId, int yAxisScanPassStartPositionInNanometers, int scanPassDirection) throws RemoteException, NativeHardwareException;

  public int remoteGetActualPositionInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public boolean remoteIsScanPathDone(int motionControllerId) throws RemoteException, NativeHardwareException;

  public boolean remoteIsScanPassDone(int motionControllerId) throws RemoteException, NativeHardwareException;

  public boolean remoteIsHomeSensorClear(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public List<HardwareConfigurationDescriptionInt> remoteGetConfigurationDescription(int motionControllerId, String parameterGroupSeparator, ArrayList<String> keys, ArrayList<String> parameters) throws RemoteException, NativeHardwareException;

  public boolean remoteIsAxisEnabled(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public void remoteSetHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove, int hysteresisOffsetInNanometers) throws RemoteException, NativeHardwareException;

  public int remoteGetForwardDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public int remoteGetReverseDirectionHysteresisOffsetInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public int remoteGetValidPositionPulseRateInNanometersPerPulse(int motionControllerId, int requestedNanometersPerPulse) throws RemoteException, NativeHardwareException;

  public boolean remoteIsInitialized(int motionControllerModel, int motionControllerId) throws RemoteException;

  public int remoteGetMinimumScanStepWidthInNanometers(int motionControllerId) throws RemoteException, NativeHardwareException;

  public int remoteGetMaximumScanStepWidthInNanometers(int motionControllerId, int xAxisScanStepStartPositionInNanometers, int scanStepDirection) throws RemoteException, NativeHardwareException;

  public void remoteSetServiceModeConfiguration(int motionControllerModel, int motionControllerId) throws RemoteException, NativeHardwareException;

  public int remoteGetYaxisScanStartPositionLimitInNanometers(int _motionControllerId, int scanPassDirection) throws RemoteException, NativeHardwareException;

  public Serializable remoteGetServiceModeConfigurationStatus(int motionControllerModel, int motionControllerId, Object key, ArrayList<String> parameters) throws RemoteException;

  public void remoteAbortServiceModeConfiguration(int motionControllerModel, int motionControllerId) throws RemoteException;

  public void remoteSetActualPositionInNanometers(int motionControllerId, int motionAxisToMove, int actualPositionInNanometers) throws RemoteException, NativeHardwareException;

  public void remoteShutdown(int motionControllerId, int motionControllerModel) throws RemoteException;

  public void remoteSetSimulationMode(int motionControllerId, int motionControllerModel) throws RemoteException;

  public void remoteSetSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException;

  public void remoteClearSimulationMode(int motionControllerId, int motionControllerModel) throws RemoteException;

  public void remoteClearSimulationMode(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException;

  public boolean remoteIsSimulationModeOn(int motionControllerId, int motionControllerModel) throws RemoteException;

  public boolean remoteIsSimulationModeOn(int motionControllerId, int motionControllerModel, int motionAxisToMove) throws RemoteException;

  public Serializable remoteGetActiveMotionProfile(int motionControllerId, int motionAxisToMove, IntegerRef motionProfileTypeId, IntegerRef motionProfile) throws RemoteException, NativeHardwareException;

  public void remoteSetHardwareResetMode(int motionControllerId, int motionControllerModel, boolean hardwareResetModeOn) throws RemoteException;

  public void remotePauseScanPath(int motionControllerId) throws RemoteException, NativeHardwareException;

  public int remoteGetPositionAccuracyToleranceInNanometers(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

  public boolean remoteIsHomingRequired(int motionControllerId, int motionAxisToMove) throws RemoteException, NativeHardwareException;

}