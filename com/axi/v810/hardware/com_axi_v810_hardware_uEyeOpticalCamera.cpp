#include <map>

#ifdef _MSC_VER
  #pragma warning(disable: 4786)
#endif  

#include "windows.h"
#include "jawt.h"
#include "jawt_md.h"
#include "com/axi/util/jniUtil.h"
#include "cpp/util/src/sptAssert.h"
#include "com_axi_v810_hardware_uEyeOpticalCamera.h"
#include "thirdParty/uEye/Include/uEye.h"
#include "../../util/image/jniImageUtil.h"
#include "thirdParty/Psp3DAlgo/include/cpsp3dalgo.h"

#include "ippi.h"

using namespace std;

class CAMERAINFO
{                 
public:
  HIDS hCam;
  double exposureTime;
  int imageCount;
  char* pcImageMemory; 
  char* accumulateMemory; 
  char* ppcMemLast;
  INT lMemoryId;   
  
  unsigned char* pObjImg1;
  unsigned char* pObjImg2;
  unsigned char* pObjImg3;
  unsigned char* pObjImg4;
  unsigned char* pRefImg1;
  unsigned char* pRefImg2;
  unsigned char* pRefImg3;
  unsigned char* pRefImg4;
  unsigned char* pObjPlusThreeImg1;
  unsigned char* pObjPlusThreeImg2;
  unsigned char* pObjPlusThreeImg3;
  unsigned char* pObjPlusThreeImg4;
  
  // ****************************************************
  // New variables supporting new PSP algorithm library
  unsigned char* pNewObjImg1;
  unsigned char* pNewObjImg2;
  unsigned char* pNewObjImg3;
  unsigned char* pNewObjImg4;
  unsigned char* pNewObjImg5;
  unsigned char* pNewObjImg6;
  unsigned char* pNewObjImg7;
  unsigned char* pNewObjImg8;
  
  unsigned char* pNewRefImg1;
  unsigned char* pNewRefImg2;
  unsigned char* pNewRefImg3;
  unsigned char* pNewRefImg4;
  unsigned char* pNewRefImg5;
  unsigned char* pNewRefImg6;
  unsigned char* pNewRefImg7;
  unsigned char* pNewRefImg8;
  
  std::vector<Roi3DStruct> inspectionRoiVector;
  
  int numberOfProjector;
  bool debugMode;
  
  // ****************************************************
  
  int *pNewCoordinateXInPixel;
  int *pNewCoordinateYInPixel;
  float *pHeightMap;
  
  unsigned int roiCount;
  unsigned int *centerX;
  unsigned int *centerY;
  unsigned int *width;
  unsigned int *height;
  
  int offsetValue;
  
  double *magnificationData;
  
  Cpsp3DAlgo* newPspAlgorithm;
  
  // Default Constructor
  CAMERAINFO()
  {
    hCam = 0;
    exposureTime = 0;
    imageCount = 0;
    pcImageMemory = NULL;     
    ppcMemLast = NULL;
    lMemoryId = 0;
    
    pObjImg1 = NULL;
    pObjImg2 = NULL;
    pObjImg3 = NULL;
    pObjImg4 = NULL;
    
    pRefImg1 = NULL;
    pRefImg2 = NULL;
    pRefImg3 = NULL;
    pRefImg4 = NULL;
    
    pObjPlusThreeImg1 = NULL;
    pObjPlusThreeImg2 = NULL;
    pObjPlusThreeImg3 = NULL;
    pObjPlusThreeImg4 = NULL;
    
    pNewObjImg1 = NULL;
    pNewObjImg2 = NULL;
    pNewObjImg3 = NULL;
    pNewObjImg4 = NULL;
    pNewObjImg5 = NULL;
    pNewObjImg6 = NULL;
    pNewObjImg7 = NULL;
    pNewObjImg8 = NULL;
    
    pNewRefImg1 = NULL;
    pNewRefImg2 = NULL;
    pNewRefImg3 = NULL;
    pNewRefImg4 = NULL;
    pNewRefImg5 = NULL;
    pNewRefImg6 = NULL;
    pNewRefImg7 = NULL;
    pNewRefImg8 = NULL;
    
    pNewCoordinateXInPixel = new int [256];
    pNewCoordinateYInPixel = new int [256];
    pHeightMap = new float [256];
    
    roiCount = 0;
    centerX = new unsigned int [256];
    centerY = new unsigned int [256];
    width = new unsigned int [256];
    height = new unsigned int [256];
    offsetValue = 0;
    magnificationData = new double [12];
    
    // Support new 3D Algorithm library
    newPspAlgorithm = new Cpsp3DAlgo();
    numberOfProjector = 1;
    debugMode = false;
  }
  
  void reset()
  {  
    imageCount = 0;
    
    // Reset pointer values
    for(unsigned int i=0; i < 256; ++i)
    {
      pNewCoordinateXInPixel[i] = 0;
      pNewCoordinateYInPixel[i] = 0;
      pHeightMap[i] = 0.0;
      
      centerX[i] = 0;
      centerY[i] = 0;
      width[i] = 0;
      height[i] = 0;
    }
    
    // Reset magnification data
    for(unsigned int i=0; i < 12; ++i)
    {
      magnificationData[i] = 0.0;
    }
    
    // Reset ROI pointer
    inspectionRoiVector.clear();
    debugMode = false;
  }
};

static map<int, CAMERAINFO*> _deviceIdToInstanceMap;
static CAMERAINFO* _pCameraInfo = 0;

double _setExposureTime = 16.667;

const int _nMaxImageWidthPixel = 360960;	// 752 x 480

///////////////////////////////////////////////////////////////////////////////
//
// @author Cheah Lee Herng
//////////////////////////////////////////////////////////////////////////////
static void setDeviceIdToInstanceMap(int deviceId)
{
  map<int, CAMERAINFO*>::iterator it = _deviceIdToInstanceMap.find(deviceId);
  CAMERAINFO* pCameraInfo = 0;

  if (it == _deviceIdToInstanceMap.end())
  {
    pCameraInfo = new CAMERAINFO();
    
    _deviceIdToInstanceMap.insert(make_pair(deviceId, pCameraInfo));

    pCameraInfo = 0;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// @author Cheah Lee Herng
//////////////////////////////////////////////////////////////////////////////
static CAMERAINFO* getCameraInfo(int deviceId)
{
  map<int, CAMERAINFO*>::iterator it = _deviceIdToInstanceMap.find(deviceId);
  sptAssert(it != _deviceIdToInstanceMap.end());

  return it->second;  
}

/**
 *  Function which allows us to log information into a text file. This is for
 *  debugging purpose. 
 */ 
void WriteLog(const char* szString)
{
  FILE* pFile = fopen("C://logFile.txt", "a");
  fprintf(pFile, "%s\n",szString);
  fclose(pFile);
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeInitialize
 * Method:    nativeInitialize
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeInitialize(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jint windowHandle)
{  
  try
  {
   int result;
   int num = 0;
   int nMaxWidth = 0;
   int nMaxHeight = 0;
   CRITICAL_SECTION csStart;
   SENSORINFO sensorInfo;
   
   setDeviceIdToInstanceMap(cameraDeviceId);
   _pCameraInfo = getCameraInfo(cameraDeviceId);
   
   // If we have an open camera, close it
	 if (_pCameraInfo->hCam != 0)
   {
    is_ExitCamera (_pCameraInfo->hCam);
   }
   
   HWND win32Handle = (HWND)windowHandle;
	 //_pCameraInfo->hCam = ((HIDS) (cameraDeviceId | IS_USE_DEVICE_ID)); // open with device ID
	 _pCameraInfo->hCam = (HIDS) cameraDeviceId;
   	 
   result = is_InitCamera (&_pCameraInfo->hCam, win32Handle);    
   if (result == IS_SUCCESS)
   {
      result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      if (result == IS_SUCCESS)
      {
        nMaxWidth  = sensorInfo.nMaxWidth;
        nMaxHeight = sensorInfo.nMaxHeight;
        
        InitializeCriticalSection (&csStart);
        
        // Set color mode
        result = is_SetColorMode( _pCameraInfo->hCam, IS_CM_MONO8 );        
        if (result == IS_SUCCESS)
        {
          // set the image size to capture
          result = is_SetImageSize(_pCameraInfo->hCam, nMaxWidth, nMaxHeight);          
          if (result == IS_SUCCESS)
          {
            // Set display mode to Direct3D
            result = is_SetDisplayMode(_pCameraInfo->hCam, IS_SET_DM_DIRECT3D);            
            if (result == IS_SUCCESS)
            {
              result = is_SetExposureTime (_pCameraInfo->hCam, (double) _setExposureTime, &_pCameraInfo->exposureTime);
              if (result == IS_SUCCESS)
              {
                 result = is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);
                 if (result == IS_SUCCESS)
                 {
                    result = is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
                 }
              
              }
            }
          }
        }
      }
   }  
   return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetWindowHandle
 * Method:    nativeGetWindowHandle
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetWindowHandle(JNIEnv *pEnv, jobject javaObject, jobject comp)
{
 try
  {
    HMODULE hAWT = 0;
    HWND hWnd = 0;
    typedef jboolean (JNICALL *PJAWT_GETAWT)(JNIEnv*, JAWT*);
    JAWT awt;
    JAWT_DrawingSurface* ds;
    JAWT_DrawingSurfaceInfo* dsi;
    JAWT_Win32DrawingSurfaceInfo* dsi_win;
    jboolean result;
    jint lock;

    
    //Load AWT Library
    if(!hAWT)
        //for Java 1.4
        hAWT = LoadLibrary("jawt.dll");
      
    if(!hAWT)
        //for Java 1.3
        hAWT = LoadLibrary("awt.dll");

    if(hAWT)
    {
      PJAWT_GETAWT JAWT_GetAWT = (PJAWT_GETAWT)GetProcAddress(hAWT, "JAWT_GetAWT");
      if(JAWT_GetAWT)
      {
        awt.version = JAWT_VERSION_1_4; // Init here with JAWT_VERSION_1_3 or JAWT_VERSION_1_4
        //Get AWT API Interface
        result = JAWT_GetAWT(pEnv, &awt);
        if(result != JNI_FALSE)
        {
            ds = awt.GetDrawingSurface(pEnv, comp);
            if(ds != NULL)
            {
                lock = ds->Lock(ds);
    
                if((lock & JAWT_LOCK_ERROR) == 0)
                {
                    dsi = ds->GetDrawingSurfaceInfo(ds);
                    if(dsi)
                    {
                        dsi_win = (JAWT_Win32DrawingSurfaceInfo*)dsi->platformInfo;
                        if(dsi_win)
                        {
                          hWnd = dsi_win->hwnd;
                        }
                        else 
                        {
                          hWnd = (HWND) -1;
                        }
                        ds->FreeDrawingSurfaceInfo(dsi);
                    }
                    else 
                    {
                      hWnd = (HWND) -2;
                    }
                    ds->Unlock(ds);
                }
                else 
                {
                  hWnd = (HWND) -3;
                }
                awt.FreeDrawingSurface(ds);
            }
            else 
            {
              hWnd = (HWND) -4;
            }
        }
        else 
        {
          hWnd = (HWND) -5;
        }
      }
      else 
      {
        hWnd = (HWND) -6;
      }
    }
    else 
    {
      hWnd = (HWND) -7;
    }
    return (jint)hWnd;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeLiveVideo
 * Method:    nativeLiveVideo
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeLiveVideo(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    int result = is_CaptureVideo( _pCameraInfo->hCam, IS_WAIT );
    if (result == IS_SUCCESS)
    {
      result = is_DirectRenderer (_pCameraInfo->hCam, DR_ENABLE_SEMI_TRANSPARENT_OVERLAY, NULL, NULL);
    }
    return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   } 
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeExitCamera
 * Method:    nativeExitCamera
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeExitCamera(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
   try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
                          
     if (_pCameraInfo->hCam != 0)
     {
      int result = is_ExitCamera ((HIDS)_pCameraInfo->hCam);
      if (result == IS_SUCCESS)
      {
        _pCameraInfo->hCam = 0;
      } 
      return result;
     }
     else
     {
      return 0;
     }
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}


/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableHardwareTrigger
 * Method:    nativeEnableHardwareTrigger
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableHardwareTrigger(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    int result = 1;
    SENSORINFO sensorInfo; 
    int nMaxWidth = 0;
    int nMaxHeight = 0;   
 
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
 
    // Reset member variables before proceed
    _pCameraInfo->reset();
    
    // If we have an open camera, close it
    if (_pCameraInfo->hCam != 0)
    {
     is_ExitCamera (_pCameraInfo->hCam);
    }
         
    //_pCameraInfo->hCam = ((HIDS) (cameraDeviceId | IS_USE_DEVICE_ID)); // open with device ID
    _pCameraInfo->hCam = (HIDS) cameraDeviceId;
      
    result = is_InitCamera (&_pCameraInfo->hCam, NULL);     
    if (result == IS_SUCCESS)
    {
      result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );     
      if (result == IS_SUCCESS)
      {
        nMaxWidth  = sensorInfo.nMaxWidth;
        nMaxHeight = sensorInfo.nMaxHeight;
        
        // set the image size to capture
        result = is_SetImageSize(_pCameraInfo->hCam, nMaxWidth, nMaxHeight);                 
        if (result == IS_SUCCESS)
        {
          // Set color mode
          result = is_SetColorMode( _pCameraInfo->hCam, IS_CM_MONO8);          
          if (result == IS_SUCCESS)
          {
            // Set display mode            
            result = is_SetDisplayMode(_pCameraInfo->hCam, IS_SET_DM_DIRECT3D);
            if (result == IS_SUCCESS)
            {
               if (result == IS_SUCCESS)
               {
                  // Set Steal Format
                  INT nMode = IS_CM_MONO8;
                  result = is_DirectRenderer (_pCameraInfo->hCam, DR_SET_STEAL_FORMAT, (void*)&nMode, sizeof (nMode));
                  if (result == IS_SUCCESS)
                  {                  
                     // memory free?
                     if (_pCameraInfo->pcImageMemory != NULL)
                     {
                        is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId);
                     }
                  }                           
               }           
            }           
          }
        } 
      }
    } 
    return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   } 
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeAcquireOfflineImages
 * Method:    nativeAcquireOfflineImages
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeAcquireOfflineImages(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jstring fullPathFileName, jboolean forceTrigger)
{
   try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
   
     SENSORINFO sensorInfo; 
     int nMaxWidth = 0;
     int nMaxHeight = 0; 
     int result = 1;
     const char *nativeFullPathFileName = NULL;
     
     result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      
     if (result == IS_SUCCESS)
     {
       nMaxWidth  = sensorInfo.nMaxWidth;
       nMaxHeight = sensorInfo.nMaxHeight;
     }
     
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);    
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
     
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
     
     HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, "");
     HANDLE m_hEvent = NULL;
     
     if ( hEvent != NULL )
     {
      is_InitEvent(_pCameraInfo->hCam, m_hEvent, IS_SET_EVENT_FRAME);
      is_EnableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
      
      // allocate memory for steal
      result = is_AllocImageMem( _pCameraInfo->hCam, nMaxWidth, nMaxHeight, 8, &_pCameraInfo->pcImageMemory, &_pCameraInfo->lMemoryId);
                 
      // set memory active
      result = is_SetImageMem(_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId );
      
      // Set external trigger (Low to High)
      //result = is_SetExternalTrigger(_pCameraInfo->hCam, IS_SET_TRIGGER_LO_HI);
      //result = is_FreezeVideo(_pCameraInfo->hCam, IS_DONT_WAIT);
     
      // Added by Lee Herng 12 June 2013 - To force trigger even we do not receive it
      //if (forceTrigger)
      //{
      //  if (WaitForSingleObject(m_hEvent, 10000) != WAIT_OBJECT_0)
      //  {
         // No trigger has been received, so force image capture
       //  is_ForceTrigger(_pCameraInfo->hCam);
      //  }
      //}
      
       // Start stealing images
      //INT wait = IS_WAIT;
      //result = is_DirectRenderer (_pCameraInfo->hCam, DR_STEAL_NEXT_FRAME, (void*)&wait, sizeof (wait));        
      //if (result == IS_SUCCESS)
      //{      
      //   result = is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
        
      //   if (result == IS_SUCCESS)
      //  {
           result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);                        
      //   }
      //} 
       
      is_DisableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
      is_ExitEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME); 
     }
                                                
     	   
	   pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);     
 
	   return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeFreezeVideo
 * Method:    nativeFreezeVideo
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeFreezeVideo(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
   try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
   
     int result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);
     if (result == IS_SUCCESS)
     {
      result = is_FreezeVideo(_pCameraInfo->hCam, IS_WAIT);
     }     
     return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetNumberOfCameras
 * Method:    nativeGetNumberOfCameras
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetNumberOfCameras(JNIEnv *pEnv, jobject javaObject)
{
  try
  {
    INT nNumCam = 0;
    if( is_GetNumberOfCameras( &nNumCam ) == IS_SUCCESS)
    {
      return nNumCam; 
    }
    return nNumCam;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableAutoGain
 * Method:    nativeEnableAutoGain
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableAutoGain(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jboolean enable)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    double dEnable = 1;
    if (enable == false)
      dEnable = 0;
      
    int result = is_SetAutoParameter (_pCameraInfo->hCam, IS_SET_ENABLE_AUTO_GAIN, &dEnable, 0);    
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableAutoSensorGain
 * Method:    nativeEnableAutoSensorGain
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeEnableAutoSensorGain(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jboolean enable)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    double dEnable = 1;
    if (enable == false)
      dEnable = 0;
      
    int result = is_SetAutoParameter (_pCameraInfo->hCam, IS_SET_ENABLE_AUTO_SENSOR_GAIN, &dEnable, 0);    
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeIsGainBoostSupported()
 * Method:    nativeIsGainBoostSupported
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeIsGainBoostSupported(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
      
    int result = is_SetGainBoost (_pCameraInfo->hCam, IS_GET_SUPPORTED_GAINBOOST);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeSetGainBoostOn()
 * Method:    nativeSetGainBoostOn
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetGainBoostOn(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
      
    int result = is_SetGainBoost (_pCameraInfo->hCam, IS_SET_GAINBOOST_ON);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeSetGainBoostOff()
 * Method:    nativeSetGainBoostOff
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetGainBoostOff(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
      
    int result = is_SetGainBoost (_pCameraInfo->hCam, IS_SET_GAINBOOST_OFF);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetMasterGainFactor()
 * Method:    nativeGetMasterGainFactor
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetMasterGainFactor(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    int result = is_SetHardwareGain (_pCameraInfo->hCam, IS_GET_MASTER_GAIN, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER, IS_IGNORE_PARAMETER);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeSetMasterGainFactor()
 * Method:    nativeSetMasterGainFactor
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetMasterGainFactor(JNIEnv *pEnv, jobject javaObject, jint cameraDeviceId, jint gainFactor)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    int result = is_SetHardwareGain (_pCameraInfo->hCam, gainFactor, gainFactor, gainFactor, gainFactor);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}
  
/**
 * 
 * 
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetDebugMode(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jboolean status)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;

    _pCameraInfo->debugMode = true;
    
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }    
}   
 
 /**
 * @author Jack Hwee
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeAcquireObjectFringeImages(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint planeId, jstring fullPathFileName, jboolean forceTrigger)
{
  try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
   
     int result = 1; 
     SENSORINFO sensorInfo; 
     int nMaxWidth = 0;
     int nMaxHeight = 0; 
     const char *nativeFullPathFileName = NULL;
                        
      result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      
      if (result == IS_SUCCESS)
      {
        nMaxWidth  = sensorInfo.nMaxWidth;
        nMaxHeight = sensorInfo.nMaxHeight;
      }
      
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);    
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
  
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
    
     HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, "");
     HANDLE m_hEvent = NULL;
     
     if ( hEvent != NULL )
     {
       is_InitEvent(_pCameraInfo->hCam, m_hEvent, IS_SET_EVENT_FRAME);
       is_EnableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
    
       // allocate memory for steal
       result = is_AllocImageMem( _pCameraInfo->hCam, nMaxWidth, nMaxHeight, 8, &_pCameraInfo->pcImageMemory, &_pCameraInfo->lMemoryId);
            
      // set memory active
       result = is_SetImageMem(_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId );
      
       // Set external trigger (Low to High)
       result = is_SetExternalTrigger(_pCameraInfo->hCam, IS_SET_TRIGGER_LO_HI);
       result = is_FreezeVideo(_pCameraInfo->hCam, IS_DONT_WAIT);
     
       // Added by Lee Herng 12 June 2013 - To force trigger even we do not receive it
       if (forceTrigger)
       {
        if (WaitForSingleObject(m_hEvent, 10000) != WAIT_OBJECT_0)
         {
          // No trigger has been received, so force image capture
          is_ForceTrigger(_pCameraInfo->hCam);
         }
       }
       
       // Start stealing images
       INT wait = IS_WAIT;
       result = is_DirectRenderer (_pCameraInfo->hCam, DR_STEAL_NEXT_FRAME, (void*)&wait, sizeof (wait));        
       if (result == IS_SUCCESS)
       {
          if(_pCameraInfo->imageCount == 0) 
          {
           _pCameraInfo->pObjImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;    
            
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);       
          }  
          else if(_pCameraInfo->imageCount == 1) 
          {
           _pCameraInfo->pObjImg2 = (unsigned char*) _pCameraInfo->pcImageMemory;    
                 
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);	        
          }
          else if(_pCameraInfo->imageCount == 2) 
          {
           _pCameraInfo->pObjImg3 = (unsigned char*) _pCameraInfo->pcImageMemory; 
                          
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);	        
          }  
          else if(_pCameraInfo->imageCount == 3) 
          {
           _pCameraInfo->pObjImg4 = (unsigned char*) _pCameraInfo->pcImageMemory; 
                          
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }   
          else if(_pCameraInfo->imageCount == 4) 
          {
           _pCameraInfo->pRefImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;
                          
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pRefImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }       
          else if(_pCameraInfo->imageCount == 5) 
          {
           _pCameraInfo->pRefImg2 = (unsigned char*) _pCameraInfo->pcImageMemory; 
                           
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pRefImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }      
          else if(_pCameraInfo->imageCount == 6) 
          {
           _pCameraInfo->pRefImg3 = (unsigned char*) _pCameraInfo->pcImageMemory;       
                        
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pRefImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }       
          else if(_pCameraInfo->imageCount == 7) 
          {
           _pCameraInfo->pRefImg4 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pRefImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }
          else if(_pCameraInfo->imageCount == 8) 
          {
           _pCameraInfo->pObjPlusThreeImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjPlusThreeImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }
          else if(_pCameraInfo->imageCount == 9) 
          {
           _pCameraInfo->pObjPlusThreeImg2 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjPlusThreeImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }
          else if(_pCameraInfo->imageCount == 10) 
          {
           _pCameraInfo->pObjPlusThreeImg3 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjPlusThreeImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }
          else if(_pCameraInfo->imageCount == 11) 
          {
           _pCameraInfo->pObjPlusThreeImg4 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
           if(fullPathFileName != NULL)
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjPlusThreeImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }                                                                                                
        
          if (result == IS_SUCCESS)
          {
            result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);                        
          }
       } 
       
       is_DisableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
       is_ExitEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
  
     }

      result = 1;
       
      if(fullPathFileName != NULL)                                         	   
	     pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
  
      _pCameraInfo->imageCount++;
	   
	   return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/**
 * This is a new acquire calibration image function 
 * 
 * @author Cheah Lee Herng
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeAcquireCalibrationImages(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint planeId, jstring fullPathFileName, jboolean forceTrigger)
{
  try
   {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
   
     int result = 1; 
     SENSORINFO sensorInfo; 
     int nMaxWidth = 0;
     int nMaxHeight = 0; 
     const char *nativeFullPathFileName = NULL;
                        
      result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      
      if (result == IS_SUCCESS)
      {
        nMaxWidth  = sensorInfo.nMaxWidth;
        nMaxHeight = sensorInfo.nMaxHeight;
      }
      
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);    
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
  
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
    
     HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, "");
     HANDLE m_hEvent = NULL;
     
     if ( hEvent != NULL )
     {
       is_InitEvent(_pCameraInfo->hCam, m_hEvent, IS_SET_EVENT_FRAME);
       is_EnableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
    
       // allocate memory for steal
       result = is_AllocImageMem( _pCameraInfo->hCam, nMaxWidth, nMaxHeight, 8, &_pCameraInfo->pcImageMemory, &_pCameraInfo->lMemoryId);
            
      // set memory active
       result = is_SetImageMem(_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId );
      
       // Set external trigger (Low to High)
       result = is_SetExternalTrigger(_pCameraInfo->hCam, IS_SET_TRIGGER_LO_HI);
       result = is_FreezeVideo(_pCameraInfo->hCam, IS_DONT_WAIT);
     
       // Added by Lee Herng 12 June 2013 - To force trigger even we do not receive it
       if (forceTrigger)
       {
        if (WaitForSingleObject(m_hEvent, 10000) != WAIT_OBJECT_0)
         {
          // No trigger has been received, so force image capture
          is_ForceTrigger(_pCameraInfo->hCam);
         }
       }
       
       // Start stealing images
       INT wait = IS_WAIT;
       result = is_DirectRenderer (_pCameraInfo->hCam, DR_STEAL_NEXT_FRAME, (void*)&wait, sizeof (wait));        
       if (result == IS_SUCCESS)
       {
          // Depending on plane id, we will increment the image pointer accordingly.
          // Currently we only support Ref plane and +3 plane.
          if (planeId == 0) // Reference Plane
          {
            if(_pCameraInfo->imageCount == 0 || _pCameraInfo->imageCount == 8) 
            {
              _pCameraInfo->pNewRefImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;    
              if(fullPathFileName != NULL)
               is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 1 || _pCameraInfo->imageCount == 9) 
            {
              _pCameraInfo->pNewRefImg2 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 2 || _pCameraInfo->imageCount == 10) 
            {
              _pCameraInfo->pNewRefImg3 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 3 || _pCameraInfo->imageCount == 11) 
            {
              _pCameraInfo->pNewRefImg4 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 4 || _pCameraInfo->imageCount == 12) 
            {
              _pCameraInfo->pNewRefImg5 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg5, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 5 || _pCameraInfo->imageCount == 13) 
            {
              _pCameraInfo->pNewRefImg6 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg6, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 6 || _pCameraInfo->imageCount == 14) 
            {
              _pCameraInfo->pNewRefImg7 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg7, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 7 || _pCameraInfo->imageCount == 15) 
            {
              _pCameraInfo->pNewRefImg8 = (unsigned char*) _pCameraInfo->pcImageMemory; 
              if(fullPathFileName != NULL)
  	           is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewRefImg8, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
          }
          else if (planeId == 2) // +3 Plane
          {
            if(_pCameraInfo->imageCount == 0 || _pCameraInfo->imageCount == 8) 
            {
              _pCameraInfo->pNewObjImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 1 || _pCameraInfo->imageCount == 9) 
            {
              _pCameraInfo->pNewObjImg2 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 2 || _pCameraInfo->imageCount == 10) 
            {
              _pCameraInfo->pNewObjImg3 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 3 || _pCameraInfo->imageCount == 11) 
            {
              _pCameraInfo->pNewObjImg4 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 4 || _pCameraInfo->imageCount == 12) 
            {
              _pCameraInfo->pNewObjImg5 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg5, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 5 || _pCameraInfo->imageCount == 13) 
            {
              _pCameraInfo->pNewObjImg6 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg6, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 6 || _pCameraInfo->imageCount == 14) 
            {
              _pCameraInfo->pNewObjImg7 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg7, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
            else if(_pCameraInfo->imageCount == 7 || _pCameraInfo->imageCount == 15) 
            {
              _pCameraInfo->pNewObjImg8 = (unsigned char*) _pCameraInfo->pcImageMemory;     
              if(fullPathFileName != NULL)
	             is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg8, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);
            }
          }                                           
        
          if (result == IS_SUCCESS)
          {
            result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);                        
          }
       } 
       
       is_DisableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
       is_ExitEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
     }
       
     if(fullPathFileName != NULL)                                         	   
	     pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
  
     // Increament image counter
     _pCameraInfo->imageCount++;
     
     // Finally set the result to 1 to indicate operation succeed
     result = 1;
     
	   return result;
   }
   catch (AssertException& aex)
   {
     raiseJavaAssertWithMessage(aex.getMessage());
     return 0;
   }
   catch (...)
   {
     raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
     return 0;
   }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeLoadCalibrationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath); 
    
    return isSuccess;    
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeLoadNewCalibrationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    // Make sure New PSP Algorithm is initialized
    if (!_pCameraInfo->newPspAlgorithm->isInitialized())
    {
      _pCameraInfo->newPspAlgorithm->Initialize(752, 480, _pCameraInfo->numberOfProjector);
    }
  
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    isSuccess = _pCameraInfo->newPspAlgorithm->LoadCalibrationAndReferenceData(nativeFullPath);
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath); 
    
    return isSuccess;    
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetInspectionROI(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint roiCount, jintArray centerX, jintArray centerY, jintArray width, jintArray height)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    boolean isSuccess = false;
    jboolean isCopy;
    
    // Get pointer to int array
    jint *centerXArray = pEnv->GetIntArrayElements(centerX, &isCopy);
    jint *centerYArray = pEnv->GetIntArrayElements(centerY, &isCopy);
    jint *widthArray = pEnv->GetIntArrayElements(width, &isCopy);
    jint *heightArray = pEnv->GetIntArrayElements(height, &isCopy);
    
    // Get the size of each input array
    jsize centerXArrayLength = pEnv->GetArrayLength(centerX);
    jsize centerYArrayLength = pEnv->GetArrayLength(centerY);
    jsize widthArrayLength = pEnv->GetArrayLength(width);
    jsize heightArrayLength = pEnv->GetArrayLength(height);
    
    // Populate CenterX array
    for(unsigned int i=0; i < centerXArrayLength; ++i)
        _pCameraInfo->centerX[i] = centerXArray[i];
        
    // Populate CenterY array
    for(unsigned int i=0; i < centerYArrayLength; ++i)
        _pCameraInfo->centerY[i] = centerYArray[i];
        
    // Populate Width array
    for(unsigned int i=0; i < widthArrayLength; ++i)
        _pCameraInfo->width[i] = widthArray[i];
        
    // Populate Height array
    for(unsigned int i=0; i < heightArrayLength; ++i)
        _pCameraInfo->height[i] = heightArray[i];
    
    _pCameraInfo->roiCount = (unsigned int) roiCount;
                                                             
    // Release pointer
    pEnv->ReleaseIntArrayElements(centerX, centerXArray, 0);
    pEnv->ReleaseIntArrayElements(centerY, centerYArray, 0);
    pEnv->ReleaseIntArrayElements(width, widthArray, 0);
    pEnv->ReleaseIntArrayElements(height, heightArray, 0);
    
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetNewInspectionROI(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jint roiCount, jintArray centerX, jintArray centerY, jintArray width, jintArray height)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
  
    jboolean isCopy;
    
    // Get pointer to int array
    jint *centerXArray = pEnv->GetIntArrayElements(centerX, &isCopy);
    jint *centerYArray = pEnv->GetIntArrayElements(centerY, &isCopy);
    jint *widthArray = pEnv->GetIntArrayElements(width, &isCopy);
    jint *heightArray = pEnv->GetIntArrayElements(height, &isCopy);
    
    // Get the size of each input array
    jsize centerXArrayLength = pEnv->GetArrayLength(centerX);
    jsize centerYArrayLength = pEnv->GetArrayLength(centerY);
    jsize widthArrayLength = pEnv->GetArrayLength(width);
    jsize heightArrayLength = pEnv->GetArrayLength(height);
    
    // Populate CenterX array
    for(unsigned int i=0; i < centerXArrayLength; ++i)
        _pCameraInfo->centerX[i] = centerXArray[i];
        
    // Populate CenterY array
    for(unsigned int i=0; i < centerYArrayLength; ++i)
        _pCameraInfo->centerY[i] = centerYArray[i];
        
    // Populate Width array
    for(unsigned int i=0; i < widthArrayLength; ++i)
        _pCameraInfo->width[i] = widthArray[i];
        
    // Populate Height array
    for(unsigned int i=0; i < heightArrayLength; ++i)
        _pCameraInfo->height[i] = heightArray[i];
    
    _pCameraInfo->roiCount = (unsigned int) roiCount;
  
    // Setup ROI vector
    _pCameraInfo->inspectionRoiVector.clear();
    for(int i=0; i<roiCount; ++i)
    {
      Roi3DStruct inspectionRoi = {};
      inspectionRoi.x  = _pCameraInfo->centerX[i];
      inspectionRoi.y  = _pCameraInfo->centerY[i];
      inspectionRoi.dX = _pCameraInfo->width[i];
      inspectionRoi.dY = _pCameraInfo->height[i];
      
      int* pHeightMap = new int[inspectionRoi.dX * inspectionRoi.dY];
      int* pQualityMap = new int[inspectionRoi.dX * inspectionRoi.dY];
  
      inspectionRoi.pHeightMap = pHeightMap;
      inspectionRoi.pQualityMap = pQualityMap;
      
      // TODO: We may need new PSP 3D Algo to compensate based on camera magnification
      // For now, we just have to use back the original coordinate X and Y
      _pCameraInfo->pNewCoordinateXInPixel[i] = inspectionRoi.x;
      _pCameraInfo->pNewCoordinateYInPixel[i] = inspectionRoi.y;
      
      _pCameraInfo->inspectionRoiVector.push_back(inspectionRoi);
    }
                                                             
    // Release pointer
    pEnv->ReleaseIntArrayElements(centerX, centerXArray, 0);
    pEnv->ReleaseIntArrayElements(centerY, centerYArray, 0);
    pEnv->ReleaseIntArrayElements(width, widthArray, 0);
    pEnv->ReleaseIntArrayElements(height, heightArray, 0);
    
    return true;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGenerateHeightMap(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jfloat adjustment, jstring fullPathFileName, jint minusPlusSet, jboolean forceTrigger)
{
  try
  {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
  
     int result = 1; 
     SENSORINFO sensorInfo; 
     int nMaxWidth = 0;
     int nMaxHeight = 0;  
     const char *nativeFullPathFileName = NULL; 
     boolean isSuccess = false;
     jfloatArray resultArray;
                         
     result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      
     if (result == IS_SUCCESS)
     {
       nMaxWidth  = sensorInfo.nMaxWidth;
       nMaxHeight = sensorInfo.nMaxHeight;
     }
     
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);    
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
  
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
    
     HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, "");
     HANDLE m_hEvent = NULL;
     
     if ( hEvent != NULL )
     {
      is_InitEvent(_pCameraInfo->hCam, m_hEvent, IS_SET_EVENT_FRAME);
      is_EnableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
      
       // allocate memory for steal
       result = is_AllocImageMem( _pCameraInfo->hCam, nMaxWidth, nMaxHeight, 8, &_pCameraInfo->pcImageMemory, &_pCameraInfo->lMemoryId);
                 
      // set memory active
       result = is_SetImageMem(_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId );
  
       // Set external trigger (Low to High)
       result = is_SetExternalTrigger(_pCameraInfo->hCam, IS_SET_TRIGGER_LO_HI);
       result = is_FreezeVideo(_pCameraInfo->hCam, IS_DONT_WAIT);
     
       // Added by Lee Herng 12 June 2013 - To force trigger even we do not receive it
       if (forceTrigger)
       {
        if (WaitForSingleObject(m_hEvent, 10000) != WAIT_OBJECT_0)
         {
           // No trigger has been received, so force image capture
           is_ForceTrigger(_pCameraInfo->hCam);
         }
       }
   
       // Start stealing images
       INT wait = IS_WAIT;
       result = is_DirectRenderer (_pCameraInfo->hCam, DR_STEAL_NEXT_FRAME, (void*)&wait, sizeof (wait));        
       if (result == IS_SUCCESS)
       {
          if(_pCameraInfo->imageCount == 0) 
          {
           _pCameraInfo->pObjImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;
            
           if(fullPathFileName != NULL) 
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);      
          }  
          else if(_pCameraInfo->imageCount == 1) 
          {
           _pCameraInfo->pObjImg2 = (unsigned char*) _pCameraInfo->pcImageMemory;
                         
           if(fullPathFileName != NULL) 
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }
          else if(_pCameraInfo->imageCount == 2) 
          {
           _pCameraInfo->pObjImg3 = (unsigned char*) _pCameraInfo->pcImageMemory;
                             
           if(fullPathFileName != NULL) 
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);	        
          }  
          else if(_pCameraInfo->imageCount == 3) 
          {
           _pCameraInfo->pObjImg4 = (unsigned char*) _pCameraInfo->pcImageMemory;
                                 
           if(fullPathFileName != NULL) 
	          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pObjImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
          }                                                                                            
        
          if (result == IS_SUCCESS)
          {
            result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);                        
          }
       }       
       is_DisableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
       is_ExitEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
     }
    
    if (_pCameraInfo->imageCount == 3)
    {
      if (isSuccess == false)
      {
        resultArray = pEnv->NewFloatArray(0);
        return resultArray;
      }
      else
      {
        // Free Image Memory 
        is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 2);
        is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 3);
        is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 4);
        is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 5);
       
        is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pObjImg1, 2);
        is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pObjImg2, 3);
        is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pObjImg3, 4);
        is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pObjImg4, 5);
      }
    }
         
    if(fullPathFileName != NULL) 
       pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
       
    _pCameraInfo->imageCount++;
    
    if(_pCameraInfo->pHeightMap != NULL && isSuccess)
    {        
     jfloat temp[256] = {0.0};
     int heightMapSize = _pCameraInfo->roiCount;
     
     // Initialize int Height Map array
     resultArray = pEnv->NewFloatArray(heightMapSize);
    
     for(unsigned int i=0; i < heightMapSize; ++i) 
        temp[i] = _pCameraInfo->pHeightMap[i];
     
     // move from the temp structure to the java structure
     pEnv->SetFloatArrayRegion(resultArray, 0, heightMapSize, temp);
     return resultArray;  
    }
    else
    {    
     resultArray = pEnv->NewFloatArray(0);
     return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Jack Hwee
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGenerateNewHeightMap(JNIEnv* pEnv, 
																		jclass object, 
																		jint cameraDeviceId, 
																		jstring fullPathFileName, 
																		jint inspectionQualityThreshold, 
																		jint refLowerPhaseLimit, 
																		jboolean forceTrigger,
																		jboolean returnActualHeightMapValue)
{
  try
  {
     setDeviceIdToInstanceMap(cameraDeviceId);
     _pCameraInfo = getCameraInfo(cameraDeviceId);
  
     int result = 1; 
     SENSORINFO sensorInfo; 
     int nMaxWidth = 0;
     int nMaxHeight = 0;  
     const char *nativeFullPathFileName = NULL; 
     boolean isSuccess = false;
     jfloatArray resultArray;
                         
     result = is_GetSensorInfo( _pCameraInfo->hCam, &sensorInfo );
      
     if (result == IS_SUCCESS)
     {
       nMaxWidth  = sensorInfo.nMaxWidth;
       nMaxHeight = sensorInfo.nMaxHeight;
     }
     
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_UPDOWN, 1, 0);    
     is_SetRopEffect (_pCameraInfo->hCam, IS_SET_ROP_MIRROR_LEFTRIGHT, 1, 0);
  
     if (fullPathFileName != NULL)
      nativeFullPathFileName = pEnv->GetStringUTFChars(fullPathFileName, 0);
    
     HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, "");
     HANDLE m_hEvent = NULL;
     
     if ( hEvent != NULL )
     {
      is_InitEvent(_pCameraInfo->hCam, m_hEvent, IS_SET_EVENT_FRAME);
      is_EnableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
      
       // allocate memory for steal
       result = is_AllocImageMem( _pCameraInfo->hCam, nMaxWidth, nMaxHeight, 8, &_pCameraInfo->pcImageMemory, &_pCameraInfo->lMemoryId);
                 
      // set memory active
       result = is_SetImageMem(_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, _pCameraInfo->lMemoryId );
  
       // Set external trigger (Low to High)
       result = is_SetExternalTrigger(_pCameraInfo->hCam, IS_SET_TRIGGER_LO_HI);
       result = is_FreezeVideo(_pCameraInfo->hCam, IS_DONT_WAIT);
     
       // Added by Lee Herng 12 June 2013 - To force trigger even we do not receive it
       if (forceTrigger)
       {
        if (WaitForSingleObject(m_hEvent, 10000) != WAIT_OBJECT_0)
         {
           // No trigger has been received, so force image capture
           is_ForceTrigger(_pCameraInfo->hCam);
         }
       }
   
      // Start stealing images
      INT wait = IS_WAIT;
      result = is_DirectRenderer (_pCameraInfo->hCam, DR_STEAL_NEXT_FRAME, (void*)&wait, sizeof (wait));        
      if (result == IS_SUCCESS)
      {
        if(_pCameraInfo->imageCount == 0) 
        {
         _pCameraInfo->pNewObjImg1 = (unsigned char*) _pCameraInfo->pcImageMemory;
          
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg1, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);      
        }  
        else if(_pCameraInfo->imageCount == 1) 
        {
         _pCameraInfo->pNewObjImg2 = (unsigned char*) _pCameraInfo->pcImageMemory;
                       
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg2, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }
        else if(_pCameraInfo->imageCount == 2) 
        {
         _pCameraInfo->pNewObjImg3 = (unsigned char*) _pCameraInfo->pcImageMemory;
                           
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg3, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);	        
        }  
        else if(_pCameraInfo->imageCount == 3) 
        {
         _pCameraInfo->pNewObjImg4 = (unsigned char*) _pCameraInfo->pcImageMemory;
                               
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg4, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }
        else if(_pCameraInfo->imageCount == 4) 
        {
         _pCameraInfo->pNewObjImg5 = (unsigned char*) _pCameraInfo->pcImageMemory;
                               
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg5, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }
        else if(_pCameraInfo->imageCount == 5) 
        {
         _pCameraInfo->pNewObjImg6 = (unsigned char*) _pCameraInfo->pcImageMemory;
                               
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg6, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }
        else if(_pCameraInfo->imageCount == 6) 
        {
         _pCameraInfo->pNewObjImg7 = (unsigned char*) _pCameraInfo->pcImageMemory;
                               
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg7, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }
        else if(_pCameraInfo->imageCount == 7) 
        {
         _pCameraInfo->pNewObjImg8 = (unsigned char*) _pCameraInfo->pcImageMemory;
                               
         if(fullPathFileName != NULL) 
          is_SaveImageMemEx (_pCameraInfo->hCam, (IS_CHAR*) nativeFullPathFileName, (char*)_pCameraInfo->pNewObjImg8, _pCameraInfo->lMemoryId, IS_IMG_PNG, 0);        
        }                                                                                           
      
        if (result == IS_SUCCESS)
        {
          result = is_StopLiveVideo (_pCameraInfo->hCam, IS_WAIT);                        
        }
      }       
      is_DisableEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
      is_ExitEvent(_pCameraInfo->hCam, IS_SET_EVENT_FRAME);
    }
    
    if (_pCameraInfo->imageCount == 7)
    {
      // Setup image vector
      std::vector<FRINGE_BUFFER3D> objImageVector;
      objImageVector.clear();
      
      FRINGE_BUFFER3D bufferObjImage1 = {};
      FRINGE_BUFFER3D bufferObjImage2 = {};
      FRINGE_BUFFER3D bufferObjImage3 = {};
      FRINGE_BUFFER3D bufferObjImage4 = {};
      FRINGE_BUFFER3D bufferObjImage5 = {};
      FRINGE_BUFFER3D bufferObjImage6 = {};
      FRINGE_BUFFER3D bufferObjImage7 = {};
      FRINGE_BUFFER3D bufferObjImage8 = {};
      
      bufferObjImage1.bufferId = 0;
      bufferObjImage2.bufferId = 1;
      bufferObjImage3.bufferId = 2;
      bufferObjImage4.bufferId = 3;
      bufferObjImage5.bufferId = 4;
      bufferObjImage6.bufferId = 5;
      bufferObjImage7.bufferId = 6;
      bufferObjImage8.bufferId = 7;
  
      bufferObjImage1.pDst = _pCameraInfo->pNewObjImg1;
      bufferObjImage2.pDst = _pCameraInfo->pNewObjImg2;
      bufferObjImage3.pDst = _pCameraInfo->pNewObjImg3;
      bufferObjImage4.pDst = _pCameraInfo->pNewObjImg4;
      bufferObjImage5.pDst = _pCameraInfo->pNewObjImg5;
      bufferObjImage6.pDst = _pCameraInfo->pNewObjImg6;
      bufferObjImage7.pDst = _pCameraInfo->pNewObjImg7;
      bufferObjImage8.pDst = _pCameraInfo->pNewObjImg8;
      
      objImageVector.push_back(bufferObjImage1);
      objImageVector.push_back(bufferObjImage2);
      objImageVector.push_back(bufferObjImage3);
      objImageVector.push_back(bufferObjImage4);
      objImageVector.push_back(bufferObjImage5);
      objImageVector.push_back(bufferObjImage6);
      objImageVector.push_back(bufferObjImage7);
      objImageVector.push_back(bufferObjImage8);
      
      // Make sure New PSP Algorithm is initialized
      if (!_pCameraInfo->newPspAlgorithm->isInitialized())
      {
        _pCameraInfo->newPspAlgorithm->Initialize(752, 480, _pCameraInfo->numberOfProjector);
      }
      
      // Call New PSP Algorithm to perform actual height map calculation
      isSuccess = _pCameraInfo->newPspAlgorithm->CalculateHeightWithReferenceInputMultiFringe_MultiThread(objImageVector, 
                                                                                                          _pCameraInfo->inspectionRoiVector, 
                                                                                                          inspectionQualityThreshold, 
                                                                                                          refLowerPhaseLimit,
                                                                                                          "C:\\temp", // This is temporary folder
                                                                                                          false);
                                                                                                          
      // Free Image Memory 
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 2);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 3);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 4);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 5);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 6);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 7);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 8);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 9);
     
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg1, 2);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg2, 3);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg3, 4);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg4, 5);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg5, 6);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg6, 7);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg7, 8);
      is_FreeImageMem (_pCameraInfo->hCam, (char*)_pCameraInfo->pNewObjImg8, 9);
      
      if (isSuccess == false)
      {
        resultArray = pEnv->NewFloatArray(0);
        return resultArray;
      }
      else
      {
        // Variable returnActualHeightMapValue is to control whether we want to return 
		// average height map value or whole height map defined by ROI
        if (returnActualHeightMapValue)
		{
			// Get total ROI size
			int heightMapSize = _pCameraInfo->roiCount;
			
			// Since we are returning the whole actual height map value,
			// we always get the last ROI
			Roi3DStruct roi = _pCameraInfo->inspectionRoiVector.at(heightMapSize - 1);
			int* pHeightMap = roi.pHeightMap;
			int totalRoiSize = roi.dX * roi.dY;
			
			// Initialize int Height Map array
			resultArray = pEnv->NewFloatArray(totalRoiSize);
			
			// Initialize temp float array to maximum image size
			jfloat* temp;
			temp = new jfloat[_nMaxImageWidthPixel];
			
			for(unsigned int j=0; j < totalRoiSize; ++j)
			{
				float heightValueInMicron = pHeightMap[j];
				float heightValueInMilimeter = heightValueInMicron * 0.001f;
				temp[j] = heightValueInMilimeter;
			}
			
			// move from the temp structure to the java structure
			pEnv->SetFloatArrayRegion(resultArray, 0, totalRoiSize, temp);
			return resultArray;
		}
		else
		{
			char msg[1024];
			jfloat temp[256] = {0.0};
			int heightMapSize = _pCameraInfo->roiCount;
			   
			// Initialize int Height Map array
			resultArray = pEnv->NewFloatArray(heightMapSize);
			  
			// Debug data for troubleshooting purpose
			if (_pCameraInfo->debugMode)
			{
			  sprintf(msg, "Total Height Map size = %d", heightMapSize);
			  WriteLog(msg);
			}
		   
			for(unsigned int i=0; i < heightMapSize; ++i) 
			{
				Roi3DStruct roi = _pCameraInfo->inspectionRoiVector.at(i);
				int* pHeightMap = roi.pHeightMap;
				int totalRoiSize = roi.dX * roi.dY;
				float totalHeightValueInMilimeter = 0.0f;
				float avgHeightValue = 0.0f;
				  
				// Debug data for troubleshooting purpose
				if (_pCameraInfo->debugMode)
				{
				  sprintf(msg, "Total ROI size = %d", totalRoiSize);
				  WriteLog(msg);
				  sprintf(msg, "ROI X = %d", roi.x);
				  WriteLog(msg);
				  sprintf(msg, "ROI Y = %d", roi.y);
				  WriteLog(msg);
				  sprintf(msg, "ROI dX = %d", roi.dX);
				  WriteLog(msg);
				  sprintf(msg, "ROI dY = %d", roi.dY);
				  WriteLog(msg);
				}
				 
				for(unsigned int j=0; j < totalRoiSize; ++j)
				{
				  float heightValueInMicron = pHeightMap[j];
				  float heightValueInMilimeter = heightValueInMicron / 1000.0f;
				  totalHeightValueInMilimeter += heightValueInMilimeter;
				   
				  if (_pCameraInfo->debugMode)
				  {
					sprintf(msg, "ROI %d, j = %d, height value (um) = %f", i, j, heightValueInMicron);
					WriteLog(msg);
				  }
				}
				 
				if (totalRoiSize > 0)
				{
				  avgHeightValue = totalHeightValueInMilimeter / totalRoiSize;
				}
				else
				  avgHeightValue = 0.0f;
				  
				// Debug data for troubleshooting purpose
				if (_pCameraInfo->debugMode)
				{
				  sprintf(msg, "totalHeightValue (mm) = %f", totalHeightValueInMilimeter);
				  WriteLog(msg);  
				  sprintf(msg, "avgHeightValue (mm) = %f", avgHeightValue);
				  WriteLog(msg);
				}
				 
				// Finally assign the computed avg value into temporary array 
				temp[i] = avgHeightValue;
			}
		   
			// move from the temp structure to the java structure
			pEnv->SetFloatArrayRegion(resultArray, 0, heightMapSize, temp);
			return resultArray;
		}
      }
    }
    
    if(fullPathFileName != NULL) 
      pEnv->ReleaseStringUTFChars(fullPathFileName, nativeFullPathFileName);   
       
    _pCameraInfo->imageCount++;
    
    // By default, return empty float array
    resultArray = pEnv->NewFloatArray(0);
    return resultArray;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGenerateOfflineHeightMap(JNIEnv* pEnv, 
                                                                            jclass object, 
                                                                            jint cameraDeviceId,                                                          
                                                                            jfloat adjustment, 
                                                                            jint minusPlusSet,
                                                                            jintArray jImageDef,
                                                                            jint roiMinX, 
                                                                            jint roiMinY, 
                                                                            jint roiWidth, 
                                                                            jint roiHeight)
{
  try
  {  
    boolean isSuccess = false;
    jfloatArray heightMapArray;
  
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    // Reset member variables before proceed
    if (_pCameraInfo->imageCount > 3)
    {
      _pCameraInfo->reset();
    }
  
    JNIImageData *pImage = getImageDataFromJava(pEnv, jImageDef); 
    
    // the result array is the size of the image.
    jfloatArray resultArray = pEnv->NewFloatArray(roiWidth * roiHeight);
    Ipp32f *pResult = reinterpret_cast<Ipp32f*>(pEnv->GetPrimitiveArrayCritical(resultArray, 0));
    
    IppiSize resultSize = { roiWidth, roiHeight };
    
    IppStatus ippResult = ippiCopy_32f_C1R(pImage->addressAt(roiMinX, roiMinY), 
                                           pImage->stride,
                                           pResult, 
                                           roiWidth * sizeof(Ipp32f), 
                                           resultSize);
    
    // must 'release' the pResult pointer back Java for the results to be stored.
    pEnv->ReleasePrimitiveArrayCritical(resultArray, reinterpret_cast<void*>(pResult), 0);
    releaseImageDataFromJavaUnmodified(pEnv, jImageDef, pImage);
                                                
    sptAssertWithMessage(ippResult == 0, ippGetStatusString(ippResult));
    
    boolean isCopy = false;
    jfloat *imageArray = pEnv->GetFloatArrayElements(resultArray, &isCopy);
        
    if(_pCameraInfo->imageCount == 0) 
    {
      _pCameraInfo->pObjImg1 = (unsigned char*)imageArray; 
    }
    else if(_pCameraInfo->imageCount == 1) 
    {
      _pCameraInfo->pObjImg2 = (unsigned char*)imageArray;
    }
    else if(_pCameraInfo->imageCount == 2) 
    {
      _pCameraInfo->pObjImg3 = (unsigned char*)imageArray;
    }
    else if(_pCameraInfo->imageCount == 3) 
    {
      _pCameraInfo->pObjImg4 = (unsigned char*)imageArray;
    }
    
    if (_pCameraInfo->imageCount == 3)
    {                                                                  
      if (isSuccess == false)
      {
        // Release any left over memory
        pEnv->ReleaseFloatArrayElements(resultArray, imageArray, 0);
                 
        heightMapArray = pEnv->NewFloatArray(0);
        return heightMapArray;
      }
    }
    
    _pCameraInfo->imageCount++;

    // Release any left over memory
    pEnv->ReleaseFloatArrayElements(resultArray, imageArray, 0);
    
    if(_pCameraInfo->pHeightMap != NULL && isSuccess)
    {    
     jfloat temp[256] = {0.0};
     int heightMapSize = _pCameraInfo->roiCount;
     
     // Initialize int Height Map array
     heightMapArray = pEnv->NewFloatArray(heightMapSize);
    
     for(unsigned int i=0; i < heightMapSize; ++i) 
        temp[i] = _pCameraInfo->pHeightMap[i];
     
     // move from the temp structure to the java structure
     pEnv->SetFloatArrayRegion(heightMapArray, 0, heightMapSize, temp);
     return heightMapArray;  
    }
    else
    {
     heightMapArray = pEnv->NewFloatArray(0);
     return heightMapArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetNewCoordinateXInPixel(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    jfloatArray resultArray;
    
    if(_pCameraInfo->pNewCoordinateXInPixel != NULL)
    {
      jfloat temp[256] = {0.0};
      int newCoordinateXSize = _pCameraInfo->roiCount;
     
      // Initialize int new Coordinate X array
      resultArray = pEnv->NewFloatArray(newCoordinateXSize);
    
      for(unsigned int i=0; i < newCoordinateXSize; ++i)
        temp[i] = _pCameraInfo->pNewCoordinateXInPixel[i];
     
      // move from the temp structure to the java structure
      pEnv->SetFloatArrayRegion(resultArray, 0, newCoordinateXSize, temp);
      return resultArray; 
    }
    else
    {
      resultArray = pEnv->NewFloatArray(0);
      return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jfloatArray JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetNewCoordinateYInPixel(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    jfloatArray resultArray;
    
    if(_pCameraInfo->pNewCoordinateYInPixel != NULL)
    {
      jfloat temp[256] = {0.0};
      int newCoordinateYSize = _pCameraInfo->roiCount;
     
      // Initialize int new Coordinate Y array
      resultArray = pEnv->NewFloatArray(newCoordinateYSize);
    
      for(unsigned int i=0; i < newCoordinateYSize; ++i)
        temp[i] = _pCameraInfo->pNewCoordinateYInPixel[i];
     
      // move from the temp structure to the java structure
      pEnv->SetFloatArrayRegion(resultArray, 0, newCoordinateYSize, temp);
      return resultArray; 
    }
    else
    {
      resultArray = pEnv->NewFloatArray(0);
      return resultArray;
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return pEnv->NewFloatArray(0);
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return pEnv->NewFloatArray(0);
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativePerformCalibration(JNIEnv* pEnv, jclass object, jint cameraDeviceId, 
                                                                      jdouble referencePlaneKnownHeight, 
                                                                      jdouble objectPlanePlusThreeKnownHeight, 
                                                                      jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
           
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
 
    //isSuccess = _pCameraInfo->pspAlgorithm->performCalibration(knownHeight, nativeFullPath);
    
    // memory free?
    if (_pCameraInfo->pcImageMemory != NULL)
    {
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 2);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 3);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 4);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 5);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 6);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 7);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 8);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 9);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 10);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 11);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 12);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 13);
    }    
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath);     
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Cheah Lee Herng
 */ 
JNIEXPORT jboolean JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativePerformNewCalibration(JNIEnv* pEnv, jclass object, jint cameraDeviceId, 
                                                                         jdouble calibrationHeight, 
                                                                         jstring fullPath)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
           
    // Make sure New PSP Algorithm is initialized
    if (!_pCameraInfo->newPspAlgorithm->isInitialized())
    {
      _pCameraInfo->newPspAlgorithm->Initialize(752, 480, _pCameraInfo->numberOfProjector);
    }  
           
    boolean isSuccess = false;
    const char *nativeFullPath = pEnv->GetStringUTFChars(fullPath, 0);
    
    // Setup vector list
    std::vector<unsigned char*> calImageVector;
    calImageVector.clear();
    calImageVector.push_back(_pCameraInfo->pNewObjImg1);
    calImageVector.push_back(_pCameraInfo->pNewObjImg2);
    calImageVector.push_back(_pCameraInfo->pNewObjImg3);
    calImageVector.push_back(_pCameraInfo->pNewObjImg4);
    calImageVector.push_back(_pCameraInfo->pNewObjImg5);
    calImageVector.push_back(_pCameraInfo->pNewObjImg6);
    calImageVector.push_back(_pCameraInfo->pNewObjImg7);
    calImageVector.push_back(_pCameraInfo->pNewObjImg8);
    
    std::vector<unsigned char*> refImageVector;
    refImageVector.clear();
    refImageVector.push_back(_pCameraInfo->pNewRefImg1);
    refImageVector.push_back(_pCameraInfo->pNewRefImg2);
    refImageVector.push_back(_pCameraInfo->pNewRefImg3);
    refImageVector.push_back(_pCameraInfo->pNewRefImg4);
    refImageVector.push_back(_pCameraInfo->pNewRefImg5);
    refImageVector.push_back(_pCameraInfo->pNewRefImg6);
    refImageVector.push_back(_pCameraInfo->pNewRefImg7);
    refImageVector.push_back(_pCameraInfo->pNewRefImg8);
 
    isSuccess = _pCameraInfo->newPspAlgorithm->oneStepCalibrateThruImageMultiFringe_12X(calibrationHeight, 
                                                                                        calImageVector, 
                                                                                        refImageVector, 
                                                                                        nativeFullPath);
    
    // memory free?
    if (_pCameraInfo->pcImageMemory != NULL)
    {
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 2);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 3);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 4);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 5);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 6);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 7);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 8);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 9);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 10);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 11);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 12);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 13);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 14);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 15);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 16);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 17);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 18);
      is_FreeImageMem (_pCameraInfo->hCam, _pCameraInfo->pcImageMemory, 19);
    }
    
    pEnv->ReleaseStringUTFChars(fullPath, nativeFullPath);     
    return isSuccess;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return false;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return false;
  }
}

/**
 * @author Jack Hwee
 */  
JNIEXPORT void JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeInitializePspAlgorithm(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
   setDeviceIdToInstanceMap(cameraDeviceId);
   _pCameraInfo = getCameraInfo(cameraDeviceId);
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFirmwareVersion
 * Method:    nativeGetFirmwareVersion
 * Signature: (I)V
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFirmwareVersion(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    CAMINFO* pInfo = new CAMINFO();
    jstring firmwareVersion;
   
    int result = is_GetCameraInfo (_pCameraInfo->hCam, pInfo);
    if (result == IS_SUCCESS)
    {
      firmwareVersion = pEnv->NewStringUTF (pInfo->Version);
    }
    return firmwareVersion;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetSerialNumber
 * Method:    nativeGetSerialNumber
 * Signature: (I)V
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetSerialNumber(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    CAMINFO* pInfo = new CAMINFO();
    jstring serialNumber;
   
    int result = is_GetCameraInfo (_pCameraInfo->hCam, pInfo);
    if (result == IS_SUCCESS)
    {
      serialNumber = pEnv->NewStringUTF (pInfo->SerNo);
    }
    return serialNumber;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetManufacturer
 * Method:    nativeGetManufacturer
 * Signature: (I)V
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetManufacturer(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    CAMINFO* pInfo = new CAMINFO();
    jstring manufacturer;
   
    int result = is_GetCameraInfo (_pCameraInfo->hCam, pInfo);
    if (result == IS_SUCCESS)
    {
      manufacturer = pEnv->NewStringUTF (pInfo->ID);
    }
    return manufacturer;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFinalQualityCheckDate
 * Method:    nativeGetFinalQualityCheckDate
 * Signature: (I)V
 */
JNIEXPORT jstring JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFinalQualityCheckDate(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    CAMINFO* pInfo = new CAMINFO();
    jstring finalQualityCheckDate;
   
    int result = is_GetCameraInfo (_pCameraInfo->hCam, pInfo);
    if (result == IS_SUCCESS)
    {
      finalQualityCheckDate = pEnv->NewStringUTF (pInfo->Date);
    }
    return finalQualityCheckDate;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFramesPerSecond
 * Method:    nativeGetFramesPerSecond
 * Signature: (I)V
 */
JNIEXPORT jdouble JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeGetFramesPerSecond(JNIEnv* pEnv, jclass object, jint cameraDeviceId)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
   
    double* dblFps = new double();
    double framePerSecond = 0.0;
   
    int result = is_GetFramesPerSecond  (_pCameraInfo->hCam, dblFps);
    if (result == IS_SUCCESS)
    {
      framePerSecond = (*dblFps);
    }
    return framePerSecond;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0.0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0.0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeSetFramesPerSecond
 * Method:    nativeSetFramesPerSecond
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetFramesPerSecond(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jdouble framesPerSecond)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    double* dblFps = new double();
   
    int result = is_SetFrameRate  (_pCameraInfo->hCam, framesPerSecond, dblFps);
    return result;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_uEyeOpticalCamera_nativeSetMagnificationData
 * Method:    nativeSetMagnificationData
 * Signature: (I)V
 */
JNIEXPORT jint JNICALL 
Java_com_axi_v810_hardware_uEyeOpticalCamera_nativeSetMagnificationData(JNIEnv* pEnv, jclass object, jint cameraDeviceId, jdoubleArray magnificationData)
{
  try
  {
    setDeviceIdToInstanceMap(cameraDeviceId);
    _pCameraInfo = getCameraInfo(cameraDeviceId);
    
    boolean isSuccess = false;
    jboolean isCopy;
    
    // Get pointer to double array
    jdouble *magnificationDataArray = pEnv->GetDoubleArrayElements(magnificationData, &isCopy);
    
    // Get the size of each input array
    jsize magnificationDataArrayLength = pEnv->GetArrayLength(magnificationData);
    
    // Populate magnification data array
    for(unsigned int i=0; i < magnificationDataArrayLength; ++i)
        _pCameraInfo->magnificationData[i] = magnificationDataArray[i];    
    
    // Release pointer
    pEnv->ReleaseDoubleArrayElements(magnificationData, magnificationDataArray, 0);
    
    if (isSuccess)
      return 0;
    else
      return -1;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}