package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 * @author George Booth
 */
public class HTubeXraySourceStatusEnum extends com.axi.util.Enum
{
  private static Map<String, HTubeXraySourceStatusEnum> _statusStringToXraySourceStatusEnumMap = new HashMap<String, HTubeXraySourceStatusEnum>();
  private String _name;

  private static int _index = 0;
  public static final HTubeXraySourceStatusEnum WARMUP_YET = new HTubeXraySourceStatusEnum(_index++, "STS 0");
  public static final HTubeXraySourceStatusEnum WARMUP = new HTubeXraySourceStatusEnum(_index++, "STS 1");
  public static final HTubeXraySourceStatusEnum STANDBY = new HTubeXraySourceStatusEnum(_index++, "STS 2");
  public static final HTubeXraySourceStatusEnum XON = new HTubeXraySourceStatusEnum(_index++, "STS 3");
  public static final HTubeXraySourceStatusEnum OVER = new HTubeXraySourceStatusEnum(_index++, "STS 4");
  public static final HTubeXraySourceStatusEnum NOT_READY = new HTubeXraySourceStatusEnum(_index++, "STS 5");
  public static final HTubeXraySourceStatusEnum SELF_TEST = new HTubeXraySourceStatusEnum(_index++, "STS 6");
  public static final HTubeXraySourceStatusEnum SELF_TEST_NOT_RUN = new HTubeXraySourceStatusEnum(_index++, "ZTE 0");
  public static final HTubeXraySourceStatusEnum SELF_TEST_DONE = new HTubeXraySourceStatusEnum(_index++, "ZTE 1");
  public static final HTubeXraySourceStatusEnum SELF_TEST_FAILED = new HTubeXraySourceStatusEnum(_index++, "ZTB 0");
  public static final HTubeXraySourceStatusEnum SELF_TEST_PASSED = new HTubeXraySourceStatusEnum(_index++, "ZTB 1");
  public static final HTubeXraySourceStatusEnum SELF_TEST_RUNNING = new HTubeXraySourceStatusEnum(_index++, "ZTB 2");

  /**
   * @author George A. David
   * @author George Booth
   */
  private HTubeXraySourceStatusEnum(int id, String statusString)
  {
    super(id);
    Object previous = _statusStringToXraySourceStatusEnumMap.put(statusString.toUpperCase(), this);
    Assert.expect(previous == null);
    _name = statusString;
  }

  /**
   * @author George A. David
   * @author George Booth
   */
  public static HTubeXraySourceStatusEnum getEnum(String statusString)
  {
    HTubeXraySourceStatusEnum enumeration = _statusStringToXraySourceStatusEnumMap.get(statusString.toUpperCase());

    Assert.expect(enumeration != null);
    return enumeration;
  }

  /**
   * @author George Booth
   */
  public String getName()
  {
    return _name;
  }
}
