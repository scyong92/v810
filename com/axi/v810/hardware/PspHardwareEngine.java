package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * This is PSP hardware class that handles all the communication to PSP-related
 * hardware. It handles different version of PSP hardware.
 * 
 * All hardware-related tasks should be called from this class.
 * 
 * @author Cheah Lee Herng
 */
public abstract class PspHardwareEngine implements HardwareStartupShutdownInt
{
  private static Map<PspModuleVersionEnum, PspHardwareEngine> _versionToInstanceMap;
  
  protected static Config _config;
  protected LinkedList<HardwareStartupShutdownInt> _allHardwareList;
  
  protected static PspModuleVersionEnum _versionEnum;
  protected static PspSettingEnum _settingEnum;

  /**
    * @author Cheah Lee Herng
    */
  static
  {
    _versionToInstanceMap = new HashMap<PspModuleVersionEnum, PspHardwareEngine>();
    _config = Config.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static PspHardwareEngine getInstance()
  {    
    PspHardwareEngine instance = null;
    
    _versionEnum = getVersionEnum();
    _settingEnum = getSettingEnum();
    
    if (_versionToInstanceMap.containsKey(_versionEnum))
    {
      instance = _versionToInstanceMap.get(_versionEnum);
    }
    else
    {
      if (_versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      {
        instance = new PspHardwareModuleVersion1();
      }
      else if (_versionEnum.equals(PspModuleVersionEnum.VERSION_2))
      {
        instance = new PspHardwareModuleVersion2();
      }        
      else
        Assert.expect(false);
    }

    Assert.expect(instance != null);
    _versionToInstanceMap.put(_versionEnum, instance);

    return instance;
  }
  
  /**
   * Default Constructor
   * 
   * @author Cheah Lee Herng
   */
  protected PspHardwareEngine()
  {
    _allHardwareList = new LinkedList<HardwareStartupShutdownInt>();
    
    buildHardwareList();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void startup() throws XrayTesterException 
  {
    for(HardwareStartupShutdownInt hardware : _allHardwareList)
    {
      hardware.startup();
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void shutdown() throws XrayTesterException 
  {
    for(HardwareStartupShutdownInt hardware : _allHardwareList)
    {
      hardware.shutdown();
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isStartupRequired() throws XrayTesterException 
  {
    boolean startupRequired = false;

    for (HardwareStartupShutdownInt hardware : _allHardwareList)
    {
      if (isSimulationModeOn() == false)
      {
        if (hardware.isStartupRequired())
        {
          startupRequired = true;
          break;
        }
      }
    }
    return startupRequired;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public static PspModuleVersionEnum getVersionEnum()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_MODULE_VERSION);
    return PspModuleVersionEnum.getEnum(version);
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return the current PspSettingEnum defined in hardware.config.
   */
  public static PspSettingEnum getSettingEnum()
  {
    int setting = _config.getIntValue(HardwareConfigEnum.PSP_SETTING);
    return PspSettingEnum.getEnum(setting);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public OpticalCameraIdEnum getOpticalCameraIdEnum(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    
    OpticalCameraIdEnum cameraId = null;
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
    else
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_2;
    return cameraId;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void loadCalibrationData(PspModuleEnum moduleEnum, String calibrateDataPath) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(calibrateDataPath != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));    
      abstractCamera.loadCalibrationData(calibrateDataPath);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<HardwareStartupShutdownInt> getHardwareList()
  {
    List<HardwareStartupShutdownInt> hardwareList = new ArrayList<HardwareStartupShutdownInt>();
    hardwareList.addAll(_allHardwareList);
    return hardwareList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCameraMagnificationData(PspModuleEnum moduleEnum, double[] magnificationData) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(magnificationData != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setMagnificationData(magnificationData);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCameraInspectionRoi(PspModuleEnum moduleEnum,
                                            int roiCount, 
                                            int[] roiCenterX, 
                                            int[] roiCenterY, 
                                            int[] roiWidth, 
                                            int[] roiHeight) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(roiCenterX != null);
    Assert.expect(roiCenterY != null);
    Assert.expect(roiWidth != null);
    Assert.expect(roiHeight != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setInspectionROI(roiCount, roiCenterX, roiCenterY, roiWidth, roiHeight);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableOpticalCameraAutoGain(PspModuleEnum moduleEnum, boolean enable) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.enableAutoGain(enable);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableOpticalCameraAutoSensorGain(PspModuleEnum moduleEnum, boolean enable) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.enableAutoSensorGain(enable);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableOpticalCameraGainBoost(PspModuleEnum moduleEnum, boolean enable) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.enableGainBoost(enable);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setOpticalCameraFrameRatePerSecond(PspModuleEnum moduleEnum, double frameRatePerSecond) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setFramesPerSecond(frameRatePerSecond);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setOpticalCameraMasterGainFactor(PspModuleEnum moduleEnum, int masterGainFactor) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setMasterGainFactor(masterGainFactor);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setOpticalCameraCropImageOffset(PspModuleEnum moduleEnum, 
                                              int frontModuleCropImageOffsetXInPixel, 
                                              int rearModuleCropImageOffsetXInPixel) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setCropImageOffset(frontModuleCropImageOffsetXInPixel, rearModuleCropImageOffsetXInPixel);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setOpticalCameraDebugMode(PspModuleEnum moduleEnum, boolean enableDebug) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.setDebugMode(enableDebug);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void acquireCalibrationImage(PspModuleEnum moduleEnum, OpticalCalibrationPlaneEnum planeEnum, String imageFullPath, boolean forceTrigger) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(planeEnum != null);
    Assert.expect(imageFullPath != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.acquireObjectFringeImages(imageFullPath, planeEnum.getId(), forceTrigger);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void acquireHeightMapImage(PspModuleEnum moduleEnum, String imageFullPath, OpticalInspectionData opticalInspectionData) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalInspectionData != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.generateHeightMap(opticalInspectionData, imageFullPath, opticalInspectionData.getReturnActualHeightMap());
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isHeightMapAvailable(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    boolean isHeightMapAvailable = false;
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      isHeightMapAvailable = abstractCamera.isHeightMapValueAvailable();
    }
    return isHeightMapAvailable;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public float[] getNewCoordinateXInPixel(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      return abstractCamera.getNewCoordinateXInPixel();
    }
    return null;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public float[] getNewCoordinateYInPixel(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      return abstractCamera.getNewCoordinateYInPixel();
    }
    return null;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public float[] getHeightMapValue(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      return abstractCamera.getHeightMapValue();
    }
    return null;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void acquireSingleImage(PspModuleEnum moduleEnum, String imagePath, boolean forceTrigger) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(imagePath != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.acquireOfflineImages(imagePath, forceTrigger);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void generateOfflineHeightMap(PspModuleEnum moduleEnum,
                                       com.axi.util.image.Image image, 
                                       com.axi.util.image.RegionOfInterest regionOfInterest, 
                                       float floatAdjustment, 
                                       int minusPlusSet) throws XrayTesterException
  {
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.generateOfflineHeightMap(image,
                                              regionOfInterest,
                                              floatAdjustment, 
                                              minusPlusSet);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void performCalibration(PspModuleEnum moduleEnum, OpticalCalibrationData opticalCalibrationData) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalCalibrationData != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.performCalibration(opticalCalibrationData); 
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isSimulationModeOn()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isTriggerModeReady(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    boolean isTriggerModeReady = false;
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      isTriggerModeReady = abstractCamera.isTriggerModeReady();
    }
    return isTriggerModeReady;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void initializeOpticalCamera(PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    
    if (isSimulationModeOn() == false)
    {
      AbstractOpticalCamera abstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(moduleEnum));
      abstractCamera.initialize();
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getImageRoiWidth()
  {
    return AbstractOpticalCamera.getRoiWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getImageRoiHeight()
  {
    return AbstractOpticalCamera.getRoiHeight();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getImageWidth() 
  {
    return AbstractOpticalCamera.getImageWidth();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getImageHeight() 
  {
    return AbstractOpticalCamera.getImageHeight();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getMaxImageWidth()
  {
    return AbstractOpticalCamera.getMaxImageWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getMaxImageHeight()
  {
    return AbstractOpticalCamera.getMaxImageHeight();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getActualImageWidthInNanometer()
  {
    return AbstractOpticalCamera.getActualImageWidthInNanometer();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getActualImageHeightInNanometer()
  {
    return AbstractOpticalCamera.getActualImageHeightInNanometer();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovActualImageWidthInNanometer()
  {
    return AbstractOpticalCamera.getLargerFovActualImageWidthInNanometer();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovActualImageHeightInNanometer()
  {
    return AbstractOpticalCamera.getLargerFovActualImageHeightInNanometer();
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return true if OpticalCamera is using Larger FOV setting.
   */
  public boolean isLargerFov()
  {
    AbstractOpticalCamera frontAbstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(PspModuleEnum.FRONT));
    AbstractOpticalCamera rearAbstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(PspModuleEnum.REAR));
    if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH))
    {
      return (frontAbstractCamera.isLargerFov() && rearAbstractCamera.isLargerFov());
    }
    else if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
    {
      return frontAbstractCamera.isLargerFov();
    }
    else //if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
    {
      return rearAbstractCamera.isLargerFov();
    }
  }

  /**
   * @author Ying-Huan.Chu
   * @param useLargerFov pass in true to set OpticalCamera to use Larger Fov, pass in false to set OpticalCamera to use Small Fov.
   * @throws com.axi.v810.util.XrayTesterException
   */
  public void setUseLargerFov(boolean useLargerFov) throws XrayTesterException
  {
    AbstractOpticalCamera frontAbstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(PspModuleEnum.FRONT));
    AbstractOpticalCamera rearAbstractCamera = AbstractOpticalCamera.getInstance(getOpticalCameraIdEnum(PspModuleEnum.REAR));
    if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH))
    {
      frontAbstractCamera.setUseLargerFov(useLargerFov);
      rearAbstractCamera.setUseLargerFov(useLargerFov);
    }
    else if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
    {
      frontAbstractCamera.setUseLargerFov(useLargerFov);
    }
    else //if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
    {
      rearAbstractCamera.setUseLargerFov(useLargerFov);
    }
  }
  
  public abstract void turnOnLED(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void turnOnAllLED() throws XrayTesterException;
  public abstract void turnOffLED(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void turnOffAllLED() throws XrayTesterException;
  public abstract void displayWhiteLight(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void displayFringePattern1(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void displayFringePattern2(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void displayFringePattern3(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void displayFringePattern4(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void sendTriggerSequence(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftNameEnum) throws XrayTesterException;
  public abstract void abort() throws XrayTesterException;
  public abstract void enableOpticalCameraHardwareTrigger(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract void adjustCurrent(PspModuleEnum moduleEnum, int current) throws XrayTesterException;

  protected abstract void buildHardwareList();
}
