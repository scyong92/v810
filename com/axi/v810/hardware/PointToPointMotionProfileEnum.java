package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class PointToPointMotionProfileEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<Integer, PointToPointMotionProfileEnum> _idToEnumMap;

  /**
   * @author Greg Esparza
   */
  static
  {
    _idToEnumMap = new HashMap<Integer, PointToPointMotionProfileEnum>();
  }

  public static final PointToPointMotionProfileEnum  PROFILE0 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE1 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE2 = new PointToPointMotionProfileEnum(++_index);
  public static final PointToPointMotionProfileEnum  PROFILE3 = new PointToPointMotionProfileEnum(++_index);


  /**
   * @author Greg Esparza
   */
  private PointToPointMotionProfileEnum(int id)
  {
    super(id);

    _idToEnumMap.put(new Integer(id), this);
  }

  /**
   * @author Greg Esparza
   */
  public static int getNumberOfMotionProfiles()
  {
    return _index + 1;
  }

  /**
   * @author Greg Esparza
   */
  public static PointToPointMotionProfileEnum getEnum(int id)
  {
    PointToPointMotionProfileEnum motionProfile = _idToEnumMap.get(new Integer(id));
    Assert.expect(motionProfile != null);

    return motionProfile;
  }
}
