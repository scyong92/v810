package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class Test_ImageReconstructionProcessorManager extends UnitTest
{
  /**
   * @author Rex Shang
   */
  private Test_ImageReconstructionProcessorManager()
  {
    try
    {
      Config.getInstance().loadIfNecessary();
      Config.checkIs64BitsIRP();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public static void main(String args[])
  {
    Test_ImageReconstructionProcessorManager myTest = new Test_ImageReconstructionProcessorManager();
    if (UnitTest.unitTesting())
      UnitTest.execute(myTest);
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    int number = ImageReconstructionProcessorManager.getNumberOfImageReconstructionProcessors();
    
    // XCR-3604 Unit Test Phase 2
    int expectedNumber = 4;
    if (Config.is64bitIrp())
      expectedNumber = 1;
    
    Expect.expect(number==expectedNumber);
  }
  
}
