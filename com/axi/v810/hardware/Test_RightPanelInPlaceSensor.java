package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Unit test for right panel in place sensor
 * @author Rex Shang
 */
class Test_RightPanelInPlaceSensor extends UnitTest
{
  private RightPanelInPlaceSensor _rightPanelInPlaceSensor = null;

  private Test_RightPanelInPlaceSensor()
  {
    _rightPanelInPlaceSensor = RightPanelInPlaceSensor.getInstance();
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // retract sensor and verify
      _rightPanelInPlaceSensor.retract();
      Expect.expect(_rightPanelInPlaceSensor.isExtended() == false);

      // extend sensor and verify
      _rightPanelInPlaceSensor.extend();
      Expect.expect(_rightPanelInPlaceSensor.isExtended() == true);

      // Verify that panel is in place
      //_rightPanelInPlaceSensor.isPanelInPlace()
    }
    catch (XrayTesterException he)
    {
      he.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    Test_RightPanelInPlaceSensor test_RightPanelInPlaceSensor = new Test_RightPanelInPlaceSensor();
    UnitTest.execute(test_RightPanelInPlaceSensor);
  }

}
