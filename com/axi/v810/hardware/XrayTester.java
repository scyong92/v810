package com.axi.v810.hardware;

import com.axi.guiUtil.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class models the entire XRay Inspection system.  This class is
 * responsible for  starting up and shutting down the system.
 *
 * @author Bill Darbie
 * @author Rex Shang
 */
public class XrayTester extends HardwareObject implements HardwareStartupShutdownInt
{
  private static XrayTester _instance;
  private static final int _STANDARD_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(17000);
  private static final int _THROUGHPUT_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(16400);
  private static final int _XXL_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS = 772160000; // 30.4 inch
  private static final int _S2EX_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(16400);  //Ngie Xing
  private static final int _STEP_SIZE_DITHER_AMPLITUDE_IN_NANOMETERS = (int) MathUtil.convertMilsToNanoMeters(53);
  private static final int _SYSTEM_FIDUCIAL_COUPON_WIDTH_IN_NANOMETERS = 1280000;
  private static final int _SYSTEM_FIDUCIAL_COUPON_HEIGHT_IN_NANOMETERS = 2544000;
  private static final int _MAX_OPTICAL_IMAGEABLE_AREA_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(6300);
  
  //Auto Calculation of min and max slice height
  private static final int _MIN_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(-500);
  private static final int _MAX_SLICE_HEIGHT_LEGACY_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(208);
  private static final int _TYPICAL_DIFF_BETWEEN_MAX_THICKNESS_AND_MAX_SLICE_HEIGHT = MathUtil.convertMilsToNanoMetersInteger(208-125);
  // for XXL only
  private static final int _TYPICAL_DIFF_BETWEEN_MAX_THICKNESS_AND_MIN_SLICE_HEIGHT_FOR_XXL = MathUtil.convertMilsToNanoMetersInteger(550-500);
  // for All system 
  private static final int _MAX_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(400);

  private RemoteMotionServerExecution _remoteMotionServerExecution;

  //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on"
  private static final int _SYSTEM_PANEL_THICKNESS_OFFSET_IN_NANOMETERS = 254000;

  private PanelPositioner _panelPositioner;
  private DigitalIo _digitalIo;
  private PanelHandler _panelHandler;
  private XrayCameraArray _xrayCameraArray;
  private Interlock _interlock;
  private AbstractXraySource _xraySource;
  private CameraTrigger _cameraTrigger;
  private ImageReconstructionProcessorManager _imageReconstructionHardware;
  private NetworkSwitch _networkSwitch;
  private PspHardwareEngine _pspHardwareEngine;

  private List<HardwareStartupShutdownInt> _allHardwareList;
  private List<HardwareStartupShutdownInt> _motionRelatedHardwareList;

  private HardwareTaskEngine _hardwareTaskEngine;
  private HardwareObservable _hardwareObservable;

  private static boolean _ALL_HARDWARE = false;
  private static boolean _MOTION_HARDWARE_ONLY = true;
  private boolean _isPreviousStartupWithResetSucceeded = true;
  private boolean _enablePsp = getConfig().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

  private static boolean _isXXLBased = false;
  private static boolean _isS2EXEnabled = false;
  private static boolean _isThroughputEnabled = false;
  private static boolean _isOfflineProgramming = false;
  private static boolean _isVariableMagEnabled = false;
   

  private static MagnificationEnum _magnification = null; // = MagnificationEnum.NOMINAL;
  /**
   * @author Bill Darbie
   */
  public static synchronized XrayTester getInstance()
  {
    if (_instance == null)
      _instance = new XrayTester();

    return _instance;
  }

  /**
   * @author Bill Darbie
   * @author Rex Shang
   */
  private XrayTester()
  {
    _remoteMotionServerExecution = RemoteMotionServerExecution.getInstance();
    _panelPositioner = PanelPositioner.getInstance();
    _digitalIo = DigitalIo.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _xrayCameraArray = XrayCameraArray.getInstance();
    _interlock = Interlock.getInstance();
    _xraySource = AbstractXraySource.getInstance();
    _cameraTrigger = CameraTrigger.getInstance();
    _imageReconstructionHardware = ImageReconstructionProcessorManager.getInstance();
    _networkSwitch = NetworkSwitch.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _pspHardwareEngine = PspHardwareEngine.getInstance();

    setupAlldHardwareList();
    setupMotionRelatedHardwareList();
  }

  /**
   * @author Rex shang
   */
  private void setupAlldHardwareList()
  {
    _allHardwareList = new ArrayList<HardwareStartupShutdownInt>();
    _allHardwareList.add(_remoteMotionServerExecution);
    _allHardwareList.add(_digitalIo);
    _allHardwareList.add(_interlock);
    _allHardwareList.add(_panelPositioner);
    _allHardwareList.add(_panelHandler);
    _allHardwareList.add(_xrayCameraArray);
    
    if (_enablePsp)
    {
      _allHardwareList.addAll(_pspHardwareEngine.getHardwareList());
    }
    
    _allHardwareList.add(_xraySource);
    _allHardwareList.add(_cameraTrigger);
    _allHardwareList.add(_imageReconstructionHardware);
    _allHardwareList.add(_networkSwitch);
  }

  /**
   * @author Rex shang
   */
  private void setupMotionRelatedHardwareList()
  {
    _motionRelatedHardwareList = new ArrayList<HardwareStartupShutdownInt>();
    _motionRelatedHardwareList.add(_remoteMotionServerExecution);
    _motionRelatedHardwareList.add(_digitalIo);
    _motionRelatedHardwareList.add(_interlock);
    _motionRelatedHardwareList.add(_panelPositioner);
    _motionRelatedHardwareList.add(_panelHandler);
  }

  /**
   * Call abort for all HardwareObject.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    if (isHardwareAvailable() == false)
      return;

    super.abort();

    _panelHandler.abort();
    _panelPositioner.abort();
    _xraySource.abort();
  }
  
  /**
   * @author Kok Chun, Tan
   * XCR-2994 - clear abort flag
   */
  public void clearAbortFlag()
  {
    for (HardwareObject hardware : getHardwareObjects())
    {
      hardware.clearAbort();
    }
  }

  /**
   * @author Rex Shang
   */
  public void setSimulationMode() throws DatastoreException
  {
    List<HardwareObject> hardwareObjects = getHardwareObjects();
    for (HardwareObject hardwareObject : hardwareObjects)
    {
      try
      {
        if (hardwareObject instanceof XrayTester == false)
          hardwareObject.setSimulationMode();
      }
      catch (XrayTesterException xte)
      {
        // @todo do something with this
        Assert.logException(xte);
      }
    }

    getConfig().setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, new Boolean(true));
  }

  /**
   * @author Rex Shang
   */
  public void clearSimulationMode() throws DatastoreException
  {
    List<HardwareObject> hardwareObjects = getHardwareObjects();
    for(HardwareObject hardwareObject : hardwareObjects)
    {
      try
      {
        if (hardwareObject instanceof XrayTester == false)
          hardwareObject.clearSimulationMode();
      }
      catch(XrayTesterException xte)
      {
        // @todo do something with this
        Assert.logException(xte);
      }
    }

    getConfig().setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, new Boolean(false));
  }

  /**
   * @author Rex Shang
   */
  public boolean isSimulationModeOn()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
  }
  
  public static void setIsOfflineProgramming(boolean value)
  {
    _isOfflineProgramming = value;
  }
  
  public static boolean isOfflineProgramming()
  {
    return _isOfflineProgramming;
  }
  //XXL Based Anthony Jan 2013
    /**
   * @author Anthony Fong
   */
  public static void setXXLBased(boolean value)
  {
    _isXXLBased = value;
  }

  /**
   * @author Anthony Fong
   * @edit Cheah Lee Herng XCR-3822 System crash when switch to xray safety test tab
   */
  public static boolean isXXLBased()
  {
    return _isXXLBased;
  }
  
  

  /**
   * @author Anthony Fong S2EX
   */
  public static void setS2EXEnabled(boolean value)
  {
    _isS2EXEnabled = value;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param value 
   */
  public static void setThroughPutEnabled(boolean value)
  {
    _isThroughputEnabled = value;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static void setIsVariableMagEnabled(boolean value)
  {
    _isVariableMagEnabled = value;
  }
    
  public static boolean isThroughputEnabled()
  {
    return _isThroughputEnabled;
  }
  /**
   * @author Anthony Fong S2EX
   * @edit Cheah Lee Herng XCR-3822 System crash when switch to xray safety test tab
   */
  public static boolean isS2EXEnabled()
  {
    return _isS2EXEnabled;
  }
  
  /**
   * This is the method we use to start the Xray Test System.
   * In this method, we employeed parallel tasks to do time consuming things
   * together to speed things up.  But the underlying dependencies has to be
   * identified.
   * Digital Io and the motion subsystems must be started in the following order
   * because of the inherent dependency.
   * 1. DigitalIO
   * 2. InterLocks
   * 3. PanelHandler
   * 4. PanelPositioner
   * And since InterLocks has to be started before XraySource, the parallel task
   * is set up to run XraySource in parallel with PanelHandler and PanelPositioner.
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    clearAbort();
    if (isHardwareAvailable() == false)
      return;

    _hardwareObservable.stateChangedBegin(this, XrayTesterEventEnum.STARTUP);
    _hardwareObservable.setEnabled(false);

    try
    {
      if (XrayTester.getInstance().isStartupRequired())
        startupSequence(_ALL_HARDWARE);
      else
        if (_panelHandler.isKeepOuterBarrierOpenForLoadingPanel() == false)
          _panelHandler.closeAllOuterBarriersInParallel();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayTesterEventEnum.STARTUP);
  }

  /**
   * Special start up sequence for just starting up the motion related hardware.
   * This is especially useful when non-motion related hardware can NOT startup,
   * but we still need to load/unload a panel.
   * @author Rex Shang
   */
  public void startupMotionRelatedHardware() throws XrayTesterException
  {
    clearAbort();
    if (isHardwareAvailable() == false)
      return;

    _hardwareObservable.stateChangedBegin(this, XrayTesterEventEnum.STARTUP_MOTION_RELATED_HARDWARE);
    _hardwareObservable.setEnabled(false);

    try
    {
      if(isStartupRequiredForMotionRelatedHardware())
        startupSequence(_MOTION_HARDWARE_ONLY);
      else
        _panelHandler.closeAllOuterBarriersInParallel();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayTesterEventEnum.STARTUP_MOTION_RELATED_HARDWARE);
  }

  /**
   * Shutdownm the motion related hardware in the reverse order of startup.
   * @author Erica Wheatcroft
   * @author Rex Shang
   */
  public void shutdownMotionRelatedHardware() throws XrayTesterException
  {
    clearAbort();
    if(isHardwareAvailable() == false)
      return;

    _panelPositioner.shutdown();
    _panelHandler.shutdown();

    _interlock.shutdown();
    _digitalIo.shutdown();
  }

  /**
   * The Digital IO normal startup will cause the output bit state to reset
   * to 0 which in turn, closes outer barriers without checking if there is
   * anything in the door way.  The follwing block of code inside the if()
   * statement will perform special startup without the reset, check the
   * door ways, then does the startup again with the necessary reset.
   * @author Rex Shang
   */
  private void startupDigitalIo() throws XrayTesterException
  {
    if (_isPreviousStartupWithResetSucceeded)
    {
      _isPreviousStartupWithResetSucceeded = false;

      boolean leftOuterBarrierBlocked = true;
      boolean rightOuterBarrierBlocked = true;

      try
      {
        // Save the current state of digital io.  This will be used later to
        // determine whether this is fresh startup or restart for DigitalIo.
        boolean isFreshStartup = _digitalIo.isStartupRequired();

        // The next two lines are not necessary when isFreshStartup is false.
        // However, considering digital io may get out of sync with motion
        // control, this is a good idea.
        if (isFreshStartup)
        {
          _digitalIo.setHardwareResetMode(false);
          _digitalIo.startup();
        }

        LeftPanelClearSensor leftPanelClearSensor = LeftPanelClearSensor.getInstance();
        RightPanelClearSensor rightPanelClearSensor = RightPanelClearSensor.getInstance();
        LeftOuterBarrier leftOuterBarrier = LeftOuterBarrier.getInstance();
        RightOuterBarrier rightOuterBarrier = RightOuterBarrier.getInstance();

        leftOuterBarrierBlocked = leftOuterBarrier.isInstalled() && leftOuterBarrier.isOpen() && leftPanelClearSensor.isBlocked();
        rightOuterBarrierBlocked = rightOuterBarrier.isInstalled() && rightOuterBarrier.isOpen() && rightPanelClearSensor.isBlocked();

        if(leftOuterBarrierBlocked == false && rightOuterBarrierBlocked == false)
          _panelHandler.closeAllOuterBarriersInParallel();
        
        // If current startup is fresh start up, we need to make sure we reset
        // the digital io at least once.
        if (isFreshStartup)
          _digitalIo.shutdown();
      }
      catch (XrayTesterException xte)
      {
        throw PanelHandlerException.getFailedToDetectPanelClearSensorStateDuringStartupException(xte);
      }

      if (leftOuterBarrierBlocked && rightOuterBarrierBlocked)
      {
        _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getBothPanelClearSensorsUnexpectedBlockedException());
      }
      else if (leftOuterBarrierBlocked)
      {
        _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getLeftPanelClearSensorUnexpectedBlockedException());
      }
      else if (rightOuterBarrierBlocked)
      {
        _hardwareTaskEngine.throwHardwareException(PanelHandlerException.getRightPanelClearSensorUnexpectedBlockedException());
      }
    }

    _digitalIo.setHardwareResetMode(true);
    _digitalIo.startup();
    _isPreviousStartupWithResetSucceeded = true;
  }

  /**
   * Used by startup() and startupMotionRelatedHardware() to either startup
   * the whole system or just do motion related hardware.
   * @param includeXray boolean ture for starting up motion only hardware
   * @author Rex Shang
   */
  private void startupSequence(boolean motionHardwareOnly) throws XrayTesterException
  {
    // clearAbort() is done at the method that calls this.
    boolean startupRequiredForMotionRelatedHardware = isStartupRequiredForMotionRelatedHardware();
    if (startupRequiredForMotionRelatedHardware)
    {
      if( _remoteMotionServerExecution.isStartupRequired() )
        _remoteMotionServerExecution.startup();

      if (isAborting())
        return;
      startupDigitalIo();
    }

    // Before we start the interlock, close the barriers that might cause problem.
    // Interesting enough, now as part of DigitalIo.startup(), the motion
    // controller might reset all output bits to 0 which will return all of the
    // harware to its default state.  All outter barrier will be shut while inner
    // barrier will be open.
    if (isAborting())
      return;
    _panelHandler.closeAllBarriersInParallel();

    if (startupRequiredForMotionRelatedHardware)
    {
      if (isAborting())
        return;
      _interlock.startup();
    }

    // Check to see if panel is loaded.  If not, we can start panel handler
    // in normal mode.  Otherwise, we have to recapture the panel,
    // unloaded it and then home the rail.
    if (_panelHandler.isPanelLoaded() == false)
    {
      if (isAborting())
        return;
      // Get the slow-pokes running on their own thread to get hardware started
      // up as quickly as possible.
      ExecuteParallelThreadTasks<Object> startupTasks = new ExecuteParallelThreadTasks<Object>();

      if (motionHardwareOnly == false)
        startupTasks.submitThreadTask(new XraySourceStartupThreadTask());
      if (startupRequiredForMotionRelatedHardware)
        startupTasks.submitThreadTask(new PanelPositionerAndPanelHandlerStartupThreadTask());

      try
      {
        startupTasks.waitForTasksToComplete();
      }
      catch (XrayTesterException xte)
      {
        _hardwareTaskEngine.throwHardwareException(xte);
      }
      catch (Exception e)
      {
        Assert.logException(e);
      }
    }
    else
    {
      // Since the panel is left in the system from previous run, we need to
      // unload the panel first.  And that will cause the
      if (startupRequiredForMotionRelatedHardware)
      {
        if (isAborting())
          return;
        _panelHandler.startupWithPanelInSystemStageOneOfTwo();
        if (isAborting())
          return;
        _panelPositioner.startup();
        if (isAborting())
          return;
        _panelHandler.startupWithPanelInSytemStageTwoOfTwo();
      }

      if (motionHardwareOnly == false)
      {
        if (isAborting())
          return;
        _xraySource.startup();
      }
    }
    
    _panelHandler.initializeSafetyChecking();

    if (motionHardwareOnly == false)
    {
      if (isAborting())
        return;
      // Now that PanelPositioner has started up, we can give the MechanicalConversions
      // its opportunity to initialize/startup.
      MechanicalConversions.startup();

      if (isAborting())
        return;
      _networkSwitch.startup();

      if (isAborting())
        return;
      // Tell the trigger board to startup.
      _cameraTrigger.startup();

      if (isAborting())
        return;
      // Estimated time 1-2 seconds.  Must be done after panelPositioner and
      // xraySource are done because that hardware is required to obtain the
      // calibrations for the cameras.
      //System.out.println("Trying to startup cameras");
      _xrayCameraArray.startup();

      // Added by Lee Herng 31 May 2011 - Used in PSP Suface Map
      if (_enablePsp)
      {
        _pspHardwareEngine.startup();
      }     

      if (isAborting())
        return;
      _imageReconstructionHardware.startup();
    }
  }

  /**
   * Shutdown is fast and not abort-able, no need to check abort().
   * @author Rex Shang
   */
  public void shutdown() throws XrayTesterException
  {
    clearAbort();
    if (isHardwareAvailable() == false)
      return;

    _hardwareObservable.stateChangedBegin(this, XrayTesterEventEnum.SHUTDOWN);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Always shutdown Xray first for safety.
      _xraySource.shutdown();

      // Do the rest in reverse order as in startup().
      _xrayCameraArray.shutdown();

      if (_enablePsp)
      {
        _pspHardwareEngine.shutdown();
      }
      
      _cameraTrigger.shutdown();

      _networkSwitch.shutdown();

      _panelPositioner.shutdown();
      _panelHandler.shutdown();

      _interlock.shutdown();
      _digitalIo.shutdown();
      _remoteMotionServerExecution.shutdown();
      _imageReconstructionHardware.shutdown();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, XrayTesterEventEnum.SHUTDOWN);
  }

  /**
   * @author Bill Darbie
   */
  public String getMachineDescription()
  {
    String machineDescription = " v810 System ";
    
    if(com.axi.v810.business.license.LicenseManager.isXXLEnabled())
    {
      machineDescription += getXXLDescription();      
    }
    else if(com.axi.v810.business.license.LicenseManager.isS2EXEnabled())
    {
      machineDescription += getS2EXDescription();
    }
    
    boolean enablePSP = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
    if(enablePSP && XrayActuator.isInstalled())
    {
      machineDescription = machineDescription + " (" + getPSPDescription() + "+" + getVMDescription() + ")";
    }
    else if(enablePSP)
    {
      machineDescription = machineDescription + " (" + getPSPDescription() + ")";
    }
    else if(XrayActuator.isInstalled())
    {
      machineDescription = machineDescription + " (" + getVMDescription() + ")";
    }
    
    machineDescription = machineDescription + " " + getSerialNumber();
    
    return machineDescription;
  }

  /**
   * @author George Booth
   */
  public String getWorkstationDescription() throws XrayTesterException
  {
    String workstationDescription = " v810 Test Development Workstation ";

    if (_isOfflineProgramming)
    {
      workstationDescription += getOLPDescription();
    }
    else if (com.axi.v810.business.license.LicenseManager.isXXLEnabled())
    {
      workstationDescription += getXXLDescription();
    }
    else if (com.axi.v810.business.license.LicenseManager.isS2EXEnabled())
    {
      workstationDescription += getS2EXDescription();
    }

    return workstationDescription;
  }
  
  /**
   * @author Wei Chin
   */
  public String getXXLDescription()
  {
    return "XXL";
  }

  /**
   * @author Ngie Xing
   */
  public String getS2EXDescription()
  {
    return "S2EX";
  }

  /**
   * @author Wei Chin
   */
  public String getVMDescription()
  {
    return "VM";
  }
  
  /**
   * @author Wei Chin
   */
  public String getPSPDescription()
  {
    return "PSP";
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   *
   * @author weng-jian.eoh
   */
  public String getOLPDescription() throws XrayTesterException
  {
    List<String> supportSystemTypes = null;
    
    supportSystemTypes = com.axi.v810.business.license.LicenseManager.getInstance().getSupportedSystemType();

    
    String systemtype = StringUtil.join(supportSystemTypes, ",");
    
    return " : " + systemtype;

  }

  /**
   * @author Bill Darbie
   */
  public String getSerialNumber()
  {
    String serialNumber = getConfig().getStringValue(HardwareConfigEnum.XRAYTESTER_SERIAL_NUMBER);
    return serialNumber;
  }

  /**
   * This method will return true if the system needs to be started, false otherwise
   * @author Erica Wheatcroft
   * @author Rex Shang
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    boolean startupRequired = false;

    for (HardwareStartupShutdownInt hardware : _allHardwareList)
    {
      if (hardware.isStartupRequired())
      {   
        startupRequired = true;
        break;
      }
    }
    return startupRequired;
  }

  /**
   * This call returns if all Adjustments have been run and pass.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public boolean isCalibrated()
  {
    return _hardwareTaskEngine.isCalibrated();
  }

  /**
   * This call returns true if all *automated* Confirmations have been run and
   * pass.
   *
   * @author Bill Darbie
   * @author Reid Hayhow
   */
  public boolean isConfirmed()
  {
    return _hardwareTaskEngine.isConfirmed();
  }

  /**
   * A method that unifies the calls to see if the system is in Adjustment and
   * has passed Confirmations.
   *
   * @author Reid Hayhow
   */
  public boolean isSystemCalibratedAndConfirmed()
  {
    return ( isCalibrated() && isConfirmed() );
  }

  /**
   * @return true if any of the motion related hardware needs to be started.
   * @author Rex Shang
   */
  public boolean isStartupRequiredForMotionRelatedHardware() throws XrayTesterException
  {
    boolean startupRequired = false;

    for (HardwareStartupShutdownInt hardware : _motionRelatedHardwareList)
    {
      if (hardware.isStartupRequired())
      {
        startupRequired = true;
        break;
      }
    }

    return startupRequired;
  }

  /**
   * WARNING - do not use this if you do not have to.  Only the low level (hardware and datastore)
   *           code should have to call this.  Business layer code should NOT have to know what
   *           system it is running on except in rare instances.  If in doubt, ask Bill.
   * @return true if the controller this software is running on is connected to a 5dx machine.  If
   * this method returns false then the code is running on a tdw (Test Development Workstation)
   * @author Bill Darbie
   */
  public static boolean isHardwareAvailable()
  {
    return getConfig().getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION);
  }

  /**
   * @author Cheah Lee Herng
   * @author George A. David
   */
  public static int getMaxImageableAreaLengthInNanoMeters()
  {
    Assert.expect(_STANDARD_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS >= 0);
    Assert.expect(_THROUGHPUT_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS >= 0);

    int maxImageableAreaLengthInNanoMeters = 0;

    SystemTypeEnum systemType = XrayTester.getSystemType();
    if (systemType.equals(SystemTypeEnum.STANDARD))
        maxImageableAreaLengthInNanoMeters = _STANDARD_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS;
    else if (systemType.equals(SystemTypeEnum.THROUGHPUT))
        maxImageableAreaLengthInNanoMeters = _THROUGHPUT_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS;
    else if (systemType.equals(SystemTypeEnum.S2EX))
        maxImageableAreaLengthInNanoMeters = _S2EX_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS;
    else if (systemType.equals(SystemTypeEnum.XXL))
        maxImageableAreaLengthInNanoMeters = _XXL_MAX_IMAGEABLE_AREA_LENGTH_IN_NANOMETERS;
    else
        Assert.expect(false, "unknown system type: " + systemType.getName());
    
    return maxImageableAreaLengthInNanoMeters;
  }
  
  /**
   * In optical system, we have the maximum imageable area which can be seen by
   * each optical camera. The value returned from this function is to make sure 
   * those regions not covered by first optical camera are covered by second 
   * optical camera.
   * 
   * @author Cheah Lee Herng
   */
  public static int getMaxOpticalImageableAreaWidthInNanoMeters()
  {
    Assert.expect(_MAX_OPTICAL_IMAGEABLE_AREA_WIDTH_IN_NANOMETERS >= 0);
    return _MAX_OPTICAL_IMAGEABLE_AREA_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaximumJointHeightInNanoMeters()
  {
    return getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_JOINT_HEIGHT_IN_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMaximumPanelUpWarpInNanoMeters()
  {
    return getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_PANEL_UP_WARP_IN_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMaximumPanelDownWarpInNanoMeters()
  {
    return getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_PANEL_DOWN_WARP_IN_NANOMETERS);
  }

  /**
   * This method returns the vertical distance from the camera array to the
   * xray source as measured from the center of the camera array (ie. at 90 degrees
   * from the plane of the camera array)
   *
   * @author Reid Hayhow
   */
  public static int getDistanceFromCameraArrayToXraySourceInNanometers()
  {
    return getConfig().getIntValue(SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_XRAY_SOURCE_IN_NANOMETERS);    
  }

  /**
   * This method returns the vertical distance from the camera array to the
   * 2nd xray source (which is in High Magnification) as measured from the center of the camera array (ie. at 90 degrees
   * from the plane of the camera array)
   *
   * @author Wei Chin
   */
  public static int getDistanceFromCameraArrayTo2ndXraySourceInNanometers()
  {
    return getConfig().getIntValue(SystemConfigEnum.DISTANCE_FROM_CAMERA_ARRAY_TO_2ND_XRAY_SOURCE_IN_NANOMETERS);
  }  
  
  /**
   * @author Roy Williams
   */
  public static int getMaxStepSizeDitherAmplitudeInNanometers()
  {
    return _STEP_SIZE_DITHER_AMPLITUDE_IN_NANOMETERS;
  }

  /**
   * @author Seng-Yew Lim
   * @author Chnee Khang Wah
   * For Thunderbolt project. Try to reduce scan passes according to panel thickness.
   * This method will return the slice height from nominal based on panel thickness instead of just max slice height from nominal.
   */
  public static double getMaxPanelSliceHeightFromNominalSliceInNanometers(double panelThickness, double boardZOffset)
  {
    boolean stepSizeOptimized = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_STEP_SIZE_OPTIMIZATION);
    
    // return the hard coded value for XXL 
    if(XrayTester.isXXLBased() || isS2EXEnabled())
    {
      double max = getMaximumSliceHeightFromNominalSliceInNanometers();
      
      if (boardZOffset>0)
        return Math.min(_MAX_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS, max+boardZOffset);
      else
        return max;
    }
    
    // For 160 maxthickness system, max slice height value:
    // (160*1.1) + (208-125*1.1) = 176 + 70.5 = 246.5mils = 6261100nm
    // 208mil and 125mil come for 125 maxthickness system.
    double maxSliceHeight = getMaximumSliceHeightFromNominalSliceInNanometers(); // got from software.config
    double maxPanelThickness = PanelHandler.getMaximumPanelThicknessInNanoMeters();// from PanelHandler class, got from hardware.config
    
    // New System preset the max thickness to 160mils (4064000nm), and max slice height to 246.5mils (6261100nm), 
    // we need to maintain max slice height for those recipes come from 125mils system
    if (panelThickness <= PanelHandler.getLegacyMaximumPanelThicknessInNanoMeters()) //125mil
    {
      maxSliceHeight = _MAX_SLICE_HEIGHT_LEGACY_IN_NANOMETERS;    //208mil
      maxPanelThickness = PanelHandler.getLegacyMaximumPanelThicknessInNanoMeters();
    }
    
    if (stepSizeOptimized==true && XrayTester.isXXLBased()==false && XrayTester.isS2EXEnabled()==false)
    {
      /* some history background
       * Current method:
       * maxSliceHeight = maxSliceHeight - (maxPanelThickness - panelThickness * 1.1);
       * 
       * Current problem for this method:
       * Thick board
       * maxSliceHeight = 208mil - (250mil - 215mil * 1.1);
       * maxSliceHeight = 5283200nm - (6350000nm - 5461000nm * 1.1);
       * maxSliceHeight = 5283200nm - (342900nm);
       * maxSliceHeight = 4940300nm;
       * maxSliceHeight = 194.5mil
       * 
       * Thin board
       * maxSliceHeight = 208mil - (250mil - 20mil * 1.1);
       * maxSliceHeight = 5283200nm - (6350000nm - 508000nm * 1.1);
       * maxSliceHeight = 5283200nm - (5791200nm);
       * maxSliceHeight = -508000nm;
       * maxSliceHeight = -20mil
       * 
       * We may finally getting negative maximum slice height if the maxboardthickness is too large and maximumsliceheight did not change accordingly.
       * In order to fix this, we may having the default maximumslice height changing together with maximum board thickness as well
       * defGap = 208mil - (125mil * 1.1) = 208mil - 137.5mil = 70.5mil = 1790700nm
       * maxSliceHeight = maxPanelThickness + defGap - (maxPanelThickness - panelThickness * 1.1);
       * maxSliceHeight = 1790700 + panelThickness*1.1;
       * value = Math.max(value, 508000*1.1+1790700); // never return negative value, minimum allowable value is 20mil of board thickness + 70.5mil of default gap.
       */
      
      double defGap = maxSliceHeight - maxPanelThickness*1.1;
      Assert.expect(defGap>0); // remember to change max slice height accordingly when increase max thickness
      
      maxSliceHeight = defGap + (panelThickness+boardZOffset)*1.1;
      // never return negative value, minimum allowable value is 20mil*1.1 of board thickness + 70.5mil of default gap.
      double minThickness = PanelHandler.getMinimumPanelThicknessInNanoMeters();
      maxSliceHeight = Math.max(maxSliceHeight, minThickness*1.1+defGap); 
      maxSliceHeight = Math.min(maxSliceHeight, _MAX_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS);
    }
    else
    {
      if(boardZOffset>0)
        maxSliceHeight = maxSliceHeight+boardZOffset;
      
      maxSliceHeight = Math.min(_MAX_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS, maxSliceHeight);
    }
    
    return maxSliceHeight;
  }

  /**
   * @author Roy Williams
   */
  public static double getMaximumSliceHeightFromNominalSliceInNanometers()
  {
    int max = getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS);
    return max;
  }

  /**
   * @author Reid Hayhow
   */
  public static double getMinimumSliceHeightFromNominalSliceInNanometers()
  {
    int min = getConfig().getIntValue(SoftwareConfigEnum.MINIMUM_SLICE_HEIGHT_FROM_NOMINAL_SLICE_IN_NANOMETERS);
    return min;
  }
  
  /**
   * @author Chnee Khang Wah, 2014-04-05, Recalculate minimum slice height when zOffset < 0 
   */
  public static double getMinimumSliceHeightFromNominalSliceInNanometers(int zOffset)
  {
    if(zOffset>=0 || isXXLBased() || isS2EXEnabled())
      return  getMinimumSliceHeightFromNominalSliceInNanometers();
    
    double minSliceHeight = getMinimumSliceHeightFromNominalSliceInNanometers() + zOffset;
    //                      (-170mils)                                          + (- z offset value)
    
    if(minSliceHeight < _MIN_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS)
      System.out.println("Min Slice Height exceed limit (Min allowable slice height:"+_MIN_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS+", Current Minimum Slice Height:"+getMinimumSliceHeightFromNominalSliceInNanometers()+", Board Z Offset:"+zOffset);
    
    minSliceHeight = Math.max(minSliceHeight, _MIN_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS); // make sure the min slice height is not less than -500mils 

    return minSliceHeight;
  }
  
  /**
   * @author Chnee Khang Wah, 2014-06-11 
   */
  public static double getMinimumAllowableSliceHeightFromNominalSliceInNanometers()
  {
    return _MIN_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS;
  }
  
  /**
   * @author Chnee Khang Wah, 2014-06-11 
   */
  public static double getMaximumAllowableSliceHeightFromNominalSliceInNanometers()
  {
    return _MAX_ALLOWABLE_SLICE_HEIGHT_IN_NANOMETERS;
  }
  
  /**
   * @author Chnee Khang Wah, 2014-06-26
   */
  public static double getTypicalDiffBetweenMaxThicknessAndMaxSliceHeight()
  {
    return _TYPICAL_DIFF_BETWEEN_MAX_THICKNESS_AND_MAX_SLICE_HEIGHT;
  }
  
  /**
   * @author Chnee Khang Wah, 2015-02-06
   */
  public static double getTypicalDiffBetweenMaxThicknessAndMinSliceHeightForXXL()
  {
    return _TYPICAL_DIFF_BETWEEN_MAX_THICKNESS_AND_MIN_SLICE_HEIGHT_FOR_XXL;
  }

  /**
   * @author Roy Williams
   */
  public static int getSystemFiducialWidthInNanometers()
  {
    return _SYSTEM_FIDUCIAL_COUPON_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author Roy Williams
   */
  public static int getSystemFiducialHeightInNanometers()
  {
    return _SYSTEM_FIDUCIAL_COUPON_HEIGHT_IN_NANOMETERS;
  }

  //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on"
  public static int getSystemPanelThicknessOffset()
  {
      return _SYSTEM_PANEL_THICKNESS_OFFSET_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   * @author Ngie Xing
   * Will need to add S2EX XXL in the future
   */
  public static SystemTypeEnum getSystemType()
  {
    if (_isOfflineProgramming)
    {
      if (_isXXLBased)
      {
        return SystemTypeEnum.XXL;
      }
      else if (_isS2EXEnabled)
      {
        return SystemTypeEnum.S2EX; 
      }
      else if (_isThroughputEnabled)
      {
        return SystemTypeEnum.THROUGHPUT;
      }
    }
    else
    {
      if(_isXXLBased)
        return SystemTypeEnum.XXL;
      else if(_isS2EXEnabled)
        return SystemTypeEnum.S2EX;
      else
        return SystemTypeEnum.THROUGHPUT;   
    }
    
    //never reach here
    return null;
//    
//    String name = getConfig().getStringValue(HardwareConfigEnum.SYSTEM_TYPE);
////    String name = getConfig().getStringValue(HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE);
//
//    return SystemTypeEnum.getSystemType(name);
  }

  /**
   * XCR-3513, Only low magnification alignment is running although VM is enable
   * @author Yong Sheng Chuan
   */
  public static boolean isVariableMagEnabled()
  {
    if (_isVariableMagEnabled)
    {
      if(_isS2EXEnabled)
        return XrayCPMotorActuator.isInstalled();
      else
        return XrayCylinderActuator.isInstalled(); 
    }
    else
      return false;
  }
  
  /**
   * @author George A. David
   */
  public static void changeSystemTypeIfNecessary(SystemTypeEnum systemType) throws DatastoreException
  {
    Assert.expect(systemType != null);
    // sanity check, we cannot change the system type of an actual system
//    Assert.expect(XrayTester.isHardwareAvailable() == false);

    if(systemType.equals(getSystemType()))
      return; // nothing to do here


    Config config = getConfig();
    config.setValue(HardwareConfigEnum.SYSTEM_TYPE, systemType.getName());
//    config.setValue(HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE, systemType.getName());

    config.forceLoad();

    // sanity check
    Assert.expect(systemType.equals(getSystemType()));
  }
  
  /**
   * only can call from IAE!!
   * @author Wei Chin
   */
  public static void changeMagnification(MagnificationEnum magnification)
  {
    Assert.expect(magnification != null);
    _magnification = magnification;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static boolean isLowMagnification()
  {
    return getMagnification().equals(MagnificationEnum.NOMINAL);
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static boolean isHighMagnification()
  {
    return getMagnification().equals(MagnificationEnum.H_NOMINAL);
  }  
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static MagnificationEnum getMagnification()
  {
    if(_magnification == null)
      _magnification = MagnificationEnum.NOMINAL;
    
    return _magnification;
  }
  
  /**
   * @author Chong Wei Chin
   */
  public static int getMaximumCarrierUpWarpInNanoMeters()
  {
    return getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_CARRIER_UP_WARP_IN_NANOMETERS);
  }

  /**
   * @author Chong Wei Chin
   */
  public static int getMaximumCarrierDownWarpInNanoMeters()
  {
    return getConfig().getIntValue(SoftwareConfigEnum.MAXIMUM_CARRIER_DOWN_WARP_IN_NANOMETERS);
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * XCR-3589 Combo STD and XXL software GUI
   * 
   * WARNING - do not use this if you do not have to. 
   * 
   * This should only be called if software license is OLP.
   * To show which system type setting is load when OLP support multi system type
   * 
   */
  public static void changeSystemType(SystemTypeEnum systemType)
  {
    if (getSystemType().equals(systemType))
    {
      return;
    }
    
    if (systemType.equals(SystemTypeEnum.XXL))
    {
      _isXXLBased = true;
      _isS2EXEnabled = false;
      _isThroughputEnabled = false;
    }
    else if (systemType.equals(SystemTypeEnum.S2EX))
    {
      _isS2EXEnabled = true;
      _isXXLBased = false;
      _isThroughputEnabled = false;
    }
    else
    {
      _isXXLBased = false;
      _isS2EXEnabled = false;
      _isThroughputEnabled = true;
    }
  }
  
}
