package com.axi.v810.hardware;

import java.io.*;
import java.net.*;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 * Test class for ProjectionRegion.
 *
 * @author Matt Wharton
 */
public class Test_ProjectionRegion extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  public Test_ProjectionRegion()
  {
    // Do nothing...
  }


  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    try
    {
      ProjectionRegion badProjectionRegion = new ProjectionRegion( null, ImageReconstructionEngineEnum.IRE1, InetAddress.getLocalHost() );
      Expect.expect( false );
    }
    catch ( UnknownHostException uhex )
    {
      Expect.expect( false );
    }
    catch ( AssertException aex )
    {
      // Do nothing...
    }

    try
    {
      ProjectionRegion badProjectionRegion = new ProjectionRegion( new ImageRectangle( 100, 100, 100, 100 ), null, InetAddress.getLocalHost() );
      Expect.expect( false );
    }
    catch ( UnknownHostException uhex )
    {
      Expect.expect( false );
    }
    catch ( AssertException aex )
    {
      // Do nothing...
    }

    try
    {
      ProjectionRegion badProjectionRegion = new ProjectionRegion( new ImageRectangle( 100, 100, 100, 100 ), ImageReconstructionEngineEnum.IRE1, null );
      Expect.expect( false );
    }
    catch ( AssertException aex )
    {
      // Do nothing...
    }

    ImageRectangle projectionRegionRectangle = new ImageRectangle( 100, 100, 100, 100 );
    ImageReconstructionEngineEnum projectionDataDestinationId = ImageReconstructionEngineEnum.IRE1;
    InetAddress projectionDataDestinationAddress = null;
    try
    {
      projectionDataDestinationAddress = InetAddress.getByName( "192.168.1.1" );
    }
    catch ( UnknownHostException uhex )
    {
      Expect.expect( false );
    }
    ProjectionRegion projectionRegion = new ProjectionRegion( projectionRegionRectangle, projectionDataDestinationId,
                                                              projectionDataDestinationAddress );

    Expect.expect( projectionRegion.getProjectionRegionRectangle().equals( projectionRegionRectangle ) );
    Expect.expect( projectionRegion.getProjectionDataDestinationId().equals( projectionDataDestinationId ) );
    Expect.expect( projectionRegion.getProjectionDataDestinationAddress().equals( projectionDataDestinationAddress ) );
  }

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ProjectionRegion() );
  }
}
