package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the serial port does not support receive timeout, then throw this exception.
 *
 * @author Bob Balliew
 */
class NoReceiveTimeoutSupportHardwareException extends HardwareException
{
  /**
   * This is for receive threshold failure on the serial port.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param timeoutInMilliseconds is the specified timeout (in milliseconds).
   * @author Bob Balliew
   */
  public NoReceiveTimeoutSupportHardwareException(String portName, int timeoutInMilliseconds)
  {
    super(new LocalizedString("HW_NO_RECEIVE_TIMEOUT_SUPPORT_EXCEPTION_KEY",
                              new Object[]{portName, timeoutInMilliseconds}));

    Assert.expect(portName != null);
    Assert.expect(timeoutInMilliseconds >= 0);
  }
}
