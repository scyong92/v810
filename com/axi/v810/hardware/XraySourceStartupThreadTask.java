package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class XraySourceStartupThreadTask extends ThreadTask<Object>
{
  private AbstractXraySource _xRaySource;

  /**
   * @author Bill Darbie
   */
  XraySourceStartupThreadTask()
  {
    super("Xray source startup thread task");

    _xRaySource = XraySource.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws Exception
  {
    _xRaySource.startup();
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _xRaySource.abort();
  }
}
