package com.axi.v810.hardware;

/**
 * @author Greg Esparza
 */
public class MotionAxisPosition implements MotionAxisPositionInt
{
  private int _xAxisPositionInNanometers;
  private int _yAxisPositionInNanometers;
  private int _railWidthAxisPositionInNanometers;

  /**
   * @author Greg Esparza
   */
  public MotionAxisPosition()
  {
    _xAxisPositionInNanometers = 0;
    _yAxisPositionInNanometers = 0;
    _railWidthAxisPositionInNanometers = 0;
  }

  /**
   * @author Greg Esparza
   */
  void setXaxisPositionInNanometers(int xAxisPositionInNanometers)
  {
    _xAxisPositionInNanometers = xAxisPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  void setYaxisPositionInNanometers(int yAxisPositionInNanometers)
  {
    _yAxisPositionInNanometers = yAxisPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  void setRailWidthAxisPositionInNanometers(int railWidthAxisPositionInNanometers)
  {
    _railWidthAxisPositionInNanometers = railWidthAxisPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getXaxisPositionInNanometers()
  {
    return _xAxisPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getYaxisPositionInNanometers()
  {
    return _yAxisPositionInNanometers;
  }

  /**
   * @author Greg Esparza
   */
  public int getRailWidthAxisPositionInNanometers()
  {
    return _railWidthAxisPositionInNanometers;
  }
}
