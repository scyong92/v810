package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Matt Wharton
 */
class Test_ScanPass extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  public Test_ScanPass()
  {
  }


  /**
   * @author Matt Wharton
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ScanPass scanPass = null;

    try
    {
      scanPass = new ScanPass( -5, new StagePositionMutable(), new StagePositionMutable() );
      Expect.expect( false );
    }
    catch ( AssertException ae )
    {
      // Do nothing...
    }

    try
    {
      scanPass = new ScanPass( 1, null, new StagePositionMutable() );
      Expect.expect( false );
    }
    catch ( AssertException ae )
    {
      // do nothing
    }

    try
    {
      scanPass = new ScanPass( 1, new StagePositionMutable(), null );
      Expect.expect( false );
    }
    catch ( AssertException ae )
    {
      // do nothing
    }

    StagePositionMutable startPoint = new StagePositionMutable();
    startPoint.setXInNanometers( 100 );
    startPoint.setYInNanometers( 200 );
    StagePositionMutable endPoint = new StagePositionMutable();
    endPoint.setXInNanometers( 300 );
    endPoint.setYInNanometers( 400 );
    scanPass = new ScanPass( 1, startPoint, endPoint );
    Expect.expect( scanPass.getStartPointInNanoMeters().equals( startPoint ) );
    Expect.expect( scanPass.getEndPointInNanoMeters().equals( endPoint ) );
  }

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ScanPass() );
  }

}