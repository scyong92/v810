package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.HardwareConfigEnum;
import com.axi.v810.util.*;

//Variable Mag Anthony August 2011
/**
 * This class controls the right safety level sensor.
 * @author Anthony Fong
 */
public class RightSafetyLevelSensor extends HardwareObject implements SafetyLevelSensorInt
{
  private static RightSafetyLevelSensor _instance = null;
  private DigitalIo _digitalIo;
  // Below are redundant sensor readings that are used to confirm whether the barrier is open or closed.
  private boolean _safetyLevelIsOK = false;
 
  private boolean _realTimeMonitor = false;
  
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private static WorkerThread _sensorPollingThread;

  static 
  {
    _sensorPollingThread = new WorkerThread("Safety Sensor Polling Thread");
  }

  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private RightSafetyLevelSensor()
  {
    _digitalIo = DigitalIo.getInstance();
  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized RightSafetyLevelSensor getInstance()
  {
    if (_instance == null)
      _instance = new RightSafetyLevelSensor ();

    return _instance;
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Anthony Fong
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _safetyLevelIsOK = _digitalIo.isXrayRightSafetyLevelOK();
  }
 

  /**
   * @return true if the safety level is OK.
   * @author Anthony Fong
   */
  public boolean isOK() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the safety level is OK.
    updateSensorStatus();
    if(isSimulationModeOn())
      return true;
    else
      return _safetyLevelIsOK;
  }
  
  /**
   * @author Wei Chin
   */
  public boolean getLastSafetyStatus()
  {
    if(isSimulationModeOn())
      return true;
    else
      return _safetyLevelIsOK;
  }

  /**
   * @return boolean whether the safety level installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_RIGH_OUTER_BARRIER_INSTALLED);
  }

    /**
   * @author Wei Chin
   */
  public void startMonitorStatus()
  {
// Start the thread to check our status constantly.
    if (_realTimeMonitor == false)
    {
      _realTimeMonitor = true;
      _sensorPollingThread.invokeLater(new Runnable()
      {
        public void run()
        {
          pollingSensorStatus();
        }
      });
    }
  }
  
  /**
   * @author Wei Chin
   */
  public void stopMonitorStatus()
  {
    _realTimeMonitor = false;
  }
  
  /**
   * To be run on a thread to poll for Safety Sensor status.  In case of detect any tall component,
   * we will generate event to notify user.
   * @author Wei Chin
   */
  private void pollingSensorStatus()
  {
    while (_realTimeMonitor)
    {
      try
      {
        Thread.sleep(_SAFETY_SENSOR_POLLING_INTERVAL_IN_MILLI_SECONDS);
      }
      catch (InterruptedException ie)
      {
        // Do nothing.
      }

      if (isSimulationModeOn() == false)
      {
        try
        {
          if (_digitalIo.isStartupRequired())
            _realTimeMonitor = false;

          if (_realTimeMonitor == false)
            return;

          updateSensorStatus();
          if(_safetyLevelIsOK == false)
          {
            _realTimeMonitor = false;
          }
        }
        catch (XrayTesterException xte)
        {
          _hardwareObservable.notifyObserversOfException(xte);
        }
      }
    }
  }
}
