package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
* This class contains all the settings a given camera needs for a given projection.
*
* These settings include:
* <li>The scan pass number</li>
* <li>The starting capture line</li>
* <li>The total number of capture lines</li>
* <li>The list of projection regions</li>
*
* @author Matt Wharton
*/
public class ProjectionSettings
{
  private ScanPass _scanPass;
  private int _startCaptureLine =-1;
  private int _numberOfCaptureLines = -1;
  private List<ProjectionRegion> _projectionRegions= null;

  /**
  * @author Matt Wharton
  */
  public ProjectionSettings( ScanPass scanPass,
                             int startCaptureLine,
                             int numberOfCaptureLines,
                             List<ProjectionRegion> projectionRegions )
  {
    Assert.expect( scanPass != null );
    Assert.expect( startCaptureLine >= 0 );
    Assert.expect( numberOfCaptureLines >= 0 );
    Assert.expect( projectionRegions != null );

    _scanPass = scanPass;
    _startCaptureLine = startCaptureLine;
    _numberOfCaptureLines = numberOfCaptureLines;
    _projectionRegions = projectionRegions;
  }


  /**
  * @author Matt Wharton
  */
  public int getScanPassNumber()
  {
    Assert.expect(_scanPass != null);

    return _scanPass.getId();
  }


  /**
  * @author Matt Wharton
  */
  public int getStartCaptureLine()
  {
    Assert.expect(_startCaptureLine >= 0);

    return _startCaptureLine;
  }


  /**
  * @author Matt Wharton
  */
  public int getNumberOfCaptureLines()
  {
    Assert.expect(_numberOfCaptureLines >= 0);

    return _numberOfCaptureLines;
  }


  /**
  * @author Matt Wharton
  */
  public List<ProjectionRegion> getProjectionRegions()
  {
    Assert.expect(_projectionRegions != null);

    return _projectionRegions;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_projectionRegions != null)
      _projectionRegions.clear();
    
    _scanPass = null;
  }
  
  /**
   * @author Wei Chin
   */
  public MechanicalConversions getMechanicalConversions()
  {
    Assert.expect(_scanPass != null);
    
    return  _scanPass.getMechanicalConversions();
  }
}
