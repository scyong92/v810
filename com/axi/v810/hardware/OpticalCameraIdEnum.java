package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraIdEnum extends com.axi.util.Enum implements Serializable
{
    private static HashMap<Integer, OpticalCameraIdEnum> _integerToEnumMap;
    private static int _index = 0;
    private int _deviceId = -1;

    /**
     * @author Cheah Lee Herng
     */
    static
    {
        _integerToEnumMap = new HashMap<Integer, OpticalCameraIdEnum>();
    }

    public static final OpticalCameraIdEnum OPTICAL_CAMERA_1 = new OpticalCameraIdEnum(++_index);
    public static final OpticalCameraIdEnum OPTICAL_CAMERA_2 = new OpticalCameraIdEnum(++_index);

    /**
   * @author Cheah Lee Herng
   */
    private OpticalCameraIdEnum(int id)
    {
        super(id);
        _deviceId = id;

        Assert.expect(_integerToEnumMap.put(new Integer(id), this) == null);
    }

    /**
    * @author Cheah Lee Herng
    */
    static public OpticalCameraIdEnum getEnum(Integer id)
    {
        Assert.expect(id != null);
        OpticalCameraIdEnum enumValue = _integerToEnumMap.get(id);

        Assert.expect(enumValue != null);
        return enumValue;
    }

    /**
     * @author Cheah Lee Herng
     */
    public int getDeviceId()
    {
        return _deviceId;
    }

    /**
    * @author Cheah Lee Herng
    */
    static public ArrayList<OpticalCameraIdEnum> getAllEnums()
    {
        return new ArrayList<OpticalCameraIdEnum>(_integerToEnumMap.values());
    }
}
