package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If a InterrupedIOException occurs, then throw this exception.
 *
 * @author Bob Balliew
 */
class InterruptedIOHardwareException extends HardwareException
{
  /**
   * This is for InterruptedIOExceptions.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param bytesTransferred is the number of bytes transferred before the
   * IO was interrupted
   * @author Bob Balliew
   */
  public InterruptedIOHardwareException(String portName, int bytesTransferred)
  {
    super(new LocalizedString("HW_INTERRUPTED_IO_EXCEPTION_KEY",
                              new Object[]{portName, bytesTransferred}));

    Assert.expect(portName != null);
  }

  /**
   * This is for InterruptedIOExceptions with a detailed message.
   *
   * @param portName is the actual connector or port used, i.e. "COM1"
   * @param bytesTransferred is the number of bytes transferred before the
   * IO was interrupted
   * @param detailedMessage is the (non null) string returned by 'getMessage()'
   * @author Bob Balliew
   */
  public InterruptedIOHardwareException(String portName, int bytesTransferred, String detailedMessage)
  {
    super(new LocalizedString("HW_INTERRUPTED_IO_WITH_MESSAGE_EXCEPTION_KEY",
                              new Object[]{portName, bytesTransferred, detailedMessage}));

    Assert.expect(portName != null);
    Assert.expect(detailedMessage != null);
  }
}
