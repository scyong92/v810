package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If the data received over the serial port does not foollow the expected protocol,
 * then throw this exception.
 *
 * @author Jeff Ryer
 */
public class SerialCommunicationProtocolHardwareException extends HardwareException
{
  /**
   * @author Jeff Ryer
   */
  SerialCommunicationProtocolHardwareException(String portName,
                                         String expectedReply,
                                         String actualReply)
  {
    super(new LocalizedString("HW_SERIAL_COMMUNICATION_PROTOCOL_ERROR_KEY",
                              new Object[]{portName,
                                           expectedReply,
                                           actualReply}));
    Assert.expect(portName != null);
    Assert.expect(expectedReply != null);
    Assert.expect(actualReply != null);
  }
}
