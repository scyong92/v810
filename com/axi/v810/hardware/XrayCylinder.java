package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the Xray Cylinder.  The Xray Cylinder is used to control
 * x-ray up and down movement.  For Variable Magnification, the x-ray tube is able
 * to turn up and down in order to change the system magnification factor.
 * @author Anthony Fong
 */
public class XrayCylinder extends HardwareObject implements XrayCylinderInt
{
  private static XrayCylinder _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the X-Ray cylinder is up or down.
  private boolean _xrayCylinderIsDown;
  private boolean _xrayCylinderIsNotUp;
  private boolean _xrayCylinderIsUp;
  private boolean _xrayCylinderIsNotDown;
  private static boolean _WAIT_NOT_ABORTABLE = false;


  private HTubeXraySource _xraySource = null;

  //Variable Mag Anthony August 2011
  private InnerBarrier _innerBarrier = null;
    
  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private XrayCylinder()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
    
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _xraySource = (HTubeXraySource)xraySource;
    _innerBarrier = InnerBarrier.getInstance();

  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized XrayCylinder getInstance()
  {
    if (_instance == null)
      _instance = new XrayCylinder();

    return _instance;
  }
  /**
   * Turn Off the X-Ray.
   * @author Anthony Fong
   */
  public void turnOffXRays(boolean serviceMode) throws XrayTesterException
  {
    _xraySource.turnOffXRays(serviceMode);
  }
  /**
   * Turn On the X-Ray.
   * @author Anthony Fong
   */
  public void turnOnImagingXRays() throws XrayTesterException
  {
    _xraySource.turnOnImagingXRays();
  }
  
  /**
   * Move the X-Ray cylinder down.
   * @author Anthony Fong
   */
  public void down(boolean loadConfig) throws XrayTesterException
  {
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupHighMagImagingXrayParameters();
        XrayCameraArray.getInstance().loadHighMagGrayScale();            
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForHighMag();
      }
    }
    
//    System.out.println("Cyliner Down");
    boolean initStateIsDown = isDown();
    if(initStateIsDown)
      return;
    
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
        _hardwareObservable.stateChangedBegin(this, XrayCylinderEventEnum.XRAY_CYLINDER_DOWN);
        _hardwareObservable.setEnabled(false);

        try
        {
          // Time the close action.
          _hardwareActionTimer.reset();
          _hardwareActionTimer.start();

          _digitalIo.turnXrayCylinderDownOn();
          waitUntilDown();
//          if(!initStateIsDown)
//          {
//            sleep(500);
//          }
        }
        finally
        {
          _hardwareActionTimer.stop();
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, XrayCylinderEventEnum.XRAY_CYLINDER_DOWN);
    }
  }

  /**
   * Move the X-Ray cylinder up.
   * @author Anthony Fong
   */
  public void up(boolean loadConfig) throws XrayTesterException
  {
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupLowMagImagingXrayParameters();      
        XrayCameraArray.getInstance().loadLowMagGrayScale();        
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
      }
    }
    
//    System.out.println("Cyliner Up");
    boolean initStateIsUp = isUp();

    if(initStateIsUp)
      return;
    
    _hardwareObservable.stateChangedBegin(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.turnXrayCylinderDownOff();
      waitUntilUp();
//      if(!initStateIsUp)
//      {
//        sleep(500);
//      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
  }

  /**
   * @return long returns how long last open/close action time.
   * @author Anthony Fong
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Anthony Fong
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _xrayCylinderIsDown = _digitalIo.isXrayCylinderDown();
    _xrayCylinderIsNotUp = _digitalIo.isXrayCylinderNotUp();

    _xrayCylinderIsUp = _digitalIo.isXrayCylinderUp();
    _xrayCylinderIsNotDown = _digitalIo.isXrayCylinderNotDown();
  }

  /**
   * When the X-Ray cylinder is being commanded to up/down, there is a window of time
   * where the cylinder is neither up nor down.
   * @return true if the cylinder sensor are giving consistent state information.
   * @author Anthony Fong
   */
  private boolean isHardwareDownSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayCylinderIsDown == _xrayCylinderIsNotUp;
    return isSettled;
  }
  private boolean isHardwareUpSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayCylinderIsUp == _xrayCylinderIsNotDown;
    return isSettled;
  }
  /**
   * @return true if the X-Ray cylinder is for sure down.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isDown() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray cylinder is down.
    updateSensorStatus();

    if (isHardwareDownSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayCylinderIsStuckException());

    return _xrayCylinderIsDown;
  }

  /**
   * @return true if the X-Ray cylinder is for sure down.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isUp() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray cylinder is up.
    updateSensorStatus();

    if (isHardwareUpSettled() == false)
      HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayCylinderIsStuckException());

    return _xrayCylinderIsUp;
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilDown() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayCylinderDownTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayCylinderSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCylinderIsDown = false;
        updateSensorStatus();
        if (isHardwareDownSettled())
          xrayCylinderIsDown = _xrayCylinderIsDown;

        return xrayCylinderIsDown;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayCylinderFailedToGoDownException();
      }
    };
    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilUp() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayCylinderUpTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayCylinderSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCylinderIsUp = false;
        updateSensorStatus();
        if (isHardwareUpSettled())
          xrayCylinderIsUp = _xrayCylinderIsUp;

        return xrayCylinderIsUp;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayCylinderFailedToGoUpException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }

    /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Anthony Fong
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }

  /**
   * For Variable Magnification, the X-Ray Cylinder is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray Cylinder is installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED);
  }

}
