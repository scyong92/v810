package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bob Balliew
 */
class BarcodeReaderEnum extends com.axi.util.Enum
{
  private BarcodeReaderConfigEnum _trigger;
  private BarcodeReaderConfigEnum _stop;
  private BarcodeReaderConfigEnum _preamble;
  private BarcodeReaderConfigEnum _postamble;
  private static Set<BarcodeReaderEnum> _allValues = new HashSet<BarcodeReaderEnum>();

  public static BarcodeReaderEnum BCR1 = new BarcodeReaderEnum(0,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_1,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_1,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_1,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_1);

  public static BarcodeReaderEnum BCR2 = new BarcodeReaderEnum(1,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_2,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_2,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_2,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_2);

  public static BarcodeReaderEnum BCR3 = new BarcodeReaderEnum(2,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_3,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_3,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_3,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_3);

  public static BarcodeReaderEnum BCR4 = new BarcodeReaderEnum(3,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_4,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_4,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_4,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_4);

  public static BarcodeReaderEnum BCR5 = new BarcodeReaderEnum(4,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_5,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_5,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_5,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_5);

  public static BarcodeReaderEnum BCR6 = new BarcodeReaderEnum(5,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_6,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_6,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_6,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_6);

  public static BarcodeReaderEnum BCR7 = new BarcodeReaderEnum(6,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_7,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_7,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_7,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_7);

  public static BarcodeReaderEnum BCR8 = new BarcodeReaderEnum(7,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_8,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_8,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_8,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_8);

  public static BarcodeReaderEnum BCR9 = new BarcodeReaderEnum(8,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_9,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_9,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_9,
                                                               BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_9);

  public static BarcodeReaderEnum BCR10 = new BarcodeReaderEnum(9,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_10,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_10,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_10,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_10);

  public static BarcodeReaderEnum BCR11 = new BarcodeReaderEnum(10,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_11,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_11,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_11,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_11);

  public static BarcodeReaderEnum BCR12 = new BarcodeReaderEnum(11,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_12,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_12,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_12,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_12);

  public static BarcodeReaderEnum BCR13 = new BarcodeReaderEnum(12,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_13,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_13,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_13,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_13);

  public static BarcodeReaderEnum BCR14 = new BarcodeReaderEnum(13,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_14,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_14,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_14,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_14);

  public static BarcodeReaderEnum BCR15 = new BarcodeReaderEnum(14,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_15,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_15,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_15,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_15);

  public static BarcodeReaderEnum BCR16 = new BarcodeReaderEnum(15,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_TRIGGER_SCANNER_16,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_STOP_SCANNER_16,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_PREAMBLE_SCANNER_16,
                                                                BarcodeReaderConfigEnum.BARCODE_READER_POSTAMBLE_SCANNER_16);

  /**
   * @author Bob Balliew
   */
  protected BarcodeReaderEnum(int order, BarcodeReaderConfigEnum trigger, BarcodeReaderConfigEnum stop, BarcodeReaderConfigEnum preamble, BarcodeReaderConfigEnum postamble)
  {
    super(order);
    Assert.expect(order >= 0);
    Assert.expect(trigger != null);
    Assert.expect(stop != null);
    Assert.expect(preamble != null);
    Assert.expect(postamble != null);
    _trigger = trigger;
    _stop = stop;
    _preamble = preamble;
    _postamble = postamble;
    Assert.expect(_allValues.add(this));
  }

  /**
   * @author Bob Balliew
   */
  BarcodeReaderConfigEnum getTrigger()
  {
    return _trigger;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  BarcodeReaderConfigEnum getStop()
  {
    return _stop;
  }

  /**
   * @author Bob Balliew
   */
  BarcodeReaderConfigEnum getPreamble()
  {
    return _preamble;
  }

  /**
   * @author Bob Balliew
   */
  BarcodeReaderConfigEnum getPostamble()
  {
    return _postamble;
  }

  /**
   * @author Bob Balliew
   */
  int getOrder()
  {
    return getId();
  }

  /**
   * @author Andy Mechtenberg
   */
  static Collection<BarcodeReaderEnum> values()
  {
    return new HashSet<BarcodeReaderEnum>(_allValues);
  }

}

