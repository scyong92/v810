package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class ScanStepDirectionEnum extends com.axi.util.Enum
{
  LocalizedString _name;

  public static final ScanStepDirectionEnum FORWARD = new ScanStepDirectionEnum(1, new LocalizedString("GUI_FORWARD_DIRECTION_KEY", null));
  public static final ScanStepDirectionEnum REVERSE = new ScanStepDirectionEnum(-1, new LocalizedString("GUI_REVERSE_DIRECTION_KEY", null));

  /**
   * @author Greg Esparza
   */
  public ScanStepDirectionEnum(int id, LocalizedString name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author Greg Esparza
   */
  public String toString()
  {
    return StringLocalizer.keyToString(_name);
  }
}
