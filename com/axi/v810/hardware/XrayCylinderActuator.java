package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import static com.axi.v810.hardware.XrayCPMotorActuator.isInstalled;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class controls the Xray Cylinder.  The Xray Cylinder is used to control
 * x-ray up and down movement.  For Variable Magnification, the x-ray tube is able
 * to turn up and down in order to change the system magnification factor.
 * @author Anthony Fong
 */
public class XrayCylinderActuator extends HardwareObject implements XrayActuatorInt
{
  private static XrayCylinderActuator _instance = null;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private TimerUtil _hardwareActionTimer;
  // Below are redundant sensor readings that are used to confirm whether the X-Ray cylinder is up or down.
  private boolean _xrayCylinderIsDown;
  private boolean _xrayCylinderIsNotUp;
  private boolean _xrayCylinderIsUp;
  private boolean _xrayCylinderIsNotDown;
  private static boolean _WAIT_NOT_ABORTABLE = false;


  private HTubeXraySource _xraySource = null;

  //Variable Mag Anthony August 2011
  private InnerBarrier _innerBarrier = null;
    
  /**
   * This contructor is not public on purpose - use the AT5dx object to get
   * references to this class
   * @author Anthony Fong
   */
  private XrayCylinderActuator()
  {
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareActionTimer = new TimerUtil();
    
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _xraySource = (HTubeXraySource)xraySource;
    _innerBarrier = InnerBarrier.getInstance();

  }

  /**
   * @return an instance of this class.
   * @author Anthony Fong
   */
  public static synchronized XrayCylinderActuator getInstance()
  {
    if (_instance == null)
      _instance = new XrayCylinderActuator();

    return _instance;
  }
  /**
   * Turn Off the X-Ray.
   * @author Anthony Fong
   */
  public void turnOffXRays(boolean serviceMode) throws XrayTesterException
  {
    _xraySource.turnOffXRays(serviceMode);
  }
  /**
   * Turn On the X-Ray.
   * @author Anthony Fong
   */
  public void turnOnImagingXRays() throws XrayTesterException
  {
    _xraySource.turnOnImagingXRays();
  }
  
  /**
   * Move the X-Ray cylinder down.
   * @author Anthony Fong
   */
  public void down(boolean loadConfig) throws XrayTesterException
  {
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupHighMagImagingXrayParameters();
        XrayCameraArray.getInstance().loadHighMagGrayScale();            
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForHighMag();
      }
    }
    
//    System.out.println("Cyliner Down");
    boolean initStateIsDown = isDown();
    if(initStateIsDown)
      return;
    
    _innerBarrier.open();
    if( _innerBarrier.isOpen())
    {
        _hardwareObservable.stateChangedBegin(this, XrayCylinderEventEnum.XRAY_CYLINDER_DOWN);
        _hardwareObservable.setEnabled(false);

        try
        {
          // Time the close action.
          _hardwareActionTimer.reset();
          _hardwareActionTimer.start();

          _digitalIo.turnXrayCylinderDownOn();
          if(_digitalIo.isInnerBarrierCommandedClose())
          {
            logHardwareStatus("WARNING: Xray tube failed to go high mag. Inner barrier is commanded closed in other thread, xray tube movement is aborted.");
            return;
          }
          waitUntilDown();
//          if(!initStateIsDown)
//          {
//            sleep(500);
//          }
          if (isSimulationModeOn())
          {
            updateSimulationSensorStatusForDown();
          }
        }
        finally
        {
          _hardwareActionTimer.stop();
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, XrayCylinderEventEnum.XRAY_CYLINDER_DOWN);
    }
    else
    {
      logHardwareStatus("WARNING: Xray tube failed to go high mag. Inner Barrier is still closed even it was commanded to open, please check the inner barrier");
      printXrayTubePositionStatus();
      printInnerBarrierPositionStatus();
    }
  }

  /**
   * Move the X-Ray cylinder up.
   * @author Anthony Fong
   */
  public void up(boolean loadConfig) throws XrayTesterException
  {
    if(loadConfig && isSimulationModeOn() == false)
    {
      if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
      {
        _xraySource.setupLowMagImagingXrayParameters();      
        XrayCameraArray.getInstance().loadLowMagGrayScale();        
        PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
      }
    }
    
//    System.out.println("Cyliner Up");
    boolean initStateIsUp = isUp();

    if(initStateIsUp)
      return;
    
    _hardwareObservable.stateChangedBegin(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.turnXrayCylinderDownOff();
      waitUntilUp();
//      if(!initStateIsUp)
//      {
//        sleep(500);
//      }
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForUp();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
  }
  
  /**
   * Move the X-Ray cylinder up.
   * @author Anthony Fong
   */
  public void upToPosition1(boolean loadConfig) throws XrayTesterException
  {
    // 20 Jan 2015 - xray cylinder actuator is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    up(loadConfig);
  }
  
  /**
   * Move the X-Ray cylinder up.
   * @author Anthony Fong
   */
  public void upToPosition2(boolean loadConfig) throws XrayTesterException
  {
    // 20 Jan 2015 - xray cylinder actuator is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    up(loadConfig);
  }

  /**
   * Move the X-Ray cylinder to home.
   * @author Anthony Fong
   */
  public void home(boolean loadConfig, boolean isHighMag) throws XrayTesterException
  {
    if(loadConfig && isSimulationModeOn() == false)
    {
      if(isHighMag==true)
      {
        if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
        {
          _xraySource.setupHighMagImagingXrayParameters();
          XrayCameraArray.getInstance().loadHighMagGrayScale();            
          PanelPositioner.getInstance().resetYaxisHysteresisOffsetForHighMag();
        }
      }
      else
      {
        if (XrayTester.getInstance().isStartupRequiredForMotionRelatedHardware() == false)
        {
          _xraySource.setupLowMagImagingXrayParameters();
          XrayCameraArray.getInstance().loadLowMagGrayScale();        
          PanelPositioner.getInstance().resetYaxisHysteresisOffsetForLowMag();
        }
      }
    }
//    System.out.println("Cyliner Up");
    boolean initStateIsUp = isUp();

    if(initStateIsUp)
      return;
    
    _hardwareObservable.stateChangedBegin(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
    _hardwareObservable.setEnabled(false);

    try
    {
      // Time the open action.
      _hardwareActionTimer.reset();
      _hardwareActionTimer.start();
      _digitalIo.turnXrayCylinderDownOff();
      waitUntilUp();
//      if(!initStateIsUp)
//      {
//        sleep(500);
//      }
      
      if (isSimulationModeOn())
      {
        updateSimulationSensorStatusForUp();
      }
    }
    finally
    {
      _hardwareActionTimer.stop();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, XrayCylinderEventEnum.XRAY_CYLINDER_UP);
  }
  
  /**
   * @return long returns how long last open/close action time.
   * @author Anthony Fong
   */
  public long getLastHardwareActionTimeInMilliSeconds()
  {
    return _hardwareActionTimer.getElapsedTimeInMillis();
  }

  /**
   * This method is used to update sensor readings.  The results can then be
   * analyzed by other methods.
   * @author Anthony Fong
   */
  private void updateSensorStatus() throws XrayTesterException
  {
    _xrayCylinderIsDown = _digitalIo.isXrayCylinderDown();
    _xrayCylinderIsNotUp = _digitalIo.isXrayCylinderNotUp();

    _xrayCylinderIsUp = _digitalIo.isXrayCylinderUp();
    _xrayCylinderIsNotDown = _digitalIo.isXrayCylinderNotDown();
  }

  /**
   * When the X-Ray cylinder is being commanded to up/down, there is a window of time
   * where the cylinder is neither up nor down.
   * @return true if the cylinder sensor are giving consistent state information.
   * @author Anthony Fong
   */
  private boolean isHardwareDownSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayCylinderIsDown == _xrayCylinderIsNotUp;
    return isSettled;
  }
  private boolean isHardwareUpSettled() throws XrayTesterException
  {
    boolean isSettled = _xrayCylinderIsUp == _xrayCylinderIsNotDown;
    return isSettled;
  }
  /**
   * @return true if the X-Ray cylinder is for sure down.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isDown() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray cylinder is down.
    if(isSimulationModeOn() == false)
    {
      updateSensorStatus();

      if (isHardwareDownSettled() == false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayCylinderIsStuckException());
      }
    }

    return _xrayCylinderIsDown;
  }

  /**
   * @return true if the X-Ray cylinder is for sure up.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isUp() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray cylinder is up.
    if (isSimulationModeOn() == false)
    {
      updateSensorStatus();

      if (isHardwareUpSettled() == false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayCylinderIsStuckException());
      }
    }

    return _xrayCylinderIsUp;
  }
  
  /**
   * @return true if the X-Ray cylinder is for sure up.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition1() throws XrayTesterException
  {
    // 20 Jan 2015 - xray cylinder actuator is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    return isUp();
  }
  
  /**
   * @return true if the X-Ray cylinder is for sure up.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Swee Yee Wong
   */
  public boolean isUpToPosition2() throws XrayTesterException
  {
    // 20 Jan 2015 - xray cylinder actuator is only set for two position, up (19um) and down (11um)
    // therefore upToPosition1 and upToPosition2 are calling the same up function
    return isUp();
  }
  
    /**
   * @return true if the X-Ray cylinder is for sure up or home.
   * @throws XrayTesterException exception will be thrown if the X-Ray cylinder is neither
   * up nor down.
   * @author Anthony Fong
   */
  public boolean isHome() throws XrayTesterException
  {
    // Update the sensor status and then we can use the readings to
    // determine if the X-Ray cylinder is up or home.
    if(isSimulationModeOn() == false)
    {
      updateSensorStatus();

      if (isHardwareUpSettled() == false)
      {
        HardwareTaskEngine.getInstance().throwHardwareException(DigitalIoException.getXrayCylinderIsStuckException());
      }
    }

    return _xrayCylinderIsUp;
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilDown() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayCylinderDownTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayCylinderSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCylinderIsDown = false;
        updateSensorStatus();
        if (isHardwareDownSettled())
          xrayCylinderIsDown = _xrayCylinderIsDown;

        return xrayCylinderIsDown;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayCylinderFailedToGoDownException();
      }
    };
    myWait.waitUntilConditionIsMet();
  }

  /**
   * @author Anthony Fong
   */
  private void waitUntilUp() throws XrayTesterException
  {
    WaitForHardwareCondition myWait = new WaitForHardwareCondition(_digitalIo.getXrayCylinderUpTimeOutInMilliSeconds(),
                                                                   _digitalIo.getXrayCylinderSettlingTimeOutInMilliSeconds(),
                                                                   _WAIT_NOT_ABORTABLE)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean xrayCylinderIsUp = false;
        updateSensorStatus();
        if (isHardwareUpSettled())
          xrayCylinderIsUp = _xrayCylinderIsUp;

        return xrayCylinderIsUp;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return DigitalIoException.getXrayCylinderFailedToGoUpException();
      }
    };

    myWait.waitUntilConditionIsMet();
  }

    /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Anthony Fong
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
  
  /**
   * @return boolean whether the X-Ray Actuator Interface is installed.
   * @author Anthony Fong
   */
  
//  public boolean isActuatorInstalled()
//  {
//    return isInstalled();
//  }

  /**
   * For Variable Magnification, the X-Ray Cylinder is needed to adjust for Low/High Mag
   * @return boolean whether the X-Ray Cylinder is installed.
   * @author Anthony Fong
   */
  public static boolean isInstalled()
  {
    return getConfig().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == true &&   
           getConfig().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == false;
  }
  
  /**
   * This method is used to update simulation sensor readings.
   * @author Swee Yee Wong
   */
  private void updateSimulationSensorStatusForUp() throws XrayTesterException
  {
    _xrayCylinderIsDown = false;
    _xrayCylinderIsNotDown = true;

    _xrayCylinderIsUp = true;
    _xrayCylinderIsNotUp = false;
  }
  
  /**
   * This method is used to update simulation sensor readings.
   * @author Swee Yee Wong
   */
  private void updateSimulationSensorStatusForDown() throws XrayTesterException
  {
    _xrayCylinderIsDown = true;
    _xrayCylinderIsNotDown = false;

    _xrayCylinderIsUp = false;
    _xrayCylinderIsNotUp = true;
  }

  /**
   * @author Swee-Yee.Wong
   */
  public void printXrayTubePositionStatus()
  {
    try
    {
      if (XrayCPMotorActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayZAxisHome())
        {
          logHardwareStatus("X-ray tube at home position");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition1())
        {
          logHardwareStatus("X-ray tube at low mag position 1");
        }
        if (DigitalIo.getInstance().isXrayZAxisUpToPosition2())
        {
          logHardwareStatus("X-ray tube at low mag position 2");
        }
        if (DigitalIo.getInstance().isXrayZAxisDown())
        {
          logHardwareStatus("X-ray tube at high mag position");
        }
        if (DigitalIo.getInstance().isXrayZAxisHome() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition1() == false && DigitalIo.getInstance().isXrayZAxisUpToPosition2() == false && DigitalIo.getInstance().isXrayZAxisDown() == false)
        {
          logHardwareStatus("ERROR - Xray tube: X-ray tube position status unknown");
        }
      }
      else if (XrayCylinderActuator.isInstalled())
      {
        if (DigitalIo.getInstance().isXrayCylinderUp())
        {
          logHardwareStatus("X-ray tube at home/low mag position");
        }
        if (DigitalIo.getInstance().isXrayCylinderDown())
        {
          logHardwareStatus("X-ray tube at high mag position 1");
        }
        if (DigitalIo.getInstance().isXrayCylinderUp() == false && DigitalIo.getInstance().isXrayCylinderDown() == false)
        {
          logHardwareStatus("ERROR - Xray tube: X-ray tube position status unknown");
        }
      }
    }
    catch (XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      System.out.println("ERROR - Xray tube: Failed to get xray tube position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public void printInnerBarrierPositionStatus()
  {
    try
    {
      if(DigitalIo.getInstance().isInnerBarrierInstalled())
      {
        if (DigitalIo.getInstance().isInnerBarrierOpen())
        {
          logHardwareStatus("Inner barrier is opened");
        }
        if (DigitalIo.getInstance().isInnerBarrierClosed())
        {
          logHardwareStatus("Inner barrier is closed");
        }
        if (DigitalIo.getInstance().isInnerBarrierOpen() == false && DigitalIo.getInstance().isInnerBarrierClosed() == false)
        {
          logHardwareStatus("ERROR - Xray tube: Inner barrier position status unknown");
        }
      }
    }
    catch(XrayTesterException dioe)
    {
      //do nothing here. Since it is just some printout here, we should not throw any exception to prevent confusion to the user.
      logHardwareStatus("ERROR - Xray tube: Failed to get inner barrier position input bit status.");
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private void logHardwareStatus(String message)
  {
    try
    {
      HardwareStatusLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to log hardwareStatus. \n" + e.getMessage());
    }
  }
}
