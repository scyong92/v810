package com.axi.v810.hardware;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Greg Esparza
 */
public class MotionControlServiceModeConfigurationStatus implements Serializable
{
  private String _key;
  private ArrayList<String> _parameters;

  /**
   * @author Greg Esparza
   */
  public MotionControlServiceModeConfigurationStatus(String key, ArrayList<String> parameters)
  {
    Assert.expect(key != null);
    Assert.expect(parameters != null);

    _key = key;
    _parameters = parameters;
  }

  /**
   * @author Greg Esparza
   */
  public String getKey()
  {
    return _key;
  }

  /**
   * @author Greg Esparza
   */
  public ArrayList<String> getParameters()
  {
    return _parameters;
  }
}
