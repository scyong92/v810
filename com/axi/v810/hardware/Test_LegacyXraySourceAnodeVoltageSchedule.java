package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Greg Loring
 */
public class Test_LegacyXraySourceAnodeVoltageSchedule extends UnitTest
{

  /**
   * @author Greg Loring
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LegacyXraySourceAnodeVoltageSchedule());
  }

  /**
   * @author Greg Loring
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      LegacyXraySourceAnodeVoltageSchedule schedule
          = new LegacyXraySourceAnodeVoltageSchedule(160.0, 15000L);

      getVoltageFromTimeTests(schedule);

      getTimeFromVoltageTests(schedule);

      // shortest possible schedule
      schedule = new LegacyXraySourceAnodeVoltageSchedule(160.0, 1L);
      Expect.expect(schedule.getTimeOffsetInMillis(160.0) == 1L);
      Expect.expect(schedule.getKiloVolts(0) == 0.0);
      Expect.expect(schedule.getKiloVolts(1) == 160.0);
      Expect.expect(schedule.getKiloVolts(2) == 160.0);
    }
    catch(Exception x)
    {
      x.printStackTrace();
    }
  }

  /**
   * @author Greg Loring
   */
  private void getVoltageFromTimeTests(LegacyXraySourceAnodeVoltageSchedule schedule)
  {
    double kV;

    // test falling off the beginning of the schedule
    try
    {
       kV = schedule.getKiloVolts(-1);
       Expect.expect(false, "should've thrown");
    }
    catch (AssertException ax)
    {
      // expected outcome
    }

    // test the very beginning of the schedule
    kV = schedule.getKiloVolts(0L);
    Expect.expect(kV == 0.0);

    // test the middle of the schedule: 1/10th of the way in (time) -> 1/2 of voltage
    //  (this IS a point on the internal table)
    kV = schedule.getKiloVolts(1500L);
    Expect.expect(kV == 80.0);

    // test the middle of the schedule: 1/4th of the way in (time) -> 3/4 of voltage
    //  (this is NOT a point on the internal table)
    kV = schedule.getKiloVolts(3750L);
    Expect.expect(kV == 120.0);

    // test the very end of the schedule
    kV = schedule.getKiloVolts(15000L);
    Expect.expect(kV == 160.0);

    // test falling off the end of the schedule
    kV = schedule.getKiloVolts(16000L);
    Expect.expect(kV == 160.0);
  }

  /**
   * @author Greg Loring
   */
  private void getTimeFromVoltageTests(LegacyXraySourceAnodeVoltageSchedule schedule)
  {
    long msOffset;

    // test falling off the bottom of the schedule
    try
    {
       msOffset = schedule.getTimeOffsetInMillis(-1.0);
       Expect.expect(false, "should've thrown");
    }
    catch (AssertException ax)
    {
      // expected outcome
    }

    // test the very bottom of the schedule
    msOffset = schedule.getTimeOffsetInMillis(0.0);
    Expect.expect(msOffset == 0L);

    // test the middle of the schedule: 1/10th of the way in (time) -> 1/2 of voltage
    //  (this IS a point on the internal table)
    msOffset = schedule.getTimeOffsetInMillis(80.0);
    Expect.expect(msOffset == 1500L);

    // test the middle of the schedule: 1/4th of the way in (time) -> 3/4 of voltage
    //  (this is NOT a point on the internal table)
    msOffset = schedule.getTimeOffsetInMillis(120.0);
    Expect.expect(msOffset == 3750L);

    // schedule should return the ceiling of floating point answer to getTimeOffsetInMillis(v);
    //   given these scales and input voltage of 2.94kV -> time offset is about 55.1ms
    msOffset = schedule.getTimeOffsetInMillis(2.94);
    Expect.expect(msOffset == 56L);

    // test the very top of the schedule
    msOffset = schedule.getTimeOffsetInMillis(160.0);
    Expect.expect(msOffset == 15000L);

    // test falling off the top of the schedule
    msOffset = schedule.getTimeOffsetInMillis(161.0);
    Expect.expect(msOffset == 15000L);
  }

}
