package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class NetworkSwitch extends HardwareObject implements HardwareStartupShutdownInt
{
  private static NetworkSwitch _instance = null;
  private static String _me = "Network Switch";

  private boolean _initialized = false;
  private String _serverIpAddress = null;
  private HardwareTaskEngine _hardwareTaskEngine;

  /**
   * @author Rex Shang
   */
  private NetworkSwitch()
  {
    _serverIpAddress = getServerIpAddress();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @author Rex Shang
   */
  public static synchronized NetworkSwitch getInstance()
  {
    if (_instance == null)
      _instance = new NetworkSwitch();

    Assert.expect(_instance != null);
    return _instance;
  }

  /**
   * @author Rex Shang
   */
  public void startup() throws XrayTesterException
  {
    initialize();
  }

  /**
   * @author Rex Shang
   */
  public void shutdown() throws XrayTesterException
  {
    _initialized = false;
  }

  /**
   * @author Rex Shang
   */
  public boolean isStartupRequired() throws XrayTesterException
  {
    boolean isStartupRequired = _initialized == false;
    return isStartupRequired;
  }

  /**
   * @author Rex Shang
   */
  private void initialize() throws XrayTesterException
  {
    if (_initialized == false)
    {
      if (isSimulationModeOn() == false)
      {
        // The switch is currently not accessible by an ip address.  But the
        // communication test is done indirectly through other network devices.
//        verifyCommunication();
      }

      _initialized = true;
    }
  }

  /**
   * @author Rex Shang
   */
  private void verifyCommunication() throws XrayTesterException
  {
    ICMPPacket.getInstance().checkRemoteHostIsUsingAgilentJumboPacket(toString(), _serverIpAddress);
  }

  /**
   * @author Rex Shang
   */
  static String getServerIpAddress()
  {
    return getConfig().getStringValue(HardwareConfigEnum.SWITCH_IP_ADDRESS);
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _me;
  }

}
