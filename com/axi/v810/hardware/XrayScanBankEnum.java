package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * This class provides all the available banks of the
 * XrayScanController hardware.  Each bank can be loaded with data
 * that defines a particular circular pattern of the xray scan beam.
 * There is one set of data per field of view.
 *
 * @author Bill Darbie
 */
public class XrayScanBankEnum extends com.axi.util.Enum
{
  public static final XrayScanBankEnum DUMP_BANK = new XrayScanBankEnum(0);
  public static final XrayScanBankEnum BANK1 = new XrayScanBankEnum(1);
  public static final XrayScanBankEnum BANK2 = new XrayScanBankEnum(2);
  public static final XrayScanBankEnum BANK3 = new XrayScanBankEnum(3);


  /**
   * @author Bill Darbie
   */
  private XrayScanBankEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  public static int size()
  {
    return 4;
  }

  /**
   * @author Bill Darbie
   */
  public int getBankInt()
  {
    return getId();
  }

  /**
   * @author Bill Darbie
   */
  public static XrayScanBankEnum getBankEnum(int bank)
  {
    XrayScanBankEnum enumeration = null;

    if (bank == 0)
      enumeration = DUMP_BANK;
    else if (bank == 1)
      enumeration = BANK1;
    else if (bank == 2)
      enumeration = BANK2;
    else if (bank == 3)
      enumeration = BANK3;
    else
      Assert.expect(false);

    Assert.expect(enumeration != null);
    return enumeration;
  }
}
