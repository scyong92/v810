package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class moves the stage to gray card position while 
 * performing other task in parallel.
 * 
 * @author Cheah Lee Herng
 */
public class MoveStageToGrayCardPositionThreadTask extends ThreadTask<Object>
{
    private PanelPositioner _panelPositioner;
    private StagePositionMutable _stagePosition;
    private static Config _config = Config.getInstance();
    
    /**
     * @author Cheah Lee Herng
     */
    public MoveStageToGrayCardPositionThreadTask(OpticalCameraIdEnum cameraId)
    {
        super("Move Stage To Gray Card Position Thread Task");
        Assert.expect(cameraId != null);
        
        _panelPositioner = PanelPositioner.getInstance();
        // Define stage position
        _stagePosition = new StagePositionMutable();
        
        if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
        {
            _stagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS));
            _stagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS));
        }
        else if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
        {
            _stagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS));
            _stagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS));
        }
        else
            Assert.expect(false, "Invalid camera Id");
    }
    
    /*
     * @author Cheah Lee Herng
     */
    protected Object executeTask() throws Exception 
    {
        // Save off the previously loaded motion profile
        MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(_stagePosition);
        
        // Now that we are done with the move, restore the motion profile.
        _panelPositioner.setMotionProfile(originalMotionProfile);
        return null;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    protected void clearCancel()
    {
        // Do nothing.
    }

    /**
     * @author Cheah Lee Herng
    */
    protected void cancel() throws XrayTesterException
    {
        // Do nothing.
    }
}
