package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class represents all the hardware exceptions that are used by the
 * XraySource class.
 * The class is a factory that can return an instance of a specific type of xray
 * source exception.  The factory methods are only accessible in hardware package
 * since no class outside of hardware package should instanctiate.
 * @author Rex Shang
 */
public class XraySourceHardwareException extends HardwareException
{
  /**
   * The constructor is private so you have to use the static methods to create
   * one.
   * @author Rex Shang
   */
  private XraySourceHardwareException(XraySourceHardwareExceptionEnum xraySourceHardwareExceptionEnum, LocalizedString message)
  {
    super(message);

    _exceptionType = xraySourceHardwareExceptionEnum;
    Assert.expect(xraySourceHardwareExceptionEnum != null, "XraySourceHardwareExceptionEnum is null.");
    Assert.expect(message != null, "Localized string is null.");
  }

  /**
   * @author Rex Shang
   */
  private XraySourceHardwareException(XraySourceHardwareExceptionEnum xraySourceHardwareExceptionEnum, String key, Object[] parameters)
  {
    this(xraySourceHardwareExceptionEnum, new LocalizedString(key, parameters));
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationTimeoutException(String portName)
  {
    Assert.expect(portName != null, "portName is null.");

    String[] exceptionParameters = new String[] {portName};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_TIMEOUT,
                                           "HW_XRAY_SOURCE_SERIAL_COMMUNICATION_TIMEOUT_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationNoSuchPortException(String portName)
  {
    Assert.expect(portName != null, "portName is null.");

    String[] exceptionParameters = new String[] {portName};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_NO_SUCH_PORT,
                                           "HW_XRAY_SOURCE_SERIAL_COMMUNICATION_NO_SUCH_PORT_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationPortInUseException(String portName, String currentPortOwner)
  {
    Assert.expect(portName != null, "portName is null.");
    Assert.expect(currentPortOwner != null, "currentPortOwner is null.");

    String[] exceptionParameters = new String[] {portName, currentPortOwner};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_PORT_IN_USE,
                                           "HW_XRAY_SOURCE_SERIAL_COMMUNICATION_PORT_IN_USE_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationUnsupportedParametersException(String portName,
                                                                                          int baudRate,
                                                                                          int dataBits,
                                                                                          int stopBits,
                                                                                          String parity)
  {
    Assert.expect(portName != null, "portName is null.");
    Assert.expect(parity != null, "parity is null");
    Assert.expect(baudRate > 0, "baudRate is less than or equal to 0");
    Assert.expect(dataBits >= 5, "dataBits is less than 5");
    Assert.expect(dataBits <= 8, "dataBits is more than 8");
    Assert.expect(stopBits >= 1, "stopBits is less than 1");
    Assert.expect(stopBits <= 3, "stopBits is more than 3");

    Object[] exceptionParameters = new Object[] {portName, new Integer(baudRate), new Integer(dataBits), new Integer(stopBits), parity};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_UNSUPPORTED_PARAMETERS,
                                           "HW_XRAY_SOURCE_UNSUPPORTED_SERIAL_COMMUNICATION_PARAMETERS_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationUnsupportedReceiveTimeoutException(String portName,
                                                                                              int timeoutInMilliSeconds)
  {
    Assert.expect(portName != null, "portName is null.");
    Assert.expect(timeoutInMilliSeconds > 0, "timeoutInMilliSeconds is less than or equal to 0");

    Object[] exceptionParameters = new Object[] {portName, new Integer(timeoutInMilliSeconds)};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_UNSUPPORTED_RECEIVE_TIMEOUT,
                                           "HW_XRAY_SOURCE_UNSUPPORTED_SERIAL_COMMUNICATION_RECEIVE_TIMEOUT_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationErrorException(String portName)
  {
    Assert.expect(portName != null, "portName is null.");

    String[] exceptionParameters = new String[] {portName};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_ERROR,
                                           "HW_XRAY_SOURCE_SERIAL_COMMUNICATION_ERROR_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getSerialCommunicationProtocolHardwareException(String portName,
                                                                                     String expectedReply,
                                                                                     String actualReply)
  {
    Assert.expect(portName != null, "portName is null.");
    Assert.expect(expectedReply != null, "expectedReply is null");
    Assert.expect(actualReply != null, "actualReply is null");

    String[] exceptionParameters = new String[] {portName, expectedReply, actualReply};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.SERIAL_COMMUNICATION_PROTOCOL_ERROR,
                                           "HW_XRAY_SOURCE_SERIAL_COMMUNICATION_PROTOCOL_ERROR_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  static XraySourceHardwareException getLeakageCurrentExceededLimitException(double allowableLeakageCurrent,
                                                                             double actualLeakageCurrent)
  {
    Object[] exceptionParameters = new Object[] {allowableLeakageCurrent, actualLeakageCurrent};
    return new XraySourceHardwareException(XraySourceHardwareExceptionEnum.LEAKAGE_CURRENT_EXCEEDED_LIMIT,
                                           "HW_XRAY_SOURCE_LEAKAGE_CURRENT_EXCEPTION_KEY",
                                           exceptionParameters);
  }

  /**
   * @author Rex Shang
   */
  public String getLocalizedMessage()
  {
    String header = StringLocalizer.keyToString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY");
    String messageBody = StringLocalizer.keyToString(getLocalizedString());
    String footer = StringLocalizer.keyToString("HW_XRAY_SOURCE_HARDWARE_EXCEPTION_FOOTER_KEY");
    String companyContactInformation = StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY");

    String finalMessage = header + messageBody + footer + companyContactInformation;
    Assert.expect(finalMessage != null, "Exception message is null.");
    return finalMessage;
  }

}
