package com.axi.v810.hardware;

/**
 * @author Rex Shang
 */
public class ImageReconstructionProcessorApplicationVersion
{
  private int _versionId;

  /**
   * @author Rex Shang
   */
  public ImageReconstructionProcessorApplicationVersion(int versionId)
  {
    _versionId = versionId;
  }

  /**
   * @author Rex Shang
   */
  public int getVersionId()
  {
    return _versionId;
  }
}
