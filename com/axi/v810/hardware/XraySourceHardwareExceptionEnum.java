package com.axi.v810.hardware;



/**
 * @author Rex Shang
 */
public class XraySourceHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_ERROR = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_TIMEOUT = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_NO_SUCH_PORT = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_PORT_IN_USE = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_PROTOCOL_ERROR = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_UNSUPPORTED_PARAMETERS = new XraySourceHardwareExceptionEnum(++_index);
  public static XraySourceHardwareExceptionEnum SERIAL_COMMUNICATION_UNSUPPORTED_RECEIVE_TIMEOUT = new XraySourceHardwareExceptionEnum(++_index);

  public static XraySourceHardwareExceptionEnum LEAKAGE_CURRENT_EXCEEDED_LIMIT = new XraySourceHardwareExceptionEnum(++_index);

  /**
   * @author Rex Shang
   */
  private XraySourceHardwareExceptionEnum(int id)
  {
    super(id);
  }

}
