package com.axi.v810.hardware;

import java.io.*;

/**
 * This class keeps track of the loaded panel information.  It is serialized
 * and deserialized when panel is loaded and unloaded.  Please keep that in
 * mind when it comes to change the variables of this class.
 * @author Rex Shang
 */
public class PanelHandlerPersistentData implements Serializable
{
  private String _name;
  private int _widthInNanometers;
  private int _lengthInNanometers;
  private boolean _panelLoaded;
  private boolean _panelIsAgainstLeftPanelInPlaceSensor;
  private boolean _panelIsAgainstRightPanelInPlaceSensor;
  private boolean _panelIsLoading;
  private int _panelExtraClearDelayInMiliSeconds;

  /**
   * @author Rex Shang
   */
  PanelHandlerPersistentData()
  {
    _name = new String("");
    _lengthInNanometers = 0;
    _widthInNanometers = 0;
    _panelLoaded = false;
    _panelIsAgainstLeftPanelInPlaceSensor = false;
    _panelIsAgainstRightPanelInPlaceSensor = false;
    _panelIsLoading = false;
    _panelExtraClearDelayInMiliSeconds = 0;
  }

  /**
   * @author Rex Shang
   */
  void setPanelName(String name)
  {
    _name = name;
  }

  /**
   * @author Rex Shang
   */
  void setLengthInNanometers(int lengthInNanometers)
  {
    _lengthInNanometers = lengthInNanometers;
  }

  /**
   * @author Rex Shang
   */
  void setWidthInNanometers(int widthInNanometers)
  {
    _widthInNanometers = widthInNanometers;
  }

  /**
   * @author Rex Shang
   */
  void setPanelLoaded(boolean panelLoaded)
  {
    _panelLoaded = panelLoaded;
  }

  /**
   * @author Rex Shang
   */
  void setPanelIsAgainstLeftPanelInPlaceSensor(boolean status)
  {
    _panelIsAgainstLeftPanelInPlaceSensor = status;
  }

  /**
   * @author Rex Shang
   */
  void setPanelIsAgainstRightPanelInPlaceSensor(boolean status)
  {
    _panelIsAgainstRightPanelInPlaceSensor = status;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  void setPanelIsLoading(boolean status)
  {
    _panelIsLoading = status;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  void setPanelExtraClearDelayInMiliSeconds(int extraDelays)
  {
    _panelExtraClearDelayInMiliSeconds = extraDelays;
  }

  /**
   * @author Rex Shang
   */
  public String getPanelName()
  {
    return _name;
  }

  /**
   * @author Rex Shang
   */
  public int getLengthInNanometers()
  {
    return _lengthInNanometers;
  }

  /**
   * @author Rex Shang
   */
  public int getWidthInNanometers()
  {
    return _widthInNanometers;
  }

  /**
   * @author Rex Shang
   */
  public boolean isPanelLoaded()
  {
    return _panelLoaded;
  }

  /**
   * @author Rex Shang
   */
  public boolean isPanelIsAgainstLeftPanelInPlaceSensor()
  {
    return _panelIsAgainstLeftPanelInPlaceSensor;
  }

  /**
   * @author Rex Shang
   */
  public boolean isPanelIsAgainstRightPanelInPlaceSensor()
  {
    return _panelIsAgainstRightPanelInPlaceSensor;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean isPanelLoading()
  {
    return _panelIsLoading;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public int getPanelExtraClearDelayInMiliSeconds()
  {
    return _panelExtraClearDelayInMiliSeconds;
  }

  /**
   * @author Rex Shang
   */
  public boolean equals(Object obj)
  {
    boolean isEqual = false;

    if (super.equals(obj))
      isEqual = true;
    else
    {
      if (obj instanceof PanelHandlerPersistentData)
      {
        PanelHandlerPersistentData compareTo = (PanelHandlerPersistentData) obj;

        if (_name.compareToIgnoreCase(compareTo._name) == 0
            && _panelLoaded == compareTo._panelLoaded
            && _widthInNanometers == compareTo._widthInNanometers
            && _lengthInNanometers == compareTo._lengthInNanometers
            && _panelIsAgainstLeftPanelInPlaceSensor == compareTo._panelIsAgainstLeftPanelInPlaceSensor
            && _panelIsAgainstRightPanelInPlaceSensor == compareTo._panelIsAgainstRightPanelInPlaceSensor
            && _panelIsLoading == compareTo._panelIsLoading
            && _panelExtraClearDelayInMiliSeconds == compareTo._panelExtraClearDelayInMiliSeconds)
          isEqual = true;
      }
    }

    return isEqual;
  }

}
