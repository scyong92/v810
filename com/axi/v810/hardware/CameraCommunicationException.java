package com.axi.v810.hardware;

import com.axi.util.LocalizedString;

/**
 * @author Roy Williams
 */
public class CameraCommunicationException extends HardwareException
{

  /**
   * @author Roy Williams
   */
  public CameraCommunicationException(LocalizedString localizedString)
  {
    super(localizedString);
  }
}
