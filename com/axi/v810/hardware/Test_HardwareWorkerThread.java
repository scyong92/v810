package com.axi.v810.hardware;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_HardwareWorkerThread extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_HardwareWorkerThread());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      final HardwareWorkerThread hardwareWorkerThread = HardwareWorkerThread.getInstance();

      Expect.expect(hardwareWorkerThread.isHardwareWorkerThread() == false);
      hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run()
        {
          Expect.expect(hardwareWorkerThread.isHardwareWorkerThread());
          System.out.println("HardwareWorkerThread test complete");
        }
      });
      // sleep long enough for the run above to have a chance to get called
      Thread.sleep(2000);
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }

  }
}