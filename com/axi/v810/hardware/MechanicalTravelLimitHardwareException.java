package com.axi.v810.hardware;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * @author Roy Williams
 */
public class MechanicalTravelLimitHardwareException extends HardwareException
{
  /**
   * @author Roy Williams
   */
  public MechanicalTravelLimitHardwareException()
  {
    super(new LocalizedString("HW_MECHANICAL_TRAVEL_LIMIT_EXCEPTION_KEY",
                              new Object[]{}));
  }
}
