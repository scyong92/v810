package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class SetRailWidthThreadTask extends ThreadTask<Object>
{
  private PanelHandler _panelHandler;
  private int _panelWidthInNanoMeters;

  /**
   * @author Bill Darbie
   */
  public SetRailWidthThreadTask(int panelWidthInNanoMeters)
  {
    super("Move stage to load position thread task");

    _panelHandler = PanelHandler.getInstance();
    _panelWidthInNanoMeters = panelWidthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws Exception
  {
    _panelHandler.setRailWidthForPanel(_panelWidthInNanoMeters);
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
