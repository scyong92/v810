package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * @author Greg Esparza
 * Swee Yee Wong - remove assert.expect the same parameters received from camera
 * Sometimes we will receive different parameters, instead of prompting internal error, we should display the error
 */
public class XrayCameraHardwareExceptionEnum extends HardwareExceptionEnum
{
  private static int _index = -1;

  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_FLASH_MODULE_TYPE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get Flash Module type");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_POWER_STATUS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get power status");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_NOT_POWERED_ON = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera not powered on");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_BOARD_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed tp get board version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_BOARD_VERSION_MISMATCH = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera board version mismatch");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_UNSUPPORTED_BOARD_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera unsupported board version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_CORE_RUNTIME_FILE_UPDATE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed core runtime file update");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_SET_UP_RUNTIME_FILE_INFORMATION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to set up runtime file information");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_EXECUTE_COMMAND = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to execute command");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_BYTE_DATA = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get byte data");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_CONNECT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to connect");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_DISCONNECT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to disconnect");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_GET_COMMUNICATION_PING_STATUS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get communication ping status");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_COMMUNICATION_PING = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed communication ping");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_RELEASE_AVAILABLE_UNIT_TEST_PORT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to release available unit test port");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_FIND_AVAILABLE_UNIT_TEST_PORT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to find available unit test port");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_SET_UNIT_TEST_SOCKET = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to set unit test socket");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_FIND_UNIT_TEST_SIMULATOR_CONFIGURATION_FILE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to find unit test simulator configuration file");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_START_UNIT_TEST_SIMULATOR = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to start unit test simulator");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_LINE_SCAN_IMAGE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get line scan image");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_REBOOT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to reboot");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_FREE_IMAGE_BUFFER_STATUS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get free image buffer status");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_IMAGE_INFORMATION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get image information");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_GET_CAMERA_MODE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to get camera mode");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_TIMED_OUT_WAITING_FOR_IMAGE_INFORMATION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera timeout waiting for image information");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_TEMPERATURE_VALUE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid temperature value");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_APPLICATION_FAILED_TO_START = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to start application");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_APPLICATION_FAILED_TO_STOP = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to stop application");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_DRIVER_FAILED_TO_START = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to start driver");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_DRIVER_FAILED_TO_STOP = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to stop driver");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_RUNTIME_FILE_CHECKSUM = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid runtime file checksum");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FAILED_TO_UPDATE_RUNTIME_FILE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed to update runtime file");
  //Swee Yee Wong - new camera development
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_PRELOADER_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid PreLoader version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_BOOTLOADER_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid BootLoader version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_KERNEL_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid Kernel version");
  //Swee Yee Wong - new camera development
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_DEVICE_TREE_BLOB_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid Device Tree Blob version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_FILE_SYSTEM_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid file system version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_FILE_SYSTEM_FLASH_MODULE_NOT_SUPPORTED = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera file system flash module not supported");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_APPLICATION_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid application version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_DRIVER_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid driver version");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_FPGA_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid FPGA version");
  //Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IPSCRIPT_VERSION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid ip script version");
  
  public static XrayCameraHardwareExceptionEnum AXI_TDI_LOCAL_RUNTIME_FILE_DOES_NOT_EXIST = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera local runtime file does not exist");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_SEGMENT_DATA_VALUE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid segment data value");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_PIXEL_DATA_VALUE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid pixel data value");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IMAGE_INFORMATION_IMAGE_NUMBER = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid image information image number");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IMAGE_INFORMATION_LEFT_POSITION_IN_PIXELS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid image information left position in pixels");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IMAGE_INFORMATION_RIGHT_POSITION_IN_PIXELS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid image information right position in pixels");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IMAGE_INFORMATION_TOP_POSITION_IN_PIXELS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid image information top position in pixels");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_IMAGE_INFORMATION_BOTTOM_POSITION_IN_PIXELS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid image information bottom position in pixels");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_INVALID_NUMBER_OF_LINE_SCAN_IMAGES_ACQUIRED_VALUE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid number of line scan images acquired value");
  public static XrayCameraHardwareExceptionEnum CAMERA_ARRAY_CAMERAS_NOT_READY_FOR_ACQUISITION = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera array not ready for acquisition");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_GRAYSCALE_ADJUSTMENT_REQUIRED = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera grayscale adjustment required");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_INSUFFICIENT_TRIGGERS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera received insufficient triggers");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_INVALID_COMMAND = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid command received");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_INVALID_COMMAND_PARAMETER = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid command parameter received");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_INVALID_SEQUENCE = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera invalid sequence");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_FAILED_PIXEL_GAIN_ADJUSTMENT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed pixel gain adjustment");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_FAILED_PIXEL_OFFSET_ADJUSTMENT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed pixel offset adjustment");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_FAILED_SEGMENT_GAIN_ADJUSTMENT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed segment gain adjustment");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_FAILED_SEGMENT_OFFSET_ADJUSTMENT = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera failed segment offset adjustment");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_SYSTEM_ERROR = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera system error");
  public static XrayCameraHardwareExceptionEnum AXI_TDI_EMBEDDED_UNEXPECTED_TRIGGERS = new XrayCameraHardwareExceptionEnum(++_index, "AXI TDI Camera unexpected triggers received");

  private String _name;


  /**
   * @author Greg Esparza
   */
  private XrayCameraHardwareExceptionEnum(int id, String name)
  {
    super(id);
    _name = name;
  }
  
  public String toString()
  {
    Assert.expect(_name != null);
    return _name;
  }
}
