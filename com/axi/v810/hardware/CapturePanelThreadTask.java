package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class CapturePanelThreadTask extends ThreadTask<Object>
{
  private PanelClamps _panelClamps;

  /**
   * @author Bill Darbie
   */
  CapturePanelThreadTask()
  {
    super("Capture panel thread task");

    _panelClamps = PanelClamps.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  protected Object executeTask() throws XrayTesterException
  {
    _panelClamps.close();
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    _panelClamps.abort();
  }
}
