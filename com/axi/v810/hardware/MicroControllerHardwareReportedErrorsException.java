package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MicroControllerHardwareReportedErrorsException extends HardwareException
{
    private static final String[] _errorKeys = new String[] {
    };

    private int _statusBitField;

    /**
     * @author Cheah Lee Herng
     */
    public MicroControllerHardwareReportedErrorsException(int statusBitField)
    {
        super (new LocalizedString("HW_MICROCONTROLLER_HARDWARE_REPORTED_ERRORS_KEY", null));
        Assert.expect(statusBitField != 1);
        _statusBitField = statusBitField;
    }

    /**
     * concatenates together all of the various individual error messages that may be
     *  represented by the statusBitField given to the ctor<p/>
     *
     * @author Cheah Lee Herng
     */
    public String getLocalizedMessage()
    {
        StringBuffer sb = new StringBuffer();

        sb.append(super.getLocalizedMessage());

        for (int i = 0; i < _errorKeys.length; i++)
        {
          int mask = 0x1 << i;
          if ((_statusBitField & mask) != 0)
          {
            String errorKey = _errorKeys[i];
            String error = StringLocalizer.keyToString(errorKey);

            sb.append(" ");
            sb.append(error);
          }
        }

        String result = sb.toString();
        return result;
    }
}
