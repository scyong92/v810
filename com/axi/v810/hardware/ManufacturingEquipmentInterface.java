package com.axi.v810.hardware;

import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * The class provides an interface for surface mount equipment to communicate.
 * It supports the standard SMEMA interface and extended signals such as
 * "good board out (GBO)".
 * SMEMA stands for Surface Mount Equipment Messaging Association
 * @author Rex Shang
 */
public class ManufacturingEquipmentInterface extends HardwareObject
{
  private static ManufacturingEquipmentInterface _instance;
  private DigitalIo _digitalIo;
  private HardwareObservable _hardwareObservable;
  private WaitForHardwareCondition _waitForHardwareCondition;
  
  private static final long _SETTLING_TIME_FOR_SENSOR_TO_CLEAR_IN_MILLI_SECONDS = 200;
  public static final long _WAIT_FOR_SMEMA_CLEAR_TIME_OUT_IN_MILLI_SECONDS = 5000;

  /**
   * This contructor is not public on purpose.
   * @author Rex Shang
   */
  private ManufacturingEquipmentInterface()
  {
    _instance = null;
    _digitalIo = DigitalIo.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
  }

  /**
   * @return an instance of this class.
   * @author Rex Shang
   */
  public static synchronized ManufacturingEquipmentInterface getInstance()
  {
    if (_instance == null)
    {
      _instance = new ManufacturingEquipmentInterface();
    }

    return _instance;
  }

  /**
   * Determines if the panel from the upstream system is ready
   * to be loaded into the xray tester.
   * @author Rex Shang
   */
  public boolean isPanelFromUpstreamSystemAvailable() throws XrayTesterException
  {
    return _digitalIo.isSmemaPanelFromUpstreamSystemAvailable();
  }

  /**
   * Wait for the upstream system to give us a panel.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param abortable to indicate whether the wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilPanelFromUpstreamSystemIsAvailable(long timeOutInMilliSeconds, boolean abortable) throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);

    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logStartWaitUntilPanelFromUpstreamSystemIsAvailable();
    
    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        return isPanelFromUpstreamSystemAvailable();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        try
        {
          // XCR-3116 Intermittent board chop during production looping using SMEMA
          SmemaLogUtil.getInstance().logWaitUntilPanelFromUpstreamSystemIsAvailableTimeOut();
        }
        catch(XrayTesterException ex)
        {
          // Do nothing
        }
        
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_PANEL_FROM_UPSTREAM_SYSTEM);
      }
    };
    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logCompleteWaitUntilPanelFromUpstreamSystemIsAvailable();
  }

  /**
   * Set the bit that says that the xray tester is ready to accept a new panel.
   * @author Rex Shang
   */
  public void setXrayTesterIsReadyToAcceptPanel(boolean readyToAcceptPanel) throws XrayTesterException
  {
    if (readyToAcceptPanel)
    {
      _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.MACHINE_READY_TO_ACCEPT_PANEL);
      _hardwareObservable.setEnabled(false);

      try
      {
        _digitalIo.setSmemaXrayTesterIsReadyToAcceptPanel();
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.MACHINE_READY_TO_ACCEPT_PANEL);
      
      // XCR-3116 Intermittent board chop during production looping using SMEMA
      SmemaLogUtil.getInstance().logXraySystemReadyToAcceptPanel();
    }
    else
    {
      _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.MACHINE_NOT_READY_TO_ACCEPT_PANEL);
      _hardwareObservable.setEnabled(false);

      try
      {
        _digitalIo.setSmemaXrayTesterIsNotReadyToAcceptPanel();
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.MACHINE_NOT_READY_TO_ACCEPT_PANEL);
      
      // XCR-3116 Intermittent board chop during production looping using SMEMA
      SmemaLogUtil.getInstance().logXraySystemNotReadyToAcceptPanel();
    }
  }

  /**
   * Determines if the xray tester is ready to accept a panel from the
   * upstream system.
   * @author Rex Shang
   */
  public boolean isXrayTesterReadyToAcceptPanel() throws XrayTesterException
  {
    return _digitalIo.isSmemaXrayTesterReadyToAcceptPanel();
  }

  /**
   * Set the bit that says that the panel from the xray tester is available.
   * @author Rex Shang
   */
  public void setPanelFromXrayTesterAvailable(boolean panelAvailable) throws XrayTesterException
  {
    if (panelAvailable)
    {
      _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_AVAILABLE);
      _hardwareObservable.setEnabled(false);

      try
      {
        _digitalIo.setSmemaPanelFromXrayTesterAvailable();
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_AVAILABLE);
      
      // XCR-3116 Intermittent board chop during production looping using SMEMA
      SmemaLogUtil.getInstance().logPanelAvailableFromXrayTester();
    }
    else
    {
      _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_NOT_AVAILABLE);
      _hardwareObservable.setEnabled(false);

      try
      {
        _digitalIo.setSmemaPanelFromXrayTesterNotAvailable();
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_NOT_AVAILABLE);
      
      // XCR-3116 Intermittent board chop during production looping using SMEMA
      SmemaLogUtil.getInstance().logPanelNotAvailableFromXrayTester();
    }
  }

  /**
   * Determines if the panel in the xray tester is available for the downstream
   * system.
   * @author Rex Shang
   */
  public boolean isPanelFromXrayTesterAvailable() throws XrayTesterException
  {
    return _digitalIo.isSmemaPanelFromXrayTesterAvailable();
  }

  /**
   * Determines if the downstream system is ready for the panel coming
   * out of the xray tester.
   * @author Rex Shang
   */
  public boolean isDownstreamSystemReadyToAcceptPanel() throws XrayTesterException
  {
    return _digitalIo.isSmemaDownstreamSystemReadyToAcceptPanel();
  }

  /**
   * Wait for the downstream system to accept our panel.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param abortable indicates whether the wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilDownstreamSystemReadyToAcceptPanel(long timeOutInMilliSeconds, boolean abortable)
      throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);

    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logStartWaitUntilDownstreamSystemReadyToAcceptPanel();
    
    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        return isDownstreamSystemReadyToAcceptPanel();
      }

      XrayTesterException getHardwareTimeoutException()
      {
        try
        {
          // XCR-3116 Intermittent board chop during production looping using SMEMA
          SmemaLogUtil.getInstance().logWaitUntilDownstreamSystemReadyToAcceptPanelTimeOut();
        }
        catch(XrayTesterException ex)
        {
          // Do nothing
        }
        
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL);
      }
    };
    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logCompleteWaitUntilDownstreamSystemReadyToAcceptPanel();
  }

  /**
   * Once we started sending panel to downstream system, we have to wait for it
   * to acknowledge it has received the panel.
   * Note: make sure time out is longer than the hardware polling interval.
   * @see com.axi.v810.hardware.DigitalIo#getHardwarePollingIntervalInMilliSeconds() HardwarePollingInterval
   * @param timeOutInMilliSeconds long time in milli seconds.  0 indicate no time out.
   * @param abortable indicates whether the wait can be aborted.
   * @author Rex Shang
   */
  public void waitUntilDownstreamSystemNotReadyToAcceptPanel(long timeOutInMilliSeconds, boolean abortable)
      throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);

    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logStartWaitUntilDownstreamSystemNotReadyToAcceptPanel();
    
    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean systemIsNotReady = (isDownstreamSystemReadyToAcceptPanel() == false);
        return systemIsNotReady;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        try
        {
          // XCR-3116 Intermittent board chop during production looping using SMEMA
          SmemaLogUtil.getInstance().logWaitUntilDownstreamSystemNotReadyToAcceptPanelTimeOut();
        }
        catch(XrayTesterException ex)
        {
          // Do nothing
        }
        
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SYSTEM_TO_ACCEPT_PANEL);
      }
    };
    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logCompleteWaitUntilDownstreamSystemNotReadyToAcceptPanel();
  }

  /**
   * Set panel state to passed (and tested)
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public void setPanelOutPassed() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_PASSED);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.setPanelTestResultPass();
      _digitalIo.setPanelTestResultTested();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_PASSED);
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logPanelOutPassed();
  }

  /**
   * Set panel state to failed (and tested)
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public void setPanelOutFailed() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_FAILED);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.setPanelTestResultFail();
      _digitalIo.setPanelTestResultTested();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_FAILED);
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logPanelOutFailed();
  }

  /**
   * Set panel state to Untested (and failed)
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public void setPanelOutUntested() throws XrayTesterException
  {
    _hardwareObservable.stateChangedBegin(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_UNTESTED);
    _hardwareObservable.setEnabled(false);

    try
    {
      _digitalIo.setPanelTestResultFail();
      _digitalIo.setPanelTestResultNotTested();
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, ManufacturingEquipmentInterfaceEventEnum.PANEL_OUT_UNTESTED);
    
    // XCR-3116 Intermittent board chop during production looping using SMEMA
    SmemaLogUtil.getInstance().logPanelOutUntested();
  }

  /**
   * If the panel has been tested and it passed this method returns true
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public boolean isPanelOutPass() throws XrayTesterException
  {
    return (_digitalIo.isPanelTestResultPass() && (_digitalIo.isPanelTestResultTested()));
  }

  /**
   * If the panel has been tested and it failed this method returns true
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public boolean isPanelOutFail() throws XrayTesterException
  {
    return ((_digitalIo.isPanelTestResultPass() == false) && (_digitalIo.isPanelTestResultTested()));
  }

  /**
   * If the panel coming out of the xray tester has been tested this returns true.
   * NOTE: This is NOT part of standard SMEMA interface.
   * @author Rex Shang
   */
  public boolean isPanelOutUntested() throws XrayTesterException
  {
    return (_digitalIo.isPanelTestResultTested() == false);
  }

  /**
   * Abort the wait.
   * @author Rex Shang
   */
  public void abort() throws XrayTesterException
  {
    super.abort();
    if (_waitForHardwareCondition != null)
      _waitForHardwareCondition.abort();
  }

  /**
   * XCR-3565 - wait panel clear sensor and downstream sensor 1 clear
   * @author Kok Chun, Tan
   */
  public void waitUntilDownstreamSensorSignalClear(long timeOutInMilliSeconds, boolean abortable, final PanelClearSensor panelClearSensor) throws XrayTesterException
  {
    Assert.expect(timeOutInMilliSeconds >= 0);
    
    SmemaLogUtil.getInstance().logStartWaitUntilDownstreamSensorSignalClear();

    _waitForHardwareCondition = new WaitForHardwareCondition(timeOutInMilliSeconds,
                                                             _SETTLING_TIME_FOR_SENSOR_TO_CLEAR_IN_MILLI_SECONDS,
                                                             abortable)
    {
      boolean getCondition() throws XrayTesterException
      {
        boolean isCleared = ((panelClearSensor.isBlocked() == false) && (isDownstreamSensorOneBlocked() == false));
        return isCleared;
      }

      XrayTesterException getHardwareTimeoutException()
      {
        return new HardwareTimeoutException(HardwareTimeoutExceptionEnum.WAIT_FOR_DOWNSTREAM_SENSOR_SIGNAL_CLEAR);
      }
    };

    _waitForHardwareCondition.waitUntilConditionIsMet();
    _waitForHardwareCondition = null;
    
    SmemaLogUtil.getInstance().logCompleteWaitUntilDownstreamSensorSignalClear();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isDownstreamSensorOneBlocked() throws XrayTesterException
  {
    return _digitalIo.isSmemaSensorOneTrigger();
  }
}
