#include <iostream>

#include "com/axi/util/jniUtil.h"
#include "com_axi_v810_hardware_DigitalIo.h"
#include "cpp/hardware/util/HardwareConfigurationDescription.h"
#include "cpp/hardware/digitalIo/DigitalIo.h"
#include "cpp/util/src32/sptAssert.h"
#include "cpp/util/src32/HardwareException.h"

using namespace std;

DigitalIo* _pDigitalIo = 0;
static string const javaHardwareException = "com/axi/v810/hardware/NativeHardwareException";

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeInitalize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeInitialize(JNIEnv *pEnv, jobject javaObject)
{
  try
  {
    try
    {
      if (_pDigitalIo == 0)
      {
        _pDigitalIo = DigitalIo::getInstance();
      }
      
      _pDigitalIo->initialize();
    }
    catch (HardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeShutdown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeShutdown(JNIEnv *pEnv, jobject javaObject)
{
  try
  {
    try
    {
      if (_pDigitalIo != 0)
      {
        _pDigitalIo->shutdown();
      }
    }
    catch (HardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeSetHardwareResetMode
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeSetHardwareResetMode(JNIEnv *pEnv, jobject javaObject, jboolean hardwareResetModeOn)
{
  try
  {
    try
    {
      if (_pDigitalIo == 0)
      {
        _pDigitalIo = DigitalIo::getInstance();
      }
      
    _pDigitalIo->setHardwareResetMode(hardwareResetModeOn);
    }
    catch (HardwareException& he)
    {
      throwJniException(pEnv,
                        he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeSetOutputBitState
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeSetOutputBitState(JNIEnv *pEnv, jobject javaObject, jint bitPosition, jint bitState)
{
  try
  {
    try
    {
      sptAssert(_pDigitalIo);
      _pDigitalIo->setBit(bitPosition, bitState);
    }
    catch (HardwareException& he)
    {
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeGetInputBitState
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeGetInputBitState(JNIEnv *pEnv, jobject javaObject, jint bitPosition)
{
  try
  {
    int bitState = 0;
    
    try
    {
      sptAssert(_pDigitalIo);
      bitState = _pDigitalIo->getBit(bitPosition);
    }
    catch (HardwareException& he)
    {
      throwJniException(pEnv, 
                      he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
    
    return bitState;
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
    return 0;
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
    return 0;
  }
}

/*
 * Class:     com_axi_v810_hardware_DigitalIo
 * Method:    nativeGetConfigurationDescription
 * Signature: (Ljava/lang/StringBuffer;Ljava/util/List;)V
 */
JNIEXPORT void JNICALL Java_com_axi_v810_hardware_DigitalIo_nativeGetConfigurationDescription(JNIEnv* pEnv, 
                                                                                                      jobject javaObject, 
                                                                                                      jobject javaKey, 
                                                                                                      jobject javaParameters)
{
  try
  {
    bool exceptionThrown = false;
    
    string message;
    string key;
    list<string> parameters;
    HardwareConfigurationDescription configDescription(message, key, parameters); 
    
    try
    {
      configDescription = _pDigitalIo->getConfigurationDescription();
    }
    catch (HardwareException& he)
    {
      exceptionThrown = true;
      
      throwJniException(pEnv, 
                        he.getKey(), 
                        he.getParameters(), 
                        javaHardwareException);
    }
    
    if (exceptionThrown == false)
    {
      setJavaStringBuffer(pEnv, javaKey, configDescription.getKey());
      loadJavaStringArrayFromStlStringList(pEnv, javaParameters, configDescription.getParameters());
    }
  }
  catch (AssertException& aex)
  {
    raiseJavaAssertWithMessage(aex.getMessage());
  }
  catch (...)
  {
    raiseJavaAssertForUnhandledException(__FILE__, __LINE__);
  }
}

