package com.axi.v810.hardware;

import java.util.*;

import com.axi.util.*;

/**
 * This class contains the information needed for a single scan pass.  A scan pass is defined as a line (in nm)
 * along which the stage moves where image data is actually being acquired--as well as the scan step.
 * NOTE: The scan pass line does NOT include the distance required to accelerate and decelerate the stage.
 *
 * @author Matt Wharton
 */
public class ScanPass implements Comparable
{
  public static final int DEFAULT_TEN_MICRON_SCAN_STEP_IN_NANOMETERS = 10160000; // 0.40 inches
  public static final int DEFAULT_SIXTEEN_MICRON_SCAN_STEP_IN_NANOMETERS = 16510000; // 0.65 inches

  private int _id = -1;
  private String _code = "Z"; // XCR1529, New Scan Route, khang-wah.chnee
  private StagePosition _startPointInNanoMeters = null;
  private StagePosition _endPointInNanoMeters = null;
  private ScanPath _scanPath;
  private MechanicalConversions _mechanicalConversions = null; // XCR1529, New Scan Route, khang-wah.chnee

  private ProjectionTypeEnum _projectionTypeEnum =  ProjectionTypeEnum.INSPECTION_AND_ALIGNMENT;
  
  private boolean _coversSystemFiducial = false;
  private boolean _coversAlignmentRegions = false;
  private boolean _coversPSHRegions = false; // XCR1529, New Scan Route, khang-wah.chnee
  
  private double _stageSpeed = 0;
  private double _userGain = 0;
  private boolean _isLowMagnification = true; 
  
  private Map<ScanPassBreakPointEnum, ScanPassBreakPoint> _scanPassBreakPoints = new TreeMap<ScanPassBreakPointEnum, ScanPassBreakPoint>();
  
  /**
   * @author Matt Wharton
   */
  public ScanPass(int id, 
                  StagePosition startPointInNanoMeters, 
                  StagePosition endPointInNanoMeters)
  {
    this(id, 
         "Z", 
         startPointInNanoMeters, 
         endPointInNanoMeters);
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public ScanPass(int id, 
                  String code, 
                  StagePosition startPointInNanoMeters, 
                  StagePosition endPointInNanoMeters)
  {
    Assert.expect(id >= 0);
    Assert.expect(startPointInNanoMeters != null);
    Assert.expect(endPointInNanoMeters != null);

    _id = id;
    _code = code;
    _startPointInNanoMeters = startPointInNanoMeters;
    _endPointInNanoMeters = endPointInNanoMeters;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public ScanPass(int id, 
                  String code, 
                  StagePosition startPointInNanoMeters, 
                  StagePosition endPointInNanoMeters, 
                  MechanicalConversions mechanicalConversions)
  {
    Assert.expect(id >= 0);
    Assert.expect(startPointInNanoMeters != null);
    Assert.expect(endPointInNanoMeters != null);

    _id = id;
    _code = code;
    _startPointInNanoMeters = startPointInNanoMeters;
    _endPointInNanoMeters = endPointInNanoMeters;
    _mechanicalConversions = mechanicalConversions;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public ScanPass(ScanPass rhs)
  {
    _id = rhs.getId();
    _code = rhs.getCode();
    _startPointInNanoMeters = rhs.getStartPointInNanoMeters();
    _endPointInNanoMeters = rhs.getEndPointInNanoMeters();
    _mechanicalConversions = rhs.getScanPassMechanicalConversions();
  }

  /**
   * @author Erica Wheatcroft
   */
  public ScanPass()
  {
    this(0, 
         "Z", 
         new StagePositionMutable(), 
         new StagePositionMutable());
  }

  /**
   * @author Matt Wharton
   */
  public int getId()
  {
    Assert.expect(_id >= 0);

    return _id;
  }


  /**
   * @author Matt Wharton
   */
  public void setId(int id)
  {
    Assert.expect(id >= 0);

    _id = id;
    if ((_id % 2) == 0)
    {
     directionOfTravel(ScanPassDirectionEnum.FORWARD);
    }
    else
    {
      directionOfTravel(ScanPassDirectionEnum.REVERSE);
    }
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public String getCode()
  {
    return _code;
  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void setCode(String code)
  {
    _code = code;
  }

  /**
   * Returns the StagePosition representing the start point of the scan pass.
   *
   * @author Matt Wharton
   */
  public StagePosition getStartPointInNanoMeters()
  {
    Assert.expect(_startPointInNanoMeters != null);

    return _startPointInNanoMeters;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setStartPointInNanoMeters(StagePosition startPointInNanometers)
  {
    Assert.expect(startPointInNanometers != null);

    _startPointInNanoMeters = startPointInNanometers;
  }

  /**
   * Returns the StagePosition representing the end point of the scan pass.
   *
   * @author Matt Wharton
   */
  public StagePosition getEndPointInNanoMeters()
  {
    Assert.expect(_endPointInNanoMeters != null);

    return _endPointInNanoMeters;
  }

  /**
    * Returns the StagePosition representing the end point of the scan pass.
    *
    * @author Matt Wharton
    */
   public void setEndPointInNanoMeters(StagePosition endPointInNanometers)
   {
     Assert.expect(endPointInNanometers != null);

     _endPointInNanoMeters = endPointInNanometers;
  }

  /**
   * Returns the distance the stage travels in the Y direction over the scan pass
   *
   * @author Dave Ferguson
   */
  public int getYScanDistanceInNanomters()
  {
    Assert.expect(_endPointInNanoMeters != null);
    Assert.expect(_startPointInNanoMeters != null);

    return Math.abs(_startPointInNanoMeters.getYInNanometers() -
                    _endPointInNanoMeters.getYInNanometers());
  }


  /**
   * Returns whether or not this ScanPass passes over the active system fiducual.
   *
   * @return true if this ScanPass passes over the active system fiducial, false otherwise.
   * @author Matt Wharton
   */
  public boolean getCoversSystemFiducial()
  {
    return _coversSystemFiducial;
  }


  /**
   * Returns whether or not this ScanPass passes over any alignment regions.
   *
   * @return true if this ScanPass passes over any alignment regions, false otherwise.
   * @author Matt Wharton
   */
  public boolean getCoversAlignmentRegions()
  {
    return _coversAlignmentRegions;
  }
  
  /**
   * Returns whether or not this ScanPass passes over any Global Surface (PSH) regions.
   *
   * @return true if this ScanPass passes over any Global Surface (PSH) regions, false otherwise.
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public boolean getCoversPSHRegions()
  {
    return _coversPSHRegions;
  }


  /**
   * Sets whether or not this ScanPass passes over the active system fiducual.
   *
   * @author Matt Wharton
   */
  void setCoversSystemFiducial(boolean coversSystemFiducial)
  {
    _coversSystemFiducial = coversSystemFiducial;
  }


  /**
   * Sets whether or not this ScanPass passes over any alignment regions.
   *
   * @author Matt Wharton
   */
  public void setCoversAlignmentRegions(boolean coversAlignmentRegions)
  {
    _coversAlignmentRegions = coversAlignmentRegions;
  }
  
  /**
   * Sets whether or not this ScanPass passes over any Global Surface (PSH) regions.
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void setCoversPSHRegions(boolean coversPSHRegions)
  {
    _coversPSHRegions = coversPSHRegions;
  }

  /**
   * After a ScanPath is reordered to cover alignment regions first the y travel
   * is not optimal for stage travel, so it will be forced to travel a more
   * optimal path.
   *
   * @author Roy Williams
   */
  public synchronized void directionOfTravel(ScanPassDirectionEnum direction)
  {
    ScanPassDirectionEnum currentStageDirection = getStageDirection();
    if (currentStageDirection.equals(direction))
      return;
    swapDirectionOfTravel();
  }

  /**
   * @author Roy Williams
   */
  private void swapDirectionOfTravel()
  {
    StagePosition temp = _startPointInNanoMeters;
    _startPointInNanoMeters = _endPointInNanoMeters;
    _endPointInNanoMeters = temp;
  }


  /**
   * Provides for 'natural' ordering based on scan pass id number.
   *
   * @author Matt Wharton
   */
  public int compareTo(Object rhs)
  {
    Assert.expect(rhs instanceof ScanPass, "ScanPasses should only be compared to other ScanPasses!");

    ScanPass rhsScanPass = (ScanPass)rhs;

    int lhsSpeed = (int)(_stageSpeed* 10);
    int rhsSpeed = (int)(rhsScanPass._stageSpeed* 10);
    
    if (lhsSpeed != rhsSpeed)
      return lhsSpeed - rhsSpeed;

    if (_userGain != rhsScanPass._userGain)
      return (int)(_userGain - rhsScanPass._userGain);
    
    int lhsIsLowMagScanPass = _isLowMagnification ? 1 :0;
    int rhsIsLowMagScanPass = rhsScanPass._isLowMagnification ? 1 :0;
    
    if (_isLowMagnification != rhsScanPass._isLowMagnification)
      return lhsIsLowMagScanPass - rhsIsLowMagScanPass;
    
    if (_id != rhsScanPass._id)
      return _id - rhsScanPass._id;
    
    return 0;
  }

  /**
   * @author Roy Williams
   */
  public ScanPassDirectionEnum getStageDirection()
  {
    if (_startPointInNanoMeters.getYInNanometers() <=
        _endPointInNanoMeters.getYInNanometers())
      return ScanPassDirectionEnum.FORWARD;
    return ScanPassDirectionEnum.REVERSE;
  }

  /**
   * @author Roy Williams
   */
  public boolean isStageDirectionForward()
  {
    if (getStageDirection().equals(ScanPassDirectionEnum.FORWARD))
      return true;
    return false;
  }

  /**
   * @author Roy Williams
   */
  public void setProjectionType(ProjectionTypeEnum projectionTypeEnum)
  {
    Assert.expect(projectionTypeEnum != null);

    _projectionTypeEnum = projectionTypeEnum;
  }

  /**
   * @author Roy Williams
   */
  public ProjectionTypeEnum getProjectionType()
  {
    return _projectionTypeEnum;
  }

  /**
   * @author Roy Williams
   */
  public ScanPath getScanPath()
  {
    Assert.expect(_scanPath != null);

    return _scanPath;
  }

  /**
   * @author Roy Williams
   */
  public void setScanPath(ScanPath scanPath)
  {
    Assert.expect(scanPath != null);

    _scanPath = scanPath;
  }

  /**
   * @author Roy Williams
   */
  public MechanicalConversions getMechanicalConversions()
  {
    Assert.expect(_scanPath != null);
	// XCR1529, New Scan Route, khang-wah.chnee
    if(hasScanPassMechanicalConversions()==true)
    {
      return getScanPassMechanicalConversions();
    }
    
    return _scanPath.getMechanicalConversions();
  }

  /**
  * @author XCR1529, New Scan Route, khang-wah.chnee
  */
  public MechanicalConversions getScanPassMechanicalConversions()
  {
    Assert.expect(_mechanicalConversions != null);

    return _mechanicalConversions;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void setUserGainValue(double userGainEnum)
  {
    Assert.expect(userGainEnum > 0);
    _userGain = userGainEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public double getUserGainValue()
  {
    return _userGain;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void setIsLowMagnification(boolean isLowMagnification)
  {
    _isLowMagnification = isLowMagnification;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean isLowMagnification()
  {
    return _isLowMagnification;
  }
  /**
   * @author Yong Sheng Chuan
   */
  public void setStageSpeedSetting(double stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum > 0);
    _stageSpeed = stageSpeedEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public double getStageSpeedValue()
  {
    return _stageSpeed;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void setScanPassMechanicalConversions(MechanicalConversions mechanicalConversions)
  {
    _mechanicalConversions = mechanicalConversions;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public boolean hasScanPassMechanicalConversions()
  {
    return (_mechanicalConversions != null);
  }
  
  /**
   * @author Roy Williams
   */
  public boolean hasBreakPoints()
  {
    return (_scanPassBreakPoints.isEmpty() == false);
  }
  
  /**
   * @author Khang Wah, 2016-05-12. MVEDR
   */
  public void addBreakPoint(ScanPassBreakPoint breakPoint)
  {
    _scanPassBreakPoints.put(breakPoint.getBreakPointEnum(), breakPoint);
  }
  
  /**
   * @author Khang Wah, 2016-05-12. MVEDR
   */
  public Collection<ScanPassBreakPoint> getBreakPoints()
  {
    return _scanPassBreakPoints.values();
  }
  
  /**
   * @author Roy Williams
   */
  public void clearBreakPoint()
  {
    if(_scanPassBreakPoints.isEmpty()==false)
    {
      for(ScanPassBreakPoint breakPoint : getBreakPoints())
      {
        breakPoint.clearBreakPoint();
      }
      
      _scanPassBreakPoints.clear();
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean isContainScanPassBreak(ScanPassBreakPointEnum breakPointClass)
  {
    Assert.expect(breakPointClass != null);
    for (Map.Entry<ScanPassBreakPointEnum, ScanPassBreakPoint> entry : _scanPassBreakPoints.entrySet())
    {
      if (entry.getKey().equals(breakPointClass))
        return true;
    }
    return false;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean equals(Object obj) 
  {
    ScanPass scanPass = (ScanPass)obj;
    if (this.compareTo(scanPass) == 0)
      return true;
    else
      return false;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static Comparator getScanPassComparator()
  {
    return _scanPassComparator;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private static Comparator<ScanPass> _scanPassComparator = new Comparator<ScanPass>()
  {
    public int compare(ScanPass lhs, ScanPass rhs)
    {
      return lhs.compareTo(rhs);
    }
  };
}
