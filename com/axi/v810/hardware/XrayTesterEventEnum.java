package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * Defines the set of states for the XrayTester machine.   The XrayTester object
 * will drive the tester between these states.
 *
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Agilent Technogies</p>
 * @author Roy Williams
 * @author Rex Shang
 */
public class XrayTesterEventEnum extends HardwareEventEnum
{
  static private int _index = -1;

  public static final XrayTesterEventEnum STARTUP = new XrayTesterEventEnum(++_index, "Xray Tester, Startup");
  public static final XrayTesterEventEnum STARTUP_MOTION_RELATED_HARDWARE = new XrayTesterEventEnum(++_index, "Xray Tester, Startup Motion Related Hardware");
  public static final XrayTesterEventEnum SHUTDOWN = new XrayTesterEventEnum(++_index, "Xray Tester, Shutdown");
  public static final XrayTesterEventEnum HARDWARE_NOT_AVAILABLE = new XrayTesterEventEnum(++_index, "Xray Tester, Hardware Not Available");

  private String _name;

  /**
   * @author Rex Shang
   */
  private XrayTesterEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return _name;
  }

}
