package com.axi.v810.hardware;

/**
 * @author Greg Esparza
 */
public interface MotionAxisPositionInt
{
  /**
   * @author Greg Esparza
   */
  int getXaxisPositionInNanometers();

  /**
   * @author Greg Esparza
   */
  int getYaxisPositionInNanometers();

  /**
   * @author Greg Esparza
   */
  int getRailWidthAxisPositionInNanometers();
}
