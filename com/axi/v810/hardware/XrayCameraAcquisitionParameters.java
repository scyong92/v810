package com.axi.v810.hardware;

import java.util.*;
import com.axi.util.*;

/**
 * This class associates an xray camera id with a list of projection settings.
 * There will be one of these classes per camera, per magnification.
 *
 * @author Matt Wharton
 */
public class XrayCameraAcquisitionParameters
{
  private AbstractXrayCamera _camera;
  private List<ProjectionSettings> _projectionSettingsForCamera;
  
  /**
   * @author Greg Esparza
   */
  public XrayCameraAcquisitionParameters(AbstractXrayCamera camera)
  {
    Assert.expect(camera  != null);

    _camera = camera;
    _projectionSettingsForCamera = new ArrayList<ProjectionSettings>();
  }

  /**
   * Copy constructor.
   *
   * @author Matt Wharton
   */
  public XrayCameraAcquisitionParameters(XrayCameraAcquisitionParameters rhs)
  {
    Assert.expect(rhs != null);

    _camera = rhs._camera;
    _projectionSettingsForCamera = new ArrayList<ProjectionSettings>(rhs._projectionSettingsForCamera);
  }

  /**
   * @author Matt Wharton
   */
  public AbstractXrayCamera getCamera()
  {
    return _camera;
  }

  /**
   * Gets the projection settings.
   *
   * @author Matt Wharton
   */
  public List<ProjectionSettings> getProjectionSettings()
  {
    return _projectionSettingsForCamera;
  }

  /**
   * Sets the projection settings.
   *
   * @author Matt Wharton
   */
  public void setProjectionSettings(List<ProjectionSettings> projectionSettings)
  {
    Assert.expect(projectionSettings != null);

    _projectionSettingsForCamera.clear();

    for (ProjectionSettings projSettings : projectionSettings)
      _projectionSettingsForCamera.add(projSettings);
  }

  /**
   * @author Greg Esparza
   */
  public void addProjectionSettings(ProjectionSettings projectionSettings)
  {
    _projectionSettingsForCamera.add(projectionSettings);
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfProjectionSettings()
  {
    return _projectionSettingsForCamera.size();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_projectionSettingsForCamera != null)
    {
      for(ProjectionSettings projectionSettings : _projectionSettingsForCamera)
        projectionSettings.clear();
      
      _projectionSettingsForCamera.clear();
    }
  }
}
