package com.axi.v810.hardware;

import com.axi.v810.util.XrayTesterException;

/**
 * This class is the interface class for all lights.
 *
 * @author Rex Shang
 */
interface LightInt
{

  /**
   * Turn light on.
   */
  abstract void on() throws XrayTesterException;

  /**
   * Turn light off.
   */
  abstract void off() throws XrayTesterException;
}
