package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class MoveStageToOuterBarrierThreadTask extends ThreadTask<Object>
{
  private PanelHandler _panelHandler;
  private BarrierInt _outerBarrier;

  /**
   * @author Bill Darbie
   */
  public MoveStageToOuterBarrierThreadTask(BarrierInt outerBarrier)
  {
    super("Move Stage To Outer Barrier Thread Task");
    Assert.expect(outerBarrier != null);

    _panelHandler = PanelHandler.getInstance();
    _outerBarrier = outerBarrier;
  }

  /**
   * @author Bill Darbie
   */
  public Object executeTask() throws Exception
  {
    _panelHandler.moveStageToOuterBarrier(_outerBarrier);
    return null;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }
}
