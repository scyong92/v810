package com.axi.v810.hardware;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class allows Observers to be updated each time something in the hardware changes.
 *
 * @author Bill Darbie
 */
public class HardwareObservable extends Observable
{
  private static HardwareObservable _instance = null;
  private int _numDisables;
  private HardwareTaskEngine _hardwareTaskEngine;
  private boolean _offline;

  /**
   * @author Bill Darbie
   */
  private HardwareObservable()
  {
    Config config = Config.getInstance();
    _offline = !config.getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION);
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  }

  /**
   * @return an instance of this object.
   * @author Bill Darbie
   */
  public static synchronized HardwareObservable getInstance()
  {
    if (_instance == null)
      _instance = new HardwareObservable();

    return _instance;
  }

  /**
   * Set enable to true to cause datastore events to trigger update() calls.
   * When enable is set to false no updates will be made.  Note that you must
   * pair each setEnabled(false) with a setEnabled(true) because this method
   * keeps track of nested calls in order to work properly.
   *
   * @author Bill Darbie
   */
  public synchronized void setEnabled(boolean enable)
  {
    // this method is synchronized so that _numDisables is not affected by 2 threads at once, which could cause
    // false asserts

    if (enable)
    {
      --_numDisables;
      if (_numDisables == 0)
      {
        // when _numDisables goes from 1 to 0, check that we are on the HardwareWorker thread
        if (UnitTest.unitTesting() == false)
        {
          Assert.expect(HardwareWorkerThread.isHardwareWorkerThread(),
                        "ERROR: The last _hardwareObservable.setEnabled(true) call was not on the HardwareWorkerThread.\n" +
                        "       Possible causes for this are:\n" +
                        "       1. setEnabled(false), setEnabled(true) pairs are not correct in the code.\n" +
                        "       2. the call that spawned a thread did not wait until the thread was complete before returning\n" +
                        "          and that spawned thread was still running when the original HardwareWorkerThread completed.");
        }
      }
    }
    else
    {
      ++_numDisables;
      if (_numDisables == 1)
      {
        // when _numDisables goes from 0 to 1, check that we are on the HardwareWorker thread
        if (UnitTest.unitTesting() == false)
        {
          Assert.expect(HardwareWorkerThread.isHardwareWorkerThread(),
                        "ERROR: The first _hardwareObservable.setEnabled(false) call was not on the HardwareWorkerThread");
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public void stateChangedBegin(Object source, HardwareEventEnum hardwareEventEnum) throws XrayTesterException
  {
    stateChanged(source, hardwareEventEnum, true);
  }

  /**
   * @author Bill Darbie
   */
  public void stateChangedEnd(Object source, HardwareEventEnum hardwareEventEnum) throws XrayTesterException
  {
    stateChanged(source, hardwareEventEnum, false);
  }


  /**
   * Each HardwareObject should call this method when ever it does something that changes the
   * state of the hardware.  Any Observers of this class will be notified that the hardware state
   * has changed.
   *
   * @author Bill Darbie
   */
  private void stateChanged(Object source,
                            HardwareEventEnum hardwareEventEnum,
                            boolean eventStart) throws XrayTesterException
  {
    Assert.expect(source != null);
    Assert.expect(hardwareEventEnum != null);

    // now update all observers of the event that just happened
    if (_numDisables == 0)
    {
      // give the HardwareTaskEngine notice that this event just happened
      if (_offline == false)
        _hardwareTaskEngine.hardwareEvent(hardwareEventEnum, eventStart, true);

      setChanged();
      notifyObservers(new HardwareEvent(source, hardwareEventEnum, eventStart, true));
    }
    else
    {
      // give the HardwareTaskEngine notice that this event just happened
      if (_offline == false)
        _hardwareTaskEngine.hardwareEvent(hardwareEventEnum, eventStart, false);

      setChanged();
      notifyObservers(new HardwareEvent(source, hardwareEventEnum, eventStart, false));
    }
  }

  /**
   * When a TimeDrivenHardwareTask has an Exception it should call this method to alert
   * observers of the problem.
   *
   * @author Bill Darbie
   */
  public void notifyObserversOfException(XrayTesterException xRayTesterException)
  {
    Assert.expect(xRayTesterException != null);

    xRayTesterException = _hardwareTaskEngine.notifyObserversOfException(xRayTesterException);

    // always send HardwareExceptions no matter if the setEnabled() call is true or false!
    setChanged();
    notifyObservers(xRayTesterException);
  }

}
