package com.axi.v810.hardware;

import com.axi.v810.util.*;

/**
 * This interface will be implemented by all the Barrier objects.
 * The barriers on the machine are there to prevent X-rays from getting
 * outside of the system.
 * @author Rex Shang
 */
public interface BarrierInt
{
  /**
   * Close the barrier.
   * @author Rex Shang
   */
  abstract void close() throws XrayTesterException;

  /**
   * Open the current barrier.
   * @author Rex Shang
   */
  abstract void open() throws XrayTesterException;

  /**
   * @return true if the barrier is open.
   * @author Rex Shang
   */
  abstract boolean isOpen() throws XrayTesterException;
}
