package com.axi.v810.hardware;

import com.axi.util.*;

/**
 * If for some reason the serial communication fails (such as a stream IO error),
 * throw this exception.
 *
 * @author Jeff Ryer
 */
class SerialCommunicationErrorHardwareException extends HardwareException
{
  /**
   * @author Jeff Ryer
   */
  SerialCommunicationErrorHardwareException(String deviceName)
  {
    super(new LocalizedString("HW_SERIAL_COMMUNICATION_ERROR_KEY",
                              new Object[]{deviceName}));
    Assert.expect(deviceName != null);
  }
}

