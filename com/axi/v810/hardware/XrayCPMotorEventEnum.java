package com.axi.v810.hardware;

import com.axi.util.*;

//Variable Mag Anthony August 2011
/**
 * @author Anthony Fong
 */
public class XrayCPMotorEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static XrayCPMotorEventEnum XRAY_Z_AXIS_MOVE_HOME = new XrayCPMotorEventEnum(++_index, "XrayZAxis, Home");
  public static XrayCPMotorEventEnum XRAY_Z_AXIS_MOVE_LOW_MAG = new XrayCPMotorEventEnum(++_index, "XrayZAxis, Low Mag");
  public static XrayCPMotorEventEnum XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION1 = new XrayCPMotorEventEnum(++_index, "XrayZAxis, Low Mag Pos 1");
  public static XrayCPMotorEventEnum XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION2 = new XrayCPMotorEventEnum(++_index, "XrayZAxis, Low Mag Pos 2");
  public static XrayCPMotorEventEnum XRAY_Z_AXIS_MOVE_HIGH_MAG = new XrayCPMotorEventEnum(++_index, "XrayZAxis, High Mag");

  private String _name;

  /**
   * @author Anthony Fong
   */
  private XrayCPMotorEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author Anthony Fong
   */
  public String toString()
  {
    return _name;
  }

}
