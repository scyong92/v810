package com.axi.v810.hardware;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for SerialCommunicationsProtocolHardwareException.
*
* @author Jeff Ryer
*/
public class Test_SerialCommunicationErrorHardwareException extends UnitTest
{

  /**
  * @author Jeff Ryer
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_SerialCommunicationErrorHardwareException());
  }

  /**
  * @author Jeff Ryer
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    SerialCommunicationErrorHardwareException ex = new SerialCommunicationErrorHardwareException("deviceName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
