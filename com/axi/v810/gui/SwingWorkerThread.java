package com.axi.v810.gui;

import com.axi.util.*;

/**
 * The only purpose for having this class is to minimize the number of threads
 * we have running at once for GUI code to do non-GUI work on.  Instead
 * of doing new WorkerThread() in many places of the code, this class provides
 * access to one or two threads that GUI code can use.  This minimizes how
 * many threads we have running unecessarily and how many threads a debugger
 * would show.
 *
 * @author Bill Darbie
 */
public class SwingWorkerThread extends WorkerThread
{
  private static SwingWorkerThread _instance;
  private static SwingWorkerThread _instance2;
  private static SwingWorkerThread _instance3;

  /**
   * Use this call most of the time.  If you need a 2nd worker thread in addition
   * to this one call getInstance2()
   *
   * @author Bill Darbie
   */
  public static synchronized SwingWorkerThread getInstance()
  {
    if (_instance == null)
      _instance = new SwingWorkerThread("Swing worker thread 1");

    return _instance;
  }

  /**
   * Sometimes a GUI needs to use a 2nd thread to do some work.  In that case
   * use the Thread returned by this method.
   * @author Bill Darbie
   */
  public static synchronized SwingWorkerThread getInstance2()
  {
    if (_instance2 == null)
      _instance2 = new SwingWorkerThread("Swing worker thread 2");

    return _instance2;
  }

  /**
   * Sometimes a GUI needs to use a 3rd thread to do some work.  In that case
   * use the Thread returned by this method.
   * @author Bill Darbie
   */
  public static synchronized SwingWorkerThread getInstance3()
  {
    if (_instance3 == null)
      _instance3 = new SwingWorkerThread("Swing worker thread 3");

    return _instance3;
  }

  /**
  /**
   * @author Bill Darbie
   */
  private SwingWorkerThread(String threadName)
  {
    super(threadName);
  }
}
