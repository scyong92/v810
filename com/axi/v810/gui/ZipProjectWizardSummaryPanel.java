package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.guiUtil.wizard.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This panel will show the user the contents of their zip file they will create. This panel is the third panel for the Zip
 * project wizard and the second panel for the collect diagnostic package wizard.
 *
 * @author Erica Wheatcroft
 */
public class ZipProjectWizardSummaryPanel extends WizardPanel
{
  private JLabel _learnedDataIncludedLabel = new JLabel();
  private JLabel _imageSetsIncludedLabel = new JLabel();
  private JLabel _sizeOfZipLabel = new JLabel();
  private JLabel _destinationLabel = new JLabel();
  private JLabel _projectNameLabel = new JLabel();
  private JLabel _instructionLabel = new JLabel();
  private JLabel _imageSetsLabel = new JLabel();
  private JLabel _verificationImagesLabel = new JLabel();
  private JLabel _focusImageSetLabel = new JLabel();
  private JLabel instructionLabel = new JLabel();
  private JCheckBox _addToProjectDatabaseCheckbox;

  private ProjectArchiver _archiver = null;

  // this flag will indicate which wizard the summary screen is for.
  private boolean _showDiagnosticPackageInformation = false;

  /**
   * @author Erica Wheatcroft
   */
  public ZipProjectWizardSummaryPanel(WizardDialog dialog, ProjectArchiver archiver, boolean showDiagnosticPackageInformation)
  {
    super(dialog);

    Assert.expect(dialog != null);
    Assert.expect(archiver != null);

    _archiver = archiver;
    _showDiagnosticPackageInformation = showDiagnosticPackageInformation;

    createPanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void unpopulate()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void removeObservers()
  {
    // do nothing since we do not have any observers
  }


  /**
   * @author Erica Wheatcroft
   */
  public void populatePanelWithData()
  {
    Assert.expect(_archiver != null);

    Assert.expect(_archiver.hasProjectName());
    _projectNameLabel.setText(_archiver.getProjectName());

    Assert.expect(_archiver.hasDestinationFilename());
    _destinationLabel.setText(_archiver.getDestinationFilename());

    int approxSizeInBytes = _archiver.getApproximateZippedSizeInBytes();
    double megaBytes = MathUtil.convertBytesToMegabytes(approxSizeInBytes);
    _sizeOfZipLabel.setText(StringLocalizer.keyToString(MathUtil.roundToPlaces(megaBytes, 1) + " " + StringLocalizer.keyToString("ZIP_MB_UNITS_KEY")));

    if(_showDiagnosticPackageInformation == false)
    {
      // set the title
      _dialog.changeTitle(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_PANEL3_TITLE_KEY"));

      if (_archiver.getIncludeVerificationImages())
        _verificationImagesLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_VERIFICATION_IMAGES_INCLUDED_KEY"));
      else
        _verificationImagesLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_VERIFICATION_IMAGES_NOT_INCLUDED_KEY"));

      if (_archiver.getIncludeFocusImageSet())
        _focusImageSetLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_FOCUS_IMAGE_SET_INCLUDED_KEY"));
      else
        _focusImageSetLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_FOCUS_IMAGE_SET_NOT_INCLUDED_KEY"));

      int numberOfImageSetsSelected = _archiver.getNumberOfImageSetsToArchive();
      if (numberOfImageSetsSelected == 1)
        _imageSetsLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_ONE_IMAGE_SET_INCLUDED_KEY"));
      else if (numberOfImageSetsSelected == 0)
        _imageSetsLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_NO_IMAGE_SETS_INCLUDED_KEY"));
      else
        _imageSetsLabel.setText(numberOfImageSetsSelected + " " + StringLocalizer.keyToString("ZIP_PROJECT_IMAGE_SETS_INCLUDED_KEY"));
    }
    else
    {
      // set the title
      _dialog.changeTitle(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_PANEL2_TITLE_KEY"));

      if (_archiver.getIncludeAlgorithmLearning())
        _learnedDataIncludedLabel.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_LEARNED_DATA_INCLUDED_KEY"));
      else
        _learnedDataIncludedLabel.setText(StringLocalizer.keyToString("COLLECT_DIAGNOSTIC_PACKAGE_WIZARD_LEARNED_DATA_NOT_INCLUDED_KEY"));

      int numberOfImageSetsSelected = _archiver.getNumberOfImageSetsToArchive();

      if (numberOfImageSetsSelected == 1)
        _imageSetsIncludedLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_ONE_IMAGE_SET_INCLUDED_KEY"));
      else if (numberOfImageSetsSelected == 0)
        _imageSetsIncludedLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_NO_IMAGE_SETS_INCLUDED_KEY"));
      else
        _imageSetsIncludedLabel.setText(numberOfImageSetsSelected + " " + StringLocalizer.keyToString("ZIP_PROJECT_IMAGE_SETS_INCLUDED_KEY"));
    }

    validatePanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void validatePanel()
  {
    // if we have gotten this far then we know the user has specified all the necessary information. so we can fire
    // the button state event next can be set to enabled, cancel can be enabled, back is enabled, and generate is enabled.
     WizardButtonState buttonState = new WizardButtonState(false, true, true, true);
     _observable.updateButtonState(buttonState);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setWizardName(String name)
  {
    Assert.expect(name != null, "Name of panel is null");
    Assert.expect(name.length() > 0, "Name of panel is an empty string");
    _name = name;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getWizardName()
  {
    Assert.expect(_name != null, "Name of panel is not set");
    return _name;
  }


  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    if(_showDiagnosticPackageInformation == false)
    {
      setLayout(new BorderLayout(5, 15));
      setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

      // now lets create the summary panel
      JPanel summaryPanel = new JPanel(new GridLayout(1, 2, 0, 5));
      summaryPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
          new Color(148, 145, 140)), StringLocalizer.keyToString("ZIP_PROJECT_SUMMARY_PANEL_TITLE_KEY") ), BorderFactory.createEmptyBorder(10, 10, 10, 10)));

      JPanel leftInnerSummaryPanel = new JPanel(new PairLayout());

      JLabel projectLabel = new JLabel();
      projectLabel.setFont(FontUtil.getBoldFont(projectLabel.getFont(),Localization.getLocale()));
      projectLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_SUMMARY_PANEL_PROJECT_LABEL_KEY"));
      leftInnerSummaryPanel.add(projectLabel);
      leftInnerSummaryPanel.add(_projectNameLabel);

      JLabel destinationLabel = new JLabel();
      destinationLabel.setFont(FontUtil.getBoldFont(destinationLabel.getFont(),Localization.getLocale()));
      destinationLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DESTINATION_LABEL_KEY"));
      leftInnerSummaryPanel.add(destinationLabel);
      leftInnerSummaryPanel.add(_destinationLabel);

      JLabel sizeOfZipLabel = new JLabel();
      sizeOfZipLabel.setFont(FontUtil.getBoldFont(sizeOfZipLabel.getFont(),Localization.getLocale()));
      sizeOfZipLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_APPROXIMATE_ZIP_SIZE_KEY"));
      leftInnerSummaryPanel.add(sizeOfZipLabel);
      leftInnerSummaryPanel.add(_sizeOfZipLabel);
      summaryPanel.add(leftInnerSummaryPanel);

      JPanel imagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));

      JPanel rightInnerSummaryPanel = new JPanel(new GridLayout(3, 1, 0, 5));
      rightInnerSummaryPanel.add(_verificationImagesLabel);
      rightInnerSummaryPanel.add(_imageSetsLabel);
//      rightInnerSummaryPanel.add(_focusImageSetLabel);
      imagePanel.add(rightInnerSummaryPanel);
      summaryPanel.add(imagePanel);
      add(summaryPanel, BorderLayout.NORTH);

      JPanel centerPanel = new JPanel(new BorderLayout(5, 15));
      JPanel projectDatabasePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      projectDatabasePanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
          new Color(148, 145, 140)), StringLocalizer.keyToString("ZIP_PROJECT_DATABASE_PANEL_TITLE_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
      _addToProjectDatabaseCheckbox = new JCheckBox();
      _addToProjectDatabaseCheckbox.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_PROJECT_DB_INSTRUCTION_KEY"));
      projectDatabasePanel.add(_addToProjectDatabaseCheckbox);
      centerPanel.add(projectDatabasePanel, BorderLayout.NORTH);

      JPanel instructionPanel = new JPanel(new FlowLayout());
      instructionPanel.setBorder(BorderFactory.createEmptyBorder(15, 5, 5, 0));
      instructionLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SUMMARY_PANEL_INSTRUCTION_KEY"));
      instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
      instructionPanel.add(instructionLabel);
      centerPanel.add(instructionPanel, BorderLayout.CENTER);
      add(centerPanel, BorderLayout.CENTER);

      _addToProjectDatabaseCheckbox.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _archiver.setAddToProjectDatabase(_addToProjectDatabaseCheckbox.isSelected());
        }
      });
    }
    else
    {
      setLayout(new BorderLayout(5, 15));
      setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

      // now lets create the summary panel
      JPanel summaryPanel = new JPanel(new GridLayout(1, 2, 0, 5));
      summaryPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                                                StringLocalizer.keyToString("ZIP_PROJECT_SUMMARY_PANEL_TITLE_KEY")),
                                                                BorderFactory.createEmptyBorder(10, 10, 10, 10)));


      JPanel innerLeftSummaryPanel = new JPanel(new PairLayout());
      JLabel projectLabel = new JLabel();
      projectLabel.setFont(FontUtil.getBoldFont(projectLabel.getFont(),Localization.getLocale()));
      projectLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_SUMMARY_PANEL_PROJECT_LABEL_KEY"));
      innerLeftSummaryPanel.add(projectLabel);
      innerLeftSummaryPanel.add(_projectNameLabel);

      JLabel destinationLabel = new JLabel();
      destinationLabel.setFont(FontUtil.getBoldFont(destinationLabel.getFont(),Localization.getLocale()));
      destinationLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_DESTINATION_LABEL_KEY"));
      innerLeftSummaryPanel.add(destinationLabel);
      innerLeftSummaryPanel.add(_destinationLabel);

      JLabel sizeLabel = new JLabel();
      sizeLabel.setFont(FontUtil.getBoldFont(sizeLabel.getFont(),Localization.getLocale()));
      sizeLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_APPROXIMATE_ZIP_SIZE_KEY"));
      innerLeftSummaryPanel.add(sizeLabel);
      innerLeftSummaryPanel.add(_sizeOfZipLabel);
      summaryPanel.add(innerLeftSummaryPanel);

      JPanel imagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
      JPanel innerRightSummaryPanel = new JPanel(new GridLayout(2, 1, 0, 5));
      innerRightSummaryPanel.add(_learnedDataIncludedLabel);
      innerRightSummaryPanel.add(_imageSetsIncludedLabel);
      imagePanel.add(innerRightSummaryPanel);
      summaryPanel.add(imagePanel);
      add(summaryPanel, BorderLayout.NORTH);

      JPanel centerPanel = new JPanel(new BorderLayout());
      JPanel instructionPanel = new JPanel(new FlowLayout());
      _instructionLabel.setFont(FontUtil.getBoldFont(_instructionLabel.getFont(),Localization.getLocale()));
      _instructionLabel.setText(StringLocalizer.keyToString("ZIP_PROJECT_WIZARD_SUMMARY_PANEL_INSTRUCTION_KEY"));
      instructionPanel.add(_instructionLabel);
      centerPanel.add(instructionPanel, BorderLayout.NORTH);
      add(centerPanel, BorderLayout.CENTER);

    }
  }
}
