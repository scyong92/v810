package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * Import a 5DX project to convert to a Genesis project.
 * @author George Booth
 */
public class ProjectImportDialog extends JDialog
{
  private static boolean _runFromMain = false;

  private static final String _assertLogFile = "ProjectImportDialog";
  private static final String _appName = StringLocalizer.keyToString("PDD_GUI_PROJECT_IMPORT_NAME_KEY");

  // gui components

  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _projectSelectionPanel = new JPanel();
  private BorderLayout _projectSelectionPanelBorderLayout = new BorderLayout();
  private JPanel _projectsDirPanel = new JPanel();
  private BorderLayout _projectsDirPanelBorderLayout = new BorderLayout();
  private JLabel _projectsDirLabel = new JLabel();
  private JLabel _projectsDirLocationLabel = new JLabel();
  private JScrollPane _projectListScrollPane = new JScrollPane();
  private DefaultListModel _projectListModel = new DefaultListModel();
  private JList _projectList = new JList(_projectListModel);
  private JPanel _controlsPanel = new JPanel();
  private BorderLayout _controlsPanelBorderLayout = new BorderLayout();
  private JPanel _southControlsPanel = new JPanel();
  private FlowLayout _southControlsPanelFlowLayout = new FlowLayout();
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private JButton _importButton = new JButton();
  private JButton _exitButton = new JButton();
//  private JFileChooser _fileChooser = null;
  private Border _empty5505SpaceBorder;
  private KeyListener _escapeKeyListener = null;

//  private String _outputDirectory = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private boolean _importPerformed = false;

  /**
   * @author George Booth, Laura Cormos
   */
  public ProjectImportDialog( Frame parentFrame, boolean modal )
  {
    super(parentFrame, _appName, modal);

    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }

    populateProjectlList();
//    _outputDirectory = Directory.getTempDir();
  }

  /**
   * @author George Booth, Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _runFromMain = true;
          MainMenuGui mainMenu = MainMenuGui.getInstance();
          mainMenu.start();
          ProjectImportDialog ProjectImportDialog = new ProjectImportDialog(mainMenu, true);
          ProjectImportDialog.pack();
          ProjectImportDialog.setVisible(true);
        }
        catch (RuntimeException re)
        {
          re.printStackTrace();
          Assert.logException(re);
        }
      }
    });
  }

  private void jbInit()
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _controlsPanel.setLayout(_controlsPanelBorderLayout);
    _importButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _importButton.setText(StringLocalizer.keyToString("PDD_GUI_PROJECT_IMPORT_BUTTON_KEY"));
    _importButton.setEnabled(false);
//    _importButton.setMnemonic((StringLocalizer.keyToString("GUI_FILE_IMPORT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _importButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        importButton_actionPerformed();
      }
    });
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _southControlsPanel.setLayout(_southControlsPanelFlowLayout);

    this.setResizable(true);
    _projectSelectionPanel.setLayout(_projectSelectionPanelBorderLayout);
    _projectSelectionPanelBorderLayout.setHgap(5);
    _projectSelectionPanelBorderLayout.setVgap(5);
    _projectList.setLayoutOrientation(JList.VERTICAL_WRAP);
    _projectList.addMouseListener(new MouseAdapter()
    {
      public void mouseReleased(MouseEvent e)
      {
        _projectList_mouseReleased(e);
      }
    });
    _projectList.setFixedCellWidth(120);
    _projectList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    _projectList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        projectList_valueChanged(e);
      }
    });
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(2);
    _buttonPanelGridLayout.setHgap(10);
    _buttonPanelGridLayout.setVgap(20);
    _empty5505SpaceBorder = BorderFactory.createEmptyBorder(5, 5, 0, 5);
    _projectsDirLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _projectsDirLabel.setHorizontalTextPosition(SwingConstants.LEFT);
    _projectsDirLabel.setText(StringLocalizer.keyToString("PZD_GUI_PROJECTS_DIR_LABEL_KEY"));
    _projectsDirLocationLabel.setText(Directory.getProjectsDir());
    _projectSelectionPanel.setBorder(_empty5505SpaceBorder);
    _projectsDirPanelBorderLayout.setHgap(5);
    _mainPanel.add(_controlsPanel, BorderLayout.SOUTH);
    _controlsPanel.add(_southControlsPanel,  BorderLayout.SOUTH);
    _southControlsPanel.add(_buttonPanel, null);
    _buttonPanel.add(_importButton, null);
    _buttonPanel.add(_exitButton, null);
    _mainPanel.add(_projectSelectionPanel, BorderLayout.CENTER);
    _projectListScrollPane.getViewport().add(_projectList, null);
    _projectsDirPanel.setLayout(_projectsDirPanelBorderLayout);
    _projectSelectionPanel.add(_projectsDirPanel, BorderLayout.NORTH);
    _projectsDirPanel.add(_projectsDirLabel, BorderLayout.WEST);
    _projectsDirPanel.add(_projectsDirLocationLabel, BorderLayout.CENTER);
    _projectSelectionPanel.add(_projectListScrollPane, BorderLayout.CENTER);
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    _escapeKeyListener = new KeyAdapter()
    {
      public void keyPressed(KeyEvent e)
      {
        checkForEscapeKeyPressed(e);
      }
    };
    this.addKeyListener(_escapeKeyListener);
    _projectList.addKeyListener(_escapeKeyListener);
//    _importButton.addKeyListener(_escapeKeyListener);
//    _exitButton.addKeyListener(_escapeKeyListener);
  }

  /**
   * @author George Booth, Laura Cormos
   */
  void projectList_valueChanged(ListSelectionEvent e)
  {
    if (_projectList.getSelectedIndex() != -1)
    {
      _importButton.setEnabled(true);
    }
    else
    {
      _importButton.setEnabled(false);
    }
  }

  /**
   * The import procedure goes here.
   * @author George Booth, Laura Cormos
   */
  void importButton_actionPerformed()
  {
    final String projectName = _projectList.getSelectedValue().toString();

    File projectNameDir = new File(Directory.getProjectDir(projectName));

    if (projectNameDir.exists())
      if (projectNameDir.isDirectory())
        if ( JOptionPane.showConfirmDialog( this,StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_KEY"),
                                            StringLocalizer.keyToString("PDD_CONFIRM_PROJECT_IMPORT_TITLE_KEY"),
                                            JOptionPane.YES_NO_OPTION,
                                            JOptionPane.QUESTION_MESSAGE ) == JOptionPane.NO_OPTION )
          return;

    // Change the cursor temporarily and wait for the import to finish.
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    setEnabled(false); // do not allow user's input during zipping
    _importPerformed = false;

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (false)
            FileUtilAxi.delete(Directory.getProjectDir(projectName));
          _importPerformed = true;
        }
        catch (final DatastoreException ie)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true); // allow user to press a button
              MessageDialog.reportIOError(ProjectImportDialog.this, ie.getLocalizedMessage());
            }
          });
        }
        if (_importPerformed == true)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true);
//              _projectListModel.removeAllElements();
//              populateProjectlList();
            }
          });
        }
      }
    });
  }

  /**
   * When the exit button is pressed the Project Delete Dialog will exit.
   * @author George Booth, Laura Cormos
   */
  void exitButton_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Exit this if the escape key is pressed
   * @author George Booth, Laura Cormos
   */
  void checkForEscapeKeyPressed(KeyEvent e)
  {
    if ( e.getKeyCode() == KeyEvent.VK_ESCAPE )
      exitDialog();
  }

  /**
   * @author George Booth, Laura Cormos
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if ( e.getID() == WindowEvent.WINDOW_CLOSING )
      exitDialog();
  }

  /**
   * @author George Booth, Laura Cormos
   */
  private void exitDialog()
  {
    if (_runFromMain)
      System.exit(0);
    else
    {
      this.setVisible(false);
      dispose();
    }
  }

  /**
   * @author George Booth, Laura Cormos
   */
  private void populateProjectlList()
  {
    java.util.List<String> projectList = FileName.getProjectNames();

    if (projectList != null)
    {
      for (String projectName : projectList)
      {
        _projectListModel.addElement(new File(projectName).getName());
      }
    }
  }


  /**
   * If user double clicks on a project name, start the import action
   * @author George Booth, Laura Cormos
   */
  public void _projectList_mouseReleased(MouseEvent e)
  {
    if(e.getClickCount() == 2)
    {
      //the user has double clicked
      importButton_actionPerformed();
    }
  }

  /**
   * @author George Booth, Laura Cormos
   */
  public void setEnabled(boolean b)
  {
    _exitButton.setEnabled(b);
    _importButton.setEnabled(b);
  }

}
