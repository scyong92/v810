package com.axi.v810.gui;

import java.io.*;

/**
* This class contains all the data for the GUI element TestOptionsDlg for the dialog box that allows the user to change test options such as
* Loop count, loop forever/finite setting.  This is serializable to save loop settings from invocation to invocation.
*
* @author Andy Mechtenberg
*/
public class TestOptions implements Serializable
{
  private int _loopCount = 1;
  private boolean _loopForever = false;

  /**
   * @author Andy Mechtenberg
   */
  public TestOptions()
  {
    // do nothing
  }

  /**
  * Get the current loop fovever / count status
  *
  * @author Andy Mechtenberg
  * @return The boolean TRUE if looping forever is set, FALSE if a finite loop count is specified.
  */
  public boolean doLoopForever()
  {
    return _loopForever;
  }

  /**
  * Set the current loop fovever status
  *
  * @author Andy Mechtenberg
  * @param loopForever The boolean TRUE if looping forever, FALSE if a finite loop count is specified.
  */
  void setLoopForever(boolean loopForever)
  {
    _loopForever = loopForever;
  }

  /**
  * Get the current loop count
  *
  * @author Andy Mechtenberg
  * @return The integer specifying the loop count.  If loop fovever is selected, this will return the last valid loop count
  */
  public int getLoopCount()
  {
    return _loopCount;
  }

  /**
  * Set the current loop count
  *
  * @author Andy Mechtenberg
  * @param loopcount The integer specifying the loop count.
  */
  void setLoopCount(int loopcount)
  {
    _loopCount = loopcount;
  }
}
