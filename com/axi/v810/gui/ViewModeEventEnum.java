package com.axi.v810.gui;


/**
 * This class enumerates all events that can
 * have an effect on the "view" of the gui. For example,
 * if we move from the panel setup screen to the algorithm tuner screen,
 * or if we choose to view a package in the graphics window rather than
 * the entire board.
 *
 * @author George A. David
 */
public class ViewModeEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static ViewModeEventEnum PACKAGE = new ViewModeEventEnum(++_index);
  public static ViewModeEventEnum LANDPATTERN = new ViewModeEventEnum(++_index);
  public static ViewModeEventEnum PANEL = new ViewModeEventEnum(++_index);
  public static ViewModeEventEnum REGION = new ViewModeEventEnum(++_index);
  public static ViewModeEventEnum SCREEN = new ViewModeEventEnum(++_index);

  /**
   * @author George A. David
   */
  private ViewModeEventEnum(int id)
  {
    super(id);
  }
}