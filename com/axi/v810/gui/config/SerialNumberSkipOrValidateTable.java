package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
public class SerialNumberSkipOrValidateTable extends JSortTable
{
  public static final int _PATTERN_TYPE_INDEX = 0;
  public static final int _PATTERN_INDEX = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double _PATTERN_COLUMN_WIDTH_PERCENTAGE = .80;

  private PatternTypeEditor _patternTypeEditor = new PatternTypeEditor();

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public SerialNumberSkipOrValidateTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int patternTypeColumnWidth = (int)(totalColumnWidth * _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int patternColumnWidth = (int)(totalColumnWidth * _PATTERN_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_PATTERN_TYPE_INDEX).setPreferredWidth(patternTypeColumnWidth);
    columnModel.getColumn(_PATTERN_INDEX).setPreferredWidth(patternColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_PATTERN_TYPE_INDEX).setCellEditor(_patternTypeEditor);

    columnModel.getColumn(_PATTERN_INDEX).setCellEditor(stringEditor);
  }
}
