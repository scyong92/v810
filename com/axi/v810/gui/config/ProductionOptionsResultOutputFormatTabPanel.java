/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 *
 * @author huai-en.janan-ezekie
 */
public class ProductionOptionsResultOutputFormatTabPanel extends AbstractTaskPanel implements Observer
{

  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private TestExecution _testExecution = TestExecution.getInstance();
  private JPanel _centerPanel = null;
  
  private Config _config = Config.getInstance();

  //Janan Wong - XCR 3551: customise measurment xml output file
  private JCheckBox _enableGenerateMeasurementsXMLCheckBox;
  private ButtonGroup _generateMeasurementsButtonGroup;
  private JRadioButton _setDefaultMeasurementsXMLRadioButton;
  private JRadioButton _setCustomizeMeasurementsXMLRadioButton;

  private JPanel _customMeasurementsXMLOptionPanel;
  
  private JButton _jointTypeButton;
  private JButton _measurementTypeButton;
  
  private Set<String> _measurementTypeStringSet = new HashSet<String>();

  private JTable _jointTypeSummaryTable;
  private JTable _measurementTypeSummaryTable;

  private DefaultTableModel _jointTypeSummaryTableModel;
  private DefaultTableModel _measurementTypeSummaryTableModel;

  private Vector<String> _jointTypeSummaryTableColumnHeader = new Vector<String>();
  private Vector<String> _measurementTypeSummaryTableColumnHeader = new Vector<String>();

  private Vector<Vector> _selectedJointTypeSummary = new Vector<Vector>();
  private Vector<Vector> _selectedMeasurementTypeSummary = new Vector<Vector>();

  private JScrollPane _jointTypeSummaryTableScrollPane;
  private JScrollPane _measurementTypeSummaryTableScrollPane;

  private JLabel _summaryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_SELECTED_MEASUREMENT_TYPE_SUMMARY_KEY"));

  private JCheckBox _setDumpZHeightCheckBox; 
  
  private ScrollableCheckBoxListPopupMenu _jointTypePopupMenu;
  private ScrollableCheckBoxListPopupMenu _measurementTypePopupMenu;
  
  private java.util.List<String> _measurementTypeList = new ArrayList<String>();
  private java.util.List<String> _jointTypeList = new ArrayList<String>();
  
  private java.util.List<Integer> _listOfSelectedMeasurementTypeIndices = new ArrayList<Integer>();
  private java.util.List<Integer> _listOfSelectedJointTypeIndices = new ArrayList<Integer>();
  
  private ArrayList<String> _populatedMeasurementTypeList;
  
  /**
   * @author Janan Wong
   */
  public ProductionOptionsResultOutputFormatTabPanel(ConfigEnvironment envPanel)
  {

    super(envPanel);
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * This class is an observer of config. We will determine what kind of changes
   * were made and decide if we need to update the gui.
   *
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum) argument;
            if (configEnum instanceof SoftwareConfigEnum)
            {
              SoftwareConfigEnum softwareConfigEnum = (SoftwareConfigEnum) configEnum;
              if (softwareConfigEnum.equals(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE)
                || softwareConfigEnum.equals(SoftwareConfigEnum.DUMP_ZHEIGHT_IN_MEASUREMENTS_XML_FILE)) //Janan Wong
              {
                updateDataOnScreen();
              }
            }
          }
        }
      }
    });
  }

  private void jbInit()
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    _centerPanel = new JPanel();
    VerticalFlowLayout centerPanelLayoutManager = new VerticalFlowLayout();
    _centerPanel.setLayout(centerPanelLayoutManager);

    add(_centerPanel, BorderLayout.CENTER);

    JPanel measurementsXMLPanel = new JPanel(new VerticalFlowLayout());
    _customMeasurementsXMLOptionPanel = new JPanel(new VerticalFlowLayout());
    JPanel measurementsXMLOptionPanel = new JPanel();

    _centerPanel.add(measurementsXMLPanel); 

    Border measurementsXMLBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
      StringLocalizer.keyToString("CFGUI_MEASUREMENTS_XML_KEY"));
    Border measurementsXMLBorder = BorderFactory.createCompoundBorder(measurementsXMLBorder1,
      BorderFactory.createEmptyBorder(0, 0, 10, 10));

    Border customMeasurementsXMLOptionBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
      StringLocalizer.keyToString("CFGUI_CUSTOMIZE_MEASUREMENTS_XML_FILE_KEY"));
    Border customMeasurementsXMLOptionBorder = BorderFactory.createCompoundBorder(customMeasurementsXMLOptionBorder1,
      BorderFactory.createEmptyBorder(0, 0, 0, 0));

    JPanel jointTypeAndMeasurementButtonPanel = new JPanel(new VerticalFlowLayout());
    jointTypeAndMeasurementButtonPanel.setBorder(customMeasurementsXMLOptionBorder);
    measurementsXMLPanel.setBorder(measurementsXMLBorder);  
    _customMeasurementsXMLOptionPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    _customMeasurementsXMLOptionPanel.add(jointTypeAndMeasurementButtonPanel);
    _enableGenerateMeasurementsXMLCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_ENABLE_GENERATE_MEASUREMENTS_XML_KEY"));
    _generateMeasurementsButtonGroup = new ButtonGroup();
    _setDefaultMeasurementsXMLRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_DEFAULT_MEASUREMENTS_XML_KEY"));
    _setCustomizeMeasurementsXMLRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_CUSTOM_MEASUREMENTS_XML_KEY"));
    _setDumpZHeightCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_DUMP_Z_HEIGHT_KEY"));
    _setDefaultMeasurementsXMLRadioButton.setSelected(true);
    _setDefaultMeasurementsXMLRadioButton.setEnabled(false);
    _setCustomizeMeasurementsXMLRadioButton.setEnabled(false);

    measurementsXMLOptionPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    measurementsXMLOptionPanel.setLayout(new BoxLayout(measurementsXMLOptionPanel, BoxLayout.Y_AXIS));
    _generateMeasurementsButtonGroup.add(_setDefaultMeasurementsXMLRadioButton);
    _generateMeasurementsButtonGroup.add(_setCustomizeMeasurementsXMLRadioButton);
    measurementsXMLOptionPanel.add(_setDefaultMeasurementsXMLRadioButton);
    measurementsXMLOptionPanel.add(_setCustomizeMeasurementsXMLRadioButton);

    JPanel jointAndMeasurementTypeButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    jointAndMeasurementTypeButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    jointAndMeasurementTypeButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    
    JPanel dumpZHeightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 0));
    dumpZHeightPanel.add(_setDumpZHeightCheckBox);

    _jointTypeButton = new JButton(StringLocalizer.keyToString("CFGUI_JOINT_TYPE_BUTTON_KEY"),Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
    _jointTypeButton.setHorizontalTextPosition(SwingConstants.LEFT);   
    _jointTypeButton.setPreferredSize(new Dimension(216, 30));
    _measurementTypeButton = new JButton(StringLocalizer.keyToString("CFGUI_MEASUREMENT_TYPE_BUTTON_KEY"),Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
    _measurementTypeButton.setHorizontalTextPosition(SwingConstants.LEFT);   
    _measurementTypeButton.setPreferredSize(new Dimension(340, 30));

    jointAndMeasurementTypeButtonPanel.add(_jointTypeButton);
    jointAndMeasurementTypeButtonPanel.add(_measurementTypeButton);

    _jointTypeSummaryTableColumnHeader.addElement(StringLocalizer.keyToString("CFGUI_JOINT_TYPE_SELECTED_KEY"));
    _measurementTypeSummaryTableColumnHeader.addElement(StringLocalizer.keyToString("CFGUI_MEASUREMENT_TYPE_SELECTED_KEY"));

    _jointTypeSummaryTableModel = new DefaultTableModel(_selectedJointTypeSummary, _jointTypeSummaryTableColumnHeader);
    _measurementTypeSummaryTableModel = new DefaultTableModel(_selectedMeasurementTypeSummary, _measurementTypeSummaryTableColumnHeader);

    JPanel summaryTablePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    summaryTablePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    summaryTablePanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

    _jointTypeSummaryTable = new JTable(_jointTypeSummaryTableModel);
    _measurementTypeSummaryTable = new JTable(_measurementTypeSummaryTableModel);

    _jointTypeSummaryTableScrollPane = new JScrollPane(_jointTypeSummaryTable);
    _measurementTypeSummaryTableScrollPane = new JScrollPane(_measurementTypeSummaryTable);

    _jointTypeSummaryTableScrollPane.setPreferredSize(new Dimension(215, 300));
    _measurementTypeSummaryTableScrollPane.setPreferredSize(new Dimension(338, 300));

    summaryTablePanel.add(_jointTypeSummaryTableScrollPane);
    summaryTablePanel.add(_measurementTypeSummaryTableScrollPane);

    _jointTypeSummaryTable.setPreferredScrollableViewportSize(_jointTypeSummaryTable.getPreferredSize());
    _jointTypeSummaryTable.setFillsViewportHeight(true);

    _measurementTypeSummaryTable.setPreferredScrollableViewportSize(_measurementTypeSummaryTable.getPreferredSize());
    _measurementTypeSummaryTable.setFillsViewportHeight(true);

    _jointTypeSummaryTable.setCellSelectionEnabled(false);
    _jointTypeSummaryTable.setDefaultEditor(Object.class, null);
    _jointTypeSummaryTable.setFocusable(false);
    
    _measurementTypeSummaryTable.setFocusable(false);
    _measurementTypeSummaryTable.setCellSelectionEnabled(false);
    _measurementTypeSummaryTable.setDefaultEditor(Object.class, null);

    jointTypeAndMeasurementButtonPanel.add(jointAndMeasurementTypeButtonPanel);
    jointTypeAndMeasurementButtonPanel.add(_summaryLabel);
    jointTypeAndMeasurementButtonPanel.add(summaryTablePanel);
    
    measurementsXMLPanel.add(_enableGenerateMeasurementsXMLCheckBox);
    measurementsXMLPanel.add(measurementsXMLOptionPanel);
    measurementsXMLPanel.add(_customMeasurementsXMLOptionPanel);    
    measurementsXMLPanel.add(dumpZHeightPanel);        
     
    //Initialise the joint type popup menu based on last configuration and populate selected joint type
    for (int i = 0; i < JointTypeEnum.getAllJointTypeEnums().size(); i++)
    {
      _jointTypeList.add(JointTypeEnum.getAllJointTypeEnums().get(i).getName());
      if (_config.getBooleanValue(CustomMeasurementFileConfigEnum.getCustomMeasurementFileConfigEnum(JointTypeEnum.getAllJointTypeEnums().get(i).getName())))
      {
        _listOfSelectedJointTypeIndices.add(i);
        Vector<String> vector = new Vector<String>();
        vector.add(JointTypeEnum.getAllJointTypeEnums().get(i).getName());
        _selectedJointTypeSummary.add(vector);
        populateMeasurementTypeInPopupMenu(InspectionFamily.getInspectionFamily(JointTypeEnum.getJointTypeEnum(JointTypeEnum.getAllJointTypeEnums().get(i).getName())),JointTypeEnum.getAllJointTypeEnums().get(i));
        CustomMeasurementSetting.getInstance().addCustomSettingJointType(JointTypeEnum.getAllJointTypeEnums().get(i).getName());
      }
    }
    
    _populatedMeasurementTypeList = new ArrayList<String>(_measurementTypeStringSet);
    Collections.sort(_populatedMeasurementTypeList);
    _measurementTypeList.clear();
    _measurementTypeList.addAll(_populatedMeasurementTypeList);
    
    //Initialise the measurement type popupmenu based on last configuration and populate selected measurement type
    for (int i = 0; i < _measurementTypeStringSet.size(); i++)
    {
      if (_config.getBooleanValue(CustomMeasurementFileConfigEnum.getCustomMeasurementFileConfigEnum(_populatedMeasurementTypeList.get(i))))
      {
        _listOfSelectedMeasurementTypeIndices.add(i);
        Vector<String> vector = new Vector<String>();
        vector.add(_populatedMeasurementTypeList.get(i));
        _selectedMeasurementTypeSummary.add(vector);
        CustomMeasurementSetting.getInstance().addCustomSettingMeasurement(_populatedMeasurementTypeList.get(i));
      }
    }
    
    _jointTypePopupMenu = new ScrollableCheckBoxListPopupMenu(_jointTypeList,
                                                              _listOfSelectedJointTypeIndices,
                                                              true,
                                                              true,
                                                              0,
                                                              _jointTypeList.size());
    _jointTypePopupMenu.setVisibleRowCount(17);
    _jointTypePopupMenu.setPreferredSize(new Dimension(_jointTypeButton.getPreferredSize().width, _jointTypePopupMenu.getPreferredSize().height));

    _measurementTypePopupMenu = new ScrollableCheckBoxListPopupMenu(_measurementTypeList,
                                                                    _listOfSelectedMeasurementTypeIndices,
                                                                    true,
                                                                    true,
                                                                    0,
                                                                    _measurementTypeList.size());
    _measurementTypePopupMenu.setVisibleRowCount(20);
    _measurementTypePopupMenu.setPreferredSize(new Dimension(_measurementTypeButton.getPreferredSize().width,_measurementTypePopupMenu.getPreferredSize().height));
 
    _jointTypePopupMenu.addPopupMenuListener(new PopupMenuListener()
    {
      public void popupMenuWillBecomeVisible(PopupMenuEvent e)
      {
        _jointTypeButton.setIcon(Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
      }

      public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
      {
        _jointTypeButton.setIcon(Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
        _populatedMeasurementTypeList.clear();
        _populatedMeasurementTypeList.addAll(_measurementTypeStringSet);
        Collections.sort(_populatedMeasurementTypeList);
        _measurementTypeList.clear();
        _measurementTypeList.addAll(_populatedMeasurementTypeList);
        _measurementTypePopupMenu.deleteObjectList();
        _measurementTypePopupMenu.addObjectList(_measurementTypeList);
        _measurementTypeSummaryTableModel.fireTableDataChanged();;
      }

      public void popupMenuCanceled(PopupMenuEvent e)
      {
        _jointTypeButton.setIcon(Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
        _selectedJointTypeSummary.clear();
        
        Collections.sort(_jointTypePopupMenu.getSelectedValuesList());        
        for(Object jointType : _jointTypePopupMenu.getSelectedValuesList())
        {
          Vector<String> vector = new Vector<String>();
          vector.add(jointType.toString());
          _selectedJointTypeSummary.add(vector);
          
          try
          {
            _config.setValue(CustomMeasurementSetting.getInstance().getKeyToConfigEnumMap().get(jointType.toString().replace(" ", "").toLowerCase()),
                             true);
            CustomMeasurementSetting.getInstance().addCustomSettingJointType(jointType.toString());
            populateMeasurementTypeInPopupMenu(InspectionFamily.getInspectionFamily(JointTypeEnum.getJointTypeEnum(jointType.toString())),JointTypeEnum.getJointTypeEnum(jointType.toString()));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }
        }

        for(Object jointType : _jointTypePopupMenu.getDeselectedValuesLists())
        {
          try
          {
            _config.setValue(CustomMeasurementSetting.getInstance().getKeyToConfigEnumMap().get(jointType.toString().replace(" ", "").toLowerCase()),
                             false);
            CustomMeasurementSetting.getInstance().removeCustomSettingJointType(jointType.toString());
            depopulateMeasurementTypeInPopupMenu(InspectionFamily.getInspectionFamily(JointTypeEnum.getJointTypeEnum(jointType.toString())),
                                                 JointTypeEnum.getJointTypeEnum(jointType.toString()));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }
        }
        
        _jointTypeSummaryTableModel.fireTableDataChanged();        
        CustomMeasurementSetting.getInstance().clearAllMeasurement();
        _selectedMeasurementTypeSummary.clear();
        
        for (Object measurements : _measurementTypePopupMenu.getSelectedValuesList())
        {
          try
          {
            _config.setValue(CustomMeasurementSetting.getInstance().getKeyToConfigEnumMap().get(measurements.toString().replace(" ", "").toLowerCase()), false);
            CustomMeasurementSetting.getInstance().removeCustomSettingMeasurement(measurements.toString());
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }
        }
      }
    });
    
    _measurementTypePopupMenu.addPopupMenuListener(new PopupMenuListener(){

      public void popupMenuWillBecomeVisible(PopupMenuEvent e)
      {
        _measurementTypePopupMenu.resetSelectAllCheckBox();
        _measurementTypeButton.setIcon(Image5DX.getImageIcon(Image5DX.VC_UP_ARROW));
      }

      public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
      {
        _measurementTypeButton.setIcon(Image5DX.getImageIcon(Image5DX.VC_DOWN_ARROW));
      }

      public void popupMenuCanceled(PopupMenuEvent e)
      {
        populateSelectedMeasurementInTable();
      }
    });
    
    _enableGenerateMeasurementsXMLCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        generateMeasurementsXMLCheckBox_actionPerformed();
      }
    });

    _setDumpZHeightCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setDumpZHeightCheckBox_actionPerformed();
      }
    });

    _enableGenerateMeasurementsXMLCheckBox.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.DESELECTED)
        {
          _jointTypeButton.setEnabled(false);
          _measurementTypeButton.setEnabled(false);
          _summaryLabel.setEnabled(false);
          _jointTypeSummaryTable.setEnabled(false);
          _measurementTypeSummaryTable.setEnabled(false);
          _customMeasurementsXMLOptionPanel.setEnabled(false);          
        }
        else if (e.getStateChange() == ItemEvent.SELECTED)
        {
          _jointTypeButton.setEnabled(true);
          _measurementTypeButton.setEnabled(true);
          _summaryLabel.setEnabled(true);
          _jointTypeSummaryTable.setEnabled(true);
          _measurementTypeSummaryTable.setEnabled(true);
          _customMeasurementsXMLOptionPanel.setEnabled(true);        
        }
      }
    });

    _jointTypeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ev)
      {
        _jointTypePopupMenu.show(_jointTypeButton, 0, _jointTypeButton.getBounds().y + 
                                 _jointTypeButton.getBounds().height);
      }
    });
    
    _measurementTypeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ev)
      {
        _measurementTypePopupMenu.show(_measurementTypeButton, 0, _measurementTypeButton.getBounds().y + 
                                       _measurementTypeButton.getBounds().height);
      }
    });
    
    _setDefaultMeasurementsXMLRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          _jointTypeButton.setEnabled(false);
          _measurementTypeButton.setEnabled(false);
          _summaryLabel.setEnabled(false);
          _jointTypeSummaryTableScrollPane.setEnabled(false);
          _measurementTypeSummaryTableScrollPane.setEnabled(false);
          try
          {
            _config.setValue(CustomMeasurementFileConfigEnum.CUSTOMIZE_MEASUREMENT_OUTPUT, false);
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }
        }
        else if (e.getStateChange() == ItemEvent.DESELECTED)
        {
          _jointTypeButton.setEnabled(true);
          _measurementTypeButton.setEnabled(true);
          _summaryLabel.setEnabled(true);
          _jointTypeSummaryTableScrollPane.setEnabled(true);
          _measurementTypeSummaryTableScrollPane.setEnabled(true);
          try
          {
            _config.setValue(CustomMeasurementFileConfigEnum.CUSTOMIZE_MEASUREMENT_OUTPUT,true);
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                          ex,
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                          true);
          }
        }
      }
    });
  }

  private void updateDataOnScreen()
  {
    _enableGenerateMeasurementsXMLCheckBox.setSelected(_testExecution.isGenerateMeasurementsXMLEnabled());
    _setDumpZHeightCheckBox.setSelected(_testExecution.isDumpZHeightInMeasurementsEnabled());
    _setCustomizeMeasurementsXMLRadioButton.setSelected(_testExecution.isCustomizeMeasurementXMLEnable());
    _setDefaultMeasurementsXMLRadioButton.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
    _setCustomizeMeasurementsXMLRadioButton.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
    _jointTypeButton.setEnabled(_setCustomizeMeasurementsXMLRadioButton.isEnabled()&&_setCustomizeMeasurementsXMLRadioButton.isSelected());
    _measurementTypeButton.setEnabled(_setCustomizeMeasurementsXMLRadioButton.isEnabled()&&_setCustomizeMeasurementsXMLRadioButton.isSelected());
    _summaryLabel.setEnabled(_setCustomizeMeasurementsXMLRadioButton.isEnabled()&&_setCustomizeMeasurementsXMLRadioButton.isSelected());
    _jointTypeSummaryTableScrollPane.setEnabled(_setCustomizeMeasurementsXMLRadioButton.isEnabled()&&_setCustomizeMeasurementsXMLRadioButton.isSelected());
    _measurementTypeSummaryTableScrollPane.setEnabled(_setCustomizeMeasurementsXMLRadioButton.isEnabled()&&_setCustomizeMeasurementsXMLRadioButton.isSelected());
    _setDumpZHeightCheckBox.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
  }

  private void generateMeasurementsXMLCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetGenerateMeasurementsXML setGenerateMeasurementsXML = new ConfigSetGenerateMeasurementsXML(_enableGenerateMeasurementsXMLCheckBox.isSelected());
      _commandManager.execute(setGenerateMeasurementsXML);
      _setDefaultMeasurementsXMLRadioButton.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
      _setCustomizeMeasurementsXMLRadioButton.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
      _setDumpZHeightCheckBox.setEnabled(_enableGenerateMeasurementsXMLCheckBox.isSelected());
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  private void setDumpZHeightCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetDumpZHeightInMeasurementsXML setDumpZHeightInMeasurementsXML = new ConfigSetDumpZHeightInMeasurementsXML(_setDumpZHeightCheckBox.isSelected());
      _commandManager.execute(setDumpZHeightInMeasurementsXML);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  public void start()
  {
    super.startMenusAndToolBar();
    _mainUI.setStatusBarText("");
    ConfigObservable.getInstance().addObserver(this);
    updateDataOnScreen();
  }

  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
  }

  public boolean isReadyToFinish()
  {
    return true;
  }

   /**
   * XCR-3551 Custom Measurement XML
   * @author Janan Wong
   * depopulate measurement type based on joint type deselected in popup menu.
   */
  public void depopulateMeasurementTypeInPopupMenu(InspectionFamily inspectionFamily,JointTypeEnum jointType)
  { 
    Assert.expect(inspectionFamily != null);
    Assert.expect(jointType != null);
    for (MeasurementEnum measurementEnum : inspectionFamily.getMeasurementEnums(jointType))
    {
      _measurementTypeStringSet.remove(measurementEnum.toString());
    }
    
    for (int i = 0; i < inspectionFamily.getAlgorithms().size(); i++)
    {
      for (int j = 0; j < inspectionFamily.getAlgorithms().get(i).getComponentMeasurementEnums().size(); j++)
      {
        _measurementTypeStringSet.remove(inspectionFamily.getAlgorithms().get(i).getComponentMeasurementEnums().toArray()[j].toString());
      }
    }

    for (int i = 0; i < inspectionFamily.getAlgorithms().size(); i++)
    {
      for (int j = 0; j < inspectionFamily.getAlgorithms().get(i).getJointMeasurementEnums().size(); j++)
      {
        _measurementTypeStringSet.remove(inspectionFamily.getAlgorithms().get(i).getJointMeasurementEnums().toArray()[j].toString());
      }
    }
    
    //need to repopulate list in measurement popup menu
    for (Object items : _jointTypePopupMenu.getSelectedValuesList())
    {
      populateMeasurementTypeInPopupMenu(InspectionFamily.getInspectionFamily(JointTypeEnum.getJointTypeEnum(items.toString())),JointTypeEnum.getJointTypeEnum(items.toString()));
    }
  }
  
   /**
   * XCR-3551 Custom Measurement XML
   * @author Janan Wong
   * populate measurement type based on joint type selected in popup menu.
   */
  public void populateMeasurementTypeInPopupMenu(InspectionFamily inspectionFamily, JointTypeEnum jointType)
  {
    Assert.expect(inspectionFamily != null);
    Assert.expect(jointType != null);
    for (MeasurementEnum measurementEnum   : inspectionFamily.getMeasurementEnums(jointType))
    {
      _measurementTypeStringSet.add(measurementEnum.toString());
    }
    
    for (int i = 0; i < inspectionFamily.getAlgorithms().size(); i++)
    {
      for (int j = 0; j < inspectionFamily.getAlgorithms().get(i).getComponentMeasurementEnums().size(); j++)
      {
        _measurementTypeStringSet.add(inspectionFamily.getAlgorithms().get(i).getComponentMeasurementEnums().toArray()[j].toString());
      }
    }

    for (int i = 0; i < inspectionFamily.getAlgorithms().size(); i++)
    {
      for (int j = 0; j < inspectionFamily.getAlgorithms().get(i).getJointMeasurementEnums().size(); j++)
      {
        _measurementTypeStringSet.add(inspectionFamily.getAlgorithms().get(i).getJointMeasurementEnums().toArray()[j].toString());
      }
    }
    
    //To remove non customer visible measurements from the measuremnts type list
    for(MeasurementEnum measurementEnum : MeasurementEnum.getNonCustomerVisibleMeasurements())
    {
      _measurementTypeStringSet.remove(measurementEnum.toString());
    }   
  }
   
  /**
   * XCR-3551 Custom Measurement XML
   * @author Janan Wong   
   * populate selected measurement type in summary table and set customMeasurementXML.config file. 
   */
  public void populateSelectedMeasurementInTable()
  {
    _selectedMeasurementTypeSummary.clear();
    Collections.sort(_measurementTypePopupMenu.getSelectedValuesList());
    
    for (Object measurement : _measurementTypePopupMenu.getSelectedValuesList())
    {
      Vector<String> vector = new Vector<String>();
      vector.add(measurement.toString());
      _selectedMeasurementTypeSummary.add(vector);      
      try
      {
        _config.setValue(CustomMeasurementSetting.getInstance().getKeyToConfigEnumMap().get(measurement.toString().replace(" ", "").toLowerCase()), true);
        CustomMeasurementSetting.getInstance().addCustomSettingMeasurement(measurement.toString());
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
     
    for (Object measurements : _measurementTypePopupMenu.getDeselectedValuesLists())
    {
      try
      {
        _config.setValue(CustomMeasurementSetting.getInstance().getKeyToConfigEnumMap().get(measurements.toString().replace(" ", "").toLowerCase()), false);
        CustomMeasurementSetting.getInstance().removeCustomSettingMeasurement(measurements.toString());
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    
    _measurementTypeSummaryTableModel.fireTableDataChanged();
  }
}
