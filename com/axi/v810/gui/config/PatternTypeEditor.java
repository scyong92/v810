package com.axi.v810.gui.config;

import javax.swing.*;

import com.axi.v810.util.*;

/**
 * <p>Description: Table cell editor for results processing rule condition pattern types: simple or regex</p>
 * @author Laura Cormos
 */
class PatternTypeEditor extends DefaultCellEditor
{
  JComboBox _comboBox = null;
  private final String _SIMPLE = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY");
  private final String _REGEX = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_REGEX_KEY");

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with values
   */
  public PatternTypeEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_SIMPLE);
    _comboBox.addItem(_REGEX);
  }
}
