package com.axi.v810.gui.config;

import javax.swing.*;
import com.axi.v810.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class DeveloperDebugConfigTableCellEditor extends DefaultCellEditor
{
  //Config vlue strings
  private final String _TRUE = StringLocalizer.keyToString("GUI_CONFIG_VALUE_TRUE_KEY");
  private final String _FALSE = StringLocalizer.keyToString("GUI_CONFIG_VALUE_FALSE_KEY");

  JComboBox _comboBox = null;

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with the shape values
   */
  public DeveloperDebugConfigTableCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    _comboBox.addItem(_TRUE);
    _comboBox.addItem(_FALSE);
 }
}
