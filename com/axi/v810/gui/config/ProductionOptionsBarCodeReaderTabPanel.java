package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ProductionOptionsBarCodeReaderTabPanel extends AbstractTaskPanel implements Observer
{
  private java.util.List<String> _bcrConfigurationNamesList;
  private BarcodeReaderManager _bcrManager;
  private String _currentSelectedConfig;
  private String _defaultNumScanners;
  private String _defaultErrorMessage = "";
  private boolean _bcrConfigurationIsDirty = false;
  private boolean _stopTestRequest = false;
  private boolean _savingConfigurationInProgress = false;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();

  // GUI elements
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JCheckBox _enableAutomaticBrcCheckbox = new JCheckBox();
  private JPanel _configurationPanel = new JPanel();
  private BorderLayout _configurationPanelBorderLayout = new BorderLayout();
  private JPanel _configurationNamePanel = new JPanel();
  private BorderLayout _configurationNamePanelBorderLayout = new BorderLayout();
  private JPanel _configurationSelectPanel = new JPanel();
  private FlowLayout _configurationSelectPanelFlowLayout = new FlowLayout();
  private JLabel _configurationSelectNameLabel = new JLabel();
  private JComboBox _configurationSelectNameCombobox = new JComboBox();
  private ActionListener _configurationSelectComboboxActionListener;
  private JPanel _configurationSelectActionsWrapperPanel = new JPanel();
  private FlowLayout _configurationSelectActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _configurationSelectActionsPanel = new JPanel();
  private GridLayout _configurationSelectActionsPanelGridLayout = new GridLayout();
  private JButton _newConfigurationButton = new JButton();
  private JButton _saveConfigurationButton = new JButton();
  private JButton _saveAsConfigurationButton = new JButton();
  private JButton _deleteConfigurationButton = new JButton();
  private JPanel _settingsPanel = new JPanel();
  private BorderLayout _settingsPanelBorderLayout = new BorderLayout();
  private JSplitPane _settingPanelSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
  private JPanel _bcrSetupPanel = new JPanel();
  private BorderLayout _bcrSetupPanelBorderLayout = new BorderLayout();
  private JPanel _codesPanel = new JPanel();
  private BorderLayout _codesPanelBorderLayout = new BorderLayout();
  private JPanel _scannersPanel = new JPanel();
  private FlowLayout _scannersPanelFlowLayout = new FlowLayout();
  private JLabel _numScannersLabel = new JLabel();
  private NumericTextField _numScannersTextField = new NumericTextField(null, 2);
  private JScrollPane _scannerCodesScrollPane = new JScrollPane();
  private BarCodeScannerCodesTable _scannerCodesTable = new BarCodeScannerCodesTable();
  private BarCodeScannerCodesTableModel _scannerCodesTableModel;
  private JPanel _fixedSetupWrapperPanel = new JPanel(new BorderLayout());
  private JPanel _fixedSetupPanel = new JPanel();
  private BoxLayout _fixedSetupPanelBoxLayout;
  private JPanel _communicationSetupPanel = new JPanel();
  private PairLayout _communicationSetupPanelPairLayout = new PairLayout();
  private JLabel _serialPortNumberLabel = new JLabel();
  private JComboBox _serialPortNameCombobox = new JComboBox();
  private ActionListener _serialPortNameComboboxActionListener;
  private JLabel _bitsPerSecondLabel = new JLabel();
  private JComboBox _bitsPerSecondCombobox = new JComboBox();
  private ActionListener _bitsPerSecondComboboxActionListener;
  private JLabel _dataBitsLabel = new JLabel();
  private JComboBox _dataBitsCombobox = new JComboBox();
  private ActionListener _dataBitsComboboxActionListener;
  private JLabel _parityLabel = new JLabel();
  private JComboBox _parityCombobox = new JComboBox();
  private ActionListener _parityComboboxActionListener;
  private JLabel _stopBitsLabel = new JLabel();
  private JComboBox _stopBitsCombobox = new JComboBox();
  private ActionListener _stopBitsComboboxActionListener;
  private JPanel _messageSetupPanel = new JPanel();
  private PairLayout _messageSetupPanelPairLayout = new PairLayout();
  private JLabel _noReadErrorLabel = new JLabel();
  private JTextField _noReadErrorTextField = new JTextField(15);
  private JLabel _badSymbolErrorLabel = new JLabel();
  private JTextField _badSymbolErrorTextField = new JTextField(15);
  private JLabel _noLabelErrorLabel = new JLabel();
  private JTextField _noLabelErrorTextField = new JTextField(15);
  private JPanel _brcTestPanel = new JPanel();
  private BorderLayout _brcTestPanelBorderLayout = new BorderLayout();
  private JScrollPane _brcTestScrollPane = new JScrollPane();
  private BarCodeScannerTestTable _scannerTestTable = new BarCodeScannerTestTable();
  private BarCodeScannerTestTableModel _scannerTestTableModel = new BarCodeScannerTestTableModel();
  private JPanel _testActionWrapperPanel = new JPanel();
  private FlowLayout _testActionWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _testActionsPanel = new JPanel();
  private GridLayout _testActionsPanelGridLayout = new GridLayout(2,1);
  private JButton _testButton = new JButton();
  private JPanel _advancedSettingsPanel = new JPanel();
  private FlowLayout _advancedSettingsPanelFlowLayout = new FlowLayout();
  private JButton _advancedConfigurationButton = new JButton();
  private JPanel _selectBcrConfigurationToUsePanel = new JPanel();
  private FlowLayout _selectBcrConfigurationToUsePanelFlowLayout = new FlowLayout();
  private JLabel _selectBcrConfigurationToUseLabel = new JLabel();
  private JComboBox _bcrConfigurationToUseComboBox = new JComboBox();
  private ActionListener _configurationToUseComboboxActionListener;

  private JButton _connectButton = new JButton();
  private BooleanLock _getSerialNumberBooleanLock = new BooleanLock(false);
  /**
  * Useful ONLY for the JBuilder designer.  Not for use with normal code.
  * @author Laura Cormos
  */
 private ProductionOptionsBarCodeReaderTabPanel()
 {
   Assert.expect(java.beans.Beans.isDesignTime());
   try
   {
     jbInit();
   }
   catch (Exception exception)
   {
     exception.printStackTrace();
   }
 }

  /**
   * @author Laura Cormos
   */
  public ProductionOptionsBarCodeReaderTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
      _bcrManager = BarcodeReaderManager.getInstance();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    // set up panel GUI components
    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _centerPanel.add(_enableAutomaticBrcCheckbox, BorderLayout.NORTH);
    _centerPanel.add(_configurationPanel, BorderLayout.CENTER);
    _centerPanel.add(_selectBcrConfigurationToUsePanel, BorderLayout.SOUTH);

    _enableAutomaticBrcCheckbox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_AUTOMATIC_BCR_CHECKBOX_KEY"));
    _enableAutomaticBrcCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableAutomaticBrcCheckbox_actionPerformed(e);
      }
    });

    _configurationPanel.setLayout(_configurationPanelBorderLayout);
    _configurationPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_CONFIGURATION_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _configurationPanel.add(_configurationNamePanel, BorderLayout.NORTH);
    _configurationPanel.add(_settingsPanel, BorderLayout.CENTER);

    _configurationNamePanel.setLayout(_configurationNamePanelBorderLayout);
    _configurationNamePanel.add(_configurationSelectPanel, BorderLayout.WEST);
    _configurationNamePanel.add(_configurationSelectActionsWrapperPanel, BorderLayout.EAST);

    _configurationSelectPanel.setLayout(_configurationSelectPanelFlowLayout);
    _configurationSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _configurationSelectPanel.add(_configurationSelectNameLabel);
    _configurationSelectPanel.add(_configurationSelectNameCombobox);

    _configurationSelectNameLabel.setText(StringLocalizer.keyToString("CFGUI_NAME_KEY"));

    _configurationSelectComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configurationSelectNameCombobox_actionPerformed(e);
      }
    };

    _configurationSelectNameCombobox.addActionListener(_configurationSelectComboboxActionListener);

    _configurationSelectActionsWrapperPanel.setLayout(_configurationSelectActionsWrapperPanelFlowLayout);
    _configurationSelectActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _configurationSelectActionsWrapperPanel.add(_configurationSelectActionsPanel);
    _configurationSelectActionsWrapperPanel.add(_advancedSettingsPanel);


    _configurationSelectActionsPanel.setLayout(_configurationSelectActionsPanelGridLayout);
    _configurationSelectActionsPanelGridLayout.setColumns(4);
    _configurationSelectActionsPanelGridLayout.setHgap(10);
    _configurationSelectActionsPanelGridLayout.setRows(0);
    _configurationSelectActionsPanelGridLayout.setVgap(10);
    _configurationSelectActionsPanel.add(_newConfigurationButton);
    _configurationSelectActionsPanel.add(_saveConfigurationButton);
    _configurationSelectActionsPanel.add(_saveAsConfigurationButton);
    _configurationSelectActionsPanel.add(_deleteConfigurationButton);

    _newConfigurationButton.setText(StringLocalizer.keyToString("CFGUI_NEW_BUTTON_KEY"));
    _newConfigurationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        newConfigButton_actionPerformed(e);
      }
    });

    _saveConfigurationButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_BUTTON_KEY"));
    _saveConfigurationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveCurrentConfiguration();
      }
    });

    _saveAsConfigurationButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_AS_BUTTON_KEY"));
    _saveAsConfigurationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveAsConfiguration();
      }
    });

    _deleteConfigurationButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteConfigurationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteConfigButton_actionPerformed(e);
      }
    });

    _advancedSettingsPanel.setLayout(_advancedSettingsPanelFlowLayout);
    _advancedSettingsPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _advancedSettingsPanel.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
    _advancedSettingsPanel.add(_advancedConfigurationButton);

    _advancedConfigurationButton.setText(StringLocalizer.keyToString("CFGUI_ADVANCED_SETTINGS_BUTTON_KEY"));
    _advancedConfigurationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        advancedConfigurationButton_actionPerformed(e);
      }
    });

    _settingsPanel.setLayout(_settingsPanelBorderLayout);
    _settingsPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_SETTINGS_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _settingsPanel.add(_settingPanelSplitPane, BorderLayout.CENTER);

    _settingPanelSplitPane.setBorder(null);
    _settingPanelSplitPane.add(_bcrSetupPanel, JSplitPane.TOP);
    _settingPanelSplitPane.add(_brcTestPanel, JSplitPane.BOTTOM);

    _bcrSetupPanel.setLayout(_bcrSetupPanelBorderLayout);
//    _bcrSetupPanel.setMinimumSize(new Dimension(0,100));
    _bcrSetupPanel.add(_codesPanel, BorderLayout.CENTER);
    _bcrSetupPanel.add(_fixedSetupWrapperPanel, BorderLayout.EAST);


    _codesPanel.setLayout(_codesPanelBorderLayout);
    _codesPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_SCANNER_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _codesPanel.add(_scannersPanel, BorderLayout.NORTH);
    _codesPanel.add(_scannerCodesScrollPane, BorderLayout.CENTER);

    _scannersPanel.setLayout(_scannersPanelFlowLayout);
    _scannersPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _scannersPanel.add(_numScannersLabel);
    _scannersPanel.add(_numScannersTextField);

    _numScannersLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_NUM_SCANNERS_LABEL_KEY"));

    NumericRangePlainDocument  numScannersTextFieldDoc = new NumericRangePlainDocument();
    numScannersTextFieldDoc.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _numScannersTextField.setDocument(numScannersTextFieldDoc);

//    StringBuffer decimalFormat = new StringBuffer("########0");
    DecimalFormat decimalFormat = new DecimalFormat();
    decimalFormat.setParseIntegerOnly(true);
    _numScannersTextField.setFormat(decimalFormat);

    _numScannersTextField.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        numScannersTextField_actionPerformed();
      }
    });

    _numScannersTextField.addFocusListener(new FocusAdapter()
     {
       public void focusLost(FocusEvent e)
       {
         numScannersTextField_actionPerformed();
       }
     });

    _scannerCodesTableModel = new BarCodeScannerCodesTableModel(this);
    _scannerCodesTable.getTableHeader().setReorderingAllowed(false);
    _scannerCodesTable.setModel(_scannerCodesTableModel);
    _scannerCodesTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

    _scannerCodesScrollPane.getViewport().add(_scannerCodesTable);

    _fixedSetupWrapperPanel.add(_fixedSetupPanel, BorderLayout.NORTH);

    _fixedSetupPanelBoxLayout = new BoxLayout(_fixedSetupPanel, BoxLayout.PAGE_AXIS);
    _fixedSetupPanel.setLayout(_fixedSetupPanelBoxLayout);
    _fixedSetupPanel.add(_communicationSetupPanel);
    _fixedSetupPanel.add(_messageSetupPanel);

    _communicationSetupPanel.setLayout(_communicationSetupPanelPairLayout);
    _communicationSetupPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_COMMUNICATION_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _communicationSetupPanel.add(_serialPortNumberLabel);
    _communicationSetupPanel.add(_serialPortNameCombobox);
    _communicationSetupPanel.add(_bitsPerSecondLabel);
    _communicationSetupPanel.add(_bitsPerSecondCombobox);
    _communicationSetupPanel.add(_dataBitsLabel);
    _communicationSetupPanel.add(_dataBitsCombobox);
    _communicationSetupPanel.add(_parityLabel);
    _communicationSetupPanel.add(_parityCombobox);
    _communicationSetupPanel.add(_stopBitsLabel);
    _communicationSetupPanel.add(_stopBitsCombobox);

    _serialPortNumberLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_SERIAL_PORT_NUMBER_LABEL_KEY"));
    _serialPortNameComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        serialPortNumberCombobox_actionPerformed(e);
      }
    };
    _serialPortNameCombobox.addActionListener(_serialPortNameComboboxActionListener);

    _bitsPerSecondLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_BITS_PER_SECOND_LABEL_KEY"));
    _bitsPerSecondComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bitsPerSecondCombobox_actionPerformed(e);
      }
    };
    _bitsPerSecondCombobox.addActionListener(_bitsPerSecondComboboxActionListener);

    _dataBitsLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_DATA_BITS_LABEL_KEY"));
    _dataBitsComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dataBitsCombobox_actionPerformed(e);
      }
    };
    _dataBitsCombobox.addActionListener(_dataBitsComboboxActionListener);

    _parityLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_PARITY_LABEL_KEY"));
    _parityComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        parityCombobox_actionPerformed(e);
      }
    };
    _parityCombobox.addActionListener(_parityComboboxActionListener);

    _stopBitsLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_STOP_BITS_LABEL_KEY"));
    _stopBitsComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        stopBitsCombobox_actionPerformed(e);
      }
    };
    _stopBitsCombobox.addActionListener(_stopBitsComboboxActionListener);

    _messageSetupPanel.setLayout(_messageSetupPanelPairLayout);
    _messageSetupPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_MESSAGE_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _messageSetupPanel.add(_noReadErrorLabel);
    _messageSetupPanel.add(_noReadErrorTextField);
    _messageSetupPanel.add(_badSymbolErrorLabel);
    _messageSetupPanel.add(_badSymbolErrorTextField);
    _messageSetupPanel.add(_noLabelErrorLabel);
    _messageSetupPanel.add(_noLabelErrorTextField);

    _noReadErrorLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_NO_READ_ERROR_LABEL_KEY"));
    _noReadErrorTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noReadErrorTextField_setValue();
      }
    });
    _noReadErrorTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        noReadErrorTextField_setValue();
      }
    });


    _badSymbolErrorLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_BAD_SYMBOL_ERROR_LABEL_KEY"));
    _badSymbolErrorTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        badSymbolErrorTextField_setValue();
      }
    });
    _badSymbolErrorTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        badSymbolErrorTextField_setValue();
      }
    });

    _noLabelErrorLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_NO_LABEL_ERROR_LABEL_KEY"));
    _noLabelErrorTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        noLabelErrorTextField_setValue();
      }
    });
    _noLabelErrorTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        noLabelErrorTextField_setValue();
      }
    });

    _brcTestPanel.setLayout(_brcTestPanelBorderLayout);
    _brcTestPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_TEST_COMMUNICATION_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _brcTestPanel.add(_brcTestScrollPane, BorderLayout.CENTER);
    _brcTestPanel.add(_testActionWrapperPanel, BorderLayout.WEST);

    _scannerTestTable.getTableHeader().setReorderingAllowed(false);
    _scannerTestTable.setModel(_scannerTestTableModel);
    _brcTestScrollPane.getViewport().add(_scannerTestTable);

    _testActionWrapperPanel.setLayout(_testActionWrapperPanelFlowLayout);
    _testActionWrapperPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _testActionWrapperPanel.add(_testActionsPanel);

    _testActionsPanel.setLayout(_testActionsPanelGridLayout);
    //Siew Yeng - XCR1728 - Able to disconnect barcode reader from V810
    _testActionsPanel.add(_connectButton);
    _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY"));
    _connectButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        connectButton_actionPerformed(e);
      }
    });
    
    _testActionsPanel.add(_testButton);

    _testButton.setText(StringLocalizer.keyToString("CFGUI_BCR_TEST_BUTTON_KEY"));
    _testButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        testButton_actionPerformed(e);
      }
    });

    _selectBcrConfigurationToUsePanel.setLayout(_selectBcrConfigurationToUsePanelFlowLayout);
    _selectBcrConfigurationToUsePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _selectBcrConfigurationToUsePanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_SELECT_CONFIGURATION_TO_USE_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));

    _selectBcrConfigurationToUsePanel.add(_selectBcrConfigurationToUseLabel);
    _selectBcrConfigurationToUsePanel.add(_bcrConfigurationToUseComboBox);

    _selectBcrConfigurationToUseLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_SELECT_CONFIG_TO_USE_LABEL_KEY"));

    _configurationToUseComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        configurationToUseNameCombobox_actionPerformed(e);
      }
    };
    _bcrConfigurationToUseComboBox.addActionListener(_configurationToUseComboboxActionListener);

    // define menus and toolBar items to be added later

  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ProductionOptionsBarCodeReaderTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");

    populateWithConfigData();
    
    //Siew Yeng - XCR1728 - Able to disconnect barcode reader from V810
    if(_bcrManager.isSerialPortOpened())
    {
      _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_DISCONNECT_BUTTON_KEY"));
      _testButton.setEnabled(true);
    }
    else
      _testButton.setEnabled(false);

    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
    _hardwareObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    if (_scannerCodesTable.isEditing())
      _scannerCodesTable.getCellEditor().cancelCellEditing();
    if (_scannerTestTable.isEditing())
      _scannerTestTable.getCellEditor().cancelCellEditing();

    return saveConfigurationIfNecessary();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    //Khaw Chek Hau - XCR2749 - Software assert when click on barcode reader tab
    disconnectSerialPortForTestCommunication();
    
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);
    _hardwareObservable.deleteObserver(this);
//    System.out.println("ProductionOptionsBarCodeReaderTabPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void setTestTablePreferredSize()
  {
    int padding = 400;
    int origTableWidth = _scannerTestTable.getMinimumSize().width;
    int scrollHeight = _brcTestScrollPane.getPreferredSize().height;
//    int tableHeight = _rulesTable.getPreferredSize().height;
//    int tableHeight = _scannerTestTable.getMaximumHeight();
    int prefHeight = scrollHeight;
//    if (tableHeight < scrollHeight)
//    {
//      prefHeight = tableHeight;
//      Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
//      _scannerTestTable.setPreferredScrollableViewportSize(prefScrollSize);
//    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateWithConfigData()
  {
    // set the enable check box based on the config setting
    // before we can query the bcrmanager about any static configuration values we need to load the configuration
    // pointed to by the BARCODE_READER_CONFIGURATION_NAME_TO_USE software config enum. This is necessary because
    // the initial values for all barcode reader enums are set from the aforementioned software config enum

    // the next inquiries on bcrmanager have to be limited to items from software config or minimum/maximum values
    //  until a load of the barcode reader configuration to edit is done
    _defaultNumScanners = String.valueOf(_bcrManager.getMinimumBarcodeReaderNumberOfScanners()); // generic value
    _enableAutomaticBrcCheckbox.setSelected(_bcrManager.getBarcodeReaderAutomaticReaderEnabled()); // sw.config
    if (_enableAutomaticBrcCheckbox.isSelected() == false)
      setEnabledAllControls(false);
    // load the list of configuration files from disk
    _bcrConfigurationNamesList = _bcrManager.getExistingBarcodeReaderConfigurationNames(); // file system
    if (_bcrConfigurationNamesList.isEmpty())
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("CFGUI_BCR_NO_BARCODE_CONFIG_FOUND_MESSAGE_KEY"),
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                    JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    populateSerialNumberCombobox(); // generic values
    populateBitsPerSecondCombobox(); // generic values
    populateDataBitsCombobox(); // generic values
    populateParityCombobox(); // generic values
    populateStopBitsCombobox(); // generic values

    //make sure the dirty bit is not set
    setConfigurationDirtyFlag(false);

    // populate combobox with list of names and load up the file pointed to by the 'to edit" swconfigenum
    populateConfigurationSelectNameCombobox();
    String configurationToEdit = _bcrManager.getBarcodeReaderConfigurationNameToEdit();

    if (_bcrConfigurationNamesList.contains(configurationToEdit) == false)
    {
      // somehow the file does not exist on disk anymore, give a warning and move on
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_MISSING_BARCODE_CONFIG_FILE_MESSAGE_KEY",
                                                                                    new Object[]{configurationToEdit})),
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                    JOptionPane.WARNING_MESSAGE);
      // set config to edit to the next item on the list
      Object selectedItem = _configurationSelectNameCombobox.getSelectedItem();
      Assert.expect(selectedItem instanceof String);
      configurationToEdit = (String)selectedItem;
    }
    Assert.expect(_bcrConfigurationNamesList.contains(configurationToEdit));
    _configurationSelectNameCombobox.setSelectedItem(configurationToEdit); // this does the GUI load as well

    _numScannersTextField.setText(String.valueOf(_bcrManager.getBarcodeReaderNumberOfScanners()));
    populateScannerTable();
    populateTestTable();

    // set the configuration to use controls based on the config setting
    populateConfigurationToUseCombobox();
    String configurationToUse;
    configurationToUse = _bcrManager.getBarcodeReaderConfigurationNameToUse();
    if (_bcrConfigurationNamesList.contains(configurationToUse) == false)
    {
      // somehow the file does not exist on disk anymore, give a warning and ask the user to choose a
      // different configuration to be used in production
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_MISSING_PRODUCTION_USE_BARCODE_CONFIG_FILE_MESSAGE_KEY",
                                                                                    new Object[]{configurationToUse})),
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                    JOptionPane.WARNING_MESSAGE);
      Object[] possibleValues = new Object[_bcrConfigurationNamesList.size()];
      int i = 0;
      // at this point we are guaranteed that there is at least one configuration file present on disk
      for (String name : _bcrConfigurationNamesList)
      {
        possibleValues[i++] = name;
      }
      Object newConfigToUse = JOptionPane.showInputDialog(_mainUI,
                                                          StringLocalizer.keyToString("CFGUI_SELECT_BCR_CONFIGURATION_FOR_PRODUCTION_USE_KEY"),
                                                          StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                                          JOptionPane.ERROR_MESSAGE,
                                                          null,
                                                          possibleValues,
                                                          possibleValues[0]);
      if (newConfigToUse == null) // user cancelled the dialog
      {
        // disable automatic BCR use and set the configuration to use to the same as what configuration
        // to edit is currently set to. Sw.config files needs to have a valid value or else it will error out
        // next time the x6000 app is started.
        try
        {
          _bcrManager.setBarcodeReaderAutomaticReaderEnabled(false);
          _bcrManager.setBarcodeReaderConfigurationNameToUse(configurationToEdit);
          
          //Siew Yeng - XCR-2969 - UI not update when disable automatic barcode reader 
          _enableAutomaticBrcCheckbox.setSelected(false);
          setEnabledAllControls(false);
          configurationToUse = configurationToEdit;
        }
        catch (DatastoreException ex)
        {
          showDatastoreError(ex);
        }
      }
      else
      {
        Assert.expect(newConfigToUse instanceof String);
        
        //Siew Yeng - XCR-2969 - software.config does not update
        try 
        {
          configurationToUse = (String)newConfigToUse;
          _bcrManager.setBarcodeReaderConfigurationNameToUse(configurationToUse);
        } 
        catch (DatastoreException ex) 
        {
          showDatastoreError(ex);
        }
      }
    }
    _bcrConfigurationToUseComboBox.removeActionListener(_configurationToUseComboboxActionListener);
    _bcrConfigurationToUseComboBox.setSelectedIndex(_bcrConfigurationNamesList.indexOf(configurationToUse));
    _bcrConfigurationToUseComboBox.addActionListener(_configurationToUseComboboxActionListener);

    setConfigurationDirtyFlag(false);
    setupTableEditors();
  }

  /**
   * @author Laura Cormos
   */
  private void populateConfigurationSelectNameCombobox()
  {
    Assert.expect(_bcrConfigurationNamesList != null);

    _configurationSelectNameCombobox.removeActionListener(_configurationSelectComboboxActionListener);
    _configurationSelectNameCombobox.removeAllItems();
    for (String name : _bcrConfigurationNamesList)
      _configurationSelectNameCombobox.addItem(name);
    _configurationSelectNameCombobox.addActionListener(_configurationSelectComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateConfigurationToUseCombobox()
  {
    Assert.expect(_bcrConfigurationNamesList != null);

    _bcrConfigurationToUseComboBox.removeActionListener(_configurationToUseComboboxActionListener);
    _bcrConfigurationToUseComboBox.removeAllItems();
    for (String name : _bcrConfigurationNamesList)
      _bcrConfigurationToUseComboBox.addItem(name);
    _bcrConfigurationToUseComboBox.addActionListener(_configurationToUseComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  public void setEnabledAllControls(boolean enabled)
  {
    _configurationSelectNameLabel.setEnabled(enabled);
    _configurationSelectNameCombobox.setEnabled(enabled);
    _newConfigurationButton.setEnabled(enabled);
    _saveConfigurationButton.setEnabled(enabled);
    _saveAsConfigurationButton.setEnabled(enabled);
    _deleteConfigurationButton.setEnabled(enabled);
    _numScannersLabel.setEnabled(enabled);
    _numScannersTextField.setEnabled(enabled);
    _scannerCodesScrollPane.setEnabled(enabled);
    _scannerCodesTable.setEnabled(enabled);
    _serialPortNumberLabel.setEnabled(enabled);
    _serialPortNameCombobox.setEnabled(enabled);
    _bitsPerSecondLabel.setEnabled(enabled);
    _bitsPerSecondCombobox.setEnabled(enabled);
    _dataBitsLabel.setEnabled(enabled);
    _dataBitsCombobox.setEnabled(enabled);
    _parityLabel.setEnabled(enabled);
    _parityCombobox.setEnabled(enabled);
    _stopBitsLabel.setEnabled(enabled);
    _stopBitsCombobox.setEnabled(enabled);
    _noReadErrorLabel.setEnabled(enabled);
    _noReadErrorTextField.setEnabled(enabled);
    _badSymbolErrorLabel.setEnabled(enabled);
    _badSymbolErrorTextField.setEnabled(enabled);
    _noLabelErrorLabel.setEnabled(enabled);
    _noLabelErrorTextField.setEnabled(enabled);
    _brcTestScrollPane.setEnabled(enabled);
    _scannerTestTable.setEnabled(enabled);
    //Siew Yeng - XCR1728 - Able to disconnect barcode reader from V810
    _connectButton.setEnabled(enabled);
    if(_connectButton.getText().equals(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY")) && enabled)
      _testButton.setEnabled(false);
    else
      _testButton.setEnabled(enabled);
    _advancedConfigurationButton.setEnabled(enabled);
    _selectBcrConfigurationToUseLabel.setEnabled(enabled);
    _bcrConfigurationToUseComboBox.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void enableAutomaticBrcCheckbox_actionPerformed(ActionEvent e)
  {
    boolean enabled = _enableAutomaticBrcCheckbox.isSelected();

    try
    {
      _commandManager.execute(new ConfigSetEnableAutomaticBarcodeCommand(enabled, this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void checkProtectedConfigurations()
  {
    Object object = _configurationSelectNameCombobox.getSelectedItem();
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String)object;
      // if the file is protected and autmatic BCR is enabled, set the save and delete buttons accordingly
      if (_enableAutomaticBrcCheckbox.isSelected())
      {
        _saveConfigurationButton.setEnabled(!(_bcrManager.isProtected(selectedName)));
        _deleteConfigurationButton.setEnabled(!(_bcrManager.isProtected(selectedName)));
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void configurationSelectNameCombobox_actionPerformed(ActionEvent e)
  {
    if (saveConfigurationIfNecessary() == false)
      return;
    
    //Khaw Chek Hau - XCR2749 - Software assert when click on barcode reader tab
    disconnectSerialPortForTestCommunication();
    
    Object object = _configurationSelectNameCombobox.getSelectedItem();
    Assert.expect(object instanceof String);
    String selectedConfig = (String)object;
    checkProtectedConfigurations();
    try
    {
      _bcrManager.load(selectedConfig);
      //Khaw Chek Hau - XCR2749 - Software assert when click on barcode reader tab
      _currentSelectedConfig = selectedConfig;
      updateConfigData(selectedConfig);
      clearUndoStack();
//      _currentSelectedConfig = selectedConfig;
      setConfigurationDirtyFlag(false);
      resetStatusBar();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void updateConfigData(String selectedConfig) throws DatastoreException
  {
    _numScannersTextField.setText(String.valueOf(_bcrManager.getBarcodeReaderNumberOfScanners()));
    populateScannerTable();
    populateTestTable();
    // set serial port number
    String setting = _bcrManager.getBarcodeReaderSerialPortName();
    
    //Khaw Chek Hau - XCR2749 - Software assert when click on barcode reader tab
    if (_bcrManager.getValidBarcodeReaderSerialPortNames().contains(setting) == false)
    {
      throw new InvalidValueDatastoreException(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME.getKey().toString(), 
                                               setting, _bcrManager.getBarcodeReaderConfigFullPath(selectedConfig));
    }
    
    Assert.expect(_bcrManager.getValidBarcodeReaderSerialPortNames().contains(setting));
    _serialPortNameCombobox.setSelectedItem(setting);
    // set bits per second
    setting = _bcrManager.getBarcodeReaderRs232BitsPerSecond();
    Assert.expect(_bcrManager.getValidBarcodeReaderRs232BitsPerSecond().contains(setting));
    _bitsPerSecondCombobox.setSelectedItem(setting);
    //set data bits
    setting = _bcrManager.getBarcodeReaderRs232DataBits();
    Assert.expect(_bcrManager.getValidBarcodeReaderRs232DataBits().contains(setting));
    _dataBitsCombobox.setSelectedItem(setting);
    // set parity
    setting = _bcrManager.getBarcodeReaderRs232Parity();
    Assert.expect(_bcrManager.getValidBarcodeReaderRs232Parity().contains(setting));
    _parityCombobox.setSelectedItem(setting);
    // set stop bits
    setting = _bcrManager.getBarcodeReaderRs232StopBits();
    Assert.expect(_bcrManager.getValidBarcodeReaderRs232StopBits().contains(setting));
    _stopBitsCombobox.setSelectedItem(setting);
    // set no read error message
    setting = _bcrManager.getBarcodeReaderNoReadMessage();
    _noReadErrorTextField.setText(setting);
    // set bad symbol error message
    setting = _bcrManager.getBarcodeReaderBadSymbolMessage();
    _badSymbolErrorTextField.setText(setting);
    // set no symbol error message
    setting = _bcrManager.getBarcodeReaderNoLabelMessage();
    _noLabelErrorTextField.setText(setting);
  }

  /**
   * @author Laura Cormos
   */
  private void populateScannerTable()
  {
    _scannerCodesTableModel.setData();
  }

/**
 * @author Laura Cormos
 */
private void populateTestTable()
{
  String numString = _numScannersTextField.getText();
  int numScanners = Integer.parseInt(numString);
  java.util.List<String> defaultTestCodes = new ArrayList<String>();
  for (int i = 0; i < numScanners; i++)
    defaultTestCodes.add(StringLocalizer.keyToString("CFGUI_BCR_BARCODE_READER_NOT_TESTED_MESSAGE_KEY"));
  _scannerTestTableModel.setData(defaultTestCodes);
}
  /**
   * @author Laura Cormos
   */
  private void populateSerialNumberCombobox()
  {
    _serialPortNameCombobox.removeActionListener(_serialPortNameComboboxActionListener);
    java.util.List<String> allValidItems = _bcrManager.getValidBarcodeReaderSerialPortNames();
    _serialPortNameCombobox.removeAllItems();
    for (String item : allValidItems)
      _serialPortNameCombobox.addItem(item);
    _serialPortNameCombobox.addActionListener(_serialPortNameComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateBitsPerSecondCombobox()
  {
    _bitsPerSecondCombobox.removeActionListener(_bitsPerSecondComboboxActionListener);
    java.util.List<String> allValidItems = _bcrManager.getValidBarcodeReaderRs232BitsPerSecond();
    _bitsPerSecondCombobox.removeAllItems();
    for (String item : allValidItems)
      _bitsPerSecondCombobox.addItem(item);
    _bitsPerSecondCombobox.addActionListener(_bitsPerSecondComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateDataBitsCombobox()
  {
    _dataBitsCombobox.removeActionListener(_dataBitsComboboxActionListener);
    java.util.List<String> allValidItems = _bcrManager.getValidBarcodeReaderRs232DataBits();
    _dataBitsCombobox.removeAllItems();
    for (String item : allValidItems)
      _dataBitsCombobox.addItem(item);
    _dataBitsCombobox.addActionListener(_dataBitsComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateParityCombobox()
  {
    _parityCombobox.removeActionListener(_parityComboboxActionListener);
    java.util.List<String> allValidItems = _bcrManager.getValidBarcodeReaderRs232Parity();
    _parityCombobox.removeAllItems();
    for (String item : allValidItems)
      _parityCombobox.addItem(item);
    _parityCombobox.addActionListener(_parityComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateStopBitsCombobox()
  {
    _stopBitsCombobox.removeActionListener(_stopBitsComboboxActionListener);
    java.util.List<String> allValidItems = _bcrManager.getValidBarcodeReaderRs232StopBits();
    _stopBitsCombobox.removeAllItems();
    for (String item : allValidItems)
      _stopBitsCombobox.addItem(item);
    _stopBitsCombobox.addActionListener(_stopBitsComboboxActionListener);
  }

   /**
   * @author Laura Cormos
   */
  private String getLegalFileName(String newConfigurationFileName)
  {
    Assert.expect(newConfigurationFileName != null);

    String whiteSpacePattern = "^\\s*$";
    java.util.List<String> tempConfigNamesList = new ArrayList<String>();
    for (String name : _bcrConfigurationNamesList)
      tempConfigNamesList.add(name.toUpperCase());

    String tempRuleSetName = newConfigurationFileName.toUpperCase();
    while (newConfigurationFileName != null &&
           (tempConfigNamesList.contains(tempRuleSetName) ||
            FileName.hasIllegalChars(newConfigurationFileName) ||
            Pattern.matches(whiteSpacePattern, newConfigurationFileName) ||
            _bcrManager.isProtected(newConfigurationFileName)))
    {
      if (tempConfigNamesList.contains(tempRuleSetName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_FILE_EXISTS_MESSAGE_KEY",
                                                                   new Object[]{newConfigurationFileName})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (FileName.hasIllegalChars(newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_ILLEGAL_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName, FileName.getIllegalChars()})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (Pattern.matches(whiteSpacePattern, newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_ILLEGAL_EMPTY_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (_bcrManager.isProtected(newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_PROTECTED_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName, _bcrManager.getProtectedIndicator()})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;

      }
    }
    return newConfigurationFileName;
  }

  /**
   * @author Laura Cormos
   */
  private void newConfigButton_actionPerformed(ActionEvent e)
  {
   // first check to see if the current configuration has been modified and needs to be saved
   if (saveConfigurationIfNecessary() == false)
     return;

   // next ask for the new configuration name
   String newConfigurationName = JOptionPane.showInputDialog(_mainUI, StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"), JOptionPane.INFORMATION_MESSAGE);
    if (newConfigurationName != null) // null means the dialog was canceled
    {
      newConfigurationName = getLegalFileName(newConfigurationName);
      if (newConfigurationName == null)
          return;

      try
      {
        // create a new configuration file with the given name
        _bcrManager.newConfig(newConfigurationName);

        // add new configuration name to the select combobox
        _configurationSelectNameCombobox.addItem(newConfigurationName);
        _configurationSelectNameCombobox.setSelectedItem(newConfigurationName);
        _bcrConfigurationNamesList.add(newConfigurationName);

        // add new configuration name to the 'config to use' combobox
        _bcrConfigurationToUseComboBox.addItem(newConfigurationName);

        // clear all fields
        _configObservable.setEnabled(false);
        _numScannersTextField.setText(_defaultNumScanners);
        _bcrManager.setBarcodeReaderNumberOfScanners(Integer.valueOf(_defaultNumScanners).intValue());
        _serialPortNameCombobox.setSelectedIndex(0);
        _bcrManager.setBarcodeReaderSerialPortName(_serialPortNameCombobox.getSelectedItem().toString());
        _bitsPerSecondCombobox.setSelectedIndex(0);
        _bcrManager.setBarcodeReaderRs232BitsPerSecond(_bitsPerSecondCombobox.getSelectedItem().toString());
        _dataBitsCombobox.setSelectedIndex(0);
        _bcrManager.setBarcodeReaderRs232DataBits(_dataBitsCombobox.getSelectedItem().toString());
        _parityCombobox.setSelectedIndex(0);
        _bcrManager.setBarcodeReaderRs232Parity(_parityCombobox.getSelectedItem().toString());
        _stopBitsCombobox.setSelectedIndex(0);
        _bcrManager.setBarcodeReaderRs232StopBits(_stopBitsCombobox.getSelectedItem().toString());
        _noReadErrorTextField.setText(_defaultErrorMessage);
        _bcrManager.setBarcodeReaderNoReadMessage(_defaultErrorMessage);
        _badSymbolErrorTextField.setText(_defaultErrorMessage);
        _bcrManager.setBarcodeReaderBadSymbolMessage(_defaultErrorMessage);
        _noLabelErrorTextField.setText(_defaultErrorMessage);
        _bcrManager.setBarcodeReaderNoLabelMessage(_defaultErrorMessage);
        _scannerTestTableModel.clearData();
        _scannerCodesTableModel.clearData();
        _configObservable.setEnabled(true);

        checkProtectedConfigurations();
        // because this is a new configuration we want the save option to be triggered if the user leaves without saving
        setConfigurationDirtyFlag(true);
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
 }

 /**
  * @author Laura Cormos
  */
 private boolean saveAsConfiguration()
 {
   String newConfigurationName = JOptionPane.showInputDialog(_mainUI,
                                                             StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_NAME_KEY"),
                                                             StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                             JOptionPane.INFORMATION_MESSAGE);
   if (newConfigurationName != null) // null means the dialog was canceled
   {
     newConfigurationName = getLegalFileName(newConfigurationName);
     if (newConfigurationName == null)
       return false;

     try
     {
       _bcrManager.saveAs(newConfigurationName);
       setConfigurationDirtyFlag(false);
       _configurationSelectNameCombobox.addItem(newConfigurationName);
       _configurationSelectNameCombobox.setSelectedItem(newConfigurationName);
       _bcrConfigurationToUseComboBox.addItem(newConfigurationName);
       _bcrConfigurationNamesList.add(newConfigurationName);
       checkProtectedConfigurations();
     }
     catch (DatastoreException ex)
     {
       showDatastoreError(ex);
       return false;
     }
   }

   return true;
 }

 /**
  * @author Laura Cormos
  */
 private void deleteConfigButton_actionPerformed(ActionEvent e)
 {
   // first check that this configuration is not the same as the one set "to use"
   if (_currentSelectedConfig.equals(_bcrManager.getBarcodeReaderConfigurationNameToUse()))
   {
     JOptionPane.showMessageDialog(_mainUI,
                                   StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_CANT_DELETE_PRODUCTION_CONFIG_MESSAGE_KEY",
                                                               new Object[]{_currentSelectedConfig})),
                                   StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                   JOptionPane.ERROR_MESSAGE);
     return;
   }

   // ask the user to confirm deleting this configuration
   int response = JOptionPane.showConfirmDialog(_mainUI,
                                                StringLocalizer.keyToString(new LocalizedString(
                                                   "CFGUI_BCR_CONFIG_DELETE_CONFIRM_MESSAGE_KEY",
                                                   new Object[]{_currentSelectedConfig})),
                                                StringLocalizer.keyToString("PDD_CONFIRM_DELETE_TITLE_KEY"),
                                                JOptionPane.OK_CANCEL_OPTION);
   if (response != JOptionPane.OK_OPTION)
     return;

   // first save the name of the config to delete
   String configToDelete = _currentSelectedConfig;

   _bcrConfigurationNamesList.remove(_bcrConfigurationNamesList.indexOf(_currentSelectedConfig));
   int index = _configurationSelectNameCombobox.getSelectedIndex();
   Assert.expect(index != -1);
   _configurationSelectNameCombobox.removeActionListener(_configurationSelectComboboxActionListener);
   _configurationSelectNameCombobox.removeItem(_currentSelectedConfig);
   _configurationSelectNameCombobox.addActionListener(_configurationSelectComboboxActionListener);

   _bcrConfigurationToUseComboBox.removeActionListener(_configurationToUseComboboxActionListener);
   _bcrConfigurationToUseComboBox.removeItem(_currentSelectedConfig);
   _bcrConfigurationToUseComboBox.addActionListener(_configurationToUseComboboxActionListener);
   // reset the dirty flag
   setConfigurationDirtyFlag(false);
   // then load a new config file to replace the deleted one in the editing screen
   int lastIndex = _configurationSelectNameCombobox.getItemCount() - 1 ;
   if (index > lastIndex)
     _configurationSelectNameCombobox.setSelectedIndex(lastIndex);
   else
     _configurationSelectNameCombobox.setSelectedIndex(index);

   // finally, delete the selected one
   try
   {
     _bcrManager.deleteConfig(configToDelete);
   }
   catch (DatastoreException ex)
   {
     showDatastoreError(ex);
   }
 }

 /**
  * @author Laura Cormos
  */
 private void numScannersTextField_actionPerformed()
 {
   String input = _numScannersTextField.getText();
   int numScanners = 0;

   if (input.length() == 0)
   {
     showIllegalNumScannersErrorMessage();
     _numScannersTextField.setText(String.valueOf(_scannerCodesTableModel.getRowCount()));
     return;
   }
   try
   {
     numScanners = Integer.parseInt(input);
   }
   catch (NumberFormatException ex)
   {
     JOptionPane.showMessageDialog(_mainUI,
                                   ex.getLocalizedMessage(),
                                   StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                   JOptionPane.ERROR_MESSAGE);
     _numScannersTextField.setText(String.valueOf(_scannerCodesTableModel.getRowCount()));
     return;
   }

   if (numScanners < _bcrManager.getMinimumBarcodeReaderNumberOfScanners() ||
       numScanners > _bcrManager.getMaximumBarcodeReaderNumberOfScanners())
   {
     showIllegalNumScannersErrorMessage();
     _numScannersTextField.setText(String.valueOf(_scannerCodesTableModel.getRowCount()));
     return;
   }
   else
   {
     if (numScanners != _scannerCodesTableModel.getRowCount())
     {
       int numRows = Math.abs(_scannerCodesTableModel.getRowCount() - numScanners);

       if (numScanners > _scannerCodesTableModel.getRowCount())
       {
         _scannerCodesTableModel.addRows(numRows);
         _scannerTestTableModel.addRows(numRows);
       }
       else
       {
         _scannerCodesTableModel.removeRows(numRows);
         _scannerTestTableModel.removeRows(numRows);
       }
     }

     try
     {
       _commandManager.execute(new ConfigSetNumberOfBarcodeReadersCommand(numScanners));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
     }

 //       _bcrManager.setBarcodeReaderNumberOfScanners(numScanners); /** @todo lc this needs to be undoable */
   }
   setConfigurationDirtyFlag(true);
   resetStatusBar();
 }

 /**
  * @author Laura Cormos
  */
 private void showIllegalNumScannersErrorMessage()
 {
   JOptionPane.showMessageDialog(_mainUI,
                                 StringLocalizer.keyToString(new LocalizedString(
                                     "CFGUI_BCR_ILLEGAL_SCANNER_NUMBER_MESSAGE_KEY",
                                     new Object[]
                                     {_bcrManager.getMinimumBarcodeReaderNumberOfScanners(),
                                     _bcrManager.getMaximumBarcodeReaderNumberOfScanners()})),
                                 StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                 JOptionPane.ERROR_MESSAGE);
 }

 /**
  * @author Laura Cormos
  */
 public void setConfigurationDirtyFlag(boolean dirty)
 {
   if (dirty)
     _bcrConfigurationIsDirty = true;
   else
     _bcrConfigurationIsDirty = false;
   resetStatusBar();
 }

 /**
  * @author Laura Cormos
  */
 private void setupTableEditors()
 {
   _scannerCodesTable.setPreferredColumnWidths();
   _scannerCodesTable.setEditorsAndRenderers();
   _scannerTestTable.setPreferredColumnWidths();
   _scannerTestTable.setEditorsAndRenderers();
 }

 /**
  * @author Laura Cormos
  */
 private boolean saveConfigurationIfNecessary()
 {
   if (_bcrConfigurationIsDirty)
   {
     int answer = JOptionPane.showConfirmDialog(
         _mainUI,
         StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_SAVE_CHANGES_CONFIRM_MESSAGE_KEY",
                                                         new Object[]{_currentSelectedConfig})),
         StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
         JOptionPane.YES_NO_CANCEL_OPTION);
     if (answer == JOptionPane.CANCEL_OPTION)
       return false;
     else if (answer == JOptionPane.YES_OPTION)
     {
       if (_bcrManager.isProtected(_currentSelectedConfig) == false)
         return saveCurrentConfiguration();
       else
         return saveAsConfiguration();
     }
   }
   // user selected to not save, dirty flag is false and 'saving' is complete
   setConfigurationDirtyFlag(false);
   return true;
 }

 /**
  * @author Laura Cormos
  */
 private boolean saveCurrentConfiguration()
 {
   _savingConfigurationInProgress = true;
   try
   {
     // set values for the 3 text fields in case someone typed in the new message text but did not terminate the edit,
     // i.e. press the Enter key or clicked somewhere else
     _bcrManager.setBarcodeReaderBadSymbolMessage((String)_badSymbolErrorTextField.getText());
     _bcrManager.setBarcodeReaderNoLabelMessage((String)_noLabelErrorTextField.getText());
     _bcrManager.setBarcodeReaderNoReadMessage((String)_noReadErrorTextField.getText());
     // save changes
     _bcrManager.save();
     _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_CONFIG_SAVED_OK_MESSAGE_KEY",
                                                                              new Object[]{_currentSelectedConfig})));
     setConfigurationDirtyFlag(false);
     _savingConfigurationInProgress = false;
     }
   catch (DatastoreException ex)
   {
     showDatastoreError(ex);
     return false;
   }
   return true;
 }

 /**
  * @author Laura Cormos
  */
 private void configurationToUseNameCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_bcrConfigurationToUseComboBox.getSelectedItem() instanceof String);

   String configToUse = (String)_bcrConfigurationToUseComboBox.getSelectedItem();
   if (configToUse != null)
   {
     try
     {
       _bcrManager.setBarcodeReaderConfigurationNameToUse(configToUse);
     }
     catch (DatastoreException ex)
     {
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void serialPortNumberCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_serialPortNameCombobox.getSelectedItem() instanceof String);

   String newPortName = (String)_serialPortNameCombobox.getSelectedItem();
   if (newPortName != null)
   {
     try
     {
       _commandManager.execute(new ConfigSetSerialPortNumberCommand(newPortName));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void bitsPerSecondCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_bitsPerSecondCombobox.getSelectedItem() instanceof String);
   String newBitsPerSecond = (String)_bitsPerSecondCombobox.getSelectedItem();
   if (newBitsPerSecond != null)
   {
     try
     {
       _commandManager.execute(new ConfigSetBitsPerSecondCommand(newBitsPerSecond));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void dataBitsCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_dataBitsCombobox.getSelectedItem() instanceof String);
   String newDataBits = (String)_dataBitsCombobox.getSelectedItem();
   if (newDataBits != null)
   {
     try
     {
       _commandManager.execute(new ConfigSetDataBitsCommand(newDataBits));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void parityCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_parityCombobox.getSelectedItem() instanceof String);
   String newParity = (String)_parityCombobox.getSelectedItem();
   if (newParity != null)
   {
     try
     {
       _commandManager.execute(new ConfigSetParityCommand(newParity));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void stopBitsCombobox_actionPerformed(ActionEvent e)
 {
   Assert.expect(_stopBitsCombobox.getSelectedItem() instanceof String);
   String newStopBits = (String)_stopBitsCombobox.getSelectedItem();
   if (newStopBits != null)
   {
     try
     {
       _commandManager.execute(new ConfigSetStopBitsCommand(newStopBits));
       setConfigurationDirtyFlag(true);
       resetStatusBar();
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void noReadErrorTextField_setValue()
 {
   String newNoReadError = (String)_noReadErrorTextField.getText();
   try
   {
     _commandManager.execute(new ConfigSetNoReadErrorMessageCommand(newNoReadError));
     setConfigurationDirtyFlag(true);
     resetStatusBar();
   }
   catch (XrayTesterException ex)
   {
     MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
   }
 }

 /**
  * @author Laura Cormos
  */
 private void badSymbolErrorTextField_setValue()
 {
   String newBadSymbolError = (String)_badSymbolErrorTextField.getText();
   try
   {
     _commandManager.execute(new ConfigSetBadSymbolErrorMessageCommand(newBadSymbolError));
     setConfigurationDirtyFlag(true);
     resetStatusBar();
   }
   catch (XrayTesterException ex)
   {
     MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
   }
 }

 /**
  * @author Laura Cormos
  */
 private void noLabelErrorTextField_setValue()
 {
   String newNoLabelError = (String)_noLabelErrorTextField.getText();
   try
   {
     _commandManager.execute(new ConfigSetNoLabelErrorMessageCommand(newNoLabelError));
     setConfigurationDirtyFlag(true);
     resetStatusBar();
   }
   catch (XrayTesterException ex)
   {
     MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
   }
 }

 /**
  * @author Laura Cormos
  */
 void showDatastoreError(DatastoreException ex)
 {
   JOptionPane.showMessageDialog(_mainUI,
                                 ex.getLocalizedMessage(),
                                 StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                 JOptionPane.ERROR_MESSAGE);
 }

 /**
  * @author Laura Cormos
  */
 private void advancedConfigurationButton_actionPerformed(ActionEvent e)
 {
   BarcodeReaderAdvancedSettingsDialog dlg = new BarcodeReaderAdvancedSettingsDialog(_mainUI, this);
   SwingUtils.centerOnComponent(dlg, _mainUI);
   dlg.setVisible(true);
   dlg.dispose();
 }

  /**
  * @author Phang Siew Yeng
  */
 private void connectButton_actionPerformed(ActionEvent e)
 {
   if (_connectButton.getText().equals(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY")))
   {
     SwingWorkerThread.getInstance().invokeLater(new Runnable()
     {
       public void run()
       {
           try
           {
             _bcrManager.connectSerialPort();
             _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_DISCONNECT_BUTTON_KEY"));
             _testButton.setEnabled(true);
           }
           catch (final HardwareException ex)
           {
             _scannerTestTableModel.setData(getErrorStringList());
             SwingUtils.invokeLater(new Runnable()
             {
               public void run()
               {
                 MessageDialog.showErrorDialog(_mainUI, StringUtil.format(ex.getLocalizedMessage(), 80),
                                               StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
               }
             });
           }
       }
     });
   }
   else
   {
     _bcrManager.disconnectSerialPort();
     _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY"));
     _testButton.setEnabled(false);
   }
 }
 
 /**
  * @author Laura Cormos
  */
 private void testButton_actionPerformed(ActionEvent e)
 {
   if (_testButton.getText().equals(StringLocalizer.keyToString("CFGUI_BCR_TEST_BUTTON_KEY")))
   {
     _testButton.setText(StringLocalizer.keyToString("CFGUI_BCR_STOP_BUTTON_KEY"));
     setControlsStateForTestLooping(false);
     SwingWorkerThread.getInstance().invokeLater(new Runnable()
     {
       public void run()
       {
         //Siew Yeng - XCR1728 - Able to disconnect barcode reader from V810
         //fix barcode reader not able to start looping test for the second time
         _stopTestRequest = false; // turn it back to false for the next time the "Test" button is pushed
         
         while (_stopTestRequest == false && _getSerialNumberBooleanLock.isFalse())
         {
           try
           {
             //Siew Yeng - add boolean lock to avoid crash when disconnect comm port immediately after stop looping test
             //Chin Seong - Check the isOutputStreamWriterIsAvailable availabilty 
             if(_bcrManager.isPortExist(_bcrManager.getBarcodeReaderSerialPortName()) == true &&
                _bcrManager.isInputStreamWriterIsAvailable() == true &&
                _bcrManager.isOutputStreamWriterIsAvailable() == true) 
             {
                _getSerialNumberBooleanLock.setValue(true);
                _scannerTestTableModel.setData(_bcrManager.getSerialNumbers());
             }
             else
             {
                stopTestLooping();
                _bcrManager.disconnectSerialPort();
                _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY"));
                
                java.util.List<String> logList = new ArrayList<String>();
                logList.add(StringLocalizer.keyToString("CFGUI_BCR_LOST_CONNECTION_KEY"));
                
                _scannerTestTableModel.setData(logList);
                _connectButton.setEnabled(true);
                _testButton.setEnabled(false);
             }
           }
           catch (final HardwareException ex)
           {      
             _scannerTestTableModel.setData(getErrorStringList());
             SwingUtils.invokeLater(new Runnable()
             {
               public void run()
               {
                 MessageDialog.showErrorDialog(_mainUI, StringUtil.format(ex.getLocalizedMessage(), 80),
                                               StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
               }
             });
           }
           finally
           {
             _getSerialNumberBooleanLock.setValue(false);
           }
         }
         //_stopTestRequest = false; // turn it back to false for the next time the "Test" button is pushed
       }
     });
   }
   else
   {
     //Siew Yeng - XCR1728 - Able to disconnect barcode reader from V810
     // fix hang problem when trying to stop looping test
      try
      {
        _stopTestRequest = true;
        _getSerialNumberBooleanLock.waitUntilFalse();
      }
      catch (InterruptedException ex)
      {
        // Do nothing ...
      }
     stopTestLooping();
   }
 }

 /**
  * @author Laura Cormos
  */
 private java.util.List<String> getErrorStringList()
 {
   String numString = _numScannersTextField.getText();
   int numScanners = Integer.parseInt(numString);
   java.util.List<String> errorTestCodes = new ArrayList<String>();
   for (int i = 0; i < numScanners; i++)
     errorTestCodes.add(StringLocalizer.keyToString("CFGUI_BCR_BARCODE_READER_GOT_ERROR_MESSAGE_KEY"));
   return errorTestCodes;
 }

 /**
  * @author Laura Cormos
  */
  private void stopTestLooping()
 {
   _stopTestRequest = true;
   _testButton.setText(StringLocalizer.keyToString("CFGUI_BCR_TEST_BUTTON_KEY"));
   setControlsStateForTestLooping(true);
   
   //Siew Yeng - send stop code to end read cycle
   _bcrManager.stopBarcodeReaderReadCycle();
 }

 /**
  * @author Laura Cormos
  */
 private void setControlsStateForTestLooping(boolean enabled)
 {
   setEnabledAllControls(enabled);
   if (enabled)
     checkProtectedConfigurations();
   _enableAutomaticBrcCheckbox.setEnabled(enabled);
   _testButton.setEnabled(true);
 }

 /**
  * @author Laura Cormos
  */
 private void resetStatusBar()
 {
   _mainUI.setStatusBarText("");
 }

 /**
  * @author Laura Cormos
  */
 private void clearUndoStack()
 {
   _commandManager.clear();
   _menuBar.updateUndoRedoMenuItems();
  }

 /**
  * @author Laura Cormos
  */
 public synchronized void update(final Observable observable, final Object object)
 {
   SwingUtils.invokeLater(new Runnable()
   {
     public void run()
     {
       Assert.expect(observable != null);
       Assert.expect(object != null);

       if (observable instanceof ConfigObservable)
       {
         handleConfigDataUpdate(object);
       }
       else if (observable instanceof HardwareObservable)
       {
         handleHardwareObservable(object);
       }
       else
         Assert.expect(false);
     }
   });
 }
 
 /**
  * @author Kee Chin Seong
 */
 private void handleHardwareObservable(Object object)
 {
   Assert.expect(object != null);
   if (object instanceof HardwareEvent)
   {
     HardwareEvent event = (HardwareEvent)object;
     HardwareEventEnum eventEnum = event.getEventEnum();
     
     if (eventEnum instanceof SerialPortCommEventEnum)
     {
        if(eventEnum.equals(SerialPortCommEventEnum.DISCONNECT))
        {
           _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY"));
           _testButton.setEnabled(false);
        }
     }
   }
 }

 /**
  * @author Laura Cormos
  */
 private void handleConfigDataUpdate(Object object)
 {
   Assert.expect(object != null);
   // update fields only if this update is not caused by the save action
   if (_savingConfigurationInProgress == false)
   {
     if (object instanceof ConfigEnum)
     {
       ConfigEnum configEnum = (ConfigEnum)object;
       if (configEnum instanceof SoftwareConfigEnum)
       {
         if (configEnum.equals(SoftwareConfigEnum.BARCODE_READER_AUTOMATIC_READER_ENABLED))
           _enableAutomaticBrcCheckbox.setSelected(_bcrManager.getBarcodeReaderAutomaticReaderEnabled());
       }
       else if (configEnum instanceof BarcodeReaderConfigEnum)
       {
         _configObservable.setEnabled(false);
         if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_NUMBER_OF_SCANNERS))
         {
           _numScannersTextField.setValue(_bcrManager.getBarcodeReaderNumberOfScanners());
           numScannersTextField_actionPerformed();
         }
         if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_SERIAL_PORT_NAME))
           _serialPortNameCombobox.setSelectedItem(_bcrManager.getBarcodeReaderSerialPortName());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_RS232_BITS_PER_SECOND))
           _bitsPerSecondCombobox.setSelectedItem(_bcrManager.getBarcodeReaderRs232BitsPerSecond());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_RS232_DATA_BITS))
           _dataBitsCombobox.setSelectedItem(_bcrManager.getBarcodeReaderRs232DataBits());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_RS232_PARITY))
           _parityCombobox.setSelectedItem(_bcrManager.getBarcodeReaderRs232Parity());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_RS232_STOP_BITS))
           _stopBitsCombobox.setSelectedItem(_bcrManager.getBarcodeReaderRs232StopBits());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_READ_MESSAGE))
           _noReadErrorTextField.setText(_bcrManager.getBarcodeReaderNoReadMessage());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_NO_READ_MESSAGE))
           _noReadErrorTextField.setText(_bcrManager.getBarcodeReaderNoReadMessage());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE))
           _badSymbolErrorTextField.setText(_bcrManager.getBarcodeReaderBadSymbolMessage());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_BAD_SYMBOL_MESSAGE))
           _badSymbolErrorTextField.setText(_bcrManager.getBarcodeReaderBadSymbolMessage());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_ENABLE_NO_LABEL_MESSAGE))
           _noLabelErrorTextField.setText(_bcrManager.getBarcodeReaderNoLabelMessage());
         else if (configEnum.equals(BarcodeReaderConfigEnum.BARCODE_READER_NO_LABEL_MESSAGE))
           _noLabelErrorTextField.setText(_bcrManager.getBarcodeReaderNoLabelMessage());
         _configObservable.setEnabled(true);
       }
 //       setConfigurationDirtyFlag(false);
     }
   }
 }
 
 /**
  * XCR2749 - Software assert when click on barcode reader tab
  * @author Khaw Chek Hau
  */
 private void disconnectSerialPortForTestCommunication()
 {
   if (_connectButton.getText().equals(StringLocalizer.keyToString("CFGUI_BCR_DISCONNECT_BUTTON_KEY")))
   {
     _bcrManager.disconnectSerialPort();
     _connectButton.setText(StringLocalizer.keyToString("CFGUI_BCR_CONNECT_BUTTON_KEY"));
     _testButton.setEnabled(false);
   }
 }
}
