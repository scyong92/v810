package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>Description: Dialog used for barcode reader configuration: advanced settings.  This is a modal dialog.</p>
 * @author Laura Cormos
 */
class BarcodeReaderAdvancedSettingsDialog extends EscapeDialog
{
  private BarcodeReaderManager _bcrManager;
  private ProductionOptionsBarCodeReaderTabPanel _caller;

  private static final int TEXT_WIDTH_MARGIN = 5;
  private static final int TEXT_HEIGHT_MARGIN = 5;
  private final int _MIN_FRAMING_BYTE;
  private final int _MAX_FRAMING_BYTE;
  private final int _MIN_TIMING;
  private final int _MAX_TIMING;
  private final int _MIN_BYTE_COUNT;
  private final int _MAX_BYTE_COUNT;

  private FocusAdapter _textFieldFocusAdapter;
  private JPanel _mainPanel = new JPanel();
  private JPanel _txOptionsPanel = new JPanel();
  private JPanel _timeoutEnablePanel = new JPanel();
  private JCheckBox _enableTimeoutCheckBox = new JCheckBox();
  private JPanel _timeoutInputPanel = new JPanel();
  private JLabel _timeLabel = new JLabel();
  private JLabel _msLabel = new JLabel();
//  private JTextField _timeTextField = new JTextField(4);
  private JPanel _framingByteEnablePanel = new JPanel();
  private JCheckBox _enableFramingByteCheckBox = new JCheckBox();
  private JPanel _framingByteInputPanel = new JPanel();
  private JLabel _framingByteLabel = new JLabel();
//  private JTextField _framingByteTextField = new JTextField(2);
  private JButton _framingByteCharsButton = new JButton();
  private JPanel _byteCountEnablePanel = new JPanel();
  private JCheckBox _enableByteCountCheckBox = new JCheckBox();
  private JPanel _byteCountInputPanel = new JPanel();
  private JLabel _byteCountLabel = new JLabel();
//  private JTextField _byteCountTextField = new JTextField(2);
  private JLabel _byteLabel = new JLabel();
  private JPanel _flowControlPanel = new JPanel();
  private JCheckBox _hwOnSendCheckbox = new JCheckBox();
  private JCheckBox _hwOnReceiveCheckbox = new JCheckBox();
  private JCheckBox _swOnSendCheckbox = new JCheckBox();
  private JCheckBox _swOnReceiveCheckbox = new JCheckBox();
  private JPanel _buttonsWrapperPanel = new JPanel();
  private JPanel _buttonsPanel = new JPanel();
  private GridLayout _buttonsPanelGridLayout = new GridLayout();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();

  private DecimalFormat _framingByteDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _framingByteDataDoc = new NumericRangePlainDocument();
  private NumericTextField _framingByteTextField = new NumericTextField();

  private DecimalFormat _timeDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _timeDataDoc = new NumericRangePlainDocument();
  private NumericTextField _timeTextField = new NumericTextField();

  private DecimalFormat _byteCountDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _byteCountDataDoc = new NumericRangePlainDocument();
  private NumericTextField _byteCountTextField = new NumericTextField();

  // enable response wait time for barcode reader
  private JPanel _responseTimeEnablePanel = new JPanel();
  private JCheckBox _enableResponseTimeCheckBox = new JCheckBox();
  private JPanel _responseTimeInputPanel = new JPanel();
  private JLabel _responseTimeLabel = new JLabel();
  private JLabel _responseMSLabel = new JLabel();
  private NumericTextField _responseTimeTextField = new NumericTextField();
  private DecimalFormat _responseTimeDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _responseTimeDataDoc = new NumericRangePlainDocument();
  
  //Barcode reader reading option
  private JPanel _scanMethodPanel = new JPanel();
  private ButtonGroup _bcrScanMethodButtonGroup =  new ButtonGroup();
  private JRadioButton _bcrScanBeforeLoadBoardRadioButton;
  private JRadioButton _bcrScanDuringBoardLoadingRadioButton;

  /**
   * Constructor.  Uses parent frame to center.
   * @author Laura Cormos
   */
  public BarcodeReaderAdvancedSettingsDialog(Frame parent, ProductionOptionsBarCodeReaderTabPanel caller)
  {
    super(parent, true);
    Assert.expect(parent != null);
    Assert.expect(caller != null);

    _caller = caller;
    _bcrManager = BarcodeReaderManager.getInstance();

    _MIN_FRAMING_BYTE = _bcrManager.getMinimumBarcodeReaderReceiveFramingByte();
    _MAX_FRAMING_BYTE = _bcrManager.getMaximumBarcodeReaderReceiveFramingByte();

    _MIN_TIMING = _bcrManager.getMinimumBarcodeReaderTimeoutInMilliseconds();
    _MAX_TIMING = _bcrManager.getMaximumBarcodeReaderTimeoutInMilliseconds();

    _MIN_BYTE_COUNT = _bcrManager.getMinimumBarcodeReaderThresholdCount();
    _MAX_BYTE_COUNT = _bcrManager.getMaximumBarcodeReaderThresholdCount();

    jbInit();
    init();

    if (_bcrManager.getBarcodeReaderEnableReceiveTimeout())
    {
      _enableTimeoutCheckBox.setSelected(true);
      _timeTextField.setText(String.valueOf(_bcrManager.getBarcodeReaderReceiveTimeoutInMilliseconds()));
    }
    else
      timeoutSetEnabled(false);
    // Wei CHin
    if (_bcrManager.getBarcodeReaderEnableResponseDelay())
    {
      _enableResponseTimeCheckBox.setSelected(true);
      _responseTimeTextField.setText(String.valueOf(_bcrManager.getBarcodeReaderResponseDelayInMilliseconds()));
    }
    else
      responseTimeSetEnabled(false);

    if (_bcrManager.getBarcodeReaderEnableReceiveFraming())
    {
      _enableFramingByteCheckBox.setSelected(true);
      _framingByteTextField.setValue(_bcrManager.getBarcodeReaderReceiveFramingByte());
    }
    else
      framingByteSetEnabled(false);
    if (_bcrManager.getBarcodeReaderEnableReceiveThreshold())
    {
      _enableByteCountCheckBox.setSelected(true);
      _byteCountTextField.setText(String.valueOf(_bcrManager.getBarcodeReaderReceiveThresholdCount()));
    }
    else
      byteCoundSetEnabled(false);
    if (_bcrManager.getBarcodeReaderRs232RtsCtsFlowControlIn())
      _hwOnSendCheckbox.setSelected(true);
    if (_bcrManager.getBarcodeReaderRs232RtsCtsFlowControlOut())
      _hwOnReceiveCheckbox.setSelected(true);
    if (_bcrManager.getBarcodeReaderRs232XonXoffFlowControlIn())
      _swOnSendCheckbox.setSelected(true);
    if (_bcrManager.getBarcodeReaderRs232XonXoffFlowControlOut())
      _swOnReceiveCheckbox.setSelected(true);
  }

  /**
   * UI initialization method.  Called and modified by the UI designer.
   * @author Laura Cormos
   */
  private void jbInit()
  {
    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }

      public void focusLost(FocusEvent e)
      {
        NumericTextField sourceTextField = (NumericTextField)e.getSource();
        handleTextFieldData(sourceTextField);
      }
    };

    this.setTitle(StringLocalizer.keyToString("CFGUI_BCR_ADVANCED_SETTINGS_TITLE_KEY"));
    this.getContentPane().add(_mainPanel, BorderLayout.CENTER);
    this.getContentPane().add(_buttonsWrapperPanel, BorderLayout.SOUTH);

    _mainPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
//    VerticalFlowLayout mainPanelVerticalFlowLayout = new VerticalFlowLayout();
//    mainPanelVerticalFlowLayout.setVerticalAlignment(VerticalFlowLayout.LEFT);
//    mainPanelVerticalFlowLayout.setHorizontalAlignment(VerticalFlowLayout.TOP);
    _mainPanel.setLayout(new VerticalFlowLayout());
    _mainPanel.add(_txOptionsPanel);
    _mainPanel.add(_scanMethodPanel);
    // this panel were removed because the java calls to the BCR driver are unreliable
//    _mainPanel.add(_flowControlPanel);
//    _mainPanel.add(_buttonsWrapperPanel);

    _txOptionsPanel.setLayout(new VerticalFlowLayout());
    _txOptionsPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_TRANSMISSION_OPTIONS_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));

    _txOptionsPanel.add(_timeoutEnablePanel);
    // these panels were removed because the java calls to the BCR driver are unreliable
//    _txOptionsPanel.add(_framingByteEnablePanel);
//    _txOptionsPanel.add(_byteCountEnablePanel);

    _timeoutEnablePanel.setLayout(new BorderLayout());
    _timeoutEnablePanel.add(_enableTimeoutCheckBox, BorderLayout.NORTH);
    _timeoutEnablePanel.add(_timeoutInputPanel, BorderLayout.CENTER);

    _enableTimeoutCheckBox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_TIMEOUT_LABEL_KEY"));
    _enableTimeoutCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_enableTimeoutCheckBox.isSelected() == false)
          timeoutSetEnabled(false);
        else
          timeoutSetEnabled(true);
      }
    });

    _timeoutInputPanel.setLayout(new FlowLayout());
    _timeoutInputPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    _timeoutInputPanel.add(_timeLabel);
    _timeoutInputPanel.add(_timeTextField);
    _timeoutInputPanel.add(_msLabel);

    _timeLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_TIME_LABEL_KEY"));

    _timeDataFormat.setParseIntegerOnly(true);
    _timeDataDoc.setRange(new IntegerRange(_MIN_TIMING, _MAX_TIMING));
    _timeTextField.setColumns(6);
    // the following 2 statements have to happen in this order: first set the document, then the format
    // because even though it looks like the format is set for the text field, it's actually set for the
    // text field's data document
    _timeTextField.setDocument(_timeDataDoc);
    _timeTextField.setFormat(_timeDataFormat);

    // Punit, XCR-2940: Assert when save barcode reader configuration file if timeout is set to 0
    // To avoid user to input '0'
    _timeTextField.getDocument().addDocumentListener(new DocumentListener()
    {
      public void changedUpdate(DocumentEvent e)
      {
        update();
      }
      public void removeUpdate(DocumentEvent e)
      {
        //Do Nothing. Everything handled in change
      }
      public void insertUpdate(DocumentEvent e)
      {
        update();
      }

      public void update()
      {
        if((Integer.parseInt(_timeTextField.getText())) <= 0)
        {
          SwingUtils.invokeLater(new Runnable() {
            @Override
            public void run() {
              _timeTextField.setText(null);
          }});
        }
      }
    });
    
    _msLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_MS_LABEL_KEY"));

    // Wei Chin (Barcode Response Wait Time)
    _txOptionsPanel.add(_responseTimeEnablePanel);

    _responseTimeEnablePanel.setLayout(new BorderLayout());
    _responseTimeEnablePanel.add(_enableResponseTimeCheckBox, BorderLayout.NORTH);
    _responseTimeEnablePanel.add(_responseTimeInputPanel, BorderLayout.CENTER);

    _enableResponseTimeCheckBox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_RESPONSE_DELAY_LABEL_KEY"));
    _enableResponseTimeCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_enableResponseTimeCheckBox.isSelected() == false)
          responseTimeSetEnabled(false);
        else
          responseTimeSetEnabled(true);
      }
    });

    _responseTimeInputPanel.setLayout(new FlowLayout());
    _responseTimeInputPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    _responseTimeInputPanel.add(_responseTimeLabel);
    _responseTimeInputPanel.add(_responseTimeTextField);
    _responseTimeInputPanel.add(_responseMSLabel);

    _responseTimeDataFormat.setParseIntegerOnly(true);
    _responseTimeDataDoc.setRange(new IntegerRange(_MIN_TIMING, _MAX_TIMING));
    _responseTimeTextField.setColumns(6);

    _responseTimeTextField.setDocument(_responseTimeDataDoc);
    _responseTimeTextField.setFormat(_responseTimeDataFormat);

    _responseTimeLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_TIME_LABEL_KEY"));
    _responseMSLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_MS_LABEL_KEY"));
    
    //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
    //barcode reader scan method
    _bcrScanBeforeLoadBoardRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_BCR_SCAN_METHOD_SCAN_BEFORE_LOAD_BOARD_KEY"));
    _bcrScanDuringBoardLoadingRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_BCR_SCAN_METHOD_SCAN_DURING_BOARD_LOADING_KEY"));
    
    _bcrScanMethodButtonGroup.add(_bcrScanBeforeLoadBoardRadioButton);
    _bcrScanMethodButtonGroup.add(_bcrScanDuringBoardLoadingRadioButton);
    
    String scanMethod = _bcrManager.getBarcodeReaderScanMethod();
    if(scanMethod.equals(BarcodeReaderScanMethodEnum.SCAN_BEFORE_BOARD_LOAD.toString()))
    {
      _bcrScanBeforeLoadBoardRadioButton.setSelected(true);
    }
    else if(scanMethod.equals(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()))
    {
      _bcrScanDuringBoardLoadingRadioButton.setSelected(true);
    }
    
    if(SerialNumberMappingManager.getInstance().isSerialNumberMappingEnabled() == false)
      _bcrScanDuringBoardLoadingRadioButton.setEnabled(false);
    
    _scanMethodPanel.setLayout(new VerticalFlowLayout());
    _scanMethodPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_SCAN_METHOD_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _scanMethodPanel.add(_bcrScanBeforeLoadBoardRadioButton);
    _scanMethodPanel.add(_bcrScanDuringBoardLoadingRadioButton);
    
    _framingByteEnablePanel.setLayout(new BorderLayout());
    _framingByteEnablePanel.add(_enableFramingByteCheckBox, BorderLayout.NORTH);
    _framingByteEnablePanel.add(_framingByteInputPanel, BorderLayout.CENTER);

    _enableFramingByteCheckBox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_FRAMING_BYTE_LABEL_KEY"));
    _enableFramingByteCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_enableFramingByteCheckBox.isSelected() == false)
          framingByteSetEnabled(false);
        else
          framingByteSetEnabled(true);
      }
    });

    _framingByteInputPanel.setLayout(new FlowLayout());
    _framingByteInputPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    _framingByteInputPanel.add(_framingByteLabel);
    _framingByteInputPanel.add(_framingByteTextField);
//    _framingByteInputPanel.add(_framingByteCharsButton);

    _framingByteLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_CHAR_CODE_LABEL_KEY"));

    _framingByteDataFormat.setParseIntegerOnly(true);
    _framingByteDataDoc.setRange(new IntegerRange(_MIN_FRAMING_BYTE, _MAX_FRAMING_BYTE));

    _framingByteTextField.setColumns(3);
    // the following 2 statements have to happen in this order: first set the document, then the format
    // because even though it looks like the format is set for the text field, it's actually set for the
    // text field's data document
    _framingByteTextField.setDocument(_framingByteDataDoc);
    _framingByteTextField.setFormat(_framingByteDataFormat);

    _framingByteCharsButton.setText(StringLocalizer.keyToString("..."));

    _byteCountEnablePanel.setLayout(new BorderLayout());
    _byteCountEnablePanel.add(_enableByteCountCheckBox, BorderLayout.NORTH);
    _byteCountEnablePanel.add(_byteCountInputPanel, BorderLayout.CENTER);

    _enableByteCountCheckBox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_BYTE_COUNT_LABEL_KEY"));
    _enableByteCountCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_enableByteCountCheckBox.isSelected() == false)
          byteCoundSetEnabled(false);
        else
          byteCoundSetEnabled(true);
      }
    });

    _byteCountInputPanel.setLayout(new FlowLayout());
    _byteCountInputPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    _byteCountInputPanel.add(_byteCountLabel);
    _byteCountInputPanel.add(_byteCountTextField);
    _byteCountInputPanel.add(_byteLabel);

    _byteCountLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_BUFFER_SIZE_LABEL_KEY"));

    _byteCountDataFormat.setParseIntegerOnly(true);
    _byteCountDataDoc.setRange(new IntegerRange(_MIN_BYTE_COUNT, _MAX_BYTE_COUNT));

    _byteCountTextField.setColumns(6);
    // the following 2 statements have to happen in this order: first set the document, then the format
    // because even though it looks like the format is set for the text field, it's actually set for the
    // text field's data document
    _byteCountTextField.setDocument(_byteCountDataDoc);
    _byteCountTextField.setFormat(_byteCountDataFormat);

    _byteLabel.setText(StringLocalizer.keyToString("CFGUI_BCR_BTYE_LABEL_KEY"));

    _flowControlPanel.setLayout(new VerticalFlowLayout());
    _flowControlPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_FLOW_CONTROL_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _flowControlPanel.add(_hwOnSendCheckbox);
    _flowControlPanel.add(_hwOnReceiveCheckbox);
    _flowControlPanel.add(_swOnSendCheckbox);
    _flowControlPanel.add(_swOnReceiveCheckbox);

    _hwOnSendCheckbox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_HW_ON_SEND_LABEL_KEY"));

    _hwOnReceiveCheckbox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_HW_ON_RECEIVE_LABEL_KEY"));

    _swOnSendCheckbox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_SW_ON_SEND_LABEL_KEY"));

    _swOnReceiveCheckbox.setText(StringLocalizer.keyToString("CFGUI_BCR_ENABLE_SW_ON_RECEIVE_LABEL_KEY"));

    _buttonsWrapperPanel.setLayout(new FlowLayout());
    _buttonsWrapperPanel.add(_buttonsPanel);

    _buttonsPanel.setLayout(_buttonsPanelGridLayout);
    _buttonsPanelGridLayout.setColumns(2);
    _buttonsPanelGridLayout.setHgap(10);
    _buttonsPanel.add(_okButton);
    _buttonsPanel.add(_cancelButton);

    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });

    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });

    JPanel[] panels = new JPanel[]{_txOptionsPanel, _flowControlPanel, _scanMethodPanel};
    SwingUtils.makeAllSameLength(panels);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  /**
   * Extra initialization needed, but not called by the UI designer.
   * @author Laura Cormos
   */
  private void init()
  {
    FontMetrics fontMetrics = getFontMetrics(getFont());

    //set the preferred width of the time text field such that it can display up to 4 digits
    int prefWidth = fontMetrics.stringWidth("999999") + TEXT_WIDTH_MARGIN;
    _timeTextField.setPreferredSize(new Dimension(prefWidth, (int)_timeTextField.getPreferredSize().getHeight()));

    //set the preferred width of the framing byte text field such that it can display up to 3 characters
    prefWidth = fontMetrics.stringWidth("999") + TEXT_WIDTH_MARGIN;
    _framingByteTextField.setPreferredSize(new Dimension(prefWidth, (int)_framingByteTextField.getPreferredSize().getHeight()));

    //set the preferred width of the byte count text field such that it can display up to 2 digits
    prefWidth = fontMetrics.stringWidth("999999") + TEXT_WIDTH_MARGIN;
    _byteCountTextField.setPreferredSize(new Dimension(prefWidth, (int)_byteCountTextField.getPreferredSize().getHeight()));

    pack();
  }

  /**
   * Information pertinent to this screen will be saved in memory for the current configuration, dialog will close
   * @param e ActionEvent Action Event fired
   * @author Laura Cormos
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    // save all values
    try
    {
      /* if we were to do undo, this is where the command block would be */
      try
      {
        // check valid range for timeout value
        boolean enabled = _enableTimeoutCheckBox.isSelected();
        int setting, minimumValue, maximumValue;
        _bcrManager.setBarcodeReaderEnableReceiveTimeout(enabled);

        if (enabled)
        {
          try
          {
            // Punit, XCR-2940: Assert when save barcode reader configuration file if timeout is set to 0
            // To avoid blank text field
            if((_timeTextField.getText().isEmpty()))
            {
              MessageDialog.showErrorDialog(this,
                                StringLocalizer.keyToString("CFGUI_BCR_BLANK_TIMEOUT_ERROR_KEY"),
                                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                true);
              return;
            }
            setting = _timeTextField.getLongValue().intValue();
          }
          catch (ParseException ex)
          {
            setting = _bcrManager.getBarcodeReaderReceiveFramingByte();
          }
          _bcrManager.setBarcodeReaderReceiveTimeoutInMilliseconds(setting);
        }
        // check valid range for byte count value
        enabled = _enableByteCountCheckBox.isSelected();
        _bcrManager.setBarcodeReaderEnableReceiveThreshold(enabled);
        if (enabled)
        {
          try
          {
            setting = _byteCountTextField.getLongValue().intValue();
          }
          catch (ParseException ex)
          {
            setting = _bcrManager.getBarcodeReaderReceiveFramingByte();
          }
          _bcrManager.setBarcodeReaderReceiveThresholdCount(setting);
        }

        // get framing byte value
        enabled = _enableFramingByteCheckBox.isSelected();
        _bcrManager.setBarcodeReaderEnableReceiveFraming(enabled);
        if (enabled)
        {
          try
          {
            setting = _framingByteTextField.getLongValue().intValue();
          }
          catch (ParseException ex)
          {
            setting = _bcrManager.getBarcodeReaderReceiveFramingByte();
          }
          _bcrManager.setBarcodeReaderReceiveFramingByte(setting);
        }

        // get response delay
        enabled = _enableResponseTimeCheckBox.isSelected();
        _bcrManager.setBarcodeReaderEnableResponseDelay(enabled);
        if (enabled)
        {
          try
          {
            setting = _responseTimeTextField.getLongValue().intValue();
          }
          catch (ParseException ex)
          {
            setting = 0;
//            setting = _bcrManager.getBarcodeReaderReceiveFramingByte();
          }
          _bcrManager.setBarcodeReaderResponseDelayInMilliseconds(setting);
        }

        //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
        if(_bcrScanBeforeLoadBoardRadioButton.isSelected())
          _bcrManager.setBarcodeReaderScanMethod(BarcodeReaderScanMethodEnum.SCAN_BEFORE_BOARD_LOAD); 
        else if(_bcrScanDuringBoardLoadingRadioButton.isSelected())
          _bcrManager.setBarcodeReaderScanMethod(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING);         
      }
      catch (NumberFormatException ex)
      {
        MessageDialog.showErrorDialog(_caller, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
        return;
      }

      // set flow control values
      _bcrManager.setBarcodeReaderRs232RtsCtsFlowControlOut(_hwOnSendCheckbox.isSelected());
      _bcrManager.setBarcodeReaderRs232RtsCtsFlowControlIn(_hwOnReceiveCheckbox.isSelected());
      _bcrManager.setBarcodeReaderRs232XonXoffFlowControlOut(_swOnSendCheckbox.isSelected());
      _bcrManager.setBarcodeReaderRs232XonXoffFlowControlIn(_swOnReceiveCheckbox.isSelected());
      _caller.setConfigurationDirtyFlag(true);
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_caller, ex, StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      return;
    }
    setVisible(false);
    dispose();
  }

  /**
   * Cancels the operation.  Dialog closes. State of the land pattern remains the same as before the dialog was opened.
   * @param e ActionEvent Action Event fired
   * @author Laura Cormos
   */
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    setVisible(false);
    dispose();
  }

  /**
   * @author Laura Cormos
   */
  private void handleTextFieldData(NumericTextField numericTextField)
  {
    Assert.expect(numericTextField != null);

    Double pos;
    try
    {
      pos = numericTextField.getDoubleValue();
    }
    catch (ParseException ex)
    {
      // this typically happens when the user deletes all characters from the field -- same as zero
      pos = new Double(0.0);
    }
    numericTextField.setValue(pos);
  }

  /**
   * @author Laura Cormos
   */
  private void timeoutSetEnabled(boolean enabled)
  {
    _timeLabel.setEnabled(enabled);
    _timeTextField.setEnabled(enabled);
    _msLabel.setEnabled(enabled);
  }

  /**
   * @author Wei Chin
   */
  private void responseTimeSetEnabled(boolean enabled)
  {
    _responseTimeLabel.setEnabled(enabled);
    _responseTimeTextField.setEnabled(enabled);
    _responseMSLabel.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void framingByteSetEnabled(boolean enabled)
  {
    _framingByteLabel.setEnabled(enabled);
    _framingByteTextField.setEnabled(enabled);
    _framingByteCharsButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void byteCoundSetEnabled(boolean enabled)
  {
    _byteCountLabel.setEnabled(enabled);
    _byteCountTextField.setEnabled(enabled);
 }
}
