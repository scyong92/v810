package com.axi.v810.gui.config;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;

/**
 *
 * @author chin-seong.kee
 * Developer Debug config enum will be populate under this table
 */
public class DeveloperDebugConfigTable extends JTable
{
  public static final int _DEVELOPER_DEBUG_CONFIG_KEY = 0;
  public static final int _DEVELOPER_DEBUG_CONFIG_VALUE = 1;  
  
  /**
   * Default constructor.
   * @author chin-seong.kee
   */
  public DeveloperDebugConfigTable(Component parent, DeveloperDebugConfigTableModel model)
  {
    Assert.expect(parent != null);
    Assert.expect(model != null);
    
    setModel(model);
    
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);    
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setEditorsAndRenderers()
  {
//    TableColumnModel columnModel = getColumnModel();
//    
//    
  }

  /**
   * @author Kee Chin Seong
   */
  public void resetEditorsAndRenderers()
  {
    //do nothing
    //_developerDebugConfigTableEditor.
  }
}
