package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
public class ResultsProcessingConditionsTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  private ResultsProcessor _resultsProcessor;
  private Rule _currentRule;
  private ResultsProcessingResultsTabPanel _parent;
  //String labels retrieved from the localization properties file
  private static final String _CONDITION_KEY_NAME_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_CONDITION_KEY_NAME_COLUMN_KEY");
  private static final String _PATTERN_TYPE_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_PATTERN_TYPE_COLUMN_KEY");
  private static final String _PATTERN_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_PATTERN_COLUMN_KEY");

  private String[] _columnLabels = { _CONDITION_KEY_NAME_LABEL,
                                   _PATTERN_TYPE_LABEL,
                                   _PATTERN_LABEL};

  private List<ResultsTableModelCondition> _selectedMatchPatterns = new ArrayList<ResultsTableModelCondition>();
  private List<ResultsTableModelCondition> _resultsTableModelConditions = new ArrayList<ResultsTableModelCondition>();

  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  /**
   * Constructor.
   * @author Laura Cormos
   */
  public ResultsProcessingConditionsTableModel(ResultsProcessingResultsTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
    _resultsProcessor = ResultsProcessor.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_resultsTableModelConditions != null)
      _resultsTableModelConditions.clear();
    if (_selectedMatchPatterns != null)
      _selectedMatchPatterns.clear();

    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_resultsTableModelConditions != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    ResultsTableModelCondition rowElement = _resultsTableModelConditions.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case ResultsProcessingConditionsTable._CONDITION_KEY_NAME_INDEX:
        value = rowElement.getKey();
        break;
      case ResultsProcessingConditionsTable._PATTERN_TYPE_INDEX:
        if (rowElement.getIsSimplePattern())
          value = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY");
        else
          value = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_REGEX_KEY");
        break;
      case ResultsProcessingConditionsTable._PATTERN_INDEX:
        value = rowElement.getPattern();
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    Assert.expect(_resultsTableModelConditions != null);

    return _resultsTableModelConditions.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if ((_selectedMatchPatterns == null) || (_selectedMatchPatterns.contains(_resultsTableModelConditions.get(rowIndex)) == false))
      return false;

    return true;
  }

  /**
   * Initialize the table with data from the results processing config file for the selected rule
   * @author Laura Cormos
   */
  public void setData(List<Condition> conditionsList)
  {
    Assert.expect(conditionsList != null);

    clearData();
    for (Condition condition : conditionsList)
    {
      List<String> patterns = condition.getDefintionExpressionList();
      for (String pattern : patterns)
      {
        ResultsTableModelCondition newElement = new ResultsTableModelCondition(condition.getConditionKeyEnum(),
                                                condition.isDefinedUsingSimpleExpression(), pattern);
        _resultsTableModelConditions.add(newElement);
      }
    }
    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    ResultsTableModelCondition rowElement = _resultsTableModelConditions.get(rowIndex);

    switch (columnIndex)
    {
      case ResultsProcessingConditionsTable._CONDITION_KEY_NAME_INDEX:
        Assert.expect(object instanceof ConditionKeyEnum);

        ConditionKeyEnum key = (ConditionKeyEnum)object;
        try
        {
          _commandManager.execute(new ConfigSetResultsConditionKeyCommand(rowElement, key));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case ResultsProcessingConditionsTable._PATTERN_TYPE_INDEX:
        Assert.expect(object instanceof String);
        String type = (String)object;
        boolean isSimplePattern = type.equals(StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY"));
        // first set the pattern, then check for validity. "Simple" patterns are always valid, except for ""
        // If the regex is complicated, don't want to loose it when the error pops up below

        try
        {
          _commandManager.execute(new ConfigSetResultsConditionPatternTypeCommand(rowElement, isSimplePattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }

        // for a regular expression, check its validity
        if (isSimplePattern == false)
        {
          try
          {
            RegularExpressionUtilAxi.checkRegExValid(rowElement.getPattern());
          }
          catch (InvalidRegularExpressionDatastoreException ex)
          {
            // give a warning for the invalid regex exception when only the pattern type is changed
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString(
                "CFGUI_INVALID_REGEX_WARNING_KEY",
                new Object[]
                {rowElement.getPattern()})),
                                          StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                          JOptionPane.WARNING_MESSAGE);
          }
        }
        break;
      case ResultsProcessingConditionsTable._PATTERN_INDEX:
        Assert.expect(object instanceof String);
        String pattern = (String)object;
        if (pattern.equals(""))
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("CFGUI_EMPTY_REGEX_NOT_VALID_MESSAGE_KEY"),
                                        StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                        JOptionPane.ERROR_MESSAGE);
          fireTableCellUpdated(rowIndex, columnIndex);
          break;
        }
        // set the pattern first so that if the regex is complex we won't loose it, even if it is not valid
        try
        {
          _commandManager.execute(new ConfigSetResultsConditionPatternCommand(rowElement, pattern));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }

        if (rowElement.getIsSimplePattern() == false)
        {
          try
          {
            RegularExpressionUtilAxi.checkRegExValid(pattern);
          }
          catch (InvalidRegularExpressionDatastoreException ex)
          {
            JOptionPane.showMessageDialog(_mainUI,
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                          JOptionPane.ERROR_MESSAGE);
          }
        }
        break;
      default:
        Assert.expect(false);
    }
    _parent.setConditionDirtyFlag();
  }

   /**
    * @author Laura Cormos
    */
   public void addNewRow()
   {
     ResultsTableModelCondition newElement = new ResultsTableModelCondition(ConditionKeyEnum.PROJECT_NAME, true, "");
     _resultsTableModelConditions.add(newElement);

     fireTableDataChanged();
   }

   /**
    * @author Laura Cormos
    */
   public void addCondition(ResultsTableModelCondition condition)
   {
     Assert.expect(condition != null);

     _resultsTableModelConditions.add(condition);

     fireTableDataChanged();
   }

   /**
    * @author Laura Cormos
    */
   public ResultsTableModelCondition getConditionAt(int index)
   {
     Assert.expect(index >= 0);
     Assert.expect(index < getRowCount());

     return _resultsTableModelConditions.get(index);
   }

   /**
    * @author Laura Cormos
    */
   public void setCurentRule(Rule rule)
   {
     Assert.expect(rule != null);

     _currentRule = rule;
   }

   /**
    * @author Laura Cormos
    */
   public void setSelectedRow(int selectedRowIndex)
   {
     Assert.expect(selectedRowIndex >= 0);
     Assert.expect(_selectedMatchPatterns != null);

     _selectedMatchPatterns.clear();
     _selectedMatchPatterns.add(_resultsTableModelConditions.get(selectedRowIndex));
   }

   /**
    * @author Laura Cormos
    */
   public boolean saveCoditions()
   {
     Map<ConditionKeyEnum, List<String>> keyToPatternListMap = new HashMap<ConditionKeyEnum,List<String>>();
     Map<ConditionKeyEnum, Boolean> keyToPatternTypeMap = new HashMap<ConditionKeyEnum,Boolean>();

     boolean errorFound = false;
     String errors = StringLocalizer.keyToString("CFGUI_JTA_FIX_ERRORS_MESSAGE_KEY");
     // iterate over the table model and recreate all the conditions
     for (ResultsTableModelCondition rowElement : _resultsTableModelConditions)
     {
       ConditionKeyEnum cke = rowElement.getKey();
       List<String> patterns = keyToPatternListMap.get(cke);
       if (patterns == null)
         patterns = new ArrayList<String>();

       // check for invalid regEx
       if (rowElement.getIsSimplePattern() == false)
       {
         try
         {
           RegularExpressionUtilAxi.checkRegExValid(rowElement.getPattern());
         }
         catch (InvalidRegularExpressionDatastoreException ex)
         {
           errors += ex.getLocalizedMessage();
           errors += "\n";
           errorFound = true;
         }
       }
       // check for empty pattern
       if (rowElement.getPattern().equals(""))
       {
         errors += StringLocalizer.keyToString("CFGUI_EMPTY_REGEX_NOT_VALID_MESSAGE_KEY");
         errors += "\n";
         errorFound = true;
       }
       patterns.add(rowElement.getPattern());
       keyToPatternListMap.put(cke, patterns);
       if (keyToPatternTypeMap.containsKey(cke))
       {
         if (rowElement.getIsSimplePattern() != keyToPatternTypeMap.get(cke).booleanValue())
         {
           // show error message
           String pattern1 = patterns.get(patterns.size()-1); // last pattern added
           String pattern2 = patterns.get(patterns.size()-2); // previous pattern added

           errors += StringLocalizer.keyToString(new LocalizedString("CFGUI_MISMATCHED_PATTERN_TYPES_KEY",
                                                                     new Object[]{cke.toString(), pattern1, pattern2}));
           errors += "\n";
           errorFound = true;
         }
       }
       else
         keyToPatternTypeMap.put(cke, rowElement.getIsSimplePattern());
     }
     if (errorFound)
     {
       JOptionPane.showMessageDialog(_mainUI,
                                     errors,
                                     StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                     JOptionPane.ERROR_MESSAGE);
       return false;
     }
     // remove existing conditions for the current rule
     _currentRule.removeAllConditions();
     // add new conditions to the current rule
     for (ConditionKeyEnum cke : keyToPatternListMap.keySet())
     {
       Condition newCondition;
       String patternType;
       if (keyToPatternTypeMap.get(cke).booleanValue())
         patternType = Condition.SIMPLE_TYPE_IDENTIFIER;
       else
         patternType = Condition.REGEX_TYPE_IDENTIFIER;
      try
      {
        newCondition = _resultsProcessor.createCondition(cke, patternType, keyToPatternListMap.get(cke));
        _currentRule.addCondition(newCondition);
      }
      catch (DatastoreException ex)
      {
        // could be InvalidRegularExpressionDatastoreException or InvalidSimpleExpressionDatastoreException
        // show error message
        Assert.expect(false, "invalid regEx escaped previous error checking! tse, tse, tse");
      }
     }
     return true;
   }

   /**
    * @author Laura Cormos
    */
   public void deleteConditionAt(int rowIndex)
   {
     Assert.expect(rowIndex >= 0);

     _resultsTableModelConditions.remove(rowIndex);
     _parent.setConditionDirtyFlag();
     fireTableRowsDeleted(rowIndex, rowIndex);
   }

   /**
    * @author Laura Cormos
    */
   public int getIndexOf(ResultsTableModelCondition condition)
   {
     Assert.expect(condition != null);

     return _resultsTableModelConditions.indexOf(condition);
   }
}
