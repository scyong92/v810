package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;
import com.axi.guiUtil.NumericRenderer;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
class BarCodeScannerCodesTable extends JTable
{
  public static final int _SCANNER_ID_INDEX = 0;
  public static final int _TRIGGER_INDEX = 1;
  public static final int _STOP_INDEX = 2;
  public static final int _PREAMBLE_INDEX = 3;
  public static final int _POSTAMBLE_INDEX = 4;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SCANNER_ID_COLUMN_WIDTH_PERCENTAGE = .04;
  private static final double _TRIGGER_COLUMN_WIDTH_PERCENTAGE = .24;
  private static final double _STOP_COLUMN_WIDTH_PERCENTAGE = .24;
  private static final double _PREAMBLE_COLUMN_WIDTH_PERCENTAGE = .24;
  private static final double _POSTAMBLE_COLUMN_WIDTH_PERCENTAGE = .24;

  private NumericRenderer _numericRenderer = new NumericRenderer(0, JLabel.LEFT);

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public BarCodeScannerCodesTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int scannerIdColumnWidth = (int)(totalColumnWidth * _SCANNER_ID_COLUMN_WIDTH_PERCENTAGE);
    int triggerColumnWidth = (int)(totalColumnWidth * _TRIGGER_COLUMN_WIDTH_PERCENTAGE);
    int stopColumnWidth = (int)(totalColumnWidth * _STOP_COLUMN_WIDTH_PERCENTAGE);
    int preambleColumnWidth = (int)(totalColumnWidth * _PREAMBLE_COLUMN_WIDTH_PERCENTAGE);
    int postambleColumnWidth = (int)(totalColumnWidth * _POSTAMBLE_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_SCANNER_ID_INDEX).setPreferredWidth(scannerIdColumnWidth);
    columnModel.getColumn(_TRIGGER_INDEX).setPreferredWidth(triggerColumnWidth);
    columnModel.getColumn(_STOP_INDEX).setPreferredWidth(stopColumnWidth);
    columnModel.getColumn(_PREAMBLE_INDEX).setPreferredWidth(preambleColumnWidth);
    columnModel.getColumn(_POSTAMBLE_INDEX).setPreferredWidth(postambleColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_SCANNER_ID_INDEX).setCellRenderer(_numericRenderer);

    columnModel.getColumn(_TRIGGER_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_STOP_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_PREAMBLE_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_POSTAMBLE_INDEX).setCellEditor(stringEditor);
  }
}

