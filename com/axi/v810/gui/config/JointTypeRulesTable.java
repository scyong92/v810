package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
public class JointTypeRulesTable extends JTable
{
  public static final int _JOINT_TYPE_NAME_INDEX = 0;
  public static final int _RULE_ENABLED_INDEX = 1;
  public static final int _REF_DES_COLUMN_INDEX = 2;
  public static final int _LP_NAME_COLUMN_INDEX = 3;
  public static final int _NUM_PADS_COLUMN_INDEX = 4;
  public static final int _NUM_ROWS_COLUMN_INDEX = 5;
  public static final int _NUM_COLS_COLUMN_INDEX = 6;
  public static final int _PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX = 7;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _JOINT_TYPE_NAME_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _RULE_ENABLED_COLUMN_WIDTH_PERCENTAGE = .05;
  private static final double _REF_DES_COLUMN_WIDTH_PERCENTAGE = .205;
  private static final double _LP_NAME_COLUMN_WIDTH_PERCENTAGE = .205;
  private static final double _NUM_PADS_COLUMN_WIDTH_PERCENTAGE = .06;
  private static final double _NUM_ROWS_COLUMN_WIDTH_PERCENTAGE = .06;
  private static final double _NUM_COLS_COLUMN_WIDTH_PERCENTAGE = .07;
  private static final double _PAD_IN_MIDDLE_ENABLED_COLUMN_WIDTH_PERCENTAGE = .2;

  private ListSelectionCellEditor _jointTypeEditor =
      new ListSelectionCellEditor(new java.util.ArrayList<Object>(JointTypeEnum.getAllJointTypeEnums()));
  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());
  private CheckCellRenderer _checkRenderer = new CheckCellRenderer();

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public JointTypeRulesTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int jointTypeNameColumnWidth = (int)(totalColumnWidth * _JOINT_TYPE_NAME_COLUMN_WIDTH_PERCENTAGE);
    int ruleEnabledColumnWidth = (int)(totalColumnWidth * _RULE_ENABLED_COLUMN_WIDTH_PERCENTAGE);
    int refDesColumnWidth = (int)(totalColumnWidth * _REF_DES_COLUMN_WIDTH_PERCENTAGE);
    int numPadsColumnWidth = (int)(totalColumnWidth * _NUM_PADS_COLUMN_WIDTH_PERCENTAGE);
    int numRowsColumnWidth = (int)(totalColumnWidth * _NUM_ROWS_COLUMN_WIDTH_PERCENTAGE);
    int numColsColumnWidth = (int)(totalColumnWidth * _NUM_COLS_COLUMN_WIDTH_PERCENTAGE);
    int lpNameColumnWidth = (int)(totalColumnWidth * _LP_NAME_COLUMN_WIDTH_PERCENTAGE);
    int padInMiddleEnabledColumnWidth = (int)(totalColumnWidth * _PAD_IN_MIDDLE_ENABLED_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_JOINT_TYPE_NAME_INDEX).setPreferredWidth(jointTypeNameColumnWidth);
    columnModel.getColumn(_RULE_ENABLED_INDEX).setPreferredWidth(ruleEnabledColumnWidth);
    columnModel.getColumn(_REF_DES_COLUMN_INDEX).setPreferredWidth(refDesColumnWidth);
    columnModel.getColumn(_NUM_PADS_COLUMN_INDEX).setPreferredWidth(numPadsColumnWidth);
    columnModel.getColumn(_NUM_ROWS_COLUMN_INDEX).setPreferredWidth(numRowsColumnWidth);
    columnModel.getColumn(_NUM_COLS_COLUMN_INDEX).setPreferredWidth(numColsColumnWidth);
    columnModel.getColumn(_LP_NAME_COLUMN_INDEX).setPreferredWidth(lpNameColumnWidth);
    columnModel.getColumn(_PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX).setPreferredWidth(padInMiddleEnabledColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_JOINT_TYPE_NAME_INDEX).setCellEditor(_jointTypeEditor);
    columnModel.getColumn(_RULE_ENABLED_INDEX).setCellEditor(_checkEditor);
     columnModel.getColumn(_RULE_ENABLED_INDEX).setCellRenderer(_checkRenderer);
    columnModel.getColumn(_REF_DES_COLUMN_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_NUM_PADS_COLUMN_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_NUM_ROWS_COLUMN_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_NUM_COLS_COLUMN_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_LP_NAME_COLUMN_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX).setCellRenderer(_checkRenderer);
  }
}
