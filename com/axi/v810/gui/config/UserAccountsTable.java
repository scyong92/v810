package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;

/**
 * This class displays the information for all the components shown in Verify CAD
 * @author George Booth
 */
class UserAccountsTable extends JSortTable
{
  public static final int _USER_NAME_INDEX = 0;
  public static final int _ACCOUNT_TYPE_INDEX = 1;
  public static final int _FULL_NAME_INDEX = 2;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _USER_NAME_COLUMN_WIDTH_PERCENTAGE = .30;
  private static final double _ACCOUNT_TYPE_COLUMN_WIDTH_PERCENTAGE = .30;
  private static final double _FULL_NAME_COLUMN_WIDTH_PERCENTAGE = .40;

  private UserAccountsTableModel _tableModel;
  private boolean _modelSet = false;

  /**
   * Default constructor.
   * @author George Booth
   */
  public UserAccountsTable(UserAccountsTableModel model)
  {
    Assert.expect(model != null);
    _tableModel = model;
    _modelSet = true;
    super.setModel(model);

//    setRowSelectionAllowed(true);
//    setColumnSelectionAllowed(true);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  /**
   * Gets an array of column's width for persisted settings
   * @author George Booth
   */
  public int[] getColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int[] columnWidths = new int[columnModel.getColumnCount()];
    for (int i = 0; i < columnModel.getColumnCount(); i++)
    {
      columnWidths[i] = columnModel.getColumn(i).getWidth();
//      System.out.println("  get column[" + i + "] width = " + columnWidths[i]);
    }
    return columnWidths;
  }

  /**
   * Sets each column's width based on persisted settings
   * @author George Booth
   */
  public void setColumnWidths(int[] columnWidths)
  {
    TableColumnModel columnModel = getColumnModel();

    for (int i = 0; i < columnWidths.length; i++)
    {
//      System.out.println("  set column[" + i + "] width = " + columnWidths[i]);
      columnModel.getColumn(i).setWidth(columnWidths[i]);
    }

  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author George Booth
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_USER_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _USER_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_ACCOUNT_TYPE_INDEX).setPreferredWidth((int)(totalColumnWidth * _ACCOUNT_TYPE_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_FULL_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _FULL_NAME_COLUMN_WIDTH_PERCENTAGE));
  }

  /**
   * @author George Booth
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_USER_NAME_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_ACCOUNT_TYPE_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_FULL_NAME_INDEX).setCellEditor(stringEditor);
  }

  /**
   * @author George Booth
   */
  public void clearSelection()
  {
    TableModel model = getModel();
    // this function gets called before the table model is set, during intialization. At that time,
    // the model will be a defaultTableModel so the assert is sure to fail
    if (_modelSet)
    {
      Assert.expect(model instanceof UserAccountsTableModel);
      ((UserAccountsTableModel)model).clearSelection();
    }
    super.clearSelection();
  }

  /**
   * @author George Booth
   */
  void cancelCellEditing()
  {
    cancelCellEditingForColumn(_USER_NAME_INDEX);
    cancelCellEditingForColumn(_ACCOUNT_TYPE_INDEX);
    cancelCellEditingForColumn(_FULL_NAME_INDEX);
  }

  /**
   * @author George Booth
   */
  void cancelCellEditingForColumn(int column)
  {
    TableCellEditor cellEditor = getColumnModel().getColumn(column).getCellEditor();
    if (cellEditor != null)
    {
      cellEditor.cancelCellEditing();
    }
  }

}
