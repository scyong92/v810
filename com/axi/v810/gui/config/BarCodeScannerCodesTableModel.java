package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
class BarCodeScannerCodesTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  private ProductionOptionsBarCodeReaderTabPanel _parent;
  private BarcodeReaderManager _bcrManager;
  private String _defaultCode = "";

  //String labels retrieved from the localization properties file
  private static final String _SCANNER_ID_LABEL = StringLocalizer.keyToString("CFGUI_SCANNER_ID_KEY");
  private static final String _TRIGGER_CODE_LABEL = StringLocalizer.keyToString("CFGUI_TRIGGER_CODE_KEY");
  private static final String _STOP_CODE_LABEL = StringLocalizer.keyToString("CFGUI_STOP_CODE_KEY");
  private static final String _PREAMBLE_CODE_LABEL = StringLocalizer.keyToString("CFGUI_PREAMBLE_CODE_KEY");
  private static final String _POSTAMBLE_CODE_LABEL = StringLocalizer.keyToString("CFGUI_POSTAMBLE_CODE_KEY");

  private String[] _columnLabels = { _SCANNER_ID_LABEL,
                                   _TRIGGER_CODE_LABEL,
                                   _STOP_CODE_LABEL,
                                   _PREAMBLE_CODE_LABEL,
                                   _POSTAMBLE_CODE_LABEL};

  private List<ScannerCodeTableElement> _scanerCodesElements = new ArrayList<ScannerCodeTableElement>();

  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();

  public BarCodeScannerCodesTableModel()
  {
    _bcrManager = BarcodeReaderManager.getInstance();
  }
  /**
   * Constructor.
   * @author Laura Cormos
   */
  public BarCodeScannerCodesTableModel(ProductionOptionsBarCodeReaderTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
    _bcrManager = BarcodeReaderManager.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_scanerCodesElements != null)
      _scanerCodesElements.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_scanerCodesElements != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    ScannerCodeTableElement rowElement = _scanerCodesElements.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case BarCodeScannerCodesTable._SCANNER_ID_INDEX:
        value = rowElement._scannerID;
        break;
      case BarCodeScannerCodesTable._TRIGGER_INDEX:
        value = rowElement._triggerCode;
        break;
      case BarCodeScannerCodesTable._STOP_INDEX:
        value = rowElement._stopCode;
        break;
      case BarCodeScannerCodesTable._PREAMBLE_INDEX:
        value = rowElement._preambleCode;
        break;
      case BarCodeScannerCodesTable._POSTAMBLE_INDEX:
        value = rowElement._postambleCode;
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    Assert.expect(_scanerCodesElements != null);

    return _scanerCodesElements.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

//    if ((_selectedConditions == null) || (_selectedConditions.contains(_conditions.get(rowIndex)) == false))
//      return false;

    if (columnIndex == BarCodeScannerCodesTable._SCANNER_ID_INDEX)
      return false;
    return true;
  }

  /**
   * Initialize the table with data from the current barcodereader config file
   * @author Laura Cormos
   */
  public void setData()
  {
    Assert.expect(_bcrManager != null);

    clearData();

    int numRows = _bcrManager.getBarcodeReaderNumberOfScanners();
    for (int i = 1; i <= numRows; i++)
    {
      ScannerCodeTableElement newRowElement = new ScannerCodeTableElement(i,
          _bcrManager.getBarcodeReaderTriggerAsHexadecimalString(i),
          _bcrManager.getBarcodeReaderStopAsHexadecimalString(i),
          _bcrManager.getBarcodeReaderPreambleAsHexadecimalString(i),
          _bcrManager.getBarcodeReaderPostambleAsHexadecimalString(i));
      _scanerCodesElements.add(newRowElement);
    }

    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    ScannerCodeTableElement rowElement = _scanerCodesElements.get(rowIndex);

    switch(columnIndex)
    {
      case BarCodeScannerCodesTable._TRIGGER_INDEX:
        Assert.expect(object instanceof String);
        rowElement.setTriggerCode((String)object);
        break;
      case BarCodeScannerCodesTable._STOP_INDEX:
        Assert.expect(object instanceof String);
        rowElement.setStopCode((String)object);
        break;
      case BarCodeScannerCodesTable._PREAMBLE_INDEX:
        Assert.expect(object instanceof String);
        rowElement.setPreambleCode((String)object);
        break;
      case BarCodeScannerCodesTable._POSTAMBLE_INDEX:
        Assert.expect(object instanceof String);
        rowElement.setPostambleCode((String)object);
        break;
      default:
        Assert.expect(false);
    }
    _parent.setConfigurationDirtyFlag(true);
  }

  /**
   * @author Laura Cormos
   */
  public void addRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows ; i++)
    {
      ScannerCodeTableElement newElement  = new ScannerCodeTableElement(getRowCount()+1, 
                                                                        _defaultCode, 
                                                                        _defaultCode, 
                                                                        _defaultCode, 
                                                                        _defaultCode);
      _scanerCodesElements.add(newElement);
    }
    fireTableDataChanged();
  }

  /**
    * @author Laura Cormos
    */
   public void removeRows(int numRows)
   {
     Assert.expect(numRows > 0);

     for (int i = 0; i < numRows ; i++)
     {
       _scanerCodesElements.remove(getRowCount() - 1);
     }
     fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */

  class ScannerCodeTableElement
  {
    int _scannerID;
    String _triggerCode;
    String _stopCode;
    String _preambleCode;
    String _postambleCode;

    /**
     * @author Laura Cormos
     */
    ScannerCodeTableElement(int id, String trigger, String stop, String preamble, String postamble)
    {
      Assert.expect(id >= 0);
      Assert.expect(trigger != null);
      Assert.expect(stop != null);
      Assert.expect(preamble != null);
      Assert.expect(postamble != null);

      _scannerID = id;
      _triggerCode = trigger;
      _stopCode = stop;
      _preambleCode = preamble;
      _postambleCode = postamble;
      try
      {
        _bcrManager.setBarcodeReaderTriggerAsHexadecimalString(_scannerID, _triggerCode);
        _bcrManager.setBarcodeReaderStopAsHexadecimalString(_scannerID, _stopCode);
        _bcrManager.setBarcodeReaderPreambleAsHexadecimalString(_scannerID, _preambleCode);
        _bcrManager.setBarcodeReaderPostambleAsHexadecimalString(_scannerID, _postambleCode);
      }
      catch (DatastoreException ex)
      {
      }
    }

    /**
     * @author Laura Cormos
     */
    void setTriggerCode(String triggerCode)
    {
      Assert.expect(triggerCode != null);
      _triggerCode = triggerCode;
      try
      {
        _bcrManager.setBarcodeReaderTriggerAsHexadecimalString(_scannerID, triggerCode);
      }
      catch (DatastoreException ex)
      {
        _parent.showDatastoreError(ex);
        return;
     }
     _triggerCode = triggerCode;
    }
    
    /**
     * @author Phang Siew Yeng
     */
    void setStopCode(String stopCode)
    {
      Assert.expect(stopCode != null);
      _stopCode = stopCode;
      try
      {
        _bcrManager.setBarcodeReaderStopAsHexadecimalString(_scannerID, stopCode);
      }
      catch (DatastoreException ex)
      {
        _parent.showDatastoreError(ex);
        return;
     }
     _stopCode = stopCode;
    }

    /**
     * @author Laura Cormos
     */
    void setPreambleCode(String preambleCode)
    {
      Assert.expect(preambleCode != null);
      _preambleCode = preambleCode;
      try
      {
        _bcrManager.setBarcodeReaderPreambleAsHexadecimalString(_scannerID, preambleCode);
      }
      catch (DatastoreException ex)
      {
         _parent.showDatastoreError(ex);
         return;
      }
      _preambleCode = preambleCode;
    }

    /**
     * @author Laura Cormos
     */
    void setPostambleCode(String postambleCode)
    {
      Assert.expect(postambleCode != null);
      try
      {
        _bcrManager.setBarcodeReaderPostambleAsHexadecimalString(_scannerID, postambleCode);
      }
      catch (DatastoreException ex)
      {
         _parent.showDatastoreError(ex);
         return;
      }
      _postambleCode = postambleCode;
    }
  }
}
