package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;

/**
 * @author wei-chin.chong
 */
public class ProductionTuneSettingsDialog extends JDialog
{
  private final int TEXTFIELD_COLUMN = 30;
  
  private JPanel _northPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _southPanel = new JPanel();
  private JPanel _centerButtonPanel = new JPanel();
  private JPanel _centerDirectoryPanel = new JPanel();
  private JScrollPane _centerScrollPane = new JScrollPane();
  
  private BorderLayout _mainBorderLayout = new BorderLayout();
  private VerticalFlowLayout _northVerticalFlowLayout = new VerticalFlowLayout();
  private BorderLayout _centerBorderLayout = new BorderLayout();
  private FlowLayout _centerBottonFlowLayout = new FlowLayout(FlowLayout.LEFT, 10, 2);
  private FlowLayout _southFlowLayout = new FlowLayout(FlowLayout.CENTER, 10, 2);
    
  private JRadioButton _saveAllImagesRadioButton = new JRadioButton();
  private JRadioButton _saveOnlyDefectImagesRadioButton = new JRadioButton();
  private JRadioButton _saveSpecificImagesRadioButton = new JRadioButton();
  private ButtonGroup _productionTuneSettingsButtonGroup = new ButtonGroup();
  
  private ProductionTuneSettingTable _productionTuneSettingTable = new ProductionTuneSettingTable();
  private ProductionTuneSettingTableModel _productionTuneSettingTableModel = new ProductionTuneSettingTableModel();
  private JButton _addRowButton = new JButton();
  private JButton _deleteRowButton = new JButton();
  
  private JLabel _directoryLabel = new JLabel();
  private JTextField _directoryTextField = new JTextField();
  private JButton _directoryChooserButton = new JButton();
  
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  
  public ProductionTuneSettingsDialog (Frame parent, String title)
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);
    
    jbInit();
    pack();
  }
  
  /**
   * @author Wei Chin
   */
  public void jbInit()
  {
    setLayout(_mainBorderLayout);
    add(_northPanel, BorderLayout.NORTH);
    add(_centerPanel, BorderLayout.CENTER);
    add(_southPanel, BorderLayout.SOUTH);
    
    //Khaw Chek Hau - XCR2367: Disable Save Only Specific Image feature in Production Tuning Option
    _northVerticalFlowLayout.setHgap(80);
    _northPanel.setLayout(_northVerticalFlowLayout);
    _centerPanel.setLayout(_centerBorderLayout);
    _centerButtonPanel.setLayout(_centerBottonFlowLayout);
    _centerDirectoryPanel.setLayout(_centerBottonFlowLayout);
    _southPanel.setLayout(_southFlowLayout);
    
    _northPanel.add(_saveOnlyDefectImagesRadioButton);
    _northPanel.add(_saveAllImagesRadioButton);
    //Khaw Chek Hau - XCR2367: Disable Save Only Specific Image feature in Production Tuning Option
    _northPanel.add(_saveSpecificImagesRadioButton);
    
    _saveOnlyDefectImagesRadioButton.setText(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_SAVE_DEFECT_ONLY_OPTION_KEY"));
    _saveAllImagesRadioButton.setText(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_SAVE_ALL_OPTION_KEY"));
    _saveSpecificImagesRadioButton.setText(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_SAVE_ONLY_SPECIFIC_OPTION_KEY"));
    
    _saveOnlyDefectImagesRadioButton.setSelected(true);
    _saveOnlyDefectImagesRadioButton.doClick();
    _saveOnlyDefectImagesRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _productionTuneSettingTable.setEnabled(false);
          _addRowButton.setEnabled(false);
          _deleteRowButton.setEnabled(false);
          _directoryTextField.setEnabled(false);
          _directoryChooserButton.setEnabled(false);
        }
      }
    });
    
    _saveAllImagesRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _productionTuneSettingTable.setEnabled(false);
          _addRowButton.setEnabled(false);
          _deleteRowButton.setEnabled(false);
          _directoryTextField.setEnabled(false);
          _directoryChooserButton.setEnabled(false);
        }
      }
    });
    
    _saveSpecificImagesRadioButton.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if(e.getStateChange() == ItemEvent.SELECTED)
        {
          _productionTuneSettingTable.setEnabled(true);
          _addRowButton.setEnabled(true);
          if(_productionTuneSettingTableModel.getRowCount() >= 0)
            _deleteRowButton.setEnabled(true);
          _directoryTextField.setEnabled(true);
          _directoryChooserButton.setEnabled(true);
        }
      }
    });
    
    _productionTuneSettingsButtonGroup.add(_saveOnlyDefectImagesRadioButton);
    _productionTuneSettingsButtonGroup.add(_saveAllImagesRadioButton);
    
    //Khaw Chek Hau - XCR2367: Disable Save Only Specific Image feature in Production Tuning Option
    _productionTuneSettingsButtonGroup.add(_saveSpecificImagesRadioButton);  
    _centerScrollPane.getViewport().setView(_productionTuneSettingTable);
    _centerPanel.add(_centerScrollPane, BorderLayout.NORTH);
    _centerPanel.add(_centerButtonPanel, BorderLayout.CENTER);
    _centerPanel.add(_centerDirectoryPanel, BorderLayout.SOUTH);
    _addRowButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _addRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _productionTuneSettingTableModel.addRows(1);
        if(_productionTuneSettingTableModel.getRowCount() >= 0)
          _deleteRowButton.setEnabled(true);
      }
    });
      
    _deleteRowButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _deleteRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if(_productionTuneSettingTable.getSelectedRow() == -1)
          _productionTuneSettingTableModel.removeRows(1);
        else
        {
          _productionTuneSettingTableModel.removeRowIndex(_productionTuneSettingTable.getSelectedRow());
        }
        if(_productionTuneSettingTableModel.getRowCount() <= 0)
          _deleteRowButton.setEnabled(false);
      }
    });
    
    //Khaw Chek Hau - XCR2367: Disable Save Only Specific Image feature in Production Tuning Option
    _centerButtonPanel.add(_addRowButton);
    _centerButtonPanel.add(_deleteRowButton); 
    
    _directoryLabel.setText(StringLocalizer.keyToString("CFGUI_SCRIPT_PATH_KEY"));
    _directoryTextField.setColumns(TEXTFIELD_COLUMN);
    _directoryChooserButton.setText(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _directoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        directoryChooser();
      }
    });;
    //Khaw Chek Hau - XCR2367: Disable Save Only Specific Image feature in Production Tuning Option
    _centerDirectoryPanel.add(_directoryLabel);
    _centerDirectoryPanel.add(_directoryTextField);
    _centerDirectoryPanel.add(_directoryChooserButton);
    
    _productionTuneSettingTable.setModel(_productionTuneSettingTableModel);
    _productionTuneSettingTable.setPreferredColumnWidths();
    _productionTuneSettingTable.setEditorsAndRenderers();
    
    _okButton.setText(StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"));
    _okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okActionPerformed(e);
      }
    });
    
    _cancelButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelActionPerformed(e);
      }
    });
    
    JComponent [] jComponents = {_okButton, _cancelButton};  
    SwingUtils.makeAllSameLength(jComponents);
    _southPanel.add(_okButton);
    _southPanel.add(_cancelButton);
  }
  
  /**
   * @author Wei Chin
   */
  public void populateData()
  {
    _productionTuneSettingTableModel.setData();
    
    ProductionTuneSettings productionTuneSettings = _productionTuneSettingTableModel.getProductionTuneSettings();
    if(productionTuneSettings.isSaveAllImages())
    {
      _saveAllImagesRadioButton.doClick();
      _saveAllImagesRadioButton.setSelected(true);
    }
    else if(productionTuneSettings.isSaveOnlyDefectImages())
    {
      _saveOnlyDefectImagesRadioButton.doClick();
      _saveOnlyDefectImagesRadioButton.setSelected(true);
    }
    else if(productionTuneSettings.isSaveOnlySelectedImages())
    {
      _saveSpecificImagesRadioButton.doClick();
      _saveSpecificImagesRadioButton.setSelected(true);
      
      if (productionTuneSettings.hasDirectoryPath())
      {
        _directoryTextField.setText(productionTuneSettings.getDirectoryPath());
      }
    }
  }
  
  /**
   * @param e 
   * @author Wei Chin
   */
  public void okActionPerformed(ActionEvent e)
  {  
    _productionTuneSettingTable.stopCellEditingForColumn();
    
    ProductionTuneSettings productionTuneSettings = _productionTuneSettingTableModel.getProductionTuneSettings();
    if(_productionTuneSettingTableModel.validateData())
    {
      _productionTuneSettingTableModel.updateProductionTuneSettingMap();
      if(_saveAllImagesRadioButton.isSelected())
      {
        productionTuneSettings.setSaveAllImages();
      }
      else if(_saveOnlyDefectImagesRadioButton.isSelected())
      {
        productionTuneSettings.setSaveOnlyDefectImages();
      }
      else if(_saveSpecificImagesRadioButton.isSelected())
      {
        productionTuneSettings.setSaveOnlySelectedImages();
        
        // If save specifc image, user is able to define the saving path
        productionTuneSettings.setDirectoryPath(_directoryTextField.getText());
      }      
      try
      {
        ProductionTuneSettings.writeSettings(productionTuneSettings);
      }
      catch(DatastoreException de)
      {
        MessageDialog.showErrorDialog(this, de.getMessage(), StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_DIALOG_KEY"), true);
      }
      dispose();
    }
    else
    {
      MessageDialog.showErrorDialog(this, StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_ERROR_EMPTY_ENTRY_KEY"), 
        StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_DIALOG_KEY"), true);
      return;
    }
  }
  
   /**
   * @param e 
   * @author Wei Chin
   */
  public void cancelActionPerformed(ActionEvent e)
  {
    dispose();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void directoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_directoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(),
                               StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _directoryTextField.setText(fileName);
      _directoryTextField.requestFocus();
    }
  }
}
