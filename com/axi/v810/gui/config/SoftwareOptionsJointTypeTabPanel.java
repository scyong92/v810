package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * This is the Joint Type Assignment tab screen under the Config UI, Software Options task.
 * @author Laura Cormos
 */
public class SoftwareOptionsJointTypeTabPanel extends AbstractTaskPanel implements Observer
{
  private JointTypeEnumAssignment _jointTypeAssignment;
  private java.util.List<String> _rulesSetsOnDisk;
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles
  private boolean _ruleSetIsDirty = false;
  private String _currentRuleSetName;
  private boolean _loadingInProgress = false;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  // GUI elements
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JPanel _rulesPanel = new JPanel();
  private BorderLayout _rulesPanelBorderLayout = new BorderLayout();
  private JPanel _ruleSetNamePanel = new JPanel();
  private BorderLayout _ruleSetNamePanelBorderLayout = new BorderLayout();
  private JPanel _ruleSetSelectPanel = new JPanel();
  private FlowLayout _ruleSetSelectPanelFlowLayout = new FlowLayout();
  private JLabel _ruleSetSelectNameLabel = new JLabel();
  private JComboBox _ruleSetSelectNameCombobox = new JComboBox();
  private ActionListener _rulesSetSelectComboboxActionListener;
  private JPanel _ruleSetSelectActionsWrapperPanel = new JPanel();
  private FlowLayout _ruleSetSelectActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _ruleSetSelectActionsPanel = new JPanel();
  private GridLayout _ruleSetSelectActionsPanelGridLayout = new GridLayout();
  private JButton _newRuleSetButton = new JButton();
  private JButton _saveRuleSetButton = new JButton();
  private JButton _saveAsRuleSetButton = new JButton();
  private JButton _deleteRuleSetButton = new JButton();

  private JPanel _rulesDefinitionPanel = new JPanel();
  private BorderLayout _rulesDefinitionPanelBorderLayout = new BorderLayout();
  private JPanel _rulesRegularExpressionPanel = new JPanel();
  private BorderLayout _rulesKeyRegularExpressionPanelBorderLayout = new BorderLayout();
  private JScrollPane _rulesRegularExpressionScrollPane = new JScrollPane();
  private JointTypeRulesTable _rulesTable = new JointTypeRulesTable();
  private JointTypeRulesTableModel _rulesTableModel;
  private JPanel _rowActivityPanel = new JPanel();
  private BorderLayout _rowActivityPanelBorderLayout = new BorderLayout();
  private JPanel _rowManipulationPanel = new JPanel();
  private FlowLayout _rowManipulationPanelFlowLayout = new FlowLayout();
  private JLabel _moveRowsLabel = new JLabel();
  private JPanel _rowManipulationButtonsPanel = new JPanel();
  private GridLayout _rowManipulationButtonsPanelGridLayout = new GridLayout();
  private JButton _rowUpButton = new JButton();
  private JButton _rowDownButton = new JButton();
  private JPanel _warningLabelsPanel = new JPanel();
  private VerticalFlowLayout _warningLabelsPanelVerticalFlowLayout = new VerticalFlowLayout();
  private JLabel _rowOrderLabel = new JLabel();
  private JLabel _columnDataLabel = new JLabel();
  private JLabel _columnAsteriskLabel = new JLabel();
  private JPanel _rowActionsWrapperPanel = new JPanel();
  private FlowLayout _rowActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _rowActionsPanel = new JPanel();
  private GridLayout _rowActionsPanelGridLayout = new GridLayout();
  private JButton _addRowButton = new JButton();
  private JButton _deleteRowButton = new JButton();

  private JPanel _selectRuleSetToUsePanel = new JPanel();
  private FlowLayout _selectRuleSetToUsePanelFlowLayout = new FlowLayout();
  private JLabel _selectRuleSetToUseLabel = new JLabel();
  private JComboBox _ruleSetToUseComboBox = new JComboBox();
  private ActionListener _ruleSetToUseComboboxActionListener;


  /**
   * @author Laura Cormos
   */
  public SoftwareOptionsJointTypeTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    try
    {
      jbInit();
      _jointTypeAssignment = JointTypeEnumAssignment.getInstance();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    // set up panel GUI components
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _centerPanel.add(_rulesPanel, BorderLayout.CENTER);
    _centerPanel.add(_selectRuleSetToUsePanel, BorderLayout.SOUTH);

    _rulesPanel.setLayout(_rulesPanelBorderLayout);
    _rulesPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_JTA_JOINT_TYPE_ASSIGNMENT_RULES_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _rulesPanel.add(_ruleSetNamePanel, BorderLayout.NORTH);
    _rulesPanel.add(_rulesDefinitionPanel, BorderLayout.CENTER);

    _ruleSetNamePanel.setLayout(_ruleSetNamePanelBorderLayout);
    _ruleSetNamePanel.add(_ruleSetSelectPanel, BorderLayout.WEST);
    _ruleSetNamePanel.add(_ruleSetSelectActionsWrapperPanel, BorderLayout.EAST);

    _ruleSetSelectPanel.setLayout(_ruleSetSelectPanelFlowLayout);
    _ruleSetSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _ruleSetSelectPanel.add(_ruleSetSelectNameLabel);
    _ruleSetSelectPanel.add(_ruleSetSelectNameCombobox);

    _ruleSetSelectNameLabel.setText(StringLocalizer.keyToString("CFGUI_NAME_KEY"));

    _rulesSetSelectComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ruleSetSelectNameCombobox_actionPerformed(e);
      }
    };

    _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);

    _ruleSetSelectActionsWrapperPanel.setLayout(_ruleSetSelectActionsWrapperPanelFlowLayout);
    _ruleSetSelectActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _ruleSetSelectActionsWrapperPanel.add(_ruleSetSelectActionsPanel);

    _ruleSetSelectActionsPanel.setLayout(_ruleSetSelectActionsPanelGridLayout);
    _ruleSetSelectActionsPanelGridLayout.setColumns(4);
    _ruleSetSelectActionsPanelGridLayout.setHgap(10);
    _ruleSetSelectActionsPanelGridLayout.setRows(0);
    _ruleSetSelectActionsPanelGridLayout.setVgap(10);
    _ruleSetSelectActionsPanel.add(_newRuleSetButton);
    _ruleSetSelectActionsPanel.add(_saveRuleSetButton);
    _ruleSetSelectActionsPanel.add(_saveAsRuleSetButton);
    _ruleSetSelectActionsPanel.add(_deleteRuleSetButton);

    _newRuleSetButton.setText(StringLocalizer.keyToString("CFGUI_NEW_BUTTON_KEY"));
    _newRuleSetButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        newRuleSetButton_actionPerformed(e);
      }
    });

    _saveRuleSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_BUTTON_KEY"));
    _saveRuleSetButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveCurrentRuleSet();
      }
    });

    _saveAsRuleSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_AS_BUTTON_KEY"));
    _saveAsRuleSetButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveRuleSetAs();
      }
    });

    _deleteRuleSetButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteRuleSetButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRuleSetButton_actionPerformed(e);
      }
    });


    _rulesDefinitionPanel.setLayout(_rulesDefinitionPanelBorderLayout);
    _rulesDefinitionPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_JTA_RULES_DEFINITION_KEY")),
        BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _rulesDefinitionPanel.add(_rulesRegularExpressionPanel, BorderLayout.CENTER);
    _rulesDefinitionPanel.add(_rowActivityPanel, BorderLayout.SOUTH);

    _rulesRegularExpressionPanel.setLayout(_rulesKeyRegularExpressionPanelBorderLayout);
    JPanel intermediatePanel = new JPanel(new BorderLayout());
    _rulesRegularExpressionPanel.add(intermediatePanel, BorderLayout.NORTH);
    _rulesRegularExpressionPanel.add(_rulesRegularExpressionScrollPane, BorderLayout.CENTER);
    _rulesRegularExpressionPanel.add(_columnAsteriskLabel, BorderLayout.SOUTH);

    intermediatePanel.add(_rowManipulationPanel, BorderLayout.WEST);
    intermediatePanel.add(_warningLabelsPanel, BorderLayout.EAST);

    _rowManipulationPanel.setLayout(_rowManipulationPanelFlowLayout);
    _rowManipulationPanel.add(_moveRowsLabel);
    _rowManipulationPanel.add(_rowManipulationButtonsPanel);

    _moveRowsLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_MOVE_SELECTED_ROW_KEY"));

    _rowManipulationButtonsPanel.setLayout(_rowManipulationButtonsPanelGridLayout);
    _rowManipulationButtonsPanelGridLayout.setRows(2);
    _rowManipulationButtonsPanel.add(_rowUpButton);
    _rowManipulationButtonsPanel.add(_rowDownButton);

    _rowUpButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_UP_ARROW));
    _rowUpButton.setEnabled(false);
    _rowUpButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rowUpButton_actionPerformed(e);
      }
    });

    _rowDownButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_DOWN_ARROW));
    _rowDownButton.setEnabled(false);
    _rowDownButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        rowDownButton_actionPerformed(e);
      }
    });

    _warningLabelsPanel.setLayout(_warningLabelsPanelVerticalFlowLayout);
    _warningLabelsPanelVerticalFlowLayout.setVerticalAlignment(VerticalFlowLayout.RIGHT);
    _warningLabelsPanel.add(_rowOrderLabel);
    _warningLabelsPanel.add(_columnDataLabel);

    _rowOrderLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_ROW_ORDER_MATTERS_NOTICE_KEY"));
    _rowOrderLabel.setForeground(_labelColor);

    _columnDataLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_COLUMN_DATA_NOTICE_KEY"));
    _columnDataLabel.setForeground(_labelColor);

    _rulesTableModel = new JointTypeRulesTableModel(this);
    _rulesTable.setModel(_rulesTableModel);
    _rulesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _rulesTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _rulesTable.getTableHeader().setReorderingAllowed(false);
//    _rulesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    ((DefaultTableCellRenderer)_rulesTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
    final ListSelectionModel ruleSetRowSelectionModel = _rulesTable.getSelectionModel();
    ruleSetRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
//        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = _rulesTable.getSelectedRow();
          _rulesTableModel.setSelectedRow(_rulesTable.getSelectedRow());
          setMoveButtonsEnabledBasedOnTableSelection(selectedRow);
          setDeleteButtonBasedOnTableSelection(selectedRow);
        }
      }
    });

    _rulesRegularExpressionScrollPane.getViewport().add(_rulesTable);

    _columnAsteriskLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_REGEX_PATTERN_REQUIRED_NOTICE_KEY"));

    _rowActivityPanel.setLayout(_rowActivityPanelBorderLayout);
    _rowActivityPanel.add(_rowActionsWrapperPanel, BorderLayout.EAST);



    _rowActionsWrapperPanel.setLayout(_rowActionsWrapperPanelFlowLayout);
    _rowActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _rowActionsWrapperPanel.add(_rowActionsPanel);

    _rowActionsPanel.setLayout(_rowActionsPanelGridLayout);
    _rowActionsPanelGridLayout.setColumns(2);
    _rowActionsPanelGridLayout.setHgap(10);
    _rowActionsPanelGridLayout.setRows(0);
    _rowActionsPanelGridLayout.setVgap(10);
    _rowActionsPanel.add(_addRowButton);
    _rowActionsPanel.add(_deleteRowButton);

    _addRowButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _addRowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addRowButton_actionPerformed(e);
      }
    });

    _deleteRowButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _deleteRowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRowButton_actionPerformed(e);
      }
    });


    _selectRuleSetToUsePanel.setLayout(_selectRuleSetToUsePanelFlowLayout);
    _selectRuleSetToUsePanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _selectRuleSetToUsePanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_JTA_SELECT_RULE_SET_TO_USE_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));

    _selectRuleSetToUsePanel.add(_selectRuleSetToUseLabel);
    _selectRuleSetToUsePanel.add(_ruleSetToUseComboBox);

    _selectRuleSetToUseLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_SELECT_RULE_SET_TO_USE_LABEL_KEY"));

    _ruleSetToUseComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ruleSetToUseComboBox_actionPerformed();
      }
    };
    _ruleSetToUseComboBox.addActionListener(_ruleSetToUseComboboxActionListener);


    setupTableEditors();
    // define menus and toolBar items to be added later

  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("SWOptionsJointTypeTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    // don't want to be able to import a project while changing a rule set
    _mainUI.getMainMenuBar().disableImportMenuItem();

    _mainUI.setStatusBarText("");

    populateWithConfigData();
    // add custom menus

    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    if (_rulesTable.isEditing())
      _rulesTable.getCellEditor().stopCellEditing();

    return saveRuleSetIfNecessary();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    // restore ability to import a project
    _mainUI.getMainMenuBar().enableImportMenuItem();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("SWOptionsJointTypeTabPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void newRuleSetButton_actionPerformed(ActionEvent e)
  {
    if (saveRuleSetIfNecessary() == false)
      return;

    String newRuleSetName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_JTA_ENTER_NEW_RULESET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_JTA_NEW_RULE_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newRuleSetName != null) // null means the dialog was canceled
    {
      newRuleSetName = getLegalFileName(newRuleSetName);
      if (newRuleSetName == null)
        return;

      // clear the rules table and joint type assignment rule hodler
      _rulesTableModel.clearData();
      JointTypeEnumRule defaultRule = new JointTypeEnumRule(JointTypeEnum.GULLWING);

      _rulesTableModel.addNewRule(defaultRule);
      java.util.List<JointTypeEnumRule> allRules = new ArrayList<JointTypeEnumRule>();
      allRules.add(defaultRule);
      _jointTypeAssignment.setNewRulesList(allRules);

      setDeleteButtonBasedOnTableSelection(_rulesTable.getSelectedRow());
      setMoveButtonsEnabledBasedOnTableSelection(_rulesTable.getSelectedRow());

      // add new rule set to the select combobox
      _ruleSetSelectNameCombobox.addItem(newRuleSetName);
      _ruleSetSelectNameCombobox.removeActionListener(_rulesSetSelectComboboxActionListener);
      _ruleSetSelectNameCombobox.setSelectedItem(newRuleSetName);
      _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);

      checkProtectedRuleSets();
      _ruleSetIsDirty = true; // cannot change selection without a save since this rule set is not saved to disk yet
      _currentRuleSetName = newRuleSetName;
    }
  }

  /**
   * @author Laura Cormos
   */
  private boolean saveCurrentRuleSet()
  {
   if (ruleSetIsValid(_jointTypeAssignment.getJointTypeEnumRules()) == false)
      return false;

    try
    {
      _jointTypeAssignment.save(_currentRuleSetName);
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      return false;
    }
    _ruleSetIsDirty = false;

    // add any new rule set that has never been saved to the 2 comboboxes
    if (comboBoxContainsItem(_ruleSetSelectNameCombobox, _currentRuleSetName) == false)
      _ruleSetSelectNameCombobox.addItem(_currentRuleSetName);
    if (comboBoxContainsItem(_ruleSetToUseComboBox, _currentRuleSetName) == false)
      _ruleSetToUseComboBox.addItem(_currentRuleSetName);

    _rulesSetsOnDisk.add(_currentRuleSetName);

    _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_SAVED_RULES_SET_KEY",
                                                                             new Object[]{_currentRuleSetName})));
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private boolean saveRuleSetAs()
  {
    String newRuleSetName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_JTA_ENTER_NEW_RULESET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_JTA_NEW_RULE_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newRuleSetName != null) // null means the dialog was canceled
    {
      newRuleSetName = getLegalFileName(newRuleSetName);
      if (newRuleSetName == null)
        return false;

      java.util.List<JointTypeEnumRule> rulesList = _rulesTableModel.getData();
      if (ruleSetIsValid(rulesList) == false)
        return false;

      try
      {
        _jointTypeAssignment.setNewRulesList(rulesList);
        _jointTypeAssignment.save(newRuleSetName);
        // clear the undo stack when a new rule set is loaded in the GUI (save as loads a new rule set)
        clearUndoStack();
        _currentRuleSetName = newRuleSetName;
        if (ruleSetNameComboBoxContainsName(newRuleSetName) == false)
          _ruleSetSelectNameCombobox.addItem(newRuleSetName);
        _ruleSetSelectNameCombobox.removeActionListener(_rulesSetSelectComboboxActionListener);
        _ruleSetSelectNameCombobox.setSelectedItem(newRuleSetName);
        _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);
        _ruleSetToUseComboBox.addItem(newRuleSetName);
        _rulesSetsOnDisk.add(newRuleSetName);
        setMoveButtonsEnabledBasedOnTableSelection(_rulesTable.getSelectedRow());
        checkProtectedRuleSets();
        _ruleSetIsDirty = false;
        return true;
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
    return false;
  }

  /**
  * @author Laura Cormos
  */
 private String getLegalFileName(String newRuleSetName)
 {
   Assert.expect(newRuleSetName != null);

   String whiteSpacePattern = "^\\s*$";
   java.util.List<String> tempConfigNamesList = new ArrayList<String>();
   for (String name : _rulesSetsOnDisk)
     tempConfigNamesList.add(name.toUpperCase());

   String tempRuleSetName = newRuleSetName.toUpperCase();
   while (newRuleSetName != null &&
          (tempConfigNamesList.contains(tempRuleSetName) ||
           FileName.hasIllegalChars(newRuleSetName) ||
           Pattern.matches(whiteSpacePattern, newRuleSetName)))
   {
     if (tempConfigNamesList.contains(tempRuleSetName))
     {
       newRuleSetName = JOptionPane.showInputDialog(_mainUI,
                                                    StringLocalizer.keyToString(new LocalizedString(
                                                        "CFGUI_JTA_RULE_SET_EXISTS_MESSAGE_KEY",
                                                        new Object[]{newRuleSetName})),
                                                    StringLocalizer.keyToString("CFGUI_JTA_NEW_RULE_SET_TITLE_KEY"),
                                                    JOptionPane.INFORMATION_MESSAGE);
       if (newRuleSetName != null)
         tempRuleSetName = newRuleSetName.toUpperCase();
       else
         return newRuleSetName;
     }
     else if (FileName.hasIllegalChars(newRuleSetName))
     {
       newRuleSetName = JOptionPane.showInputDialog(_mainUI,
                                                    StringLocalizer.keyToString(new LocalizedString(
                                                        "CFGUI_JTA_RULE_SET_ILLEGAL_NAME_KEY",
                                                        new Object[]{newRuleSetName, FileName.getIllegalChars()})),
                                                    StringLocalizer.keyToString("CFGUI_JTA_NEW_RULE_SET_TITLE_KEY"),
                                                    JOptionPane.INFORMATION_MESSAGE);
       if (newRuleSetName != null)
         tempRuleSetName = newRuleSetName.toUpperCase();
       else
         return newRuleSetName;
     }
     else if (Pattern.matches(whiteSpacePattern, newRuleSetName))
     {
       newRuleSetName = JOptionPane.showInputDialog(_mainUI,
                                                    StringLocalizer.keyToString(new LocalizedString(
                                                        "CFGUI_JTA_RULE_SET_ILLEGAL_EMPTY_NAME_KEY",
                                                        new Object[]{newRuleSetName})),
                                                    StringLocalizer.keyToString("CFGUI_JTA_NEW_RULE_SET_TITLE_KEY"),
                                                    JOptionPane.INFORMATION_MESSAGE);
       if (newRuleSetName != null)
         tempRuleSetName = newRuleSetName.toUpperCase();
       else
         return newRuleSetName;
     }
   }
   return newRuleSetName;
  }


  /**
   * @author Laura Cormos
   */
  private void deleteRuleSetButton_actionPerformed(ActionEvent e)
  {
    Object value = _ruleSetSelectNameCombobox.getSelectedItem();
    Assert.expect(value instanceof String);
    String selectedRuleSetName = (String)value;

    int answer = JOptionPane.showConfirmDialog(_mainUI,
                                               StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_DELETE_RULE_SET_KEY",
                                                                                               new Object[]{selectedRuleSetName})),
                                               StringLocalizer.keyToString("CFGUI_JTA_CONFIRM_DELETE_TITLE_KEY"),
                                               JOptionPane.YES_NO_OPTION);
    if (answer != JOptionPane.YES_OPTION)
      return;

    try
    {
      _jointTypeAssignment.deleteRuleSet(selectedRuleSetName);

      _ruleSetSelectNameCombobox.removeActionListener(_rulesSetSelectComboboxActionListener);
      _ruleSetSelectNameCombobox.removeItem(selectedRuleSetName);
      _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);
      _ruleSetToUseComboBox.removeActionListener(_ruleSetToUseComboboxActionListener);
      _ruleSetToUseComboBox.removeItem(selectedRuleSetName);
      _ruleSetToUseComboBox.removeActionListener(_ruleSetToUseComboboxActionListener);
      ruleSetToUseComboBox_actionPerformed();
      _rulesSetsOnDisk.remove(selectedRuleSetName);
      _ruleSetIsDirty = false;
      populateRuleSetToUseCombobox();
      clearUndoStack();
      // set selection to another item in the list for the 'select name' combobox
      int index = _ruleSetSelectNameCombobox.getSelectedIndex();
      int lastIndex = _ruleSetSelectNameCombobox.getItemCount() - 1 ;
      if (index > lastIndex ||
          (index == -1 && lastIndex != -1))
        _ruleSetSelectNameCombobox.setSelectedIndex(lastIndex);
      else if (index != -1)
        _ruleSetSelectNameCombobox.setSelectedIndex(index);
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void ruleSetSelectNameCombobox_actionPerformed(ActionEvent e)
  {
    if (saveRuleSetIfNecessary() == false)
    {
      // reverse the selection if the save fell through
      _ruleSetSelectNameCombobox.setSelectedItem(_currentRuleSetName);
      return;
    }
    _ruleSetIsDirty = false;
    Object selection = _ruleSetSelectNameCombobox.getSelectedItem();
    if (selection != null)
    {
      Assert.expect(selection instanceof String);
      String selectedRuleSet = (String)selection;

      checkProtectedRuleSets();
      try
      {
        _loadingInProgress = true;
        _jointTypeAssignment.load(selectedRuleSet);
        _loadingInProgress = false;
        // clear the undo stack when a new rule set is loaded in the GUI
        clearUndoStack();
        _rulesTableModel.setData(_jointTypeAssignment.getJointTypeEnumRules());
        setMoveButtonsEnabledBasedOnTableSelection(_rulesTable.getSelectedRow());
        setDeleteButtonBasedOnTableSelection(_rulesTable.getSelectedRow());
        _currentRuleSetName = selectedRuleSet;
        resetStatusBar();
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void checkProtectedRuleSets()
  {
    Object object = _ruleSetSelectNameCombobox.getSelectedItem();
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String)object;
      Boolean enabled = true;
      // if the file is the default file, disable the save and delete buttons, otherwise, enable them
      if (selectedName.equals(FileName.getJointTypeAssignmentDefaultFileWithoutExtension()))
        enabled = false;
      _saveRuleSetButton.setEnabled(enabled);
      _deleteRuleSetButton.setEnabled(enabled);
    }
  }

  /**
   * @author Laura Cormos
   */
  private boolean isRuleSetProtected(String ruleSetName)
  {
    Assert.expect(ruleSetName != null);

    if (ruleSetName.equals(FileName.getJointTypeAssignmentDefaultFileWithoutExtension()))
      return true;
    return false;
  }

  /**
   * @author Laura Cormos
   */
  private void addRowButton_actionPerformed(ActionEvent e)
  {
    JointTypeEnumRule newRule = new JointTypeEnumRule(JointTypeEnum.GULLWING); // always use Gullwing as default, catch all rule
    java.util.List<JointTypeEnumRule> allRules = _jointTypeAssignment.getJointTypeEnumRules();
    allRules.add(newRule);
    _jointTypeAssignment.setNewRulesList(allRules);
    _rulesTableModel.addNewRule(newRule);
    _rulesTable.setRowSelectionInterval(_rulesTableModel.getRowCount() - 1, _rulesTableModel.getRowCount() - 1);
    _ruleSetIsDirty = true;
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void deleteRowButton_actionPerformed(ActionEvent e)
  {
    int selectedRowIndex = _rulesTable.getSelectedRow();
    JointTypeEnumRule selectedRule = _rulesTableModel.getRuleAt(selectedRowIndex);
    try
    {
      _commandManager.execute(new ConfigDeleteJointTypeRuleCommand(_rulesTable, selectedRule));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      return;
    }

    // if the last row was deleted, disable both move buttons
    if (selectedRowIndex == 0)
      setMoveButtonsEnabledBasedOnTableSelection( -1);
    else
      setMoveButtonsEnabledBasedOnTableSelection(selectedRowIndex);

    // enable/disable the Delete button based on table selection
    setDeleteButtonBasedOnTableSelection(_rulesTable.getSelectedRow());

    // set another selection when the current rule is removed
    int lastRowIndex = _rulesTableModel.getRowCount() - 1;
    if (lastRowIndex >= 0)
    {
      if (selectedRowIndex > lastRowIndex)
        _rulesTable.setRowSelectionInterval(lastRowIndex, lastRowIndex);
      else
        _rulesTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
    }

    _ruleSetIsDirty = true;
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void rowUpButton_actionPerformed(ActionEvent e)
  {
    int rowIndex = _rulesTable.getSelectedRow();
    _rulesTableModel.moveRowUp(rowIndex);
    _rulesTable.setRowSelectionInterval(rowIndex - 1, rowIndex - 1);
    _ruleSetIsDirty = true;
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  public void resetRuleSet(java.util.List<JointTypeEnumRule> newRulesList)
  {
    Assert.expect(newRulesList != null);
    _jointTypeAssignment.setNewRulesList(newRulesList);
  }

  /**
   * @author Laura Cormos
   */
  private void rowDownButton_actionPerformed(ActionEvent e)
  {
    int rowIndex = _rulesTable.getSelectedRow();
    _rulesTableModel.moveRowDown(rowIndex);
    _rulesTable.setRowSelectionInterval(rowIndex + 1, rowIndex + 1);
    _ruleSetIsDirty = true;
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void ruleSetToUseComboBox_actionPerformed()
  {
    Object object = _ruleSetToUseComboBox.getSelectedItem();
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String)object;
      Object object2 = _ruleSetSelectNameCombobox.getSelectedItem();
      Assert.expect(object2 instanceof String);
      String ruleSetInEditScreenName = (String)object2;
      // can't set a rules set to use on project import unless the set is first saved to disk
      if (selectedName.equals(ruleSetInEditScreenName))
      {
        if (saveRuleSetIfNecessary() == false)
        {
          resetRuleSetToUseComboBox();
          return;
        }
      }
      try
      {
        _commandManager.execute(new ConfigSetJointTypeAssignmentFileNameToUseCommand(selectedName));
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setupTableEditors()
  {
    _rulesTable.setPreferredColumnWidths();
    _rulesTable.setEditorsAndRenderers();
  }

  /**
   * @author Laura Cormos
   */
  private void populateWithConfigData()
  {
    Assert.expect(_jointTypeAssignment != null);
    populateRuleSetSelectCombobox();
    populateRuleSetToUseCombobox();
    try
    {
      if (_currentRuleSetName == null)
      {
        // load the rule set pointed to by the sw config enum
        _jointTypeAssignment.load();
        _currentRuleSetName = _jointTypeAssignment.getJointTypeAssignmentConfigFileNameWithoutExtension();
      }
      else
        _jointTypeAssignment.load(_currentRuleSetName);

      java.util.List<JointTypeEnumRule> rules = _jointTypeAssignment.getJointTypeEnumRules();
      _rulesTableModel.setData(rules);
      setMoveButtonsEnabledBasedOnTableSelection(_rulesTable.getSelectedRow());
      setDeleteButtonBasedOnTableSelection(_rulesTable.getSelectedRow());
      checkProtectedRuleSets();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateRuleSetSelectCombobox()
  {
    _rulesSetsOnDisk = FileName.getJointTypeAssignmentFiles();
    if (_rulesSetsOnDisk.isEmpty())
    {
      try
      {
        _jointTypeAssignment.load();
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
      _rulesSetsOnDisk = FileName.getJointTypeAssignmentFiles();
      Assert.expect(_rulesSetsOnDisk.isEmpty() == false);
    }

    _ruleSetSelectNameCombobox.removeActionListener(_rulesSetSelectComboboxActionListener);
    _ruleSetSelectNameCombobox.removeAllItems();
    for (String ruleSetName : _rulesSetsOnDisk)
    {
      _ruleSetSelectNameCombobox.addItem(ruleSetName);
    }

    String ruleSetToSelect;
    if (_currentRuleSetName != null)
      ruleSetToSelect = _currentRuleSetName;
    else
      ruleSetToSelect = _jointTypeAssignment.getJointTypeAssignmentConfigFileNameWithoutExtension();

    // if the file set in sw.config is missing on disk, reset to default JTA file
    if(_rulesSetsOnDisk.contains(ruleSetToSelect) == false)
    {
      String defaultJTAFileName = FileName.getJointTypeAssignmentDefaultFileWithoutExtension();
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_RULE_SET_DOES_NOT_EXIST_MSG_KEY",
                                                                                    new Object[]{ruleSetToSelect,
                                                                                                 defaultJTAFileName})),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      ruleSetToSelect = defaultJTAFileName;
      try
      {
        _jointTypeAssignment.setJointTypeAssignmentConfigFileNameWithoutExtension(ruleSetToSelect);
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
    _ruleSetSelectNameCombobox.setSelectedItem(ruleSetToSelect);
    _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  private void populateRuleSetToUseCombobox()
  {
    Assert.expect(_rulesSetsOnDisk != null);

    _ruleSetToUseComboBox.removeActionListener(_ruleSetToUseComboboxActionListener);

    _ruleSetToUseComboBox.removeAllItems();
    for (String ruleSetName : _rulesSetsOnDisk)
    {
      _ruleSetToUseComboBox.addItem(ruleSetName);
    }
    String ruleSetToUse = _jointTypeAssignment.getJointTypeAssignmentConfigFileNameWithoutExtension();
    // if the file set in sw.config is missing on disk, reset to default JTA file
    if(_rulesSetsOnDisk.contains(ruleSetToUse) == false)
    {
      String defaultJTAFileName = FileName.getJointTypeAssignmentDefaultFileWithoutExtension();
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_RULE_SET_DOES_NOT_EXIST_MSG_KEY",
                                                                                    new Object[]{ruleSetToUse,
                                                                                                 defaultJTAFileName})),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      ruleSetToUse = defaultJTAFileName;
      try
      {
        _jointTypeAssignment.setJointTypeAssignmentConfigFileNameWithoutExtension(ruleSetToUse);
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
    _ruleSetToUseComboBox.setSelectedItem(ruleSetToUse);
    _ruleSetToUseComboBox.addActionListener(_ruleSetToUseComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  public void setRuleSetIsDirty()
  {
    _ruleSetIsDirty = true;
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  public void setEnabledRowMoveButtons(boolean enabled)
  {
    _rowUpButton.setEnabled(enabled);
    _rowDownButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void setMoveButtonsEnabledBasedOnTableSelection(int selectedRowIndex)
  {
    if (selectedRowIndex == -1)
      setEnabledRowMoveButtons(false);
    else
    {
      setEnabledRowMoveButtons(true);
      if (selectedRowIndex == _rulesTableModel.getRowCount() - 1) // last row
        _rowDownButton.setEnabled(false);
      if (selectedRowIndex == 0) // first row
        _rowUpButton.setEnabled(false);
    }
  }

  /**
   * @author Laura Cormos
   */
  void showDatastoreError(DatastoreException ex)
  {
    Assert.expect(ex != null);

    JOptionPane.showMessageDialog(_mainUI,
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Laura Cormos
   */
  private boolean comboBoxContainsItem(JComboBox comboBox, String item)
  {
    Assert.expect(comboBox != null);
    Assert.expect(item != null);

    java.util.List<String> allItmes = new ArrayList<String>();
    for (int i = 0; i < comboBox.getModel().getSize(); i++)
    {
      Object comboBoxItem = comboBox.getModel().getElementAt(i);
      Assert.expect(comboBoxItem instanceof String);
      allItmes.add((String)comboBoxItem);
    }

    if (allItmes.contains(item))
      return true;

    return false;
  }

  /**
   * @author Laura Cormos
   */
  private boolean saveRuleSetIfNecessary()
  {
    if (_ruleSetIsDirty)
    {
      String saveChangesQuestion;
      if (isRuleSetProtected(_currentRuleSetName))
        saveChangesQuestion = StringLocalizer.keyToString("CFGUI_JTA_SAVE_CHANGES_PROTECTED_RULE_SET_KEY");
      else
        saveChangesQuestion = StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_SAVE_CHANGES_KEY",
                                                                              new Object[]{_currentRuleSetName}));
      int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 saveChangesQuestion,
                                                 StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
                                                 JOptionPane.YES_NO_CANCEL_OPTION);
      if (answer == JOptionPane.CANCEL_OPTION)
        return false;
      else if (answer == JOptionPane.YES_OPTION)
      {
        if (isRuleSetProtected(_currentRuleSetName))
          return saveRuleSetAs();
        else
          return saveCurrentRuleSet();
      }
      else
      {
        // if the user wants to discard the changes to a new rule set, remove it from the select combobox because
        // it has never been saved to disk. A rule set is new if it doesn't exist on disk
        if (_rulesSetsOnDisk.contains(_currentRuleSetName) == false)
        {
          _ruleSetSelectNameCombobox.removeActionListener(_rulesSetSelectComboboxActionListener);
          _ruleSetSelectNameCombobox.removeItem(_currentRuleSetName);
          _ruleSetSelectNameCombobox.addActionListener(_rulesSetSelectComboboxActionListener);
          if (_ruleSetSelectNameCombobox.getSelectedIndex() >= 0)
            _currentRuleSetName = (String)_ruleSetSelectNameCombobox.getSelectedItem();
          else
            _currentRuleSetName = null;
        }
        // clear undo stack because what ever changes the user made, they chose to discard them
        clearUndoStack();
      }
    }
    _ruleSetIsDirty = false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteButtonBasedOnTableSelection(int rowIndex)
  {
    _deleteRowButton.setEnabled(rowIndex >= 0);
  }

  /**
   * @author Laura Cormos
   */
  void editCell(int rowIndex, int colIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(colIndex >= 0);

    _rulesTable.setRowSelectionInterval(rowIndex, rowIndex);
    _rulesTable.editCellAt(rowIndex, colIndex);
  }

  /**
   * @author Laura Cormos
   */
  private boolean ruleSetIsValid(java.util.List<JointTypeEnumRule> rulesList)
  {
    Assert.expect(rulesList != null);

    boolean errorFound = false;
    boolean warningFound = false;

    String errorMessages = StringLocalizer.keyToString("CFGUI_JTA_FIX_ERRORS_MESSAGE_KEY");

    for (JointTypeEnumRule rule : rulesList)
    {
      if (rule.isDefaultRule() && rulesList.lastIndexOf(rule) < (rulesList.size() - 1))
      {
        errorMessages += StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_DEFAULT_RULE_IS_NOT_LAST_MSG_KEY",
                                                                         new Object[]{(rulesList.lastIndexOf(rule) + 1)}));
        errorMessages += "\n\n";
        warningFound = true;
      }
      if (rule.isRefDesPatternSet())
      {
        String pattern = rule.getRefDesPattern();
        try
        {
          RegularExpressionUtilAxi.checkRegExValid(pattern);
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          errorMessages += ex.getLocalizedMessage();
          errorMessages += "\n\n";
          errorFound = true;
        }
      }
      if (rule.isLandPatternNamePatternSet())
      {
        String pattern = rule.getLandPatternNamePattern();
        try
        {
          RegularExpressionUtilAxi.checkRegExValid(pattern);
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          errorMessages += ex.getLocalizedMessage();
          errorMessages += "\n\n";
          errorFound = true;
        }
      }
    }
    // check that the last enabled rule in the set is a default catch all rule with no definitions
    int index = rulesList.size() - 1;
    // if the rules set is empty, put out an error message
    if (index == -1)
    {
      errorMessages += StringLocalizer.keyToString("CFGUI_JTA_DEFAULT_RULE_DOES_NOT_EXIST_MSG_KEY");
      errorMessages += "\n\n";
      errorFound = true;
    }
    else
    {
      JointTypeEnumRule lastRule = rulesList.get(index);
      // keep looping until we find an enabled rule
      while (lastRule.isEnabled() == false)
      {
        --index;
        if (index == -1)
        {
          errorMessages += StringLocalizer.keyToString("CFGUI_JTA_DEFAULT_RULE_DOES_NOT_EXIST_MSG_KEY");
          errorMessages += "\n\n";
          errorFound = true;
          break;
        }
        else
          lastRule = rulesList.get(index);
      }

      if (errorFound == false && lastRule.isDefaultRule() == false)
      {
        errorMessages += StringLocalizer.keyToString("CFGUI_JTA_DEFAULT_RULE_DOES_NOT_EXIST_MSG_KEY");
        errorMessages += "\n\n";
        errorFound = true;
      }
      else
      {
        // everything looks good, one last check whether the default rule's joint type is cap, res or pcap
        // these 3 are not allowed as catch-all joint types
        if (lastRule.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR) ||
            lastRule.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) ||
            lastRule.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP))
            // Wei Chin (Tall Cap)
//            lastRule.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
        {
          errorMessages += StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_DEFAULT_RULE_WRONG_JOINT_TYPE_MSG_KEY",
                                                                           new Object[]{lastRule.getJointTypeEnum().getName()}));
          errorMessages += "\n\n";
          errorFound = true;
        }
      }
    }
    if (errorFound || warningFound)
    {
      // give a slightly different message for warnings
      if (warningFound)
        errorMessages = errorMessages.substring(StringLocalizer.keyToString("CFGUI_JTA_FIX_ERRORS_MESSAGE_KEY").length(),
                                                errorMessages.length());
      JOptionPane.showMessageDialog(_mainUI,
                                    errorMessages,
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      if (errorFound)
        return false;
    }
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private void clearUndoStack()
  {
    _commandManager.clear();
    _menuBar.updateUndoRedoMenuItems();
  }

  /**
   * @author Laura Cormos
   */
  private void resetStatusBar()
  {
    _mainUI.setStatusBarText("");
  }

  /**
   * @author Laura Cormos
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof com.axi.v810.gui.undo.CommandManager)
          handleUndoState(object);
        else if (observable instanceof ConfigObservable)
          handleConfigDataUpdate(object);
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void handleUndoState(final Object stateObject)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        UndoState undoState = (UndoState)stateObject;
        int selectedIndex = undoState.getJtaRulesSetComboboxSelectedIndex();
        if (_ruleSetSelectNameCombobox.getSelectedIndex() != selectedIndex)
          _ruleSetSelectNameCombobox.setSelectedIndex(selectedIndex);
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  UndoState getCurrentUndoState()
  {
    UndoState undoState = new UndoState();
    undoState.setJtaRulesSetComboboxSelectedIndex(_ruleSetSelectNameCombobox.getSelectedIndex());

    return undoState;
  }

  /**
   * @author Laura Cormos
   */
  private void handleConfigDataUpdate(final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(object != null);

        if (object instanceof JointTypeEnumRule)
        {
          if (_loadingInProgress == false)
          {
            JointTypeEnumRule jointTypeEnumRule = (JointTypeEnumRule)object;
            if (_rulesTableModel.contains(jointTypeEnumRule)) // this is not a delete rule event
            {
              int rowIndex = _rulesTableModel.getIndexOf(jointTypeEnumRule);
              _rulesTableModel.fireTableRowsUpdated(rowIndex, rowIndex);
              _rulesTable.setRowSelectionInterval(rowIndex, rowIndex);
            }
          }
        }
        else if (object instanceof SoftwareConfigEnum)
        {
          // the only SoftwareConfigEnum in this screen is the joint type assignment rule set to use on import
          // so we're going to update it
          resetRuleSetToUseComboBox();
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void resetRuleSetToUseComboBox()
  {
    String usedRuleSet = _jointTypeAssignment.getJointTypeAssignmentConfigFileNameWithoutExtension();
    _ruleSetToUseComboBox.removeActionListener(_ruleSetToUseComboboxActionListener);
    _ruleSetToUseComboBox.setSelectedItem(usedRuleSet);
    _ruleSetToUseComboBox.addActionListener(_ruleSetToUseComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  boolean ruleSetNameComboBoxContainsName(String ruleSetName)
  {
    Assert.expect(ruleSetName != null);

    for (int i = 0; i < _ruleSetSelectNameCombobox.getItemCount(); i++)
    {
      Object item = _ruleSetSelectNameCombobox.getItemAt(i);
      Assert.expect(item instanceof String);
      String name = (String)item;
      if (name.equalsIgnoreCase(ruleSetName))
        return true;
    }
    return false;
  }
}
