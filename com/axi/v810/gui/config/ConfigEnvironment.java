package com.axi.v810.gui.config;

import java.awt.event.*;
import java.util.regex.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

public class ConfigEnvironment extends AbstractEnvironmentPanel
{
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private TaskPanelScreenEnum _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  private TaskPanelScreenEnum _nextScreen = null;
  private TaskPanelScreenEnum _configLastScreen = null;
  private AbstractTaskPanel _configLastTaskPanel = null;


  private String[] _buttonNames = {StringLocalizer.keyToString("CFGUI_SW_OPTIONS_BUTTON_LABEL_KEY"),
                                  StringLocalizer.keyToString("CFGUI_PROD_OPTIONS_BUTTON_LABEL_KEY"),
                                  StringLocalizer.keyToString("CFGUI_RESULTS_PROCESSING_BUTTON_LABEL_KEY"),
                                  StringLocalizer.keyToString("CFGUI_CUSTOMER_OPTIONS_BUTTON_LABEL_KEY")};

  private final int _softwareOptionsScreen = 0;
  private final int _productionOptionsScreen = 1;
  private final int _resultsProcessingScreen = 2;
  private final int _customerOptionsScreen = 3;

  // Not user visible strings -- used to switch in the CardLayout between different work spaces
  private final static String _SW_OPTIONS_TASK_PANEL = "SW";
  private final static String _PRODUCTION_OPTIONS_TASK_PANEL = "Production";
  private final static String _RESULTS_PROCESSING_TASK_PANEL = "Results";
  private final static String _CUSTOMER_OPTIONS_TASK_PANEL = "Customer";

  private SoftwareOptionsTaskPanel _softwareOptionsTaskPanel = null;
  private ProductionOptionsTaskPanel _productionOptionsTaskPanel = null;
  private ResultsProcessingTaskPanel _resultsProcessingTaskPanel = null;
  private CustomerOptionsTaskPanel _customerOptionsTaskPanel = null;

  private JMenu _configMenu = new JMenu();
  private JMenuItem _saveConfigMenuItem = new JMenuItem();

  private ConfigGuiPersistence _guiPersistSettings;

  // Email pattern derived from the official RFC
  // George Booth found this on the web at http://www.leshazlewood.com/?p=5 in an article:
  // "Java Email Address Validation using Regular Expressions (the Right Way)"
  // RFC 2822 token definitions for valid email - only used together to form a java Pattern object:
  private static final String sp = "\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\_\\`\\{\\|\\}\\~";
  private static final String atext = "[a-zA-Z0-9" + sp + "]";
  private static final String atom = atext + "+"; //one or more atext chars
  private static final String dotAtom = "\\." + atom;
  private static final String localPart = atom + "(" + dotAtom + ")*"; //one atom followed by 0 or more dotAtoms.

  // RFC 1035 tokens for domain names:
  private static final String letter = "[a-zA-Z]";
  private static final String letDig = "[a-zA-Z0-9]";
  private static final String letDigHyp = "[a-zA-Z0-9-]";
  private static final String rfcLabel = letDig + letDigHyp + "{0,61}" + letDig;
  private static final String domain = rfcLabel + "(\\." + rfcLabel + ")*\\." + letter + "{2,6}";

  // Combined together, these form the allowed email regexp allowed by RFC 2822:
  private static final String addrSpec = "^" + localPart + "@" + domain + "$";

  // Now compile them:
  private static final Pattern DOMAIN_VALID_PATTERN = Pattern.compile(domain);
  private static final Pattern EMAIL_VALID_PATTERN = Pattern.compile(addrSpec);

  public ConfigEnvironment(MainMenuGui mainUI)
  {
    super(mainUI);
    _navigationPanel = new ConfigNavigationPanel(_buttonNames, this);
    setNavigationPanel(_navigationPanel);
    restoreUIState();
   try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }


  private void jbInit() throws Exception
  {
    // initialize panels (AbstractEnvrionmentPanel defines _centerPanel with a CardLayout)
    _softwareOptionsTaskPanel = new SoftwareOptionsTaskPanel(this);

    _centerPanel.add(_softwareOptionsTaskPanel, _SW_OPTIONS_TASK_PANEL);
    _mainCardLayout.first(_centerPanel);

    _productionOptionsTaskPanel = new ProductionOptionsTaskPanel(this);
    _centerPanel.add(_productionOptionsTaskPanel, _PRODUCTION_OPTIONS_TASK_PANEL);

    _resultsProcessingTaskPanel = new ResultsProcessingTaskPanel(this);
    _centerPanel.add(_resultsProcessingTaskPanel, _RESULTS_PROCESSING_TASK_PANEL);

    _customerOptionsTaskPanel = new CustomerOptionsTaskPanel(this);
    _centerPanel.add(_customerOptionsTaskPanel, _CUSTOMER_OPTIONS_TASK_PANEL);

    // define custom menus and toolBar items
    // they are not actually until start()
    _configMenu.setText("&Config");
    _configMenu.setName("_configMenu");

    _saveConfigMenuItem.setText("&Save Config");
    _saveConfigMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveConfig(e);
      }
    });
  }

  /**
   * change to a different screen.
   * @author Andy Mechtenberg
   */
  public void switchToScreen(TaskPanelScreenEnum screen)
  {
    Assert.expect(screen != null);
    if (_currentScreen.equals(screen))
      return;
    if(screen.equals(screen.CONFIG_SOFTWARE))
    {
      switchToTask(_centerPanel, _softwareOptionsTaskPanel, _SW_OPTIONS_TASK_PANEL);
      _navigationPanel.highlightButton(_buttonNames[_softwareOptionsScreen]);
    }
    else if(screen.equals(screen.CONFIG_PRODUCTION))
    {
      switchToTask(_centerPanel, _productionOptionsTaskPanel, _PRODUCTION_OPTIONS_TASK_PANEL);
      _navigationPanel.highlightButton(_buttonNames[_productionOptionsScreen]);
    }
    else if(screen.equals(screen.CONFIG_RESULTS))
    {
      switchToTask(_centerPanel, _resultsProcessingTaskPanel, _RESULTS_PROCESSING_TASK_PANEL);
      _navigationPanel.highlightButton(_buttonNames[_resultsProcessingScreen]);
    }
    else if(screen.equals(screen.CONFIG_CUSTOMER))
    {
      switchToTask(_centerPanel, _customerOptionsTaskPanel, _CUSTOMER_OPTIONS_TASK_PANEL);
      _navigationPanel.highlightButton(_buttonNames[_customerOptionsScreen]);
    }
    // XCR-3007 Assert when press undo after add production tuning recipe
    // Cheah Lee Herng 23-Oct-2015    
    _currentScreen = screen;
    _guiObservable.stateChanged(null, _currentScreen);
  }

  /**
   * called when a navigation button is clicked.
   * @param buttonName String
   * @author George Booth
   */
  public void navButtonClicked(String buttonName)
  {
    Assert.expect(buttonName != null);

    if (buttonName.equals(_buttonNames[_softwareOptionsScreen]))
    {
      _nextScreen = TaskPanelScreenEnum.CONFIG_SOFTWARE;
    }
    else if (buttonName.equals(_buttonNames[_productionOptionsScreen]))
    {
      _nextScreen = TaskPanelScreenEnum.CONFIG_PRODUCTION;
    }
    else if (buttonName.equals(_buttonNames[_resultsProcessingScreen]))
    {
      _nextScreen = TaskPanelScreenEnum.CONFIG_RESULTS;
    }
    else if (buttonName.equals(_buttonNames[_customerOptionsScreen]))
    {
      _nextScreen = TaskPanelScreenEnum.CONFIG_CUSTOMER;
    }

    TaskPanelChangeScreenCommand command = new
      TaskPanelChangeScreenCommand(this, _currentScreen, _nextScreen);
    _commandManager.execute(command);
    _currentScreen = _nextScreen;
    _guiObservable.stateChanged(null, _currentScreen);
  }

  /**
   * returns the config menu
   * @return JMenu
   * @author George Booth
   */
  JMenu getConfigMenu()
  {
    return _configMenu;
  }

  /**
   * returns the config tool bar
   * @return the config tool Bar
   * @author George Booth
   */
  MainUIToolBar getConfigToolBar()
  {
    return _envToolBar;
  }

  /**
   * clear config action
   * @param e ActionEvent
   * @author George Booth
   */
  void saveConfig(ActionEvent e)
  {
//    System.out.println("ConfigEnvironment().saveConfig()");
  }

  /**
   * config 1 action
   * @param e ActionEvent
   * @author George Booth
   */
  void configToolBarButton(ActionEvent e)
  {
//    System.out.println("ConfigEnvironment().configToolBarButton()");
  }

  /**
   * The environment is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("Config().start()");

    // use the common status bar
    _mainUI.addStatusBar();
    _mainUI.setStatusBarText("");

//    super.startMenusAndEmptyToolBar();
    super.startMenusAndDisableToolBar();

    // add custom menus
//    MainUIMenuBar menuBar = _mainUI.getMainMenuBar();

//    menuBar.addMenu(_menuRequesterID, 2, _configMenu);

//    menuBar.addMenuItem(_menuRequesterID, _configMenu, 0, _saveConfigMenuItem);

    // turn on the tool bar for this environment
    // addToolBar();

    // revert to SW Options (default task) unless a previous screen was defined
    if (_configLastTaskPanel != null)
    {
      _currentTaskPanel = _configLastTaskPanel;
      _currentScreen = _configLastScreen;
      _guiObservable.stateChanged(null, _currentScreen);
      _configLastTaskPanel.start();
    }
    else
    {
      // start default task
      _navigationPanel.selectDefaultTask();
    }

    restoreUIState();
  }

  /**
   * User has requested an environment change. Is it OK to leave this environment?
   * @return true if environment can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("Config().isReadyToFinish()");
    return currentTaskPanelReadyToFinish();
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    _configLastScreen = _currentScreen;
    _configLastTaskPanel = _currentTaskPanel;
    // finish current task
    finishCurrentTaskPanel();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    // remove status bar
    _mainUI.removeStatusBar();


    saveUIState();

//    System.out.println("Config().finish()\n");
  }

  /**
   * This would write individual screen's persistance data, if any.
   * Called from the Main Menu when exiting.
   * @author Andy Mechtenberg
   */
  public void writePersistance()
  {
    if (_guiPersistSettings != null)
    {
      try
      {
        _guiPersistSettings.writeSettings();
      }
      catch (DatastoreException de)
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      de.getLocalizedMessage(),
                                      StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public ConfigGuiPersistence getPersistance()
  {
    Assert.expect(_guiPersistSettings != null);
    return _guiPersistSettings;
  }

  /**
   * This will read in the persist data
   * @author Andy Mechtenberg
   */
  private ConfigGuiPersistence readPersistance()
  {
    _guiPersistSettings = new ConfigGuiPersistence();
    _guiPersistSettings = (ConfigGuiPersistence)_guiPersistSettings.readSettings();
     return _guiPersistSettings;
  }

  /**
   * @author Laura Cormos
   */
  private void saveUIState()
  {
    if (_guiPersistSettings != null)
      writePersistance();
  }

  /**
   * @author Laura Cormos
   */
  private void restoreUIState()
  {
    if (_guiPersistSettings == null)
      readPersistance();
  }

  /**
   * @author George Booth
   */
  public boolean isEmailAddressValid(String emailAddress)
  {
    String emailString = emailAddress.trim();
    if (emailString.length() > 0 && EMAIL_VALID_PATTERN.matcher(emailString).matches() == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_EMAIL_ADDRESS_INVALID_MESSAGE_KEY",
                                                    new Object[]
                                                    {emailString});

      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      return false;
    }
    return true;
  }

  /**
   * @author George Booth
   */
  public boolean isMailServerValid(String serverName)
  {
    String mailServerString = serverName.trim();
    if (mailServerString.length() > 0 && DOMAIN_VALID_PATTERN.matcher(mailServerString).matches() == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_MAIL_SERVER_ADDRESS_INVALID_MESSAGE_KEY",
                                                    new Object[]
                                                    {mailServerString});

      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      return false;
    }
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  boolean isPathValid(String fullPath)
  {
    if (FileName.isLegalFullPath(fullPath) == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_ILLEGAL_FULLPATH_KEY", new Object[]{fullPath, FileName.getIllegalFullPathChars()});
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(StringLocalizer.keyToString(message), 50),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      return false;
    }
    if (FileUtilAxi.existsDirectoryAndAbleToReadWrite(fullPath))
      return true;
    else if (FileUtilAxi.existsDirectory(fullPath) == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_DIRECTORY_DOES_NOT_EXIST_KEY", new Object[]{fullPath});
      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringLocalizer.keyToString(message),
                                                 StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                                 JOptionPane.YES_NO_OPTION);
      if (answer == JOptionPane.YES_OPTION)
      {
        try
        {
          FileUtilAxi.createDirectory(fullPath);
          return true;
        }
        catch (DatastoreException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex,
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                        true);
          return false;
        }
      }
      else
        return false;
    }
    else
    {
      // directory exists, but not read/writable
      LocalizedString message = new LocalizedString("CFGUI_DIRECTORY_NO_PERMISSIONS_KEY", new Object[]{fullPath});
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      return false;

    }
  }
}


