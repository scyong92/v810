package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
public class JointTypeRulesTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  private SoftwareOptionsJointTypeTabPanel _parent;
  //String labels retrieved from the localization properties file
  private static final String _JOINT_TYPE_KEY_NAME_LABEL = StringLocalizer.keyToString("CFGUI_JTA_JOINT_TYPE_NAME_KEY");
  private static final String _RULE_ENABLED_LABEL = StringLocalizer.keyToString("CFGUI_JTA_RULE_ENABLED_KEY");
  private static final String _REF_DES_LABEL = StringLocalizer.keyToString("CFGUI_JTA_REF_DES_KEY");
  private static final String _NUM_PADS_LABEL = StringLocalizer.keyToString("CFGUI_JTA_NUM_PADS_KEY");
  private static final String _NUM_ROWS_LABEL = StringLocalizer.keyToString("CFGUI_JTA_NUM_ROWS_KEY");
  private static final String _NUM_COLS_LABEL = StringLocalizer.keyToString("CFGUI_JTA_NUM_COLS_KEY");
  private static final String _LP_NAME_LABEL = StringLocalizer.keyToString("CFGUI_JTA_LP_NAME_KEY");
  private static final String _PAD_IN_MIDDLE_ENABLED_LABEL = StringLocalizer.keyToString("CFGUI_JTA_PAD_IN_MIDDLE_KEY");

  private String[] _columnLabels = { _JOINT_TYPE_KEY_NAME_LABEL,
                                   _RULE_ENABLED_LABEL,
                                   _REF_DES_LABEL,
                                   _LP_NAME_LABEL,
                                   _NUM_PADS_LABEL,
                                   _NUM_ROWS_LABEL,
                                   _NUM_COLS_LABEL,
                                   _PAD_IN_MIDDLE_ENABLED_LABEL};

  private List<JointTypeEnumRule> _selectedJointType = new ArrayList<JointTypeEnumRule>();
  private List<JointTypeEnumRule> _jointTypeRules = new ArrayList<JointTypeEnumRule>();
  private String _defaultString = "";

  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  /**
   * Constructor.
   * @author Laura Cormos
   */
  public JointTypeRulesTableModel(SoftwareOptionsJointTypeTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_jointTypeRules != null)
      _jointTypeRules.clear();
    if (_selectedJointType != null)
      _selectedJointType.clear();

    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_jointTypeRules != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    JointTypeEnumRule rowElement = _jointTypeRules.get(rowIndex);

    //get the appropriate data item that corresponds to the columnIndex
    switch(columnIndex)
    {
      case JointTypeRulesTable._JOINT_TYPE_NAME_INDEX:
        value = rowElement.getJointTypeEnum();
        break;
      case JointTypeRulesTable._RULE_ENABLED_INDEX:
        value = rowElement.isEnabled();
        break;
      case JointTypeRulesTable._REF_DES_COLUMN_INDEX:
        if (rowElement.isRefDesPatternSet())
          value = rowElement.getRefDesPattern();
        else
          value = _defaultString;
        break;
      case JointTypeRulesTable._NUM_PADS_COLUMN_INDEX:
        if (rowElement.isNumPadsSet())
          value = rowElement.getNumPadsPattern();
        else
          value = _defaultString;
        break;
      case JointTypeRulesTable._NUM_ROWS_COLUMN_INDEX:
        if (rowElement.isNumRowsSet())
          value = rowElement.getNumRowsPattern();
        else
          value = _defaultString;
        break;
      case JointTypeRulesTable._NUM_COLS_COLUMN_INDEX:
        if (rowElement.isNumColsSet())
          value = rowElement.getNumColsPattern();
        else
          value = _defaultString;
        break;
      case JointTypeRulesTable._LP_NAME_COLUMN_INDEX:
        if (rowElement.isLandPatternNamePatternSet())
          value = rowElement.getLandPatternNamePattern();
        else
          value = _defaultString;
        break;
      case JointTypeRulesTable._PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX:
        if (rowElement.isPadOneInCenterOfRowSet())
          value = rowElement.isPadOneInCenterOfRow();
        else
          value = false;
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    Assert.expect(_jointTypeRules != null);

    return _jointTypeRules.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_selectedJointType == null || (_selectedJointType.contains(_jointTypeRules.get(rowIndex)) == false))
      return false;

    return true;
  }

  /**
   * Initialize the table with data from the current joint type assignment config file
   * @author Laura Cormos
   */
  void setData(List<JointTypeEnumRule> rulesList)
  {
    Assert.expect(rulesList != null);

    _jointTypeRules = rulesList;
    fireTableDataChanged();
  }

  /**
    * Initialize the table with data from the current joint type assignment config file
    * @author Laura Cormos
    */
   List<JointTypeEnumRule> getData()
   {
     Assert.expect(_jointTypeRules != null);

     return _jointTypeRules;
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    JointTypeEnumRule rowElement = _jointTypeRules.get(rowIndex);

    switch (columnIndex)
    {
      case JointTypeRulesTable._JOINT_TYPE_NAME_INDEX:
        Assert.expect(object instanceof JointTypeEnum);
        JointTypeEnum jointTypeEnum = (JointTypeEnum)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ConfigSetJointTypeEnumCommand(rowElement, jointTypeEnum));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case JointTypeRulesTable._RULE_ENABLED_INDEX:
        Assert.expect(object instanceof Boolean);
        Boolean enabled = (Boolean)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ConfigSetJointTypeRuleEnabledCommand(rowElement, enabled));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case JointTypeRulesTable._REF_DES_COLUMN_INDEX:
        Assert.expect(object instanceof String);

        String pattern = (String)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ConfigSetJointTypeRuleRefDesPatternCommand(rowElement, pattern));
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case JointTypeRulesTable._NUM_PADS_COLUMN_INDEX:
        Assert.expect(object instanceof String);
        pattern = (String)object;
        if (rowElement.isNumPatternValid(pattern))
        {
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new ConfigSetJointTypeRuleNumPadsPatternCommand(rowElement, pattern));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
            return;
          }
        }
        else
          showInvalidNumberPatternErrorDialog(pattern);
        break;
      case JointTypeRulesTable._NUM_ROWS_COLUMN_INDEX:
        Assert.expect(object instanceof String);
        pattern = (String)object;
        if (rowElement.isNumPatternValid(pattern))
        {
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new ConfigSetJointTypeRuleNumRowsPatternCommand(rowElement, pattern));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
            return;
          }
        }
        else
          showInvalidNumberPatternErrorDialog(pattern);
        break;
      case JointTypeRulesTable._NUM_COLS_COLUMN_INDEX:
        Assert.expect(object instanceof String);
        pattern = (String)object;
        if (rowElement.isNumPatternValid(pattern))
        {
          try
          {
            _commandManager.trackState(_parent.getCurrentUndoState());
            _commandManager.execute(new ConfigSetJointTypeRuleNumColsPatternCommand(rowElement, pattern));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
            return;
          }
        }

        else
          showInvalidNumberPatternErrorDialog(pattern);
        break;
      case JointTypeRulesTable._LP_NAME_COLUMN_INDEX:
        Assert.expect(object instanceof String);
        pattern = (String)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ConfigSetJointTypeRuleLandPatternNamePatternCommand(rowElement, pattern));
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                        JOptionPane.ERROR_MESSAGE);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case JointTypeRulesTable._PAD1_NOT_AT_END_OF_ROW_ENABLED_COLUMN_INDEX:
        Assert.expect(object instanceof Boolean);
        enabled = (Boolean)object;
        try
        {
          _commandManager.trackState(_parent.getCurrentUndoState());
          _commandManager.execute(new ConfigSetJointTypeRulePadOneEnabledCommand(rowElement, enabled));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      default:
        Assert.expect(false);
    }
    _parent.setRuleSetIsDirty();
  }

   /**
    * @author Laura Cormos
    */
   public void addNewRule(JointTypeEnumRule newRule)
   {
     _jointTypeRules.add(newRule);

     fireTableDataChanged();
   }

   /**
    * @author Laura Cormos
    */
   public void setSelectedRow(int selectedRowIndex)
   {
     Assert.expect(selectedRowIndex >= 0);
     Assert.expect(_selectedJointType != null);

     _selectedJointType.clear();
     _selectedJointType.add(_jointTypeRules.get(selectedRowIndex));
   }

   /**
    * @author Laura Cormos
    */
   public void deleteRuleAt(int rowIndex)
   {
     Assert.expect(rowIndex >= 0);
     Assert.expect(_jointTypeRules != null);

     _jointTypeRules.remove(rowIndex);
     fireTableRowsDeleted(rowIndex, rowIndex);
   }

   /**
    * @author Laura Cormos
    */
   public void deleteRule(JointTypeEnumRule rule)
   {
     Assert.expect(rule != null);
     Assert.expect(_jointTypeRules != null);

     int rowIndex = _jointTypeRules.indexOf(rule);
     // expect the given rule to exist in the model already
     Assert.expect(_jointTypeRules.remove(rule) == true);
     fireTableRowsDeleted(rowIndex, rowIndex);
   }

   /**
    * @author Laura Cormos
    */
   public JointTypeEnumRule getRuleAt(int rowIndex)
   {
     Assert.expect(rowIndex >= 0);
     Assert.expect(_jointTypeRules != null);

     return _jointTypeRules.get(rowIndex);
   }

   /**
    * @author Laura Cormos
    */
   public void moveRowUp(int rowIndex)
   {
     Assert.expect(rowIndex > 0);
     Assert.expect(_jointTypeRules != null);
     Assert.expect(_jointTypeRules.size() > rowIndex);

     JointTypeEnumRule rule = _jointTypeRules.get(rowIndex);
     _jointTypeRules.remove(rowIndex);
     _jointTypeRules.add(rowIndex - 1, rule);
     _parent.resetRuleSet(_jointTypeRules);

     fireTableRowsUpdated(rowIndex - 1, rowIndex);
   }


   /**
    * @author Laura Cormos
    */
   public void moveRowDown(int rowIndex)
   {
     Assert.expect(rowIndex >= 0);
     Assert.expect(_jointTypeRules != null);
     Assert.expect(_jointTypeRules.size() > rowIndex);

     JointTypeEnumRule rule = _jointTypeRules.get(rowIndex);
     _jointTypeRules.remove(rowIndex);
     _jointTypeRules.add(rowIndex + 1, rule);
     _parent.resetRuleSet(_jointTypeRules);

     fireTableRowsUpdated(rowIndex, rowIndex + 1);
   }

   /**
    * @author Laura Cormos
    */
   private void showInvalidNumberPatternErrorDialog(String pattern)
   {
     Assert.expect(pattern != null);

     JOptionPane.showMessageDialog(_mainUI,
                                   StringLocalizer.keyToString("CFGUI_JTA_INVALID_NUMBER_PATTERN_KEY"),
                                   StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                   JOptionPane.ERROR_MESSAGE);
   }

   /**
    * @author Laura Cormos
    */
   int getIndexOf(JointTypeEnumRule jointTypeEnumRule)
   {
     Assert.expect(jointTypeEnumRule != null);
     Assert.expect(_jointTypeRules != null);
     Assert.expect(_jointTypeRules.contains(jointTypeEnumRule));

     return _jointTypeRules.indexOf(jointTypeEnumRule);
   }

   /**
    * @author Laura Cormos
    */
   boolean contains(JointTypeEnumRule jointTypeEnumRule)
   {
     Assert.expect(jointTypeEnumRule != null);
     Assert.expect(_jointTypeRules != null);

     return _jointTypeRules.contains(jointTypeEnumRule);
   }

   /**
    * @author Laura Cormos
    */
   private void printBadNumberErrorDialog(LocalizedString message)
   {
     Assert.expect(message != null);

     // this string does not represent a proper number
     String errorMessage = StringLocalizer.keyToString(message) +
                           StringLocalizer.keyToString(new LocalizedString("CFGUI_JTA_ENTER_VALID_INTEGER_ERROR_MSG_KEY",
                                                                           new Object[]{Integer.MAX_VALUE}));
     MessageDialog.showErrorDialog(_mainUI, errorMessage,
                                   StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
   }
 }
