package com.axi.v810.gui.config;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 * @version 1.0
 */
public class ProductionTuneTableModel extends DefaultSortTableModel
{
  private MainMenuGui _mainUI;
  private ConfigEnvironment _configEnv;
  //String labels retrieved from the localization properties file
  private static final String _PROJECT_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_PROJECT_KEY");
  private static final String _IMAGE_DESTINATION_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_IMAGE_DEST_KEY");
  private static final String _RESULTS_DESTINATION_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_RESULTS_DEST_KEY");
  private static final String _INITIAL_COUNT_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_COUNT_KEY");
  private static final String _RE_ENABLE_DEFECT_THRESHOLD_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_RE_ENABLE_THR_KEY");
  private static final String _RE_ENABLE_COUNT_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_RE_ENABLE_COUNT_KEY");
  private static final String _NOTIFICATION_EMAIL_LABEL = StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TABLE_NOTIFY_EMAIL_KEY");


  private String[] _columnLabels = { _PROJECT_LABEL,
                                     _IMAGE_DESTINATION_LABEL,
                                     _RESULTS_DESTINATION_LABEL,
                                     _INITIAL_COUNT_LABEL,
                                     _RE_ENABLE_DEFECT_THRESHOLD_LABEL,
                                     _RE_ENABLE_COUNT_LABEL,
                                     _NOTIFICATION_EMAIL_LABEL};

  private List<ProductionTuneData> _selectedProductionTuneData = new ArrayList<ProductionTuneData>();
  private List<ProductionTuneData> _productionTuneData = new ArrayList<ProductionTuneData>();

  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  private CustomerInfo _customerInfo = CustomerInfo.getInstance();

  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * Constructor.
   * @author Andy Mechtenberg
   */
  public ProductionTuneTableModel(ConfigEnvironment parent)
  {
    Assert.expect(parent != null);

    _configEnv = parent;
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  void clearData()
  {
    if (_productionTuneData != null)
      _productionTuneData.clear();
    if (_selectedProductionTuneData != null)
      _selectedProductionTuneData.clear();

    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Andy Mechtenberg
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Andy Mechtenberg
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Andy Mechtenberg
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Andy Mechtenberg
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_productionTuneData != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    ProductionTuneData rowElement = _productionTuneData.get(rowIndex);

    //get the appropriate data item that corresponds to the columnIndex
    switch(columnIndex)
    {
      case ProductionTuneTable._PROJECT_NAME_INDEX:
        value = rowElement.getProjectName();
        break;
      case ProductionTuneTable._IMAGE_DESTINATION_INDEX:
        value = rowElement.getImageDestination();
        break;
      case ProductionTuneTable._RESULTS_DESTINATION_INDEX:
        value = rowElement.getResultsDestination();
        break;
      case ProductionTuneTable._CURRENT_COUNT_INDEX:
        value = rowElement.getCurrentCount();
        break;
      case ProductionTuneTable._RE_ENABLE_DEFECT_THRESHOLD_INDEX:
        value = rowElement.getReEnableThreshold();
        break;
      case ProductionTuneTable._RE_ENABLE_COUNT_INDEX:
        value = rowElement.getReEnableCount();
        break;
      case ProductionTuneTable._NOTIFICATION_EMAIL_INDEX:
        value = rowElement.getNotificationEmailAddress();
        break;
      default:
      {
        Assert.expect(false);
      }
    }
    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if(_productionTuneData == null)
      return 0;
    else
      return _productionTuneData.size();
  }

  /**
   * Overridden method.
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if (_selectedProductionTuneData == null || (_selectedProductionTuneData.contains(_productionTuneData.get(rowIndex)) == false))
      return false;

    return true;
  }

  /**
   * Initialize the table with data from the current production tune config file
   * @author Andy Mechtenberg
   */
  void setData(Collection<ProductionTuneData> rulesList)
  {
    Assert.expect(rulesList != null);

    _productionTuneData = new ArrayList<ProductionTuneData>(rulesList);
    if (_productionTuneData.isEmpty())
      _selectedProductionTuneData.clear();
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
    * Get all the current production tune data
    * @author Andy Mechtenberg
    */
   List<ProductionTuneData> getData()
   {
     Assert.expect(_productionTuneData != null);

     return _productionTuneData;
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    if (object == null)  // nothing to do if null -- this may happen if editing is cancelled.
      return;
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    ProductionTuneData rowElement = _productionTuneData.get(rowIndex);

    switch (columnIndex)
    {
      case ProductionTuneTable._PROJECT_NAME_INDEX:
        Assert.expect(object instanceof String);
        String projectName = (String)object;

        if (containsProject(projectName))
        {
          // give error -- same project not allowed twice
          LocalizedString errorMessage = new LocalizedString("CFGUI_RESPROC_PRODUCTION_TUNE_PROJECT_ALREADY_EXISTS_KEY",
                                                             new Object[]
                                                             {projectName});
          MessageDialog.showErrorDialog(_mainUI,
                                        StringLocalizer.keyToString(errorMessage),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);

        }
        else
        {
          try
          {
            _commandManager.execute(new ConfigSetProductionTuneProjectNameCommand(rowElement, projectName));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
            return;
          }
        }
        break;
      case ProductionTuneTable._IMAGE_DESTINATION_INDEX:
        Assert.expect(object instanceof String);
        String imageDestination = (String)object;
        try
        {
          _commandManager.execute(new ConfigSetProductionTuneImageDestinationCommand(rowElement, imageDestination));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case ProductionTuneTable._RESULTS_DESTINATION_INDEX:
        Assert.expect(object instanceof String);
        String resultsDestination = (String)object;
        try
        {
          _commandManager.execute(new ConfigSetProductionTuneResultsDestinationCommand(rowElement, resultsDestination));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case ProductionTuneTable._CURRENT_COUNT_INDEX:
        Assert.expect(object instanceof Long);
        Long currentCount = (Long)object;
        try
        {
          _commandManager.execute(new ConfigSetProductionTuneCurrentCountCommand(rowElement, currentCount.intValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case ProductionTuneTable._RE_ENABLE_DEFECT_THRESHOLD_INDEX:
        Assert.expect(object instanceof Long);
        Long reEnableThreshold = (Long)object;
        try
        {
          _commandManager.execute(new ConfigSetProductionTuneReEnableThresholdCommand(rowElement, reEnableThreshold.intValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case ProductionTuneTable._RE_ENABLE_COUNT_INDEX:
        Assert.expect(object instanceof Long);
        Long reEnableCount = (Long)object;
        try
        {
          _commandManager.execute(new ConfigSetProductionTuneReEnableCountCommand(rowElement, reEnableCount.intValue()));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }
        break;
      case ProductionTuneTable._NOTIFICATION_EMAIL_INDEX:
        Assert.expect(object instanceof String);
        String notificationEmail = (String)object;

        // check for valid email addresses
        // convert ";" to spaces, then split
        notificationEmail = notificationEmail.replace(';', ' ');
        notificationEmail = notificationEmail.replace(',', ' ');
        String[] addresses = notificationEmail.split(" ");
        for (int i = 0; i < addresses.length; i++)
        {
          if (_configEnv.isEmailAddressValid(addresses[i]) == false)
            return;
        }

        // have a valid email address -- make sure the SMTP server is set, otherwise issue a WARNING, but let the setting stay
        warnIfMainServerNotSet();

        try
        {
          _commandManager.execute(new ConfigSetProductionTuneNotifyEmailCommand(rowElement, notificationEmail));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          return;
        }

        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
    if (column == ProductionTuneTable._PROJECT_NAME_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.PROJECT_NAME));
    else if (column == ProductionTuneTable._IMAGE_DESTINATION_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.IMAGE_DESTINATION));
    else if (column == ProductionTuneTable._RESULTS_DESTINATION_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.RESULTS_DESTINATION));
    else if (column == ProductionTuneTable._CURRENT_COUNT_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.CURRENT_COUNT));
    else if (column == ProductionTuneTable._RE_ENABLE_DEFECT_THRESHOLD_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.RE_ENABLE_THRESHOLD));
    else if (column == ProductionTuneTable._RE_ENABLE_COUNT_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.RE_ENABLE_COUNT));
    else if (column == ProductionTuneTable._NOTIFICATION_EMAIL_INDEX)
      Collections.sort(_productionTuneData, new ProductionTuneDataComparator(ascending, ProductionTuneDataComparatorEnum.NOTIFICATION_EMAIL));

    _currentSortColumn = column;
    _currentSortAscending = ascending;
  }


   /**
    * @author Andy Mechtenberg
    */
   int getIndexOf(ProductionTuneData productionTuneData)
   {
     Assert.expect(productionTuneData != null);
     Assert.expect(_productionTuneData != null);
     //Assert.expect(_productionTuneData.contains(productionTuneData));
     
     int index = -1;
     for(ProductionTuneData currentProductionTuneData : _productionTuneData)
     {
       if (currentProductionTuneData == productionTuneData)
       {
         index = _productionTuneData.indexOf(productionTuneData);
         break;
       }
     }

     return index;
   }

   /**
    * @author Andy Mechtenberg
    */
   boolean contains(ProductionTuneData productionTuneData)
   {
     Assert.expect(productionTuneData != null);
     Assert.expect(_productionTuneData != null);

     return _productionTuneData.contains(productionTuneData);
   }

   /**
    * @author Andy Mechtenberg
    */
   boolean containsProject(String projectName)
   {
     for(ProductionTuneData dataElement : _productionTuneData)
     {
       String dataProjectName = dataElement.getProjectName();

       if ((dataProjectName.length() > 0) && dataProjectName.equalsIgnoreCase(projectName))
         return true;
     }
     return false;
   }

   /**
    * @author Andy Mechtenberg
    */
   int getRowForProject(String projectName)
   {
     for(ProductionTuneData dataElement : _productionTuneData)
     {
       String dataProjectName = dataElement.getProjectName();

       if ((dataProjectName.length() > 0) && dataProjectName.equalsIgnoreCase(projectName))
         return _productionTuneData.indexOf(dataElement);
     }
     Assert.expect(false, "Unable to find project " + projectName);
     return -1;
   }

   /**
    * @author Andy Mechtenberg
    */
   void addNewRow(String projectName, String defaultLocation)
   {
     Assert.expect(projectName != null);

     ProductionTuneData newElement = null;
     newElement = new ProductionTuneData(projectName, defaultLocation + "/projects/" + projectName, defaultLocation + "/results", 50, 500, 10, "someGuy@domain.net");
     _productionTuneData.add(newElement);

     fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
   }

   /**
    * @author Andy Mechtenberg
    */
   void setSelectedRow(int selectedRowIndex)
   {
     Assert.expect(selectedRowIndex >= 0);
     Assert.expect(_productionTuneData != null);

     _selectedProductionTuneData.clear();
     _selectedProductionTuneData.add(_productionTuneData.get(selectedRowIndex));
   }

   /**
    * @author Andy Mechtenberg
    */
   boolean isSelectedRow()
   {
     if (_selectedProductionTuneData.isEmpty())
       return false;
     else
       return true;
   }

   /**
    * @author Andy Mechtenberg
    */
   ProductionTuneData getSelectedRowProductionTuneData()
   {
     if (_selectedProductionTuneData.isEmpty() == false)
     {
       return _selectedProductionTuneData.get(0);
     }
     else
     {
       Assert.expect(false, "Nothing selected");
       return null;
     }
   }

   /**
    * @author Andy Mechtenberg
    */
   private void warnIfMainServerNotSet()
   {
     if (_customerInfo.getMailServer().length() == 0)
     {
       MessageDialog.showWarningDialog(MainMenuGui.getInstance(),
                                       StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_EMAIL_SERVER_MISSING_KEY"),
                                       StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                       true);

     }
   }
 }
