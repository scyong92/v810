package com.axi.v810.gui.config;

import java.util.*;
import java.io.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.ThresholdTableModel;
import com.axi.v810.gui.testDev.UndoState;
import com.axi.v810.gui.undo.ConfigSetInitialThresholdsValueCommand;
import com.axi.v810.util.*;

/**
 * The model for all threshold tables for Initial Thresholds feature
 * @author Andy Mechtenberg
 */
public class InitialThresholdsTableModel extends ThresholdTableModel //AbstractTableModel
{
  private SoftwareOptionsInitialThresholdsTabPanel _parent = null;  
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();  
  private InitialThresholds _initialThresholds = InitialThresholds.getInstance();
  private JointTypeEnum _jointTypeEnum;
  private Algorithm _algorithm;  // this is the algorithm object for this table, which is typically only one expect for the Slice Setup table
  private Map<AlgorithmSetting, Algorithm> _algorithmSettingMap = new HashMap<AlgorithmSetting, Algorithm>(); // for Slice Setup ONLY -- stores the algorithm used for each setting
  private List<List<Object>> _displayData = null; // a list of lists
  private boolean _sliceHeightThresholds = false;  // tells us whether to use the _algorithm or use the map.

  private boolean _testRunning = false;
  private boolean _setValueValid = true;
  
  private String _standardLabel = StringLocalizer.keyToString("ATGUI_BASIC_THRESHOLDS_KEY");
  private String _additionalLabel = StringLocalizer.keyToString("ATGUI_ADDITIONAL_THRESHOLDS_KEY");

  /**
   * @author Andy Mechtenberg
   */
  public InitialThresholdsTableModel(SoftwareOptionsInitialThresholdsTabPanel parent,
                                     JointTypeEnum jointTypeEnum,
                                     Algorithm algorithm,
                                     boolean displaySliceThresholdsOnly)
  {
    super();
    Assert.expect(parent != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    
    _parent = parent;
    _jointTypeEnum = jointTypeEnum;
    _algorithm = algorithm;
    _sliceHeightThresholds = displaySliceThresholdsOnly;
    _displayData = new LinkedList<List<Object>>();

    List<AlgorithmSetting> algorithmSettings = algorithm.getAlgorithmSettings(jointTypeEnum);
    // now, remove settings that are HIDDEN or don't match the incoming "standard" parameter
    Iterator<AlgorithmSetting> it = algorithmSettings.iterator();
    while(it.hasNext())
    {
      AlgorithmSetting algoSetting = it.next();
      AlgorithmSettingTypeEnum typeEnum = algoSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion());
      if (typeEnum.equals(AlgorithmSettingTypeEnum.HIDDEN))
        it.remove();      
    }

    Collections.sort(algorithmSettings, new AlgorithmSettingDisplayOrderComparator());
    List<AlgorithmSetting> standardAlgorithmSettings = new ArrayList<AlgorithmSetting>();
    List<AlgorithmSetting> additionalAlgorithmSettings = new ArrayList<AlgorithmSetting>();
    
    if (displaySliceThresholdsOnly)  // look only for SLICE_HEIGHT and SLICE_HEIGHT_ADDITIONAL
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);
      java.util.List<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      Collections.sort(algorithmEnums, new AlgorithmEnumInspectionOrderComparator());
      for (AlgorithmEnum algoEnum : algorithmEnums)
      {
        Algorithm algo = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), algoEnum);
        List<AlgorithmSetting> algoSettings = algo.getAlgorithmSettings(jointTypeEnum);
        Collections.sort(algoSettings, new AlgorithmSettingDisplayOrderComparator());        
        for (AlgorithmSetting algorithmSetting : algoSettings)
        {
          if (algorithmSetting.getAlgorithmSettingTypeEnum(algo.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT))
          {
            standardAlgorithmSettings.add(algorithmSetting);
            _algorithmSettingMap.put(algorithmSetting, algo);
          }
          else if (algorithmSetting.getAlgorithmSettingTypeEnum(algo.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
          {
            additionalAlgorithmSettings.add(algorithmSetting);
            _algorithmSettingMap.put(algorithmSetting, algo);
          }
        }
      }
    }
    else  // look only for STANDARD and ADDITIONAL thresholds
    {
      for (AlgorithmSetting algorithmSetting : algorithmSettings)
      {
        if (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.STANDARD))
          standardAlgorithmSettings.add(algorithmSetting);
        else if (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.ADDITIONAL))
          additionalAlgorithmSettings.add(algorithmSetting);
      }
    }
    
    List<Object> threshList = new LinkedList<Object>();
      threshList.add(_standardLabel);
      threshList.add("");
      threshList.add("");
      threshList.add("");
      threshList.add("");
      _displayData.add(threshList);
    for (AlgorithmSetting algorithmSetting : standardAlgorithmSettings)
    {
      addAlgorithmSetting(algorithmSetting);
    }
    if (additionalAlgorithmSettings.isEmpty() == false)
    {
      threshList = new LinkedList<Object>();
      threshList.add(_additionalLabel);
      threshList.add("");
      threshList.add("");
      threshList.add("");
      threshList.add("");
      _displayData.add(threshList);
    }
    for (AlgorithmSetting algorithmSetting : additionalAlgorithmSettings)
    {
      addAlgorithmSetting(algorithmSetting);
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void addAlgorithmSetting(AlgorithmSetting algorithmSetting)
  {
    List<Object> threshList = new LinkedList<Object>();
//      Serializable value = algorithmSetting.getDefaultValue(algorithm.getVersion());
//      Serializable value = _subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    Algorithm algorithm = _algorithm;
    if (_sliceHeightThresholds)  // only for slice setup tab do need to find the real algorithm
      algorithm = _algorithmSettingMap.get(algorithmSetting);
    Serializable value = _initialThresholds.getAlgorithmSettingValue(_jointTypeEnum, algorithm, algorithmSetting);
    if (value instanceof Boolean)
    {
      Boolean bool = (Boolean) value;
      if (bool.booleanValue())
      {
        value = new String("Yes");
      }
      else
      {
        value = new String("No");
      }
    }

      Object defaultValue = algorithmSetting.getDefaultValue(algorithm.getVersion());
      if (defaultValue instanceof Boolean)
      {
        Boolean bool = (Boolean)defaultValue;
        if (bool.booleanValue())
          defaultValue = new String("Yes");
        else
          defaultValue = new String("No");
      }

      Object minValue = new String("");
      Object maxValue = new String("");
      if ((value instanceof Integer) || (value instanceof Float))
      {
        minValue = algorithmSetting.getMinimumValue(algorithm.getVersion());
        maxValue = algorithmSetting.getMaximumValue(algorithm.getVersion());
      }
      threshList.add(algorithmSetting);
      threshList.add(value);
      threshList.add(defaultValue);
      threshList.add(minValue);
      threshList.add(maxValue);
      _displayData.add(threshList);
  }

  /**
   * This constructor is to be used specifically for the slice height algorithm settings ONLY!!
   * @author Laura Cormos
   */
//  public ThresholdTableModel(JFrame frame,
//                             AlgoTunerGui testDev,
//                             PanelDataAdapter panelDataAdapter,
//                             Subtype subtype)
//  {
//    Assert.expect(frame != null);
//    Assert.expect(testDev != null);
//    Assert.expect(panelDataAdapter != null);
//    _panelDataAdapter = panelDataAdapter;
//    _parent = frame;
//    _algoTunerGui = testDev;
//    _subtype = subtype;
//    _inspectionFamilyEnum = _subtype.getInspectionFamilyEnum();
//    _displayData = new LinkedList<List<Object>>();
//
//    List<AlgorithmSetting> algorithmSettings = _panelDataAdapter.getSliceSetupThresholds(_subtype,
//                                                                                         _inspectionFamilyEnum);
//    Collections.sort(algorithmSettings, new AlgorithmSettingDisplayOrderComparator());
//
//    for (AlgorithmSetting algorithmSetting : algorithmSettings)
//    {
//      List<Object> threshList = new LinkedList<Object>();
//      Serializable value = _subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
//      if (value instanceof Boolean)
//      {
//        Boolean bool = (Boolean)value;
//        if (bool.booleanValue())
//          value = new String("Yes");
//        else
//          value = new String("No");
//      }
//
//      Object defaultValue = _subtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
//      if (defaultValue instanceof Boolean)
//      {
//        Boolean bool = (Boolean)defaultValue;
//        if (bool.booleanValue())
//          defaultValue = new String("Yes");
//        else
//          defaultValue = new String("No");
//      }
//
//      Object minValue = new String("");
//      Object maxValue = new String("");
//      if ((value instanceof Integer) || (value instanceof Float))
//      {
//        minValue = _subtype.getAlgorithmSettingMinimumValue(algorithmSetting.getAlgorithmSettingEnum());
//        maxValue = _subtype.getAlgorithmSettingMaximumValue(algorithmSetting.getAlgorithmSettingEnum());
//      }
//      threshList.add(algorithmSetting);
//      threshList.add(value);
//      threshList.add(defaultValue);
//      threshList.add(minValue);
//      threshList.add(maxValue);
//      _displayData.add(threshList);
//    }
//  }

  /**
  * Get the current column count -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The integer specifying the column count.
  */
  // from AbstractTableModel
  public int getColumnCount()
  {
    return _thresholdColumns.length;
  }

  /**
  * Get the current column name -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The string specifying the column header name
  * @param col Column number
  */
  // from AbstractTableModel
  public String getColumnName(int col)
  {
    Assert.expect(col >= 0 && col < getColumnCount());
    return (String)_thresholdColumns[col];
  }

  /**
  * Get the value at row/column -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The object at the specified row/column
  * @param row Row number
  * @param col Column number
  */
  // from AbstractTableModel
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    if ((rowData.get(0) == _standardLabel) || (rowData.get(0) == _additionalLabel))
    {
      if (col == NAME_COLUMN)
        return rowData.get(0);
      else
        return "";
    }
    
    if (col > NAME_COLUMN)
    {
      // check to see if we should be returning metric or imperial units.
      // we will convert to Mils if the units are millimeters
      AlgorithmSetting algorithmSetting = (AlgorithmSetting) rowData.get(NAME_COLUMN);
      Algorithm algorithm = _algorithm;
      if (_sliceHeightThresholds)
        algorithm = _algorithmSettingMap.get(algorithmSetting);
      // update the value here to keep in sync with real data
      Serializable value = _initialThresholds.getAlgorithmSettingValue(_jointTypeEnum, algorithm, algorithmSetting);
      if (col == VALUE_COLUMN)
      {
        data = value;
        rowData.set(col, data);                
      }
      
      MeasurementUnitsEnum thresholdUnits = algorithmSetting.getUnits(_algorithm.getVersion());      
      MathUtilEnum tableUnits = _initialThresholds.getCurrentUnits();
      
      data = AlgorithmSettingUtil.getAlgorithmSettingDisplayValue(data, thresholdUnits, tableUnits);
      
    }

    return data;
  }

  /**
   * OK -- this is a problem.  Over in AlgoTunerGUI, I have a focus listener on the cell editor for the table.  If
   * that editor loses focus, it calls the stopCellEditing function.  (I need to do that so accept editor values
   * if another GUI button is clicked)  That function will eventually trickle down
   * to this setValueAt function.  If the value is out of range, then the user will see 2 JOptionPanes.  Why?
   * Consider this scenario:
   * The user types in an out-of-range value, and hits enter.
   *    This function (setValueAt) is called, and then the JOptionPane pops up.
   *    The very fact that a JOptionPane pops up causes the cell to lose focus, and then it wants to stopCellEditing
   *    That call to stop cell editing will also pop up a JOptionPane, and the user sees 2.
   *
   * Possible solutions -- throw an exception.  Problem is, who would catch it?  This is all part of the
   * swing architecture, and it wont' know about a ValueOutOfRange exception
   * A kludgey solution is to use a flag variable that makes sure only one of these is called per value/row/column
   * The kludgey solution is currently a GO.
   * @author Andy Mechtenberg
   */
  public void setValueAt(Object value, int row, int col)
  {
    if (!_setValueValid) return;  // exit if it's not valid
    _setValueValid = false;
    /** @todo Replace this will an Assert when datastore is more complete  */
    if (value == null)
      return;

    // Check to see if the incoming new value is the same as the old one.  If so, do no more work
    if (value.equals(getValueAt(row,col)))
    {
      //System.out.println("New value " + value + " old value " + getValueAt(row,col));
      _setValueValid = true;
      return;
    }

    // A bug can arise where the value type does not match the cell's value type.  ComboBoxes cause this, but I don't
    // know why.  This check should fix it.
    if (value.getClass() != getValueAt(row, col).getClass())
    {
      //System.out.println("New value class" + value.getClass() + " old value " + getValueAt(row,col).getClass());
      _setValueValid = true;
      return;
    }

    List<Object> rowData = _displayData.get(row);
    AlgorithmSetting algorithmSetting = (AlgorithmSetting)rowData.get(NAME_COLUMN);
    Algorithm algorithm = _algorithm;
    if (_sliceHeightThresholds)
      algorithm = _algorithmSettingMap.get(algorithmSetting);
    String valStr = value.toString();

    if (value instanceof Integer)
    {
      try
      {
        Integer integerVal = new Integer(valStr);
        // check to see if it's in the right range (between min and max, inclusive)
        
        Integer min = (Integer)algorithmSetting.getMinimumValue(algorithm.getVersion());
        Integer max = (Integer)algorithmSetting.getMaximumValue(algorithm.getVersion());

        if ( (integerVal.intValue() <= max.intValue()) && (integerVal.intValue() >= min.intValue()))
        {        
          ConfigSetInitialThresholdsValueCommand setCommand = new ConfigSetInitialThresholdsValueCommand(_jointTypeEnum, algorithm, algorithmSetting, integerVal);
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, algorithm, algorithmSetting);
            _commandManager.trackState(undoState);
            _commandManager.execute(setCommand);
//            _initialThresholds.setAlgorithmSettingValue(_jointTypeEnum, _algorithm, algorithmSetting, floatVal);
            rowData.set(col, integerVal);
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }                    
        }
        else
        {
          // Here I want to display a error dialog.  For some reason, even though this is on the AWT-Event queue
          // if this method is called when the user types in a too large value, then clicks on some other window (so focus
          // is lost), this will hang unless I call invokeLater.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(_parent, StringLocalizer.keyToString("ATGUI_THRESHOLD_OUT_OF_RANGE_KEY"),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"), JOptionPane.ERROR_MESSAGE);
            }
          });
        }
      }
      catch (NumberFormatException nfe)
      {
        // shouldn't ever happen because the IntegerNumberField class will make sure only integers get put here
        Assert.expect(false);
      }
    }
    else if (value instanceof Float)
    {
      try
      {
        Float floatVal = new Float(valStr);

        // @author Patrick Lacz
        // we handle algorithm settings using millimeters in a special way - the project's display units may convert entry/display to mils.        
        MeasurementUnitsEnum thresholdUnits = algorithmSetting.getUnits(algorithm.getVersion());      
        MathUtilEnum tableUnits = _initialThresholds.getCurrentUnits();
                
        if (thresholdUnits.equals(MeasurementUnitsEnum.MILLIMETERS) &&
            tableUnits.isMetric() == false)
        {
          // convert from mils (mils is what the user has input)
          floatVal = MathUtil.convertMilsToMillimeters(floatVal.floatValue());
        }
        else if (thresholdUnits.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
            tableUnits.isMetric() == false)
        {
          // convert from sq mils (sq mils is what the user has input)
          floatVal = MathUtil.convertSquareMilsToSquareMillimeters(floatVal.floatValue());
        }
        else if (thresholdUnits.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS) &&
            tableUnits.isMetric() == false)
        {
          // convert from cu mils (cu mils is what the user has input)
          floatVal = MathUtil.convertCubicMilsToCubicMillimeters(floatVal.floatValue());
        }
        // check to see if it's in the right range (between min and max, inclusive)        
        Float min = (Float)algorithmSetting.getMinimumValue(algorithm.getVersion());
        Float max = (Float)algorithmSetting.getMaximumValue(algorithm.getVersion());
        if ( (floatVal.floatValue() <= max.floatValue()) && (floatVal.floatValue() >= min.floatValue()))
        {          
          ConfigSetInitialThresholdsValueCommand setCommand = new ConfigSetInitialThresholdsValueCommand(_jointTypeEnum, algorithm, algorithmSetting, floatVal);
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, algorithm, algorithmSetting);
            _commandManager.trackState(undoState);
            _commandManager.execute(setCommand);
//            _initialThresholds.setAlgorithmSettingValue(_jointTypeEnum, _algorithm, algorithmSetting, floatVal);
            rowData.set(col, floatVal);
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
        }
        else
        {
          // Here I want to display a error dialog.  For some reason, even though this is on the AWT-Event queue
          // if this method is called when the user types in a too large value, then clicks on some other window (so focus
          // is lost), this will hang unless I call invokeLater.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              JOptionPane.showMessageDialog(_parent, StringLocalizer.keyToString("ATGUI_THRESHOLD_OUT_OF_RANGE_KEY"),
                                            StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
                                            JOptionPane.ERROR_MESSAGE);
            }
          });
        }
      }
      catch (NumberFormatException nfe)
      {
        // shouldn't ever happen because the FloatNumberField class will make sure floats get put here
        Assert.expect(false);
      }
    }
    else if (value instanceof String)
    {
      rowData.set(VALUE_COLUMN, value);
      // a Boolean type threshold will come in to here as a string, because it's represented with a "Yes" or "No"
      // in the table.  So if the value is a string with contents "Yes" or "No", convert it back to a boolean when setting
      // the value
      if ((valStr.equalsIgnoreCase("Yes")) || (valStr.equalsIgnoreCase("No")))
      {
        if (valStr.equalsIgnoreCase("Yes"))
        {
          ConfigSetInitialThresholdsValueCommand setCommand = new ConfigSetInitialThresholdsValueCommand(_jointTypeEnum, algorithm, algorithmSetting, new Boolean(true));
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, algorithm, algorithmSetting);
            _commandManager.trackState(undoState);
            _commandManager.execute(setCommand);
            rowData.set(col, new Boolean(true));
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
        }
        else
        {
          ConfigSetInitialThresholdsValueCommand setCommand = new ConfigSetInitialThresholdsValueCommand(_jointTypeEnum, algorithm, algorithmSetting, new Boolean(false));
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, algorithm, algorithmSetting);
            _commandManager.trackState(undoState);
            _commandManager.execute(setCommand);
            rowData.set(col, new Boolean(false));
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
        }
      }
      else
      {
        ConfigSetInitialThresholdsValueCommand setCommand = new ConfigSetInitialThresholdsValueCommand(_jointTypeEnum, algorithm, algorithmSetting, (String)value);
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, algorithm, algorithmSetting);
            _commandManager.trackState(undoState);
            _commandManager.execute(setCommand);
            rowData.set(col, (String)value);
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }        
      }
    }
    else if (value instanceof java.util.List)// it's a list of string
    {
      rowData.set(VALUE_COLUMN, value);
      /** @todo Undo? */
//      _subtype.setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), (ArrayList)value);
    }
    else
      Assert.expect(false);

    _setValueValid = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isCellEditable(int row, int col)
  {
    if (_testRunning)
      return false;
    
    List<Object> rowData = _displayData.get(row);    
    if ((rowData.get(0) == _standardLabel) || (rowData.get(0) == _additionalLabel))
    {
      return false;
    }

    if (col == VALUE_COLUMN)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  /**
   * @author Andy Mechtenberg
   */
  void setTestRunning(boolean testRunning)
  {
    _testRunning = testRunning;
  }
}
