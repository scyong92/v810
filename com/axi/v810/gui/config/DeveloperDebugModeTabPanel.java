package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Chin-Seong, Kee
 * this panel is new panel for Developer Debug Mode use only, normally it wont appear
 * until license is under Developer License
 * 
 */
public class DeveloperDebugModeTabPanel extends AbstractTaskPanel implements Observer
{
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();

  private Config _config = Config.getInstance();
  private ConfigEnvironment _envPanel = null;
 
  protected Border _taskPanelBorder;
  protected BorderLayout _taskPanelLayout = null;
  
  private JPanel _centerPanel = new JPanel();
  private DeveloperDebugConfigTable _developerDebugConfigTable;
  private DeveloperDebugConfigTableModel _developerDebugConfigTableModel;
  private JScrollPane _developerDebugConfigScrollPane = new JScrollPane();
  
  private JPanel _developerConfigPanel = new JPanel();
  private BorderLayout _developerConfigPanelBorderLayout = new BorderLayout();
  private JPanel _buttonPanel = new JPanel(new FlowLayout());
  private JLabel _developerDebugConfigInfoLabel = new JLabel();
  /**
   * @author Chin-Seong, Kee
   */
  public DeveloperDebugModeTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
    finally
    {
    }
  }

  /**
   * This class is an observer of config. We will determine what
   * kind of changes were made and decide if we need to update the gui.
   * @author chin-seong, Kee
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)argument;
            if (configEnum instanceof DeveloperDebugConfigEnum)
            {
              if (DeveloperDebugConfigEnum.isConfigEnumExists(configEnum))
              {
                updateDataOnScreen();
              }
            }
          }
        }
      }
    });
  }
  
  private void jbInitialize()
  {
    _taskPanelBorder = BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,
                                                                                          Color.white,
                                                                                          new Color(182, 182, 182),
                                                                                          new Color(62, 62, 62)
                                                                                          ,new Color(89, 89, 89)),
                                                          BorderFactory.createEmptyBorder(0, 0, 0, 0));
    _taskPanelLayout = new BorderLayout();
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void jbInit() throws Exception
  {
    jbInitialize();
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);
    
    add(_centerPanel, BorderLayout.CENTER);

    _developerDebugConfigTableModel = new DeveloperDebugConfigTableModel(this);
    _developerDebugConfigTableModel.populateConfigData();
    
    _developerDebugConfigTable = new DeveloperDebugConfigTable(this, _developerDebugConfigTableModel);
    final ListSelectionModel ruleSetRowSelectionModel = _developerDebugConfigTable.getSelectionModel();
    _developerDebugConfigScrollPane.getViewport().add(_developerDebugConfigTable);

    _centerPanel.add(_developerConfigPanel, BorderLayout.WEST);
    _centerPanel.setVisible(true);
    _developerConfigPanel.setLayout(_developerConfigPanelBorderLayout);
    _developerConfigPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_JTA_DEVELOPER_DEBUG_CONFIG_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _developerConfigPanel.add(_developerDebugConfigScrollPane, BorderLayout.CENTER);
    //_developerConfigPanel.add(_buttonPanel, BorderLayout.NORTH);

    _developerDebugConfigInfoLabel.setText(StringLocalizer.keyToString("CFGUI_JTA_DEVELOPER_DEBUG_INFO_LABEL_KEY"));
    _developerConfigPanel.add(_developerDebugConfigInfoLabel, BorderLayout.NORTH);
    
    add(_centerPanel, BorderLayout.WEST);
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author Chin-Seong, Kee
   */
  public void start()
  {
//    System.out.println("SWOptionsGeneralTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");
    ConfigObservable.getInstance().addObserver(this);
    //updateDataOnScreen();
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author Chin-Seong, Kee
  */
  public boolean isReadyToFinish()
  {
    return true;
  }
  
  private void updateDataOnScreen()
  {
    _developerDebugConfigTableModel.populateConfigData();
    _developerDebugConfigTable.setEditorsAndRenderers();
    //System.out.println("test");
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author Chin-Seong, Kee
   */
  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
  }
}
