package com.axi.v810.gui.config;

import java.util.*;
import javax.swing.table.*;

import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * @author wei-chin.chong
 */
public class ProductionTuneSettingTableModel extends AbstractTableModel
{
  private ProductionTuneSettings _productionTuneSettings = null;

  //String labels retrieved from the localization properties file
  private static final String _TYPE_LABEL = StringLocalizer.keyToString("CFGUI_TYPE_KEY");
  private static final String _NAME_LABEL = StringLocalizer.keyToString("CFGUI_START_WITH_NAME_TITLE_KEY");

  private String[] _columnLabels = { _TYPE_LABEL, _NAME_LABEL};
  
  private List<ProductionTuneSettingData> _listOfProductionTuneSettingData = new ArrayList() ;
  
  /**
   * Constructor.
   * @author Wei Chin
   */
  public ProductionTuneSettingTableModel()
  {
    // do nothing
  }

  /**
   * @author Wei Chin
   */
  void clearData()
  {
    if(_listOfProductionTuneSettingData != null)
      _listOfProductionTuneSettingData.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Wei Chin
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Wei Chin
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Wei Chin
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Wei Chin
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;
    
    ProductionTuneSettingData productionTuneSetting = _listOfProductionTuneSettingData.get(rowIndex);    
    
    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case ProductionTuneSettingTable._TYPE_INDEX:
        value = productionTuneSetting.getType();
        break;
      case ProductionTuneSettingTable._NAME_INDEX:
        value = productionTuneSetting.getName();
        break;
      default:
        Assert.expect(false);
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Wei Chin
   */
  public int getRowCount()
  {
    Assert.expect(_listOfProductionTuneSettingData != null);

    return _listOfProductionTuneSettingData.size();
  }

  /**
   * Overridden method.
   * @author Wei Chin
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    
    return true;
  }

  /**
   * @author Wei Chin
   */
  public void setData()
  {
    clearData();
    
    _productionTuneSettings = null;
    _productionTuneSettings = ProductionTuneSettings.readSettings();
    
    int index = 0;
    for(String type : _productionTuneSettings.getTypeToNamesMap().keySet())
    {
      for( String name :  _productionTuneSettings.getSelectedType(type).keySet())
      {
        _listOfProductionTuneSettingData.add(new ProductionTuneSettingData(index++, type, name));
      }
    }
    
    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Wei Chin
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if(_listOfProductionTuneSettingData.isEmpty())
      return;
    
    if(rowIndex >= _listOfProductionTuneSettingData.size())
      return;
    
    ProductionTuneSettingData productionTuneSetting = _listOfProductionTuneSettingData.get(rowIndex);
    switch(columnIndex)
    {
      case ProductionTuneSettingTable._TYPE_INDEX:
        Assert.expect(object instanceof String);
        productionTuneSetting.setType((String)object);
        break;
      case ProductionTuneSettingTable._NAME_INDEX:
        Assert.expect(object instanceof String);
        productionTuneSetting.setName((String)object);
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * @author Wei Chin
   */
  public void addRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows ; i++)
    {
      ProductionTuneSettingData newElement = new ProductionTuneSettingData(getRowCount()+1, ProductionTuneSettings._JOINTTYPE, "");
      _listOfProductionTuneSettingData.add(newElement);
    }
    fireTableDataChanged();
  }

  /**
    * @author Wei Chin
    */
   public void removeRowIndex(int rowIndex)
   {
     Assert.expect(rowIndex >= 0);

     if(getRowCount() <= 0)
       return;
     
     _listOfProductionTuneSettingData.remove(rowIndex);
     
     fireTableDataChanged();
  }
  
  /**
    * @author Wei Chin
    */
   public void removeRows(int numRows)
   {
     Assert.expect(numRows > 0);

     if(getRowCount() <= 0)
       return;
     
     for (int i = 0; i < numRows ; i++)
     {
       _listOfProductionTuneSettingData.remove(getRowCount() - 1);
     }
     fireTableDataChanged();
  }

  /**
   * @return the _listOfProductionTuneSetting
   * @author Wei Chin
   */
  public boolean validateData()
  {
    for(ProductionTuneSettingData productionTuneSetting : _listOfProductionTuneSettingData)
    {
      if(productionTuneSetting.getType().trim().isEmpty() || 
         productionTuneSetting.getName().trim().isEmpty())
      {
        return false;
      }
    }
    return true;
  }
    
  /**
   * @return
   * @author Wei Chin
   */
  public void  updateProductionTuneSettingMap()
  {
    _productionTuneSettings.getTypeToNamesMap().clear();
    
    for(ProductionTuneSettingData productionTuneSetting : _listOfProductionTuneSettingData)
    {
      _productionTuneSettings.addTypeToNameMap(productionTuneSetting.getType(), productionTuneSetting.getName());
    }
  }

  /**
   * @return the _productionTuneSettings
   * @author Wei Chin
   */
  public ProductionTuneSettings getProductionTuneSettings()
  {
    return _productionTuneSettings;
  }
   
   /**
    * @author Wei Chin
    */
   public class ProductionTuneSettingData 
   {
     int _id;
     private String _type;
     private String _name;
     
     /**
      * @param id
      * @param type
      * @param name 
      * @author Wei Chin
      */
     public ProductionTuneSettingData(int id, String type, String name)
     {
       _id = id;
       _type = type;
       _name = name;
     }

    /**
     * @return the _type
     * @author Wei Chin
     */
    public String getType()
    {
      return _type;
    }

    /**
     * @param type the _type to set
     * @author Wei Chin
     */
    public void setType(String type)
    {
      _type = type;
    }

    /**
     * @return the _name
     * @author Wei Chin
     */
    public String getName()
    {
      return _name;
    }

    /**
     * @param name the _name to set
     * @author Wei Chin
     */
    public void setName(String name)
    {
      _name = name;
    }
   }
}
