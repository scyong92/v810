package com.axi.v810.gui.config;

import java.util.*;


import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import java.util.Map.Entry;

/**
 * @author Kee Chin Seong
 * @version 1.0
 */
public class PreOrPostInspectionScriptCustomizeInputTableModel extends DefaultSortTableModel
{
  private MainMenuGui _mainUI;
  private ProductionOptionsGeneralTabPanel _parent;
  private PreOrPostInspectionScriptCustomizeInputTable _table;
  
  //String labels retrieved from the localization properties file
  private static final String _OUTPUT_PARAMETER_INDEX = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_PARAMETER_NO_KEY");
  private static final String _SEQUENCE_NO_INDEX = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_SEQUENCE_NO_KEY");
  
  private static final String _PANEL_SERIAL_NUMBER = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_PANEL_SERIAL_NUMBER_KEY");
  private static final String _PROJECT_NAME = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_PROJECT_NAME_KEY");
  private static final String _SERIAL_NUMBER = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_SERIAL_NUMBER_KEY");
  private static final String _USER_NAME = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_USER_NAME_KEY");
  private static final String _EQUIPMENT_CODE = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_EQUIPMENT_CODE_KEY");
  private static final String _ERROR_MESSAGE = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_ERROR_MESSAGE_KEY");
  private static final String _WARNING_MESSAGE = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_WARNING_MESSAGE_KEY");
  
  private String[] _columnLabels = {_SEQUENCE_NO_INDEX, _OUTPUT_PARAMETER_INDEX};

  private Map<String, Boolean> _sequenceOutputData = new HashMap<String, Boolean>();

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  /**
   * Constructor.
   * @author Kee Chin Seong
   */
  public PreOrPostInspectionScriptCustomizeInputTableModel(ProductionOptionsGeneralTabPanel parent,boolean isInput)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
    
    _sequenceOutputData.clear();
    if (isInput)
    {
      _sequenceOutputData.put(_PROJECT_NAME, true);
      _sequenceOutputData.put(_USER_NAME, true);
      _sequenceOutputData.put(_EQUIPMENT_CODE, true);
    }
    else
    {
      _sequenceOutputData.put(_PANEL_SERIAL_NUMBER, true);
      _sequenceOutputData.put(_ERROR_MESSAGE, true);
      _sequenceOutputData.put(_WARNING_MESSAGE, true);
    }
   
  }

  /**
   * @author Kee Chin Seong
   */
  void clearData()
  {
    if (_sequenceOutputData != null)
      _sequenceOutputData.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Kee Chin Seong
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Kee Chin Seong
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Kee Chin Seong
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Kee Chin Seong
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_sequenceOutputData != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case PreOrPostInspectionScriptCustomizeInputTable._OUTPUT_PARAMETER_INDEX:
        return new ArrayList<String>(_sequenceOutputData.keySet()).get(rowIndex);
      case PreOrPostInspectionScriptCustomizeInputTable._PARAMETER_ENABLED_INDEX:
      {
        if(_table == null || _table.getEnabledCustomizeOutputParametersFromConfig() == null)
              System.out.println("table null");
        else if(_sequenceOutputData == null)
              System.out.println(" null ");
        if(_table.getEnabledCustomizeOutputParametersFromConfig().contains(new ArrayList<String>(_sequenceOutputData.keySet()).get(rowIndex)))
            return true;
        else
            return false;
        //return new ArrayList<Boolean>(_sequenceOutputData.values()).get(rowIndex);
      }
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }
  
  /**
   * @author Kee Chin Seong
   * @param table 
   */
  public void setTable(PreOrPostInspectionScriptCustomizeInputTable table)
  {
      Assert.expect(table != null);
      
      _table = table;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Kee Chin Seong
   */
  public int getRowCount()
  {
    if (_sequenceOutputData == null)
        return 0;
    else
        return _sequenceOutputData.size();
  }

  /**
   * Overridden method.
   * @author Kee Chin Seong
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    switch (columnIndex)
    {
      case PreOrPostInspectionScriptCustomizeInputTable._OUTPUT_PARAMETER_INDEX:
      {
          return false;
      }
    }
    
    return true;
  }
  
  /**
   * @author Kee Chin Seong
   * @param rowIndex 
   */
  public void enableParameter(int rowIndex, boolean enabled)
  {
     List<String> paramList = new ArrayList<String>(_sequenceOutputData.keySet());
     
     _sequenceOutputData.put(paramList.get(rowIndex), enabled);
     
     this.fireTableDataChanged();
  }

   /**
    * @author Kee Chin Seong
    */
   Map<String, Boolean> getData()
   {
     return _sequenceOutputData;
   }
 
   /**
    * @author Kee Chin Seong
   */
   public void sortColumn(int column, boolean ascending)
   {
     // do nothing
   }
 }
