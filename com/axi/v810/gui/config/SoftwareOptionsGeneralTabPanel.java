package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

//CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
import com.axi.v810.hardware.*;
/**
 * @author Andy Mechtenberg
 */
public class SoftwareOptionsGeneralTabPanel extends AbstractTaskPanel implements Observer
{
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private ResultsProcessor _resultsProcessor = ResultsProcessor.getInstance();
  private HistoryLog _historyLog = HistoryLog.getInstance();

  private Config _config = Config.getInstance();

  private JPanel _centerPanel = null;
  private JTextField _projectDirectoryTextField;
  private JTextField _ndfDirectoryTextField;
  private JTextField _cadDirectoryTextField;
  private JTextField _resultsDirectoryTextField;
  private JTextField _verificationImageDirectoryField;
  
  private JTextField _projectDatabaseDirectoryTextField;
  private JCheckBox _autoPullCheckBox;
  private JCheckBox _deleteLocalResultsCheckBox;
  //Create & delete recipe's history log file- hsia-fen.tan 3-April-2013
  private JCheckBox _createHistoryLogCheckBox ;
  private JCheckBox _deleteHistoryFileCheckBox;
  //Prompt warning message before run alignment if recipe contains high mag- hsia-fen.tan
  private JCheckBox _promptWarningMsgIfHighMagRecipeCheckBox;
  //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
  private JCheckBox _xrayAutoShutdownCheckBox;
  private JCheckBox _promptSavingRecipeAfterLearningCheckBox;
  private DecimalFormat _xrayAutoShutdownTimerDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _xrayAutoShutdownTimerDataDoc = new NumericRangePlainDocument();
  private NumericTextField _xrayAutoShutdownTimerDataTextField = new NumericTextField();
  private static final double _MIN_TIMEOUTS = 0;
  private LegacyXraySource _xraySource = null;
  private HTubeXraySource  _hTubeXraySource = null;
  
  //Auto Startup Scheduler task
  //@author Kee Chin Seong
  private static final int _MAX_MINUTES = 60;
  private static final int _MAX_HOUR  = 24;
  private JLabel _startupHourLabel; 
  private JLabel _startupMinuteLabel; 
  private JLabel _userStartupInfo;
  private JButton _setSchedulerStartupButton;
  private JCheckBox _enableMachineAutoStartup;
  private JComboBox _startupMin, _startupHour;
  
  private JPanel _machineAutoStartupMainPanel;
  private JPanel _machineAutoStartupHourPanel;
  private JPanel _machineAutoStartupMinutePanel;
  private JPanel _userStartupInputPanel;
  private JPanel _userStartupInputMainPanel;
  
  private static final int _MIN_RUNS = 0;
  private DecimalFormat _measurementDataFormat = new DecimalFormat();
  private DecimalFormat _repairDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _measurementDataDoc = new NumericRangePlainDocument();
  private NumericRangePlainDocument _repairDataDoc = new NumericRangePlainDocument();
  private NumericTextField _measurementDataTextField = new NumericTextField();
  private NumericTextField _repairDataTextField = new NumericTextField();
  
  //keep latest {number} of history log file- hsia-fen.tan
  private DecimalFormat _historyLogDataFormat = new DecimalFormat();
  private NumericRangePlainDocument _historyLogDataDoc = new NumericRangePlainDocument();
  private NumericTextField _historyFileTextField = new NumericTextField();
  private UserTypeEnum _currentUserType = null;

  private ButtonGroup _inspectionImageTypeButtonGroup;
  private JRadioButton _inspectionImageTypePNGRadioButton;
  private JRadioButton _inspectionImageTypeTIFFRadioButton;

  private ConfigEnvironment _envPanel = null;
  private boolean _inUse = false;
  
  //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
  private JCheckBox _useSpecificHistoryLogPathCheckBox;
  private JPanel _useSpecificHistoryLogPathPanel = new JPanel();
  private FlowLayout _useSpecificHistoryLogPathPanelFlowLayout = new FlowLayout();
  private JLabel _historyLogDirLabel = new JLabel();
  private JTextField _historyLogDirTextField = new JTextField(30);
  private JButton _historyLogDirBrowseButton = new JButton();
  
  //Khaw Chek Hau - XCR3063: Notification Message When Using DRO 
  private JCheckBox _promptWarningMsgIfModifyDROLevelCheckBox;

  /**
   * @author Laura Cormos
   */
  public SoftwareOptionsGeneralTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
    finally
    {
      //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
      if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
      {
          AbstractXraySource xraySource = AbstractXraySource.getInstance();
          Assert.expect(xraySource instanceof LegacyXraySource);
          _xraySource = (LegacyXraySource)xraySource;

      }
      else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
      {
          AbstractXraySource xraySource = AbstractXraySource.getInstance();
          Assert.expect(xraySource instanceof HTubeXraySource );
          _hTubeXraySource = (HTubeXraySource )xraySource;

      }
    }
  }

  /**
   * This class is an observer of config. We will determine what
   * kind of changes were made and decide if we need to update the gui.
   * @author Andy Mechtenberg
   * @edited By Kee Chin Seong - XCR-3139 Allow user to change verification image path
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)argument;
            if (configEnum instanceof SoftwareConfigEnum)
            {
              SoftwareConfigEnum softwareConfigEnum = (SoftwareConfigEnum)configEnum;
              if (softwareConfigEnum.equals(SoftwareConfigEnum.PROJECT_DIR) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.NDF_LEGACY_DIR) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.RESULTS_DIR) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PROJECT_DATABASE_PATH) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.AUTOMATICALLY_PULL_LATEST_PROJECT_VERSION_ON_LOAD) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.AUTOMATICALLY_DELETE_RESULTS) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.KEEP_N_MOST_RECENT_RESULT_FILES) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.KEEP_N_MOST_RECENT_REPAIR_FILE_RESULTS) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.AUTOMATICALLY_DELETE_HISTORY_FILES) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.KEEP_N_MOST_RECENT_HISTORY_FILE) ||
                  //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
                  softwareConfigEnum.equals(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES)||
                  softwareConfigEnum.equals(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.VERIFICATION_DIR) )
              {
                updateDataOnScreen();
              }
            }
          }
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   * @edited By Kee Chin Seong - XCR-3139 Allow user to change verification image path
   */
  private void jbInit() throws Exception
  {
    int textFieldColumns = 30;
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    _centerPanel = new JPanel();
    VerticalFlowLayout centerPanelLayoutManager = new VerticalFlowLayout();
    _centerPanel.setLayout(centerPanelLayoutManager);

    add(_centerPanel, BorderLayout.CENTER);

    JPanel pathOptionsPanel = new JPanel(new PairLayout(0, 0, false));
    JPanel projectDatabaseOptionsPanel = new JPanel(new BorderLayout());
    JPanel resultsCleanupOptionsPanel = new JPanel(new BorderLayout());
    JPanel inspectionImageTypeOptionsPanel = new JPanel(new VerticalFlowLayout());

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    JPanel xrayAutoShutdownOptionsPanel = new JPanel(new BorderLayout());
    
    JPanel recipeSavingOptionsPanel = new JPanel(new BorderLayout());
    
    //Create history log CR- hsia-fen.tan
    JPanel showHistoryLogPanel = new JPanel(new VerticalFlowLayout()); 
    JPanel WarningMsgIfHighMagRecipe = new JPanel(new BorderLayout());
    //enhancement - for auto startup machine 
    //@author Kee chin Seong
    _machineAutoStartupMainPanel = new JPanel(new BorderLayout());
    _machineAutoStartupHourPanel = new JPanel(new PairLayout());
    _machineAutoStartupMinutePanel = new JPanel(new PairLayout());
    _userStartupInputPanel = new JPanel(new PairLayout());
    _userStartupInputMainPanel = new JPanel(new PairLayout());
    
    _startupHourLabel = new JLabel("Hour");
    _startupMinuteLabel = new JLabel("Minutes");
    _userStartupInfo = new JLabel(StringLocalizer.keyToString("CFGUI_AUTO_STARTUP_USER_INFO_KEY"));
    _setSchedulerStartupButton = new JButton("Schedule");
    _enableMachineAutoStartup = new JCheckBox("Enabled Scheduling Auto Startup");
    
    _startupHour = new JComboBox();
    _startupMin = new JComboBox();
    
    DecimalFormat twoDecimalPlaces = new DecimalFormat("#00");
    for(int hour = 0; hour < _MAX_HOUR; hour ++)
    {
      _startupHour.addItem(twoDecimalPlaces.format(hour));
    }
    for(int minute = 0; minute < _MAX_MINUTES; minute ++)
    {
      _startupMin.addItem(twoDecimalPlaces.format(minute));
    }
   
    _userStartupInfo.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _machineAutoStartupHourPanel.add(_startupHourLabel);
    _machineAutoStartupHourPanel.add(_startupHour);
    _machineAutoStartupMinutePanel.add(_startupMinuteLabel);
    _machineAutoStartupMinutePanel.add(_startupMin);
    //machineAutoStartupHourPanel.setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 10));
    
    _userStartupInputPanel.add(_machineAutoStartupHourPanel);
    _userStartupInputPanel.add(_machineAutoStartupMinutePanel);
    
    _userStartupInputMainPanel.add(_userStartupInputPanel);
    _userStartupInputMainPanel.add(_setSchedulerStartupButton);
    _userStartupInputMainPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    
    _machineAutoStartupMainPanel.add(_userStartupInputMainPanel, BorderLayout.SOUTH);
    _machineAutoStartupMainPanel.add(_userStartupInfo, BorderLayout.NORTH);
    _machineAutoStartupMainPanel.add(_enableMachineAutoStartup, BorderLayout.CENTER);
    //machineAutoStartupMainPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
    
    _centerPanel.add(pathOptionsPanel);
    _centerPanel.add(projectDatabaseOptionsPanel);
    _centerPanel.add(resultsCleanupOptionsPanel);
    _centerPanel.add(recipeSavingOptionsPanel);
    //Prompt warning message if recipe contain high mag CR- hsia-fen.tan
    _centerPanel.add(WarningMsgIfHighMagRecipe);
    //Create history log CR- hsia-fen.tan
    _centerPanel.add(showHistoryLogPanel);
    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
      _centerPanel.add(xrayAutoShutdownOptionsPanel);
    }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
      _centerPanel.add(xrayAutoShutdownOptionsPanel);
    }
    _centerPanel.add(inspectionImageTypeOptionsPanel);
    _centerPanel.add(_machineAutoStartupMainPanel);
    
    Border pathOptionsTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_PATH_OPTIONS_KEY"));
    Border pathOptionsTitledBorder = BorderFactory.createCompoundBorder(pathOptionsTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 5, 0));

    Border projectDatabaseTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_PROJECT_DATABASE_OPTIONS_KEY"));
    Border projectDatabaseTitledBorder = BorderFactory.createCompoundBorder(projectDatabaseTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 5, 0));

    Border resultsCleanupTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_RESULTS_CLEANUP_KEY"));
    Border resultsCleanupTitledBorder = BorderFactory.createCompoundBorder(resultsCleanupTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    Border xrayAutoShutdownTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_XRAY_AUTO_SHUTDOWN_KEY"));
    Border xrayAutoShutdownTitledBorder = BorderFactory.createCompoundBorder(xrayAutoShutdownTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));
    
    Border recipeSavingTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_RECIPE_SAVING_KEY"));
    Border recipeSavingTitledBorder = BorderFactory.createCompoundBorder(recipeSavingTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));
   
    //Create history log CR - hsia-fen.tan    
    Border showHistoryLogTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_SHOW_HISTORY_LOG_KEY"));
    Border showHistoryLogTitledBorder = BorderFactory.createCompoundBorder(showHistoryLogTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));
    //Prompt warning message if recipe contain high mag CR- hsia-fen.tan
    Border HighMagWarningMsgTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_HIGH_MAG_WARNING_MESSAGE_KEY"));
    Border HighMagWarningMsgTitledBorder = BorderFactory.createCompoundBorder(HighMagWarningMsgTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));
    
    Border inspectionImageTypeTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_INSPECTION_IMAGE_TYPE_KEY"));
    Border inspectionImageTypeTitledBorder = BorderFactory.createCompoundBorder(inspectionImageTypeTitledBorder1, BorderFactory.createEmptyBorder(0, 10, 0, 0));
    //enhancement Auto Startup Machine 
    //@author - Kee Chin Seong
    Border AutoStartupTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_XRAY_AUTO_STARTUP_KEY"));
    Border AutoStartupTitledBorder = BorderFactory.createCompoundBorder(AutoStartupTitledBorder1, BorderFactory.createEmptyBorder(0, 15, 5, 200));
    
    Border smemaSequenceTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                StringLocalizer.keyToString("CFGUI_SMEMA_SEQUENCE_KEY"));

    pathOptionsPanel.setBorder(pathOptionsTitledBorder);
    projectDatabaseOptionsPanel.setBorder(projectDatabaseTitledBorder);
    resultsCleanupOptionsPanel.setBorder(resultsCleanupTitledBorder);
    
    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    xrayAutoShutdownOptionsPanel.setBorder(xrayAutoShutdownTitledBorder);
    
    recipeSavingOptionsPanel.setBorder(recipeSavingTitledBorder);
    WarningMsgIfHighMagRecipe.setBorder(HighMagWarningMsgTitledBorder );
    
    //Create history log CR- hsia-fen.tan
    showHistoryLogPanel.setBorder(showHistoryLogTitledBorder ); 
    inspectionImageTypeOptionsPanel.setBorder( inspectionImageTypeTitledBorder );
        //
    _machineAutoStartupMainPanel.setBorder(AutoStartupTitledBorder); 

    // Path Options panel setup
    JPanel projectDirectoryTextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    JLabel projectDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_PROJECTS_DIRECTORY_KEY"));
    _projectDirectoryTextField = new JTextField(textFieldColumns);
    JButton projectDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    projectDirectoryTextButtonPanel.add(_projectDirectoryTextField);
    projectDirectoryTextButtonPanel.add(projectDirectoryChooserButton);

    // ndf directory options
    JPanel ndfDirectoryTextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    JLabel ndfDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_NDFS_DIRECTORY_KEY"));
    _ndfDirectoryTextField = new JTextField(textFieldColumns);
    JButton ndfDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    ndfDirectoryTextButtonPanel.add(_ndfDirectoryTextField);
    ndfDirectoryTextButtonPanel.add(ndfDirectoryChooserButton);
    
    JPanel cadDirectoryTextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    JLabel cadDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_CADS_DIRECTORY_KEY"));
    _cadDirectoryTextField = new JTextField(textFieldColumns);
    JButton cadDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    cadDirectoryTextButtonPanel.add(_cadDirectoryTextField);
    cadDirectoryTextButtonPanel.add(cadDirectoryChooserButton);
    
    JPanel verificationImageDirectoryTestButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
    
    JLabel verificationImageDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_VERIFICATION_IMAGE_DIRECTORY_KEY"));
    _verificationImageDirectoryField = new JTextField(textFieldColumns);
    JButton verificationImageDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    
    verificationImageDirectoryTestButtonPanel.add(_verificationImageDirectoryField);
    verificationImageDirectoryTestButtonPanel.add(verificationImageDirectoryChooserButton);
    
    // results directory options
    JPanel resultsDirectoryTextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    JLabel resultsDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_RESULTS_DIRECTORY_KEY"));
    _resultsDirectoryTextField = new JTextField(textFieldColumns);
    JButton resultsDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    resultsDirectoryTextButtonPanel.add(_resultsDirectoryTextField);
    resultsDirectoryTextButtonPanel.add(resultsDirectoryChooserButton);

    pathOptionsPanel.add(projectDirectoryLabel);
    pathOptionsPanel.add(projectDirectoryTextButtonPanel);
    pathOptionsPanel.add(ndfDirectoryLabel);
    pathOptionsPanel.add(ndfDirectoryTextButtonPanel);
    pathOptionsPanel.add(cadDirectoryLabel);
    pathOptionsPanel.add(cadDirectoryTextButtonPanel);
    pathOptionsPanel.add(resultsDirectoryLabel);
    pathOptionsPanel.add(resultsDirectoryTextButtonPanel);
    pathOptionsPanel.add(verificationImageDirectoryLabel);
    pathOptionsPanel.add(verificationImageDirectoryTestButtonPanel);

    // project database options
    JPanel projectDatabaseDirectoryTextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));

    JLabel projectDatabaseDirectoryLabel = new JLabel(StringLocalizer.keyToString("CFGUI_PROJECT_DATABASE_DIRECTORY_KEY"));
    _projectDatabaseDirectoryTextField = new JTextField(textFieldColumns);
    JButton projectDatabaseDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _autoPullCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_AUTO_PULL_LATEST_PROJECT_KEY"));

    projectDatabaseDirectoryTextButtonPanel.add(_projectDatabaseDirectoryTextField);
    projectDatabaseDirectoryTextButtonPanel.add(projectDatabaseDirectoryChooserButton);

    JPanel projectDatabaseDirectoryPanel = new JPanel(new PairLayout(0, 0 , false));
    projectDatabaseDirectoryPanel.add(projectDatabaseDirectoryLabel);
    projectDatabaseDirectoryPanel.add(projectDatabaseDirectoryTextButtonPanel);
    projectDatabaseOptionsPanel.add(projectDatabaseDirectoryPanel, BorderLayout.NORTH);
    projectDatabaseOptionsPanel.add(_autoPullCheckBox, BorderLayout.SOUTH);

    JLabel[] allLabels = new JLabel[]{projectDirectoryLabel,
                         ndfDirectoryLabel,
                         resultsDirectoryLabel,
                         projectDatabaseDirectoryLabel};
    SwingUtils.makeAllSameLength(allLabels);

    // results clean up panel
    _deleteLocalResultsCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_AUTO_DELETE_LOCAL_RESULTS_KEY"));
    JLabel keepOnly_1 = new JLabel(StringLocalizer.keyToString("CFGUI_KEEP_ONLY_LATEST_KEY"));
    JLabel keepOnly_2 = new JLabel(StringLocalizer.keyToString("CFGUI_KEEP_ONLY_LATEST_KEY"));
    _measurementDataFormat.setParseIntegerOnly(true);
    _repairDataFormat.setParseIntegerOnly(true);
    _measurementDataTextField.setDocument(_measurementDataDoc);
    _repairDataTextField.setDocument(_repairDataDoc);
    _measurementDataTextField.setFormat(_measurementDataFormat);
    _repairDataTextField.setFormat(_repairDataFormat);
    _measurementDataDoc.setRange(new IntegerRange(_MIN_RUNS, Integer.MAX_VALUE));
    _repairDataDoc.setRange(new IntegerRange(_MIN_RUNS, Integer.MAX_VALUE));
    _measurementDataTextField.setColumns(5);
    _repairDataTextField.setColumns(5);
    JLabel measurementDataLabel = new JLabel(StringLocalizer.keyToString("CFGUI_MEASUREMENT_DATA_KEY"));
    JLabel repairDataLabel = new JLabel(StringLocalizer.keyToString("CFGUI_REPAIR_DATA_KEY"));

    JPanel measurementDataPanel = new JPanel();
    JPanel repairDataPanel = new JPanel();
    measurementDataPanel.add(keepOnly_1);
    measurementDataPanel.add(_measurementDataTextField);
    measurementDataPanel.add(measurementDataLabel);
    repairDataPanel.add(keepOnly_2);
    repairDataPanel.add(_repairDataTextField);
    repairDataPanel.add(repairDataLabel);
    repairDataPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

    JPanel overallDataPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.LEFT, VerticalFlowLayout.TOP, 0, 0));
    overallDataPanel.add(measurementDataPanel);
    overallDataPanel.add(repairDataPanel);
    overallDataPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));

    resultsCleanupOptionsPanel.add(_deleteLocalResultsCheckBox, BorderLayout.NORTH);
    resultsCleanupOptionsPanel.add(overallDataPanel, BorderLayout.CENTER);

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    // Xray Auto Short Term Shutdown panel
    _xrayAutoShutdownCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_EANBLE_XRAY_AUTO_SHUTDOWN_KEY"));
    JLabel shutdownAfter = new JLabel(StringLocalizer.keyToString("CFGUI_XRAY_AUTO_SHUTDOWN_AFTER_KEY"));
    _xrayAutoShutdownTimerDataFormat.setParseIntegerOnly(false);
    _xrayAutoShutdownTimerDataTextField.setDocument(_xrayAutoShutdownTimerDataDoc);
    _xrayAutoShutdownTimerDataTextField.setFormat(_xrayAutoShutdownTimerDataFormat);
    _xrayAutoShutdownTimerDataDoc.setRange(new DoubleRange(_MIN_TIMEOUTS, Double.MAX_VALUE));
    _xrayAutoShutdownTimerDataTextField.setColumns(5);
    JLabel xrayAutoShutdownTimerDataLabel = new JLabel(StringLocalizer.keyToString("CFGUI_XRAY_AUTO_SHUTDOWN_TIMER_UNIT_KEY"));

    JPanel xrayAutoShutdownTimerDataPanel = new JPanel();
    xrayAutoShutdownTimerDataPanel.add(shutdownAfter);
    xrayAutoShutdownTimerDataPanel.add(_xrayAutoShutdownTimerDataTextField);
    xrayAutoShutdownTimerDataPanel.add(xrayAutoShutdownTimerDataLabel);
    xrayAutoShutdownTimerDataPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

    JPanel overallXrayAutoShutdownTimerDataPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.LEFT, VerticalFlowLayout.TOP, 0, 0));
    overallXrayAutoShutdownTimerDataPanel.add(xrayAutoShutdownTimerDataPanel);
    overallXrayAutoShutdownTimerDataPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));

    //Prompt Warning message if recipe contain high mag CR- hsia-fen.tan
    _promptWarningMsgIfHighMagRecipeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_PROMPT_WARNING_MESSAGE_FOR_HIGH_MAG_RECIPE_KEY"));
    WarningMsgIfHighMagRecipe.add(_promptWarningMsgIfHighMagRecipeCheckBox, BorderLayout.NORTH);
    
    //Khaw Chek Hau - XCR3063: Notification Message When Using DRO 
    _promptWarningMsgIfModifyDROLevelCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_PROMPT_WARNING_MESSAGE_FOR_CHANGING_DRO_LEVEL_KEY"));
    WarningMsgIfHighMagRecipe.add(_promptWarningMsgIfModifyDROLevelCheckBox, BorderLayout.SOUTH);
    
    xrayAutoShutdownOptionsPanel.add(_xrayAutoShutdownCheckBox, BorderLayout.NORTH);
    xrayAutoShutdownOptionsPanel.add(overallXrayAutoShutdownTimerDataPanel, BorderLayout.CENTER);
    
    _promptSavingRecipeAfterLearningCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_PROMPT_SAVE_RECIPE_AFTER_LEARNING_KEY"));
    recipeSavingOptionsPanel.add(_promptSavingRecipeAfterLearningCheckBox, BorderLayout.NORTH);
    
    //Create & delete history log CR- hsia-fen.tan
    _createHistoryLogCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_SHOW_PROJECT_HISTORY_LOG_KEY"));
    showHistoryLogPanel.add(_createHistoryLogCheckBox);  
    
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    _useSpecificHistoryLogPathCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_USE_SPECIFIC_HISTORY_LOG_PATH_KEY"));
    showHistoryLogPanel.add(_useSpecificHistoryLogPathCheckBox);  
    
    _useSpecificHistoryLogPathPanel.setLayout(_useSpecificHistoryLogPathPanelFlowLayout);
    _useSpecificHistoryLogPathPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _useSpecificHistoryLogPathPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0)); // to indent the install dir line
    _useSpecificHistoryLogPathPanel.add(_historyLogDirLabel);
    _useSpecificHistoryLogPathPanel.add(_historyLogDirTextField);
    _useSpecificHistoryLogPathPanel.add(_historyLogDirBrowseButton);
    
    showHistoryLogPanel.add(_useSpecificHistoryLogPathPanel);

    _historyLogDirLabel.setText(StringLocalizer.keyToString("CFGUI_HISTORY_LOG_DIRECTORY_KEY"));
    
    _deleteHistoryFileCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_AUTO_DELETE_HISTORY_FILE_KEY"));
    
    JLabel historyLogLabel = new JLabel(StringLocalizer.keyToString("CFGUI_HISTORY_FILE_KEY"));
    JLabel keepOnly_3 = new JLabel(StringLocalizer.keyToString("CFGUI_KEEP_ONLY_LATEST_KEY"));
   
    JPanel historyFilePanel = new JPanel();
    historyFilePanel.add(keepOnly_3);
    historyFilePanel.add(_historyFileTextField);
    historyFilePanel.add(historyLogLabel); 
     
    JPanel  historyFileDataPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.LEFT, VerticalFlowLayout.TOP, 0, 0));
    historyFileDataPanel.add(historyFilePanel);
    historyFilePanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    
    showHistoryLogPanel.add(_deleteHistoryFileCheckBox);
    showHistoryLogPanel.add(historyFileDataPanel);
    
    _historyLogDataFormat.setParseIntegerOnly(true);
    _historyLogDataDoc.setRange(new IntegerRange(_MIN_RUNS, Integer.MAX_VALUE));
    _historyFileTextField.setDocument(_historyLogDataDoc);
    _historyFileTextField.setFormat(_historyLogDataFormat);   
    _historyFileTextField.setColumns(5);   
   
    _inspectionImageTypePNGRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_PNG_KEY"));
    _inspectionImageTypeTIFFRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_TIFF_KEY"));
    
    _inspectionImageTypeButtonGroup = new ButtonGroup();
    _inspectionImageTypeButtonGroup.add(_inspectionImageTypePNGRadioButton);
    _inspectionImageTypeButtonGroup.add(_inspectionImageTypeTIFFRadioButton);   

    inspectionImageTypeOptionsPanel.add(_inspectionImageTypePNGRadioButton);
    inspectionImageTypeOptionsPanel.add(_inspectionImageTypeTIFFRadioButton);

    JPanel[] allPanels = new JPanel[]{pathOptionsPanel,
                         projectDatabaseOptionsPanel,
                         resultsCleanupOptionsPanel,
                         xrayAutoShutdownOptionsPanel,
                         recipeSavingOptionsPanel,
                         WarningMsgIfHighMagRecipe,
                         showHistoryLogPanel,
                         inspectionImageTypeOptionsPanel};
    SwingUtils.makeAllSameLength(allPanels);


    // hook up all action listeners
    _projectDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        projectDirectoryTextField_setValue();
      }
    });

    _projectDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDirectoryTextField_setValue();
      }
    });

    projectDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDirectoryChooser();
      }
    });
    
     //Kee Chin Seong - verification image path chooser
    _verificationImageDirectoryField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        verificationImageDirectoryTextField_setValue();
      }
    });

     //Kee Chin Seong - verification image path chooser
    _verificationImageDirectoryField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verificationImageDirectoryTextField_setValue();
      }
    });

     //Kee Chin Seong - verification image path chooser
    verificationImageDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        verificationImageDirectoryChooser();
      }
    });

    _ndfDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        ndfDirectoryTextField_setValue();
      }
    });

    _ndfDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ndfDirectoryTextField_setValue();
      }
    });

    ndfDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ndfDirectoryChooser();
      }
    });
    
    _cadDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        cadDirectoryTextField_setValue();
      }
    });

    _cadDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadDirectoryTextField_setValue();
      }
    });
    
    cadDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadDirectoryChooser();
      }
    });
    
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    _historyLogDirTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        historyLogDirTextField_setValue();
      }
    });
    
    _historyLogDirTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        historyLogDirTextField_setValue();
      }
    });

    _resultsDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        resultsDirectoryTextField_setValue();
      }
    });

    _resultsDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resultsDirectoryTextField_setValue();
      }
    });

    resultsDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resultsDirectoryChooser();
      }
    });

    _projectDatabaseDirectoryTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        projectDatabaseDirectoryTextField_setValue();
      }
    });

    _projectDatabaseDirectoryTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDatabaseDirectoryTextField_setValue();
      }
    });

    projectDatabaseDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        projectDatabaseDirectoryChooser();
      }
    });

    _autoPullCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        autoPullCheckBox_actionListener();
      }
    });

    _deleteLocalResultsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteLocalResultsCheckBox_actionListener();
      }
    });

    _measurementDataTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        measurementDataTextField_setValue();
      }
    });

    _measurementDataTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measurementDataTextField_setValue();
      }
    });
    
    //Auto delete history log file- hsia-fen.tan  
      _deleteHistoryFileCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteHistoryFileCheckBox_actionListener();
      }
    });
       
      _historyFileTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        historyFileTextField_setValue();
      }
    });

     _historyFileTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        historyFileTextField_setValue();
      }
    });
     
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    _historyLogDirBrowseButton.setText(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _historyLogDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        historyLogDirBrowseButton_actionPerformed(e);
      }
    });
    
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    _useSpecificHistoryLogPathCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        useSpecificHistoryLogPathCheckBox_actionListener();
      }
    });

    _repairDataTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        repairDataTextField_setValue();
      }
    });

    _repairDataTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairDataTextField_setValue();
      }
    });
    
    _setSchedulerStartupButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        scheduleStartupMachine_setValue();
      }
    });

    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    _xrayAutoShutdownCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        xrayAutoShutdownCheckBox_actionListener();
      }
    });

    _xrayAutoShutdownTimerDataTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        xrayAutoShutdownTimerDataTextField_setValue();
      }
    });

    _xrayAutoShutdownTimerDataTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        xrayAutoShutdownTimerDataTextField_setValue();
      }
    });
    
    _promptSavingRecipeAfterLearningCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        promptSavingRecipeAfterLearningCheckBox_actionListener();
      }
    });
     
       //Prompt warning message if recipe contains high mag- hsia-fen.tan
       _promptWarningMsgIfHighMagRecipeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         promptWarningMsgIfHighMagRecipeCheckBox_actionListener();
      }
    });
       
    //Khaw Chek Hau - XCR3063: Notification Message When Using DRO 
    _promptWarningMsgIfModifyDROLevelCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        promptWarningMsgIfModifyDROLevelCheckBox_actionListener();
      }
    });
       
    //Create history log- hsia-fen.tan, 3 April 2013
      _createHistoryLogCheckBox .addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         createHistoryLogCheckBox_actionListener();
      }
    });
    
    _enableMachineAutoStartup.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        v810_autoStartup_setValue(_enableMachineAutoStartup.isSelected());
      }
    });
    

    _inspectionImageTypePNGRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        inspectionImageTypeRadioButton_actionListener();
      }
    });
  
    _inspectionImageTypeTIFFRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        inspectionImageTypeRadioButton_actionListener();
      }
    });


    // define menus and toolBar items to be added later

//    _setDefaultMenuItem.setText("Set &Default User Config");
//    _setDefaultMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        setDefault(e);
//      }
//    });
//
//    _configHelpMenuItem.setText("&Config User Help");
//    _configHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        configHelp(e);
//      }
//    });
  }
  
  /*
   * @author Kee Chin Seong
   * to set the value to startup the machine
   */
  private void v810_autoStartup_setValue(boolean isEnabled)
  {
    try
    {  
      ConfigSetMachineAutoStartupSchedulerEnabledCommand setMachineAutoStartupSchedulerEnabledCommand = 
                        new ConfigSetMachineAutoStartupSchedulerEnabledCommand(isEnabled);
      _commandManager.execute(setMachineAutoStartupSchedulerEnabledCommand);
      _startupMin.setEnabled(isEnabled);
      _startupHour.setEnabled(isEnabled);
      _userStartupInputMainPanel.setEnabled(isEnabled);
      
      _setSchedulerStartupButton.setEnabled(isEnabled);
      
      if(isEnabled == true)
      {
        if(_mainUI.getSchedulerAlarm() == null)
        {
          _mainUI.setSchedulerState(isEnabled);
        }
        _mainUI.getSchedulerAlarm().scheduleTask(_startupHour.getSelectedIndex(), _startupMin.getSelectedIndex(), 0);
      }
      else
      {
        _mainUI.setSchedulerState(isEnabled);
        //_mainUI.getSchedulerAlarm().scheduleTask(_startupHour.getSelectedIndex(), _startupMin.getSelectedIndex(), 0);
      }    
    }
    catch(XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  private void scheduleStartupMachine_setValue()
  {
    try
    {
      ConfigSetMachineAutoStartupSchedulerTimeCommand setMachineAutoStartupSchedulerTimeCommand = 
                                        new ConfigSetMachineAutoStartupSchedulerTimeCommand(_startupHour.getSelectedIndex(), _startupMin.getSelectedIndex());
      _commandManager.execute(setMachineAutoStartupSchedulerTimeCommand);
      
      //alarmClock = _mainUI.getSchedulerAlarm();
      // Kee, Chin Seong make sure is not null (meaning in home or option enviroment) only schedule it.
      Assert.expect(_mainUI.getSchedulerAlarm() != null);
      _mainUI.getSchedulerAlarm().scheduleTask(_startupHour.getSelectedIndex(), _startupMin.getSelectedIndex(), 0);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    _projectDirectoryTextField.setText(Directory.getProjectsDir());
    _ndfDirectoryTextField.setText(Directory.getLegacyNdfDir());
    _cadDirectoryTextField.setText(Directory.getCadDirectory());
     //Kee Chin Seong - verification image path chooser
    _verificationImageDirectoryField.setText(Directory.getXrayVerificationImagesDirectory());
    
    _resultsDirectoryTextField.setText(Directory.getResultsDir());

    _projectDatabaseDirectoryTextField.setText(Directory.getProjectDatabaseDir());

    _autoPullCheckBox.setSelected(ProjectDatabase.isAutoUpdateLocalProjectEnabled());

    _deleteLocalResultsCheckBox.setSelected(_resultsProcessor.getUsesAutomaticResultDeletion());

    _measurementDataTextField.setValue(_resultsProcessor.getMaximumNumberOfResultsToKeep());
    _repairDataTextField.setValue(_resultsProcessor.getMaximumNumberOfRepairResultsToKeep());

    _measurementDataTextField.setEnabled(_deleteLocalResultsCheckBox.isSelected());
    _repairDataTextField.setEnabled(_deleteLocalResultsCheckBox.isSelected());

    //delete history log automatically - hsia-fen.tan
     _currentUserType=LoginManager.getInstance().getUserTypeFromLogIn();
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _deleteHistoryFileCheckBox.setSelected(_historyLog.getUsesAutomaticHistoryFileDeletion());
    }
    else
    {
      _deleteHistoryFileCheckBox.setSelected(false);
    }
    _historyFileTextField.setValue(_historyLog.getMaximumNumberOfHistoryToKeep());
    _historyFileTextField.setEnabled(_deleteHistoryFileCheckBox.isSelected());
    
    //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
    {
        _xrayAutoShutdownCheckBox.setSelected(_xraySource.getUsesXrayAutoShortTermShutdownEnabled());
        _xrayAutoShutdownTimerDataTextField.setValue(_xraySource.getXrayAutoShortTermShutdownTimeoutMinutes());
     }
    else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
        _xrayAutoShutdownCheckBox.setSelected(_hTubeXraySource.getUsesXrayAutoShortTermShutdownEnabled());
        _xrayAutoShutdownTimerDataTextField.setValue(_hTubeXraySource.getXrayAutoShortTermShutdownTimeoutMinutes());
     }
    _xrayAutoShutdownTimerDataTextField.setEnabled(_xrayAutoShutdownCheckBox.isSelected());
    
    _promptSavingRecipeAfterLearningCheckBox.setSelected(TestExecution.getInstance().isPromptSavingRecipeAfterLearning());
     // Prompt warning message if recipe contain high mag CR- hsia-fen.tan
    _promptWarningMsgIfHighMagRecipeCheckBox.setSelected(TestExecution.getInstance().isPromptWarningMsgIfRecipeContainHighMag());
    
    //Khaw Chek Hau - XCR3063: Notification Message When Using DRO 
    _promptWarningMsgIfModifyDROLevelCheckBox.setSelected(TestExecution.getInstance().isPromptWarningMsgIfModifyDROLevel());
    
    //Only administrator can create history log -hsia-fen.tan
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
      _createHistoryLogCheckBox.setSelected(TestExecution.getInstance().isShowHistoryLog());
    else
      _createHistoryLogCheckBox.setSelected(false);
    
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    _historyLogDirTextField.setText(_config.getStringValue(SoftwareConfigEnum.SPECIFIC_HISTORY_LOG_DIR));
    _useSpecificHistoryLogPathCheckBox.setSelected(TestExecution.getInstance().isUseSpecificHistoryLogDir());
    setEnabledHistoryLogDir(_useSpecificHistoryLogPathCheckBox.isSelected());
    
    if (_config.getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT)  == 0)
    {
        _inspectionImageTypePNGRadioButton.setSelected(true);
     }
    else if (_config.getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT)  == 1)
    {
      _inspectionImageTypeTIFFRadioButton.setSelected(true);
    }

    _startupHour.setSelectedIndex(_config.getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_HOUR));
    _startupMin.setSelectedIndex(_config.getIntValue(SoftwareConfigEnum.V810_AUTO_START_UP_MINUTES));
    
    _enableMachineAutoStartup.setSelected(_config.getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED));
    
    _startupMin.setEnabled(_config.getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED));
    _startupHour.setEnabled(_config.getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED));
    _userStartupInputMainPanel.setEnabled(_config.getBooleanValue(SoftwareConfigEnum.V810_AUTO_START_UP_ENABLED));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void projectDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;

    String fullPath = _projectDirectoryTextField.getText();
    String previousDirPath = Directory.getProjectsDir();
    boolean isProjectLoadedPreviously = Project.isCurrentProjectLoaded();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetProjectDirectoryCommand setProjectDirectoryCommand = new ConfigSetProjectDirectoryCommand(fullPath);
        _commandManager.execute(setProjectDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    _projectDirectoryTextField.setText(Directory.getProjectsDir());
    _inUse = false;
    
    if(previousDirPath.equalsIgnoreCase(fullPath) == false && isProjectLoadedPreviously && Project.isCurrentProjectLoaded() == false )
    {
      if( _mainUI.getMainToolBar().isLastVisitPageConfigEnv())
        _mainUI.getMainToolBar().switchToAXIHome();
      else
        _mainUI.getMainToolBar().recoverToLastVisitPage();
    }  
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void verificationImageDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;

    String fullPath = _verificationImageDirectoryField.getText();    
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetProjectVerificationImageDirectoryCommand setVerificationImageDirectoryCommand = new ConfigSetProjectVerificationImageDirectoryCommand(fullPath);
        _commandManager.execute(setVerificationImageDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
      _verificationImageDirectoryField.setText(Directory.getProjectsDir());
    _inUse = false;
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void verificationImageDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_verificationImageDirectoryField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(),
                               StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _verificationImageDirectoryField.setText(fileName);
      _verificationImageDirectoryField.requestFocus();
      verificationImageDirectoryTextField_setValue();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void projectDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_projectDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(),
                               StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _projectDirectoryTextField.setText(fileName);
      _projectDirectoryTextField.requestFocus();
      projectDirectoryTextField_setValue();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void ndfDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;
    String fullPath = _ndfDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetNdfDirectoryCommand setNdfDirectoryCommand = new ConfigSetNdfDirectoryCommand(fullPath);
        _commandManager.execute(setNdfDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
      _ndfDirectoryTextField.setText(Directory.getLegacyNdfDir());
    _inUse = false;
  }
  
  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void cadDirectoryTextField_setValue()
  {
    if (_inUse)
    {
      return;
    }
    _inUse = true;
    String fullPath = _cadDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetCadDirectoryCommand setCadDirectoryCommand = new ConfigSetCadDirectoryCommand(fullPath);
        _commandManager.execute(setCadDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
          ex,
          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
          true);
      }
    }
    else
    {
      _cadDirectoryTextField.setText(Directory.getCadDirectory());
    }
    _inUse = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void ndfDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_ndfDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _ndfDirectoryTextField.setText(fileName);
      _ndfDirectoryTextField.requestFocus();
    }
  }
  
   /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void cadDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_cadDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
    {
      return;
    }
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _cadDirectoryTextField.setText(fileName);
      _cadDirectoryTextField.requestFocus();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resultsDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;
    String fullPath = _resultsDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetResultsDirectoryCommand setResultsDirectoryCommand = new ConfigSetResultsDirectoryCommand(fullPath);
        _commandManager.execute(setResultsDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
      _resultsDirectoryTextField.setText(Directory.getResultsDir());
    _inUse = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resultsDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_resultsDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _resultsDirectoryTextField.setText(fileName);
      _resultsDirectoryTextField.requestFocus();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void projectDatabaseDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;
    String fullPath = _projectDatabaseDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {

      try
      {
        ConfigSetProjectDatabaseDirectoryCommand setProjectDatabaseDirectoryCommand = new
            ConfigSetProjectDatabaseDirectoryCommand(fullPath);
        _commandManager.execute(setProjectDatabaseDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
      _projectDatabaseDirectoryTextField.setText(Directory.getProjectDatabaseDir());
    _inUse = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void projectDatabaseDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_projectDatabaseDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _projectDatabaseDirectoryTextField.setText(fileName);
      _projectDatabaseDirectoryTextField.requestFocus();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void autoPullCheckBox_actionListener()
  {
    try
    {
      ConfigSetPullProjectCommand setPullProjectCommand = new ConfigSetPullProjectCommand(_autoPullCheckBox.isSelected());
      _commandManager.execute(setPullProjectCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
   /** Create recipe's history log file once load a recipe
   * @author hsia-fen.tan
   */
  private void createHistoryLogCheckBox_actionListener()
  {   
     _currentUserType=LoginManager.getInstance().getUserTypeFromLogIn();
     if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      try
      {
        ConfigSetCreateHistoryLogCommand setCreateHistoryLogCommand = new ConfigSetCreateHistoryLogCommand(_createHistoryLogCheckBox.isSelected());
        _commandManager.execute(setCreateHistoryLogCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                ex,
                StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                true);
      }
    }
    else
    {
      MessageDialog.showInformationDialog(this,
              "Only administrator can create history log!",
              StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY"),
              true);
      updateDataOnScreen();
      return;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteLocalResultsCheckBox_actionListener()
  {
    _measurementDataTextField.setEnabled(_deleteLocalResultsCheckBox.isSelected());
    _repairDataTextField.setEnabled(_deleteLocalResultsCheckBox.isSelected());

    try
    {
      ConfigSetAutoDeleteResultsCommand setAutoDeleteResultsCommand = new ConfigSetAutoDeleteResultsCommand(_deleteLocalResultsCheckBox.isSelected());
      _commandManager.execute(setAutoDeleteResultsCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**Enable to auto delete history file, depend on user want to keep latest {} of files
   * @author hsia-fen.tan
   */
  private void deleteHistoryFileCheckBox_actionListener()
  {
    _currentUserType=LoginManager.getInstance().getUserTypeFromLogIn();
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
    _historyFileTextField.setEnabled(_deleteHistoryFileCheckBox.isSelected());
    try
    {   
       ConfigSetAutoDeleteHistoryLogCommand setAutoDeleteHistoryFileCommand = new ConfigSetAutoDeleteHistoryLogCommand(_deleteHistoryFileCheckBox.isSelected());
      _commandManager.execute(setAutoDeleteHistoryFileCommand);
     }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
    }
  }
    else
    {
         MessageDialog.showInformationDialog(this,
          "Only administrator can delete the file!",
          StringLocalizer.keyToString("HP_PROJECT_HISTORY_LOG_TITLE_KEY"),
          true);
         updateDataOnScreen();
         return; 
        
    }
  }
  
  
  /**
   * @author AnthonyFong
   */
  //CR33099-Auto Short Term Shutdown for Xray- AnthonyFong 19-March-2009
  private void xrayAutoShutdownCheckBox_actionListener()
  {
    _xrayAutoShutdownTimerDataTextField.setEnabled(_xrayAutoShutdownCheckBox.isSelected());

    try
    {
      ConfigSetXrayAutoShortTermShutdownEnableCommand setXrayAutoShortTermShutdownTimeoutCommand = new ConfigSetXrayAutoShortTermShutdownEnableCommand(_xrayAutoShutdownCheckBox.isSelected());
      _commandManager.execute(setXrayAutoShortTermShutdownTimeoutCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  /**
   * @author AnthonyFong
   */
  private void xrayAutoShutdownTimerDataTextField_setValue()
  {
    Double val;
    try
    {
      val = _xrayAutoShutdownTimerDataTextField.getDoubleValue();
    }
    catch (ParseException pex)
    {
      val = new Double(0);
    }

    try
    {
      // Deafault Idle Timeout = 15.0 minutes
      if (val < 15.0)
      {
        // Restore back to last saved value
        if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
        {
            val = _xraySource.getXrayAutoShortTermShutdownTimeoutMinutes();
        }
        else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
        {
            val = _hTubeXraySource.getXrayAutoShortTermShutdownTimeoutMinutes();
        }
        else
        {
            val = 15.0;
        }
        _xrayAutoShutdownTimerDataTextField.setValue(val);
        MessageDialog.showInformationDialog(MainMenuGui.getInstance(),
                                      StringLocalizer.keyToString("CFGUI_XRAY_AUTO_SHUTDOWN_TIMER_DEFAULT_KEY"),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);

      }

      ConfigSetXrayAutoShortTermShutdownTimeoutCommand setXrayAutoShortTermShutdownTimeoutCommand = new ConfigSetXrayAutoShortTermShutdownTimeoutCommand(val.doubleValue());
      _commandManager.execute(setXrayAutoShortTermShutdownTimeoutCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void promptSavingRecipeAfterLearningCheckBox_actionListener()
  {
    try
    {
      ConfigEnableSavingRecipeAfterLearningCommand promptSavingRecipeAfterLearningCommand = new ConfigEnableSavingRecipeAfterLearningCommand(_promptSavingRecipeAfterLearningCheckBox.isSelected());
      _commandManager.execute(promptSavingRecipeAfterLearningCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

     /**Enable to prompt warning message before run alignment
   * @author hsia-fen.tan
   */
  public void promptWarningMsgIfHighMagRecipeCheckBox_actionListener()
  {
    try
    {
      ConfigEnablePromptWarningMsgIfHighMagRecipeCommand promptWarningMsgIfHighMagRecipeCommand = new ConfigEnablePromptWarningMsgIfHighMagRecipeCommand(_promptWarningMsgIfHighMagRecipeCheckBox.isSelected());
      _commandManager.execute(promptWarningMsgIfHighMagRecipeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public void promptWarningMsgIfModifyDROLevelCheckBox_actionListener()
  {
    try
    {
      ConfigEnablePromptWarningMsgIfModifyDROLevelCommand promptWarningMsgIfModifyDROLevelCommand = new ConfigEnablePromptWarningMsgIfModifyDROLevelCommand(_promptWarningMsgIfModifyDROLevelCheckBox.isSelected());
      _commandManager.execute(promptWarningMsgIfModifyDROLevelCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
  }
        
  /**
   * @author Andy Mechtenberg
   */
  private void measurementDataTextField_setValue()
  {
    Long val;
    try
    {
      val = _measurementDataTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(0);
    }

    try
    {
      ConfigSetMeasurementDataRunsCommand setMeasurementDataRunsCommand = new ConfigSetMeasurementDataRunsCommand(val.intValue());
      _commandManager.execute(setMeasurementDataRunsCommand);
    }

    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  //Get current number of history file to delete- hsia-fen.tan, 3 April 2013
    private void historyFileTextField_setValue()
  {
    Long val;
    try
    {
      val = _historyFileTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(0);
    }

    try
    {
      ConfigSetToKeepHistoryFileCommand setHistoryFileCommand = new ConfigSetToKeepHistoryFileCommand(val.intValue());
      _commandManager.execute(setHistoryFileCommand);
    }

    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  /**
   * @author Andy Mechtenberg
   */
  private void repairDataTextField_setValue()
  {
    Long val;
    try
    {
      val = _repairDataTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(0);
    }

    try
    {
      ConfigSetRepairDataRunsCommand setRepairDataRunsCommand = new ConfigSetRepairDataRunsCommand(val.intValue());
      _commandManager.execute(setRepairDataRunsCommand);
    }

    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Wei Chin
   */
  private void inspectionImageTypeRadioButton_actionListener()
  {
    int ImageType = 0;
    if(_inspectionImageTypePNGRadioButton.isSelected())
    {
      ImageType = SoftwareConfigEnum.getPngImageType();
    }
    if(_inspectionImageTypeTIFFRadioButton.isSelected())
    {
      ImageType = SoftwareConfigEnum.getTiffImageType() ;
    }
      
    try
    {
      ConfigSetInspectionImageTypeCommand setInspectionImageTypeCommand = new ConfigSetInspectionImageTypeCommand(ImageType);
      _commandManager.execute(setInspectionImageTypeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("SWOptionsGeneralTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");
    ConfigObservable.getInstance().addObserver(this);
    updateDataOnScreen();
    // add custom menus
//    JMenu configMenu = _envPanel.getConfigMenu();
//    _menuBar.addMenuItem(_menuRequesterID, configMenu, _setDefaultMenuItem);
//
//    JMenu helpMenu = _menuBar.getHelpMenu();
//    _menuBar.addMenuItem(_menuRequesterID, helpMenu, 0, _configHelpMenuItem);
//
//    _menuBar.addMenuSeparator(_menuRequesterID, helpMenu, 1);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return _inUse == false;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("SWOptionsGeneralTabPanel().finish()");
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  private void historyLogDirBrowseButton_actionPerformed(ActionEvent e)
  {
    JFileChooser fileChooser = new JFileChooser(_historyLogDirTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _historyLogDirTextField.setText(fileName);
      _historyLogDirTextField.requestFocus();
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  private void setEnabledHistoryLogDir(boolean enabled)
  {    
    _historyLogDirLabel.setEnabled(enabled);
    _historyLogDirTextField.setEnabled(enabled);
    _historyLogDirBrowseButton.setEnabled(enabled);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  private void useSpecificHistoryLogPathCheckBox_actionListener()
  {
    setEnabledHistoryLogDir(_useSpecificHistoryLogPathCheckBox.isSelected());
    
    try
    {
      ConfigSetUseSpecificHistoryLogDirCommand setUseSpecificHistoryLogDirCommand = new ConfigSetUseSpecificHistoryLogDirCommand(_useSpecificHistoryLogPathCheckBox.isSelected());
      _commandManager.execute(setUseSpecificHistoryLogDirCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
              ex,
              StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
              true);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  private void historyLogDirTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;
    String fullPath = _historyLogDirTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if (_envPanel.isPathValid(fullPath))
    {
      try
      {
        ConfigSetHistoryLogDirectoryCommand setHistoryLogDirectoryCommand = new ConfigSetHistoryLogDirectoryCommand(fullPath);
        _commandManager.execute(setHistoryLogDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
      _historyLogDirTextField.setText(_config.getStringValue(SoftwareConfigEnum.SPECIFIC_HISTORY_LOG_DIR));
    _inUse = false;
  }
}
