package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
//import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;


/**
 * @author Andy Mechtenberg
 */
public class ResultsProcessingProductionTuneTabPanel extends AbstractTaskPanel implements Observer
{
  private ConfigEnvironment _envPanel = null;
  // production tuning screen controls
  private BorderLayout _taskPanelLayout = new BorderLayout();  // remove -- only needed for Designer

  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelLayout = new BorderLayout();
  private JPanel _defaultLocationPanel = new JPanel();
  private BorderLayout _defaultLocationPanelLayout = new BorderLayout();
  private JPanel _locationPanel = new JPanel();
  private FlowLayout _locationPanelLayout = new FlowLayout(FlowLayout.LEFT);

  private JLabel _defaultLocationLabel = new JLabel(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_DEFAULT_DESTINATION_KEY"));
  private JTextField _defaultLocationTextField = new JTextField(30);
  private JButton _quickAddButton = new JButton();
  private JPanel _mainTablePanel = new JPanel();
  private JScrollPane _productionTuneTableScrollPane = new JScrollPane();
  private BorderLayout _mainTablePanelLayout = new BorderLayout();
  private ProductionTuneTable _productionTuneTable = new ProductionTuneTable();
  private ProductionTuneTableModel _productionTuneTableModel;
  private JPanel _addDeleteButtonPanel = new JPanel();
  private FlowLayout _addDeleteButtonPanelLayout = new FlowLayout();
  private JButton _addRowButton = new JButton();
  private JButton _deleteRowButton = new JButton();
  private Border _empty10Border = BorderFactory.createEmptyBorder(10, 10, 10, 10);
  private Border _empty5Border = BorderFactory.createEmptyBorder(0, 5, 0, 5);
  private JPanel _addDeleteButtonInnerPanel = new JPanel();
  private GridLayout _addDeleteButtonInnerPanelLayout = new GridLayout();

  private ProductionTuneManager _productionTuneManager = ProductionTuneManager.getInstance();
  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // Wei Chin added
  private JButton _productionTuneSettingsButton = new JButton();
  private ProductionTuneSettingsDialog _productionTuneSettingsDialog = null;
  private JPanel _northPanel = new JPanel();

  /**
   * Useful ONLY for the JBuilder designer.  Not for use with normal code.
   * @author Laura Cormos
   */
  private ResultsProcessingProductionTuneTabPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    try
    {

      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public ResultsProcessingProductionTuneTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("SWOptionsGeneralTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    ConfigObservable.getInstance().addObserver(this);
    ProjectObservable.getInstance().addObserver(this);
    _productionTuneTableModel.clearData();
    refreshData();
    _productionTuneTable.setPreferredColumnWidths();
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    // make sure each entry has a non-empty project name
    java.util.List<ProductionTuneData> allData = _productionTuneTableModel.getData();
    for(ProductionTuneData pda : allData)
    {
      if (pda.getProjectName().length() == 0)
      {
        // give error -- empty project not allowed
        MessageDialog.showErrorDialog(_mainUI,
                                      StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_PROJECT_MISSING_KEY"),
                                      StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);

        return false;
      }
      if (pda.getImageDestination().length() == 0)
      {
        // give error -- empty project not allowed
        MessageDialog.showErrorDialog(_mainUI,
                                      StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_IMAGE_DEST_MISSING_KEY"),
                                      StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);

        return false;
      }
      if (pda.getResultsDestination().length() == 0)
      {
        // give error -- empty project not allowed
        MessageDialog.showErrorDialog(_mainUI,
                                      StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_RESULTS_DEST_MISSING_KEY"),
                                      StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);

        return false;
      }


    }
    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    ProjectObservable.getInstance().deleteObserver(this);
    try
    {
      _productionTuneManager.save();
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
    }

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("SWOptionsGeneralTabPanel().finish()");
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProjectObservable)
        {
          handleProjectObservable(arg);
        }
        else if (observable instanceof ConfigObservable)
        {
          handleConfigObservable(arg);
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleProjectObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof ProjectChangeEvent)
    {
      ProjectChangeEvent event = (ProjectChangeEvent)arg;
      ProjectChangeEventEnum eventEnum = event.getProjectChangeEventEnum();
      if (eventEnum instanceof ProjectEventEnum)
      {
        ProjectEventEnum projectEventEnum = (ProjectEventEnum)eventEnum;

        if (projectEventEnum.equals(ProjectEventEnum.FAST_LOAD) ||
            projectEventEnum.equals(ProjectEventEnum.SLOW_LOAD) ||
            projectEventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS) ||
            projectEventEnum.equals(ProjectEventEnum.UNLOAD))
        {
          _quickAddButton.setEnabled(Project.isCurrentProjectLoaded());
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleConfigObservable(Object arg)
  {
    Assert.expect(arg != null);
    if (arg instanceof ProductionTuneData)
    {
      ProductionTuneData productionTuneData = (ProductionTuneData)arg;
      int rowToSelect = _productionTuneTableModel.getIndexOf(productionTuneData);
      refreshData();
      _productionTuneTable.addRowSelectionInterval(rowToSelect, rowToSelect);
    }
    else if (arg instanceof ProductionTuneDataWriter)
    {
      // this indicates a default destination change
      try
      {
        _defaultLocationTextField.setText(_productionTuneManager.getDefaultLocation());
      }
      catch (DatastoreException ex)
      {
        MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      }
    }
    else if (arg instanceof ProductionTuneManager)
    {
      ProductionTuneData lastData = _productionTuneManager.getLastOperationData();

      if (_productionTuneManager.wasLastOperationAdd())
      {
        // add operation
        refreshData();
        int rowToSelect = _productionTuneTableModel.getIndexOf(lastData);
        _productionTuneTable.getSelectionModel().addSelectionInterval(rowToSelect, rowToSelect);
      }
      else
      {
        // remove operation
        int lastDataRowNumber = _productionTuneTableModel.getIndexOf(lastData);  // 0 based
        refreshData();
        int tableRowCount = _productionTuneTableModel.getRowCount();
        if (lastDataRowNumber < tableRowCount)
          _productionTuneTable.getSelectionModel().addSelectionInterval(lastDataRowNumber, lastDataRowNumber);
        else
          _productionTuneTable.getSelectionModel().addSelectionInterval(tableRowCount-1, tableRowCount-1);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    this.setLayout(_taskPanelLayout);
    this.setBorder(_taskPanelBorder);
    _mainTablePanel.setLayout(_mainTablePanelLayout);
    _addDeleteButtonPanel.setLayout(_addDeleteButtonPanelLayout);
    _addDeleteButtonPanelLayout.setAlignment(FlowLayout.RIGHT);
    _addRowButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _addRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addRowButton_actionPerformed();
      }
    });
    _deleteRowButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _deleteRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRowButton_actionPerformed();
      }
    });
    _deleteRowButton.setEnabled(false);
    _mainTablePanel.setBorder(_empty10Border);
    _defaultLocationPanel.setBorder(_empty5Border);
    _addDeleteButtonPanel.setBorder(_empty5Border);
    _quickAddButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        quickAddButton_actionPerformed();
      }
    });
    _addDeleteButtonInnerPanel.setLayout(_addDeleteButtonInnerPanelLayout);
    _addDeleteButtonInnerPanelLayout.setHgap(5);
    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelLayout);
    _defaultLocationPanel.setLayout(_defaultLocationPanelLayout);

    _locationPanel.setLayout(_locationPanelLayout);
    _locationPanel.add(_defaultLocationLabel);
    _locationPanel.add(_defaultLocationTextField);
    _defaultLocationTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        defaultLocationTextField_setValue();
      }
    });
    _defaultLocationTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defaultLocationTextField_setValue();
      }
    });

    _locationPanel.add(_quickAddButton);

    _quickAddButton.setText(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_ADD_CURRENT_PROJECT_KEY"));

    _productionTuneSettingsButton.setText(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_DIALOG_KEY"));
    _productionTuneSettingsButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        productionTuneSettingsButton_actionPerformed(e);
      }
    });
    
    _locationPanel.add(_productionTuneSettingsButton);

    _defaultLocationPanel.add(_locationPanel, BorderLayout.WEST);
    _centerPanel.add(_mainTablePanel, java.awt.BorderLayout.CENTER);
    _mainTablePanel.add(_productionTuneTableScrollPane, java.awt.BorderLayout.CENTER);
    _centerPanel.add(_defaultLocationPanel, java.awt.BorderLayout.NORTH);
    _centerPanel.add(_addDeleteButtonPanel, java.awt.BorderLayout.SOUTH);
    _addDeleteButtonPanel.add(_addDeleteButtonInnerPanel);
    _addDeleteButtonInnerPanel.add(_addRowButton);
    _addDeleteButtonInnerPanel.add(_deleteRowButton);
    _productionTuneTableScrollPane.getViewport().add(_productionTuneTable);
    _productionTuneTableModel = new ProductionTuneTableModel(_envPanel);
    _productionTuneTable.setModel(_productionTuneTableModel);

    _productionTuneTable.setEditorsAndRenderers();
    final ListSelectionModel matchTableRowSelectionModel = _productionTuneTable.getSelectionModel();
    matchTableRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
        if (listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else if (listSelectionModel.isSelectionEmpty())
        {
          _deleteRowButton.setEnabled(false);
        }
        else
        {
          int selectedRow = _productionTuneTable.getSelectedRow();
          _productionTuneTableModel.setSelectedRow(selectedRow);
          _deleteRowButton.setEnabled(selectedRow >= 0);
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void refreshData()
  {
    try
    {
      _defaultLocationTextField.setText(_productionTuneManager.getDefaultLocation());
      Collection<ProductionTuneData> productionTuneData = _productionTuneManager.getProductionTuneData();
      boolean isSelectedRow = _productionTuneTableModel.isSelectedRow();
      if (isSelectedRow)
      {
        ProductionTuneData ptd = _productionTuneTableModel.getSelectedRowProductionTuneData();

        _productionTuneTableModel.setData(productionTuneData);

        if (_productionTuneTableModel.contains(ptd))
        {
          final int selectedRow = _productionTuneTableModel.getIndexOf(ptd);
          if (selectedRow >= 0) // still need this check as the row may have been removed
          {
            // have to do an invokeLater, as the fire event in the table model hasn't done it's job yet re-ordering the table
            // so put this at the end of the swing queue so it actually works.
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _productionTuneTable.addRowSelectionInterval(selectedRow, selectedRow);
                SwingUtils.scrollTableToShowSelection(_productionTuneTable);
              }
            });
          }
        }
      }
      else
        _productionTuneTableModel.setData(productionTuneData);
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      _defaultLocationTextField.setText("");
      _productionTuneTableModel.clearData();
    }
    updateDataOnScreen();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    _quickAddButton.setEnabled(Project.isCurrentProjectLoaded());

    _productionTuneTableModel.fireTableDataChanged();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void addRowButton_actionPerformed()
  {
    try
    {
      String defaultDestination = _productionTuneManager.getDefaultLocation();
      String projectDestination;
      String resultsDestination;
      if (defaultDestination.length() == 0)
      {
        projectDestination = Directory.getProjectsDir();
        resultsDestination = Directory.getResultsDir();
      }
      else
      {
        projectDestination = Directory.getProductionTuneProjectsDir(defaultDestination);
        resultsDestination = Directory.getProductionTuneResultsDir(defaultDestination);
      }
      _commandManager.execute(new ConfigAddProductionTuneDataCommand(new ProductionTuneData("",
                                                                                            projectDestination,
                                                                                            resultsDestination,
                                                                                            50,
                                                                                            500,
                                                                                            10,
                                                                                            "")));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);

    }

//    javax.swing.table.TableColumnModel columnModel = _productionTuneTable.getColumnModel();
//
//    int totalColumnWidth = columnModel.getTotalColumnWidth();
//    int one = columnModel.getColumn(0).getWidth();
//    int two = columnModel.getColumn(1).getWidth();
//    int three = columnModel.getColumn(2).getWidth();
//    int four = columnModel.getColumn(3).getWidth();
//    int five = columnModel.getColumn(4).getWidth();
//    int six = columnModel.getColumn(5).getWidth();
//    int seven = columnModel.getColumn(6).getWidth();
//
//    int total = columnModel.getTotalColumnWidth();
//    System.out.println("Total: " + total);
//    System.out.println("column 1: " + (double)one/total);
//    System.out.println("column 2: " + (double)two/total);
//    System.out.println("column 3: " + (double)three/total);
//    System.out.println("column 4: " + (double)four/total);
//    System.out.println("column 5: " + (double)five/total);
//    System.out.println("column 6: " + (double)six/total);
//    System.out.println("column 7: " + (double)seven/total);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void deleteRowButton_actionPerformed()
  {
    // testing code
//    try
//    {
//      _productionTuneManager.sendNotificationEmail(_productionTuneTableModel.getSelectedRowProductionTuneData(), "123456", 567);
//    }
//    catch (Exception ex1)
//    {
//      Assert.logException(ex1);
//    }

    try
    {
      _commandManager.execute(new ConfigRemoveProductionTuneDataCommand(_productionTuneTableModel.getSelectedRowProductionTuneData()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void defaultLocationTextField_setValue()
  {
    String oldDefaultDestination = "";
    try
    {
      oldDefaultDestination = _productionTuneManager.getDefaultLocation();
    }
    catch (DatastoreException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
      return;
    }
    String newDefaultDestination = _defaultLocationTextField.getText();
    newDefaultDestination = _defaultLocationTextField.getText();
    newDefaultDestination = FileUtilAxi.removeEndFileSeparator(newDefaultDestination);

    try
    {
      _commandManager.execute(new ConfigSetDefaultProductionTuneDestinationCommand(newDefaultDestination, oldDefaultDestination));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void quickAddButton_actionPerformed()
  {
    String currentProjectName = Project.getCurrentlyLoadedProject().getName();
    if (_productionTuneTableModel.containsProject(currentProjectName))
    {
      // this project already exists -- don't try to add it, just select it in the table
      int row = _productionTuneTableModel.getRowForProject(currentProjectName);
      _productionTuneTable.addRowSelectionInterval(row, row);
    }
    else
    {
      try
      {
        String defaultDestination = _productionTuneManager.getDefaultLocation();
        String projectDestination;
        String resultsDestination;
        if (defaultDestination.length() == 0)
        {
          projectDestination = Directory.getProjectsDir();
          resultsDestination = Directory.getResultsDir();
        }
        else
        {
          projectDestination = Directory.getProductionTuneProjectsDir(defaultDestination);
          resultsDestination = Directory.getProductionTuneResultsDir(defaultDestination);
        }
        _commandManager.execute(new ConfigAddProductionTuneDataCommand(new ProductionTuneData(currentProjectName,
                                                                                              projectDestination,
                                                                                              resultsDestination,
                                                                                              50,
                                                                                              500,
                                                                                              10,
                                                                                              "")));
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(_mainUI,
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                      true);
      }
    }
  }

  /**
   *  @param e 
   *  @author Wei Chin
   */
  void productionTuneSettingsButton_actionPerformed(ActionEvent e)
  {
    if(_productionTuneSettingsDialog == null)
    {
      _productionTuneSettingsDialog = new ProductionTuneSettingsDialog(_mainUI, 
        StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_SETTING_DIALOG_KEY"));
      SwingUtils.centerOnComponent(_productionTuneSettingsDialog, _mainUI);
    }
    _productionTuneSettingsDialog.populateData();
    _productionTuneSettingsDialog.setVisible(true);
  }
}

