package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ProductionOptionsSerialNumberTabPanel extends AbstractTaskPanel implements Observer
{
  private final int _TO_MATCH = 0;
  private final int _TO_SKIP = 1;
  private final int _TO_VALIDATE = 2;

  private ConfigEnvironment _envPanel = null;
  private SerialNumberManager _serialNumberManager;
  private boolean _serialNumbersToMatchDataIsDirty = false;
  private boolean _serialNumbersToSkipDataIsDirty = false;
  private boolean _serialNumbersToValidateDataIsDirty = false;

  private boolean _loadingInProgress = false;
  private boolean _addRowInProgress = false;
  private boolean _deleteRowInProgress = false;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  // GUI components
  private JPanel _centerPanel = new JPanel();
  private JPanel _tablesPanel = new JPanel();
  private JPanel _serialNumberMatchingPanel = new JPanel();
  private JPanel _serialNumberMatchingCheckboxPanel = new JPanel();
  private JScrollPane _serialNumberMatchScrollPane = new JScrollPane();
  private SerialNumberMatchingTable _matchedSerialNumberTable;
  private SerialNumberMatchingTableModel _matchedSerialNumberTableModel;
  private JPanel _serialNumberMatchingActionsWrapperPanel = new JPanel();
  private JPanel _serialNumberMatchingBottomPanel = new JPanel();
  private JCheckBox _allowOnlyNewSerialNumbersCheckbox = new JCheckBox();
  private JPanel _serialNumberMatchingActionsPanel = new JPanel();
  private JButton _serialNumberMatchingAddButton = new JButton();
  private JButton _serialNumberMatchingDeleteButton = new JButton();
  private JPanel _radioButtonPanel = new JPanel();

  private JPanel _serialNumberSkipAndValidatePanel = new JPanel();
  private JPanel _serialNumberSkipPanel = new JPanel();
  private JCheckBox _skipSerialNumbersCheckbox = new JCheckBox();
  private JPanel _saveSkipSerialNumbersPanel = new JPanel();
  private JScrollPane _serialNumberSkipScrollPane = new JScrollPane();
  private SerialNumberSkipOrValidateTable _skippedSerialNumberTable;
  private SerialNumberSkipOrValidateTableModel _skippedSerialNumberTableModel;
  private JPanel _serialNumberSkipActionsWrapperPanel = new JPanel();
  private JPanel _serialNumberSkipActionsPanel = new JPanel();
  private JButton _serialNumberSkipAddButton = new JButton();
  private JButton _serialNumberSkipDeleteButton = new JButton();
  private JPanel _serialNumberValidatePanel = new JPanel();
  private JCheckBox _validateSerialNumbersCheckbox = new JCheckBox();
  private JPanel _saveValidateSerialNumbersPanel = new JPanel();
  private JScrollPane _serialNumberValidateScrollPane = new JScrollPane();
  private SerialNumberSkipOrValidateTable _validatedSerialNumberTable;
  private SerialNumberSkipOrValidateTableModel _validatedSerialNumberTableModel;
  private JPanel _serialNumberValidateActionsWrapperPanel = new JPanel();
  private JPanel _serialNumberValidateActionsPanel = new JPanel();
  private JButton _serialNumberValidateAddButton = new JButton();
  private JButton _serialNumberValidateDeleteButton = new JButton();

  private JPanel _serialNumberOptionsPanel = new JPanel();
  private JPanel _generalSerialNumberOptionsPanel = new JPanel();
  private JCheckBox _useBoardSerialNumberCheckbox = new JCheckBox();
  private JCheckBox _enablePreSelectPanelCheckbox = new JCheckBox();
  private JCheckBox _checkForDuplicateSerialNumberCheckbox = new JCheckBox();
  private JPanel _testExecSerialNumberOptionsPanel = new JPanel();
  private JCheckBox _displayLastCADImageInQueueCheckbox = new JCheckBox();
  private JCheckBox _checkForAutoBarcodeReadErrorCheckbox = new JCheckBox();
  
  private JCheckBox _enablePatternMatchingCheckbox = new JCheckBox();
  private ButtonGroup _radioButtonGroup = new ButtonGroup();
  private JRadioButton _useSerialNumberToFindProjectRadioButton = new JRadioButton();
  private JRadioButton _useFirstSerialNumberRadioButton = new JRadioButton();

  /**
   * @author Laura Cormos
   */
  public ProductionOptionsSerialNumberTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
      _serialNumberManager = SerialNumberManager.getInstance();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    // set up panel GUI components
    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(new BorderLayout());
    _centerPanel.add(_tablesPanel, BorderLayout.CENTER);
//    _centerPanel.add(_snMatchingPanel);
//    _centerPanel.add(_snSkipAndValidatePanel);
    _centerPanel.add(_serialNumberOptionsPanel, BorderLayout.SOUTH);

    _tablesPanel.setLayout(new GridLayout(2, 0));
    _tablesPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
    _tablesPanel.add(_serialNumberMatchingPanel);
    _tablesPanel.add(_serialNumberSkipAndValidatePanel);

    _serialNumberMatchingPanel.setLayout(new BorderLayout());
    _serialNumberMatchingPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_SN_MATCHING_PANEL_TITLE_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));

    _serialNumberMatchingPanel.add(_serialNumberMatchingCheckboxPanel, BorderLayout.NORTH);
    _serialNumberMatchingPanel.add(_serialNumberMatchScrollPane, BorderLayout.CENTER);
    _serialNumberMatchingPanel.add(_serialNumberMatchingBottomPanel, BorderLayout.SOUTH);

    _radioButtonGroup.add(_useSerialNumberToFindProjectRadioButton);
    _radioButtonGroup.add(_useFirstSerialNumberRadioButton);
    _radioButtonPanel.setLayout(new BorderLayout());
    _radioButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
    _radioButtonPanel.add(_useSerialNumberToFindProjectRadioButton, BorderLayout.NORTH);
    _radioButtonPanel.add(_useFirstSerialNumberRadioButton, BorderLayout.SOUTH);
    
    _serialNumberMatchingCheckboxPanel.setLayout(new BorderLayout());
    _serialNumberMatchingCheckboxPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
    _serialNumberMatchingCheckboxPanel.add(_enablePatternMatchingCheckbox, BorderLayout.NORTH);
    _serialNumberMatchingCheckboxPanel.add(_radioButtonPanel, BorderLayout.SOUTH);
    
    _enablePatternMatchingCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_ENABLE_PATTERN_MATCHING_LABEL_KEY"));
    _enablePatternMatchingCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enablePatternMatchingCheckbox_actionPerformed(e);
      }
    });

    _useSerialNumberToFindProjectRadioButton.setText(StringLocalizer.keyToString("CFGUI_SN_USE_SN_TO_FIND_PROJECT_LABEL_KEY"));
    _useSerialNumberToFindProjectRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        useSerialNumberToFindProjectRadioButton_actionPerformed(e);
      }
    });
    
    _useFirstSerialNumberRadioButton.setText(StringLocalizer.keyToString("CFGUI_SN_USE_FIRST_SN_FOR_CAD_ONLY_LABEL_KEY"));
    _useFirstSerialNumberRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        useFirstSerialNumberRadioButton_actionPerformed(e);
      }
    });

    _matchedSerialNumberTable = new SerialNumberMatchingTable();
    _matchedSerialNumberTableModel = new SerialNumberMatchingTableModel(this, _TO_MATCH);
    _matchedSerialNumberTable.setModel(_matchedSerialNumberTableModel);
    _matchedSerialNumberTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _matchedSerialNumberTable.getTableHeader().setReorderingAllowed(false);
    final ListSelectionModel matchTableRowSelectionModel = _matchedSerialNumberTable.getSelectionModel();
    matchTableRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
        //        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = _matchedSerialNumberTable.getSelectedRow();
          _matchedSerialNumberTableModel.setSelectedRow(selectedRow);
          setDeleteButtonBasedOnTableSelection(_TO_MATCH, selectedRow);
        }
      }
    });

    _serialNumberMatchScrollPane.getViewport().add(_matchedSerialNumberTable);

    _serialNumberMatchingBottomPanel.setLayout(new BorderLayout());
    _serialNumberMatchingBottomPanel.add(_serialNumberMatchingActionsWrapperPanel, BorderLayout.EAST);



    _serialNumberMatchingActionsWrapperPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    _serialNumberMatchingActionsWrapperPanel.add(_serialNumberMatchingActionsPanel);

    _serialNumberMatchingActionsPanel.setLayout(new GridLayout(0, 2, 10, 10));
    _serialNumberMatchingActionsPanel.add(_serialNumberMatchingAddButton);
    _serialNumberMatchingActionsPanel.add(_serialNumberMatchingDeleteButton);

    _serialNumberMatchingAddButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _serialNumberMatchingAddButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addRowButton_actionPerformed(_TO_MATCH);
      }
    });

    _serialNumberMatchingDeleteButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _serialNumberMatchingDeleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRowButton_actionPerformed(_TO_MATCH);
      }
    });

    _serialNumberSkipAndValidatePanel.setLayout(new GridLayout(0, 2));
    _serialNumberSkipAndValidatePanel.add(_serialNumberSkipPanel);
    _serialNumberSkipAndValidatePanel.add(_serialNumberValidatePanel);

    _serialNumberSkipPanel.setLayout(new BorderLayout());
    _serialNumberSkipPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_SN_SKIPPING_PANEL_TITLE_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _serialNumberSkipPanel.add(_saveSkipSerialNumbersPanel, BorderLayout.NORTH);
    _serialNumberSkipPanel.add(_serialNumberSkipScrollPane, BorderLayout.CENTER);
    _serialNumberSkipPanel.add(_serialNumberSkipActionsWrapperPanel, BorderLayout.SOUTH);

    _saveSkipSerialNumbersPanel.setLayout(new BorderLayout());
    _saveSkipSerialNumbersPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
    _saveSkipSerialNumbersPanel.add(_skipSerialNumbersCheckbox, BorderLayout.WEST);

    _skipSerialNumbersCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_SKIP_LABEL_KEY"));
    _skipSerialNumbersCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableSerialNumberSkippingCheckbox_actionPerformed(e);
      }
    });

    _skippedSerialNumberTable = new SerialNumberSkipOrValidateTable();
    _skippedSerialNumberTableModel = new SerialNumberSkipOrValidateTableModel(this, _TO_SKIP);
    _skippedSerialNumberTable.setModel(_skippedSerialNumberTableModel);
    _skippedSerialNumberTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _skippedSerialNumberTable.getTableHeader().setReorderingAllowed(false);
    final ListSelectionModel skipTableRowSelectionModel = _skippedSerialNumberTable.getSelectionModel();
    skipTableRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
        //        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = _skippedSerialNumberTable.getSelectedRow();
          _skippedSerialNumberTableModel.setSelectedRow(selectedRow);
          setDeleteButtonBasedOnTableSelection(_TO_SKIP, selectedRow);
        }
      }
    });

    _serialNumberSkipScrollPane.getViewport().add(_skippedSerialNumberTable);

    _serialNumberSkipActionsWrapperPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    _serialNumberSkipActionsWrapperPanel.add(_serialNumberSkipActionsPanel);

    _serialNumberSkipActionsPanel.setLayout(new GridLayout(0, 2, 10, 10));
    _serialNumberSkipActionsPanel.add(_serialNumberSkipAddButton);
    _serialNumberSkipActionsPanel.add(_serialNumberSkipDeleteButton);

    _serialNumberSkipAddButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _serialNumberSkipAddButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addRowButton_actionPerformed(_TO_SKIP);
      }
    });

    _serialNumberSkipDeleteButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _serialNumberSkipDeleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRowButton_actionPerformed(_TO_SKIP);
      }
    });

    _serialNumberValidatePanel.setLayout(new BorderLayout());
    _serialNumberValidatePanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_SN_VALIDATION_PANEL_TITLE_KEY")),
        BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _serialNumberValidatePanel.add(_saveValidateSerialNumbersPanel, BorderLayout.NORTH);
    _serialNumberValidatePanel.add(_serialNumberValidateScrollPane, BorderLayout.CENTER);
    _serialNumberValidatePanel.add(_serialNumberValidateActionsWrapperPanel, BorderLayout.SOUTH);

    _saveValidateSerialNumbersPanel.setLayout(new BorderLayout());
    _saveValidateSerialNumbersPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
    _saveValidateSerialNumbersPanel.add(_validateSerialNumbersCheckbox, BorderLayout.WEST);

    _validateSerialNumbersCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_VALIDATE_LABEL_KEY"));
    _validateSerialNumbersCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableSerialNumberValidationCheckbox_actionPerformed(e);
      }
    });

    _validatedSerialNumberTable = new SerialNumberSkipOrValidateTable();
    _validatedSerialNumberTableModel = new SerialNumberSkipOrValidateTableModel(this, _TO_VALIDATE);
    _validatedSerialNumberTable.setModel(_validatedSerialNumberTableModel);
    _validatedSerialNumberTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _validatedSerialNumberTable.getTableHeader().setReorderingAllowed(false);
    final ListSelectionModel validateTableRowSelectionModel = _validatedSerialNumberTable.getSelectionModel();
    validateTableRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
        //        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = _validatedSerialNumberTable.getSelectedRow();
          _validatedSerialNumberTableModel.setSelectedRow(selectedRow);
          setDeleteButtonBasedOnTableSelection(_TO_VALIDATE, selectedRow);
        }
      }
    });

    _serialNumberValidateScrollPane.getViewport().add(_validatedSerialNumberTable);

    _serialNumberValidateActionsWrapperPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    _serialNumberValidateActionsWrapperPanel.add(_serialNumberValidateActionsPanel);

    _serialNumberValidateActionsPanel.setLayout(new GridLayout(0, 2, 10, 10));
    _serialNumberValidateActionsPanel.add(_serialNumberValidateAddButton);
    _serialNumberValidateActionsPanel.add(_serialNumberValidateDeleteButton);

    _serialNumberValidateAddButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _serialNumberValidateAddButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addRowButton_actionPerformed(_TO_VALIDATE);
      }
    });

    _serialNumberValidateDeleteButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _serialNumberValidateDeleteButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRowButton_actionPerformed(_TO_VALIDATE);
      }
    });

    _serialNumberOptionsPanel.setLayout(new BorderLayout());
    JPanel _generalSerialNumberOptionsWrapperPanel = new JPanel();
    _serialNumberOptionsPanel.add(_generalSerialNumberOptionsWrapperPanel, BorderLayout.WEST);
    JPanel _testExecSerialNumberOptionsWrapperPanel = new JPanel();
    _serialNumberOptionsPanel.add(_testExecSerialNumberOptionsWrapperPanel, BorderLayout.CENTER);

    _generalSerialNumberOptionsWrapperPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _generalSerialNumberOptionsWrapperPanel.add(_generalSerialNumberOptionsPanel);

    _generalSerialNumberOptionsPanel.setLayout(new PairLayout(5, 2));
    _generalSerialNumberOptionsPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_SN_GENERAL_OPTIONS_PANEL_TITLE_KEY")),
        BorderFactory.createEmptyBorder()));
    _generalSerialNumberOptionsPanel.add(_useBoardSerialNumberCheckbox);
    _generalSerialNumberOptionsPanel.add(_allowOnlyNewSerialNumbersCheckbox);
    _generalSerialNumberOptionsPanel.add(_checkForDuplicateSerialNumberCheckbox);
    _generalSerialNumberOptionsPanel.add(_enablePreSelectPanelCheckbox);
    _generalSerialNumberOptionsPanel.add(new JLabel(" ")); // fake component to make the 5th checkbox show up

    _allowOnlyNewSerialNumbersCheckbox.setText(StringLocalizer.keyToString(
        "CFGUI_SN_ALLOW_ONLY_NEW_SN_LABEL_KEY"));
    _allowOnlyNewSerialNumbersCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        allowOnlyNewSerialNumbersCheckbox_actionPerformed(e);
      }
    });


    _useBoardSerialNumberCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_USE_BOARD_SN_LABEL_KEY"));
    _useBoardSerialNumberCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        useBoardSerialNumberCheckbox_actionPerformed(e);
      }
    });

    _enablePreSelectPanelCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_PRESELECT_PANEL_LABEL_KEY"));
    _enablePreSelectPanelCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enablePreSelectPanelCheckbox_actionPerformed(e);
      }
    });

    _checkForDuplicateSerialNumberCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_CHECK_DUPLICATE_SN_LABEL_KEY"));
    _checkForDuplicateSerialNumberCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        checkForDuplicateSerialNumberCheckbox_actionPerformed(e);
      }
    });

    _testExecSerialNumberOptionsWrapperPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _testExecSerialNumberOptionsWrapperPanel.add(_testExecSerialNumberOptionsPanel);

    _testExecSerialNumberOptionsPanel.setLayout(new VerticalFlowLayout(VerticalFlowLayout.LEFT, VerticalFlowLayout.TOP,
                                                                       1, 1));
    _testExecSerialNumberOptionsPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                         StringLocalizer.keyToString("CFGUI_SN_TEST_EXECUTION_OPTIONS_PANEL_TITLE_KEY")),
        BorderFactory.createEmptyBorder()));
    _testExecSerialNumberOptionsPanel.add(_displayLastCADImageInQueueCheckbox);
    _testExecSerialNumberOptionsPanel.add(_checkForAutoBarcodeReadErrorCheckbox);
    _testExecSerialNumberOptionsPanel.add(new JLabel(" ")); // fake label to make this panel line up with the panel to the left

    _displayLastCADImageInQueueCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_DISPLAY_LAST_CAD_IMAGE_LABEL_KEY"));
    _displayLastCADImageInQueueCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayLastCADImageCheckbox_actionPerformed(e);
      }
    });

    _checkForAutoBarcodeReadErrorCheckbox.setText(StringLocalizer.keyToString("CFGUI_SN_CHECK_AUTOMATIC_BCR_READ_ERROR_LABEL_KEY"));
    _checkForAutoBarcodeReadErrorCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableAutoCheckForBarcodeReadErrorCheckbox_actionPerformed(e);
      }
    });

  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ProductionOptionsSerialNumberTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");

    try
    {
      // add custom menus
      populateWithConfigData();
    }
    catch (DatastoreException ex)
    {
      _mainUI.handleXrayTesterException(ex);
    }

    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   * @author Laura Cormos
   */
  public boolean isReadyToFinish()
  {
    Collection<SerialNumberRegularExpressionData> allSerialNumbers = new ArrayList<SerialNumberRegularExpressionData>();
    Collection<SerialNumberRegularExpressionData> serialNumbersToMatch = null;
    Collection<SerialNumberRegularExpressionData> serialNumbersToSkip = null;
    Collection<SerialNumberRegularExpressionData> serialNumbersToValidate = null;

    // first check that if the validate SN checkbox is checked, there are valid patterns as well
    if (_validatedSerialNumberTableModel.getData().size() == 0 && _validateSerialNumbersCheckbox.isSelected())
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("CFGUI_SN_MISSING_VALID_SN_PATTERNS_MESSAGE_KEY"),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // gather all the serial number regex patterns
    if (_serialNumbersToMatchDataIsDirty)
    {
      cancelEditingIfNecessary(_matchedSerialNumberTable);
      serialNumbersToMatch = _matchedSerialNumberTableModel.getData();
      Assert.expect(serialNumbersToMatch != null);

      allSerialNumbers.addAll(serialNumbersToMatch);
    }
    if (_serialNumbersToSkipDataIsDirty)
    {
      cancelEditingIfNecessary(_skippedSerialNumberTable);
      serialNumbersToSkip = _skippedSerialNumberTableModel.getData();
      Assert.expect(serialNumbersToSkip != null);

      allSerialNumbers.addAll(serialNumbersToSkip);
    }
    if (_serialNumbersToValidateDataIsDirty)
    {
      cancelEditingIfNecessary(_validatedSerialNumberTable);
      serialNumbersToValidate = _validatedSerialNumberTableModel.getData();
      Assert.expect(serialNumbersToValidate != null);

      allSerialNumbers.addAll(serialNumbersToValidate);
    }

    // check for errors for patterns and missing projects (if any)
    if (_serialNumbersToMatchDataIsDirty)
    {
      if (areRegExSerialNumberPatternsValidAndProjectNamesSet(allSerialNumbers, serialNumbersToMatch) == false)
        return false;
    }
    else
    {
      if (areRegExSerialNumberPatternsValid(allSerialNumbers) == false)
        return false;
    }

    // save changes if everything is good
    try
    {
      if (_serialNumbersToMatchDataIsDirty)
      {
        Assert.expect(serialNumbersToMatch != null);

        _serialNumberManager.saveSerialNumberToProjectRegularExpressionData(serialNumbersToMatch);
        _serialNumbersToMatchDataIsDirty = false;
      }
      if (_serialNumbersToSkipDataIsDirty)
      {
        Assert.expect(serialNumbersToSkip != null);

        _serialNumberManager.saveSerialNumbersToSkipRegularExpressionData(serialNumbersToSkip);
        _serialNumbersToSkipDataIsDirty = false;
      }
      if (_serialNumbersToValidateDataIsDirty)
      {
        Assert.expect(serialNumbersToValidate != null);

        _serialNumberManager.saveValidSerialNumbersRegularExpressionData(serialNumbersToValidate);
        _serialNumbersToValidateDataIsDirty = false;
      }
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }

    // throw away the values in any cells that might be in edit mode at the time the user wants to leave this screen
    if (_matchedSerialNumberTable.isEditing())
      _matchedSerialNumberTable.getCellEditor().stopCellEditing();
    if (_skippedSerialNumberTable.isEditing())
      _skippedSerialNumberTable.getCellEditor().stopCellEditing();
    if (_validatedSerialNumberTable.isEditing())
      _validatedSerialNumberTable.getCellEditor().stopCellEditing();

    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    // clear undo stack when ready to move to another screen since the serial number data is now saved to disk
    clearUndoStack();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("ProductionOptionsSerialNumberTabPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void setupTableEditors()
  {
    _matchedSerialNumberTable.setPreferredColumnWidths();
    _matchedSerialNumberTable.setEditorsAndRenderers();
    _skippedSerialNumberTable.setPreferredColumnWidths();
    _skippedSerialNumberTable.setEditorsAndRenderers();
    _validatedSerialNumberTable.setPreferredColumnWidths();
    _validatedSerialNumberTable.setEditorsAndRenderers();
  }

  /**
   * @author Laura Cormos
   */
  private void useBoardSerialNumberCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetUseSerialNumberPerBoardCommand(_useBoardSerialNumberCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void useFirstSerialNumberRadioButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_SET_USE_FIRST_SN_FOR_LOOKUP_ONLY_KEY"));
      _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(_useFirstSerialNumberRadioButton.isSelected()));
      if (_useFirstSerialNumberRadioButton.isSelected() && _serialNumberManager.isUseSerialNumberToFindProjectEnabled())
        _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(false, this));
      _commandManager.endCommandBlock();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void enablePreSelectPanelCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_SET_PRE_SELECT_PANEL_KEY"));
      _commandManager.execute(new ConfigSetPreSelectPanelCommand(_enablePreSelectPanelCheckbox.isSelected(),
                                                                 this));
      if (_enablePreSelectPanelCheckbox.isSelected())
      {
        _enablePatternMatchingCheckbox.setSelected(false);
        _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(false, this));
        _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(false));
      }
      _commandManager.endCommandBlock();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void setUseSerialNumberToMatchEnabled(boolean enabled)
  {
    //enable/disable controls for CAD matching
    _useFirstSerialNumberRadioButton.setEnabled(enabled);
    // enable/disable controls for serial number matching
    _useSerialNumberToFindProjectRadioButton.setEnabled(enabled);
    _matchedSerialNumberTable.setEnabled(enabled);
    _serialNumberMatchingAddButton.setEnabled(enabled);
    if (enabled)
      setDeleteButtonBasedOnTableSelection(_TO_MATCH, _matchedSerialNumberTable.getSelectedRow());
    else
      _serialNumberMatchingDeleteButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void checkForDuplicateSerialNumberCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetCheckForDuplicateSerialNumbersCommand(_checkForDuplicateSerialNumberCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  /**
   * @author Laura Cormos
   */
  private void displayLastCADImageCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetDisplayLastCADImageInQueueCommand(_displayLastCADImageInQueueCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }

  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void enablePatternMatchingCheckbox_actionPerformed(ActionEvent e)
  {
    setUseSerialNumberToMatchEnabled(_enablePatternMatchingCheckbox.isSelected()); 
    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_SET_PATTERN_MATCHING_KEY"));
      if (_enablePatternMatchingCheckbox.isSelected())
      {
        if (_enablePreSelectPanelCheckbox.isSelected())
          _commandManager.execute(new ConfigSetPreSelectPanelCommand(false, this));
        if (_useSerialNumberToFindProjectRadioButton.isSelected())
        {
          _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(true, this));
          _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(false));
        }
        else if (_useFirstSerialNumberRadioButton.isSelected())
        {
          _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(false, this));
          _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(true));
        }
      }
      else
      {
        _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(false, this));
        _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(false));
      }
      _commandManager.endCommandBlock();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void useSerialNumberToFindProjectRadioButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SN_SET_USE_SN_TO_FIND_PROJECT_KEY"));
      // if the user is disabling serial number matching, then they can't set/unset the "allow only new serial numbers" choice
      _commandManager.execute(new ConfigSetUseSerialNumberToFindProjectCommand(_useSerialNumberToFindProjectRadioButton.isSelected(), this));
      if (_useSerialNumberToFindProjectRadioButton.isSelected() && _serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled())
        _commandManager.execute(new ConfigSetUseFirstSerialNumberForProjectLookupOnlyCommand(false));
      _commandManager.endCommandBlock();
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  /**
   * @author Laura Cormos
   */
  private void allowOnlyNewSerialNumbersCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetAllowOnlyNewSerialNumbersCommand(_allowOnlyNewSerialNumbersCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  /**
   * @author Laura Cormos
   */
  private void enableSerialNumberSkippingCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetSkipSerialNumbersCommand(_skipSerialNumbersCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void enableSerialNumberValidationCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetValidateSerialNumbersCommand(_validateSerialNumbersCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void enableAutoCheckForBarcodeReadErrorCheckbox_actionPerformed(ActionEvent e)
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _commandManager.execute(new ConfigSetBarcodeScannerReadErrorCheckCommand(_checkForAutoBarcodeReadErrorCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateWithConfigData() throws DatastoreException
  {
    Assert.expect(_serialNumberManager != null);

    // pre select panel has higher priority compare to pattern matching
    boolean isPreSelectPanel = _serialNumberManager.isTestExecPreSelectingProject();
    if (isPreSelectPanel)
    {
      _enablePatternMatchingCheckbox.setEnabled(false);
      _serialNumberManager.setUseSerialNumberToFindProject(false);
      _serialNumberManager.setUseFirstSerialNumberForProjectLookupOnly(false);
    }
    _enablePreSelectPanelCheckbox.setSelected(isPreSelectPanel);
    
    // if both settings is true, we use CAD matching
    if (_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() &&
        _serialNumberManager.isUseSerialNumberToFindProjectEnabled())
    {
      _serialNumberManager.setUseSerialNumberToFindProject(false);
      _useFirstSerialNumberRadioButton.setSelected(true);
    }
    else if (_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() == false &&
             _serialNumberManager.isUseSerialNumberToFindProjectEnabled() == false)
    {
      _useSerialNumberToFindProjectRadioButton.setSelected(true);
      _useFirstSerialNumberRadioButton.setSelected(false);
    }
    else
    {
      _useSerialNumberToFindProjectRadioButton.setSelected(_serialNumberManager.isUseSerialNumberToFindProjectEnabled());
      _useFirstSerialNumberRadioButton.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled());
    }
    // anyone is true, then it is enabled.
    _enablePatternMatchingCheckbox.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() || _serialNumberManager.isUseSerialNumberToFindProjectEnabled());
    setUseSerialNumberToMatchEnabled(_serialNumberManager.isTestExecPreSelectingProject() == false && _enablePatternMatchingCheckbox.isSelected());
    populateMatchedSerialNumbersTable();

    _skipSerialNumbersCheckbox.setSelected(_serialNumberManager.isSkipSerialNumbersEnabled());
    populateSkippedSerialNumbersTable();

    _validateSerialNumbersCheckbox.setSelected(_serialNumberManager.isValidateBoardSerialNumbersEnabled());
    populateValidatedSerialNumbersTable();

    _useBoardSerialNumberCheckbox.setSelected(_serialNumberManager.isUseUniqueSerialNumberPerBoardEnabled());
    _allowOnlyNewSerialNumbersCheckbox.setSelected(_serialNumberManager.areOnlyNewSerialNumbersAllowedWhenCadMatchFails());
    _checkForDuplicateSerialNumberCheckbox.setSelected(_serialNumberManager.isCheckForDuplicateSerialNumbersEnabled());

    _displayLastCADImageInQueueCheckbox.setSelected(_serialNumberManager.isTestExecDisplayingLastCadImageInQueue());
    _checkForAutoBarcodeReadErrorCheckbox.setSelected(_serialNumberManager.isBarcodeScannerReadErrorCheckEnabled());

    setDeleteButtonsState();

    setupTableEditors();
  }

  /**
   * @author Laura Cormos
   */
  private void populateMatchedSerialNumbersTable()
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _loadingInProgress = true;
      _matchedSerialNumberTableModel.setData(_serialNumberManager.getSerialNumberToProjectRegularExpressionData());
      _loadingInProgress = false;
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateSkippedSerialNumbersTable()
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _loadingInProgress = true;
      _skippedSerialNumberTableModel.setData(_serialNumberManager.getSerialNumbersToSkipRegularExpressionData());
      _loadingInProgress = false;
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateValidatedSerialNumbersTable()
  {
    Assert.expect(_serialNumberManager != null);

    try
    {
      _loadingInProgress = true;
      _validatedSerialNumberTableModel.setData(_serialNumberManager.getValidSerialNumbersRegularExpressionData());
      _loadingInProgress = false;
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  void showDatastoreError(DatastoreException ex)
  {
    JOptionPane.showMessageDialog(_mainUI,
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Laura Cormos
   */
  public void setSerialNumbersDataIsDirty(int serialNumbersType)
  {
    switch (serialNumbersType)
    {
      case _TO_MATCH:
        _serialNumbersToMatchDataIsDirty = true;
        break;
      case _TO_SKIP:
        _serialNumbersToSkipDataIsDirty = true;
        break;
      case _TO_VALIDATE:
        _serialNumbersToValidateDataIsDirty = true;
        break;
      default:
        break;
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteButtonBasedOnTableSelection(int serialNumbersType, int rowIndex)
  {
    switch (serialNumbersType)
    {
      case _TO_MATCH:
        _serialNumberMatchingDeleteButton.setEnabled(rowIndex >= 0);
        break;
      case _TO_SKIP:
        _serialNumberSkipDeleteButton.setEnabled(rowIndex >= 0);
        break;
      case _TO_VALIDATE:
        _serialNumberValidateDeleteButton.setEnabled(rowIndex >= 0);
        break;
      default:
        Assert.expect(false, "Unknown serial number type");
    }
  }

  /**
   * @author Laura Cormos
   */
  private void addRowButton_actionPerformed(int serialNumbersType)
  {
    _addRowInProgress = true;
    switch (serialNumbersType)
    {
      case _TO_MATCH:
        if (FileName.getProjectNames().size() == 0)
        {
          MessageDialog.showErrorDialog(_mainUI, StringLocalizer.keyToString("CFGUI_SN_NO_PROJECTS_ON_ADD_MSG_KEY"),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
        }
        else
        {
        _matchedSerialNumberTableModel.addNewRow();
        _matchedSerialNumberTable.setRowSelectionInterval(_matchedSerialNumberTableModel.getRowCount() - 1,
                                                          _matchedSerialNumberTableModel.getRowCount() - 1);
        _serialNumbersToMatchDataIsDirty = true;
        }
        break;
      case _TO_SKIP:
        _skippedSerialNumberTableModel.addNewRow();
        _skippedSerialNumberTable.setRowSelectionInterval(_skippedSerialNumberTableModel.getRowCount() - 1,
                                                          _skippedSerialNumberTableModel.getRowCount() - 1);
        _serialNumbersToSkipDataIsDirty = true;
        break;
      case _TO_VALIDATE:
        _validatedSerialNumberTableModel.addNewRow();
        _validatedSerialNumberTable.setRowSelectionInterval(_validatedSerialNumberTableModel.getRowCount() - 1,
                                                            _validatedSerialNumberTableModel.getRowCount() - 1);
        _serialNumbersToValidateDataIsDirty = true;
        break;
      default:
        Assert.expect(false, "Unknown serial number type");
    }
  }

  /**
   * @author Laura Cormos
   */
  private void deleteRowButton_actionPerformed(int serialNumbersType)
  {
    _deleteRowInProgress = true;
    switch (serialNumbersType)
    {
      case _TO_MATCH:
        try
        {
          _commandManager.execute(new ConfigDeleteSerialNumberEntryCommand(_matchedSerialNumberTable, this, _TO_MATCH));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
        }
        break;
      case _TO_SKIP:
        try
        {
          _commandManager.execute(new ConfigDeleteSerialNumberEntryCommand(_skippedSerialNumberTable, this, _TO_SKIP));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
        }
        break;
      case _TO_VALIDATE:
        try
        {
          _commandManager.execute(new ConfigDeleteSerialNumberEntryCommand(_validatedSerialNumberTable, this, _TO_VALIDATE));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
        }
        break;
      default:
        Assert.expect(false, "Unknown serial number type");
    }
  }

  /**
   * @author Laura Cormos
   */
  void setSelectedRowInTable(int rowIndex, int serialNumbersType)
  {
    Assert.expect(rowIndex >= 0);

    switch (serialNumbersType)
    {
      case _TO_MATCH:
        _matchedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        break;
      case _TO_SKIP:
        _skippedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        break;
      case _TO_VALIDATE:
        _validatedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        break;
      default:
        Assert.expect(false, "Unknown serial number type");
    }
  }

  /**
   * @author Laura Cormos
   */
  private boolean areRegExSerialNumberPatternsValidAndProjectNamesSet(
      Collection<SerialNumberRegularExpressionData> serialNumbersWithRegexData,
      Collection<SerialNumberRegularExpressionData> serialNumbersToMatchProjectData)
  {
    Assert.expect(serialNumbersWithRegexData != null);

    boolean errorFound = false;
    String errors = StringLocalizer.keyToString("CFGUI_JTA_FIX_ERRORS_MESSAGE_KEY");
    // first check for missing project name
    for (SerialNumberRegularExpressionData serialNumberData : serialNumbersToMatchProjectData)
    {
      if (serialNumberData.isProjectNameSet() && serialNumberData.getProjectName().length() == 0)
      {
        errors += StringLocalizer.keyToString(new LocalizedString("CFGUI_SN_MISSING_PROJECT_NAME_MESSAGE_KEY",
                                                                  new Object[]{serialNumberData.getOriginalExpression()}));
        errors += "\n";
        errorFound = true;
      }
    }
    // then check for invalid regex patterns.
    for (SerialNumberRegularExpressionData serialNumberData : serialNumbersWithRegexData)
    {
      if (serialNumberData.isInterpretedAsRegularExpression())
      {
        try
        {
          RegularExpressionUtilAxi.checkRegExValid(serialNumberData.getOriginalExpression());
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          errors += ex.getLocalizedMessage();
          errors += "\n";
          errorFound = true;
        }
      }
      if (serialNumberData.getOriginalExpression().equals(""))
      {
        errors += StringLocalizer.keyToString("CFGUI_EMPTY_REGEX_NOT_VALID_MESSAGE_KEY");
        errors += "\n";
        errorFound = true;
      }
    }

    if (errorFound)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    errors,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  /**
   * @author Laura Cormos
   */
  private boolean areRegExSerialNumberPatternsValid(Collection<SerialNumberRegularExpressionData> serialNumbersWithRegexData)
  {
    Assert.expect(serialNumbersWithRegexData != null);

    boolean errorFound = false;
    String errors = StringLocalizer.keyToString("CFGUI_JTA_FIX_ERRORS_MESSAGE_KEY");

    for (SerialNumberRegularExpressionData serialNumberData : serialNumbersWithRegexData)
    {
      if (serialNumberData.isInterpretedAsRegularExpression())
      {
        try
        {
          RegularExpressionUtilAxi.checkRegExValid(serialNumberData.getOriginalExpression());
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          errors += ex.getLocalizedMessage();
          errors += "\n";
          errorFound = true;
        }
      }
      if (serialNumberData.getOriginalExpression().equals(""))
      {
        errors += StringLocalizer.keyToString("CFGUI_EMPTY_REGEX_NOT_VALID_MESSAGE_KEY");
        errors += "\n";
        errorFound = true;
      }
    }

    if (errorFound)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    errors,
                                    StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  /**
   * @author Laura Cormos
   */
  void editCell(int rowIndex, int colIndex, int serialNumberType)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(colIndex >= 0);

    switch (serialNumberType)
    {
      case _TO_MATCH:
        _matchedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        _matchedSerialNumberTable.editCellAt(rowIndex, colIndex);
        break;
      case _TO_SKIP:
        _skippedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        _skippedSerialNumberTable.editCellAt(rowIndex, colIndex);
        break;
      case _TO_VALIDATE:
        _validatedSerialNumberTable.setRowSelectionInterval(rowIndex, rowIndex);
        _validatedSerialNumberTable.editCellAt(rowIndex, colIndex);
        break;
      default:
        Assert.expect(false, "Unknown serial number type");
    }
  }

  /**
   * @author Laura Cormos
   */
  private void cancelEditingIfNecessary(JTable table)
  {
    // cancel cell editing in case user deletes the row while editing is still going on.
    int column = table.getEditingColumn();
    if (column > -1)
    {
      TableCellEditor cellEditor = table.getColumnModel().getColumn(column).getCellEditor();
      Assert.expect(cellEditor != null);
      cellEditor.cancelCellEditing();
    }
  }

  /**
   * @author Laura Cormos
   */
  public void setDeleteButtonsState()
  {
    if (_enablePreSelectPanelCheckbox.isSelected())
      _serialNumberMatchingDeleteButton.setEnabled(false);
    else
      setDeleteButtonBasedOnTableSelection(_TO_MATCH, _matchedSerialNumberTable.getSelectedRow());

//    setDeleteButtonBasedOnTableSelection(_TO_MATCH, _matchedSerialNumberTable.getSelectedRow());
    setDeleteButtonBasedOnTableSelection(_TO_SKIP, _skippedSerialNumberTable.getSelectedRow());
    setDeleteButtonBasedOnTableSelection(_TO_VALIDATE, _validatedSerialNumberTable.getSelectedRow());
  }

  /**
    * @author Laura Cormos
    */
   private void clearUndoStack()
   {
     _commandManager.clear();
     _menuBar.updateUndoRedoMenuItems();
  }

  /**
   * @author Laura Cormos
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ConfigObservable)
          handleConfigDataUpdate(object);
        else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          // do nothing - there is no undo State for this screen
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void handleConfigDataUpdate(final Object object)
  {
    Assert.expect(object != null);

    if (object instanceof ConfigEnum)
    {
      ConfigEnum configEnum = (ConfigEnum)object;
      if (configEnum instanceof SoftwareConfigEnum)
      {
        if (configEnum.equals(SoftwareConfigEnum.ALLOW_ONLY_NEW_SERIAL_NUMBERS_WHEN_CAD_MATCH_FAILS))
          _allowOnlyNewSerialNumbersCheckbox.setSelected(_serialNumberManager.areOnlyNewSerialNumbersAllowedWhenCadMatchFails());
        else if (configEnum.equals(SoftwareConfigEnum.USE_SERIAL_NUMBER_TO_FIND_PROJECT))
        {
          _useSerialNumberToFindProjectRadioButton.setSelected(_serialNumberManager.isUseSerialNumberToFindProjectEnabled());
          _enablePatternMatchingCheckbox.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() || _serialNumberManager.isUseSerialNumberToFindProjectEnabled());
          setUseSerialNumberToMatchEnabled(_serialNumberManager.isTestExecPreSelectingProject() == false && _enablePatternMatchingCheckbox.isSelected());
        }
        else if (configEnum.equals(SoftwareConfigEnum.SKIP_SERIAL_NUMBERS))
          _skipSerialNumbersCheckbox.setSelected(_serialNumberManager.isSkipSerialNumbersEnabled());
        else if (configEnum.equals(SoftwareConfigEnum.VALIDATE_BOARD_SERIAL_NUMBERS))
          _validateSerialNumbersCheckbox.setSelected(_serialNumberManager.isValidateBoardSerialNumbersEnabled());
        else if (configEnum.equals(SoftwareConfigEnum.USE_UNIQUE_SERIAL_NUMBER_PER_BOARD))
          _useBoardSerialNumberCheckbox.setSelected(_serialNumberManager.isUseUniqueSerialNumberPerBoardEnabled());
        else if (configEnum.equals(SoftwareConfigEnum.USE_FIRST_SERIAL_NUMBER_FOR_PROJECT_LOOKUP_ONLY))
        {
          _useFirstSerialNumberRadioButton.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled());
          _enablePatternMatchingCheckbox.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() || _serialNumberManager.isUseSerialNumberToFindProjectEnabled());
          setUseSerialNumberToMatchEnabled(_serialNumberManager.isTestExecPreSelectingProject() == false && _enablePatternMatchingCheckbox.isSelected());
        }
        else if (configEnum.equals(SoftwareConfigEnum.TEST_EXEC_PRE_SELECT_PROJECT))
        {
          boolean isPreSelectPanel = _serialNumberManager.isTestExecPreSelectingProject();
          _enablePreSelectPanelCheckbox.setSelected(isPreSelectPanel);
          _enablePatternMatchingCheckbox.setEnabled(isPreSelectPanel == false);
          _enablePatternMatchingCheckbox.setSelected(_serialNumberManager.isUseFirstSerialNumberForProjectLookupOnlyEnabled() || _serialNumberManager.isUseSerialNumberToFindProjectEnabled());
          setUseSerialNumberToMatchEnabled(_serialNumberManager.isTestExecPreSelectingProject() == false && _enablePatternMatchingCheckbox.isSelected());
        }
        else if (configEnum.equals(SoftwareConfigEnum.CHECK_FOR_DUPLICATE_SERIAL_NUMBERS))
          _checkForDuplicateSerialNumberCheckbox.setSelected(_serialNumberManager.isCheckForDuplicateSerialNumbersEnabled());
        else if (configEnum.equals(SoftwareConfigEnum.TEST_EXEC_DISPLAY_LAST_CAD_IMAGE_IN_QUEUE))
          _displayLastCADImageInQueueCheckbox.setSelected(_serialNumberManager.isTestExecDisplayingLastCadImageInQueue());
        else if (configEnum.equals(SoftwareConfigEnum.BARCODE_SCANNER_READ_ERROR_CHECK))
          _checkForAutoBarcodeReadErrorCheckbox.setSelected(_serialNumberManager.isBarcodeScannerReadErrorCheckEnabled());
      }
    }
    else if (object instanceof SerialNumberRegularExpressionData)
    {
      if (_loadingInProgress == false && _addRowInProgress == false && _deleteRowInProgress == false)
      {
        // refresh the tables and select the row where the object of the update is found
        // when only 1 row exists in the table if the selection is not imposed, that row will never be selectable
        // for deletion purposes
        SerialNumberRegularExpressionData selectedItem = (SerialNumberRegularExpressionData)object;
        int selectedItemIndex = _matchedSerialNumberTableModel.getIndexOf(selectedItem);
        if (selectedItemIndex >= 0)
        {
          _matchedSerialNumberTableModel.fireTableDataChanged();
          _matchedSerialNumberTable.setRowSelectionInterval(selectedItemIndex, selectedItemIndex);
        }
        selectedItemIndex = _skippedSerialNumberTableModel.getIndexOf(selectedItem);
        if (selectedItemIndex >= 0)
        {
          _skippedSerialNumberTableModel.fireTableDataChanged();
          _skippedSerialNumberTable.setRowSelectionInterval(selectedItemIndex, selectedItemIndex);
        }
        selectedItemIndex = _validatedSerialNumberTableModel.getIndexOf(selectedItem);
        if (selectedItemIndex >= 0)
        {
          _validatedSerialNumberTableModel.fireTableDataChanged();
          _validatedSerialNumberTable.setRowSelectionInterval(selectedItemIndex, selectedItemIndex);
        }
      }
      _addRowInProgress = false;
      _deleteRowInProgress = false;
      setDeleteButtonsState();
    }
  }
}
