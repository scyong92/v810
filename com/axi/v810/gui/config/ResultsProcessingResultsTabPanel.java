package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ResultsProcessingResultsTabPanel extends AbstractTaskPanel implements Observer
{
  private ConfigGuiPersistence _persistSettings;
  private ResultsProcessor _resultsProcessor;
  private java.util.List<Rule> _ruleList;
  private Rule _currentSelectedRule;
  private boolean _conditionIsDirty = false;
  private boolean _currentSelectedRuleIsDirty = false;
  private boolean _processRuleSelection = true;
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  // UI components
  private JTabbedPane _tabbedPane = new JTabbedPane();
  private JPanel _centerPanel = new JPanel();
  private JPanel _productionTuneCenterPanel = new JPanel();

  // Results Tab UI components
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JCheckBox _enableProcessingCheckBox = new JCheckBox();
  private JPanel _rulesPanel = new JPanel();
  private BorderLayout _rulesPanelBorderLayout = new BorderLayout();
  private JPanel _rulesListPanel = new JPanel();
  private BorderLayout _rulesListPanelBorderLayout = new BorderLayout();
  private JPanel _rulesListSelectionPanel = new JPanel();
  private FlowLayout _rulesListSelectionPanelFlowLayout = new FlowLayout();
  private JScrollPane _rulesTableScrollPane = new JScrollPane();
  private ResultsProcessingRulesTable _rulesTable = new ResultsProcessingRulesTable();
  private ResultsProcessingRulesTableModel _rulesTableModel;
  private JPanel _rulesListActionsWrapperPanel = new JPanel();
  private FlowLayout _rulesListActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _rulesListActionsPanel = new JPanel();
  private GridLayout _rulesListActionsPanelGridLayout = new GridLayout();
  private JButton _saveRuleButton = new JButton();
  private JButton _saveRuleAsButton = new JButton();
  private JButton _deleteRuleButton = new JButton();
  private JButton _newRuleButton = new JButton();
  private JPanel _rulesDefinitionPanel = new JPanel();
  private BorderLayout _rulesDefinitionPanelBorderLayout = new BorderLayout();
  private JSplitPane _rulesDefinitionSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
  private JPanel _conditionDefinitionPanel = new JPanel();
  private BorderLayout _conditionDefinitionPanelBorderLayout = new BorderLayout();
  private JPanel _conditionKeyPanel = new JPanel();
  private BorderLayout _conditionKeyPanelBorderLayout = new BorderLayout();
  private JLabel _conditionKeySelectLabel = new JLabel();
  private JComboBox _conditionKeySelectComboBox = new JComboBox();
  private JPanel _conditionKeyActionsWrapperPanel = new JPanel();
  private FlowLayout _conditionKeyActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _conditionKeyActionsPanel = new JPanel();
  private GridLayout _conditionKeyActionsPanelGridLayout = new GridLayout();
  private JButton _addConditionRowButton = new JButton();
  private JButton _deleteConditionRowButton = new JButton();
  private JPanel _conditionKeyRegularExpressionPanel = new JPanel();
  private BorderLayout _conditionKeyRegularExpressionPanelBorderLayout = new BorderLayout();
  private JScrollPane _conditionKeyRegularExpressionScrollPane = new JScrollPane();
  private ResultsProcessingConditionsTable _conditionsTable = new ResultsProcessingConditionsTable();
  private ResultsProcessingConditionsTableModel _conditionsTableModel;
  private JPanel _fileTransferSetupPanel = new JPanel();
  private BorderLayout _fileTransferSetupPanelBorderLayout = new BorderLayout();
  private JSplitPane _fileTransferSetupSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
  private JScrollPane _targetsAndDestinationsScrollPane = new JScrollPane();
  private JPanel _targetsAndDestinationsPanel = new JPanel();
  private PairLayout _targetsAndDestinationsPanelPairLayout = new PairLayout();
  private JLabel _resultsFilesHeadeLabel = new JLabel(StringLocalizer.keyToString(
      "CFGUI_RESPROC_RESULTS_FILES_TO_BE_COPIES_LABEL_KEY"));
  private JLabel _destinationDirectoriesHeaderLabel = new JLabel(StringLocalizer.keyToString(
      "CFGUI_RESPROC_DESTINATION_DIRECTORIES_FOR_EACH_RESULTS_FILE_LABEL_KEY"));
  private JPanel _cadXMLPanel = new JPanel();
  private BorderLayout _cadXMLPanelBorderLayout = new BorderLayout();
  private JCheckBox _cadXMLFileCheckBox = new JCheckBox();
  private JScrollPane _cadXMLDestinationsListScrollPane = new JScrollPane();
  private JList _cadXMLDestinationsList = new JList();
  private DefaultListModel _cadXMLDestinationsListModel = new DefaultListModel();
  private JPanel _settingsXMLPanel = new JPanel();
  private BorderLayout _settingsXMLPanelBorderLayout = new BorderLayout();
  private JCheckBox _settingsXMLFileCheckBox = new JCheckBox();
  private JScrollPane _settingsXMLDestinationsListScrollPane = new JScrollPane();
  private JList _settingsXMLDestinationsList = new JList();
  private DefaultListModel _settingsXMLDestinationsListModel = new DefaultListModel();
  private JPanel _indictmentsXMLPanel = new JPanel();
  private BorderLayout _indictmentsXMLPanelBorderLayout = new BorderLayout();
  private JCheckBox _indictmentsXMLFileCheckBox = new JCheckBox();
  private JScrollPane _indictmentsXMLDestinationsListScrollPane = new JScrollPane();
  private JList _indictmentsXMLDestinationsList = new JList();
  private DefaultListModel _indictmentsXMLDestinationsListModel = new DefaultListModel();
  private JPanel _repairImagesPanel = new JPanel();
  private BorderLayout _repairImagesPanelBorderLayout = new BorderLayout();
  private JCheckBox _repairImagesFileCheckBox = new JCheckBox();
  private JScrollPane _repairImagesDestinationsListScrollPane = new JScrollPane();
  private JList _repairImagesDestinationsList = new JList();
  private DefaultListModel _repairImagesDestinationsListModel = new DefaultListModel();
  private JPanel _measurementsXMLPanel = new JPanel();
  private BorderLayout _measurementsXMLPanelBorderLayout = new BorderLayout();
  private JCheckBox _measurementsXMLFileCheckBox = new JCheckBox();
  private JScrollPane _measurementsXMLDestinationsListScrollPane = new JScrollPane();
  private JList _measurementsXMLDestinationsList = new JList();
  private DefaultListModel _measurementsXMLDestinationsListModel = new DefaultListModel();
  private JPanel _cadXMLTransferButtonsWrapperPanel = new JPanel();
  private VerticalFlowLayout _cadXMLTransferButtonsWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _cadXMLTransferButtonsPanel = new JPanel();
  private GridLayout _cadXMLTransferButtonsPanelGridLayout = new GridLayout();
  private JButton _cadXMLRightArrowButton = new JButton();
  private JButton _cadXMLLeftArrowButton = new JButton();
  private JPanel _settingsXMLTransferButtonsWrapperPanel = new JPanel();
  private VerticalFlowLayout _settingsXMLTransferButtonsWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _settingsXMLTransferButtonsPanel = new JPanel();
  private GridLayout _settingsXMLTransferButtonsPanelGridLayout = new GridLayout();
  private JButton _settingsXMLRightArrowButton = new JButton();
  private JButton _settingsXMLLeftArrowButton = new JButton();
  private JPanel _indictmentsXMLTransferButtonsWrapperPanel = new JPanel();
  private VerticalFlowLayout _indictmentsXMLTransferButtonsWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _indictmentsXMLTransferButtonsPanel = new JPanel();
  private GridLayout _indictmentsXMLTransferButtonsPanelGridLayout = new GridLayout();
  private JButton _indictmentsXMLRightArrowButton = new JButton();
  private JButton _indictmentsXMLLeftArrowButton = new JButton();
  private JPanel _repairImagesTransferButtonsWrapperPanel = new JPanel();
  private VerticalFlowLayout _repairImagesTransferButtonsWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _repairImagesTransferButtonsPanel = new JPanel();
  private GridLayout _repairImagesTransferButtonsPanelGridLayout = new GridLayout();
  private JButton _repairImagesRightArrowButton = new JButton();
  private JButton _repairImagesLeftArrowButton = new JButton();    
  private JPanel _measurementsXMLTransferButtonsWrapperPanel = new JPanel();
  private VerticalFlowLayout _measurementsXMLTransferButtonsWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _measurementsXMLTransferButtonsPanel = new JPanel();
  private GridLayout _measurementsXMLTransferButtonsPanelGridLayout = new GridLayout();
  private JButton _measurementsXMLRightArrowButton = new JButton();
  private JButton _measurementsXMLLeftArrowButton = new JButton();
  private JPanel _allDestinationsPanel = new JPanel();
  private BorderLayout _allDestinationsPanelBorderLayout = new BorderLayout();
  private JPanel _allDestinationsListBoxPanel = new JPanel();
  private BorderLayout _allDestinationsListBoxPanelBorderLayout = new BorderLayout();
  private JLabel _allDestinationDirsHeaderLabel = new JLabel(StringLocalizer.keyToString(
      "CFGUI_RESPROC_ADD_ALL_DESTINATION_DIRECTORIES_HERE_LABEL_KEY"));
  private JScrollPane _allDestinationsListBoxScrollPane = new JScrollPane();
  private JList _allDestinationsList = new JList();
  private DefaultListModel _allDestinationsListModel = new DefaultListModel();
  private JPanel _allDestinationsActionWrapperPanel = new JPanel();
  private VerticalFlowLayout _allDestinationsActionWrapperPanelFlowLayout = new VerticalFlowLayout();
  private JPanel _allDestinationsActionPanel = new JPanel();
  private GridLayout _allDestinationsActionPanelGridLayout = new GridLayout();
  private JButton _addDestinationButton = new JButton();
  private JButton _editDestinationButton = new JButton();
  private JButton _browseDestinationButton = new JButton();
  private JButton _deleteDestinationButton = new JButton();
  private JPanel _repairToolPanel = new JPanel();
  private BorderLayout _repairToolPanelBorderLayout = new BorderLayout();
  private JPanel _enableRepairPanel = new JPanel();
  private FlowLayout _enableRepairPanelFlowLayout = new FlowLayout();
  private JCheckBox _enableRepairCheckBox = new JCheckBox();
  private JPanel _repairToolInstallDirPanel = new JPanel();
  private FlowLayout _repairToolInstallDirPanelFlowLayout = new FlowLayout();
  private JLabel _repairToolInstallDirLabel = new JLabel();
  private JTextField _repairToolInstallDirTextField = new JTextField(30);
  private JButton _repairToolInstallDirBrowseButton = new JButton();
  private JFileChooser _fileChooser = null;
  private JCheckBox _createTimeStampFolderCheckBox = new JCheckBox();

  // production tuning screen controls
  private JPanel _defaultLocationPanel = new JPanel();

  //Janan - XCR-3836 Measurement XML able to transfer to other location
  private Boolean _isGenerateMeasurementsXMLEnabled = Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE);

  private ConfigEnvironment _envPanel = null;


  /**
   * Useful ONLY for the JBuilder designer.  Not for use with normal code.
   * @author Laura Cormos
   */
  private ResultsProcessingResultsTabPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    try
    {
      _resultsProcessor = ResultsProcessor.getInstance();
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  public ResultsProcessingResultsTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    Assert.expect(envPanel != null);

    _envPanel = envPanel;
    _resultsProcessor = ResultsProcessor.getInstance();
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void restorePersistSettings()
  {
    _persistSettings = _envPanel.getPersistance();
    int conditionFileTransferDividerLocation = _persistSettings.getConditionFileTransferDivider();
    if (conditionFileTransferDividerLocation > 0)
      _fileTransferSetupSplitPane.setDividerLocation(conditionFileTransferDividerLocation);

    int resultsDestinationDividerLocation = _persistSettings.getDestinationsDivider();
    if (resultsDestinationDividerLocation > 0)
      _rulesDefinitionSplitPane.setDividerLocation(resultsDestinationDividerLocation);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void savePersistSettings()
  {
    int conditionFileTransferDividerLocation = _fileTransferSetupSplitPane.getDividerLocation();
    int resultsDestinationDividerLocation = _rulesDefinitionSplitPane.getDividerLocation();
    if (_persistSettings != null)
    {
      _persistSettings.setConditionFileTransferDivider(conditionFileTransferDividerLocation);
      _persistSettings.setDestinationsDivider(resultsDestinationDividerLocation);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _centerPanel.add(_enableProcessingCheckBox, BorderLayout.NORTH);
    _centerPanel.add(_rulesPanel, BorderLayout.CENTER);
    _centerPanel.add(_repairToolPanel, BorderLayout.SOUTH);

    _enableProcessingCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_ENABLE_RESULTS_PROCESSING_LABEL_KEY"));
    _enableProcessingCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableProcessingCheckBox_actionPerformed(e);
      }
    });

    _rulesPanel.setLayout(_rulesPanelBorderLayout);
    _rulesPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_RESPROC_RULES_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _rulesPanel.add(_rulesListPanel, BorderLayout.NORTH);
    _rulesPanel.add(_rulesDefinitionPanel, BorderLayout.CENTER);

    _rulesListPanel.setLayout(_rulesListPanelBorderLayout);
    _rulesListPanel.add(_rulesListSelectionPanel, BorderLayout.CENTER);
    _rulesListPanel.add(_rulesListActionsWrapperPanel, BorderLayout.EAST);

    _rulesListSelectionPanel.setLayout(_rulesListSelectionPanelFlowLayout);
    _rulesListSelectionPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _rulesListSelectionPanel.add(_rulesTableScrollPane);

    _rulesTableModel = new ResultsProcessingRulesTableModel(this);
    // allowing column reordering in the rules table (which has a custom check box renderer) causes havoc with the data
    _rulesTable.getTableHeader().setReorderingAllowed(false);
    _rulesTable.setModel(_rulesTableModel);
    _rulesTableScrollPane.getViewport().add(_rulesTable);
    _rulesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    final ListSelectionModel ruleRowSelectionModel = _rulesTable.getSelectionModel();
    ruleRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (_processRuleSelection)
        {
          ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
  //        if (lsm.isSelectionEmpty() == false)
          if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
          {
            // do nothing
          }
          else
          {
            // don't do anything if the save was canceled
            int selectedRow = _rulesTable.getSelectedRow();
            int previousSelectionIndex;
            if (_currentSelectedRule != null)
              previousSelectionIndex = _rulesTableModel.getIndexForRule(_currentSelectedRule);
            else
              previousSelectionIndex = -1;
            // if the row is the same as currently selected then it's an action on the current rule not a selection change
            if (selectedRow != previousSelectionIndex)
            {
              if (saveRuleIfNecessary() == false)
              {
                _processRuleSelection = false;
                _rulesTable.setRowSelectionInterval(previousSelectionIndex, previousSelectionIndex);
                _processRuleSelection = true;;
                return;
              }
              setCurrentSelectedRuleIsDirty(false);
              _rulesTableModel.setSelectedRow(_rulesTable.getSelectedRow());
              populateWithRuleData(_rulesTableModel.getRuleAt(selectedRow));
              setDeleteButtonsState();
              setSaveButtonsState();
              setAddConditionButtonState();
              clearUndoStack();
              resetStatusBar();
            }
          }
        }
      }
    });

//    int width = _rulesPanel.getPreferredSize().width;
//    _rulesTableScrollPane.setPreferredSize(new Dimension(_rulesPanel.getWidth()/2,
//                                                         _rulesTable.getMaximumHeight()));

    _rulesListActionsWrapperPanel.setLayout(_rulesListActionsWrapperPanelFlowLayout);
    _rulesListActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _rulesListActionsWrapperPanel.add(_rulesListActionsPanel);

    _rulesListActionsPanel.setLayout(_rulesListActionsPanelGridLayout);
    _rulesListActionsPanelGridLayout.setColumns(4);
    _rulesListActionsPanelGridLayout.setHgap(10);
    _rulesListActionsPanelGridLayout.setRows(0);
    _rulesListActionsPanelGridLayout.setVgap(10);

    _rulesListActionsPanel.add(_newRuleButton, null);
    _rulesListActionsPanel.add(_saveRuleButton, null);
    _rulesListActionsPanel.add(_saveRuleAsButton, null);
    _rulesListActionsPanel.add(_deleteRuleButton, null);

    _newRuleButton.setText(StringLocalizer.keyToString("CFGUI_NEW_BUTTON_KEY"));
    _newRuleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        newRuleButton_actionPerformed(e);
      }
    });

    _saveRuleButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_BUTTON_KEY"));
    _saveRuleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveRuleButton_actionPerformed(e);
      }
    });

    _saveRuleAsButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_AS_BUTTON_KEY"));
    _saveRuleAsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveAsRuleButton_actionPerformed(e);
      }
    });

    _deleteRuleButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteRuleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteRuleButton_actionPerformed(e);
      }
    });

    _rulesDefinitionPanel.setLayout(_rulesDefinitionPanelBorderLayout);
    _rulesDefinitionPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_RESPROC_RULE_DEFINITION_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 0, 0, 0)));
    _rulesDefinitionPanel.add(_rulesDefinitionSplitPane, BorderLayout.CENTER);

    _rulesDefinitionSplitPane.setBorder(null);
    _rulesDefinitionSplitPane.add(_conditionDefinitionPanel, JSplitPane.TOP);
    _rulesDefinitionSplitPane.add(_fileTransferSetupPanel, JSplitPane.BOTTOM);

    _conditionDefinitionPanel.setLayout(_conditionDefinitionPanelBorderLayout);
    _conditionDefinitionPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_RESPROC_CONDITION_DEFINITION_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _conditionDefinitionPanel.add(_conditionKeyPanel, BorderLayout.SOUTH);
    _conditionDefinitionPanel.add(_conditionKeyRegularExpressionPanel, BorderLayout.CENTER);

    _conditionKeyPanel.setLayout(_conditionKeyPanelBorderLayout);
    _conditionKeyPanel.add(_conditionKeyActionsWrapperPanel, BorderLayout.EAST);

    _conditionKeyActionsWrapperPanel.setLayout(_conditionKeyActionsWrapperPanelFlowLayout);
    _conditionKeyActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _conditionKeyActionsWrapperPanel.add(_conditionKeyActionsPanel);

    _conditionKeyActionsPanel.setLayout(_conditionKeyActionsPanelGridLayout);
    _conditionKeyActionsPanelGridLayout.setColumns(2);
    _conditionKeyActionsPanelGridLayout.setHgap(10);
    _conditionKeyActionsPanelGridLayout.setRows(0);
    _conditionKeyActionsPanelGridLayout.setVgap(10);
    _conditionKeyActionsPanel.add(_addConditionRowButton);
    _conditionKeyActionsPanel.add(_deleteConditionRowButton);

    _addConditionRowButton.setText(StringLocalizer.keyToString("CFGUI_ADD_ROW_BUTTON_KEY"));
    _addConditionRowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addConditionRowButton_actionPerformed(e);
      }
    });

    _deleteConditionRowButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ROW_BUTTON_KEY"));
    _deleteConditionRowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteConditionRowButton_actionPerformed(e);
      }
    });


    _conditionKeyRegularExpressionPanel.setLayout(_conditionKeyRegularExpressionPanelBorderLayout);
    _conditionKeyRegularExpressionPanel.add(_conditionKeyRegularExpressionScrollPane, BorderLayout.CENTER);

    _conditionsTableModel = new ResultsProcessingConditionsTableModel(this);
    _conditionsTable.setModel(_conditionsTableModel);
    _conditionsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _conditionsTable.getTableHeader().setReorderingAllowed(false);
    _conditionsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    final ListSelectionModel conditionRowSelectionModel = _conditionsTable.getSelectionModel();
    conditionRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
//        if (lsm.isSelectionEmpty() == false)
        if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = _conditionsTable.getSelectedRow();
          _conditionsTableModel.setSelectedRow(_conditionsTable.getSelectedRow());
          setDeleteConditionButtonBasedOnTableSelection(selectedRow);
        }
      }
    });

    _conditionKeyRegularExpressionScrollPane.getViewport().add(_conditionsTable);

    _fileTransferSetupPanel.setLayout(_fileTransferSetupPanelBorderLayout);
    _fileTransferSetupPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_RESPROC_FILE_TRANSFER_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _fileTransferSetupPanel.setMinimumSize(new Dimension(0,0));
    _fileTransferSetupPanel.add(_fileTransferSetupSplitPane, BorderLayout.CENTER);

    JPanel fileTransferSetupPanel = new JPanel();
    fileTransferSetupPanel.setLayout(new BorderLayout());
    JPanel targetAndDestinationsMainPanel = new JPanel();
    targetAndDestinationsMainPanel.setLayout(new BorderLayout());
   
    targetAndDestinationsMainPanel.add(_targetsAndDestinationsPanel, BorderLayout.NORTH);
    
    fileTransferSetupPanel.add(targetAndDestinationsMainPanel, BorderLayout.NORTH);
    fileTransferSetupPanel.add(_createTimeStampFolderCheckBox, BorderLayout.SOUTH);
    
    _fileTransferSetupSplitPane.setBorder(null);
    _fileTransferSetupSplitPane.add(_targetsAndDestinationsScrollPane, JSplitPane.LEFT);
    _fileTransferSetupSplitPane.add(_allDestinationsPanel, JSplitPane.RIGHT);
    
    _targetsAndDestinationsScrollPane.getViewport().add(fileTransferSetupPanel);
    // set the scrollpane's border to an empty one so no border is shown. Setting it to null will not work
    _targetsAndDestinationsScrollPane.setBorder(BorderFactory.createEmptyBorder());

    _targetsAndDestinationsPanel.setLayout(_targetsAndDestinationsPanelPairLayout);
    _targetsAndDestinationsPanel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
    _targetsAndDestinationsPanel.setMinimumSize(new Dimension(0,0));   
    _targetsAndDestinationsPanel.add(_resultsFilesHeadeLabel);
    _targetsAndDestinationsPanel.add(_destinationDirectoriesHeaderLabel);
    _targetsAndDestinationsPanel.add(_cadXMLFileCheckBox);
    _targetsAndDestinationsPanel.add(_cadXMLPanel);
    _targetsAndDestinationsPanel.add(_settingsXMLFileCheckBox);
    _targetsAndDestinationsPanel.add(_settingsXMLPanel);
    _targetsAndDestinationsPanel.add(_indictmentsXMLFileCheckBox);
    _targetsAndDestinationsPanel.add(_indictmentsXMLPanel);
    _targetsAndDestinationsPanel.add(_repairImagesFileCheckBox);
    _targetsAndDestinationsPanel.add(_repairImagesPanel);
    _targetsAndDestinationsPanel.add(_measurementsXMLFileCheckBox);
    _targetsAndDestinationsPanel.add(_measurementsXMLPanel);

    _resultsFilesHeadeLabel.setForeground(_labelColor);

    _destinationDirectoriesHeaderLabel.setForeground(_labelColor);

    _cadXMLFileCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_CAD_XML_FILE_BUTTON_KEY"));
    _cadXMLFileCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadXMLFileCheckBox_actionPerformed();
      }
    });

    _cadXMLPanel.setLayout(_cadXMLPanelBorderLayout);
    _cadXMLPanel.add(_cadXMLDestinationsListScrollPane, BorderLayout.CENTER);
    _cadXMLPanel.add(_cadXMLTransferButtonsWrapperPanel, BorderLayout.EAST);

    _cadXMLDestinationsListScrollPane.getViewport().add(_cadXMLDestinationsList);
    _cadXMLDestinationsList.setVisibleRowCount(1);
    _cadXMLDestinationsList.setModel(_cadXMLDestinationsListModel);

    _cadXMLTransferButtonsWrapperPanel.setLayout(_cadXMLTransferButtonsWrapperPanelFlowLayout);
    _cadXMLTransferButtonsWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _cadXMLTransferButtonsWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _cadXMLTransferButtonsWrapperPanel.add(_cadXMLTransferButtonsPanel);


    _cadXMLTransferButtonsPanel.setLayout(_cadXMLTransferButtonsPanelGridLayout);
    _cadXMLTransferButtonsPanelGridLayout.setColumns(1);
    _cadXMLTransferButtonsPanelGridLayout.setHgap(0);
    _cadXMLTransferButtonsPanelGridLayout.setRows(2);
    _cadXMLTransferButtonsPanelGridLayout.setVgap(0);
    _cadXMLTransferButtonsPanel.add(_cadXMLLeftArrowButton);
    _cadXMLTransferButtonsPanel.add(_cadXMLRightArrowButton);

    _cadXMLLeftArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _cadXMLLeftArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadXMLLeftArrowButton_actionPerformed(e);
      }
    });

    _cadXMLRightArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW));
    _cadXMLRightArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cadXMLRightArrowButton_actionPerformed(e);
      }
    });

    _settingsXMLFileCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_SETTINGS_XML_FILE_BUTTON_KEY"));
    _settingsXMLFileCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        settingsXMLFileCheckBox_actionPerformed();
      }
    });

    _settingsXMLPanel.setLayout(_settingsXMLPanelBorderLayout);
    _settingsXMLPanel.add(_settingsXMLDestinationsListScrollPane, BorderLayout.CENTER);
    _settingsXMLPanel.add(_settingsXMLTransferButtonsWrapperPanel, BorderLayout.EAST);

    _settingsXMLDestinationsListScrollPane.getViewport().add(_settingsXMLDestinationsList);
    _settingsXMLDestinationsList.setVisibleRowCount(1);
    _settingsXMLDestinationsList.setModel(_settingsXMLDestinationsListModel);

    _settingsXMLTransferButtonsWrapperPanel.setLayout(_settingsXMLTransferButtonsWrapperPanelFlowLayout);
    _settingsXMLTransferButtonsWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _settingsXMLTransferButtonsWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _settingsXMLTransferButtonsWrapperPanel.add(_settingsXMLTransferButtonsPanel);

    _settingsXMLTransferButtonsPanel.setLayout(_settingsXMLTransferButtonsPanelGridLayout);
    _settingsXMLTransferButtonsPanelGridLayout.setColumns(1);
    _settingsXMLTransferButtonsPanelGridLayout.setHgap(0);
    _settingsXMLTransferButtonsPanelGridLayout.setRows(2);
    _settingsXMLTransferButtonsPanelGridLayout.setVgap(0);
    _settingsXMLTransferButtonsPanel.add(_settingsXMLLeftArrowButton);
    _settingsXMLTransferButtonsPanel.add(_settingsXMLRightArrowButton);

    _settingsXMLLeftArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _settingsXMLLeftArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        settingsXMLLeftArrowButton_actionPerformed(e);
      }
    });

    _settingsXMLRightArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW));
    _settingsXMLRightArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        settingsXMLRightArrowButton_actionPerformed(e);
      }
    });

    _indictmentsXMLFileCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_INDICTMENTS_XML_FILE_BUTTON_KEY"));
    _indictmentsXMLFileCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        indictmentsXMLFileCheckBox_actionPerformed();
      }
    });

    _indictmentsXMLPanel.setLayout(_indictmentsXMLPanelBorderLayout);
    _indictmentsXMLPanel.add(_indictmentsXMLDestinationsListScrollPane, BorderLayout.CENTER);
    _indictmentsXMLPanel.add(_indictmentsXMLTransferButtonsWrapperPanel, BorderLayout.EAST);

    _indictmentsXMLDestinationsListScrollPane.getViewport().add(_indictmentsXMLDestinationsList);
    _indictmentsXMLDestinationsList.setVisibleRowCount(1);
    _indictmentsXMLDestinationsList.setModel(_indictmentsXMLDestinationsListModel);

    _indictmentsXMLTransferButtonsWrapperPanel.setLayout(_indictmentsXMLTransferButtonsWrapperPanelFlowLayout);
    _indictmentsXMLTransferButtonsWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _indictmentsXMLTransferButtonsWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _indictmentsXMLTransferButtonsWrapperPanel.add(_indictmentsXMLTransferButtonsPanel);


    _indictmentsXMLTransferButtonsPanel.setLayout(_indictmentsXMLTransferButtonsPanelGridLayout);
    _indictmentsXMLTransferButtonsPanelGridLayout.setColumns(1);
    _indictmentsXMLTransferButtonsPanelGridLayout.setHgap(0);
    _indictmentsXMLTransferButtonsPanelGridLayout.setRows(2);
    _indictmentsXMLTransferButtonsPanelGridLayout.setVgap(0);
    _indictmentsXMLTransferButtonsPanel.add(_indictmentsXMLLeftArrowButton);
    _indictmentsXMLTransferButtonsPanel.add(_indictmentsXMLRightArrowButton);

    _indictmentsXMLLeftArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _indictmentsXMLLeftArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        indictmentsXMLLeftArrowButton_actionPerformed(e);
      }
    });

    _indictmentsXMLRightArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW));
    _indictmentsXMLRightArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        indictmentsXMLRightArrowButton_actionPerformed(e);
      }
    });

    _repairImagesFileCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_REPAIR_IMAGE_FILES_BUTTON_KEY"));
    _repairImagesFileCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairImagesFileCheckBox_actionPerformed();
      }
    });

    _repairImagesPanel.setLayout(_repairImagesPanelBorderLayout);
    _repairImagesPanel.add(_repairImagesDestinationsListScrollPane, BorderLayout.CENTER);
    _repairImagesPanel.add(_repairImagesTransferButtonsWrapperPanel, BorderLayout.EAST);

    _repairImagesDestinationsListScrollPane.getViewport().add(_repairImagesDestinationsList);
    _repairImagesDestinationsList.setVisibleRowCount(1);
    _repairImagesDestinationsList.setModel(_repairImagesDestinationsListModel);

    _repairImagesTransferButtonsWrapperPanel.setLayout(_repairImagesTransferButtonsWrapperPanelFlowLayout);
    _repairImagesTransferButtonsWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _repairImagesTransferButtonsWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _repairImagesTransferButtonsWrapperPanel.add(_repairImagesTransferButtonsPanel);

    _repairImagesTransferButtonsPanel.setLayout(_repairImagesTransferButtonsPanelGridLayout);
    _repairImagesTransferButtonsPanelGridLayout.setColumns(1);
    _repairImagesTransferButtonsPanelGridLayout.setHgap(0);
    _repairImagesTransferButtonsPanelGridLayout.setRows(2);
    _repairImagesTransferButtonsPanelGridLayout.setVgap(0);
    _repairImagesTransferButtonsPanel.add(_repairImagesLeftArrowButton);
    _repairImagesTransferButtonsPanel.add(_repairImagesRightArrowButton);

    _repairImagesLeftArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _repairImagesLeftArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairImagesLeftArrowButton_actionPerformed(e);
      }
    });

    _repairImagesRightArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW));
    _repairImagesRightArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairImagesRightArrowButton_actionPerformed(e);
      }
    });
    
    _measurementsXMLFileCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_MEASUREMENTS_XML_FILES_BUTTON_KEY"));
    _measurementsXMLFileCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measurementsXMLFileCheckBox_actionPerformed();
      }
    });

    _measurementsXMLPanel.setLayout(_measurementsXMLPanelBorderLayout);
    _measurementsXMLPanel.add(_measurementsXMLDestinationsListScrollPane, BorderLayout.CENTER);
    _measurementsXMLPanel.add(_measurementsXMLTransferButtonsWrapperPanel, BorderLayout.EAST);

    _measurementsXMLDestinationsListScrollPane.getViewport().add(_measurementsXMLDestinationsList);
    _measurementsXMLDestinationsList.setVisibleRowCount(1);
    _measurementsXMLDestinationsList.setModel(_measurementsXMLDestinationsListModel);

    _measurementsXMLTransferButtonsWrapperPanel.setLayout(_measurementsXMLTransferButtonsWrapperPanelFlowLayout);
    _measurementsXMLTransferButtonsWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _measurementsXMLTransferButtonsWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _measurementsXMLTransferButtonsWrapperPanel.add(_measurementsXMLTransferButtonsPanel);

    _measurementsXMLTransferButtonsPanel.setLayout(_measurementsXMLTransferButtonsPanelGridLayout);
    _measurementsXMLTransferButtonsPanelGridLayout.setColumns(1);
    _measurementsXMLTransferButtonsPanelGridLayout.setHgap(0);
    _measurementsXMLTransferButtonsPanelGridLayout.setRows(2);
    _measurementsXMLTransferButtonsPanelGridLayout.setVgap(0);
    _measurementsXMLTransferButtonsPanel.add(_measurementsXMLLeftArrowButton);
    _measurementsXMLTransferButtonsPanel.add(_measurementsXMLRightArrowButton);

    _measurementsXMLLeftArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_LEFT_ARROW));
    _measurementsXMLLeftArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measurementsXMLLeftArrowButton_actionPerformed(e);
      }
    });

    _measurementsXMLRightArrowButton.setIcon(Image5DX.getImageIcon(Image5DX.CT_RIGHT_ARROW));
    _measurementsXMLRightArrowButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        measurementsXMLRightArrowButton_actionPerformed(e);
      }
    });

    _allDestinationsPanel.setLayout(_allDestinationsPanelBorderLayout);
    _allDestinationsPanel.add(_allDestinationsListBoxPanel, BorderLayout.CENTER);
    _allDestinationsPanel.add(_allDestinationsActionWrapperPanel, BorderLayout.EAST);

    _allDestinationsListBoxPanel.setLayout(_allDestinationsListBoxPanelBorderLayout);
    _allDestinationsListBoxPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    _allDestinationsListBoxPanel.add(_allDestinationDirsHeaderLabel, BorderLayout.NORTH);
    _allDestinationsListBoxPanel.add(_allDestinationsListBoxScrollPane, BorderLayout.CENTER);

    _allDestinationDirsHeaderLabel.setForeground(_labelColor);

    _allDestinationsListBoxScrollPane.getViewport().add(_allDestinationsList);

    _allDestinationsList.setToolTipText(StringLocalizer.keyToString("CFGUI_RESPROC_ALL_DEST_LIST_TOOLTIP_KEY"));
    _allDestinationsList.setModel(_allDestinationsListModel);

    final ListSelectionModel destinationRowSelectionModel = _allDestinationsList.getSelectionModel();
     destinationRowSelectionModel.addListSelectionListener(new ListSelectionListener()
     {
       public void valueChanged(ListSelectionEvent e)
       {
         ListSelectionModel listSelectionModel = (ListSelectionModel)e.getSource();
//        if (lsm.isSelectionEmpty() == false)
         if (listSelectionModel.isSelectionEmpty() || listSelectionModel.getValueIsAdjusting())
         {
           // do nothing
         }
         else
         {
           int selectedRow = _allDestinationsList.getSelectedIndex();
           setDeleteDestinationButtonBasedOnTableSelection(selectedRow);
           setEditDestinationButtonBasedOnTableSelection(selectedRow);
         }
       }
    });


    _allDestinationsActionWrapperPanel.setLayout(_allDestinationsActionWrapperPanelFlowLayout);
    _allDestinationsActionWrapperPanelFlowLayout.setVerticalAlignment(VerticalFlowLayout.CENTER);
    _allDestinationsActionWrapperPanelFlowLayout.setHorizontalAlignment(VerticalFlowLayout.MIDDLE);
    _allDestinationsActionWrapperPanel.add(_allDestinationsActionPanel);

    _allDestinationsActionPanel.setLayout(_allDestinationsActionPanelGridLayout);
//    _allDestinationsActionPanelGridLayout.setColumns(1);
    _allDestinationsActionPanelGridLayout.setHgap(10);
    _allDestinationsActionPanelGridLayout.setRows(4);
    _allDestinationsActionPanelGridLayout.setVgap(10);
    _allDestinationsActionPanel.add(_browseDestinationButton);
    _allDestinationsActionPanel.add(_addDestinationButton);
    _allDestinationsActionPanel.add(_editDestinationButton);
    _allDestinationsActionPanel.add(_deleteDestinationButton);

    _browseDestinationButton.setText(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _browseDestinationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        browseDestinationButton_actionPerformed(e);
      }
    });

    _addDestinationButton.setText(StringLocalizer.keyToString("CFGUI_ADD_DOTDOTDOT_BUTTON_KEY"));
    _addDestinationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addDestinationButton_actionPerformed(e);
      }
    });

    _editDestinationButton.setText(StringLocalizer.keyToString("CFGUI_EDIT_DOTDOTDOT_BUTTON_KEY"));
    _editDestinationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        editDestinationButton_actionPerformed(e);
      }
    });

    _deleteDestinationButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteDestinationButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteDestinationButton_actionPerformed(e);
      }
    });


    _repairToolPanel.setLayout(_repairToolPanelBorderLayout);
    _repairToolPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_RESPROC_MEDALIST_REPAIR_TOOL_SETUP_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 5, 5, 5)));
    _repairToolPanel.add(_enableRepairPanel, BorderLayout.NORTH);
    _repairToolPanel.add(_repairToolInstallDirPanel, BorderLayout.SOUTH);

    _enableRepairPanel.setLayout(_enableRepairPanelFlowLayout);
    _enableRepairPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _enableRepairPanel.add(_enableRepairCheckBox);

    _enableRepairCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_MRT_ENABLE_BUTTON_KEY"));
    _enableRepairCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableRepairCheckBox_actionPerformed(e);
      }
    });

    _repairToolInstallDirPanel.setLayout(_repairToolInstallDirPanelFlowLayout);
    _repairToolInstallDirPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _repairToolInstallDirPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0)); // to indent the install dir line
    _repairToolInstallDirPanel.add(_repairToolInstallDirLabel);
    _repairToolInstallDirPanel.add(_repairToolInstallDirTextField);
    _repairToolInstallDirPanel.add(_repairToolInstallDirBrowseButton);

    _repairToolInstallDirLabel.setText(StringLocalizer.keyToString("CFGUI_RESPROC_INSTALLATION_DIRECTORY_LABEL_KEY"));

    // hook up all action listeners
    _repairToolInstallDirTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        repairToolInstallDirTextField_setValue();
      }
    });

    _repairToolInstallDirTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairToolInstallDirTextField_setValue();
      }
    });

    _repairToolInstallDirBrowseButton.setText(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _repairToolInstallDirBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairToolInstallDirBrowseButton_actionPerformed(e);
      }
    });
    
    _createTimeStampFolderCheckBox.setText(StringLocalizer.keyToString("CFGUI_RESPROC_CREATE_TIMESTAMP_FOLDER_BUTTON_KEY"));
    _createTimeStampFolderCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createTimeStampFolderCheckBox_actionPerformed();
      }
    });

    // define menus and toolBar items to be added later
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ResultProcessingTaskPanel().start()");

    resetStatusBar();

    // set up for custom menus and tool bar
    startMenusAndToolBar();

     // add custom menus


     restorePersistSettings();
    // read resultsProcessor.config only if this is the first time when the user navigates to this task
    // for subsequent re-entry into this task, assume the fields have already been populated
    if (_currentSelectedRule == null)
      populateWithConfigData();
    else
      populateWithRuleData(_currentSelectedRule);

    setSaveButtonsState();
    setDeleteButtonsState();

    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    // if anything has been modified need to ask the user if they want to save changes before leaving.
    // if yes, save and leave - return true
    // if no, stay put - return false
    boolean readyToFinish = true;
    if (repairToolPathIsSet() == false)
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("CFGUI_RESPROC_NO_MRT_PATH_SET_ERROR_KEY"),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      readyToFinish = false;
    }
    if (readyToFinish)
    {
      if (_rulesTable.isEditing())
        _rulesTable.getCellEditor().cancelCellEditing();
      if (_conditionsTable.isEditing())
        _conditionsTable.getCellEditor().cancelCellEditing();

      return saveRuleIfNecessary();
    }
    else
      return false;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    savePersistSettings();
    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("ResultProcessingTaskPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void populateWithConfigData()
  {
    boolean processResults = _resultsProcessor.isResultsProcessingEnabled();
    _enableProcessingCheckBox.setSelected(processResults);

    boolean enableCreateTimeStampFolder = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER);
    _createTimeStampFolderCheckBox.setSelected(enableCreateTimeStampFolder);
    // read the results processing configuration file
    try
    {
      _resultsProcessor.loadFromConfigurationFile();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }

    _ruleList = _resultsProcessor.getCopyOfRuleList();
    if (_ruleList.isEmpty())
      _currentSelectedRule = null;

    _processRuleSelection = false;
    populateRuleList();
    _processRuleSelection = true;
    // populate the conditions table with any regular expressions already defined for the selected rule,
    // if any rule is selected, otherwise, leave the table empty
    if (_currentSelectedRule != null)
    {
      populateConditionTable();
      // populate the file transfer panel
      populateFileTransferPanel();
    }

    populateRepairToolPanel();
    setupTableEditors();

    setDeleteButtonsState();
    setEnabledRepairToolInstallDir(_enableRepairCheckBox.isSelected());
    setRepairToolState(processResults);
//    setEnabledAllComponents(processResults);
  }

  /**
   * @author Laura Cormos
   */
  private void setRepairToolState(boolean processResults)
  {
    // if the overall result processing is disabled, we want to automatically unselect (and disable) the MRT option
    if (processResults == false)
    {
      _enableRepairCheckBox.setSelected(false);
      _enableRepairCheckBox.setEnabled(false);
    }
    setEnabledRepairToolInstallDir(processResults && _enableRepairCheckBox.isSelected());
  }

  /**
   * @author Laura Cormos
   */
  private void populateRuleList()
  {
    _rulesTableModel.setData(_ruleList);
    // if there are no rules yet defined, disable the configuration panel
    if(_ruleList.isEmpty())
      setEnabledRulesDefinitionPanel(false);
    else
    {
      _rulesTable.setRowSelectionInterval(0,0);
      _rulesTableModel.setSelectedRow(0);
      setCurrentRule(_rulesTableModel.getRuleAt(0));
//      _currentSelectedRule = _rulesTableModel.getRuleAt(0);
//      _conditionsTableModel.setCurentRule(_currentSelectedRule);
    }

    // enable/disable rules buttons based on results processing checkbox
    setRuleButtonsState(_enableProcessingCheckBox.isSelected());
  }

  /**
   * @author Laura Cormos
   */
  private void newRuleButton_actionPerformed(ActionEvent e)
  {
    if (saveRuleIfNecessary() == false)
      return;
    String newRuleName = JOptionPane.showInputDialog(this,
                                                     StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_NAME_KEY"),
                                                     StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_TITLE_KEY"),
                                                     JOptionPane.INFORMATION_MESSAGE);
    if (newRuleName != null) // null means the dialog was canceled
    {
      newRuleName = getLegalFileName(newRuleName);
      if (newRuleName == null)
        return;
      // create a new rule
      Rule newRule = new Rule();
      newRule.setName(newRuleName);
      _resultsProcessor.appendRule(newRule);
      addRuleToGUI(newRule);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateWithRuleData(Rule rule)
  {
    Assert.expect(rule != null);

    setCurrentRule(rule);
    setupTableEditors();

    // enable rule definition panel
    setEnabledRulesDefinitionPanel(_resultsProcessor.isResultsProcessingEnabled());
    populateConditionTable();
    populateFileTransferPanel();
  }
  /**
   * @author Laura Cormos
   */
  private void deleteRuleButton_actionPerformed(ActionEvent e)
  {
    int selectedRowIndex = _rulesTable.getSelectedRow();
    Rule selectedRule = _rulesTableModel.getRuleAt(selectedRowIndex);
    Assert.expect(selectedRule == _currentSelectedRule);
    _rulesTableModel.removeRule(selectedRule);

    int ruleIndex = getIndexForRule(selectedRule);
    Assert.expect(ruleIndex >= 0);
    _resultsProcessor.removeRule(ruleIndex);

    // save the changes to disk
    try
    {
      _resultsProcessor.saveToConfigurationFile();
      setCurrentSelectedRuleIsDirty(false);
      clearUndoStack();

      // set another selection when the current rule is removed
      int lastRowIndex = _rulesTableModel.getRowCount() - 1;
      if (lastRowIndex >= 0)
      {
        if (selectedRowIndex > lastRowIndex)
        {
          _rulesTable.setRowSelectionInterval(lastRowIndex, lastRowIndex);
          setCurrentRule(_rulesTableModel.getRuleAt(lastRowIndex));
//          _currentSelectedRule = _rulesTableModel.getRuleAt(lastRowIndex);
        }
        else
        {
          _rulesTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
          setCurrentRule(_rulesTableModel.getRuleAt(selectedRowIndex));
//          _currentSelectedRule = _rulesTableModel.getRuleAt(selectedRowIndex);
        }
      }
      else
      {
        // clear the gui components since the last rule was deleted from the table
        clearAllComponents();
        setEnabledRulesDefinitionPanel(false);
      }
      setSaveButtonsState();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void saveRuleButton_actionPerformed(ActionEvent e)
  {
    saveCurrentRule();
  }

  /**
   * @author Laura Cormos
   */
  private void saveAsRuleButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_currentSelectedRule != null);

    // pop a dialog for the new rule name
    String newRuleName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newRuleName != null) // null means the dialog was canceled
    {
      newRuleName = getLegalFileName(newRuleName);
      if (newRuleName == null)
        return;

      // copy existing rule into the new rule with the given name
      Rule newRule;
      if (_currentSelectedRuleIsDirty)
      {
        newRule = new Rule();
        setCurrentRule(newRule);
        _currentSelectedRule.setName(newRuleName);
        // set condition dirty flag to force conditions from current rule to be saved with the new rule
        _conditionIsDirty = true;
        saveCurrentRule(); // this also adds the rule to results processor and saves the config file to disk
      }
      else
      {
        newRule = _currentSelectedRule.createCopy(newRuleName);
        // append new rule to the results processor
        _resultsProcessor.appendRule(newRule);

        try
        {
          _resultsProcessor.saveToConfigurationFile();

          _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_RESPROC_RULE_SAVED_OK_MESSAGE_KEY",
                                                                                   new Object[]{newRule.getName()})));
        }
        catch (DatastoreException ex)
        {
          showDatastoreError(ex);
          return;
        }
      }

      clearUndoStack();
      addRuleToGUI(newRule);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void populateConditionTable()
  {
    Assert.expect(_currentSelectedRule != null);

    Collection<Condition> conditions = _currentSelectedRule.getConditions();
    _conditionsTableModel.setData(new ArrayList<Condition>(conditions));
    setDeleteConditionButtonBasedOnTableSelection(_conditionsTable.getSelectedRow());
  }

  /**
   * @author Laura Cormos
   */
  private void populateFileTransferPanel()
  {
    Assert.expect(_currentSelectedRule != null);

    // populate the all destinations Jlists
    _allDestinationsList.removeAll();
    _allDestinationsListModel.clear();
    Collection<String> allDirectories = _currentSelectedRule.getDirectoriesUsedInTargets();
    if (allDirectories.isEmpty())
    {
      setEnabledLeftArrowButtons(false);
      _allDestinationsListModel.clear();
    }
    else
    {
      for (String directory : allDirectories)
        _allDestinationsListModel.addElement(directory);
      setEnabledLeftArrowButtons(true);
    }

    java.util.List<String> destinationsList;
    // populate each individual target and its destinations list
    _cadXMLDestinationsList.removeAll();
    _cadXMLDestinationsListModel.clear();
    if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE))
    {
      _cadXMLFileCheckBox.setSelected(true);
      Target cadXML = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE);
      destinationsList = cadXML.getDestinationList();
      if (destinationsList.isEmpty())
        _cadXMLRightArrowButton.setEnabled(false);
      else
      {
        for (String directory : destinationsList)
          _cadXMLDestinationsListModel.addElement(directory);
        _cadXMLRightArrowButton.setEnabled(true);
      }
    }
    else
    {
      _cadXMLFileCheckBox.setSelected(false);
      _cadXMLRightArrowButton.setEnabled(false);
      _cadXMLLeftArrowButton.setEnabled(false);
      _cadXMLDestinationsListModel.clear();
    }
    _settingsXMLDestinationsList.removeAll();
    _settingsXMLDestinationsListModel.clear();
    if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE))
    {
      _settingsXMLFileCheckBox.setSelected(true);
      Target settingsXML = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE);
      destinationsList = settingsXML.getDestinationList();
      if (destinationsList.isEmpty())
        _settingsXMLRightArrowButton.setEnabled(false);
      else
      {
        for (String directory : destinationsList)
          _settingsXMLDestinationsListModel.addElement(directory);
        _settingsXMLRightArrowButton.setEnabled(true);
      }
    }
    else
    {
      _settingsXMLFileCheckBox.setSelected(false);
      _settingsXMLRightArrowButton.setEnabled(false);
      _settingsXMLLeftArrowButton.setEnabled(false);
      _settingsXMLDestinationsListModel.clear();
    }
    _indictmentsXMLDestinationsList.removeAll();
    _indictmentsXMLDestinationsListModel.clear();
    if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE))
    {
      _indictmentsXMLFileCheckBox.setSelected(true);
      Target indictmentsXML = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE);
      destinationsList = indictmentsXML.getDestinationList();
      if (destinationsList.isEmpty())
        _indictmentsXMLRightArrowButton.setEnabled(false);
      else
      {
        for (String directory : destinationsList)
          _indictmentsXMLDestinationsListModel.addElement(directory);
        _indictmentsXMLRightArrowButton.setEnabled(true);
      }
    }
    else
    {
      _indictmentsXMLFileCheckBox.setSelected(false);
      _indictmentsXMLRightArrowButton.setEnabled(false);
      _indictmentsXMLLeftArrowButton.setEnabled(false);
      _indictmentsXMLDestinationsListModel.clear();
    }
    _repairImagesDestinationsList.removeAll();
    _repairImagesDestinationsListModel.clear();
    if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES))
    {
      _repairImagesFileCheckBox.setSelected(true);
      Target repairImages = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES);
      destinationsList = repairImages.getDestinationList();
      if (destinationsList.isEmpty())
        _repairImagesRightArrowButton.setEnabled(false);
      else
      {
        for (String directory : destinationsList)
          _repairImagesDestinationsListModel.addElement(directory);
        _repairImagesRightArrowButton.setEnabled(true);
      }
    }
    else
    {
      _repairImagesFileCheckBox.setSelected(false);
      _repairImagesRightArrowButton.setEnabled(false);
      _repairImagesLeftArrowButton.setEnabled(false);
      _repairImagesDestinationsListModel.clear();
    }
    
    _measurementsXMLDestinationsList.removeAll();
    _measurementsXMLDestinationsListModel.clear();
    if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE))
    {
      _measurementsXMLFileCheckBox.setSelected(true);
      Target measurementsXML = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE);
      destinationsList = measurementsXML.getDestinationList();
      if (destinationsList.isEmpty())
      {
        _measurementsXMLRightArrowButton.setEnabled(false);
      }
      else
      {
        for (String directory : destinationsList)
        {
          _measurementsXMLDestinationsListModel.addElement(directory);
        }
        if(_isGenerateMeasurementsXMLEnabled)
        {
          _measurementsXMLRightArrowButton.setEnabled(true);
        }
        else
        {
          _measurementsXMLRightArrowButton.setEnabled(false);
        }
      }
    }
    else
    {
      _measurementsXMLFileCheckBox.setSelected(false);
      _measurementsXMLRightArrowButton.setEnabled(false);
      _measurementsXMLLeftArrowButton.setEnabled(false);
      _measurementsXMLDestinationsListModel.clear();
    }
    
    setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    setEditDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    if (_enableProcessingCheckBox.isSelected() == false)
      setEnabledAllComponents(false);
    else if (_cadXMLFileCheckBox.isSelected() || _settingsXMLFileCheckBox.isSelected() || _indictmentsXMLFileCheckBox.isSelected() || _repairImagesFileCheckBox.isSelected()||_measurementsXMLFileCheckBox.isSelected())  
      _createTimeStampFolderCheckBox.setEnabled(true);
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledRulesDefinitionPanel(boolean enabled)
  {
    _conditionKeySelectLabel.setEnabled(enabled);
    _conditionKeySelectComboBox.setEnabled(enabled);
    _addConditionRowButton.setEnabled(enabled);
    _deleteConditionRowButton.setEnabled(enabled);
    _conditionKeyRegularExpressionScrollPane.setEnabled(enabled);
    _conditionsTable.setEnabled(enabled);
    _cadXMLFileCheckBox.setEnabled(enabled);
    _cadXMLDestinationsList.setEnabled(enabled);
    _cadXMLLeftArrowButton.setEnabled(enabled);
    _cadXMLRightArrowButton.setEnabled(enabled);
    _settingsXMLFileCheckBox.setEnabled(enabled);
    _settingsXMLDestinationsList.setEnabled(enabled);
    _settingsXMLLeftArrowButton.setEnabled(enabled);
    _settingsXMLRightArrowButton.setEnabled(enabled);
    _indictmentsXMLFileCheckBox.setEnabled(enabled);
    _indictmentsXMLDestinationsList.setEnabled(enabled);
    _indictmentsXMLLeftArrowButton.setEnabled(enabled);
    _indictmentsXMLRightArrowButton.setEnabled(enabled);
    _repairImagesFileCheckBox.setEnabled(enabled);
    _repairImagesDestinationsList.setEnabled(enabled);
    _repairImagesLeftArrowButton.setEnabled(enabled);
    _repairImagesRightArrowButton.setEnabled(enabled);
    _allDestinationsList.setEnabled(enabled);
    _browseDestinationButton.setEnabled(enabled);
    _addDestinationButton.setEnabled(enabled);
    _editDestinationButton.setEnabled(enabled);
    _deleteDestinationButton.setEnabled(enabled);
    _createTimeStampFolderCheckBox.setEnabled(enabled);
    
    //need to check everytime if the enable generate measurements XML option is turned on 
    _isGenerateMeasurementsXMLEnabled = Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE);
    if (_isGenerateMeasurementsXMLEnabled && enabled)
    {
      _measurementsXMLFileCheckBox.setEnabled(true);
      _measurementsXMLDestinationsList.setEnabled(true);
      _measurementsXMLLeftArrowButton.setEnabled(true);
      _measurementsXMLRightArrowButton.setEnabled(true);
    }
    else
    {
      _measurementsXMLFileCheckBox.setEnabled(false);
      _measurementsXMLDestinationsList.setEnabled(false);
      _measurementsXMLLeftArrowButton.setEnabled(false);
      _measurementsXMLRightArrowButton.setEnabled(false);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setupTableEditors()
  {
    _rulesTable.setPreferredColumnWidths();
    _rulesTable.setEditorsAndRenderers();
    setRulesTablePreferredSize();
    _conditionsTable.setPreferredColumnWidths();
    _conditionsTable.setEditorsAndRenderers();
  }

  /**
   * @author Laura Cormos
   */
  private void setRulesTablePreferredSize()
  {
    int padding = 400;
    int origTableWidth = _rulesTable.getMinimumSize().width;
    int scrollHeight = _rulesTableScrollPane.getPreferredSize().height;
//    int tableHeight = _rulesTable.getPreferredSize().height;
    int tableHeight = _rulesTable.getMaximumHeight();
    int prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
      _rulesTable.setPreferredScrollableViewportSize(prefScrollSize);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void addConditionRowButton_actionPerformed(ActionEvent e)
  {
    _conditionsTableModel.addNewRow();
    _conditionsTable.setRowSelectionInterval(_conditionsTableModel.getRowCount() - 1, _conditionsTableModel.getRowCount() - 1);
    setCurrentSelectedRuleIsDirty(true);
    _conditionIsDirty = true;

    // scroll to make the newly added row visible
    SwingUtils.scrollTableToShowSelection(_conditionsTable);

    setupTableEditors();
  }

  /**
   * @author Laura Cormos
   */
  private void deleteConditionRowButton_actionPerformed(ActionEvent e)
  {
    int selectedRowIndex = _conditionsTable.getSelectedRow();

    try
    {
      _commandManager.execute(new ConfigDeleteResultsConditionCommand(_conditionsTableModel, selectedRowIndex));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      return;
    }

    // set another selection when the current rule is removed
    int lastRowIndex = _conditionsTableModel.getRowCount() - 1;
    if (lastRowIndex >= 0)
    {
      if (selectedRowIndex > lastRowIndex)
      {
        _conditionsTable.setRowSelectionInterval(lastRowIndex, lastRowIndex);
        _conditionsTableModel.setSelectedRow(lastRowIndex);
      }
      else
      {
        _conditionsTable.setRowSelectionInterval(selectedRowIndex, selectedRowIndex);
        _conditionsTableModel.setSelectedRow(selectedRowIndex);
      }
    }
    setAddConditionButtonState();
  }

  /**
   * @author Laura Cormos
   */
  private void browseDestinationButton_actionPerformed(ActionEvent e)
  {
    String selectedDirectory = selectDirectory();
    if (selectedDirectory != null)
    {
      selectedDirectory = FileUtil.removeEndFileSeparator(selectedDirectory);
      selectedDirectory += File.separator;
      _allDestinationsListModel.addElement(selectedDirectory);
      // select the last element added
      _allDestinationsList.setSelectedIndex(_allDestinationsListModel.size());
      setEnabledLeftArrowButtonsIfTargetExists(true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void addDestinationButton_actionPerformed(ActionEvent e)
  {
    String newDirectoryName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_RESPROC_ENTER_DIRECTORY_TO_ADD_LABEL_KEY"),
        StringLocalizer.keyToString("CFGUI_RESPROC_NEW_DEST_DIR_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newDirectoryName != null) // null means the dialog was canceled
    {
      while (newDirectoryName != null && FileUtilAxi.isDirectoryPathFileSystemLegal(newDirectoryName) == false)
      {
        JOptionPane.showMessageDialog(_mainUI,
                                      StringLocalizer.keyToString("CFGUI_RESPROC_ILLEGAL_DIRECTORY_NAME_CHARS_MSG_KEY"),
                                      StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                      JOptionPane.ERROR_MESSAGE);
        newDirectoryName = (String)JOptionPane.showInputDialog(
            _mainUI,
            StringLocalizer.keyToString("CFGUI_RESPROC_ENTER_DIRECTORY_TO_ADD_LABEL_KEY"),
            StringLocalizer.keyToString("CFGUI_RESPROC_NEW_DEST_DIR_TITLE_KEY"),
            JOptionPane.INFORMATION_MESSAGE,
            null,
            null,
            newDirectoryName);
      }
      if (newDirectoryName == null)
        return; // dialog was canceled
      newDirectoryName = FileUtilAxi.removeEndFileSeparator(newDirectoryName);
      newDirectoryName += File.separator;
      _allDestinationsListModel.addElement(newDirectoryName);
      // select the last element added
      _allDestinationsList.setSelectedIndex(_allDestinationsListModel.size()-1);
      setEnabledLeftArrowButtonsIfTargetExists(true);
    }
    setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    setEditDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
  }


  /**
   * @author Laura Cormos
   */
  private void editDestinationButton_actionPerformed(ActionEvent e)
  {
    Object selectedItem = _allDestinationsList.getSelectedValue();
    String newDirectoryName = (String)JOptionPane.showInputDialog(
      _mainUI,
      StringLocalizer.keyToString("CFGUI_RESPROC_ENTER_DIRECTORY_TO_EDIT_LABEL_KEY"),
      StringLocalizer.keyToString("CFGUI_RESPROC_NEW_DEST_DIR_TITLE_KEY"),
      JOptionPane.INFORMATION_MESSAGE,
      null,
      null,
      selectedItem);
    if (newDirectoryName != null) // null means the dialog was canceled
    {
      newDirectoryName = FileUtil.removeEndFileSeparator(newDirectoryName);
      newDirectoryName += File.separator;
      // remove the old name
      _allDestinationsListModel.removeElement(selectedItem);
      // put in the new name
      _allDestinationsListModel.addElement(newDirectoryName);
      // select the last element added
      _allDestinationsList.setSelectedIndex(_allDestinationsListModel.size()-1);
      setEnabledLeftArrowButtonsIfTargetExists(true);
    }
    setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    setEditDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
  }

  /**
   * @author Laura Cormos
   */
  private void deleteDestinationButton_actionPerformed(ActionEvent e)
  {
    Object object = _allDestinationsList.getSelectedValue();
    Assert.expect(object instanceof String);

    try
    {
      _commandManager.execute(new ConfigDeleteResultsTargetDestinationCommand(_allDestinationsListModel, object));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      return;
    }

    if (_allDestinationsListModel.isEmpty())
      setEnabledLeftArrowButtons(false);
    setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    setEditDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
  }

  /**
   * @author Laura Cormos
   */
  private String selectDirectory()
  {
    String dirName = null;

    if (_fileChooser == null)
    {
      _fileChooser = new JFileChooser();

      // initialize the JFileChooser
      FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
      _fileChooser.removeChoosableFileFilter(fileFilter);

      _fileChooser.setFileHidingEnabled(false);
      _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    if (_fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) !=
        JFileChooser.APPROVE_OPTION)
      return null;

    File selectedFile = _fileChooser.getSelectedFile();

    if (selectedFile.exists() == false)
    {
      int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 StringLocalizer.keyToString(new LocalizedString(
                                                     "CFGUI_RESPROC_CREATE_DIR_DIALOG_LABEL_KEY", new Object[]{selectedFile.getName()})),
                                                 StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                                 JOptionPane.YES_NO_OPTION);
      if (answer != JOptionPane.YES_OPTION)
        return dirName;
      try
      {
        FileUtil.createDirectory(selectedFile.getAbsolutePath());
      }
      catch (CouldNotCreateFileException ex)
      {
        JOptionPane.showMessageDialog(_mainUI,
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
    dirName = selectedFile.getPath();
    dirName = dirName.replace('/', '\\');

    if (dirName.endsWith("\\.")) // current directory
      dirName = dirName.substring(0, dirName.length() - 2);
    else if (dirName.endsWith("\\"))
      dirName = dirName.substring(0, dirName.length() - 1);

    return dirName;
  }

  /**
   * @author Laura Cormos
   */
  private void repairImagesLeftArrowButton_actionPerformed(ActionEvent e)
  {
    Object [] selectedValues = _allDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_repairImagesDestinationsListModel.contains(selectedDirectory) == false)
        _repairImagesDestinationsListModel.addElement(selectedDirectory);
    }
    if (selectedValues.length != 0)
    {
      _repairImagesRightArrowButton.setEnabled(true);
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void indictmentsXMLLeftArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _allDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_indictmentsXMLDestinationsListModel.contains(selectedDirectory) == false)
        _indictmentsXMLDestinationsListModel.addElement(selectedDirectory);
    }
    if (selectedValues.length != 0)
    {
      _indictmentsXMLRightArrowButton.setEnabled(true);
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void settingsXMLLeftArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _allDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_settingsXMLDestinationsListModel.contains(selectedDirectory) == false)
        _settingsXMLDestinationsListModel.addElement(selectedDirectory);
    }
    if (selectedValues.length != 0)
    {
      _settingsXMLRightArrowButton.setEnabled(true);
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void cadXMLLeftArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _allDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_cadXMLDestinationsListModel.contains(selectedDirectory) == false)
        _cadXMLDestinationsListModel.addElement(selectedDirectory);
    }
    if (selectedValues.length != 0)
    {
      _cadXMLRightArrowButton.setEnabled(true);
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
    }
  }
  
  /**
   * @author Janan Wong - XCR-3836
   */
  private void measurementsXMLLeftArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _allDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String) object;
      if (_measurementsXMLDestinationsListModel.contains(selectedDirectory) == false)
      {
        _measurementsXMLDestinationsListModel.addElement(selectedDirectory);
      }
    }
    if (selectedValues.length != 0)
    {
      _measurementsXMLRightArrowButton.setEnabled(true);
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void repairImagesRightArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _repairImagesDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_allDestinationsListModel.contains(selectedDirectory) == false)
        _allDestinationsListModel.addElement(selectedDirectory);
      _repairImagesDestinationsListModel.removeElement(selectedDirectory);
    }
    if (_repairImagesDestinationsListModel.isEmpty())
      _repairImagesRightArrowButton.setEnabled(false);
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void indictmentsXMLRightArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _indictmentsXMLDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_allDestinationsListModel.contains(selectedDirectory) == false)
        _allDestinationsListModel.addElement(selectedDirectory);
      _indictmentsXMLDestinationsListModel.removeElement(selectedDirectory);
    }
    if (_indictmentsXMLDestinationsListModel.isEmpty())
      _indictmentsXMLRightArrowButton.setEnabled(false);
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void settingsXMLRightArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _settingsXMLDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String)object;
      if (_allDestinationsListModel.contains(selectedDirectory) == false)
        _allDestinationsListModel.addElement(selectedDirectory);
      _settingsXMLDestinationsListModel.removeElement(selectedDirectory);
    }
    if (_settingsXMLDestinationsListModel.isEmpty())
      _settingsXMLRightArrowButton.setEnabled(false);
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void cadXMLRightArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _cadXMLDestinationsList.getSelectedValues();
     for (Object object : selectedValues)
     {
       Assert.expect(object instanceof String);
       String selectedDirectory = (String)object;
       if (_allDestinationsListModel.contains(selectedDirectory) == false)
         _allDestinationsListModel.addElement(selectedDirectory);
       _cadXMLDestinationsListModel.removeElement(selectedDirectory);
     }
     if (_cadXMLDestinationsListModel.isEmpty())
      _cadXMLRightArrowButton.setEnabled(false);
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }
  
  /**
   * @author Janan Wong - XCR-3836
   */
  private void measurementsXMLRightArrowButton_actionPerformed(ActionEvent e)
  {
    Object[] selectedValues = _measurementsXMLDestinationsList.getSelectedValues();
    for (Object object : selectedValues)
    {
      Assert.expect(object instanceof String);
      String selectedDirectory = (String) object;
      if (_allDestinationsListModel.contains(selectedDirectory) == false)
      {
        _allDestinationsListModel.addElement(selectedDirectory);
      }
      _measurementsXMLDestinationsListModel.removeElement(selectedDirectory);
    }
    if (_measurementsXMLDestinationsListModel.isEmpty())
    {
      _measurementsXMLRightArrowButton.setEnabled(false);
    }
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledLeftArrowButtons(boolean enabled)
  {
    _cadXMLLeftArrowButton.setEnabled(enabled);
    _settingsXMLLeftArrowButton.setEnabled(enabled);
    _indictmentsXMLLeftArrowButton.setEnabled(enabled);
    _repairImagesLeftArrowButton.setEnabled(enabled);
    if (_isGenerateMeasurementsXMLEnabled)
    {
      _measurementsXMLLeftArrowButton.setEnabled(enabled);
    }
    else
    {
      _measurementsXMLLeftArrowButton.setEnabled(false);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledLeftArrowButtonsIfTargetExists(boolean enabled)
  {
    if (_currentSelectedRule != null)
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE) || _cadXMLFileCheckBox.isSelected())
        _cadXMLLeftArrowButton.setEnabled(enabled);
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE) || _settingsXMLFileCheckBox.isSelected())
        _settingsXMLLeftArrowButton.setEnabled(enabled);
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE) || _indictmentsXMLFileCheckBox.isSelected())
        _indictmentsXMLLeftArrowButton.setEnabled(enabled);
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES) || _repairImagesFileCheckBox.isSelected())
        _repairImagesLeftArrowButton.setEnabled(enabled);
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE) || _measurementsXMLFileCheckBox.isSelected())
        _measurementsXMLLeftArrowButton.setEnabled(enabled);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void enableRepairCheckBox_actionPerformed(ActionEvent e)
  {
    boolean mrtEnabled = _enableRepairCheckBox.isSelected();

    try
    {
      _commandManager.execute(new ConfigEnableRepairDataTransferCommand(mrtEnabled));
      // set path to repair station agent based on environment variable
      if (mrtEnabled)
      {
        String inputDirPath = System.getenv("MRT_X6000_AGENT_INSTALL_PATH");
        if (inputDirPath != null)
        {
          _repairToolInstallDirTextField.setText(inputDirPath);
          repairToolInstallDirTextField_setValue();
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
      return;
    }

    // set the directory elements in sync with the checkbox state
    setEnabledRepairToolInstallDir(mrtEnabled);
  }

  /**
   * @author Laura Cormos
   */
  private void populateRepairToolPanel()
  {
    boolean mrtEnabled = _resultsProcessor.getIsRepairToolRuleEnabled();
    _enableRepairCheckBox.setSelected(mrtEnabled);

    // set the directory elements in sync with the checkbox state
    setEnabledRepairToolInstallDir(mrtEnabled);
    if (mrtEnabled)
      _repairToolInstallDirTextField.setText(_resultsProcessor.getRepairToolDirectory());
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledRepairToolInstallDir(boolean enabled)
  {
    _repairToolInstallDirLabel.setEnabled(enabled);
    _repairToolInstallDirTextField.setEnabled(enabled);
    _repairToolInstallDirBrowseButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void repairToolInstallDirTextField_setValue()
  {
    try
    {
      _commandManager.execute(new ConfigSetRepairToolInstallDirectoryCommand(_repairToolInstallDirTextField.getText()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI,
                                    ex,
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void repairToolInstallDirBrowseButton_actionPerformed(ActionEvent e)
  {
    String selectedDirectory = selectDirectory();
    if (selectedDirectory != null)
    {
      selectedDirectory = FileUtil.removeEndFileSeparator(selectedDirectory);

      _repairToolInstallDirTextField.setText(selectedDirectory);
      repairToolInstallDirTextField_setValue();
    }
  }

  /**
   * @author Laura Cormos
   */
  void setConditionDirtyFlag()
  {
    _conditionIsDirty = true;
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
    * @author Laura Cormos
    */
   private void resetStatusBar()
   {
     _mainUI.setStatusBarText("");
  }

  /**
   * @author Laura Cormos
   */
  boolean saveCurrentRule()
  {
    Assert.expect(_currentSelectedRule != null);

    if (_conditionIsDirty)
      if(_conditionsTableModel.saveCoditions() == false)
        return false;
    _conditionIsDirty = false;

    // cad XML target
    if (_cadXMLFileCheckBox.isSelected())
    {
      Target cadTarget;
      java.util.List<String> destinations = new ArrayList<String>();
      for (int i = 0; i < _cadXMLDestinationsListModel.size(); i++)
        destinations.add((String)_cadXMLDestinationsListModel.getElementAt(i));
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE))
      {
        cadTarget = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE);
        cadTarget.setDestinationList(destinations);
      }
      else
      {
        cadTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_CAD_FILE, destinations);
        _currentSelectedRule.addTarget(cadTarget);
      }
    }
    else
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE))
        _currentSelectedRule.removeTargetForResultFileType(ResultFileTypeEnum.XML_CAD_FILE);
    }
    // settings XML target
    if (_settingsXMLFileCheckBox.isSelected())
    {
      Target settingsTarget;
      java.util.List<String> destinations = new ArrayList<String>();
      for (int i = 0; i < _settingsXMLDestinationsListModel.size(); i++)
        destinations.add((String)_settingsXMLDestinationsListModel.getElementAt(i));
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE))
      {
        settingsTarget = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE);
        settingsTarget.setDestinationList(destinations);
      }
      else
      {
        settingsTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_SETTINGS_FILE, destinations);
        _currentSelectedRule.addTarget(settingsTarget);
      }
    }
    else
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE))
        _currentSelectedRule.removeTargetForResultFileType(ResultFileTypeEnum.XML_SETTINGS_FILE);
    }
    // indictments XML target
    if (_indictmentsXMLFileCheckBox.isSelected())
    {
      Target indictmentsTarget;
      java.util.List<String> destinations = new ArrayList<String>();
      for (int i = 0; i < _indictmentsXMLDestinationsListModel.size(); i++)
        destinations.add((String)_indictmentsXMLDestinationsListModel.getElementAt(i));
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE))
      {
        indictmentsTarget = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE);
        indictmentsTarget.setDestinationList(destinations);
      }
      else
      {
        indictmentsTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_INDICTMENTS_FILE, destinations);
        _currentSelectedRule.addTarget(indictmentsTarget);
      }
    }
    else
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE))
        _currentSelectedRule.removeTargetForResultFileType(ResultFileTypeEnum.XML_INDICTMENTS_FILE);
    }
    // repair images target
    if (_repairImagesFileCheckBox.isSelected())
    {
      Target repairImagesTarget;
      java.util.List<String> destinations = new ArrayList<String>();
      for (int i = 0; i < _repairImagesDestinationsListModel.size(); i++)
        destinations.add((String)_repairImagesDestinationsListModel.getElementAt(i));
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES))
      {
        repairImagesTarget = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES);
        repairImagesTarget.setDestinationList(destinations);
      }
      else
      {
        repairImagesTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.DEFECT_IMAGE_FILES, destinations);
        _currentSelectedRule.addTarget(repairImagesTarget);
      }
    }
    else
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES))
        _currentSelectedRule.removeTargetForResultFileType(ResultFileTypeEnum.DEFECT_IMAGE_FILES);
    }
    
    // Janan - XCR-3836 
    if (_measurementsXMLFileCheckBox.isSelected())
    {
      Target measurementsXMLTarget;
      java.util.List<String> destinations = new ArrayList<String>();
      for (int i = 0; i < _measurementsXMLDestinationsListModel.size(); i++)
      {
        destinations.add((String) _measurementsXMLDestinationsListModel.getElementAt(i));
      }
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE))
      {
        measurementsXMLTarget = _currentSelectedRule.getTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE);
        measurementsXMLTarget.setDestinationList(destinations);
      }
      else
      {
        measurementsXMLTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_MEASUREMENTS_FILE, destinations);
        _currentSelectedRule.addTarget(measurementsXMLTarget);
      }
    }
    else
    {
      if (_currentSelectedRule.hasTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE))
        _currentSelectedRule.removeTargetForResultFileType(ResultFileTypeEnum.XML_MEASUREMENTS_FILE);
    }

    // replace or append current rule in the results processor
    int ruleIndex = getIndexForRule(_currentSelectedRule);
    if (ruleIndex >= 0)
    {
      // replace an existing rule
      _resultsProcessor.removeRule(ruleIndex);
      _resultsProcessor.insertRule(_currentSelectedRule, ruleIndex);
    }
    else
     {
       // append a new rule
       _resultsProcessor.appendRule(_currentSelectedRule);
     }

    try
    {
      _resultsProcessor.saveToConfigurationFile();
      setCurrentSelectedRuleIsDirty(false);

      _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_RESPROC_RULE_SAVED_OK_MESSAGE_KEY",
                                                                               new Object[]{_currentSelectedRule.getName()})));
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      return false;
    }

    clearUndoStack();
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private void addRuleToGUI(Rule newRule)
  {
    Assert.expect(newRule != null);

    _ruleList.add(newRule);
    _rulesTableModel.setData(_ruleList);
    int ruleRowIndex = _rulesTableModel.getIndexForRule(newRule);
    _rulesTable.setRowSelectionInterval(ruleRowIndex, ruleRowIndex); // this will also populate the rule data on screen

    // set new added rule visible in the table
    SwingUtils.scrollTableToShowSelection(_rulesTable);
  }

  /**
   * @author Laura Cormos
   */
  private boolean saveRuleIfNecessary()
  {
    cancelEditingIfNecessary(_rulesTable);
    cancelEditingIfNecessary(_conditionsTable);

    if (_currentSelectedRuleIsDirty)
    {
      int answer = JOptionPane.showConfirmDialog(
          _envPanel,
          StringLocalizer.keyToString(new LocalizedString("CFGUI_RESPROC_SAVE_RULE_CHANGES_MESSAGE_KEY",
                                                          new Object[]{_currentSelectedRule.getName()})),
          StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
          JOptionPane.YES_NO_CANCEL_OPTION);
      if (answer == JOptionPane.CANCEL_OPTION)
        return false;
      else if (answer == JOptionPane.YES_OPTION)
        return saveCurrentRule();
    }
    setCurrentSelectedRuleIsDirty(false);
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private void cancelEditingIfNecessary(JTable table)
  {
    // cancel cell editing in case user deletes the row while editing is still going on.
    int column = table.getEditingColumn();
    if (column > -1)
    {
      TableCellEditor cellEditor = table.getColumnModel().getColumn(column).getCellEditor();
      Assert.expect(cellEditor != null);
      cellEditor.cancelCellEditing();
    }
  }

  /**
   * @author Laura Cormos
   */
  void showDatastoreError(DatastoreException ex)
  {
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Laura Cormos
   */
  private void setEnabledAllComponents(boolean enabled)
  {
    setEnabledRulesDefinitionPanel(enabled);
    _newRuleButton.setEnabled(enabled);
    _saveRuleAsButton.setEnabled(enabled);
    _saveRuleButton.setEnabled(enabled);
    _deleteRuleButton.setEnabled(enabled);
    _rulesTable.setEnabled(enabled);
    _enableRepairCheckBox.setEnabled(enabled);


  }

  /**
   * @author Laura Cormos
   */
  private void enableProcessingCheckBox_actionPerformed(ActionEvent e)
  {
    boolean processResults = _enableProcessingCheckBox.isSelected();
    if (processResults == false)
      if (saveRuleIfNecessary() == false)
        return;
    try
    {
      _commandManager.execute(new ConfigEnableResultsProcessingCommand(processResults));
      setEnabledAllComponents(processResults);
      setRuleButtonsState(processResults);
      setRepairToolState(processResults);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  void editCell(int rowIndex, int colIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(colIndex >= 0);

    _conditionsTable.setRowSelectionInterval(rowIndex, rowIndex);
    _conditionsTable.editCellAt(rowIndex, colIndex);
  }

  /**
   * @author Laura Cormos
   */
  String getLegalFileName(String newRuleName)
  {
    Assert.expect(newRuleName != null);

    java.util.List<String> tempRulesList = new ArrayList<String>();
    java.util.List<String> ruleNamesList = new ArrayList<String>();
    for (Rule rule : _ruleList)
      ruleNamesList.add(rule.getName());
    for (String name : ruleNamesList)
      tempRulesList.add(name.toUpperCase());

    String whiteSpacePattern = "^\\s*$"; // match an entry of all white space, such as ' ', 'tab', etc...
    String tempRuleSetName = newRuleName.toUpperCase();
    while (newRuleName != null &&
           (tempRulesList.contains(tempRuleSetName) || Pattern.matches(whiteSpacePattern, newRuleName)))
    {
      if (tempRulesList.contains(tempRuleSetName))
      {
        newRuleName = JOptionPane.showInputDialog(_mainUI,
                                                  StringLocalizer.keyToString(new LocalizedString(
                                                      "CFGUI_RESPROC_RULE_EXISTS_MESSAGE_KEY",
                                                      new Object[]{newRuleName})),
                                                  StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
        if (newRuleName != null)
          tempRuleSetName = newRuleName.toUpperCase();
        else
          return newRuleName;
      }
      else if (Pattern.matches(whiteSpacePattern, newRuleName))
      {
        newRuleName = JOptionPane.showInputDialog(_mainUI,
                                                  StringLocalizer.keyToString(new LocalizedString(
                                                      "CFGUI_RESPROC_RULE_ILLEGAL_NAME_KEY",
                                                      new Object[]{newRuleName, FileName.getIllegalChars()})),
                                                  StringLocalizer.keyToString("CFGUI_RESPROC_NEW_RULE_TITLE_KEY"),
                                                  JOptionPane.INFORMATION_MESSAGE);
        if (newRuleName != null)
          tempRuleSetName = newRuleName.toUpperCase();
        else
          return newRuleName;
      }
    }
    return newRuleName;
  }

  /**
   * @author Laura Cormos
   */
  private void cadXMLFileCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSelectCadXmlFileTargetCommand(_cadXMLFileCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void settingsXMLFileCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSelectSettingsXmlFileTargetCommand(_settingsXMLFileCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void indictmentsXMLFileCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSelectIndictmentsXmlFileTargetCommand(_indictmentsXMLFileCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void repairImagesFileCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSelectRepairImageFilesTargetCommand(_repairImagesFileCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  
  /**
   * @author Janan Wong - XCR-3836
   * 
   */
  private void measurementsXMLFileCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSelectMeasurementsXmlFileTargetCommand(_measurementsXMLFileCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  
  /**
   * @author Jack Hwee
   */
  private void createTimeStampFolderCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigCreateTimeStampFolderCommand(_createTimeStampFolderCheckBox.isSelected(), this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void setCadXMLFileCheckboxState(boolean state)
  {
    _cadXMLFileCheckBox.setSelected(state);
    setControlsStateForCadXMLFileCheckBox();
    _configObservable.stateChanged(new Object());
  }

  /**
     * @author Laura Cormos
     */
    public void setControlsStateForCadXMLFileCheckBox()
    {
      if (_cadXMLFileCheckBox.isSelected())
      {
        if (_allDestinationsListModel.isEmpty() == false)
          _cadXMLLeftArrowButton.setEnabled(true);
        if (_cadXMLDestinationsListModel.isEmpty() == false)
          _cadXMLRightArrowButton.setEnabled(true);
      }
      else
      {
        _cadXMLLeftArrowButton.setEnabled(false);
        _cadXMLRightArrowButton.setEnabled(false);
      }
      setCurrentSelectedRuleIsDirty(true);
      resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  public void setSettingsXMLFileCheckboxState(boolean state)
  {
    _settingsXMLFileCheckBox.setSelected(state);
    setControlsStateForSettingsXMLFileCheckBox();
    _configObservable.stateChanged(new Object());
  }

  /**
   * @author Laura Cormos
   */
  public void setControlsStateForSettingsXMLFileCheckBox()
  {
    if (_settingsXMLFileCheckBox.isSelected())
    {
      if (_allDestinationsListModel.isEmpty() == false)
        _settingsXMLLeftArrowButton.setEnabled(true);
      if (_settingsXMLDestinationsListModel.isEmpty() == false)
        _settingsXMLRightArrowButton.setEnabled(true);
    }
    else
    {
      _settingsXMLLeftArrowButton.setEnabled(false);
      _settingsXMLRightArrowButton.setEnabled(false);
    }
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  public void setIndictmentsXMLFileCheckboxState(boolean state)
  {
    _indictmentsXMLFileCheckBox.setSelected(state);
    setControlsStateForIndictmetnsXMLFileCheckBox();
    _configObservable.stateChanged(new Object());
  }

  /**
   * @author Laura Cormos
   */
  public void setControlsStateForIndictmetnsXMLFileCheckBox()
  {
    if (_indictmentsXMLFileCheckBox.isSelected())
    {
      if (_allDestinationsListModel.isEmpty() == false)
        _indictmentsXMLLeftArrowButton.setEnabled(true);
      if (_indictmentsXMLDestinationsListModel.isEmpty() == false)
        _indictmentsXMLRightArrowButton.setEnabled(true);
    }
    else
    {
      _indictmentsXMLLeftArrowButton.setEnabled(false);
      _indictmentsXMLRightArrowButton.setEnabled(false);
    }
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }

  /**
   * @author Laura Cormos
   */
  public void setRepairImagesFileCheckboxState(boolean state)
  {
    _repairImagesFileCheckBox.setSelected(state);
    setControlsStateForRepairImagesFileCheckBox();
    _configObservable.stateChanged(new Object());
  }

  /**
   * @author Laura Cormos
   */
  public void setControlsStateForRepairImagesFileCheckBox()
  {
    if (_repairImagesFileCheckBox.isSelected())
    {
      if (_allDestinationsListModel.isEmpty() == false)
        _repairImagesLeftArrowButton.setEnabled(true);
      if (_repairImagesDestinationsListModel.isEmpty() == false)
        _repairImagesRightArrowButton.setEnabled(true);
    }
    else
    {
      _repairImagesLeftArrowButton.setEnabled(false);
      _repairImagesRightArrowButton.setEnabled(false);
    }
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }
  
  /**
   * @author Janan Wong - XCR-3836
   */
  public void setMeasurementsXMLFileCheckboxState(boolean state)
  {
    _measurementsXMLFileCheckBox.setSelected(state);
    setControlsStateForMeasurementsXMLFileCheckBox();
    _configObservable.stateChanged(new Object());
  }

  /**
   * @author Janan Wong - XCR-3836
   */
  public void setControlsStateForMeasurementsXMLFileCheckBox()
  {
    _isGenerateMeasurementsXMLEnabled = Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE);
    if (_measurementsXMLFileCheckBox.isSelected() && _isGenerateMeasurementsXMLEnabled)
    {
      if (_allDestinationsListModel.isEmpty() == false)
      {
        _measurementsXMLLeftArrowButton.setEnabled(true);
      }
      if (_measurementsXMLDestinationsListModel.isEmpty() == false)
      {
        _measurementsXMLRightArrowButton.setEnabled(true);
      }
    }
    else
    {
      _measurementsXMLLeftArrowButton.setEnabled(false);
      _measurementsXMLRightArrowButton.setEnabled(false);
    }
    setCurrentSelectedRuleIsDirty(true);
    resetStatusBar();
  }
  
   /**
   * @author Jack Hwee
   */
  public void setCreateTimeStampFolderCheckboxState(boolean state) throws DatastoreException
  {
    _createTimeStampFolderCheckBox.setSelected(state);
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteConditionButtonBasedOnTableSelection(int rowIndex)
  {
    _deleteConditionRowButton.setEnabled(rowIndex >= 0);
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteRuleButtonBasedOnTableSelection(int rowIndex)
  {
    _deleteRuleButton.setEnabled(rowIndex >= 0);
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteDestinationButtonBasedOnTableSelection(int rowIndex)
  {
    _deleteDestinationButton.setEnabled(rowIndex >= 0);
  }

  /**
   * @author Laura Cormos
   */
  private void setEditDestinationButtonBasedOnTableSelection(int rowIndex)
  {
    _editDestinationButton.setEnabled(rowIndex >= 0);
  }

  /**
   * @author Laura Cormos
   */
  private int getIndexForRule(Rule ruleToFind)
  {
    java.util.List<Rule> rules = _resultsProcessor.getRuleList();
    int ruleIndex = -1;
    for (int i = 0; i < rules.size(); i++)
    {
      if (rules.get(i).getName().equals(ruleToFind.getName()))
      {
        ruleIndex = i;
        break;
      }
    }

    return ruleIndex;
  }

  /**
   * @author Laura Cormos
   */
  private void clearAllComponents()
  {
    _conditionsTableModel.clearData();
    _allDestinationsList.removeAll();
    _allDestinationsListModel.clear();
    _cadXMLDestinationsList.removeAll();
    _cadXMLDestinationsListModel.clear();
    _settingsXMLDestinationsList.removeAll();
    _settingsXMLDestinationsListModel.clear();
    _indictmentsXMLDestinationsList.removeAll();
    _indictmentsXMLDestinationsListModel.clear();
    _repairImagesDestinationsList.removeAll();
    _repairImagesDestinationsListModel.clear();
    _measurementsXMLDestinationsList.removeAll();
    _measurementsXMLDestinationsListModel.clear();
    setCadXMLFileCheckboxState(false);
    setSettingsXMLFileCheckboxState(false);
    setIndictmentsXMLFileCheckboxState(false);
    setRepairImagesFileCheckboxState(false);
    setMeasurementsXMLFileCheckboxState(false);
    setDeleteButtonsState();
    setCurrentSelectedRuleIsDirty(false);
    _currentSelectedRule = null;
  }

  void setCurrentSelectedRuleIsDirty(boolean isDirty)
  {
    _currentSelectedRuleIsDirty = isDirty;
  }

  /**
   * @author Laura Cormos
   */
  private void setCurrentRule(Rule rule)
  {
    _currentSelectedRule = rule;
    _conditionsTableModel.setCurentRule(_currentSelectedRule);
  }

  /**
   * @author Laura Cormos
   */
  private void setDeleteButtonsState()
  {
    if (_resultsProcessor.isResultsProcessingEnabled())
    {
      setDeleteRuleButtonBasedOnTableSelection(_rulesTable.getSelectedRow());
      setDeleteConditionButtonBasedOnTableSelection(_conditionsTable.getSelectedRow());
      setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    }
    else
      setDeleteButtonsState(false);
  }

  /**
    * @author Laura Cormos
    */
   private void setDeleteButtonsState(boolean state)
   {
     _deleteRuleButton.setEnabled(state);
     _deleteConditionRowButton.setEnabled(state);
     _deleteDestinationButton.setEnabled(state);
   }

  /**
   * @author Laura Cormos
   */
  private void clearUndoStack()
  {
    _commandManager.clear();
    _menuBar.updateUndoRedoMenuItems();
  }

  /**
   * @author Laura Cormos
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ConfigObservable)
        {
          handleConfigDataUpdate(object);
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void handleConfigDataUpdate(Object object)
  {
    Assert.expect(object != null);

    if (object instanceof Rule)
    {
      // for the repair rule set the directory elements in sync with the checkbox state
      _enableRepairCheckBox.setSelected(_resultsProcessor.getIsRepairToolRuleEnabled());
      setEnabledRepairToolInstallDir(_resultsProcessor.getIsRepairToolRuleEnabled());
      // for the rule table, refresh everything
      if (_currentSelectedRule != null)
      {
        int selectedRuleIndex = _rulesTableModel.getIndexForRule(_currentSelectedRule);
        if (selectedRuleIndex >= 0)
        {
          _rulesTableModel.fireTableDataChanged();
          _rulesTable.setRowSelectionInterval(selectedRuleIndex, selectedRuleIndex);
        }
        setDeleteRuleButtonBasedOnTableSelection(_rulesTable.getSelectedRow());
      }
    }
    else if (object instanceof ResultsProcessor)
    {
      // for the install directory, update the textfield
      _repairToolInstallDirTextField.setText(_resultsProcessor.getRepairToolDirectory());
    }
    else if (object instanceof ResultsTableModelCondition)
    {
      ResultsTableModelCondition condition = (ResultsTableModelCondition)object;
      int conditionIndex = _conditionsTableModel.getIndexOf(condition);
      if (conditionIndex >= 0)
      {
        // for the condition table, refresh everything
        _conditionsTableModel.fireTableDataChanged();
        _conditionsTable.setRowSelectionInterval(conditionIndex, conditionIndex);
      }
      setDeleteConditionButtonBasedOnTableSelection(_conditionsTable.getSelectedRow());
    }
    else if (object instanceof SoftwareConfigEnum)
    {
      SoftwareConfigEnum swConfigEnum = (SoftwareConfigEnum)object;
      if (swConfigEnum.equals(SoftwareConfigEnum.ENABLE_RESULTS_PROCESSING))
      {
        Boolean value = (Boolean)swConfigEnum.getValue();
        _enableProcessingCheckBox.setSelected(value);
        setEnabledAllComponents(value);
        setRuleButtonsState(value);
      }
      else if (swConfigEnum.equals(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER))
      {
        Boolean value = (Boolean)swConfigEnum.getValue();
        
        if (_enableProcessingCheckBox.isSelected() && _ruleList.isEmpty()== false)
          _createTimeStampFolderCheckBox.setSelected(value);
      }
    }
    else
    {
      // the destination directory box could generate an Object
      if (_allDestinationsListModel.isEmpty())
        setEnabledLeftArrowButtons(false);
      else
        setEnabledLeftArrowButtonsIfTargetExists(true);
      setDeleteDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
      setEditDestinationButtonBasedOnTableSelection(_allDestinationsList.getSelectedIndex());
    }
  }

  /**
   * @author Laura Cormos
   */
  private void setRuleButtonsState(boolean processResults)
  {
    if (processResults)
    {
      if (_ruleList.isEmpty())
      {
        // disable save, save as and delete buttons as well
        setSaveButtonsState();
        setDeleteButtonsState();
        setEnabledRulesDefinitionPanel(false);
      }
      else
      {
        populateConditionTable();
        populateFileTransferPanel();
      }
      
      if (_isGenerateMeasurementsXMLEnabled == false)
      {
        _measurementsXMLFileCheckBox.setEnabled(false);
        _measurementsXMLDestinationsList.setEnabled(false);
        _measurementsXMLLeftArrowButton.setEnabled(false);
        _measurementsXMLRightArrowButton.setEnabled(false);
      }
    }
    // keep the 'new' rule button in sync with the state of the results processing checkbox
    _newRuleButton.setEnabled(processResults);
  }

  /**
   * @author Laura Cormos
   */
  private void setSaveButtonsState()
  {
    boolean enabled = true;
    if (_resultsProcessor.isResultsProcessingEnabled() == false)
      enabled = false;
    else if (_currentSelectedRule == null)
      enabled = false;

    _saveRuleAsButton.setEnabled(enabled);
    _saveRuleButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void setAddConditionButtonState()
  {
    boolean enabled = true;
    if (_resultsProcessor.isResultsProcessingEnabled() == false)
      enabled = false;
    else if (_currentSelectedRule == null)
      enabled = false;

    _addConditionRowButton.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private boolean repairToolPathIsSet()
  {
    String repairToolDirectory = _repairToolInstallDirTextField.getText();

    if (_enableRepairCheckBox.isSelected())
     if (StringUtil.isEmpty(repairToolDirectory))
       return false;
    return true;
  }
}
