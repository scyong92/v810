package com.axi.v810.gui.config;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.testDev.UndoState;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import javax.swing.table.*;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 * @return
 */
public class InitialRecipeSettingTableModel extends AbstractTableModel
{
  private SoftwareOptionsInitialRecipeSettingTabPanel _parent = null;  
  private static com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();  
  private InitialRecipeSetting _initialRecipeSetting = InitialRecipeSetting.getInstance();
  private List<List<Object>> _displayData = null; // a list of lists.
  private List<RecipeAdvanceSettingEnum> recipeSettingList = RecipeAdvanceSettingEnum.getRecipeAdvanceSettingEnumList();

  private boolean _testRunning = false;
  private boolean _setValueValid = true;
 
  protected final String[] _recipeSettingColumn = {StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_NAME_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_VALUE_KEY"),
                                                StringLocalizer.keyToString("ATGUI_THRESHOLD_TABLE_DEFAULT_KEY")};

  public static final int NAME_COLUMN = 0;
  public static final int VALUE_COLUMN = 1;
  public static final int DEFAULT_COLUMN = 2;
  
  private static final int _maxDroLevel = com.axi.v810.datastore.config.Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL);

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public InitialRecipeSettingTableModel(SoftwareOptionsInitialRecipeSettingTabPanel parent)
  {
    super();
    Assert.expect(parent != null);
    
    _parent = parent;

    _displayData = new LinkedList<List<Object>>();
 
    for (RecipeAdvanceSettingEnum recipeSettingEnum : recipeSettingList)
    {
      addRecipeSetting(recipeSettingEnum);
    }
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void addRecipeSetting(RecipeAdvanceSettingEnum setting)
  {
    List<Object> recipeSettingList = new LinkedList<Object>();
//      Serializable value = algorithmSetting.getDefaultValue(algorithm.getVersion());
//      Serializable value = _subtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    Object value = _initialRecipeSetting.getRecipeSettingValue(setting);
    
    Object defaultValue = setting.getDefaultValue();
    
    if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_MAGNIFICATION_INTIAL_SETITNG))
    {
      if (XrayTester.getSystemType().equals(SystemTypeEnum.THROUGHPUT) || XrayTester.getSystemType().equals(SystemTypeEnum.XXL))
      {
        if (com.axi.v810.datastore.config.Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED))
        {
          recipeSettingList.add(setting.getName());
          recipeSettingList.add(value);
          recipeSettingList.add(defaultValue);
          _displayData.add(recipeSettingList);
        }
      }
      else
      {
        recipeSettingList.add(setting.getName());
        recipeSettingList.add(value);
        recipeSettingList.add(defaultValue);
        _displayData.add(recipeSettingList);
      }
    }
//    else if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING))
//    {
//      if (com.axi.v810.datastore.config.Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION) < 
//          XrayCameraIdEnum.getAllEnums().size())
//      {
//        recipeSettingList.add(setting.getName());
//        recipeSettingList.add(value);
//        recipeSettingList.add(defaultValue);
//        _displayData.add(recipeSettingList);
//      }
//    }
    else if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_DRO_INTIAL_SETTING))
    {
      if (_maxDroLevel > 1 )
      {
        List<String> newDroDefaultValue = new ArrayList<String>();
        for (int i = 0 ; i < _maxDroLevel; i++)
        {
          String droName = DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(i+1).toString();
          newDroDefaultValue.add(droName);
        }
        
        setting.setValidSelectionBasedOnConfigValue(newDroDefaultValue);
        
        recipeSettingList.add(setting.getName());
        recipeSettingList.add(value);
        recipeSettingList.add(defaultValue);
        _displayData.add(recipeSettingList);
      }
    }
    else
    {
      recipeSettingList.add(setting.getName());
      recipeSettingList.add(value);
      recipeSettingList.add(defaultValue);
      _displayData.add(recipeSettingList);
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  // from AbstractTableModel
  public int getColumnCount()
  {
    return _recipeSettingColumn.length;
  }

  /**
  * Get the current column name -- from AbstractTableModel
  *
  * @author Andy Mechtenberg
  * @return The string specifying the column header name
  * @param col Column number
  */
  // from AbstractTableModel
  public String getColumnName(int col)
  {
    Assert.expect(col >= 0 && col < getColumnCount());
       
    return (String)_recipeSettingColumn[col];
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  // from AbstractTableModel
  public Object getValueAt(int row, int col)
  {
    List<Object> rowData = _displayData.get(row);
    Object data = null;
    data = rowData.get(col);

    return data;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void setValueAt(Object value, int row, int col)
  {
    if (!_setValueValid) return;  // exit if it's not valid
    _setValueValid = false;
    /** @todo Replace this will an Assert when datastore is more complete  */
    if (value == null)
      return;

    // Check to see if the incoming new value is the same as the old one.  If so, do no more work
    if (value.equals(getValueAt(row,col)))
    {
      //System.out.println("New value " + value + " old value " + getValueAt(row,col));
      _setValueValid = true;
      return;
    }

//    // A bug can arise where the value type does not match the cell's value type.  ComboBoxes cause this, but I don't
//    // know why.  This check should fix it.
//    if (value.getClass() != getValueAt(row, col).getClass())
//    {
//      //System.out.println("New value class" + value.getClass() + " old value " + getValueAt(row,col).getClass());
//      _setValueValid = true;
//      return;
//    }

    List<Object> rowData = _displayData.get(row);
    String name = (String) rowData.get(NAME_COLUMN);
    RecipeAdvanceSettingEnum recipeSetting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(name);
    
    rowData.set(VALUE_COLUMN, value);
    // a Boolean type threshold will come in to here as a string, because it's represented with a "Yes" or "No"
    // in the table.  So if the value is a string with contents "Yes" or "No", convert it back to a boolean when setting
    // the value
    if (recipeSetting.equals(RecipeAdvanceSettingEnum.RECIPE_GENERATE_NEW_SCAN_ROUTE_INTIAL_SETTING))
    {
      String valueName = (String) value;
      ConfigSetInitialRecipeSettingValueCommand setCommand = new ConfigSetInitialRecipeSettingValueCommand(recipeSetting, value);
      try
      {
        UndoState undoState = _parent.getCurrentUndoState(row, recipeSetting);
        _commandManager.trackState(undoState);
        _commandManager.execute(setCommand);
        rowData.set(col, (String)value);
      }
      catch (XrayTesterException ex)
      {
        Assert.expect(false);
      }
      if (valueName.equalsIgnoreCase("false"))
      {
        Object stageSpeedValue = InitialRecipeSetting.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING);
        
        List<String> stageSpeedNameList = (List<String>) stageSpeedValue;
        
        if (stageSpeedNameList.size() > 1 )
        {
          ConfigSetInitialRecipeSettingValueCommand setStageSpeedCommand = new ConfigSetInitialRecipeSettingValueCommand(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING, 
                                                                                                                         RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING.getDefaultValue());
          try
          {
            UndoState undoState = _parent.getCurrentUndoState(row, RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING);
            _commandManager.trackState(undoState);
            _commandManager.execute(setStageSpeedCommand);
//            rowData.set(col, ((List<String>)RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING.getDefaultValue()).toString());
            for(List<Object> tempRowData : _displayData)
            {
              String settingName = (String) tempRowData.get(NAME_COLUMN);
              RecipeAdvanceSettingEnum setting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(settingName);
              if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING))
              {
                tempRowData.set(col,((List<String>)RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING.getDefaultValue()).toString());
                fireTableCellUpdated(_displayData.indexOf(tempRowData), VALUE_COLUMN);
              }
            }
          }
          catch (XrayTesterException ex)
          {
            Assert.expect(false);
          }
        }
      }
    }
    else
    {
      ConfigSetInitialRecipeSettingValueCommand setCommand = new ConfigSetInitialRecipeSettingValueCommand(recipeSetting, value);
      try
      {
        UndoState undoState = _parent.getCurrentUndoState(row, recipeSetting);
        _commandManager.trackState(undoState);
        _commandManager.execute(setCommand);
        if (value instanceof String)
          rowData.set(col, (String)value);
        else if (value instanceof List)
          rowData.set(col, ((List<String>)value).toString());
          
      }
      catch (XrayTesterException ex)
      {
        Assert.expect(false);
      }
    }


    _setValueValid = true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isCellEditable(int row, int col)
  {
    if (_testRunning)
      return false;
    
    List<Object> rowData = _displayData.get(row);    
    
    String name = (String)rowData.get(NAME_COLUMN);
    
    if (RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(name).equals(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING))
//      ||  RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(name).equals(RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING))
    {
      return false;
    }
      
    if (col == VALUE_COLUMN)
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getRowCount()
  {
    if (_displayData == null)
      return 0;
    else
      return _displayData.size();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  void setTestRunning(boolean testRunning)
  {
    _testRunning = testRunning;
  }
  
}
