package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
public class SerialNumberMatchingTableModel extends DefaultSortTableModel
{
  private MainMenuGui _mainUI;
  private ProductionOptionsSerialNumberTabPanel _parent;
  private int _type;
  //String labels retrieved from the localization properties file
  private static final String _PATTERN_TYPE_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_PATTERN_TYPE_COLUMN_KEY");
  private static final String _PATTERN_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_PATTERN_COLUMN_KEY");
  private static final String _PROJECT_NAME_LABEL = StringLocalizer.keyToString("CFGUI_SN_TABLE_PROJECT_COLUMN_KEY");
  private static final String _VARIATION_NAME_LABEL = StringLocalizer.keyToString("CFGUI_SN_TABLE_VARIATION_COLUMN_KEY");

  private String[] _columnLabels = {_PATTERN_TYPE_LABEL,
                                    _PATTERN_LABEL,
                                    _PROJECT_NAME_LABEL,
                                    _VARIATION_NAME_LABEL};

  private List<SerialNumberRegularExpressionData> _selectedSerialNumberData = new ArrayList<
      SerialNumberRegularExpressionData>();
  private List<SerialNumberRegularExpressionData> _serialNumberData = new ArrayList<SerialNumberRegularExpressionData>();

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  /**
   * Constructor.
   * @author Laura Cormos
   */
  public SerialNumberMatchingTableModel(ProductionOptionsSerialNumberTabPanel parent, int serialNumbersType)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
    _type = serialNumbersType;
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_serialNumberData != null)
      _serialNumberData.clear();
    if (_selectedSerialNumberData != null)
      _selectedSerialNumberData.clear();
    _currentSortColumn = 0;
    _currentSortAscending = true;
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[column];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_serialNumberData != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    SerialNumberRegularExpressionData rowElement = _serialNumberData.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case SerialNumberMatchingTable._PATTERN_TYPE_INDEX:
        if (rowElement.isInterpretedAsRegularExpression())
          value = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_REGEX_KEY");
        else
          value = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY");
        break;
      case SerialNumberMatchingTable._PATTERN_INDEX:
        value = rowElement.getOriginalExpression();
        break;
      case SerialNumberMatchingTable._PROJECT_NAME_INDEX:
        value = rowElement.getProjectName();
        break;
      case SerialNumberMatchingTable._VARIATION_NAME_INDEX:
        value = rowElement.getVariationName();
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    if (_serialNumberData == null)
        return 0;
    else
        return _serialNumberData.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if ((_selectedSerialNumberData == null) || (_selectedSerialNumberData.contains(_serialNumberData.get(rowIndex)) == false))
      return false;

    if (FileName.getProjectNames().size() == 0)
    {
      MessageDialog.showErrorDialog(_mainUI, StringLocalizer.keyToString("CFGUI_SN_NO_PROJECTS_ON_SELECT_PROJECT_MSG_KEY"),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
      return false;
    }
    
    if (columnIndex == SerialNumberMatchingTable._VARIATION_NAME_INDEX)
    {
      try
      {
        String projectName = getValueAt(rowIndex, SerialNumberMatchingTable._PROJECT_NAME_INDEX).toString();
        
        if (FileName.getProjectNames().contains(projectName) == false)
          return false;
        
        boolean isPreSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(projectName);
        if (isPreSelectVariation)
        {
          return false;
        }
      }
      catch (DatastoreException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                      true);
        return false;
      }
    }
    
    return true;
  }

  /**
   * Initialize the table with data from the serial number to project match config file
   * @author Laura Cormos
   */
  void setData(Collection<SerialNumberRegularExpressionData> serialNumbersCollection)
  {
    Assert.expect(serialNumbersCollection != null);

    clearData();
    _serialNumberData = new ArrayList<SerialNumberRegularExpressionData>(serialNumbersCollection);
    sortColumn(_currentSortColumn, _currentSortAscending);

    fireTableDataChanged();
  }

  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    SerialNumberRegularExpressionData rowElement = _serialNumberData.get(rowIndex);

    switch (columnIndex)
    {
      case SerialNumberMatchingTable._PATTERN_TYPE_INDEX:
        Assert.expect(object instanceof String);
        String type = (String)object;
        boolean isRegExPattern;
        if (type.equals(StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY")))
          isRegExPattern = false;
        else
          isRegExPattern = true;

        try
        {
          _commandManager.execute(new ConfigSetSerialNumberPatternTypeCommand(rowElement,
                                                                              rowElement.getOriginalExpression(),
                                                                              isRegExPattern));
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          // do nothing, we'll check for invalid regex below
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          break;
        }
        // check validity of pattern if the pattern type was changed from simple to regex
        if (isRegExPattern)
        {
          try
          {
            RegularExpressionUtilAxi.checkRegExValid(rowElement.getOriginalExpression());
          }
          catch (InvalidRegularExpressionDatastoreException ex)
          {
            // give a warning, not an error, for the invalid regex exception when the pattern type is changed
            // since they could be in the process of making more changes
            JOptionPane.showMessageDialog(_mainUI,
                                          StringLocalizer.keyToString(new LocalizedString(
                                              "CFGUI_INVALID_REGEX_WARNING_KEY",
                                              new Object[]{rowElement.getOriginalExpression()})),
                                          StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                          JOptionPane.WARNING_MESSAGE);
          }
        }
        break;
      case SerialNumberMatchingTable._PATTERN_INDEX:
        Assert.expect(object instanceof String);
        String pattern = (String)object;

        // check for the "" pattern - not allowed
        if (pattern.equals(""))
        {
          JOptionPane.showMessageDialog(_mainUI,
                                        StringLocalizer.keyToString("CFGUI_EMPTY_REGEX_NOT_VALID_MESSAGE_KEY"),
                                        StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                        JOptionPane.ERROR_MESSAGE);
          fireTableCellUpdated(rowIndex, columnIndex);
          break;
        }
        // first set the pattern, if is not legal, do nothing, check later
        // don't want to loose the pattern in case is is some complex regex with a minor error
        try
        {
          _commandManager.execute(new ConfigSetSerialNumberPatternCommand(rowElement,
                                                                          pattern,
                                                                          rowElement.isInterpretedAsRegularExpression()));
        }
        catch (InvalidRegularExpressionDatastoreException ex)
        {
          // do nothing, we'll check it again next
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
          break;
        }


        // next, if a regEx, check the pattern and keep them on the cell edit until the problem is fixed
        if (rowElement.isInterpretedAsRegularExpression())
        {
          // the set call above does do a validity check too but it does it after it adds a ^ and $ to the beginning aand
          // th end of the string so the error message would refer to a pattern different than the one the user entered.
          try
          {
            RegularExpressionUtilAxi.checkRegExValid(pattern);
          }
          catch (InvalidRegularExpressionDatastoreException ex)
          {
            JOptionPane.showMessageDialog(_mainUI,
                                          ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                          JOptionPane.ERROR_MESSAGE);
          }
        }
        break;
      case SerialNumberMatchingTable._PROJECT_NAME_INDEX:
        Assert.expect(object instanceof String);

        try
        {
          rowElement.clearVariationName((String)object);
        }
        catch (DatastoreException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                                        true);
        }
        
        try
        {
          _commandManager.execute(new ConfigSetSerialNumberProjectNameCommand(rowElement, (String)object));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      case SerialNumberMatchingTable._VARIATION_NAME_INDEX:
        Assert.expect(object instanceof String);

        try
        {
          _commandManager.execute(new ConfigSetSerialNumberVariationNameCommand(rowElement, (String)object));
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      default:
        Assert.expect(false);
    }
    _parent.setSerialNumbersDataIsDirty(_type);
  }

  /**
   * @author Laura Cormos
   */
  void addNewRow()
  {
    SerialNumberRegularExpressionData newElement = null;
    try
    {
      newElement = new SerialNumberRegularExpressionData("", "", false, "");
    }
    catch (DatastoreException ex)
    {
      //this is an invalid regular expression exception which should never happen at this point
      Assert.expect(false, "Code generated an invalid regex");
    }
    _serialNumberData.add(newElement);

    fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
  }

  /**
   * @author Laura Cormos
   */
  public void addRow(SerialNumberRegularExpressionData newElement)
  {
    Assert.expect(newElement != null);

    _serialNumberData.add(newElement);

    fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
  }

  /**
   * @author Laura Cormos
   */
  void setSelectedRow(int selectedRowIndex)
  {
    Assert.expect(selectedRowIndex >= 0);
    Assert.expect(_selectedSerialNumberData != null);

    _selectedSerialNumberData.clear();
    _selectedSerialNumberData.add(_serialNumberData.get(selectedRowIndex));
  }

  /**
   * @author Laura Cormos
   */
  Collection<SerialNumberRegularExpressionData> getData()
  {
    return _serialNumberData;
  }

  /**
   * @author Laura Cormos
   */
  public void deleteElementAt(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    _serialNumberData.remove(rowIndex);
    fireTableRowsDeleted(rowIndex, rowIndex);
  }

  /**
   * @author Laura Cormos
   */
  public boolean deleteElement(SerialNumberRegularExpressionData item)
  {
    Assert.expect(item != null);

    int rowIndex = _serialNumberData.indexOf(item);
    if (rowIndex >= 0)
    {
      _serialNumberData.remove(item);
      fireTableRowsDeleted(rowIndex, rowIndex);
      return true;
    }
    return false;
  }

  /**
   * @author Laura Cormos
   */
  public SerialNumberRegularExpressionData getElementAt(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    return _serialNumberData.get(rowIndex);
  }

  /**
   * @author Laura Cormos
   */
  public int getIndexOf(SerialNumberRegularExpressionData item)
  {
    Assert.expect(item != null);

    return _serialNumberData.indexOf(item);
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public void sortColumn(int column, boolean ascending)
  {
      if (column == 0)  // sort on patter type
          Collections.sort(_serialNumberData, new SerialNumberRegularExpressionDataComparator(ascending, SerialNumberRegularExpressionDataComparatorEnum.PATTERN_TYPE));
      else if (column == 1)  // sort on pattern
          Collections.sort(_serialNumberData, new SerialNumberRegularExpressionDataComparator(ascending, SerialNumberRegularExpressionDataComparatorEnum.PATTERN));
      else if (column == 2)  // sort on project name
          Collections.sort(_serialNumberData, new SerialNumberRegularExpressionDataComparator(ascending, SerialNumberRegularExpressionDataComparatorEnum.PROJECT_NAME));
      
      _currentSortColumn = column;
      _currentSortAscending = ascending;
  }
}
