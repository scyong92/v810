package com.axi.v810.gui.config;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * This class displays the list of results processing conditions
 * @author Jack Hwee
 */
class FalseCallMonitoringDynamicCallNumberTable extends JTable
{
  public static final int _TOTAL_NUMBER_OF_COMPONENT_INDEX = 0;
  public static final int _NUMBER_OF_ERROR = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _TOTAL_NUMBER_OF_COMPONENT_COLUMN_WIDTH_PERCENTAGE = .50;
  private static final double _NUMBER_OF_ERROR_COLUMN_WIDTH_PERCENTAGE = .50;

  private NumericRenderer _numericRenderer = new NumericRenderer(0, JLabel.LEFT);

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public FalseCallMonitoringDynamicCallNumberTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int scannerIdColumnWidth = (int)(totalColumnWidth * _TOTAL_NUMBER_OF_COMPONENT_COLUMN_WIDTH_PERCENTAGE);
    int barcodeColumnWidth = (int)(totalColumnWidth * _NUMBER_OF_ERROR_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_TOTAL_NUMBER_OF_COMPONENT_INDEX).setPreferredWidth(scannerIdColumnWidth);
    columnModel.getColumn(_NUMBER_OF_ERROR).setPreferredWidth(barcodeColumnWidth);
  }

  /**
  * @author Laura Cormos
  */
 public void setEditorsAndRenderers()
 {
   TableColumnModel columnModel = getColumnModel();

   columnModel.getColumn(_TOTAL_NUMBER_OF_COMPONENT_INDEX).setCellRenderer(_numericRenderer);
 }

}

