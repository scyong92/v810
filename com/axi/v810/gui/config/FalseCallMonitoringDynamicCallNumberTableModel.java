package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Jack Hwee
 * @version 1.0
 */
class FalseCallMonitoringDynamicCallNumberTableModel extends AbstractTableModel 
{
  private ProductionOptionsFalseCallTriggeringTabPanel _parent;
  private FalseCallMonitoring _falseCallMonitoring;
  //String labels retrieved from the localization properties file
  private static final String _TOTAL_NUMBER_OF_PINS_LABEL = StringLocalizer.keyToString("CFGUI_FCT_NUMBER_OF_PINS_PROCESSED_TITLE_KEY");
  private static final String _NUMBER_OF_ERROR_LABEL = StringLocalizer.keyToString("CFGUI_FCT_NUMBER_OF_DEFECTIVE_NUMBER_TITLE_KEY");

  private String[] _columnLabels = { _TOTAL_NUMBER_OF_PINS_LABEL,
                                   _NUMBER_OF_ERROR_LABEL};

  private List<FalseCallMonitoringTableElement> _falseCallMonitoringElements = new ArrayList<FalseCallMonitoringTableElement>();
  private List<Integer> _totalNumberOfComponents = new ArrayList<Integer>();
  private Map<Integer, Integer> _falseCallMonitoringElementsMap = new HashMap<Integer, Integer>();

  public FalseCallMonitoringDynamicCallNumberTableModel()
  {
    _falseCallMonitoring = FalseCallMonitoring.getInstance();
  }
  /**
   * Constructor.
   * @author Jack Hwee
   */
  public FalseCallMonitoringDynamicCallNumberTableModel(ProductionOptionsFalseCallTriggeringTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _falseCallMonitoring = FalseCallMonitoring.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_falseCallMonitoringElements != null)
      _falseCallMonitoringElements.clear();
    
    if (_totalNumberOfComponents != null)
      _totalNumberOfComponents.clear(); 
    
    if (_falseCallMonitoringElementsMap != null)
      _falseCallMonitoringElementsMap.clear();      
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

//    return getValueAt(0, columnIndex).getClass();
    return new String().getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_falseCallMonitoringElements != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    FalseCallMonitoringTableElement rowElement = _falseCallMonitoringElements.get(rowIndex);
    int totalNumberOfComponent = _totalNumberOfComponents.get(rowIndex);
    int totalError = _falseCallMonitoringElementsMap.get(totalNumberOfComponent);
    
    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case FalseCallMonitoringDynamicCallNumberTable._TOTAL_NUMBER_OF_COMPONENT_INDEX:
        value = totalNumberOfComponent;
        break;
      case FalseCallMonitoringDynamicCallNumberTable._NUMBER_OF_ERROR:
        value = totalError;
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    Assert.expect(_falseCallMonitoringElements != null);

    return _falseCallMonitoringElements.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    return false;
  }
  
  /**
   * 
   * @author Jack Hwee
   */
  public void setData()
  {
    Assert.expect(_falseCallMonitoring != null);
    
    clearData();

    int numRows = _falseCallMonitoring.getDynamicCallNumber().size();
    
    for (int i = 0; i < numRows; i++)
    {
      int a = Integer.parseInt(_falseCallMonitoring.getDynamicCallNumber().get(i));
      int b = Integer.parseInt(_falseCallMonitoring.getDynamicCallNumber().get(i + 1));     
      i++;
      FalseCallMonitoringTableElement newRow = new FalseCallMonitoringTableElement(a, b);
      _falseCallMonitoringElements.add(newRow);
      _totalNumberOfComponents.add(a);
      _falseCallMonitoringElementsMap.put(a, b);
      if (i + 1 == numRows)
          break;
    }
    
    Collections.sort(_totalNumberOfComponents, new AlphaNumericComparator(true));
  
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  public void addRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows; i++)
    {
      FalseCallMonitoringTableElement newElement = new FalseCallMonitoringTableElement(getRowCount()+1,
                                                               getRowCount()+1);
      _falseCallMonitoringElements.add(newElement);
    }
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  public void removeRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows; i++)
    {
      _falseCallMonitoringElements.remove(getRowCount() - 1);
    }
    fireTableDataChanged();
  }

  class FalseCallMonitoringTableElement
  {
    int _totalNumberOfComponent;
    int _numberOfError;

    FalseCallMonitoringTableElement(int totalNumberOfComponent, int numberOfError)
    {
      Assert.expect(totalNumberOfComponent >= 0);
      Assert.expect(numberOfError >= 0);

      _totalNumberOfComponent = totalNumberOfComponent;
      _numberOfError = numberOfError;
    }
   
  }
}
