package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.Assert;
import com.axi.v810.business.BusinessException;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.util.StringLocalizer;

/**
 * This class displays the list of results processing conditions
 * @author Kee Chin Seong
 */
public class PreOrPostInspectionScriptCustomizeInputTable extends JSortTable
{
  public static final int _PARAMETER_ENABLED_INDEX = 0;
  public static final int _OUTPUT_PARAMETER_INDEX = 1;

  private PreOrPostInspectionScriptOutputFileReader _reader = null;
  private PreOrPostInspectionScriptOutputFileWriter _writer = null;
  private PreOrPostInspectionScriptCustomizeInputTableModel _tableModel = null;
  
  private ProductionOptionsGeneralTabPanel _parent;

  /**
   * Default constructor.
   * @author Kee Chin Seong
   */
  public PreOrPostInspectionScriptCustomizeInputTable(ProductionOptionsGeneralTabPanel parent)
  {
    // do nothing
    _parent = parent;
    
    _reader = new PreOrPostInspectionScriptOutputFileReader();
    _writer = new PreOrPostInspectionScriptOutputFileWriter();
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public List<String> getEnabledCustomizeOutputParametersFromConfig()
  {
     Assert.expect(_reader != null);
     
     try
     {
        _reader.readPreviousSequenceSettings();
     }
     catch(DatastoreException ex2)
     {
         
     }
     return _reader.getPreviousSequenceSettings();
  }
  
  /**
   * @author Kee Chin Seong
   * @param fileName 
   */
  public void setCustomizeConfigPath(String fileName)
  {
    if(_reader == null)
        _reader = new PreOrPostInspectionScriptOutputFileReader();
    if(_writer == null)
       _writer = new PreOrPostInspectionScriptOutputFileWriter();
     
    _writer.setFilePath(fileName);
    _reader.setFilePath(fileName);
  }
  
  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Kee Chin Seong
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();
  }

  /**
   * @author Kee Chin Seong
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    
    TableColumn test = columnModel.getColumn(_PARAMETER_ENABLED_INDEX);
    CheckCellEditor cellEditor = new CheckCellEditor(_parent.getBackground(), _parent.getForeground());
    test.setCellEditor( cellEditor );
    test.setCellRenderer(new CheckCellRenderer());
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setTableModel(PreOrPostInspectionScriptCustomizeInputTableModel tableModel)
  {
     Assert.expect(tableModel != null);
     
     _tableModel = tableModel;
     setModel(tableModel);
  }
  
 
  /**
   * Overridden method.  Saves the data entered by the user in the table
   * to the project object.
   * @author Kee Chin Seong
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    
    //prepare for command manager
    switch (columnIndex)
    {
      case PreOrPostInspectionScriptCustomizeInputTable._OUTPUT_PARAMETER_INDEX: 
        break;
      case PreOrPostInspectionScriptCustomizeInputTable._PARAMETER_ENABLED_INDEX:
      {
        _tableModel.enableParameter(rowIndex, (boolean)object);
        break;
      }
      default:
        Assert.expect(false);
    }
    
    try
    {
        List<String>enabledParameter = new ArrayList<String>();
        for(Map.Entry<String, Boolean> entry : _tableModel.getData().entrySet())
        {
            if(entry.getValue() == true)
               enabledParameter.add(entry.getKey());
        }
            
        _writer.saveSettings(enabledParameter);
    }
    catch(DatastoreException ex)
    {
    }
    catch(BusinessException ex2)
    {
    }
    
    _tableModel.fireTableDataChanged();
    _tableModel.fireTableCellUpdated(rowIndex, columnIndex);
  }
}
