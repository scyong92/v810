package com.axi.v810.gui.config;

import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;
import javax.swing.*;

/**
 * @author Laura Cormos
 */
class ParameterEditor extends DefaultCellEditor
{
  JComboBox _comboBox = null;
  
  private String _NEW_PARAMETER = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_NEW_PARAMETER_KEY");

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with values
   */
  public ParameterEditor(PreOrPostInspectionScriptOutputFileReader reader)
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    try
    {
        reader.readPreviousSequenceSettings();
    }
    catch(com.axi.v810.datastore.DatastoreException ex)
    {
        
    }
    
    //Adding first Parameter always is "New Parameter" alwys let user have choice to create new parameter
    _comboBox.addItem(_NEW_PARAMETER);
    for(String sequenceData : reader.getPreviousSequenceSettings())
    {
        _comboBox.addItem(sequenceData);
    }
  }
}
