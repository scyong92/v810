package com.axi.v810.gui.config;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.util.*;

/**
 * @author George Booth
 */
public class UserAccountsTableModel extends DefaultSortTableModel
{
  private final String[] _userAccountsTableColumnNames = {
      StringLocalizer.keyToString("CFGUI_USER_NAME_TABLE_COLUMN_KEY"),
      StringLocalizer.keyToString("CFGUI_ACCOUNT_TYPE_TABLE_COLUMN_KEY"),
      StringLocalizer.keyToString("CFGUI_FULL_NAME_TABLE_COLUMN_KEY"),
  };

  private UserAccountsManager _userAccountsManager = UserAccountsManager.getInstance();

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  private ArrayList<UserAccountInfo> _loginAccounts;

  // default to sort by component, ascending
  protected int _currentSortColumn = 0;
  protected boolean _currentSortAscending = true;

  /**
   * @author George Booth
   */
  public UserAccountsTableModel()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public void sortColumn(int column, boolean ascending)
  {
    _currentSortColumn = column;
    _currentSortAscending = ascending;

    switch (column)
    {
      case 0: // account name
        Collections.sort(_loginAccounts, new UserAccountsComparator(ascending, UserAccountsComparatorEnum.ACCOUNT_NAME));
        break;
      case 1: // account type
        Collections.sort(_loginAccounts, new UserAccountsComparator(ascending, UserAccountsComparatorEnum.ACCOUNT_TYPE));
        break;
      case 2: // full name
        Collections.sort(_loginAccounts, new UserAccountsComparator(ascending, UserAccountsComparatorEnum.FULL_NAME));
        break;
      default:
        Assert.expect(false, "Column does not exist in table model");
        break;
     }
  }

  /**
   * @author George Booth
   */
  public boolean isSortable(int col)
  {
      return true;
  }

  /**
   * @author George Booth
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    UserAccountInfo accountData = _loginAccounts.get(rowIndex);
    switch(columnIndex)
    {
      case 0:
        return accountData.getAccountName();
      case 1:
        return accountData.getAccountType();
      case 2:
        return accountData.getFullName();
      default:
        // this should not happen if so then assert
        Assert.expect(false, "no case for column" + columnIndex);
        return null;
    }
  }

  /**
   * @author George Booth
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_userAccountsTableColumnNames[columnIndex];
  }

  /**
   * @author George Booth
   */
  public void setValueAt(Object value, int rowIndex, int columnIndex)
  {
    // table is not editable
    Assert.expect(false, "setValue() called");
  }

  /**
   * @author George Booth
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author George Booth
   */
  public int getRowCount()
  {
    if (_loginAccounts == null)
      return 0;
    else
      return _loginAccounts.size();
  }

  /**
   * @author George Booth
   */
  public int getColumnCount()
  {
    return _userAccountsTableColumnNames.length;
  }

  /**
   * @author George Booth
   */
  void clearSelection()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public void populateWithLoginAccountData()
  {
    _loginAccounts  = _userAccountsManager.getAccounts();
    // sort by last sort directive
    sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableDataChanged();
  }

  /**
   * @author George Booth
   */
  int getRowForUserName(String userName)
  {
    Assert.expect(userName != null);

    if (_loginAccounts == null)
    {
      return -1;
    }
    else
    {
      for (int row = 0; row < _loginAccounts.size(); row++)
      {
        UserAccountInfo account = _loginAccounts.get(row);
        if (account.getAccountName().equals(userName))
          return row;
      }
    }
    // not found
    return -1;
  }

  /**
   * @author George Booth
   */
  UserAccountInfo getUserAccount(int row)
  {
    Assert.expect(row > -1 && row < _loginAccounts.size());

    return _loginAccounts.get(row);
  }

}
