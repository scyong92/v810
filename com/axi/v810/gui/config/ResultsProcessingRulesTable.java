package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.resultsProcessing.*;
import java.awt.*;

/**
 * This class displays the list of results processing rules and their status (enbled/disabled)
 * @author Laura Cormos
 */
class ResultsProcessingRulesTable extends JTable
{
  public static final int _RULE_NAME_INDEX = 0;
  public static final int _RULE_ENABLED_INDEX = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _RULE_NAME_COLUMN_WIDTH_PERCENTAGE = .75;
  private static final double _RULE_ENABLED_COLUMN_WIDTH_PERCENTAGE = .25;

  private static final int _MAX_ROW_COUNT = 4;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public ResultsProcessingRulesTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int ruleNameColumnWidth = (int)(totalColumnWidth * _RULE_NAME_COLUMN_WIDTH_PERCENTAGE);
    int ruleEnabledColumnWidth = (int)(totalColumnWidth * _RULE_ENABLED_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_RULE_NAME_INDEX).setPreferredWidth(ruleNameColumnWidth);
    columnModel.getColumn(_RULE_ENABLED_INDEX).setPreferredWidth(ruleEnabledColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    columnModel.getColumn(_RULE_NAME_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_RULE_ENABLED_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_RULE_ENABLED_INDEX).setCellRenderer(new CheckCellRenderer());
  }


  /**
   * @return a list of selected rules
   * @author Laura Cormos
   */
  public java.util.List<Rule> getSelectedRules()
  {
    ResultsProcessingRulesTableModel model = (ResultsProcessingRulesTableModel)getModel();
    java.util.List<Rule> selectedRules = new ArrayList<Rule>();
    int rowCount = getRowCount();

    for(int i = 0; i < rowCount; ++i)
    {
      if(selectionModel.isSelectedIndex(i))
      {
        selectedRules.add(model.getRule(i));
      }
    }

    return selectedRules;
  }

  /**
   * @author Laura Cormos
   */
  public int getMaximumHeight()
  {
    FontMetrics fm = getFontMetrics(getFont());
    int prefHeight = fm.getHeight();
    return prefHeight * _MAX_ROW_COUNT;
  }
}
