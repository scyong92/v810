package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.guiUtil.*;
import com.axi.guiUtil.ListSelectionCellEditor;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
public class SerialNumberMatchingTable extends JSortTable
{
  public static final int _PATTERN_TYPE_INDEX = 0;
  public static final int _PATTERN_INDEX = 1;
  public static final int _PROJECT_NAME_INDEX = 2;
  public static final int _VARIATION_NAME_INDEX = 3;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _PATTERN_COLUMN_WIDTH_PERCENTAGE = .50;
  private static final double _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE = .20;
  private static final double _VARIATION_NAME_COLUMN_WIDTH_PERCENTAGE = .20;

  private PatternTypeEditor _patternTypeEditor = new PatternTypeEditor();
  private ListSelectionCellEditor _projectNameEditor;
  private ListSelectionCellEditor _variationNameEditor;

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public SerialNumberMatchingTable()
  {
    // do nothing
    _projectNameEditor = new ListSelectionCellEditor(new ArrayList<Object>(FileName.getProjectNames()));
    _variationNameEditor = new ListSelectionCellEditor(new ArrayList<Object>());
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int patternTypeColumnWidth = (int)(totalColumnWidth * _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int patternColumnWidth = (int)(totalColumnWidth * _PATTERN_COLUMN_WIDTH_PERCENTAGE);
    int projectNameColumnWidth = (int)(totalColumnWidth * _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE);
    int variationNameColumnWidth = (int)(totalColumnWidth * _VARIATION_NAME_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_PATTERN_TYPE_INDEX).setPreferredWidth(patternTypeColumnWidth);
    columnModel.getColumn(_PATTERN_INDEX).setPreferredWidth(patternColumnWidth);
    columnModel.getColumn(_PROJECT_NAME_INDEX).setPreferredWidth(projectNameColumnWidth);
    columnModel.getColumn(_VARIATION_NAME_INDEX).setPreferredWidth(variationNameColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_PATTERN_TYPE_INDEX).setCellEditor(_patternTypeEditor);

    columnModel.getColumn(_PATTERN_INDEX).setCellEditor(stringEditor);

    columnModel.getColumn(_PROJECT_NAME_INDEX).setCellEditor(_projectNameEditor);
    
    columnModel.getColumn(_VARIATION_NAME_INDEX).setCellEditor(_variationNameEditor);
  }

  /**
   * @author Laura Cormos
   */
  public boolean editCellAt(int row, int column, EventObject e)
  {
    if (column == _PROJECT_NAME_INDEX)
    {
      // want to update the list of existing projects every time the project name cell gets selected
      // to keep the projects list up to date with what's on disk
      //Punit: XCR-3040
      java.util.List<Object> projectNames = new ArrayList<>();
      for(String project : FileName.getProjectNames())
      {
        if(Project.isProjectNameValid(project))
        {
          projectNames.add(project);
        }
      }
      _projectNameEditor.setData(projectNames);
//      _projectNameEditor.setData(new ArrayList<Object>(FileName.getProjectNames()));
    }
    
    if (column == _VARIATION_NAME_INDEX)
    {
      try
      {
        String projectName = (String)getModel().getValueAt(row, _PROJECT_NAME_INDEX);
        List<Object> variationList = new ArrayList<Object> ();
        variationList.addAll(VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(projectName));
        variationList.add(VariationSettingManager.ALL_LOADED);
        _variationNameEditor.setData(variationList);
      }
      catch (DatastoreException ex)
      {
        //nothing
      }
    }
    return super.editCellAt(row, column, e);
  }
}
