package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
/**
 *
 * @author chin-seong.kee
 * table model for Developer Debug config table
 */
public class DeveloperDebugConfigTableModel extends AbstractTableModel
{  
  private MainMenuGui _mainUI;
  private DeveloperDebugModeTabPanel _parent;

  //String labels retrieved from the localization properties file
  private static final String _CONFIG_KEY_LABEL = StringLocalizer.keyToString("GUI_CONFIG_KEY_TITLE_KEY");
  private static final String _CONFIG_VALUE_LABEL = StringLocalizer.keyToString("GUI_CONFIG_VALUE_TITLE_KEY");

  private String[] _developerDebugConfigTableColumns = {_CONFIG_KEY_LABEL,
                                                        _CONFIG_VALUE_LABEL};

   
  // default to sort by component, ascending
  private int _currentSortColumn = 0;
  private boolean _currentSortAscending = true;

  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  
  private static java.util.List<ConfigEnum>_configEnums = DeveloperDebugConfigEnum.getDeveloperDebugConfigEnum();
  
  public DeveloperDebugConfigTableModel()
  {
      super();
    //setupColumns();
  }
  
  public DeveloperDebugConfigTableModel(DeveloperDebugModeTabPanel parent)
  {
    super();
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  void clear()
  {
    _currentSortColumn = 0;
    _currentSortAscending = true;
    
    //_configEnums.clear();
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public int getColumnCount()
  {
    return _developerDebugConfigTableColumns.length;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public int getRowCount()
  {
     return _configEnums.size();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);
    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @author Chin-Seong, Kee
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _developerDebugConfigTableColumns[columnIndex];
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    // column 0 is Key - not editable
    // column 1 is value - TEMPORARY not editable
    if (columnIndex == 0)
      return false;
    if (columnIndex == 1)
      return true;
    
    return true;
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    DeveloperDebugConfigEnum configKey = (DeveloperDebugConfigEnum)_configEnums.get(rowIndex);
    if (columnIndex == DeveloperDebugConfigTable._DEVELOPER_DEBUG_CONFIG_KEY) // Key name
      return (String)configKey.getKey();
    else if (columnIndex == DeveloperDebugConfigTable._DEVELOPER_DEBUG_CONFIG_VALUE) // Value
    {
      Object values = configKey.getValue();
      if(values instanceof Boolean)
      {
        Boolean booleanValue = (Boolean)values;
        return booleanValue.toString();
      }
      else if(values instanceof Integer)
      {
        Integer integerValue = (Integer)values;
        return integerValue.toString();
      }
      return values;
    }
    return null;
  }
  
  public Object getConfigEnumObject(int rowIndex, int columnIndex)
  {
    return _configEnums.get(rowIndex);
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == DeveloperDebugConfigTable._DEVELOPER_DEBUG_CONFIG_KEY) // config key
    {
      return;
    }
    else if (columnIndex == DeveloperDebugConfigTable._DEVELOPER_DEBUG_CONFIG_VALUE) // config value
    {
      DeveloperDebugConfigEnum _developerConfigEnum = (DeveloperDebugConfigEnum)getConfigEnumObject(rowIndex, columnIndex);
      String valueString = (String) aValue; 
      if(_developerConfigEnum.getType() == TypeEnum.BOOLEAN)
      {
        if(valueString.equalsIgnoreCase("false") || valueString.equalsIgnoreCase("true"))
        {
          try
          {
            Boolean booleanValue = new Boolean(valueString);
            _commandManager.execute(new ConfigSetDevelopDebugConfigCommand(_developerConfigEnum, booleanValue));
          }
          catch (XrayTesterException ex)
          {
            MessageDialog.showErrorDialog(getMainMenuInstance(), ex.getLocalizedMessage(),
                                            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
            fireTableCellUpdated(rowIndex, columnIndex);
          }
        }
        else
        {
          MessageDialog.showErrorDialog(getMainMenuInstance(), "TRUE or FALSE only",
            StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
      }
      else if(_developerConfigEnum.getType() == TypeEnum.INTEGER)
      {
        try
        {
          Integer integerValue = new Integer(valueString);
          _commandManager.execute(new ConfigSetDevelopDebugConfigCommand(_developerConfigEnum, integerValue));
        }
        catch(NumberFormatException nfe)
        {
          MessageDialog.showErrorDialog(getMainMenuInstance(), nfe.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(getMainMenuInstance(), ex.getLocalizedMessage(),
                                          StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"), true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        fireTableCellUpdated(rowIndex, columnIndex);
      }
      
    }
  }
  
  public void populateConfigData()
  {
    //sortColumn(_currentSortColumn, _currentSortAscending);
    fireTableStructureChanged();
  }
  
  public MainMenuGui getMainMenuInstance()
  {
    return _mainUI;
  }
}
