/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.config;

import com.axi.guiUtil.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jack Hwee
 */
public class AddDynamicCallNumberDialog extends EscapeDialog
{
  java.util.List<PanelCoordinate> _selectedPoints = null; 
  BoardType _currentBoardType;
  NumericTextField _totalComponentTextField = new NumericTextField();
  NumericTextField _totalErrorTextField = new NumericTextField();
  private List<PanelCoordinate> _panelCoordinate = new ArrayList<PanelCoordinate>();  // List of ComponentType objects
  private PanelCoordinate _addedCoordinate = null;
  private FalseCallMonitoring _falseCallMonitoring;
  private List<Integer> _totalComponentList = new ArrayList<Integer>();
  private List<Integer> _totalErrorList = new ArrayList<Integer>();
  private List<String> _dynamicCallNumberList = new ArrayList<String>();
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getDevelopTestInstance();
  private int _totalComponentAdded = -1;
  private int _totalErrorAdded = -1;
  
    /**
   * @author Jack Hwee
   */
  public AddDynamicCallNumberDialog(Frame parent, String title) throws XrayTesterException
  {
    super(parent, title, true);
    Assert.expect(parent != null);
    Assert.expect(title != null);

    _falseCallMonitoring = FalseCallMonitoring.getInstance();
   
    int numRows = _falseCallMonitoring.getDynamicCallNumber().size();
    
    _totalComponentList.clear();
    _totalErrorList.clear();
    
    for (int i = 0; i < numRows; i++)
    {
      int totalComponent = Integer.parseInt(_falseCallMonitoring.getDynamicCallNumber().get(i));
      int totalError = Integer.parseInt(_falseCallMonitoring.getDynamicCallNumber().get(i + 1)); 
      
      _dynamicCallNumberList.add(_falseCallMonitoring.getDynamicCallNumber().get(i));
      _dynamicCallNumberList.add(_falseCallMonitoring.getDynamicCallNumber().get(i + 1));
      
      i++;    
      _totalComponentList.add(totalComponent);
      _totalErrorList.add(totalError);
      
      if (i + 1 == numRows)
          break;
    }
    
    jbInit();
    pack();
    
  }

    /**
   * @author Jack Hwee
   */
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
  }

  /**
   * @author Jack Hwee
   */
  private void jbInit() throws XrayTesterException
  {     
     // this.setResizable(false);
     // this.setTitle("Add Points");
      this.setLayout(new BorderLayout());
    
      JPanel xCoordinatePanel = new JPanel();
      xCoordinatePanel.setLayout(new BorderLayout());
      xCoordinatePanel.setMinimumSize(new Dimension(405, 27));
      
      JPanel yCoordinatePanel = new JPanel();
      yCoordinatePanel.setLayout(new BorderLayout());
      yCoordinatePanel.setMinimumSize(new Dimension(405, 27));
      
      JPanel addCoordinatePanel = new JPanel();
      addCoordinatePanel.setLayout(new BorderLayout());
      addCoordinatePanel.setMinimumSize(new Dimension(405, 27));

    //  addCoordinateDialog.setPreferredSize(new Dimension(400, 400));
    //  addCoordinateDialog.setBounds(0, 0, 400, 400);
      
      _totalComponentTextField = new NumericTextField();
      _totalComponentTextField.setHorizontalAlignment(SwingConstants.LEFT);
      _totalComponentTextField.setPreferredSize(new Dimension(220, 30));
      xCoordinatePanel.add(_totalComponentTextField, BorderLayout.EAST);     
    
      JLabel xCoordinateLabel = new JLabel();
      xCoordinateLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_PINS_PROCESSED_TITLE_KEY"));
      xCoordinatePanel.add(xCoordinateLabel, BorderLayout.WEST);
         
      _totalErrorTextField = new NumericTextField();
      _totalErrorTextField.setHorizontalAlignment(SwingConstants.LEFT);
      _totalErrorTextField.setPreferredSize(new Dimension(220, 30));
      yCoordinatePanel.add(_totalErrorTextField, BorderLayout.EAST);  
      
      JLabel yCoordinateLabel = new JLabel();
      yCoordinateLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_DEFECTIVE_PINS_TITLE_KEY"));
      yCoordinatePanel.add(yCoordinateLabel, BorderLayout.WEST);
      
      JButton addButton = new JButton();
      addButton.setText(StringLocalizer.keyToString("PSP_ADD_POINTS_BUTTON_KEY"));
      addButton.addActionListener(new ActionListener() 
      {

          public void actionPerformed(ActionEvent e) 
          {
              addButton_actionPerformed();
          }
      });
      
      addCoordinatePanel.add(addButton, BorderLayout.CENTER);
      this.add(xCoordinatePanel, BorderLayout.NORTH);
      this.add(yCoordinatePanel, BorderLayout.CENTER);
      this.add(addCoordinatePanel, BorderLayout.SOUTH);
         
      _totalErrorTextField.addKeyListener(new java.awt.event.KeyAdapter()
      {
        public void keyPressed(KeyEvent ke)
        {
          if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          {
              addButton_actionPerformed();
          }
        }
      });
      
      _totalComponentTextField.addKeyListener(new java.awt.event.KeyAdapter()
      {
        public void keyPressed(KeyEvent ke)
        {
          if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          {
              addButton_actionPerformed();
          }
        }
      });
    
  }
  
    /**
     * @author Jack Hwee
     */
    private void addButton_actionPerformed() 
    {
      int totalComponent;
      int totalError;
      
      try 
      {
           if(_totalComponentTextField.getText().isEmpty() == false)
           {
                if(_totalErrorTextField.getText().isEmpty() == false)
                {
                    totalComponent = StringUtil.convertStringToInt(_totalComponentTextField.getText());
                    totalError = StringUtil.convertStringToInt(_totalErrorTextField.getText());

                    if (totalComponent > 0 && totalError > 0)
                    {
                       if (_totalComponentList.contains(totalComponent) == false)
                       {
                           //addDynamicCallNumber(totalComponent, totalError)
                           _totalComponentAdded = totalComponent;
                           _totalErrorAdded = totalError;
                           
                       }
                       else
                       {
                            String message = StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_COMPONENT_ALREADY_EXIST_TITLE_KEY");

                            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    message,
                                    StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                                    JOptionPane.ERROR_MESSAGE);

                            return;
                        }           
                    }
                    else 
                    {
                        String message = StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_COMPONENT_OR_TOTAL_ERROR_CANNOT_BLANK_AND_NEGATIVE_TITLE_KEY");

                        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                message,
                                StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                                JOptionPane.ERROR_MESSAGE);

                        return;
                    }

                    this.dispose();
              }
               else 
              {
                    String message = StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_COMPONENT_OR_TOTAL_ERROR_CANNOT_BLANK_TITLE_KEY");

                    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                            message,
                            StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                            JOptionPane.ERROR_MESSAGE);

                    return;
              }
         }
         else 
         {
                String message = StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_DIALOG_TOTAL_COMPONENT_OR_TOTAL_ERROR_CANNOT_BLANK_TITLE_KEY");

                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                        message,
                        StringLocalizer.keyToString("PSP_GENERATE_SURFACE_MAPPING_KEY"),
                        JOptionPane.ERROR_MESSAGE);

                return;
         }
      } 
      catch (BadFormatException ex) 
      {
            // Shouldn't ever occur.
        System.out.println( "first string: " +  _totalComponentTextField.getText() );
        System.out.println( "second string: " + _totalErrorTextField.getText() );
        Assert.logException(ex);
      }            
    }
    
  /*
   * @author Jack Hwee
   */
  public PanelCoordinate getAddedCoordinate()
  {
     return _addedCoordinate;
  }
  
  /*
   * @author Jack Hwee
   */
  public boolean isAddedDynamicCallNumberAvailable()
  {
     if (_addedCoordinate != null)
       return true;
     else
       return false;          
  }
 
   /*
   * @author Jack Hwee
   */
  public int getTotalComponentAdded()
  {     
      return _totalComponentAdded;
  }
  
   /*
   * @author Jack Hwee
   */
  public int getTotalErrorAdded()
  {     
      return _totalErrorAdded;
  }
 
}
