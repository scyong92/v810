package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;

import com.axi.v810.business.resultsProcessing.*;

/**
 * <p>Description: Table cell editor for results processing rule condition key values</p>
 * @author Laura Cormos
 */
class ConditionKeyCellEditor extends DefaultCellEditor
{
  JComboBox _comboBox = null;

  /**
   * @author Laura Cormos
   * Constructor.
   * Initializes the drop down box with all the condition key enum values
   */
  public ConditionKeyCellEditor()
  {
    super(new JComboBox());
    _comboBox = (JComboBox)getComponent();

    Collection<ConditionKeyEnum> conditionKeyEnums = ConditionKeyEnum.getAllEnums();
    for (ConditionKeyEnum conditionKeyEnum : conditionKeyEnums)
      _comboBox.addItem(conditionKeyEnum);
  }
}
