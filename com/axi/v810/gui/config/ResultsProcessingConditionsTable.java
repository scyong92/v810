package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
class ResultsProcessingConditionsTable extends JTable
{
  public static final int _CONDITION_KEY_NAME_INDEX = 0;
  public static final int _PATTERN_TYPE_INDEX = 1;
  public static final int _PATTERN_INDEX = 2;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _CONDITION_NAME_COLUMN_WIDTH_PERCENTAGE = .15;
  private static final double _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _PATTERN_COLUMN_WIDTH_PERCENTAGE = .75;

  private ConditionKeyCellEditor _conditionKeyEditor = new ConditionKeyCellEditor();
  private PatternTypeEditor _patternTypeEditor = new PatternTypeEditor();

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public ResultsProcessingConditionsTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int conditionNameColumnWidth = (int)(totalColumnWidth * _CONDITION_NAME_COLUMN_WIDTH_PERCENTAGE);
    int simpleRegexColumnWidth = (int)(totalColumnWidth * _PATTERN_TYPE_COLUMN_WIDTH_PERCENTAGE);
    int regexColumnWidth = (int)(totalColumnWidth * _PATTERN_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_CONDITION_KEY_NAME_INDEX).setPreferredWidth(conditionNameColumnWidth);
    columnModel.getColumn(_PATTERN_TYPE_INDEX).setPreferredWidth(simpleRegexColumnWidth);
    columnModel.getColumn(_PATTERN_INDEX).setPreferredWidth(regexColumnWidth);
  }

  /**
   * @author Laura Cormos
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    columnModel.getColumn(_CONDITION_KEY_NAME_INDEX).setCellEditor(_conditionKeyEditor);

    columnModel.getColumn(_PATTERN_TYPE_INDEX).setCellEditor(_patternTypeEditor);

    columnModel.getColumn(_PATTERN_INDEX).setCellEditor(stringEditor);
  }


  /**
   * @return a list of selected boards.
   * @author George A. David
   */
//  public java.util.List<Rule> getSelectedRules()
//  {
//    ResultsProcessingRulesTableModel model = (ResultsProcessingRulesTableModel)getModel();
//    java.util.List<Rule> selectedRules = new ArrayList<Rule>();
//    int rowCount = getRowCount();
//
//    for(int i = 0; i < rowCount; ++i)
//    {
//      if(selectionModel.isSelectedIndex(i))
//      {
//        selectedRules.add(model.getRule(i));
//      }
//    }
//
//    return selectedRules;
//  }
}
