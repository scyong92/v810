package com.axi.v810.gui.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
class ConfigGuiPersistence implements Serializable
{
  // Initial Thresholds persistance settings
  private int _initialThresholdsTableSummaryDivider = -1;
  private java.util.List<Integer> _initialThresholdsTableColumnSizes = new ArrayList<Integer>();
  
  private int _intialRecipeSettingTableSummaryDivider = -1;
  private java.util.List<Integer> _initialRecipeSettingTableColumnSizes = new ArrayList<Integer>();
  // results processing settings
  private int _conditionFileTransferDivider = -1;
  private int _destinationsDivider = -1;

  /**
   * @author Laura Cormos
   */
  ConfigGuiPersistence()
  {
    // default settings for the alg tuner
    _initialThresholdsTableColumnSizes.add(new Integer(200)); // Name column default size
    _initialThresholdsTableColumnSizes.add(new Integer( -1)); //  Value column default size
    _initialThresholdsTableColumnSizes.add(new Integer( -1)); //  Default column default size
    _initialThresholdsTableColumnSizes.add(new Integer( -1)); //  Min column default size
    _initialThresholdsTableColumnSizes.add(new Integer( -1)); //  Max column default size
    
    _initialRecipeSettingTableColumnSizes.add(new Integer(200)); // Name column default size
    _initialRecipeSettingTableColumnSizes.add(new Integer( -1)); //  Value column default size
    _initialRecipeSettingTableColumnSizes.add(new Integer( -1)); 
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getInitialThresholdsTableSummaryDivider()
  {
    return _initialThresholdsTableSummaryDivider;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public int getInitialRecipeSettingTableSummaryDivider()
  {
    return _intialRecipeSettingTableSummaryDivider;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialThresholdsTableSummaryDivider(int initialThresholdsTableSummaryDivider)
  {
    _initialThresholdsTableSummaryDivider = initialThresholdsTableSummaryDivider;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public void setInitialRecipeSettigTableSummaryDivider(int initialRecipeSettngTableSummaryDivider)
  {
    _intialRecipeSettingTableSummaryDivider = initialRecipeSettngTableSummaryDivider;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public int getInitialThresholdsTableColumnWidth(int column)
  {
    Integer size = (Integer)_initialThresholdsTableColumnSizes.get(column);
    Assert.expect(size != null);
    return size.intValue();
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public int getInitialRecipeSettingTableColumnWidth(int column)
  {
    Integer size = (Integer)_initialRecipeSettingTableColumnSizes.get(column);
    Assert.expect(size != null);
    return size.intValue();
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public void setInitialThresholdsTableColumnWidth(int column, int width)
  {
    Integer size = new Integer(width);
    _initialThresholdsTableColumnSizes.set(column, size);
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public void setInitialRecipeSettingTableColumnWidth(int column, int width)
  {
    Integer size = new Integer(width);
    _initialRecipeSettingTableColumnSizes.set(column, size);
  }

  /**
   * @author Laura Cormos
   */
  int getConditionFileTransferDivider()
  {
    return _conditionFileTransferDivider;
  }

  /**
   * @author Laura Cormos
   */
  void setConditionFileTransferDivider(int dividerPosition)
  {
    _conditionFileTransferDivider = dividerPosition;
  }

  /**
   * @author Laura Cormos
   */
  int getDestinationsDivider()
  {
    return _destinationsDivider;
  }

  /**
   * @author Laura Cormos
   */
  void setDestinationsDivider(int dividerPosition)
  {
    _destinationsDivider = dividerPosition;
  }

   /**
   * @author Laura Cormos
   */
  public ConfigGuiPersistence readSettings()
  {
    ConfigGuiPersistence persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getConfigUIStatePersistFullPath()))
        persistance = (ConfigGuiPersistence)FileUtilAxi.loadObjectFromSerializedFile(FileName.getConfigUIStatePersistFullPath());
      else
        persistance = new ConfigGuiPersistence();
    }
    catch (DatastoreException de)
    {
      // do nothing..
      persistance = new ConfigGuiPersistence();
    }

    return persistance;
  }

  /**
   * @author Laura Cormos
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getConfigUIStatePersistFullPath());
  }

}
