package com.axi.v810.gui.config;

import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.datastore.config.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

public class SoftwareOptionsTaskPanel extends AbstractTaskPanel
{
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JTabbedPane _tabbedPane = new JTabbedPane();
  private SoftwareOptionsGeneralTabPanel _generalTabPanel;
  private SoftwareOptionsInitialThresholdsTabPanel _initialThresholdsTabPanel;
  private SoftwareOptionsInitialRecipeSettingTabPanel _initialRecipeSettingTabPanel;
  private SoftwareOptionsJointTypeTabPanel _jointTypeTabPanel;
  private SoftwareOptionsCustomizeSettingTabPanel _customizeSettingTabPanel;
  private DeveloperDebugModeTabPanel _developDebugModeTabPanel;

  private ConfigEnvironment _envPanel = null;

  private JMenuItem _setDefaultMenuItem = new JMenuItem();
  private JMenuItem _configHelpMenuItem = new JMenuItem();

  private AbstractTaskPanel _currentActiveTabPanel;

  /**
   * @author Laura Cormos
   */
  public SoftwareOptionsTaskPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.add(_tabbedPane, BorderLayout.CENTER);
    _generalTabPanel = new SoftwareOptionsGeneralTabPanel(_envPanel);
    _tabbedPane.add(_generalTabPanel, StringLocalizer.keyToString("CFGUI_GENERAL_TAB_LABEL_KEY"));
    _initialThresholdsTabPanel = new SoftwareOptionsInitialThresholdsTabPanel(_envPanel);
    _tabbedPane.add(_initialThresholdsTabPanel, StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_TAB_LABEL_KEY"));
    _initialRecipeSettingTabPanel = new SoftwareOptionsInitialRecipeSettingTabPanel(_envPanel);
    _tabbedPane.add(_initialRecipeSettingTabPanel, StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_LABEL_KEY"));
    _jointTypeTabPanel = new SoftwareOptionsJointTypeTabPanel(_envPanel);
    _tabbedPane.add(_jointTypeTabPanel, StringLocalizer.keyToString("CFGUI_JOINT_TYPE_TAB_LABEL_KEY"));
    _customizeSettingTabPanel = new SoftwareOptionsCustomizeSettingTabPanel(_envPanel);
    _tabbedPane.add(_customizeSettingTabPanel, StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_TAB_LABEL_KEY"));
    
    if (Config.isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
    {
      _developDebugModeTabPanel = new DeveloperDebugModeTabPanel(_envPanel);
      _tabbedPane.add(_developDebugModeTabPanel, StringLocalizer.keyToString("CFGUI_JTA_DEVELOPER_DEBUG_CONFIG_KEY"));
    }
    
    _tabbedPane.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tabbedPane_stateChanged(e);
      }
    });

    _currentActiveTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();

    // define menus and toolBar items to be added later

//    _setDefaultMenuItem.setText("Set &Default User Config");
//    _setDefaultMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        setDefault(e);
//      }
//    });

//    _configHelpMenuItem.setText("&Config User Help");
//    _configHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        configHelp(e);
//      }
//    });
  }

  /**
   * set default action
   * @param e ActionEvent
   * @author George Booth
   */
  void setDefault(ActionEvent e)
  {
//    System.out.println("SWOptionsTaskPanel().setDefault()");
  }

  /**
   * config help action
   * @param e ActionEvent
   * @author George Booth
   */
  void configHelp(ActionEvent e)
  {
//    System.out.println("SWOptionsTaskPanel().configHelp()");
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("SWOptionsTaskPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();


    // add custom menus
//    JMenu configMenu = _envPanel.getConfigMenu();
//    _menuBar.addMenuItem(_menuRequesterID, configMenu, _setDefaultMenuItem);

//    JMenu helpMenu = _menuBar.getHelpMenu();
//    _menuBar.addMenuItem(_menuRequesterID, helpMenu, 0, _configHelpMenuItem);

//    _menuBar.addMenuSeparator(_menuRequesterID, helpMenu, 1);

    _currentActiveTabPanel.start();
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
//    System.out.println("SWOptionsTaskPanel().isReadyToFinish()");
    boolean flag = _currentActiveTabPanel.isReadyToFinish();
//    System.out.println("Returning " + flag);
    return flag;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    _currentActiveTabPanel.finish();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

//    System.out.println("SWOptionsTaskPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void tabbedPane_stateChanged(ChangeEvent e)
  {
    AbstractTaskPanel selectedTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();

    if (_currentActiveTabPanel == selectedTabPanel)
      return;

    // first finish the currently active panel
    if (_currentActiveTabPanel.isReadyToFinish())
    {
      _currentActiveTabPanel.finish();
    }
    else
    {
      _tabbedPane.setSelectedComponent(_currentActiveTabPanel);
      return;
    }

    // then start the chosen tab is the previous tab is finished
    selectedTabPanel.start();
    _currentActiveTabPanel = selectedTabPanel;
  }
}
