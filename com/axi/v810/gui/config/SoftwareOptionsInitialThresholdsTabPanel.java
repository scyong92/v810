package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class SoftwareOptionsInitialThresholdsTabPanel extends AbstractTaskPanel implements Observer
{
  private ConfigGuiPersistence _persistSettings;
  private com.axi.v810.gui.undo.CommandManager _commandManager =
          com.axi.v810.gui.undo.CommandManager.getConfigInstance();  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();
  private InitialThresholds _initialThresholds = InitialThresholds.getInstance();
  private boolean _settingWidthsProgrammatically = false;  // used to turn off width persistance unless user is changing widths

  // GUI controls
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdsPanel = new JPanel();
  private BorderLayout _thresholdsPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdsSetNamePanel = new JPanel();
  private BorderLayout _thresholdsSetNamePanelBorderLayout = new BorderLayout();
  private JPanel _thresholdSetSelectPanel = new JPanel();
  private FlowLayout _thresholdSetSelectPanelFlowLayout = new FlowLayout();
  private JLabel _thresholdSetSelectNameLabel = new JLabel();
  private JComboBox _thresholdSetSelectNameCombobox = new JComboBox();
  private ActionListener _thresholdSetSelectComboboxActionListener;
  private ActionListener _jointTypeSelectComboboxActionListener;
  private JPanel _thresholdSetSelectActionsWrapperPanel = new JPanel();
  private FlowLayout _thresholdSetSelectActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _thresholdSetSelectActionsPanel = new JPanel();
  private GridLayout _thresholdSetSelectActionsPanelGridLayout = new GridLayout();
  private JButton _newThresholdSetButton = new JButton();
  private JButton _saveThresholdSetButton = new JButton();
  private JButton _saveAsThresholdSetButton = new JButton();
  private JButton _deleteThresholdSetButton = new JButton();
  private JPanel _thresholdSetDefinitionPanel = new JPanel();
  private BorderLayout _thresholdSetDefinitionPanelBorderLayout = new BorderLayout();
  private JPanel _thresholdTablePanel = new JPanel();
  private BorderLayout _thresholdTablePanelPanelBorderLayout = new BorderLayout();
  private JTabbedPane _thresholdsTabbedPane = new JTabbedPane();
  private JComboBox _jointTypeComboBox = new JComboBox();
  private JComboBox _unitsComboBox = new JComboBox();
  private JPanel _setControlPanel = new JPanel();
  private JPanel _jointTypeComboBoxPanel = new JPanel();
  private JPanel _unitsComboBoxPanel = new JPanel();
  private JLabel _unitsLabel = new JLabel();
  private JLabel _jointTypeLabel = new JLabel();
  private JEditorPane _summaryEditorPane = new JEditorPane();
  private JPanel _summaryInformationPane = new JPanel();
  private BorderLayout _summaryInformationBorderLayout = new BorderLayout();
  private JLabel _summaryLabel = new JLabel();
  private JScrollPane _summaryScrollPane;
  private JSplitPane _thresholdSummarySplitPane = new JSplitPane();

  //@author Wei Chin, Chong
  private JLabel _textExplainationLabel = new JLabel();
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles
  private JPanel _thresholdsSetLocationPanel = new JPanel();
  private JLabel _thresholdsLocationLabel = new JLabel();
  private JTextField _thresholdsDirectoryTextField;
  private ConfigEnvironment _envPanel = null;
  private boolean _inUse = false;
  private boolean _isChangingDirectories = false;
  private ActionListener _thresholdDirectoryTextFieldActionListener;
  private FocusAdapter _thresholdDirectoryFocusAdapter;

  /**
   * @author Andy Mechtenberg
   */
  public SoftwareOptionsInitialThresholdsTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    _persistSettings = envPanel.getPersistance();
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit() throws Exception
  {
    // set up panel GUI components
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _centerPanel.add(_thresholdsPanel, BorderLayout.CENTER);

    _thresholdsPanel.setLayout(_thresholdsPanelBorderLayout);
    _thresholdsPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
            StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_SET_KEY")),
            BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _thresholdsPanel.add(_thresholdsSetNamePanel, BorderLayout.NORTH);
    _thresholdsPanel.add(_thresholdSetDefinitionPanel, BorderLayout.CENTER);

    _thresholdsSetNamePanel.setLayout(_thresholdsSetNamePanelBorderLayout);
    _thresholdsSetNamePanel.add(_thresholdSetSelectPanel, BorderLayout.WEST);
    _thresholdsSetNamePanel.add(_thresholdSetSelectActionsWrapperPanel, BorderLayout.EAST);

    _thresholdSetSelectPanel.setLayout(_thresholdSetSelectPanelFlowLayout);
    _thresholdSetSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _thresholdSetSelectPanel.add(_thresholdSetSelectNameLabel);
    _thresholdSetSelectPanel.add(_thresholdSetSelectNameCombobox);

    _thresholdSetSelectNameLabel.setText(StringLocalizer.keyToString("CFGUI_NAME_KEY"));

    //@author Wei Chin, Chong
    int textFieldColumns = 50;
    _thresholdsSetLocationPanel.setLayout(_thresholdSetSelectPanelFlowLayout);
    _thresholdsLocationLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_LOCATION_KEY"));

    _thresholdsDirectoryTextField = new JTextField(textFieldColumns);
    JButton thresholdsDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    _thresholdsDirectoryTextField.setText(Directory.getInitialThresholdsDir());
    _thresholdsSetLocationPanel.add(_thresholdsLocationLabel);
    _thresholdsSetLocationPanel.add(_thresholdsDirectoryTextField);

    _thresholdsSetLocationPanel.add(thresholdsDirectoryChooserButton);
    _thresholdsSetNamePanel.add(_thresholdsSetLocationPanel, BorderLayout.NORTH);

    JLabel [] labels = new JLabel[]{_thresholdSetSelectNameLabel, _thresholdsLocationLabel};
    SwingUtils.makeAllSameLength(labels);
    thresholdsDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        thresholdsDirectoryChooser();
      }
    });

    _thresholdDirectoryFocusAdapter = new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        thresholdsDirectoryTextField_setValue();
      }
    };

    _thresholdDirectoryTextFieldActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        thresholdsDirectoryTextField_setValue();
      }
    };
    _thresholdsDirectoryTextField.addFocusListener(_thresholdDirectoryFocusAdapter);
    _thresholdsDirectoryTextField.addActionListener(_thresholdDirectoryTextFieldActionListener);
    // end

    _thresholdSetSelectComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        thresholdSetChanged(false);
      }
    };

    _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);

    _thresholdSetSelectActionsWrapperPanel.setLayout(_thresholdSetSelectActionsWrapperPanelFlowLayout);
    _thresholdSetSelectActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _thresholdSetSelectActionsWrapperPanel.add(_thresholdSetSelectActionsPanel);

    _thresholdSetSelectActionsPanel.setLayout(_thresholdSetSelectActionsPanelGridLayout);
    _thresholdSetSelectActionsPanelGridLayout.setColumns(4);
    _thresholdSetSelectActionsPanelGridLayout.setHgap(10);
    _thresholdSetSelectActionsPanelGridLayout.setRows(0);
    _thresholdSetSelectActionsPanelGridLayout.setVgap(10);
    _thresholdSetSelectActionsPanel.add(_newThresholdSetButton);
    _thresholdSetSelectActionsPanel.add(_saveThresholdSetButton);
    _thresholdSetSelectActionsPanel.add(_saveAsThresholdSetButton);
    _thresholdSetSelectActionsPanel.add(_deleteThresholdSetButton);

    _newThresholdSetButton.setText(StringLocalizer.keyToString("CFGUI_NEW_BUTTON_KEY"));
    _newThresholdSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        newThresholdSetButton_actionPerformed();
      }
    });

    _saveThresholdSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_BUTTON_KEY"));
    _saveThresholdSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        saveCurrentThresholdSet();
      }
    });

    _saveAsThresholdSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_AS_BUTTON_KEY"));
    _saveAsThresholdSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        saveThresholdSetAs();
      }
    });

    _deleteThresholdSetButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteThresholdSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        deleteThresholdSetButton_actionPerformed();
      }
    });



    _thresholdSetDefinitionPanel.setLayout(_thresholdSetDefinitionPanelBorderLayout);
    _thresholdSetDefinitionPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
            StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_SET_DEFINITION_KEY")),
            BorderFactory.createEmptyBorder(0, 5, 5, 5)));


    _thresholdTablePanel.setLayout(_thresholdTablePanelPanelBorderLayout);
    _thresholdTablePanel.add(_thresholdsTabbedPane, BorderLayout.CENTER);
    _thresholdsTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        thresholdsTabbedPane_stateChanged(e);
      }
    });
    _thresholdSummarySplitPane.add(_thresholdTablePanel, JSplitPane.LEFT);

    // summary panel stuff
    _summaryInformationBorderLayout.setVgap(5);
    _summaryInformationPane.setLayout(_summaryInformationBorderLayout);

    _summaryEditorPane.setEditable(false);
    _summaryEditorPane.setContentType("text/html");
    _summaryScrollPane = new JScrollPane(_summaryEditorPane);
    _summaryLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_SUMMARY_KEY"));
    _summaryLabel.setFont(FontUtil.getBoldFont(_summaryLabel.getFont(),Localization.getLocale()));
    _summaryLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    _summaryInformationPane.add(_summaryLabel, BorderLayout.NORTH);
    _summaryInformationPane.add(_summaryScrollPane, BorderLayout.CENTER);
    _thresholdSummarySplitPane.add(_summaryInformationPane, JSplitPane.RIGHT);
    _thresholdSummarySplitPane.setDividerLocation(0.70);

    _thresholdSetDefinitionPanel.add(_thresholdSummarySplitPane, BorderLayout.CENTER);

    _unitsComboBoxPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _unitsLabel.setText(StringLocalizer.keyToString("RDGUI_UNITS_COLUMN_KEY") + ":");
    _unitsComboBoxPanel.add(_unitsLabel);
    _unitsComboBoxPanel.add(_unitsComboBox);

    _jointTypeComboBoxPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    _jointTypeLabel.setText(StringLocalizer.keyToString("ATGUI_FAMILY_KEY") + ":");
    _jointTypeComboBoxPanel.add(_jointTypeLabel);
    _jointTypeComboBoxPanel.add(_jointTypeComboBox);

    JLabel[] allLabels = new JLabel[]{_unitsLabel,_jointTypeLabel};
    SwingUtils.makeAllSameLength(allLabels);

    GridLayout setControlGridLayout = new GridLayout();
    setControlGridLayout.setRows(3);
    setControlGridLayout.setVgap(-5);
    _textExplainationLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_EXPLAINATION_KEY"));
    _textExplainationLabel.setForeground(_labelColor);
    _setControlPanel.setLayout(setControlGridLayout);
    _setControlPanel.add(_unitsComboBoxPanel);
    _setControlPanel.add(_jointTypeComboBoxPanel);
    _setControlPanel.add(_textExplainationLabel);

    _thresholdSetDefinitionPanel.add(_setControlPanel, BorderLayout.NORTH);
    //add measurement unit options if they have not been previously added
    if(_unitsComboBox.getItemCount() == 0)
    {
//      _unitsComboBox.addItem(MeasurementUnits.MEASURE_INCHES);  // no inches -- threshold values are never displayed in inches anyway
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILLIMETERS);
      _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILS);
    }
    _unitsComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unitsChanged();
      }
    });
    populateJointTypeComboBox();
    populateThresholdTables();

    _jointTypeSelectComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jointTypeChanged();
      }
    };
    _jointTypeComboBox.addActionListener(_jointTypeSelectComboboxActionListener);

    JComboBox[] allComboboxes = new JComboBox[]{_unitsComboBox,_jointTypeComboBox};
    SwingUtils.makeAllSameLength(allComboboxes);
//    setupTableEditors();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateJointTypeComboBox()
  {
    _jointTypeComboBox.removeAllItems();

    FontMetrics fontMetrics = _jointTypeComboBox.getFontMetrics(_jointTypeComboBox.getFont());
    int maxStringLength = 0;

    java.util.List<JointTypeEnum> supportedJointTypes = JointTypeEnum.getAllJointTypeEnums();

    for (JointTypeEnum jte : supportedJointTypes)
    {
      _jointTypeComboBox.addItem(jte);
    }


    for (JointTypeEnum jointType : supportedJointTypes)
    {
      int itemStringLength = fontMetrics.stringWidth(jointType.getName());
      if (maxStringLength < itemStringLength)
      {
        maxStringLength = itemStringLength;
      }
    }
    _jointTypeComboBox.setMaximumRowCount(supportedJointTypes.size());
    _jointTypeComboBox.setPreferredSize(new Dimension(maxStringLength + VariableWidthComboBox.widthExtension, CommonUtil._COMBO_BOX_HEIGHT));
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateThresholdTables()
  {
    // clear out all the tabs and the tables in them in preparation for creating new ones
    _thresholdsTabbedPane.removeAll();

    JointTypeEnum currentJointType = (JointTypeEnum) _jointTypeComboBox.getSelectedItem();

    //System.out.println("Populate Threshold Table");
    // populate the threshold tabs and tables with them with Datastore data
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(currentJointType);
    java.util.List<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();

    Collections.sort(algorithmEnums, new AlgorithmEnumInspectionOrderComparator());

    for (AlgorithmEnum algoEnum : algorithmEnums)
    {
      Algorithm algo = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), algoEnum);
      InitialThresholdsTableModel threshTableModel = new InitialThresholdsTableModel(this, currentJointType, algo, false);
      if (threshTableModel.getRowCount() > 0)  // don't add the table if there are no thresholds
      {
        JPanel tablePanel = new JPanel();
        BorderLayout tablePanelBorderLayout = new BorderLayout();
        tablePanel.setLayout(tablePanelBorderLayout);
        JScrollPane jsp = new JScrollPane();

        //_thresholdsTabbedPane.setSelectedComponent(jsp);

        JTable tt = new JTable(threshTableModel);
        jsp.getViewport().add(tt);
        tablePanel.add(jsp, BorderLayout.CENTER);
        _thresholdsTabbedPane.add(tablePanel, algo.getName());
        setupTableCustomizations(tt);
        setupTableEditorsAndRenderers(algo, tt);
        tt.changeSelection(0, 1, false, false);
      }
     }
     populateSliceHeightTable(currentJointType);
    _thresholdsTabbedPane.requestFocus();

    validate();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateSliceHeightTable(JointTypeEnum jointTypeEnum)
  {
    // this extracts all the slice height thresholds out, and creates a new tab to put them all in
    // add the last tab for slice height settings
    JPanel panel = new JPanel();
    BorderLayout panelBorderLayout = new BorderLayout();
    panel.setLayout(panelBorderLayout);

    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);
    // all slice controlling thresholds are in the Measurement algorithm
    Algorithm algorithm = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);

    ThresholdTableModel threshTableModel = new InitialThresholdsTableModel(this,
                                                                           jointTypeEnum,
                                                                           algorithm,
                                                                           true); // slice settings
    //table model cannot be empty, all joint types have at least one slice height algorithm setting
    Assert.expect(threshTableModel.getRowCount() > 0);

    JScrollPane jsp = new JScrollPane();
    JTable tt = new JTable(threshTableModel);
    jsp.getViewport().add(tt);
    panel.add(jsp, BorderLayout.CENTER);
    _thresholdsTabbedPane.add(panel, StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY"));

    setupTableCustomizations(tt);
    setupTableEditorsAndRenderers(algorithm, tt);
    tt.changeSelection(0, 1, false, false);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void setupTableCustomizations(final JTable tt)
  {
    tt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    _settingWidthsProgrammatically = true;
    for (int i = 0; i < tt.getColumnCount(); i++)
    {
//      System.out.println("SETUP: Setting table column " + i + " to width " + _persistSettings.getInitialThresholdsTableColumnWidth(i));
      tt.getColumnModel().getColumn(i).setPreferredWidth(_persistSettings.getInitialThresholdsTableColumnWidth(i));
    }
    _settingWidthsProgrammatically = false;

    tt.getColumnModel().addColumnModelListener(getThresholdTableColumnModelListener(tt));
    tt.getTableHeader().setReorderingAllowed(false);

//    tt.setRowSelectionAllowed(false);
//    tt.setColumnSelectionAllowed(false);

    // add the listener for table column selection
    ListSelectionModel colSM = tt.getColumnModel().getSelectionModel();
    colSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        //int selectedColumn = lsm.getMinSelectionIndex();
        tt.setColumnSelectionInterval(InitialThresholdsTableModel.VALUE_COLUMN, InitialThresholdsTableModel.VALUE_COLUMN);  // don't allow any column but 1 -- the value column
      }
    });

    // add the listener for a table row selection
    ListSelectionModel rowSM = tt.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {

      public void valueChanged(ListSelectionEvent e)
      {
        //System.out.println("List Value Changed changed");
        ListSelectionModel lsm = (ListSelectionModel) e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = lsm.getMinSelectionIndex();
          //System.out.println("List Selection Changed To Row: " + selectedRow);
          //CustomThresholdEditor tce = (CustomThresholdEditor)tt.getColumn(tt.getModel().getColumnName(1)).getCellEditor();
          tt.changeSelection(selectedRow, InitialThresholdsTableModel.VALUE_COLUMN, false, false);
//          updateThresholdDescription(subtype, tt, selectedRow);
          if (!tt.isEditing())
          {
            tt.editCellAt(selectedRow, InitialThresholdsTableModel.VALUE_COLUMN);
            java.awt.Component comp = tt.getEditorComponent();
            // do a request focus on the cell editor lets the default behaviour of selection all the text happen
            if (comp instanceof JButton)
            {
              tt.getCellEditor(selectedRow, InitialThresholdsTableModel.VALUE_COLUMN).stopCellEditing();
            }
            if (comp instanceof JTextField)
            {
              comp.requestFocus();
            }
          }
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void thresholdsTabbedPane_stateChanged(ChangeEvent e)
  {
    // first capture the current tab's table column widths, and set them to the persistance
    // then set the new tab to those widths
    int selectedTab = _thresholdsTabbedPane.getSelectedIndex();
    if (selectedTab < 0)  // no tabs
      return;
    JTable table = findCurrentTable();
    _settingWidthsProgrammatically = true;
    for (int i = 0; i < table.getColumnCount(); i++)
    {
//      System.out.println("TAB CHANGE: Setting table column " + i + " to width " + _persistSettings.getInitialThresholdsTableColumnWidth(i));
      table.getColumnModel().getColumn(i).setPreferredWidth(_persistSettings.getInitialThresholdsTableColumnWidth(i));
    }
    _settingWidthsProgrammatically = false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private JTable findCurrentTable()
  {
    JTable table = null;
    java.awt.Component comp = _thresholdsTabbedPane.getSelectedComponent();
    Assert.expect(comp instanceof JPanel);
    JPanel compPanel = (JPanel)comp;
    java.awt.Component[] componentArray = compPanel.getComponents();
    for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
    {
      java.awt.Component potentialComp = componentArray[compIndex];
      if (potentialComp instanceof JScrollPane)
      {
        JScrollPane jsp = (JScrollPane)potentialComp;
        java.awt.Component comp1 = (JTable)jsp.getViewport().getView();
        if (comp1 == null)
          table = null;
        else
        {
          table = (JTable)comp1;
        }
      }
    }
    Assert.expect(table != null);
    return table;
  }

  /**
   * @author Andy Mechtenberg, George A. David
   */
  private void setupTableEditorsAndRenderers(final Algorithm algorithm, final JTable tt)
  {
    Assert.expect(tt != null);

    int rows = tt.getRowCount();
    final CustomThresholdEditor rowEditor = new CustomThresholdEditor(tt);
    tt.getColumn(tt.getModel().getColumnName(1)).setCellEditor(rowEditor);

    for (int i = 0; i < rows; i++)
    {
      Object elementValue = tt.getModel().getValueAt(i, 0);
      if (elementValue instanceof AlgorithmSetting)
      {
        final AlgorithmSetting algorithmSetting = (AlgorithmSetting) tt.getModel().getValueAt(i, 0);  // 0 column is the AlgorithmSetting object
        Object value = algorithmSetting.getDefaultValue(algorithm.getVersion());
        Assert.expect(value != null);
        if (value instanceof Integer)
        {
          //System.out.println("Integer");
          final Integer defaultVal = (Integer) algorithmSetting.getDefaultValue(algorithm.getVersion());
          final IntegerNumberField integerField = new IntegerNumberField(0, 5);
          DefaultCellEditor integerEditor = new DefaultCellEditor(integerField)
          {

            public Object getCellEditorValue()
            {
              int intvalue = integerField.getValue();
              if (intvalue == IntegerNumberField.EMPTY_VALUE)
              {
                intvalue = defaultVal.intValue();
              }
              return new Integer(intvalue);
            //return new Integer(integerField.getValue());
            }
          };
          integerEditor.setClickCountToStart(1);
          integerField.addFocusListener(new java.awt.event.FocusAdapter()
          {

            public void focusGained(FocusEvent e)
            {
              integerField.selectAll();
            }

            public void focusLost(FocusEvent e)
            {
              // This was an experiment to handle full keyboard navigation.  Didn't work, but I'll keep the code here
              // commented out to record what I tried.
//            if(e.isTemporary())
//              return;
//            System.out.println("Lost Focus @ " + e.paramString());
              //This focus listener was added to catch the event of losing
              //focus when another part of the GUI is clicked on, say the Run button.
              //However, this is also called when the table loses focus, but that is already
              //handled correctly by swing. The problem arises when using the up/down arrow
              //keys to navigate the table. When the key is pressed, Swing tells the cell
              //to stop editing and focuses the next cell and begins editing it. Then
              //this also gets called because the previous cell lost focus. At this
              //point though, Swing has already correctly handled the event. If we allow
              //it to continue, this code will stop the new cell from editing  This is
              //not the behavior we want. We only want to run this code if this FocusEvent was
              //initiated by another component.
              //George A. David
              if (tt.isEditing() && e.getOppositeComponent() != tt)
              {
                rowEditor.stopCellEditing();
              }
            }
          });
          rowEditor.setEditorAt(i, integerEditor);
        }
        else if (value instanceof Float)
        {
          final Float defaultVal = (Float) algorithmSetting.getDefaultValue(algorithm.getVersion());
          final FloatNumberField floatField = new FloatNumberField(0, 5);
          DefaultCellEditor floatEditor = new DefaultCellEditor(floatField)
          {

            public Object getCellEditorValue()
            {
              float floatvalue = floatField.getValue();
              if (floatvalue == FloatNumberField.EMPTY_VALUE)
              {
                floatvalue = defaultVal.floatValue();
                MeasurementUnitsEnum thresholdUnits = algorithmSetting.getUnits(algorithm.getVersion());
                MathUtilEnum tableUnits = _initialThresholds.getCurrentUnits();
                if (thresholdUnits.equals(MeasurementUnitsEnum.MILLIMETERS) &&
                        tableUnits.isMetric() == false)
                {
                  // convert from mils (mils is what the user has input)
                  floatvalue = MathUtil.convertMillimetersToMils(floatvalue);
                }
              }
              return new Float(floatvalue);
            //return new Float(floatField.getValue());
            }
          };
          floatEditor.setClickCountToStart(1);
          floatField.addFocusListener(new java.awt.event.FocusAdapter()
          {

            public void focusGained(FocusEvent e)
            {
              floatField.selectAll();
            }

            public void focusLost(FocusEvent e)
            {
//            System.out.println("Lost Focus: " + e.paramString());
              //This focus listener was added to catch the event of losing
              //focus when another part of the GUI is clicked on, say the Run button.
              //However, this is also called when the table loses focus, but that is already
              //handled correctly by swing. The problem arises when using the up/down arrow
              //keys to navigate the table. When the key is pressed, Swing tells the cell
              //to stop editing and focuses the next cell and begins editing it. Then
              //this also gets called because the previous cell lost focus. At this
              //point though, Swing has already correctly handled the event. If we allow
              //it to continue, this code will stop the new cell from editing  This is
              //not the behavior we want. We only want to run this code if this FocusEvent was
              //initiated by another component.
              //George A. David
              if (tt.isEditing() && e.getOppositeComponent() != tt)
              {
                rowEditor.stopCellEditing();
              }
            }
          });
          rowEditor.setEditorAt(i, floatEditor);
        }
        else if (value instanceof Boolean)
        {
          // use a combo box with "Yes" and "No" in it
          final VariableWidthComboBox comboBox = new VariableWidthComboBox();
          comboBox.setPopupWidthToDefault();
          // set the popup width to the max of the strings only if that's longer than the box itself
          java.util.List<String> availableValues = new ArrayList<String>();
          availableValues.add(StringLocalizer.keyToString("GUI_YES_BUTTON_KEY"));
          availableValues.add(StringLocalizer.keyToString("GUI_NO_BUTTON_KEY"));
          FontMetrics fontMetrics = comboBox.getFontMetrics(comboBox.getFont());
          int maxStringLength = 0;
          for (String valueString : availableValues)
          {
            int itemStringLength = fontMetrics.stringWidth(valueString);
            if (maxStringLength < itemStringLength)
            {
              maxStringLength = itemStringLength;
            }
          }
          if ((maxStringLength + VariableWidthComboBox.widthExtension) <= comboBox.getWidth())
          {
            comboBox.setPopupWidthToDefault();
          }
          else
          {
            comboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.
          }
          comboBox.addItem(StringLocalizer.keyToString("GUI_YES_BUTTON_KEY"));
          comboBox.addItem(StringLocalizer.keyToString("GUI_NO_BUTTON_KEY"));
          comboBox.getInputMap().put(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true), "enableComboBox");
//        comboBox.getActionMap().put("enableComboBox", new EnableComboBoxAction());
          rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
        }
        else if ((value instanceof String) || (value instanceof java.util.List))  // must be string value
        {
          // create combo box for this cell's editor
          java.util.List<String> availableValues = (java.util.List<String>) algorithmSetting.getValuesList(algorithm.getVersion());
          final VariableWidthComboBox comboBox = new VariableWidthComboBox(availableValues.toArray());
          comboBox.setPopupWidthToDefault();
          // set the popup width to the max of the strings only if that's longer than the box itself
          FontMetrics fontMetrics = comboBox.getFontMetrics(comboBox.getFont());
          int maxStringLength = 0;
          for (String valueString : availableValues)
          {
            int itemStringLength = fontMetrics.stringWidth(valueString);
            if (maxStringLength < itemStringLength)
            {
              maxStringLength = itemStringLength;
            }
          }
          if ((maxStringLength + VariableWidthComboBox.widthExtension) <= comboBox.getWidth())
          {
            comboBox.setPopupWidthToDefault();
          }
          else
          {
            comboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.
          }
          comboBox.getInputMap().put(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true), "enableComboBox");
//        comboBox.getActionMap().put("enableComboBox", new EnableComboBoxAction());
          rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
        }
        else
        {
          Assert.expect(false);
        }
      }
    }
    CustomThresholdRenderer renderer = new CustomThresholdRenderer(algorithm, null);
    renderer.setProjectAsUnitSource(false);
    tt.setDefaultRenderer(Object.class, renderer);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void newThresholdSetButton_actionPerformed()
  {
    if (saveThresholdSetIfNecessary() == false)
      return;

    String newThresholdSetName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_ENTER_NEW_SET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_NEW_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newThresholdSetName != null) // null means the dialog was canceled
    {
      newThresholdSetName = getLegalFileName(newThresholdSetName);
      if (newThresholdSetName == null)
        return;

      _initialThresholds.createNewSet(newThresholdSetName);
      // add new threshold set to the select combobox
      _thresholdSetSelectNameCombobox.addItem(newThresholdSetName);
      _thresholdSetSelectNameCombobox.removeActionListener(_thresholdSetSelectComboboxActionListener);
      _thresholdSetSelectNameCombobox.setSelectedItem(newThresholdSetName);
      _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);

      populateThresholdTables();
      checkProtectedSet();
      updateSummaryPanel();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean saveCurrentThresholdSet()
  {
    try
    {
      _initialThresholds.save();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      return false;
    }
    _isChangingDirectories = false;
    _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_THRESHOLDS_SAVED_SET_KEY",
                                                                             new Object[]{_initialThresholds.getThresholdSetName()})));
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean saveThresholdSetAs()
  {
    String newThresholdSetName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_ENTER_NEW_SET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_NEW_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newThresholdSetName != null) // null means the dialog was canceled
    {
      newThresholdSetName = getLegalFileName(newThresholdSetName);
      if (newThresholdSetName == null)
        return false;

      try
      {
        _initialThresholds.changeThresholdSetName(newThresholdSetName);

        _initialThresholds.save();
        // clear the undo stack when a new set is loaded in the GUI (save as loads a new set)
        clearUndoStack();

        if (thresholdSetNameComboBoxContainsName(newThresholdSetName) == false)
          _thresholdSetSelectNameCombobox.addItem(newThresholdSetName);
        _thresholdSetSelectNameCombobox.removeActionListener(_thresholdSetSelectComboboxActionListener);
        _thresholdSetSelectNameCombobox.setSelectedItem(newThresholdSetName);
        _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);
        populateThresholdTables();
        checkProtectedSet();
        updateSummaryPanel();
        _isChangingDirectories = false;
        return true;
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void showDatastoreError(DatastoreException ex)
  {
    Assert.expect(ex != null);

    JOptionPane.showMessageDialog(_mainUI,
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getLegalFileName(String newThresholdSetName)
  {
    Assert.expect(newThresholdSetName != null);

    String whiteSpacePattern = "^\\s*$";
    java.util.List<String> tempConfigNamesList = new ArrayList<String>();
    for (String name : FileName.getAllInitialThresholdSetNames())
    {
      tempConfigNamesList.add(name.toUpperCase());
    }
    String tempThresholdSetName = newThresholdSetName.toUpperCase();
    while (newThresholdSetName != null &&
            (tempConfigNamesList.contains(tempThresholdSetName) ||
            FileName.hasIllegalChars(newThresholdSetName) ||
            Pattern.matches(whiteSpacePattern, newThresholdSetName)))
    {
      if (tempConfigNamesList.contains(tempThresholdSetName))
      {
        newThresholdSetName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_THRESHOLDS_SET_EXISTS_MESSAGE_KEY",
                                                                new Object[]{newThresholdSetName})),
                StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newThresholdSetName != null)
        {
          tempThresholdSetName = newThresholdSetName.toUpperCase();
        }
        else
        {
          return newThresholdSetName;
        }
      }
      else if (FileName.hasIllegalChars(newThresholdSetName))
      {
        newThresholdSetName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString(
                "CFGUI_INITIAL_THRESHOLDS_SET_ILLEGAL_NAME_KEY",
                new Object[]
                {
                  newThresholdSetName, FileName.getIllegalChars()
                })),
                StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newThresholdSetName != null)
        {
          tempThresholdSetName = newThresholdSetName.toUpperCase();
        }
        else
        {
          return newThresholdSetName;
        }
      }
      else if (Pattern.matches(whiteSpacePattern, newThresholdSetName))
      {
        newThresholdSetName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString(
                "CFGUI_INITIAL_THRESHOLDS_SET_ILLEGAL_EMPTY_NAME_KEY",
                new Object[]
                {
                  newThresholdSetName
                })),
                StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newThresholdSetName != null)
        {
          tempThresholdSetName = newThresholdSetName.toUpperCase();
        }
        else
        {
          return newThresholdSetName;
        }
      }
    }
    return newThresholdSetName;
  }


  /**
   * @author Andy Mechtenberg
   */
  private void deleteThresholdSetButton_actionPerformed()
  {
    Object value = _thresholdSetSelectNameCombobox.getSelectedItem();
    Assert.expect(value instanceof String);
    String selectedThresholdSetName = (String)value;

    int answer = JOptionPane.showConfirmDialog(_mainUI,
                                               StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_THRESHOLDS_DELETE_SET_KEY",
                                                                                               new Object[]{selectedThresholdSetName})),
                                               StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_CONFIRM_DELETE_TITLE_KEY"),
                                               JOptionPane.YES_NO_OPTION);
    if (answer != JOptionPane.YES_OPTION)
      return;

    try
    {
      _initialThresholds.delete();

      _thresholdSetSelectNameCombobox.removeActionListener(_thresholdSetSelectComboboxActionListener);
      _thresholdSetSelectNameCombobox.removeItem(selectedThresholdSetName);
      _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);

      clearUndoStack();
      // set selection to another item in the list for the 'select name' combobox
      int index = _thresholdSetSelectNameCombobox.getSelectedIndex();
      int lastIndex = _thresholdSetSelectNameCombobox.getItemCount() - 1 ;
      if (index > lastIndex ||
          (index == -1 && lastIndex != -1))
        _thresholdSetSelectNameCombobox.setSelectedIndex(lastIndex);
      else if (index != -1)
        _thresholdSetSelectNameCombobox.setSelectedIndex(index);
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void start()
  {
//    System.out.println("SWOptionsJointTypeTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    // don't want to be able to import a project while changing a threshold set
    _mainUI.getMainMenuBar().disableImportMenuItem();

    _mainUI.setStatusBarText("");

    populateWithConfigData();

    int dividerLocation = _persistSettings.getInitialThresholdsTableSummaryDivider();
    if (dividerLocation > 0)
      _thresholdSummarySplitPane.setDividerLocation(dividerLocation);
    else
      _thresholdSummarySplitPane.setDividerLocation(0.70);
    // add custom menus
    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author Andy Mechtenberg
   */
  public boolean isReadyToFinish()
  {
    return saveThresholdSetIfNecessary();
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean saveThresholdSetIfNecessary()
  {
    if (_initialThresholds.isDirty())
    {
      String currentSetName = _initialThresholds.getThresholdSetName();
      String saveChangesQuestion;
      if (_initialThresholds.isDefaultSet(currentSetName))
        saveChangesQuestion = StringLocalizer.keyToString("CFGUI_INITIAL_THRESHOLDS_SAVE_CHANGES_PROTECTED_SET_KEY");
      else
        saveChangesQuestion = StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_THRESHOLDS_SAVE_CHANGES_KEY",
                                                                              new Object[]{currentSetName}));
      int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 saveChangesQuestion,
                                                 StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
                                                 JOptionPane.YES_NO_CANCEL_OPTION);
      if (answer == JOptionPane.CANCEL_OPTION)
      {
        return false;
      }
      else if (answer == JOptionPane.YES_OPTION)
      {
        if (_initialThresholds.isDefaultSet(currentSetName))
          return saveThresholdSetAs();
        else
          return saveCurrentThresholdSet();
      }
      else
      {
        // if the user wants to discard the changes to a new threshold set, remove it from the select combobox because
        // it has never been saved to disk. A threshold set is new if it doesn't exist on disk
        if (_initialThresholds.getAllSetNames().contains(_initialThresholds.getThresholdSetName()) == false)
        {
          _thresholdSetSelectNameCombobox.removeActionListener(_thresholdSetSelectComboboxActionListener);
          _thresholdSetSelectNameCombobox.removeItem(_initialThresholds.getThresholdSetName());
          _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);
          if (_thresholdSetSelectNameCombobox.getSelectedIndex() >= 0)
            thresholdSetChanged(true);
//            _initialThresholds.loadNewThresholdSet((String)_thresholdSetSelectNameCombobox.getSelectedItem());
        }
        // clear undo stack because what ever changes the user made, they chose to discard them
        clearUndoStack();
        _initialThresholds.loadDefaultSet();
      }
    }
    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author Andy Mechtenberg
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    int dividerLocation = _thresholdSummarySplitPane.getDividerLocation();
    _persistSettings.setInitialThresholdsTableSummaryDivider(dividerLocation);

    // restore ability to import a project
    _mainUI.getMainMenuBar().enableImportMenuItem();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("SWOptionsJointTypeTabPanel().finish()");
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          handleUndoState(object);
        }
        else if (observable instanceof ConfigObservable)
        {
          handleConfigDataUpdate(object);
        }
        else
        {
          Assert.expect(false);
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  UndoState getCurrentUndoState(int thresholdRow, Algorithm algorithm, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);
    UndoState undoState = new UndoState();
    undoState.setInitialThresholdsSetComboboxSelectedIndex(_thresholdSetSelectNameCombobox.getSelectedIndex());
    int undoTabIndex = -1;
    String algorithmName = "";
    // find the index for the passed in algorithm
    // if the algorithmSetting is a slice setup, then look for that index
    if (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT) ||
       (algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)))
      algorithmName = StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY");
    else
      algorithmName = algorithm.getName();

    for (int i = 0; i < _thresholdsTabbedPane.getTabCount(); i++)
    {
      if (_thresholdsTabbedPane.getTitleAt(i).equals(algorithmName))
        undoTabIndex = i;
    }

    undoState.setIntialThresholdsTabIndex(undoTabIndex);
    undoState.setInitialThresholdsThresholdRow(thresholdRow);

    return undoState;
  }

  /**
   * @author Andy Mechtenberg
   */
  UndoState getCurrentUndoState(int thresholdRow)
  {
    UndoState undoState = new UndoState();
    undoState.setInitialThresholdsSetComboboxSelectedIndex(_thresholdSetSelectNameCombobox.getSelectedIndex());
    undoState.setIntialThresholdsTabIndex(_thresholdsTabbedPane.getSelectedIndex());
    undoState.setInitialThresholdsThresholdRow(thresholdRow);
    return undoState;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleConfigDataUpdate(final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(object != null);
        if (object instanceof InitialThresholds)
        {
          MathUtilEnum newUnits = _initialThresholds.getCurrentUnits();
//          System.out.println("Must be UNITS change: " + newUnits);
          int currentAlgorithmTab =_thresholdsTabbedPane.getSelectedIndex();
          String newUnitsString = MeasurementUnits.getMeasurementUnitString(newUnits);
          _unitsComboBox.setSelectedItem(newUnitsString);
          populateThresholdTables();
          _thresholdsTabbedPane.setSelectedIndex(currentAlgorithmTab);
        }
        else if (object instanceof SingleInitialThreshold)
        {
//          SingleInitialThreshold sit = (SingleInitialThreshold)object;
          // we don't need to do anything here because:
          // 1.  When the user changes the data, we get this event but in the tableModel.setValueAt, we also update the cell right then
          // 2.  During an UNDO operation, getValueAt is called because of the UndoState stuff, and in there we update to current initialThresholds data
//          System.out.println("Must be Threshold value change change for: " + sit.getJointTypeEnum() + " " + sit.getAlgorithm() + " " + sit.getAlgorithmSetting() + " " + sit.getValue());
        }
        else  // this should happen only if the directory changed, which is a Config event, not an InitialThresholds event
        {
          populateWithConfigData();
        }
        updateSummaryPanel();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void handleUndoState(final Object stateObject)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        UndoState undoState = (UndoState)stateObject;

        // revert to correct path
        if (_thresholdsDirectoryTextField.getText().equalsIgnoreCase(Directory.getInitialThresholdsDir()) == false)
        {
          updateThresholdsDirectoryTextField();
          populateThresholdSetSelectCombobox();
        }
        else
        {
          int selectedIndex = undoState.getInitialThresholdsSetComboboxSelectedIndex();
          if (_thresholdSetSelectNameCombobox.getSelectedIndex() != selectedIndex)
            _thresholdSetSelectNameCombobox.setSelectedIndex(selectedIndex);

          // revert to correct tab
          int newTabIndex = undoState.getIntialThresholdsTabIndex();
          _thresholdsTabbedPane.setSelectedIndex(newTabIndex);

          checkProtectedSet();

          // select the threshold row
          java.awt.Component comp = _thresholdsTabbedPane.getSelectedComponent();
          Assert.expect(comp instanceof JPanel);
          JPanel compPanel = (JPanel)comp;
          java.awt.Component[] componentArray = compPanel.getComponents();
          for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
          {
            java.awt.Component potentialComp = componentArray[compIndex];
            if (potentialComp instanceof JScrollPane)
            {
              JScrollPane jsp = (JScrollPane)potentialComp;
              java.awt.Component comp1 = (JTable)jsp.getViewport().getView();
              if (comp1 == null)
                return;
              else
              {
                JTable tt = (JTable)comp1;
                int thresholdRow = undoState.getInitialThresholdsThresholdRow();
                if (thresholdRow < 0)
                  return;
                tt.setRowSelectionInterval(thresholdRow, thresholdRow);
                TableCellEditor tce = tt.getCellEditor();
                if (tce != null)
                  tce.cancelCellEditing();
                tt.requestFocus();
              }
            }
          }
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateWithConfigData()
  {
    populateThresholdSetSelectCombobox();
    String currentSetName = _initialThresholds.getThresholdSetName();

    // if set name no longer exists, we'll rever to SystemDefaults at index 0
    if (thresholdSetNameComboBoxContainsName(currentSetName))
      _thresholdSetSelectNameCombobox.setSelectedItem(currentSetName);
    else
      _thresholdSetSelectNameCombobox.setSelectedIndex(0);
    checkProtectedSet();
  }

  /**
   * Will populate the summary panel with formating to highlight joint type and algorithm
   * Special case for slice setup thresholds - instead of measurement algorithm, use the Slice Setup name instead,
   * which is localized as StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY")
   * @author Andy Mechtenberg
   */
  private void updateSummaryPanel()
  {
    String algorithmTab = "&nbsp;&nbsp;&nbsp;";
    String thresholdTab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    JointTypeEnum currentJointTypeEnum = null;
    String currentAlgorithmName = null;

    MathUtilEnum displayUnits = _initialThresholds.getCurrentUnits();
    String totalText = "";
    _summaryEditorPane.setText(null);
    _summaryEditorPane.setContentType("text/html");

    for(SingleInitialThreshold sit : _initialThresholds.getAllSettingsInSortedOrder())
    {
      String text = "";

      JointTypeEnum jointTypeEnum = sit.getJointTypeEnum();
      Algorithm algorithm = sit.getAlgorithm();
      String algorithmName = algorithm.getName();
      AlgorithmSetting setting = sit.getAlgorithmSetting();

      // change the algorithm name if the threshold belongs under Slice Setup
      if ((setting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT)) ||
            (setting.getAlgorithmSettingTypeEnum(algorithm.getVersion()).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)))
        algorithmName = StringLocalizer.keyToString("ATGUI_SLICE_HEIGHT_TAB_TITLE_KEY");

      // we need to convert the threshold setting from it's native units to a pleasing to read text
      // this code ripped from CustomThresholdRenderer and the getValueAt of InitialThresholdsTableModel
      Object settingValue = sit.getValue();
      MeasurementUnitsEnum settingUnits = setting.getUnits(algorithm.getVersion());
      String valueWithUnitsLabelText = AlgorithmSettingUtil.getAlgorithmSettingValueDisplayText(settingValue, settingUnits, displayUnits);

      if ((currentJointTypeEnum == null) || (currentJointTypeEnum.equals(jointTypeEnum) == false))
      {
        currentJointTypeEnum = jointTypeEnum;
        text = "<b>" + jointTypeEnum.getName() + "</b><br>\n";
        // we're on to a new joint type, so reset the currentAlgorithm
        currentAlgorithmName = null;
      }
      if ((currentAlgorithmName == null) || (currentAlgorithmName.equalsIgnoreCase(algorithmName) == false))
      {
        currentAlgorithmName = algorithmName;
        text += algorithmTab + "<u>" + currentAlgorithmName + "</u><br>";
      }
      text += thresholdTab + setting + ": " + valueWithUnitsLabelText + "<br>";
      totalText += text;
    }
    javax.swing.text.Document document = _summaryEditorPane.getEditorKit().createDefaultDocument();
    _summaryEditorPane.setDocument(document);
    _summaryEditorPane.setText(totalText);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void checkProtectedSet()
  {
    Object object = _thresholdSetSelectNameCombobox.getSelectedItem();
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String) object;
      Boolean enabled = true;
      // if the file is the default file, disable the save and delete buttons, otherwise, enable them
      if (_initialThresholds.isDefaultSet(selectedName))
      {
        enabled = false;
      }
      _saveThresholdSetButton.setEnabled(enabled);
      _deleteThresholdSetButton.setEnabled(enabled);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void populateThresholdSetSelectCombobox()
  {
    _thresholdSetSelectNameCombobox.removeActionListener(_thresholdSetSelectComboboxActionListener);
    _thresholdSetSelectNameCombobox.removeAllItems();
    java.util.List<String> allSetNames = _initialThresholds.getAllSetNames();
    for(String setName : allSetNames)
      _thresholdSetSelectNameCombobox.addItem(setName);
    _thresholdSetSelectNameCombobox.addActionListener(_thresholdSetSelectComboboxActionListener);
  }

  /**
   * @author Andy Mechtenberg
   */
   private void jointTypeChanged()
   {
     populateThresholdTables();
   }

  /**
   * @author Andy Mechtenberg
   */
   private void unitsChanged()
   {
     String units = _unitsComboBox.getSelectedItem().toString();
     MathUtilEnum newUnits = MeasurementUnits.getMeasurementUnitEnum(units);
     ConfigSetInitialThresholdsUnitsCommand unitsCommand = new ConfigSetInitialThresholdsUnitsCommand(newUnits);
     try
     {
       _commandManager.trackState(getCurrentUndoState(-1));
       _commandManager.execute(unitsCommand);
     }
     catch (XrayTesterException ex)
     {
       Assert.expect(false);
     }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void thresholdSetChanged(boolean skipSave)
  {
    if (skipSave == false)
    {
      if (saveThresholdSetIfNecessary() == false)
        return;
    }

    try
    {
      String newSetName = (String) _thresholdSetSelectNameCombobox.getSelectedItem();
      if(_initialThresholds.isThresholdSetNameExist(newSetName))
      {
        _initialThresholds.loadNewThresholdSet(newSetName);
      }
      else
      {
//        _thresholdSetSelectNameCombobox.setSelectedItem(_initialThresholds.getThresholdSetName());
        _initialThresholds.loadDefaultSet();
      }
      MathUtilEnum units = _initialThresholds.getCurrentUnits();
      String measUnits = MeasurementUnits.getMeasurementUnitString(units);
      _unitsComboBox.setSelectedItem(measUnits);
      populateThresholdTables();
      checkProtectedSet();
      updateSummaryPanel();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      // revert back to SystemDefault setting
      _thresholdSetSelectNameCombobox.setSelectedIndex(0);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private boolean thresholdSetNameComboBoxContainsName(String thresholdSetName)
  {
    Assert.expect(thresholdSetName != null);

    for (int i = 0; i < _thresholdSetSelectNameCombobox.getItemCount(); i++)
    {
      Object item = _thresholdSetSelectNameCombobox.getItemAt(i);
      Assert.expect(item instanceof String);
      String name = (String)item;
      if (name.equalsIgnoreCase(thresholdSetName))
        return true;
    }
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void clearUndoStack()
  {
    _commandManager.clear();
    _menuBar.updateUndoRedoMenuItems();
  }

  /**
   * @author Andy Mechtenberg
   */
  private TableColumnModelListener getThresholdTableColumnModelListener(final JTable table)
  {
    return new TableColumnModelListener()
    {
      // It is necessary to keep the table since it is not possible
      // to determine the table from the event's source
      public void columnAdded(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnRemoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMarginChanged(ChangeEvent e)
      {
        if (_settingWidthsProgrammatically)
          return;
        if (table.isValid() == false)
          return;
        for (int i = 0; i < table.getColumnCount(); i++)
        {
//           System.out.println("Setting persist file data for column " + i + " to width " + table.getColumnModel().getColumn(i).getWidth());
          _persistSettings.setInitialThresholdsTableColumnWidth(i, table.getColumnModel().getColumn(i).getWidth());
        }
      }

      public void columnSelectionChanged(ListSelectionEvent e)
      {
        // do nothing
      }
    };
  }

  /**
   * @author Wei Chin, Chong
   */
  private void thresholdsDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_thresholdsDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(),
                               StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _thresholdsDirectoryTextField.setText(fileName);
      _thresholdsDirectoryTextField.requestFocus();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void thresholdsDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;

    // if the value has not changed, then do nothing
    if (_thresholdsDirectoryTextField.getText().equalsIgnoreCase(Directory.getInitialThresholdsDir()))
    {
      _inUse = false;
      return;
    }

    // see if the current set is dirty, and don't change the text field if they choose to cancel
    if (saveThresholdSetIfNecessary() == false)
    {
      // user cancelled, so let's not accept the new directory text field entry
      updateThresholdsDirectoryTextField();
      _inUse = false;
      return;
    }
    if(_isChangingDirectories == false)
    {
      clearUndoStack();
      _isChangingDirectories = true;
    }

    String fullPath = _thresholdsDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if ( _envPanel.isPathValid(fullPath) )
    {
      ConfigSetInitialThresholdDirectoryCommand setInitialThresholdDirectoryCommand =
            new ConfigSetInitialThresholdDirectoryCommand(fullPath);
      try
      {
        _commandManager.execute(setInitialThresholdDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
    {
      // this is if isPathValid returns false -- most likely because the user chose NOT to create the new directory
      // we should revert back to current directory
      updateThresholdsDirectoryTextField();
    }
    _inUse = false;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void updateThresholdsDirectoryTextField()
  {
    _thresholdsDirectoryTextField.removeActionListener(_thresholdDirectoryTextFieldActionListener);
    _thresholdsDirectoryTextField.removeFocusListener(_thresholdDirectoryFocusAdapter);
    _thresholdsDirectoryTextField.setText(Directory.getInitialThresholdsDir());
    _thresholdsDirectoryTextField.addActionListener(_thresholdDirectoryTextFieldActionListener);
    _thresholdsDirectoryTextField.addFocusListener(_thresholdDirectoryFocusAdapter);
  }
}
