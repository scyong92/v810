package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;
import java.util.regex.Pattern;

/**
 * @author Andy Mechtenberg
 */
public class CustomerOptionsInfoTabPanel extends AbstractTaskPanel implements Observer
{
  private ConfigEnvironment _envPanel = null;

  private CustomerInfo _customerInfo = CustomerInfo.getInstance();
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private JTextField _companyNameTextField;
  private JTextField _customerNameTextField;
  private JTextField _emailTextField;
  private JTextField _phoneNumberTextField;
  private JTextField _mailServerTextField;
  private Border _emptyBorder = BorderFactory.createEmptyBorder(10, 5, 5, 5);

  // Phone number patterns (try to support some reasonable formats but don't obsess over it)

  // US phone numbers - need 10 digits plus optional prefix and extension
  // (800)555-1212
  // 1(800) 555-1212
  // 1-800-555-1212
  // 1.800.555 1212 ext 122
  // Etc.
  private static final String usPrefix = "[01]?";
  private static final String usAreaCode = "\\s*[\\(\\.-]?(\\d{3})[\\)\\.-]?";
  private static final String usTrunk = "\\s*(\\d{3})";
  private static final String usNumber = "[\\s\\.-](\\d{4})";
  private static final String extension = "(\\s*ext\\s(\\d+))?";
  private static final String usPhoneNumber = "^" + usPrefix + usAreaCode + usTrunk + usNumber + extension + "$";
  private static final Pattern US_PHONE_NUMBER_VALID_PATTERN = Pattern.compile(usPhoneNumber);

  // International phone numbers - need "+<some digits> " + more digits w/spaces plus optional extension
  // +212 455 551212
  // +11 55444 4435 ext 99
  // Etc.
  private static final String intlCountryCode = "\\+\\d+\\s+";
  private static final String intlNumber = "[\\d\\s]+";
  private static final String intlPhoneNumber = "^" + intlCountryCode + intlNumber + extension + "$";
  public static final Pattern INTL_PHONE_NUMBER_VALID_PATTERN = Pattern.compile(intlPhoneNumber);

  // create focusAdapters so they can be added and removed as needed
  // author George Booth
  private FocusAdapter emailTextFieldFocusListener = new FocusAdapter()
  {
    public void focusLost(FocusEvent e)
    {
      emailTextField_setValue();
    }
  };

  private FocusAdapter phoneNumberTextFieldFocusListener = new FocusAdapter()
  {
    public void focusLost(FocusEvent e)
    {
      phoneNumberTextField_setValue();
    }
  };

  private FocusAdapter mailServerTextFieldFocusListener = new FocusAdapter()
  {
    public void focusLost(FocusEvent e)
    {
      mailServerTextField_setValue();
    }
  };

  /**
   * @author Andy Mechtenberg
   */
  private CustomerOptionsInfoTabPanel()
  {
    jbInit();
  }
  /**
   * @author Laura Cormos
   */
  public CustomerOptionsInfoTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    jbInit();
  }

  /**
   * This class is an observer of config. We will determine what
   * kind of changes were made and decide if we need to update the gui.
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)argument;
            if (configEnum instanceof SoftwareConfigEnum)
            {
              SoftwareConfigEnum softwareConfigEnum = (SoftwareConfigEnum)configEnum;
              if (softwareConfigEnum.equals(SoftwareConfigEnum.CUSTOMER_COMPANY_NAME) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.CUSTOMER_NAME) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.CUSTOMER_EMAIL_ADDRESS) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.CUSTOMER_PHONE_NUMBER) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.SMTP_SERVER))
              {
                updateDataOnScreen();
              }
            }
          }
        }

      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    Border titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                            StringLocalizer.keyToString("CFGUI_COMPANY_CUSTOMER_INFO_KEY"));
    Border titledBorder = BorderFactory.createCompoundBorder(titledBorder1, BorderFactory.createEmptyBorder(10, 10, 0, 0));
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(_emptyBorder);
    int textFieldColumns = 30;
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);
    PairLayout pairLayout = new PairLayout(10, 20, false);
    JPanel dataPanel = new JPanel(pairLayout);
    dataPanel.setBorder(titledBorder);
    centerPanel.add(dataPanel, BorderLayout.CENTER);
    add(centerPanel, BorderLayout.CENTER);
    JLabel companyNameLabel = new JLabel(StringLocalizer.keyToString("CFGUI_COMPANY_NAME_KEY"));
    JLabel customerNameLabel = new JLabel(StringLocalizer.keyToString("CFGUI_CUSTOMER_NAME_KEY"));
    JLabel emailLabel = new JLabel(StringLocalizer.keyToString("CFGUI_EMAIL_ADDRESS_KEY"));
    JLabel phoneNumberLabel = new JLabel(StringLocalizer.keyToString("CFGUI_PHONE_NUMBER_KEY"));
    JLabel mailServerLabel = new JLabel(StringLocalizer.keyToString("CFGUI_MAIL_SERVER_KEY"));

    _companyNameTextField = new JTextField(textFieldColumns);
    _customerNameTextField = new JTextField(textFieldColumns);
    _emailTextField = new JTextField(textFieldColumns);
    _phoneNumberTextField = new JTextField(textFieldColumns);
    _mailServerTextField = new JTextField(textFieldColumns);

    dataPanel.add(companyNameLabel);
    dataPanel.add(_companyNameTextField);

    dataPanel.add(customerNameLabel);
    dataPanel.add(_customerNameTextField);

    dataPanel.add(emailLabel);
    dataPanel.add(_emailTextField);

    dataPanel.add(phoneNumberLabel);
    dataPanel.add(_phoneNumberTextField);

    dataPanel.add(mailServerLabel);
    dataPanel.add(_mailServerTextField);


    _companyNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        companyNameTextField_setValue();
      }
    });

    _companyNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        companyNameTextField_setValue();
      }
    });

    _customerNameTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        customerNameTextField_setValue();
      }
    });

    _customerNameTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customerNameTextField_setValue();
      }
    });

    _emailTextField.addFocusListener(emailTextFieldFocusListener);
    _emailTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        emailTextField_setValue();
      }
    });

    _phoneNumberTextField.addFocusListener(phoneNumberTextFieldFocusListener);
    _phoneNumberTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        phoneNumberTextField_setValue();
      }
    });

    _mailServerTextField.addFocusListener(mailServerTextFieldFocusListener);
    _mailServerTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        mailServerTextField_setValue();
      }
    });

    // define menus and toolBar items to be added later

//    _setDefaultMenuItem.setText("Set &Default User Config");
//    _setDefaultMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        setDefault(e);
//      }
//    });
//
//    _configHelpMenuItem.setText("&Config User Help");
//    _configHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        configHelp(e);
//      }
//    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void companyNameTextField_setValue()
  {
    try
    {
      ConfigSetCompanyNameCommand setCompanyNameCommand = new ConfigSetCompanyNameCommand(_companyNameTextField.getText());
      _commandManager.execute(setCompanyNameCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void customerNameTextField_setValue()
  {
    try
    {
      ConfigSetCustomerNameCommand setCustomerNameCommand = new ConfigSetCustomerNameCommand(_customerNameTextField.getText());
      _commandManager.execute(setCustomerNameCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void emailTextField_setValue()
  {
    // remove the focus listener since the error will remove focus and may cause a second check
    _emailTextField.removeFocusListener(emailTextFieldFocusListener);
    if (_envPanel.isEmailAddressValid(_emailTextField.getText()))
    {
      try
      {
        ConfigSetEmailAddressCommand setEmailAddressCommand = new ConfigSetEmailAddressCommand(_emailTextField.getText());
        _commandManager.execute(setEmailAddressCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    _emailTextField.addFocusListener(emailTextFieldFocusListener);
  }

  /**
   * @author George Booth
   */
  private boolean phoneNumberValid()
  {
    String phoneNumberString = _phoneNumberTextField.getText().trim();
    if (phoneNumberString.length() > 0 && US_PHONE_NUMBER_VALID_PATTERN.matcher(phoneNumberString).matches() == false &&
        INTL_PHONE_NUMBER_VALID_PATTERN.matcher(phoneNumberString).matches() == false)
    {
      LocalizedString message = new LocalizedString("CFGUI_PHONE_NUMBER_INVALID_MESSAGE_KEY",
                                                    new Object[]
                                                    {phoneNumberString});

      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString(message),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      return false;
    }
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void phoneNumberTextField_setValue()
  {
    // remove the focus listener since the error will remove focus and may cause a second check
    _phoneNumberTextField.removeFocusListener(phoneNumberTextFieldFocusListener);
    if (phoneNumberValid())
    {
      try
      {
        ConfigSetPhoneNumberCommand sePhoneNumberCommand = new ConfigSetPhoneNumberCommand(_phoneNumberTextField.getText());
        _commandManager.execute(sePhoneNumberCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    _phoneNumberTextField.addFocusListener(phoneNumberTextFieldFocusListener);
  }



  /**
   * @author Andy Mechtenberg
   */
  private void mailServerTextField_setValue()
  {
    // remove the focus listener since the error will remove focus and may cause a second check
    _mailServerTextField.removeFocusListener(mailServerTextFieldFocusListener);
    if (_envPanel.isMailServerValid(_mailServerTextField.getText()))
    {
      try
      {
        ConfigSetMailServerCommand setMailServerCommand = new ConfigSetMailServerCommand(_mailServerTextField.getText());
        _commandManager.execute(setMailServerCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    _mailServerTextField.addFocusListener(mailServerTextFieldFocusListener);
  }

  /**
   * both email and smpt server are required if either is specified
   * @author George Booth
   */
  boolean fieldsAreConsistent()
  {
    String emailString = _emailTextField.getText().trim();
    String mailServerString = _mailServerTextField.getText().trim();
    if ((emailString.length() > 0 && mailServerString.length() == 0) ||
        (emailString.length() == 0 && mailServerString.length() > 0))
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    StringLocalizer.keyToString("CFGUI_EMAIL_AND_SMTP_ADDRESS_BOTH_REQUIRED_MESSAGE_KEY"),
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
      return false;
    }
    return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    _companyNameTextField.setText(_customerInfo.getCompanyName());
    _customerNameTextField.setText(_customerInfo.getCustomerName());
    _emailTextField.setText(_customerInfo.getEmailAddress());
    _phoneNumberTextField.setText(_customerInfo.getPhoneNumber());
    _mailServerTextField.setText(_customerInfo.getMailServer());
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("CustomerOptionsInfoTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");

    ConfigObservable.getInstance().addObserver(this);
    updateDataOnScreen();


    // add custom menus
//    JMenu configMenu = _envPanel.getConfigMenu();
//    _menuBar.addMenuItem(_menuRequesterID, configMenu, _setDefaultMenuItem);
//
//    JMenu helpMenu = _menuBar.getHelpMenu();
//    _menuBar.addMenuItem(_menuRequesterID, helpMenu, 0, _configHelpMenuItem);
//
//    _menuBar.addMenuSeparator(_menuRequesterID, helpMenu, 1);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return _envPanel.isEmailAddressValid(_emailTextField.getText()) && phoneNumberValid() && _envPanel.isMailServerValid(_mailServerTextField.getText()) && fieldsAreConsistent();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // set up the AssertUtil information now that the config file data may have changed
    AssertUtil.updateConfigData();

    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("CustomerOptionsInfoTabPanel().finish()");
  }


}
