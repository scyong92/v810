package com.axi.v810.gui.config;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.datastore.config.*;

/**
 * @author wei-chin.chong
 */
public class ProductionTuneSettingTable extends JTable
{
  public static final int _TYPE_INDEX = 0;
  public static final int _NAME_INDEX = 1;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _TYPE_COLUMN_WIDTH_PERCENTAGE = .30;
  private static final double _NAME_COLUMN_WIDTH_PERCENTAGE = .70;
  /**
   * Default constructor.
   * @author Wei Chin
   */
  public ProductionTuneSettingTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Wei Chin
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int typeColumnWidth = (int)(totalColumnWidth * _TYPE_COLUMN_WIDTH_PERCENTAGE);
    int nameColumnWidth = (int)(totalColumnWidth * _NAME_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_TYPE_INDEX).setPreferredWidth(typeColumnWidth);
    columnModel.getColumn(_NAME_INDEX).setPreferredWidth(nameColumnWidth);
  }

  /**
   * @author Wei Chin
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    
    JComboBox typeComboBox = new JComboBox();
    for (String type : ProductionTuneSettings.getListOfTypes())
    {
      typeComboBox.addItem(type);
    }
    
    TableCellEditor comboEditor = new DefaultCellEditor(typeComboBox)
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };
    
    columnModel.getColumn(_TYPE_INDEX).setCellEditor(comboEditor);
    columnModel.getColumn(_NAME_INDEX).setCellEditor(stringEditor);
  }
  
  /**
   * @author sheng chuan
   */
  void stopCellEditingForColumn()
  {
    TableCellEditor cellTypeEditor = getColumnModel().getColumn(_TYPE_INDEX).getCellEditor();
    TableCellEditor cellNameEditor = getColumnModel().getColumn(_NAME_INDEX).getCellEditor();
    
    if (cellTypeEditor != null)
      cellTypeEditor.stopCellEditing();
    
    if(cellNameEditor != null)
      cellNameEditor.stopCellEditing();
  }
}
