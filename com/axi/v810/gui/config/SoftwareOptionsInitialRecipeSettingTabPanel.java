package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.algoTuner.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.Map.Entry;

/**
 * Create a task panel for apply initial recipe setting.
 */
public class SoftwareOptionsInitialRecipeSettingTabPanel extends AbstractTaskPanel implements Observer
{
  private ConfigGuiPersistence _persistSettings;
  private com.axi.v810.gui.undo.CommandManager _commandManager =
          com.axi.v810.gui.undo.CommandManager.getConfigInstance();  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();
  private InitialRecipeSetting _initialRecipeSetting = InitialRecipeSetting.getInstance();
  private boolean _settingWidthsProgrammatically = false;  // used to turn off width persistance unless user is changing widths

  // GUI controls
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JPanel _recipeSettingPanel = new JPanel();
  private BorderLayout _recipeSettingPanelBorderLayout = new BorderLayout();
  private JPanel _recipeSettingSetNamePanel = new JPanel();
  private BorderLayout _recipeSettingSetNamePanelBorderLayout = new BorderLayout();
  private JPanel _recipeSettingSetSelectPanel = new JPanel();
  private FlowLayout _recipeSettingSetSelectPanelFlowLayout = new FlowLayout();
  private JLabel _recipeSettingSetSelectNameLabel = new JLabel();
  private JComboBox _recipeSettingSetSelectNameCombobox = new JComboBox();
  private ActionListener _recipeSettingSetSelectComboboxActionListener;
  private JPanel _recipeSettingSetSelectActionsWrapperPanel = new JPanel();
  private FlowLayout _recipeSettingSetSelectActionsWrapperPanelFlowLayout = new FlowLayout();
  private JPanel _recipeSettingSetSelectActionsPanel = new JPanel();
  private GridLayout _recipeSettingSetSelectActionsPanelGridLayout = new GridLayout();
  private JButton _newRecipeSettingSetButton = new JButton();
  private JButton _saveRecipeSettingSetButton = new JButton();
  private JButton _saveAsRecipeSettingSetButton = new JButton();
  private JButton _deleteRecipeSettignSetButton = new JButton();
  private JPanel _recipeSettingSetDefinitionPanel = new JPanel();
  private BorderLayout _recipeSettingDefinitionPanelBorderLayout = new BorderLayout();
  private JPanel _recipeSettingTablePanel = new JPanel();
  private BorderLayout _recipeSettingTablePanelPanelBorderLayout = new BorderLayout();
  private JTabbedPane _recipeSettingTabbedPane = new JTabbedPane();
  private JPanel _setControlPanel = new JPanel();
  private JEditorPane _summaryEditorPane = new JEditorPane();
  private JPanel _summaryInformationPane = new JPanel();
  private BorderLayout _summaryInformationBorderLayout = new BorderLayout();
  private JLabel _summaryLabel = new JLabel();
  private JScrollPane _summaryScrollPane;
  private JSplitPane _recipeSettingSummarySplitPane = new JSplitPane();

  //@author Wei Chin, Chong
  private JLabel _textExplainationLabel = new JLabel();
  private Color _labelColor = new Color(0,70,213); //trying to match the blue of the panel titles
  private JPanel _recipeSettingSetLocationPanel = new JPanel();
  private JLabel _recipeSettingLocationLabel = new JLabel();
  private JTextField _recipeSettingDirectoryTextField;
  private ConfigEnvironment _envPanel = null;
  private boolean _inUse = false;
  private boolean _isChangingDirectories = false;
  private ActionListener _recipeSettingDirectoryTextFieldActionListener;
  private FocusAdapter _recipeSettingDirectoryFocusAdapter;
  
  private static final int _defaultMinimumSelectionForStageSpeed = 0;
  private static final int _defaultMaximumSelectionForStageSpeed = 1;
  private static final int _defaultMaximumSelectionForCameraAngle = 14;
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public SoftwareOptionsInitialRecipeSettingTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    _persistSettings = envPanel.getPersistance();
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void jbInit() throws Exception
  {
    // set up panel GUI components
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _centerPanel.add(_recipeSettingPanel, BorderLayout.CENTER);

    _recipeSettingPanel.setLayout(_recipeSettingPanelBorderLayout);
    _recipeSettingPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
            StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_SET_KEY")),
            BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _recipeSettingPanel.add(_recipeSettingSetNamePanel, BorderLayout.NORTH);
    _recipeSettingPanel.add(_recipeSettingSetDefinitionPanel, BorderLayout.CENTER);

    _recipeSettingSetNamePanel.setLayout(_recipeSettingSetNamePanelBorderLayout);
    _recipeSettingSetNamePanel.add(_recipeSettingSetSelectPanel, BorderLayout.WEST);
    _recipeSettingSetNamePanel.add(_recipeSettingSetSelectActionsWrapperPanel, BorderLayout.EAST);

    _recipeSettingSetSelectPanel.setLayout(_recipeSettingSetSelectPanelFlowLayout);
    _recipeSettingSetSelectPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    _recipeSettingSetSelectPanel.add(_recipeSettingSetSelectNameLabel);
    _recipeSettingSetSelectPanel.add(_recipeSettingSetSelectNameCombobox);

    _recipeSettingSetSelectNameLabel.setText(StringLocalizer.keyToString("CFGUI_NAME_KEY"));

    int textFieldColumns = 50;
    _recipeSettingSetLocationPanel.setLayout(_recipeSettingSetSelectPanelFlowLayout);
    _recipeSettingLocationLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_LOCATION_KEY"));

    _recipeSettingDirectoryTextField = new JTextField(textFieldColumns);
    JButton recipeSettingDirectoryChooserButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    _recipeSettingDirectoryTextField.setText(Directory.getInitialRecipeSettingDir());
    _recipeSettingSetLocationPanel.add(_recipeSettingLocationLabel);
    _recipeSettingSetLocationPanel.add(_recipeSettingDirectoryTextField);

    _recipeSettingSetLocationPanel.add(recipeSettingDirectoryChooserButton);
    _recipeSettingSetNamePanel.add(_recipeSettingSetLocationPanel, BorderLayout.NORTH);

    JLabel [] labels = new JLabel[]{_recipeSettingSetSelectNameLabel, _recipeSettingLocationLabel};
    SwingUtils.makeAllSameLength(labels);
    recipeSettingDirectoryChooserButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        recipeSettingDirectoryChooser();
      }
    });

    _recipeSettingDirectoryFocusAdapter = new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        recipeSettingDirectoryTextField_setValue();
      }
    };

    _recipeSettingDirectoryTextFieldActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        recipeSettingDirectoryTextField_setValue();
      }
    };
    _recipeSettingDirectoryTextField.addFocusListener(_recipeSettingDirectoryFocusAdapter);
    _recipeSettingDirectoryTextField.addActionListener(_recipeSettingDirectoryTextFieldActionListener);
    // end

    _recipeSettingSetSelectComboboxActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        recipeSettingSetChanged(false);
      }
    };

    _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);

    _recipeSettingSetSelectActionsWrapperPanel.setLayout(_recipeSettingSetSelectActionsWrapperPanelFlowLayout);
    _recipeSettingSetSelectActionsWrapperPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _recipeSettingSetSelectActionsWrapperPanel.add(_recipeSettingSetSelectActionsPanel);

    _recipeSettingSetSelectActionsPanel.setLayout(_recipeSettingSetSelectActionsPanelGridLayout);
    _recipeSettingSetSelectActionsPanelGridLayout.setColumns(4);
    _recipeSettingSetSelectActionsPanelGridLayout.setHgap(10);
    _recipeSettingSetSelectActionsPanelGridLayout.setRows(0);
    _recipeSettingSetSelectActionsPanelGridLayout.setVgap(10);
    _recipeSettingSetSelectActionsPanel.add(_newRecipeSettingSetButton);
    _recipeSettingSetSelectActionsPanel.add(_saveRecipeSettingSetButton);
    _recipeSettingSetSelectActionsPanel.add(_saveAsRecipeSettingSetButton);
    _recipeSettingSetSelectActionsPanel.add(_deleteRecipeSettignSetButton);

    _newRecipeSettingSetButton.setText(StringLocalizer.keyToString("CFGUI_NEW_BUTTON_KEY"));
    _newRecipeSettingSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        newRecipeSettingSetButton_actionPerformed();
      }
    });

    _saveRecipeSettingSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_BUTTON_KEY"));
    _saveRecipeSettingSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        saveCurrentRecipeSetting();
      }
    });

    _saveAsRecipeSettingSetButton.setText(StringLocalizer.keyToString("CFGUI_SAVE_AS_BUTTON_KEY"));
    _saveAsRecipeSettingSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        saveRecipeSettingSetAs();
      }
    });

    _deleteRecipeSettignSetButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_BUTTON_KEY"));
    _deleteRecipeSettignSetButton.addActionListener(new java.awt.event.ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        deleteRecipeSettingSetButton_actionPerformed();
      }
    });



    _recipeSettingSetDefinitionPanel.setLayout(_recipeSettingDefinitionPanelBorderLayout);
    _recipeSettingSetDefinitionPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
            StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_SET_DEFINITION_KEY")),
            BorderFactory.createEmptyBorder(0, 5, 5, 5)));


    _recipeSettingTablePanel.setLayout(_recipeSettingTablePanelPanelBorderLayout);
    _recipeSettingTablePanel.add(_recipeSettingTabbedPane, BorderLayout.CENTER);
    _recipeSettingSummarySplitPane.add(_recipeSettingTablePanel, JSplitPane.LEFT);

    // summary panel stuff
    _summaryInformationBorderLayout.setVgap(5);
    _summaryInformationPane.setLayout(_summaryInformationBorderLayout);

    _summaryEditorPane.setEditable(false);
    _summaryEditorPane.setContentType("text/html");
    _summaryScrollPane = new JScrollPane(_summaryEditorPane);
    _summaryLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_SUMMARY_KEY"));
    _summaryLabel.setFont(FontUtil.getBoldFont(_summaryLabel.getFont(),Localization.getLocale()));
    _summaryLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    _summaryInformationPane.add(_summaryLabel, BorderLayout.NORTH);
    _summaryInformationPane.add(_summaryScrollPane, BorderLayout.CENTER);
    _recipeSettingSummarySplitPane.add(_summaryInformationPane, JSplitPane.RIGHT);
    _recipeSettingSummarySplitPane.setDividerLocation(0.70);

    _recipeSettingSetDefinitionPanel.add(_recipeSettingSummarySplitPane, BorderLayout.CENTER);

    GridLayout setControlGridLayout = new GridLayout();
    setControlGridLayout.setRows(3);
    setControlGridLayout.setVgap(-5);
    _textExplainationLabel.setText(StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_EXPLAINATION_KEY"));
    _textExplainationLabel.setForeground(_labelColor);
    _setControlPanel.setLayout(setControlGridLayout);
    _setControlPanel.add(_textExplainationLabel);

    _recipeSettingSetDefinitionPanel.add(_setControlPanel, BorderLayout.NORTH);
    
    populateRecipeSettingTables();

  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void populateRecipeSettingTables()
  {
    // clear out all the tabs and the tables in them in preparation for creating new ones
    _recipeSettingTabbedPane.removeAll();
    

    InitialRecipeSettingTableModel recipeSettingModel = new InitialRecipeSettingTableModel(this);
    if (recipeSettingModel.getRowCount() > 0)  
    {
      JPanel tablePanel = new JPanel();
      BorderLayout tablePanelBorderLayout = new BorderLayout();
      tablePanel.setLayout(tablePanelBorderLayout);
      JScrollPane jsp = new JScrollPane();

      final JTable tt = new JTable(recipeSettingModel);
      jsp.getViewport().add(tt);
      tablePanel.add(jsp, BorderLayout.CENTER);
      _recipeSettingTabbedPane.add(tablePanel, "Recipe Advance Setting");
      setupTableCustomizations(tt);
      setupTableEditorsAndRenderers(tt);
      tt.addMouseListener(new MouseAdapter()
      {
        public void mouseReleased(MouseEvent e)
        {
          multiEditTableMouseReleased(e,tt);
        }
      });
      tt.changeSelection(0, 1, false, false);
    }

    _recipeSettingTabbedPane.requestFocus();

    validate();
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void multiEditTableMouseReleased(MouseEvent e,final JTable table)
  {
    Assert.expect(e != null);

    Point p = e.getPoint();
    final int row = table.rowAtPoint(p);
    final int col = table.columnAtPoint(p);
    
    Object recipeSettingName = table.getValueAt(row, 0);
    
    final RecipeAdvanceSettingEnum setting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName((String)recipeSettingName);
    
    if (col == 1)
    {
      if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING))
      {
        Rectangle rect = table.getCellRect(row, col, true);
        java.util.List<String> stageSpeedNameList = StageSpeedEnum.getOrderStageSpeedName();     
        java.util.List<Integer> listOfSelectedIndeces = new ArrayList<Integer>();  
        Object value = InitialRecipeSetting.getInstance().getRecipeSettingValue(setting);      
        if (value instanceof Object[])
        {
          Object[] values = (Object[]) value;
          for (Object o : values)
          {
            String name = (String) o;
            listOfSelectedIndeces.add(stageSpeedNameList.indexOf(name));
          }
        }
        else if (value instanceof java.util.List)
        {
          java.util.List<String> nameList = (java.util.List<String>) value;

          for (String name : nameList)
          {
            listOfSelectedIndeces.add(stageSpeedNameList.indexOf(name));
          }
        }

        Object isNewScanRoute = InitialRecipeSetting.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_NEW_SCAN_ROUTE_INTIAL_SETTING);

        Boolean booleanValue = new Boolean((String)isNewScanRoute);
        ScrollableCheckBoxListPopupMenu popupMenu = null;
        if (booleanValue)
        {
          final ScrollableCheckBoxListPopupMenu<String> popUpMenu = new ScrollableCheckBoxListPopupMenu(stageSpeedNameList,
                                                                                                        listOfSelectedIndeces,
                                                                                                        true,
                                                                                                        false,
                                                                                                        _defaultMinimumSelectionForStageSpeed,
                                                                                                        Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_SPEED_SELECTION));
          popupMenu = popUpMenu;
          popUpMenu.setVisibleRowCount(10);
          popUpMenu.setPreferredSize(new Dimension((int) rect.getWidth(), popUpMenu.getPreferredSize().height));
          popUpMenu.addPopupMenuListener(new PopupMenuListener()
          {
            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuWillBecomeVisible(PopupMenuEvent e)
            {
              //do nothing
            }

            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
            {
              if (popUpMenu.getSelectedValuesList().isEmpty())
              {
                table.setValueAt(setting.getDefaultValue(), row, col);
              }
              else
               table.setValueAt(new ArrayList<String>(popUpMenu.getSelectedValuesList()), row, col);
            }

            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuCanceled(PopupMenuEvent e)
            {
              //do nothing
            }
          });
        }
        else
        {
          final ScrollableCheckBoxListPopupMenu<String> popUpMenu = new ScrollableCheckBoxListPopupMenu(stageSpeedNameList,
                                                                                                                listOfSelectedIndeces, 
                                                                                                                true,
                                                                                                                false,
                                                                                                                _defaultMinimumSelectionForStageSpeed,
                                                                                                                _defaultMaximumSelectionForStageSpeed);
          popupMenu = popUpMenu;
          popUpMenu.setVisibleRowCount(10);
          popUpMenu.setPreferredSize(new Dimension((int) rect.getWidth(), popUpMenu.getPreferredSize().height));
          popUpMenu.addPopupMenuListener(new PopupMenuListener()
          {
            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuWillBecomeVisible(PopupMenuEvent e)
            {
              //do nothing
            }

            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
            {
              if (popUpMenu.getSelectedValuesList().isEmpty())
              {
                table.setValueAt(setting.getDefaultValue(), row, col);
              }
              else
              {
                table.setValueAt(new ArrayList<String>(popUpMenu.getSelectedValuesList()), row, col);
              }
            }

            /**
             * @author Yong Sheng Chuan
             */
            public void popupMenuCanceled(PopupMenuEvent e)
            {
              //do nothing
            }
          });
        }

        popupMenu.show(table,
                       (int) rect.getX(),
                       (int) (rect.getY() + rect.getHeight()));
      }
//      else if (setting.equals(RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING))
//      {
//        Rectangle rect = table.getCellRect(row, col, true);
//        java.util.List<String> cameraIdNameList = XrayCameraIdEnum.getCameraIDNameList();
//        java.util.List<Integer> listOfSelectedIndeces = new ArrayList<Integer>();
//        Object value = InitialRecipeSetting.getInstance().getRecipeSettingValue(setting);
//        if (value instanceof Object[])
//        {
//          Object[] values = (Object[]) value;
//          for (Object o : values)
//          {
//            String name = (String) o;
//            listOfSelectedIndeces.add(cameraIdNameList.indexOf(name));
//          }
//        }
//        else if (value instanceof java.util.List)
//        {
//          java.util.List<String> nameList = (java.util.List<String>) value;
//
//          for (String name : nameList)
//          {
//            listOfSelectedIndeces.add(cameraIdNameList.indexOf(name));
//          }
//        }
//
//        final ScrollableCheckBoxListPopupMenu<String> popUpMenu = new ScrollableCheckBoxListPopupMenu(cameraIdNameList,
//          listOfSelectedIndeces,
//          true,
//          false,
//          Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION),
//          _defaultMaximumSelectionForCameraAngle);
//        
//        popUpMenu.setVisibleRowCount(10);
//        popUpMenu.setPreferredSize(new Dimension((int) rect.getWidth(), popUpMenu.getPreferredSize().height));
//        popUpMenu.addPopupMenuListener(new PopupMenuListener()
//        {
//          /**
//           * @author Yong Sheng Chuan
//           */
//          public void popupMenuWillBecomeVisible(PopupMenuEvent e)
//          {
//            //do nothing
//          }
//
//          /**
//           * @author Yong Sheng Chuan
//           */
//          public void popupMenuWillBecomeInvisible(PopupMenuEvent e)
//          {
//            table.setValueAt(new ArrayList<String>(popUpMenu.getSelectedValuesList()), row, col);
//          }
//
//          /**
//           * @author Yong Sheng Chuan
//           */
//          public void popupMenuCanceled(PopupMenuEvent e)
//          {
//            //do nothing
//          }
//        });
//        
//        popUpMenu.show(table,
//                       (int) rect.getX(),
//                       (int) (rect.getY() + rect.getHeight()));
//      }
    }
  }
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void setupTableCustomizations(final JTable tt)
  {
    tt.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    _settingWidthsProgrammatically = true;
    for (int i = 0; i < tt.getColumnCount(); i++)
    {
      tt.getColumnModel().getColumn(i).setPreferredWidth(_persistSettings.getInitialRecipeSettingTableColumnWidth(i));
    }
    _settingWidthsProgrammatically = false;

    tt.getColumnModel().addColumnModelListener(getRecipeSettingColumnModelListener(tt));
    tt.getTableHeader().setReorderingAllowed(false);

//    tt.setRowSelectionAllowed(false);
//    tt.setColumnSelectionAllowed(false);

    // add the listener for table column selection
    ListSelectionModel colSM = tt.getColumnModel().getSelectionModel();
    colSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        //int selectedColumn = lsm.getMinSelectionIndex();
        tt.setColumnSelectionInterval(InitialRecipeSettingTableModel.VALUE_COLUMN, InitialRecipeSettingTableModel.VALUE_COLUMN);  // don't allow any column but 1 -- the value column
      }
    });

    // add the listener for a table row selection
    ListSelectionModel rowSM = tt.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {

      public void valueChanged(ListSelectionEvent e)
      {
        //System.out.println("List Value Changed changed");
        ListSelectionModel lsm = (ListSelectionModel) e.getSource();

        if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else
        {
          int selectedRow = lsm.getMinSelectionIndex();
          //System.out.println("List Selection Changed To Row: " + selectedRow);
          //CustomThresholdEditor tce = (CustomThresholdEditor)tt.getColumn(tt.getModel().getColumnName(1)).getCellEditor();
          tt.changeSelection(selectedRow, InitialRecipeSettingTableModel.VALUE_COLUMN, false, false);
//          updateThresholdDescription(subtype, tt, selectedRow);
          if (!tt.isEditing())
          {
            tt.editCellAt(selectedRow, InitialRecipeSettingTableModel.VALUE_COLUMN);
            java.awt.Component comp = tt.getEditorComponent();
            // do a request focus on the cell editor lets the default behaviour of selection all the text happen
            if (comp instanceof JButton)
            {
              tt.getCellEditor(selectedRow, InitialRecipeSettingTableModel.VALUE_COLUMN).stopCellEditing();
            }
            if (comp instanceof JTextField)
            {
              comp.requestFocus();
            }
          }
        }
      }
    });
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void setupTableEditorsAndRenderers(final JTable tt)
  {
    Assert.expect(tt != null);

    int rows = tt.getRowCount();
    final CustomThresholdEditor rowEditor = new CustomThresholdEditor(tt);
    tt.getColumn(tt.getModel().getColumnName(1)).setCellEditor(rowEditor);
    CustomRecipeSettingRenderer renderer = new CustomRecipeSettingRenderer();
    for (int i = 0; i < rows; i++)
    {
      String settingName = (String) tt.getModel().getValueAt(i, 0);
      final RecipeAdvanceSettingEnum recipeSetting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName(settingName);  // 0 column is the AlgorithmSetting object
      Object value = recipeSetting.getDefaultValue();
      Assert.expect(value != null);
      if (value instanceof java.util.List)  // must be string value
      {
        
      }
      else if (value instanceof String)
      {
        Object[] validValues = recipeSetting.getValidSelection();
        
        java.util.List<String> availableValues = new ArrayList<String>();
        
        for (Object object : validValues)
        {
          availableValues.add((String) object);
        }
        final VariableWidthComboBox comboBox = new VariableWidthComboBox(availableValues.toArray());
        comboBox.setPopupWidthToDefault();
        // set the popup width to the max of the strings only if that's longer than the box itself
        FontMetrics fontMetrics = comboBox.getFontMetrics(comboBox.getFont());
        int maxStringLength = 0;
        for (String valueString : availableValues)
        {
          int itemStringLength = fontMetrics.stringWidth(valueString);
          if (maxStringLength < itemStringLength)
          {
            maxStringLength = itemStringLength;
          }
        }
        if ((maxStringLength + VariableWidthComboBox.widthExtension) <= comboBox.getWidth())
        {
          comboBox.setPopupWidthToDefault();
        }
        else
        {
          comboBox.setPopupWidth(maxStringLength + VariableWidthComboBox.widthExtension); // add a bit more so the last char is not RIGHT at the popup right side.
        }
        comboBox.getInputMap().put(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true), "enableComboBox");
//        comboBox.getActionMap().put("enableComboBox", new EnableComboBoxAction());
        rowEditor.setEditorAt(i, new DefaultCellEditor(comboBox));
      }
      else
      {
        Assert.expect(false);
      }
    }
    tt.setDefaultRenderer(Object.class, renderer);

  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void newRecipeSettingSetButton_actionPerformed()
  {
    if (saveRecipeSettingSetIfNecessary() == false)
      return;

    String recipeSettingSet = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_ENTER_NEW_SET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_NEW_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (recipeSettingSet != null) // null means the dialog was canceled
    {
      recipeSettingSet = getLegalFileName(recipeSettingSet);
      if (recipeSettingSet == null)
        return;

      _initialRecipeSetting.createNewSet(recipeSettingSet);

      _recipeSettingSetSelectNameCombobox.addItem(recipeSettingSet);
      _recipeSettingSetSelectNameCombobox.removeActionListener(_recipeSettingSetSelectComboboxActionListener);
      _recipeSettingSetSelectNameCombobox.setSelectedItem(recipeSettingSet);
      _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);

      populateRecipeSettingTables();
      checkProtectedSet();
      updateSummaryPanel();
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private boolean saveCurrentRecipeSetting()
  {
    try
    {
      _initialRecipeSetting.save();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      return false;
    }
    _isChangingDirectories = false;
    _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_RECIPE_SETTING_SAVED_SET_KEY",
                                                                             new Object[]{_initialRecipeSetting.getRecipeSettingName()})));
    return true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private boolean saveRecipeSettingSetAs()
  {
    String newRecipeSettingName = JOptionPane.showInputDialog(_mainUI,
        StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_ENTER_NEW_SET_NAME_KEY"),
        StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_NEW_SET_TITLE_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    if (newRecipeSettingName != null) // null means the dialog was canceled
    {
      newRecipeSettingName = getLegalFileName(newRecipeSettingName);
      if (newRecipeSettingName == null)
        return false;

      try
      {
        _initialRecipeSetting.changeRecipeSettingName(newRecipeSettingName);

        _initialRecipeSetting.save();
        // clear the undo stack when a new set is loaded in the GUI (save as loads a new set)
        clearUndoStack();

        if (recipeSettingSetNameComboBoxContainsName(newRecipeSettingName) == false)
          _recipeSettingSetSelectNameCombobox.addItem(newRecipeSettingName);
        _recipeSettingSetSelectNameCombobox.removeActionListener(_recipeSettingSetSelectComboboxActionListener);
        _recipeSettingSetSelectNameCombobox.setSelectedItem(newRecipeSettingName);
        _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);
        populateRecipeSettingTables();
        checkProtectedSet();
        updateSummaryPanel();
        _isChangingDirectories = false;
        return true;
      }
      catch (DatastoreException ex)
      {
        showDatastoreError(ex);
      }
    }
    return false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void showDatastoreError(DatastoreException ex)
  {
    Assert.expect(ex != null);

    JOptionPane.showMessageDialog(_mainUI,
                                  ex.getLocalizedMessage(),
                                  StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private String getLegalFileName(String newRecipeSettingName)
  {
    Assert.expect(newRecipeSettingName != null);

    String whiteSpacePattern = "^\\s*$";
    java.util.List<String> tempConfigNamesList = new ArrayList<String>();
    for (String name : FileName.getAllInitialRecipeSettingNames())
    {
      tempConfigNamesList.add(name.toUpperCase());
    }
    String tempRecipeSettingName = newRecipeSettingName.toUpperCase();
    while (newRecipeSettingName != null &&
            (tempConfigNamesList.contains(tempRecipeSettingName) ||
            FileName.hasIllegalChars(newRecipeSettingName) ||
            Pattern.matches(whiteSpacePattern, newRecipeSettingName)))
    {
      if (tempConfigNamesList.contains(tempRecipeSettingName))
      {
        newRecipeSettingName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_RECIPE_SETTING_SET_EXISTS_MESSAGE_KEY",
                                                                new Object[]{newRecipeSettingName})),
                StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newRecipeSettingName != null)
        {
          tempRecipeSettingName = newRecipeSettingName.toUpperCase();
        }
        else
        {
          return newRecipeSettingName;
        }
      }
      else if (FileName.hasIllegalChars(newRecipeSettingName))
      {
        newRecipeSettingName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString(
                "CFGUI_INITIAL_RECIPE_SETTING_SET_ILLEGAL_NAME_KEY",
                new Object[]
                {
                  newRecipeSettingName, FileName.getIllegalChars()
                })),
                StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newRecipeSettingName != null)
        {
          tempRecipeSettingName = newRecipeSettingName.toUpperCase();
        }
        else
        {
          return newRecipeSettingName;
        }
      }
      else if (Pattern.matches(whiteSpacePattern, newRecipeSettingName))
      {
        newRecipeSettingName = JOptionPane.showInputDialog(_mainUI,
                StringLocalizer.keyToString(new LocalizedString(
                "CFGUI_INITIAL_RECIPE_SETTING_SET_ILLEGAL_EMPTY_NAME_KEY",
                new Object[]
                {
                  newRecipeSettingName
                })),
                StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_NEW_SET_TITLE_KEY"),
                JOptionPane.INFORMATION_MESSAGE);
        if (newRecipeSettingName != null)
        {
          tempRecipeSettingName = newRecipeSettingName.toUpperCase();
        }
        else
        {
          return newRecipeSettingName;
        }
      }
    }
    return newRecipeSettingName;
  }


  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void deleteRecipeSettingSetButton_actionPerformed()
  {
    Object value = _recipeSettingSetSelectNameCombobox.getSelectedItem();
    Assert.expect(value instanceof String);
    String selectedThresholdSetName = (String)value;

    int answer = JOptionPane.showConfirmDialog(_mainUI,
                                               StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_RECIPE_SETTING_DELETE_SET_KEY",
                                                                                               new Object[]{selectedThresholdSetName})),
                                               StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_CONFIRM_DELETE_TITLE_KEY"),
                                               JOptionPane.YES_NO_OPTION);
    if (answer != JOptionPane.YES_OPTION)
      return;

    try
    {
      _initialRecipeSetting.delete();

      _recipeSettingSetSelectNameCombobox.removeActionListener(_recipeSettingSetSelectComboboxActionListener);
      _recipeSettingSetSelectNameCombobox.removeItem(selectedThresholdSetName);
      _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);

      clearUndoStack();
      // set selection to another item in the list for the 'select name' combobox
      int index = _recipeSettingSetSelectNameCombobox.getSelectedIndex();
      int lastIndex = _recipeSettingSetSelectNameCombobox.getItemCount() - 1 ;
      if (index > lastIndex ||
          (index == -1 && lastIndex != -1))
        _recipeSettingSetSelectNameCombobox.setSelectedIndex(lastIndex);
      else if (index != -1)
        _recipeSettingSetSelectNameCombobox.setSelectedIndex(index);
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void start()
  {
//    System.out.println("SWOptionsJointTypeTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    // don't want to be able to import a project while changing a recipe setting set
    _mainUI.getMainMenuBar().disableImportMenuItem();

    _mainUI.setStatusBarText("");

    populateWithConfigData();

    int dividerLocation = _persistSettings.getInitialRecipeSettingTableSummaryDivider();
    if (dividerLocation > 0)
      _recipeSettingSummarySplitPane.setDividerLocation(dividerLocation);
    else
      _recipeSettingSummarySplitPane.setDividerLocation(0.70);
    // add custom menus
    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isReadyToFinish()
  {
    return saveRecipeSettingSetIfNecessary();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private boolean saveRecipeSettingSetIfNecessary()
  {
    if (_initialRecipeSetting.isDirty())
    {
      String currentSetName = _initialRecipeSetting.getRecipeSettingName();
      String saveChangesQuestion;
      if (_initialRecipeSetting.isDefaultSet(currentSetName))
        saveChangesQuestion = StringLocalizer.keyToString("CFGUI_INITIAL_RECIPE_SETTING_SAVE_CHANGES_PROTECTED_SET_KEY");
      else
        saveChangesQuestion = StringLocalizer.keyToString(new LocalizedString("CFGUI_INITIAL_RECIPE_SETTING_SAVE_CHANGES_KEY",
                                                                              new Object[]{currentSetName}));
      int answer = JOptionPane.showConfirmDialog(_mainUI,
                                                 saveChangesQuestion,
                                                 StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
                                                 JOptionPane.YES_NO_CANCEL_OPTION);
      if (answer == JOptionPane.CANCEL_OPTION)
      {
        return false;
      }
      else if (answer == JOptionPane.YES_OPTION)
      {
        if (_initialRecipeSetting.isDefaultSet(currentSetName))
          return saveRecipeSettingSetAs();
        else
          return saveCurrentRecipeSetting();
      }
      else
      {
        // if the user wants to discard the changes to a new recipe setting set, remove it from the select combobox because
        // it has never been saved to disk. A recipe setting set is new if it doesn't exist on disk
        if (_initialRecipeSetting.getAllSetNames().contains(_initialRecipeSetting.getRecipeSettingName()) == false)
        {
          _recipeSettingSetSelectNameCombobox.removeActionListener(_recipeSettingSetSelectComboboxActionListener);
          _recipeSettingSetSelectNameCombobox.removeItem(_initialRecipeSetting.getRecipeSettingName());
          _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);
          if (_recipeSettingSetSelectNameCombobox.getSelectedIndex() >= 0)
            recipeSettingSetChanged(true);
//            _initialThresholds.loadNewThresholdSet((String)_recipe settingSetSelectNameCombobox.getSelectedItem());
        }
        // clear undo stack because what ever changes the user made, they chose to discard them
        clearUndoStack();
        _initialRecipeSetting.loadDefaultSet();
      }
    }
    return true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    int dividerLocation = _recipeSettingSummarySplitPane.getDividerLocation();
    _persistSettings.setInitialRecipeSettigTableSummaryDivider(dividerLocation);

    // restore ability to import a project
    _mainUI.getMainMenuBar().enableImportMenuItem();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("SWOptionsJointTypeTabPanel().finish()");
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof com.axi.v810.gui.undo.CommandManager)
        {
          handleUndoState(object);
        }
        else if (observable instanceof ConfigObservable)
        {
          handleConfigDataUpdate(object);
        }
        else
        {
          Assert.expect(false);
        }
      }
    });
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  UndoState getCurrentUndoState(int recipeSettingRow,RecipeAdvanceSettingEnum recipeSetting)
  {
    UndoState undoState = new UndoState();
    undoState.setInitialRecipeSettingSetComboboxSelectedIndex(_recipeSettingSetSelectNameCombobox.getSelectedIndex());
    int undoTabIndex = -1;
    String recipeSettingName = recipeSetting.getName();

    for (int i = 0; i < _recipeSettingTabbedPane.getTabCount(); i++)
    {
      if (_recipeSettingTabbedPane.getTitleAt(i).equals(recipeSettingName))
        undoTabIndex = i;
    }

    undoState.setInitialRecipeSettingRow(recipeSettingRow);

    return undoState;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  UndoState getCurrentUndoState(int recipeSettingRow)
  {
    UndoState undoState = new UndoState();
    undoState.setInitialRecipeSettingSetComboboxSelectedIndex(_recipeSettingSetSelectNameCombobox.getSelectedIndex());
    undoState.setInitialRecipeSettingRow(recipeSettingRow);
    return undoState;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void handleConfigDataUpdate(final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(object != null);
        
        updateSummaryPanel();
      }
    });
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void handleUndoState(final Object stateObject)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        UndoState undoState = (UndoState)stateObject;

        // revert to correct path
        if (_recipeSettingDirectoryTextField.getText().equalsIgnoreCase(Directory.getInitialRecipeSettingDir()) == false)
        {
          updateRecipeSettingDirectoryTextField();
          populateRecipeSettignSetSelectCombobox();
        }
        else
        {
          int selectedIndex = undoState.getInitialRecipeSettingSetComboboxSelectedIndex();
          if (_recipeSettingSetSelectNameCombobox.getSelectedIndex() != selectedIndex)
            _recipeSettingSetSelectNameCombobox.setSelectedIndex(selectedIndex);

          // select the recipe setting row
          java.awt.Component comp = _recipeSettingTabbedPane.getSelectedComponent();
          Assert.expect(comp instanceof JPanel);
          JPanel compPanel = (JPanel)comp;
          java.awt.Component[] componentArray = compPanel.getComponents();
          for (int compIndex = 0; compIndex < componentArray.length; compIndex++)
          {
            java.awt.Component potentialComp = componentArray[compIndex];
            if (potentialComp instanceof JScrollPane)
            {
              JScrollPane jsp = (JScrollPane)potentialComp;
              java.awt.Component comp1 = (JTable)jsp.getViewport().getView();
              if (comp1 == null)
                return;
              else
              {
                JTable tt = (JTable)comp1;
                int recipeSettingRow = undoState.getInitialRecipeSettingRow();
                if (recipeSettingRow < 0)
                  return;
                tt.setRowSelectionInterval(recipeSettingRow, recipeSettingRow);
                RecipeAdvanceSettingEnum setting = RecipeAdvanceSettingEnum.getRecipeSettingEnumByName((String)tt.getValueAt(recipeSettingRow, InitialRecipeSettingTableModel.NAME_COLUMN));
                Object value = _initialRecipeSetting.getRecipeSettingValue(setting);
                tt.setValueAt(value, recipeSettingRow, InitialRecipeSettingTableModel.VALUE_COLUMN);
                TableCellEditor tce = tt.getCellEditor();
                if (tce != null)
                  tce.cancelCellEditing();
                tt.requestFocus();
              }
            }
          }
        }
      }
    });
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void populateWithConfigData()
  {
    populateRecipeSettignSetSelectCombobox();
    String currentSetName = _initialRecipeSetting.getRecipeSettingName();

    // if set name no longer exists, we'll rever to SystemDefaults at index 0
    if (recipeSettingSetNameComboBoxContainsName(currentSetName))
      _recipeSettingSetSelectNameCombobox.setSelectedItem(currentSetName);
    else
      _recipeSettingSetSelectNameCombobox.setSelectedIndex(0);
    checkProtectedSet();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void updateSummaryPanel()
  {
    String recipeSettingTab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    String totalText = "";
    _summaryEditorPane.setText(null);
    _summaryEditorPane.setContentType("text/html");

    for(Entry<RecipeAdvanceSettingEnum,Object> entrySet : _initialRecipeSetting.getRecipeAdvanceSettingEnumsInOrder().entrySet())
    {
      String text = "";
      
      RecipeAdvanceSettingEnum recipeSetting = entrySet.getKey(); 
      Object value = entrySet.getValue();
      
      if (value.equals(recipeSetting.getDefaultValue()) == false)
      {
        text += recipeSettingTab + recipeSetting.getName() + ": " + value + "<br>";
      }
      totalText += text;
    }
    javax.swing.text.Document document = _summaryEditorPane.getEditorKit().createDefaultDocument();
    _summaryEditorPane.setDocument(document);
    _summaryEditorPane.setText(totalText);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   * @return
   */
  private void checkProtectedSet()
  {
    Object object = _recipeSettingSetSelectNameCombobox.getSelectedItem();
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String) object;
      Boolean enabled = true;
      // if the file is the default file, disable the save and delete buttons, otherwise, enable them
      if (_initialRecipeSetting.isDefaultSet(selectedName))
      {
        enabled = false;
      }
      _saveRecipeSettingSetButton.setEnabled(enabled);
      _deleteRecipeSettignSetButton.setEnabled(enabled);
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void populateRecipeSettignSetSelectCombobox()
  {
    _recipeSettingSetSelectNameCombobox.removeActionListener(_recipeSettingSetSelectComboboxActionListener);
    _recipeSettingSetSelectNameCombobox.removeAllItems();
    java.util.List<String> allSetNames = _initialRecipeSetting.getAllSetNames();
    for(String setName : allSetNames)
      _recipeSettingSetSelectNameCombobox.addItem(setName);
    _recipeSettingSetSelectNameCombobox.addActionListener(_recipeSettingSetSelectComboboxActionListener);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void recipeSettingSetChanged(boolean skipSave)
  {
    if (skipSave == false)
    {
      if (saveRecipeSettingSetIfNecessary() == false)
        return;
    }

    try
    {
      String newSetName = (String) _recipeSettingSetSelectNameCombobox.getSelectedItem();
      
      
      if(_initialRecipeSetting.isRecipeSettingNameExist(newSetName))
      {
        _initialRecipeSetting.loadNewRecipeSetting(newSetName);
      }
      else
      {
        _initialRecipeSetting.loadDefaultSet();
      }
      populateRecipeSettingTables();
      checkProtectedSet();
      updateSummaryPanel();
    }
    catch (DatastoreException ex)
    {
      showDatastoreError(ex);
      // revert back to SystemDefault setting
      _recipeSettingSetSelectNameCombobox.setSelectedIndex(0);
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private boolean recipeSettingSetNameComboBoxContainsName(String recipeSettingName)
  {
    Assert.expect(recipeSettingName != null);

    for (int i = 0; i < _recipeSettingSetSelectNameCombobox.getItemCount(); i++)
    {
      Object item = _recipeSettingSetSelectNameCombobox.getItemAt(i);
      Assert.expect(item instanceof String);
      String name = (String)item;
      if (name.equalsIgnoreCase(recipeSettingName))
        return true;
    }
    return false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void clearUndoStack()
  {
    _commandManager.clear();
    _menuBar.updateUndoRedoMenuItems();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private TableColumnModelListener getRecipeSettingColumnModelListener(final JTable table)
  {
    return new TableColumnModelListener()
    {
      // It is necessary to keep the table since it is not possible
      // to determine the table from the event's source
      public void columnAdded(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnRemoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMoved(TableColumnModelEvent e)
      {
        // do nothing
      }

      public void columnMarginChanged(ChangeEvent e)
      {
        if (_settingWidthsProgrammatically)
          return;
        if (table.isValid() == false)
          return;
        for (int i = 0; i < table.getColumnCount(); i++)
        {
//           System.out.println("Setting persist file data for column " + i + " to width " + table.getColumnModel().getColumn(i).getWidth());
          _persistSettings.setInitialRecipeSettingTableColumnWidth(i, table.getColumnModel().getColumn(i).getWidth());
        }
      }

      public void columnSelectionChanged(ListSelectionEvent e)
      {
        // do nothing
      }
    };
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void recipeSettingDirectoryChooser()
  {
    JFileChooser fileChooser = new JFileChooser(_recipeSettingDirectoryTextField.getText());
    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(),
                               StringLocalizer.keyToString("GUI_SELECT_FOLDER_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isDirectory())
    {
      String fileName = selectedFile.getPath();
      _recipeSettingDirectoryTextField.setText(fileName);
      _recipeSettingDirectoryTextField.requestFocus();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void recipeSettingDirectoryTextField_setValue()
  {
    if (_inUse)
      return;
    _inUse = true;

    // if the value has not changed, then do nothing
    if (_recipeSettingDirectoryTextField.getText().equalsIgnoreCase(Directory.getInitialRecipeSettingDir()))
    {
      _inUse = false;
      return;
    }

    // see if the current set is dirty, and don't change the text field if they choose to cancel
    if (saveRecipeSettingSetIfNecessary() == false)
    {
      // user cancelled, so let's not accept the new directory text field entry
      updateRecipeSettingDirectoryTextField();
      _inUse = false;
      return;
    }
    if(_isChangingDirectories == false)
    {
      clearUndoStack();
      _isChangingDirectories = true;
    }

    String fullPath = _recipeSettingDirectoryTextField.getText();
    fullPath = Directory.shortenPath(fullPath);
    if ( _envPanel.isPathValid(fullPath) )
    {
      ConfigSetInitialRecipeSettingDirectoryCommand setInitialRecipeSettingDirectoryCommand =
            new ConfigSetInitialRecipeSettingDirectoryCommand(fullPath);
      try
      {
        _commandManager.execute(setInitialRecipeSettingDirectoryCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
    else
    {
      // this is if isPathValid returns false -- most likely because the user chose NOT to create the new directory
      // we should revert back to current directory
      updateRecipeSettingDirectoryTextField();
    }
    _inUse = false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void updateRecipeSettingDirectoryTextField()
  {
    _recipeSettingDirectoryTextField.removeActionListener(_recipeSettingDirectoryTextFieldActionListener);
    _recipeSettingDirectoryTextField.removeFocusListener(_recipeSettingDirectoryFocusAdapter);
    _recipeSettingDirectoryTextField.setText(Directory.getInitialRecipeSettingDir());
    _recipeSettingDirectoryTextField.addActionListener(_recipeSettingDirectoryTextFieldActionListener);
    _recipeSettingDirectoryTextField.addFocusListener(_recipeSettingDirectoryFocusAdapter);
  }
}
