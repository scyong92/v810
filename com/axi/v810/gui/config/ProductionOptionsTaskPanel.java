package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

public class ProductionOptionsTaskPanel extends AbstractTaskPanel
{
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JTabbedPane _tabbedPane = new JTabbedPane();
  private ProductionOptionsGeneralTabPanel _generalTabPanel;
  private ProductionOptionsSerialNumberTabPanel _serialNumberTypeTabPanel;
  private ProductionOptionsBarCodeReaderTabPanel _barCodeReaderTabPanel;
  private ProductionOptionsFalseCallTriggeringTabPanel _falseCallTriggeringTabPanel;
  private ProductionOptionsResultOutputFormatTabPanel _measurementsXMLTabPanel;  //janan

  private ConfigEnvironment _envPanel = null;

  private AbstractTaskPanel _currentActiveTabPanel;

  /**
   * @author Laura Cormos
   */
  public ProductionOptionsTaskPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.add(_tabbedPane, BorderLayout.CENTER);
    _generalTabPanel = new ProductionOptionsGeneralTabPanel(_envPanel);
    _tabbedPane.add(_generalTabPanel, StringLocalizer.keyToString("CFGUI_GENERAL_TAB_LABEL_KEY"));
    _serialNumberTypeTabPanel = new ProductionOptionsSerialNumberTabPanel(_envPanel);
    _tabbedPane.add(_serialNumberTypeTabPanel, StringLocalizer.keyToString("CFGUI_SERIAL_NUMBER_TAB_LABEL_KEY"));
    _barCodeReaderTabPanel = new ProductionOptionsBarCodeReaderTabPanel(_envPanel);
    _tabbedPane.add(_barCodeReaderTabPanel, StringLocalizer.keyToString("CFGUI_BARCODEREADER_TAB_LABEL_KEY"));
    _tabbedPane.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tabbedPane_stateChanged(e);
      }
    });
    
     _falseCallTriggeringTabPanel  = new ProductionOptionsFalseCallTriggeringTabPanel(_envPanel);
    _tabbedPane.add(_falseCallTriggeringTabPanel, StringLocalizer.keyToString("False Call Triggering"));
    _tabbedPane.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tabbedPane_stateChanged(e);
      }
    });

    _currentActiveTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();

    _measurementsXMLTabPanel = new ProductionOptionsResultOutputFormatTabPanel(_envPanel);
    _tabbedPane.add(_measurementsXMLTabPanel, StringLocalizer.keyToString("CFGUI_RESULT_OUTPUT_FORMAT_TAB_LABEL_KEY"));

    // define menus and toolBar items to be added later

//    _setDefaultMenuItem.setText("Set &Default User Config");
//    _setDefaultMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        setDefault(e);
//      }
//    });

//    _configHelpMenuItem.setText("&Config User Help");
//    _configHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        configHelp(e);
//      }
//    });
  }

  /**
   * set default action
   * @param e ActionEvent
   * @author George Booth
   */
  void setDefault(ActionEvent e)
  {
//    System.out.println("ProductionOptionsTaskPanel().setDefault()");
  }

  /**
   * config help action
   * @param e ActionEvent
   * @author George Booth
   */
  void configHelp(ActionEvent e)
  {
//    System.out.println("ProductionOptionsTaskPanel().configHelp()");
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ProductionOptionsTaskPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();


    // add custom menus
//    JMenu configMenu = _envPanel.getConfigMenu();
//    _menuBar.addMenuItem(_menuRequesterID, configMenu, _setDefaultMenuItem);

//    JMenu helpMenu = _menuBar.getHelpMenu();
//    _menuBar.addMenuItem(_menuRequesterID, helpMenu, 0, _configHelpMenuItem);

//    _menuBar.addMenuSeparator(_menuRequesterID, helpMenu, 1);

    _currentActiveTabPanel.start();
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return _currentActiveTabPanel.isReadyToFinish();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    _currentActiveTabPanel.finish();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("ProductionOptionsTaskPanel().finish()");
  }

  /**
   * @author Laura Cormos
   */
  private void tabbedPane_stateChanged(ChangeEvent e)
  {
    AbstractTaskPanel selectedTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();

    if (_currentActiveTabPanel == selectedTabPanel)
      return;

    // first finish the currently active panel
    if (_currentActiveTabPanel.isReadyToFinish())
    {
      _currentActiveTabPanel.finish();
    }
    else
    {
      _tabbedPane.setSelectedComponent(_currentActiveTabPanel);
      return;
    }

    // then start the chosen tab is the previous tab is finished
    selectedTabPanel.start();
    _currentActiveTabPanel = selectedTabPanel;
  }

}
