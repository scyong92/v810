package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
class ResultsProcessingRulesTableModel extends AbstractTableModel
{
  private MainMenuGui _mainUI;
  //String labels retrieved from the localization properties file
  private static final String _RULE_NAME_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_RULE_NAME_COLUMN_KEY");
  private static final String _RULE_ENABLED_LABEL = StringLocalizer.keyToString("CFGUI_RESULTS_RULE_ENABLE_COLUMN_KEY");

  private String[] _columnLabels = { _RULE_NAME_LABEL,
                                     _RULE_ENABLED_LABEL };
  private List<Rule> _rules;
  private List<Rule> _selectedRules = new ArrayList<Rule>();

  private ResultsProcessingResultsTabPanel _parent;
  private static com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  /**
   * Constructor.
   * @author Laura Cormos
   */
  public ResultsProcessingRulesTableModel(ResultsProcessingResultsTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _mainUI = MainMenuGui.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_rules != null)
      _rules.clear();
    if (_selectedRules != null)
      _selectedRules.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    Rule rule = _rules.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case ResultsProcessingRulesTable._RULE_NAME_INDEX:
      {
        value = rule.getName();
        break;
      }
      case ResultsProcessingRulesTable._RULE_ENABLED_INDEX:
      {
        value = new Boolean(rule.getIsEnabled());
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    if(_rules != null)
    {
      return _rules.size();
    }
    else
      return 0;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if ((_selectedRules == null) || (_selectedRules.contains(_rules.get(rowIndex)) == false))
      return false;

    return true;
  }

  /**
   * Initialize the table with data from the results processing config file
   * @author Laura Cormos
   */
  public void setData(List<Rule> rulesList)
  {
    Assert.expect(rulesList != null);

    _rules = rulesList;

    fireTableDataChanged();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Rule rule = _rules.get(rowIndex);

    switch(columnIndex)
    {
      case ResultsProcessingRulesTable._RULE_NAME_INDEX:
      {
        Assert.expect(object instanceof String);
        String ruleName = (String)object;
        ruleName = _parent.getLegalFileName(ruleName);
        if (ruleName == null)
          return;

        try
        {
          _commandManager.execute(new ConfigSetResultsRuleNameCommand(rule, ruleName));
          _parent.setCurrentSelectedRuleIsDirty(true);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      }
      case ResultsProcessingRulesTable._RULE_ENABLED_INDEX:
      {
        Assert.expect(object instanceof Boolean);
        boolean isEnabled = ((Boolean)object).booleanValue();
        try
        {
          _commandManager.execute(new ConfigSetResultsRuleEnabledCommand(rule, isEnabled));
          _parent.setCurrentSelectedRuleIsDirty(true);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(_mainUI,
                                        ex.getLocalizedMessage(),
                                        StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                        true);
          fireTableCellUpdated(rowIndex, columnIndex);
        }
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
  }

  /**
   * Get the Rule instance associated with the row index
   * @author Laura Cormos
   */
  Rule getRule(int rowIndex)
  {
    Assert.expect(rowIndex >= 0);

    return _rules.get(rowIndex);
  }

  /**
   * @author Laura Cormos
   */
  int getIndexForRule(Rule rule)
  {
    Assert.expect(rule != null);
    return _rules.indexOf(rule);
  }

  /**
   * @author Laura Cormos
   */
  void addRule(Rule rule)
  {
    Assert.expect(rule != null);
    _rules.add(rule);
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  Rule getRuleAt(int index)
  {
    Assert.expect(index >= 0);
    Assert.expect(_rules != null);

    return _rules.get(index);
  }

  /**
   * @author Laura Cormos
   */
  void setSelectedRow(int selectedRowIndex)
  {
    Assert.expect(selectedRowIndex >= 0);
    Assert.expect(_selectedRules != null);

    _selectedRules.clear();
    _selectedRules.add(_rules.get(selectedRowIndex));
  }

  /**
   * @author Laura Cormos
   */
  public void removeRule(Rule rule)
  {
    Assert.expect(rule != null);

    _rules.remove(_rules.indexOf(rule));
    fireTableDataChanged();
  }
}
