package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: CustomerOptionsLoginTabPanel</p>
 *
 * <p>Description: A panel in the Config environment that displays and maintains
 * user login accounts</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class CustomerOptionsLoginTabPanel extends AbstractTaskPanel implements Observer
{
  private JPanel _westPanel = null;

  private JPanel _userAccountsPanel = null;
  private JScrollPane _userAccountsScrollPane = null;
  private UserAccountsTableModel _userAccountsTableModel = null;
  private UserAccountsTable _userAccountsTable = null;
  private FocusAdapter _textFieldFocusAdapter;

  private JPanel _accountDetailsPanel = null;
  private JPanel _detailsPanel = null;
  private JLabel _accountNameLabel = null;
  private JTextField _accountNameTextField = null;
  private JLabel _fullNameLabel = null;
  private JTextField _fullNameTextField = null;
  private JLabel _passwordLabel = null;
  private JPasswordField _passwordField = null;
  private JLabel _confirmPasswordLabel = null;
  private JPasswordField _confirmPasswordField = null;
  private JLabel _accountTypeLabel = null;
  private JComboBox _accountTypeComboBox = null;
  private JPanel _buttonPanel = null;
  private JButton _clearAccountButton = null;
  private JButton _newAccountButton = null;
  private JButton _updateAccountButton = null;
  private JButton _deleteAccountButton = null;

  final private int _TEXT_FIELD_COLUMNS = 30;
  final private boolean _NEW_ACCOUNT = true;
  final private boolean _UPDATE_ACCOUNT = false;
  private List<UserTypeEnum> _userTypeEnums = null;
  // the current logged in user name and type
  private String _currentUserName = null;
  private UserTypeEnum _currentUserType = null;

  private LoginManager _loginManager = LoginManager.getInstance();
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private UserAccountsObservable _userAccountsObservable = UserAccountsObservable.getInstance();

  /**
   * @author George Booth
   */
  public CustomerOptionsLoginTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    _textFieldFocusAdapter = new java.awt.event.FocusAdapter()
    {
      public void focusGained(FocusEvent e)
      {
        JTextField sourceTextField = (JTextField)e.getSource();
        sourceTextField.selectAll();
      }
    };

    // set up panel GUI components

    _westPanel = new JPanel();
    _westPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 5));
    _westPanel.setLayout(new BorderLayout());
    add(_westPanel, BorderLayout.WEST);

    // user accounts table

    _userAccountsPanel = new JPanel();
    _userAccountsPanel.setLayout(new BorderLayout());
    _westPanel.add(_userAccountsPanel, BorderLayout.CENTER);

    _userAccountsTableModel = new UserAccountsTableModel();
    _userAccountsTable = new UserAccountsTable(_userAccountsTableModel)
    {
      /**
       * Overriding this method allows us to make sure the selected rows
       * are still the proper selected rows after a sort.
       * @author George Booth
       */
      public void mouseReleased(MouseEvent event)
      {
        // make sure selection is correct after a sort
        int selectedRow = _userAccountsTable.getSelectedRow();
        if (selectedRow == -1)
        {
          // no selection, no worries
          super.mouseReleased(event);
        }
        else
        {
          String selectedUserName = _userAccountsTableModel.getUserAccount(selectedRow).getAccountName();
          super.mouseReleased(event);
          selectedRow = _userAccountsTableModel.getRowForUserName(selectedUserName);
          Assert.expect(selectedRow > -1);
          _userAccountsTable.setRowSelectionInterval(selectedRow, selectedRow);
        }
      }
    };
    setupUserAccountsTableListSelection();
    _userAccountsScrollPane = new JScrollPane();
    _userAccountsScrollPane.getViewport().add(_userAccountsTable);
    _userAccountsPanel.add(_userAccountsScrollPane, BorderLayout.CENTER);

    // user account details

    _accountDetailsPanel = new JPanel();
    _accountDetailsPanel.setBorder(
    BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(
              Color.white, new Color(166, 166, 166)),
              StringLocalizer.keyToString("CFGUI_USER_ACCOUNTS_DETAILS_PANEL_KEY")),
        BorderFactory.createEmptyBorder(10, 5, 0, 5))
      );
    _accountDetailsPanel.setLayout(new BorderLayout(20, 20));
    _westPanel.add(_accountDetailsPanel, BorderLayout.SOUTH);

    _detailsPanel = new JPanel();
    _detailsPanel.setLayout(new PairLayout(10, 10));
    _accountDetailsPanel.add(_detailsPanel, BorderLayout.CENTER);

    _accountNameLabel = new JLabel();
    _accountNameLabel.setText(StringLocalizer.keyToString("CFGUI_ACCOUNT_NAME_LABEL_KEY"));
    _accountNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _detailsPanel.add(_accountNameLabel);

    _accountNameTextField = new JTextField(_TEXT_FIELD_COLUMNS);
    _accountNameTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _accountNameTextField.addFocusListener(_textFieldFocusAdapter);
    _detailsPanel.add(_accountNameTextField);

    Dimension fieldSize = _accountNameTextField.getPreferredSize();

    _fullNameLabel = new JLabel();
    _fullNameLabel.setText(StringLocalizer.keyToString("CFGUI_FULL_NAME_LABEL_KEY"));
    _fullNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _detailsPanel.add(_fullNameLabel);

    _fullNameTextField = new JTextField(_TEXT_FIELD_COLUMNS);
    _fullNameTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _fullNameTextField.setPreferredSize(fieldSize);
    _fullNameTextField.addFocusListener(_textFieldFocusAdapter);
    _detailsPanel.add(_fullNameTextField);

    _passwordLabel = new JLabel();
    _passwordLabel.setText(StringLocalizer.keyToString("CFGUI_PASSWORD_LABEL_KEY"));
    _passwordLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _detailsPanel.add(_passwordLabel);

    _passwordField = new JPasswordField(_TEXT_FIELD_COLUMNS);
    _passwordField.setHorizontalAlignment(SwingConstants.LEFT);
    _passwordField.setPreferredSize(fieldSize);
    _passwordField.addFocusListener(_textFieldFocusAdapter);
    _detailsPanel.add(_passwordField);

    _confirmPasswordLabel = new JLabel();
    _confirmPasswordLabel.setText(StringLocalizer.keyToString("CFGUI_CONFIRM_PASSWORD_LABEL_KEY"));
    _confirmPasswordLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _detailsPanel.add(_confirmPasswordLabel);

    _confirmPasswordField = new JPasswordField(_TEXT_FIELD_COLUMNS);
    _confirmPasswordField.setHorizontalAlignment(SwingConstants.LEFT);
    _confirmPasswordField.setPreferredSize(fieldSize);
    _confirmPasswordField.addFocusListener(_textFieldFocusAdapter);
    _detailsPanel.add(_confirmPasswordField);

    _accountTypeLabel = new JLabel();
    _accountTypeLabel.setText(StringLocalizer.keyToString("CFGUI_ACCOUNT_TYPE_LABEL_KEY"));
    _accountTypeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _detailsPanel.add(_accountTypeLabel);

    _accountTypeComboBox = new JComboBox();
    _accountTypeComboBox.setPreferredSize(fieldSize);
    // blank first entry
    _accountTypeComboBox.addItem("");
    // get user types that can be added by customer
    _userTypeEnums = UserTypeEnum.getCustomerUserTypeEnums();
    for (UserTypeEnum userTypeEnum : _userTypeEnums)
    {
      _accountTypeComboBox.addItem(userTypeEnum);
    }
    _accountTypeComboBox.setSelectedIndex(0);
    _detailsPanel.add(_accountTypeComboBox);

    _buttonPanel = new JPanel();
    _buttonPanel.setLayout(new FlowLayout());
    _accountDetailsPanel.add(_buttonPanel, BorderLayout.SOUTH);

    _clearAccountButton = new JButton();
    _clearAccountButton.setText(StringLocalizer.keyToString("CFGUI_CLEAR_ACCOUNT_BUTTON_KEY"));
    _clearAccountButton.setToolTipText(StringLocalizer.keyToString("CFGUI_CLEAR_ACCOUNT_BUTTON_TOOLTIP_KEY"));
    _clearAccountButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        clearAccountButton_actionPerformed();
      }
    });
    _buttonPanel.add(_clearAccountButton);

    _newAccountButton = new JButton();
    _newAccountButton.setText(StringLocalizer.keyToString("CFGUI_NEW_ACCOUNT_BUTTON_KEY"));
    _newAccountButton.setToolTipText(StringLocalizer.keyToString("CFGUI_NEW_ACCOUNT_BUTTON_TOOLTIP_KEY"));
    _newAccountButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        newAccountButton_actionPerformed();
      }
    });
    _buttonPanel.add(_newAccountButton);

    _updateAccountButton = new JButton();
    _updateAccountButton.setText(StringLocalizer.keyToString("CFGUI_UPDATE_ACCOUNT_BUTTON_KEY"));
    _updateAccountButton.setToolTipText(StringLocalizer.keyToString("CFGUI_UPDATE_ACCOUNT_BUTTON_TOOLTIP_KEY"));
    _updateAccountButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateAccountButton_actionPerformed();
      }
    });
    _buttonPanel.add(_updateAccountButton);

    _deleteAccountButton = new JButton();
    _deleteAccountButton.setText(StringLocalizer.keyToString("CFGUI_DELETE_ACCOUNT_BUTTON_KEY"));
    _deleteAccountButton.setToolTipText(StringLocalizer.keyToString("CFGUI_DELETE_ACCOUNT_BUTTON_TOOLTIP_KEY"));
    _deleteAccountButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        deleteAccountButton_actionPerformed();
      }
    });
    _buttonPanel.add(_deleteAccountButton);
  }

  /**
   * Creates a list selection listener for the available projects table
   * @author George Booth
   */
  public void setupUserAccountsTableListSelection()
  {
    // add the selection listener for the available projects table
    ListSelectionModel userAccountsTableLSM = _userAccountsTable.getSelectionModel();
    userAccountsTableLSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.getValueIsAdjusting())
        {
          // do nothing
        }
        else if (lsm.isSelectionEmpty())
        {
          clearUserAccountDetails();
        }
        else
        {
          int index = lsm.getAnchorSelectionIndex();
          setUserAccountsTableSelection(index);
          _mainUI.setStatusBarText("");
        }
      }
    });
  }

  /**
   * Called when user accounts table has no selection
   * @author George Booth
   */
  void clearUserAccountDetails()
  {
    _accountNameTextField.setText("");
    _fullNameTextField.setText("");
    _passwordField.setText("");
    _confirmPasswordField.setText("");
    _accountTypeComboBox.setSelectedIndex(0);
    setButtonAndTextFieldStates();
  }

  /**
   * Called when user accounts table has a selection
   * @author George Booth
   */
  void setUserAccountsTableSelection(int row)
  {
    UserAccountInfo userAccount = _userAccountsTableModel.getUserAccount(row);
    _accountNameTextField.setText(userAccount.getAccountName());
    _fullNameTextField.setText(userAccount.getFullName());
    for (UserTypeEnum userTypeEnum : _userTypeEnums)
    {
      if (userTypeEnum.equals(userAccount.getAccountType()))
      {
        _accountTypeComboBox.setSelectedItem(userTypeEnum);
      }
    }
    _passwordField.setText(userAccount.getPassword());
    _confirmPasswordField.setText(userAccount.getPassword());

    setButtonAndTextFieldStates();
  }

  /**
   * @author George Booth
   */
  void setButtonAndTextFieldStates()
  {
    UserAccountInfo selectedUserAccount = null;
    String selectedUserName = null;
    boolean isCustomerAccount = true;
    int selectedRow = _userAccountsTable.getSelectedRow();
    if (selectedRow > -1)
    {
      selectedUserAccount = _userAccountsTableModel.getUserAccount(selectedRow);
      selectedUserName = selectedUserAccount.getAccountName();
      isCustomerAccount = selectedUserAccount.isInternalAccount() == false;
    }

    // Clear Account button
    // Only an admin can clear account data if it is a customer account
    if (isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _clearAccountButton.setEnabled(true);
    }
    else
    {
      _clearAccountButton.setEnabled(false);
    }

    // New Account button
    // Only an admin can create a new account
    if (isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _newAccountButton.setEnabled(true);
    }
    else
    {
      _newAccountButton.setEnabled(false);
    }

    // Update Account button
    // Only an admin or account owner can update a selected  account
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR) ||
        (selectedUserName != null && selectedUserName.equalsIgnoreCase(_currentUserName)))
    {
      _updateAccountButton.setEnabled(selectedUserAccount != null);
    }
    else
    {
      _updateAccountButton.setEnabled(false);
    }

    // Delete Account button
    // Only an admin can delete a customer account but not themselves
    if (isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR) &&
        (selectedUserName != null && selectedUserName.equalsIgnoreCase(_currentUserName) == false))
    {
      _deleteAccountButton.setEnabled(true);
    }
    else
    {
      _deleteAccountButton.setEnabled(false);
    }

    // Account Name text field
    // Only an admin can change the account name if it is a customer account
    if (isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _accountNameTextField.setEnabled(true);
    }
    else
    {
      _accountNameTextField.setEnabled(false);
    }

    // Full Name text field
    // An admin or account owner can change the full name if it is a customer account
    if ((isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR) ||
        (isCustomerAccount && selectedUserName != null && selectedUserName.equalsIgnoreCase(_currentUserName))))
    {
      _fullNameTextField.setEnabled(true);
    }
    else
    {
      _fullNameTextField.setEnabled(false);
    }

    // Password and Confirm Password fields
    // An admin or account owner can change password
    if (_currentUserType.equals(UserTypeEnum.ADMINISTRATOR) ||
        (selectedUserName != null && selectedUserName.equalsIgnoreCase(_currentUserName)))
    {
      _passwordField.setEnabled(true);
      _confirmPasswordField.setEnabled(true);
    }
    else
    {
      _passwordField.setEnabled(false);
      _confirmPasswordField.setEnabled(false);
    }

    // Account Type text field
    // Only an admin can change the account type if it is a customer account
    if (isCustomerAccount && _currentUserType.equals(UserTypeEnum.ADMINISTRATOR))
    {
      _accountTypeComboBox.setEnabled(true);
    }
    else
    {
      _accountTypeComboBox.setEnabled(false);
    }
  }

  /*
   * @author George Booth
   */
  UserAccountInfo getAccountDetails(boolean newAccount)
  {
    UserAccountInfo userAccount = new UserAccountInfo();

    String accountName = getAccountName(newAccount);
    if (accountName.length() > 0)
    {
      userAccount.setAccountName(accountName);
    }
    else
    {
      return userAccount;
    }

    String fullName = getFullName();
    if (fullName.length() > 0)
    {
      userAccount.setFullName(fullName);
    }
    else
    {
      return userAccount;
    }

    if (isValidPassword())
    {
      userAccount.setPassword(getPassword());
    }
    else
    {
      return userAccount;
    }

    String accountTypeName = getAccountType();
    if (accountTypeName.length() > 0)
    {
      userAccount.setAccountType(UserTypeEnum.getUserTypeEnum(accountTypeName));
    }
    return userAccount;
  }

  /*
   * @author George Booth
   */
  void clearAccountButton_actionPerformed()
  {
    clearUserAccountDetails();
    _mainUI.setStatusBarText("");
  }

  /*
   * @author George Booth
   */
  void newAccountButton_actionPerformed()
  {
    UserAccountInfo userAccount = getAccountDetails(_NEW_ACCOUNT);
    if (userAccount.isValidAccount())
    {
      try
      {
        ConfigUserAccountsAddCommand addUserAccountCommand = new ConfigUserAccountsAddCommand(userAccount);
        _commandManager.execute(addUserAccountCommand);
      }
      catch (XrayTesterException ex)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                      ex,
                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                      true);
      }
    }
  }

  /**
   * @author George Booth
   */
  void updateAccountButton_actionPerformed()
  {
    int selectedRow = _userAccountsTable.getSelectedRow();
    UserAccountInfo oldAccount = _userAccountsTableModel.getUserAccount(selectedRow);
    String oldAccountName = oldAccount.getAccountName();
    UserAccountInfo updatedAccount = getAccountDetails(_UPDATE_ACCOUNT);
    if (updatedAccount.isValidAccount())
    {
      String updatedAccountName = updatedAccount.getAccountName();
      // if the user name is changed, it must be unique
      if (oldAccountName.equalsIgnoreCase(updatedAccountName) == false)
      {
        if (_userAccountsTableModel.getRowForUserName(updatedAccountName) != -1)
        {
          errorMessage(StringLocalizer.keyToString("CFGUI_DUPLICATE_USER_NAME_MESSAGE_KEY"));
          return;
        }
      }
      // only update if some data has changed
      if (updatedAccount.getAccountName().equals(oldAccount.getAccountName()) == false ||
          updatedAccount.getFullName().equals(oldAccount.getFullName()) == false ||
          updatedAccount.getPassword().equals(oldAccount.getPassword()) == false ||
          updatedAccount.getAccountType().equals(oldAccount.getAccountType()) == false)
      {
        try
        {
          ConfigUserAccountsUpdateCommand updateUserAccountCommand =
              new ConfigUserAccountsUpdateCommand(oldAccount, updatedAccount);
          _commandManager.execute(updateUserAccountCommand);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex,
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  void deleteAccountButton_actionPerformed()
  {
    int row = _userAccountsTable.getSelectedRow();
    if (row > -1)
    {
      if ((JOptionPane.showConfirmDialog(
          this,
          StringLocalizer.keyToString(new LocalizedString("CFGUI_DELETE_ACCOUNT_CONFIRM_KEY", null)),
          StringLocalizer.keyToString("CFGUI_DELETE_ACCOUNT_CONFIRM_TITLE_KEY"),
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION))
      {
        UserAccountInfo userAccount = _userAccountsTableModel.getUserAccount(row);
        try
        {
          ConfigUserAccountsDeleteCommand deleteUserAccountCommand = new ConfigUserAccountsDeleteCommand(userAccount);
          _commandManager.execute(deleteUserAccountCommand);
        }
        catch (XrayTesterException ex)
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                        ex,
                                        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                        true);
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  String getAccountName(boolean newAccount)
  {
    String accountName = _accountNameTextField.getText();
    if (accountName.length() == 0)
    {
      errorMessage(StringLocalizer.keyToString("CFGUI_ACCOUNT_NAME_MUST_BE_ENTERED_MESSAGE_KEY"));
      return "";
    }
    if (newAccount)
    {
      // account name must be unique
      if (_userAccountsTableModel.getRowForUserName(accountName) != -1)
      {
        errorMessage(StringLocalizer.keyToString("CFGUI_DUPLICATE_USER_NAME_MESSAGE_KEY"));
        return "";
      }
    }
    return accountName;
  }

  /**
   * @author George Booth
   */
  String getAccountType()
  {
    String accountTypeName = "";
    if (_accountTypeComboBox.getSelectedIndex() > 0)
    {
      UserTypeEnum userTypeEnum = (UserTypeEnum)_accountTypeComboBox.getSelectedItem();
      accountTypeName = userTypeEnum.getName();
    }
    if (accountTypeName.length() == 0)
    {
      errorMessage(StringLocalizer.keyToString("CFGUI_ACCOUNT_TYPE_MUST_BE_SELECTED_MESSAGE_KEY"));
    }
    return accountTypeName;
  }

  /**
   * @author George Booth
   */
  String getFullName()
  {
    String fullName = _fullNameTextField.getText();
    if (fullName.length() == 0)
    {
      errorMessage(StringLocalizer.keyToString("CFGUI_FULL_NAME_MUST_BE_ENTERED_MESSAGE_KEY"));
      return "";
    }
    return fullName;
  }

  /**
   * @author George Booth
   */
  boolean isValidPassword()
  {
    char[] password1 = _passwordField.getPassword();
    char[] password2 = _confirmPasswordField.getPassword();
    if (password1.length != password2.length)
    {
      errorMessage(StringLocalizer.keyToString("CFGUI_BAD_PASSWORD_MESSAGE_KEY"));
      return false;
    }
    // prompt the user if they entered a blank password
    if (password1.length == 0)
    {
      if ((JOptionPane.showConfirmDialog(
          this,
          StringLocalizer.keyToString(new LocalizedString("CFGUI_BLANK_PASSWORD_MESSAGE_KEY", null)),
          StringLocalizer.keyToString("CFGUI_BLANK_PASSWORD_MESSAGE_TITLE_KEY"),
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION))
      return false;
    }
    // only a fool would use a password longer than 24 characters
    if (password1.length > 24)
    {
      errorMessage(StringLocalizer.keyToString("CFGUI_PASSWORD_TOO_LONG_MESSAGE_KEY"));
      return false;
    }
    for (int i = 0; i < password1.length; i++)
    {
      if (password1[i] != password2[i])
      {
        errorMessage(StringLocalizer.keyToString("CFGUI_BAD_PASSWORD_MESSAGE_KEY"));
        return false;
      }
    }
    return true;
  }

  /**
   * @author George Booth
   */
  String getPassword()
  {
    char[] password1 = _passwordField.getPassword();
    return new String(password1);
  }

  /**
   * @author George Booth
   */
  void errorMessage(String message)
  {
    JOptionPane.showMessageDialog(
        _mainUI,
        message,
        StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
        JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof UserAccountsObservable)
        {
          Assert.expect(object != null);
          UserAccountsEvent userAccountsEvent = (UserAccountsEvent)object;
          UserAccountsEventEnum userAccountsEventEnum = ((UserAccountsEvent)object).getUserAccountsEventEnum();
          if (userAccountsEventEnum instanceof UserAccountsEventEnum)
          {
            if (userAccountsEventEnum.equals(UserAccountsEventEnum.USER_ACCOUNT_ADDED))
            {
              _userAccountsTableModel.populateWithLoginAccountData();
              UserAccountInfo newAccount = (UserAccountInfo)userAccountsEvent.getSource();
              String newAccountName = newAccount.getAccountName();
              int newRow = _userAccountsTableModel.getRowForUserName(newAccountName);
              // select the new account
              _userAccountsTable.setRowSelectionInterval(newRow, newRow);
              String message = StringLocalizer.keyToString(
                  new LocalizedString("CFGUI_NEW_ACCOUNT_BUTTON_STATUS_MESSAGE_KEY",
                                      new Object[] {newAccountName})); ;
              _mainUI.setStatusBarText(message);
            }
            else if (userAccountsEventEnum.equals(UserAccountsEventEnum.USER_ACCOUNT_UPDATED))
            {
              _userAccountsTableModel.populateWithLoginAccountData();
              List<UserAccountInfo> userAccountsList = (List<UserAccountInfo>)userAccountsEvent.getSource();
              Assert.expect(userAccountsList.size() ==2);
              UserAccountInfo oldAccount = userAccountsList.get(0);
              String oldAccountName = oldAccount.getAccountName();
              UserAccountInfo updatedAccount = userAccountsList.get(1);
              String updatedAccountName = updatedAccount.getAccountName();

              // reselect the updated account
              int selectedRow = _userAccountsTableModel.getRowForUserName(updatedAccountName);
              _userAccountsTable.setRowSelectionInterval(selectedRow, selectedRow);
              String message = StringLocalizer.keyToString(
                  new LocalizedString("CFGUI_UPDATE_ACCOUNT_BUTTON_STATUS_MESSAGE_KEY",
                                      new Object[] {updatedAccountName})); ;
              _mainUI.setStatusBarText(message);

              // update the login manager if the current login name was changed
              if (oldAccountName.equalsIgnoreCase(_currentUserName))
              {
                _loginManager.setLoginInfo(updatedAccountName);
                _currentUserName = updatedAccountName;
              }
              setButtonAndTextFieldStates();
            }
            else if (userAccountsEventEnum.equals(UserAccountsEventEnum.USER_ACCOUNT_DELETED))
            {
              _userAccountsTableModel.populateWithLoginAccountData();
              UserAccountInfo deletedAccount = (UserAccountInfo)userAccountsEvent.getSource();
              _userAccountsTable.clearSelection();
              setButtonAndTextFieldStates();
              String message = StringLocalizer.keyToString(
                  new LocalizedString("CFGUI_DELETE_ACCOUNT_BUTTON_STATUS_MESSAGE_KEY",
                                      new Object[] {deletedAccount.getAccountName()})); ;
              _mainUI.setStatusBarText(message);
            }
          }
        }
        else
        {
          Assert.expect(false, "No update code for observable: " + observable.getClass().getName());
        }
      }
    });
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("CustomerOptionsLoginTabPanel().start()");

    _userAccountsObservable.addObserver(this);

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText(" ");

    _currentUserName = _loginManager.getUserNameFromLogIn();
    _currentUserType = _loginManager.getUserTypeFromLogIn();
    _userAccountsTableModel.populateWithLoginAccountData();
    setButtonAndTextFieldStates();
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("CustomerOptionsLoginTabPanel().finish()");

    _userAccountsObservable.deleteObserver(this);
  }

}
