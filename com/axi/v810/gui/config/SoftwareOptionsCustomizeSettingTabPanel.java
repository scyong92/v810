/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * XCR-3436
 *
 * @author weng-jian.eoh
 */
public class SoftwareOptionsCustomizeSettingTabPanel extends AbstractTaskPanel implements Observer
{

  private Config _config = Config.getInstance();
  private boolean _inUse = false;

  private JPanel _centerPanel;
  private JCheckBox _customizeUICheckBox;
  private JCheckBox _showVersionCheckBox;
  private JCheckBox _showNumberingCheckBox;
  private JCheckBox _showCustomerNameCheckBox;
  private JCheckBox _showLastModifyDateCheckBox;
  private JCheckBox _showVariationCheckBox;
  private JCheckBox _showSystemTypeCheckBox;
  private JPanel _customizeUIMainPanel;
  private JPanel _customizeUIUserInputPanel;
  private JPanel _customizeUIUserEastPanel;
  private JPanel _customizeUIUserCenterPanel;
  private JPanel _customizeUIUserWestPanel;

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @param envPanel
   */
  public SoftwareOptionsCustomizeSettingTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    jbInit();

  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @throws Exception
   */
  private void jbInit() 
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    _centerPanel = new JPanel();
    _centerPanel.setLayout(new VerticalFlowLayout());

    add(_centerPanel, BorderLayout.CENTER);

    _customizeUIMainPanel = new JPanel(new BorderLayout());
    _customizeUIUserInputPanel = new JPanel(new BorderLayout());
    _customizeUIUserEastPanel = new JPanel(new VerticalFlowLayout());
    _customizeUIUserCenterPanel = new JPanel(new VerticalFlowLayout());
    _customizeUIUserWestPanel = new JPanel(new VerticalFlowLayout());

    _customizeUICheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_HOME_PANEL_COLUMN_LABEL_KEY"));
    _showNumberingCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_NUMBERING_KEY"));
    _showVariationCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_VARIATIAON_KEY"));
    _showCustomerNameCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_CUSTOMER_NAME_KEY"));
    _showVersionCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_VERSION_KEY"));
    _showLastModifyDateCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_LAST_MODIFIED_DATE_KEY"));
    _showSystemTypeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_CUSTOMIZE_SETTING_SYSTEM_TYPE_KEY"));
    _customizeUICheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showVariationCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showNumberingCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showLastModifyDateCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showCustomerNameCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showSystemTypeCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _showVersionCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
    _customizeUIUserInputPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    _customizeUIUserWestPanel.add(_showNumberingCheckBox);
    _customizeUIUserWestPanel.add(_showVersionCheckBox);
    _customizeUIUserCenterPanel.add(_showVariationCheckBox);
    _customizeUIUserCenterPanel.add(_showLastModifyDateCheckBox);
    _customizeUIUserEastPanel.add(_showCustomerNameCheckBox);
    _customizeUIUserEastPanel.add(_showSystemTypeCheckBox);
    _customizeUIUserInputPanel.add(_customizeUIUserEastPanel, BorderLayout.EAST);
    _customizeUIUserInputPanel.add(_customizeUIUserCenterPanel, BorderLayout.CENTER);
    _customizeUIUserInputPanel.add(_customizeUIUserWestPanel, BorderLayout.WEST);
    _customizeUIMainPanel.add(_customizeUICheckBox, BorderLayout.NORTH);
    _customizeUIMainPanel.add(_customizeUIUserInputPanel, BorderLayout.WEST);

    Border CustomizeMainTitledUIBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
      StringLocalizer.keyToString("CFGUI_CUSTOMIZE_UI_SETTING_TAB_LABEL_KEY"));
    Border CustomizeMainTitledUIBorder = BorderFactory.createCompoundBorder(CustomizeMainTitledUIBorder1, BorderFactory.createEmptyBorder(0, 15, 5, 15));

    _customizeUIMainPanel.setBorder(CustomizeMainTitledUIBorder);

    _centerPanel.add(_customizeUIMainPanel);

    _customizeUICheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MainUIChecustomizeckBox_actionListener();
      }
    });

    _showVersionCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideVersionCheckBox_actionListener();
      }
    });

    _showVariationCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideVariationCheckBox_actionListener();
      }
    });

    _showSystemTypeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideSystemTypeCheckBox_actionListener();
      }
    });

    _showNumberingCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideNumberingCheckBox_actionListener();
      }
    });

    _showLastModifyDateCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideLastModifyDateCheckBox_actionListener();
      }
    });

    _showCustomerNameCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        customizeMainUIHideCustomerNameCheckBox_actionListener();
      }
    });
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @param enabled
   */
  private void setEnabledCustomiseUI(boolean enabled)
  {
    _showVariationCheckBox.setEnabled(enabled);
    _showNumberingCheckBox.setEnabled(enabled);
    _showLastModifyDateCheckBox.setEnabled(enabled);
    _showCustomerNameCheckBox.setEnabled(enabled);
    _showSystemTypeCheckBox.setEnabled(enabled);
    _showVersionCheckBox.setEnabled(enabled);
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void MainUIChecustomizeckBox_actionListener()
  {
    setEnabledCustomiseUI(_customizeUICheckBox.isSelected());
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI, _customizeUICheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideVersionCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION, _showVersionCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideVariationCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VARIATION, _showVariationCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideSystemTypeCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE, _showSystemTypeCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideLastModifyDateCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE, _showLastModifyDateCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideNumberingCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING, _showNumberingCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void customizeMainUIHideCustomerNameCheckBox_actionListener()
  {
    try
    {
      _config.setValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME, _showCustomerNameCheckBox.isSelected());
    }
    catch (DatastoreException ex)
    {

    }
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @return
   */
  public boolean isReadyToFinish()
  {
    return _inUse == false;
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  public void start()
  {
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");
    ConfigObservable.getInstance().addObserver(this);
    updateDataOnScreen();
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   */
  private void updateDataOnScreen()
  {

    _showVariationCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VARIATION));
    _showNumberingCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_NUMBERING));
    _showLastModifyDateCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_MODIFIED_DATE));
    _showCustomerNameCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_CUSTOMER_NAME));
    _showSystemTypeCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMISE_UI_SHOW_SYSTEMTYPE));
    _showVersionCheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_UI_SHOW_VERSION));
    _customizeUICheckBox.setSelected(_config.getBooleanValue(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI));
    setEnabledCustomiseUI(_customizeUICheckBox.isSelected());
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @param observable
   * @param argument
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum) argument;
            if (configEnum instanceof UISettingCustomizationConfigEnum)
            {
              UISettingCustomizationConfigEnum customizeSettingConfigEnum = (UISettingCustomizationConfigEnum) configEnum;
              if (customizeSettingConfigEnum.equals(UISettingCustomizationConfigEnum.CUSTOMIZE_MAIN_UI))
              {
                updateDataOnScreen();
              }
            }
          }
        }
      }
    });
  }
}
