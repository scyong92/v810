package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.testDev.UndoState;
import com.axi.v810.gui.testExec.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Jack Hwee
 */
public class ProductionOptionsFalseCallTriggeringTabPanel extends AbstractTaskPanel implements Observer
{
  private java.util.List<String> _bcrConfigurationNamesList;
  private BarcodeReaderManager _bcrManager;
  private String _currentSelectedConfig;
  private String _defaultNumScanners;
  private String _defaultErrorMessage = "";
  private boolean _bcrConfigurationIsDirty = false;
  private boolean _savingConfigurationInProgress = false;

  // command manager for undo functionality
  private com.axi.v810.gui.undo.CommandManager _commandManager =
      com.axi.v810.gui.undo.CommandManager.getConfigInstance();

  // observables to register with
  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  // GUI elements
  private JPanel _centerPanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private JCheckBox _enableFalseCallMonitoringCheckbox = new JCheckBox();
  private JCheckBox _enforceAdministratorLoginCheckbox = new JCheckBox();
  private JCheckBox _autoDeleteProductionResultsCheckbox = new JCheckBox();
  private JPanel _configurationPanel = new JPanel();
  private VerticalFlowLayout _configurationPanelVerticalFlowLayout = new VerticalFlowLayout();
  private JPanel _configurationTriggeringSettingPanel = new JPanel();
  private JPanel _configurationSelectPanel = new JPanel();
  private JLabel _configurationSetConsecutiveCountLabel = new JLabel();
  private JLabel _configurationSetTotalInspectionLabel = new JLabel();
  private JLabel _configurationSetTotalSampleTriggeringLabel = new JLabel();
  private JLabel _configurationSetMaximumErrorTriggerLabel = new JLabel();
  private JSplitPane _settingPanelSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
  private JLabel _numScannersLabel = new JLabel();
  private NumericTextField _numScannersTextField = new NumericTextField(null, 2);
  private JScrollPane _scannerCodesScrollPane = new JScrollPane();
  private BarCodeScannerCodesTable _scannerCodesTable = new BarCodeScannerCodesTable();
  private BarCodeScannerCodesTableModel _scannerCodesTableModel;
  private JLabel _serialPortNumberLabel = new JLabel();
  private JComboBox _serialPortNameCombobox = new JComboBox();
  private JLabel _bitsPerSecondLabel = new JLabel();
  private JComboBox _bitsPerSecondCombobox = new JComboBox();
  private JLabel _dataBitsLabel = new JLabel();
  private JComboBox _dataBitsCombobox = new JComboBox();
  private JLabel _parityLabel = new JLabel();
  private JComboBox _parityCombobox = new JComboBox();
  private JLabel _stopBitsLabel = new JLabel();
  private JComboBox _stopBitsCombobox = new JComboBox();
  private PairLayout _messageSetupPanelPairLayout = new PairLayout();
  private JLabel _noReadErrorLabel = new JLabel();
  private JTextField _noReadErrorTextField = new JTextField(15);
  private JLabel _badSymbolErrorLabel = new JLabel();
  private JTextField _badSymbolErrorTextField = new JTextField(15);
  private JLabel _noLabelErrorLabel = new JLabel();
  private JTextField _noLabelErrorTextField = new JTextField(15);
  private JPanel _brcTestPanel = new JPanel();
  private BorderLayout _brcTestPanelBorderLayout = new BorderLayout();
  private JScrollPane _brcTestScrollPane = new JScrollPane();
  private FalseCallMonitoringDynamicCallNumberTable _falseCallMonitoringDynamicCallNumberTable = new FalseCallMonitoringDynamicCallNumberTable();
  private FalseCallMonitoringDynamicCallNumberTableModel _falseCallMonitoringDynamicCallNumberTableModel = new FalseCallMonitoringDynamicCallNumberTableModel();
  private JPanel _testActionWrapperPanel = new JPanel();
  private FlowLayout _testActionWrapperPanelFlowLayout = new FlowLayout();
  private JButton _testButton = new JButton();
  private JLabel _selectBcrConfigurationToUseLabel = new JLabel();
  private JComboBox _bcrConfigurationToUseComboBox = new JComboBox();
  private ActionListener _configurationToUseComboboxActionListener;
  
  private static final int _MIN_CONSECUTIVE_COUNT = 1;
  private DecimalFormat _consecutiveCountFormat = new DecimalFormat();
  private NumericRangePlainDocument _consecutiveCountFormatDataDoc = new NumericRangePlainDocument();
  private DecimalFormat _totalInspectionFormat = new DecimalFormat();
  private NumericRangePlainDocument _totalInspectionFormatDataDoc = new NumericRangePlainDocument();
  private DecimalFormat _totalSampleTriggeringFormat = new DecimalFormat();
  private NumericRangePlainDocument _totalSampleTriggeringFormatDataDoc = new NumericRangePlainDocument();
  private DecimalFormat _maximumErrorTriggerFormat = new DecimalFormat();
  private NumericRangePlainDocument _maximumErrorTriggerFormatDataDoc = new NumericRangePlainDocument();
  private NumericTextField _consecutiveCountTextField = new NumericTextField();
  private NumericTextField _totalInspectionTextField = new NumericTextField();
  private NumericTextField _totalSampleTriggeringTextField = new NumericTextField();
  private NumericTextField _maximumErrorTriggerTextField = new NumericTextField();
  private FalseCallMonitoring _falseCallMonitoring = FalseCallMonitoring.getInstance();
  private JButton _addRowButton = new JButton();
  private JButton _deleteRowButton = new JButton();
  private AddDynamicCallNumberDialog _addDynamicCallNumberDialog;
  private JButton _writeRemarkButton = new JButton();

  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement  
  private JCheckBox _enforceWriteRemarkCheckbox = new JCheckBox();
  private ButtonGroup _defectThresholdTypeButtonGroup;
  private JRadioButton _defectThresholdTypePinsNumberRadioButton;
  private JRadioButton _defectThresholdTypePPMPinsRadioButton;
  private JLabel _defectThresholdTypeLabel = new JLabel();
  
  /**
  * Useful ONLY for the JBuilder designer.  Not for use with normal code.
  * @author Laura Cormos
  */
 private ProductionOptionsFalseCallTriggeringTabPanel()
 {
   Assert.expect(java.beans.Beans.isDesignTime());
   try
   {
     jbInit();
   }
   catch (Exception exception)
   {
     exception.printStackTrace();
   }
 }

  /**
   * @author Laura Cormos
   */
  public ProductionOptionsFalseCallTriggeringTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    _envPanel = envPanel;
    try
    {
      jbInit();
      _falseCallMonitoring = FalseCallMonitoring.getInstance();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Jack Hwee
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    // set up panel GUI components
    add(_centerPanel, BorderLayout.CENTER);
    
    _writeRemarkButton.setText(StringLocalizer.keyToString("CFGUI_FCT_WRITE_REMARK_TITLE_KEY"));
    _writeRemarkButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        writeRemarkButton_actionPerformed();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _defectThresholdTypeLabel = new JLabel(StringLocalizer.keyToString("CFGUI_DEFECT_THRESHOLD_TYPE_KEY") + "  ");
    JPanel defectThresholdTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    _defectThresholdTypePinsNumberRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_DEFECT_CALL_TYPE_KEY") + " ");
    _defectThresholdTypePPMPinsRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_DEFECT_PPM_PIN_TYPE_KEY"));    
    _defectThresholdTypeButtonGroup = new ButtonGroup();
    _defectThresholdTypeButtonGroup.add(_defectThresholdTypePinsNumberRadioButton);
    _defectThresholdTypeButtonGroup.add(_defectThresholdTypePPMPinsRadioButton);
    defectThresholdTypePanel.add(_defectThresholdTypeLabel);
    defectThresholdTypePanel.add(_defectThresholdTypePinsNumberRadioButton);
    defectThresholdTypePanel.add(_defectThresholdTypePPMPinsRadioButton);
    
    JPanel falseCallMonitoringPanel = new JPanel();
    FlowLayout mainPanelFlowLayout = new FlowLayout();
    mainPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    //mainPanelFlowLayout.setHgap(10);
    falseCallMonitoringPanel.setLayout(mainPanelFlowLayout);
    falseCallMonitoringPanel.add(_enableFalseCallMonitoringCheckbox, null);
    falseCallMonitoringPanel.add(_writeRemarkButton, null);
    
    JPanel falseCallPanel = new JPanel();
    falseCallPanel.setLayout(new BorderLayout());
    falseCallPanel.add(falseCallMonitoringPanel, BorderLayout.NORTH);
    falseCallPanel.add(defectThresholdTypePanel, BorderLayout.CENTER);
    
    //BorderLayout fpyPanelBorderLayout = new BorderLayout();
    FlowLayout fpyPanelFlowLayout = new FlowLayout();
    fpyPanelFlowLayout.setAlignment(FlowLayout.LEFT);
    fpyPanelFlowLayout.setHgap(10);
    JPanel fpyPanel = new JPanel(new BorderLayout(0, -5)); //JPanel preInspectionPanel = new JPanel(new BorderLayout(0, -5));

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    _centerPanel.add(falseCallPanel, BorderLayout.NORTH);
    _centerPanel.add(_configurationPanel, BorderLayout.CENTER);
    //_centerPanel.add(_selectBcrConfigurationToUsePanel, BorderLayout.SOUTH);

    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _defectThresholdTypePinsNumberRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defectTypeRadioButtonGroup_actionListener();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _defectThresholdTypePPMPinsRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        defectTypeRadioButtonGroup_actionListener();
      }
    });

    _enableFalseCallMonitoringCheckbox.setText(StringLocalizer.keyToString("CFGUI_FCT_ENABLE_FALSE_CALL_MONITORING_TITLE_KEY"));
    _enableFalseCallMonitoringCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableFalseCallMonitoringCheckbox_actionPerformed(e);
      }
    });

    _enforceAdministratorLoginCheckbox.setText(StringLocalizer.keyToString("CFGUI_FCT_PROMPT_LOGIN_KEY"));
    _enforceAdministratorLoginCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enforceAdministratorLoginCheckbox_actionPerformed(e);
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _enforceWriteRemarkCheckbox.setText(StringLocalizer.keyToString("CFGUI_FCT_ENFORE_WRITE_REMARK_KEY"));
    _enforceWriteRemarkCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enforceWriteRemarkCheckbox_actionPerformed(e);
      }
    });

    _autoDeleteProductionResultsCheckbox.setText(StringLocalizer.keyToString("CFGUI_FCT_DELETE_INDICTMENT_KEY"));
    _autoDeleteProductionResultsCheckbox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        autoDeleteProductionResultsCheckbox_actionPerformed(e);
      }
    });
    
    _configurationPanel.setLayout(_configurationPanelVerticalFlowLayout);
    _configurationPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_BCR_CONFIGURATION_PANEL_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _configurationPanel.add(_configurationTriggeringSettingPanel, BorderLayout.NORTH);
    _configurationTriggeringSettingPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_FCT_TRIGGERING_SETTING_TITLE_KEY")),
    BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    
    _configurationTriggeringSettingPanel.setLayout(new VerticalFlowLayout());
    _configurationTriggeringSettingPanel.add(_enforceWriteRemarkCheckbox);
    _configurationTriggeringSettingPanel.add(_enforceAdministratorLoginCheckbox);
    _configurationTriggeringSettingPanel.add(_autoDeleteProductionResultsCheckbox);
    
    _configurationPanel.add(_configurationSelectPanel, BorderLayout.CENTER);
    _configurationSelectPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_FCT_MONITORING_SETTING_TITLE_KEY")),
    BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    
   
    JPanel thresholdPanel = new JPanel();
    _configurationPanel.add(_settingPanelSplitPane, BorderLayout.SOUTH);

    _configurationSelectPanel.setLayout(new BorderLayout());
    _configurationSelectPanel.add(thresholdPanel,  BorderLayout.WEST);
   
    thresholdPanel.setLayout(_messageSetupPanelPairLayout);
    thresholdPanel.add(_configurationSetConsecutiveCountLabel);
    thresholdPanel.add(_consecutiveCountTextField);
    thresholdPanel.add(_configurationSetTotalInspectionLabel);
    thresholdPanel.add(_totalInspectionTextField);
    thresholdPanel.add(_configurationSetTotalSampleTriggeringLabel);
    thresholdPanel.add(_totalSampleTriggeringTextField);
    thresholdPanel.add(_configurationSetMaximumErrorTriggerLabel);
    thresholdPanel.add(_maximumErrorTriggerTextField);
  
    _configurationSetConsecutiveCountLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_CONSECUTIVE_COUNT_TITLE_KEY"));
    _configurationSetTotalInspectionLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_TOTAL_INSPECTION_TITLE_KEY"));
    _configurationSetTotalSampleTriggeringLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_TOTAL_SAMPLE_TRIGGERING_TITLE_KEY"));
    _configurationSetMaximumErrorTriggerLabel.setText(StringLocalizer.keyToString("CFGUI_FCT_MAXIMUM_ERROR_TRIGGER_TITLE_KEY"));

    _consecutiveCountFormat.setParseIntegerOnly(true);
    _consecutiveCountFormatDataDoc.setRange(new IntegerRange(_MIN_CONSECUTIVE_COUNT, Integer.MAX_VALUE));
    _consecutiveCountTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _consecutiveCountTextField.setDocument(_consecutiveCountFormatDataDoc);
    _consecutiveCountTextField.setFormat(_consecutiveCountFormat);
    _consecutiveCountTextField.setColumns(4);
   
    _consecutiveCountTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        consecutiveCountTextField();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _consecutiveCountTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        consecutiveCountTextField();
      }
    });
    
    _consecutiveCountTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent ke)
      {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          _consecutiveCountTextField.selectAll();
      }
    });
    
    _totalInspectionFormat.setParseIntegerOnly(true);
    _totalInspectionFormatDataDoc.setRange(new IntegerRange(_MIN_CONSECUTIVE_COUNT, Integer.MAX_VALUE));
    _totalInspectionTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _totalInspectionTextField.setDocument(_totalInspectionFormatDataDoc);
    _totalInspectionTextField.setFormat(_totalInspectionFormat);
    _totalInspectionTextField.setColumns(4);
    
    _totalInspectionTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        totalInspectionTextField();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _totalInspectionTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        totalInspectionTextField();
      }
    });
    
    _totalInspectionTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent ke)
      {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          _totalInspectionTextField.selectAll();
      }
    });
    
    _totalSampleTriggeringFormat.setParseIntegerOnly(true);
    _totalSampleTriggeringFormatDataDoc.setRange(new IntegerRange(_MIN_CONSECUTIVE_COUNT, Integer.MAX_VALUE));
    _totalSampleTriggeringTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _totalSampleTriggeringTextField.setDocument(_totalSampleTriggeringFormatDataDoc);
    _totalSampleTriggeringTextField.setFormat(_totalSampleTriggeringFormat);
    _totalSampleTriggeringTextField.setColumns(4);
    
    _totalSampleTriggeringTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        totalSampleTriggeringTextField();
      }
    });
    
    _totalSampleTriggeringTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent ke)
      {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          _totalSampleTriggeringTextField.selectAll();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _totalSampleTriggeringTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        totalSampleTriggeringTextField();
      }
    });
    
    _maximumErrorTriggerFormat.setParseIntegerOnly(true);
    _maximumErrorTriggerFormatDataDoc.setRange(new IntegerRange(_MIN_CONSECUTIVE_COUNT, Integer.MAX_VALUE));
    _maximumErrorTriggerTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _maximumErrorTriggerTextField.setDocument(_maximumErrorTriggerFormatDataDoc);
    _maximumErrorTriggerTextField.setFormat(_maximumErrorTriggerFormat);
    _maximumErrorTriggerTextField.setColumns(4);
    
    _maximumErrorTriggerTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        maximumErrorTriggerTextField();
      }
    });
    
    _maximumErrorTriggerTextField.addKeyListener(new java.awt.event.KeyAdapter()
    {
      public void keyPressed(KeyEvent ke)
      {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER)
          _maximumErrorTriggerTextField.selectAll();
      }
    });
    
    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    _maximumErrorTriggerTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        maximumErrorTriggerTextField();
      }
    });
    
    _settingPanelSplitPane.setBorder(null);
    _settingPanelSplitPane.add(_configurationSelectPanel, JSplitPane.TOP);
    _settingPanelSplitPane.add(_brcTestPanel, JSplitPane.BOTTOM);

    _brcTestPanel.setLayout(_brcTestPanelBorderLayout);
    _brcTestPanel.setBorder(BorderFactory.createCompoundBorder(
      new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)),
                       StringLocalizer.keyToString("CFGUI_FCT_DYNAMIC_CALL_NUMBER_TITLE_KEY")),
      BorderFactory.createEmptyBorder(0, 2, 2, 2)));
    _brcTestPanel.add(_brcTestScrollPane, BorderLayout.WEST);
    _brcTestPanel.add(_testActionWrapperPanel, BorderLayout.EAST);
       
    _addRowButton = new JButton(StringLocalizer.keyToString("CFGUI_FCT_ADD_ROW_TITLE_KEY"));
    
    _addRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         addRowButton_actionPerformed();
      }
    });
    
    _deleteRowButton = new JButton(StringLocalizer.keyToString("CFGUI_FCT_DELETE_ROW_TITLE_KEY"));
    
    _deleteRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         deleteButton_actionPerformed();
      }
    });
    
     JPanel _editButtonPanel = new JPanel();
     FlowLayout _editButtonPanelFlowLayout = new FlowLayout();
     _editButtonPanelFlowLayout.setAlignment(FlowLayout.LEFT);
     _editButtonPanelFlowLayout.setHgap(10);
     _editButtonPanel.setLayout(_editButtonPanelFlowLayout);
     _editButtonPanel.add(_addRowButton, null);
     _editButtonPanel.add(_deleteRowButton, null);
     
     _brcTestPanel.add(_editButtonPanel, BorderLayout.SOUTH);
    
    _falseCallMonitoringDynamicCallNumberTable.getTableHeader().setReorderingAllowed(false);
    _falseCallMonitoringDynamicCallNumberTable.setModel(_falseCallMonitoringDynamicCallNumberTableModel);
    _brcTestScrollPane.getViewport().add(_falseCallMonitoringDynamicCallNumberTable);
    _falseCallMonitoringDynamicCallNumberTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    _testActionWrapperPanel.setLayout(_testActionWrapperPanelFlowLayout);
    _testActionWrapperPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
 
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ProductionOptionsBarCodeReaderTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();

    _mainUI.setStatusBarText("");

    populateWithConfigData();

    _commandManager.addObserver(this);
    _configObservable.addObserver(this);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    if (_scannerCodesTable.isEditing())
      _scannerCodesTable.getCellEditor().cancelCellEditing();
    if (_falseCallMonitoringDynamicCallNumberTable.isEditing())
      _falseCallMonitoringDynamicCallNumberTable.getCellEditor().cancelCellEditing();

    return saveConfigurationIfNecessary();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    _commandManager.deleteObserver(this);
    _configObservable.deleteObserver(this);

//    System.out.println("ProductionOptionsBarCodeReaderTabPanel().finish()");
  }


  /**
   * @author Jack Hwee
   */
  private void populateWithConfigData()
  {
    // set the enable check box based on the config setting
    // before we can query the bcrmanager about any static configuration values we need to load the configuration
    // pointed to by the BARCODE_READER_CONFIGURATION_NAME_TO_USE software config enum. This is necessary because
    // the initial values for all barcode reader enums are set from the aforementioned software config enum

    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    if (_mainUI.getCurrentUserType().equals(UserTypeEnum.ADMINISTRATOR) == false)
    {
      setEnabledFalseCallTriggeringFeature(false);
      setEnabledAllControls(false);
    }
    else
    {
      setEnabledFalseCallTriggeringFeature(true);
      _enableFalseCallMonitoringCheckbox.setSelected(_falseCallMonitoring.isFalseCallMonitoringModeEnabled()); // sw.config
      if (_enableFalseCallMonitoringCheckbox.isSelected() == false)
        setEnabledAllControls(false);
      else
        setEnabledAllControls(true);
    }

    if (Config.getInstance().getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE) == FalseCallTriggeringConfigEnum.getDefectivePinsNumberThresholdType())
      _defectThresholdTypePinsNumberRadioButton.setSelected(true);
    else
      _defectThresholdTypePPMPinsRadioButton.setSelected(true);
    
    _enforceWriteRemarkCheckbox.setSelected(_falseCallMonitoring.isEnforceWriteRemark());
    _enforceAdministratorLoginCheckbox.setSelected(_falseCallMonitoring.isEnforceAdministratorLogin());
    _autoDeleteProductionResultsCheckbox.setSelected(_falseCallMonitoring.isAutoDeleteProductionResults());

    _writeRemarkButton.setEnabled(_falseCallMonitoring.isFalseCallTriggeringTriggered());
//    populateScannerTable();
    populateDynamicCallNumberTable();
    
    updateConfigData();
//    setupTableEditors();
  }

  /**
   * @author Laura Cormos
   */
  private void populateConfigurationToUseCombobox()
  {
    Assert.expect(_bcrConfigurationNamesList != null);

    _bcrConfigurationToUseComboBox.removeActionListener(_configurationToUseComboboxActionListener);
    _bcrConfigurationToUseComboBox.removeAllItems();
    for (String name : _bcrConfigurationNamesList)
      _bcrConfigurationToUseComboBox.addItem(name);
    _bcrConfigurationToUseComboBox.addActionListener(_configurationToUseComboboxActionListener);
  }

  /**
   * @author Laura Cormos
   */
  public void setEnabledAllControls(boolean enabled)
  {
    _configurationTriggeringSettingPanel.setEnabled(enabled);
    _enforceWriteRemarkCheckbox.setEnabled(enabled);
    _enforceAdministratorLoginCheckbox.setEnabled(enabled);
    _autoDeleteProductionResultsCheckbox.setEnabled(enabled);
    _configurationSetConsecutiveCountLabel.setEnabled(enabled);
    _configurationSetTotalInspectionLabel.setEnabled(enabled);
    _configurationSetTotalSampleTriggeringLabel.setEnabled(enabled);
    _configurationSetMaximumErrorTriggerLabel.setEnabled(enabled);
    _consecutiveCountTextField.setEnabled(enabled);
    _totalInspectionTextField.setEnabled(enabled);
    _totalSampleTriggeringTextField.setEnabled(enabled);
    _maximumErrorTriggerTextField.setEnabled(enabled);
    _numScannersLabel.setEnabled(enabled);
    _numScannersTextField.setEnabled(enabled);
    _scannerCodesScrollPane.setEnabled(enabled);
    _scannerCodesTable.setEnabled(enabled);
    _serialPortNumberLabel.setEnabled(enabled);
    _serialPortNameCombobox.setEnabled(enabled);
    _bitsPerSecondLabel.setEnabled(enabled);
    _bitsPerSecondCombobox.setEnabled(enabled);
    _dataBitsLabel.setEnabled(enabled);
    _dataBitsCombobox.setEnabled(enabled);
    _parityLabel.setEnabled(enabled);
    _parityCombobox.setEnabled(enabled);
    _stopBitsLabel.setEnabled(enabled);
    _stopBitsCombobox.setEnabled(enabled);
    _noReadErrorLabel.setEnabled(enabled);
    _noReadErrorTextField.setEnabled(enabled);
    _badSymbolErrorLabel.setEnabled(enabled);
    _badSymbolErrorTextField.setEnabled(enabled);
    _noLabelErrorLabel.setEnabled(enabled);
    _noLabelErrorTextField.setEnabled(enabled);
    _brcTestScrollPane.setEnabled(enabled);
    _falseCallMonitoringDynamicCallNumberTable.setEnabled(enabled);
    _addRowButton.setEnabled(enabled);
    _deleteRowButton.setEnabled(enabled);
    _selectBcrConfigurationToUseLabel.setEnabled(enabled);
    _bcrConfigurationToUseComboBox.setEnabled(enabled);
    _defectThresholdTypePinsNumberRadioButton.setEnabled(enabled);
    _defectThresholdTypePPMPinsRadioButton.setEnabled(enabled);
    _defectThresholdTypeLabel.setEnabled(enabled);
  }

  /**
   * @author Laura Cormos
   */
  private void enableFalseCallMonitoringCheckbox_actionPerformed(ActionEvent e)
  {
    boolean enabled = _enableFalseCallMonitoringCheckbox.isSelected();

    try
    {
      _commandManager.execute(new ConfigSetEnableFalseCallMonitoringCommand(enabled, this));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  
  /**
   * XCR-3328 Allow False Call Triggering to Remove indictment.xml
   * @author Cheah Lee Herng
   */
  private void enforceAdministratorLoginCheckbox_actionPerformed(ActionEvent e)
  {
    boolean enabled = _enforceAdministratorLoginCheckbox.isSelected();
    
    try
    {
      _commandManager.execute(new ConfigSetEnforceAdministratorLoginFalseCallMonitoringCommand(enabled));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
  
  /**
   * XCR-3328 Allow False Call Triggering to Remove indictment.xml
   * @author Cheah Lee Herng
   */
  private void autoDeleteProductionResultsCheckbox_actionPerformed(ActionEvent e)
  {
    boolean enabled = _autoDeleteProductionResultsCheckbox.isSelected();
    
    try
    {
      _commandManager.execute(new ConfigSetAutoDeleteProductionResultsFalseCallMonitoringCommand(enabled));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void checkProtectedConfigurations()
  {
    //Object object = _configurationSelectNameCombobox.getSelectedItem();
    Object object = null;
    if (object != null)
    {
      Assert.expect(object instanceof String);
      String selectedName = (String)object;
    
    }
  }

  /**
   * @author Jack Hwee
   */
  private void updateConfigData()
  {
  //  populateScannerTable();
  //  populateTestTable();
 
    _consecutiveCountTextField.setValue(_falseCallMonitoring.getConsecutiveCount());
    _totalInspectionTextField.setValue(_falseCallMonitoring.getTotalInspection());
    _totalSampleTriggeringTextField.setValue(_falseCallMonitoring.getTotalSampleTriggering());
    _maximumErrorTriggerTextField.setValue(_falseCallMonitoring.getMaximumErrorTrigger());
  }

   /**
   * @author Laura Cormos
   */
  private String getLegalFileName(String newConfigurationFileName)
  {
    Assert.expect(newConfigurationFileName != null);

    String whiteSpacePattern = "^\\s*$";
    java.util.List<String> tempConfigNamesList = new ArrayList<String>();
    for (String name : _bcrConfigurationNamesList)
      tempConfigNamesList.add(name.toUpperCase());

    String tempRuleSetName = newConfigurationFileName.toUpperCase();
    while (newConfigurationFileName != null &&
           (tempConfigNamesList.contains(tempRuleSetName) ||
            FileName.hasIllegalChars(newConfigurationFileName) ||
            Pattern.matches(whiteSpacePattern, newConfigurationFileName) ||
            _bcrManager.isProtected(newConfigurationFileName)))
    {
      if (tempConfigNamesList.contains(tempRuleSetName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_FILE_EXISTS_MESSAGE_KEY",
                                                                   new Object[]{newConfigurationFileName})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (FileName.hasIllegalChars(newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_ILLEGAL_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName, FileName.getIllegalChars()})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (Pattern.matches(whiteSpacePattern, newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_ILLEGAL_EMPTY_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;
      }
      else if (_bcrManager.isProtected(newConfigurationFileName))
      {
        newConfigurationFileName = JOptionPane.showInputDialog(_mainUI,
                                                               StringUtil.format(StringLocalizer.keyToString(new LocalizedString(
                                                                   "CFGUI_BCR_CONFIG_PROTECTED_NAME_KEY",
                                                                   new Object[]{newConfigurationFileName, _bcrManager.getProtectedIndicator()})), 50),
                                                               StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                               JOptionPane.INFORMATION_MESSAGE);
        if (newConfigurationFileName != null)
          tempRuleSetName = newConfigurationFileName.toUpperCase();
        else
          return newConfigurationFileName;

      }
    }
    return newConfigurationFileName;
  }

 /**
  * @author Laura Cormos
  */
 private boolean saveAsConfiguration()
 {
   String newConfigurationName = JOptionPane.showInputDialog(_mainUI,
                                                             StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_NAME_KEY"),
                                                             StringLocalizer.keyToString("CFGUI_BCR_NEW_CONFIGURATION_TITLE_KEY"),
                                                             JOptionPane.INFORMATION_MESSAGE);
   if (newConfigurationName != null) // null means the dialog was canceled
   {
     newConfigurationName = getLegalFileName(newConfigurationName);
     if (newConfigurationName == null)
       return false;

     try
     {
       _bcrManager.saveAs(newConfigurationName);
       setConfigurationDirtyFlag(false);
//       _configurationSelectNameCombobox.addItem(newConfigurationName);
 //      _configurationSelectNameCombobox.setSelectedItem(newConfigurationName);
       _bcrConfigurationToUseComboBox.addItem(newConfigurationName);
       _bcrConfigurationNamesList.add(newConfigurationName);
       checkProtectedConfigurations();
     }
     catch (DatastoreException ex)
     {
       showDatastoreError(ex);
       return false;
     }
   }

   return true;
 }


 /**
  * @author Laura Cormos
  */
 private void showIllegalNumScannersErrorMessage()
 {
   JOptionPane.showMessageDialog(_mainUI,
                                 StringLocalizer.keyToString(new LocalizedString(
                                     "CFGUI_BCR_ILLEGAL_SCANNER_NUMBER_MESSAGE_KEY",
                                     new Object[]
                                     {_bcrManager.getMinimumBarcodeReaderNumberOfScanners(),
                                     _bcrManager.getMaximumBarcodeReaderNumberOfScanners()})),
                                 StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                 JOptionPane.ERROR_MESSAGE);
 }

 /**
  * @author Laura Cormos
  */
 public void setConfigurationDirtyFlag(boolean dirty)
 {
   if (dirty)
     _bcrConfigurationIsDirty = true;
   else
     _bcrConfigurationIsDirty = false;
   resetStatusBar();
 }

 /**
  * @author Laura Cormos
  */
 private void setupTableEditors()
 {
   _scannerCodesTable.setPreferredColumnWidths();
   _scannerCodesTable.setEditorsAndRenderers();
   _falseCallMonitoringDynamicCallNumberTable.setPreferredColumnWidths();
   _falseCallMonitoringDynamicCallNumberTable.setEditorsAndRenderers();
 }

 /**
  * @author Laura Cormos
  */
 private boolean saveConfigurationIfNecessary()
 {
   if (_bcrConfigurationIsDirty)
   {
     int answer = JOptionPane.showConfirmDialog(
         _mainUI,
         StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_SAVE_CHANGES_CONFIRM_MESSAGE_KEY",
                                                         new Object[]{_currentSelectedConfig})),
         StringLocalizer.keyToString("CFGUI_SAVE_CHANGES_TITLE_KEY"),
         JOptionPane.YES_NO_CANCEL_OPTION);
     if (answer == JOptionPane.CANCEL_OPTION)
       return false;
     else if (answer == JOptionPane.YES_OPTION)
     {
       if (_bcrManager.isProtected(_currentSelectedConfig) == false)
         return saveCurrentConfiguration();
       else
         return saveAsConfiguration();
     }
   }
   // user selected to not save, dirty flag is false and 'saving' is complete
   setConfigurationDirtyFlag(false);
   return true;
 }

 /**
  * @author Laura Cormos
  */
 private boolean saveCurrentConfiguration()
 {
   _savingConfigurationInProgress = true;
   try
   {
     // set values for the 3 text fields in case someone typed in the new message text but did not terminate the edit,
     // i.e. press the Enter key or clicked somewhere else
     _bcrManager.setBarcodeReaderBadSymbolMessage((String)_badSymbolErrorTextField.getText());
     _bcrManager.setBarcodeReaderNoLabelMessage((String)_noLabelErrorTextField.getText());
     _bcrManager.setBarcodeReaderNoReadMessage((String)_noReadErrorTextField.getText());
     // save changes
     _bcrManager.save();
     _mainUI.setStatusBarText(StringLocalizer.keyToString(new LocalizedString("CFGUI_BCR_CONFIG_SAVED_OK_MESSAGE_KEY",
                                                                              new Object[]{_currentSelectedConfig})));
     setConfigurationDirtyFlag(false);
     _savingConfigurationInProgress = false;
     }
   catch (DatastoreException ex)
   {
     showDatastoreError(ex);
     return false;
   }
   return true;
 }

 /**
  * @author Laura Cormos
  */
 void showDatastoreError(DatastoreException ex)
 {
   JOptionPane.showMessageDialog(_mainUI,
                                 ex.getLocalizedMessage(),
                                 StringLocalizer.keyToString(new LocalizedString("CFGUI_ENV_NAME_KEY", null)),
                                 JOptionPane.ERROR_MESSAGE);
 }

 /**
  * @author Laura Cormos
  */
 private void resetStatusBar()
 {
   _mainUI.setStatusBarText("");
 }

 /**
  * @author Laura Cormos
  */
 public synchronized void update(final Observable observable, final Object object)
 {
   SwingUtils.invokeLater(new Runnable()
   {
     public void run()
     {
       Assert.expect(observable != null);
       Assert.expect(object != null);

       if (observable instanceof ConfigObservable)
       {
         handleConfigDataUpdate(object);
       }
       else if (observable instanceof com.axi.v810.gui.undo.CommandManager)
       {
         _falseCallMonitoringDynamicCallNumberTableModel.setData();
       }
//       else
//         Assert.expect(false);
     }
   });
 }
 
  /**
   * @author Jack Hwee
   */
  private void consecutiveCountTextField()
  {
    Long val;
    try
    {
      val = _consecutiveCountTextField.getLongValue(); 
      
      if (val > _totalInspectionTextField.getLongValue())       
      {
        JOptionPane.showMessageDialog(_mainUI,
                               StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_SET_THRESHOLD_ERROR_LOG_KEY"),
                               StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_DIALOG_KEY"),
                               JOptionPane.ERROR_MESSAGE);
        
        val = _totalInspectionTextField.getLongValue();
        _consecutiveCountTextField.setValue(val);
      }
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_CONSECUTIVE_COUNT);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_CONSECUTIVE_COUNT);   
    
    try
    {
      _commandManager.execute(new ConfigSetConsecutiveCountCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void totalInspectionTextField()
  {
    Long val;
    try
    {
      val = _totalInspectionTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_CONSECUTIVE_COUNT);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_CONSECUTIVE_COUNT);

    try
    {
      _commandManager.execute(new ConfigSetTotalInspectionCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
    /**
   * @author Jack Hwee
   */
  private void totalSampleTriggeringTextField()
  {
    Long val;
    try
    {
      val = _totalSampleTriggeringTextField.getLongValue();
      
      if (val > _totalInspectionTextField.getLongValue())       
      {
        JOptionPane.showMessageDialog(_mainUI,
                               StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGER_SET_THRESHOLD_ERROR_LOG_KEY"),
                               StringLocalizer.keyToString("TESTEXEC_GUI_FALSE_CALL_TRIGGERING_DIALOG_KEY"),
                               JOptionPane.ERROR_MESSAGE);
        
        val = _totalInspectionTextField.getLongValue();
        _totalSampleTriggeringTextField.setValue(val);      
      }
      
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_CONSECUTIVE_COUNT);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_CONSECUTIVE_COUNT);

    try
    {
      _commandManager.execute(new ConfigSetTotalSampleTriggeringCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void maximumErrorTriggerTextField()
  {
    Long val;
    try
    {
      val = _maximumErrorTriggerTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_CONSECUTIVE_COUNT);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_CONSECUTIVE_COUNT);

    try
    {
      _commandManager.execute(new ConfigSetMaximumErrorTriggerCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
 /**
  * @author Jack Hwee
  */
   private void addRowButton_actionPerformed()
  {   
      try 
      {
          _addDynamicCallNumberDialog = new AddDynamicCallNumberDialog(_mainUI, StringLocalizer.keyToString("CFGUI_FCT_ADD_DYNAMIC_CALL_NUMBER_TITLE_KEY"));
          SwingUtils.centerOnComponent(_addDynamicCallNumberDialog, _mainUI);

          _addDynamicCallNumberDialog.setVisible(true);
          
          int totalComponent = _addDynamicCallNumberDialog.getTotalComponentAdded();
          int totalError = _addDynamicCallNumberDialog.getTotalErrorAdded();
          java.util.List<String> dynamicCallNumberList = _falseCallMonitoring.getDynamicCallNumber();
          
          if (totalComponent > 0 && totalError > 0)
              addDynamicCallNumber(dynamicCallNumberList, totalComponent, totalError);
          
         // _falseCallMonitoringDynamicCallNumberTableModel.setData();
                  
      } 
      catch (XrayTesterException ex) 
      {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                  ex,
                  StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                  true);
      }
        _falseCallMonitoringDynamicCallNumberTableModel.setData();
        _falseCallMonitoringDynamicCallNumberTableModel.fireTableStructureChanged();
    
  }
  
   /**
   * @author Jack Hwee
   */
   private void deleteButton_actionPerformed()
  {   
    int[] selectedRows = _falseCallMonitoringDynamicCallNumberTable.getSelectedRows();
    
    if (selectedRows.length > 0)
    {
         for (int index : selectedRows)
         //for(int i = 0; i < selectedRows.length; i++)
         {
             int selectedRow = _falseCallMonitoringDynamicCallNumberTable.getSelectedRow();
             int totalComponent = (Integer)_falseCallMonitoringDynamicCallNumberTableModel.getValueAt(index, 0);
             int totalError = (Integer)_falseCallMonitoringDynamicCallNumberTableModel.getValueAt(index, 1);
             java.util.List<String> dynamicCallNumberList = _falseCallMonitoring.getDynamicCallNumber();
             
             deleteDynamicCallNumber(dynamicCallNumberList, totalComponent, totalError);
            
            // _falseCallMonitoringDynamicCallNumberTableModel.setData();
         }

         _falseCallMonitoringDynamicCallNumberTableModel.setData();
         _falseCallMonitoringDynamicCallNumberTableModel.fireTableStructureChanged();
    
    }  
  }
   
   /**
 * @author Jack Hwee
 */
private void populateDynamicCallNumberTable()
{
  _falseCallMonitoringDynamicCallNumberTableModel.setData();
}

 /**
  * @author Laura Cormos
  */
 private void handleConfigDataUpdate(Object object)
 {
   Assert.expect(object != null);
   // update fields only if this update is not caused by the save action
   if (_savingConfigurationInProgress == false)
   {
     if (object instanceof ConfigEnum)
     {
       ConfigEnum configEnum = (ConfigEnum)object;
       if (configEnum instanceof SoftwareConfigEnum)
       {
         populateWithConfigData();
       }
       //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
       else if (configEnum instanceof FalseCallTriggeringConfigEnum)
       {
          populateWithConfigData();
       }
     }
   }
 }
 
   /*
   * @author Jack Hwee
   */
  private void addDynamicCallNumber(java.util.List<String> dynamicCallNumberList, int totalComponent, int totalError)
  {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_FALSE_CALL_TRIGGERING_ADD_DYNAMIC_CALL_NUMBER_KEY"));
      
      try
      {
         _commandManager.execute(new FalseCallTriggeringAddDynamicCallNumberCommand(dynamicCallNumberList, totalComponent, totalError));
      }
      catch (XrayTesterException xte)
      {
         MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                               xte.getLocalizedMessage(),
                               StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                               true);
      }
     
      _commandManager.endCommandBlock();
      
  }
  
   /*
   * @author Jack Hwee
   */
  private void deleteDynamicCallNumber(java.util.List<String> dynamicCallNumberList, int totalComponent, int totalError)
  {
      _commandManager.trackState(getCurrentUndoState());
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_FALSE_CALL_TRIGGERING_DELETE_DYNAMIC_CALL_NUMBER_KEY"));
      
      try
      {
         _commandManager.execute(new FalseCallTriggeringDeleteDynamicCallNumberCommand(dynamicCallNumberList, totalComponent, totalError));
      }
      catch (XrayTesterException xte)
      {
         MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                               xte.getLocalizedMessage(),
                               StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                               true);
      }
     
      _commandManager.endCommandBlock();
      
  }
     
    /*
   * @author Jack Hwee
   */
  private UndoState getCurrentUndoState()
  {
      UndoState undoState = new UndoState();
      
      return undoState;
  }
  
  /**
  * @author Jack Hwee
  */
  private void writeRemarkButton_actionPerformed()
  {   
    FalseCallTriggeringRemarkDialog remarkDialog = new FalseCallTriggeringRemarkDialog(_mainUI, 
                                                                                       StringLocalizer.keyToString("CFGUI_FCT_REMARK_TITLE_KEY"), 
                                                                                       StringLocalizer.keyToString("CFGUI_FCT_OPTION_DIALOG_WARNING_MESSAGE_KEY"));
    SwingUtils.centerOnComponent(remarkDialog, _mainUI);
    remarkDialog.setVisible(true);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void setEnabledFalseCallTriggeringFeature(boolean enabled)
  {
    _enableFalseCallMonitoringCheckbox.setEnabled(enabled);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void defectTypeRadioButtonGroup_actionListener()
  {
    int defectThresholdType = 0;
    if(_defectThresholdTypePinsNumberRadioButton.isSelected())
    {
      defectThresholdType = FalseCallTriggeringConfigEnum.getDefectivePinsNumberThresholdType();
    }
    else
    {
      defectThresholdType = FalseCallTriggeringConfigEnum.getDefectivePPMPinThresholdType();
    }

    try
    {
      ConfigSetDefectThresholdTypeCommand setDefectThresholdTypeCommand = new ConfigSetDefectThresholdTypeCommand(defectThresholdType);
      _commandManager.execute(setDefectThresholdTypeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void enforceWriteRemarkCheckbox_actionPerformed(ActionEvent e)
  {
    boolean enabled = _enforceWriteRemarkCheckbox.isSelected();
    
    try
    {
      _commandManager.execute(new ConfigSetEnforceWriteRemarkFalseCallMonitoringCommand(enabled));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(_mainUI, ex.getLocalizedMessage(), StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"), true);
    }
  }
}
