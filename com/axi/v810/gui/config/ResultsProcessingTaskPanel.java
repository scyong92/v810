package com.axi.v810.gui.config;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ResultsProcessingTaskPanel extends AbstractTaskPanel
{
  private AbstractTaskPanel _currentActiveTabPanel;

  // UI components
  private JTabbedPane _tabbedPane = new JTabbedPane();
  private ResultsProcessingResultsTabPanel _resultsTabPanel;
  private ResultsProcessingProductionTuneTabPanel _productionTuneTabPanel;
  private JPanel _centerPanel = new JPanel();

  // Results Tab UI components
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private ConfigEnvironment _envPanel = null;


  /**
   * Useful ONLY for the JBuilder designer.  Not for use with normal code.
   * @author Laura Cormos
   */
  private ResultsProcessingTaskPanel()
  {
    Assert.expect(java.beans.Beans.isDesignTime());
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  public ResultsProcessingTaskPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    Assert.expect(envPanel != null);

    _envPanel = envPanel;
    jbInit();
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    add(_centerPanel, BorderLayout.CENTER);

    _centerPanel.setLayout(_centerPanelBorderLayout);
    _centerPanel.add(_tabbedPane, BorderLayout.CENTER);
    _resultsTabPanel = new ResultsProcessingResultsTabPanel(_envPanel);
    _tabbedPane.add(_resultsTabPanel, StringLocalizer.keyToString("CFGUI_RESPROC_RESULTS_TAB_TITLE_KEY"));
    _productionTuneTabPanel = new ResultsProcessingProductionTuneTabPanel(_envPanel);
    _tabbedPane.add(_productionTuneTabPanel, StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TAB_TITLE_KEY"));
    _tabbedPane.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e)
      {
        tabbedPane_stateChanged(e);
      }
    });

    _currentActiveTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();
    // define menus and toolBar items to be added later
  }

  /**
   * @author Laura Cormos
   */
  private void tabbedPane_stateChanged(ChangeEvent e)
  {
    AbstractTaskPanel selectedTabPanel = (AbstractTaskPanel)_tabbedPane.getSelectedComponent();

    if (_currentActiveTabPanel == selectedTabPanel)
      return;

    // first finish the currently active panel
    if (_currentActiveTabPanel.isReadyToFinish())
    {
      _currentActiveTabPanel.finish();
    }
    else
    {
      _tabbedPane.setSelectedComponent(_currentActiveTabPanel);
      return;
    }

    // then start the chosen tab is the previous tab is finished
    selectedTabPanel.start();
    _currentActiveTabPanel = selectedTabPanel;
  }


  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
    // set up for custom menus and tool bar
    startMenusAndToolBar();
    _currentActiveTabPanel.start();
     // add custom menus
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    boolean flag = _currentActiveTabPanel.isReadyToFinish();
    return flag;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    _currentActiveTabPanel.finish();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
  }
}
