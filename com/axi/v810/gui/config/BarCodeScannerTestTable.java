package com.axi.v810.gui.config;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;

/**
 * This class displays the list of results processing conditions
 * @author Laura Cormos
 */
class BarCodeScannerTestTable extends JTable
{
  public static final int _SCANNER_ID_INDEX = 0;
  public static final int _BARCODE_INDEX = 1;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SCANNER_ID_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _BARCODE_COLUMN_WIDTH_PERCENTAGE = .90;

  private NumericRenderer _numericRenderer = new NumericRenderer(0, JLabel.LEFT);

  /**
   * Default constructor.
   * @author Laura Cormos
   */
  public BarCodeScannerTestTable()
  {
    // do nothing
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Laura Cormos
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int scannerIdColumnWidth = (int)(totalColumnWidth * _SCANNER_ID_COLUMN_WIDTH_PERCENTAGE);
    int barcodeColumnWidth = (int)(totalColumnWidth * _BARCODE_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_SCANNER_ID_INDEX).setPreferredWidth(scannerIdColumnWidth);
    columnModel.getColumn(_BARCODE_INDEX).setPreferredWidth(barcodeColumnWidth);
  }

  /**
  * @author Laura Cormos
  */
 public void setEditorsAndRenderers()
 {
   TableColumnModel columnModel = getColumnModel();

   columnModel.getColumn(_SCANNER_ID_INDEX).setCellRenderer(_numericRenderer);
 }

}

