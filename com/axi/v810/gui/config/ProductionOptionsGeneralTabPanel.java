package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.business.panelSettings.Project;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author Laura Cormos
 */
public class ProductionOptionsGeneralTabPanel extends AbstractTaskPanel implements Observer
{
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getConfigInstance();
  private TestExecution _testExecution = TestExecution.getInstance();
  private Sampling _sampling = Sampling.getInstance();
  private LightStack _lightStack = LightStack.getInstance();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private RepairImagesConfigManager _repairManager = RepairImagesConfigManager.getInstance();
  private ScriptRunner _scriptRunner = ScriptRunner.getInstance();

  private JPanel _centerPanel = null;

  private JCheckBox _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox;
  
  // production options
  private static final int _MIN_XOUT_DETECTION_THRESHOLD = 1;
  private static final int _MAX_XOUT_DETECTION_THRESHOLD = 100;
  private JCheckBox _ejectPanelOnAlignmentFailureCheckBox;
  private JCheckBox _demoModeCheckBox;
  private JCheckBox _bypassModeCheckBox;
  private JCheckBox _bypassWithSerialControlModeCheckBox;
  private JCheckBox _bypassWithResultsCheckBox;
  private JCheckBox _semiAutomatedModeCheckBox; //Siew Yeng - XCR1745 - Semi-automated mode
  private JCheckBox _enableBuzzerCheckBox;
  private JCheckBox _enableXoutDetectionCheckBox;
  private JCheckBox _alwaysDisplayVariationNameCheckBox;
  
  //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
  private JCheckBox _displayProductionPanelLoadingUnloadingTimeCheckBox; 

  private ButtonGroup _xoutDetectionMethodButtonGroup;
  private JRadioButton _useAlignmentFailedXoutDetectionRadioButton;
  private JRadioButton _useDefectThresholdXoutDetectionRadioButton;

  private JLabel _xoutDetectionFirstLabel;
  private JLabel _xoutDetectionSecondLabel;
  private NumericTextField _xoutDetectionThresholdTextField = new NumericTextField();
  private DecimalFormat _xoutDetectionThresholdFormat = new DecimalFormat();
  private NumericRangePlainDocument _xoutDetectionThresholdDataDoc = new NumericRangePlainDocument();
  
  //for production mode
  private JLabel _confirmationAndAdjustmentModeLabel;
  private JComboBox _confirmationAndAdjustmentModeComboBox;
  private String _standardModeString = StringLocalizer.keyToString("CFGUI_CONFIRMATION_AND_ADJUSTMENT_MODE_STANDARD_KEY");
  private String _passiveModeString = StringLocalizer.keyToString("CFGUI_CONFIRMATION_AND_ADJUSTMENT_MODE_PASSIVE_KEY");

  // sampling options
  private static final int _MIN_PANEL_SAMPLING = 1;
  private JCheckBox _panelSamplingCheckBox;
  private DecimalFormat _panelSamplingSkipFormat = new DecimalFormat();
  private NumericRangePlainDocument _panelSamplingSkipDataDoc = new NumericRangePlainDocument();
  private NumericTextField _panelSamplingSkipEveryNthTextField = new NumericTextField();
  private DecimalFormat _panelSamplingTestFormat = new DecimalFormat();
  private NumericRangePlainDocument _panelSamplingTestDataDoc = new NumericRangePlainDocument();
  private NumericTextField _panelSamplingTestEveryNthTextField = new NumericTextField();
  private JRadioButton _panelSamplingSkipRadioButton;
  private JRadioButton _panelSamplingTestRadioButton;
  private JLabel _skipPanelLabel;
  private JLabel _testPanelLabel;
  private JLabel _ORLabel;


  // panel handling
  private JCheckBox _smemaCheckBox;
  private JCheckBox _ignorePIPCheckBox; //Khang Wah (12-Oct-2009)
  private JCheckBox _checkSmemaSensorSignalDuringUnloading;
  private JCheckBox _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox;
  private JCheckBox _keepOuterBarrierOpenAfterScanningCompeleteCheckBox;
  private JComboBox _panelLoadingModeComboBox;
  private JComboBox _panelFlowDirectionComboBox;
  private String _flowThroughString = StringLocalizer.keyToString("CFGUI_FLOW_THROUGH_LOADING_KEY");
  private String _passBackString = StringLocalizer.keyToString("CFGUI_PASS_BACK_LOADING_KEY");
  private String _leftToRightString = StringLocalizer.keyToString("CFGUI_LEFT_TO_RIGHT_FLOW_KEY");
  private String _rightToLeftString = StringLocalizer.keyToString("CFGUI_RIGHT_TO_LEFT_FLOW_KEY");

  // repair image saving
  private final int _MIN_NUM_IMAGES = 0;
  private JCheckBox _limitNumRepairImagesToSaveCheckbox;
  private DecimalFormat _numRepairImagesToSaveFormat = new DecimalFormat();
  private NumericRangePlainDocument _numRepairImagesToSaveDataDoc = new NumericRangePlainDocument();
  private NumericTextField _numRepairImagesToSaveTextField = new NumericTextField();
  private JCheckBox _saveFocusConfirmationRepairInfoCheckbox;
  private JLabel _saveFocusConfirmationWarningLabel;
  //private JCheckBox _generateMultiAngleImagesCheckbox; // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development  
  private ButtonGroup _repairImageTypeButtonGroup;
  private JRadioButton _repairImageTypeJPGRadioButton;
  private JRadioButton _repairImageTypePNGRadioButton;

  // script setup
  private JCheckBox _preInspectionScriptCheckBox;
  private JTextField _preInspectionScriptTextField;
  private JCheckBox _postInspectionScriptCheckBox;
  private JTextField _postInspectionScriptTextField;
  private JButton _preInspectionScriptButton;
  private JButton _postInspectionScriptButton;
  
  private PreOrPostInspectionScriptCustomizeInputTableModel _preInspectionScriptCustomizeOutputTableModel;
  private PreOrPostInspectionScriptCustomizeInputTable _preInspectionScriptCustomizeOutputTable;
  private JScrollPane _preInsepctionScriptCustomizeOutputScrollPane = new JScrollPane();
  
  private PreOrPostInspectionScriptCustomizeInputTableModel _preInspectionScriptCustomizeInputTableModel;
  private PreOrPostInspectionScriptCustomizeInputTable _preInspectionScriptCustomizeInputTable;
  private JScrollPane _preInsepctionScriptCustomizeInputScrollPane = new JScrollPane();

  ButtonGroup _group = new ButtonGroup();
  private JRadioButton _panelBasedPreInspectionScriptOutputChkBox = new JRadioButton(StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_PANEL_BASED_KEY"));
  private JRadioButton _boardBasedPreInspectionScriptOutputChkBox = new JRadioButton(StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_BOARD_BASED_KEY"));
  
  //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
  private NumericTextField _xoutDetectionFailureThresholdTextField = new NumericTextField();
  private DecimalFormat _xoutDetectionFailureThresholdFormat = new DecimalFormat();
  private NumericRangePlainDocument _xoutDetectionFailureThresholdDoc = new NumericRangePlainDocument();
  private JLabel _xoutDetectionLabel;

  /**
   * @author Laura Cormos
   */
  public ProductionOptionsGeneralTabPanel(ConfigEnvironment envPanel)
  {
    super(envPanel);
    try
    {
      jbInit();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
    }
  }

  /**
   * This class is an observer of config. We will determine what
   * kind of changes were made and decide if we need to update the gui.
   * @author Andy Mechtenberg
   */
  public synchronized void update(final Observable observable, final Object argument)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        Assert.expect(argument != null);

        if (observable instanceof ConfigObservable)
        {
          if (argument instanceof ConfigEnum)
          {
            ConfigEnum configEnum = (ConfigEnum)argument;
            if (configEnum instanceof SoftwareConfigEnum)
            {
              SoftwareConfigEnum softwareConfigEnum = (SoftwareConfigEnum)configEnum;
              if (softwareConfigEnum.equals(SoftwareConfigEnum.EJECT_PANEL_IF_ALIGNMENT_FAILS) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.TEST_EXEC_DEMO_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.TEST_EXEC_BYPASS_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_SERIAL_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.LIGHT_STACK_BUZZER_ENABLE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PANEL_SAMPLING_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.SKIP_EVERY_NTH_PANEL_FOR_SAMPLING) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.TEST_EVERY_NTH_PANEL_FOR_SAMPLING) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PANEL_HANDLER_SMEMA_ENABLE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PANEL_HANDLER_LOADING_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PANEL_HANDLER_PANEL_FLOW_DIRECTION) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.ENABLE_PRE_INSPECTION_SCRIPT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.ENABLE_POST_INSPECTION_SCRIPT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PRE_INSPECTION_SCRIPT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.POST_INSPECTION_SCRIPT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.LIMIT_NUM_REPAIR_IMAGES_TO_SAVE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.NUM_REPAIR_IMAGES_TO_SAVE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.XOUT_DETECTION_ENABLE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.XOUT_DETECTION_THRESHOLD) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.ENABLE_SET_PANEL_AVAILABLE_AFTER_DOWNSTREAM_ACCEPT_PANEL) ||
                  softwareConfigEnum.equals(SoftwareConfigEnum.PANEL_HANDLER_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_ENABLE))
              {
                updateDataOnScreen();
              }
            }
          }
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit() throws Exception
  {
    setLayout(_taskPanelLayout);
    setBorder(_taskPanelBorder);

    _centerPanel = new JPanel();
    VerticalFlowLayout centerPanelLayoutManager = new VerticalFlowLayout();
    _centerPanel.setLayout(centerPanelLayoutManager);

    add(_centerPanel, BorderLayout.CENTER);

    // set up panel GUI components
    JPanel productionOptionsPanel = new JPanel(new VerticalFlowLayout());
    JPanel samplingOptionsPanel = new JPanel(new BorderLayout(0,0));
    JPanel panelHandlingPanel = new JPanel(new VerticalFlowLayout());
    JPanel repairImagesSaveLimitPanel = new JPanel(new VerticalFlowLayout());
    JPanel scriptSetupPanel = new JPanel(new BorderLayout());

    _centerPanel.add(productionOptionsPanel);
    _centerPanel.add(samplingOptionsPanel);
    _centerPanel.add(panelHandlingPanel);
    _centerPanel.add(repairImagesSaveLimitPanel);
    _centerPanel.add(scriptSetupPanel);

    Border productionOptionsTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
        StringLocalizer.keyToString("CFGUI_PRODUCTION_OPTIONS_KEY"));
    Border productionOptionsTitledBorder = BorderFactory.createCompoundBorder(productionOptionsTitledBorder1,
        BorderFactory.createEmptyBorder(0, 0, 5, 0));

    Border samplingOptionsTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
        StringLocalizer.keyToString("CFGUI_SAMPLING_OPTIONS_KEY"));
    Border samplingOptionsTitledBorder = BorderFactory.createCompoundBorder(samplingOptionsTitledBorder1,
        BorderFactory.createEmptyBorder(0, 5, 5, 0));

    Border panelHandlingTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
        StringLocalizer.keyToString("CFGUI_PANEL_HANDLING_KEY"));
    Border panelHandlingTitledBorder = BorderFactory.createCompoundBorder(panelHandlingTitledBorder1,
        BorderFactory.createEmptyBorder(0, 0, 5, 0));

    Border repairImagesSaveLimitTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
        StringLocalizer.keyToString("CFGUI_REPAIR_IMAGES_LIMIT_KEY"));
    Border repairImagesSaveLimitTitledBorder = BorderFactory.createCompoundBorder(repairImagesSaveLimitTitledBorder1,
        BorderFactory.createEmptyBorder(0, 0, 5, 0));

    Border scriptSetupTitledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
        StringLocalizer.keyToString("CFGUI_SCRIPT_SETUP_KEY"));
    Border scriptSetupTitledBorder = BorderFactory.createCompoundBorder(scriptSetupTitledBorder1,
        BorderFactory.createEmptyBorder(0, 10, 0, 0));

    productionOptionsPanel.setBorder(productionOptionsTitledBorder);
    samplingOptionsPanel.setBorder(samplingOptionsTitledBorder);
    panelHandlingPanel.setBorder(panelHandlingTitledBorder);
    repairImagesSaveLimitPanel.setBorder(repairImagesSaveLimitTitledBorder);
    scriptSetupPanel.setBorder(scriptSetupTitledBorder);

    // production options
    _ejectPanelOnAlignmentFailureCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_EJECT_PANEL_IF_ALIGN_FAILS_KEY"));
    _demoModeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_DEMO_MODE_KEY"));
    _bypassModeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_BYPASS_MODE_KEY"));
    _bypassWithSerialControlModeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_BYPASS_MODE_WITH_SERIAL_CONTROL_KEY"));
    _bypassWithResultsCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_BYPASS_MODE_WITH_CREATE_RESULTS_KEY"));
    //Siew Yeng - XCR1745
    _semiAutomatedModeCheckBox =  new JCheckBox(StringLocalizer.keyToString("CFGUI_SEMI_AUTOMATED_MODE_KEY"));
    _enableBuzzerCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_ENABLE_BUZZER_KEY"));
    _enableXoutDetectionCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_ENABLE_XOUT_DETECTION_KEY"));
    _alwaysDisplayVariationNameCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_DISPLAY_VARIATION_NAME_KEY"));
    
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    _displayProductionPanelLoadingUnloadingTimeCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_DISPLAY_PANEL_LOADING_UNLOADING_TIME_NAME_KEY"));

    _xoutDetectionFirstLabel = new JLabel(StringLocalizer.keyToString("CFGUI_XOUT_DETECTION_LABEL_1_KEY"));
    _xoutDetectionSecondLabel = new JLabel(StringLocalizer.keyToString("CFGUI_XOUT_DETECTION_LABEL_2_KEY"));
    _xoutDetectionThresholdFormat.setParseIntegerOnly(true);
    _xoutDetectionThresholdDataDoc.setRange(new IntegerRange(_MIN_XOUT_DETECTION_THRESHOLD, _MAX_XOUT_DETECTION_THRESHOLD));
    _xoutDetectionThresholdTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _xoutDetectionThresholdTextField.setDocument(_xoutDetectionThresholdDataDoc);
    _xoutDetectionThresholdTextField.setFormat(_xoutDetectionThresholdFormat);
    _xoutDetectionThresholdTextField.setColumns(3);

    // added by Wei Chin for x-out feature
    _xoutDetectionMethodButtonGroup = new ButtonGroup();
    _useAlignmentFailedXoutDetectionRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_USE_ALIGNMENT_FAILURE_XOUT_DETECTION_KEY"));
    _useDefectThresholdXoutDetectionRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_XOUT_DETECTION_LABEL_1_KEY") + " ");
    _xoutDetectionMethodButtonGroup.add(_useAlignmentFailedXoutDetectionRadioButton);
    _xoutDetectionMethodButtonGroup.add(_useDefectThresholdXoutDetectionRadioButton);
    _xoutDetectionLabel = new JLabel(StringLocalizer.keyToString("CFGUI_XOUT_DETECTION_LABEL_2_KEY"));
    
    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    GridLayout xoutMethodGridLayout = new GridLayout();
    xoutMethodGridLayout.setColumns(2);
    xoutMethodGridLayout.setHgap(-1);
    xoutMethodGridLayout.setRows(2);
    xoutMethodGridLayout.setVgap(-1);

    JPanel bypassWithSerialControlPanel = new JPanel();
    bypassWithSerialControlPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    bypassWithSerialControlPanel.setLayout(new BoxLayout(bypassWithSerialControlPanel, BoxLayout.Y_AXIS));
    bypassWithSerialControlPanel.add(_bypassWithSerialControlModeCheckBox);
    
    JPanel bypassWithResultsPanel = new JPanel();
    bypassWithResultsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    bypassWithResultsPanel.setLayout(new BoxLayout(bypassWithResultsPanel, BoxLayout.Y_AXIS));
    bypassWithResultsPanel.add(_bypassWithResultsCheckBox);
    
    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    _xoutDetectionFailureThresholdFormat.setParseIntegerOnly(true);
    _xoutDetectionFailureThresholdDoc.setRange(new IntegerRange(_MIN_XOUT_DETECTION_THRESHOLD, _MAX_XOUT_DETECTION_THRESHOLD));
    _xoutDetectionFailureThresholdTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _xoutDetectionFailureThresholdTextField.setDocument(_xoutDetectionFailureThresholdDoc);
    _xoutDetectionFailureThresholdTextField.setFormat(_xoutDetectionFailureThresholdFormat);
    _xoutDetectionFailureThresholdTextField.setColumns(8);
    
    JPanel defectThresholdJPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    defectThresholdJPanel.add(_useDefectThresholdXoutDetectionRadioButton);
    defectThresholdJPanel.add(_xoutDetectionFailureThresholdTextField);
    defectThresholdJPanel.add(_xoutDetectionLabel);
    
    JPanel xoutAlignmentPanel = new JPanel();
    xoutAlignmentPanel.setLayout(xoutMethodGridLayout);
    xoutAlignmentPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    //Khaw Chek Hau - XCR3373: X-out board by using number of false call
    xoutAlignmentPanel.add(_useAlignmentFailedXoutDetectionRadioButton);
    xoutAlignmentPanel.add(defectThresholdJPanel);
    JPanel xoutLabelsPanel = new JPanel();
    xoutLabelsPanel.setBorder(BorderFactory.createEmptyBorder(0, 40, 0, 0));
    xoutLabelsPanel.add(_xoutDetectionFirstLabel);
    xoutLabelsPanel.add(_xoutDetectionThresholdTextField);
    xoutLabelsPanel.add(_xoutDetectionSecondLabel);

    productionOptionsPanel.add(_ejectPanelOnAlignmentFailureCheckBox);
    productionOptionsPanel.add(_demoModeCheckBox);
    productionOptionsPanel.add(_bypassModeCheckBox);
    productionOptionsPanel.add(bypassWithSerialControlPanel);
    productionOptionsPanel.add(bypassWithResultsPanel);
    productionOptionsPanel.add(_semiAutomatedModeCheckBox); //Siew Yeng - XCR1745
    productionOptionsPanel.add(_enableBuzzerCheckBox);
    productionOptionsPanel.add(_alwaysDisplayVariationNameCheckBox);
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    productionOptionsPanel.add(_displayProductionPanelLoadingUnloadingTimeCheckBox);
    
    // this if statment was added on 10/23/08 to remove the xout feature from customer visibility
    // because MRT (Repair tool) was not updated in time to handle the new x-out status
    // in the indictments.xml file.  If/When MRT gets updated, the user should be able
    // to toggle this config setting to gain access to the x-out feature.
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_XOUT_FEATURE))
    {
      productionOptionsPanel.add(_enableXoutDetectionCheckBox);
//      productionOptionsPanel.add(xoutDefectThresholdPanel);
//      productionOptionsPanel.add(xoutLabelsPanel);
      productionOptionsPanel.add(xoutAlignmentPanel);
    }
    else
    {
      // make sure feature is really turned off
      _testExecution.setXoutDetectionMode(false);
    }

    // for confirmation and adjustment mode
    _confirmationAndAdjustmentModeLabel = new JLabel(StringLocalizer.keyToString("CFGUI_CONFIRMATION_AND_ADJUSTMENT_MODE_KEY"));
    _confirmationAndAdjustmentModeComboBox = new JComboBox();
    _confirmationAndAdjustmentModeComboBox.addItem(_standardModeString);
    _confirmationAndAdjustmentModeComboBox.addItem(_passiveModeString);

    JPanel productionModePanel = new JPanel(new PairLayout(10, 10, false));
    productionModePanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    productionModePanel.add(_confirmationAndAdjustmentModeLabel);
    productionModePanel.add(_confirmationAndAdjustmentModeComboBox);
    
    if(LicenseManager.isVariableMagnificationEnabled())
      productionOptionsPanel.add(productionModePanel);
    
    // sampling options
    _panelSamplingCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_PANEL_SAMPLING_KEY"));
    _panelSamplingTestFormat.setParseIntegerOnly(true);
    _panelSamplingTestDataDoc.setRange(new IntegerRange(_MIN_PANEL_SAMPLING, Integer.MAX_VALUE));
    _panelSamplingTestEveryNthTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _panelSamplingTestEveryNthTextField.setDocument(_panelSamplingTestDataDoc);
    _panelSamplingTestEveryNthTextField.setFormat(_panelSamplingTestFormat);
    _panelSamplingTestEveryNthTextField.setColumns(4);

    _panelSamplingSkipFormat.setParseIntegerOnly(true);
    _panelSamplingSkipDataDoc.setRange(new IntegerRange(_MIN_PANEL_SAMPLING, Integer.MAX_VALUE));
    _panelSamplingSkipEveryNthTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _panelSamplingSkipEveryNthTextField.setDocument(_panelSamplingSkipDataDoc);
    _panelSamplingSkipEveryNthTextField.setFormat(_panelSamplingSkipFormat);
    _panelSamplingSkipEveryNthTextField.setColumns(4);


    _skipPanelLabel = new JLabel(StringLocalizer.keyToString("CFGUI_PANELS_KEY"));
    _testPanelLabel = new JLabel(StringLocalizer.keyToString("CFGUI_PANELS_KEY"));
    _ORLabel = new JLabel(StringLocalizer.keyToString("CFGUI_OR_KEY"));

    _panelSamplingSkipRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_SKIP_ONE_IN_EVERY_N_KEY"), false);
    _panelSamplingTestRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_TEST_ONE_IN_EVERY_N_KEY"), false);
    // by default we test every panel
    _panelSamplingTestRadioButton.setSelected(true);
     //Group the radio buttons.
    ButtonGroup group = new ButtonGroup();
    group.add(_panelSamplingSkipRadioButton);
    group.add(_panelSamplingTestRadioButton);

    JPanel skipPanelSampleJPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
    skipPanelSampleJPanel.add(_panelSamplingSkipRadioButton);
    skipPanelSampleJPanel.add(_panelSamplingSkipEveryNthTextField);
    skipPanelSampleJPanel.add(_skipPanelLabel);

    JPanel testPanelSampleJPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
    testPanelSampleJPanel.add(_panelSamplingTestRadioButton);
    testPanelSampleJPanel.add(_panelSamplingTestEveryNthTextField);
    testPanelSampleJPanel.add(_testPanelLabel);

    JPanel entryJPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
    entryJPanel.add(testPanelSampleJPanel);
    entryJPanel.add(_ORLabel);
    entryJPanel.add(skipPanelSampleJPanel);

    samplingOptionsPanel.add(_panelSamplingCheckBox, BorderLayout.NORTH);
    samplingOptionsPanel.add(entryJPanel, BorderLayout.CENTER);

    // panel handling
    _smemaCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_SMEMA_KEY"));
    _panelLoadingModeComboBox = new JComboBox();
    java.util.List<PanelHandlerPanelLoadingModeEnum> loadingEnums = PanelHandlerPanelLoadingModeEnum.getAllValues();
    for(PanelHandlerPanelLoadingModeEnum loadingEnum : loadingEnums)
    {
      if (loadingEnum.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
        _panelLoadingModeComboBox.addItem(_flowThroughString);
      if (loadingEnum.equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))
        _panelLoadingModeComboBox.addItem(_passBackString);
    }

    _panelFlowDirectionComboBox = new JComboBox();
    java.util.List<PanelHandlerPanelFlowDirectionEnum> flowEnums = PanelHandlerPanelFlowDirectionEnum.getAllValues();
    for(PanelHandlerPanelFlowDirectionEnum flowEnum : flowEnums)
    {
      if (flowEnum.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT))
        _panelFlowDirectionComboBox.addItem(_leftToRightString);
      if (flowEnum.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT))
        _panelFlowDirectionComboBox.addItem(_rightToLeftString);
    }

    JComboBox[] comboBoxes = new JComboBox[]{_panelLoadingModeComboBox, _panelFlowDirectionComboBox, _confirmationAndAdjustmentModeComboBox};
    SwingUtils.makeAllSameLength(comboBoxes);

    JLabel loadingModeLabel = new JLabel(StringLocalizer.keyToString("CFGUI_LOADING_MODE_KEY"));
    JLabel flowDirectionLabel = new JLabel(StringLocalizer.keyToString("CFGUI_FLOW_DIRECTION_KEY"));
    JPanel loadingFlowPanel = new JPanel(new PairLayout(10, 10, false));
    loadingFlowPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    loadingFlowPanel.add(loadingModeLabel);
    loadingFlowPanel.add(_panelLoadingModeComboBox);
    loadingFlowPanel.add(flowDirectionLabel);
    loadingFlowPanel.add(_panelFlowDirectionComboBox);

    /*Khang Wah (12-Oct-2009)*/
     JPanel ignorePIPModePanel = new JPanel();
    ignorePIPModePanel.setLayout(new VerticalFlowLayout());
    _ignorePIPCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_IGNORE_PIP_KEY"));
    _ignorePIPCheckBox.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_ENABLED_SIGNAL_DOWNSTREAM_BEFORE_WAITING_ACCEPT_PANEL_DURING_UNLOADING_KEY"));    
    _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    _checkSmemaSensorSignalDuringUnloading = new JCheckBox(StringLocalizer.keyToString("CFGUI_CHECK_SMEMA_SENSOR_SIGNAL_DURING_UNLOADING_KEY"));    
    _checkSmemaSensorSignalDuringUnloading.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_KEEP_OUTER_BARRIER_OPEN_FOR_LOADING_UNLOADING_PANEL_KEY"));    
    _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    _keepOuterBarrierOpenAfterScanningCompeleteCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGUI_KEEP_OUTER_BARRIER_OPEN_AFTER_SCANNING_COMPLETE_KEY"));    
    _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    ignorePIPModePanel.add(_ignorePIPCheckBox);
    ignorePIPModePanel.add(_enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox);
    ignorePIPModePanel.add(_keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox);
    if (XrayTester.isS2EXEnabled())
    {
      ignorePIPModePanel.add(_keepOuterBarrierOpenAfterScanningCompeleteCheckBox);
    }
    // Kok Chun, Tan - XCR-3565 - only S2EX and XXL have this feature
    if (XrayTester.isS2EXEnabled() || XrayTester.isXXLBased())
      ignorePIPModePanel.add(_checkSmemaSensorSignalDuringUnloading);

    _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.setSelected(_panelHandler.isPanelAvailableAfterDownStreamAcceptPanelModeEnabled());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
         _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.setEnabled(false);
    }
    
    _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setSelected(_panelHandler.isEnabledKeepOuterBarrierOpenForLoadingAndUnloadingPanel());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
      _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setEnabled(false);
    }
    
    _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setSelected(_panelHandler.isEnabledOuterBarrierOpenAfterCompleteScanningForS2EX());
     if(_panelHandler.isInLineModeEnabled()==false)
    {
      _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setEnabled(false);
    }
     
    panelHandlingPanel.add(_smemaCheckBox);
    panelHandlingPanel.add(ignorePIPModePanel); //Khang Wah (12-Oct-2009)
    panelHandlingPanel.add(loadingFlowPanel);

    // repair images limit
    _limitNumRepairImagesToSaveCheckbox = new JCheckBox(StringLocalizer.keyToString("CFGUI_LIMIT_NUM_IMAGES_TO_SAVE_CHECKBOX_KEY"));
    _numRepairImagesToSaveFormat.setParseIntegerOnly(true);
    _numRepairImagesToSaveTextField.setHorizontalAlignment(NumericTextField.TRAILING);
    _numRepairImagesToSaveTextField.setDocument(_numRepairImagesToSaveDataDoc);
    _numRepairImagesToSaveTextField.setFormat(_numRepairImagesToSaveFormat);
    _numRepairImagesToSaveTextField.setColumns(4);
    _numRepairImagesToSaveDataDoc.setRange(new IntegerRange(_MIN_NUM_IMAGES, Integer.MAX_VALUE));
    JLabel maxNumLabel = new JLabel(StringLocalizer.keyToString("CFGUI_MAX_NUM_LABEL_KEY"));
    JPanel numRepairImagesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    numRepairImagesPanel.add(maxNumLabel);
    numRepairImagesPanel.add(_numRepairImagesToSaveTextField);
    numRepairImagesPanel.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    JPanel repairImagesLimitPanel = new JPanel(new VerticalFlowLayout());
    repairImagesLimitPanel.add(_limitNumRepairImagesToSaveCheckbox);
    repairImagesLimitPanel.add(numRepairImagesPanel);

    _saveFocusConfirmationRepairInfoCheckbox = new JCheckBox(StringLocalizer.keyToString("CFGUI_SAVE_FOCUS_CONF_REPAIR_INFO_CHECKBOX_KEY"));
    JPanel focusConfirmationWarningPanel = new JPanel();
    focusConfirmationWarningPanel.setLayout(new VerticalFlowLayout());
    _saveFocusConfirmationWarningLabel = new JLabel("* " + StringLocalizer.keyToString("CFGUI_SAVE_FOCUS_CONF_REPAIR_INFO_WARNING_KEY"));
    _saveFocusConfirmationWarningLabel.setForeground(new Color(0,70,213)); // panels title blue
    _saveFocusConfirmationWarningLabel.setBorder(BorderFactory.createEmptyBorder(0,25,0,0));
    focusConfirmationWarningPanel.add(_saveFocusConfirmationRepairInfoCheckbox);
    focusConfirmationWarningPanel.add(_saveFocusConfirmationWarningLabel);
    
    // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
//    _generateMultiAngleImagesCheckbox = new JCheckBox(StringLocalizer.keyToString("CFGUI_GENERATE_MULTI_ANGLE_IMAGES_CHECKBOX_KEY"));
//    JPanel generateMultiAngleImagesPanel = new JPanel();
//    generateMultiAngleImagesPanel.setLayout(new VerticalFlowLayout());
//    generateMultiAngleImagesPanel.add(_generateMultiAngleImagesCheckbox);

    
    // XCR-2465 Ability to transfer PNG image to VVTS - Cheah Lee Herng
    JLabel repairImageTypeLabel = new JLabel(StringLocalizer.keyToString("CFGUI_REPAIR_IMAGE_TYPE_KEY"));
    JPanel repairImageTypePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));     
    _repairImageTypeJPGRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_JPG_KEY"));
    _repairImageTypePNGRadioButton = new JRadioButton(StringLocalizer.keyToString("CFGUI_PNG_KEY"));    
    
    _repairImageTypeButtonGroup = new ButtonGroup();
    _repairImageTypeButtonGroup.add(_repairImageTypeJPGRadioButton);
    _repairImageTypeButtonGroup.add(_repairImageTypePNGRadioButton);
    
    repairImageTypePanel.add(repairImageTypeLabel);
    repairImageTypePanel.add(_repairImageTypeJPGRadioButton);
    repairImageTypePanel.add(_repairImageTypePNGRadioButton);
    
    repairImagesSaveLimitPanel.add(focusConfirmationWarningPanel);
    repairImagesSaveLimitPanel.add(repairImagesLimitPanel);
    repairImagesSaveLimitPanel.add(repairImageTypePanel);
    //if(Config.isDeveloperDebugModeOn() && LicenseManager.getInstance().isDeveloperSystemEnabled())
    //{
      //repairImagesSaveLimitPanel.add(generateMultiAngleImagesPanel); // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
    //}

    // script setup
    _preInspectionScriptCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGU_PRE_INSPECTION_SCRIPT_KEY"));
    _postInspectionScriptCheckBox = new JCheckBox(StringLocalizer.keyToString("CFGU_POST_INSPECTION_SCRIPT_KEY"));
    JLabel preInspectionPathLabel = new JLabel(StringLocalizer.keyToString("CFGUI_SCRIPT_PATH_KEY"));
    JLabel postInspectionPathLabel = new JLabel(StringLocalizer.keyToString("CFGUI_SCRIPT_PATH_KEY"));
    _preInspectionScriptTextField = new JTextField();
    _preInspectionScriptTextField.setColumns(30);
    _postInspectionScriptTextField = new JTextField();
    _postInspectionScriptTextField.setColumns(30);
    _preInspectionScriptButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));
    _postInspectionScriptButton = new JButton(StringLocalizer.keyToString("CFGUI_BROWSE_BUTTON_KEY"));

    JPanel preInspectionPanel = new JPanel(new BorderLayout(0, -5));
    JPanel preInspectionScriptPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel preInspectionScriptCustomizeOutputPanel = new JPanel(new BorderLayout());
    JPanel preInspectionScriptCustomizeInputPanel = new JPanel(new BorderLayout());
    
    preInspectionScriptPanel.add(preInspectionPathLabel);
    preInspectionScriptPanel.add(_preInspectionScriptTextField);
    preInspectionScriptPanel.add(_preInspectionScriptButton);
    preInspectionScriptPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    preInspectionPanel.add(_preInspectionScriptCheckBox, BorderLayout.NORTH);
    preInspectionPanel.add(preInspectionScriptPanel, BorderLayout.CENTER);

    JPanel postInspectionPanel = new JPanel(new BorderLayout(0, -5));
    JPanel postInspectionScriptPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    postInspectionScriptPanel.add(postInspectionPathLabel);
    postInspectionScriptPanel.add(_postInspectionScriptTextField);
    postInspectionScriptPanel.add(_postInspectionScriptButton);
    postInspectionScriptPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 0));
    postInspectionPanel.add(_postInspectionScriptCheckBox, BorderLayout.NORTH);
    postInspectionPanel.add(postInspectionScriptPanel, BorderLayout.CENTER);
    
    _preInspectionScriptCustomizeOutputTable = new PreOrPostInspectionScriptCustomizeInputTable(this);
    _preInspectionScriptCustomizeOutputTableModel = new PreOrPostInspectionScriptCustomizeInputTableModel(this, false);
    _preInspectionScriptCustomizeOutputTable.setTableModel(_preInspectionScriptCustomizeOutputTableModel);
    _preInspectionScriptCustomizeOutputTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _preInspectionScriptCustomizeOutputTable.getTableHeader().setReorderingAllowed(false);
    _preInspectionScriptCustomizeOutputTable.setPreferredScrollableViewportSize(new Dimension(30, 120));
    _preInspectionScriptCustomizeOutputTable.setCustomizeConfigPath(FileName.getPreInspectionScriptOutputConfigFullPath());
    
    _preInspectionScriptCustomizeOutputTableModel.setTable(_preInspectionScriptCustomizeOutputTable);
    
    final ListSelectionModel preInspectionCustomizeRowOutputSelectionModel = _preInspectionScriptCustomizeOutputTable.getSelectionModel();
    preInspectionCustomizeRowOutputSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
      }
    });
    
    _preInspectionScriptCustomizeInputTable = new PreOrPostInspectionScriptCustomizeInputTable(this);
    _preInspectionScriptCustomizeInputTableModel = new PreOrPostInspectionScriptCustomizeInputTableModel(this , true);
    _preInspectionScriptCustomizeInputTable.setTableModel(_preInspectionScriptCustomizeInputTableModel);
    _preInspectionScriptCustomizeInputTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _preInspectionScriptCustomizeInputTable.getTableHeader().setReorderingAllowed(false);
    _preInspectionScriptCustomizeInputTable.setPreferredScrollableViewportSize(new Dimension(30, 120));
    _preInspectionScriptCustomizeInputTable.setCustomizeConfigPath(FileName.getPreInspectionScriptInputConfigFullPath());
    
    _preInspectionScriptCustomizeInputTableModel.setTable(_preInspectionScriptCustomizeInputTable);
    
    final ListSelectionModel preInspectionCustomizeRowSelectionModel = _preInspectionScriptCustomizeInputTable.getSelectionModel();
    preInspectionCustomizeRowSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
      }
    });
    
    _preInsepctionScriptCustomizeInputScrollPane.getViewport().add(_preInspectionScriptCustomizeInputTable);
    _preInsepctionScriptCustomizeOutputScrollPane.getViewport().add(_preInspectionScriptCustomizeOutputTable);
    
    preInspectionScriptCustomizeOutputPanel.add(_preInsepctionScriptCustomizeOutputScrollPane, BorderLayout.CENTER);
    preInspectionScriptCustomizeOutputPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
    preInspectionScriptCustomizeOutputPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                                 StringLocalizer.keyToString("CFGUI_CUSTOMIZE_PRE_INSPECTION_OUTPUT_PARAMETER_KEY")));
    
    preInspectionScriptCustomizeInputPanel.add(_preInsepctionScriptCustomizeInputScrollPane, BorderLayout.CENTER);
    preInspectionScriptCustomizeInputPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
    preInspectionScriptCustomizeInputPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(166, 166, 166)),
                                                                 StringLocalizer.keyToString("CFGUI_CUSTOMIZE_PRE_INSPECTION_INPUT_PARAMETER_KEY")));
    
    JPanel customizePreInspectionScriptOutput = new JPanel();
    customizePreInspectionScriptOutput.setLayout(new PairLayout());
    customizePreInspectionScriptOutput.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
    
    _group.add(_panelBasedPreInspectionScriptOutputChkBox);
    _group.add(_boardBasedPreInspectionScriptOutputChkBox);
    customizePreInspectionScriptOutput.add(_panelBasedPreInspectionScriptOutputChkBox);
    customizePreInspectionScriptOutput.add(_boardBasedPreInspectionScriptOutputChkBox);
    
    _panelBasedPreInspectionScriptOutputChkBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectPanelOrBoardBaseSerialNumberCheckBox_actionPerformed();
      }
    });
    
    _boardBasedPreInspectionScriptOutputChkBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectPanelOrBoardBaseSerialNumberCheckBox_actionPerformed();
      }
    });
    
    _panelBasedPreInspectionScriptOutputChkBox.setSelected(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PRE_PANEL_SERIAL_NUMBER_INSPECTION_SCRIPT));
    _boardBasedPreInspectionScriptOutputChkBox.setSelected(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PRE_BOARD_SERIAL_NUMBER_INSPECTION_SCRIPT));
    JPanel customizeInputOutputWrapperPanel = new JPanel();
    customizeInputOutputWrapperPanel.setLayout(new BorderLayout());
    customizeInputOutputWrapperPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));
    customizeInputOutputWrapperPanel.add(preInspectionScriptCustomizeOutputPanel, BorderLayout.SOUTH);
    customizeInputOutputWrapperPanel.add(preInspectionScriptCustomizeInputPanel, BorderLayout.CENTER);
    customizeInputOutputWrapperPanel.add(customizePreInspectionScriptOutput, BorderLayout.NORTH);
    
    scriptSetupPanel.add(preInspectionPanel, BorderLayout.NORTH);
    scriptSetupPanel.add(postInspectionPanel, BorderLayout.CENTER);
    scriptSetupPanel.add(customizeInputOutputWrapperPanel, BorderLayout.SOUTH);

    JPanel[] allPanels = new JPanel[]{productionOptionsPanel,
                         samplingOptionsPanel,
                         panelHandlingPanel,
                         repairImagesSaveLimitPanel,
                         scriptSetupPanel};
    SwingUtils.makeAllSameLength(allPanels);

    _ejectPanelOnAlignmentFailureCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ejectPanelOnAlignmentFailureCheckBox_actionPerformed();
      }
    });


    // add in all the action listeners
    _demoModeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        demoModeCheckBox_actionPerformed();
      }
    });

    _bypassModeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bypassModeCheckBox_actionPerformed();
      }
    });
    
    _bypassWithSerialControlModeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bypassWithSerialControlModeCheckBox_actionPerformed();
      }
    });
    
    _bypassWithResultsCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bypassWithResultsCheckBox_actionPerformed();
      }
    });
    
     _semiAutomatedModeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        semiAutomatedModeCheckBox_actionPerformed();
      }
    });

    _enableBuzzerCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableBuzzerCheckBox_actionPerformed();
      }
    });

    _enableXoutDetectionCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableXoutDetectionCheckBox_actionPerformed();
      }
    });

    _useAlignmentFailedXoutDetectionRadioButton.addItemListener(new ItemListener()
    {
      @Override
      public void itemStateChanged(ItemEvent e)
      {
        switchXoutDetectionMethodRadioButton_actionPerformed();
      }
    });

    _useDefectThresholdXoutDetectionRadioButton.addItemListener(new ItemListener()
    {
      @Override
      public void itemStateChanged(ItemEvent e)
      {
        switchXoutDetectionMethodRadioButton_actionPerformed();
      }
    });

    _xoutDetectionThresholdTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        xoutDetectionThresholdTextField();
      }
    });

    _xoutDetectionThresholdTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        xoutDetectionThresholdTextField();
      }
    });

    _confirmationAndAdjustmentModeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        confirmationAndAdjustmentMode_actionPerformed();
      }
    });
    
    _panelSamplingCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelSamplingCheckBox_actionPerformed();
      }
    });

    _panelSamplingTestEveryNthTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        panelSamplingTestTextField();
      }
    });

    _panelSamplingTestEveryNthTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelSamplingTestTextField();
      }
    });
    
    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    _xoutDetectionFailureThresholdTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        xoutDetectionFailureThresholdTextField();
      }
    });

    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    _xoutDetectionFailureThresholdTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        xoutDetectionFailureThresholdTextField();
      }
    });

    _panelSamplingSkipEveryNthTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        panelSamplingSkipTextField();
      }
    });

    _panelSamplingSkipEveryNthTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelSamplingSkipTextField();
      }
    });

    _panelSamplingTestRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelSamplingTestRadioButton_actionPerformed();
      }
    });

    _panelSamplingSkipRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelSamplingSkipRadioButton_actionPerformed();
      }
    });

    _smemaCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        smemaCheckBox_actionPerformed();
      }
    });

    /**
     * @author Chnee Khang Wah (12-Oct-2009)
     */
    _ignorePIPCheckBox.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            ignorePIPCheckBox_actionPerformed();
        }
    });
    
    _checkSmemaSensorSignalDuringUnloading.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
            checkSmemaSensorSignalDuringUnloadingCheckBox_actionPerformed();
        }
    });

    _panelLoadingModeComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelLoadingMode_actionPerformed();
      }
    });

    _panelFlowDirectionComboBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        panelFlowDirection_actionPerformed();
      }
    });

    _saveFocusConfirmationRepairInfoCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        saveFocusConfirmationRepairInfoCheckbox_actionPerformed();
      }
    });

    _limitNumRepairImagesToSaveCheckbox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        limitNumRepairImagesToSaveCheckbox_actionPerformed();
      }
    });

    _numRepairImagesToSaveTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        numRepairImagesToSaveTextField();
      }
    });

    _numRepairImagesToSaveTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        numRepairImagesToSaveTextField();
      }
    });
    
    _repairImageTypeJPGRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairImageTypeRadioButton_actionListener();
      }
    });
    
    _repairImageTypePNGRadioButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        repairImageTypeRadioButton_actionListener();
      }
    });
    
    // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
//    _generateMultiAngleImagesCheckbox.addActionListener(new ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        generateMultiAngleImagesCheckbox_actionPerformed();
//      }
//    });

    _preInspectionScriptCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        preInspectionScriptCheckBox_actionPerformed();
      }
    });

    _preInspectionScriptTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        preInspectionScriptTextField_setValue();
      }
    });

    _preInspectionScriptTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        preInspectionScriptTextField_setValue();
      }
    });

    _preInspectionScriptButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        preInspectionScriptChooser();
      }
    });

    _postInspectionScriptCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        postInspectionScriptCheckBox_actionPerformed();
      }
    });

    _postInspectionScriptTextField.addFocusListener(new FocusAdapter()
    {
      public void focusLost(FocusEvent e)
      {
        postInspectionScriptTextField_setValue();
      }
    });

    _postInspectionScriptTextField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        postInspectionScriptTextField_setValue();
      }
    });

    _postInspectionScriptButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        postInspectionScriptChooser();
      }
    });

    // define menus and toolBar items to be added later

//    _setDefaultMenuItem.setText("Set &Default User Config");
//    _setDefaultMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        setDefault(e);
//      }
//    });
//
//    _configHelpMenuItem.setText("&Config User Help");
//    _configHelpMenuItem.addActionListener(new java.awt.event.ActionListener()
//    {
//      public void actionPerformed(ActionEvent e)
//      {
//        configHelp(e);
//      }
//    });

    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    _displayProductionPanelLoadingUnloadingTimeCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        displayProductionPanelLoadingUnloadingTimeCheckBox_actionPerformed();
      }
    });

    _alwaysDisplayVariationNameCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        alwaysDisplayVariationNameCheckBox_actionPerformed();
      }
    });
    
    _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableSetPanelAvailableAfterDownStreamAcceptPanelActionPerformed();
      }
    });
    
    _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableSetRemainOuterBarrierOpenForNextInspectionActionPerformed();
      }
    });
    
    _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        enableSetOpenOuterBarrierBeforeImageClassificationFinishActionPerformed();
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateDataOnScreen()
  {
    _ejectPanelOnAlignmentFailureCheckBox.setSelected(_testExecution.shouldPanelBeEjectedIfAlignmentFails());
    _demoModeCheckBox.setSelected(_testExecution.isDemoModeEnabled());
    _bypassModeCheckBox.setSelected(_testExecution.isBypassModeEnabled());
    _bypassWithSerialControlModeCheckBox.setSelected(_testExecution.isBypassWithSerialControlModeEnabled());
    _bypassWithSerialControlModeCheckBox.setEnabled(_bypassModeCheckBox.isSelected());
    _bypassWithResultsCheckBox.setSelected(_testExecution.isBypassWithResultsEnabled());
    _semiAutomatedModeCheckBox.setSelected(_testExecution.isSemiAutomatedModeEnabled()); //Siew Yeng - XCR1745 - Semi-automated mode
    _alwaysDisplayVariationNameCheckBox.setSelected(_testExecution.isAlwaysDisplayVariationNameMode());
    //Khaw Chek Hau - XCR3774: Display Panel Loading and Unloading time during production
    _displayProductionPanelLoadingUnloadingTimeCheckBox.setSelected(_testExecution.isDisplayProductionPanelLoadingAndUnloadingTimeMode());
    _enableBuzzerCheckBox.setSelected(_lightStack.isBuzzerEnabled());
    _enableXoutDetectionCheckBox.setSelected(_testExecution.isXoutDetectionModeEnabled());

    if( _testExecution.getXoutDetectionMethod().equals(XoutDetectionMethodEnum.ALIGNMENT_FAILURE) )
      _useAlignmentFailedXoutDetectionRadioButton.setSelected(true);
    else if( _testExecution.getXoutDetectionMethod().equals(XoutDetectionMethodEnum.DEFECT_THRESHOLD))
      _useDefectThresholdXoutDetectionRadioButton.setSelected(true);

    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting    
    if (_enableXoutDetectionCheckBox.isSelected() && _useDefectThresholdXoutDetectionRadioButton.isSelected())
    {
      _xoutDetectionFailureThresholdTextField.setEnabled(true);
    }
    else
    {
      _xoutDetectionFailureThresholdTextField.setEnabled(false);
    }
    _xoutDetectionFailureThresholdTextField.setValue(_testExecution.getXoutDetectionThreshold());

    _useAlignmentFailedXoutDetectionRadioButton.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _useDefectThresholdXoutDetectionRadioButton.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _xoutDetectionLabel.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _xoutDetectionThresholdTextField.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _xoutDetectionThresholdTextField.setValue(_testExecution.getXoutDetectionThreshold());

    //Confirmation And Adjustmend Mode
    if(Config.getInstance().getStringValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE).equals(ConfirmationAndAdjustmentModeEnum.STANDARD_MODE.toString()))
      _confirmationAndAdjustmentModeComboBox.setSelectedItem(_standardModeString);
    else if(Config.getInstance().getStringValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE).equals(ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE.toString()))
      _confirmationAndAdjustmentModeComboBox.setSelectedItem(_passiveModeString);
    else
      Assert.expect(false);
    
    PanelSamplingModeEnum panelSamplingModeEnum = _sampling.getPanelSamplingModeEnum();
    _skipPanelLabel.setEnabled(_panelSamplingCheckBox.isSelected());
    _testPanelLabel.setEnabled(_panelSamplingCheckBox.isSelected());
    _ORLabel.setEnabled(_panelSamplingCheckBox.isSelected());
    if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
    {
      _panelSamplingCheckBox.setSelected(false);
      _panelSamplingTestEveryNthTextField.setEnabled(false);
      _panelSamplingSkipEveryNthTextField.setEnabled(false);
      _panelSamplingSkipRadioButton.setEnabled(false);
      _panelSamplingTestRadioButton.setEnabled(false);
    }
    else
    {
      _panelSamplingCheckBox.setSelected(true);
      _panelSamplingTestEveryNthTextField.setEnabled(true);
      _panelSamplingSkipEveryNthTextField.setEnabled(true);
      if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
      {
        _panelSamplingSkipRadioButton.setEnabled(true);
        _panelSamplingTestRadioButton.setEnabled(true);
        _panelSamplingSkipRadioButton.setSelected(true);
        _panelSamplingTestEveryNthTextField.setEnabled(false);
      }
      else
      {
        _panelSamplingSkipRadioButton.setEnabled(true);
        _panelSamplingTestRadioButton.setEnabled(true);
        _panelSamplingTestRadioButton.setSelected(true);
        _panelSamplingSkipEveryNthTextField.setEnabled(false);
      }
    }
    _panelSamplingTestEveryNthTextField.setValue(_sampling.getTestEveryNthPanel());
    _panelSamplingSkipEveryNthTextField.setValue(_sampling.getSkipEveryNthPanel());

    _smemaCheckBox.setSelected(_panelHandler.isInLineModeEnabled());
    _checkSmemaSensorSignalDuringUnloading.setSelected(_panelHandler.isCheckSmemaSensorSignalDuringUnloadingModeEnabled());
    _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.setSelected(_panelHandler.isPanelAvailableAfterDownStreamAcceptPanelModeEnabled());    
    
    /*Khang Wah (12-Oct-2009)*/
    _ignorePIPCheckBox.setSelected(_panelHandler.isIgnorePIPModeEnabled());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
         _ignorePIPCheckBox.setEnabled(false);
    }
    
    _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setSelected(_panelHandler.isEnabledKeepOuterBarrierOpenForLoadingAndUnloadingPanel());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
         _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setEnabled(false);
    }
    
    _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setSelected(_panelHandler.isEnabledOuterBarrierOpenAfterCompleteScanningForS2EX());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
         _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setEnabled(false);
    }
    _checkSmemaSensorSignalDuringUnloading.setSelected(_panelHandler.isCheckSmemaSensorSignalDuringUnloadingModeEnabled());
    if(_panelHandler.isInLineModeEnabled()==false)
    {
      _checkSmemaSensorSignalDuringUnloading.setEnabled(false);
    }

    PanelHandlerPanelLoadingModeEnum loadingMode = _panelHandler.getPanelLoadingMode();
    if (loadingMode.equals(PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH))
      _panelLoadingModeComboBox.setSelectedItem(_flowThroughString);
    else if (loadingMode.equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))
      _panelLoadingModeComboBox.setSelectedItem(_passBackString);
    else
      Assert.expect(false);

    PanelHandlerPanelFlowDirectionEnum flowMode = _panelHandler.getPanelFlowDirection();
    if (flowMode.equals(PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT))
      _panelFlowDirectionComboBox.setSelectedItem(_leftToRightString);
    else if (flowMode.equals(PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT))
      _panelFlowDirectionComboBox.setSelectedItem(_rightToLeftString);
    else
      Assert.expect(false);

    // Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
    //_generateMultiAngleImagesCheckbox.setSelected(_repairManager.isGenerateMultiAngleImagesEnabled());
    
    _saveFocusConfirmationRepairInfoCheckbox.setSelected(_repairManager.isSaveFocusConfirmationRepairInfoEnabled());
    _limitNumRepairImagesToSaveCheckbox.setSelected(_repairManager.isLimitRepairImagesEnabled());
    _numRepairImagesToSaveTextField.setValue(_repairManager.getNumRepairImagesToSave());
    _numRepairImagesToSaveTextField.setEnabled(_limitNumRepairImagesToSaveCheckbox.isSelected());
    
    // XCR-2465 Ability to transfer PNG image to VVTS - Cheah Lee Herng
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT)  == 0)
    {
      _repairImageTypePNGRadioButton.setSelected(true);
    }
    else if (Config.getInstance().getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT)  == 2)
    {
      _repairImageTypeJPGRadioButton.setSelected(true);
    }

    _preInspectionScriptCheckBox.setSelected(_scriptRunner.isPreInspectionScriptEnabled());
    _preInspectionScriptTextField.setText(_scriptRunner.getPreInspectionScript());

    _preInspectionScriptButton.setEnabled(_preInspectionScriptCheckBox.isSelected());
    _preInspectionScriptTextField.setEnabled(_preInspectionScriptCheckBox.isSelected());

    _postInspectionScriptCheckBox.setSelected(_scriptRunner.isPostInspectionScriptEnabled());
    _postInspectionScriptTextField.setText(_scriptRunner.getPostInspectionScript());

    _postInspectionScriptButton.setEnabled(_postInspectionScriptCheckBox.isSelected());
    _postInspectionScriptTextField.setEnabled(_postInspectionScriptCheckBox.isSelected());
  }
  
  /**
   * @author Kee Chin Seong
   */
  private void selectPanelOrBoardBaseSerialNumberCheckBox_actionPerformed()
  {
    try
    {
       Config.getInstance().setValue(SoftwareConfigEnum.ENABLE_PRE_PANEL_SERIAL_NUMBER_INSPECTION_SCRIPT, _panelBasedPreInspectionScriptOutputChkBox.isSelected());
       Config.getInstance().setValue(SoftwareConfigEnum.ENABLE_PRE_BOARD_SERIAL_NUMBER_INSPECTION_SCRIPT, _boardBasedPreInspectionScriptOutputChkBox.isSelected());
    }
    catch(DatastoreException ex)
    {
    } 
  }

  /**
   * @author Andy Mechtenberg
   */
  private void ejectPanelOnAlignmentFailureCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetEjectPanelOnAligmnentFailureCommand setAlignmentFailureCommand = new ConfigSetEjectPanelOnAligmnentFailureCommand(_ejectPanelOnAlignmentFailureCheckBox.isSelected());
      _commandManager.execute(setAlignmentFailureCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void demoModeCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetDemoModeCommand setDemoModeCommand = new ConfigSetDemoModeCommand(_demoModeCheckBox.isSelected());
      _commandManager.execute(setDemoModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bypassModeCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SET_BYPASS_MODE_KEY"));
      ConfigSetBypassModeCommand setBypassModeCommand = new ConfigSetBypassModeCommand(_bypassModeCheckBox.isSelected());
      _commandManager.execute(setBypassModeCommand);
      if (_bypassModeCheckBox.isSelected() == false)
      {
        if (_bypassWithSerialControlModeCheckBox.isSelected())
        {
          ConfigSetBypassWithSerialControlModeCommand setBypassWithSerialControlModeCommand = new ConfigSetBypassWithSerialControlModeCommand(false);
          _commandManager.execute(setBypassWithSerialControlModeCommand);
        }
      }
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
    finally
    {
      _commandManager.endCommandBlockIfNecessary();
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void bypassWithSerialControlModeCheckBox_actionPerformed()
  {
    try
    {
      _commandManager.beginCommandBlock(StringLocalizer.keyToString("GUI_COMMAND_SET_BYPASS_MODE_KEY"));
      ConfigSetBypassWithSerialControlModeCommand setBypassModeCommand = new ConfigSetBypassWithSerialControlModeCommand(_bypassWithSerialControlModeCheckBox.isSelected());
      _commandManager.execute(setBypassModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
    finally
    {
      _commandManager.endCommandBlockIfNecessary();
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void bypassWithResultsCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetBypassWithResultsCommand setBypassModeCommand = new ConfigSetBypassWithResultsCommand(_bypassWithResultsCheckBox.isSelected());
      _commandManager.execute(setBypassModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  private void displayProductionPanelLoadingUnloadingTimeCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetDisplayProductionPanelLoadingAndUnloadingTimeCommand setDisplayProductionPanelLoadingAndUnloadingTimeCommand = new ConfigSetDisplayProductionPanelLoadingAndUnloadingTimeCommand(_displayProductionPanelLoadingUnloadingTimeCheckBox.isSelected());
      _commandManager.execute(setDisplayProductionPanelLoadingAndUnloadingTimeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void alwaysDisplayVariationNameCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetAlwaysDisplayVariationNameCommand setAlwaysDisplayVariationNameModeCommand = new ConfigSetAlwaysDisplayVariationNameCommand(_alwaysDisplayVariationNameCheckBox.isSelected());
      _commandManager.execute(setAlwaysDisplayVariationNameModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void semiAutomatedModeCheckBox_actionPerformed()
  {
    try
    {
      boolean isSemiAutomatedSelected = _semiAutomatedModeCheckBox.isSelected();
      
      if(Project.isCurrentProjectLoaded())
      {
        if(_mainUI.getTestDev().okToCloseProject() == false && _mainUI.getTestDev().areClosingProject() == false)
        {
          if(isSemiAutomatedSelected)
            _semiAutomatedModeCheckBox.setSelected(false);
          else
            _semiAutomatedModeCheckBox.setSelected(true);
          
          return;
        }
        else
          _mainUI.getMainMenuBar().projectClose(true);
      }
      
      ConfigSetSemiAutomatedModeCommand setSemiAutomatedModeCommand = new ConfigSetSemiAutomatedModeCommand(isSemiAutomatedSelected);
      _commandManager.execute(setSemiAutomatedModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void enableBuzzerCheckBox_actionPerformed()
  {
    try
    {
      HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws XrayTesterException
        {
          LightStack lightStack = LightStack.getInstance();
          if (lightStack.isStartupRequired() == false)
            lightStack.turnBuzzerOff();
        }
      });

      ConfigEnableBuzzerCommand setEnableBuzzerCommand = new ConfigEnableBuzzerCommand(_enableBuzzerCheckBox.isSelected());
      _commandManager.execute(setEnableBuzzerCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void enableXoutDetectionCheckBox_actionPerformed()
  {
    _xoutDetectionThresholdTextField.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _useAlignmentFailedXoutDetectionRadioButton.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    _useDefectThresholdXoutDetectionRadioButton.setEnabled(_enableXoutDetectionCheckBox.isSelected());
    
    try
    {
      ConfigEnableXoutDetectionCommand setEnableXoutDetectionCommand = new ConfigEnableXoutDetectionCommand(_enableXoutDetectionCheckBox.isSelected());
      _commandManager.execute(setEnableXoutDetectionCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void switchXoutDetectionMethodRadioButton_actionPerformed()
  {
    XoutDetectionMethodEnum xoutDetectionMethod = null;
    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    if( _useAlignmentFailedXoutDetectionRadioButton.isSelected())
    {
      xoutDetectionMethod = XoutDetectionMethodEnum.ALIGNMENT_FAILURE;
      _xoutDetectionFailureThresholdTextField.setEnabled(false);
    }
    if(_useDefectThresholdXoutDetectionRadioButton.isSelected())
    {
      xoutDetectionMethod = XoutDetectionMethodEnum.DEFECT_THRESHOLD;
      _xoutDetectionFailureThresholdTextField.setEnabled(true);
    }

    if(xoutDetectionMethod == null)
      return;

    try
    {
      ConfigXoutDetectionMethodCommand configXoutDetectionMethodCommand = new ConfigXoutDetectionMethodCommand(xoutDetectionMethod);
      _commandManager.execute(configXoutDetectionMethodCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura COrmos
   */
  private void xoutDetectionThresholdTextField()
  {
    Long val;
    try
    {
      val = _xoutDetectionThresholdTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_XOUT_DETECTION_THRESHOLD);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_XOUT_DETECTION_THRESHOLD);

    try
    {
      _commandManager.execute(new ConfigSetXoutDetectionThresholdCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /*
  * Author Kee Chin Seong - Setting SMEMA Seuqence mode
  */
  private void enableSetPanelAvailableAfterDownStreamAcceptPanelActionPerformed()
  {
    try
    {
      ConfigSetPanelAvailableAfterDownStreamAcceptPanelCommand setCheckPanelAvailableAfterDownStreamAcceptPanelCommand = new ConfigSetPanelAvailableAfterDownStreamAcceptPanelCommand(_enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.isSelected());
      _commandManager.execute(setCheckPanelAvailableAfterDownStreamAcceptPanelCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  private void enableSetRemainOuterBarrierOpenForNextInspectionActionPerformed()
  {
    try
    {
      ConfigSetKeepOuterBarrierOpenForLoadingAndUnloadingPanelCommand setRemainOuterBarrierOpenForNextInspectionCommand = new ConfigSetKeepOuterBarrierOpenForLoadingAndUnloadingPanelCommand(_keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.isSelected());
      _commandManager.execute(setRemainOuterBarrierOpenForNextInspectionCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  private void enableSetOpenOuterBarrierBeforeImageClassificationFinishActionPerformed()
  {
    try
    {
      ConfigSetKeepOuterBarrierOpenAfterScannngCompleteCommand setOpenOuterBarrierBeforeImageClassificationFinishCommand = new ConfigSetKeepOuterBarrierOpenAfterScannngCompleteCommand(_keepOuterBarrierOpenAfterScanningCompeleteCheckBox.isSelected());
      _commandManager.execute(setOpenOuterBarrierBeforeImageClassificationFinishCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private void confirmationAndAdjustmentMode_actionPerformed()
  {
    ConfirmationAndAdjustmentModeEnum systemConfirmationAndAdjustmentMode = null;
    
    String selectedString = (String)_confirmationAndAdjustmentModeComboBox.getSelectedItem();
    if(selectedString.equals(_standardModeString))
      systemConfirmationAndAdjustmentMode = ConfirmationAndAdjustmentModeEnum.STANDARD_MODE;
    else if(selectedString.equals(_passiveModeString))
      systemConfirmationAndAdjustmentMode = ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE;
    else
      Assert.expect(false, "Unexpected value: " + selectedString);
    
    try
    {
       ConfigSetConfirmationAndAdjustmentModeCommand setProductionModeCommand = new ConfigSetConfirmationAndAdjustmentModeCommand(systemConfirmationAndAdjustmentMode);
      _commandManager.execute(setProductionModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void panelSamplingCheckBox_actionPerformed()
  {
    _panelSamplingTestEveryNthTextField.setEnabled(_panelSamplingCheckBox.isSelected());
    _panelSamplingSkipEveryNthTextField.setEnabled(_panelSamplingCheckBox.isSelected());
    _skipPanelLabel.setEnabled(_panelSamplingCheckBox.isSelected());
    _testPanelLabel.setEnabled(_panelSamplingCheckBox.isSelected());
    _ORLabel.setEnabled(_panelSamplingCheckBox.isSelected());

    boolean testEveryNth = _panelSamplingTestRadioButton.isSelected();
    boolean skipEveryNth = _panelSamplingSkipRadioButton.isSelected();
    PanelSamplingModeEnum sampleMode = PanelSamplingModeEnum.NO_SAMPLING;

    if (_panelSamplingCheckBox.isSelected())
    {
      if (testEveryNth)
        sampleMode = PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL;
      else if (skipEveryNth)
        sampleMode = PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL;
      else
        Assert.expect(false, "Neither panel sampling radio button is selected");
    }
    try
    {
      _commandManager.execute(new ConfigSetPanelSamplingModeCommand(sampleMode));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void panelSamplingTestRadioButton_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSetPanelSamplingModeCommand(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void panelSamplingSkipRadioButton_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSetPanelSamplingModeCommand(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void panelSamplingSkipTextField()
  {
    Long val;
    try
    {
      val = _panelSamplingSkipEveryNthTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_PANEL_SAMPLING);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_PANEL_SAMPLING);

    try
    {
      _commandManager.execute(new ConfigSetPanelSamplingSkipPanelsCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void panelSamplingTestTextField()
  {
    Long val;
    try
    {
      val = _panelSamplingTestEveryNthTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_PANEL_SAMPLING);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
      val = new Long(_MIN_PANEL_SAMPLING);

    try
    {
      _commandManager.execute(new ConfigSetPanelSamplingTestPanelsCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void smemaCheckBox_actionPerformed()
  {
    try
    {
        /*Khang Wah (12-Oct-2009)*/
        boolean isSmemaSelected = _smemaCheckBox.isSelected();
        _ignorePIPCheckBox.setEnabled(isSmemaSelected);
        _enableSetPanelAvailableAfterDownStreamAcceptPanelChkBox.setEnabled(isSmemaSelected);
        _checkSmemaSensorSignalDuringUnloading.setEnabled(isSmemaSelected);
        _keepOuterBarrierOpenForLoadingAndUnloadingPanelCheckBox.setEnabled(isSmemaSelected);
        _keepOuterBarrierOpenAfterScanningCompeleteCheckBox.setEnabled(isSmemaSelected);
        
        ConfigSetSmemaModeCommand setSmemaCommand = new ConfigSetSmemaModeCommand(isSmemaSelected);
        _commandManager.execute(setSmemaCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Chnee Khang Wah (12-Oct-2009)
   */
  private void ignorePIPCheckBox_actionPerformed()
  {
      try
    {
      ConfigSetIgnorePIPModeCommand setIgnorePIPCommand = new ConfigSetIgnorePIPModeCommand(_ignorePIPCheckBox.isSelected());
      _commandManager.execute(setIgnorePIPCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void checkSmemaSensorSignalDuringUnloadingCheckBox_actionPerformed()
  {
    try
    {
      ConfigSetCheckSmemaSensorSignalDuringUnloadingCommand setCheckSensorSignalDuringUnloadingCommand = new ConfigSetCheckSmemaSensorSignalDuringUnloadingCommand(_checkSmemaSensorSignalDuringUnloading.isSelected());
      _commandManager.execute(setCheckSensorSignalDuringUnloadingCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void panelLoadingMode_actionPerformed()
  {
    PanelHandlerPanelLoadingModeEnum loadingMode = null;
    // the combobox contains a string, so we need to convert that to the enum
    String selectedString = (String)_panelLoadingModeComboBox.getSelectedItem();
    if (selectedString.equals(_flowThroughString))
      loadingMode = PanelHandlerPanelLoadingModeEnum.FLOW_THROUGH;
    else if (selectedString.equals(_passBackString))
      loadingMode = PanelHandlerPanelLoadingModeEnum.PASS_BACK;
    else
      Assert.expect(false, "Unexpected value: " + selectedString);
    try
    {
      ConfigSetPanelLoadingModeCommand setLoadingModeCommand = new ConfigSetPanelLoadingModeCommand(loadingMode);
      _commandManager.execute(setLoadingModeCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void panelFlowDirection_actionPerformed()
  {
    PanelHandlerPanelFlowDirectionEnum flowMode = null;
    // the combobox contains a string, so we need to convert that to the enum
    String selectedString = (String)_panelFlowDirectionComboBox.getSelectedItem();
    if (selectedString.equals(_leftToRightString))
      flowMode = PanelHandlerPanelFlowDirectionEnum.LEFT_TO_RIGHT;
    else if (selectedString.equals(_rightToLeftString))
      flowMode = PanelHandlerPanelFlowDirectionEnum.RIGHT_TO_LEFT;
    else
      Assert.expect(false, "Unexpected value: " + selectedString);
    try
    {
      ConfigSetPanelFlowDirectionCommand setFlowDirectionCommand = new ConfigSetPanelFlowDirectionCommand(flowMode);
      _commandManager.execute(setFlowDirectionCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void saveFocusConfirmationRepairInfoCheckbox_actionPerformed()
  {
    try
    {
      _commandManager.execute(new ConfigSetSaveFocusConfirmationRepairInfoCommand(_saveFocusConfirmationRepairInfoCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Laura Cormos
   */
  private void limitNumRepairImagesToSaveCheckbox_actionPerformed()
  {
    _numRepairImagesToSaveTextField.setEnabled(_limitNumRepairImagesToSaveCheckbox.isSelected());
    try
    {
      _commandManager.execute(new ConfigSetLimitNumRepairImagesToSaveCommand(_limitNumRepairImagesToSaveCheckbox.isSelected()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
    * @author Laura Cormos
    */
   private void numRepairImagesToSaveTextField()
   {
     Long val;
     try
     {
       val = _numRepairImagesToSaveTextField.getLongValue();
     }
     catch (ParseException pex)
     {
       val = new Long(0);
     }

     try
     {
       _commandManager.execute(new ConfigSetNumRepairImagesToSaveCommand(val.intValue()));
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     ex,
                                     StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                     true);
     }
  }
   
   /**
    * @author Cheah Lee Herng
    */   
   private void repairImageTypeRadioButton_actionListener()
   {
     int imageType = 0;
     if(_repairImageTypePNGRadioButton.isSelected())
     {
       imageType = SoftwareConfigEnum.getPngImageType();
     }
     if(_repairImageTypeJPGRadioButton.isSelected())
     {
       imageType = SoftwareConfigEnum.getJpegImageType();
     }
     
     try
     {
       ConfigSetRepairImageTypeCommand setRepairImageTypeCommand = new ConfigSetRepairImageTypeCommand(imageType);
       _commandManager.execute(setRepairImageTypeCommand);
     }
     catch (XrayTesterException ex)
     {
       MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                     ex,
                                     StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                     true);
     }
   }

   /**
   * @author Chnee Khang Wah, 2011-12-13, 2.5D HIP Development
   */
//  private void generateMultiAngleImagesCheckbox_actionPerformed()
//  {
//    try
//    {
//      boolean current = false;
//      if(Project.isCurrentProjectLoaded())
//          current = Project.getCurrentlyLoadedProject().getTestProgram().getTestProgramGenerationData().isGenerateMultiAngleImage();
//          
//      _commandManager.execute(new ConfigSetGenerateMultiAngleImagesCommand(_generateMultiAngleImagesCheckbox.isSelected()));
//      
//      // A popup dialog box to remaind user to re-load their current recipe
//      if(Project.isCurrentProjectLoaded())
//      {
//          if (_generateMultiAngleImagesCheckbox.isSelected() != current)
//          {
//              SwingUtils.invokeLater(new Runnable()
//              {
//                public void run()
//                {
//                  LocalizedString savingStr = new LocalizedString("MMGUI_HOME_RELOAD_RECIPE_TO_MAKE_CHANGE_KEY", 
//                                                              new Object[]{Project.getCurrentlyLoadedProject().getName()});
//                  MessageDialog.showInformationDialog(MainMenuGui.getInstance(), 
//                                                      StringLocalizer.keyToString(savingStr), 
//                                                      StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"));
//                }
//              });
//          }
//      }
//    }
//    catch (XrayTesterException ex)
//    {
//      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
//                                    ex,
//                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
//                                    true);
//    }
//  }
  
  /**
   * @author Andy Mechtenberg
   */
  private void preInspectionScriptCheckBox_actionPerformed()
  {
    _preInspectionScriptButton.setEnabled(_preInspectionScriptCheckBox.isSelected());
    _preInspectionScriptTextField.setEnabled(_preInspectionScriptCheckBox.isSelected());

    try
    {
      ConfigEnablePreInspectionScriptCommand enablePreScriptCommand = new ConfigEnablePreInspectionScriptCommand(_preInspectionScriptCheckBox.isSelected());
      _commandManager.execute(enablePreScriptCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void preInspectionScriptTextField_setValue()
  {
    try
    {
      ConfigSetPreInspectionScriptCommand setPreInspectionScriptCommand = new ConfigSetPreInspectionScriptCommand(_preInspectionScriptTextField.getText());
      _commandManager.execute(setPreInspectionScriptCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void preInspectionScriptChooser()
  {
    String currentScript = _preInspectionScriptTextField.getText();
    if (currentScript.length() == 0)
      currentScript = Directory.getInspectionDataDir();
    JFileChooser fileChooser = new JFileChooser(currentScript);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isFile())
    {
      String fileName = selectedFile.getPath();
      _preInspectionScriptTextField.setText(fileName);
      _preInspectionScriptTextField.requestFocus();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void postInspectionScriptChooser()
  {
    String currentScript = _postInspectionScriptTextField.getText();
    if (currentScript.length() == 0)
      currentScript = Directory.getInspectionDataDir();
    JFileChooser fileChooser = new JFileChooser(currentScript);

    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    if (fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION)
      return;
    File selectedFile = fileChooser.getSelectedFile();
    if (selectedFile.isFile())
    {
      String fileName = selectedFile.getPath();
      _postInspectionScriptTextField.setText(fileName);
      _postInspectionScriptTextField.requestFocus();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void postInspectionScriptCheckBox_actionPerformed()
  {
    _postInspectionScriptButton.setEnabled(_postInspectionScriptCheckBox.isSelected());
    _postInspectionScriptTextField.setEnabled(_postInspectionScriptCheckBox.isSelected());

    try
    {
      ConfigEnablePostInspectionScriptCommand enablePostScriptCommand = new ConfigEnablePostInspectionScriptCommand(_postInspectionScriptCheckBox.isSelected());
      _commandManager.execute(enablePostScriptCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void postInspectionScriptTextField_setValue()
  {
    try
    {
      ConfigSetPostInspectionScriptCommand setPostInspectionScriptCommand = new ConfigSetPostInspectionScriptCommand(_postInspectionScriptTextField.getText());
      _commandManager.execute(setPostInspectionScriptCommand);
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("ProductionOptionsGeneralTabPanel().start()");

    // set up for custom menus and tool bar
    super.startMenusAndToolBar();
    
    _preInspectionScriptCustomizeOutputTable.setEditorsAndRenderers();
    //_preInspectionScriptCustomizeInputTable.setEditorsAndRenderers();
    
    _mainUI.setStatusBarText("");
    ConfigObservable.getInstance().addObserver(this);
    updateDataOnScreen();

    // add custom menus
//    JMenu configMenu = _envPanel.getConfigMenu();
//    _menuBar.addMenuItem(_menuRequesterID, configMenu, _setDefaultMenuItem);
//
//    JMenu helpMenu = _menuBar.getHelpMenu();
//    _menuBar.addMenuItem(_menuRequesterID, helpMenu, 0, _configHelpMenuItem);
//
//    _menuBar.addMenuSeparator(_menuRequesterID, helpMenu, 1);
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    if (_preInspectionScriptCheckBox.isSelected() && (_preInspectionScriptTextField.getText().length() == 0))
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringUtil.format(StringLocalizer.keyToString("CFGUI_NO_PRESCRIPT_PATH_SET_ERROR_KEY"), 50),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (_postInspectionScriptCheckBox.isSelected() && (_postInspectionScriptTextField.getText().length() == 0))
    {
      JOptionPane.showMessageDialog(_mainUI,
                                    StringUtil.format(StringLocalizer.keyToString("CFGUI_NO_POSTSCRIPT_PATH_SET_ERROR_KEY"), 50),
                                    StringLocalizer.keyToString("CFGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      return false;
    }


    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    ConfigObservable.getInstance().deleteObserver(this);
    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
//    System.out.println("ProductionOptionsGeneralTabPanel().finish()");
  }
  
  /**
   * @author Kee Chin Seong
   * Delete row for customize settings.
   */
  private void deleteRow_actionPerformed()
  {
  }
  
  /**
   * @author Khaw Chek Hau
   ?@XCR3385: X-out board by defect threshold setting
   */
  private void xoutDetectionFailureThresholdTextField()
  {
    Long val;
    try
    {
      val = _xoutDetectionFailureThresholdTextField.getLongValue();
    }
    catch (ParseException pex)
    {
      val = new Long(_MIN_XOUT_DETECTION_THRESHOLD);
    }

    // the number parser in NumericPlainDocument will default to 0 if nothing was parsed (like an empty string)
    if (val == 0)
    {
      val = new Long(_MIN_XOUT_DETECTION_THRESHOLD);
    }

    try
    {
      _commandManager.execute(new ConfigSetXoutDetectionThresholdCommand(val.intValue()));
    }
    catch (XrayTesterException ex)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(),
                                    ex,
                                    StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                    true);
    }
    updateDataOnScreen();
  }
}
