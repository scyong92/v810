package com.axi.v810.gui.config;

import java.util.*;

import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 * @version 1.0
 */
class BarCodeScannerTestTableModel extends AbstractTableModel
{
  private ProductionOptionsBarCodeReaderTabPanel _parent;
  private BarcodeReaderManager _bcrManager;
  //String labels retrieved from the localization properties file
  private static final String _SCANNER_ID_LABEL = StringLocalizer.keyToString("CFGUI_SCANNER_ID_KEY");
  private static final String _BARCODE_LABEL = StringLocalizer.keyToString("CFGUI_BARCODE_KEY");

  private String[] _columnLabels = { _SCANNER_ID_LABEL,
                                   _BARCODE_LABEL};

  private List<BarcodeTableElement> _barcodeElements = new ArrayList<BarcodeTableElement>();

  public BarCodeScannerTestTableModel()
  {
    _bcrManager = BarcodeReaderManager.getInstance();
  }
  /**
   * Constructor.
   * @author Laura Cormos
   */
  public BarCodeScannerTestTableModel(ProductionOptionsBarCodeReaderTabPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;
    _bcrManager = BarcodeReaderManager.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  void clearData()
  {
    if (_barcodeElements != null)
      _barcodeElements.clear();
  }

  /**
   * Overridden method.
   * @return Number of columns in the table to be displayed.
   * @author Laura Cormos
   */
  public int getColumnCount()
  {
    return _columnLabels.length;
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public String getColumnName(int column)
  {
    Assert.expect(column >= 0 && column < _columnLabels.length);

    return _columnLabels[ column ];
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public Class getColumnClass(int columnIndex)
  {
    Assert.expect(columnIndex >= 0);

//    return getValueAt(0, columnIndex).getClass();
    return new String().getClass();
  }

  /**
   * Overridden method.  Gets the values from the project object.
   * @author Laura Cormos
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);
    Assert.expect(_barcodeElements != null);

    Object value = null;

    if (rowIndex >= getRowCount())
      return value;

    BarcodeTableElement rowElement = _barcodeElements.get(rowIndex);

    //get the appropriate data item that corresponds to the column
    switch(columnIndex)
    {
      case BarCodeScannerTestTable._SCANNER_ID_INDEX:
        value = rowElement._scannerID;
        break;
      case BarCodeScannerTestTable._BARCODE_INDEX:
        value = rowElement._barcode;
        break;
      default:
      {
        Assert.expect(false);
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * Overridden method.
   * @return Number of rows to be displayed in the table.
   * @author Laura Cormos
   */
  public int getRowCount()
  {
    Assert.expect(_barcodeElements != null);

    return _barcodeElements.size();
  }

  /**
   * Overridden method.
   * @author Laura Cormos
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    return false;
  }

  /**
   * Initialize the table with data coming back from a test of the bar code reader(s)
   * @author Laura Cormos
   */
  public void setData(List<String> serialNumbers)
  {
    Assert.expect(_bcrManager != null);
    Assert.expect(serialNumbers != null);

    clearData();

    //int numRows = _bcrManager.getBarcodeReaderNumberOfScanners();
    int numRows = serialNumbers.size();
    Assert.expect(numRows == serialNumbers.size());
    for (int i = 1; i <= numRows; i++)
    {
      BarcodeTableElement newRow = new BarcodeTableElement(i, serialNumbers.get(i - 1));
      _barcodeElements.add(newRow);
    }

    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  public void addRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows; i++)
    {
      BarcodeTableElement newElement = new BarcodeTableElement(getRowCount()+1,
                                                               StringLocalizer.keyToString("CFGUI_BCR_BARCODE_READER_NOT_TESTED_KEY"));
      _barcodeElements.add(newElement);
    }
    fireTableDataChanged();
  }

  /**
   * @author Laura Cormos
   */
  public void removeRows(int numRows)
  {
    Assert.expect(numRows > 0);

    for (int i = 0; i < numRows; i++)
    {
      _barcodeElements.remove(getRowCount() - 1);
    }
    fireTableDataChanged();
  }

  class BarcodeTableElement
  {
    int _scannerID;
    String _barcode;

    BarcodeTableElement(int id, String barcode)
    {
      Assert.expect(id > 0);
      Assert.expect(barcode != null);

      _scannerID = id;
      _barcode = barcode;
    }
  }
}
