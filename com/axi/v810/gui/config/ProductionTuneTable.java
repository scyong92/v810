package com.axi.v810.gui.config;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class displays the list of projects and their production tune settings
 * @author Andy Mechtenberg
 */
public class ProductionTuneTable extends JSortTable
{
  public static final int _PROJECT_NAME_INDEX = 0;
  public static final int _IMAGE_DESTINATION_INDEX = 1;
  public static final int _RESULTS_DESTINATION_INDEX = 2;
  public static final int _CURRENT_COUNT_INDEX = 3;
  public static final int _RE_ENABLE_DEFECT_THRESHOLD_INDEX = 4;
  public static final int _RE_ENABLE_COUNT_INDEX = 5;
  public static final int _NOTIFICATION_EMAIL_INDEX = 6;

  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE = .10;
  private static final double _IMAGE_DESTINATION_COLUMN_WIDTH_PERCENTAGE = .18;
  private static final double _RESULTS_DESTINATION_COLUMN_WIDTH_PERCENTAGE = .18;
  private static final double _CURRENT_COUNT_COLUMN_WIDTH_PERCENTAGE = .12;
  private static final double _RE_ENABLE_DEFECT_THRESHOLD_COLUMN_WIDTH_PERCENTAGE = .12;
  private static final double _RE_ENABLE_COUNT_COLUMN_WIDTH_PERCENTAGE = .13;
  private static final double _NOTIFICATION_EMAIL_COLUMN_WIDTH_PERCENTAGE = .13;

  private ListSelectionCellEditor _projectNameEditor;

  /**
   * Default constructor.
   * @author Andy Mechtenberg
   */
  public ProductionTuneTable()
  {
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    getTableHeader().setReorderingAllowed(false);
    _projectNameEditor = new ListSelectionCellEditor(new ArrayList<Object>(FileName.getProjectNames()));
  }

  /**
   * Sets each column's width based on the total width of the table and the
   * percentage of room that each column is allocated
   * @author Andy Mechtenberg
   */
  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    int jointTypeNameColumnWidth = (int)(totalColumnWidth * _PROJECT_NAME_COLUMN_WIDTH_PERCENTAGE);
    int ruleEnabledColumnWidth = (int)(totalColumnWidth * _IMAGE_DESTINATION_COLUMN_WIDTH_PERCENTAGE);
    int refDesColumnWidth = (int)(totalColumnWidth * _RESULTS_DESTINATION_COLUMN_WIDTH_PERCENTAGE);
    int numPadsColumnWidth = (int)(totalColumnWidth * _RE_ENABLE_DEFECT_THRESHOLD_COLUMN_WIDTH_PERCENTAGE);
    int numRowsColumnWidth = (int)(totalColumnWidth * _RE_ENABLE_COUNT_COLUMN_WIDTH_PERCENTAGE);
    int numColsColumnWidth = (int)(totalColumnWidth * _NOTIFICATION_EMAIL_COLUMN_WIDTH_PERCENTAGE);
    int lpNameColumnWidth = (int)(totalColumnWidth * _CURRENT_COUNT_COLUMN_WIDTH_PERCENTAGE);

    columnModel.getColumn(_PROJECT_NAME_INDEX).setPreferredWidth(jointTypeNameColumnWidth);
    columnModel.getColumn(_IMAGE_DESTINATION_INDEX).setPreferredWidth(ruleEnabledColumnWidth);
    columnModel.getColumn(_RESULTS_DESTINATION_INDEX).setPreferredWidth(refDesColumnWidth);
    columnModel.getColumn(_RE_ENABLE_DEFECT_THRESHOLD_INDEX).setPreferredWidth(numPadsColumnWidth);
    columnModel.getColumn(_RE_ENABLE_COUNT_INDEX).setPreferredWidth(numRowsColumnWidth);
    columnModel.getColumn(_NOTIFICATION_EMAIL_INDEX).setPreferredWidth(numColsColumnWidth);
    columnModel.getColumn(_CURRENT_COUNT_INDEX).setPreferredWidth(lpNameColumnWidth);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();

    TableCellEditor stringEditor = new DefaultCellEditor(new JTextField())
    {
      public boolean isCellEditable(EventObject evt)
      {
        return true; // allows editing with a single click (the default is a double click)
      }
    };

    NumericEditor numericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 0, 0.0, Double.MAX_VALUE);
    NumericRenderer numericRenderer = new NumericRenderer(0, JLabel.LEFT);

    columnModel.getColumn(_PROJECT_NAME_INDEX).setCellEditor(_projectNameEditor);
    columnModel.getColumn(_IMAGE_DESTINATION_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_RESULTS_DESTINATION_INDEX).setCellEditor(stringEditor);
    columnModel.getColumn(_CURRENT_COUNT_INDEX).setCellEditor(numericEditor);
    columnModel.getColumn(_RE_ENABLE_DEFECT_THRESHOLD_INDEX).setCellEditor(numericEditor);
    columnModel.getColumn(_RE_ENABLE_COUNT_INDEX).setCellEditor(numericEditor);
    columnModel.getColumn(_NOTIFICATION_EMAIL_INDEX).setCellEditor(stringEditor);

    // renderers
    columnModel.getColumn(_CURRENT_COUNT_INDEX).setCellRenderer(numericRenderer);
    columnModel.getColumn(_RE_ENABLE_DEFECT_THRESHOLD_INDEX).setCellRenderer(numericRenderer);
    columnModel.getColumn(_RE_ENABLE_COUNT_INDEX).setCellRenderer(numericRenderer);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean editCellAt(int row, int column, EventObject e)
  {
    if (column == _PROJECT_NAME_INDEX)
    {
      // want to update the list of existing projects every time the project name cell gets selected
      // to keep the projects list up to date with what's on disk
      //Punit: XCR-3040
      java.util.List<Object> projectNames = new ArrayList<>();
      for(String project : FileName.getProjectNames())
      {
        if(Project.isProjectNameValid(project))
        {
          projectNames.add(project);
        }
      }
      _projectNameEditor.setData(projectNames);
//      _projectNameEditor.setData(new ArrayList<Object>(FileName.getProjectNames()));
    }
    return super.editCellAt(row, column, e);
  }

  /**
   * We override this to make sure the selected rows are still the proper selected
   * rows after a sort.
   * @author Andy Mechtenberg
   */
  public void mouseReleased(MouseEvent event)
  {
    // this is a bit overkill because only one selection is allow with this table, but
    // I'll code it this way to handle it anyway
    java.util.List<Object> selectedData = new ArrayList<Object>();
    ProductionTuneTableModel model = (ProductionTuneTableModel)getModel();
    if (model.isSelectedRow())
      selectedData.add(model.getSelectedRowProductionTuneData());

    super.mouseReleased(event);

    clearSelection();

    // now, reselect everything
    Iterator it = selectedData.iterator();
    while (it.hasNext())
    {
      int row;
      Object obj = it.next();

      row = model.getIndexOf((ProductionTuneData)obj);
      addRowSelectionInterval(row, row);
    }
    SwingUtils.scrollTableToShowSelection(this);
  }
}
