package com.axi.v810.gui.config;

import com.axi.v810.business.resultsProcessing.*;
import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * Class responsible for holding a GUI view of a results processing rule condition
 * @author Laura Cormos
 */

public class ResultsTableModelCondition
{
  private ConditionKeyEnum _conditionKey;
  private boolean _simplePattern = true;
  private String _pattern;

  /**
   * @author Laura Cormos
   */
  public ResultsTableModelCondition(ConditionKeyEnum key, boolean simplePattern, String pattern)
  {
    Assert.expect(key != null);
    Assert.expect(pattern != null);

    _conditionKey = key;
    _simplePattern = simplePattern;
    _pattern = pattern;
  }

  /**
   * @author Laura Cormos
   */
  public void setKey(ConditionKeyEnum key)
  {
    Assert.expect(key != null);

    _conditionKey = key;
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * @author Laura Cormos
   */
  public void setIsSimplePattern(boolean simplePattern)
  {
    _simplePattern = simplePattern;
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * @author Laura Cormos
   */
  public void setPattern(String pattern)
  {
    Assert.expect(pattern != null);

    _pattern = pattern;
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * @author Laura Cormos
   */
  public ConditionKeyEnum getKey()
  {
    Assert.expect(_conditionKey != null);

     return _conditionKey;
  }

  /**
   * @author Laura Cormos
   */
  public boolean getIsSimplePattern()
  {
    return _simplePattern;
  }

  /**
   * @author Laura Cormos
   */
  public String getPattern()
  {
    Assert.expect(_pattern != null);

    return _pattern;
  }

}
