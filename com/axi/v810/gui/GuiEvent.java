package com.axi.v810.gui;

import com.axi.util.*;

/**
 * All gui events should notify their observers with this class
 * @author George A. David
 */
public class GuiEvent
{
  private Object _source;
  private GuiEventEnum _guiEventEnum;

  /**
   * @author George A. David
   */
  public GuiEvent(Object source, GuiEventEnum guiEventEnum)
  {
    Assert.expect(guiEventEnum != null);

    _source = source;
    _guiEventEnum = guiEventEnum;
  }

  /**
   * @author George A. David
   */
  public Object getSource()
  {
    return _source;
  }

  /**
   * @author George A. David
   */
  public GuiEventEnum getGuiEventEnum()
  {
    Assert.expect(_guiEventEnum != null);

    return _guiEventEnum;
  }
}
