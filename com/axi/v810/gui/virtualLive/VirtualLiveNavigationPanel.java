package com.axi.v810.gui.virtualLive;

import com.axi.v810.gui.mainMenu.*;

public class VirtualLiveNavigationPanel extends AbstractNavigationPanel
{
  public VirtualLiveNavigationPanel(String[] buttonNames, AbstractEnvironmentPanel envPanel)
  {
    super(buttonNames, envPanel);
  }
  public VirtualLiveNavigationPanel(AbstractEnvironmentPanel envPanel)
  {
    super(envPanel);
  }
}
