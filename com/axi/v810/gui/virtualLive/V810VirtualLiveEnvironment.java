package com.axi.v810.gui.virtualLive;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.guiUtil.ChoiceInputDialog.*;
import com.axi.guiUtil.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.testExec.*;

/**
 * @author Scott Richardson
 */
public class V810VirtualLiveEnvironment extends AbstractEnvironmentPanel
{
  private GuiObservable _guiObservable = GuiObservable.getInstance();

  private com.axi.v810.gui.undo.CommandManager _commandManager =
          com.axi.v810.gui.undo.CommandManager.getVirtualLiveInstance();
  private TaskPanelScreenEnum _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  //private TaskPanelScreenEnum _nextScreen;
  private boolean _environmentIsActive = false;
  private final static String _VIRTUAL_LIVE_PANEL = "VirtualLivePanel";
  private JMenu _virtualLiveMenu = new JMenu();
  private String[] _buttonNames =
  {
    StringLocalizer.keyToString("CFGUI_SW_OPTIONS_BUTTON_LABEL_KEY")
  };
  private DrawCadPanel _panelCadPanel;
  public javax.swing.Timer _t = null;
  public javax.swing.Timer timer = null;
  public String _action = "display";
  public String _selectedSavedImageDir = "";
  private Project _project;
  
  private VirtualLivePersistance _guiPersistSettings = null;
  
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  
  MainMenuGui _mainUI = null;

  /**
   * @author sham
   * @author Kee Chin Seong
   */
  public V810VirtualLiveEnvironment(MainMenuGui mainUI,String projectName,DrawCadPanel panelCadPanel)
  {
    super(mainUI);
    Assert.expect(panelCadPanel != null);

    _mainUI = mainUI;
    _panelCadPanel = panelCadPanel;
    VirtualLivePanel.getInstance().createVirtualLivePanel(_panelCadPanel);
    VirtualLivePanel virtualLivePanel = VirtualLivePanel.getInstance().getVirtualLivePanel();
    _centerPanel.add(virtualLivePanel,_VIRTUAL_LIVE_PANEL);
    
    //saving the environment object.
    virtualLivePanel.setVirtualLiveEnvironment(this);
    
    _mainCardLayout.first(_centerPanel);
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  }

  /**
   * @author Andy Mechtenberg
   * @author Scott Richardson
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /**
   * @author Scott Richardson
   */
  public V810VirtualLiveEnvironment getVirtualLiveEnvironmentPanel()
  {
    return this;
  }

  /**
   * returns the VirtualLive Menu
   * @author George Booth
   * @author Scott Richardson
   */
  JMenu getVirtualLiveMenu()
  {
    Assert.expect(_virtualLiveMenu != null);

    return _virtualLiveMenu;
  }

  /**
   * @return the VirtualLive ToolBar
   * @author George Booth
   * @author Scott Richardson
   */
  MainUIToolBar getVirtualLiveToolBar()
  {
    return _envToolBar;
  }

  /**
   * reset VirtualLive Mode
   * @author George Booth
   * @author Scott Richardson
   */
  void resetVirtualLive(ActionEvent e)
  {
    if(Config.isDeveloperDebugModeOn())
    {
      System.out.println("VirtualLiveEnvironment::resetVirtualLive()");
    }
  }

  /**
   * @author George Booth
   * @author Scott Richardson
   */
  void virtualLiveToolBarButton(ActionEvent e)
  {
    if(Config.isDeveloperDebugModeOn())
    {
      System.out.println("VirtualLiveEnvironment::virtualLiveToolBarButton()");
    }
  }

  /**
   * @author sham
   */
  public void populateWithProjectData(final Project project)
  {
    _project = project;
    VirtualLivePanel.getInstance().populateWithProjectData(project);
  }

  /**
   * User has requested an environment change. Is it OK to leave this environment?
   * @return true if environment can be exited
   * @author George Booth
   * @author Scott Richardson
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The environment is about to be exited and should perform any cleanup needed
   * @author George Booth
   * @author Scott Richardson
   * @author Chin Seong , Kee
   */
  public void finish()
  {
    setVirtualLivePanelUIPersist();

    // Swee Yee Wong XCR-3354 - Insufficient trigger error when run alignment at Alignment setup tab after alignment failed at VL
    MainMenuGui.getInstance().resetVirtualLiveMode();

    //unload current project if is DUMMY project
    if(_project!= null && _project.getName().equals(StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_DUMMY_PROJECT_NAME_KEY")))
    {
      unloadCurrentLoadedVirtualLiveProject();
    }
    else 
    {
    // XCR-3159 UI hang after create a dummy project and assert if generate virtual live image
    //Kee Chin Seong - invalidate the check sum !
      if (_project != null)
      {
        final BusyCancelDialog busyDialog = new BusyCancelDialog(
          _mainUI,
          StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
          StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
          true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, _mainUI);
        _swingWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _project.getTestProgram().invalidateCheckSum();
            }
            finally
            {
              while (busyDialog.isVisible() == false)
              {
                try
                {
                  Thread.sleep(100);
                }
                catch (InterruptedException ex)
                {
                  //do nothing
                }
              }
              busyDialog.setVisible(false);
            }
          }
        });
        busyDialog.setVisible(true);
      }
    }
  
    _environmentIsActive = false;
    finishCurrentTaskPanel();
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    
    removeGraphicEngineObserver();
    //Jack Hwee
    VirtualLivePanel.getInstance().clearComponentList();
    VirtualLivePanel.getInstance().unpopulate();
    
    // bee-hoon.goh - Delete the dummy project verification images folder when leave the virtual live environment
    String dummyVerificationDir = Directory.getXrayVerificationImagesDir(StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_DUMMY_PROJECT_NAME_KEY")); 
    if (FileUtilAxi.exists(dummyVerificationDir))
    {
      try 
      {
        FileUtilAxi.delete(dummyVerificationDir);
      } 
      catch (DatastoreException ex) 
      {
        System.out.println("Unable to delete the Virtual Live Dummy Project verification directory...");
      }
    }
    
    String dummyVirtualLiveVerificationDir = Directory.getVirtualLiveVerificationImagesDir(StringLocalizer.keyToString("MMGUI_VIRTUAL_LIVE_DUMMY_PROJECT_NAME_KEY"));
    if (FileUtilAxi.exists(dummyVirtualLiveVerificationDir))
    {
      try 
      {
        FileUtilAxi.delete(dummyVirtualLiveVerificationDir);
      } 
      catch (DatastoreException ex) 
      {
        System.out.println("Unable to delete the Virtual Live Dummy Project verification directory...");
      }
    }
    
    _panelCadPanel.getPanelGraphicsEngineSetup().clearVirtualLiveVerificationImageDisplay();
  }
  
  private void unloadCurrentLoadedVirtualLiveProject()
  {
    Assert.expect(_project != null);
    
    try
    {
      _project.destroyTestProgram();
      _project.unloadCurrentProject();
    }
    catch(DatastoreException ex)
    {
      System.out.println(ex.getMessage());
    }
    finally
    {
      _mainUI.getTestDev().unpopulate();
      unpopulate();
    }
  }
  /*
   * @author Chin-Seong, Kee
   * when switch back to another tab from virtual live, make sure the Xray Tube 
   * is set to original voltage and current settings.
   */
  public void resetXrayTubeVoltageAndCurrent()
  {
    _mainUI.setXrayTubeVoltageAndCurrent(Config.getInstance().getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_IMAGING_VOLTAGE_KILOVOLTS), 
                                         Config.getInstance().getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_IMAGING_CURRENT_MICROAMPS));
  }

  public void navButtonClicked(String buttonName)
  {
    // throw new UnsupportedOperationException("Not supported yet.");
  }

  public void switchToScreen(TaskPanelScreenEnum newScreen)
  {
    // throw new UnsupportedOperationException("Not supported yet.");
  }
  
    /**
   * This will read in the persist data
   * @author Kee Chin Seong
   */
  private VirtualLivePersistance readPersistance()
  {
    if(_guiPersistSettings == null)
    {
      _guiPersistSettings = new VirtualLivePersistance();
    }
    _guiPersistSettings = (VirtualLivePersistance)_guiPersistSettings.readSettings();
    return _guiPersistSettings;
  }

  /**
   * @author Kee Chin Seong
   */
  public VirtualLivePersistance getPersistance()
  {
    if(_guiPersistSettings == null)
      readPersistance();
    
    return _guiPersistSettings;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void writePersistance()
  {
    if (_guiPersistSettings != null)
      _guiPersistSettings.writeSettings();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setVirtualLivePanelUIPersist()
  {
    getPersistance();
    
    _guiPersistSettings.setCadSplitPanePosition(VirtualLivePanel.getInstance().getCadSplitPaneLocation());
    _guiPersistSettings.setContentSplitPanePosition(VirtualLivePanel.getInstance().getContentSplitPaneLocation());
    _guiPersistSettings.setRightSplitPanePosition(VirtualLivePanel.getInstance().getRightSplitPaneLocation());
    
    writePersistance();
  }

  /**
   * @author sham
   * @author Kee Chin Seong - Make use of the switch task loading perisistant file
   */
  public void start()
  {
    if(_project != null)
    {      
      //Khaw Chek Hau - XCR-2508: Assert when generate Virtual Live images of the rotated board.
      final BusyCancelDialog busyDialog = new BusyCancelDialog(
        _mainUI,
        StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
        StringLocalizer.keyToString("MMGUI_REGENERATING_TEST_PROGRAM_KEY"),
        true);
      busyDialog.pack();
      //XCR-3803Panel image is not refresh at VL tab after change panel loading direction	
      _panelCadPanel.setPanelLoadArrows();
      SwingUtils.centerOnComponent(busyDialog, _mainUI);
      _swingWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _project.getTestProgram();
          }
          finally
          {
            while (busyDialog.isVisible() == false)
            {
              try
              {
                Thread.sleep(100);
              }
              catch (InterruptedException ex)
              {
                //do nothing
              }
            }
            busyDialog.setVisible(false);
          }
        }
      });
      busyDialog.setVisible(true);
      if(_panelCadPanel.isCadDrawed())
      {
        VirtualLivePanel.getInstance().createVirtualLiveSelectImageSetPanel(_project.getName());
        //swee yee - 16 June 2014
        //initiaze graphicsEngine
        VirtualLivePanel.getInstance().initializeGraphicsEngine();
      }
      else
      {
        readPersistance();
        final BusyCancelDialog dlg = new BusyCancelDialog(
                _mainUI,
                StringLocalizer.keyToString("MM_GUI_LOADING_USER_INTERFACE_KEY"),
                StringLocalizer.keyToString("MM_GUI_TDT_NAME_KEY"),
                true);
        dlg.pack();
        SwingUtils.centerOnComponent(dlg,_mainUI);
        dlg.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        _mainUI.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        // using SwingUtilities here instead of SwingUtils, because I need this to go to the END of the swing queue to all
        // the dialog to come up and not BLOCK
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            dlg.paint(dlg.getGraphics());
            VirtualLivePanel.getInstance().drawCadPanel(_project);
            //swee yee - 16 June 2014
            //initiaze graphicsEngine
            VirtualLivePanel.getInstance().initializeGraphicsEngine();
            // wait until dlg is visible
            while (dlg.isVisible() == false)
            {
              try
              {
                Thread.sleep(200);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
            dlg.setVisible(false);
            _mainUI.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          }
        });
        dlg.setVisible(true);
      }
      
      //Khaw Chek Hau - XCR2295: Virtual Live Unable To Drag On Some Part Of The Board
      VirtualLiveImageGenerationSettings.getInstance().setPanelShape(_project.getPanel().getShapeInNanoMeters()); 
    }
    addGraphicEngineObserver();
    //Ngie Xing
    _panelCadPanel.getPanelGraphicsEngineSetup().getGraphicsEngine().setGroupSelectMode(false);
    
    //swee yee - 16 June 2014
    //Enable the toggle button after GroupSelectMode disabled to ensure correct mode is selected
    VirtualLivePanel.getInstance().setEnableSelectRegionToggleButton(true);
    
    // XCR-3592 Software version and build date missing after switch from Virtual Live enviroment
    _guiObservable.stateChanged(null, TaskPanelScreenEnum.VIRTUAL_LIVE);
  }

  /**
   * @author sham
   */
  public void unpopulate()
  {
    _project=null;
    VirtualLivePanel.getInstance().unpopulate();
  }

  /**
   * @author sham
   */
  public void clearVirtualLiveImagePanel()
  {
    VirtualLivePanel.getInstance().clearVirtualLiveImagepanel();
  }
  
  /**
   * XCR1650 Clear panel and board graphic engine observer when TestDev environment finish
   * @author Bee Hoon
   */
  private void removeGraphicEngineObserver()
  {
    _panelCadPanel.removePanelGraphicObserver();
  }
  
  /**
   * XCR1650 Add panel and board graphic engine observer when TestDev environment start
   * @author Bee Hoon
   */
  private void addGraphicEngineObserver()
  {
    _panelCadPanel.addPanelGraphicObserver();
  }
}
