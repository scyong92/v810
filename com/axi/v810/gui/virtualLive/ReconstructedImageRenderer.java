package com.axi.v810.gui.virtualLive;

import java.awt.image.*;
import com.axi.v810.gui.imagePane.*;

/**
 * This class draws reconstructed in mages.
 *
 * @author Bill Darbie
 * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
 *    Clear code for centralization to ImageProcessingRenderer.
 */
public class ReconstructedImageRenderer extends ImageProcessingRenderer
{

  /**
   * @author Bill Darbie
   */
  ReconstructedImageRenderer(BufferedImage image, int x, int y)
  {
    super(image, x, y);
  }
}
