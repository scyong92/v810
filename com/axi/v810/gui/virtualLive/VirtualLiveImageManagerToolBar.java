package com.axi.v810.gui.virtualLive;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.Project;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.MeasurementUnits;
import com.axi.v810.gui.drawCad.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.MagnificationEnum;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class VirtualLiveImageManagerToolBar extends JToolBar implements Observer
{
  // protected ImagePanelGraphicsEngineSetupInt _graphicsEngineSetup;

  private ImageIcon _viewTopSideImage;
  private ImageIcon _viewBottomSideImage;
  private ImageIcon _viewBothSidesImage;
  private ImageIcon _zoomInImage;
  private ImageIcon _zoomOutImage;
  private ImageIcon _zoomResetImage;
  private ImageIcon _zoomRectangleImage;
  private ImageIcon _selectRegionImage;
  private ImageIcon _dragImage;
  private ImageIcon _saveImage;
  private ImageIcon _saveAllImage;
  private ImageIcon _autoDisplay;
  private ImageIcon _saveAllScreen;
  
  //chin-seong, kee - add in measurement tools.
  private ImageIcon _measurementTools;
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing
  private ImageIcon _postProcessing;
  VirtualliveReconstructedImagePanel _reconstructedImagePanel;
  
  private static final double _ZOOM_FACTOR = 1.2;
  protected JButton _zoomInButton = new JButton();
  protected JButton _zoomOutButton = new JButton();
  private JToggleButton _zoomRectangleToggleButton = new JToggleButton();
  private JToggleButton _selectRegionToggleButton = new JToggleButton();
  protected JToggleButton _dragToggleButton = new JToggleButton();
  protected JButton _resetGraphicsButton = new JButton();
  private JToggleButton _topSideToggleButton = new JToggleButton();
  private JToggleButton _bottomSideToggleButton = new JToggleButton();
  private JToggleButton _bothSidesToggleButton = new JToggleButton();
  private ButtonGroup _viewSidesButtonGroup = new ButtonGroup();
  private GraphicsEngine _graphicsEngine;
  private PanelGraphicsEngineSetup _panelGraphicsEngineSetup;
  private VirtualLiveReconstructedImageGraphicsEngineSetup _reconstructedImageGraphicsEngineSetup;
  private JButton _saveImageButton = new JButton();
  private JButton _saveAllImageButton = new JButton();
  private JButton _saveAllScreenButton = new JButton();
  private JButton _autoDisplayButton = new JButton();
  private String _fileFullPath = Directory.getLogDir();
  private JFileChooser _fileChooser = null;
  private boolean _useInCadPanel = false;
  private boolean _useInImagePanel = false;
  
  //chin-seong, kee - add in measurement tools
  private JButton _measurementToolsButton = new JButton();
  Project _project = null;
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing
  private JToggleButton _postProcessingButton = new JToggleButton();

  /**
   * @author Erica Wheatcroft
   */
  public VirtualLiveImageManagerToolBar(GraphicsEngine graphicsEngine, PanelGraphicsEngineSetup panelGraphicsEngineSetup)
  {
    _graphicsEngine = graphicsEngine;
    _panelGraphicsEngineSetup = panelGraphicsEngineSetup;
    _useInCadPanel = true;
    jbInit();
  }

  /**
   * @author sham
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   *     Add _reconstructedImagePanel variable input to show the image post processing toolBar. 
   */
  public VirtualLiveImageManagerToolBar(VirtualLiveReconstructedImageGraphicsEngineSetup reconstructedImageGraphicsEngineSetup, 
    VirtualliveReconstructedImagePanel reconstructedImagePanel)
  {
    Assert.expect(reconstructedImageGraphicsEngineSetup != null);

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _reconstructedImagePanel = reconstructedImagePanel;
    
    _graphicsEngine = reconstructedImageGraphicsEngineSetup.getGraphicsEngine();
    _reconstructedImageGraphicsEngineSetup = reconstructedImageGraphicsEngineSetup;
    //use in reconstructed images
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    _useInImagePanel = true;
    jbInit();
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomIn(boolean allowZoomIn)
  {
    _zoomInButton.setEnabled(allowZoomIn);
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomOut(boolean allowZoomOut)
  {
    _zoomOutButton.setEnabled(allowZoomOut);
  }

  /**
   * @author George A. David
   */
  public void setAllowZoomRectangle(boolean allowZoomRectangle)
  {
    _zoomRectangleToggleButton.setEnabled(allowZoomRectangle);
  }

  /**
   * @author George A. David
   */
  public void setAllowDragGraphics(boolean allowDragGraphics)
  {
    _dragToggleButton.setEnabled(allowDragGraphics);
    if (allowDragGraphics == false)
    {
      _dragToggleButton.setSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setAllowResetGraphics(boolean allowResetGraphics)
  {
    _resetGraphicsButton.setEnabled(allowResetGraphics);
    if (allowResetGraphics == false)
    {
      _resetGraphicsButton.setSelected(false);
    }
  }

  /**
   * @author George A. David
   */
  public void setAllowViewTopSide(boolean allowViewTopSide)
  {
    _topSideToggleButton.setEnabled(allowViewTopSide);
  }

  /**
   * @author George A. David
   */
  public void setAllowViewBottomSide(boolean allowViewBottomSide)
  {
    _bottomSideToggleButton.setEnabled(allowViewBottomSide);
  }

  /**
   * @author George A. David
   */
  public void setAllowViewBothSides(boolean allowViewBothSides)
  {
    _bothSidesToggleButton.setEnabled(allowViewBothSides);
  }

  /**
   * @author George A. David
   */
  public void setAllowSelectRegion(boolean allowSelectRegion)
  {
    _selectRegionToggleButton.setEnabled(allowSelectRegion);
  }

  /**
   * @author George A. David
   */
  public void setAllowSaveImage(boolean allowSaveImage)
  {
    _saveImageButton.setEnabled(allowSaveImage);
  }

  /**
   * @author George A. David
   */
  public void setAllowSaveAllImage(boolean allowSaveAllImage)
  {
    _saveAllImageButton.setEnabled(allowSaveAllImage);
  }

  /**
   * @author George A. David
   */
  public void setAllowSaveAllScreen(boolean allowSaveAllScreen)
  {
    _saveAllScreenButton.setEnabled(allowSaveAllScreen);
  }

  /**
   * @author George A. David
   */
  public void setAllowAutoDisplay(boolean allowAutoDisplay)
  {
    _autoDisplayButton.setEnabled(allowAutoDisplay);
  }
  
  /**
   * @author Chin-seOng, Kee
   */
  public void setMeasurementTools(boolean enableMeasurementTools)
  {
    _measurementToolsButton.setEnabled(enableMeasurementTools);
  }
  
  /**
   * @author George A. David
   */
  protected void createToolBar()
  {
    removeAll();

    Dimension separatorSpacing = new Dimension(15, 25); // 10 is width, 25 is height

    add(_zoomInButton);
    add(_zoomOutButton);
    add(_zoomRectangleToggleButton);
    add(_dragToggleButton);
    add(_resetGraphicsButton);
    if (_useInCadPanel)
    {
      addSeparator(separatorSpacing);
      add(_topSideToggleButton);
      add(_bottomSideToggleButton);
      add(_bothSidesToggleButton);
      addSeparator(separatorSpacing);
      add(_selectRegionToggleButton);
    }
    if(_useInImagePanel)
    {
      addSeparator(separatorSpacing);
      add(_saveImageButton);
      add(_saveAllImageButton);
      add(_saveAllScreenButton);
      add(_autoDisplayButton);
      add(_measurementToolsButton);
    }

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    addSeparator(separatorSpacing);
    add(_postProcessingButton);
  }

  /**
   * @author George A. David
   */
  private void jbInit()
  {
    _viewTopSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_TOPSIDE);
    _viewBottomSideImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTTOMSIDE);
    _viewBothSidesImage = Image5DX.getImageIcon(Image5DX.DC_VIEW_BOTHSIDES);
    _zoomInImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_IN);
    _zoomOutImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_OUT);
    _zoomResetImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RESET);
    _zoomRectangleImage = Image5DX.getImageIcon(Image5DX.DC_ZOOM_RECTANGLE);
    _selectRegionImage = Image5DX.getImageIcon(Image5DX.DC_SELECT_REGION);
    _dragImage = Image5DX.getImageIcon(Image5DX.DC_MOVE);
    _saveImage = Image5DX.getImageIcon(Image5DX.SAVE_FILE);
    _saveAllImage = Image5DX.getImageIcon(Image5DX.SAVE_ALL_FILE);
    _saveAllScreen = Image5DX.getImageIcon(Image5DX.SAVE_SCREEN);
    _autoDisplay = Image5DX.getImageIcon(Image5DX.AUTO_DISPLAY);
    _measurementTools = Image5DX.getImageIcon(Image5DX.MEASURE);
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _postProcessing = Image5DX.getImageIcon(Image5DX.TOGGLE_POST_PROCESSING_SMALL);

    _zoomOutButton.setRolloverEnabled(true);

    _viewSidesButtonGroup.add(_topSideToggleButton);
    _viewSidesButtonGroup.add(_bottomSideToggleButton);
    _viewSidesButtonGroup.add(_bothSidesToggleButton);

    _zoomInButton.setFocusable(false);
    _zoomInButton.setEnabled(false);

    _zoomOutButton.setFocusable(false);
    _zoomOutButton.setEnabled(false);

    _zoomRectangleToggleButton.setFocusable(false);
    _zoomRectangleToggleButton.setEnabled(false);

    _selectRegionToggleButton.setFocusable(false);
    _selectRegionToggleButton.setEnabled(false);

    _dragToggleButton.setFocusable(false);
    _dragToggleButton.setEnabled(false);

    _resetGraphicsButton.setFocusable(false);
    _resetGraphicsButton.setEnabled(false);

    _topSideToggleButton.setFocusable(false);
    _topSideToggleButton.setEnabled(false);

    _bothSidesToggleButton.setFocusable(false);
    _bothSidesToggleButton.setEnabled(false);

    _bottomSideToggleButton.setFocusable(false);
    _bottomSideToggleButton.setEnabled(false);

    _saveImageButton.setFocusable(false);
    _saveImageButton.setEnabled(false);

    _saveAllImageButton.setFocusable(false);
    _saveAllImageButton.setEnabled(false);

    _saveAllScreenButton.setFocusable(false);
    _saveAllScreenButton.setEnabled(false);

    _autoDisplayButton.setFocusable(false);
    _autoDisplayButton.setEnabled(false);
    
    //chin-seong.kee - Add in measurement tools 
    _measurementToolsButton.setFocusable(true);
    _measurementToolsButton.setEnabled(true);

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _postProcessingButton.setFocusable(true);
    _postProcessingButton.setEnabled(true);
    
    _zoomInButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMIN_TOOLTIP_KEY"));
    _zoomInButton.setIcon(_zoomInImage);
    _zoomInButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomInButton_actionPerformed(e);
      }
    });

    _zoomOutButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMOUT_TOOLTIP_KEY"));
    _zoomOutButton.setIcon(_zoomOutImage);
    _zoomOutButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomOutButton_actionPerformed(e);
      }
    });

    _zoomRectangleToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMWINDOW_TOOLTIP_KEY"));
    _zoomRectangleToggleButton.setIcon(_zoomRectangleImage);
    _zoomRectangleToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        zoomRectangleToggleButton_actionPerformed(e);
      }
    });

    _selectRegionToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SELECT_REGION_KEY"));
    _selectRegionToggleButton.setIcon(_selectRegionImage);
    _selectRegionToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectRegionToggleButton_actionPerformed(e);
      }
    });

    _bottomSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWBOTTOM_TOOLTIP_KEY"));
    _bottomSideToggleButton.setIcon(_viewBottomSideImage);
    _bottomSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bottomSideToggleButton_actionPerformed(e);
      }
    });

    _topSideToggleButton.setSelected(true);
    _topSideToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWTOP_TOOLTIP_KEY"));
    _topSideToggleButton.setIcon(_viewTopSideImage);
    _topSideToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        topSideToggleButton_actionPerformed(e);
      }
    });

    _bothSidesToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_VIEWBOTH_TOOLTIP_KEY"));
    _bothSidesToggleButton.setIcon(_viewBothSidesImage);
    _bothSidesToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        bothSidesToggleButton_actionPerformed(e);
      }
    });
    _dragToggleButton.setToolTipText(StringLocalizer.keyToString("DCGUI_DRAG_TOOLTIP_KEY"));
    _dragToggleButton.setIcon(_dragImage);
    _dragToggleButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dragToggleButton_actionPerformed(e);
      }
    });

    _resetGraphicsButton.setToolTipText(StringLocalizer.keyToString("DCGUI_ZOOMRESTORE_TOOLTIP_KEY"));
    _resetGraphicsButton.setIcon(_zoomResetImage);
    _resetGraphicsButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        resetGraphicsButton_actionPerformed(e);
      }
    });

    _saveImageButton.setIcon(_saveImage);
    _saveImageButton.setToolTipText(StringLocalizer.keyToString("DCGUI_SAVE_IMAGE_KEY"));
    _saveImageButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        saveImageButton_actionPerformed();
      }
    });

    _saveAllImageButton.setIcon(_saveAllImage);
    _saveAllImageButton.setToolTipText("Save All ");
    _saveAllImageButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent ae)
      {
        saveAllImageButton_actionPerformed();
      }
    });

    _saveAllScreenButton.setIcon(_saveAllScreen);
    _saveAllScreenButton.setToolTipText("Save All Screen");
    _saveAllScreenButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent ae)
      {
        saveAllScreenButton_actionPerformed();
      }
    });

    _autoDisplayButton.setIcon(_autoDisplay);
    _autoDisplayButton.setToolTipText("Auto Display");
    _autoDisplayButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent ae)
      {
        autoDisplayButton_actionPerformed();
      }
    });
    
    _measurementToolsButton.setIcon(_measurementTools);
    _measurementToolsButton.setToolTipText("Measure");
    _measurementToolsButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent ae)
      {
        reviewMeasurement_actionPerformed();
      }
    });

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _postProcessingButton.setIcon(_postProcessing);
    _postProcessingButton.setToolTipText(StringLocalizer.keyToString("IMTB_IMAGE_PROCESSING_KEY"));
    _postProcessingButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {        
        if(_reconstructedImagePanel != null)
          _reconstructedImagePanel.showImagePostProcessingToolBar(_postProcessingButton.isSelected());       
      }
    });    
      
    setFloatable(false);
    setRollover(true);

    Insets margins = new Insets(0, 2, 2, 0);
    _zoomInButton.setMargin(margins);
    _zoomOutButton.setMargin(margins);
    _zoomRectangleToggleButton.setMargin(margins);
    _selectRegionToggleButton.setMargin(margins);


    _dragToggleButton.setMargin(margins);
    _topSideToggleButton.setMargin(margins);
    _bottomSideToggleButton.setMargin(margins);
    _saveImageButton.setMargin(margins);
    _saveAllImageButton.setMargin(margins);
    _saveAllScreenButton.setMargin(margins);
    _autoDisplayButton.setMargin(margins);
    _measurementToolsButton.setMargin(margins);

    _zoomInButton.setName(".zoomInButton");
    _zoomOutButton.setName(".zoomOutButton");
    _zoomRectangleToggleButton.setName(".zoomRectangleToggleButton");
    _selectRegionToggleButton.setName(".selectRegionToggleButton");

    _dragToggleButton.setName(".dragToggleButton");
    _resetGraphicsButton.setName(".resetGraphicsButton");
    _topSideToggleButton.setName(".topSideToggleButton");
    _bottomSideToggleButton.setName(".bottomSideToggleButton");
    _saveImageButton.setName(".saveButton");
    _saveAllImageButton.setName(".saveAllButton");
    _saveAllImageButton.setName(".saveAllScreenButton");
    _autoDisplayButton.setName(".autoDisplayButton");
    _measurementToolsButton.setName(".measurementToolsButton");
    
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    _postProcessingButton.setName(".postProcessingButton");
    
    createToolBar();
  }

  /**
   * @author Chong, Wei Chin
   */
  void saveImageButton_actionPerformed()
  {
    Assert.expect(_reconstructedImageGraphicsEngineSetup != null);
    saveImage();
  }

  /**
   * @author sham
   */
  void saveAllImageButton_actionPerformed()
  {
    Assert.expect(_reconstructedImageGraphicsEngineSetup != null);
    saveAllImages();
  }

  /**
   * @author sham
   */
  void saveAllScreenButton_actionPerformed()
  {
    Assert.expect(_reconstructedImageGraphicsEngineSetup != null);
    saveAllScreen();
  }

  /**
   * @author sham
   */
  void autoDisplayButton_actionPerformed()
  {
    Assert.expect(_reconstructedImageGraphicsEngineSetup != null);
    autoDisplay();
  }
  
  /*
   * chin-seong, Kee
   */
  void reviewMeasurement_actionPerformed()
  {
    Assert.expect(_graphicsEngine != null);
    //if(_measurementToolsButton.get
    if (_measurementToolsButton.isSelected() == false)
    {
      _measurementToolsButton.setSelected(true);
      
      MathUtilEnum displayUnits = VirtualLivePanel.getInstance().getProject().getDisplayUnits();
      
      if(displayUnits.equals(MathUtilEnum.NANOMETERS) == false)
      {
        _graphicsEngine.setMeasurementInfo(MathUtil.convertUnits(MagnificationEnum.NOMINAL.getNanoMetersPerPixel(), MathUtilEnum.NANOMETERS, displayUnits),
                                         MeasurementUnits.getMeasurementUnitDecimalPlaces(displayUnits),
                                         MeasurementUnits.getMeasurementUnitString(displayUnits));
      }
      _graphicsEngine.setMeasurementMode(true);
    }
    else
    {
      _measurementToolsButton.setSelected(false);
      _graphicsEngine.setMeasurementMode(false);
    }
  
  }
  
  /**
   * This method displays a file chooser and return the directory selected by the user.
   * @return String
   *
   * @author Wei Chin, Chong
   */
  private void saveImage()
  {
    File outputDirectory = null;

    _fileFullPath = _fileFullPath + File.separator + "virtualLiveImage.jpg";
    outputDirectory = new File(_fileFullPath);

    if (_fileChooser == null)
    {
      _fileChooser = new JFileChooser();

      // initialize the JFIle Dialog.
      FileFilterUtil fileFilter = new FileFilterUtil("jpg", "Jpeg only");
      _fileChooser.setAcceptAllFileFilterUsed(false);
      _fileChooser.setFileFilter(fileFilter);

      _fileChooser.setFileHidingEnabled(false);
      _fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }
    else
    {
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }

    if (_fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      File selectedFile = _fileChooser.getSelectedFile();

      _fileFullPath = selectedFile.getPath();
      int index = _fileFullPath.lastIndexOf(".");
      if (index < 0)  // We could not find the extension period
      {
        _fileFullPath += ".jpg";
      }
      else
      {
        String extension = _fileFullPath.substring(index, _fileFullPath.length());
        if (extension.equalsIgnoreCase(".jpg") == false)
        {
          _fileFullPath += ".jpg";
        }
      }
      
      try
      {
        _reconstructedImageGraphicsEngineSetup.saveBufferedImageForImagePanel(_fileFullPath);
      }
      catch (IOException ex)
      {
        //do nothing
      }

    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    //Assert.expect(observable != null, " observable is null");
  }

  /**
   * @author Andy Mechtenberg
   */
  public void resetForNewProject()
  {
    _dragToggleButton.setSelected(false);
    _topSideToggleButton.setSelected(false);
    _bottomSideToggleButton.setSelected(false);
  }

  /**
   * will disable all the buttons and other controls for tool bar
   * Used when there are no graphics to display because no project is loaded yet
   * @author Andy Mechteberg
   */
  public void disableAllControls()
  {
    Component components[] = getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel) comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          nestedComp.setEnabled(false);
        }
      }
      else
      {
        comp.setEnabled(false);
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void topSideToggleButton_actionPerformed(ActionEvent e)
  {
    showTopSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  void showTopSide()
  {
    _panelGraphicsEngineSetup.showTopSide();
    clearCrossHairsIfNecessary();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bothSidesToggleButton_actionPerformed(ActionEvent e)
  {
    showBothSides();
  }

  /**
   * Looks to see if the selected with crosshair component is visible, and removes the crosshairs
   * if it is not
   * @author Andy Mechtenberg
   */
  private void clearCrossHairsIfNecessary()
  {
    _graphicsEngine.clearCrossHairs();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void bottomSideToggleButton_actionPerformed(ActionEvent e)
  {
    showBottomSide();
  }

  /**
   * @author Andy Mechtenberg
   */
  void showBottomSide()
  {
    _panelGraphicsEngineSetup.showBottomSide();
    clearCrossHairsIfNecessary();
  }

  void showBothSides()
  {
    if (_panelGraphicsEngineSetup != null)
    {
      _panelGraphicsEngineSetup.showBothSides();
    }
    else
    {
      Assert.expect(false);
    }
    clearCrossHairsIfNecessary();

    _topSideToggleButton.setSelected(true);
    _bottomSideToggleButton.setSelected(true);
    _bothSidesToggleButton.setSelected(true);
  }

  /**
   * Recieves the current selected thing's description
   * @author Andy Mechtenberg
   */
  private void setDescription(String description)
  {
//    _parent.setDescription(description);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomInButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.zoom(_ZOOM_FACTOR);

  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomOutButton_actionPerformed(ActionEvent e)
  {

    _graphicsEngine.zoom(1.0 / _ZOOM_FACTOR);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void dragToggleButton_actionPerformed(ActionEvent e)
  {
    if (_dragToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _selectRegionToggleButton.setSelected(false);
      _graphicsEngine.setDragGraphicsMode(true);
    }
    else
    {
      _graphicsEngine.setDragGraphicsMode(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void zoomRectangleToggleButton_actionPerformed(ActionEvent e)
  {
    if (_zoomRectangleToggleButton.isSelected())
    {
      _dragToggleButton.setSelected(false);
      _selectRegionToggleButton.setSelected(false);
      _graphicsEngine.setZoomRectangleMode(true);
    }
    else
    {
      _graphicsEngine.setZoomRectangleMode(false);
       if(MainMenuGui.getInstance().isVirtualLiveCurrentEnvironment() == false)
        _graphicsEngine.setGroupSelectMode(true);
    }
  }

  private void selectRegionToggleButton_actionPerformed(ActionEvent e)
  {
    if (_selectRegionToggleButton.isSelected())
    {
      _zoomRectangleToggleButton.setSelected(false);
      _dragToggleButton.setSelected(false);
      _graphicsEngine.setSelectRegionMode(true);
    }
    else
    {
      _graphicsEngine.setSelectRegionMode(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void resetGraphicsButton_actionPerformed(ActionEvent e)
  {
    _graphicsEngine.resetZoom();
    _graphicsEngine.center();
  }

  /**
   * This method displays a file chooser and return the directory selected by the user.
   * @return String
   *
   * @author sham
   */
  private void saveAllImages()
  {
    String selectedDir = getUserSelectDir();
    try
    {
      if(selectedDir.equals("")==false)
        _reconstructedImageGraphicsEngineSetup.saveAllBufferedImages(selectedDir);
    }
    catch (IOException ex)
    {
      //do nothing
    }
  }

  public void autoDisplay()
  {
    VirtualLivePanel.getInstance().autoDisplay("display"," ");
  }

  public void saveAllScreen()
  {
    String selectedDir = getUserSelectDir();
    if(selectedDir.equals("")==false)
      VirtualLivePanel.getInstance().autoDisplay("save",selectedDir);
  }

  /**
   * author sham
   */
  private String getUserSelectDir()
  {
    File outputDirectory = null;
    outputDirectory = new File(".");
    if(_fileChooser == null)
    {
      _fileChooser = new JFileChooser();
      // initialize the JFIle Dialog.
      _fileChooser.setAcceptAllFileFilterUsed(false);
      _fileChooser.setFileHidingEnabled(false);
      _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }
    else
    {
      _fileChooser.setSelectedFile(outputDirectory);
      _fileChooser.setCurrentDirectory(outputDirectory);
    }
    String selectedDir = "";
    if(_fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      selectedDir = _fileChooser.getSelectedFile().getAbsolutePath();
    }

    return selectedDir;
  }

    /**
   * This will enable all the buttons and other controls for this tool bar
   * Used when we re-initialize to display something
   * @author Andy Mechteberg
   */
  public void enableAllControls()
  {
    Component components[] = this.getComponents();
    for (int i = 0; i < components.length; i++)
    {
      Component comp = components[i];
      // if this is a JPanel, then we can't just disable the panel, we have to disable all the controls in the panel
      // I suppose this should be recursive, but hopefully only one level of nesting will occur.
      if (comp instanceof JPanel)
      {
        Component nestedComponents[] = ((JPanel)comp).getComponents();
        for (int j = 0; j < nestedComponents.length; j++)
        {
          Component nestedComp = nestedComponents[j];
          nestedComp.setEnabled(true);
        }
      }
      else
        comp.setEnabled(true);
    }
  }
}
