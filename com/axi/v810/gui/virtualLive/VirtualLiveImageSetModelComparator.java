package com.axi.v810.gui.virtualLive;

import java.util.*;

/**
 * @author sham
 */
public class VirtualLiveImageSetModelComparator implements Comparator
{
  boolean _ascending;
  boolean _compareImageSetName=false;
  boolean _compareImageSetDate=false;

  /**
   * @author sham
   */
  public VirtualLiveImageSetModelComparator(boolean ascending)
  {
    _ascending = ascending;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setAscending(boolean compareInAscendingOrder)
  {
    _ascending = compareInAscendingOrder;
  }

  /**
   * @author sham
   */
  public void setCompareImageSetName(boolean compareImageSetName)
  {
    _compareImageSetName = compareImageSetName;
  }

  /**
   * @author sham
   */
  public void setCompareImageSetDate(boolean compareImageSetDate)
  {
    _compareImageSetDate = compareImageSetDate;
  }

  /**
   * @author sham
   */
  public int compare(Object a, Object b)
  {
    if (a instanceof Vector)
    {
      Vector v = (Vector) a;
      if(_compareImageSetName)
      {
        a = String.valueOf(v.elementAt(1));
      }
      else if(_compareImageSetDate)
      {
        a = String.valueOf(v.elementAt(2));
      }
    }

    if (b instanceof Vector)
    {
      Vector v = (Vector) b;
      if(_compareImageSetName)
      {
        b = String.valueOf(v.elementAt(1));
      }
      else if(_compareImageSetDate)
      {
        b = String.valueOf(v.elementAt(2));
      }
    }

    if (a instanceof String && ((String) a).length() == 0)
    {
      a = null;
    }
    if (b instanceof String && ((String) b).length() == 0)
    {
      b = null;
    }
    if (a == null && b == null)
    {
      return 0;
    }
    else if (a == null)
    {
      return 1;
    }
    else if (b == null)
    {
      return -1;
    }
    else if (a instanceof Comparable)
    {
      int result = ((Comparable) a).compareTo(b);
      return (_ascending ? result : -result);
    }
    else
    {
      int result = a.toString().compareTo(b.toString());
      return (_ascending ? result : -result);
    }
  }
}


