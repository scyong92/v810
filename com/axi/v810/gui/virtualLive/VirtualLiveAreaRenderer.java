package com.axi.v810.gui.virtualLive;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * This class is responsible for drawing the Board outline.
 *
 * @author Bill Darbie
 */
public class VirtualLiveAreaRenderer extends ShapeRenderer
{
  private Board _board;

  /**
   * @author Andy Mechtenberg
   */
  public VirtualLiveAreaRenderer(java.awt.Shape shape, int enlargeWidth)
  {
    super(Renderer._COMPONENT_IS_NOT_SELECTABLE);
    Assert.expect(shape != null);
    refreshData(shape.getBounds2D().getMinX() - enlargeWidth, shape.getBounds2D().getMinY() - enlargeWidth, shape.getBounds2D().getWidth() + (enlargeWidth * 2), shape.getBounds2D().getHeight() + (enlargeWidth * 2));
  }

  protected void refreshData(double x, double y, double w, double h)
  {
    java.awt.Shape shape = null;
    shape = new Rectangle2D.Double(x, y, w, h);
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    shape = transform.createTransformedShape(shape);
    initializeShape(shape);
  }

  /**
   * @return Board object associated with renderer
   * @author Carli Connally
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);

    return _board;
  }

  @Override
  protected void refreshData()
  {
    //
  }


}
