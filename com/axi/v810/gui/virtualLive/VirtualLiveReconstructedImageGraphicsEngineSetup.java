package com.axi.v810.gui.virtualLive;

import java.awt.image.*;
import java.io.*;
import java.util.*;

import javax.imageio.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.virtuallive.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class sets up the GraphicsEngine for a particular recipe so the
 * GraphicsEngine can draw the virtual live image set
 *
 * @author sham
 */
public class VirtualLiveReconstructedImageGraphicsEngineSetup implements Observer
{
  private static final int _BORDER_SIZE_IN_PIXELS = 15;//50;
  private GraphicsEngine _graphicsEngine;
  // private ControlToolBar _controlToolBar;
  private int _layerNumber = - 1;
  private int MILS_TO_NANOMETER = 25400;
  private int LOW_MAG_NANOMETER_TO_PIXELS = 19000;
  private int HIGH_MAG_NANOMETER_TO_PIXELS = 11000;
  private Map<Integer, Integer> _sliceLayerMap = new HashMap<Integer, Integer>();
  private int _displayLayer;
  
  //Lim, Lay Ngor - XCR1652 - Image Post Processing
  VirtualliveReconstructedImagePanel _reconstructedImagePanel;

  /**
   * @author sham
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing 
   *     Add reconstructedImagePanel to update the layer status for post processing use.
   */
  public VirtualLiveReconstructedImageGraphicsEngineSetup(GraphicsEngine graphicsEngine,
    VirtualliveReconstructedImagePanel reconstructedImagePanel)
  {
    Assert.expect(graphicsEngine != null);
    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    Assert.expect(reconstructedImagePanel != null);
    _reconstructedImagePanel = reconstructedImagePanel;

    _graphicsEngine = graphicsEngine;
    _graphicsEngine.setPixelBorderSize(_BORDER_SIZE_IN_PIXELS);
    _graphicsEngine.setShouldGraphicsBeFitToScreen(false);
    addObservers();
  }

  /**
   *
   * @author sham
   */
  public void drawOfflineReconstructedImagePanel()
  {
    if(stitchVirtualLiveImagesIfNecessary())
    {
      displayVirtualLiveImages();
    }
    else
      displayAllImages();

  }

  /**
   * @author sham
   */
  public boolean stitchVirtualLiveImagesIfNecessary()
  {
    int version = VirtualLiveReconstructedImagesData.getInstance().getVersion();
    Image image;
    if(version == 0)
    {
      double x = 0;
      double y = 0;
      
      //Siew Yeng - XCR-3284 - calculate width,height before combining image(remove redundant for loop)
      int width = 0;
      int height = 0;
      
      int deltaX = -1;
      int deltaY = -1;
      
      int firstX = -1;
      int firstY = -1;
    
      clearAllInternalEngineData();
      ArrayList sliceList = VirtualLiveReconstructedImagesData.getInstance().getSliceList();
      String imageSetDir = VirtualLiveReconstructedImagesData.getInstance().getImageSetDir();
      Map<BufferedImage,Pair<Double,Double>> imagePositionMap = new HashMap<BufferedImage,Pair<Double,Double>>();
      int zHeightInNanometers=0;
      //Move next key and value of Map by iterator
      for(int j=0; j < sliceList.size(); j++)
      {
        imagePositionMap.clear();
        zHeightInNanometers = (Integer)sliceList.get(j);
        java.util.List<String> sliceImageNameList = null;
        sliceImageNameList = VirtualLiveReconstructedImagesData.getInstance().getSliceImageNameList(zHeightInNanometers);
        String[] strArray = null;
        String filePath = null;
        java.awt.image.BufferedImage imageBuffer = null;
//        List<BufferedImage> bufferedImageList = new ArrayList<BufferedImage>();
        for (String imageName : sliceImageNameList)
        {
          filePath = imageSetDir + File.separator + imageName;
          try
          {
            if (imageName.matches(".*tiff"))
            {
              image = ImageIoUtil.loadTiffImage(filePath);
              imageBuffer = image.getBufferedImage();
              image.decrementReferenceCount();
            }
            else
            {
              imageBuffer = ImageIO.read(new File(filePath));
            }
          }
          catch (IOException ex)
          {
            filePath = filePath.replaceAll("jpg","png");
            try
            {
              imageBuffer = ImageIO.read(new File(filePath));
            }
            catch (IOException ex2)
            {
              //Assert.expect(imageBuffer != null);
              //do nothing
            }
          }
//          bufferedImageList.add(imageBuffer);
          strArray = imageName.split("_");
          if(imageName.matches(".*high.*"))
          {
            x = (double)Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
            y = (double)Math.round(Integer.parseInt(strArray[3]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
          }
          else
          {
            x = (double)Math.round(Integer.parseInt(strArray[1]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
            y = (double)Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
          }
          
          if(firstX == -1)
          {
            firstX = (int)x;
          }
          
          if(firstY == -1)
          {
            firstY = (int)y;
          }
          
          if(deltaX == -1)
          {
            deltaX = (int)x;
          }

          if(deltaY == -1)
          {
            deltaY = (int)y;
          }

          //accumulate width by fix position y
          if((int)y == firstY)
          {
            width += imageBuffer.getWidth() - 1;
          }
          //accumulate height by fix position x
          if((int)x == firstX)
          {
            height += imageBuffer.getHeight() - 1;
          }
          
          if((int)x < deltaX)
          {
            deltaX = (int)x;
          }
          
          if((int)y < deltaY)
          {
            deltaY = (int)y;
          }
          imagePositionMap.put(imageBuffer,new Pair<Double,Double>(x, y)) ;
        }
//        for (int i = 0; i < bufferedImageList.size(); i++)
//        {
//          strArray = sliceImageNameList.get(i).split("_");
//          if(sliceImageNameList.get(i).matches(".*high.*"))
//          {
//            x = (double)Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
//            y = (double)Math.round(Integer.parseInt(strArray[3]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
//          }
//          else
//          {
//            x = (double)Math.round(Integer.parseInt(strArray[1]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
//            y = (double)Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
//          }
//          imagePositionMap.put(bufferedImageList.get(i),new Pair<Double,Double>(x, y)) ;
//        }
        BufferedImage stitchedImage = BufferedImageUtil.getInstance().combineBufferedImagesToSingleBufferedImage(imagePositionMap, width, height, deltaX, deltaY);
        Assert.expect(stitchedImage != null);
        drawStitchedImage(stitchedImage,zHeightInNanometers);
        //saveStitchedImage(stitchedImage,zHeightInNanometers, VirtualLiveReconstructedImagesData.getInstance().getImageSetDir());
        stitchedImage=null;
        imagePositionMap.clear();
//        bufferedImageList = null;
        MemoryUtil.garbageCollect();
      }
      //updateXMLData();
      return true;
    }
    return false;
  }

  /**
   * @author sham
   */
  public void displayAllImages()
  {
    Assert.expect(_graphicsEngine != null);
    
    clearAllInternalEngineData();
    populateLayersWithRenderers();
    setVisibility();
    setColors();
    //Siew Yeng - move this to populateImageLayer
//    _graphicsEngine.setAxisInterpretation(true, false);
//    _graphicsEngine.fitGraphicsToScreen();
//    _graphicsEngine.center();
  }

  /**
   * @author sham
   */
  public void drawStitchedImage(java.awt.image.BufferedImage stitchedImage, int zHeightInNanometers)
  {
    Assert.expect(stitchedImage!= null);
    
    createLayer(zHeightInNanometers);
    populateImageLayer(stitchedImage, zHeightInNanometers);
    stitchedImage=null;
  }

  /**
   * @author sham
   */
  public void displayVirtualLiveImages()
  {
    Assert.expect(_graphicsEngine != null);

    setVisibility();
    setColors();
    //Siew Yeng - XCR-3284 - remove from here as these will be call when populate image
//    _graphicsEngine.setAxisInterpretation(true, false);
//    _graphicsEngine.fitGraphicsToScreen();
//    _graphicsEngine.center();
  }

  /**
   * @author sham
   */
  void addObservers()
  {
    VirtualLiveImageReconstructionStitchObservable.getInstance().addObserver(this);
  }

  /**
   * Set the top layers to be visible by default
   * sham
   */
  private void setVisibility()
  {
    Assert.expect(_graphicsEngine != null);

    _displayLayer = Math.round(_sliceLayerMap.size() / 2);

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    //XCR2125 - Due to VL too slow in apply certain post processing to large image.
    //We change to reset all the image processing button and revert all the images 
    //back to original buffer image.    
//    _reconstructedImagePanel.updateCurrentDisplayLayerForImageProcessing(_displayLayer);
    _reconstructedImagePanel.revertCurrentAndPreviousDisplayLayerToOriginal(_displayLayer);

    _graphicsEngine.setVisible(getAllLayers(), false);
    _graphicsEngine.setVisible(_displayLayer, true);
  }

  /**
   * @return a List of Integers of all the bottom side layers
   * @author sham
   */
  List<Integer> getAllLayers()
  {
    Assert.expect(_sliceLayerMap != null);

    List<Integer> layers = new ArrayList<Integer>();
    layers.addAll(_sliceLayerMap.values());

    return layers;
  }

  /**
   * Set up the proper colors for the layers
   * sham
   */
  private void setColors()
  {
    Assert.expect(_graphicsEngine != null);
  }

  /**
   * @author sham
   */
  private void populateLayersWithRenderers()
  {
    //Siew Yeng - XCR-3284 - remove redundant for loop
    String imageSetDir = VirtualLiveReconstructedImagesData.getInstance().getImageSetDir();
    //Siew Yeng - XCR-3362 - System crash when alternate click on display button many times
    List sliceList = new ArrayList(VirtualLiveReconstructedImagesData.getInstance().getSliceList());
    Iterator iterator = sliceList.iterator();

    try
    {
      while (iterator.hasNext())
      {
        int sliceLayerIndex = (Integer)iterator.next();
        java.util.List<String> sliceImageNameList = null;
        sliceImageNameList = VirtualLiveReconstructedImagesData.getInstance().getSliceImageNameList(sliceLayerIndex);
        String filePath = null;
        java.awt.image.BufferedImage imageBuffer = null;

        for (String imageName : sliceImageNameList)
        {
          filePath = imageSetDir + File.separator + imageName;
          try
          {
            if(imageName.matches(".*tiff"))
            {
              Image image = ImageIoUtil.loadTiffImage(filePath);
              imageBuffer = image.getBufferedImage();
              image.decrementReferenceCount();
            }
            else
            {
  //            imageBuffer = ImageIO.read(new File(filePath));
              imageBuffer = ImageIoUtil.loadPngBufferedImage(filePath);
            }
          }
          catch (IOException ex)
          {
            filePath = filePath.replaceAll("jpg","png");
            try
            {
  //            imageBuffer = ImageIO.read(new File(filePath));
              imageBuffer = ImageIoUtil.loadPngBufferedImage(filePath);
            }
            catch (IOException ex2)
            {
              //Assert.expect(imageBuffer != null);
              //do nothing
            }
          }

          if(imageBuffer == null)
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(StringLocalizer.keyToString("VIRTUAL_LIVE_IMAGESET_INCOMPLETE_KEY"), 50),
                                      StringLocalizer.keyToString("VP_RECONSTRUCTED_IMAGE_KEY"),
                                      JOptionPane.WARNING_MESSAGE);
            return;
          }

          drawStitchedImage(imageBuffer, sliceLayerIndex);
          imageBuffer = null;
        }
      }
    }
    finally
    {
      sliceList.clear();
    }
  }

  /**
   * @author sham
   */
  private void createLayer(int zHeightInNanometers)
  {
    Assert.expect(_sliceLayerMap != null);

    _sliceLayerMap.put(zHeightInNanometers, new Integer(++_layerNumber));
  }

  /**
   * @author shamn
   */
  private void populateImageLayer(java.awt.image.BufferedImage bufferedImage, int zHeightInNanometers)
  {
    Assert.expect(bufferedImage != null);
    Assert.expect(_graphicsEngine != null);

    int layerNumber = getLayerNumber(zHeightInNanometers);
    ReconstructedImageRenderer imageRenderer = new ReconstructedImageRenderer(bufferedImage, 0, 0);
    bufferedImage = null;
    _graphicsEngine.addRenderer(layerNumber, imageRenderer);
    //Siew Yeng - XCR-3284
    _graphicsEngine.setAxisInterpretation(true, false);
    _graphicsEngine.fitGraphicsToScreen();
    _graphicsEngine.center();
  }

  /**
   * @author sham
   * @Lim, Lay Ngor - Image Post processing
   */
  public void changeDisplayLayer(int zHeightInNanometers, boolean updateDisplayLayer)
  {
    Assert.expect(_graphicsEngine != null);

    int layer = getLayerNumber(zHeightInNanometers);
    _displayLayer = layer;

    //Lim, Lay Ngor - XCR1652 - Image Post Processing
    //XCR2125 - Due to VL too slow in apply certain post processing to large image.
    //We change to reset all the image processing button and revert all the images 
    //back to original buffer image.
    if(updateDisplayLayer) //don't need to update if it is false
      _reconstructedImagePanel.revertCurrentAndPreviousDisplayLayerToOriginal(_displayLayer);
    //    _reconstructedImagePanel.updateCurrentDisplayLayerForImageProcessing(_displayLayer);
    
    _graphicsEngine.setVisible(getAllLayers(), false);
    _graphicsEngine.setVisible(layer, true);
  }

  /**
   * @author sham
   */
  public void saveBufferedImageForImagePanel(String filename) throws IOException
  {
    Assert.expect(filename != null);
    Assert.expect(_graphicsEngine != null);

    RendererPanel rendererPanel = _graphicsEngine.getLayerNumberToRendererPanelMap().get(_displayLayer);
    saveSingleBufferedImage(filename, rendererPanel);
  }

  /**
   * @author sham
   */
  public void saveAllBufferedImages(String selectedDir) throws IOException
  {
    Assert.expect(selectedDir != null);
    Assert.expect(_graphicsEngine != null);
    Assert.expect(_sliceLayerMap != null);

    String imageName;
    String[] str = null;
    int zValue;
    String filename;
    String fname;
    String zValueStr="";
    Set set = _sliceLayerMap.entrySet();
    //Move next key and value of Map by iterator
    Iterator iterator = set.iterator();
    while (iterator.hasNext())
    {
      Map.Entry sliceLayermapEntry = (Map.Entry) iterator.next();
      RendererPanel rendererPanel = _graphicsEngine.getLayerNumberToRendererPanelMap().get((Integer) sliceLayermapEntry.getValue());
      imageName = VirtualLiveReconstructedImagesData.getInstance().getSliceImageNameList((Integer) sliceLayermapEntry.getKey()).get(0);
      str = imageName.split("_");
      zValueStr = str[str.length - 1].replaceAll(".jpg","");      
      zValueStr = zValueStr.replaceAll(".tiff","");
      zValueStr = zValueStr.replaceAll(".png","");
      zValue = Integer.parseInt(zValueStr) / MILS_TO_NANOMETER;
      
      if(zValue > 0)
      {
        fname="b_"+ String.valueOf((1000-zValue))+"_"+String.valueOf(zValue);
      }
      else if(zValue == 0)
      {
        fname="m_"+String.valueOf(zValue);
      }
      else
      {
        fname="s_"+ String.valueOf(zValue *(-1))+ "_"+ String.valueOf(zValue);
      }
      filename = selectedDir +  File.separator +"" + fname + ".jpg";
      saveSingleBufferedImage(filename, rendererPanel);
    }
  }
  /*
   * @author sham
   */
  public GraphicsEngine getGraphicsEngine()
  {
    return _graphicsEngine;
  }

  /*
   * @author sham
   */
  public Map<Integer,Integer> getSliceLayerMap()
  {
    return _sliceLayerMap;
  }

  /**
   * @author sham
   */
  public void saveSingleBufferedImage(String filename, RendererPanel rendererPanel) throws IOException
  {
    Assert.expect(filename != null);

    java.awt.Component[] componentArray = rendererPanel.getComponents();
    BufferedImage bufferedImage = null;
    for (java.awt.Component component : componentArray)
    {
      ReconstructedImageRenderer renderer = (ReconstructedImageRenderer) component;
      bufferedImage = renderer.getBufferedImageFromRenderer();
    }
    Assert.expect(bufferedImage != null);
    BufferedImageUtil.getInstance().writeImageToJpegFile(bufferedImage, filename);
  }

  /**
   * @author sham
   */
  public void unpopulate()
  {
    clearAllInternalEngineData();
  }

    /**
   * @author Andy Mechtenberg
   */
  private void clearAllInternalEngineData()
  {
    Assert.expect(_graphicsEngine != null);
    Assert.expect(_sliceLayerMap != null);

    for (int layer : getAllLayers())
    {
      _graphicsEngine.removeRenderers(layer);
      _graphicsEngine.removeLayer(layer);
    }
    _layerNumber = - 1;
    _sliceLayerMap.clear();
    _graphicsEngine.clearAllFlashingRenderersAndLayers();
  }

  /**
   * @author Sham
   */
  public void update(final Observable o, final Object arg)
  {  
    //Siew Yeng - XCR-3284 - remove SwinguUtils to solve progress bar lag
//    SwingUtils.invokeLater(new Runnable()
//    {
//      public void run()
//      {
        if(o instanceof VirtualLiveImageReconstructionStitchObservable)
        {
          if (arg instanceof Integer)
          {
            int zHeightInNanometers =(Integer)arg;
            try
            {
              handleVirtualLiveImageReconstructionStitchObservable(zHeightInNanometers);
            }
            catch (DatastoreException ex)
            {
              //
            }
            catch (XrayTesterException ex)
            {
              //
            }
          }
        }
//      }
//    });
  }
  
  /**
   * @author sham
   */
  public void handleVirtualLiveImageReconstructionStitchObservable(int zHeightInNanometers) throws DatastoreException, XrayTesterException
  {
    boolean ifUsingTiffIMageType = Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType();
    BufferedImage stitchedBufferedImage = stitchReconstructedImage(zHeightInNanometers);
    
    if(VirtualLiveImageGenerationSettings.getInstance().isNeedSaveStitchImage())
    { 
      Image virtualLiveImage = Image.createFloatImageFromBufferedImage(stitchedBufferedImage);
      if(ifUsingTiffIMageType)
        stitchedBufferedImage = virtualLiveImage.get32BitBufferedImage();
      else
        stitchedBufferedImage = virtualLiveImage.getBufferedImage();
      
      saveStitchedImage(stitchedBufferedImage,zHeightInNanometers, null);
      writeVirtualLiveXMLData(zHeightInNanometers);
      
      virtualLiveImage.decrementReferenceCount();
      virtualLiveImage = null;
    }
    
    if (ifUsingTiffIMageType)
    {
      try
      {
        String filePath = VirtualLiveReconstructedImagesData.getInstance().getImageSetDir() + File.separator + FileName.getVirtualLiveImageNamePrefix() + "_"+ String.valueOf(zHeightInNanometers) + FileName.getVirtualLiveTiffImageExtension(); 
        Image image = ImageIoUtil.loadTiffImage(filePath);
        stitchedBufferedImage = image.getBufferedImage();
        image.decrementReferenceCount();
      }
      catch (FileDoesNotExistException ex)
      {
        //
      }
      catch (CouldNotReadFileException ex)
      {
        //
      }
      drawStitchedImage(stitchedBufferedImage, zHeightInNanometers);
      //prepare for zSlider
      VirtualLiveReconstructedImagesData.getInstance().addImageName(FileName.getVirtualLiveImageNamePrefix()+"_"+String.valueOf(zHeightInNanometers)+ FileName.getVirtualLiveTiffImageExtension());
    }
    else
    {
      drawStitchedImage(stitchedBufferedImage, zHeightInNanometers);
      VirtualLiveReconstructedImagesData.getInstance().addImageName(FileName.getVirtualLiveImageNamePrefix()+"_"+String.valueOf(zHeightInNanometers)+ FileName.getVirtualLiveImageExtension());
    }
    stitchedBufferedImage=null;
  }

  /**
   * @author sham
   */
  public void saveStitchedImage(BufferedImage bufferedImage,int zHeightInNanometers,String imageSetDir)
  {
    Assert.expect(bufferedImage != null);
    //Assert.expect(imageSetDir != null);

    String projectName = MainMenuGui.getInstance().getCurrentProjectName();
    try
    {
      ImageManager.getInstance().saveVirtualLiveImage(projectName,bufferedImage,zHeightInNanometers,imageSetDir);
      bufferedImage = null;
    }
    catch (DatastoreException ex)
    {
      //
    }
   }

  /**
   * @author sham
   */
  public BufferedImage stitchReconstructedImage(int zHeightInNanometers) throws DatastoreException, XrayTesterException
  {
    java.util.List<Pair<String,Pair<Double, Double>>> imageNameWithXYPositionList = VirtualLiveImageProducer.getInstance().getVirtualLiveMemoryManagedImageMap().getImageNameWithXYPositionList(zHeightInNanometers);
//    java.util.List<BufferedImage> bufferedImageList = new ArrayList<BufferedImage>();
    Map<BufferedImage, Pair<Double, Double>> imagePositionMap = new HashMap<BufferedImage, Pair<Double, Double>>();
    BufferedImage bufferedImage;
    
    //Siew Yeng - XCR-3284 - calculate width, height before combining image(remove redundant for loop)
    int width = 0;
    int height = 0;
    
    int deltaX = -1;
    int deltaY = -1;
    
    int x = -1;
    int y = -1;
    
    for (Pair<String,Pair<Double, Double>> imageNameWithXYPosition : imageNameWithXYPositionList)
    {
      bufferedImage = VirtualLiveImageProducer.getInstance().getImage(imageNameWithXYPosition.getFirst());
      
      //Siew Yeng - move from BufferedImageUtil.getInstance().combineBufferedImagesToSingleBufferedImage
      Pair<Double, Double> XYPosition = imageNameWithXYPosition.getSecond();
      
      if(deltaX == -1)
      {
        deltaX = XYPosition.getFirst().intValue();
      }
      
      if(deltaY == -1)
      {
        deltaY = XYPosition.getSecond().intValue();
      }
      
      if(x == -1)
      {
        x = XYPosition.getFirst().intValue();
      }
      
      if(y == -1)
      {
        y = XYPosition.getSecond().intValue();
      }
      
      //accumulate width by fix position y
      if(XYPosition.getSecond().intValue() == y)
      {
        width += bufferedImage.getWidth() - 1;
      }
      //accumulate height by fix position x
      if(XYPosition.getFirst().intValue() == x)
      {
        height += bufferedImage.getHeight() - 1;
      }
      
      if(XYPosition.getFirst().intValue() < deltaX)
      {
        deltaX = XYPosition.getFirst().intValue();
      }
      if(XYPosition.getSecond().intValue()< deltaY)
      {
        deltaY = XYPosition.getSecond().intValue();
      }
      
      imagePositionMap.put(bufferedImage,imageNameWithXYPosition.getSecond());
    }
    
    //only read 8 bit image because current graphic engine cnnot support
//    if(Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
//    {
//      stitchedImage = BufferedImageUtil.getInstance().combine32BitBufferedImagesToSingleBufferedImage(bufferedImageList, imagePositionMap);
//    }
//    else
//    {
    BufferedImage stitchedImage = BufferedImageUtil.getInstance().combineBufferedImagesToSingleBufferedImage(imagePositionMap, width, height, deltaX, deltaY);      
//    }
    imagePositionMap.clear();
    return stitchedImage;
  }

  /**
   * @author sham
   */
  public void writeVirtualLiveXMLData(int zHeightInNanometers)
  {
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
    {
      VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.IMAGE_NAME,FileName.getVirtualLiveImageNamePrefix()+"_"+String.valueOf(zHeightInNanometers) + FileName.getVirtualLiveTiffImageExtension());
    }
    else
    {
      VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.IMAGE_NAME,FileName.getVirtualLiveImageNamePrefix()+"_"+String.valueOf(zHeightInNanometers) + FileName.getVirtualLiveImageExtension());
    }
  }

  /*
   * @author sham
   */
  public void updateXMLData()
  {
    String imageSetDataXMLFile = FileName.getVirtualLiveOfflineImageSetDataXMLFile(MainMenuGui.getInstance().getCurrentProjectName());
    VirtualLiveImageSetDataXMLWriter.getInstance().createVirtualLiveImageSetDataXMLWriter(imageSetDataXMLFile);
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.X_CENTER, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getCenterXInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.Y_CENTER, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getCenterYInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.WIDTH, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getWidthInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.HEIGHT, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getHeightInNanoMeter()));
    if (VirtualLiveImageGenerationSettings.getInstance().getComponentRefDes() != "")
    {
      VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.COMPONENT_NAME, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getComponentRefDes()));
    }
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.ENLARGE_UNCERTAINTY, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getEnlargeUncertaintyInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.Z_SIZE, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getZStepSizeInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.INTEGRATION_LEVEL, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getIntegrationLevel()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.BYPASS_ALIGNMENT, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getBypassAlignment()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.MIN_Z_HEIGHT_KEY, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getMinZHeightInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.MAX_Z_HEIGHT_KEY, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getMinZHeightInNanoMeter()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.GAIN_KEY, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getUserGain()));
    VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.STAGE_SPEED_KEY, String.valueOf(VirtualLiveImageGenerationSettings.getInstance().getStageSpeed()));
    ArrayList sliceList = VirtualLiveReconstructedImagesData.getInstance().getSliceList();
    for(int i=0; i < sliceList.size(); i++)
    {
      VirtualLiveImageSetDataXMLWriter.getInstance().addImageSetData(VirtualLiveImageSetDataXMLWriter.IMAGE_NAME,FileName.getVirtualLiveImageNamePrefix()+"_"+ String.valueOf(sliceList.get(i))+ FileName.getVirtualLiveImageExtension());
    }
    try
    {
      VirtualLiveImageSetDataXMLWriter.getInstance().writeToXMLFile();
    }
    catch (DatastoreException ex)
    {
      //
    }
    VirtualLiveImageSetDataXMLWriter.getInstance().deleteVirtualLiveImageSetDataXMLWriter();
  }
  
  /**
   * @author sham
   */
  public int getLayerNumber(int zHeightInNanometers)
  {
    return _sliceLayerMap.get(zHeightInNanometers);
  }

  /**
   * @author Lim, Lay Ngor - XCR1652 - Image Post Processing
   */
  public int getCurrentDisplayLayer()
  {
    return _displayLayer;
  }
}
