package com.axi.v810.gui.virtualLive;

import java.awt.*;
import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.gui.imagePane.*;
import javax.swing.plaf.basic.*;

/**
 * @author khang-shian.sham
 */
public class VirtualliveReconstructedImagePanel extends JPanel
{

  private int _defaultWidth = 800;
  private int _defaultHeight = 600;
  private VirtualLiveReconstructedImageGraphicsEngineSetup _virtualLiveReconstructedImageGraphicsEngineSetup;
  private GraphicsEngine _graphicsEngine;
  private VirtualLiveImageManagerToolBar _imageManagerToolBar;
  
  //Lim, Lay Ngor - Post Processing Image Enhancer
  JPanel _northPanel;
  private ImageProcessingToolBar _imageProcessingToolBar;

  /**
   * @author sham
   */
  public VirtualliveReconstructedImagePanel()
  {
    _graphicsEngine = new GraphicsEngine(0);
    _virtualLiveReconstructedImageGraphicsEngineSetup = new VirtualLiveReconstructedImageGraphicsEngineSetup(_graphicsEngine, this);
    jbInit();
  }

  /**
   * @author sham
   */
  private void jbInit()
  {
    setBackground(Color.black);
//    setPreferredSize(new Dimension(_defaultWidth,_defaultHeight));
    setLayout(new BorderLayout());
    //Lim, Lay Ngor - Post Processing Image Enhancer
    _northPanel = new JPanel();
    
    _imageManagerToolBar = new VirtualLiveImageManagerToolBar(_virtualLiveReconstructedImageGraphicsEngineSetup, this);
    _imageManagerToolBar.disableAllControls();
    
    //Lim, Lay Ngor - Post Processing Image Enhancer
    _imageProcessingToolBar = new ImageProcessingToolBar(_graphicsEngine, _virtualLiveReconstructedImageGraphicsEngineSetup.getCurrentDisplayLayer());
    
    _northPanel.setBackground(Color.BLACK);    
    _imageProcessingToolBar.setForeground(Color.DARK_GRAY);
    _imageProcessingToolBar.setBorderPainted(true);
    
    BasicToolBarUI ui =  (BasicToolBarUI)_imageProcessingToolBar.getUI();
    ui.setFloatingColor(Color.DARK_GRAY);
    ui.setDockingColor(Color.DARK_GRAY);
    
    _northPanel.setLayout(new BorderLayout());
    _northPanel.add(_imageManagerToolBar, BorderLayout.NORTH);
    _northPanel.add(_imageProcessingToolBar, BorderLayout.SOUTH);
    
    add(_northPanel, BorderLayout.NORTH);
    add(_graphicsEngine,BorderLayout.CENTER);
    setPreferredSize(new Dimension(_imageProcessingToolBar.getWidth(), getHeight()));
  }

  /**
   * Removes all the renderers to free up the memory
   * This must be called on the Swing thread!
   * @author Andy Mechtenberg
   */
  public void dispose()
  {
    _graphicsEngine.dispose();
  }

  /*
   * @author sham khang shian
   */
  public VirtualLiveReconstructedImageGraphicsEngineSetup getReconstructedImageGraphicsEngineSetup()
  {
    return _virtualLiveReconstructedImageGraphicsEngineSetup;
  }

  /**
   * @author sham
   */
  public void displayPanel()
  {
    if(VirtualLiveReconstructedImagesData.getInstance().isOnlineMode()==false)
    {
      _virtualLiveReconstructedImageGraphicsEngineSetup.drawOfflineReconstructedImagePanel();
    }
    else
      _virtualLiveReconstructedImageGraphicsEngineSetup.displayVirtualLiveImages();

    _imageManagerToolBar.enableAllControls();
    _imageManagerToolBar.setAllowZoomIn(true);
    _imageManagerToolBar.setAllowZoomOut(true);
    _imageManagerToolBar.setAllowDragGraphics(true);
    _imageManagerToolBar.setAllowResetGraphics(true);
    _imageManagerToolBar.setAllowSaveImage(true);
    _imageManagerToolBar.setAllowSaveAllImage(true);
    _imageManagerToolBar.setAllowSaveAllScreen(true);
    _imageManagerToolBar.setAllowAutoDisplay(true);
    _imageManagerToolBar.setMeasurementTools(true);
  }

  /**
   * @author sham
   */
  public void unpopulate()
  {
    Assert.expect(_virtualLiveReconstructedImageGraphicsEngineSetup != null);
    
    _virtualLiveReconstructedImageGraphicsEngineSetup.unpopulate();
    VirtualLiveReconstructedImagesData.getInstance().clear();
    dispose();
  }
  
  /**
   * @author Lim, Lay Ngor- Post Processing Image Enhancer  
   */
  public void showImagePostProcessingToolBar(boolean isVisibled)
  {
    //To let ImagePaneToolbar control the visibility of this toolBar.
    _imageProcessingToolBar.setVisible(isVisibled);   
    setPreferredSize(new Dimension(_imageProcessingToolBar.getWidth(), getHeight()));
    updateUI();
    validate();
  }  
  
  /**
   *  Update the Virtual Live image layer with post processing applied to display GUI.
   * @author Lim, Lay Ngor- Post Processing Image Enhancer  
   */
  public void updateCurrentDisplayLayerForImageProcessing(int currentDisplayLayer)
  {
    _imageProcessingToolBar.updateCurrentDisplayLayer(currentDisplayLayer);
  }
  
  /**
   * XCR2125: Revert current and previous Virtual Live image layer to original image and display it at GUI.
   * @author Lim, Lay Ngor- Post Processing Image Enhancer  
   */
  public void revertCurrentAndPreviousDisplayLayerToOriginal(int currentDisplayLayer)
  {
    _imageProcessingToolBar.revertCurrentAndPreviousDisplayLayerToOriginal(currentDisplayLayer);
  }  
}
