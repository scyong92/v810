package com.axi.v810.gui.virtualLive;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.imagePane.*;

/**
 * @author khang-shian.sham
 */
public class VirtualLiveImageSetTableModel extends DefaultSortTableModel
{
  ArrayList<Vector> _rowData;
  String[] _columnNames;
  
  //Siew Yeng - XCR-3284
  private int _selectedImageSetRowIndex = -1;
  private boolean _ascending = true;
  private VirtualLiveImageSetModelComparator _comparator = new VirtualLiveImageSetModelComparator(true);
  
  
 /**
  * @author khang-shian.sham
  */
  public VirtualLiveImageSetTableModel(ArrayList<Vector> rowData, String[] columnNames)
  {
    _rowData = rowData;
    _columnNames = columnNames;
  }

  /**
   * @author khang-shian.sham
   */
  public void deleteRows(Collection<Vector> rowValues)
  {
    //Siew Yeng - XCR-3284
    //store previous selection
    Vector currentSelected = null;
    if(_selectedImageSetRowIndex != -1)
      currentSelected = _rowData.get(_selectedImageSetRowIndex);
    
    for (Vector value : rowValues)
    {
      if((_selectedImageSetRowIndex != 1) && (value.equals(currentSelected)))
        _selectedImageSetRowIndex = -1;
      _rowData.remove(value);
    }
    
    if(currentSelected != null)
      _selectedImageSetRowIndex = _rowData.indexOf(currentSelected);
    
    this.fireTableStructureChanged();
  }

  /**
   * Add a new generated image set into table
   * @author khang-shian.sham
   */
  public void addRow(String virtualLiveImageSetName)
  {
    //Siew Yeng - XCR-3284
    Vector data = new Vector(_columnNames.length);
    data.addElement(true);
    data.addElement((String) virtualLiveImageSetName);
    try
    {
      data.addElement(StringUtil.convertMilliSecondsToTimeAndDate(StringUtil.convertStringToTimeInMilliSeconds(virtualLiveImageSetName)));
    }
    catch (BadFormatException ex)
    {
      //
    }
    data.addElement(false);
    _rowData.add(data);
    
    //Siew Yeng - XCR-3284 - set new added row as image set selected
    if(_selectedImageSetRowIndex != -1)
      _rowData.get(_selectedImageSetRowIndex).setElementAt(false, 0);
    
    _selectedImageSetRowIndex = _rowData.indexOf(data);
    
    this.fireTableDataChanged();
  }

  /**
   * @author khang-shian.sham
   */
  public int getColumnCount()
  {
    return _columnNames.length;
  }

  /**
   * @author khang-shian.sham
   */
  public String getColumnName(int column)
  {
    return _columnNames[column];
  }

  /**
   * @author khang-shian.sham
   */
  public int getRowCount()
  {
    if(_rowData ==  null)
      return 0;
    
    return _rowData.size();
  }

  /**
   * @author khang-shian.sham
   */
  public Object getValueAt(int row, int column)
  {
    return _rowData.get(row).elementAt(column);
  }

  /**
   * @author khang-shian.sham
   */
  public Class getColumnClass(int column)
  {
    return (getValueAt(0, column).getClass());
  }

  /**
   * @author khang-shian.sham
   */
  public void setValueAt(Object value, int row, int column)
  {
    if(column == 0)// radio button
    {
      //Siew Yeng XCR-3284 - if already selected, remain radio button as selected
      if(_selectedImageSetRowIndex == row)
      {
        _rowData.get(row).setElementAt(true, column);
        fireTableRowsUpdated(row, row);
        return;
      }
      
      //new image set selected
      _selectedImageSetRowIndex = row;
      
      for(int index = 0; index < _rowData.size(); index++)
      {
        if(index == row)
        {
          //set radio button status as selected at selected image set
          _rowData.get(index).setElementAt(true, column);
        }
        else
        {
          //clear others radio button selection
          _rowData.get(index).setElementAt(false, column);
        }
        
        fireTableRowsUpdated(index, index);
      }
      notifyImageSetSelectionChanged(getValueAt(row, 1).toString());
      return;
    }
    
    _rowData.get(row).setElementAt(value, column);
    fireTableRowsUpdated(row, row);
  }

  /**
   * @author khang-shian.sham
   */
  public boolean isCellEditable(int row, int column)
  {
    return (column != 1 && column != 2);
  }
  
  /**
   * @author Siew Yeng
   */
  public void sortColumn(int col, boolean ascending)
  {
    Vector currentSelected = _rowData.get(_selectedImageSetRowIndex);
    
    _comparator.setAscending(ascending);
    if(col == 0 || col == getColumnCount() - 1)
    {
      return;//return if click on last column's header
    }
    else if(col == 1)
    {
      _comparator.setCompareImageSetName(true);
    }
    else if(col == 2)
    {
      _comparator.setCompareImageSetDate(true);
    }
    
    Collections.sort(_rowData,_comparator);

    //update current selected row index after sorting
    _selectedImageSetRowIndex = _rowData.indexOf(currentSelected);
  }
  
  /**
   * @author Siew Yeng
   */
  private void notifyImageSetSelectionChanged(String imageSetName)
  {
    GuiObservable.getInstance().stateChanged(imageSetName, ImageSetEventEnum.VIRTUAL_LIVE_IMAGE_SET_SELECTED);
  }
  
  /**
   * @author Siew Yeng
   */
  public void clear()
  {
    _rowData.clear();
    _selectedImageSetRowIndex = -1;
  }
}
