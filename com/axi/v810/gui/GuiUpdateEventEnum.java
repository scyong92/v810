package com.axi.v810.gui;

/**
 * @author George A. David
 */
public class GuiUpdateEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static GuiUpdateEventEnum CAD_GRAPHICS = new GuiUpdateEventEnum(++_index);
  public static GuiUpdateEventEnum TUNER_INSPECTION_START = new GuiUpdateEventEnum(++_index);
  public static GuiUpdateEventEnum TUNER_INSPECTION_FINISH = new GuiUpdateEventEnum(++_index);
  public static GuiUpdateEventEnum INSPECTION_RUNS_UPDATED = new GuiUpdateEventEnum(++_index);
  public static GuiUpdateEventEnum TUNER_INSPECTION_START_FROM_OTHER_PANEL = new GuiUpdateEventEnum(++_index);	//Ngie Xing
  public static GuiUpdateEventEnum TUNER_INSPECTION_FINISHED_FROM_OTHER_PANEL = new GuiUpdateEventEnum(++_index);

  /**
   * @author George A. David
   */
  private GuiUpdateEventEnum(int id)
  {
    super(id);
  }
}
