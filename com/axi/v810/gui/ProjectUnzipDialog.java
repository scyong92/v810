package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.ndfReaders.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;



/**
 * Unzip the project files from a project .zip file.
 * @author Laura Cormos
 * @author Jack Hwee

 */
public class ProjectUnzipDialog extends EscapeDialog
{
  private static boolean _runFromMain = false;

  private static final String _appName = StringLocalizer.keyToString("PUD_GUI_NAME_KEY");
  private static final String _assertLogFile = "ProjectUnzipDialog";
  private static final String _zipFileFilterDescription = StringLocalizer.keyToString("PUD_ZIP_FILE_DESCRIPTION_KEY");

  // gui components
  private JPanel _initialThresholdSetNamePanel = new JPanel(new FlowLayout());    //Kok Chun, Tan
  private JPanel _optionPanel = new JPanel();
  private JPanel _actionPanel = new JPanel();
  private JButton _unzipButton = new JButton();
  private JButton _exitButton = new JButton();
  private JFileChooser _fileChooser = null;
  private JComboBox<Object> _initialThresholdSetCombobox;    //Kok Chun, Tan

  private String _inputDirectory = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private JPanel _chooseUnzipFilePanel = new JPanel();
  private BorderLayout _chooseUnzipFilePanelBorderLayout = new BorderLayout();
  private JLabel _zipFileLabel = new JLabel();
  private JButton _unzipFileBrowseButton = new JButton();
  private JTextField _zipFileTextField = new JTextField();
  private BorderLayout _unzipSelectionPanelBorderLayout = new BorderLayout();
  private JPanel _unzipSelectionPanel = new JPanel();
  private Border _empty55105SpaceBorder;
  private Border _empty5555SpaceBorder;
  private JPanel _buttonPanel = new JPanel();
  private GridLayout _buttonPanelGridLayout = new GridLayout();
  private FlowLayout _actionPanelFlowLayout = new FlowLayout();
  private JPanel _mainPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _projectsDirPanel = new JPanel();
  private BorderLayout _projectsDirPanelBorderLayout = new BorderLayout();
  private JLabel _projectsDirLabel = new JLabel();
  private JLabel _projectsDirLocationLabel = new JLabel();
  private JLabel _initialThresholdSetNameLabel = new JLabel(StringLocalizer.keyToString("MMGUI_SELECT_INITIAL_THRESHOLDS_ON_IMPORT_KEY") + ":"); //Kok Chun, Tan

  private JPanel _radioPanel = new JPanel();
  private ButtonGroup zipTypeSelectionButtonGroup = new ButtonGroup();
  private JRadioButton _v810NdfRadioButton = new JRadioButton(StringLocalizer.keyToString("PUD_SELECT_V810_RADIO_BUTTON_KEY"));
  private JRadioButton _5dxNdfRadioButton = new JRadioButton(StringLocalizer.keyToString("PUD_SELECT_5DX_RADIO_BUTTON_KEY"));
  private JRadioButton _5dxNdfDiffSubtypeRadioButton = new JRadioButton(StringLocalizer.keyToString("PUD_SELECT_5DX__DIFF_SUBTYPE_RADIO_BUTTON_KEY"));
  private FlowLayout _radiobuttonPanelFLowLayout = new FlowLayout();
  
  private MainMenuGui _mainUI = null;   //Kok Chun, Tan
  /**
   * @author Vincent Wong
   */
  public ProjectUnzipDialog(Frame parentFrame, boolean modal)
  {
    super(parentFrame, _appName, modal);
    Assert.expect(parentFrame != null);
    try
    {
      jbInit();
    }
    catch(RuntimeException e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _runFromMain = true;
          MainMenuGui mainMenu = MainMenuGui.getInstance();
          mainMenu.start();
          ProjectUnzipDialog projectUnzipDialog = new ProjectUnzipDialog(mainMenu, true);
          projectUnzipDialog.pack();
          projectUnzipDialog.setVisible(true);
        }
        catch (RuntimeException re)
        {
          re.printStackTrace();
          Assert.logException(re);
        }
      }
    });
  }

  /**
   * @author Laura Cormos
   */
  private void jbInit()
  {
    _empty55105SpaceBorder = BorderFactory.createEmptyBorder(5,5,10,5);
    _empty5555SpaceBorder = BorderFactory.createEmptyBorder(5,5,5,5);

    _unzipButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _unzipButton.setText(StringLocalizer.keyToString("PUD_GUI_UNZIP_BUTTON_KEY"));
    _unzipButton.setEnabled(false);
    _unzipButton.setMnemonic((StringLocalizer.keyToString("GUI_UNZIP_BUTTON_MNEMONIC_KEY")).charAt(0));
    _unzipButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unzipButton_actionPerformed(e);
      }
    });
    _exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _exitButton.setText(StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"));
    _exitButton.setMnemonic((StringLocalizer.keyToString("GUI_EXIT_BUTTON_MNEMONIC_KEY")).charAt(0));
    _exitButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        exitButton_actionPerformed(e);
      }
    });
    _actionPanel.setLayout(_actionPanelFlowLayout);
    setResizable(true);
    _actionPanel.setMinimumSize(new Dimension(405, 37));
    _chooseUnzipFilePanel.setLayout(_chooseUnzipFilePanelBorderLayout);
    _zipFileLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _zipFileLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _zipFileLabel.setLabelFor(_zipFileTextField);
    _zipFileLabel.setText(StringLocalizer.keyToString("PZD_GUI_ZIP_FILE_LABEL_KEY"));
    _unzipFileBrowseButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _unzipFileBrowseButton.setText(StringLocalizer.keyToString("GUI_BROWSE_BUTTON_KEY"));
    _unzipFileBrowseButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unzipFileBrowseButton_actionPerformed(e);
      }
    });

    zipTypeSelectionButtonGroup.add(_5dxNdfDiffSubtypeRadioButton); // Bee Hoon
    zipTypeSelectionButtonGroup.add(_5dxNdfRadioButton);
    zipTypeSelectionButtonGroup.add(_v810NdfRadioButton);
    //Kok Chun, Tan - add action listener for radio button, when v810 radio button is clicked, _initialThresholdSetCombobox is disable.
    _v810NdfRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _initialThresholdSetCombobox.setEnabled(false);
      }
    });
    _5dxNdfRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _initialThresholdSetCombobox.setEnabled(true);
      }
    });
    _5dxNdfDiffSubtypeRadioButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _initialThresholdSetCombobox.setEnabled(true);
      }
    });
    
    _v810NdfRadioButton.setSelected(true); //Jack Hwee
    
    _zipFileTextField.setText("");
    _zipFileTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _zipFileTextField.setPreferredSize(new Dimension(220, 20));
    _zipFileTextField.setEditable(false);
    _unzipSelectionPanelBorderLayout.setHgap(10);
    _unzipSelectionPanelBorderLayout.setVgap(10);
    _unzipSelectionPanel.setLayout(_unzipSelectionPanelBorderLayout);
    _unzipSelectionPanel.setBorder(_empty55105SpaceBorder);
    _unzipSelectionPanel.setMinimumSize(new Dimension(405, 27));
    _buttonPanel.setLayout(_buttonPanelGridLayout);
    _buttonPanelGridLayout.setColumns(2);
    _buttonPanelGridLayout.setHgap(10);
    _mainPanel.setLayout(new BorderLayout(5,10));
    _projectsDirPanel.setLayout(_projectsDirPanelBorderLayout);
    _projectsDirPanelBorderLayout.setHgap(5);
    _projectsDirPanel.setBorder(_empty5555SpaceBorder);
    _projectsDirLabel.setText(StringLocalizer.keyToString("PZD_GUI_PROJECTS_DIR_LABEL_KEY"));
    _projectsDirLocationLabel.setText(Directory.getProjectsDir());
     

    _radioPanel.setLayout(_radiobuttonPanelFLowLayout);
    _radiobuttonPanelFLowLayout.setHgap(5);
    _buttonPanel.add(_unzipButton, null);
    _buttonPanel.add(_exitButton, null);
    _actionPanel.add(_buttonPanel, null);
    _radioPanel.add(_v810NdfRadioButton);
    _radioPanel.add(_5dxNdfRadioButton);
    _radioPanel.add(_5dxNdfDiffSubtypeRadioButton);

    //Kok Chun, Tan
    //create select initial threshold set 
    _initialThresholdSetNameLabel.setFont(FontUtil.getBoldFont(_initialThresholdSetNameLabel.getFont(), Localization.getLocale()));
    java.util.List<String> initialThresholdsFileNames = FileName.getAllInitialThresholdSetNames();
    Object[] initialThresholdsAdditionalComboboxItems = initialThresholdsFileNames.toArray();
    String initialThresholdsFileUsed = ProjectInitialThresholds.getInstance().getInitialThresholdsFileNameWithoutExtension();

    _initialThresholdSetCombobox = new JComboBox<>((Object[]) initialThresholdsAdditionalComboboxItems);
    _initialThresholdSetCombobox.setSelectedItem(initialThresholdsFileUsed);
    _initialThresholdSetNamePanel.add(_initialThresholdSetNameLabel);
    _initialThresholdSetNamePanel.add(_initialThresholdSetCombobox);
    _optionPanel.setLayout(new BorderLayout(5, 10));
    _optionPanel.add(_radioPanel, BorderLayout.NORTH);
    _optionPanel.add(_initialThresholdSetNamePanel, BorderLayout.SOUTH);

    //disable _initialThresholdSetNamePanel because v810 radio button is selected initially
    _initialThresholdSetCombobox.setEnabled(false);

    _initialThresholdSetCombobox.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        initialThresholdSetCombobox_actionPerformed();
      }
    });
    
    _unzipSelectionPanel.add(_zipFileLabel, BorderLayout.WEST);
    _unzipSelectionPanel.add(_unzipFileBrowseButton, BorderLayout.EAST);
    _unzipSelectionPanel.add(_zipFileTextField, BorderLayout.CENTER);
    _chooseUnzipFilePanel.add(_projectsDirPanel, BorderLayout.NORTH);
    _projectsDirPanel.add(_projectsDirLabel, BorderLayout.WEST);
    _projectsDirPanel.add(_projectsDirLocationLabel, BorderLayout.CENTER);
    //_chooseUnzipFilePanel.add(_projectsDirPanel, BorderLayout.NORTH);
    _mainPanel.add(_optionPanel, BorderLayout.CENTER);
    _mainPanel.add(_actionPanel,  BorderLayout.SOUTH);
  
    
    getContentPane().add(_mainPanel,  BorderLayout.CENTER);
    _chooseUnzipFilePanel.add(_unzipSelectionPanel, BorderLayout.SOUTH);

    _mainPanel.add(_chooseUnzipFilePanel, BorderLayout.NORTH);
    //_projectsDirPanel.add(_projectsDirLocationLabel, BorderLayout.CENTER);
    _mainPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
  }
  
  /**
   * XCR-2188
   * @author Kok Chun, Tan
   */
  private void initialThresholdSetCombobox_actionPerformed()
  {
    String initialThresholdsFileName = (String) _initialThresholdSetCombobox.getSelectedItem();
    try
    {
      ProjectInitialThresholds.getInstance().setImportInitialThresholdsFileNameWithoutExtension(initialThresholdsFileName);
    }
    catch (DatastoreException ex)
    {
// something's wrong with software.config. Show the error and stop the import
      MessageDialog.showErrorDialog(_mainUI,
        ex.getLocalizedMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        true);

      return;
    }
  }
  
  /**
   * @author Vincent Wong
   * @author Jack Hwee
   */
  void unzipFileBrowseButton_actionPerformed(ActionEvent e)
  {

    if (_5dxNdfRadioButton.isSelected() == true || _5dxNdfDiffSubtypeRadioButton.isSelected() == true) {

      String fileNameFullPath = selectFile();

    if (fileNameFullPath.length() > 0)
    {
      _inputDirectory = FileUtil.getParent(fileNameFullPath);

        _zipFileTextField.setText(fileNameFullPath);
        _unzipButton.setEnabled(true);
      

      String extension = FileUtil.getExtension(fileNameFullPath).toLowerCase();

      if (extension.endsWith(FileName.getZipFileExtension().toLowerCase()) == false) {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_INVALID_FILE_EXTENSION_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);

        _unzipButton.setEnabled(false);

        return;
      }

      String parent = FileUtil.getParent(fileNameFullPath);

      if (parent.length() == 0) // need to specify full path.
      {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_DIRECTORY_NOT_SPECIFIED_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);

        _unzipButton.setEnabled(false);

        return;
      }

      File inFile = new File(fileNameFullPath);

      if (inFile.exists() == false) {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PUD_FILE_DOES_NOT_EXIST_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);

        _unzipButton.setEnabled(false);

        return;
      }

      }
    }

    else {
      String fileNameFullPath = selectFile();

      if (fileNameFullPath.length() > 0){

          _inputDirectory = FileUtil.getParent(fileNameFullPath);
          _zipFileTextField.setText(fileNameFullPath);
          _unzipButton.setEnabled(true);
            
      }
    }
  }

   /**
    * Reads a Zip file, iterating through each entry and dumping the contents
    * to the console.
    * Check the project belong to 5dx or not
    * @author Jack Hwee
    */
  private static boolean checkZipFile(String fileNameFullPath)
  {
     Assert.expect(fileNameFullPath != null);

    String dirNameFile = FileUtil.getNameWithoutExtension(fileNameFullPath);
    String zipFile = FileName.getNdfZipFileExtension();
    String projectNameFile = dirNameFile + zipFile;
    String rtfFile = FileName.getRtfZipFileExtension();
    String projectNameFile2 = dirNameFile + rtfFile;
  //  String outputDirectory = Directory.get5dxProjectDir(dirNameFile);
  //  String dirFile = FileName.getPanelNameFile();
  // String projectNameFile3 = outputDirectory + File.separator + projectNameFile + File.separator + dirFile;
    boolean isFiveDx = false;

    try
    {

      FileInputStream fis = new FileInputStream(fileNameFullPath);
      ZipInputStream zis = new ZipInputStream(fis);
      ZipEntry ze;

      while ((ze = zis.getNextEntry()) != null)
      {

        if ((ze.getName().contains(projectNameFile)))
        {
          isFiveDx = true;
          break;
        }
        else
        {
          isFiveDx = false;
        }

      }
      zis.close();

      FileInputStream fis2 = new FileInputStream(fileNameFullPath);
      ZipInputStream zis2 = new ZipInputStream(fis2);
      ZipEntry ze2;
      if (isFiveDx)
      {
        while ((ze2 = zis2.getNextEntry()) != null)
        {
          if (ze2.getName().contains(projectNameFile2))
          {
            isFiveDx = true;
            break;
          }
          else
          {
            isFiveDx = false;
          }

        }
        zis2.close();
      }

   /*   FileInputStream fis3 = new FileInputStream(projectNameFile3);
      InputStreamReader isr3 = new InputStreamReader(fis3);
      BufferedReader is3 = new BufferedReader(isr3);
      String projectName;

      while ((projectName = is3.readLine()) != null)
      {

        if (projectName.contains(".CHECKSUM:"))
        {

          isFiveDx = true;
          break;
        }
        else
        {
          isFiveDx = false;
        }
      }

      is3.close();  */

    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return isFiveDx;
  }

  private boolean isFiveDxZip(String fileNameFullPath)
  {
    return checkZipFile(fileNameFullPath);
  }


   /**
   * @author Vincent Wong
   * @author Jack Hwee
   */
  void unzipButton_actionPerformed(ActionEvent e)
  {
    Assert.expect(_zipFileTextField != null);

    String zipFileName = _zipFileTextField.getText();

    if ( zipFileName.length() == 0 )
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_FILE_NOT_SPECIFIED_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );

      _unzipButton.setEnabled(false);
      _v810NdfRadioButton.setEnabled(true);
      _5dxNdfRadioButton.setEnabled(true);
      _5dxNdfDiffSubtypeRadioButton.setEnabled(true);
      
      return;
    }

    String extension = FileUtil.getExtension(zipFileName).toLowerCase();

    if ( extension.endsWith( FileName.getZipFileExtension().toLowerCase()) == false )
    {
      JOptionPane.showMessageDialog( this, StringLocalizer.keyToString("PZD_INVALID_FILE_EXTENSION_KEY"),
                                     _appName, JOptionPane.ERROR_MESSAGE );

      _unzipButton.setEnabled(false);
      _v810NdfRadioButton.setEnabled(true);
      _5dxNdfRadioButton.setEnabled(true);
      _5dxNdfDiffSubtypeRadioButton.setEnabled(true);
    

      return;
    }

    String parent = FileUtil.getParent(zipFileName);

    if (parent.length() == 0) // need to specify full path.
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_DIRECTORY_NOT_SPECIFIED_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _unzipButton.setEnabled(false);
      _v810NdfRadioButton.setEnabled(true);
      _5dxNdfRadioButton.setEnabled(true);
      _5dxNdfDiffSubtypeRadioButton.setEnabled(true);
     

      return;
    }

    File inFile = new File(zipFileName);

    if (inFile.exists() == false)
    {
      JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PUD_FILE_DOES_NOT_EXIST_KEY"),
        _appName, JOptionPane.ERROR_MESSAGE);

      _unzipButton.setEnabled(false);

    
      return;
    }

    if (_5dxNdfRadioButton.isSelected() == true)
    {
      NdfReader.set5dxNdfWithDiffSubtype('0');
      if (isFiveDxZip(zipFileName))
      {
         String projectName = FileUtil.getNameWithoutExtension(zipFileName);
         // check for a valid project name
        if (Project.isProjectNameValid(projectName) == false)
        {
          invalidProjectNameMessage(projectName, true);
          return;  // error, do not continue
        }

        unzip5dxFile(_zipFileTextField.getText());
   
      }
      else
      {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_INVALID_5DX_ZIP_FILE_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);
      }

    }
    else if (_5dxNdfDiffSubtypeRadioButton.isSelected() == true)
    {
      NdfReader.set5dxNdfWithDiffSubtype('1');
      if (isFiveDxZip(zipFileName))
      {
         String projectName = FileUtil.getNameWithoutExtension(zipFileName);
         // check for a valid project name
        if (Project.isProjectNameValid(projectName) == false)
        {
          invalidProjectNameMessage(projectName, true);
          return;  // error, do not continue
        }

        unzip5dxFile(_zipFileTextField.getText());
   
      }
      else
      {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_INVALID_5DX_ZIP_FILE_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);
      }
    }
    else
    {
	  NdfReader.set5dxNdfWithDiffSubtype('0'); 
	  if (!isFiveDxZip(zipFileName))
      {  
        NdfReader.set5dxNdfFlag(false);
        unzipFile(_zipFileTextField.getText());
        _v810NdfRadioButton.setEnabled(true);
        _5dxNdfRadioButton.setEnabled(true);
      }
      else
      {
        JOptionPane.showMessageDialog(this, StringLocalizer.keyToString("PZD_INVALID_V810_ZIP_FILE_KEY"),
          _appName, JOptionPane.ERROR_MESSAGE);
      }
    }

  }


  /**
   * @author Laura Cormos
   * @author Erica Wheatcroft
   */
  private void unzipFile(final String zipFileName)
  {
    Assert.expect(zipFileName != null);

    // Construct the output directory path in the default projects directory
    String projectName = FileUtil.getNameWithoutExtension(zipFileName);
    final String outputDirectory = Directory.getProjectDir(projectName);
    File od = new File(outputDirectory);

    // check to see if this project is the currently open project
    if (Project.isCurrentProjectLoaded())
    {
      String loadedProjectName = Project.getCurrentlyLoadedProject().getName();
      String unzipProjectName = FileUtil.getNameWithoutExtension(zipFileName);
      if (loadedProjectName.equalsIgnoreCase(unzipProjectName))
      {
        LocalizedString message = new LocalizedString("PUD_PROJECT_ALREADY_LOADED_KEY", new Object[]{loadedProjectName});
        JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
                                      _appName, JOptionPane.ERROR_MESSAGE);

        _unzipButton.setEnabled(false);

        return;

      }
    }

    if (od.isDirectory() && od.exists())
    {
      LocalizedString message = new LocalizedString("PUD_PROJECT_EXISTS_OVERWRITE_KEY", new Object[]{projectName});
      if (JOptionPane.showConfirmDialog( this,
                                         StringUtil.format(StringLocalizer.keyToString(message), 50),
                                         StringLocalizer.keyToString("PUD_DIRECTORY_ALREADY_EXISTS_KEY"),
                                         JOptionPane.YES_NO_OPTION,
                                         JOptionPane.QUESTION_MESSAGE ) == JOptionPane.YES_OPTION)
      {
        // delete all project (not image) files

        String imagesDirectory = Directory.getXrayImagesDir(projectName);
        java.util.List<String> directoriesNotToDelete = new ArrayList<String>();
        directoriesNotToDelete.add(imagesDirectory);

        // now, do we need to remove the algorithm Learning or not?
        // if the zip file contains algorithmLearning, then we should remove, else not

        try
        {
          FileUtilAxi.deleteDirectoryContents(od.getPath(), directoriesNotToDelete);
        }
        catch (DatastoreException ex)
        {
          MessageDialog.showErrorDialog(ProjectUnzipDialog.this, ex, _appName, true);
        }
      }
      else
        return;
    }
    // Change the cursor temporarily and wait for the unzip to finish.
    setEnabled(false);
    setCursor(new Cursor(Cursor.WAIT_CURSOR));

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          ProjectArchiver archiver = new ProjectArchiver();
          archiver.unzip(zipFileName, outputDirectory);
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true);
              JOptionPane.showMessageDialog(ProjectUnzipDialog.this, StringLocalizer.keyToString("PUD_UNZIP_COMPLETED_KEY"),
                                            _appName, JOptionPane.INFORMATION_MESSAGE);
              // notify Home panel that project info may have changed
              MainMenuGui.getInstance().updateHomePanel();
            }
          });
        }
        catch (final DatastoreException ie)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
              setEnabled(true); // allow user to press a button
              MessageDialog.reportIOError(ProjectUnzipDialog.this, ie.getLocalizedMessage());
            }
          });
        }
      }
    });
  }
  /**
   * When the exit button is pressed the Project Unzip Dialog will exit.
   * @author Vincent Wong
   */
  void exitButton_actionPerformed(ActionEvent e)
  {
    exitDialog();
  }

  /**
   * Exit this if the escape key is pressed.
   * @author Vincent Wong
   */
  private void checkForEscapeKeyPressed(KeyEvent e)
  {
    if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
      exitDialog();
  }

  /**
   * @author Vincent Wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_CLOSING)
      exitDialog();
  }

  /**
   * @author Vincent Wong
   */
  private void exitDialog()
  {
    if (_runFromMain)
      System.exit(0);
    else
    {
      setVisible(false);
      dispose();
    }
  }

  /**
   * This method displays a file chooser and return the file selected by the user.
   * @author Vincent Wong
   */
  private String selectFile()
  {
    if ( _fileChooser == null )
    {
      // start in the same directory where the zip dialog puts zipped projects by default
       _fileChooser = new JFileChooser()
       {
         public void approveSelection()
         {
           File selectedFile = getSelectedFile();
           if (selectedFile.exists())
             super.approveSelection();
           else
           {
             String message = StringLocalizer.keyToString(new LocalizedString("FILE_DIALOG_SELECTION_DOES_NOT_EXIST_KEY", new Object[]{selectedFile}));
             JOptionPane.showMessageDialog(this, StringUtil.format(message, 50));
           }
         }
       };

//       _fileChooser.setCurrentDirectory(new File(Directory.getLegacyNdfDir()));
       _fileChooser.setCurrentDirectory(new File(Directory.getProjectsDir()));

       // initialize the JFIle Dialog.
       FileFilter fileFilter = _fileChooser.getAcceptAllFileFilter();
       _fileChooser.removeChoosableFileFilter(fileFilter);

       // create and instance of Zip File Filter
       FileFilterUtil zipFileFilter = new FileFilterUtil(FileName.getZipFileExtension(), _zipFileFilterDescription);

       _fileChooser.addChoosableFileFilter(zipFileFilter);
       _fileChooser.setFileFilter(zipFileFilter); // default to new zip file
    }

    if ( _fileChooser.showDialog(this, StringLocalizer.keyToString("GUI_SELECT_FILE_KEY")) != JFileChooser.APPROVE_OPTION )
      return "";

    File selectedFile = _fileChooser.getSelectedFile();

    if (selectedFile.exists())
    {
      if (selectedFile.isFile())
        return selectedFile.getPath();
      else
        return "";
    }
    else
      return "";
  }

   private void invalidProjectNameMessage(String projectName, boolean insertFixMessage)
  {
    LocalizedString localizedMessage = new LocalizedString("GUI_SAVE_AS_INVALID_PROJECT_NAME_KEY",
        new Object[]{projectName, Project.getProjectNameIllegalChars()});

    String finalMessage = StringLocalizer.keyToString(localizedMessage);
    if (insertFixMessage)
    {
      finalMessage += "\n\n" + StringLocalizer.keyToString("GUI_IMPORT_FIX_PROJECT_NAME_KEY");
    }

    JOptionPane.showMessageDialog(
      this,
      finalMessage,
      StringLocalizer.keyToString("GUI_ERROR_MESSAGE_DIALOG_TITLE_KEY"),
      JOptionPane.ERROR_MESSAGE);
   }

   /**
   * @author Jack Hwee
   *
   */
  private void unzip5dxFile(final String zipFileName)
  {
    Assert.expect(zipFileName != null);

    // Construct the output directory path in the default projects directory
    String projectName = FileUtil.getNameWithoutExtension(zipFileName);
    final String outputDirectory = Directory.get5dxProjectDir(projectName);

    File od = new File(outputDirectory);

    // check to see if this project is the currently open project
    if (Project.isCurrentProjectLoaded()) {
      String loadedProjectName = Project.getCurrentlyLoadedProject().getName();
      String unzipProjectName = FileUtil.getNameWithoutExtension(zipFileName);
      if (loadedProjectName.equalsIgnoreCase(unzipProjectName)) {
        LocalizedString message = new LocalizedString("PUD_PROJECT_ALREADY_LOADED_KEY", new Object[]{loadedProjectName});
        JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
          _appName, JOptionPane.ERROR_MESSAGE);

       // _unzipButton.setEnabled(false);

        return;

      }
    }

    if (od.isDirectory() && od.exists()) {
      LocalizedString message = new LocalizedString("PUD_PROJECT_EXISTS_OVERWRITE_KEY", new Object[]{projectName});
      if (JOptionPane.showConfirmDialog(this,
        StringUtil.format(StringLocalizer.keyToString(message), 50),
        StringLocalizer.keyToString("PUD_DIRECTORY_ALREADY_EXISTS_KEY"),
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) 
      {
        // delete all project (not image) files

        String imagesDirectory = Directory.getXrayImagesDir(projectName);
        java.util.List<String> directoriesNotToDelete = new ArrayList<String>();
        directoriesNotToDelete.add(imagesDirectory);

        // now, do we need to remove the algorithm Learning or not?
        // if the zip file contains algorithmLearning, then we should remove, else not

        try {
          FileUtilAxi.deleteDirectoryContents(od.getPath(), directoriesNotToDelete);
        }
        catch (DatastoreException ex) {
          MessageDialog.showErrorDialog(ProjectUnzipDialog.this, ex, _appName, true);
        }
      }
      else 
        return;
    }
    // Change the cursor temporarily and wait for the unzip to finish.


    try {
      ProjectArchiver archiver = new ProjectArchiver();
      archiver.unzip(zipFileName, outputDirectory);
      unzipNdfFile(outputDirectory);

      //  setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      // setEnabled(true);
         JOptionPane.showMessageDialog(ProjectUnzipDialog.this, StringLocalizer.keyToString("PUD_UNZIP_COMPLETED_KEY"),
                                    _appName, JOptionPane.INFORMATION_MESSAGE);
      // notify Home panel that project info may have changed
      MainMenuGui.getInstance().updateHomePanel();
      
      dispose();
      NdfReader.set5dxNdfFlag(true);
      autoImportProject(zipFileName);

    }
    catch (final DatastoreException ie) {
      setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      setEnabled(true); // allow user to press a button
      MessageDialog.reportIOError(ProjectUnzipDialog.this, ie.getLocalizedMessage());
    }

  }


  /**
   * @author Jack Hwee
   *
   */
  private void unzipNdfFile(final String zipNdfFileDir)
  {
    Assert.expect(zipNdfFileDir != null);

    // Construct the output directory path in the default projects directory
    String projectName = FileUtil.getNameWithoutExtension(zipNdfFileDir);
    String zipFile = FileName.getNdfZipFileExtension();
    String NdfName = projectName + zipFile;
    final String NdfZipName = zipNdfFileDir + File.separator + NdfName;

    final String outputDirectory = FileUtil.getPathWithoutFileName(zipNdfFileDir);
    final String outputDirectory2 = outputDirectory + File.separator + projectName;

    File od = new File(outputDirectory2);

    // check to see if this project is the currently open project
    if (Project.isCurrentProjectLoaded()) {
      String loadedProjectName = Project.getCurrentlyLoadedProject().getName();
      String unzipProjectName = FileUtil.getNameWithoutExtension(zipNdfFileDir);
      if (loadedProjectName.equalsIgnoreCase(unzipProjectName)) {
        LocalizedString message = new LocalizedString("PUD_PROJECT_ALREADY_LOADED_KEY", new Object[]{loadedProjectName});
        JOptionPane.showMessageDialog(this, StringUtil.format(StringLocalizer.keyToString(message), 50),
          _appName, JOptionPane.ERROR_MESSAGE);

        _unzipButton.setEnabled(false);

        return;

      }
    }

    try {
      ProjectArchiver archiver = new ProjectArchiver();
      archiver.unzip(NdfZipName, outputDirectory2);

    }
    catch (final DatastoreException ie) {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
          setEnabled(true); // allow user to press a button
          MessageDialog.reportIOError(ProjectUnzipDialog.this, ie.getLocalizedMessage());
        }
      });
    }

  }

  /**
   * This method is overwritten from ancenstor Component. It's meant to enable/disable all controls on the dialog while
   * something else is going on
   * @author Laura Cormos
   */
  public void setEnabled(boolean b)
  {
    _unzipFileBrowseButton.setEnabled(b);
    _unzipButton.setEnabled(b);
    _exitButton.setEnabled(b);
  }
  

  /**
   * @author Jack Hwee
   */
  public void autoImportProject(final String projectToLoad)
  {
    Assert.expect(projectToLoad != null);

    final String projectName = FileUtil.getNameWithoutExtension(projectToLoad);
    final MainMenuGui mainMenu = MainMenuGui.getInstance();

    mainMenu.getMainMenuBar().loadOrImportProject(false, projectName, projectName);

    // wei chin added for prompt user the test information
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // Bee Hoon, XCR1653 - Do not prompt this message if the project is not loaded or imported
        if (mainMenu.getMainMenuBar().isProjectLoaded())
          MessageDialog.showInformationDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("MMGUI_IMPORT_NDF_WARNING_MESSAGE_KEY"), StringLocalizer.keyToString("MM_GUI_LOAD_PROJECT_TITLE_KEY"));
      }
    });
  }
}
  

