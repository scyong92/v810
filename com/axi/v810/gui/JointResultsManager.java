package com.axi.v810.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Panel;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This is a helper class for GUI to access results information saved to disk.
 * It usually returns a list of JointResultsEntry object to be displayed by GUI.
 * @author Rex Shang
 */
public class JointResultsManager
{
  private static JointResultsManager _instance = null;
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private ProgressDialog _jointResultsProgressCancelDialog;
  private Font _dialogFont = FontUtil.getDefaultFont();
  private boolean _collectJointResults = false;
  private boolean _jointResultsReadCancelled = false;
  private ArrayList<JointResultsEntry> _selectedJointResults = new ArrayList<JointResultsEntry>();  // for Review Results (all)
  private ArrayList<JointResultsEntry> _jointResultsEntries;
  private InspectionResultsReader _inspResultsReader;
  // map (boardname, refDesig) to component results
  private Map<Pair<String, String>, ComponentResults> _allComponentResultsMap = new HashMap<Pair<String, String>, ComponentResults>();  // for Review Results (all)
  long _maxMem;
  private boolean _lowOnMemory = false;
  List<Long> _completedResults = new ArrayList<Long>();
  private ArrayList<JointResultsEntry> _tempJointResults = new ArrayList<JointResultsEntry>();   // for Review Results single file read results
  private Map<Pair<String, String>, ComponentResults> _tempComponentResultsMap = new HashMap<Pair<String, String>, ComponentResults>(); // for Review Results single file read results
  // XCR-3255 Software Unable to load all results (Fine Tuning) due to Insufficient Memory - intermittent
  private final long _MINIMUM_MEMORY_REQUIRED_IN_BYTES = 300647710;

  /**
   * @return an instance to the JointResultsManager class
   * @author Rex Shang
   */
  public static JointResultsManager getInstance()
  {
    if (_instance == null)
      _instance = new JointResultsManager();

    return _instance;
  }

  /**
   * @author Rex Shang
   */
  private JointResultsManager()
  {
    _maxMem = Runtime.getRuntime().maxMemory();
  }

  /**
   * @return a list of PanelResults for a list of inspection runs for given
   * project name.  A PanelResults contains the panel serial # or run
   * description. No measurement data is included.
   * @author Rex Shang
   */
  public List<PanelResults> getPanelResultsList(String projectName, List<Long> inspectionRuns)
  {
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(inspectionRuns != null, "Inspection run list is null.");
//    System.out.println("JointResultsManager.getPanelResultsList(" + projectName + ")");

    List<PanelResults> panelResultsList = new ArrayList<PanelResults>();
    InspectionResultsHeaderReader inspResultsHeaderReader = new InspectionResultsHeaderReader();
    // list of path names that had open errors - the user can't fix them so just create
    // a list of the bad ones that should be deleted and put in one error dialog. Doing a
    // dialog per bad file could get annoying.
    List <String> datastoreExceptionFilePaths = new ArrayList<String>();

    int i = 0;
    for (Long runId : inspectionRuns)
    {
      i++;
      try
      {
        // Open the run result file indicated by run start time.
        inspResultsHeaderReader.open(projectName, runId);
        PanelResults panelResults = inspResultsHeaderReader.getPanelResults();
        panelResultsList.add(panelResults);
      }
      catch (DatastoreException de)
      {
        // some kind of error occurred - save the name for a mass error message
        String filePath = FileName.getResultsFileFullPath(projectName, runId);
        datastoreExceptionFilePaths.add(filePath);
      }
      finally
      {
        // Close result file.
        inspResultsHeaderReader.close();
      }
    }

    // Show all file open errors in a single dialog

    // XCR-3341 Result file could not be removed during production
    // Temporary commented out by Lee Herng 02 Mar 2016 - To prevent multiple error message prompt out
    // during reading result files, we decided to dump the file path into XrayTester log instead.
    if (datastoreExceptionFilePaths.size() > 0)
    {
      String allFilePaths = "";
      for (String filePath : datastoreExceptionFilePaths)
      {
        allFilePaths += "\n   " + filePath;
      }
      
      System.out.println("JointResultsManager: Detected error while reading below result files:" + allFilePaths);

//      final String finalExceptionString =
//      StringLocalizer.keyToString(
//      new LocalizedString("RDGUI_BAD_RESULTS_FILE_KEY",
//      new Object[]{allFilePaths}));
//
//      SwingUtils.invokeLater(new Runnable()
//      {
//        public void run()
//        {
//          MessageDialog.showErrorDialog(
//            MainMenuGui.getInstance(),
//            finalExceptionString,
//            StringLocalizer.keyToString("RDGUI_BAD_RESULTS_FILE_TITLE_KEY"),
//            true);
//        }
//      });
    }

    return panelResultsList;
  }

  /**
   * @author George Booth
   */
  private void createJointResultsProgressCancelDialog(int lastNum)
  {
//    System.out.println("createJointResultsProgressCancelDialog(" + lastNum + ")");
    _jointResultsProgressCancelDialog = new ProgressDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString("RDGUI_LOADING_MEASUREMENT_RESULTS_TITLE_KEY"),
      StringLocalizer.keyToString("RDGUI_READING_MEASUREMENT_RESULTS_MESSAGE_KEY"),
      StringLocalizer.keyToString("RDGUI_LOADING_MEASUREMENT_RESULTS_MESSAGE_KEY"),
      StringLocalizer.keyToString("RDGUI_LOADING_MEASUREMENT_RESULTS_CLOSE_MESSAGE_KEY"),
      StringLocalizer.keyToString("RDGUI_LOADING_MEASUREMENT_RESULTS_CANCEL_MESSAGE_KEY"),
      StringLocalizer.keyToString("RDGUI_LOADING_MEASUREMENT_RESULTS_STATUS_KEY"),
      0, lastNum, true);

    _jointResultsProgressCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _collectJointResults = false;
        _jointResultsReadCancelled = true;
      }
    });

    SwingUtils.setFont(_jointResultsProgressCancelDialog, _dialogFont);
    _jointResultsProgressCancelDialog.pack();
    SwingUtils.centerOnComponent(_jointResultsProgressCancelDialog, MainMenuGui.getInstance());
    _collectJointResults = true;
    _jointResultsReadCancelled = false;
    _lowOnMemory = false;
  }

  /**
   * @author George Booth
   */
  public boolean jointResultsReadCancelled()
  {
    return _jointResultsReadCancelled;
  }

  /**
   * @author George Booth
   */
  private void closeJointResultsProgressCancelDialog(boolean force)
  {
//    System.out.println("closeJointResultsProgressCancselDialog(" + force + ")");
    if (_jointResultsProgressCancelDialog != null)
    {
      // wait until visible if not forced
      if (force == false)
      {
        while (_jointResultsProgressCancelDialog.isVisible() == false)
        {
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
      }
      _jointResultsProgressCancelDialog.dispose();
      _jointResultsProgressCancelDialog = null;
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<Long> getCompletedResultsList()
  {
    Assert.expect(_completedResults != null);
    
    // return a copy of the list -- we don't want outside guys modifying this list for us.
    return new ArrayList<Long>(_completedResults);
  }
  
  /**
   * Get failed component and all joint results from the inspection runs for projectName
   * @author George Booth
   */
  public List<JointResultsEntry> getAllResultsFromRuns(final String projectName, final List<Long> inspectionRuns)
  {
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(inspectionRuns != null, "Inspection runs object is null");
//    System.out.println("JointResultsManager.getAllResultsFromRuns(" + projectName + ")");
    
    // analyze if we need to re-read stuff or can we just add to our already read in list.
    // first, if we need to REMOVE any results data, let's just re-read everything
    if (inspectionRuns.containsAll(_completedResults) == false)
    {
      _completedResults.clear();
      _selectedJointResults.clear();
      _allComponentResultsMap.clear();
    }    
    
    createJointResultsProgressCancelDialog(inspectionRuns.size() - _completedResults.size());

    _inspResultsReader = new InspectionResultsReader();

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        int num = 0;
        if (_jointResultsProgressCancelDialog != null)
          _jointResultsProgressCancelDialog.updateProgressBar(num);
        
        for (Long runId : inspectionRuns)
        {
          
          if (_collectJointResults)
          {
                     
            try
            {
              if (_completedResults.contains(runId) == false)
              {
                num++;
                // clear out the single runId results before we read them in
                _tempJointResults.clear();
                _tempComponentResultsMap.clear();
                // read results for one runId
                getReviewDefectsInspectionResultsFor(projectName, runId);
                // since no exception was thrown, add those results to our bigger list
                _selectedJointResults.addAll(_tempJointResults);
                _allComponentResultsMap.putAll(_tempComponentResultsMap);
                _completedResults.add(runId);
                _tempJointResults.clear();
                _tempComponentResultsMap.clear();
              }
            }
            catch (OutOfMemoryDatastoreException memEx)
            {
              // failed because of out of memory
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {                 
                  MessageDialog.showErrorDialog(MainMenuGui.getInstance(), 
                          StringLocalizer.keyToString("DS_ERROR_LOW_MEMORY_FOR_RESULTS_KEY"), 
                          StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), 
                          true);
                  closeJointResultsProgressCancelDialog(true);
                }
              });
              return;
            }            
            if (_jointResultsProgressCancelDialog != null)
              _jointResultsProgressCancelDialog.updateProgressBar(num);
          }
        }

        // get back on swing thread
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            closeJointResultsProgressCancelDialog(true);
          }
        });
      }
    });
    
    _jointResultsProgressCancelDialog.setVisible(true);
    _inspResultsReader = null;
    _tempJointResults.clear();
    _tempComponentResultsMap.clear();

    return _selectedJointResults;
  }

  /**
   * @author George Booth
   */
  private void getReviewDefectsInspectionResultsFor(final String projectName, final long runId) throws OutOfMemoryDatastoreException
  {
    try
    {
      // Open the run result file indicated by run start time.
      _inspResultsReader.open(projectName, runId);

      // get all component results
      List<ComponentName> componentNames = _inspResultsReader.getComponentNames();
//      System.out.println("glb - get " + componentNames.size() + " component results");
      for (ComponentName componentName : componentNames)
      {
        if (componentExists(componentName))
        {
//          System.out.println("  component = " + componentName.toString());
          checkForMemory();
          ComponentResults componentResults = _inspResultsReader.getComponentResult(componentName);
          String boardSerialNumber = _inspResultsReader.getBoardResults().get(componentName.getBoardName()).getSerialNumber();
          Assert.expect(componentResults != null);

          // add to boardName,RefDesig to ComponentResults map
          Pair<String, String> key = new Pair<String, String>(
              componentName.getBoardName(), componentName.getReferenceDesignator());
          _tempComponentResultsMap.put(key, componentResults);

          // add to the list of results
          JointResultsEntry entry = new JointResultsEntry();
          entry.setRunStartTime(runId);
          entry.setComponentResults(componentResults);
          
          //Jack Hwee - If the boardSerialNumber is empty string, get the userDescription from image set as boardSerialNumber 
          //            so that user can get serial number from image set, not only during production run.
          if (boardSerialNumber.isEmpty())
            boardSerialNumber = _inspResultsReader.getImageSetUserDescription();         
          entry.setBoardSerialNumber(boardSerialNumber);

          //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
          //base on selected measurement type in Measurements Tab.
          //We use the first measurement index since it is first load from database.
          if(componentResults.getAllMeasurementResults().isEmpty() == false)
          {
            MeasurementResult measurementResult = componentResults.getAllMeasurementResults().get(0);
            entry.setSliceName(measurementResult.getSliceNameEnum().getName());
            entry.setMeasurementName(measurementResult.getMeasurementEnum().getName());
            entry.setMeasurementUnit(measurementResult.getMeasurementUnitsEnum().getName());
            entry.setMeasurementEnum(measurementResult.getMeasurementEnum());
          }
          
          _tempJointResults.add(entry);
        }
        else
        {
//          System.out.println("  component = " + componentName.toString() + " no longer exists");
        }
      }

      // get all joint results matching pass or fail
      List<JointName> jointNames = _inspResultsReader.getJointNames();
//      System.out.println("glb - get " + jointNames.size() + " joint results");
      for (JointName jointName : jointNames)
      {
        if (jointExists(jointName))
        {
//          System.out.println("  joint name = " + jointName.toString());
          checkForMemory();
          JointResults jointResults = _inspResultsReader.getJointResult(jointName);
          String boardSerialNumber = _inspResultsReader.getBoardResults().get(jointName.getBoardName()).getSerialNumber();
          JointResultsEntry entry = new JointResultsEntry();
          entry.setRunStartTime(runId);
          entry.setJointResults(jointResults);
          
          //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
          //base on selected measurement type in Measurements Tab.
          //We use the first measurement index since it is first load from database.
          if(jointResults.getAllMeasurementResults().isEmpty() == false)
          {
            MeasurementResult measurementResult = jointResults.getAllMeasurementResults().get(0);
            entry.setSliceName(measurementResult.getSliceNameEnum().getName());
            entry.setMeasurementName(measurementResult.getMeasurementEnum().getName());
            entry.setMeasurementUnit(measurementResult.getMeasurementUnitsEnum().getName());
            entry.setMeasurementEnum(measurementResult.getMeasurementEnum());
          }
        
          //Jack Hwee - If the boardSerialNumber is empty string, get the userDescription from image set as boardSerialNumber 
          //            so that user can get serial number from image set, not only during production run.
          if (boardSerialNumber.isEmpty())
            boardSerialNumber = _inspResultsReader.getImageSetUserDescription();
          entry.setBoardSerialNumber(boardSerialNumber);

          _tempJointResults.add(entry);
        }
        else
        {
//          System.out.println("  joint name = " + jointName.toString() + " no longer exists");
        }
      }      
    }
    catch (OutOfMemoryDatastoreException memEx)
    {
      // don't save up this exception, we must throw it
      throw memEx;
    }
    catch (final DatastoreException de)
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showErrorDialog(MainMenuGui.getInstance(), de, StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
        }
      });
    }
    finally
    {
      // Close result file.
      _inspResultsReader.close();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void checkForMemory() throws OutOfMemoryDatastoreException
  {       
    long freeMemAfter = Runtime.getRuntime().freeMemory();    
//    double percentAvail = (100 - (100 * (_maxMem - freeMemAfter) / _maxMem));
    
    // XCR-3255 : Software Unable to load all results (Fine Tuning) due to Insufficient Memory - intermittent
    // Wei Chin - change from % to fixed amount as the memory had increase from 4GB to 24GB. The memory available should be able to handle most of the case. 
    if (freeMemAfter < _MINIMUM_MEMORY_REQUIRED_IN_BYTES)
    {
      if (_lowOnMemory)
      {
        // we gave it a small chance to garbage collect, but now we must throw the exception
        throw new OutOfMemoryDatastoreException();
      }
      else
      {
        _lowOnMemory = true;
      }
    }
  }
  
  /**
   * @author Rex Shang
   */
  public List<JointResultsEntry> getResultsBySubtypeMeasurementTypeSliceName(
      final String projectName,
      final List<Long>inspectionRuns,
      final Subtype subtype,
      final MeasurementEnum measurementType,
      final SliceNameEnum sliceType)
  {
    Assert.expect(subtype != null, "Subtype is null.");
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(inspectionRuns != null, "Inspection run list is null.");
    Assert.expect(measurementType != null, "MeasurementType is null.");
    Assert.expect(sliceType != null, "SliceType is null");
//    System.out.println("JointResultsManager.getResultsBySubtypeMeasurementTypeSliceName(" + projectName + " " + subtype + " " + measurementType + " " + sliceType + ")");

    createJointResultsProgressCancelDialog(inspectionRuns.size());

    _jointResultsEntries = new ArrayList<JointResultsEntry>();
    // boardName,RefDesig to ComponentResults map
    _allComponentResultsMap = new HashMap<Pair<String, String>, ComponentResults>();

    _inspResultsReader = new InspectionResultsReader();

    _swingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        int num = 0;
        if (_jointResultsProgressCancelDialog != null)
          _jointResultsProgressCancelDialog.updateProgressBar(num);

        for (Long runId : inspectionRuns)
        {
          if (_collectJointResults)
          {
            num++;

            getReviewMeasurementsInspectionResultsFor(projectName, runId, subtype, measurementType, sliceType);

            if (_jointResultsProgressCancelDialog != null)
              _jointResultsProgressCancelDialog.updateProgressBar(num);
          }
        }

        // get back on swing thread
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            closeJointResultsProgressCancelDialog(true);
          }
        });
      }
    });
    
    _jointResultsProgressCancelDialog.setVisible(true);
    _inspResultsReader = null;

    return _jointResultsEntries;
  }

  /**
   * @author George Booth
   */
  private void getReviewMeasurementsInspectionResultsFor(String projectName,
      Long runId,
      Subtype subtype,
      MeasurementEnum measurementType,
      SliceNameEnum sliceType)
  {
    try
    {
      // Open the run result file indicated by run start time.
      _inspResultsReader.open(projectName, runId);

      // get component results
      List<ComponentName> componentNames = _inspResultsReader.getComponentNames();
//      System.out.println("glb - get " + componentNames.size() + " component results");
      for (ComponentName componentName : componentNames)
      {
        if (componentExists(componentName))
        {
//            System.out.println("  component = " + componentName.toString());

          ComponentResults componentResults = _inspResultsReader.getComponentResult(componentName);
          Assert.expect(componentResults != null);

          // add to boardName,RefDesig to ComponentResults map
          Pair<String, String> key = new Pair<String, String>(
              componentName.getBoardName(), componentName.getReferenceDesignator());
          _allComponentResultsMap.put(key, componentResults);

          // add to the list of results if filter matches
          List<MeasurementResult> measurementResults = componentResults.getAllMeasurementResults();
          for (MeasurementResult measureResult : measurementResults)
          {
            if (subtype.getLongName().equals(measureResult.getSubtypeLongName())
                && measurementType.equals(measureResult.getMeasurementEnum())
                && sliceType.equals(measureResult.getSliceNameEnum()))
            {
              String boardSerialNumber = _inspResultsReader.getBoardResults().get(componentName.getBoardName()).getSerialNumber();
              JointResultsEntry entry = new JointResultsEntry();
              entry.setRunStartTime(runId);
              entry.setComponentResults(componentResults);
              //Jack Hwee - If the boardSerialNumber is empty string, get the userDescription from image set as boardSerialNumber 
              //            so that user can get serial number from image set, not only during production run.
              if (boardSerialNumber.isEmpty())
                boardSerialNumber = _inspResultsReader.getImageSetUserDescription();
              entry.setBoardSerialNumber(boardSerialNumber);

              // Added by Lee Herng 11 Feb 2014 - To handle all measurement dumping
              entry.setSliceName(sliceType.getName());
              entry.setMeasurementName(measurementType.getName());
              entry.setMeasurementUnit(measureResult.getMeasurementUnitsEnum().getName());
              //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
              //base on selected measurement type in Measurements Tab.
              entry.setMeasurementEnum(measurementType);
              
              _jointResultsEntries.add(entry);
              break;
            }
          }
        }
        else
        {
//            System.out.println("  component = " + componentName.toString() + " no longer exists");
        }
      }

      List<JointName> jointNames = _inspResultsReader.getJointNames();
//      System.out.println("glb - get " + jointNames.size() + " joint results");
      for (JointName jointName : jointNames)
      {
        if (jointExists(jointName))
        {
//            System.out.println("  joint name = " + jointName.toString());
          JointResults jointResults = _inspResultsReader.getJointResult(jointName);
          if (subtype.getLongName().equals(jointResults.getSubtypeLongName()))
          {
            List<MeasurementResult> measurementResults = jointResults.getAllMeasurementResults();
            for (MeasurementResult measureResult : measurementResults)
            {
              if (measurementType.equals(measureResult.getMeasurementEnum())
                  && sliceType.equals(measureResult.getSliceNameEnum()))
              {
                String boardSerialNumber = _inspResultsReader.getBoardResults().get(jointName.getBoardName()).getSerialNumber();
                JointResultsEntry entry = new JointResultsEntry();
                entry.setRunStartTime(runId);
                entry.setJointResults(jointResults);
                //Jack Hwee - If the boardSerialNumber is empty string, get the userDescription from image set as boardSerialNumber 
                //            so that user can get serial number from image set, not only during production run.
                if (boardSerialNumber.isEmpty())
                  boardSerialNumber = _inspResultsReader.getImageSetUserDescription();
                entry.setBoardSerialNumber(boardSerialNumber);

                // Added by Lee Herng 11 Feb 2014 - To handle all measurement dumping
                entry.setSliceName(sliceType.getName());
                entry.setMeasurementName(measurementType.getName());
                entry.setMeasurementUnit(measureResult.getMeasurementUnitsEnum().getName());
                //Lim, Lay Ngor - Fix bug: Show non-resize & non-enhance image & roi location for short
                //base on selected measurement type in Measurements Tab.
                entry.setMeasurementEnum(measurementType);
                
                _jointResultsEntries.add(entry);
                break;
              }
            }
          }
        }
        else
        {
//            System.out.println("  joint name = " + jointName.toString() + " no longer exists");
        }
      }
    }
    catch (DatastoreException de)
    {
      MessageDialog.showErrorDialog(MainMenuGui.getInstance(), de, StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
    }
    finally
    {
      // Close result file.
      _inspResultsReader.close();
    }
  }

  /**
   * @author Rex Shang
   */
  public Set<MeasurementEnum> getMeasurementTypes(
      String projectName,
      List<Long> inspectionRuns,
      Subtype subtype,
      SliceNameEnum sliceName)
  {
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(subtype != null, "Subtype is null.");
    Assert.expect(sliceName != null, "SliceNameEnum is null.");
//    System.out.println("JointResultsManager.getMeasurementTypes(" + projectName + " " + subtype + " " + sliceName + ")");

    Set<MeasurementEnum> jointMeasurements = new TreeSet<MeasurementEnum>();

    InspectionResultsReader inspResultsReader = new InspectionResultsReader();
    for (Long runId : inspectionRuns)
    {
      try
      {
        // Open the run result file indicated by run start time.
        inspResultsReader.open(projectName, runId);
        Set<MeasurementEnum> measurementSet = inspResultsReader.getMeasurementEnumForSubtypeAndSliceNameEnum(subtype, sliceName);
        jointMeasurements.addAll(measurementSet);
      }
      catch (DatastoreException de)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(), de, StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
      }
      finally
      {
        // Close result file.
        inspResultsReader.close();
      }
    }

    return jointMeasurements;
  }

  /**
   * @author Rex Shang
   */
  public Set<SliceNameEnum> getSliceNames(
      String projectName,
      List<Long> inspectionRuns,
      Subtype subtype)
  {
    Assert.expect(projectName != null, "Project name is null.");
    Assert.expect(subtype != null, "Subtype is null.");
//    System.out.println("JointResultsManager.getSliceNames(" + projectName + " " + subtype + ")");

    Set<SliceNameEnum> jointSliceNames = new TreeSet<SliceNameEnum>();

    InspectionResultsReader inspResultsReader = new InspectionResultsReader();
    for (Long runId : inspectionRuns)
    {
      try
      {
        // Open the run result file indicated by run start time.
        inspResultsReader.open(projectName, runId);
        Set<SliceNameEnum> sliceSet = inspResultsReader.getSlicesForSubtype(subtype);
        jointSliceNames.addAll(sliceSet);
      }
      catch (DatastoreException de)
      {
        MessageDialog.showErrorDialog(MainMenuGui.getInstance(), de, StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"), true);
      }
      finally
      {
        // Close result file.
        inspResultsReader.close();
      }
    }

    return jointSliceNames;
  }

  /**
   * @author George Booth
   * @author Tan Hock Zoon
   */
  private boolean componentExists(ComponentName componentName)
  {
    Assert.expect(componentName != null);

    Panel panel = Project.getCurrentlyLoadedProject().getPanel();

    //check the board if it's exist else return false
    if (panel.hasBoard(componentName.getBoardName()))
    {
      Board board = panel.getBoard(componentName.getBoardName());
      return board.getBoardType().hasComponentType(componentName.getReferenceDesignator());
    }
    else
    {
      return false;
    }
  }

  /**
   * @author George Booth
   * @author Tan Hock Zoon
   */
  private boolean jointExists(JointName jointName)
  {
    Assert.expect(jointName != null);

    Panel panel = Project.getCurrentlyLoadedProject().getPanel();

    //check the board if it's exist else return false
    if (panel.hasBoard(jointName.getBoardName()))
    {
      Board board = panel.getBoard(jointName.getBoardName());
      if (board.getBoardType().hasComponentType(jointName.getReferenceDesignator()))
      {
        ComponentType componentType = board.getBoardType().getComponentType(jointName.getReferenceDesignator());
        return componentType.hasPadType(jointName.getPadName());
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }

  /**
   * @author George Booth
   */
  public void clearComponentResults()
  {
    _completedResults.clear();
    _allComponentResultsMap.clear();
    if (_selectedJointResults != null)  // new code to clear results measurements   
      _selectedJointResults.clear();    
    
  }

  /**
   * @author George Booth
   */
  public boolean hasComponentResults(String boardName, String referenceDesignator)
  {
    Assert.expect(boardName != null);
    Assert.expect(referenceDesignator != null);

    Pair<String, String> key = new Pair<String, String>(boardName, referenceDesignator);
    return _allComponentResultsMap.containsKey(key);
  }

  /**
   * @author George Booth
   */
  public ComponentResults getComponentResults(String boardName, String referenceDesignator)
  {
    Assert.expect(boardName != null);
    Assert.expect(referenceDesignator != null);

    Pair<String, String> key = new Pair<String, String>(boardName, referenceDesignator);
    Assert.expect(_allComponentResultsMap.containsKey(key));

    return _allComponentResultsMap.get(key);
  }

}
