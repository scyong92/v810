package com.axi.v810.gui;

/**
 * This class provides the ability to communicate highlight information to the
 * graphics engine.  Highlighting changes the color of the renderer from it's
 * original, but is NOT a selection
 *
 * @author Andy Mechtenberg
 */
public class HighlightEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static HighlightEventEnum HIGHLIGHT_PAD_INSTANCES = new HighlightEventEnum(++_index);
  public static HighlightEventEnum CLEAR_HIGHLIGHTS = new HighlightEventEnum(++_index);
  public static HighlightEventEnum CENTER_FEATURE = new HighlightEventEnum(++_index);

  /**
   * @author Andy Mechtenberg
   */
  private HighlightEventEnum(int id)
  {
    super(id);
  }
}

