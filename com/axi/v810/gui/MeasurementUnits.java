package com.axi.v810.gui;

import com.axi.util.Assert;
import com.axi.util.MathUtilEnum;
import com.axi.v810.util.StringLocalizer;

/**
 *
 * <p>Title: MeasurementUnits</p>
 * <p>Description: This class provides string/enum mappings for the MathUtilEnum objects.
 * The reason that we do not simply use the strings in the MathUtilEnum object is that
 * they are not localized because they are not in the XRayTest package.  This class also
 * provides information as to how to display the decimals for each measurement unit.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Carli Connally
 * @version 1.0
 */
public class MeasurementUnits
{
  public static final String MEASURE_MILS = StringLocalizer.keyToString("MMGUI_SETUP_MEASURE_MILS_KEY");
  public static final String MEASURE_MILLIMETERS = StringLocalizer.keyToString("MMGUI_SETUP_MEASURE_MILLIMETERS_KEY");
  public static final String MEASURE_INCHES = StringLocalizer.keyToString("MMGUI_SETUP_MEASURE_INCHES_KEY");
  public static final String MEASURE_NANOMETERS = StringLocalizer.keyToString("MMGUI_SETUP_MEASURE_NANOMETERS_KEY");

  /**
   * This method provides a mapping from the string displayed in the UI to the
   * MathUtilEnum object.
   * @param measureUnit The display string for the measurement units.  It must
   * be one of the constants defined in this class.
   * @return The corresponding MathUtilEnum object.
   * @author Carli Connally
   */
  public static MathUtilEnum getMeasurementUnitEnum(String measureUnit)
  {
    Assert.expect(measureUnit != null);

    MathUtilEnum unit = null;

    if(measureUnit.equalsIgnoreCase(MEASURE_INCHES))
    {
      unit = MathUtilEnum.INCHES;
    }
    else if(measureUnit.equalsIgnoreCase(MEASURE_MILLIMETERS))
    {
      unit = MathUtilEnum.MILLIMETERS;
    }
    else if(measureUnit.equalsIgnoreCase(MEASURE_MILS))
    {
      unit = MathUtilEnum.MILS;
    }
    else if(measureUnit.equalsIgnoreCase(MEASURE_NANOMETERS))
    {
      unit = MathUtilEnum.NANOMETERS;
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }

    Assert.expect(unit != null);
    return unit;
  }

  /**
   * @param measureUnit MathUtilEnum object for desired measurement units
   * @return The corresponding string to be displayed in the UI.  This string is localized.
   * @author Carli Connally
   */
  public static String getMeasurementUnitString(MathUtilEnum measureUnit)
  {
    Assert.expect(measureUnit != null);

    String unitName = null;

    if(measureUnit.equals(MathUtilEnum.INCHES))
    {
      unitName = MEASURE_INCHES;
    }
    else if(measureUnit.equals(MathUtilEnum.MILLIMETERS))
    {
      unitName = MEASURE_MILLIMETERS;
    }
    else if(measureUnit.equals(MathUtilEnum.MILS))
    {
      unitName = MEASURE_MILS;
    }
    else if(measureUnit.equals(MathUtilEnum.NANOMETERS))
    {
      unitName = MEASURE_NANOMETERS;
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }

    Assert.expect(unitName != null);
    return unitName;
  }

  /**
   * Each measurement unit has a recommended number of decimal places to display.  This method
   * returns the appropriate number of decimal places to be displayed for the units specified.
   * @return Number of decimal places to be displayed for the currently selected measurement units.
   * @author Carli Connally
   */
  public static int getMeasurementUnitDecimalPlaces(MathUtilEnum measurementUnit)
  {
    Assert.expect(measurementUnit != null);

    int decimalPlaces = -1;

    if(measurementUnit.equals(MathUtilEnum.INCHES))
    {
      decimalPlaces = 4;
    }
    else if(measurementUnit.equals(MathUtilEnum.MILLIMETERS))
    {
      decimalPlaces = 3;
    }
    else if(measurementUnit.equals(MathUtilEnum.MILS))
    {
      decimalPlaces = 1;
    }
    else if(measurementUnit.equals(MathUtilEnum.NANOMETERS))
    {
      decimalPlaces = 0;
    }
    else
    {
      // this should not happen
      Assert.expect(false);
    }

    Assert.expect(decimalPlaces >= 0);
    return decimalPlaces;
  }
}
