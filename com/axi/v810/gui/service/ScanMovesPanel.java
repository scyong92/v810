package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import com.axi.util.*;


/**
 * This panel allows the user to define and run their scan paths.
 *
 * @author Erica Wheatcroft
 */
class ScanMovesPanel extends JPanel implements TaskPanelInt, Observer
{
  private static final ImageIcon _stopIcon = Image5DX.getImageIcon(Image5DX.AT_STOP);
  private static final ImageIcon _runAllIcon = Image5DX.getImageIcon(Image5DX.AT_START);
  private static final ImageIcon _runOneIcon = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_JOINT);

  private JButton _abortButton = new JButton();
  private JButton _runOneScanButton = new JButton();
  private JButton _runAllButton = new JButton();
  private JButton _removeAllButton = new JButton();
  private JButton _removeLastEntryButton = new JButton();
  private JButton _addScanStepDefinitionButton = new JButton();
  private JScrollPane _scanTableScrollPane = new JScrollPane();
  private JTable _scanDefinitionTable = new JTable();
  private ScanPathDefinitionTableModel _tableModel = null;
  private JLabel _numberOfCyclesLabel = new JLabel();
  private JLabel _miliSecondsLabel = new JLabel();
  private JLabel _scanPassDelayLabel = new JLabel();
  
  //Swee Yee Wong - add total time elapsed and time elapsed for each scan pass
  private JLabel _totalTimeElapsedLabel = new JLabel();
  private JLabel _timeElapsedForEachScanPassLabel = new JLabel();
  private TimerUtil _scanPathTimer;
  
  private NumericTextField _numberOfCyclesTextField = new NumericTextField();
  private NumericRangePlainDocument _cylcesNumericRangePlainDocument = new NumericRangePlainDocument();
  private NumericTextField _scanPassDelayTextField = new NumericTextField();
  private NumericRangePlainDocument _scanPassNumericRangePlainDocument = new NumericRangePlainDocument();
  private JCheckBox _loopContinuoslyCheckBox = new JCheckBox();
  private ListSelectionModel _listSelectionModel = _scanDefinitionTable.getSelectionModel();

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _lengthNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);

  private PanelPositioner _panelPositioner = null;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private boolean _loopContinuosly = false;
  private int _numberOfLoops = 1;
  private int _delayBetweenScanPass = 0;
  private boolean _runOneScanPassAtATime = false;
  private boolean _runOneScanPassHasStarted = false;
  private boolean _scanCancelled = false;
  private boolean _independentAxisAreInSim = false;

  // flag used while the user is initiating the scan pass run.
  private boolean _scanInProgress = false;
  private int _indexOfRunningScanPass = 0;

  private MathUtilEnum _currentUnits = null;

  private PanelPositionerPanel _parent = null;

  // listeners i create them here so that i can quickly add them and remove them as the panel becomes visible
  public ActionListener _removeAllButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _tableModel.clearAllRows();
      _removeAllButton.setEnabled(false);
      _removeLastEntryButton.setEnabled(false);
      _parent.enableLoadConfigurationButton();
      _parent.enableSaveConfigurationButton(false);
      _runOneScanButton.setEnabled(false);
      _runAllButton.setEnabled(false);
    }
  };

  public ActionListener _removeLastEntryButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _tableModel.removeLastEntry();
      if(_tableModel.getRowCount() == 0)
      {
        _removeAllButton.setEnabled(false);
        _removeLastEntryButton.setEnabled(false);
        _parent.enableLoadConfigurationButton();
        _parent.enableSaveConfigurationButton(false);
        _runOneScanButton.setEnabled(false);
        _runAllButton.setEnabled(false);
      }
    }
  };
  public ActionListener _runScanPathActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // first check to make sure the user has not disabled the x or y axis.
      if(_parent.areAnyAxesDisabled() == false)
      {
        _runOneScanPassAtATime = false;
        setMotionProfile();
        runScanPath();
      }
      else
      {
        // they have an axis disabled, notify the user to enable it and return out of the function
        String message = "Please enable all axes and try again.";
        JOptionPane.showMessageDialog(_parent, StringUtil.format(message, 50), "Cannot Run Scan Path", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
  };
  public ActionListener _addScanPassActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _tableModel.insertRow();
      if (_tableModel.getRowCount() == 1)
      {
        _parent.enableSaveConfigurationButton(true);
        _removeAllButton.setEnabled(true);
        _removeLastEntryButton.setEnabled(true);
        _runOneScanButton.setEnabled(true);
        _runAllButton.setEnabled(true);
      }
    }
  };

  private FocusListener _numberOfCyclesTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleNumberOfLoopsEntry();
    }
  };

  private ActionListener _numberOfCyclesTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleNumberOfLoopsEntry();
    }
  };

  private FocusListener _scanPassDelayTextFielddListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleScanPassDelayEntry();
    }
  };

  private ActionListener _scanPassDelayTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleScanPassDelayEntry();
    }
  };

  private ActionListener _loopContinuoslyCheckBoxActionlistener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
      if (_loopContinuosly)
        _numberOfCyclesTextField.setEnabled(false);
      else
        _numberOfCyclesTextField.setEnabled(true);
    }
  };
  private ActionListener _abortButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // indicate the the run has been cancelled.
      _scanCancelled = true;
      try
      {
         _panelPositioner.abort();
         if (_runOneScanPassAtATime)
         {
           _indexOfRunningScanPass = 0;
           _runOneScanPassHasStarted = false;
         }

         // turn on the controls
         disableControls(false, false);
      }
      catch (XrayTesterException xte)
      {
        _parent.handleXrayTesterException(xte);
      }
    }
  };

  private ListSelectionListener _tableListSelectionListener = new ListSelectionListener()
  {
    public void valueChanged(ListSelectionEvent e)
    {
      ListSelectionModel lsm = (ListSelectionModel)e.getSource();

      if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
      {
        // do nothing
      }
      else
      {
        // now lets set which entry the user is selecting.
        int selectedRowIndex = _scanDefinitionTable.getSelectedRow();
        ScanPassDefinitionTableEntry tableEntry =  _tableModel.getTableEntryAt(selectedRowIndex);
        _tableModel.setSelectedRowEntry(tableEntry);
      }

    }
  };

  private MouseAdapter _scanDefinitionTableMouseListener = new MouseAdapter()
  {
    public void mouseClicked(MouseEvent evt)
    {}

    public void mouseReleased(MouseEvent e)
    {
      int selectedRowFromClick = _scanDefinitionTable.rowAtPoint(e.getPoint());
      if (selectedRowFromClick > -1)
      {
        int selectedRow = _scanDefinitionTable.getSelectedRow();

        if (selectedRow != selectedRowFromClick)
        {
          // first we need to stop the cell editing
          TableCellEditor cellEditor = _scanDefinitionTable.getColumnModel().getColumn(0).getCellEditor();
          if (cellEditor != null)
            cellEditor.cancelCellEditing();
          // clear the selection
          _scanDefinitionTable.clearSelection();
          // then select the row
          _scanDefinitionTable.setRowSelectionInterval(selectedRowFromClick, selectedRowFromClick);
        }
      }
    }
  };

  public ActionListener _runOneScanPassActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _runOneScanPassAtATime = true;
      if(_runOneScanPassHasStarted == false)
      {
        if(_parent.areAnyAxesDisabled() == false)
         {
           setMotionProfile();
         }
         else
         {
           // they have an axis disabled, notify the user to enable it and return out of the function
           String message = "Please enable all axes and try again.";
           JOptionPane.showMessageDialog(_parent, StringUtil.format(message, 50), "Cannot Run Scan Path", JOptionPane.ERROR_MESSAGE);
           return;
         }
      }

      runOneScanPass();
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  ScanMovesPanel(PanelPositionerPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;
    _scanPathTimer = new TimerUtil();
    createPanel();

    // we need to listen to when the user changes display units
    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void restoreLastUIState()
  {
    ServiceGuiPersistence persistance = ServiceGuiPersistence.getInstance();
    _loopContinuoslyCheckBox.setSelected(persistance.loopContinuoslyForScanPassPanel());
    if (_loopContinuoslyCheckBox.isSelected())
      _numberOfCyclesTextField.setEnabled(false);
    _numberOfCyclesTextField.setValue(persistance.getLoopCountForScanPassPanel());
    _scanPassDelayTextField.setValue(persistance.getScanPassDelay());
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleScanPassDelayEntry()
  {
    try
    {
      int intValue = _scanPassDelayTextField.getNumberValue().intValue();
      _delayBetweenScanPass = intValue;
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _scanPassDelayTextField.setValue(_delayBetweenScanPass);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleNumberOfLoopsEntry()
  {
    try
    {
      int intValue = _numberOfCyclesTextField.getNumberValue().intValue();
      _numberOfLoops = intValue;
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _numberOfCyclesTextField.setValue(_numberOfLoops);
    }

    _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    ServiceGuiPersistence persistance = ServiceGuiPersistence.getInstance();
    persistance.setLoopCountForScanPassPanel(_numberOfLoops);
    persistance.setScanPassDelay(_delayBetweenScanPass);
    persistance.loopContinuoslyForScanPassPanel(_loopContinuosly);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListners()
  {
    _addScanStepDefinitionButton.addActionListener(_addScanPassActionListener);
    _loopContinuoslyCheckBox.addActionListener(_loopContinuoslyCheckBoxActionlistener);
    _scanPassDelayTextField.addFocusListener(_scanPassDelayTextFielddListener);
    _scanPassDelayTextField.addActionListener(_scanPassDelayTextFieldActionListener);
    _numberOfCyclesTextField.addFocusListener(_numberOfCyclesTextFieldListener);
    _numberOfCyclesTextField.addActionListener(_numberOfCyclesTextFieldActionListener);
    _runAllButton.addActionListener(_runScanPathActionListener);
    _runOneScanButton.addActionListener(_runOneScanPassActionListener);
    _removeAllButton.addActionListener(_removeAllButtonActionListener);
    _removeLastEntryButton.addActionListener(_removeLastEntryButtonActionListener);
    _scanDefinitionTable.addMouseListener(_scanDefinitionTableMouseListener);
    _abortButton.addActionListener(_abortButtonActionListener);
    _listSelectionModel.addListSelectionListener(_tableListSelectionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _addScanStepDefinitionButton.removeActionListener(_addScanPassActionListener);
    _loopContinuoslyCheckBox.removeActionListener(_loopContinuoslyCheckBoxActionlistener);
    _scanPassDelayTextField.removeFocusListener(_scanPassDelayTextFielddListener);
    _scanPassDelayTextField.removeActionListener(_scanPassDelayTextFieldActionListener);
    _numberOfCyclesTextField.removeFocusListener(_numberOfCyclesTextFieldListener);
    _numberOfCyclesTextField.removeActionListener(_numberOfCyclesTextFieldActionListener);
    _runAllButton.removeActionListener(_runScanPathActionListener);
    _runOneScanButton.removeActionListener(_runOneScanPassActionListener);
    _removeAllButton.removeActionListener(_removeAllButtonActionListener);
    _removeLastEntryButton.removeActionListener(_removeLastEntryButtonActionListener);
    _scanDefinitionTable.removeMouseListener(_scanDefinitionTableMouseListener);
    _abortButton.removeActionListener(_abortButtonActionListener);
    _listSelectionModel.removeListSelectionListener(_tableListSelectionListener);
  }


  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public void start()
  {
    _panelPositioner = PanelPositioner.getInstance();

    java.util.List<MotionControllerIdEnum> ids = MotionControllerIdEnum.getAllEnums();
    try
    {
      // now see if any of the independent axis are in sim mode
      if (MotionControl.getInstance(ids.get(0)).isStartupRequired() == false)
      {
        if(MotionControl.getInstance(ids.get(0)).isSimulationModeOn() == false)
        {
          if (MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS) ||
              MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS))
            _independentAxisAreInSim = true;
          else
            _independentAxisAreInSim = false;
        }
        else
        {
          // since all of motion control is in sim we can run scan paths.
          _independentAxisAreInSim = false;
        }
      }
      else
      {
        // a start up is required so we can assume the independent axis are not in sim mode
        _independentAxisAreInSim = false;
      }
    }
    catch(XrayTesterException xte)
    {
      _independentAxisAreInSim = false;
      _parent.handleXrayTesterException(xte);
    }

    addListners();

    restoreLastUIState();

    setMotionProfile();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setMotionProfile()
  {
    try
    {
      if (_panelPositioner.isStartupRequired() == false)
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            // now lets set up the scan profile. this is important so that the values entered by the user are
            // correctly validated.
            try
            {
              if(MagnificationEnum.getCurrentNorminal().equals(MagnificationEnum.NOMINAL))
              {
                String zAxisMotorPositionForLowMag = Config.getSystemCurrentLowMagnification();
                _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForLowMag)));
              }
              else if (MagnificationEnum.getCurrentNorminal().equals(MagnificationEnum.H_NOMINAL))
              {
                String zAxisMotorPositionForHighMag = Config.getSystemCurrentHighMagnification();
                _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForHighMag)));
              }
            }
            catch (XrayTesterException xte)
            {
              _parent.handleXrayTesterException(xte);
            }
          }
        });
      }
    }
    catch(XrayTesterException xte)
    {
      _parent.handleXrayTesterException(xte);
    }
  }

  /**
   * User has requested a task panel change. Is it OK to leave this task panel?
   * @return true if task panel can be exited
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    removeListeners();

    saveUIState();
  }

  /**
   * author Erica Wheatcroft
   */
  private void createScanPathDefinitionPanel()
  {
    // create the NORTH portion of the center panel - adding and clearing points
    JPanel addAndClearScanPathsInnerButtonPanel = new JPanel(new GridLayout(1, 3, 15, 0));
    _removeAllButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_REMOVE_ALL_BUTTON_KEY"));
    _removeLastEntryButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_REMOVE_LAST_ENTRY_BUTTON_KEY"));
    _addScanStepDefinitionButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_ADD_SCAN_PASS_BUTTON_KEY"));
    addAndClearScanPathsInnerButtonPanel.add(_addScanStepDefinitionButton);
    addAndClearScanPathsInnerButtonPanel.add(_removeAllButton);
    _removeLastEntryButton.setEnabled(false);
    addAndClearScanPathsInnerButtonPanel.add(_removeLastEntryButton);
    JPanel addAndClearScanPathsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    addAndClearScanPathsPanel.add(addAndClearScanPathsInnerButtonPanel);

    // now lets create the second part of the center panel - table
    JPanel centerPanel = new JPanel(new BorderLayout(0, 10));
    _tableModel = new ScanPathDefinitionTableModel();
    _scanDefinitionTable.setModel(_tableModel);
    _scanDefinitionTable.getTableHeader().setReorderingAllowed(false);
    _scanDefinitionTable.setDragEnabled(false);
    _scanDefinitionTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _scanDefinitionTable.setRowSelectionAllowed(true);
    _scanDefinitionTable.setColumnSelectionAllowed(false);
    _scanDefinitionTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    _scanTableScrollPane.getViewport().add(_scanDefinitionTable);
    centerPanel.add(_scanTableScrollPane, BorderLayout.CENTER);
    centerPanel.add(addAndClearScanPathsPanel, BorderLayout.NORTH);

    setUpTable();

    add(centerPanel, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

    // create the main panel
    createScanPathDefinitionPanel();

    // create the control panel
    createControlPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createControlPanel()
  {
    // lets create the button panel
    JPanel controlPanel = new JPanel(new FlowLayout());
    JPanel innerControlPanel = new JPanel(new GridLayout(1, 3));
    JPanel controlButtonPanel = new JPanel(new FlowLayout());
    JPanel innerControlButtonPanel = new JPanel(new GridLayout(1, 1, 25, 0));
    _abortButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _abortButton.setIcon(_stopIcon);
    _abortButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_ABORT_BUTTON_KEY"));
    _abortButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _abortButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _runOneScanButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _runOneScanButton.setIcon(_runOneIcon);
    _runOneScanButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_RUN_ONE_BUTTON_KEY"));
    _runOneScanButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _runOneScanButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _runAllButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _runAllButton.setIcon(_runAllIcon);
    _runAllButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_RUN_ALL_BUTTON_KEY"));
    _runAllButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _runAllButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    innerControlButtonPanel.add(_abortButton);
    innerControlButtonPanel.add(_runAllButton);
    innerControlButtonPanel.add(_runOneScanButton);
    controlButtonPanel.add(innerControlButtonPanel);

    // now lets create the panel that will describe the iteration and delay between points
    JPanel numberOfCyclesPanel = new JPanel(new FlowLayout());
    _numberOfCyclesLabel.setText(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY"));

    _numberOfCyclesTextField.setColumns(5);
    DecimalFormat numberOfCyclesDecimalformat = new DecimalFormat();
    numberOfCyclesDecimalformat.setParseIntegerOnly(true);
    _numberOfCyclesTextField.setDocument(_cylcesNumericRangePlainDocument);
    _numberOfCyclesTextField.setFormat(numberOfCyclesDecimalformat);
    _cylcesNumericRangePlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    _numberOfCyclesTextField.setText(String.valueOf(_numberOfLoops));
    numberOfCyclesPanel.add(_numberOfCyclesLabel);
    numberOfCyclesPanel.add(_numberOfCyclesTextField);
    innerControlPanel.add(numberOfCyclesPanel);

    JPanel loopCheckBoxPanel = new JPanel(new FlowLayout());
    _loopContinuoslyCheckBox.setText(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY"));
    loopCheckBoxPanel.add(_loopContinuoslyCheckBox);
    innerControlPanel.add(loopCheckBoxPanel);

    JPanel delayBetweenScanPathsPanel = new JPanel(new FlowLayout());
    _miliSecondsLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_MILISECONDS_KEY"));
    _scanPassDelayTextField.setColumns(5);
    _scanPassDelayLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_DELAY_KEY"));
    DecimalFormat format = new DecimalFormat();
    format.setParseIntegerOnly(true);
    _scanPassDelayTextField.setDocument(_scanPassNumericRangePlainDocument);
    _scanPassDelayTextField.setFormat(format);
    _scanPassNumericRangePlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    delayBetweenScanPathsPanel.add(_scanPassDelayLabel);
    delayBetweenScanPathsPanel.add(_scanPassDelayTextField);
    delayBetweenScanPathsPanel.add(_miliSecondsLabel);
    innerControlPanel.add(delayBetweenScanPathsPanel);
    
    // total elapsed time
    JPanel totalTimeElapsedPanel = new JPanel(new FlowLayout());

    long initialTimeElapsed = 0;
    int initialNumberOfLoop = 1;
    int initialNumberOfScanPasses = 1;
    updateTimeElapsedInformation(initialTimeElapsed, initialNumberOfLoop, initialNumberOfScanPasses);
    totalTimeElapsedPanel.add(_totalTimeElapsedLabel);
    innerControlPanel.add(totalTimeElapsedPanel);
    
    //elapsed time for each scan pass
    JPanel timeElapsedForEachScanPassPanel = new JPanel(new FlowLayout());

    timeElapsedForEachScanPassPanel.add(_timeElapsedForEachScanPassLabel);

    innerControlPanel.add(timeElapsedForEachScanPassPanel);
    controlPanel.add(innerControlPanel);

    JPanel executeScanMovesDefinitionsPanel = new JPanel(new BorderLayout(0, 20));
    executeScanMovesDefinitionsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    executeScanMovesDefinitionsPanel.add(controlPanel, BorderLayout.NORTH);
    executeScanMovesDefinitionsPanel.add(controlButtonPanel, BorderLayout.CENTER);

    add(executeScanMovesDefinitionsPanel, BorderLayout.SOUTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setUpTable()
  {
    // we want to center all the data in the table
    int numberOfColumns = _scanDefinitionTable.getColumnModel().getColumnCount();
    for(int i = 0; i < numberOfColumns; i++)
    {
      TableColumn col = _scanDefinitionTable.getColumnModel().getColumn(i);
      DefaultTableCellRenderer columnCenterRenderer = new DefaultTableCellRenderer();
      columnCenterRenderer.setHorizontalAlignment( JLabel.CENTER );
      col.setCellRenderer(columnCenterRenderer);
    }

    // bold the table headers
    _scanDefinitionTable.getTableHeader().setFont(FontUtil.getBoldFont(_scanDefinitionTable.getTableHeader().getFont(),Localization.getLocale()));

    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_scanDefinitionTable, true);

    _scanDefinitionTable.getTableHeader().setReorderingAllowed(false);

    NumericRenderer numericRenderer = new NumericRenderer(2, JLabel.CENTER);

    // set up the editors
    TableColumn passDirectionColumn = _scanDefinitionTable.getColumnModel().getColumn(1);
    JComboBox passDirectionComboBox = new JComboBox();
    passDirectionComboBox.addItem(ScanPassDirectionEnum.FORWARD);
    passDirectionComboBox.addItem(ScanPassDirectionEnum.REVERSE);
    passDirectionColumn.setCellEditor(new DefaultCellEditor(passDirectionComboBox));

    TableColumn xStartColumn = _scanDefinitionTable.getColumnModel().getColumn(2);
    xStartColumn.setCellEditor(_xNumericEditor);
    xStartColumn.setCellRenderer(numericRenderer);

    TableColumn yStartColumn = _scanDefinitionTable.getColumnModel().getColumn(3);
    yStartColumn.setCellEditor(_yNumericEditor);
    yStartColumn.setCellRenderer(numericRenderer);

    TableColumn lengthColumn = _scanDefinitionTable.getColumnModel().getColumn(4);
    lengthColumn.setCellEditor(_lengthNumericEditor);
    lengthColumn.setCellRenderer(numericRenderer);

    updateDecimalPlaces();

    TableColumn scanStepDirectionColumn = _scanDefinitionTable.getColumnModel().getColumn(6);
    JComboBox scanStepDirectionComboBox = new JComboBox();
    scanStepDirectionComboBox.addItem(ScanStepDirectionEnum.FORWARD);
    scanStepDirectionComboBox.addItem(ScanStepDirectionEnum.REVERSE);
    scanStepDirectionColumn.setCellEditor(new DefaultCellEditor(scanStepDirectionComboBox));
  }


  /**
   * @author Erica Wheatcroft
   */
  void enablePanel(boolean subsystemInitialized)
  {
    _addScanStepDefinitionButton.setEnabled(subsystemInitialized);
    _removeAllButton.setEnabled(subsystemInitialized);
    _numberOfCyclesTextField.setEnabled(subsystemInitialized);
    _loopContinuoslyCheckBox.setEnabled(subsystemInitialized);
    _scanPassDelayTextField.setEnabled(subsystemInitialized);
    _scanDefinitionTable.setEnabled(subsystemInitialized);

    if (subsystemInitialized == false)
    {
      _abortButton.setEnabled(subsystemInitialized);
      _runAllButton.setEnabled(subsystemInitialized);
      _runOneScanButton.setEnabled(subsystemInitialized);
    }
    else
    {
      _abortButton.setEnabled(false);

      if(_tableModel.getRowCount() == 0)
      {
        _runAllButton.setEnabled(false);
        _runOneScanButton.setEnabled(false);
        _removeAllButton.setEnabled(false);
        _removeLastEntryButton.setEnabled(false);
      }
      else
      {
        _removeAllButton.setEnabled(true);
        _runOneScanButton.setEnabled(true);
        _runAllButton.setEnabled(true);
        _removeLastEntryButton.setEnabled(true);
      }

      if (_loopContinuosly)
        _numberOfCyclesTextField.setEnabled(false);
      else
        _numberOfCyclesTextField.setEnabled(true);

      if(_independentAxisAreInSim)
      {
        // if any of the independent axis are in sim the user can not run a scan path
        _runAllButton.setEnabled(false);
        _runOneScanButton.setEnabled(false);
      }

      restoreLastUIState();

      setMotionProfile();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void disableControls(final boolean testRunning, final boolean userIsControllingScanPass)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // the panel positioner needs to be notified as well
        _parent.disableControls(testRunning);

        _runAllButton.setEnabled(! testRunning);
        _addScanStepDefinitionButton.setEnabled(! testRunning);
        _removeAllButton.setEnabled(! testRunning);
        _removeLastEntryButton.setEnabled(! testRunning);
        _numberOfCyclesTextField.setEnabled(! testRunning);
        _loopContinuoslyCheckBox.setEnabled(! testRunning);
        if(_loopContinuoslyCheckBox.isSelected())
          _numberOfCyclesTextField.setEnabled(false);
        _scanPassDelayTextField.setEnabled(! testRunning);
        _scanDefinitionTable.setEnabled(! testRunning);

        if(testRunning)
        {
          // we need to disable everything and only turn on the appropriate buttons.
          _abortButton.setEnabled(true); // abort is set to true

          if (userIsControllingScanPass)
          {
            // the user is controlling when the next scan pass is starting so this needs to be enabled
            _runOneScanButton.setEnabled(true);
          }
          else
            _runOneScanButton.setEnabled(false);
        }
        else
        {
          _abortButton.setEnabled(false);
          _runOneScanButton.setEnabled(true);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  void commandHardware(boolean firstScanPassRun, java.util.List<ScanPass> scanPath) throws XrayTesterException
  {
    for (ScanPass scanPass : scanPath)
    {
      if (_scanCancelled == false)
      {
        _panelPositioner.runNextScanPass();
      }
      try
      {
        if (firstScanPassRun)
          Thread.sleep(_delayBetweenScanPass * 2);
        else
          Thread.sleep(_delayBetweenScanPass);
      }
      catch (InterruptedException ie)
      {
        Assert.expect(false);
      }
      firstScanPassRun = false;
    }
    boolean scanPathDone = false;
    do
    {
      // NOTE: the actuall call runScanPass() is some what of a blocking call but not 100%. Once the hardware
      // starts moving the call returns. So we want to make sure the hardware is done before we disable
      // the scan path. Yes there is potential that this loop could be infinite if the hardware locks up. But
      // we want to have the panel positioner updated to rebort those problems.
      scanPathDone = _panelPositioner.isScanPathDone();
    }
    while (scanPathDone == false && _scanCancelled == false);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void runScanPath()
  {
    Assert.expect(_panelPositioner != null);

    _scanCancelled = false;

    disableControls(true, false);

    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        // get the list of scan passes from the table model.
        java.util.List<ScanPass> scanPath = _tableModel.getDefinedScanPath();

        try
        {
          // run the scan pass - check to make sure we are controlling when the next scan pass is ran
          Assert.expect(_runOneScanPassAtATime == false);

          // depending on where the the stage is when the scan path is started the delay needs to be doubled
          // on the first pass so that the delay actually occurs. The call to runNextScanPass is asynchronous
          // and returns right away so if we are doing the normal delay and the stage has a fair bit to move the delay
          // will not occur.
          boolean firstScanPassRun = true;
          int numberOfLoop = 0;
          // Time the scan path action.
          _scanPathTimer.reset();
          _scanPathTimer.start();
      
          // if we are looping for a finate amount of cycles....
          if(_loopContinuosly == false && _numberOfLoops > 1)
          {
            for (int loopCount = 0; loopCount < _numberOfLoops; loopCount++)
            {
              if (_scanCancelled)
                break;

              setupHardwareForScanPath(scanPath);
              commandHardware(firstScanPassRun, scanPath);
              numberOfLoop++;
            }
          }
          else if(_loopContinuosly)
          {
            // if we are looping until the user tells us to stop..
            while(_scanCancelled == false)
            {
              setupHardwareForScanPath(scanPath);
              commandHardware(firstScanPassRun, scanPath);
              numberOfLoop++;
            }
          }
          else
          {
            numberOfLoop = 1;
            // just run the scan path once.
            setupHardwareForScanPath(scanPath);
            commandHardware(firstScanPassRun, scanPath);
          }
          
          _scanPathTimer.stop();
          updateTimeElapsedInformation(_scanPathTimer.getElapsedTimeInMillis(), numberOfLoop, scanPath.size());
          
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              if(_scanCancelled == false)
                disableControls(false, false);
            }
          });
        }
        catch (XrayTesterException xte)
        {
          _parent.handleXrayTesterException(xte);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupHardwareForScanPath(java.util.List<ScanPass> scanPath) throws XrayTesterException
  {
    // just encase an abort occured lets clear out an old one if it exists.
    _panelPositioner.disableScanPath();
    
    int distanceInNanoMetersPerPulse = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    //Swee Yee Wong - XCR-3606 Failed to run long scanpass for M23 after run full CDNA
    cameraTrigger.setNumberOfRequiredSampleTriggers();
    int numberOfStartingPulses = cameraTrigger.getNumberOfRequiredSampleTriggers();
    
    // Swee Yee Wong - Include starting pulse information for scan pass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    // validate the scan pass data by enabling the scan path in panel positioner
    _panelPositioner.enableScanPath(scanPath, distanceInNanoMetersPerPulse, numberOfStartingPulses);
    _panelPositioner.setPositionPulse(distanceInNanoMetersPerPulse, numberOfStartingPulses);

    // If we abort right before panelPositioner.enableScanPath(), we need
    // to clear the state.
    if (_scanCancelled)
      _panelPositioner.abort();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void runOneScanPass()
  {
    Assert.expect(_panelPositioner != null);

    _scanCancelled = false;

    disableControls(true, true);

    // lets select the row of the scan pass we are running
    _scanDefinitionTable.setRowSelectionInterval(_indexOfRunningScanPass, _indexOfRunningScanPass);

    if(_scanCancelled != true)
    {
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          // get the list of scan passes from the table model.
          java.util.List<ScanPass> scanPath = _tableModel.getDefinedScanPath();

          try
          {
            if (_scanInProgress == false)
            {
              // the is the first time the user has pressed the run one sacn pass button. so we will need to do all
              // the set up work
              _runOneScanPassHasStarted = true;
              _scanInProgress = true;

              // Disable any previous scan path data before we start
              _panelPositioner.disableScanPath();

              int distanceInNanoMetersPerPulse = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
              CameraTrigger cameraTrigger = CameraTrigger.getInstance();
              int numberOfStartingPulses = cameraTrigger.getNumberOfRequiredSampleTriggers();
              // Swee Yee Wong - Include starting pulse information for scan pass adjustment
              // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
              // validate the scan pass data by enabling the scan path in panel positioner
              _panelPositioner.enableScanPath(scanPath, distanceInNanoMetersPerPulse, numberOfStartingPulses);
              _panelPositioner.setPositionPulse(distanceInNanoMetersPerPulse, numberOfStartingPulses);
            }
            
            // Time the scan pass action.
            _scanPathTimer.reset();
            _scanPathTimer.start();

            // now lets run the scan pass
            _panelPositioner.runNextScanPass();
            
            _scanPathTimer.stop();
            updateTimeElapsedInformation(_scanPathTimer.getElapsedTimeInMillis(), 1, 1);

            if (_indexOfRunningScanPass == scanPath.size() - 1)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  // turn on the controls
                  disableControls(false, false);
                }
              });

              _scanInProgress = false;
              _indexOfRunningScanPass = 0;

              // okay we are running the last scan pass so lets ge ready to do all the clean up work.
              boolean scanPathDone = false;
              do
              {
                // NOTE: the actuall call runScanPass() is some what of a blocking call but not 100%. Once the hardware
                // starts moving the call returns. So we want to make sure the hardware is done before we disable
                // the scan path. Yes there is potential that this loop could be infinite if the hardware locks up. But
                // we want to have the panel positioner updated to rebort those problems.
                scanPathDone = _panelPositioner.isScanPathDone();
              }
              while (scanPathDone == false && _scanCancelled == false);

              // disable the scan pass
              _panelPositioner.disableScanPath();
              _scanInProgress = false;
              _runOneScanPassHasStarted = false;
            }
            else
            {
              // increment our index.
              _indexOfRunningScanPass++;
            }
          }
          catch (XrayTesterException xte)
          {
            _scanInProgress = false;
            _runOneScanPassHasStarted = false;
            _parent.handleXrayTesterException(xte);
          }
        }
      });
    }
  }

  /**
   * Write out a configuration file that will represent the scan path the user has defined in the table
   * @author Erica Wheatcroft
   */
  void saveConfigurationFile(String fileName)
  {
    Assert.expect(fileName != null);

    MathUtilEnum currentUnits = _tableModel.getCurrentUnits();
    // now tell that table to change the units to nanometers since we will write out all values in nanometers
    _tableModel.changeUnits(MathUtilEnum.NANOMETERS);

    try
    {
      // now write out the visible file.
      JTableToTextFileExporter exporter = new JTableToTextFileExporter();
      exporter.writeFile(_scanDefinitionTable, fileName, "\t", true);
      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_PATH_CONFIG_LOCATION_KEY",
                                                                                     new Object[]{fileName}));

      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message,50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);
      _tableModel.changeUnits(currentUnits);
      _tableModel.fireTableDataChanged();
      _parent.enableLoadConfigurationButton();
    }
    catch(FileNotFoundException fnfe)
    {
      _tableModel.changeUnits(currentUnits);
      String message = fnfe.getMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  boolean isTableCurrentlyPopulated()
  {
    if(_scanDefinitionTable != null)
    {
      if (_scanDefinitionTable.getRowCount() >= 1)
        return true;
    }

    return false;
  }

  /**
   * @author Erica WHeatcroft
   */
  public synchronized void update(Observable observable, Object arg)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if (arg instanceof GuiEvent)
      {
        GuiEvent event = (GuiEvent)arg;
        GuiEventEnum eventEnum = event.getGuiEventEnum();
        if (eventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)eventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
          {
            Assert.expect(event.getSource() instanceof MathUtilEnum);
            _currentUnits = (MathUtilEnum)event.getSource();
            updateDecimalPlaces();
          }
        }
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateDecimalPlaces()
  {
    if(_currentUnits != null)
    {
      int numberOfDecimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_currentUnits);
      _xNumericEditor.setDecimalPlaces(numberOfDecimalPlaces);
      _yNumericEditor.setDecimalPlaces(numberOfDecimalPlaces);
      _lengthNumericEditor.setDecimalPlaces(numberOfDecimalPlaces);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void loadConfigurationFile(String fileName)
  {
    Assert.expect(fileName != null);

    // clear the table model
    _tableModel.clearAllRows();

    // read the file and populated the table
    ScanPathDefinitionTableReader reader = new ScanPathDefinitionTableReader();
    try
    {
      reader.read(_scanDefinitionTable, fileName);

      _tableModel.fireTableDataChanged();

      // // notify the user load was successfull
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_PATH_CONFIG_FILE_LOADED_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message,50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

      // now enable the save config button
      _parent.enableSaveConfigurationButton(true);

    }
    catch(FileCorruptDatastoreException fcde)
    {
      // the file was corrupt so give the user an error message and then clear out the table model.
      _tableModel.clearAllRows();
      _tableModel.fireTableDataChanged();
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_PATH_CONFIG_FILE_CORRUPT_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }
    catch(DatastoreException de)
    {
      _tableModel.clearAllRows();
      _tableModel.fireTableDataChanged();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(de.getMessage(), 50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void updateTimeElapsedInformation(long timeElapsedInMilliseconds, int numberOfLoops, int numberOfScanPasses)
  {
    if(numberOfLoops < 1)
      numberOfLoops = 1;
    LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_POSITIONER_TOTAL_TIME_ELAPSED_KEY",
                    new Object[]
                    {
                      timeElapsedInMilliseconds
                    });
    _totalTimeElapsedLabel.setText(StringLocalizer.keyToString(localizedMessage));
    
    localizedMessage = new LocalizedString("SERVICE_UI_PANEL_POSITIONER_TIME_ELAPSED_FOR_EACH_SCANPASS_KEY",
                    new Object[]
                    {
                      timeElapsedInMilliseconds/(numberOfLoops * numberOfScanPasses)
                    });
    _timeElapsedForEachScanPassLabel.setText(StringLocalizer.keyToString(localizedMessage));
  }
}
