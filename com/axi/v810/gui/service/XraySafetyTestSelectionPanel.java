package com.axi.v810.gui.service;

import java.awt.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class supports the X-ray Safety Test test selection panel. The panel displays the available tests,
 * displays a brief description of the test when it is selected and creates a list of process steps needed
 * for that test.  The X-ray Safety Test task panel is informed of the test selection and required steps
 * so they may be sequenced for the user.
 *
 * @author George Booth
 */
public class XraySafetyTestSelectionPanel extends XraySafetyTestAbstractPanel
{
  private JEditorPane _headingPane;
  private JPanel _centerPanel;
  private JList _testList;
  private ListSelectionListener _testListSelectionListener;
  private JPanel _southPanel;
  private JPanel _testDescriptionPanel;
  private JEditorPane _testDescriptionPane;
  
  // XCR-3801 System crash if select run high Mag High power test
  private String[] _testNames;

  private int _STAGE_X_POSITION_NANOMETERS = 0;
  private int _STAGE_Y_POSITION_NANOMETERS = 0;
  private int _RAIL_WIDTH_NANOMETERS = 0;

  private final int _MINIMUM_WIDTH = 100;
  private final int _MINIMUM_HEIGHT = 50;
  private final int _PREFERRED_WIDTH = 820;
  // the followimg heights should add up to about 500 for a default 1024x768 screen size
  // (takes into account the three BoarderLayout vertical gaps)
  private final int _PREFERRED_HEADER_HEIGHT = 40;
  private final int _PREFERRED_TEST_LIST_HEIGHT = 230;
  private final int _PREFERRED_TEST_DESCRIPTION_HEIGHT = 170;

  private GuiObservable _guiObservable = GuiObservable.getInstance();

  private int _selectedIndex = -1;

  /**
   * @author George Booth
   */
  public XraySafetyTestSelectionPanel(ServiceEnvironmentPanel parent)
  {
    super(parent);
    Assert.expect(parent != null);

    this.setLayout(new BorderLayout(0, 10));

    // set up heading
    _headingPane = new JEditorPane();
    _headingPane.setEditable(false);
    _headingPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_HEADER_HEIGHT));
    _headingPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _headingPane.setBackground(this.getBackground());
    add(_headingPane, BorderLayout.NORTH);

    URL heading = convertKeyToURL("SERVICE_XST_TEST_SELECTION_HEADING_KEY");
    displayURL(_headingPane, heading);

    // create list of tests
    _centerPanel = new JPanel();
    _centerPanel.setBorder(BorderFactory.createEtchedBorder(Color.white,new Color(165, 163, 151)));
    add(_centerPanel, BorderLayout.CENTER);

    _testNames = getTestNames();
    _testList = new JList(_testNames);
    _testList.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_TEST_LIST_HEIGHT));
    _testList.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _testList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    _centerPanel.add(_testList);

    _testListSelectionListener = new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        testListValueChanged(e);
      }
    };

    // create test description and prompt area
    _southPanel = new JPanel();
    _southPanel.setLayout(new BorderLayout(0, 10));
    add(_southPanel, BorderLayout.SOUTH);

    // create test description area
    _testDescriptionPanel = new JPanel();
    _testDescriptionPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(
      Color.white,new Color(165, 163, 151)),
        "Test Description"));
    _southPanel.add(_testDescriptionPanel, BorderLayout.CENTER);

    _testDescriptionPane = new JEditorPane();
    _testDescriptionPane.setEditable(false);
    _testDescriptionPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_TEST_DESCRIPTION_HEIGHT));
    _testDescriptionPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _testDescriptionPane.setBackground(this.getBackground());
    _testDescriptionPanel.add(_testDescriptionPane);
  }

  protected void selectTest(String testName)
  {
    Assert.expect(testName != null);
    if (testName.equals(""))
    {
      // make sure a change is seen
//      _testList.setSelectedIndex(0);
      _testList.clearSelection();
    }
    else
    {
      for (int i = 0; i < _testNames.length; i++)
      {
        if (_testNames[i].equals(testName))
        {
          // make sure a change is seen
//          _testList.clearSelection();
          _testList.setSelectedIndex(i);
          return;
        }
      }
      Assert.expect(false, "unknown test name = \"" + testName + "\"");
    }
  }

  protected String getSelectedTest()
  {
    if (_selectedIndex < 0)
    {
      return "";
    }
    else
    {
      return _testNames[_selectedIndex];
    }
  }

  /**
   * @author George Booth
   */
  void testListValueChanged(ListSelectionEvent e)
  {
//    System.out.println("\ntestListValueChanged()");
    if (e.getValueIsAdjusting())
    {
//      System.out.println("  selection is adjusting");
      // Do nothing.
    }
    else
    {
      ListSelectionModel selectionModel = _testList.getSelectionModel();

      if (selectionModel.isSelectionEmpty())
      {
//        System.out.println("  selection is empty");
        // inform listeners no tests were selected
        _selectedIndex = -1;
        URL description = convertKeyToURL("SERVICE_XST_EMPTY_TEST_DESCRIPTION_KEY");
        displayURL(_testDescriptionPane, description);
        ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
        _guiObservable.stateChanged(newSteps, XraySafetyTestEventEnum.TEST_SELECTED);
      }
      else
      {
        _selectedIndex = selectionModel.getAnchorSelectionIndex();
//       System.out.println("  selected index = " + _selectedIndex);

        URL description = null;
        ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
        switch (_selectedIndex)
        {
          case 0:
            description = convertKeyToURL("SERVICE_XST_COMPREHENSIVE_TEST_DESCRIPTION_KEY");
            newSteps = generateComprehensiveTestSteps();
            break;
          case 1:
            description = convertKeyToURL("SERVICE_XST_SEMI_ANNUAL_TEST_DESCRIPTION_KEY");
            newSteps = generateSemiAnnualTestSteps();
            break;
          case 2:
            description = convertKeyToURL("SERVICE_XST_REPLACED_TUBE_HVPS_TEST_DESCRIPTION_KEY");
            newSteps = generateReplacedTubeHVPSTestSteps();
            break;
          case 3:
            description = convertKeyToURL("SERVICE_XST_REPLACED_LEAD_SHIELD_TEST_DESCRIPTION_KEY");
            newSteps = generateReplacedLeadShieldTestSteps();
            break;
          case 4:
            description = convertKeyToURL("SERVICE_XST_REPLACED_INNER_BARRIER_TEST_DESCRIPTION_KEY");
            newSteps = generateReplacedInnerBarrierTestSteps();
            break;
          case 5:
            description = convertKeyToURL("SERVICE_XST_LOW_POWER_TEST_DESCRIPTION_KEY");
            newSteps = generateLowPowerTestSteps();
            break;
//          case 6:
//            description = convertKeyToURL("SERVICE_XST_NORMAL_POWER_TEST_DESCRIPTION_KEY");
//            newSteps = generateNormalPowerTestSteps();
//            break;
          case 6:
            description = convertKeyToURL("SERVICE_XST_HIGH_POWER_TEST_DESCRIPTION_KEY");
            newSteps = generateHighPowerTestSteps();
            break;
          case 7:
            description = convertKeyToURL("SERVICE_XST_HIGH_MAG_HIGH_POWER_TEST_DESCRIPTION_KEY");
            newSteps = generateHighMagHighPowerTestSteps();
            break;
          default:
            Assert.expect(false, "Case for test " + _selectedIndex + " not handled");
            break;
        }
        displayURL(_testDescriptionPane, description);
        Assert.expect(newSteps != null);
        _guiObservable.stateChanged(newSteps, XraySafetyTestEventEnum.TEST_SELECTED);
      }
    }
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateComprehensiveTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(preparationStep());
//    newSteps.add(interlockVerificationStep());
    newSteps.add(interlockVerification1Step());
    newSteps.add(interlockVerification2Step());
    newSteps.add(interlockVerification3Step());
    newSteps.add(lowPowerTest1Step());
    newSteps.add(lowPowerTest2Step());
    newSteps.add(highPowerTest1Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateSemiAnnualTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(preparationStep());
//    newSteps.add(interlockVerificationStep());
    newSteps.add(interlockVerification1Step());
    newSteps.add(interlockVerification2Step());
    newSteps.add(interlockVerification3Step());
    newSteps.add(highPowerTest1Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateReplacedTubeHVPSTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(preparationStep());
    newSteps.add(interlockVerificationOnAffectedAssemblyStep());
    newSteps.add(highPowerTest1Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateReplacedLeadShieldTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(preparationStep());
    newSteps.add(interlockVerificationOnAffectedAssemblyStep());
    newSteps.add(lowPowerTest1Step());
    newSteps.add(lowPowerTest2Step());
    newSteps.add(highPowerTest1Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateReplacedInnerBarrierTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(preparationStep());
//    newSteps.add(interlockVerificationOnAffectedAssemblyStep());
    newSteps.add(interlockVerification2Step());
    newSteps.add(lowPowerTest2Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateLowPowerTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(lowPowerTest1Step());
    newSteps.add(lowPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
//  ArrayList<XraySafetyTestProcessStep> generateNormalPowerTestSteps()
//  {
//    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
//    newSteps.add(normalPowerTest1Step());
//    newSteps.add(normalPowerTest2Step());
//    newSteps.add(processCompleteStep());
//    return newSteps;
//  }

  /**
   * @author George Booth
   */
  ArrayList<XraySafetyTestProcessStep> generateHighPowerTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(highPowerTest1Step());
    newSteps.add(highPowerTest2Step());
    newSteps.add(processCompleteStep());
    return newSteps;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  ArrayList<XraySafetyTestProcessStep> generateHighMagHighPowerTestSteps()
  {
    ArrayList<XraySafetyTestProcessStep> newSteps = new ArrayList<XraySafetyTestProcessStep>();
    newSteps.add(highMagHighPowerTestStep());
    newSteps.add(processCompleteStep());
    return newSteps;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep preparationStep()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_PREPARATION_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep interlockVerificationStep()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_INTERLOCK_VERIFICATION_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep interlockVerification1Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_INTERLOCK_VERIFICATION_STEP_1_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep interlockVerification2Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_INTERLOCK_VERIFICATION_STEP_2_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.closeInnerBarrier();
    step.openOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep interlockVerification3Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_INTERLOCK_VERIFICATION_STEP_3_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.openInnerBarrier();
    step.closeOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep interlockVerificationOnAffectedAssemblyStep()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_INTERLOCK_VERIFICATION_ON_AFFECTED_ASSEMBLY_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep lowPowerTest1Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_LOW_POWER_TEST_1_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_LOW_POWER_TEST_1_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.LOW_POWER);
    step.openInnerBarrier();
    step.closeOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep lowPowerTest2Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_LOW_POWER_TEST_2_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_LOW_POWER_TEST_2_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.LOW_POWER);
    step.closeInnerBarrier();
    step.openOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep normalPowerTest1Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_NORMAL_POWER_TEST_1_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_NORMAL_POWER_TEST_1_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.NORMAL_POWER);
    step.openInnerBarrier();
    step.closeOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep normalPowerTest2Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_NORMAL_POWER_TEST_2_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_NORMAL_POWER_TEST_2_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.NORMAL_POWER);
    step.closeInnerBarrier();
    step.openOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep highPowerTest1Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_HIGH_POWER_TEST_1_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_HIGH_POWER_TEST_1_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.HIGH_POWER);
    step.openInnerBarrier();
    step.closeOuterBarriers();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep highPowerTest2Step()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_HIGH_POWER_TEST_2_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_HIGH_POWER_TEST_2_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.HIGH_POWER);
    step.closeInnerBarrier();
    step.openOuterBarriers();
    return step;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  XraySafetyTestProcessStep highMagHighPowerTestStep()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_HIGH_MAG_HIGH_POWER_TEST_PART_1_STEP_KEY");
    step.setDocument2Key("SERVICE_XST_HIGH_MAG_HIGH_POWER_TEST_PART_2_STEP_KEY");
    step.setPromptKey("SERVICE_XST_CONTINUE_TEST_PROMPT_KEY");
    step.setXrayPower(XraySafetyTestPowerEnum.HIGH_POWER);
    step.openInnerBarrier();
    step.closeOuterBarriers();
    step.downXrayActuator();
    return step;
  }

  /**
   * @author George Booth
   */
  XraySafetyTestProcessStep processCompleteStep()
  {
    XraySafetyTestProcessStep step = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step.setDocument1Key("SERVICE_XST_PROCESS_COMPLETE_STEP_KEY");
    step.setPromptKey("SERVICE_XST_PROCESS_COMPLETE_PROMPT_KEY");
    return step;
  }

  /**
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("XraySafetyTestSelectionPanel.start()");

    _testList.addListSelectionListener(_testListSelectionListener);
  }

  /**
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("XraySafetyTestSelectionPanel.finish()");

    _testList.removeListSelectionListener(_testListSelectionListener);
  }

  /**
   * XCR-3801 System crash if select run high Mag High power test
   * @author Cheah Lee Herng
   */
  private String[] getTestNames()
  {
    java.util.LinkedList<String> testList = new LinkedList<String>();
    if (XrayActuator.isInstalled())
    {
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_COMPREHENSIVE_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_SEMI_ANNUAL_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_XRAY_TUBE_HVPS_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_LEAD_SHIELD_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_INNER_BARRIER_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_LOW_POWER_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_HIGH_POWER_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_HIGH_MAG_HIGH_POWER_TEST_NAME_KEY"));
    }
    else
    {
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_COMPREHENSIVE_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_SEMI_ANNUAL_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_XRAY_TUBE_HVPS_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_LEAD_SHIELD_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_REPLACED_INNER_BARRIER_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_LOW_POWER_TEST_NAME_KEY"));
      testList.add(StringLocalizer.keyToString("SERVICE_UI_XST_HIGH_POWER_TEST_NAME_KEY"));      
    }
    return testList.toArray(new String[testList.size()]);
  }
}
