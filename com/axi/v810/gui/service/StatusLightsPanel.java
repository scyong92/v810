package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Andy Mechtenberg
 */
class StatusLightsPanel extends JPanel
{
  private final Color OFF_RED = new Color(160,0,0);
  private final Color OFF_YELLOW = new Color(160,160,0);
  private final Color OFF_GREEN = new Color(0,160,0);

  // Variables that control the 5DX here
  private boolean _redLightOn     = false;
  private boolean _yellowLightOn  = false;
  private boolean _greenLightOn   = false;
  private boolean _buzzerLightOn  = false;
  private boolean _errorLightOn   = false;

  // all components go here
  private JPanel _redLightInfoPanel = new JPanel();
  private JPanel _redLightPanel = new JPanel();
  private JPanel _redLightButtonPanel = new JPanel();
  private JPanel _redLightStatePanel = new JPanel();
  private JPanel _buzzerInfoPanel = new JPanel();
  private JPanel _buzzerPanel = new JPanel();
  private JPanel _buzzerButtonPanel = new JPanel();
  private JPanel _buzzerStatePanel = new JPanel();
  private JPanel _yellowLightInfoPanel = new JPanel();
  private JPanel _yellowLightPanel = new JPanel();
  private JPanel _yellowLightButtonPanel = new JPanel();
  private JPanel _yellowLightStatePanel = new JPanel();
  private JPanel _greenLightInfoPanel = new JPanel();
  private JPanel _greenLightPanel = new JPanel();
  private JPanel _greenLightButtonPanel = new JPanel();
  private JPanel _greenLightStatePanel = new JPanel();
  private JPanel _lightStackPanel = new JPanel();

  private BorderLayout _redLightBorderLayout = new BorderLayout();
  private BorderLayout _yellowLightBorderLayout = new BorderLayout();
  private BorderLayout _greenLightBorderLayout = new BorderLayout();
  private BorderLayout _buzzerBorderLayout = new BorderLayout();
  private FlowLayout _greenLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _greenLightStateFlowLayout = new FlowLayout();
  private FlowLayout _buzzerButtonFlowLayout = new FlowLayout();
  private FlowLayout _buzzerStateFlowLayout = new FlowLayout();
  private FlowLayout _redLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _redLightStateFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightStateFlowLayout = new FlowLayout();
  private FlowLayout _redLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _greenLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _buzzerInfoFlowLayout = new FlowLayout();

  private JLabel _buzzerLabel = new JLabel();
  private JLabel _buzzerStateLabel = new JLabel();
  private JLabel _yellowLightLabel = new JLabel();
  private JLabel _yellowLightStateLabel = new JLabel();
  private JLabel _redLightLabel = new JLabel();
  private JLabel _redLightStateLabel = new JLabel();
  private JLabel _greenLightLabel = new JLabel();
  private JLabel _greenLightStateLabel = new JLabel();
  private JButton _redLightButton = new JButton();
  private JButton _buzzerButton = new JButton();
  private JButton _yellowLightButton = new JButton();
  private JButton _greenLightButton = new JButton();

  private ImageIcon _imageIcon = com.axi.v810.images.Image5DX.getImageIcon(com.axi.v810.images.Image5DX.AXI_SPARK);
  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  StatusLightsPanel()
  {
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void jbInit()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    // lets create the red light panel
    _redLightInfoPanel.setLayout(_redLightInfoFlowLayout);
    _redLightLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _redLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _redLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _redLightLabel.setText("Red");
    _redLightStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _redLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _redLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _redLightStateLabel.setText("");
    _redLightButton.setPreferredSize(new Dimension(75, 25));
    _redLightButton.setText("Off");
    _redLightPanel.setLayout(_redLightBorderLayout);
    _redLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _redLightPanel.setPreferredSize(new Dimension(70, 60));
    _redLightButtonFlowLayout.setVgap(9);
    _redLightButtonPanel.setLayout(_redLightButtonFlowLayout);
    _redLightStatePanel.setLayout(_redLightStateFlowLayout);
    _redLightStateFlowLayout.setVgap(15);
    _redLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _redLightInfoFlowLayout.setHgap(15);
    _redLightInfoFlowLayout.setVgap(0);
    _redLightStatePanel.setFont(new java.awt.Font("Dialog", 1, 12));
    _redLightInfoPanel.add(_redLightPanel, null);
    _redLightPanel.add(_redLightLabel, BorderLayout.CENTER);
    _redLightInfoPanel.add(_redLightStatePanel, null);
    _redLightStatePanel.add(_redLightStateLabel, null);
    _redLightInfoPanel.add(_redLightButtonPanel, null);
    _redLightButtonPanel.add(_redLightButton, null);

    // now lets create the yellow light panel
    _yellowLightInfoPanel.setLayout(_yellowLightInfoFlowLayout);
    _yellowLightLabel.setText("Yellow");
    _yellowLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _yellowLightLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _yellowLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _yellowLightStateLabel.setText("");
    _yellowLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _yellowLightStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _yellowLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _yellowLightButtonFlowLayout.setVgap(9);
    _yellowLightStateFlowLayout.setVgap(15);
    _yellowLightButton.setPreferredSize(new Dimension(75, 25));
    _yellowLightButton.setText("Off");
    _yellowLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _yellowLightPanel.setPreferredSize(new Dimension(70, 60));
    _yellowLightPanel.setLayout(_yellowLightBorderLayout);
    _yellowLightButtonPanel.setLayout(_yellowLightButtonFlowLayout);
    _yellowLightStatePanel.setLayout(_yellowLightStateFlowLayout);
    _yellowLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _yellowLightInfoFlowLayout.setHgap(15);
    _yellowLightInfoFlowLayout.setVgap(0);
    _yellowLightInfoPanel.add(_yellowLightPanel, null);
    _yellowLightPanel.add(_yellowLightLabel, BorderLayout.CENTER);
    _yellowLightInfoPanel.add(_yellowLightStatePanel, null);
    _yellowLightStatePanel.add(_yellowLightStateLabel, null);
    _yellowLightInfoPanel.add(_yellowLightButtonPanel, null);
    _yellowLightButtonPanel.add(_yellowLightButton, null);

    // now lets create the green light panel
    _greenLightLabel.setText("Green");
    _greenLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _greenLightLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _greenLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _greenLightStateLabel.setText("");
    _greenLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _greenLightStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _greenLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _greenLightButtonFlowLayout.setVgap(9);
    _greenLightStateFlowLayout.setVgap(15);
    _greenLightButton.setPreferredSize(new Dimension(75, 25));
    _greenLightButton.setText("Off");
    _greenLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _greenLightPanel.setPreferredSize(new Dimension(70, 60));
    _greenLightPanel.setLayout(_greenLightBorderLayout);
    _greenLightButtonPanel.setLayout(_greenLightButtonFlowLayout);
    _greenLightStatePanel.setLayout(_greenLightStateFlowLayout);
    _greenLightInfoPanel.setLayout(_greenLightInfoFlowLayout);
    _greenLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _greenLightInfoFlowLayout.setHgap(15);
    _greenLightInfoFlowLayout.setVgap(0);
    _greenLightInfoPanel.add(_greenLightPanel, null);
    _greenLightPanel.add(_greenLightLabel, BorderLayout.CENTER);
    _greenLightInfoPanel.add(_greenLightStatePanel, null);
    _greenLightStatePanel.add(_greenLightStateLabel, null);
    _greenLightInfoPanel.add(_greenLightButtonPanel, null);
    _greenLightButtonPanel.add(_greenLightButton, null);

    // now lets create the buzzer panel
    _buzzerLabel.setText("Buzzer");
    _buzzerLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _buzzerLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _buzzerLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _buzzerStateLabel.setText("");
    _buzzerStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _buzzerStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _buzzerStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _buzzerButtonFlowLayout.setVgap(9);
    _buzzerStateFlowLayout.setVgap(15);
    _buzzerButton.setPreferredSize(new Dimension(75, 25));
    _buzzerButton.setText("Off");
    _buzzerPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _buzzerPanel.setPreferredSize(new Dimension(70, 60));
    _buzzerPanel.setLayout(_buzzerBorderLayout);
    _buzzerButtonPanel.setLayout(_buzzerButtonFlowLayout);
    _buzzerStatePanel.setLayout(_buzzerStateFlowLayout);
    _buzzerInfoPanel.setLayout(_buzzerInfoFlowLayout);
    _buzzerInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _buzzerInfoFlowLayout.setHgap(15);
    _buzzerInfoFlowLayout.setVgap(0);
    _buzzerInfoPanel.add(_buzzerPanel, null);
    _buzzerPanel.add(_buzzerLabel, BorderLayout.CENTER);
    _buzzerInfoPanel.add(_buzzerStatePanel, null);
    _buzzerStatePanel.add(_buzzerStateLabel, null);
    _buzzerInfoPanel.add(_buzzerButtonPanel, null);
    _buzzerButtonPanel.add(_buzzerButton, null);

    // lets set all the panels to opaque = false so that our axi logo will show up
    _lightStackPanel.setOpaque(false);
    _greenLightButtonPanel.setOpaque(false);
    _greenLightStatePanel.setOpaque(false);
    _greenLightPanel.setOpaque(false);
    _greenLightInfoPanel.setOpaque(false);
    _buzzerButtonPanel.setOpaque(false);
    _buzzerStatePanel.setOpaque(false);
    _buzzerPanel.setOpaque(false);
    _buzzerInfoPanel.setOpaque(false);
    _yellowLightInfoPanel.setOpaque(false);
    _yellowLightButtonPanel.setOpaque(false);
    _yellowLightStatePanel.setOpaque(false);
    _yellowLightPanel.setOpaque(false);
    _redLightInfoPanel.setOpaque(false);
    _redLightButtonPanel.setOpaque(false);
    _redLightStatePanel.setOpaque(false);
    _redLightPanel.setOpaque(false);

    Box box = Box.createVerticalBox();
    box.add(_redLightInfoPanel);
    box.add(Box.createVerticalStrut(50));
    box.add(_yellowLightInfoPanel);
    box.add(Box.createVerticalStrut(50));
    box.add(_greenLightInfoPanel);
    box.add(Box.createVerticalStrut(50));
    box.add(_buzzerInfoPanel);

    add(box, BorderLayout.WEST);

    _redLightButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        redLightButton_actionPerformed(e);
      }
    });
    _yellowLightButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        yellowLightButton_actionPerformed(e);
      }
    });
    _greenLightButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        greenLightButton_actionPerformed(e);
      }
    });
    _buzzerButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {

      }
    });

  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _imageIcon.getIconWidth();
    int iconHeight = _imageIcon.getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _imageIcon.paintIcon(this, graphics, xCoordinate, yCoordinate);
  }


  /**
   * @author Andy Mechtenberg
   */
  private void updateStateLabels()
  {
    if (_redLightOn)
    {
      _redLightPanel.setBackground(Color.red);
      _redLightStateLabel.setText("is On");
    }
    else
    {
      _redLightPanel.setBackground(OFF_RED);
      _redLightStateLabel.setText("is Off");
    }

    if (_yellowLightOn)
    {
      _yellowLightPanel.setBackground(Color.yellow);
      _yellowLightStateLabel.setText("is On");
    }
    else
    {
      _yellowLightPanel.setBackground(OFF_YELLOW);
      _yellowLightStateLabel.setText("is Off");
    }

    if (_greenLightOn)
    {
      _greenLightPanel.setBackground(Color.green);
      _greenLightStateLabel.setText("is On");
    }
    else
    {
      _greenLightPanel.setBackground(OFF_GREEN);
      _greenLightStateLabel.setText("is Off");
    }

    if (_buzzerLightOn)
    {
    }
    else
    {
    }

    if (_errorLightOn)
    {
    }
    else
    {
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void updateButtonLabels()
  {
    if (_redLightOn)
      _redLightButton.setText("Off");
    else
      _redLightButton.setText("On");

    if (_yellowLightOn)
      _yellowLightButton.setText("Off");
    else
      _yellowLightButton.setText("On");

    if (_greenLightOn)
      _greenLightButton.setText("Off");
    else
      _greenLightButton.setText("On");

    if (_buzzerLightOn)
    {}
    else
    {}

    if (_errorLightOn)
    {}
    else
    {}

  }

  /**
   * @author Andy Mechtenberg
   */
  private void redLightButton_actionPerformed(ActionEvent e)
  {
  }

  /**
   * @author Andy Mechtenberg
   */
  private void yellowLightButton_actionPerformed(ActionEvent e)
  {

  }

  /**
   * @author Andy Mechtenberg
   */
  private void greenLightButton_actionPerformed(ActionEvent e)
  {

  }
  /**
   * @author Andy Mechtenberg
   */
  private void panelLightsButton_actionPerformed(ActionEvent e)
  {

  }
}
