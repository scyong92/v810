package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.testExec.*;

/**
 * @author Erica Wheatcroft
 */
public class PanelHandlingPanel extends JPanel implements TaskPanelInt, Observer
{

  private JButton _loopControlButton = new JButton();
  private JButton _initializeButton = new JButton();
  private JButton _unloadPanelButton = new JButton();
  private JButton _loadPanelButton = new JButton();
  private JButton _setRailWidthButton = new JButton();
  private JButton _homeRailsButton = new JButton();
  private JButton _innerBarrierCommandButton = new JButton();
  private JButton _clampsCommandButton = new JButton();
  private JButton _leftOuterBarrierCommandButton = new JButton();
  private JButton _rightOuterBarrierCommandButton = new JButton();
  private JButton _changeLeftPipStateButton = new JButton();
  private JButton _changeMotionStateButton = new JButton();
  private JButton _changeRightPipStateButton = new JButton();
  private JButton _changeDirectionButton = new JButton();
  private JLabel _railWidthUnitsLabel = new JLabel();
  private JLabel _currentRailWidthMeasurementLabel = new JLabel();
  private JLabel _currentStageRailHomeSensorStateLabel = new JLabel();
  private JLabel _innerBarrierStateLabel = new JLabel();
  private JLabel _clampsStateLabel = new JLabel();
  private JLabel _leftOuterBarrierStateLabel = new JLabel();
  private JLabel _leftOuterBarrierLabel = new JLabel();
  private JLabel _rightOuterBarrierStateLabel = new JLabel();
  private JLabel _rightOuterBarrierLabel = new JLabel();
  private JLabel _leftPIPStateLabel = new JLabel();
  private JLabel _leftPipPositionLabel = new JLabel();
  private JLabel _leftPanelInPlaceLabel = new JLabel();
  private JLabel _rightPIPPositionStateLabel = new JLabel();
  private JLabel _rightPIPSensorStateLabel = new JLabel();
  private JLabel _directionStateLabel = new JLabel();
  private JLabel _motionStateLabel = new JLabel();
  private JLabel _leftPanelClearSensorStateLabel = new JLabel();
  private JLabel _rightPanelClearSensorStateLabel = new JLabel();
  private JLabel _panelHandlerStateLabel = new JLabel();
  private JLabel _innerBarrierTimeLabel = new JLabel();
  private JLabel _outerLeftBarrierTimeLabel = new JLabel();
  private JLabel _outerRightBarrierTimeLabel = new JLabel();
  private JLabel _leftSafetyLevelSensorStateLabel = new JLabel();
  private JLabel _rightSafetyLevelSensorStateLabel = new JLabel();
  private JTextField _railWidthTextField = new JTextField();
  
  private JButton _xrayZAxisHomeCommandButton = new JButton();
  private JButton _xrayZAxisUpToPosition1CommandButton = new JButton();
  private JButton _xrayZAxisUpToPosition2CommandButton = new JButton();
  private JButton _xrayZAxisDownCommandButton = new JButton();
  private JLabel _xrayZAxisSensorStateLabel = new JLabel();
  private JLabel _xrayZAxisTimeLabel = new JLabel();
  private JLabel _xrayZAxisStateLabel = new JLabel();
  
  //Swee Yee Wong - add a panel with motorized height level sensor
  private JButton _motorizedHeightLevelSensorUpDownCommandButton = new JButton();
  private JLabel _motorizedHeightLevelSensorStateLabel = new JLabel();
  private JLabel _motorizedHeightLevelSensorTimeLabel = new JLabel();
  
  private XrayTesterImagePanel _innerMachineImagePanel = new XrayTesterImagePanel();
  private ProgressDialog _progressDialog = null;
  private SubsystemStatusAndControlTaskPanel _parent = null;
  private boolean _subsystemInitialized = false;
  private XrayTester _xrayTester = null;
  private PanelHandler _panelHandler = null;
  private StageBelts _belts = null;
  private RightOuterBarrier _rightOuterBarrier = null;
  private LeftOuterBarrier _leftOuterBarrier = null;
  private InnerBarrier _innerBarrier = null;
  private PanelClamps _panelClamps = null;
  private RightPanelClearSensor _rightPanelClearSensor = null;
  private LeftPanelClearSensor _leftPanelClearSensor = null;
  private LeftPanelInPlaceSensor _leftPanelInPlaceSensor = null;
  private RightPanelInPlaceSensor _rightPanelInPlaceSensor = null;
  private LeftSafetyLevelSensor _leftSafetyLevelSensor = null;
  private RightSafetyLevelSensor _rightSafetyLevelSensor = null;
  private XrayMotorizedHeightLevelSensor _xrayMotorizedHeightLevelSensor = null;
  private XrayActuatorInt _xrayActuator = null;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private MathUtilEnum _units = null;
  private boolean _monitorEnabled = false;
  // no need to localize just thread names.
  private WorkerThread _monitorRightPanelClearSensorStateThread = new WorkerThread("Right Panel Clear Monitor");
  private WorkerThread _monitorLeftPanelClearSensorStateThread = new WorkerThread("Left Panel Clear Monitor");
  private WorkerThread _monitorLeftPanelInPlaceStateThread = new WorkerThread("Left Panel In Place Monitor");
  private WorkerThread _monitorRightPanelPanelInPlaceStateThread = new WorkerThread("Right Panel In Place Monitor");
  private WorkerThread _monitorStageRailHomeSensorStateThread = new WorkerThread("Stage Rail Home Sensor State Monitor");
  private WorkerThread _monitorSafetyLevelSensorStateThread = new WorkerThread("Safety Level Sensor State Monitor");
  private static final Color _RED = new Color(160, 0, 0);
  private static final Color _GREEN = new Color(0, 160, 0);
  private static final Color _YELLOW = new Color(160, 160, 0);
  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");
  private static final String _LEFT_TO_RIGHT = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LEFT_TO_RIGHT_BELT_DIRECTION_KEY");
  private static final String _RIGHT_TO_LEFT = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RIGHT_TO_LEFT_BELT_DIRECTION_KEY");
  private static final String _OPEN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPEN_STATE_KEY");
  private static final String _CLOSED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLOSED_STATE_KEY");
  private static final String _CLOSE = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLOSE_COMMAND_KEY");
  private static final String _EXTENDED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_EXTENDED_STATE_KEY");
  private static final String _RETRACT = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RETRACT_COMMAND_KEY");
  private static final String _EXTEND = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_EXTEND_COMMAND_KEY");
  private static final String _RETRACTED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RECTRACTED_STATE_KEY");
  private static final String _BLOCKED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_BLOCKED_STATE_KEY");
  private static final String _CLEAR = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLEAR_STATE_KEY");
  private static final String _YES = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_YES_STATE_KEY");
  private static final String _NO = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NO_STATE_KEY");
  private static final String _ON = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_ON_STATE_KEY");
  private static final String _OFF = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OFF_STATE_KEY");
  private static final String _HIGH = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_HIGH_STATE_KEY");
  private static final String _LOW = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LOW_STATE_KEY");
  private static final String _UP = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_UP_STATE_KEY");
  private static final String _UP_POS_1 = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_UP_TO_POSITION_1_STATE_KEY");
  private static final String _UP_POS_2 = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_UP_TO_POSITION_2_STATE_KEY");
  private static final String _DOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_DOWN_STATE_KEY");
  private static final String _HOME = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_Z_AXIS_HOME_STATE_KEY");
  private boolean _setRailWidthInProgress = false;
  private boolean _panelLoadWasCanceled = false;
  private boolean _panelLoadInProgress = false;
  private boolean _panelUnloadInProgress = false;
  private BusyCancelDialog _busyCancelDialog = null;
  //XXL Optical Stop Sensor - Anthony (May 2013)
  private JLabel _leftLoadingPositionLabel = new JLabel();
  private JLabel _leftLoadingStateLabel = new JLabel();
  private JButton _changeLeftLoadingStateButton = new JButton();
  private JLabel _rightLoadingPositionStateLabel = new JLabel();
  private JButton _changeRightLoadingStateButton = new JButton();
  //Swee Yee Wong
  private DigitalIo _digitalIo = DigitalIo.getInstance();
  
  // listeners create them here so that i can easly add them and remove them when the panel becomes visible and
  // not visible.
  private ActionListener _initializeButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent a)
    {
      Assert.expect(_panelHandler != null);
      createProgressDialog(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZE_COMPLETE_KEY"),
              StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
              false);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {           
            _subsystemInitialized = false;
            _monitorEnabled = false;
            // shutdown first
            _xrayTester.shutdownMotionRelatedHardware();
            // startup
            _xrayTester.startupMotionRelatedHardware();

            _subsystemInitialized = true;
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _progressDialog.setVisible(false);
                _progressDialog = null;
              }
            });
          }
          finally
          {
            populatePanel();
            startMonitoring();
          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;
//      populatePanel();
//      startMonitoring();
    }
  };
  private ActionListener _loopControlButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      // tell panel handler panel to ignore what the hardware is doing.
      stopMonitoring();
      stopObserving();

      LoopControlDialog dlg = new LoopControlDialog();
      dlg.pack();
      SwingUtils.centerOnComponent(dlg, MainMenuGui.getInstance());
      dlg.setVisible(true);
      dlg.dispose();

      // update the panel handler panel to reflect the state the user has left the system in.

      try
      {
        _subsystemInitialized = !_xrayTester.isStartupRequiredForMotionRelatedHardware();
        populatePanel();
        if (_subsystemInitialized)
        {
          startMonitoring();
          startObserving();
        }
      }
      catch (XrayTesterException xte)
      {
        _subsystemInitialized = false;
        handleException(xte);
      }

    }
  };
  private ActionListener _homeRailsButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelHandler != null);

      // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
      try
      {
        if (_panelHandler.isPanelLoaded() == true)
        {
          String message = StringLocalizer.keyToString("SERVICE_PANEL_HANDLER_DISABLE_HOMING_OR_ADJUSTING_RAIL_WIDTH_WITH_PANEL_LOADED_KEY");
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
          return;
        }
      }
      catch (final XrayTesterException xte)
      {
        handleException(xte);
      }

      createProgressDialog(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_HOME_RAILS_COMPLETED_KEY"),
              StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_HOMEING_STAGE_RAILS_KEY"),
              false);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            _panelHandler.homeStageRails();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _progressDialog.setVisible(false);
                _progressDialog = null;
              }
            });

          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;
      populatePanel();

    }
  };
  private ActionListener _setRailWidthButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelHandler != null);
      String railWidth = _railWidthTextField.getText();

      if (railWidth.length() == 0)
      {
        return;
      }

      double parsedRailWidth = Double.parseDouble(railWidth);
      final double convertedRailWidth = MathUtil.convertUnits(parsedRailWidth, _units, MathUtilEnum.NANOMETERS);

      try
      {
        // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
        if (_panelHandler.isPanelLoaded() == true)
        {
          String message = StringLocalizer.keyToString("SERVICE_PANEL_HANDLER_DISABLE_HOMING_OR_ADJUSTING_RAIL_WIDTH_WITH_PANEL_LOADED_KEY");
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
          return;
        }
        double minRailWidthInNanometers = _panelHandler.getMinimumRailWidthInNanoMeters();
        double maxRailWidthInNanometers = _panelHandler.getMaximumRailWidthInNanoMeters();

        _busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_SETTING_RAIL_WIDTH_KEY"),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                true);
        _busyCancelDialog.pack();
        SwingUtils.centerOnComponent(_busyCancelDialog, MainMenuGui.getInstance());
        _setRailWidthInProgress = true;

        if (convertedRailWidth >= minRailWidthInNanometers && convertedRailWidth <= maxRailWidthInNanometers)
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {

            public void run()
            {
              try
              {
                _panelHandler.setRailWidthInNanoMeters((int) convertedRailWidth);
                SwingUtils.invokeLater(new Runnable()
                {

                  public void run()
                  {
                    populateCurrentRailWidth();
                    _railWidthTextField.setText("");
                  }
                });
              }
              catch (final XrayTesterException xte)
              {
                handleException(xte);
                _railWidthTextField.setText("");
              }
            }
          });
          _busyCancelDialog.setVisible(true);
        }
        else
        {
          // the value is not correct. display an error to the user and do not enable the set rail width button
          double minRailWidthConverted = MathUtil.convertUnits(minRailWidthInNanometers, MathUtilEnum.NANOMETERS, _units);
          double maxRailWidthConverted = MathUtil.convertUnits(maxRailWidthInNanometers, MathUtilEnum.NANOMETERS, _units);

          String messageToDisplay = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_HANDLING_RAIL_WIDTH_ENTERED_INVALID_KEY",
                  new Object[]
                  {
                    minRailWidthConverted,
                    _units,
                    maxRailWidthConverted,
                    _units
                  }));

          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringUtil.format(messageToDisplay, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);

          _railWidthTextField.setText("");
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
        _railWidthTextField.setText("");
      }
    }
  };
  private FocusListener _railWidthTextFieldFocusListener = new FocusListener()
  {

    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleManualRailWidthEntry();
    }
  };
  private ActionListener _railWidthTextFieldActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent evt)
    {
      handleManualRailWidthEntry();
    }
  };
  private ActionListener _innerBarrierCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_innerBarrier != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if (_innerBarrier.isOpen())
            {
              _innerBarrier.close();
            }
            else
            {
              _innerBarrier.open();
            }

            long time = _innerBarrier.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _innerBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _innerBarrierStateLabel.setForeground(Color.RED);
                _innerBarrierStateLabel.setText(_UNKNOWN);
                _innerBarrierCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  
  private ActionListener _xrayZAxisHomeCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {

      Assert.expect(_xrayActuator != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
           
            if (XrayActuator.isInstalled() == true)
            {
              _innerBarrier.open();
              _xrayActuator.home(false, false);
            }

            long time = _xrayActuator.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _xrayZAxisTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _xrayZAxisStateLabel.setForeground(Color.RED);
                _xrayZAxisStateLabel.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  
  private ActionListener _xrayZAxisUpToPosition1CommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {

      Assert.expect(_xrayActuator != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
           
            if (XrayActuator.isInstalled() == true)
            {
              _innerBarrier.open();
              _xrayActuator.upToPosition1(false);
            }

            long time = _xrayActuator.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _xrayZAxisTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _xrayZAxisStateLabel.setForeground(Color.RED);
                _xrayZAxisStateLabel.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  
  private ActionListener _xrayZAxisUpToPosition2CommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {

      Assert.expect(_xrayActuator != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
           
            if (XrayActuator.isInstalled() == true)
            {
              _innerBarrier.open();
              _xrayActuator.upToPosition2(false);
            }

            long time = _xrayActuator.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _xrayZAxisTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _xrayZAxisStateLabel.setForeground(Color.RED);
                _xrayZAxisStateLabel.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  
  private ActionListener _xrayZAxisDownCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {

      Assert.expect(_xrayActuator != null);
      try
      {
        if (_panelHandler.isPanelLoaded())
        {
          if (XrayActuator.isInstalled() == true)
          {
            String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_XRAY_CYLINDER_DOWN_WARNING_KEY");

            int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                    message,
                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE);
            if (answer == JOptionPane.NO_OPTION)
            {
              return;
            }
          }
        }
      }
      catch (final XrayTesterException xte)
      {
        handleException(xte);
      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
           
            if (XrayActuator.isInstalled() == true)
            {
              _innerBarrier.open();
              _xrayActuator.down(false);
            }

            long time = _xrayActuator.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _xrayZAxisTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _xrayZAxisStateLabel.setForeground(Color.RED);
                _xrayZAxisStateLabel.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  
   private ActionListener _motorizedHeightLevelSensorUpDownCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_xrayMotorizedHeightLevelSensor != null);
      
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
            {
              if (_xrayMotorizedHeightLevelSensor.isUp())
              {
                _xrayMotorizedHeightLevelSensor.down(false);
              }
              else
              {
                _xrayMotorizedHeightLevelSensor.up(false);
              }
            }

            long time = _xrayMotorizedHeightLevelSensor.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _motorizedHeightLevelSensorTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _motorizedHeightLevelSensorStateLabel.setForeground(Color.RED);
                _motorizedHeightLevelSensorStateLabel.setText(_UNKNOWN);
                _motorizedHeightLevelSensorUpDownCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };

  
  private ActionListener _clampsCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelClamps != null);
      try
      {
        if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsOpened() && (_leftPanelInPlaceSensor.isExtended() || _rightPanelInPlaceSensor.isExtended()))
        {
          String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_CLAMP_WITH_PIP_WARNING_KEY");

          int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                  message,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.YES_NO_OPTION,
                  JOptionPane.WARNING_MESSAGE);
          if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.CLOSED_OPTION)
          {
            return;
          }
        }
        if (_panelClamps.areClampsOpened() && _belts.areStageBeltsOn())
        {
          String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_CLAMP_WITH_BELT_RUNNING_WARNING_KEY");

          int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                  message,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.YES_NO_OPTION,
                  JOptionPane.WARNING_MESSAGE);
          if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.CLOSED_OPTION)
          {
            return;
          }
        }
      }
      catch (final XrayTesterException xte)
      {
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            handleException(xte);
            _clampsStateLabel.setForeground(Color.RED);
            _clampsStateLabel.setText(_UNKNOWN);
            _clampsCommandButton.setText(_UNKNOWN);
          }
        });
      }
      
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsOpened() && _leftPanelInPlaceSensor.isExtended())
            {
              _leftPanelInPlaceSensor.retract();
            }
            if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsOpened() && _rightPanelInPlaceSensor.isExtended())
            {
              _rightPanelInPlaceSensor.retract();
            }
            if (_panelClamps.areClampsOpened() && _belts.areStageBeltsOn())
            {
              _belts.off();
            }
            if (_panelClamps.areClampsClosed())
            {
              _panelClamps.open();
            }
            else
            {
              _panelClamps.close();
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _clampsStateLabel.setForeground(Color.RED);
                _clampsStateLabel.setText(_UNKNOWN);
                _clampsCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _changeLeftPipStateButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_leftPanelInPlaceSensor != null);

      try
      {
        if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsClosed())
        {
          String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_EXTEND_WARNING_KEY");

          int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                  message,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.YES_NO_OPTION,
                  JOptionPane.WARNING_MESSAGE);
          if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.CLOSED_OPTION)
          {
            return;
          }
        }
      }
      catch (final XrayTesterException xte)
      {
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            handleException(xte);
            _leftPIPStateLabel.setForeground(Color.RED);
            _leftPIPStateLabel.setText(_UNKNOWN);
            _changeLeftPipStateButton.setText(_UNKNOWN);
          }
        });
      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsClosed())
            {
              _panelClamps.open();
            }

            if (_leftPanelInPlaceSensor.isExtended())
            {
              _leftPanelInPlaceSensor.retract();
            }
            else
            {
              _leftPanelInPlaceSensor.extend();
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _leftPIPStateLabel.setForeground(Color.RED);
                _leftPIPStateLabel.setText(_UNKNOWN);
                _changeLeftPipStateButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _changeRightPipStateButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_rightPanelInPlaceSensor != null);

      try
      {
        if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsClosed())
        {
          String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_EXTEND_WARNING_KEY");

          int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                  message,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.YES_NO_OPTION,
                  JOptionPane.WARNING_MESSAGE);
          if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.CLOSED_OPTION)
          {
            return;
          }
        }
      }
      catch (final XrayTesterException xte)
      {
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            handleException(xte);
            _rightPIPSensorStateLabel.setForeground(Color.RED);
            _rightPIPSensorStateLabel.setText(_UNKNOWN);
            _changeRightPipStateButton.setText(_UNKNOWN);
          }
        });
      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) && _panelClamps.areClampsClosed())
            {
              _panelClamps.open();
            }

            if (_rightPanelInPlaceSensor.isExtended())
            {
              _rightPanelInPlaceSensor.retract();
            }
            else
            {
              _rightPanelInPlaceSensor.extend();
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _rightPIPSensorStateLabel.setForeground(Color.RED);
                _rightPIPSensorStateLabel.setText(_UNKNOWN);
                _changeRightPipStateButton.setText(_UNKNOWN);

              }
            });
          }
        }
      });
    }
  };
  private ActionListener _rightOuterBarrierCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_rightOuterBarrier != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if (_rightOuterBarrier.isOpen())
            {
              _rightOuterBarrier.close();
            }
            else
            {
              _rightOuterBarrier.open();
            }

            long time = _rightOuterBarrier.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _outerRightBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _rightOuterBarrierStateLabel.setForeground(Color.RED);
                _rightOuterBarrierStateLabel.setText(_UNKNOWN);
                _rightOuterBarrierCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _leftOuterBarrierCommandButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_leftOuterBarrier != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if (_leftOuterBarrier.isOpen())
            {
              _leftOuterBarrier.close();
            }
            else
            {
              _leftOuterBarrier.open();
            }

            long time = _leftOuterBarrier.getLastHardwareActionTimeInMilliSeconds();
            LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
                    new Object[]
                    {
                      time
                    });
            _outerLeftBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _leftOuterBarrierStateLabel.setForeground(Color.RED);
                _leftOuterBarrierStateLabel.setText(_UNKNOWN);
                _leftOuterBarrierCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _changeMotionStateButtonaActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_belts != null);
      try
      {
        if ((_belts.areStageBeltsOn() == false) && _panelClamps.areClampsClosed())
        {
          String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_MOVE_BELT_WITH_PANEL_CLAMP_CLOSED_WARNING_KEY");

          int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                  message,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.YES_NO_OPTION,
                  JOptionPane.WARNING_MESSAGE);
          if (answer == JOptionPane.NO_OPTION || answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.CLOSED_OPTION)
          {
            return;
          }
        }
      }
      catch (final XrayTesterException xte)
      {
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            handleException(xte);
            _motionStateLabel.setForeground(Color.RED);
            _motionStateLabel.setText(_UNKNOWN);
            _changeMotionStateButton.setText(_UNKNOWN);
          }
        });
      }
      
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if ((_belts.areStageBeltsOn() == false) && _panelClamps.areClampsClosed())
            {
              _panelClamps.open();
            }
            if (_belts.areStageBeltsOn())
            {
              _belts.off();
            }
            else
            {
              _belts.on();
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _motionStateLabel.setForeground(Color.RED);
                _motionStateLabel.setText(_UNKNOWN);
                _changeMotionStateButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _changeDirectionButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_belts != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {

        public void run()
        {
          try
          {
            if (_belts.areStageBeltsDirectionLeftToRight())
            {
              _belts.setDirectionRightToLeft();
            }
            else
            {
              _belts.setDirectionLeftToRight();
            }
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                handleException(xte);
                _directionStateLabel.setForeground(Color.RED);
                _directionStateLabel.setText(_UNKNOWN);
                _changeDirectionButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _loadPanelButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      loadPanel();
      populateCurrentRailWidth();
      populatePanelHandlingStatus();
    }
  };
  private ActionListener _unloadPanelButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      unLoadPanel();
      populateCurrentRailWidth();
      populatePanelHandlingStatus();
    }
  };
  private ActionListener _changeLeftLoadingStateButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("HW_STAGE_NOT_SAFE_TO_MOVE_EXCEPTION_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.ERROR_MESSAGE);
        }
        else
        {
          _panelHandler.moveXYStageToLeftLoadPosition(true);
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _changeRightLoadingStateButtonActionListener = new ActionListener()
  {

    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_digitalIo.isLeftOuterBarrierOpen() || _digitalIo.isRightOuterBarrierOpen() || _digitalIo.isInterlockChain1Open() || _digitalIo.isInterlockChain2Open())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("HW_STAGE_NOT_SAFE_TO_MOVE_EXCEPTION_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.ERROR_MESSAGE);
        }
        else
        {
          _panelHandler.moveXYStageToRightUnloadPosition(true);
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  PanelHandlingPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);

    _parent = parent;

    // create the ui components
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleManualRailWidthEntry()
  {
    String valueEntered = _railWidthTextField.getText();
    if (valueEntered != null && valueEntered.equalsIgnoreCase("") == false)
    {

      try
      {
        double minRailWidthInNanometers = _panelHandler.getMinimumRailWidthInNanoMeters();
        double maxRailWidthInNanometers = _panelHandler.getMaximumRailWidthInNanoMeters();

        double minRailWidthConverted = MathUtil.convertUnits(minRailWidthInNanometers, MathUtilEnum.NANOMETERS, _units);
        double maxRailWidthConverted = MathUtil.convertUnits(maxRailWidthInNanometers, MathUtilEnum.NANOMETERS, _units);

        // first check to make sure the value entered is not greater than or less than the allowed values.
        double parsedRailWidth = 0.0;
        double convertedRailWidth = 0.0;
        try
        {
          parsedRailWidth = StringUtil.convertStringToDouble(valueEntered);
          convertedRailWidth = MathUtil.convertUnits(parsedRailWidth, _units, MathUtilEnum.NANOMETERS);
        }
        catch (BadFormatException bfe)
        {
          _railWidthTextField.setText("");
          LocalizedString badFormatMessageKey = new LocalizedString("SERVICE_UI_SSACTP_PANEL_HANDLING_RAIL_WIDTH_ENTERED_INVALID_KEY",
                  new Object[]
                  {
                    minRailWidthConverted,
                    _units,
                    maxRailWidthConverted,
                    _units
                  });

          String messageToDisplay = StringUtil.format(StringLocalizer.keyToString(badFormatMessageKey), 50);
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  messageToDisplay,
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
          return;
        }


        if (convertedRailWidth >= minRailWidthInNanometers && convertedRailWidth <= maxRailWidthInNanometers)
        {
          // so the value is good. enable the set rail width button
          _setRailWidthButton.setEnabled(true);
        }
        else
        {
          // the value is not correct. display an error to the user and do not enable the set rail width button

          _railWidthTextField.setText("");

          String messageToDisplay = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_HANDLING_RAIL_WIDTH_ENTERED_INVALID_KEY",
                  new Object[]
                  {
                    minRailWidthConverted,
                    _units,
                    maxRailWidthConverted,
                    _units
                  }));
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringUtil.format(messageToDisplay, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
        _railWidthTextField.setText("");
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog(String operationCompletedMessage,
          String operationInProgressMessage,
          boolean cancelableOperation)
  {
    Assert.expect(operationCompletedMessage != null);
    Assert.expect(operationInProgressMessage != null);

    // now lets create the progress bar.
    if (cancelableOperation)
    {
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + " - " + StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_TAB_NAME_KEY"),
              operationInProgressMessage,
              operationCompletedMessage,
              StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
              StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY") + " ",
              StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
              0,
              100,
              true);

      // now create the listiner tied to the cancel button for the progress bar dialog.
      _progressDialog.addCancelActionListener(new ActionListener()
      {

        public void actionPerformed(ActionEvent e)
        {
          try
          {
            _panelHandler.abort();
          }
          catch (XrayTesterException xte)
          {
            handleException(xte);
          }
          finally
          {
            _progressDialog.dispose();
          }
        }
      });
    }
    else
    {
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + " - " + StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_TAB_NAME_KEY"),
              operationInProgressMessage,
              operationCompletedMessage,
              StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
              StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
              0,
              100,
              true);
    }

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());

  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get all the necessary subsystems.
    _xrayTester = XrayTester.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _belts = StageBelts.getInstance();
    _rightOuterBarrier = RightOuterBarrier.getInstance();
    _leftOuterBarrier = LeftOuterBarrier.getInstance();
    _innerBarrier = InnerBarrier.getInstance();
    _panelClamps = PanelClamps.getInstance();
    _rightPanelClearSensor = RightPanelClearSensor.getInstance();
    _leftPanelClearSensor = LeftPanelClearSensor.getInstance();
    _leftPanelInPlaceSensor = LeftPanelInPlaceSensor.getInstance();
    _rightPanelInPlaceSensor = RightPanelInPlaceSensor.getInstance();

    _leftSafetyLevelSensor = LeftSafetyLevelSensor.getInstance();
    _rightSafetyLevelSensor = RightSafetyLevelSensor.getInstance();
    _xrayMotorizedHeightLevelSensor = XrayMotorizedHeightLevelSensor.getInstance();
    _xrayActuator = XrayActuator.getInstance();

    try
    {
      _subsystemInitialized = !_xrayTester.isStartupRequiredForMotionRelatedHardware();
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }

    populatePanel();

    addListeners();

    startObserving();

    if (_subsystemInitialized)
    {
      startMonitoring();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    enablePanel();

    populateCurrentRailWidth();

    populateCurrentStageRailHomeSensorState();

    populatePanelHandlingStatus();

    populatePanelClearSensorStatePanel();

    populateBarrierStatePanels();

    populateClampsStatePanel();

    populatePanelInPlaceSensorPanels();

    populateBeltsPanel();
    
    //XCR-2677 - Panel handling status not update as real time respond (Anthony Fong)
    updateXrayTesterImagePanelState();
    
    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      {
        if(XrayCylinderActuator.isInstalled() || XrayCPMotorActuator.isInstalled())
        {
          populateXrayZAxisSensorPanel();
          populateXrayZAxisPanel();
          populateSafetyLevelSensorPanel();         
        }
        if(DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
        {
          populateMotorizedHeightLevelSensorPanel();
        }
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }

  }

  /**
   * This method will be responsible for updating the current rail width measurement.
   * @author Erica Wheatcroft
   */
  private void populateCurrentRailWidth()
  {
    if (_subsystemInitialized == false)
    {
      _currentRailWidthMeasurementLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      int railWidthInNanometers = _panelHandler.getRailWidthInNanoMeters();
      // now convert the units to the units the user has selected
      double convertedRailWidth = MathUtil.roundToPlaces(
              (double) MathUtil.convertUnits(railWidthInNanometers, MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      _currentRailWidthMeasurementLabel.setForeground(Color.BLACK);
      _currentRailWidthMeasurementLabel.setText(convertedRailWidth + " " + _units);

    }
    catch (XrayTesterException xte)
    {
      _currentRailWidthMeasurementLabel.setForeground(Color.RED);
      _currentRailWidthMeasurementLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanelHandlingStatus()
  {
    if (_subsystemInitialized == false)
    {
      _panelHandlerStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_SUBSYSTEM_NOT_INITIALIZED_KEY"));
      return;
    }

    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    try
    {
      if (_panelHandler.isPanelLoaded())
      {
        _loadPanelButton.setEnabled(false);
        _unloadPanelButton.setEnabled(true);
        _panelHandlerStateLabel.setForeground(Color.BLACK);
        _panelHandlerStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_LOADED_MSG_KEY"));
      }
      else
      {
        _loadPanelButton.setEnabled(true);
        _unloadPanelButton.setEnabled(false);
        _panelHandlerStateLabel.setForeground(Color.BLACK);
        _panelHandlerStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_NOT_LOADED_MSG_KEY"));
      }

    }
    catch (final XrayTesterException xte)
    {
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateCurrentStageRailHomeSensorState()
  {
    if (_subsystemInitialized == false)
    {
      _currentStageRailHomeSensorStateLabel.setForeground(Color.black);
      _currentStageRailHomeSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_panelHandler.isRailWidthHomeSensorClear())
      {
        _currentStageRailHomeSensorStateLabel.setForeground(_GREEN);
        _currentStageRailHomeSensorStateLabel.setText(_CLEAR);
      }
      else
      {
        _currentStageRailHomeSensorStateLabel.setForeground(_YELLOW);
        _currentStageRailHomeSensorStateLabel.setText(_BLOCKED);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _currentStageRailHomeSensorStateLabel.setForeground(Color.RED);
      _currentStageRailHomeSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateBeltsPanel()
  {
    if (_subsystemInitialized == false)
    {
      _directionStateLabel.setForeground(Color.BLACK);
      _directionStateLabel.setText(_UNKNOWN);
      _motionStateLabel.setText(_UNKNOWN);
      _changeDirectionButton.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_belts.areStageBeltsDirectionLeftToRight())
      {
        _directionStateLabel.setForeground(Color.BLACK);
        _directionStateLabel.setText(_LEFT_TO_RIGHT);
        _changeDirectionButton.setText(_RIGHT_TO_LEFT);
      }
      else
      {
        _directionStateLabel.setForeground(Color.BLACK);
        _directionStateLabel.setText(_RIGHT_TO_LEFT);
        _changeDirectionButton.setText(_LEFT_TO_RIGHT);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _directionStateLabel.setForeground(Color.RED);
      _directionStateLabel.setText(_UNKNOWN);
      _changeDirectionButton.setText(_UNKNOWN);
    }

    // update the belt motion

    try
    {
      if (_belts.areStageBeltsOn())
      {
        _motionStateLabel.setForeground(Color.BLACK);
        _motionStateLabel.setText(_ON);
        _changeMotionStateButton.setText(_OFF);
      }
      else
      {
        _motionStateLabel.setForeground(Color.BLACK);
        _motionStateLabel.setText(_OFF);
        _changeMotionStateButton.setText(_ON);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _motionStateLabel.setForeground(Color.RED);
      _motionStateLabel.setText(_UNKNOWN);
      _changeMotionStateButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanelInPlaceSensorPanels()
  {
    populateLeftPanelInPlaceSensorsPanel();
    populateRightPanelInPlaceSensorsPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateRightPanelInPlaceSensorsPanel()
  {
    if (_subsystemInitialized == false)
    {
      _rightPIPPositionStateLabel.setForeground(Color.BLACK);
      _rightPIPPositionStateLabel.setText(_UNKNOWN);
      _changeRightPipStateButton.setText(_UNKNOWN);
      _rightPIPSensorStateLabel.setText(_UNKNOWN);
      return;
    }
    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {

      if (DigitalIo.getInstance().isOpticalStopControllerEndStopperInstalled())
      {
		try
		      {
		        if (_rightPanelInPlaceSensor.isExtended())
		        {
		          _rightPIPPositionStateLabel.setForeground(Color.BLACK);
		          _rightPIPPositionStateLabel.setText(_EXTENDED);
		          _changeRightPipStateButton.setText(_RETRACT);
		        }
		        else
		        {
		          _rightPIPPositionStateLabel.setForeground(Color.BLACK);
		          _rightPIPPositionStateLabel.setText(_RETRACTED);
		          _changeRightPipStateButton.setText(_EXTEND);
		        }
		      }
		      catch (final XrayTesterException xte)
		      {
		        handleException(xte);
		        _rightPIPPositionStateLabel.setForeground(Color.RED);
		        _rightPIPPositionStateLabel.setText(_UNKNOWN);
		        _changeRightPipStateButton.setText(_UNKNOWN);
		      }
      }
 

    }
    else
    {
      try
      {
        if (_rightPanelInPlaceSensor.isExtended())
        {
          _rightPIPPositionStateLabel.setForeground(Color.BLACK);
          _rightPIPPositionStateLabel.setText(_EXTENDED);
          _changeRightPipStateButton.setText(_RETRACT);
        }
        else
        {
          _rightPIPPositionStateLabel.setForeground(Color.BLACK);
          _rightPIPPositionStateLabel.setText(_RETRACTED);
          _changeRightPipStateButton.setText(_EXTEND);
        }
      }
      catch (final XrayTesterException xte)
      {
        handleException(xte);
        _rightPIPPositionStateLabel.setForeground(Color.RED);
        _rightPIPPositionStateLabel.setText(_UNKNOWN);
        _changeRightPipStateButton.setText(_UNKNOWN);
      }
    }

    // now lets check to see if the sensor is blocked
    try
    {
      //Don't call _rightPanelInPlaceSensor.isPanelInPlace(true)
      //This will cause the Panel Clamps reopen again,
      //even though try to close it manually
      if (_rightPanelInPlaceSensor.isPanelInPlace(false))         
      {
        _rightPIPSensorStateLabel.setForeground(Color.BLACK);
        _rightPIPSensorStateLabel.setText(_YES);
      }
      else
      {
        _rightPIPSensorStateLabel.setForeground(Color.BLACK);
        _rightPIPSensorStateLabel.setText(_NO);
      }
    }
    catch (final XrayTesterException xte)
    {
      _rightPIPSensorStateLabel.setForeground(Color.RED);
      _rightPIPSensorStateLabel.setText(_UNKNOWN);
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateLeftPanelInPlaceSensorsPanel()
  {
    if (_subsystemInitialized == false)
    {
      _leftPIPStateLabel.setForeground(Color.BLACK);
      _leftPanelInPlaceLabel.setText(_UNKNOWN);
      _leftPIPStateLabel.setText(_UNKNOWN);
      _changeLeftPipStateButton.setText(_UNKNOWN);
      return;
    }

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if( (DigitalIo.isOpticalStopControllerInstalled() == false) ||(_digitalIo.isOpticalStopControllerEndStopperInstalled()))
    {
      // first lets check the panel in place sensor
      try
      {
        if (_leftPanelInPlaceSensor.isExtended())
        {
          _leftPIPStateLabel.setForeground(Color.BLACK);
          _leftPIPStateLabel.setText(_EXTENDED);
          _changeLeftPipStateButton.setText(_RETRACT);
        }
        else
        {
          _leftPIPStateLabel.setForeground(Color.BLACK);
          _leftPIPStateLabel.setText(_RETRACTED);
          _changeLeftPipStateButton.setText(_EXTEND);
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
        _leftPIPStateLabel.setForeground(Color.RED);
        _leftPIPStateLabel.setText(_UNKNOWN);
        _changeLeftPipStateButton.setText(_UNKNOWN);

      }
    }

    // now lets check to see if the sensor is blocked

    try
    {
      //Don't call _leftPanelInPlaceSensor.isPanelInPlace(true)  
      //This will cause the Panel Clamps reopen again, 
      //even though try to close it manually
      if (_leftPanelInPlaceSensor.isPanelInPlace(false)) 
      {
        _leftPanelInPlaceLabel.setForeground(Color.BLACK);
        _leftPanelInPlaceLabel.setText(_YES);
      }
      else
      {
        _leftPanelInPlaceLabel.setForeground(Color.BLACK);
        _leftPanelInPlaceLabel.setText(_NO);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _leftPanelInPlaceLabel.setForeground(Color.RED);
      _leftPanelInPlaceLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateClampsStatePanel()
  {
    if (_subsystemInitialized == false)
    {
      _clampsStateLabel.setForeground(Color.BLACK);
      _clampsStateLabel.setText(_UNKNOWN);
      _clampsCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (_panelClamps.areClampsClosed())
      {
        _clampsStateLabel.setForeground(Color.BLACK);
        _clampsStateLabel.setText(_CLOSED);
        _clampsCommandButton.setText(_OPEN);
      }
      else
      {
        _clampsStateLabel.setForeground(Color.BLACK);
        _clampsStateLabel.setText(_OPEN);
        _clampsCommandButton.setText(_CLOSED);
      }
    }
    catch (final XrayTesterException xte)
    {
      handleException(xte);
      _clampsStateLabel.setForeground(Color.RED);
      _clampsStateLabel.setText(_UNKNOWN);
      _clampsCommandButton.setText(_UNKNOWN);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateLeftPanelClearSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _leftPanelClearSensorStateLabel.setForeground(Color.BLACK);
      _rightPanelClearSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_leftPanelClearSensor.isBlocked())
      {
        _innerMachineImagePanel.leftPanelClearSensorStateBlocked();
        _leftPanelClearSensorStateLabel.setForeground(_YELLOW);
        _leftPanelClearSensorStateLabel.setText(_BLOCKED);
      }
      else
      {
        _innerMachineImagePanel.leftPanelClearSensorStateClear();
        _leftPanelClearSensorStateLabel.setForeground(_GREEN);
        _leftPanelClearSensorStateLabel.setText(_CLEAR);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _leftPanelClearSensorStateLabel.setForeground(Color.RED);
      _leftPanelClearSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Anthony Fong
   */
  private void populateLeftSafetyLevelSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _leftSafetyLevelSensorStateLabel.setForeground(Color.BLACK);
      _leftSafetyLevelSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_leftSafetyLevelSensor.isOK())
      {
        _innerMachineImagePanel.leftSafetyLevelOK();
        _leftSafetyLevelSensorStateLabel.setForeground(_YELLOW);
        _leftSafetyLevelSensorStateLabel.setText(_YES);
      }
      else
      {
        _innerMachineImagePanel.leftSafetyLevelNotOK();
        _leftSafetyLevelSensorStateLabel.setForeground(_GREEN);
        _leftSafetyLevelSensorStateLabel.setText(_NO);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _leftSafetyLevelSensorStateLabel.setForeground(Color.RED);
      _leftSafetyLevelSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Anthony Fong
   */
  private void populateRightSafetyLevelSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _rightSafetyLevelSensorStateLabel.setForeground(Color.BLACK);
      _rightSafetyLevelSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_rightSafetyLevelSensor.isOK())
      {
        _innerMachineImagePanel.rightSafetyLevelOK();
        _rightSafetyLevelSensorStateLabel.setForeground(_YELLOW);
        _rightSafetyLevelSensorStateLabel.setText(_YES);
      }
      else
      {
        _innerMachineImagePanel.rightSafetyLevelNotOK();
        _rightSafetyLevelSensorStateLabel.setForeground(_GREEN);
        _rightSafetyLevelSensorStateLabel.setText(_NO);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _rightSafetyLevelSensorStateLabel.setForeground(Color.RED);
      _rightSafetyLevelSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Anthony Fong
   */
  private void populateSafetyLevelSensorPanel()
  {
    populateLeftSafetyLevelSensorPanel();
    populateRightSafetyLevelSensorPanel();
  }

  /**
   * @author Anthony Fong
   */
  private void populateXrayZAxisSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _xrayZAxisSensorStateLabel.setForeground(Color.BLACK);
      _xrayZAxisSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (XrayActuator.isInstalled())
      {
        if (_xrayActuator.isUpToPosition1())
        {
          _innerMachineImagePanel.xrayCylinderUpToPosition1();
          _xrayZAxisSensorStateLabel.setForeground(_YELLOW);
          if (XrayCylinderActuator.isInstalled())
          {
            _xrayZAxisSensorStateLabel.setText(_UP);
          }
          else
          {
            _xrayZAxisSensorStateLabel.setText(_UP_POS_1);
          }
        }
        else if (_xrayActuator.isUpToPosition2())
        {
          _innerMachineImagePanel.xrayCylinderUpToPosition2();
          _xrayZAxisSensorStateLabel.setForeground(_YELLOW);
          _xrayZAxisSensorStateLabel.setText(_UP_POS_2);
        }
        else if (_xrayActuator.isDown())
        {
          _innerMachineImagePanel.xrayCylinderHighMag();
          _xrayZAxisSensorStateLabel.setForeground(_YELLOW);
          _xrayZAxisSensorStateLabel.setText(_DOWN);
        }
        else if (_xrayActuator.isHome())
        {
          _innerMachineImagePanel.xrayCylinderHome();
          _xrayZAxisSensorStateLabel.setForeground(_GREEN);
          _xrayZAxisSensorStateLabel.setText(_HOME);
        }
        
      }
      else
      {
        _xrayZAxisSensorStateLabel.setForeground(Color.RED);
        _xrayZAxisSensorStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _xrayZAxisSensorStateLabel.setForeground(Color.RED);
      _xrayZAxisSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Anthony Fong
   */
  private void populateXrayZAxisPanel()
  {
    if (_subsystemInitialized == false)
    {
      _xrayZAxisStateLabel.setForeground(Color.BLACK);
      _xrayZAxisStateLabel.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (XrayActuator.isInstalled())
      {
        if (_xrayActuator.isUpToPosition1())
        {
          _xrayZAxisStateLabel.setForeground(Color.BLACK);
          if (XrayCylinderActuator.isInstalled())
          {
            _xrayZAxisStateLabel.setText(_UP);
          }
          else
          {
            _xrayZAxisStateLabel.setText(_UP_POS_1);
          }
        }
        else if (_xrayActuator.isUpToPosition2())
        {
          _xrayZAxisStateLabel.setForeground(Color.BLACK);
          _xrayZAxisStateLabel.setText(_UP_POS_2);
        }
        else if (_xrayActuator.isDown())
        {
          _xrayZAxisStateLabel.setForeground(Color.BLACK);
          _xrayZAxisStateLabel.setText(_DOWN);
        }
        else if (_xrayActuator.isHome())
        {
          _xrayZAxisStateLabel.setForeground(Color.BLACK);
          _xrayZAxisStateLabel.setText(_HOME);
        }
      }
      else
      {
        _xrayZAxisStateLabel.setForeground(Color.RED);
        _xrayZAxisStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
        _xrayZAxisHomeCommandButton.setEnabled(false);
        _xrayZAxisUpToPosition1CommandButton.setEnabled(false);
        _xrayZAxisUpToPosition2CommandButton.setEnabled(false);
        _xrayZAxisDownCommandButton.setEnabled(false);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _xrayZAxisStateLabel.setForeground(Color.RED);
      _xrayZAxisStateLabel.setText(_UNKNOWN);
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void populateMotorizedHeightLevelSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _motorizedHeightLevelSensorStateLabel.setForeground(Color.BLACK);
      _motorizedHeightLevelSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
      {
        if (_xrayMotorizedHeightLevelSensor.isUp())
        {
          _innerMachineImagePanel.motorizedHeightLevelSensorUp();
          _motorizedHeightLevelSensorStateLabel.setForeground(_GREEN);
          _motorizedHeightLevelSensorStateLabel.setText(_UP);
          _motorizedHeightLevelSensorUpDownCommandButton.setText(_DOWN);
        }
        else if (_xrayMotorizedHeightLevelSensor.isDown())
        {
          _innerMachineImagePanel.motorizedHeightLevelSensorDown();
          _motorizedHeightLevelSensorStateLabel.setForeground(_YELLOW);
          _motorizedHeightLevelSensorStateLabel.setText(_DOWN);
          _motorizedHeightLevelSensorUpDownCommandButton.setText(_UP);
        }
        else if (_xrayMotorizedHeightLevelSensor.isHome())
        {
          _innerMachineImagePanel.motorizedHeightLevelSensorUp();
          _motorizedHeightLevelSensorStateLabel.setForeground(_GREEN);
          _motorizedHeightLevelSensorStateLabel.setText(_HOME);
          _motorizedHeightLevelSensorUpDownCommandButton.setText(_DOWN);
        }
        
      }
      else
      {
        _motorizedHeightLevelSensorStateLabel.setForeground(Color.RED);
        _motorizedHeightLevelSensorUpDownCommandButton.setEnabled(false);
        _motorizedHeightLevelSensorStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _motorizedHeightLevelSensorStateLabel.setForeground(Color.RED);
      _motorizedHeightLevelSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateRightPanelClearSensorPanel()
  {
    if (_subsystemInitialized == false)
    {
      _rightPanelClearSensorStateLabel.setForeground(Color.BLACK);
      _rightPanelClearSensorStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_rightPanelClearSensor.isBlocked())
      {
        _innerMachineImagePanel.rightPanelClearSensorStateBlocked();
        _rightPanelClearSensorStateLabel.setForeground(_YELLOW);
        _rightPanelClearSensorStateLabel.setText(_BLOCKED);
      }
      else
      {
        _innerMachineImagePanel.rightPanelClearSensorStateClear();
        _rightPanelClearSensorStateLabel.setForeground(_GREEN);
        _rightPanelClearSensorStateLabel.setText(_CLEAR);
      }
    }
    catch (final XrayTesterException xte)
    {
      handleException(xte);
      _rightPanelClearSensorStateLabel.setForeground(Color.RED);
      _rightPanelClearSensorStateLabel.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanelClearSensorStatePanel()
  {
    populateLeftPanelClearSensorPanel();
    populateRightPanelClearSensorPanel();
  }

  /**
   * this method will update the status of the inner and outer barriers
   * @author Erica Wheatcroft
   */
  private void populateBarrierStatePanels()
  {
    // lets do the outer barriers first
    populateRightOuterBarrierPanel();
    populateLeftOuterBarrierPanel();

    // now the inner barrier panel
    populateInnerBarrierPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateInnerBarrierPanel()
  {

    if (_subsystemInitialized == false)
    {
      _innerBarrierStateLabel.setForeground(Color.BLACK);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (_innerBarrier.isInstalled())
      {
        if (_innerBarrier.isOpen())
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_OPEN);
          _innerBarrierCommandButton.setText(_CLOSED);
        }
        else
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_CLOSED);
          _innerBarrierCommandButton.setText(_OPEN);
        }
      }
      else
      {
        _innerBarrierStateLabel.setForeground(Color.RED);
        _innerBarrierStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
        _innerBarrierCommandButton.setEnabled(false);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _innerBarrierStateLabel.setForeground(Color.RED);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateLeftOuterBarrierPanel()
  {
    if (_subsystemInitialized == false)
    {
      _leftOuterBarrierStateLabel.setForeground(Color.BLACK);
      _leftOuterBarrierStateLabel.setText(_UNKNOWN);
      _leftOuterBarrierCommandButton.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_leftOuterBarrier.isOpen())
      {
        _leftOuterBarrierStateLabel.setForeground(Color.BLACK);
        _leftOuterBarrierStateLabel.setText(_OPEN);
        _leftOuterBarrierCommandButton.setText(_CLOSE);
      }
      else
      {
        _leftOuterBarrierStateLabel.setForeground(Color.BLACK);
        _leftOuterBarrierStateLabel.setText(_CLOSED);
        _leftOuterBarrierCommandButton.setText(_OPEN);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _leftOuterBarrierStateLabel.setForeground(Color.RED);
      _leftOuterBarrierStateLabel.setText(_UNKNOWN);
      _leftOuterBarrierCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateRightOuterBarrierPanel()
  {
    if (_subsystemInitialized == false)
    {
      _rightOuterBarrierStateLabel.setForeground(Color.BLACK);
      _rightOuterBarrierStateLabel.setText(_UNKNOWN);
      _rightOuterBarrierCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (_rightOuterBarrier.isOpen())
      {
        _rightOuterBarrierStateLabel.setForeground(Color.BLACK);
        _rightOuterBarrierStateLabel.setText(_OPEN);
        _rightOuterBarrierCommandButton.setText(_CLOSE);
      }
      else
      {
        _rightOuterBarrierStateLabel.setForeground(Color.BLACK);
        _rightOuterBarrierStateLabel.setText(_CLOSED);
        _rightOuterBarrierCommandButton.setText(_OPEN);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _rightOuterBarrierStateLabel.setForeground(Color.RED);
      _rightOuterBarrierStateLabel.setText(_UNKNOWN);
      _rightOuterBarrierCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private synchronized void startMonitoring()
  {
    //Siew Yeng
    //This is to check if monitor thread is already running. Run it only when it is not running to avoid creating extra thread.
    if (!_monitorEnabled)
    {
      _monitorEnabled = true;
      _monitorLeftPanelClearSensorStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateLeftPanelClearSensorPanel();
              }
            });
            try
            {
              Thread.sleep(250);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
      _monitorStageRailHomeSensorStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateCurrentStageRailHomeSensorState();
              }
            });
            try
            {
              Thread.sleep(1000);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
      _monitorLeftPanelInPlaceStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateLeftPanelInPlaceSensorsPanel();
              }
            });
            try
            {
              Thread.sleep(250);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
      _monitorRightPanelClearSensorStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateRightPanelClearSensorPanel();
              }
            });
            try
            {
              Thread.sleep(250);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });

      _monitorRightPanelPanelInPlaceStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateRightPanelInPlaceSensorsPanel();
              }
            });
            try
            {
              Thread.sleep(250);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
      _monitorSafetyLevelSensorStateThread.invokeLater(new Runnable()
      {

        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                populateSafetyLevelSensorPanel();
              }
            });
            try
            {
              Thread.sleep(250);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enablePanel()
  {
    // this is always enabled
    _initializeButton.setEnabled(true);

    // rail width always disabled until the user enters a valid value.
    _setRailWidthButton.setEnabled(_subsystemInitialized);
    _loopControlButton.setEnabled(_subsystemInitialized);
    _unloadPanelButton.setEnabled(_subsystemInitialized);
    _loadPanelButton.setEnabled(_subsystemInitialized);
    _homeRailsButton.setEnabled(_subsystemInitialized);
    _innerBarrierCommandButton.setEnabled(_subsystemInitialized);
    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      {
        _xrayZAxisHomeCommandButton.setEnabled(_subsystemInitialized);
        _xrayZAxisUpToPosition1CommandButton.setEnabled(_subsystemInitialized);
        _xrayZAxisUpToPosition2CommandButton.setEnabled(_subsystemInitialized);
        _xrayZAxisDownCommandButton.setEnabled(_subsystemInitialized);
        if(DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
        {
          _motorizedHeightLevelSensorUpDownCommandButton.setEnabled(_subsystemInitialized);
        }
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }

    _clampsCommandButton.setEnabled(_subsystemInitialized);
    _leftOuterBarrierCommandButton.setEnabled(_subsystemInitialized);
    _rightOuterBarrierCommandButton.setEnabled(_subsystemInitialized);
    _changeLeftPipStateButton.setEnabled(_subsystemInitialized);
    _changeMotionStateButton.setEnabled(_subsystemInitialized);
    _changeRightPipStateButton.setEnabled(_subsystemInitialized);
    _changeDirectionButton.setEnabled(_subsystemInitialized);
    _railWidthTextField.setEnabled(_subsystemInitialized);

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _changeLeftLoadingStateButton.setEnabled(_subsystemInitialized);
      _changeRightLoadingStateButton.setEnabled(_subsystemInitialized);
    }
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    try
    {
      if (_subsystemInitialized)
      {
        if (_panelHandler.isPanelLoaded())
        {
          // a panel is loaded so the load panel should be not enabled
          _loadPanelButton.setEnabled(false);
          _unloadPanelButton.setEnabled(true);
        }
        else
        {
          _loadPanelButton.setEnabled(true);
          _unloadPanelButton.setEnabled(false);
        }
      }
    }
    catch (final XrayTesterException xte)
    {
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _loopControlButton.addActionListener(_loopControlButtonActionListener);
    _homeRailsButton.addActionListener(_homeRailsButtonActionListener);
    _setRailWidthButton.addActionListener(_setRailWidthButtonActionListener);
    _railWidthTextField.addFocusListener(_railWidthTextFieldFocusListener);
    _railWidthTextField.addActionListener(_railWidthTextFieldActionListener);
    _innerBarrierCommandButton.addActionListener(_innerBarrierCommandButtonActionListener);

    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      { 
        if(XrayCylinderActuator.isInstalled() || XrayCPMotorActuator.isInstalled())
        {
          _xrayZAxisHomeCommandButton.addActionListener(_xrayZAxisHomeCommandButtonActionListener);
          _xrayZAxisUpToPosition1CommandButton.addActionListener(_xrayZAxisUpToPosition1CommandButtonActionListener);
          _xrayZAxisUpToPosition2CommandButton.addActionListener(_xrayZAxisUpToPosition2CommandButtonActionListener);
          _xrayZAxisDownCommandButton.addActionListener(_xrayZAxisDownCommandButtonActionListener);
        }
        if(DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
        {
          _motorizedHeightLevelSensorUpDownCommandButton.addActionListener(_motorizedHeightLevelSensorUpDownCommandButtonActionListener);
        }
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }
    _clampsCommandButton.addActionListener(_clampsCommandButtonActionListener);
    _changeLeftPipStateButton.addActionListener(_changeLeftPipStateButtonActionListener);
    _changeRightPipStateButton.addActionListener(_changeRightPipStateButtonActionListener);
    _rightOuterBarrierCommandButton.addActionListener(_rightOuterBarrierCommandButtonActionListener);
    _leftOuterBarrierCommandButton.addActionListener(_leftOuterBarrierCommandButtonActionListener);
    _changeDirectionButton.addActionListener(_changeDirectionButtonActionListener);
    _changeMotionStateButton.addActionListener(_changeMotionStateButtonaActionListener);
    _loadPanelButton.addActionListener(_loadPanelButtonActionListener);
    _unloadPanelButton.addActionListener(_unloadPanelButtonActionListener);

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _changeLeftLoadingStateButton.addActionListener(_changeLeftLoadingStateButtonActionListener);
      _changeRightLoadingStateButton.addActionListener(_changeRightLoadingStateButtonActionListener);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _loopControlButton.removeActionListener(_loopControlButtonActionListener);
    _homeRailsButton.removeActionListener(_homeRailsButtonActionListener);
    _setRailWidthButton.removeActionListener(_setRailWidthButtonActionListener);
    _railWidthTextField.removeFocusListener(_railWidthTextFieldFocusListener);
    _railWidthTextField.removeActionListener(_railWidthTextFieldActionListener);
    _innerBarrierCommandButton.removeActionListener(_innerBarrierCommandButtonActionListener);
    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      {
        if(XrayCylinderActuator.isInstalled() || XrayCPMotorActuator.isInstalled())
        {
          _xrayZAxisHomeCommandButton.removeActionListener(_xrayZAxisHomeCommandButtonActionListener);
          _xrayZAxisUpToPosition1CommandButton.removeActionListener(_xrayZAxisUpToPosition1CommandButtonActionListener);
          _xrayZAxisUpToPosition2CommandButton.removeActionListener(_xrayZAxisUpToPosition2CommandButtonActionListener);
          _xrayZAxisDownCommandButton.removeActionListener(_xrayZAxisDownCommandButtonActionListener);
        }
        if(DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
        {
          _motorizedHeightLevelSensorUpDownCommandButton.removeActionListener(_motorizedHeightLevelSensorUpDownCommandButtonActionListener);
        }
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }
    _clampsCommandButton.removeActionListener(_clampsCommandButtonActionListener);
    _changeLeftPipStateButton.removeActionListener(_changeLeftPipStateButtonActionListener);
    _changeRightPipStateButton.removeActionListener(_changeRightPipStateButtonActionListener);
    _rightOuterBarrierCommandButton.removeActionListener(_rightOuterBarrierCommandButtonActionListener);
    _leftOuterBarrierCommandButton.removeActionListener(_leftOuterBarrierCommandButtonActionListener);
    _changeDirectionButton.removeActionListener(_changeDirectionButtonActionListener);
    _changeMotionStateButton.removeActionListener(_changeMotionStateButtonaActionListener);
    _loadPanelButton.removeActionListener(_loadPanelButtonActionListener);
    _unloadPanelButton.removeActionListener(_unloadPanelButtonActionListener);

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _changeLeftLoadingStateButton.removeActionListener(_changeLeftLoadingStateButtonActionListener);
      _changeRightLoadingStateButton.removeActionListener(_changeRightLoadingStateButtonActionListener);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    HardwareObservable.getInstance().addObserver(this);
    GuiObservable.getInstance().addObserver(this);
    ProgressObservable.getInstance().addObserver(this);
    _innerMachineImagePanel.startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    HardwareObservable.getInstance().deleteObserver(this);
    GuiObservable.getInstance().deleteObserver(this);
    ProgressObservable.getInstance().deleteObserver(this);
    _innerMachineImagePanel.stopObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    stopMonitoring();

    stopObserving();

    removeListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {

      public void run()
      {
        Assert.expect(observable != null);
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport) arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
          {
            percentDone = 99;
          }

          if (_progressDialog != null)
          {
            _progressDialog.updateProgressBarPrecent(percentDone);
          }
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent) arg;
            HardwareEventEnum eventEnum = event.getEventEnum();

            if (eventEnum instanceof XrayTesterEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(XrayTesterEventEnum.STARTUP_MOTION_RELATED_HARDWARE))
              {
                if (_progressDialog != null)
                {
                  _progressDialog.updateProgressBarPrecent(100);
                }

                //Ngie Xing, update Image Panel status
                updateXrayTesterImagePanelState();
              }
            }
            else if (eventEnum instanceof PanelHandlerEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(PanelHandlerEventEnum.HOME_STAGE_RAILS))
              {
                if (_progressDialog != null)
                {
                  _progressDialog.updateProgressBarPrecent(100);
                }
              }

              if (eventEnum.equals(PanelHandlerEventEnum.LOAD_PANEL))
              {
                if (event.isStart() == false && _panelLoadInProgress)
                {
                  closeBusyCancelDialog();
                  _panelLoadInProgress = false;
                  populatePanelHandlingStatus();
                }
              }
              else if (eventEnum.equals(PanelHandlerEventEnum.UNLOAD_PANEL))
              {
                if (event.isStart() == false && _panelUnloadInProgress)
                {
                  closeBusyCancelDialog();
                  _panelUnloadInProgress = false;
                  populatePanelHandlingStatus();
                }
              }
              else if (eventEnum.equals(PanelHandlerEventEnum.CANCEL_LOAD_PANEL))
              {
                if (event.isStart() == false && _panelUnloadInProgress)
                {
                  closeBusyCancelDialog();
                  _panelUnloadInProgress = false;
                  populatePanelHandlingStatus();
                }
              }
              else if (eventEnum.equals(PanelHandlerEventEnum.SET_RAIL_WIDTH))
              {
                if (event.isStart() == false && _setRailWidthInProgress)
                {
                  closeBusyCancelDialog();
                  _setRailWidthInProgress = false;
                }
              }
            }
            else if (eventEnum instanceof InnerBarrierEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(InnerBarrierEventEnum.BARRIER_OPEN)
                      || eventEnum.equals(InnerBarrierEventEnum.BARRIER_CLOSE)))
              {
                populateInnerBarrierPanel();
              }
            }
            else if (eventEnum instanceof XrayCylinderEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(XrayCylinderEventEnum.XRAY_CYLINDER_UP)
                      || eventEnum.equals(XrayCylinderEventEnum.XRAY_CYLINDER_DOWN)))
              {
                try
                {
                  if (LicenseManager.isVariableMagnificationEnabled() == true)
                  {  
                    if(XrayCylinderActuator.isInstalled())
                    {
                      populateXrayZAxisPanel();
                      populateXrayZAxisSensorPanel();
                    }
                  }
                }
                catch (BusinessException bex)
                {
                  //do nothing
                }
              }
            }
            else if (eventEnum instanceof XrayCPMotorEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HOME)
                      || eventEnum.equals(XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG)
                      || eventEnum.equals(XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_HIGH_MAG)
                      || eventEnum.equals(XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION1)
                      || eventEnum.equals(XrayCPMotorEventEnum.XRAY_Z_AXIS_MOVE_LOW_MAG_POSITION2)))
              {
                try
                {
                  if (LicenseManager.isVariableMagnificationEnabled() == true)
                  {
                    if(XrayCPMotorActuator.isInstalled())
                    {
                      populateXrayZAxisPanel();
                      populateXrayZAxisSensorPanel();
                    }
                  }
                }
                catch (BusinessException bex)
                {
                  //do nothing
                }
              }
            }
            else if (eventEnum instanceof XrayMotorizedHeightLevelSensorEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HIGH_MAG)
                      || eventEnum.equals(XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_HOME)
                      || eventEnum.equals(XrayMotorizedHeightLevelSensorEventEnum.XRAY_MOTORIZED_HEIGHT_LEVEL_SENSOR_MOVE_LOW_MAG)))
              {
                try
                {
                  if (LicenseManager.isVariableMagnificationEnabled() == true)
                  {
                    if (DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
                    {
                      populateMotorizedHeightLevelSensorPanel();
                    }
                  }
                }
                catch (BusinessException bex)
                {
                  //do nothing
                }
              }
            }
            else if (eventEnum instanceof PanelClampsEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(PanelClampsEventEnum.CLAMPS_CLOSE)
                      || eventEnum.equals(PanelClampsEventEnum.CLAMPS_OPEN)))
              {
                populateClampsStatePanel();
              }
            }
            else if (eventEnum instanceof LeftPanelInPlaceSensorEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(LeftPanelInPlaceSensorEventEnum.SENSOR_EXTEND)
                      || eventEnum.equals(LeftPanelInPlaceSensorEventEnum.SENSOR_RETRACT)))
              {
                try
                {
                  if (LicenseManager.isVariableMagnificationEnabled() == true)
                  {
                    populateLeftPanelInPlaceSensorsPanel();
                  }
                }
                catch (BusinessException bex)
                {
                  //do nothing
                }
              }
            }
            else if (eventEnum instanceof RightPanelInPlaceSensorEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(RightPanelInPlaceSensorEventEnum.SENSOR_EXTEND)
                      || eventEnum.equals(RightPanelInPlaceSensorEventEnum.SENSOR_RETRACT)))
              {
                try
                {
                  if (LicenseManager.isVariableMagnificationEnabled() == true)
                  {
                    populateRightPanelInPlaceSensorsPanel();
                  }
                }
                catch (BusinessException bex)
                {
                  //do nothing
                }
              }
            }
            else if (eventEnum instanceof RightOuterBarrierEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(RightOuterBarrierEventEnum.BARRIER_CLOSE)
                      || eventEnum.equals(RightOuterBarrierEventEnum.BARRIER_OPEN)))
              {
                populateRightOuterBarrierPanel();
              }
            }
            else if (eventEnum instanceof LeftOuterBarrierEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(LeftOuterBarrierEventEnum.BARRIER_CLOSE)
                      || eventEnum.equals(LeftOuterBarrierEventEnum.BARRIER_OPEN)))
              {
                populateLeftOuterBarrierPanel();
              }
            }
            else if (eventEnum instanceof StageBeltsEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(StageBeltsEventEnum.BELTS_DIRECTION_LEFT_TO_RIGHT)
                      || eventEnum.equals(StageBeltsEventEnum.BELTS_DIRECTION_RIGHT_TO_LEFT)
                      || eventEnum.equals(StageBeltsEventEnum.BELTS_OFF)
                      || eventEnum.equals(StageBeltsEventEnum.BELTS_ON)))
              {
                populateBeltsPanel();
              }
            }
          }
          else
          {
            if (arg instanceof HardwareException && arg instanceof InterlockHardwareException)
            {
              // do nothing as we do not need to display this for this tab.
            }
          }
        }
        else if (observable instanceof GuiObservable)
        {
          GuiEvent event = (GuiEvent) arg;
          GuiEventEnum eventEnum = event.getGuiEventEnum();
          if (eventEnum instanceof SelectionEventEnum)
          {
            SelectionEventEnum selectionEventEnum = (SelectionEventEnum) eventEnum;
            if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
            {
              _units = (MathUtilEnum) event.getSource();
              // so update the current rail width
              populateCurrentRailWidth();
              // and update the panel that allows user to set the rail width.
              _railWidthUnitsLabel.setText(MeasurementUnits.getMeasurementUnitString(_units));
            }
          }
        }
        else
        {
          Assert.expect(false);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  void closeBusyCancelDialog()
  {
    if (_busyCancelDialog != null)
    {
      // wait until visible
      while (_busyCancelDialog.isVisible() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      _busyCancelDialog.setVisible(false);
      _busyCancelDialog = null;
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void unLoadPanel()
  {
    _busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_UNLOADING_PANEL_MSG_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            true);
    _busyCancelDialog.pack();
    SwingUtils.centerOnComponent(_busyCancelDialog, MainMenuGui.getInstance());
    _panelUnloadInProgress = true;

    // if we have made it this far lets now load the panel.
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          _panelHandler.unloadPanel();
        }
        catch (XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              _panelLoadInProgress = false;
              closeBusyCancelDialog();
              populatePanelHandlingStatus();
            }
          });
          handleException(xte);
        }
      }
    });
    _busyCancelDialog.setVisible(true);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void loadPanel()
  {
    LoadPanelDialog dialog = new LoadPanelDialog(MainMenuGui.getInstance(), true, _units);
    int returnValue = dialog.showDialog();

    if (returnValue != JOptionPane.OK_OPTION)
    {
      return;
    }

    final int panelLengthInNanometers = dialog.getPanelLengthInNanometers();
    final int panelWidthInNanometers = dialog.getPanelWidthInNanometers();
    dialog = null;

    // now lets check the values entered
    int maxPanelLengthInNanometers = _panelHandler.getMaximumPanelLengthInNanoMeters();
    int minPanelLengthInNanometers = _panelHandler.getMinimumPanelLengthInNanoMeters();
    if ((panelLengthInNanometers <= maxPanelLengthInNanometers
            && panelLengthInNanometers >= minPanelLengthInNanometers) == false)
    {
      // the length entered does not meet the requirements. let the user know and eixt the method.
      double convertedPanelLength = MathUtil.roundToPlaces((double) MathUtil.convertUnits(panelLengthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      double convertedMaxPanelLength = MathUtil.roundToPlaces((double) MathUtil.convertUnits(maxPanelLengthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      double convertedMinPanelLength = MathUtil.roundToPlaces((double) MathUtil.convertUnits(minPanelLengthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_LENGTH_INVALID_KEY",
              new Object[]
              {
                convertedPanelLength,
                _units,
                convertedMinPanelLength,
                _units,
                convertedMaxPanelLength,
                _units
              }));

      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringUtil.format(message, 50),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.ERROR_MESSAGE);
      return;
    }

    int maxPanelWidthInNanometers = _panelHandler.getMaximumPanelWidthInNanoMeters();
    int minPanelWidthInNanometers = _panelHandler.getMinimumPanelWidthInNanoMeters();
    if ((panelWidthInNanometers <= maxPanelWidthInNanometers && panelWidthInNanometers >= minPanelWidthInNanometers) == false)
    {
      // the length entered does not meet the requirements. let the user know and eixt the method.
      double convertedPanelWidth = MathUtil.roundToPlaces((double) MathUtil.convertUnits(panelWidthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      double convertedMaxPanelWidth = MathUtil.roundToPlaces((double) MathUtil.convertUnits(maxPanelWidthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      double convertedMinPanelWidth = MathUtil.roundToPlaces((double) MathUtil.convertUnits(minPanelWidthInNanometers,
              MathUtilEnum.NANOMETERS, _units),
              MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_WIDTH_INVALID_KEY",
              new Object[]
              {
                convertedPanelWidth,
                _units,
                convertedMinPanelWidth,
                _units,
                convertedMaxPanelWidth,
                _units
              }));

      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringUtil.format(message, 50),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (XrayActuator.isInstalled() == true)
    {
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_XRAY_CYLINDER_WARNING_KEY");

      int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
              message,
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      if (answer == JOptionPane.NO_OPTION)
      {
        return;
      }
    }
    _busyCancelDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LOADING_PANEL_MSG_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
            true);

    _busyCancelDialog.addCancelActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (_panelLoadInProgress)
        {
          try
          {
            _panelHandler.abort();
          }
          catch (XrayTesterException xte)
          {
            handleException(xte);
          }
          closeBusyCancelDialog();
          _panelLoadInProgress = false;
          _panelLoadWasCanceled = true;
        }
        else
        {
          Assert.expect(false);
        }

        closeBusyCancelDialog();
        _panelLoadInProgress = false;
      }
    });

    _busyCancelDialog.pack();
    SwingUtils.centerOnComponent(_busyCancelDialog, MainMenuGui.getInstance());
    _panelLoadWasCanceled = false;
    _panelLoadInProgress = true;

    // if we have made it this far lets now load the panel.
    _hardwareWorkerThread.invokeLater(new Runnable()
    {

      public void run()
      {
        try
        {
          // Must make sure xray tube is not lowered down before bypassing height sensor.
          if (XrayActuator.isInstalled() == true)
          {
            if (XrayCylinderActuator.isInstalled() == true)
            {
              _xrayActuator.up(false);
            }
            else if (XrayCPMotorActuator.isInstalled() == true)
            {
              _xrayActuator.home(false,false);
            }
          }
          // this string does not need to be lcoalized since it is not visible to the user.
          // load Panel with no safety check if Xray Cylinder is installed.

          // comment by Wei Chin
          // should not check the safety sensor (due to it don't lower down the xray tube)
          // Please be aware if it need to run this board in high mag! 
          _panelHandler.loadPanel("Panel for Service Gui", panelWidthInNanometers, panelLengthInNanometers, false, 0);
          if (_panelHandler.isPanelLoaded())
          {
            ImageManager.getInstance().deleteOnlineCachedImages();
          }

          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              populatePanelHandlingStatus();
            }
          });

        }
        catch (XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              _panelLoadInProgress = false;
              closeBusyCancelDialog();
              populatePanelHandlingStatus();
            }
          });
          handleException(xte);
        }
      }
    });
    _busyCancelDialog.setVisible(true);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    // create the button panel
    createButtonPanel();

    // create the center panel
    createRailWidthAndXrayTesterImagePanel();

    // now lets create the south panel - which is the hardware state
    createHardwareStatePanel();

  }

  /**
   * THis method will create all the buttons on the North Side of the panel. Load Panel, Unload Panel, Initialize
   * @author Erica Wheatcroft
   */
  private void createButtonPanel()
  {
    JPanel buttonPanel = new JPanel(new BorderLayout());
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 0, 20));

    // create the initialize button & panel
    JPanel initializeButtonPanel = new JPanel(new FlowLayout());
    initializeButtonPanel.add(_initializeButton);
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    buttonPanel.add(initializeButtonPanel, BorderLayout.EAST);

    // create the panel handling buttons and panel
    JPanel panelHandlingButtonPanel = new JPanel(new FlowLayout());
    _unloadPanelButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNLOAD_PANEL_BUTTON_TEXT_KEY"));
    _loadPanelButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_LOAD_PANEL_BUTTON_TEXT_KEY"));
    panelHandlingButtonPanel.add(_loadPanelButton);
    panelHandlingButtonPanel.add(_unloadPanelButton);
    buttonPanel.add(panelHandlingButtonPanel, BorderLayout.CENTER);

    JPanel loopControlButtonPanel = new JPanel(new FlowLayout());

    _loopControlButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_LOOP_CONTROL_BUTTON_TEXT_KEY"));
    loopControlButtonPanel.add(_loopControlButton);
    buttonPanel.add(loopControlButtonPanel, BorderLayout.WEST);
    // add it to the panel
    add(buttonPanel, BorderLayout.NORTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createHardwareStatePanel()
  {
    JPanel hardwareStatus = new JPanel();
    hardwareStatus.setLayout(new BorderLayout(5, 25));

    JPanel panelInSystemStatusPanel = new JPanel(new FlowLayout());
//    _panelHandlerStateLabel.setFont(FontUtil.getBoldFont(_panelHandlerStateLabel.getFont()));
    panelInSystemStatusPanel.add(_panelHandlerStateLabel);

    hardwareStatus.add(panelInSystemStatusPanel, BorderLayout.NORTH);

    JPanel hardwareSubsystemsStatePanel = new JPanel(new BorderLayout());
    Box hardwareControlMainBox = Box.createHorizontalBox();

    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      {
        hardwareControlMainBox.add(createClearAndSafetyLevelSensorPanel());
      }
      else
      {
        hardwareControlMainBox.add(createPanelClearSensorStatePanel());
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }

    hardwareControlMainBox.add(Box.createHorizontalStrut(15));

    hardwareControlMainBox.add(createBarriersAndClampsPanel());
    hardwareControlMainBox.add(Box.createHorizontalStrut(15));

    hardwareControlMainBox.add(createPanelInPlacePanel());
    hardwareControlMainBox.add(Box.createHorizontalStrut(15));
    
    // Swee Yee Wong - create a panel with outer barriers and motorized height level sensor
    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true && DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
      {
        hardwareControlMainBox.add(createOuterBarriersAndMotorizedHeightLevelSensorPanel());
      }

      else
      {
        hardwareControlMainBox.add(createOuterBarriersPanel());
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }
    
    hardwareControlMainBox.add(Box.createHorizontalStrut(15));

    try
    {
      if (LicenseManager.isVariableMagnificationEnabled() == true)
      { 
        if(XrayCylinderActuator.isInstalled() || XrayCPMotorActuator.isInstalled())
        {
          hardwareControlMainBox.add(createBeltsAndXrayZAxisPanel());
        }
      }
      else
      {
        hardwareControlMainBox.add(createBeltsPanel());
      }
    }
    catch (BusinessException bex)
    {
      //do nothing
    }

    hardwareSubsystemsStatePanel.add(hardwareControlMainBox, BorderLayout.CENTER);

    hardwareStatus.add(hardwareSubsystemsStatePanel, BorderLayout.CENTER);

    JPanel centerPanel = new JPanel(new BorderLayout(5, 30));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 20, 20));
    centerPanel.add(hardwareStatus, BorderLayout.SOUTH);
    add(centerPanel, BorderLayout.SOUTH);

  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createBeltsPanel()
  {
    JPanel beltsPanel = new JPanel(new GridLayout());
    beltsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
            new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_BELTS_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    Box beltsBox = Box.createVerticalBox();

    JPanel beltDirectionPanel = new JPanel(new FlowLayout());
    JPanel innerDirectionPanel = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel directionLabel = new JLabel();
    directionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CURRENT_DIRECTION_KEY"));
    innerDirectionPanel.add(directionLabel);
    _directionStateLabel.setFont(FontUtil.getBoldFont(_directionStateLabel.getFont(), Localization.getLocale()));
    _directionStateLabel.setText(_UNKNOWN);
    innerDirectionPanel.add(_directionStateLabel);
    _changeDirectionButton.setText(_UNKNOWN);
    innerDirectionPanel.add(_changeDirectionButton);
    beltDirectionPanel.add(innerDirectionPanel);
    beltsBox.add(beltDirectionPanel);

    JPanel beltMotionPanel = new JPanel(new FlowLayout());
    JPanel innerBeltMotionPanel = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel motionLabel = new JLabel();
    motionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_MOTION_STATE_KEY"));
    innerBeltMotionPanel.add(motionLabel);
    _motionStateLabel.setFont(FontUtil.getBoldFont(_motionStateLabel.getFont(), Localization.getLocale()));
    _motionStateLabel.setText(_UNKNOWN);
    _changeMotionStateButton.setText(_UNKNOWN);
    innerBeltMotionPanel.add(_motionStateLabel);
    innerBeltMotionPanel.add(_changeMotionStateButton);

    beltMotionPanel.add(innerBeltMotionPanel);
    beltsBox.add(beltMotionPanel);
    beltsPanel.add(beltsBox);
    return beltsPanel;
  }
  
  /**
   * @author Swee Yee Wong
   */
  private Box createOuterBarriersAndMotorizedHeightLevelSensorPanel()
  {
    Box outerBarriersAndMotorizedHeightLevelSensorBox = Box.createVerticalBox();
    JPanel outerBarriersPanel = createOuterBarriersPanel();

    JPanel motorizedHeightLevelSensorPanel = createMotorizedHeightLevelSensorPanel();

    outerBarriersAndMotorizedHeightLevelSensorBox.add(outerBarriersPanel);
    outerBarriersAndMotorizedHeightLevelSensorBox.add(Box.createVerticalStrut(10));
    outerBarriersAndMotorizedHeightLevelSensorBox.add(motorizedHeightLevelSensorPanel);
    return outerBarriersAndMotorizedHeightLevelSensorBox;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createOuterBarriersPanel()
  {
    JPanel outerBarriersPanel = new JPanel(new GridLayout());
    outerBarriersPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(
            Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OUTER_BARRIER_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));

    Box outerBarriersBox = Box.createVerticalBox();
    JPanel rightOuterBarrierPanel = new JPanel();
    JPanel innerRightOuterBarrierPanel = new JPanel(new VerticalFlowLayout(1, 5));
    _rightOuterBarrierLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RIGHT_KEY"));
    innerRightOuterBarrierPanel.add(_rightOuterBarrierLabel);
    _rightOuterBarrierStateLabel.setFont(FontUtil.getBoldFont(_rightOuterBarrierStateLabel.getFont(), Localization.getLocale()));
    innerRightOuterBarrierPanel.add(_rightOuterBarrierStateLabel);

    _rightOuterBarrierStateLabel.setText(_UNKNOWN);
    _rightOuterBarrierCommandButton.setText(_UNKNOWN);
    innerRightOuterBarrierPanel.add(_rightOuterBarrierCommandButton);
    LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
            new Object[]
            {
              0
            });
    _outerRightBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));
    innerRightOuterBarrierPanel.add(_outerRightBarrierTimeLabel);
    rightOuterBarrierPanel.add(innerRightOuterBarrierPanel);
    outerBarriersBox.add(rightOuterBarrierPanel);

    JPanel leftOuterBarrierPanel = new JPanel();
    JPanel innerLeftOuterBarrierPanel = new JPanel(new VerticalFlowLayout(1, 5));
    _leftOuterBarrierLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LEFT_KEY"));
    innerLeftOuterBarrierPanel.add(_leftOuterBarrierLabel);
    _leftOuterBarrierStateLabel.setFont(FontUtil.getBoldFont(_leftOuterBarrierStateLabel.getFont(), Localization.getLocale()));
    _leftOuterBarrierStateLabel.setText(_UNKNOWN);
    innerLeftOuterBarrierPanel.add(_leftOuterBarrierStateLabel);
    _leftOuterBarrierCommandButton.setText(_UNKNOWN);
    localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
            new Object[]
            {
              0
            });
    _outerLeftBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));
    innerLeftOuterBarrierPanel.add(_leftOuterBarrierCommandButton);
    innerLeftOuterBarrierPanel.add(_outerLeftBarrierTimeLabel);
    leftOuterBarrierPanel.add(innerLeftOuterBarrierPanel);
    outerBarriersBox.add(leftOuterBarrierPanel);

    outerBarriersPanel.add(outerBarriersBox);

    return outerBarriersPanel;
  }
  
  /**
   * @author Anthony Fong
   */
  private JPanel createMotorizedHeightLevelSensorPanel()
  {

    JPanel motorizedHeightLevelSensorPanel = new JPanel(new FlowLayout());
    motorizedHeightLevelSensorPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_MOTORIZED_HEIGHT_LEVEL_SENSOR_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    
    JPanel tempPanel = new JPanel(new VerticalFlowLayout(1, 5));
    
    _motorizedHeightLevelSensorStateLabel.setText(_UNKNOWN);
    tempPanel.add(_motorizedHeightLevelSensorStateLabel);
    
    _motorizedHeightLevelSensorUpDownCommandButton.setText(_UNKNOWN);
    tempPanel.add(_motorizedHeightLevelSensorUpDownCommandButton);

    LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
            new Object[]
            {
              0
            });
    _motorizedHeightLevelSensorTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));
    tempPanel.add(_motorizedHeightLevelSensorTimeLabel);

    motorizedHeightLevelSensorPanel.add(tempPanel);

    return motorizedHeightLevelSensorPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createPanelInPlacePanel()
  {
    JPanel panelInPlacePanel = new JPanel(new GridLayout());
    panelInPlacePanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_SENSORS_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));

    JPanel innerLeftPipPanel = new JPanel(new VerticalFlowLayout(1, 5));

    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      _leftLoadingPositionLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftLoadingPositionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      _leftLoadingPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPTICAL_STOP_STAGE_KEY"));

      innerLeftPipPanel.add(_leftLoadingPositionLabel);
      _leftLoadingStateLabel.setText(_UNKNOWN);
      _changeLeftLoadingStateButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPTICAL_STOP_STAGE_MOVE_LEFT_KEY"));
      innerLeftPipPanel.add(_changeLeftLoadingStateButton);
       if (DigitalIo.getInstance().isOpticalStopControllerEndStopperInstalled())
      {
      innerLeftPipPanel.add(new JLabel(" ",SwingConstants.CENTER));

_leftPipPositionLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      innerLeftPipPanel.add(_leftPIPStateLabel);
      _leftPipPositionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        _leftPipPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_KEY"));        
      }
      else
      {
        _leftPipPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_END_STOPPER_KEY"));
      }
      innerLeftPipPanel.add(_leftPipPositionLabel);
      _leftPIPStateLabel.setFont(FontUtil.getBoldFont(_leftPIPStateLabel.getFont(), Localization.getLocale()));
      _leftPIPStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      innerLeftPipPanel.add(_leftPIPStateLabel);
      _leftPIPStateLabel.setText(_UNKNOWN);
      _changeLeftPipStateButton.setText(_UNKNOWN);
      innerLeftPipPanel.add(_changeLeftPipStateButton);
      }
    }
    else
    {
      _leftPipPositionLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      innerLeftPipPanel.add(_leftPIPStateLabel);
      _leftPipPositionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      _leftPipPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_KEY"));
      innerLeftPipPanel.add(_leftPipPositionLabel);
      _leftPIPStateLabel.setFont(FontUtil.getBoldFont(_leftPIPStateLabel.getFont(), Localization.getLocale()));
      _leftPIPStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _leftPIPStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      innerLeftPipPanel.add(_leftPIPStateLabel);
      _leftPIPStateLabel.setText(_UNKNOWN);
      _changeLeftPipStateButton.setText(_UNKNOWN);
      innerLeftPipPanel.add(_changeLeftPipStateButton);
    }

    JPanel leftPipPanel = new JPanel(new FlowLayout());
    leftPipPanel.add(innerLeftPipPanel);

    JPanel jPanel27 = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel leftPIPSensorStateLabel = new JLabel();
    leftPIPSensorStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    leftPIPSensorStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    leftPIPSensorStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_ENGAGED_KEY"));
    jPanel27.add(leftPIPSensorStateLabel);
    _leftPanelInPlaceLabel.setFont(FontUtil.getBoldFont(_leftPanelInPlaceLabel.getFont(), Localization.getLocale()));
    jPanel27.add(_leftPanelInPlaceLabel);

    JPanel jPanel5 = new JPanel(new FlowLayout());
    jPanel5.add(jPanel27);

    Box leftPIPBox = Box.createVerticalBox();
    leftPIPBox.add(leftPipPanel);
    //leftPIPBox.add(Box.createVerticalGlue());
    leftPIPBox.add(jPanel5);

    JPanel leftPanelInPlacePanel = new JPanel(new BorderLayout());
    leftPanelInPlacePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
            new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LEFT_KEY")));
    leftPanelInPlacePanel.add(leftPIPBox, BorderLayout.CENTER);
    Box panelInPlaceBox = Box.createHorizontalBox();
    panelInPlaceBox.add(leftPanelInPlacePanel);
    panelInPlaceBox.add(Box.createHorizontalStrut(10));
    JPanel rightPanelInPlacePanel = new JPanel(new BorderLayout());
    rightPanelInPlacePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
            new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RIGHT_KEY")));
    Box rightPIPBox = Box.createVerticalBox();
    JPanel jPanel6 = new JPanel(new FlowLayout());

    JPanel innerPipPositionPanel = new JPanel(new VerticalFlowLayout(1, 5));
    //XXL Optical Stop Sensor - Anthony (May 2013)
    if (DigitalIo.isOpticalStopControllerInstalled())
    {
      JLabel pipPositionLabel = new JLabel();
      pipPositionLabel.setHorizontalAlignment(SwingConstants.CENTER);
      pipPositionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      pipPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPTICAL_STOP_STAGE_KEY"));
      innerPipPositionPanel.add(pipPositionLabel);


      _rightLoadingPositionStateLabel.setFont(FontUtil.getBoldFont(_rightLoadingPositionStateLabel.getFont(), Localization.getLocale()));
      _rightLoadingPositionStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _rightLoadingPositionStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);

      _changeRightLoadingStateButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPTICAL_STOP_STAGE_MOVE_RIGHT_KEY"));
      innerPipPositionPanel.add(_rightLoadingPositionStateLabel);

      innerPipPositionPanel.add(_changeRightLoadingStateButton);

       if (DigitalIo.getInstance().isOpticalStopControllerEndStopperInstalled())
      {
      innerPipPositionPanel.add(new JLabel(" ",SwingConstants.CENTER));
 
      JLabel pipPositionLabel2 = new JLabel();
      pipPositionLabel2.setHorizontalAlignment(SwingConstants.CENTER);
      pipPositionLabel2.setHorizontalTextPosition(SwingConstants.CENTER);
      
      if (DigitalIo.isOpticalStopControllerEndStopperResumedAsNormalPIP())
      {
        pipPositionLabel2.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_KEY"));
      }
      else
      {
        pipPositionLabel2.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_END_STOPPER_KEY"));
      }
      innerPipPositionPanel.add(pipPositionLabel2);


      _rightPIPPositionStateLabel.setFont(FontUtil.getBoldFont(_rightPIPPositionStateLabel.getFont(), Localization.getLocale()));
      _rightPIPPositionStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _rightPIPPositionStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);

      _changeRightPipStateButton.setText(_UNKNOWN);
      innerPipPositionPanel.add(_rightPIPPositionStateLabel);

      innerPipPositionPanel.add(_changeRightPipStateButton);
 
      }
    }
    else
    {
      JLabel pipPositionLabel = new JLabel();
      pipPositionLabel.setHorizontalAlignment(SwingConstants.CENTER);
      pipPositionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
      pipPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_KEY"));
      innerPipPositionPanel.add(pipPositionLabel);


      _rightPIPPositionStateLabel.setFont(FontUtil.getBoldFont(_rightPIPPositionStateLabel.getFont(), Localization.getLocale()));
      _rightPIPPositionStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
      _rightPIPPositionStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);

      _changeRightPipStateButton.setText(_UNKNOWN);
      innerPipPositionPanel.add(_rightPIPPositionStateLabel);

      innerPipPositionPanel.add(_changeRightPipStateButton);
    }
    jPanel6.add(innerPipPositionPanel);

    rightPIPBox.add(jPanel6);
    JPanel jPanel7 = new JPanel();
    JPanel innerPanelInPlacePanel = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel panelInPlaceLabel = new JLabel();
    panelInPlaceLabel.setHorizontalAlignment(SwingConstants.CENTER);
    panelInPlaceLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    panelInPlaceLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PIP_ENGAGED_KEY"));
    innerPanelInPlacePanel.add(panelInPlaceLabel);
    _rightPIPSensorStateLabel.setFont(FontUtil.getBoldFont(_rightPIPSensorStateLabel.getFont(), Localization.getLocale()));
    _rightPIPSensorStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _rightPIPSensorStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _rightPIPSensorStateLabel.setText(_UNKNOWN);
    innerPanelInPlacePanel.add(_rightPIPSensorStateLabel);

    jPanel7.add(innerPanelInPlacePanel);
    rightPIPBox.add(jPanel7);

    rightPanelInPlacePanel.add(rightPIPBox, BorderLayout.CENTER);
    panelInPlaceBox.add(rightPanelInPlacePanel);
    panelInPlacePanel.add(panelInPlaceBox);
    return panelInPlacePanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private Box createBarriersAndClampsPanel()
  {
    Box barrierAndClampsBox = Box.createVerticalBox();
    JPanel innerBarrierPanel = new JPanel(new FlowLayout());
    innerBarrierPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INNER_BARRIER_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    JPanel tempPanel = new JPanel(new VerticalFlowLayout(1, 5));
//    _innerBarrierStateLabel.setFont(FontUtil.getBoldFont(_innerBarrierStateLabel.getFont()));
    _innerBarrierStateLabel.setText(_UNKNOWN);
    tempPanel.add(_innerBarrierStateLabel);
    _innerBarrierCommandButton.setText(_UNKNOWN);
    tempPanel.add(_innerBarrierCommandButton);
    LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
            new Object[]
            {
              0
            });
    _innerBarrierTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));
    tempPanel.add(_innerBarrierTimeLabel);

    JPanel clampsPanel = new JPanel(new FlowLayout());
    clampsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLAMPS_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    JPanel tempPanel2 = new JPanel(new VerticalFlowLayout(1, 5));
    clampsPanel.add(tempPanel2);
    tempPanel2.add(_clampsStateLabel);
//    _clampsStateLabel.setFont(FontUtil.getBoldFont(_clampsStateLabel.getFont()));
    _clampsStateLabel.setText(_UNKNOWN);
    _clampsCommandButton.setText(_UNKNOWN);
    tempPanel2.add(_clampsCommandButton);
    innerBarrierPanel.add(tempPanel);
    barrierAndClampsBox.add(innerBarrierPanel);
    barrierAndClampsBox.add(Box.createVerticalStrut(10));
    barrierAndClampsBox.add(clampsPanel);
    return barrierAndClampsBox;
  }

  /**
   * @author Anthony Fong
   */
  private Box createClearAndSafetyLevelSensorPanel()
  {
    Box clearAndSafetyLevelBox = Box.createVerticalBox();
    JPanel innerBarrierPanel = createPanelClearSensorStatePanel();


    JPanel clampsPanel = createSafetyLevelSensorStatePanel();

    clearAndSafetyLevelBox.add(innerBarrierPanel);
    clearAndSafetyLevelBox.add(Box.createVerticalStrut(10));
    clearAndSafetyLevelBox.add(clampsPanel);
    return clearAndSafetyLevelBox;
  }
  
  /**
   * @author Anthony Fong
   */
  private Box createBeltsAndXrayZAxisPanel()
  {
    Box beltsAndXrayZAxisBox = Box.createVerticalBox();
    JPanel beltsPanel = createBeltsPanel();


    JPanel xrayZAxisPanel = createXrayZAxisStatePanel();

    beltsAndXrayZAxisBox.add(beltsPanel);
    beltsAndXrayZAxisBox.add(Box.createVerticalStrut(10));
    beltsAndXrayZAxisBox.add(xrayZAxisPanel);
    return beltsAndXrayZAxisBox;
  }
  /**
   * @author Anthony Fong
   */
  private JPanel createSafetyLevelSensorStatePanel()
  {
    JPanel safetyLevelSensorStatePanel = new JPanel(new VerticalFlowLayout(1, 5));
    JPanel rightSafetyLevelSensorStatePanel = new JPanel();
    rightSafetyLevelSensorStatePanel.setLayout(new FlowLayout());
    JPanel innerRightSafetyLevelSensorState = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel rightSafetyLevelSensorState = new JLabel();
    rightSafetyLevelSensorState.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RIGHT_KEY"));
    innerRightSafetyLevelSensorState.add(rightSafetyLevelSensorState);
//    _rightSafetyLevelSensorStateLabel.setFont(FontUtil.getBoldFont(_rightSafetyLevelSensorStateLabel.getFont()));
    _rightSafetyLevelSensorStateLabel.setText(_UNKNOWN);
    innerRightSafetyLevelSensorState.add(_rightSafetyLevelSensorStateLabel);
    rightSafetyLevelSensorStatePanel.add(innerRightSafetyLevelSensorState);
    Box safetyLevelSensorStateBox = Box.createVerticalBox();
    //Box safetyLevelSensorStateBox = Box.createHorizontalBox();
    safetyLevelSensorStateBox.add(rightSafetyLevelSensorStatePanel);
    JPanel leftSafetyLevelSensorStatePanel = new JPanel();
    leftSafetyLevelSensorStatePanel.setLayout(new FlowLayout());
    JPanel innerLeftSafetyLevelSensorStatePanel = new JPanel(new VerticalFlowLayout(1, 5));
    leftSafetyLevelSensorStatePanel.add(innerLeftSafetyLevelSensorStatePanel);

    JLabel leftSafetyLevelSensorState = new JLabel();
    leftSafetyLevelSensorState.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LEFT_KEY"));
    innerLeftSafetyLevelSensorStatePanel.add(leftSafetyLevelSensorState);
//    _leftSafetyLevelSensorStateLabel.setFont(FontUtil.getBoldFont(_leftSafetyLevelSensorStateLabel.getFont()));
    _leftSafetyLevelSensorStateLabel.setText(_UNKNOWN);
    innerLeftSafetyLevelSensorStatePanel.add(_leftSafetyLevelSensorStateLabel);
    safetyLevelSensorStateBox.add(Box.createVerticalStrut(5));
    //safetyLevelSensorStateBox.add(Box.createHorizontalStrut(5));
    safetyLevelSensorStateBox.add(leftSafetyLevelSensorStatePanel);
    safetyLevelSensorStatePanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_SAFETY_LEVEL_SENSOR_STATE_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 5, 10)));
    safetyLevelSensorStatePanel.setLayout(new GridLayout());
    safetyLevelSensorStatePanel.add(safetyLevelSensorStateBox, null);

    return safetyLevelSensorStatePanel;
  }

    /**
   * @author Anthony Fong
   */
  private JPanel createXrayZAxisStatePanel()
  {

    JPanel xrayZAxisPanel = new JPanel(new FlowLayout());
    xrayZAxisPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_XRAY_Z_AXIS_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    JPanel tempPanel = new JPanel(new VerticalFlowLayout(1, 5));
//    _innerBarrierStateLabel.setFont(FontUtil.getBoldFont(_innerBarrierStateLabel.getFont()));
    _xrayZAxisSensorStateLabel.setText(_UNKNOWN);
    tempPanel.add(_xrayZAxisSensorStateLabel);
    
    if(XrayCylinderActuator.isInstalled())
    {
      _xrayZAxisUpToPosition1CommandButton.setText(_UP);
      _xrayZAxisDownCommandButton.setText(_DOWN);
      
      tempPanel.add(_xrayZAxisUpToPosition1CommandButton);
      tempPanel.add(_xrayZAxisDownCommandButton);
    }
    else 
    {
      _xrayZAxisHomeCommandButton.setText(_HOME);
      _xrayZAxisUpToPosition1CommandButton.setText(_UP_POS_1);
      _xrayZAxisUpToPosition2CommandButton.setText(_UP_POS_2);
      _xrayZAxisDownCommandButton.setText(_DOWN);
      
      tempPanel.add(_xrayZAxisHomeCommandButton);
      tempPanel.add(_xrayZAxisUpToPosition1CommandButton);
      tempPanel.add(_xrayZAxisUpToPosition2CommandButton);
      tempPanel.add(_xrayZAxisDownCommandButton);
    }
   
    LocalizedString localizedMessage = new LocalizedString("SERVICE_UI_PANEL_HANDLING_TIME_KEY",
            new Object[]
            {
              0
            });
    _xrayZAxisTimeLabel.setText(StringLocalizer.keyToString(localizedMessage));
    tempPanel.add(_xrayZAxisTimeLabel);

    xrayZAxisPanel.add(tempPanel);

    return xrayZAxisPanel;
  }
  
  /**
   * @author Erica Wheatcroft
   */
  private JPanel createPanelClearSensorStatePanel()
  {
    JPanel panelClearSensorStatePanel = new JPanel(new VerticalFlowLayout(1, 5));
    JPanel rightPanelClearSensorStatePanel = new JPanel();
    rightPanelClearSensorStatePanel.setLayout(new FlowLayout());
    JPanel innerRightPanelClearSensorState = new JPanel(new VerticalFlowLayout(1, 5));
    JLabel rightPanelClearSensorState = new JLabel();
    rightPanelClearSensorState.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RIGHT_KEY"));
    innerRightPanelClearSensorState.add(rightPanelClearSensorState);
//    _rightPanelClearSensorStateLabel.setFont(FontUtil.getBoldFont(_rightPanelClearSensorStateLabel.getFont()));
    _rightPanelClearSensorStateLabel.setText(_UNKNOWN);
    innerRightPanelClearSensorState.add(_rightPanelClearSensorStateLabel);
    rightPanelClearSensorStatePanel.add(innerRightPanelClearSensorState);
    Box panelClearSensorStateBox = Box.createVerticalBox();
    //Box panelClearSensorStateBox = Box.createHorizontalBox();
    panelClearSensorStateBox.add(rightPanelClearSensorStatePanel);
    JPanel leftPanelClearSensorStatePanel = new JPanel();
    leftPanelClearSensorStatePanel.setLayout(new FlowLayout());
    JPanel innerLeftPanelClearSensorStatePanel = new JPanel(new VerticalFlowLayout(1, 5));
    leftPanelClearSensorStatePanel.add(innerLeftPanelClearSensorStatePanel);

    JLabel leftPanelClearSensorState = new JLabel();
    leftPanelClearSensorState.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_LEFT_KEY"));
    innerLeftPanelClearSensorStatePanel.add(leftPanelClearSensorState);
//    _leftPanelClearSensorStateLabel.setFont(FontUtil.getBoldFont(_leftPanelClearSensorStateLabel.getFont()));
    _leftPanelClearSensorStateLabel.setText(_UNKNOWN);
    innerLeftPanelClearSensorStatePanel.add(_leftPanelClearSensorStateLabel);
    panelClearSensorStateBox.add(Box.createVerticalStrut(5));
    //panelClearSensorStateBox.add(Box.createHorizontalStrut(5));
    panelClearSensorStateBox.add(leftPanelClearSensorStatePanel);
    panelClearSensorStatePanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_CLEAR_SENSOR_STATE_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 5, 10)));
    panelClearSensorStatePanel.setLayout(new GridLayout());
    panelClearSensorStatePanel.add(panelClearSensorStateBox, null);

    return panelClearSensorStatePanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createRailWidthAndXrayTesterImagePanel()
  {
    JPanel railWidthAdjustmentPanel = new JPanel(new BorderLayout(0, 15));
    railWidthAdjustmentPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
            StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RAIL_WIDTH_ADJUSTMENT_KEY")), BorderFactory.createEmptyBorder(5, 5, 5, 5)));

    // lets create the manual alignment rail width adjustment panel
    JPanel manualRailWidthAdjustmentPanel = new JPanel(new BorderLayout());
    JLabel setRailWidthAdjustmentLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_SET_MANUAL_ADJUSTMENT_KEY"));
    JPanel setRailWidthAdjustmentLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 0));
    setRailWidthAdjustmentLabelPanel.add(setRailWidthAdjustmentLabel);
    manualRailWidthAdjustmentPanel.add(setRailWidthAdjustmentLabelPanel, BorderLayout.NORTH);

    JPanel innerRailWidthAdjustmentPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
    if (_units == null)
    {
      _units = MathUtilEnum.INCHES;
      _railWidthUnitsLabel.setText(MeasurementUnits.getMeasurementUnitString(_units));
    }
    else
    {
      _railWidthUnitsLabel.setText(MeasurementUnits.getMeasurementUnitString(_units));
    }
    _setRailWidthButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_SET_RAIL_WIDTH_ADJUSTMENT_KEY"));
    _railWidthTextField.setColumns(10);
    innerRailWidthAdjustmentPanel.add(_railWidthTextField);
    innerRailWidthAdjustmentPanel.add(_railWidthUnitsLabel);
    innerRailWidthAdjustmentPanel.add(_setRailWidthButton);
    manualRailWidthAdjustmentPanel.add(innerRailWidthAdjustmentPanel, BorderLayout.CENTER);

    JPanel homeRailsButtonPanel = new JPanel(new FlowLayout());
    homeRailsButtonPanel.add(_homeRailsButton);
    _homeRailsButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_HOME_RAILS_KEY"));
    manualRailWidthAdjustmentPanel.add(homeRailsButtonPanel, BorderLayout.SOUTH);

    JPanel labelPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.LEFT, VerticalFlowLayout.TOP, 5, 0));

    // now lets add the Stage rail home sensor status
    JPanel stageRailHomeSensorStatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _currentStageRailHomeSensorStateLabel.setFont(FontUtil.getBoldFont(_currentRailWidthMeasurementLabel.getFont(), Localization.getLocale()));
    JLabel currentStageRailHomeSensorLabel = new JLabel();
    currentStageRailHomeSensorLabel.setHorizontalTextPosition(JLabel.LEFT);
    currentStageRailHomeSensorLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_HOME_SENSOR_KEY"));
    stageRailHomeSensorStatePanel.add(currentStageRailHomeSensorLabel);
    stageRailHomeSensorStatePanel.add(_currentStageRailHomeSensorStateLabel);
    labelPanel.add(stageRailHomeSensorStatePanel);

    // now lets add the current rail width status
    JPanel currentRailWidthStatusPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _currentRailWidthMeasurementLabel.setFont(FontUtil.getBoldFont(_currentRailWidthMeasurementLabel.getFont(), Localization.getLocale()));
    JLabel currentRailWidthLabel = new JLabel();
    currentRailWidthLabel.setHorizontalTextPosition(JLabel.LEFT);
    currentRailWidthLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CURRENT_RAIL_WIDTH_KEY"));
    currentRailWidthStatusPanel.add(currentRailWidthLabel);
    currentRailWidthStatusPanel.add(_currentRailWidthMeasurementLabel);
    labelPanel.add(currentRailWidthStatusPanel);

    railWidthAdjustmentPanel.add(labelPanel, BorderLayout.NORTH);

    // now add the manual rail width adjustment panel
    railWidthAdjustmentPanel.add(manualRailWidthAdjustmentPanel, BorderLayout.CENTER);

    // lets create the machine view panel
    JPanel outerManualRailWidthAdjustmentPanel = new JPanel(new VerticalFlowLayout(1, 5));
    outerManualRailWidthAdjustmentPanel.add(railWidthAdjustmentPanel);

    JPanel machineViewPanel = new JPanel(new BorderLayout());
    Box box = Box.createHorizontalBox();
    box.add(outerManualRailWidthAdjustmentPanel);
    box.add(Box.createGlue());
    JPanel machineImagePanel = new JPanel(new BorderLayout());
    machineImagePanel.add(_innerMachineImagePanel, BorderLayout.CENTER);
    box.add(machineImagePanel);

    machineViewPanel.add(box, BorderLayout.CENTER);
    add(machineViewPanel, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    // stop any system monitoring
    stopMonitoring();

    // force the system to be re-intialized
    _subsystemInitialized = false;

    // re-populate gui to show subsystem needs to be intialized
    populatePanel();

    // handle the exception
    _parent.handleXrayTesterException(xte);
  }
  
  
  /**
   * @author Ngie Xing
   */
  private void updateXrayTesterImagePanelState()
  {
    if (_subsystemInitialized == false)
    {
      return;
    }
    try
    {
      _innerMachineImagePanel.setBeltDirectionState(_belts.areStageBeltsDirectionLeftToRight());
      _innerMachineImagePanel.setRightOuterBarrierState(_rightOuterBarrier.isOpen());
      _innerMachineImagePanel.setLeftOuterBarrierState(_leftOuterBarrier.isOpen());
      _innerMachineImagePanel.setInnerBarrierState(_innerBarrier.isOpen());
      _innerMachineImagePanel.repaint();
    }
    catch (XrayTesterException xte)
    {
      //do nothing
    }
  }
    
}
