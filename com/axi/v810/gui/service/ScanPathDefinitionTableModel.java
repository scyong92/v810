package com.axi.v810.gui.service;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This tablemodel is used to define all scan passes for a given scan path
 *
 * @author Erica Wheatcroft
 */
public class ScanPathDefinitionTableModel extends DefaultTableModel implements Observer
{
  private java.util.List<ScanPassDefinitionTableEntry> _scanPasses = null;
  private ScanPassDefinitionTableEntry _currentSelectedRowEntry = null;

  private final String[] _columnNames =
      {StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_NUMBER_OF_ENTRIES_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_PASS_DIRECTION_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_X_START_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_Y_START_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_LENGTH_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_MAX_PASS_LENGTH_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_STEP_DIRECTION_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_MAX_STEP_WIDTH_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_UNITS_KEY")};

  private PanelPositioner _panelPositioner = null;

  private MathUtilEnum _units = null;

  /**
   * @author Erica Wheatcroft
   */
  public ScanPathDefinitionTableModel()
  {
    _scanPasses = new ArrayList<ScanPassDefinitionTableEntry>();
    _panelPositioner = PanelPositioner.getInstance();

    // we need to listen to when the user changes display units
    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _columnNames[columnIndex];
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and <code>columnIndex</code> is editable.
   *
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if(_currentSelectedRowEntry == null || _currentSelectedRowEntry.equals(_scanPasses.get(rowIndex)) == false)
      return false;

    switch (columnIndex)
    {
      case 0:
        return false;
      case 1:
        if (rowIndex == 0)
          return true;
        else
          return false;
      case 2:
        return true;
      case 3:
        return true;
      case 4:
        return true;
      case 5:
        return false;
      case 6:
        return true;
      case 7:
        return false;
      case 8:
        return false;
      default:
        Assert.expect(false);
    }

    Assert.expect(false);
    return false;
  }

  /**
   * Sets the value in the cell at <code>columnIndex</code> and <code>rowIndex</code> to <code>aValue</code>.
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(_scanPasses != null);
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    ScanPassDefinitionTableEntry tableEntry = _scanPasses.get(rowIndex);
    switch (columnIndex)
    {
      case 0: // id
        Assert.expect(false); // not editable
        break;
      case 1:
        // scan step direction
        if (rowIndex >= 1)
          Assert.expect(false); // only the first row is editable
        // now if they change this value and they have more rows defined we will have to notify them
        // that this change will invalidate all other rows and ask if they would like to continue with this change
        validateScanPassDirection(aValue, tableEntry);
        break;
      case 2:
        // x start value must be between min and max x start defined by the panel posisitioner
        validateXStartValue(aValue, tableEntry);
        break;
      case 3:
        // y start value must be between min and max y start defined by the panel posisitioner
        validateYStartValue(aValue, tableEntry);
        break;
      case 4:
        // scan pass length
        validateScanPassLengthValue(aValue, tableEntry);
        break;
      case 5:
        // estimated scan pass length
        Assert.expect(false); // not editable
        break;
      case 6:
        // scan step direction
        // if they change the step direction and they have rows following the row they have changed we need
        // to notify them that this change will invalidate and delete all following rows and would like to
        // continue with this change
        validateScanStepDirection(aValue, tableEntry, rowIndex);
        break;
      case 7:
        // estimated scan step size
        Assert.expect(false);
        break;
      case 8:
        // units
        Assert.expect(false);
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void validateScanStepDirection(Object aValue, ScanPassDefinitionTableEntry tableEntry, int rowIndex)
  {
    ScanStepDirectionEnum newDirection = (ScanStepDirectionEnum)aValue;
    ScanStepDirectionEnum originalDirection = tableEntry.getScanStepDirection();

    if (newDirection.equals(originalDirection) == false)
    {
      int numberOfEntries = _scanPasses.size() - 1;
      if (numberOfEntries > rowIndex)
      {
        String message = StringLocalizer.keyToString("SERVICE_UI_STEP_DIRECTION_CHANGED_MSG_KEY");
        message = StringUtil.format(message, 50);

        int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
            message,
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE);

        if (response == JOptionPane.YES_OPTION)
        {
          tableEntry.setScanStepDirection(newDirection);

          _scanPasses = _scanPasses.subList(0, rowIndex + 1);

          fireTableDataChanged();
        }
        else // everything else will be considered as NO
        {
          tableEntry.setScanStepDirection(originalDirection);
        }
      }
      else
      {
        tableEntry.setScanStepDirection((ScanStepDirectionEnum)aValue);
      }
      try
      {
        tableEntry.setEstimatedStepWidthInNanometers(_panelPositioner.getMaximumScanStepWidthInNanometers(tableEntry.
            getXInNanometers(),
            tableEntry.getScanStepDirection()));

      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
        tableEntry.setEstimatedStepWidthInNanometers(0);
      }

      fireTableDataChanged();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void validateScanPassDirection(Object aValue, ScanPassDefinitionTableEntry tableEntry)
  {
    ScanPassDirectionEnum newDirection = (ScanPassDirectionEnum)aValue;
    ScanPassDirectionEnum originalDirection = tableEntry.getScanPassDirection();
    if (newDirection.equals(originalDirection) == false)
    {
      int numberOfEntries = _scanPasses.size();
      if (numberOfEntries > 1)
      {
        String message = StringLocalizer.keyToString("SERVICE_UI_INITIAL_STEP_DIRECTION_CHANGED_MSG_KEY");
        message = StringUtil.format(message, 50);
        int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
            message,
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE);
        if (response == JOptionPane.YES_OPTION)
        {
          tableEntry.setScanPassDirection(newDirection);
          _scanPasses.clear();
          _scanPasses.add(tableEntry);
        }
        else
        {
          // everything else will be considered as NO
          tableEntry.setScanPassDirection(originalDirection);
        }
      }
      else
      {
        // only one row defined.. so set the value and move on
        tableEntry.setScanPassDirection(newDirection);
      }
      try
      {
        int yScanStartPositionLimitInNanometers = _panelPositioner.getYaxisScanStartPositionLimitInNanometers(newDirection);
        // now set this limit so that if the user does not change this value they can run with out getting an assert.
        tableEntry.setYInNanometers(yScanStartPositionLimitInNanometers);
        fireTableDataChanged();
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    String message = xte.getMessage();
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringUtil.format(message, 50),
                                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);

  }

  /**
   * THis method will validate the value entered for the y start posistion.
   * @author Erica Wheatcroft
   */
  private void validateScanPassLengthValue(Object aValue, ScanPassDefinitionTableEntry tableEntry)
  {
    int originalValue = tableEntry.getScanPassLengthInNanometers();

    int convertedValue = 0;
    if (((Number)aValue) instanceof Long && _units.equals(MathUtilEnum.NANOMETERS))
      convertedValue = (int)((Number)aValue).longValue();
    else
      convertedValue = (int) MathUtil.convertUnits( ((Number)aValue).floatValue(), _units, MathUtilEnum.NANOMETERS);

    // now lets verify that the value entered is correct
    try
    {
      // first lets figure out how badly the round from nanometers to the units display is
      int nanometerRoundError = (int) Math.pow(10, - MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

      long scanPathLengthMinValueInNanometers = _panelPositioner.getMinimumScanPassLengthInNanometers();
      long scanPathLengthMaxValueInNanometers = _panelPositioner.getMaximumScanPassLengthInNanometers(tableEntry.getYInNanometers(),
          tableEntry.getScanPassDirection());

      if(MathUtil.fuzzyGreaterThanOrEquals(convertedValue, scanPathLengthMinValueInNanometers, nanometerRoundError) &&
         MathUtil.fuzzyLessThanOrEquals(scanPathLengthMaxValueInNanometers, convertedValue, nanometerRoundError))
      {
        double scanPathMinValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(scanPathLengthMinValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        double scanPathMaxValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(scanPathLengthMaxValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

        // the value entered is not between the min and max defined by the panel positioner

        String message = null;
        if(((Number)aValue) instanceof Long)
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_SCAN_PATH_LENGTH_INVALID_KEY",
                                                                   new Object[]{((Number)aValue).longValue(), _units, scanPathMinValue, _units, scanPathMaxValue, _units}));
        else
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_SCAN_PATH_LENGTH_INVALID_KEY",
                                                                   new Object[]{((Number)aValue).doubleValue(), _units, scanPathMinValue, _units, scanPathMaxValue, _units}));

        // format the message
        message = StringUtil.format(message, 50);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        // set the original value back
        tableEntry.setScanPassLengthInNanometers(originalValue);
      }
      else
      {
        tableEntry.setScanPassLengthInNanometers(convertedValue);
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      tableEntry.setEstimatedPassLengthInNanometers(0);
    }
  }


  /**
   * THis method will validate the value entered for the y start posistion.
   * @author Erica Wheatcroft
   */
  private void validateYStartValue(Object aValue, ScanPassDefinitionTableEntry tableEntry)
  {
    Assert.expect(aValue instanceof Number);
    int originalValue = tableEntry.getYInNanometers();

    int convertedValue = 0;
    if (((Number)aValue) instanceof Long && _units.equals(MathUtilEnum.NANOMETERS))
      convertedValue = (int)((Number)aValue).longValue();
    else
      convertedValue = (int)MathUtil.convertUnits(((Number)aValue).floatValue(), _units, MathUtilEnum.NANOMETERS);

    // now lets verify that the value entered is correct
    try
    {
      ScanPassDirectionEnum direction = tableEntry.getScanPassDirection();
      int yScanStartPositionLimitInNanometers = _panelPositioner.getYaxisScanStartPositionLimitInNanometers(direction);

      // first lets figure out how badly the round from nanometers to the units display is
      int nanometerRoundError = (int) Math.pow(10, - MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

      if( (direction.equals(ScanPassDirectionEnum.FORWARD) &&
                   MathUtil.fuzzyLessThanOrEquals(convertedValue, yScanStartPositionLimitInNanometers, nanometerRoundError)) ||
           (direction.equals(ScanPassDirectionEnum.REVERSE) &&
                   MathUtil.fuzzyGreaterThanOrEquals(convertedValue, yScanStartPositionLimitInNanometers, nanometerRoundError)))
      {
        String message = null;
        double valueConverted = MathUtil.roundToPlaces(
            (double)MathUtil.convertUnits(yScanStartPositionLimitInNanometers, MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        if (direction.equals(ScanPassDirectionEnum.FORWARD))
        {
         if(((Number)aValue) instanceof Long)
           message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_FWD_KEY",
                                                                      new Object[]{((Number)aValue).longValue(), _units, valueConverted, _units}));
         else
           message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_FWD_KEY",
                                                                      new Object[]{((Number)aValue).doubleValue(), _units, valueConverted, _units}));
        }
        else
        {
          if(((Number)aValue) instanceof Long)
            message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_REVERSE_KEY",
                                                                     new Object[]{((Number)aValue).longValue(), _units, valueConverted, _units}));
          else
            message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_REVERSE_KEY",
                                                                     new Object[]{((Number)aValue).doubleValue(), _units, valueConverted, _units}));

        }
        // set the original value back
        tableEntry.setYInNanometers(originalValue);

        tableEntry.setEstimatedPassLengthInNanometers(0);
        tableEntry.setEstimatedStepWidthInNanometers(0);

        message = StringUtil.format(message, 50);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        return;
      }

      int yStartMinValueInNanometers = _panelPositioner.getYaxisMinimumPositionLimitInNanometers();
      int yStartMaxValueInNanometers = _panelPositioner.getYaxisMaximumPositionLimitInNanometers();
      if ((convertedValue >= yStartMinValueInNanometers && convertedValue <= yStartMaxValueInNanometers) == false)
      {
        double yStartMinValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(yStartMinValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        double yStartMaxValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(yStartMaxValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

        // the value entered is not between the min and max defined by the panel positioner

        String message = null;

        if(((Number)aValue) instanceof Long)
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_KEY",
                                                                         new Object[]{((Number)aValue).longValue(), _units, yStartMinValue, _units, yStartMaxValue, _units}));
        else
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_KEY",
                                                                        new Object[]{((Number)aValue).doubleValue(), _units, yStartMinValue, _units, yStartMaxValue, _units}));

        message = StringUtil.format(message, 50);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        // set the original value back
        tableEntry.setYInNanometers(originalValue);
      }
      else
      {
        tableEntry.setYInNanometers(convertedValue);
      }

      tableEntry.setEstimatedPassLengthInNanometers(_panelPositioner.getMaximumScanPassLengthInNanometers(tableEntry.getYInNanometers(),
          tableEntry.getScanPassDirection()));
      tableEntry.setEstimatedStepWidthInNanometers(_panelPositioner.getMaximumScanStepWidthInNanometers(tableEntry.getXInNanometers(),
          tableEntry.getScanStepDirection()));

      fireTableDataChanged();

    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      tableEntry.setEstimatedPassLengthInNanometers(0);
      tableEntry.setEstimatedStepWidthInNanometers(0);
    }
  }


  /**
   * THis method will validate the value entered for the x start posistion.
   * @author Erica Wheatcroft
   */
  private void validateXStartValue(Object aValue, ScanPassDefinitionTableEntry tableEntry)
  {
    Assert.expect(aValue instanceof Number);
    int originalValue = tableEntry.getXInNanometers();
    int convertedValue = 0;
    if(((Number)aValue) instanceof Long && _units.equals(MathUtilEnum.NANOMETERS))
      convertedValue =  (int) ((Number)aValue).longValue();
    else
      convertedValue = (int)MathUtil.convertUnits(((Number)aValue).doubleValue(), _units, MathUtilEnum.NANOMETERS);

    // now lets verify that the value entered is correct
    try
    {
      // first lets figure out how badly the round from nanometers to the units display is
      int nanometerRoundError = (int) Math.pow(10, - MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

      int xStartMinValueInNanometers = _panelPositioner.getXaxisMinimumPositionLimitInNanometers();
      int xStartMaxValueInNanometers = _panelPositioner.getXaxisMaximumPositionLimitInNanometers();

      if(MathUtil.fuzzyGreaterThanOrEquals(convertedValue, xStartMinValueInNanometers, nanometerRoundError) &&
         MathUtil.fuzzyLessThanOrEquals(xStartMaxValueInNanometers, convertedValue, nanometerRoundError))
      {
        double xStartMinValue = MathUtil.roundToPlaces( (double) MathUtil.convertUnits(xStartMinValueInNanometers, MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        double xStartMaxValue = MathUtil.roundToPlaces( (double) MathUtil.convertUnits(xStartMaxValueInNanometers, MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

        // the value entered is not between the min and max defined by the panel positioner
        String message = null;
        if(((Number)aValue) instanceof Long)
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_X_START_INVALID_KEY",
                                                                         new Object[]{((Number)aValue).longValue(), _units, xStartMinValue, _units, xStartMaxValue, _units}));
        else
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_X_START_INVALID_KEY",
                                                                         new Object[]{((Number)aValue).doubleValue(), _units, xStartMinValue, _units, xStartMaxValue, _units}));

        message = StringUtil.format(message, 50);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        // set the original value back
        tableEntry.setXInNanometers(originalValue);
      }
      else
      {
        tableEntry.setXInNanometers(convertedValue);
      }

      tableEntry.setEstimatedPassLengthInNanometers(
          _panelPositioner.getMaximumScanPassLengthInNanometers(tableEntry.getYInNanometers(),
          tableEntry.getScanPassDirection()));

      tableEntry.setEstimatedStepWidthInNanometers(_panelPositioner.getMaximumScanStepWidthInNanometers(tableEntry.getXInNanometers(),
          tableEntry.getScanStepDirection()));

      fireTableDataChanged();

    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      tableEntry.setEstimatedPassLengthInNanometers(0);
      tableEntry.setEstimatedStepWidthInNanometers(0);
    }
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return the number of columns in the model
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_columnNames != null);
    return _columnNames.length;
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return the number of rows in the model
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if (_scanPasses == null)
      return 0;

    return _scanPasses.size();
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
   *
   * @param rowIndex the row whose value is to be queried
   * @param columnIndex the column whose value is to be queried
   * @return the value Object at the specified cell
   * @todo Implement this javax.swing.table.TableModel method
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_scanPasses != null);
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    ScanPassDefinitionTableEntry tableEntry = _scanPasses.get(rowIndex);
    Object returnValue = null;
    switch (columnIndex)
    {
      case 0:
        returnValue = tableEntry.getId() + 1;
        break;
      case 1:
        returnValue = tableEntry.getScanPassDirection();
        break;
      case 2:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getXInNanometers();
        else
          returnValue = MathUtil.roundToPlaces(MathUtil.convertUnits(tableEntry.getXInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 3:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getYInNanometers();
        else
          returnValue = MathUtil.roundToPlaces(MathUtil.convertUnits(tableEntry.getYInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 4:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getScanPassLengthInNanometers();
        else
          returnValue = MathUtil.roundToPlaces(  MathUtil.convertUnits(tableEntry.getScanPassLengthInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 5:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getEstimatedPassLengthInNanometers();
        else
          returnValue = MathUtil.roundToPlaces( MathUtil.convertUnits(tableEntry.getEstimatedPassLengthInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 6:
        returnValue = tableEntry.getScanStepDirection();
        break;
      case 7:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getEstimatedStepWidthInNanometers();
        else
          returnValue = MathUtil.roundToPlaces( MathUtil.convertUnits(tableEntry.getEstimatedStepWidthInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 8:
        returnValue = _units;
        break;
      default:
        Assert.expect(false);
    }

    return returnValue;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ScanPassDefinitionTableEntry insertRow()
  {
    ScanPassDefinitionTableEntry tableEntry = new ScanPassDefinitionTableEntry();
    int numberOfScanPasses = _scanPasses.size();
    tableEntry.setId(numberOfScanPasses);
    _scanPasses.add(tableEntry);
    // now lets figure out what the pass direction should be. if this is the first scan pass to be defined
    // then we will just default to forward. however for every other row the value will be the oposite of the
    // previous row

    ScanPassDefinitionTableEntry previousEntry = null;
    if (numberOfScanPasses > 0)
    {
      previousEntry = _scanPasses.get(numberOfScanPasses - 1);
      if (previousEntry.getScanPassDirection().equals(ScanPassDirectionEnum.FORWARD))
        tableEntry.setScanPassDirection(ScanPassDirectionEnum.REVERSE);
      else
        tableEntry.setScanPassDirection(ScanPassDirectionEnum.FORWARD);
    }

    // now prepopulate the the entry
    try
    {
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
      {
        int xStartMinValueInNanometers = 0;
        tableEntry.setXInNanometers(xStartMinValueInNanometers);

        if (previousEntry == null)
        {
          int yScanStartPositionLimitInNanometers = 0;
          tableEntry.setYInNanometers(yScanStartPositionLimitInNanometers);

          int scanPathLengthMinValueInNanometers = 39583300;
          tableEntry.setScanPassLengthInNanometers(scanPathLengthMinValueInNanometers);
        }
        else
        {
          // if there was a previous row lets prepopulate from the values the user has entered for the previous row
          tableEntry.setYInNanometers(previousEntry.getYInNanometers()
            + (previousEntry.getScanPassDirection().getId() * previousEntry.getScanPassLengthInNanometers()));
          tableEntry.setScanPassLengthInNanometers(previousEntry.getScanPassLengthInNanometers());
        }

        tableEntry.setEstimatedPassLengthInNanometers(611069932);
        tableEntry.setEstimatedStepWidthInNanometers(532650000);
      }
      else
      {
        int xStartMinValueInNanometers = _panelPositioner.getXaxisMinimumPositionLimitInNanometers();
        tableEntry.setXInNanometers(xStartMinValueInNanometers);

        if (previousEntry == null)
        {
          int yScanStartPositionLimitInNanometers = _panelPositioner.getYaxisScanStartPositionLimitInNanometers(tableEntry.getScanPassDirection());
          tableEntry.setYInNanometers(yScanStartPositionLimitInNanometers);

          int scanPathLengthMinValueInNanometers = _panelPositioner.getMinimumScanPassLengthInNanometers();
          tableEntry.setScanPassLengthInNanometers(scanPathLengthMinValueInNanometers);
        }
        else
        {
          // if there was a previous row lets prepopulate from the values the user has entered for the previous row
          tableEntry.setYInNanometers(previousEntry.getYInNanometers()
            + (previousEntry.getScanPassDirection().getId() * previousEntry.getScanPassLengthInNanometers()));
          tableEntry.setScanPassLengthInNanometers(previousEntry.getScanPassLengthInNanometers());
        }

        tableEntry.setEstimatedPassLengthInNanometers(_panelPositioner.getMaximumScanPassLengthInNanometers(tableEntry.
          getYInNanometers(),
          tableEntry.getScanPassDirection()));
        tableEntry.setEstimatedStepWidthInNanometers(_panelPositioner.getMaximumScanStepWidthInNanometers(tableEntry.
          getXInNanometers(),
          tableEntry.getScanStepDirection()));
      }

      fireTableDataChanged();
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }

    fireTableRowsInserted(_scanPasses.size(), _scanPasses.size());
    return tableEntry;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clearSelectedRows(int row)
  {
    Assert.expect(row >= 0);
    _scanPasses.remove(row);

    fireTableRowsDeleted(row, row);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clearAllRows()
  {
    int numberOfRows = _scanPasses.size();
    _scanPasses.clear();

    fireTableRowsDeleted(0, numberOfRows);
  }

  /**
   * @author Erica WHeatcroft
   */
  public synchronized void update(Observable observable, Object arg)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if (arg instanceof GuiEvent)
      {
        GuiEvent event = (GuiEvent)arg;
        GuiEventEnum eventEnum = event.getGuiEventEnum();
        if (eventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)eventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
          {
            Assert.expect(event.getSource() instanceof MathUtilEnum);
            _units = (MathUtilEnum)event.getSource();
            fireTableDataChanged();
          }
        }
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  void setSelectedRowEntry(ScanPassDefinitionTableEntry entry)
  {
    Assert.expect(entry != null);
    _currentSelectedRowEntry = entry;
  }

  /**
   * This method will convert the data in the table model to a list of scan passes so that we can run the defined scan path.
   * @author Erica Wheatcroft
   */
  public java.util.List<ScanPass> getDefinedScanPath()
  {
    java.util.List<ScanPass> _scanPath = new LinkedList<ScanPass>();

    for (ScanPassDefinitionTableEntry entry : _scanPasses)
      _scanPath.add(entry.createScanPassFromEntry());

    return _scanPath;
  }

  /**
   * @author Erica Wheatcroft
   */
  ScanPassDefinitionTableEntry getTableEntryAt(int rowIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _scanPasses.size());
    Assert.expect(_scanPasses != null);

    return _scanPasses.get(rowIndex);
  }

  /**
   * This method will change the units to whatever is sent in. It WILL NOT fire tableDataChangedEvent. This method
   * is intended to change all values to nanometers so it can be written out.
   *
   * @author Erica Wheatcroft
   */
  void changeUnits(MathUtilEnum units)
  {
    Assert.expect(units != null);
    _units = units;
  }

  /**
   * @author Erica Wheatcroft
   */
  MathUtilEnum getCurrentUnits()
  {
    Assert.expect(_units != null);
    return _units;
  }

  /**
   * @author Erica Wheatcroft
   */
  void removeLastEntry()
  {
    Assert.expect(_scanPasses != null);
    if (_scanPasses.size() > 0)
    {
      _scanPasses.remove(_scanPasses.size() - 1);
      fireTableDataChanged();
    }
    else
    {
      Assert.expect(false);
    }
  }
}
