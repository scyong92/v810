package com.axi.v810.gui.service;

/**
 * <p>Title: XraySafetyTestPowerEnum</p>
 * <p>Description: Enumeration of power levels for the Xray Safety Test. These correlate to
 * defined xray power levels</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Agilent Technogies</p>
 * @author George Booth
 */
public class XraySafetyTestPowerEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static XraySafetyTestPowerEnum POWER_OFF = new XraySafetyTestPowerEnum(++_index);
  public static XraySafetyTestPowerEnum LOW_POWER = new XraySafetyTestPowerEnum(++_index);
  public static XraySafetyTestPowerEnum NORMAL_POWER = new XraySafetyTestPowerEnum(++_index);
  public static XraySafetyTestPowerEnum HIGH_POWER = new XraySafetyTestPowerEnum(++_index);

  /**
   * @author George Booth
   */
  private XraySafetyTestPowerEnum(int id)
  {
    super(id);
  }
}
