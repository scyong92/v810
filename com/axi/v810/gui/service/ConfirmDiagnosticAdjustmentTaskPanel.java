package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.cAndD.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.psp.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This panel of the service environment allows the user to run confirmation,
 * diagnostic, & adjustments
 *
 * @author Erica Wheatcroft
 */
public class ConfirmDiagnosticAdjustmentTaskPanel extends AbstractTaskPanel implements Observer
{

  private JPanel _imageAndButtonPanel = new JPanel();
  private JPanel _tablePanel = new JPanel();
  private JPanel _statusPanel = new JPanel();
  private JPanel _innerStatusPanel = new JPanel();
  private JPanel _showFailurePanel = new JPanel();
  private JPanel _resultsTablePanel = new JPanel();
  private BorderLayout _frameBorderLayout = new BorderLayout();
  private BorderLayout _buttonStatusBorderLayout = new BorderLayout();
  private BorderLayout _centerBorderLayout = new BorderLayout();
  private BorderLayout _tableBorderLayeut = new BorderLayout();
  private BorderLayout _resultsTableBorderLayout = new BorderLayout();
  private GridLayout _statusGridLayout = new GridLayout();
  private GridLayout _innerStatusGridLayout = new GridLayout();
  private GridLayout _buttonGridLayout = new GridLayout();
  private JSplitPane _taskTreeButtonPanelResultsPanelSplitPane = new JSplitPane();
  private JButton _runButton = new JButton();
  private JButton _saveTableButton = new JButton();
  private JButton _analyzeTableButton = new JButton();
  private NumericTextField _numberOfLoopsTextField = new NumericTextField();
  private NumericRangePlainDocument _numericRangePlainDocument = new NumericRangePlainDocument();
  private JCheckBox _loopContinuoslyCheckBox = new JCheckBox();
  private JCheckBox _runToFirstFailureCheckBox = new JCheckBox();
  private JScrollPane _testTreeScrollPane = new JScrollPane();
  private JScrollPane _resultsScrollPane = new JScrollPane();
  private JTable _resultsTable = null;
  private JLabel _statusBar = new JLabel();
  private JLabel _versionStatus = new JLabel();
  private JTree _testTree = null;
  private TreeNode _rootNode = null;
  private TitledBorder _resultsTitledBorder;
  private TitledBorder _testsTitledBorder;
  private JPopupMenu _testTreePopupMenu = new JPopupMenu();
  private JMenuItem _enableAllTestsMenuItem = new JMenuItem();
  private JMenuItem _enableTestMenuItem = new JMenuItem();
  private JMenuItem _disableTestMenuItem = new JMenuItem();
  private JMenuItem _enableTestPopupMenuItem = new JMenuItem();
  private JMenuItem _enableAllTestsPopupMenuItem = new JMenuItem();
  private JMenuItem _disableTestPopupMenuItem = new JMenuItem();
  private JMenuItem _runTestPopupMenuItem = new JMenuItem();
  private JMenuItem _runTestWithoutRunningDependentTestsPopupMenuItem = new JMenuItem();
  private JMenuItem _loopTestPopupMenuItem = new JMenuItem();
  private JMenuItem _runTestWithoutRunningDependentTestsMenuItem = new JMenuItem();
  private JPopupMenu _resultTablePopupMenu = new JPopupMenu();
  private JMenuItem _viewActionMenuItem = new JMenuItem();
  private JMenuItem _viewMessageMenuItem = new JMenuItem();
  private JCheckBoxMenuItem _runAdjustmentsAtStartupCheckboxMenuItem = new JCheckBoxMenuItem();
  private TestResultModel _testResultTableModel = null;
  private ProgressDialog _runningTestProgressCancelDialog;
  private boolean _testCancelled = false;
  private boolean _loopContinuosly = false;
  private boolean _runToFirstFailure = true;
  private boolean _executeDependantTasks = true;
  private int _lastPercentDone = 0;
  private int _numberOfLoops = 1;
  private TestNode _currentTest = null;
  private ServiceEnvironmentPanel _parent = null;
  private ListOfManualHardwareTasks _taskListInstance = null;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private HardwareTaskEngine _hardwareTaskEngine = HardwareTaskEngine.getInstance();
  private SwingWorkerThread _swingWorkerThread = SwingWorkerThread.getInstance();
  private OpticalLiveVideoDialog _opticalLiveVideoDialog;
  private FringePatternDialog _fringePatternDialog;
  private boolean _enableCDNABenchmark = false;
  // listeners - i create my listeners here so that i can add and remove them easly.
  private ActionListener _numberOfLoopsTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleLoopCountEntry();
    }
  };
  private FocusListener _numberOfLoopsTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleLoopCountEntry();
    }
  };
  private ActionListener _loopContinuoslyCheckBoxActionlistener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
      if (_loopContinuosly)
      {
        _numberOfLoopsTextField.setEnabled(false);
      }
      else
      {
        _numberOfLoopsTextField.setEnabled(true);
      }

      setCustomizations();
    }
  };
  private ActionListener _runToFirstFailureCheckBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _runToFirstFailure = _runToFirstFailureCheckBox.isSelected();
      //RFH - In order to stop execution of dependent tasks if a failure is seen,
      //the checkbox value needs to be pushed down to the HardwareTasks
      HardwareTask.setStopOnFail(_runToFirstFailure);
    }
  };
  private ActionListener _runButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // the user wants to run all selected tests
      runTests(_testTree.getSelectionPaths(), false, _loopContinuosly);
    }
  };
  private ActionListener _analyzeTableResultsActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt) //throws IOException 
    {        
      new BenchmarkJsonFileAnalysisPanel(MainMenuGui.getInstance(), BenchmarkJsonFileAnalysisPanel.BenchmarkTableModel.getColumnNames(), BenchmarkJsonFileAnalysisPanel.BenchmarkTableModel.getEmptyData());

//      //Use the event dispatch thread for Swing components 
//      EventQueue.invokeLater(new Runnable()
//      {
//        @Override
//        public void run()
//        {
//          new BenchmarkJsonFileAnalysisPanel(MainMenuGui.getInstance(), BenchmarkJsonFileAnalysisPanel.BenchmarkTableModel.getColumnNames(), BenchmarkJsonFileAnalysisPanel.BenchmarkTableModel.getEmptyData());
//        }
//      });
    }
  };
  private ActionListener _saveTableResultsActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt) //throws IOException 
    {    
      JFileChooser fileChooser = new JFileChooser(Directory.getBenchmarkConfirmationLogDir());
      fileChooser.setAcceptAllFileFilterUsed(false);
      fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      FileFilterUtil csvFileFilter = new FileFilterUtil(FileName.getCommaSeparatedValueFileExtension(), StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY"));
      fileChooser.setFileFilter(csvFileFilter);


      int returnVal = fileChooser.showDialog(MainMenuGui.getInstance(),
        StringLocalizer.keyToString("GUI_SAVE_CDNA_TEST_RESULT_KEY"));
      if (returnVal != JFileChooser.APPROVE_OPTION)
      {
        return;
      }
      File selectedFile = fileChooser.getSelectedFile();
      String selectedFileName = selectedFile.getPath();
      String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
      if (seelctedFileExtension.equalsIgnoreCase(FileName.getCommaSeparatedValueFileExtension()) == false)
      {
        selectedFileName += FileName.getCommaSeparatedValueFileExtension();
      }

      try
      {
        // write report
        String reportTitle = StringLocalizer.keyToString("GUI_SAVE_CDNA_TEST_RESULT_KEY");
        TableModelReportWriter reportWriter = new TableModelReportWriter(StringLocalizer.keyToString("CD_CONFIRM_TABLE_MODEL_CSV_REPORT_DELIMITER_KEY"));
        reportWriter.writeCSVFile(selectedFileName, reportTitle, _testResultTableModel);
      }
      catch (IOException e)
      {
        // if I/O error occurs
        e.printStackTrace();
      }
      finally
      {
        // releases any system resources associated with the stream
      }

    }

    public void actionPerformed_FixedWidthTable(ActionEvent evt) //throws IOException 
    {
      //TableModelReportWriter
      int[] colLenghts = new int[_resultsTable.getColumnCount()];
      colLenghts[0] = 10;
      colLenghts[1] = 80;
      colLenghts[2] = 30;
      colLenghts[3] = 255;
      colLenghts[4] = 255;

      BufferedWriter bfw = null;

      JFileChooser fileChooser = new JFileChooser(Directory.getBenchmarkConfirmationLogDir());
      int returnVal = fileChooser.showDialog(MainMenuGui.getInstance(),
        StringLocalizer.keyToString("GUI_SAVE_CDNA_TEST_RESULT_KEY"));
      if (returnVal == JFileChooser.APPROVE_OPTION)
      {
        try
        {
          File file = fileChooser.getSelectedFile();
          bfw = new BufferedWriter(new FileWriter(file));
          //bfw = new BufferedWriter(new FileWriter("C:\\Data.txt"));

          for (int i = 0; i < _resultsTable.getColumnCount(); i++)
          {//first loop is used for titles of each column

            String name = String.valueOf(_resultsTable.getColumnName(i));

            if (name.length() > colLenghts[i])
            {//colLenghts (characters long) is the constant I chose to make each value
              name = name.substring(0, colLenghts[i]);
            }
            else if (name.length() == colLenghts[i])
            {
            }
            else
            {
              String spaces = "";
              int diff = colLenghts[i] - name.length();
              while (diff > 0)
              {
                spaces = spaces + " ";
                diff--;
              }
              name = name.concat(spaces);
            }

            bfw.write(name);
            bfw.write("\t");
          }

          for (int i = 0; i < _resultsTable.getRowCount(); i++)
          {//for all the data in the Jtable excluding column headers
            bfw.newLine();
            for (int j = 0; j < _resultsTable.getColumnCount(); j++)
            {

              if (_resultsTable.getValueAt(i, j) == null)
              {
                //bfw.write("                    ");
                String spaces = "";
                int diff = colLenghts[j];
                while (diff > 0)
                {
                  spaces = spaces + " ";
                  diff--;
                }
                bfw.write(spaces);
                bfw.write("\t");
              }
              else
              {

                String name = String.valueOf((_resultsTable.getValueAt(i, j)));
                name = name.replace("**", "");
                name = name.replace("__", "");
                name = name.replace('\r', ' ');
                name = name.replace('\n', ' ');
                //if (name.contains("("))
                //{
                //  name = name.substring(0, name.indexOf("("));
                //}

                if (name.length() > colLenghts[j])
                {
                  name = name.substring(0, colLenghts[j]);
                }
                else if (name.length() == colLenghts[j])
                {
                }
                else
                {
                  String spaces = "";
                  int diff = colLenghts[j] - name.length();
                  while (diff > 0)
                  {
                    spaces = spaces + " ";
                    diff--;
                  }
                  name = name.concat(spaces);
                }

                bfw.write(name);
                bfw.write("\t");
              }
            }
          }
          if (bfw != null)
          {
            bfw.close();
          }
        }
        catch (IOException e)
        {
          // if I/O error occurs
          e.printStackTrace();
        }
        finally
        {
          // releases any system resources associated with the stream
        }
      }
    }

    public void actionPerformed_FullTableWithoutFormat(ActionEvent evt)
    {
      JFileChooser fileChooser = new JFileChooser();
      int returnVal = fileChooser.showSaveDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION)
      {
        try
        {
          File file = fileChooser.getSelectedFile();
          PrintWriter os = new PrintWriter(file);
          os.println("");
          for (int col = 0; col < _resultsTable.getColumnCount(); col++)
          {
            os.print(_resultsTable.getColumnName(col) + "\t");
          }

          os.println("");
          os.println("");

          for (int i = 0; i < _resultsTable.getRowCount(); i++)
          {
            for (int j = 0; j < _resultsTable.getColumnCount(); j++)
            {
              os.print(_resultsTable.getValueAt(i, j).toString() + "\t");

            }
            os.println("");
          }
          os.close();
          System.out.println("Done!");
        }
        catch (IOException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  };
  private ActionListener _stopButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _testCancelled = true;
    }
  };
  private MouseAdapter _testTreeMouseListener = new MouseAdapter()
  {
    public void mouseReleased(MouseEvent e)
    {
      testTree_mouseReleased(e);
    }

    public void mousePressed(MouseEvent e)
    {
      testTree_mousePressed(e);
    }
  };
  private ActionListener _enableAllTestsPopupMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      enableAllTests();
    }
  };
  private ActionListener _enableTestMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      enableTests(_testTree.getSelectionPaths(), true);
    }
  };
  private ActionListener _disableTestMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      enableTests(_testTree.getSelectionPaths(), false);
    }
  };
  private ActionListener _runTestPopupMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // if the user wants to run once we want to ignore the loop count if its set to something
      // greater than 1
      int savedLoopCount = _numberOfLoops;
      _numberOfLoops = 0;
      runTests(_testTree.getSelectionPaths(), true, false);
      _numberOfLoops = savedLoopCount;
    }
  };
  private ActionListener _loopTestPopupMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      if (_numberOfLoops == 0)
      {
        return;
      }

      runTests(_testTree.getSelectionPaths(), false, _loopContinuosly);
    }
  };
  private ActionListener _runTestWithoutRunningDependentTestsPopupMenuItemMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _executeDependantTasks = false;
      runTests(_testTree.getSelectionPaths(), false, _loopContinuosly);
      _executeDependantTasks = true;
    }
  };
  private ActionListener _runAdjustmentsAtStartupCheckboxMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        HardwareTaskEngine.getInstance().
          setRunAllCalibrationsOnSoftwareStartup(_runAdjustmentsAtStartupCheckboxMenuItem.isSelected());;
      }
      catch (DatastoreException de)
      {
        handleException(de);
      }
    }
  };
  private ActionListener _viewActionMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      int selectedRow = _resultsTable.getSelectedRow();
      Assert.expect(selectedRow >= -1);
      Result selectedResult = _testResultTableModel.getResultAtRow(selectedRow);
      String action = selectedResult.getSuggestedUserActionMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        StringUtil.format(action, 50),
        StringLocalizer.keyToString("CDGUI_SUGGESTION_USER_ACTION_MENU_ITEM_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    }
  };
  private ActionListener _viewMessageMenuItemActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      int selectedRow = _resultsTable.getSelectedRow();
      Assert.expect(selectedRow >= -1);
      Result selectedResult = _testResultTableModel.getResultAtRow(selectedRow);
      String message = selectedResult.getTaskStatusMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        StringUtil.format(message, 50),
        StringLocalizer.keyToString("CDGUI_MESSAGE_MENU_ITEM_KEY"),
        JOptionPane.INFORMATION_MESSAGE);
    }
  };
  private MouseListener _resultsTableMouseListener = new MouseAdapter()
  {
    public void mouseClicked(MouseEvent evt)
    {
    }

    public void mouseReleased(MouseEvent e)
    {
      int selectedRowFromClick = _resultsTable.rowAtPoint(e.getPoint());
      if (selectedRowFromClick > -1)
      {
        if (e.isPopupTrigger())
        {
          // show the popup menu
          if (selectedRowFromClick >= 0)
          {
            _resultsTable.setRowSelectionInterval(selectedRowFromClick, selectedRowFromClick);
            Result selectedResult = _testResultTableModel.getResultAtRow(selectedRowFromClick);
            if (selectedResult.getStatus() != HardwareTaskStatusEnum.PASSED)
            {
              _resultTablePopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
          }
        }
      }
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  public ConfirmDiagnosticAdjustmentTaskPanel(ServiceEnvironmentPanel environmentPanel)
  {
    super(environmentPanel);

    _parent = environmentPanel;

    _enableCDNABenchmark = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK);

    // now lets create the UI components
    try
    {
      createPanel();
    }
    catch(DatastoreException ex)
    {
      //throw ex;
    }    
  } 

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   *
   * @todo Implement this com.axi.v810.gui.mainMenu.TaskPanelInt method
   */
  public void finish()
  {
    // enable all tasks
    enableAllTests();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();

    // stop observing
    stopObserving();

    // unpopulate the panel
    unpopulatePanel();

  }

  /**
   * @author Erica Wheatcroft
   * @edited hee-jihn.chuah XCR-3079: System no respond when execute CDA task by selecting "run test without running dependent test" after clear loop count
   */
  private void handleLoopCountEntry()
  {
    try
    {
      int intValue = _numberOfLoopsTextField.getNumberValue().intValue();
      if (intValue == 0)
      {
        _numberOfLoops = 1;
        _numberOfLoopsTextField.setValue(_numberOfLoops);
      }  
      else
        _numberOfLoops = intValue;
      setCustomizations();
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _numberOfLoopsTextField.setValue(_numberOfLoops);
    }

    _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
    setLoopCountTips();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void unpopulatePanel()
  {
    // save the persistence
    saveUIState();

    // remove listeners
    removeListeners();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    ServiceGuiPersistence persistence = ServiceGuiPersistence.getInstance();
    persistence.runToFirstFailure(_runToFirstFailureCheckBox.isSelected());
    persistence.loopContinuosly(_loopContinuoslyCheckBox.isSelected());
    try
    {
      persistence.setLoopCount(_numberOfLoopsTextField.getNumberValue().intValue());
    }
    catch (ParseException pe)
    {
      // do nothing.
    }
    persistence.setCandDSplitLocation(_taskTreeButtonPanelResultsPanelSplitPane.getDividerLocation());
    persistence.writeSettings();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enableAllTests()
  {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) _rootNode;

    for (Enumeration allNodes = rootNode.preorderEnumeration(); allNodes.hasMoreElements();)
    {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) allNodes.nextElement();
      if (node.getUserObject() instanceof TestNode)
      {
        TestNode testNode = (TestNode) node.getUserObject();
        testNode.getTest().enable();
      }
    }

    revalidate();
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void restoreLastUIState()
  {
    ServiceGuiPersistence persistence = ServiceGuiPersistence.getInstance();
    _runToFirstFailureCheckBox.setSelected(persistence.runToFirstFailure());
    _loopContinuoslyCheckBox.setSelected(persistence.loopContinuosly());
    if (_loopContinuoslyCheckBox.isSelected())
    {
      _numberOfLoopsTextField.setEnabled(false);
    }
    _numberOfLoopsTextField.setValue(persistence.getLoopCount());
    _taskTreeButtonPanelResultsPanelSplitPane.setDividerLocation(persistence.getCandDSplitLocation());
  }

  /**
   * User has requested a task panel change.
   *
   * @return true if task panel can be exited
   * @todo Implement this com.axi.v810.gui.mainMenu.TaskPanelInt method
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   *
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get an instance to the list of tasks we will need to display
    _taskListInstance = ListOfManualHardwareTasks.getInstance();

    // create the tool bar and menu items
    createMenuItems();

    // populate the UI controls
    populatePanel();

    // add observers if necessary
    startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createMenuItems()
  {
    startMenusAndToolBar();

    MainUIMenuBar menuBar = _mainUI.getMainMenuBar();
    JMenu serviceMenu = _parent.getMenu();

    menuBar.addMenuItem(_menuRequesterID, serviceMenu, serviceMenu.getItemCount(), _enableAllTestsMenuItem);
    menuBar.addMenuSeparator(_menuRequesterID, serviceMenu, serviceMenu.getItemCount());
    menuBar.addMenuItem(_menuRequesterID, serviceMenu, serviceMenu.getItemCount(), _runAdjustmentsAtStartupCheckboxMenuItem);
    menuBar.addMenuItem(_menuRequesterID, serviceMenu, serviceMenu.getItemCount(), _runTestWithoutRunningDependentTestsMenuItem);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    HardwareTask.addObserver(this);
    ProgressObservable.getInstance().addObserver(this);
    OpticalLiveVideoObservable.getInstance().addObserver(this);
    FringePatternObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    HardwareTask.deleteObserver(this);
    ProgressObservable.getInstance().deleteObserver(this);
    OpticalLiveVideoObservable.getInstance().deleteObserver(this);
    FringePatternObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    // populate the tree
    populateTaskListTree();

    // now populate the run adjustments at startup checkbox
    try
    {
      _runAdjustmentsAtStartupCheckboxMenuItem.setSelected(HardwareTaskEngine.getInstance().
        areAllCalibrationsRunAtSoftwareStartup());
    }
    catch (DatastoreException de)
    {
      handleException(de);
      _runAdjustmentsAtStartupCheckboxMenuItem.setSelected(false);
    }

    // select the root node
    setCustomizations();

    // add action listeners
    addListeners();

    // load the UI persistence
    restoreLastUIState();

    // setup the table
    setupTable();

    // Have the root node selected
    _testTree.setSelectionPath(new TreePath(_testTree.getModel().getRoot()));

    //RFH - In order to stop execution of dependent tasks if a failure is seen,
    //the checkbox value needs to be pushed down to the HardwareTasks
    HardwareTask.setStopOnFail(_runToFirstFailure);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateTaskListTree()
  {
    HardwareTaskFolder rootFolder = _taskListInstance.getRootFolder();
    Assert.expect(rootFolder != null);

    // create the root node
    _rootNode = new DefaultMutableTreeNode(rootFolder, true);

    // now lets get the list of folders... we will have to traverse through and populate the tree
    java.util.List<HardwareTaskFolder> folders = rootFolder.getListOfFolders();
    Assert.expect(folders != null);

    for (HardwareTaskFolder folder : folders)
    {
      Assert.expect(folder != null);

      java.util.List<HardwareTaskFolder> subFolders = folder.getListOfFolders();
      Assert.expect(subFolders != null);

      java.util.List<HardwareTaskExecutionInterface> tasks = folder.getListOfTasks();
      Assert.expect(tasks != null);

      DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(folder, !subFolders.isEmpty() || !tasks.isEmpty());
      ((DefaultMutableTreeNode) _rootNode).add(childNode);

      // now lets create all the children for this child node.
      // lets start with the folders first.
      parseFolderStructure(subFolders, childNode);

      // lets add the leaf nodes - these are just hardware tasks
      for (HardwareTaskExecutionInterface hardwareTask : tasks)
      {
        // add each hardware task and make it a leaf node
        TestNode testNode = new TestNode(hardwareTask, this);
        DefaultMutableTreeNode child = new DefaultMutableTreeNode(testNode, false);
        ((DefaultMutableTreeNode) childNode).add(child);
      }
    }

    // lets add the leaf nodes - these are just hardware tasks to the root node
    java.util.List<HardwareTaskExecutionInterface> tasks = rootFolder.getListOfTasks();
    Assert.expect(tasks != null);
    for (HardwareTaskExecutionInterface hardwareTask : tasks)
    {
      // add each hardware task and make it a leaf node
      TestNode testNode = new TestNode(hardwareTask, this);
      DefaultMutableTreeNode child = new DefaultMutableTreeNode(testNode, false);
      ((DefaultMutableTreeNode) _rootNode).add(child);
    }

    _testTree = new JTree(_rootNode, true);

    // expand the tree
    expandTree(true);

    // set up all the properties for the tree
    _testTree.setCellRenderer(new TestTreeCellRenderer());
    _testTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    _testTreeScrollPane.getViewport().removeAll();
    _testTreeScrollPane.getViewport().add(_testTree, null);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void parseFolderStructure(java.util.List<HardwareTaskFolder> folders, DefaultMutableTreeNode parent)
  {
    Assert.expect(folders != null);
    Assert.expect(parent != null);

    for (HardwareTaskFolder folder : folders)
    {
      java.util.List<HardwareTaskFolder> subFolders = folder.getListOfFolders();
      Assert.expect(subFolders != null);

      java.util.List<HardwareTaskExecutionInterface> tasks = folder.getListOfTasks();
      Assert.expect(tasks != null);

      DefaultMutableTreeNode child = new DefaultMutableTreeNode(folder, !subFolders.isEmpty() || !tasks.isEmpty());
      ((DefaultMutableTreeNode) parent).add(child);

      // now lets create all the children for this child node.
      // lets start with the folders first.
      parseFolderStructure(subFolders, child);

      // lets add the leaf nodes - these are just hardware tasks
      for (HardwareTaskExecutionInterface hardwareTask : tasks)
      {
        // add each hardware task and make it a leaf node
        TestNode testNode = new TestNode(hardwareTask, this);
        DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(testNode, false);
        ((DefaultMutableTreeNode) child).add(childNode);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void expandTree(boolean expand)
  {
    TreeNode root = (TreeNode) _rootNode;

    // Traverse tree from root
    expandTree(new TreePath(root), expand);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void expandTree(TreePath parent, boolean expand)
  {
    // Traverse children
    TreeNode node = (TreeNode) parent.getLastPathComponent();
    if (node.getChildCount() >= 0)
    {
      for (Enumeration e = node.children(); e.hasMoreElements();)
      {
        TreeNode n = (TreeNode) e.nextElement();
        TreePath path = parent.pathByAddingChild(n);
        expandTree(path, expand);
      }
    }

    // Expansion or collapse must be done bottom-up
    if (expand)
    {
      _testTree.expandPath(parent);
    }
    else
    {
      _testTree.collapsePath(parent);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void createPanel()throws DatastoreException
  {
    setLayout(_frameBorderLayout);
    JPopupMenu.setDefaultLightWeightPopupEnabled(false);
    _testResultTableModel = new TestResultModel();
    _resultsTable = new JTable(_testResultTableModel);
    _resultsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _resultsTable.getTableHeader().setReorderingAllowed(false);

    _resultsTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new java.awt.Color(134, 134, 134)), StringLocalizer.keyToString("SERVICE_UI_CDA_TEST_RESULTS_BORDER_TITLE_KEY"));
    _testsTitledBorder = new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new java.awt.Color(134, 134, 134)), StringLocalizer.keyToString("SERVICE_UI_CDA_TEST_LIST_BORDER_TITLE_KEY"));

    //_statusBar.setBorder(BorderFactory.createLoweredBevelBorder());
    _runButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_MNEMONIC_KEY").charAt(0));
    _runButton.setText(StringLocalizer.keyToString("SERVICE_UI_CDA_RUN_SELECTED_TEST_BUTTON_KEY"));
    _runButton.setIcon(_parent.getStartImage());
    _runButton.setVerticalAlignment(SwingConstants.CENTER);
    _runButton.setHorizontalAlignment(SwingConstants.CENTER);
    _runButton.setIconTextGap(10);

    if (_enableCDNABenchmark)
    {
      //_saveTableButton.setMnemonic(StringLocalizer.keyToString("ATGUI_REMOVE_MNEMONIC_KEY").charAt(0));
      _saveTableButton.setText(StringLocalizer.keyToString("SERVICE_UI_CDA_SAVE_TABLE_BUTTON_KEY"));
      //.setIcon(_parent.getStopImage());
      _saveTableButton.setVerticalAlignment(SwingConstants.CENTER);
      _saveTableButton.setHorizontalAlignment(SwingConstants.CENTER);
      _saveTableButton.setIconTextGap(10);

      _analyzeTableButton.setText(StringLocalizer.keyToString("SERVICE_UI_CDA_ANALYZE_TABLE_BUTTON_KEY"));
      _analyzeTableButton.setVerticalAlignment(SwingConstants.CENTER);
      _analyzeTableButton.setHorizontalAlignment(SwingConstants.CENTER);
      _analyzeTableButton.setIconTextGap(10);
    }
    _tablePanel.setLayout(_tableBorderLayeut);
    _resultsScrollPane.setBorder(null);
    _buttonStatusBorderLayout.setHgap(10);
    _buttonStatusBorderLayout.setVgap(5);
    _centerBorderLayout.setVgap(10);
    _frameBorderLayout.setVgap(5);
    _imageAndButtonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    _testTreeScrollPane.setBorder(_testsTitledBorder);
    _statusPanel.setLayout(_statusGridLayout);
    //_versionStatus.setBorder(BorderFactory.createLoweredBevelBorder());
    _versionStatus.setHorizontalAlignment(SwingConstants.LEFT);
    _versionStatus.setText(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY"));
    _statusGridLayout.setColumns(2);
    _statusGridLayout.setHgap(2);
    _innerStatusPanel.setLayout(_innerStatusGridLayout);
    _enableAllTestsMenuItem.setText(StringLocalizer.keyToString("CDGUI_ENABLE_ALL_TESTS_LABEL_KEY"));
    _runAdjustmentsAtStartupCheckboxMenuItem.setText(StringLocalizer.keyToString("SERVICE_UI_CDAPANEL_RUN_ADJUSTMENTS_AT_STARTUP_KEY"));
    _enableTestPopupMenuItem.setText(StringLocalizer.keyToString("GUI_ENABLE_KEY"));
    _disableTestPopupMenuItem.setText(StringLocalizer.keyToString("CDGUI_DISABLE_TEST_LABEL_KEY"));
    _enableAllTestsPopupMenuItem.setText(StringLocalizer.keyToString("CDGUI_ENABLE_ALL_TESTS_LABEL_KEY"));
    _enableTestMenuItem.setText(StringLocalizer.keyToString("GUI_ENABLE_KEY"));
    _disableTestMenuItem.setText(StringLocalizer.keyToString("CDGUI_DISABLE_TEST_LABEL_KEY"));
    _runTestPopupMenuItem.setText(StringLocalizer.keyToString("CDGUI_RUN_ONCE_MENU_ITEM_KEY"));
    _loopTestPopupMenuItem.setText(StringLocalizer.keyToString("CDGUI_LOOP_BUTTON_KEY"));
    _runTestWithoutRunningDependentTestsPopupMenuItem.setText(StringLocalizer.keyToString("SERVICE_UI_CDAPANEL_RUN_WO_DEPENDENT_TESTS_MENU_ITEM_KEY"));
    _runTestWithoutRunningDependentTestsMenuItem.setText(StringLocalizer.keyToString("SERVICE_UI_CDAPANEL_RUN_WO_DEPENDENT_TESTS_MENU_ITEM_KEY"));
    _tablePanel.setBorder(_resultsTitledBorder);
    _buttonGridLayout.setColumns(1);
    _buttonGridLayout.setRows(4);
    _buttonGridLayout.setVgap(5);

    _resultsTablePanel.setLayout(_resultsTableBorderLayout);
    add(_statusPanel, BorderLayout.SOUTH);
    _statusPanel.add(_statusBar, null);
    _statusPanel.add(_innerStatusPanel, null);
    _innerStatusPanel.add(_versionStatus, null);

    _tablePanel.add(_showFailurePanel, BorderLayout.SOUTH);
    _tablePanel.add(_resultsTablePanel, BorderLayout.CENTER);
    _resultsTablePanel.add(_resultsScrollPane, BorderLayout.CENTER);
    _resultsScrollPane.getViewport().add(_resultsTable, null);

    // create the tree pop up menu
    createPopupMenu(true, false);

    // create the result table pop up menu
    _viewActionMenuItem.setText(StringLocalizer.keyToString("CDGUI_VIEW_ACTION_MENU_ITEM_KEY"));
    _resultTablePopupMenu.add(_viewActionMenuItem);

    _viewMessageMenuItem.setText(StringLocalizer.keyToString("CDGUI_VIEW_MESSAGE_MENU_ITEM_KEY"));
    _resultTablePopupMenu.add(_viewMessageMenuItem);

    _statusBar.setText(XrayTester.getInstance().getMachineDescription());

    _imageAndButtonPanel.setLayout(new BorderLayout(5, 15));
    // create the button panel
    JPanel buttonPanel = new JPanel(new FlowLayout());
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    buttonPanel.add(_runButton);
    if (_enableCDNABenchmark)
    {
      buttonPanel.add(_saveTableButton);
      buttonPanel.add(_analyzeTableButton);
    }

    _imageAndButtonPanel.add(buttonPanel, BorderLayout.CENTER);

    // create the test options panel.
    JPanel testOptionsPanel = new JPanel(new GridLayout(0, 3, 0, 0));
    testOptionsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(0, 5, 0, 5)));

    JPanel numberOfLoopsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 5));
    numberOfLoopsPanel.add(new JLabel(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY")));
    _numberOfLoopsTextField.setColumns(10);
    DecimalFormat format = new DecimalFormat();
    format.setParseIntegerOnly(true);
    _numberOfLoopsTextField.setDocument(_numericRangePlainDocument);
    _numberOfLoopsTextField.setFormat(format);
    _numericRangePlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    _numberOfLoopsTextField.setValue(_numberOfLoops);
    numberOfLoopsPanel.add(_numberOfLoopsTextField);
    testOptionsPanel.add(numberOfLoopsPanel);

    JPanel loopContinuoslyPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    loopContinuoslyPanel.add(_loopContinuoslyCheckBox);
    _loopContinuoslyCheckBox.setText(StringLocalizer.keyToString("CDGUI_LOOP_CONTINUOSLY_KEY"));
    _loopContinuoslyCheckBox.setSelected(false);
    testOptionsPanel.add(loopContinuoslyPanel);

    JPanel runToFirstFailureCheckBoxPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    runToFirstFailureCheckBoxPanel.add(_runToFirstFailureCheckBox);
    _runToFirstFailureCheckBox.setText(StringLocalizer.keyToString("CDGUI_RUN_TO_FIRST_FAILURE_LABEL_KEY"));
    testOptionsPanel.add(runToFirstFailureCheckBoxPanel);

    JPanel testPanel = new JPanel(new FlowLayout());
    testPanel.add(testOptionsPanel);
    _imageAndButtonPanel.add(testPanel, BorderLayout.SOUTH);

    // create the split panes

    // first create the inner split pane that will be between the button panel and the results panel.
    JPanel testOptionsResultPanel = new JPanel(new BorderLayout());
    testOptionsResultPanel.add(_imageAndButtonPanel, BorderLayout.NORTH);
    testOptionsResultPanel.add(_resultsTablePanel, BorderLayout.CENTER);

    // now lets create the main split pane.
    _taskTreeButtonPanelResultsPanelSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
    _taskTreeButtonPanelResultsPanelSplitPane.add(_testTreeScrollPane, JSplitPane.LEFT);
    _taskTreeButtonPanelResultsPanelSplitPane.add(testOptionsResultPanel, JSplitPane.RIGHT);

    add(_taskTreeButtonPanelResultsPanelSplitPane, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPopupMenu(boolean folderMenu, boolean addEnableMenuItem)
  {
    _testTreePopupMenu.removeAll();
    _testTreePopupMenu.add(_runTestPopupMenuItem);
    _testTreePopupMenu.add(_runTestWithoutRunningDependentTestsPopupMenuItem);
    _testTreePopupMenu.addSeparator();
    _testTreePopupMenu.add(_loopTestPopupMenuItem);

    if (folderMenu == false)
    {
      _testTreePopupMenu.addSeparator();

      if (addEnableMenuItem)
      {
        _testTreePopupMenu.removeAll();
        _testTreePopupMenu.add(_enableTestPopupMenuItem);
      }
      else
      {
        _testTreePopupMenu.add(_disableTestPopupMenuItem);
      }
    }
    else
    {
      _testTreePopupMenu.addSeparator();
    }

    _testTreePopupMenu.add(_enableAllTestsPopupMenuItem);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _runButton.addActionListener(_runButtonActionListener);
    if (_enableCDNABenchmark)
    {
      _saveTableButton.addActionListener(_saveTableResultsActionPerformed);
      _analyzeTableButton.addActionListener(_analyzeTableResultsActionPerformed);
    }
    _testTree.addMouseListener(_testTreeMouseListener);
    _enableAllTestsMenuItem.addActionListener(_enableAllTestsPopupMenuItemActionListener);
    _enableAllTestsPopupMenuItem.addActionListener(_enableAllTestsPopupMenuItemActionListener);
    _runTestPopupMenuItem.addActionListener(_runTestPopupMenuItemActionListener);
    _runToFirstFailureCheckBox.addActionListener(_runToFirstFailureCheckBoxActionListener);
    _loopContinuoslyCheckBox.addActionListener(_loopContinuoslyCheckBoxActionlistener);
    _numberOfLoopsTextField.addFocusListener(_numberOfLoopsTextFieldListener);
    _numberOfLoopsTextField.addActionListener(_numberOfLoopsTextFieldActionListener);
    _loopTestPopupMenuItem.addActionListener(_loopTestPopupMenuItemActionListener);
    _enableTestPopupMenuItem.addActionListener(_enableTestMenuItemActionListener);
    _disableTestPopupMenuItem.addActionListener(_disableTestMenuItemActionListener);
    _enableTestMenuItem.addActionListener(_enableTestMenuItemActionListener);
    _disableTestMenuItem.addActionListener(_disableTestMenuItemActionListener);
    _runTestWithoutRunningDependentTestsPopupMenuItem.addActionListener(_runTestWithoutRunningDependentTestsPopupMenuItemMenuItemActionListener);
    _runTestWithoutRunningDependentTestsMenuItem.addActionListener(_runTestWithoutRunningDependentTestsPopupMenuItemMenuItemActionListener);
    _runAdjustmentsAtStartupCheckboxMenuItem.addActionListener(_runAdjustmentsAtStartupCheckboxMenuItemActionListener);
    _resultsTable.addMouseListener(_resultsTableMouseListener);
    _viewActionMenuItem.addActionListener(_viewActionMenuItemActionListener);
    _viewMessageMenuItem.addActionListener(_viewMessageMenuItemActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _runButton.removeActionListener(_runButtonActionListener);
    if (_enableCDNABenchmark)
    {
      _saveTableButton.removeActionListener(_saveTableResultsActionPerformed);
      _analyzeTableButton.removeActionListener(_analyzeTableResultsActionPerformed);
    }
    _testTree.removeMouseListener(_testTreeMouseListener);
    _enableAllTestsMenuItem.removeActionListener(_enableAllTestsPopupMenuItemActionListener);
    _enableAllTestsPopupMenuItem.removeActionListener(_enableAllTestsPopupMenuItemActionListener);
    _runTestPopupMenuItem.removeActionListener(_runTestPopupMenuItemActionListener);
    _runToFirstFailureCheckBox.removeActionListener(_runToFirstFailureCheckBoxActionListener);
    _loopContinuoslyCheckBox.removeActionListener(_loopContinuoslyCheckBoxActionlistener);
    _numberOfLoopsTextField.removeFocusListener(_numberOfLoopsTextFieldListener);
    _numberOfLoopsTextField.removeActionListener(_numberOfLoopsTextFieldActionListener);
    _loopTestPopupMenuItem.removeActionListener(_loopTestPopupMenuItemActionListener);
    _enableTestMenuItem.removeActionListener(_enableTestMenuItemActionListener);
    _disableTestMenuItem.removeActionListener(_disableTestMenuItemActionListener);
    _enableTestPopupMenuItem.removeActionListener(_enableTestMenuItemActionListener);
    _disableTestPopupMenuItem.removeActionListener(_disableTestMenuItemActionListener);
    _runTestWithoutRunningDependentTestsPopupMenuItem.removeActionListener(_runTestWithoutRunningDependentTestsPopupMenuItemMenuItemActionListener);
    _runTestWithoutRunningDependentTestsMenuItem.removeActionListener(_runTestWithoutRunningDependentTestsPopupMenuItemMenuItemActionListener);
    _runAdjustmentsAtStartupCheckboxMenuItem.removeActionListener(_runAdjustmentsAtStartupCheckboxMenuItemActionListener);
    _resultsTable.removeMouseListener(_resultsTableMouseListener);
    _viewActionMenuItem.removeActionListener(_viewActionMenuItemActionListener);
    _viewMessageMenuItem.removeActionListener(_viewMessageMenuItemActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enableTests(TreePath[] selectedNodes, boolean enable)
  {
    Assert.expect(selectedNodes != null);
    for (TreePath treePath : selectedNodes)
    {
      TreeNode treeNode = (TreeNode) treePath.getLastPathComponent();
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeNode;

      // now lets check to see if the user has selected a test or a folder
      if (treeNode.isLeaf())
      {
        Assert.expect(node.getUserObject() instanceof TestNode);
        if (enable)
        {
          ((TestNode) node.getUserObject()).getTest().enable();
        }
        else
        {
          ((TestNode) node.getUserObject()).getTest().disable();
        }
      }
      else
      {
        // do nothing who cares you can't enable / disable folders :)
      }
    }
    revalidate();
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void clearTaskState()
  {
    DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) _rootNode;

    for (Enumeration allNodes = rootNode.preorderEnumeration(); allNodes.hasMoreElements();)
    {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) allNodes.nextElement();
      if (node.getUserObject() instanceof TestNode)
      {
        TestNode testNode = (TestNode) node.getUserObject();
        testNode.getTest().clearTaskStatus();
      }
    }

    revalidate();
    repaint();
  }

  /**
   * This method will exectue all selected nodes defined by the array of tree
   * paths.
   *
   * @author Erica Wheatcroft
   */
  private void runTests(final TreePath[] selectedNodes, final boolean runOnce, final boolean loopContinuosly)
  {
    Assert.expect(selectedNodes != null);

    // first lets check to make sure we have done an startup on the hardware
    try
    {
      _mainUI.performStartupIfNeeded();
      if (XrayTester.getInstance().isStartupRequired())
      {
        return;
      }
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte); // XCR1144, Chnee Khang Wah, 15-Feb-2011
      return;
    }

    _testCancelled = false;
    // clear the table model we will have new results
    _testResultTableModel.clearCompletedTests();

    // now clear out the task state
    clearTaskState();
    // configure the table properly
    setupTable();

    // RFH - Try to save the state of the system
    try
    {
      _hardwareTaskEngine.saveSystemState();
    }
    // RFH - If there is a problem, display it and don't continue
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte); // XCR1144, Chnee Khang Wah, 15-Feb-2011
      return;
    }

    // set up the progress cancel dialog
    openProgressCancelDialog();
//    System.out.println("Beginning a new TEST RUN");
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // lets disable the UI
          enableUiControls(true);

          // lets loop for a finite #
          if (loopContinuosly == false && _numberOfLoops > 1)
          {
            if (_runToFirstFailure == false)
            {
              for (int loopCount = 0; loopCount < _numberOfLoops; loopCount++)
              {
                traverseTreeAndExecuteTests(selectedNodes, loopCount);
              }
              // enable the controls
              enableUiControls(false);
            }
            else
            {
              boolean allTestsPassed = true;
              for (int loopCount = 0; loopCount < _numberOfLoops && allTestsPassed; loopCount++)
              {
                allTestsPassed = traverseTreeAndExecuteTests(selectedNodes, loopCount);
              }
              // enable the controls
              enableUiControls(false);
            }

            return;
          }
          else if (loopContinuosly)
          {
            boolean allTestsPassed = true;

            int loopCount = 0;
            // loop continously
            while (_testCancelled == false && ((_runToFirstFailure == false) || (_runToFirstFailure && allTestsPassed)))
            {
              allTestsPassed = traverseTreeAndExecuteTests(selectedNodes, loopCount);
              loopCount++;
            }

            // enable the controls
            enableUiControls(false);

          }
          else if (runOnce || _numberOfLoops == 1)
          {
            // just run the test once.
            traverseTreeAndExecuteTests(selectedNodes, 0);

            // enable the controls
            enableUiControls(false);
            return;
          }
        }
        catch (XrayTesterException xte)
        {
          if(xte instanceof PanelInterferesWithAdjustmentAndConfirmationTaskException)
            Assert.expectWithErrorPrompt(false, ErrorHandlerEnum.HW_COMM_SET_PIP_HWL_CONFIRM_PANEL_INSIDE);
          
          handleXrayTesterException(xte); // XCR1144, Chnee Khang Wah, 15-Feb-2011

          // enable the controls
          enableUiControls(false);

          return;
        }
        finally
        {
          // RFH - Try to restore the state of the system
          try
          {
            _hardwareTaskEngine.restoreSystemState();
          }
          // RFH - If there is a problem, display it and don't continue
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte); // XCR1144, Chnee Khang Wah, 15-Feb-2011
          }

          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              if (_runningTestProgressCancelDialog != null)
              {
                // wait until visible
                while (_runningTestProgressCancelDialog.isVisible() == false)
                {
                  try
                  {
                    Thread.sleep(200);
                  }
                  catch (InterruptedException ex)
                  {
                    // do nothing
                  }
                }
                _runningTestProgressCancelDialog.dispose();
                _runningTestProgressCancelDialog = null;
              }
            }
          });
        }
      }
    });

    _runningTestProgressCancelDialog.setVisible(true);
  }

  /**
   * @author Erica Wheatcroft
   */
  private boolean traverseTreeAndExecuteTests(TreePath[] selectedNodes, int loopCount) throws XrayTesterException
  {
    Assert.expect(selectedNodes != null);
//    System.out.println("    Running test");
    for (TreePath path : selectedNodes)
    {
      if (_testCancelled == false)
      {
        TreeNode treeNode = (TreeNode) path.getLastPathComponent();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeNode;
        boolean isTestByPassDependantTasks = false;

        // now lets check to see if the user has selected a test or a folder
        if (treeNode.isLeaf())
        {
          Assert.expect(node.getUserObject() instanceof TestNode);
          _currentTest = ((TestNode) node.getUserObject());

          // CR33963 fix by LeeHerng - Sometimes we want to bypass dependant tasks for some tests.
          // This is determined by the tests itself.
          HardwareTaskExecutionInterface hardwareTaskExecutionInterface = _currentTest.getTest();
          if (hardwareTaskExecutionInterface instanceof HardwareTask)
          {
            HardwareTask hardwareTask = (HardwareTask) hardwareTaskExecutionInterface;
            isTestByPassDependantTasks = hardwareTask.isByPassDependantTasks();
            hardwareTask.resetTaskTimer();
            hardwareTask.setLoopCount(loopCount);
          }

          if (_executeDependantTasks && isTestByPassDependantTasks == false)
          {
            //RFH - In order to stop if a dependent task fails, capture the state
            //and return early if it failed *and* _runToFirstFailure is true
            boolean status = _currentTest.getTest().executeDependentTasks(loopCount);

            //RFH - If a dependent task failed *and* _runToFirstFailure is true, bail out
            if (_runToFirstFailure && status == false)
            {
              return false;
            }
          }
          boolean testPassed = _currentTest.execute();
          if (testPassed == false && _runToFirstFailure)
          {
            return false;
          }
        }
        else
        {
          // the user wants to execute a folder - so we need to exectute all the children
          Assert.expect(node.getUserObject() instanceof HardwareTaskFolder);
          return executeFolder(node, loopCount);
        }
      }
      else
      {
        break;
      }
    }
    return true;
  }

  /**
   * @author Erica Wheatcroft
   * @author Reid Hayhow
   */
  private boolean executeFolder(DefaultMutableTreeNode parentNode, int loopCount) throws XrayTesterException
  {
    Assert.expect(parentNode != null);
    int numberOfChildren = parentNode.getChildCount();
    boolean testPassed = true;
    boolean haveRunDependentTasks = false;

    for (int count = 0; count < numberOfChildren && _testCancelled == false; count++)
    {
      DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) parentNode.getChildAt(count);
      if (childNode.getUserObject() instanceof TestNode)
      {
        // execute the task
        _currentTest = ((TestNode) childNode.getUserObject());

        HardwareTaskExecutionInterface hardwareTaskExecutionInterface = _currentTest.getTest();

        if (hardwareTaskExecutionInterface instanceof HardwareTask)
        {
          HardwareTask hardwareTask = (HardwareTask) hardwareTaskExecutionInterface;
          hardwareTask.resetTaskTimer();
          hardwareTask.setLoopCount(loopCount);
        }

        // Run dependent tasks for folders, but do it in a special way. By executing dependent
        // tasks for the first task we should be able to avoid any strange behaviors.
        if (_executeDependantTasks && haveRunDependentTasks == false)
        {
          // Run the dependencies for the first task in the list
          testPassed = _currentTest.getTest().executeDependentTasks(loopCount);

          // OK, everything should be up to date for all the other tasks
          haveRunDependentTasks = true;

          // If run to first failure is set AND the task failed, bail out
          if (_runToFirstFailure && testPassed == false)
          {
            return testPassed;
          }
        }

        boolean status = _currentTest.execute();
        testPassed = testPassed && status;
        if (_runToFirstFailure && testPassed == false)
        {
          return testPassed;
        }
      }
      else if ((childNode.getUserObject() instanceof HardwareTaskFolder))
      {
        // recurse the folder
        executeFolder(childNode, loopCount);
      }
      else
      {
        Assert.expect(false);
      }
    }
    return testPassed;
  }

  /**
   * Called after frame has been constructed. This sets up some user interface
   * customizations all in one place
   *
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  void setCustomizations()
  {
    setLoopCountTips();

    repaint(); // neccessary to make all the text visible in the controls.  Mainly for the simulation indicator.
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void testTree_mouseReleased(MouseEvent e)
  {
    if (e.isPopupTrigger() && _testTree.isEnabled())
    {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) _testTree.getLastSelectedPathComponent();

      if (node == null)
      {
        return;
      }

      // if not is a test node then set the enable / disable properly.
      if (node.getUserObject() instanceof TestNode)
      {
        TestNode test = (TestNode) node.getUserObject();
        if (test.getTest().isEnabled())
        {
          // if the test is enabled then remove the enabled key and show only the disable key
          createPopupMenu(false, false);
        }
        else
        {
          // if the test is disabled then remove the disabled key and show only the enable key
          createPopupMenu(false, true);
        }
      }
      else
      {
        createPopupMenu(true, true);
      }

      // now show the pop up menu
      _testTreePopupMenu.show(e.getComponent(), e.getX(), e.getY());

    }
  }

  /**
   * Turns off the run button, the loop button and disables the tree control.
   * Also enables the cancel button
   *
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  void enableUiControls(final boolean testRunning)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        _runButton.setEnabled(!testRunning);
        if (_enableCDNABenchmark)
        {
          _saveTableButton.setEnabled(!testRunning);
          _analyzeTableButton.setEnabled(!testRunning);
        }
        _testTree.setEnabled(!testRunning);
        _resultsTable.setEnabled(!testRunning);
        _numberOfLoopsTextField.setEnabled(!testRunning);
        _loopContinuoslyCheckBox.setEnabled(!testRunning);
        _runToFirstFailureCheckBox.setEnabled(!testRunning);

        if (testRunning == false)
        {
          if (_loopContinuosly)
          {
            _numberOfLoopsTextField.setEnabled(false);
          }
          else
          {
            _numberOfLoopsTextField.setEnabled(true);
          }
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void setLoopCountTips()
  {
    if (_loopContinuosly)
    {
      _versionStatus.setText(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY"));
      _loopTestPopupMenuItem.setText(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY"));
    }
    else
    {
      _versionStatus.setText(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY") + " "
        + _numberOfLoops);
      _loopTestPopupMenuItem.setText(StringLocalizer.keyToString("CDGUI_LOOP_BUTTON_KEY") + " "
        + _numberOfLoops + " " + StringLocalizer.keyToString("CDGUI_TIMES_KEY"));
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupTable()
  {
    // column 1 configurations
    TableColumn testResultNumberColumn = _resultsTable.getColumnModel().getColumn(0);
    ResultTableCellRenderer testResultNumberColumnCenterRenderer = new ResultTableCellRenderer();
    testResultNumberColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    testResultNumberColumn.setCellRenderer(testResultNumberColumnCenterRenderer);

    // column 2 configurations
    TableColumn testResultNameColumn = _resultsTable.getColumnModel().getColumn(0);
    ResultTableCellRenderer testResultNameColumnColumnCenterRenderer = new ResultTableCellRenderer();
    testResultNameColumnColumnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
    testResultNameColumn.setCellRenderer(testResultNameColumnColumnCenterRenderer);

    // column 3 configurations  NOTE: we only want to show icons for the status
    TableColumn testResultStatusColumn = _resultsTable.getColumnModel().getColumn(2);
    ResultTableCellRenderer testResultStatusColumnCenterRenderer = new ResultTableCellRenderer();
    testResultStatusColumnCenterRenderer.renderIconsOnly();
    testResultStatusColumn.setCellRenderer(testResultStatusColumnCenterRenderer);

    // column 4 configurations for status messages
    TableColumn testResultMessageColumn = _resultsTable.getColumnModel().getColumn(3);
    ResultTableCellRenderer testResultMessageColumnCellRenderer = new ResultTableCellRenderer();
    testResultMessageColumnCellRenderer.renderErrorMessage();
    testResultMessageColumnCellRenderer.renderToolTips();
    testResultMessageColumn.setCellRenderer(testResultMessageColumnCellRenderer);

    // column 5 for action messages
    TableColumn testResultActionColumn = _resultsTable.getColumnModel().getColumn(4);
    ResultTableCellRenderer testResultActionColumnCellRenderer = new ResultTableCellRenderer();
    testResultActionColumnCellRenderer.renderToolTips();
    testResultActionColumn.setCellRenderer(testResultActionColumnCellRenderer);

    // now center the column headers
    ((DefaultTableCellRenderer) _resultsTable.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_resultsTable, true);

    _resultsTable.getTableHeader().setReorderingAllowed(false);

    // now lets set up the % of relestate for each column.
    // set up column widths
    int iTotalColumnWidth = _resultsTable.getColumnModel().getTotalColumnWidth();
    _resultsTable.getColumnModel().getColumn(0).setPreferredWidth((int) (iTotalColumnWidth * .05));
    _resultsTable.getColumnModel().getColumn(1).setPreferredWidth((int) (iTotalColumnWidth * .50));
    if (_enableCDNABenchmark)
    {
      _resultsTable.getColumnModel().getColumn(2).setPreferredWidth((int) (iTotalColumnWidth * .25));
    }
    else
    {
      _resultsTable.getColumnModel().getColumn(2).setPreferredWidth((int) (iTotalColumnWidth * .10));
    }
    _resultsTable.getColumnModel().getColumn(3).setPreferredWidth((int) (iTotalColumnWidth * .50));
    _resultsTable.getColumnModel().getColumn(4).setPreferredWidth((int) (iTotalColumnWidth * .50));
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void testTree_mousePressed(MouseEvent e)
  {
    if (_testTree.isEnabled() == false)
    {
      return;
    }
    TreePath path = _testTree.getPathForLocation(e.getX(), e.getY());
    if (path == null)
    {
      return;
    }
    _testTree.setSelectionPath(path);

  }

  /**
   * This method should not be called. If you want to handle the exception call
   * handleXrayTesterException. That method will handle the swing thread.
   *
   * @author Erica Wheatcroft XCR1144, Chnee Khang Wah, 15-Feb-2011
   */
  private void showException(XrayTesterException xte)
  {
    String messageToDisplay = StringUtil.format(xte.getMessage(), 50);
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
      messageToDisplay,
      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
      JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Erica Wheatcroft XCR1144, Chnee Khang Wah, 15-Feb-2011
   */
  synchronized void handleXrayTesterException(final XrayTesterException xte)
  {
    if (SwingUtilities.isEventDispatchThread())
    {
      showException(xte);

      if (xte instanceof PanelPositionerException)
      {
        MainMenuGui.getInstance().handleXrayTesterException(xte);
      }
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          handleXrayTesterException(xte);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void handleException(final Throwable exception)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        String messageToDisplay = StringUtil.format(exception.getLocalizedMessage(), 50);
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
          messageToDisplay,
          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          JOptionPane.ERROR_MESSAGE);
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  void openProgressCancelDialog()
  {
    _testCancelled = false;

    _runningTestProgressCancelDialog = new ProgressDialog(
      MainMenuGui.getInstance(),
      StringLocalizer.keyToString("SERVICE_UI_CDA_RUNNING_TESTS_MESSAGE_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_CDA_RUNNING_SELECTED_TESTS_MESSAGE_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_CDA_TESTING_COMPLETED_MESSAGE_KEY"),
      StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
      StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
      StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
      0, 100, true);

    _runningTestProgressCancelDialog.addCancelActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (_runningTestProgressCancelDialog != null)
        {
          String reporterAction = StringLocalizer.keyToString("SERVICE_UI_CDA_CANCEL_TESTING_MESSAGE_KEY");
          _runningTestProgressCancelDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
        }
        _currentTest.cancel();
        _testCancelled = true;
      }
    });

    _runningTestProgressCancelDialog.pack();
    _runningTestProgressCancelDialog.setSize(new Dimension(450, 175));
    SwingUtils.centerOnComponent(_runningTestProgressCancelDialog, MainMenuGui.getInstance());
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        if (observable instanceof ProgressObservable)
        {
          if (arg instanceof ProgressReport)
          {
            ProgressReport progressReport = (ProgressReport) arg;
            int percentDone = progressReport.getPercentDone();
            // don't indicate 100% until event actually completes
            if (percentDone == 100)
            {
              percentDone = 99;
            }
            if (_testCancelled == false)
            {
              _lastPercentDone = percentDone;
            }
            String reporterAction = progressReport.getAction();
            if (_testCancelled)
            {
              reporterAction = StringLocalizer.keyToString("SERVICE_UI_CDA_CANCEL_TESTING_MESSAGE_KEY");
            }

            if (_runningTestProgressCancelDialog != null)
            {
              _runningTestProgressCancelDialog.updateProgressBarPrecent(_lastPercentDone, reporterAction);
            }
          }
        }
        else if (observable instanceof HardwareTaskObservable)
        {
          Assert.expect(arg != null);
          Assert.expect(arg instanceof Result);

          // check to see if the result is passing or failing
          _testResultTableModel.populateTable((Result) arg);

          Rectangle rect = _resultsTable.getCellRect(_resultsTable.getRowCount(), 0, true);
          _resultsTable.scrollRectToVisible(rect);
        }
        else if (observable instanceof OpticalLiveVideoObservable)
        {
          if (arg instanceof OpticalLiveVideoInfo)
          {
            OpticalLiveVideoInfo opticalLiveVideoInfo = (OpticalLiveVideoInfo) arg;
            OpticalLiveVideoEnum opticalLiveVideoEnum = opticalLiveVideoInfo.getOpticalLiveVideoEnum();
            if (opticalLiveVideoEnum.equals(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO))
            {
                OpticalCameraIdEnum cameraid = opticalLiveVideoInfo.getCameraId();
                try
                {
                  String dialogTitle = StringLocalizer.keyToString("PSP_LIVE_VIDEO_KEY") + cameraid.getId();
                  _opticalLiveVideoDialog = new OpticalLiveVideoDialog(_mainUI, 
                                                                       dialogTitle, 
                                                                       cameraid,
                                                                       opticalLiveVideoInfo.getActionButtonText(),
                                                                       opticalLiveVideoInfo.getOpticalLiveVideoInteractionInterface(),
                                                                       opticalLiveVideoInfo.isDisplayFringePatternPanel(),
                                                                       opticalLiveVideoInfo.isDisplayGridPanel(),
                                                                       opticalLiveVideoInfo.isAllowSurfaceMapPointSelection());
                  SwingUtils.centerOnComponent(_opticalLiveVideoDialog, _mainUI);
                  _opticalLiveVideoDialog.setVisible(true);
                }
                catch (XrayTesterException ex)
                {
                  handleException(ex);
                }
            }
            else if (opticalLiveVideoEnum.equals(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO))
            {
                Assert.expect(_opticalLiveVideoDialog != null);
                try
                {
                  _opticalLiveVideoDialog.setVisible(false);
                  _opticalLiveVideoDialog.unInitialize();
                  _opticalLiveVideoDialog = null;
                }
                catch (XrayTesterException ex)
                {
                  handleException(ex);
                }
            }
            else
            {
              Assert.expect(false, "invalid optical live video enum");
            }
          }
        }
        else if (observable instanceof FringePatternObservable)
        {
          if (arg instanceof FringePatternInfo)
          {
            FringePatternInfo fringePatternInfo = (FringePatternInfo) arg;
            final OpticalCameraIdEnum cameraId = fringePatternInfo.getCameraId();
            FringePatternEnum fringePatternEnum = fringePatternInfo.getFringePatternEnum();

            if (fringePatternEnum.equals(FringePatternEnum.INITIALIZE))
            {
              if (_fringePatternDialog != null)
              {
                _fringePatternDialog.dispose();
                _fringePatternDialog = null;
              }

              // Get FringePattern instance
              _fringePatternDialog = new FringePatternDialog(_mainUI, "Fringe Pattern", cameraId);
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_WHITE_LIGHT))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.displayWhiteLight(cameraId);
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.displayWhiteLight(cameraId);
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_NO_LIGHT))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.closeFringePattern();
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.closeFringePattern();
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_ONE))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.displayFringePatternShiftOne(cameraId);
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.displayFringePatternShiftOne(cameraId);
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_TWO))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.displayFringePatternShiftTwo(cameraId);
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_THREE))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.displayFringePatternShiftThree(cameraId);
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.displayFringePatternShiftThree(cameraId);
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.PROJECT_FRINGE_PATTERN_SHIFT_FOUR))
            {
              if (SwingUtilities.isEventDispatchThread())
              {
                _fringePatternDialog.displayFringePatternShiftFour(cameraId);
              }
              else
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    _fringePatternDialog.displayFringePatternShiftFour(cameraId);
                  }
                });
              }
            }
            else if (fringePatternEnum.equals(FringePatternEnum.FINISH_DISPLAY_FRINGE_PATTERN))
            {
              try
              {
                PspImageAcquisitionEngine.getInstance().handlePostImageDisplay(PspModuleEnum.FRONT);
              }
              catch (XrayTesterException ex)
              {
                handleException(ex);
                _runAdjustmentsAtStartupCheckboxMenuItem.setSelected(false);
              }
            }
            else
            {
              Assert.expect(false, "invalid display mode");
            }
          }
        }
        else
        {
          Assert.expect(false, "unknown observable");
        }
      }
    });
  }
}
