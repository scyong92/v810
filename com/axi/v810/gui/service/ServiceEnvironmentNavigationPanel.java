package com.axi.v810.gui.service;

import com.axi.v810.gui.mainMenu.*;

/**
 * Navigation panel that will be user with in the service environment.
 * @author Erica Wheatcroft
 */
public class ServiceEnvironmentNavigationPanel extends AbstractNavigationPanel
{
  /**
   * @author Erica Wheatcroft
   */
  public ServiceEnvironmentNavigationPanel(String[] buttonNames, AbstractEnvironmentPanel envPanel)
  {
    super(buttonNames, envPanel);
  }
}
