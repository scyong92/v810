package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
class PanelPositionerPanel extends JPanel implements TaskPanelInt, Observer
{
  private JLabel _currentPositionLabel = new JLabel();
  private JLabel _currentXPositionLabel = new JLabel();
  private JLabel _currentYPositionLabel = new JLabel();
  private JLabel _yAxisHomeSensorStateLabel = new JLabel();
  private JLabel _xAxisHomeSensorStateLabel = new JLabel();
  private JLabel _yAxisMotionSensorStateLabel = new JLabel();
  private JLabel _xAxisMotionSensorStateLabel = new JLabel();
  private JButton _initializeButton = new JButton();
  private JButton _saveConfigurationButton = new JButton();
  private JButton _homeYButton = new JButton();
  private JButton _homeXButton = new JButton();
  private JButton _loadConfigurationButton = new JButton();
  private JButton _enableOrDisableXAxisButton = new JButton();
  private JButton _enableOrDisableYAxisButton = new JButton();
  private JTabbedPane _tabbedPane = new JTabbedPane();

  private ProgressDialog _progressDialog = null;

  private static final String _CURRENT_POSITION_LABEL_HEADER = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_CURRENT_POSITION_KEY");
  private static final String _X_LABEL = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_CURRENT_POSITION_X_KEY") + " ";
  private static final String _Y_LABEL = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_CURRENT_POSITION_Y_KEY") + " ";
  private static final String _BLOCKED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_BLOCKED_STATE_KEY");
  private static final String _CLEAR = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLEAR_STATE_KEY");
  private static final String _ENABLED = StringLocalizer.keyToString("GUI_ENABLED_KEY");
  private static final String _DISABLED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_DISABLED_KEY");
  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");

  private static final Color _RED = new Color(160, 0, 0);
  private static final Color _GREEN = new Color(0, 160, 0);
  private static final Color _YELLOW = new Color(160, 160, 0);

  private boolean _monitorEnabled = false;

  // these strings do not need to be localized... they are thread names
  private WorkerThread _monitorHomeSensorThread = new WorkerThread("Home Sensor Monitor");
  private WorkerThread _monitorCurrentPositionThread = new WorkerThread("Current Position Monitor");
  private WorkerThread _monitorCurrentAxesStateThread = new WorkerThread("Current Axes State");
  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();

  private PanelPositioner _panelPositioner = null;
  private int _currentTabIndex = -1;
  private boolean _subsystemInitialized = false;

  private String _scanMovesConfigurationDir = Directory.getScantMoveDir();
  private String _pointToPointConfigurationDir = Directory.getPointToPointMoveDir();

  private PointToPointMovesPanel _pointToPointMovesPanel = null;
  private ScanMovesPanel _scanMovesPanel = null;

  private boolean _firstMessageDisplayed = false;
  private boolean _initializingSubsystem = false; // used so that the progress bar shows the correct state.

  private SubsystemStatusAndControlTaskPanel _parent = null;

  private MathUtilEnum _units;

  private double _currentXPositionInNanometers = 0.0;
  private double _currentYPositionInNanometers = 0.0;
  private boolean _xHomeSensorStateClear = false;
  private boolean _yHomeSensorStateClear = false;

  // listners i create them here so that i can add them and remove them easly.
  private ChangeListener _tabPaneChangeListener = new ChangeListener()
  {
    public void stateChanged(ChangeEvent evt)
    {
      changeTab();
    }
  };

  private ActionListener _enableOrDisableXAxisButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (_panelPositioner.isXaxisEnabled())
              _panelPositioner.disableXaxis();
            else
              _panelPositioner.enableXaxis();

            updateXAxisMotionState();

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleXrayTesterException(xte);
                _xAxisMotionSensorStateLabel.setText(_UNKNOWN);
                _xAxisMotionSensorStateLabel.setForeground(_RED);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _enableOrDisableYAxisButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (_panelPositioner.isYaxisEnabled())
              _panelPositioner.disableYaxis();
            else
              _panelPositioner.enableYaxis();

            updateYAxisMotionState();

          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleXrayTesterException(xte);
                _yAxisMotionSensorStateLabel.setText(_UNKNOWN);
                _yAxisMotionSensorStateLabel.setForeground(_RED);
              }
            });
          }
        }
      });
    }
  };


  private ActionListener _saveConfigurationButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // the user wants to write out the data in the table they have showing.
      saveTableInformation();
    }
  };

  private ActionListener _loadConfigurationButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      loadTableInformation();
      enableLoadConfigurationButton();
      enablePanel();
    }
  };

  private ActionListener _homeYAxisButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelPositioner != null);
      createProgressDialog(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOME_Y_AXIS_COMPLETED_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOMING_Y_AXIS_KEY"), true);
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _panelPositioner.homeYaxis();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleXrayTesterException(xte);
                _progressDialog.setVisible(true);
                _progressDialog.dispose();
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog.dispose();
    }
  };

  private ActionListener _homeXAxisButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelPositioner != null);
      createProgressDialog(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOME_X_AXIS_COMPLETED_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOMING_X_AXIS_KEY"), true);
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _panelPositioner.homeXaxis();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleXrayTesterException(xte);
                _progressDialog.setVisible(true);
                _progressDialog.dispose();
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog.dispose();
    }
  };

  private ActionListener _initializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_panelPositioner != null);
            
      _initializingSubsystem = true;
      createProgressDialog(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_INITIALIZE_COMPLETE_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"), false);
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _firstMessageDisplayed = false;

            // we do not want to be monitoring while the system is shutting down and starting up.
            _monitorEnabled = false;

            // shutdown
            _panelPositioner.shutdown();

            // startup
            _panelPositioner.startup();

            _subsystemInitialized = true;
          }
          catch (final XrayTesterException xte)
          {
            _initializingSubsystem = false;
            _subsystemInitialized = false;
            handleXrayTesterException(xte);
            _progressDialog.setVisible(true);
            _progressDialog.dispose();
          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog.dispose();
      populatePanel();
      startMonitoring();
      _initializingSubsystem = false;
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  PanelPositionerPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    createPanel();

    // we will only start observing gui changes nothing else.. everything else
    // needs to start and stop when the start and finish methods are called
    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void changeTab()
  {
    // tell the current tab to finish and start the new tab the user wants to switch too
    int currentSelectedTab = _tabbedPane.getSelectedIndex();
    Assert.expect(currentSelectedTab != -1);

    if (_currentTabIndex >= 0)
    {
      Assert.expect(_tabbedPane.getComponentAt(currentSelectedTab) instanceof TaskPanelInt);
      TaskPanelInt panel = (TaskPanelInt)_tabbedPane.getComponentAt(_currentTabIndex);
      panel.finish();
    }

    Assert.expect(_tabbedPane.getComponentAt(currentSelectedTab) instanceof TaskPanelInt);
    TaskPanelInt panel = (TaskPanelInt)_tabbedPane.getComponentAt(currentSelectedTab);
    panel.start();

    _currentTabIndex = currentSelectedTab;
  }

  private void populatePanel()
  {
    changeTab();

    // enable controls depending on if the system is initialized or not.
    enablePanel();

    // get the current position
    updateCurrentPosition();

    // get the home sensor state
    updateHomeSensorState();

    // get the motion state for the x and y axis
    updateMotionState();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateMotionState()
  {
    try
    {
      updateXAxisMotionState();
      updateYAxisMotionState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      _xAxisMotionSensorStateLabel.setText(_UNKNOWN);
      _xAxisMotionSensorStateLabel.setForeground(_RED);
      _yAxisMotionSensorStateLabel.setText(_UNKNOWN);
      _yAxisMotionSensorStateLabel.setForeground(_RED);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateYAxisMotionState() throws XrayTesterException
  {
    if (_subsystemInitialized)
    {
      try
      {
        boolean yAxisMotionSensorState = _panelPositioner.isYaxisEnabled();
        if (yAxisMotionSensorState)
        {
          _yAxisMotionSensorStateLabel.setForeground(_GREEN);
          _yAxisMotionSensorStateLabel.setText(_ENABLED);
          _enableOrDisableYAxisButton.setText(_DISABLED);
        }
        else
        {
          _yAxisMotionSensorStateLabel.setForeground(_RED);
          _yAxisMotionSensorStateLabel.setText(_DISABLED);
          _enableOrDisableYAxisButton.setText(_ENABLED);
        }

      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
        _yAxisMotionSensorStateLabel.setText(_UNKNOWN);
        _yAxisMotionSensorStateLabel.setForeground(_RED);
      }
    }
    else
    {
      _yAxisMotionSensorStateLabel.setText(_UNKNOWN);
      _yAxisMotionSensorStateLabel.setForeground(Color.BLACK);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateXAxisMotionState() throws XrayTesterException
  {
    if (_subsystemInitialized)
    {
      try
      {
        boolean xAxisMotionSensorState = _panelPositioner.isXaxisEnabled();
        if (xAxisMotionSensorState)
        {
          _xAxisMotionSensorStateLabel.setForeground(_GREEN);
          _xAxisMotionSensorStateLabel.setText(_ENABLED);
          _enableOrDisableXAxisButton.setText(_DISABLED);
        }
        else
        {
          _xAxisMotionSensorStateLabel.setForeground(_RED);
          _xAxisMotionSensorStateLabel.setText(_DISABLED);
          _enableOrDisableXAxisButton.setText(_ENABLED);
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
        _xAxisMotionSensorStateLabel.setText(_UNKNOWN);
        _xAxisMotionSensorStateLabel.setForeground(_RED);
      }
    }
    else
    {
      _xAxisMotionSensorStateLabel.setText(_UNKNOWN);
      _xAxisMotionSensorStateLabel.setForeground(Color.BLACK);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  synchronized void handleXrayTesterException(final XrayTesterException xte)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      showException(xte);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          showException(xte);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showException(XrayTesterException xte)
  {
    // and exception has been generated the system needs to be reinitialized
    stopMonitoring();
    _subsystemInitialized = false;
    enablePanel();

    // only display the first error message
    if (_firstMessageDisplayed == false)
    {
      _parent.handleXrayTesterException(xte);
      _firstMessageDisplayed = true;
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateCurrentPosition()
  {
    if (_subsystemInitialized)
    {
      int xPositionInNanometers = 0;
      int yPositionInNanometers = 0;

      try
      {
        xPositionInNanometers = _panelPositioner.getXaxisActualPositionInNanometers();
        yPositionInNanometers = _panelPositioner.getYaxisActualPositionInNanometers();
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            String currentPosition = _CURRENT_POSITION_LABEL_HEADER + " " + _UNKNOWN;
            _currentPositionLabel.setText(currentPosition.intern());
            _currentPositionLabel.setForeground(_RED);
            _currentXPositionLabel.setText("");
            _currentYPositionLabel.setText("");
          }
        });
        return;
      }

      // if the current position is the same as previously reported then lets not do anything
      if (_currentXPositionInNanometers == xPositionInNanometers && _currentYPositionInNanometers == yPositionInNanometers && _currentXPositionLabel.getText().equalsIgnoreCase("")==false)
        return;

      final int xPositionInNanometersConst = xPositionInNanometers;
      final int yPositionInNanometersConst = yPositionInNanometers;

      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          // set the new values
          _currentXPositionInNanometers = xPositionInNanometersConst;
          _currentYPositionInNanometers = yPositionInNanometersConst;

          double xPosition = MathUtil.roundToPlaces((double) MathUtil.convertUnits(xPositionInNanometersConst, MathUtilEnum.NANOMETERS, _units),
                                                    MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
          double yPosition = MathUtil.roundToPlaces((double) MathUtil.convertUnits(yPositionInNanometersConst, MathUtilEnum.NANOMETERS, _units),
                                                    MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

          _currentPositionLabel.setText(_CURRENT_POSITION_LABEL_HEADER.intern());
          _currentPositionLabel.setForeground(Color.BLACK);
          _currentXPositionLabel.setText(new String(_X_LABEL + " " + xPosition + " " + _units).intern());
          _currentXPositionLabel.setForeground(Color.BLACK);
          _currentYPositionLabel.setText(new String(_Y_LABEL + " " + yPosition + " " + _units).intern());
          _currentYPositionLabel.setForeground(Color.BLACK);
        }
      });
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          // the subsystem is not initialized
          String currentPosition = _CURRENT_POSITION_LABEL_HEADER + " " + _UNKNOWN;
          _currentPositionLabel.setText(currentPosition.intern());
          _currentPositionLabel.setForeground(Color.BLACK);
          _currentXPositionLabel.setText("");
          _currentYPositionLabel.setText("");

        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateHomeSensorState()
  {
    if (_subsystemInitialized)
    {

      boolean xAxisHomeSensorState = false;
      boolean yAxisHomeSensorState = false;

      try
      {
        xAxisHomeSensorState = _panelPositioner.isXaxisHomeSensorClear();
        yAxisHomeSensorState = _panelPositioner.isYaxisHomeSensorClear();

        // since its the same as last time we checked you can ignore
        if (xAxisHomeSensorState == _xHomeSensorStateClear && yAxisHomeSensorState == _yHomeSensorStateClear)
          return;
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            _xAxisHomeSensorStateLabel.setText(_UNKNOWN);
            _xAxisHomeSensorStateLabel.setForeground(_RED);
            _yAxisHomeSensorStateLabel.setText(_UNKNOWN);
            _yAxisHomeSensorStateLabel.setForeground(_RED);
          }
        });

        return;
      }

      final boolean xAxisHomeSensorStateConst = xAxisHomeSensorState;
      final boolean yAxisHomeSensorStateConst = yAxisHomeSensorState;

      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          // set the new state
          _xHomeSensorStateClear = xAxisHomeSensorStateConst;
          _yHomeSensorStateClear = yAxisHomeSensorStateConst;

          if (xAxisHomeSensorStateConst)
          {
            _xAxisHomeSensorStateLabel.setForeground(_GREEN);
            _xAxisHomeSensorStateLabel.setText(_CLEAR.intern());
          }
          else
          {
            _xAxisHomeSensorStateLabel.setForeground(_YELLOW);
            _xAxisHomeSensorStateLabel.setText(_BLOCKED.intern());
          }

          if (yAxisHomeSensorStateConst)
          {
            _yAxisHomeSensorStateLabel.setForeground(_GREEN);
            _yAxisHomeSensorStateLabel.setText(_CLEAR.intern());
          }
          else
          {
            _yAxisHomeSensorStateLabel.setForeground(_YELLOW);
            _yAxisHomeSensorStateLabel.setText(_BLOCKED.intern());
          }
        }
      });
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _xAxisHomeSensorStateLabel.setForeground(Color.BLACK);
          _xAxisHomeSensorStateLabel.setText(_UNKNOWN);
          _yAxisHomeSensorStateLabel.setForeground(Color.BLACK);
          _yAxisHomeSensorStateLabel.setText(_UNKNOWN);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enablePanel()
  {
    _homeYButton.setEnabled(_subsystemInitialized);
    _homeXButton.setEnabled(_subsystemInitialized);
    enableLoadConfigurationButton();
    _enableOrDisableXAxisButton.setEnabled(_subsystemInitialized);
    _enableOrDisableYAxisButton.setEnabled(_subsystemInitialized);
    _pointToPointMovesPanel.enablePanel(_subsystemInitialized);
    _scanMovesPanel.enablePanel(_subsystemInitialized);
    _tabbedPane.setEnabled(true);
    _initializeButton.setEnabled(true);
  }

  /**
   * @author Erica Wheatcroft
   */
  void enableLoadConfigurationButton()
  {
    if (_subsystemInitialized)
    {
      Object[] values = null;
      if (_tabbedPane.getSelectedComponent() instanceof ScanMovesPanel)
      {
        Collection<String> files = FileUtil.listAllFilesInDirectory(_scanMovesConfigurationDir);
        values = files.toArray();
      }
      else if (_tabbedPane.getSelectedComponent() instanceof PointToPointMovesPanel)
      {
        Collection<String> files = FileUtil.listAllFilesInDirectory(_pointToPointConfigurationDir);
        values = files.toArray();
      }

      if (values.length > 0)
        _loadConfigurationButton.setEnabled(_subsystemInitialized);
      else
        _loadConfigurationButton.setEnabled(false);
    }
    else
      _loadConfigurationButton.setEnabled(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  void disableControls(boolean testRunning)
  {
    _initializeButton.setEnabled(true);

    if(testRunning)
      _initializeButton.setEnabled(false);
    else
      _initializeButton.setEnabled(true);

    _saveConfigurationButton.setEnabled(!testRunning);
    _homeYButton.setEnabled(!testRunning);
    _homeXButton.setEnabled(!testRunning);
    _loadConfigurationButton.setEnabled(!testRunning);
    _enableOrDisableXAxisButton.setEnabled(!testRunning);
    _enableOrDisableYAxisButton.setEnabled(!testRunning);
    _tabbedPane.setEnabled(!testRunning);
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean areAnyAxesDisabled()
  {
    Assert.expect(_panelPositioner != null);
    try
    {
      if (_panelPositioner.isXaxisEnabled() && _panelPositioner.isYaxisEnabled())
        return false;
    }
    catch(XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      return true;
    }

    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    _firstMessageDisplayed = false;

    // starting up the panel positioner
    _panelPositioner = PanelPositioner.getInstance();

    try
    {
      _subsystemInitialized = !_panelPositioner.isStartupRequired();
    }
    catch (XrayTesterException xte)
    {
      _subsystemInitialized = false;
      handleXrayTesterException(xte);
    }

    // populate the panel
    populatePanel();

    // add the listeners
    addListeners();

    startObserving(true);

    if (_subsystemInitialized)
      startMonitoring();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startMonitoring()
  {
    _monitorEnabled = true;
    _monitorCurrentPositionThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (_monitorEnabled)
        {
          updateCurrentPosition();
          try
          {
            Thread.sleep(200);
          }
          catch (InterruptedException ie)
          {
            Assert.expect(false);
          }
        }
      }
    });
    _monitorHomeSensorThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (_monitorEnabled)
        {
          updateHomeSensorState();
          try
          {
            Thread.sleep(500);
          }
          catch (InterruptedException ie)
          {
            Assert.expect(false);
          }
        }
      }
    });
    _monitorCurrentAxesStateThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (_monitorEnabled)
        {
          updateMotionState();
          try
          {
            Thread.sleep(500);
          }
          catch (InterruptedException ie)
          {
            Assert.expect(false);
          }
        }
      }
    });
  }


  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica WHeatcroft
   */
  public void finish()
  {
    // stop polling
    stopMonitoring();

    // remove action listeners
    removeListeners();

    // stop observing
    startObserving(false);

    // tell all current visible panel to finish
    if (_currentTabIndex >= 0)
    {
      TaskPanelInt panel = (TaskPanelInt)_tabbedPane.getComponentAt(_currentTabIndex);
      panel.finish();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog(String operationCompletedMessage, String operationInProgressMessage, boolean cancelableOperation)
  {
    Assert.expect(operationCompletedMessage != null);
    Assert.expect(operationInProgressMessage != null);

    // now lets create the progress bar.
    if (cancelableOperation)
    {
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + " - " +
                                           StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TAB_NAME_KEY"),
                                           operationInProgressMessage,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY") + " ",
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);
      // now create the listner tied to the cancel button for the progress bar dialog.
      _progressDialog.addCancelActionListener(new ActionListener()
      {
        public void actionPerformed(java.awt.event.ActionEvent e)
        {
          try
          {
            _panelPositioner.abort();
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
          finally
          {
            _progressDialog.dispose();

            // If we cancel from homing, as an example, we need to set the panel up again.  In this case,
            // PanelPositioner will require start up because homing was not complete.  We need to ensure that
            // the PanelPositioner is put into a known state before allowing any point-to-point or scan moves
            // to occur.
            try
            {
              _subsystemInitialized = _panelPositioner.isStartupRequired();
            }
            catch (XrayTesterException xte)
            {
              // do nothing
            }

            enablePanel();
          }
        }
      });
    }
    else
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + " - " +
                                           StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TAB_NAME_KEY"),
                                           operationInProgressMessage,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());

  }

  /**
   * @author Erica WHeatcroft
   */
  private void createStatusPanel()
  {
    JPanel currentPositionAndButtonPanel = new JPanel(new BorderLayout());
    currentPositionAndButtonPanel.setBorder(BorderFactory.createEmptyBorder(30, 10, 15, 30));

    JPanel northPanel = new JPanel(new BorderLayout(5, 45));
    JPanel labelPanel = new JPanel(new GridLayout(3, 1, 5, 8));
    labelPanel.add(_currentPositionLabel);
    _currentPositionLabel.setFont(FontUtil.getBiggerFont(_currentPositionLabel.getFont(),Localization.getLocale()));
    _currentPositionLabel.setHorizontalAlignment(JLabel.CENTER);
    _currentPositionLabel.setFont(FontUtil.getBoldFont(_currentPositionLabel.getFont(),Localization.getLocale()));
    _currentPositionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_CURRENT_POSITION_KEY")
                                  + StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY"));
    labelPanel.add(_currentXPositionLabel);
    _currentXPositionLabel.setFont(FontUtil.getBiggerFont(_currentXPositionLabel.getFont(),Localization.getLocale()));
    _currentXPositionLabel.setFont(FontUtil.getBoldFont(_currentXPositionLabel.getFont(),Localization.getLocale()));
    _currentXPositionLabel.setText("");
    _currentXPositionLabel.setHorizontalAlignment(JLabel.CENTER);
    labelPanel.add(_currentYPositionLabel);
    _currentYPositionLabel.setFont(FontUtil.getBiggerFont(_currentYPositionLabel.getFont(),Localization.getLocale()));
    _currentYPositionLabel.setFont(FontUtil.getBoldFont(_currentYPositionLabel.getFont(),Localization.getLocale()));
    _currentYPositionLabel.setText("");
    _currentYPositionLabel.setHorizontalAlignment(JLabel.CENTER);
    northPanel.add(labelPanel, BorderLayout.NORTH);

    JPanel buttonPanel = new JPanel(new GridLayout(3, 1, 5, 20));
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 30));
    _homeYButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOME_Y_AXIS_KEY"));
    _homeXButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOME_X_AXIS_KEY"));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    buttonPanel.add(_initializeButton);
    buttonPanel.add(_homeXButton);
    buttonPanel.add(_homeYButton);

    northPanel.add(buttonPanel, BorderLayout.CENTER);

    currentPositionAndButtonPanel.add(northPanel, BorderLayout.NORTH);

    // create the state panels
    createStatePanels(currentPositionAndButtonPanel);

    add(currentPositionAndButtonPanel, BorderLayout.EAST);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createStatePanels(JPanel currentPositionAndButtonPanel)
  {
    JPanel statePanels = new JPanel(new FlowLayout());
    JPanel innerStatePanels = new JPanel(new GridLayout(2, 1, 5, 20));

    JPanel homeSensorPanel = new JPanel(new FlowLayout());
    homeSensorPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                                                                  StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_HOME_SENSOR_KEY")),
                                                                 BorderFactory.createEmptyBorder(15, 10, 10, 10)));
    JLabel yaxisLabel = new JLabel();
    yaxisLabel.setFont(FontUtil.getBiggerFont(yaxisLabel.getFont(),Localization.getLocale()));
    yaxisLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_Y_AXIS_KEY"));
    _yAxisHomeSensorStateLabel.setText(_BLOCKED);
    JLabel xAxisLabel = new JLabel();
    xAxisLabel.setFont(FontUtil.getBiggerFont(xAxisLabel.getFont(),Localization.getLocale()));
    xAxisLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_X_AXIS_KEY"));
    _xAxisHomeSensorStateLabel.setText(_CLEAR);
    JPanel homeSensorInnerPanel = new JPanel(new PairLayout(5, 10));
    homeSensorInnerPanel.add(xAxisLabel);
    homeSensorInnerPanel.add(_xAxisHomeSensorStateLabel);
    _xAxisHomeSensorStateLabel.setFont(FontUtil.getBiggerFont(_xAxisHomeSensorStateLabel.getFont(),Localization.getLocale()));
    _xAxisHomeSensorStateLabel.setFont(FontUtil.getBoldFont(_xAxisHomeSensorStateLabel.getFont(),Localization.getLocale()));
    homeSensorInnerPanel.add(yaxisLabel);
    homeSensorInnerPanel.add(_yAxisHomeSensorStateLabel);
    _yAxisHomeSensorStateLabel.setFont(FontUtil.getBiggerFont(_yAxisHomeSensorStateLabel.getFont(),Localization.getLocale()));
    _yAxisHomeSensorStateLabel.setFont(FontUtil.getBoldFont(_yAxisHomeSensorStateLabel.getFont(),Localization.getLocale()));
    homeSensorPanel.add(homeSensorInnerPanel);
    innerStatePanels.add(homeSensorPanel);

    JPanel motionStatePanel = new JPanel(new BorderLayout(5, 5));
    motionStatePanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                                                                   StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_MOTION_KEY")),
                                                                  BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    JPanel xAxisPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
    JLabel motionXAxisLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_X_AXIS_KEY"));
    motionXAxisLabel.setFont(FontUtil.getBiggerFont(motionXAxisLabel.getFont(),Localization.getLocale()));
    _xAxisMotionSensorStateLabel.setText(_ENABLED);
    _xAxisMotionSensorStateLabel.setForeground(_GREEN);
    _xAxisMotionSensorStateLabel.setFont(FontUtil.getBiggerFont(_xAxisMotionSensorStateLabel.getFont(),Localization.getLocale()));
    _xAxisMotionSensorStateLabel.setFont(FontUtil.getBoldFont(_xAxisMotionSensorStateLabel.getFont(),Localization.getLocale()));
    _enableOrDisableXAxisButton.setText(_DISABLED);
    xAxisPanel.add(motionXAxisLabel);
    xAxisPanel.add(_xAxisMotionSensorStateLabel);
    xAxisPanel.add(_enableOrDisableXAxisButton);
    motionStatePanel.add(xAxisPanel, BorderLayout.NORTH);

    JPanel yAxisPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
    JLabel motionYAxisLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_Y_AXIS_KEY"));
    motionYAxisLabel.setFont(FontUtil.getBiggerFont(motionYAxisLabel.getFont(),Localization.getLocale()));
    _yAxisMotionSensorStateLabel.setText(_ENABLED);
    _yAxisMotionSensorStateLabel.setForeground(_GREEN);
    _yAxisMotionSensorStateLabel.setFont(FontUtil.getBiggerFont(_yAxisMotionSensorStateLabel.getFont(),Localization.getLocale()));
    _yAxisMotionSensorStateLabel.setFont(FontUtil.getBoldFont(_yAxisMotionSensorStateLabel.getFont(),Localization.getLocale()));
    _enableOrDisableYAxisButton.setText(_DISABLED);
    yAxisPanel.add(motionYAxisLabel);
    yAxisPanel.add(_yAxisMotionSensorStateLabel);
    yAxisPanel.add(_enableOrDisableYAxisButton);
    motionStatePanel.add(yAxisPanel, BorderLayout.SOUTH);
    innerStatePanels.add(motionStatePanel);
    statePanels.add(innerStatePanels);
    currentPositionAndButtonPanel.add(statePanels, BorderLayout.SOUTH);
  }

  /**
   * @author Erica WHeatcroft
   */
  private void createTabbedPanel()
  {
    _tabbedPane.setTabPlacement(JTabbedPane.TOP);
    _scanMovesPanel = new ScanMovesPanel(this);
    _tabbedPane.add(_scanMovesPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_MOVES_PANEL_TAB_NAME_KEY"));
    _pointToPointMovesPanel = new PointToPointMovesPanel(this);
    _tabbedPane.add(_pointToPointMovesPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_PT_2_PT_PANEL_TAB_NAME_KEY"));
    add(_tabbedPane, BorderLayout.CENTER);
  }

  /**
   * @author Erica WHeatcroft
   */
  private void createConfigurationPanel()
  {
    JPanel configurationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel buttonPanel = new JPanel(new GridLayout(1, 2, 15, 5));
    _loadConfigurationButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_LOAD_CONFIG_BUTTON_KEY"));
    enableLoadConfigurationButton();
    _saveConfigurationButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SAVE_CONFIG_BUTTON_KEY"));
    enableSaveConfigurationButton(false);
    buttonPanel.add(_loadConfigurationButton);
    buttonPanel.add(_saveConfigurationButton);
    configurationPanel.add(buttonPanel);
    add(configurationPanel, BorderLayout.NORTH);
  }

  /**
   * @author Erica WHeatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(50, 30));
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    createStatusPanel();
    createTabbedPanel();
    createConfigurationPanel();
  }

  /**
   * @author Erica WHeatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _saveConfigurationButton.addActionListener(_saveConfigurationButtonActionListener);
    _homeYButton.addActionListener(_homeYAxisButtonActionListener);
    _homeXButton.addActionListener(_homeXAxisButtonActionListener);
    _loadConfigurationButton.addActionListener(_loadConfigurationButtonActionListener);
    _enableOrDisableXAxisButton.addActionListener(_enableOrDisableXAxisButtonActionListener);
    _enableOrDisableYAxisButton.addActionListener(_enableOrDisableYAxisButtonActionListener);
    _tabbedPane.addChangeListener(_tabPaneChangeListener);
  }

  /**
   * @author Erica WHeatcroft
   */
  private void startObserving(boolean start)
  {
    if (start)
    {
      HardwareObservable.getInstance().addObserver(this);
      ProgressObservable.getInstance().addObserver(this);
    }
    else
    {
      HardwareObservable.getInstance().deleteObserver(this);
      ProgressObservable.getInstance().deleteObserver(this);
    }
  }

  /**
   * @author Erica WHeatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _saveConfigurationButton.removeActionListener(_saveConfigurationButtonActionListener);
    _homeYButton.removeActionListener(_homeYAxisButtonActionListener);
    _homeXButton.removeActionListener(_homeXAxisButtonActionListener);
    _loadConfigurationButton.removeActionListener(_loadConfigurationButtonActionListener);
    _enableOrDisableXAxisButton.removeActionListener(_enableOrDisableXAxisButtonActionListener);
    _enableOrDisableYAxisButton.removeActionListener(_enableOrDisableYAxisButtonActionListener);
    _tabbedPane.removeChangeListener(_tabPaneChangeListener);
  }

  /**
   * @author Erica WHeatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        Assert.expect(observable != null);
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();

            if (eventEnum instanceof PanelPositionerEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(PanelPositionerEventEnum.INITIALIZE))
                _progressDialog.updateProgressBarPrecent(100);
              else if((event.isStart() == false && eventEnum.equals(PanelPositionerEventEnum.HOME_X_AXIS)) && _initializingSubsystem == false)
                _progressDialog.updateProgressBarPrecent(100);
              else if ((event.isStart() == false && eventEnum.equals(PanelPositionerEventEnum.HOME_Y_AXIS)) && _initializingSubsystem == false)
                _progressDialog.updateProgressBarPrecent(100);
            }
          }
          else if (arg instanceof InterlockHardwareException)
          {
            //do nothing if interlock break
          }
          else if (arg instanceof XrayTesterException)
          {
            handleXrayTesterException((XrayTesterException)arg);
          }
          else
            Assert.expect(false);
        }
        else if (observable instanceof GuiObservable)
        {
          if (arg instanceof GuiEvent)
          {
            GuiEvent event = (GuiEvent)arg;
            GuiEventEnum eventEnum = event.getGuiEventEnum();
            if (eventEnum instanceof SelectionEventEnum)
            {
              SelectionEventEnum selectionEventEnum = (SelectionEventEnum)eventEnum;
              if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
              {
                _units = (MathUtilEnum)event.getSource();
                updateCurrentPosition();
              }
            }
          }
        }
        else
          Assert.expect(false);

      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void loadTableInformation()
  {
    // first prompt the user with the list of available configuration files and have them select the one
    // they would like to load.

    Object[] values = null;
    String loadDialogTitle = null;
    String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SELECT_CONFIG_KEY");
    boolean scanMovesPanelVisible = false;
    boolean tableCurrentlyPopulated = false;

    if (_tabbedPane.getSelectedComponent() instanceof ScanMovesPanel)
    {
      tableCurrentlyPopulated = _scanMovesPanel.isTableCurrentlyPopulated();
      Collection<String> files = FileUtil.listAllFilesInDirectory(_scanMovesConfigurationDir);
      values = files.toArray();
      loadDialogTitle = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SELECT_SCANPATH_CONFIG_KEY");
      scanMovesPanelVisible = true;
    }
    else if (_tabbedPane.getSelectedComponent() instanceof PointToPointMovesPanel)
    {
      tableCurrentlyPopulated = _pointToPointMovesPanel.isTableCurrentlyPopulated();
      Collection<String> files = FileUtil.listAllFilesInDirectory(_pointToPointConfigurationDir);
      values = files.toArray();
      loadDialogTitle = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SELECT_PT2PT_CONFIG_KEY");
      scanMovesPanelVisible = false;
    }

    if (tableCurrentlyPopulated)
    {
      // prompt the user that the table is currently visible and that loading the configuration file
      // will remove all defined rows.
      String tablePopulatedContinueLoadMessage = null;
      if (scanMovesPanelVisible)
        tablePopulatedContinueLoadMessage = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TABLE_POPULATED_SCANMOVES_KEY");
      else
        tablePopulatedContinueLoadMessage = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TABLE_POPULATED_PT2PT_KEY");

      tablePopulatedContinueLoadMessage = StringUtil.format(tablePopulatedContinueLoadMessage, 50);

      int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                   tablePopulatedContinueLoadMessage,
                                                   StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.WARNING_MESSAGE);
      if (response != JOptionPane.OK_OPTION)
      {
        // the load will not continue
        return;
      }
    }

    SelectItemDialog loadDialog = new SelectItemDialog(MainMenuGui.getInstance(),
                                                       loadDialogTitle,
                                                       message,
                                                       StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                       StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                       values,
                                                       null);

    String fileToLoad = null;
    int returnValue = loadDialog.showDialog();

    if (returnValue == JOptionPane.OK_OPTION)
    {
      if (scanMovesPanelVisible)
        fileToLoad = _scanMovesConfigurationDir + File.separator + (String)loadDialog.getSelectedValue();
      else
        fileToLoad = _pointToPointConfigurationDir + File.separator + (String)loadDialog.getSelectedValue();
    }

    else // i will assume that everything else is NO / CANCEL
      return;

    if (scanMovesPanelVisible)
      _scanMovesPanel.loadConfigurationFile(fileToLoad);
    else
      _pointToPointMovesPanel.loadConfigurationFile(fileToLoad);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveTableInformation()
  {
    // first prompt the user to select a name for the configuration to save the file too
    Object[] values = null;
    String instruction = "";
    String saveConfigurationDialogTitle = "";
    String fileExtension = "";
    String configurationFileFullPath = "";
    String defaultConfigurationFile = "";
    boolean scanMovesPanelVisible = false;

    if (_tabbedPane.getSelectedComponent() instanceof ScanMovesPanel)
    {
      Collection<String> files = FileUtil.listAllFilesInDirectory(_scanMovesConfigurationDir);
      values = files.toArray();
      saveConfigurationDialogTitle = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SAVE_SCANPATH_CONFIG_KEY");
      instruction = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SAVE_SCANPATH_CONFIG_INSTRUCTION_KEY");
      fileExtension = FileName.getScanMoveFileExtenstion();
      configurationFileFullPath = Directory.getScantMoveDir() + File.separator;
      scanMovesPanelVisible = true;
      defaultConfigurationFile = "scanPathConfiguration";
    }
    else if (_tabbedPane.getSelectedComponent() instanceof PointToPointMovesPanel)
    {
      Collection<String> files = FileUtil.listAllFilesInDirectory(_pointToPointConfigurationDir);
      values = files.toArray();
      saveConfigurationDialogTitle = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SAVE_PT2PT_CONFIG_KEY");
      instruction = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SAVE_PT2PT_CONFIG_INSTRUCTION_KEY");
      fileExtension = FileName.getPointToPointMoveFileExtenstion();
      configurationFileFullPath = Directory.getPointToPointMoveDir() + File.separator;
      scanMovesPanelVisible = false;
      defaultConfigurationFile = "pointToPointConfiguration";
    }
    else
      Assert.expect(false);


    SelectItemDialog selectConfigDialog = new SelectItemDialog(MainMenuGui.getInstance(),
                                                               saveConfigurationDialogTitle,
                                                               instruction,
                                                               StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                                               StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"),
                                                               values,
                                                               StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_NAME_KEY"));

    int value = selectConfigDialog.showDialog();

    if (value == JOptionPane.OK_OPTION)
    {
      Assert.expect(selectConfigDialog.getRenamedValue() instanceof String);

      String configurationFileName = (String)selectConfigDialog.getRenamedValue();
      if (configurationFileName == "")
        configurationFileFullPath += defaultConfigurationFile + fileExtension;
      else
      {
        if (configurationFileName.endsWith(fileExtension) == false)
          configurationFileFullPath += configurationFileName + fileExtension;
        else
          configurationFileFullPath += configurationFileName;
      }
    }
    else
    {
      // everything else i'm assuming the user does not wish to save the configuration
      return;
    }

    // now lets see if the file exists and prompt the user if they would like to override this.
    File configurationFile = new File(configurationFileFullPath);
    if (configurationFile.exists())
    {
      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_POSITIONER_CONFIG_FILE_EXISTS_KEY",
                                                                       new Object[]
                                                                       {configurationFileFullPath}));

      message = StringUtil.format(message, 50);

      int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                   message,
                                                   StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                                   JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.WARNING_MESSAGE);
      if (response != JOptionPane.YES_OPTION)
      {
        // the user does not want to over write.. so return out
        return;
      }
    }

    if (scanMovesPanelVisible)
      _scanMovesPanel.saveConfigurationFile(configurationFileFullPath);
    else
      _pointToPointMovesPanel.saveConfigurationFile(configurationFileFullPath);
  }

  /**
   * @author Erica Wheatcroft
   */
  void enableSaveConfigurationButton(boolean enabled)
  {
    _saveConfigurationButton.setEnabled(enabled);
  }

}
