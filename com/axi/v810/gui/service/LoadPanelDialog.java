package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class LoadPanelDialog extends EscapeDialog
{
  private NumericTextField _panelLengthTextField = new NumericTextField();
  private NumericRangePlainDocument _panelLengthDocument = new NumericRangePlainDocument();
  private NumericTextField _panelWidthTextField = new NumericTextField();
  private NumericRangePlainDocument _panelWidthDocument = new NumericRangePlainDocument();
  private JButton _loadPanelButton = new JButton();
  private JButton _cancelButton = new JButton();

  private int _panelLengthInNanometers = -1;
  private int _panelWidthInNanometers = -1;

  private int _returnValue = JOptionPane.CANCEL_OPTION;

  private MathUtilEnum _units = null;
  private Frame _frame = null;

  private ActionListener _loadPanelButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _returnValue = JOptionPane.OK_OPTION;
      dispose();
    }
  };

  private ActionListener _cancelButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      cancelAction();
    }
  };

  private ActionListener _panelLengthTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      handlePanelLengthEntry();
    }
  };

  private FocusListener _panelLengthTextFieldFocusListener = new FocusListener()
  {
    public void focusGained(FocusEvent e)
    {
    }
    public void focusLost(FocusEvent e)
    {
      handlePanelLengthEntry();
    }

  };

  private ActionListener _panelWidthTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      handlePanelWidthEntry();
    }
  };

  private FocusListener _panelWidthTextFieldFocusListener = new FocusListener()
  {
    public void focusGained(FocusEvent e)
    {
    }

    public void focusLost(FocusEvent e)
    {
      handlePanelWidthEntry();
    }

  };


  /**
   * @author Erica Wheatcroft
   */
  public LoadPanelDialog(Frame frame, boolean modal, MathUtilEnum units)
  {
    super(frame, modal);

    Assert.expect(units != null);
    _units = units;
    _frame = frame;

    createDialog();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handlePanelWidthEntry() throws NumberFormatException
  {
    String panelWidthString = _panelWidthTextField.getText();
    if (panelWidthString != null && panelWidthString.equalsIgnoreCase("") == false)
    {
      double convertedPanelLength = Double.parseDouble(panelWidthString);
      _panelWidthInNanometers = (int)MathUtil.convertUnits(convertedPanelLength, _units, MathUtilEnum.NANOMETERS);

      if (_panelLengthInNanometers != -1 && _panelWidthInNanometers != -1)
      {
        _loadPanelButton.setEnabled(true);
        _loadPanelButton.requestFocus();
      }
      else
        _loadPanelButton.setEnabled(false);
    }
    else
    {
      _panelWidthInNanometers = -1;
      _loadPanelButton.setEnabled(false);
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private void handlePanelLengthEntry() throws NumberFormatException
  {
    String panelLengthString = _panelLengthTextField.getText();
    if (panelLengthString != null && panelLengthString.equalsIgnoreCase("") == false)
    {
      double convertedPanelLength = Double.parseDouble(panelLengthString);
      _panelLengthInNanometers = (int)MathUtil.convertUnits(convertedPanelLength, _units, MathUtilEnum.NANOMETERS);

      if (_panelWidthInNanometers != -1 && _panelLengthInNanometers != -1)
      {
        _loadPanelButton.setEnabled(true);
        _loadPanelButton.requestFocus();
      }
      else
        _loadPanelButton.setEnabled(false);
    }
    else
    {
      _panelLengthInNanometers = -1;
      _loadPanelButton.setEnabled(false);
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private void createDialog()
  {
    getContentPane().setLayout(new BorderLayout());

    JPanel instructionPanel = new JPanel();
    instructionPanel.setLayout(new FlowLayout());
    JLabel instructionLabel = new JLabel();
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
    instructionLabel.setText(StringLocalizer.keyToString("SERVICE_LOAD_PANEL_INSTRUCTION_KEY"));
    instructionPanel.add(instructionLabel);
    getContentPane().add(instructionPanel, BorderLayout.NORTH);

    JPanel centerPanel = new JPanel();
    centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    centerPanel.setLayout(new PairLayout());
    JLabel panelWidthLabel = new JLabel();
    centerPanel.add(panelWidthLabel);
    centerPanel.add(_panelWidthTextField);
    _panelWidthTextField.setDocument(_panelWidthDocument);
    JLabel panelLengthLabel = new JLabel();
    centerPanel.add(panelLengthLabel);
    centerPanel.add(_panelLengthTextField);
    _panelLengthTextField.setDocument(_panelLengthDocument);

    _panelWidthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));
    _panelLengthDocument.setRange(new DoubleRange(0.0, Double.MAX_VALUE));

    panelWidthLabel.setText(StringLocalizer.keyToString(new LocalizedString("SERVICE_LOAD_PANEL_PANEL_WIDTH_KEY",
                                                                   new Object[]{ _units.toString()})));
    _panelLengthTextField.setColumns(10);
    panelLengthLabel.setText(StringLocalizer.keyToString(new LocalizedString("SERVICE_LOAD_PANEL_PANEL_LENGTH_KEY",
                                                                   new Object[]{ _units.toString()})));
    _panelWidthTextField.setColumns(10);
    getContentPane().add(centerPanel, BorderLayout.CENTER);

    JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    JPanel innerButtonPanel = new JPanel(new GridLayout(1,2,10,0));
    _loadPanelButton.setText(StringLocalizer.keyToString("SERVICE_LOAD_PANEL_BUTTON_KEY"));
    _loadPanelButton.setEnabled(false);
    _cancelButton.setText(StringLocalizer.keyToString("SERVICE_CANCEL_PANEL_BUTTON_KEY"));

    innerButtonPanel.add(_loadPanelButton);
    innerButtonPanel.add(_cancelButton);

    this.getRootPane().setDefaultButton(_loadPanelButton);

    buttonPanel.add(innerButtonPanel);
    getContentPane().add(buttonPanel, BorderLayout.SOUTH);

    setResizable(false);
    setTitle(StringLocalizer.keyToString("SERVICE_LOAD_KEY"));

    // add the listeners
    _loadPanelButton.addActionListener(_loadPanelButtonActionListener);
    _cancelButton.addActionListener(_cancelButtonActionListener);
    _panelLengthTextField.addFocusListener(_panelLengthTextFieldFocusListener);
    _panelLengthTextField.addActionListener(_panelLengthTextFieldActionListener);
    _panelWidthTextField.addFocusListener(_panelWidthTextFieldFocusListener);
    _panelWidthTextField.addActionListener(_panelWidthTextFieldActionListener);
  }

  /**
   * @author Erica WHeatcroft
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);

    int eventId = e.getID();

    if (eventId == WindowEvent.WINDOW_CLOSING)
      cancelAction();
  }

  /**
   * @author Erica Wheatcroft
   */
  public int showDialog()
  {
    pack();
    SwingUtils.centerOnComponent(this, _frame);
    setVisible(true);
    return _returnValue;
  }


  /**
   * @author Erica Wheatcroft
   */
  private void cancelAction()
  {
    _returnValue = JOptionPane.CANCEL_OPTION;
    _panelWidthInNanometers = -1;
    _panelLengthInNanometers = -1;
    dispose();
  }

  /**
   * @author Erica Wheatcroft
   */
  public int getPanelWidthInNanometers()
  {
    return _panelWidthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  public int getPanelLengthInNanometers()
  {
    return _panelLengthInNanometers;
  }
}
