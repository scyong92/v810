package com.axi.v810.gui.service;

/**
 * Statistical Process Control Math Library V 1.00.
 * Copyright (C) 2002 Thamaraiselvan.P
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; rsion.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * For any questions or suggestions, you can write to me at :
 * Email :p_thamarai@hotmail.com
 */
// XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import java.awt.*;
import java.io.File;
import javax.swing.*;
/**
 * A simple demonstration application showing how to create a line chart using data from an
 * XYDataset.
 */

public class XrayCameraSPCPanel extends JPanel
{
    public XrayCameraChartPanAndZoom panelXbarChart = new XrayCameraChartPanAndZoom();

    GridBagConstraints gbc  = new GridBagConstraints ();


    public XrayCameraSPCPanel(String imagePath, String title)
    {
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        try
        {
           panelXbarChart.setXBarChartImage(imagePath);
        }
        catch(Exception ie)
        {
        }

        JPanel topPanel = new JPanel(new FlowLayout());
        topPanel.add(panelXbarChart.getControl());
        topPanel.setPreferredSize(new Dimension(800, 50));

        JPanel bottomPanel = new JPanel(new FlowLayout());
        bottomPanel.add(new JScrollPane(panelXbarChart.getContent()));

        Config _config = Config.getInstance();

        int   chartWidth=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_WIDTH);
        int   chartHeight=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_HEIGHT);

        bottomPanel.setPreferredSize(new Dimension(chartWidth + 20, chartHeight +20));

        //JPanel groupPanel = new JPanel( new GridLayout(2,1));
        JPanel groupPanel = new JPanel( new BorderLayout());
        groupPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        //groupPanel.add(topPanel,BorderLayout.NORTH);
        groupPanel.add(bottomPanel,BorderLayout.CENTER);
        this.add(groupPanel, BorderLayout.CENTER);

        this.setVisible(true);

    }
}




