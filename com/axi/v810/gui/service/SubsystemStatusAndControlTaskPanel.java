package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * Subsystem Status And Control Task Panel gives the user the ability to work with individual hardware subsystems.
 *
 * @author Erica Wheatcroft
 */
public class SubsystemStatusAndControlTaskPanel extends AbstractTaskPanel
{
  private static ImageIcon _spark = Image5DX.getImageIcon(Image5DX.AXI_SPARK);

  private JTabbedPane _centerTabPane = new JTabbedPane();
  private JLabel _unitsLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNITS_LABEL_KEY"));
  private JComboBox _unitsComboBox = new JComboBox();

  private MathUtilEnum _selectedUnitToDisplayMeasurementsIn = null;
  private int _currentTabIndex = -1;

  private static final String _SIM_ON = StringLocalizer.keyToString("SERVICE_UI_SSACTP_SIMULATION_TEXT_KEY");

  // the individual subsystem panels
  private PanelHandlingPanel _panelHandlingPanel = null;
  private PanelPositionerPanel _panelPositioningPanel = null;
  private AbstractXraySourcePanel _xraySourcePanel = null;
  private IndicatorsPanel _indicators = null;
  private SMTCommunicationPanel _smemaPanel = null;
  private InterlocksPanel _interlocksPanel = null;
  private MotionControlPanel _motionControlPanel = null;
  private XrayCameraPanel _xrayCameraPanel = null;
  private OpticalCameraPanel _opticalCameraPanel = null;
  private PspControllerPanel _pspControllerPanel = null;
  private DigitalIOPanel _digitialIoPanel = null;
  private ImagingChainPanel _imagingChainPanel = null;
  private EthernetBasedLightEnginePanel _eblePanel = null;
  private HardwareSimulationPanel _hwSimulationPanel = null;

  // index of all subsystem panels
  private static int _PANEL_HANDLING_PANEL;
  private static int _PANEL_POSITIONER_PANEL;
  private static int _LEGACY_XRAY_SOURCE_PANEL;
  private static int _XRAY_CAMERA_PANEL;
  private static int _OPTICAL_CAMERA_PANEL;
  private static int _MICROCONTROLLER_PANEL;
  private static int _INDICATORS_PANEL;
  private static int _SMT_COMMUNICATION_PANEL;
  private static int _INTERLOCKS_PANEL;
  private static int _MOTION_CONTROL_PANEL;
  private static int _DIGITAL_IO_PANEL;
  private static int _IMAGING_CHAIN_PANEL;
  private static int _EBLE_PANEL;
  private static int _HARDWARE_SIM_PANEL;

  // parent panel
  private ServiceEnvironmentPanel _parent = null;

  private boolean _startingTask = false;

  private boolean _enablePsp = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);

  // listners - i create them here so that i can easly remove them and add them.
  private ChangeListener _centerTabPaneChangeListener = new ChangeListener()
  {
     public void stateChanged(ChangeEvent evt)
     {
       changeTab();
     }
  };

  private ActionListener _unitsComboBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(MeasurementUnits.getMeasurementUnitEnum((String)_unitsComboBox.getSelectedItem()) instanceof MathUtilEnum);
      _selectedUnitToDisplayMeasurementsIn = MeasurementUnits.getMeasurementUnitEnum((String)_unitsComboBox.getSelectedItem());
      GuiObservable.getInstance().stateChanged(_selectedUnitToDisplayMeasurementsIn, SelectionEventEnum.DISPLAY_UNITS);
      ServiceGuiPersistence.getInstance().setSelectedUnits(_selectedUnitToDisplayMeasurementsIn);
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  private void changeTab()
  {
    // tell the current tab to finish and start the new tab the user wants to switch too
    int currentSelectedTab = _centerTabPane.getSelectedIndex();
    Assert.expect(currentSelectedTab != -1);

    if(_startingTask == false && _currentTabIndex == currentSelectedTab)
      return;

    if (_currentTabIndex >= 0)
    {
      Assert.expect(_centerTabPane.getComponentAt(currentSelectedTab) instanceof TaskPanelInt);
      TaskPanelInt panel = (TaskPanelInt)_centerTabPane.getComponentAt(_currentTabIndex);
      if(panel.isReadyToFinish())
        panel.finish();
      else
      {
        _centerTabPane.setSelectedIndex(_currentTabIndex);
        return;
      }
    }

    Assert.expect(_centerTabPane.getComponentAt(currentSelectedTab) instanceof TaskPanelInt);
    TaskPanelInt panel = (TaskPanelInt)_centerTabPane.getComponentAt(currentSelectedTab);
    panel.start();

    _currentTabIndex = currentSelectedTab;
  }

  /**
   * This method should not be called. If you want to handle the exception call handleXrayTesterException.
   * That method will handle the swing thread.
   *
   * @author Erica Wheatcroft
   */
  private void showException(XrayTesterException xte)
  {
    String messageToDisplay = StringUtil.format(xte.getMessage(), 50);
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  messageToDisplay,
                                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }


  /**
   * @author Erica Wheatcroft
   */
  synchronized void handleXrayTesterException(final XrayTesterException xte)
  {
    if (SwingUtilities.isEventDispatchThread())
    {
      showException(xte);

      // XCR1144, Chnee Khang Wah, 15-Feb-2011
      if(xte instanceof PanelPositionerException)
        MainMenuGui.getInstance().handleXrayTesterException(xte);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          showException(xte);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  synchronized void handleHardwareException(final HardwareException he)
  {
    if (SwingUtilities.isEventDispatchThread())
    {
      showHardwareException(he);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          showHardwareException(he);
        }
      });
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private synchronized void showHardwareException(final HardwareException he)
  {
    String message = he.getMessage();
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringUtil.format(message, 50),
                                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);

  }


  /**
   * @author Andy Mechtenberg
   */
  private void createPanel()
  {
    // set up the combox box that will display the units available to the user
    _unitsComboBox.addItem(MeasurementUnits.MEASURE_INCHES);
    _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILS);
    _unitsComboBox.addItem(MeasurementUnits.MEASURE_MILLIMETERS);
    _unitsComboBox.addItem(MeasurementUnits.MEASURE_NANOMETERS);
    _selectedUnitToDisplayMeasurementsIn = MathUtilEnum.MILS;
    _unitsComboBox.setSelectedItem(MeasurementUnits.getMeasurementUnitString(_selectedUnitToDisplayMeasurementsIn));

    _panelHandlingPanel = new PanelHandlingPanel(this);
    _panelPositioningPanel = new PanelPositionerPanel(this);
    XraySourceTypeEnum sourceTypeEnum = AbstractXraySource.getSourceTypeEnum();
    if (sourceTypeEnum.equals(XraySourceTypeEnum.LEGACY))
    {
      _xraySourcePanel = new LegacyXraySourcePanel(this);
    }
    else if (sourceTypeEnum.equals(XraySourceTypeEnum.HTUBE))
    {
      _xraySourcePanel = new HTubeXraySourcePanel(this);
    }
    else
    {
      Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      _xraySourcePanel = null;
    }
    _indicators = new IndicatorsPanel(this);
    _smemaPanel = new SMTCommunicationPanel(this);
    _interlocksPanel = new InterlocksPanel(this);
    _motionControlPanel = new MotionControlPanel(this);
    _xrayCameraPanel = new XrayCameraPanel(this);
    if (_enablePsp)
    {
      if (PspEngine.isVersion1())
      {
        _opticalCameraPanel = new OpticalCameraPanel(this);
        _pspControllerPanel = new PspControllerPanel(this);
      }
      else
      {
        _opticalCameraPanel = new OpticalCameraPanel(this);
        _eblePanel = new EthernetBasedLightEnginePanel(this);
      }
    }
    _digitialIoPanel = new DigitalIOPanel(this);
    _imagingChainPanel = new ImagingChainPanel(this);
    _hwSimulationPanel = new HardwareSimulationPanel(this);

    setLayout(new BorderLayout());
    add(_centerTabPane, BorderLayout.CENTER);
    _centerTabPane.setFont(FontUtil.getBoldFont(_centerTabPane.getFont(),Localization.getLocale()));
    _centerTabPane.add(_panelHandlingPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_TAB_NAME_KEY"));
    _centerTabPane.add(_panelPositioningPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TAB_NAME_KEY"));
    _centerTabPane.add(_xraySourcePanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"));
    _centerTabPane.add(_xrayCameraPanel, StringLocalizer.keyToString("SERVICE_XRAY_CAMERAS_KEY"));
    if (_enablePsp)
    {
      if (PspEngine.isVersion1())
      {
        _centerTabPane.add(_opticalCameraPanel, StringLocalizer.keyToString("SERVICE_OPTICAL_CAMERAS_KEY"));
        _centerTabPane.add(_pspControllerPanel, StringLocalizer.keyToString("SERVICE_PSPCONTROLLER_KEY"));
      }
      else
      {
        _centerTabPane.add(_opticalCameraPanel, StringLocalizer.keyToString("SERVICE_OPTICAL_CAMERAS_KEY"));
        _centerTabPane.add(_eblePanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_EBLE_TAB_NAME_KEY"));
      }
    }
    _centerTabPane.add(_indicators, StringLocalizer.keyToString("SERVICE_UI_SSACTP_INDICATORS_TAB_NAME_KEY"));
    _centerTabPane.add(_smemaPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_SMEMA_TAB_NAME_KEY"));
    _centerTabPane.add(_interlocksPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_INTERLOCKS_TAB_NAME_KEY"));
    _centerTabPane.add(_motionControlPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_MOTION_CONTROL_TAB_NAME_KEY"));
    _centerTabPane.add(_digitialIoPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_DIGITAL_IO_TAB_NAME_KEY"));
    _centerTabPane.add(_imagingChainPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_IMAGING_CHAIN_TAB_NAME_KEY"));
    _centerTabPane.add(_hwSimulationPanel, StringLocalizer.keyToString("SERVICE_UI_SSACTP_HARDWARE_SIMULATION_CONTROL_TAB_NAME_KEY"));

    // Configure the tabs to scroll
    _centerTabPane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
    _currentTabIndex = _centerTabPane.getSelectedIndex();

    _unitsComboBox.setMaximumSize(_unitsComboBox.getPreferredSize());
  }

  /**
   * @author Erica Wheatcroft
   */
  SubsystemStatusAndControlTaskPanel(ServiceEnvironmentPanel parent)
  {
    super(parent);

    Assert.expect(parent != null);

    _parent = parent;

    createPanel();
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    _startingTask = false;

    // finish the last current visible tab
    if (_currentTabIndex >= 0)
    {
      TaskPanelInt panel = (TaskPanelInt)_centerTabPane.getComponentAt(_currentTabIndex);
      panel.finish();
    }

    // remove menu items
    finishMenusAndToolBar();

    // remove the listeners
    removeListners();

    // save UI state
    saveUIState();

    // now since we are finished with this piece lets set the current tab back to zero
    _currentTabIndex = 0;

    // re-enable
    if (XrayTester.getInstance().isHardwareAvailable() == false)
      _centerTabPane.setEnabledAt( _HARDWARE_SIM_PANEL,true);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    ServiceGuiPersistence.getInstance().setSelectedUnits(_selectedUnitToDisplayMeasurementsIn);
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    TaskPanelInt panel = (TaskPanelInt)_centerTabPane.getComponentAt(_currentTabIndex);
    return panel.isReadyToFinish();
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // we are starting the first tab in this set of tasks
    _startingTask = true;
    
    // setting up tab index, depending on software.config enablePsp true/false
    setupTabIndex();

    // create menu items
    createMenuItems();

    // add action listeners
    addListeners();

    changeTab();

    // we have started our first task so set the flag
    _startingTask = false;

    // load the UI persistence
    restoreLastUIState();

    // check the sim state
    updatePanelWithSimulationStatus();

    // if offline then do not allow hardware sim to be enabled
    if (XrayTester.getInstance().isHardwareAvailable() == false)
      _centerTabPane.setEnabledAt(_HARDWARE_SIM_PANEL, false);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupTabIndex()
  {
      if (_enablePsp == false)
      {
         _PANEL_HANDLING_PANEL = 0;
        _PANEL_POSITIONER_PANEL = 1;
        _LEGACY_XRAY_SOURCE_PANEL = 2;
        _XRAY_CAMERA_PANEL = 3;
        _INDICATORS_PANEL = 4;
        _SMT_COMMUNICATION_PANEL = 5;
        _INTERLOCKS_PANEL = 6;
        _MOTION_CONTROL_PANEL = 7;
        _DIGITAL_IO_PANEL = 8;
        _IMAGING_CHAIN_PANEL = 9;
        _HARDWARE_SIM_PANEL = 10; 
      }
      else
      {
        if (PspEngine.isVersion1())
        {
          _PANEL_HANDLING_PANEL = 0;
          _PANEL_POSITIONER_PANEL = 1;
          _LEGACY_XRAY_SOURCE_PANEL = 2;
          _XRAY_CAMERA_PANEL = 3;
          _OPTICAL_CAMERA_PANEL = 4;
          _MICROCONTROLLER_PANEL = 5;
          _INDICATORS_PANEL = 6;
          _SMT_COMMUNICATION_PANEL = 7;
          _INTERLOCKS_PANEL = 8;
          _MOTION_CONTROL_PANEL = 9;
          _DIGITAL_IO_PANEL = 10;
          _IMAGING_CHAIN_PANEL = 11;
          _HARDWARE_SIM_PANEL = 12;
        }
        else
        {
          _PANEL_HANDLING_PANEL = 0;
          _PANEL_POSITIONER_PANEL = 1;
          _LEGACY_XRAY_SOURCE_PANEL = 2;
          _XRAY_CAMERA_PANEL = 3;
          _OPTICAL_CAMERA_PANEL = 4;
          _EBLE_PANEL = 5;
          _INDICATORS_PANEL = 6;
          _SMT_COMMUNICATION_PANEL = 7;
          _INTERLOCKS_PANEL = 8;
          _MOTION_CONTROL_PANEL = 9;
          _DIGITAL_IO_PANEL = 10;
          _IMAGING_CHAIN_PANEL = 11;
          _HARDWARE_SIM_PANEL = 12;
        }
      }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void clearTabTitles()
  {
    _centerTabPane.setTitleAt(_PANEL_HANDLING_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_PANEL_POSITIONER_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_LEGACY_XRAY_SOURCE_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_XRAY_CAMERA_PANEL, StringLocalizer.keyToString("SERVICE_XRAY_CAMERAS_KEY"));
    if (_enablePsp)
    {
      if (PspEngine.isVersion1())
      {
        _centerTabPane.setTitleAt(_OPTICAL_CAMERA_PANEL, StringLocalizer.keyToString("SERVICE_OPTICAL_CAMERAS_KEY"));
        _centerTabPane.setTitleAt(_MICROCONTROLLER_PANEL, StringLocalizer.keyToString("SERVICE_PSPCONTROLLER_KEY"));
      }
      else
      {
        _centerTabPane.setTitleAt(_OPTICAL_CAMERA_PANEL, StringLocalizer.keyToString("SERVICE_OPTICAL_CAMERAS_KEY"));
        _centerTabPane.setTitleAt(_EBLE_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_EBLE_TAB_NAME_KEY"));
      }
    }
    _centerTabPane.setTitleAt(_INDICATORS_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_INDICATORS_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_SMT_COMMUNICATION_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_SMEMA_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_INTERLOCKS_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_INTERLOCKS_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_MOTION_CONTROL_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_MOTION_CONTROL_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_DIGITAL_IO_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_DIGITAL_IO_TAB_NAME_KEY"));
    _centerTabPane.setTitleAt(_IMAGING_CHAIN_PANEL, StringLocalizer.keyToString("SERVICE_UI_SSACTP_IMAGING_CHAIN_TAB_NAME_KEY"));
  }

  /**
   * @author Erica Wheatcroft
   */
  void updatePanelWithSimulationStatus()
  {
    int numberOfTabes = _centerTabPane.getTabCount();

    clearTabTitles();

    if(XrayTester.getInstance().isSimulationModeOn())
    {
      // since sim mode is on we need to update the title of all tabs except for the last one to reflect that.
      for(int i = 0; i < numberOfTabes - 1; i++)
      {
        String title = "<html>" + _centerTabPane.getTitleAt(i) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(i, title);
      }
    }
    else
    {
      // lets check each subsystem
      if(PanelHandler.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_PANEL_HANDLING_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_PANEL_HANDLING_PANEL, title);
      }

      if(PanelPositioner.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_PANEL_POSITIONER_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_PANEL_POSITIONER_PANEL, title);
      }

      if(AbstractXraySource.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_LEGACY_XRAY_SOURCE_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_LEGACY_XRAY_SOURCE_PANEL, title);
      }

      if (LightStack.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_INDICATORS_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_INDICATORS_PANEL, title);
      }

      if (ManufacturingEquipmentInterface.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_SMT_COMMUNICATION_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_SMT_COMMUNICATION_PANEL, title);
      }

      if(Interlock.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_INTERLOCKS_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_INTERLOCKS_PANEL, title);
      }
      
      if(EthernetBasedLightEngineProcessor.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_EBLE_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_EBLE_PANEL, title);
      }

      java.util.List<MotionControllerIdEnum> ids = MotionControllerIdEnum.getAllEnums();
      if(MotionControl.getInstance(ids.get(0)).isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_MOTION_CONTROL_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_MOTION_CONTROL_PANEL, title);
      }
      else
      {
        try
        {
          if (MotionControl.getInstance(ids.get(0)).isStartupRequired() == false)
          {
            if ( (MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS)) ||
                 (MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS)) ||
                 (MotionControl.getInstance(ids.get(0)).isSimulationModeOn(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS)) )
            {
              String title = "<html>" + _centerTabPane.getTitleAt(_MOTION_CONTROL_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
              _centerTabPane.setTitleAt(_MOTION_CONTROL_PANEL, title);
            }
          }
        }
        catch(XrayTesterException xte)
        {
          String messageToDisplay = StringUtil.format(xte.getMessage(), 50);
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                        messageToDisplay,
                                        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                        JOptionPane.ERROR_MESSAGE);

        }
      }

      if(XrayCameraArray.getInstance().isSimulationModeOn())
      {
        String title2 = "<html>" + _centerTabPane.getTitleAt(_XRAY_CAMERA_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_XRAY_CAMERA_PANEL, title2);
      }

      if(OpticalCameraArray.getInstance().isSimulationModeOn())
      {
        String title2 = "<html>" + _centerTabPane.getTitleAt(_OPTICAL_CAMERA_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_OPTICAL_CAMERA_PANEL, title2);
      }
      
      if (AbstractMicroController.getInstance().isSimulationModeOn() && AbstractProjector.getInstance().isSimulationModeOn())
      {
        String title2 = "<html>" + _centerTabPane.getTitleAt(_MICROCONTROLLER_PANEL) + " " + "<font color=#CC0000>" + _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_MICROCONTROLLER_PANEL, title2);  
      }

      if(DigitalIo.getInstance().isSimulationModeOn())
      {
        String title = "<html>" + _centerTabPane.getTitleAt(_DIGITAL_IO_PANEL) + " " + "<font color=#CC0000>"+ _SIM_ON + "</html>";
        _centerTabPane.setTitleAt(_DIGITAL_IO_PANEL, title);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void restoreLastUIState()
  {
    ServiceGuiPersistence persistance = ServiceGuiPersistence.getInstance();
    MathUtilEnum units = persistance.getSelectedUnits();

    if(units != null)
    {
      _selectedUnitToDisplayMeasurementsIn = units;
    }
    else
    {
      // since there is nothing persisted set the units to inches by default
      _selectedUnitToDisplayMeasurementsIn = MathUtilEnum.MILS;
    }

    _unitsComboBox.setSelectedItem(MeasurementUnits.getMeasurementUnitString(_selectedUnitToDisplayMeasurementsIn));
    GuiObservable.getInstance().stateChanged(_selectedUnitToDisplayMeasurementsIn, SelectionEventEnum.DISPLAY_UNITS);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _centerTabPane.addChangeListener(_centerTabPaneChangeListener);
    _unitsComboBox.addActionListener(_unitsComboBoxActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListners()
  {
    _centerTabPane.removeChangeListener(_centerTabPaneChangeListener);
    _unitsComboBox.removeActionListener(_unitsComboBoxActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createMenuItems()
  {
    // set up for custom menus and tool bar
    super.startMenusAndToolBar();


    // add custom toolbar components
     _envToolBar.addToolBarComponent(_envToolBarRequesterID, 	_unitsLabel);
     _envToolBar.addToolBarComponent(_envToolBarRequesterID, 	_unitsComboBox);
     
     _envToolBar.removeDefectPackagerButtons();
     _envToolBar.addDefectPackagerButtons();
  }

  /**
   * @author Erica Wheatcroft
   */
  ImageIcon getSparkIcon()
  {
    Assert.expect(_spark != null);
    return _spark;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ServiceEnvironmentPanel getEnvironment()
  {
    return _parent;
  }
}
