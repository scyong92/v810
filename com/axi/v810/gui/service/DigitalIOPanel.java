package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for displaying all digital io input and output bits to the user.
 *
 * @author Erica Wheatcroft
 */
class DigitalIOPanel extends JPanel implements TaskPanelInt, Observer
{
  private JPanel _digitalIOStatePanel = new JPanel();
  private JPanel _buttonPanel = new JPanel();
  private JPanel _inputBitsPanel = new JPanel();
  private JPanel _outputBitsPanel = new JPanel();
  private JPanel _outputBitsMainPanel = new JPanel();

  private JButton _showHardwareDescriptionButton = new JButton();
  private JButton _initializeButton = new JButton();

  private GridLayout _inputBitsPanelGridLayout = new GridLayout();
  private GridLayout _outputBitsPanelGridLayout = new GridLayout();

  private DigitalIo _digitalIo = null;

  private Map<String, DigitalIoBitLabel> _inputBitNameToLabelMap = null;
  private Map<String, DigitalIoOutputBitButton> _outputBitNameToButtonMap = null;

  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();

  private final int _NUMBER_OF_COLUMNS_TO_DISPLAY = 4;
  private final int _HORIZONTAL_GAP = 60;
  private final int _INPUT_BIT_PANEL_VERTICAL_GAP = 25;
  private final int _OUTPUT_BIT_PANEL_VERTICAL_GAP = 25;

  private int _numberOfOutputBitRows;
  private int _numberOfInputBitRows;
  private int _maxLabelWidth;
  private int _maxLabelHeight;
  private int _maxButtonWidth;
  private int _maxButtonHeight;

  private WorkerThread _monitorThread = new WorkerThread("Digital IO Monitor");
  private boolean _monitorEnabled = false;
  private boolean _initialized = false;

  private ProgressDialog _progressDialog = null;

  private static final ImageIcon _onStateIcon = Image5DX.getImageIcon(Image5DX.ON_STATE);
  private static final ImageIcon _offStateIcon = Image5DX.getImageIcon(Image5DX.OFF_STATE);
  private static final ImageIcon _errorOccurredStateIcon = Image5DX.getImageIcon(Image5DX.ERROR_STATE);

  private SubsystemStatusAndControlTaskPanel _parent = null;

  // listeners - i create them here so i can and and remove them as needed.
  private ActionListener _outputBitButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent event)
    {
      Assert.expect(event.getSource() instanceof JButton);
      JButton buttonPressed = (JButton) event.getSource();

      // now lets get the text which is the actual output bit name
      String text = buttonPressed.getText();
      Assert.expect(_outputBitNameToButtonMap.containsKey(text));
      DigitalIoOutputBitButton outputBitButton = _outputBitNameToButtonMap.get(text);

      // Prompt warning message whenever users try to lower down xray tube.
      // Only check this when xray cylinder is in up position.
      if (text.equals(StringLocalizer.keyToString("HW_XRAY_CYLINDER_UPDOWN_OUTPUT_KEY")))
      {
        try
        {
          if (_digitalIo.getOutputBitState(outputBitButton.getBitName()) == false)
          {
            String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_PANEL_XRAY_CYLINDER_DOWN_WARNING_KEY");

            int answer = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                            message,
                                            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                            JOptionPane.YES_NO_OPTION,
                                            JOptionPane.WARNING_MESSAGE);
            if (answer == JOptionPane.NO_OPTION)
                return;
          }
        }
        catch(XrayTesterException xte)
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(), "Error when trying to check if both PIP and clamp output bits.");
          return;
        }
      }
      //String text2=outputBitButton.getBitName();//.getText();
      // now lets toggle the state of that output bit.
      outputBitButton.toggleState();

      // now refresh the input bits to reflect the toggled state
      refreshInputBits();
    }
  };

  private ActionListener _showConfigurationDescription = new ActionListener()
  {
    public void actionPerformed(ActionEvent event)
    {
      HardwareConfigurationDescriptionInt description = _digitalIo.getConfigurationDescription();
      LocalizedString localizedString = new LocalizedString(description.getKey(),description.getParameters().toArray());
      String messageToDisplay = StringLocalizer.keyToString(localizedString);
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(messageToDisplay, 50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.PLAIN_MESSAGE);
    }
  };

  private ActionListener _initializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _monitorEnabled = false;

            // lets shutdown first
            _digitalIo.shutdown();

            // now lets start back up
            _digitalIo.startup();

            // its completed lets set our flag
            _initialized = true;

          }
          catch(final XrayTesterException xte)
          {
           SwingUtils.invokeLater(new Runnable()
           {
             public void run()
             {
               handleException(xte);
               _progressDialog.setVisible(false);
               _progressDialog = null;
             }
           });
          }
          finally
          {
            try
            {
              //re-check if digital Io is initialized successfully, populate the panel if it only fails at initializing inner barrier, z-axis or filter height sensor
              if (_digitalIo.isStartupRequired() == false)
              {
                _initialized = true;
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    // turn on the buttons
                    enableButtons();

                    // populate the UI
                    populatePanel();

                    //start monitoring
                    startMonitoring();
                  }
                });
              }
            }
            catch (XrayTesterException xte)
            {

            }
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  /**
   * @author Andy Mechtenberg
   */
  private ComponentAdapter _inputBitsComponentAdapter = new ComponentAdapter()
  {
    public void componentResized(ComponentEvent e)
    {
      sizeInputBitsPanel();
    }
  };

  /**
   * @author Andy Mechtenberg
   */
  private ComponentAdapter _outputBitsComponentAdapter = new ComponentAdapter()
  {
    public void componentResized(ComponentEvent e)
    {
      sizeOutputBitsPanel();
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  DigitalIOPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    ProgressObservable.getInstance().addObserver(this);
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    ProgressObservable.getInstance().deleteObserver(this);
    HardwareObservable.getInstance().deleteObserver(this);
  }


  /**
   * @author Erica Wheatcroft
   */
  private void refreshInputBits()
  {
    Assert.expect(_inputBitNameToLabelMap != null);
    
    Collection<DigitalIoBitLabel> labels =  _inputBitNameToLabelMap.values();
    try
    {
      for(DigitalIoBitLabel label : labels)
        label.refresh();
    }
    catch(XrayTesterException xte)
    {
      _monitorEnabled = false;
      handleException(xte);
    }

    try
    {
      if (XrayCylinderActuator.getInstance().isInstalled())
      {
        // monitor State Based On InputBit for Xray Cylinder Clamp
        DigitalIoOutputBitButton outputBitButton = _outputBitNameToButtonMap.get(_digitalIo.getXrayCylinderClampOutputBitString());
        if (outputBitButton != null)
        {
          outputBitButton.monitorStateBasedOnInputBit();
        }
      }
      if (InnerBarrier.isInstalled())
      {
        //Anthony Fong - Set initial inner barrier closed state
        DigitalIoOutputBitButton outputBitButton2 = _outputBitNameToButtonMap.get(_digitalIo.getInnerBarrierOutputBitString());
        if (outputBitButton2 != null)
        {
          outputBitButton2.monitorStateBasedOnInputBit();
        }
      }
    }
    catch (XrayTesterException xte)
    {
      _monitorEnabled = false;
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get an instance to the digital io interface
    _digitalIo = DigitalIo.getInstance();

    //XCR-3365 - Outer barrier will close when switch tab from Panel Handling tab to Digital IO (Anthony Fong)
    //remove the _digitalIo.closeAllOuterBarriersAfterPanelFailedToLoadUnload checking
    
    // lets check to see if we need to start it up.
    try
    {
      _initialized = ! _digitalIo.isStartupRequired();
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }

    if(_initialized)
    {
      //start monitoring
      startMonitoring();
    }
    // populate the UI
    populatePanel();

    enableButtons();

    // add the listeners
    addListeners();

    startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void enableButtons()
  {
    if(_initialized)
      _showHardwareDescriptionButton.setEnabled(true);
    else
      _showHardwareDescriptionButton.setEnabled(false);

    _initializeButton.setEnabled(true);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // stop monitoring
    stopMonitoring();

    // remove the listeners
    removeListeners();

    stopObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    Assert.expect(_digitalIo != null);

    // lets get the input keys and populate the ui with them

    if(_initialized == false)
    {
      // no inputs are defined
      _inputBitsPanel.removeAll();
      _inputBitsPanel.setLayout(new FlowLayout());
      _inputBitsPanel.add( new JLabel(StringLocalizer.keyToString("SERVICE_DIGITAL_IO_NO_INPUT_KEY")) );
    }
    else
    {
      _inputBitsPanel.removeAll();
      java.util.List<String> inputKeys = _digitalIo.getinputBitKeys();
      Assert.expect(inputKeys != null);

      int numberOfInputs = inputKeys.size();
      // lets figure out how many rows to create.
      int remainder = numberOfInputs % _NUMBER_OF_COLUMNS_TO_DISPLAY;
      _numberOfInputBitRows = (int) numberOfInputs / _NUMBER_OF_COLUMNS_TO_DISPLAY;
      if(remainder > 0)
        _numberOfInputBitRows++;

      _inputBitsPanelGridLayout.setColumns(_NUMBER_OF_COLUMNS_TO_DISPLAY);
      _inputBitsPanelGridLayout.setRows(_numberOfInputBitRows);
      _inputBitsPanelGridLayout.setHgap(_HORIZONTAL_GAP);
      _inputBitsPanelGridLayout.setVgap(_INPUT_BIT_PANEL_VERTICAL_GAP);
      _inputBitsPanel.setLayout(_inputBitsPanelGridLayout);
      _inputBitNameToLabelMap = new HashMap<String,DigitalIoBitLabel>();
      _maxLabelWidth = 0;
      _maxLabelHeight = 0;
      for(String inputKey : inputKeys)
      {
        DigitalIoBitLabel label = new DigitalIoBitLabel(inputKey, _digitalIo);
        Dimension labelDim = label.getPreferredSize();
        if (labelDim.getWidth() > _maxLabelWidth)
          _maxLabelWidth = (int)labelDim.getWidth();
        if (labelDim.getHeight() > _maxLabelHeight)
          _maxLabelHeight = (int)labelDim.getHeight() + 3;

        _inputBitNameToLabelMap.put(inputKey, label);
        _inputBitsPanel.add(label);
      }
    }

    // now lets get the output bits and do the same thing
    if(_initialized == false)
    {
      // no outputs are defined
      _outputBitsPanel.removeAll();
      _outputBitsPanel.setLayout(new FlowLayout());
      _outputBitsPanel.add(new JLabel(StringLocalizer.keyToString("SERVICE_DIGITAL_IO_NO_OUTPUT_KEY")));
    }
    else
    {
      // lets figure out how many rows to create.
      java.util.List<String> outputKeys = _digitalIo.getOutputBitKeys();
      Assert.expect(outputKeys != null);

      _outputBitsPanel.removeAll();

      int numberOfOutputs = outputKeys.size();
      int remainder = numberOfOutputs % _NUMBER_OF_COLUMNS_TO_DISPLAY;
      _numberOfOutputBitRows = (int)numberOfOutputs / _NUMBER_OF_COLUMNS_TO_DISPLAY;
      if (remainder > 0)
        _numberOfOutputBitRows++;

      _outputBitsPanelGridLayout.setColumns(_NUMBER_OF_COLUMNS_TO_DISPLAY);
      _outputBitsPanelGridLayout.setRows(_numberOfOutputBitRows);
      _outputBitsPanelGridLayout.setHgap(_HORIZONTAL_GAP);
      _outputBitsPanelGridLayout.setVgap(_OUTPUT_BIT_PANEL_VERTICAL_GAP);
      _outputBitsPanel.setLayout(_outputBitsPanelGridLayout);
      _outputBitNameToButtonMap = new HashMap<String, DigitalIoOutputBitButton>();

      _maxButtonWidth = 0;
      _maxButtonHeight = 0;
      for (String outputKey : outputKeys)
      {
        DigitalIoOutputBitButton button = new DigitalIoOutputBitButton(outputKey, _digitalIo);
        Dimension buttonDim = button.getPreferredSize();
        if (buttonDim.getWidth() > _maxButtonWidth)
          _maxButtonWidth = (int)buttonDim.getWidth();
        if (buttonDim.getHeight() > _maxButtonHeight)
          _maxButtonHeight = (int)buttonDim.getHeight() + 4;
        button.addActionListener(_outputBitButtonActionListener);
        _outputBitNameToButtonMap.put(outputKey, button);
        _outputBitsPanel.add(button);
      }
    }
    sizeOutputBitsPanel();
    sizeInputBitsPanel();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void sizeOutputBitsPanel()
  {
    if (_initialized)
    {
      // vgap
      int height = _outputBitsPanel.getHeight();
      int totalButtonHeight = _numberOfOutputBitRows * _maxButtonHeight;
      int remainderHeight = height - totalButtonHeight;
      int remainderSpacePerRow = (remainderHeight / _numberOfOutputBitRows);
      if (remainderSpacePerRow < 0)
        remainderSpacePerRow = 0;
      _outputBitsPanelGridLayout.setVgap(remainderSpacePerRow);

      // hgap
      int width = _outputBitsPanel.getWidth();
      int totalButtonWidth = _maxButtonWidth * _NUMBER_OF_COLUMNS_TO_DISPLAY;
      int remainderWidth = width - totalButtonWidth;
      int remainderWidthPerColumn = (remainderWidth / _NUMBER_OF_COLUMNS_TO_DISPLAY);
      if (remainderWidthPerColumn < 0)
        remainderWidthPerColumn = 0;
      _outputBitsPanelGridLayout.setHgap(remainderWidthPerColumn);

      _outputBitsPanel.revalidate();
      _outputBitsPanel.repaint();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void sizeInputBitsPanel()
  {
    if (_initialized)
    {
      // vgap
      int height = _inputBitsPanel.getHeight();
      int totalLabelHeight = _numberOfInputBitRows * _maxLabelHeight;
      int remainderHeight = height - totalLabelHeight;
      int remainderSpacePerRow = (remainderHeight / _numberOfInputBitRows);
      if (remainderSpacePerRow < 0)
        remainderSpacePerRow = 0;
      _inputBitsPanelGridLayout.setVgap(remainderSpacePerRow);

      // hgap
      int width = _inputBitsPanel.getWidth();
      int totalLabelWidth = _maxLabelWidth * _NUMBER_OF_COLUMNS_TO_DISPLAY;
      int remainderWidth = width - totalLabelWidth;
      int remainderWidthPerColumn = (remainderWidth / _NUMBER_OF_COLUMNS_TO_DISPLAY);
      if (remainderWidthPerColumn < 0)
        remainderWidthPerColumn = 0;
      _inputBitsPanelGridLayout.setHgap(remainderWidthPerColumn);

      _inputBitsPanel.revalidate();
      _inputBitsPanel.repaint();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _showHardwareDescriptionButton.removeActionListener(_showConfigurationDescription);
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _inputBitsPanel.removeComponentListener(_inputBitsComponentAdapter);
    _outputBitsPanel.removeComponentListener(_outputBitsComponentAdapter);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _showHardwareDescriptionButton.addActionListener(_showConfigurationDescription);
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _inputBitsPanel.addComponentListener(_inputBitsComponentAdapter);
    _outputBitsPanel.addComponentListener(_outputBitsComponentAdapter);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(10,5));
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 15));

    add(createButtonPanel(),BorderLayout.NORTH);

    _inputBitsPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(148, 145, 140)),StringLocalizer.keyToString("SERVICE_DIGITAL_IO_INPUT_BITS_KEY") ),
        BorderFactory.createEmptyBorder(15, 15, 15, 15)));
    _inputBitsPanel.setLayout(_inputBitsPanelGridLayout);

    _outputBitsMainPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(
        Color.white, new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_DIGITAL_IO_OUTPUT_BITS_KEY")),
        BorderFactory.createEmptyBorder(15, 15, 15, 15)));
    _outputBitsMainPanel.setLayout(new BorderLayout(0, 15));

    JPanel instructionPanel = new JPanel(new FlowLayout());
    JLabel instructionLabel = new JLabel(StringLocalizer.keyToString("SERVICE_CHANGE_STATE_LABEL_KEY"));
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
    instructionPanel.add(instructionLabel);
    _outputBitsMainPanel.add(instructionPanel, BorderLayout.NORTH);
    _outputBitsPanel.setLayout(_outputBitsPanelGridLayout);
    _outputBitsMainPanel.add(_outputBitsPanel, BorderLayout.CENTER);
    _digitalIOStatePanel.setLayout(new GridLayout(2, 1, 5, 10));
    _digitalIOStatePanel.add(_inputBitsPanel);
    _digitalIOStatePanel.add(_outputBitsMainPanel);


    add(_digitalIOStatePanel, BorderLayout.CENTER);

    _buttonPanel.setLayout(new FlowLayout());
    _showHardwareDescriptionButton.setText(StringLocalizer.keyToString("SERVICE_GET_HW_COFIG_DESCR_KEY"));
    _buttonPanel.add( createLegendPanel());

    add(_buttonPanel, BorderLayout.SOUTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createButtonPanel()
  {
    JPanel buttonPanel = new JPanel(new GridLayout(1,2,5,5));
    JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    leftPanel.add(_showHardwareDescriptionButton);
    buttonPanel.add(leftPanel);

    JPanel rightPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    rightPanel.add(_initializeButton);
    buttonPanel.add(rightPanel);

    return buttonPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private JPanel createLegendPanel()
  {
    JPanel legendPanel = new JPanel(new FlowLayout());
    JPanel innerLegendPanel = new JPanel(new GridLayout(1, 3, 10, 0));
    innerLegendPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.
        white, new Color(148, 145, 140)),StringLocalizer.keyToString("SERVICE_DIGITIAL_IO_LEGEND_KEY")), BorderFactory.createEmptyBorder(10, 50, 10, 50)));

    JLabel onStateLabel = new JLabel();
    onStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    onStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    onStateLabel.setIcon(_onStateIcon);
    onStateLabel.setText(StringLocalizer.keyToString("SERVICE_ON_BUTTON_TEXT_KEY"));
    innerLegendPanel.add(onStateLabel);

    JLabel offStateLabel = new JLabel();
    offStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    offStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    offStateLabel.setIcon(_offStateIcon);
    offStateLabel.setText(StringLocalizer.keyToString("SERVICE_OFF_BUTTON_TEXT_KEY"));
    innerLegendPanel.add(offStateLabel);

    JLabel unknownStateLabel = new JLabel();
    unknownStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    unknownStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    unknownStateLabel.setIcon(_errorOccurredStateIcon);
    unknownStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_ERROR_KEY"));
    innerLegendPanel.add(unknownStateLabel);

    legendPanel.add(innerLegendPanel);
    return legendPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+" - "+ StringLocalizer.keyToString("SERVICE_UI_SSACTP_DIGITAL_IO_TAB_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZING_MSG_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }


  /**
   * @author Erica Wheatcroft
   */
  private synchronized void startMonitoring()
  {
    //Siew Yeng 
    //This is to check if monitor thread is already running. Run it only when it is not running to avoid creating extra thread.
    if(!_monitorEnabled)
    {
      _monitorEnabled = true;
      _monitorThread.invokeLater(new Runnable()
      {
        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                refreshInputBits();
              }
            });
            try
            {
              Thread.sleep(500);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }

          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof DigitalIoEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(DigitalIoEventEnum.INITIALIZE))
                _progressDialog.updateProgressBarPrecent(100);
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

}
