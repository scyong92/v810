package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft (original LegacyXraySourcePanel)
 * @author George Booth (leveraged from LegacyXraySourcePanel for the Hamamatsu Xray Source, aka HTube)
 */
class HTubeXraySourcePanel extends AbstractXraySourcePanel implements TaskPanelInt, Observer
{
  private JScrollPane _errorMessagesTextAreaScrollPane = new JScrollPane();
  private JTextArea _errorMessageTextArea = new JTextArea();
//  private JButton _turnXraysOffForImagingButton = new JButton();
  private JButton _turnXrayPowerOffButton = new JButton();
  private JButton _clearErrorMessagesButton = new JButton();
  private JButton _turnXraysOnButton = new JButton();
  private JButton _initializeButton = new JButton();
  private JButton _innerBarrierCommandButton = new JButton();
  private JButton _selfTestCommandButton = new JButton();
  private JLabel _xrayStatusLabel = new JLabel();
  private JPanel _firmwareRevisionPanel = new JPanel();
  private JLabel _firmwareRevisionLabel = new JLabel();
  private JLabel _gridValueLabel = new JLabel();
  private JLabel _powerOnTimeLabel = new JLabel();
  private JLabel _xrayEmissionTimeLabel = new JLabel();
  private JLabel _inputSupplyLabel = new JLabel();
  private JLabel _operationalStatusLabel = new JLabel();
  private JLabel _warmupModeLabel = new JLabel();
  private JLabel _preheatMonitorLabel = new JLabel();
  private JLabel _gunTempLabel = new JLabel();
  private JLabel _currentAnodeValueLabel = new JLabel();
  private JLabel _currentCathodeValueLabel = new JLabel();
  private JLabel _currentFocalSpotValueLabel = new JLabel();
  private JLabel _innerBarrierStateLabel = new JLabel();
  private JLabel _selfTestStateLabel = new JLabel();
  private NumericTextField _anodeTextField = new NumericTextField();
  private NumericTextField _cathodeTextField = new NumericTextField();
  private NumericTextField _focalSpotTextField = new NumericTextField();
  private Box _mainBox = Box.createVerticalBox();
  private ProgressDialog _progressDialog = null;

  private static final int _NUMBER_OF_DECIMAL_PLACES = 2;
  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");
  private static final String _OPEN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPEN_STATE_KEY");
  private static final String _CLOSED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLOSED_STATE_KEY");

  private HTubeXraySource _xraySource = null;
  private DigitalIo _digitialIo = null;
  private InnerBarrier _innerBarrier = null;

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private WorkerThread _monitorThread = new WorkerThread("Xray Source Monitor");
  private boolean _subsystemInitialized = false;

  private boolean _monitorEnabled = false;
  private boolean _initializingSubsystem = false;
  private boolean _shuttingDownSystem = false;
  private boolean _shuttingDownComplete = false;
  private boolean _startingSystem = false;
  private boolean _startingComplete = false;
  private boolean _populatingUiControls = false;
  private boolean _runningSelfTest = false;

  private boolean _oneTimeOnly = true;
  private int _powerOnTime;
  private int _xrayEmissionTime;
  private boolean _handlingAnodeTextField = false;
  private boolean _handlingCathodeTextField = false;
  private boolean _handlingFocalSpotTextField = false;

  // default values or what the user typed in
  private int _lastVoltageProgrammed = 0;
  private int _lastCurrentProgrammed = 0;
  private int _lastFocalSpotModeProgrammed = 0;

  // Wei Chin added for warming up xray
  private JButton _quickWarmupXrayButton = new JButton();
  private JButton _slowWarmupXrayButton = new JButton();

//  private boolean _firstMessageDisplayed = false;

  private SubsystemStatusAndControlTaskPanel _parent = null;

  // action listeners - i create them here so i can add and remove them as needed.
  private ActionListener _selfTestCommandButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
//      _firstMessageDisplayed = false;
      // self test progress bar
      createProgressDialog(true,
                           StringLocalizer.keyToString("SERVICE_UI_HTUBE_RUNNING_SELF_TEST_MSG_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+"-"+StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_HTUBE_SELF_TEST_DONE_MSG_KEY"));

      // xrays are on during self test
//      displayXraysAreOn();

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _selfTestStateLabel.setText("Running...");
            _selfTestStateLabel.setForeground(Color.BLACK);
            _runningSelfTest = true;

            _xraySource.performSelfTest();
            _xraySource.logSelfTestResults();

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                populateSelfTestPanel();

//                displayXraysAreOff();

                _errorMessageTextArea.append(_xraySource.getSelfTestResultsString());
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _parent.handleXrayTesterException(xte);
                _selfTestStateLabel.setForeground(Color.RED);
                _selfTestStateLabel.setText(_UNKNOWN);
                _selfTestCommandButton.setText(_UNKNOWN);
                _progressDialog.setVisible(false);
                _progressDialog = null;
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;

      _runningSelfTest = false;
//      displayXraysAreOff();
    }
  };
  private ActionListener _innerBarrierCommandButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_innerBarrier != null);

//      _firstMessageDisplayed = false;
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (_innerBarrier.isOpen())
              _innerBarrier.close();
            else
              _innerBarrier.open();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _parent.handleXrayTesterException(xte);
                _innerBarrierStateLabel.setForeground(Color.RED);
                _innerBarrierStateLabel.setText(_UNKNOWN);
                _innerBarrierCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _initializeActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
//      _firstMessageDisplayed = false;
      // turn off xrays for imaging only
      createProgressDialog(true,
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZING_MSG_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+"-"+StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"));

      // xrays are on during warm up
//      displayXraysAreOn();

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // make sure xraySource is initialized
            boolean isStartupRequired = _xraySource.isStartupRequired();

            _initializingSubsystem = true;
            _monitorEnabled = false;

            //Siew Yeng - XCR1426 
            // xraySource must shut down 1st before shutting down digitalIo. This is because 
            // xraySource need to get innerBarrier status from digitalIo when shutting down, 
            // if digitalIo is shut down, xraySource could not get innerBarrier status it will 
            // then call for digitalIo initialization. _digitalIo.startup() will then initialize 
            // digitalIo again. 
            // shutdown xray source
            if(isStartupRequired == false)
              _xraySource.shutdown();

            // shutdown digitial io
            if(isStartupRequired == false)
              _digitialIo.shutdown();
            
            //startup digitial io
            _digitialIo.startup();

            // start monitoring.
            startMonitoring();

            // startup xray source
            _xraySource.startup();

            _subsystemInitialized = true;

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                enablePanel();

                // populate the panel
                populatePanel();

                // start monitoring.
                startMonitoring();

//                displayXraysAreOff();
              }
            });
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;

      _initializingSubsystem = false;
    }
  };
//  private ActionListener _turnXraysOffForImagingButtonActionListener = new ActionListener()
//  {
//    public void actionPerformed(ActionEvent e)
//    {
//      _firstMessageDisplayed = false;
//      createProgressDialog(false,
//                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURNING_OFF_CURRENT_KEY"),
//                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
//                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_CURRENT_OFF_KEY"));
//
//      // turn off xrays for imaging only
//      _hardwareWorkerThread.invokeLater(new Runnable()
//      {
//        public void run()
//        {
//          try
//          {
//            _xraySource.offForServiceMode();
//
//            SwingUtils.invokeLater(new Runnable()
//            {
//              public void run()
//              {
//                updateXrayStatus();
//              }
//            });
//          }
//          catch (XrayTesterException xte)
//          {
//            handleXrayTesterException(xte);
//            _progressDialog.setVisible(false);
//            _progressDialog = null;
//
//          }
//        }
//      });
//      _progressDialog.setVisible(true);
//      _progressDialog = null;
//    }
//  };
  private ActionListener _turnXrayPowerOffButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
//      _firstMessageDisplayed = false;
    
      createProgressDialog(false,
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURNING_OFF_CURRENT_AND_VOLTAGE_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_CURRENT_AND_VOLTAGE_OFF_KEY"));

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.offForServiceMode();

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                updateXrayStatus();
              }
            });
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  private ActionListener _turnXraysOnButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
//      _firstMessageDisplayed = false;
    
      if (_xraySource.isWarmStartAllowed())
        createProgressDialog(false,
//                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_WARM_START_MSG_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));
      else
        createProgressDialog(false,
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_COLD_START_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if(_xraySource.isStartupRequired())
              _xraySource.startup();
                    
            _xraySource.setXraySourceParametersForServiceMode(_lastVoltageProgrammed,
                                                              _lastCurrentProgrammed,
                                                              _lastFocalSpotModeProgrammed);
            _xraySource.on();

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                updateXrayStatus();
              }
            });
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
//          catch (ParseException pe)
//          {
//            // should not happen
//            Assert.expect(false, "Unexpected parse expection");
//          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  private ActionListener _clearErrorMessagesButtonActionListner = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
//      _firstMessageDisplayed = false;
      try
      {
        _xraySource.resetHardwareReportedErrors();
      }
      catch (XrayTesterException he)
      {
        handleXrayTesterException(he);
      }
      try
      {
        _errorMessageTextArea.getDocument().remove(0, _errorMessageTextArea.getDocument().getLength());
      }
      catch (javax.swing.text.BadLocationException ex)
      {
        // this should not happen
        Assert.expect(false);
      }

    }
  };
  private ActionListener _anodeTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
//      _firstMessageDisplayed = false;
      handleAnodeTextFieldEntry();
    }
  };
  private FocusListener _anodeTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
      _anodeTextField.setText(Double.toString(_lastVoltageProgrammed));
      _anodeTextField.selectAll();
      _populatingUiControls = false;
    }

    public void focusLost(FocusEvent evt)
    {
      handleAnodeTextFieldEntry();
    }
  };

  private ActionListener _cathodeTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
//      _firstMessageDisplayed = false;
      handleCathodeTextFieldEntry();
    }
  };

  private FocusListener _cathodeTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
     _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));
      _cathodeTextField.selectAll();
     _populatingUiControls = false;

    }

    public void focusLost(FocusEvent evt)
    {
      handleCathodeTextFieldEntry();
    }
  };

  private ActionListener _focalSpotTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
//      _firstMessageDisplayed = false;
      handlefocalSpotTextFieldEntry();
    }
  };

  private FocusListener _focalSpotTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
      _focalSpotTextField.setText(Integer.toString(_lastFocalSpotModeProgrammed));
      _focalSpotTextField.selectAll();
      _populatingUiControls = false;
    }

    public void focusLost(FocusEvent evt)
    {
      handlefocalSpotTextFieldEntry();
    }
  };
  
  // Wei Chin addded
  private ActionListener _quickWarmupXrayButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog(false,
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_COLD_START_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));
      
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            //call xray quick warmup
            _xraySource.manualWarmupXRays(_xraySource.FAST_WARMUP_MODE);
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                // do something
                handleXrayTesterException(xte);
                _progressDialog.setVisible(false);
                _progressDialog = null;
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  
  // Wei Chin addded
  private ActionListener _slowWarmupXrayButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog(false,
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_COLD_START_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            //call xray Slow warmup
            _xraySource.manualWarmupXRays(_xraySource.SLOW_WARMUP_MODE);
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                // do something
                handleXrayTesterException(xte);
                _progressDialog.setVisible(false);
                _progressDialog = null;  
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  HTubeXraySourcePanel(SubsystemStatusAndControlTaskPanel parent)
  {
    super(parent);
    Assert.expect(parent != null);

    _parent = parent;

    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enablePanel()
  {
    _errorMessagesTextAreaScrollPane.setEnabled(_subsystemInitialized);
    _errorMessageTextArea.setEnabled(_subsystemInitialized);
    _clearErrorMessagesButton.setEnabled(_subsystemInitialized);
    try
    {
      if (LicenseManager.isDeveloperSystemEnabled() && Config.isDeveloperDebugModeOn())
      {
        _anodeTextField.setEnabled(_subsystemInitialized);
        _cathodeTextField.setEnabled(_subsystemInitialized);
        _focalSpotTextField.setEnabled(_subsystemInitialized);
      }
      else
      {
        _anodeTextField.setEnabled(false);
        _cathodeTextField.setEnabled(false);
        _focalSpotTextField.setEnabled(false);
      }
    }
    catch (BusinessException ex)
    {
      _anodeTextField.setEnabled(false);
      _cathodeTextField.setEnabled(false);
      _focalSpotTextField.setEnabled(false);
    }
    _selfTestCommandButton.setEnabled(_subsystemInitialized);
    _innerBarrierCommandButton.setEnabled(_subsystemInitialized);
  }

  /**
   * Checks the power level and returns adjusted current if needed
   *
   * @author George Booth
   */
  private double getCurrentForSpotMode(double voltage, double current, int mode)
  {
    // the maximum allowed power level depends on the focal spot mode:
    //    small focal spot  (0) = 8 watts
    //    medium focal spot (1) = 16 watts
    //    large focal spot  (2) = 39 watts (limited by max V and I)
    double power = (voltage * current) / 1000.0;
    int adjustedCurrent = 0;
    boolean needToAdjustCurrent = false;
    // don't respond to text field change
    _handlingCathodeTextField = true;

    if (mode == 0 && power > 8.0)
    {
      adjustedCurrent = (int)(1000.0 * (8.0 / voltage));
      needToAdjustCurrent = true;
      _cathodeTextField.setText(Double.toString(adjustedCurrent));
      // display the message
      String message = StringLocalizer.keyToString("SERVICE_INVALID_SMALL_SPOT_SIZE_POWER_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("Invalid Power Level"),
                                    JOptionPane.ERROR_MESSAGE);
    }

    if (mode == 1 && power > 16.0)
    {
      // adjust the current
      adjustedCurrent = (int)(1000.0 * (16.0 / voltage));
      needToAdjustCurrent = true;
      _cathodeTextField.setText(Double.toString(adjustedCurrent));
      // display the message
      String message = StringLocalizer.keyToString("SERVICE_INVALID_MEDIUM_SPOT_SIZE_POWER_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("Invalid Power Level"),
                                    JOptionPane.ERROR_MESSAGE);
    }
    _handlingCathodeTextField = false;

    if (needToAdjustCurrent)
    {
      return adjustedCurrent;
    }
    else
    {
      return current;
    }
  }

  /**
   * Checks the power level and adjusts the level if needed
   *
   * @author George Booth
   */
  private void checkAndAdjustPowerLevel() throws HeadlessException
  {
    // get values to be programmed if xrays were on
//    double voltage = _currentAnodeValue; // kV
//    double current = _currentCathodeValue; // uA
//    int focalSpotSize = _currentFocalSpotValue;
    double voltage = 0.0f;
    double current = 0.0f;
    int focalSpotMode = 0;
//    try
//    {
      voltage = _lastVoltageProgrammed; // _anodeTextField.getNumberValue().doubleValue(); // kV
      current = _lastCurrentProgrammed; // _cathodeTextField.getNumberValue().doubleValue(); // uA
      focalSpotMode = _lastFocalSpotModeProgrammed; // _focalSpotTextField.getNumberValue().intValue();
//    }
//    catch (ParseException pe)
//    {
//      // should not happen
//      Assert.expect(false, "Unexpected parse expection");
//    }

    final double adjustedCurrent = getCurrentForSpotMode(voltage, current, focalSpotMode);

    if (adjustedCurrent != current)
    {
      // adjust the current and wait for it to reach the required value
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // adjust the current if the source is on
            if (_xraySource.couldUnsafeXraysBeEmitted())
            {
              // wait for current to reach correct value
              _xraySource.setCathodeCurrentInMicroAmpsAndTrack(adjustedCurrent);
            }
            _lastCurrentProgrammed = (int)adjustedCurrent;
            _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
  }

  /**
   * @author George Booth
   */
  private void handlefocalSpotTextFieldEntry() throws HeadlessException
  {
    if(_populatingUiControls || _handlingFocalSpotTextField)
      return;

    // get the value of focal spot
    int focalSpot = 0;
    try
    {
      focalSpot = _focalSpotTextField.getNumberValue().intValue();
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _focalSpotTextField.setText(Integer.toString(_lastFocalSpotModeProgrammed));
    }
    // needs to be final for invokeLater()
    final int focalSpotMode = focalSpot;

    // don't re-send the same value - it disturbs the source stability circuits
    if (focalSpotMode == _lastFocalSpotModeProgrammed)
    {
      return;
    }

    _handlingFocalSpotTextField = true;
    boolean xraysAreOn = false;
    try
    {
      // spot size can't be changed while xrays are on
      xraysAreOn = _xraySource.couldUnsafeXraysBeEmitted();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      return;
    }
    // needs to be final for invokeLater()
    final boolean xraysWereOn = xraysAreOn;

    // for the focal spot the only allowed values are 0 - 2
    // lets check to make sure that the value entered is correct.
    if (focalSpotMode < 0 || focalSpotMode > 2)
    {
      _focalSpotTextField.setText(Integer.toString(_lastFocalSpotModeProgrammed));
      // the value entered is not correct. Notify the user
      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_FOCAL_SPOT_VALUE_KEY",
                                                                       new Object[]
                                                                       {focalSpot}));
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("SERVICE_INVALID_FOCAL_SPOT_VALUE_TITLE_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
      _handlingFocalSpotTextField = false;
      return;
    }

    // update current value so textfield.focusGained() doesn't set the wrong value
    _lastFocalSpotModeProgrammed = focalSpotMode;
    // check and adjust power level if needed
    checkAndAdjustPowerLevel();

    // Spot size can't be changed while xrays are on.
    // If they were on, we will need to cycle xray power to change spot size.
    if (xraysWereOn)
    {
      // focal spot change progress bar
      createProgressDialog(true,
                           StringLocalizer.keyToString("SERVICE_UI_CHANGING_FOCAL_SPOT_MODE_MSG_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY") + "-" + StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_CHANGING_FOCAL_SPOT_MODE_DONE_MSG_KEY"));

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            //Turn them off.
            _xraySource.offForServiceMode();

            _xraySource.setFocalSpotMode(focalSpotMode);

            // Turn them back on.
            _xraySource.on();
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
    else
    {
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setFocalSpotMode(focalSpotMode);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    _handlingFocalSpotTextField = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleCathodeTextFieldEntry()
  {
    if(_populatingUiControls || _handlingCathodeTextField)
    {
//      System.out.println(" handleCathodeTextFieldEntry() ignored - populating UI or handling cathode TextField");
      return;
    }

    try
    {
      final double current = _cathodeTextField.getNumberValue().doubleValue();

      // don't re-send the same value - it disturbs the source stability circuits
      if (current == _lastCurrentProgrammed) //  && _adjustingCurrent == false)
      {
//        System.out.println(" handleCathodeTextFieldEntry() ignored - trying to resend same value");
        return;
      }

//      System.out.println("handleCathodeTextFieldEntry()");

      _handlingCathodeTextField = true;

      // the cathode value must be between 0 and 300
      if (current < 0 || current > 300)
      {
        _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));

        // the user has entered a invalid value, display the message and return out
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_CATHODE_VALUE_KEY",
                                                                         new Object[]
                                                                         {current}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("Invalid Cathode Value"),
                                      JOptionPane.ERROR_MESSAGE);
        _handlingCathodeTextField = false;
        return;
      }

      // check and adjust power level if needed
      final double adjustedCurrent = getCurrentForSpotMode(_lastVoltageProgrammed,  //anodeTextField.getNumberValue().doubleValue(),
                                                           current,
                                                           _lastFocalSpotModeProgrammed); //_focalSpotTextField.getNumberValue().intValue());
      // update current value so textfield.focusGained() doesn't set the wrong value
      _lastCurrentProgrammed = (int)adjustedCurrent;

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setCathodeCurrentInMicroAmps(_lastCurrentProgrammed);
            if (adjustedCurrent != current)
            {
              // update text field
              _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));
            }
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
     _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));
    }
    _handlingCathodeTextField = false;
  }



  /**
   * @author Erica Wheatcroft
   */
  private void handleAnodeTextFieldEntry()
  {
    if(_populatingUiControls || _handlingAnodeTextField)
      return;

    try
    {
      final double voltage = _anodeTextField.getNumberValue().doubleValue();

      // don't re-send the same value - it disturbs the source stability circuits
      if (voltage == _lastVoltageProgrammed)
      {
        return;
      }

      _handlingAnodeTextField = true;

      // the anode value must be between 0 and 130
      if (voltage < 0 || voltage > 130)
      {
        _anodeTextField.setText(Double.toString(_lastVoltageProgrammed));
        // the user has entered a invalid value, display the message and return out
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_ANODE_VALUE_KEY",
                                                                         new Object[]
                                                                         {voltage}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("Invalid Anode Value"),
                                      JOptionPane.ERROR_MESSAGE);
        _handlingAnodeTextField = false;
        return;
      }

      // update value so textfield.focusGained() doesn't set the wrong value
      _lastVoltageProgrammed = (int)voltage;
      // check and adjust power level if needed
      checkAndAdjustPowerLevel();

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setAnodeVoltageInKiloVolts(voltage);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original value displayed in the label
      _anodeTextField.setText(Double.toString(_lastVoltageProgrammed));
    }
    _handlingAnodeTextField = false;
  }


  /**
    * @author Erica WHeatcroft
    */
   private void startObserving(boolean start)
   {
     if (start)
     {
       ProgressObservable.getInstance().addObserver(this);
       HardwareObservable.getInstance().addObserver(this);
     }
     else
     {
       ProgressObservable.getInstance().deleteObserver(this);
       HardwareObservable.getInstance().deleteObserver(this);
     }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    startObserving(true);

    createHardwareHandles();

    enablePanel();

    // populate the UI
    populatePanel();

    // add the listeners
    addListeners();

    if(_subsystemInitialized)
      startMonitoring();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createHardwareHandles()
  {
    _digitialIo = DigitalIo.getInstance();
    _innerBarrier = InnerBarrier.getInstance();
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    _xraySource = (HTubeXraySource)xraySource;

    try
    {
      _subsystemInitialized = ! _digitialIo.isStartupRequired();
    }
    catch(XrayTesterException xte)
    {
      _subsystemInitialized = false;
      _parent.handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog(boolean initializingSubsystem,
                                    String message,
                                    String title,
                                    String operationCompletedMessage)
  {
    if (initializingSubsystem == false)
    {
      // now lets create the progress bar.
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           title,
                                           message,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY") + " ",
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);

      // now create the listner tied to the cancel button for the progress bar dialog.
      _progressDialog.addCancelActionListener(new ActionListener()
      {
        public void actionPerformed(java.awt.event.ActionEvent e)
        {
          try
          {
            _xraySource.abort();
          }
          catch (XrayTesterException xrayTesterException)
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringUtil.format(xrayTesterException.getMessage(), 50),
                                          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                          JOptionPane.ERROR_MESSAGE);
          }
          finally
          {
            _progressDialog.dispose();
          }
        }
      });
    }
    else
    {
      // now lets create the progress bar.
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           title,
                                           message,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);
    }
    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());

  }


  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    // set values to be programmed
    _lastVoltageProgrammed = _xraySource.getImagingVoltage();
    _anodeTextField.setText(Double.toString(_lastVoltageProgrammed));

    _lastCurrentProgrammed = _xraySource.getImagingCurrent();
    _cathodeTextField.setText(Double.toString(_lastCurrentProgrammed));

    _lastFocalSpotModeProgrammed = _xraySource.getImagingFocalSpotMode();
    _focalSpotTextField.setText(Integer.toString(_lastFocalSpotModeProgrammed));

    // update the error message text area if messages exist.
    updateMessagesAndFirmwareVersion();
    
    // lets check for xrays
    HardwareWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            updateXrayStatus();   
          }
        });
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateMessagesAndFirmwareVersion()
  {
    if(_subsystemInitialized == false)
      return;

    try
    {
      XraySourceHardwareReportedErrorsException error = _xraySource.getHardwareReportedErrors();
      if (error != null)
        _errorMessageTextArea.append(error.getLocalizedMessage());
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
    }

    // Commented by LeeHerng - This is to make sure we never display our X-ray source firmware in the GUI
    // so that we will not reveal our X-ray model to our competitors.
    /*
    try
    {
      // highlight for HTube
      _firmwareRevisionPanel.setBackground(Color.YELLOW);
      _firmwareRevisionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_FIRMWARE_REV_KEY") +
                                     " " +
                                     _xraySource.getFirmwareRevisionCode());
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
      _firmwareRevisionLabel.setText("");
    }
    */
    _firmwareRevisionLabel.setText("");
  }

  /**
   * @author Erica WHeatcroft
   */
  private void handleXrayTesterException(final XrayTesterException xte)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      showXrayTesterException(xte);
    }
    else
    {
      try
      {
        SwingUtils.invokeAndWait(new Runnable()
        {

          public void run()
          {
            showXrayTesterException(xte);
          }
        });
      }
      catch (InterruptedException iex)
      {
        Assert.logException(iex);
      }
      catch (InvocationTargetException itex)
      {
        Assert.logException(itex);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showXrayTesterException(XrayTesterException xte)
  {
    _subsystemInitialized = false;

    enablePanel();
    // repopulate to show that the subsystem needs to reinitialized
    populatePanel();

    // stop thread that are polling
    stopMonitoring();

    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateXrayParameters()
  {
    // don't change current values if we are handling text field changes!
    if (_handlingAnodeTextField || _handlingCathodeTextField || _handlingFocalSpotTextField)
      return;

//    System.out.println("--- Servivce UI - updateXrayParameters ----------");
    try
    {
      final double currentAnodeValue = _xraySource.getAnodeVoltageInKiloVolts();

      final double currentCathodeValue = _xraySource.getCathodeCurrentInMicroAmps();

      final int currentFocalSpotValue = _xraySource.getFocalSpotMode();

      final double inputSupplyValue = MathUtil.roundToPlaces(_xraySource.getInputSupplyVoltageInVolts(),
                                                                _NUMBER_OF_DECIMAL_PLACES);

      if (_oneTimeOnly)
      {
        _powerOnTime = _xraySource.getPowerOnTime();

        _xrayEmissionTime = _xraySource.getXrayEmissionTime();
        _oneTimeOnly = false;
      }

      final String operationalStatusString = _xraySource.getOperationalStatusString();

      final String warmupModeString = _xraySource.getWarmupModeString();

      final String preheatMonitorString = _xraySource.getPreheatMonitorString();

      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          updateXrayStatusLabel();
          
          _currentAnodeValueLabel.setText(Double.toString(currentAnodeValue));
          _currentCathodeValueLabel.setText(Double.toString(currentCathodeValue));
          _currentFocalSpotValueLabel.setText(Integer.toString(currentFocalSpotValue));

          _gunTempLabel.setText("N/A"); // Double.toString(gunTempValue));
          _gridValueLabel.setText("N/A"); // Double.toString(gridVoltageValue));
          _inputSupplyLabel.setText(Double.toString(inputSupplyValue));

          _powerOnTimeLabel.setText(Integer.toString(_powerOnTime));
          _xrayEmissionTimeLabel.setText(Integer.toString(_xrayEmissionTime));
          _operationalStatusLabel.setText(operationalStatusString);
          _warmupModeLabel.setText(warmupModeString);
          _preheatMonitorLabel.setText(preheatMonitorString);

          // self can be run if status is STANDBY
          _selfTestCommandButton.setEnabled(operationalStatusString.equals("STS 2"));
        }
      });
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateInnerBarrierPanel()
  {
    if (_subsystemInitialized == false)
    {
      _innerBarrierStateLabel.setForeground(Color.BLACK);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (_innerBarrier.isInstalled())
      {
        if (_innerBarrier.isOpen())
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_OPEN);
          _innerBarrierCommandButton.setText(_CLOSED);
        }
        else
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_CLOSED);
          _innerBarrierCommandButton.setText(_OPEN);
        }
      }
      else
      {
        _innerBarrierStateLabel.setForeground(Color.RED);
        _innerBarrierStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
        _innerBarrierCommandButton.setEnabled(false);
      }
    }
    catch (XrayTesterException xte)
    {
      _parent.handleXrayTesterException(xte);
      _innerBarrierStateLabel.setForeground(Color.RED);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateSelfTestPanel()
  {
    if (_subsystemInitialized == false)
    {
      _selfTestStateLabel.setForeground(Color.BLACK);
      _selfTestStateLabel.setText(_UNKNOWN);
      _selfTestCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      HTubeXraySourceStatusEnum status = _xraySource.getSelfTestStatus();
      if (status.equals(HTubeXraySourceStatusEnum.SELF_TEST_DONE))
      {
        _selfTestStateLabel.setForeground(Color.BLACK);
        _selfTestStateLabel.setText("Done");
        _selfTestCommandButton.setText("Start");
      }
      else if (status.equals(HTubeXraySourceStatusEnum.SELF_TEST_FAILED))
      {
        _selfTestStateLabel.setForeground(Color.RED);
        _selfTestStateLabel.setText("FAILED");
        _selfTestCommandButton.setText("Start");
      }
      else if (status.equals(HTubeXraySourceStatusEnum.SELF_TEST_PASSED))
      {
        _selfTestStateLabel.setForeground(Color.BLACK);
        _selfTestStateLabel.setText("Passed");
        _selfTestCommandButton.setText("Start");
      }
      else if (status.equals(HTubeXraySourceStatusEnum.SELF_TEST_NOT_RUN))
      {
        _selfTestStateLabel.setForeground(Color.BLACK);
        _selfTestStateLabel.setText("Not Run");
        _selfTestCommandButton.setText("Start");
      }
      else if (status.equals(HTubeXraySourceStatusEnum.SELF_TEST_RUNNING))
      {
        _selfTestStateLabel.setForeground(Color.BLACK);
        _selfTestStateLabel.setText("Running...");
        _selfTestCommandButton.setText("Stop");
      }
    }
    catch (XrayTesterException xte)
    {
      _parent.handleXrayTesterException(xte);
      _selfTestStateLabel.setForeground(Color.RED);
      _selfTestStateLabel.setText(_UNKNOWN);
      _selfTestCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void checkVoltage()
  {
    if(_subsystemInitialized)
    {
      try
      {
        if (_xraySource.isAnodeVoltageOff())
        {
          // its off so the user can't tweak the current
          _cathodeTextField.setEnabled(false);
          _focalSpotTextField.setEnabled(false);
        }
        else
        {
          // its on so the user can continue
          _cathodeTextField.setEnabled(true);
          _focalSpotTextField.setEnabled(true);
        }
      }
      catch(XrayTesterException xte)
      {
        _parent.handleXrayTesterException(xte);
      }
    }
  }

  /**
   * @uthor Erica Wheatcroft
   */
  private void updateXrayStatus()
  {
    updateXrayStatusLabel();
    
    if (_subsystemInitialized == false)
    {
      _xrayStatusLabel.setForeground(Color.BLACK);
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SUBSYSTEM_NOT_INITIALIZED_KEY"));
      _turnXraysOnButton.setEnabled(false);
//      _turnXraysOffForImagingButton.setEnabled(false);
      _turnXrayPowerOffButton.setEnabled(false);
      _quickWarmupXrayButton.setEnabled(false);
      _slowWarmupXrayButton.setEnabled(false);
      return;
    }

//Siew Yeng - XCR-3266 - remove swingutils here as calling to this function already inside swingutils
//    SwingUtils.invokeLater(new Runnable()
//    {
//      public void run()
//      {
    try
    {
      boolean areXraysOn = _xraySource.areXraysOnForServiceMode();
      boolean couldUnsafeXraysBeEmitted = _xraySource.couldUnsafeXraysBeEmitted();
      
      //Siew Yeng - XCR-3266 - remove return statement from each if else condition below as this will always skip 
      //                       inner barrier and self test status update
      if (areXraysOn == false && couldUnsafeXraysBeEmitted)
      {
        _xrayStatusLabel.setForeground(Color.BLACK);
//            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_ON_KEY"));
        _turnXraysOnButton.setEnabled(false);
//            _turnXraysOffForImagingButton.setEnabled(false);
        _turnXrayPowerOffButton.setEnabled(true);
        _quickWarmupXrayButton.setEnabled(true);
        _slowWarmupXrayButton.setEnabled(true);
//        return;
      }
      else if (areXraysOn == false && couldUnsafeXraysBeEmitted == false)
      {
        _xrayStatusLabel.setForeground(Color.RED);
//            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_OFF_KEY"));
        _turnXraysOnButton.setEnabled(true);
//            _turnXraysOffForImagingButton.setEnabled(false);
        _turnXrayPowerOffButton.setEnabled(false);
        _quickWarmupXrayButton.setEnabled(true);
        _slowWarmupXrayButton.setEnabled(true);
//        return;
      }
      else if (areXraysOn && couldUnsafeXraysBeEmitted)
      {
        _xrayStatusLabel.setForeground(Color.BLACK);
//            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_ON_KEY"));
        _turnXraysOnButton.setEnabled(false);
//            _turnXraysOffForImagingButton.setEnabled(false);
        _turnXrayPowerOffButton.setEnabled(true);
        _quickWarmupXrayButton.setEnabled(true);
        _slowWarmupXrayButton.setEnabled(true);
//        return;
      }
      else if (areXraysOn && couldUnsafeXraysBeEmitted == false)
      {
        _xrayStatusLabel.setForeground(Color.BLACK);
//            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_ON_KEY"));
        _turnXraysOnButton.setEnabled(false);
//            _turnXraysOffForImagingButton.setEnabled(false);
        _turnXrayPowerOffButton.setEnabled(true);
        _quickWarmupXrayButton.setEnabled(true);
        _slowWarmupXrayButton.setEnabled(true);
//        return;
      }

      populateInnerBarrierPanel();
      populateSelfTestPanel();

    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_ERROR_KEY"));
      _turnXraysOnButton.setEnabled(false);
//          _turnXraysOffForImagingButton.setEnabled(false);
      _turnXrayPowerOffButton.setEnabled(false);
    }
//      }
//    });
  }


  /**
   * @author George Booth
   */
  public void displayXraysAreOn()
  {
    _xrayStatusLabel.setForeground(Color.BLACK);
    _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_ON_KEY"));
  }

  /**
   * @author George Booth
   */
  public void displayXraysAreOff()
  {
    _xrayStatusLabel.setForeground(Color.RED);
    _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_OFF_KEY"));
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    stopMonitoring();

    startObserving(false);

    // remove the listeners
    removeListeners();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _turnXrayPowerOffButton.addActionListener(_turnXrayPowerOffButtonActionListener);
//    _turnXraysOffForImagingButton.addActionListener(_turnXraysOffForImagingButtonActionListener);
    _turnXraysOnButton.addActionListener(_turnXraysOnButtonActionListener);
    _clearErrorMessagesButton.addActionListener(_clearErrorMessagesButtonActionListner);
    _anodeTextField.addFocusListener(_anodeTextFieldListener);
    _cathodeTextField.addFocusListener(_cathodeTextFieldListener);
    _focalSpotTextField.addFocusListener(_focalSpotTextFieldListener);
    _anodeTextField.addActionListener(_anodeTextFieldActionListener);
    _cathodeTextField.addActionListener(_cathodeTextFieldActionListener);
    _focalSpotTextField.addActionListener(_focalSpotTextFieldActionListener);
    _initializeButton.addActionListener(_initializeActionListener);
    _innerBarrierCommandButton.addActionListener(_innerBarrierCommandButtonActionListener);
    _selfTestCommandButton.addActionListener(_selfTestCommandButtonActionListener);
    
    _quickWarmupXrayButton.addActionListener(_quickWarmupXrayButtonActionListener);
    _slowWarmupXrayButton.addActionListener(_slowWarmupXrayButtonActionListener);      
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _turnXrayPowerOffButton.removeActionListener(_turnXrayPowerOffButtonActionListener);
//    _turnXraysOffForImagingButton.removeActionListener(_turnXraysOffForImagingButtonActionListener);
    _turnXraysOnButton.removeActionListener(_turnXraysOnButtonActionListener);
    _clearErrorMessagesButton.removeActionListener(_clearErrorMessagesButtonActionListner);
    _anodeTextField.removeFocusListener(_anodeTextFieldListener);
    _cathodeTextField.removeFocusListener(_cathodeTextFieldListener);
    _focalSpotTextField.removeFocusListener(_focalSpotTextFieldListener);
    _anodeTextField.removeActionListener(_anodeTextFieldActionListener);
    _cathodeTextField.removeActionListener(_cathodeTextFieldActionListener);
    _focalSpotTextField.removeActionListener(_focalSpotTextFieldActionListener);
    _initializeButton.removeActionListener(_initializeActionListener);
    _innerBarrierCommandButton.removeActionListener(_innerBarrierCommandButtonActionListener);
    _selfTestCommandButton.removeActionListener(_selfTestCommandButtonActionListener);
    
    _quickWarmupXrayButton.removeActionListener(_quickWarmupXrayButtonActionListener);
    _slowWarmupXrayButton.removeActionListener(_slowWarmupXrayButtonActionListener);
  }
 
  /**
   * This method will create panel displaying all parameters that we expose about the tube to the user
   * @author Erica Wheatcroft
   */
  private void createXrayParametersPanel()
  {
    int textFieldColumnLength = 5;

    JPanel xraySourceParametersPanel = new JPanel(new BorderLayout());
    Border etchedBorder = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
    Border emptyBorderForPanelWithOneComponent = BorderFactory.createEmptyBorder(7, 5, 5, 5);
    Border emptyBorderForPanelWithTwoComponents = BorderFactory.createEmptyBorder(7, 5, 5, 5);

    JPanel anodePanel = new JPanel(new FlowLayout());
    JPanel innerAnodePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border anodePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_ANODE_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    anodePanel.setBorder(anodePanelBorder);
    innerAnodePanel.add(_currentAnodeValueLabel);


    DecimalFormat formatForAnodeTextField = new DecimalFormat();
    NumericRangePlainDocument anodeTextFieldDocument = new NumericRangePlainDocument();
    _anodeTextField.setDocument(anodeTextFieldDocument);
    _anodeTextField.setFormat(formatForAnodeTextField);
    anodeTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _anodeTextField.setColumns(textFieldColumnLength);
    anodePanel.add(innerAnodePanel);

    JPanel cathodePanel = new JPanel(new FlowLayout());
    JPanel innerCathodePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border cathodePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_CATHODE_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    cathodePanel.setBorder(cathodePanelBorder);
    innerCathodePanel.add(_currentCathodeValueLabel);

    DecimalFormat formatForcathodeTextField = new DecimalFormat();
    NumericRangePlainDocument cathodeTextFieldDocument = new NumericRangePlainDocument();
    _cathodeTextField.setDocument(cathodeTextFieldDocument);
    _cathodeTextField.setFormat(formatForcathodeTextField);
    cathodeTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _cathodeTextField.setColumns(textFieldColumnLength);
    cathodePanel.add(innerCathodePanel);

    JPanel focalSpotPanel = new JPanel(new FlowLayout());
    JPanel innerfocalSpotPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border focalSpotPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_FOCAL_SPOT_MODE_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    focalSpotPanel.setBorder(focalSpotPanelBorder);
    innerfocalSpotPanel.add(_currentFocalSpotValueLabel);
    
    try
    {
      if (LicenseManager.isDeveloperSystemEnabled() && Config.isDeveloperDebugModeOn())
      {
        innerAnodePanel.add(_anodeTextField);
        innerfocalSpotPanel.add(_focalSpotTextField);
        innerCathodePanel.add(_cathodeTextField);
      }
    }
    catch(BusinessException ex)
    {
      // do nothing
    }

    DecimalFormat formatForfocalSpotTextField = new DecimalFormat();
    NumericRangePlainDocument focalSpotTextFieldDocument = new NumericRangePlainDocument();
    _focalSpotTextField.setDocument(focalSpotTextFieldDocument);
    _focalSpotTextField.setFormat(formatForfocalSpotTextField);
    focalSpotTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _focalSpotTextField.setColumns(textFieldColumnLength);
    focalSpotPanel.add(innerfocalSpotPanel);

    JPanel selfTestPanel = new JPanel();
    JPanel innerSelfTestPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border selfTestPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("Self Test")),
        emptyBorderForPanelWithTwoComponents);
    selfTestPanel.setBorder(selfTestPanelBorder);
    innerSelfTestPanel.add(_selfTestStateLabel);
    _selfTestStateLabel.setText("Not Run");
    innerSelfTestPanel.add(_selfTestCommandButton);
    _selfTestCommandButton.setText("Start");
    selfTestPanel.add(innerSelfTestPanel);

    JPanel innerBarrierPanel = new JPanel();
    JPanel innerInnerBarrierPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border innerBarrierPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("Inner Barrier")),
        emptyBorderForPanelWithTwoComponents);
    innerBarrierPanel.setBorder(innerBarrierPanelBorder);
    innerInnerBarrierPanel.add(_innerBarrierStateLabel);
    _innerBarrierStateLabel.setText(_UNKNOWN);//Siew Yeng - XCR-3266 - initial status before startup should be "unknown"
    innerInnerBarrierPanel.add(_innerBarrierCommandButton);
    _innerBarrierCommandButton.setText(_UNKNOWN);
    innerBarrierPanel.add(innerInnerBarrierPanel);

    JPanel gunTempPanel = new JPanel(new FlowLayout());
    JPanel innerGunTempPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.CENTER, VerticalFlowLayout.MIDDLE));
    Border gunTempPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_GUN_TEMP_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    gunTempPanel.setBorder(gunTempPanelBorder);
    innerGunTempPanel.add(_gunTempLabel);
    gunTempPanel.add(innerGunTempPanel);

    JPanel gridPanel = new JPanel(new FlowLayout());
    JPanel innerGridPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border gridPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_GRID_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    gridPanel.setBorder(gridPanelBorder);
    innerGridPanel.add(_gridValueLabel);
    gridPanel.add(innerGridPanel);

    JPanel inputSupplyPanel = new JPanel(new FlowLayout());
    JPanel innerInputSupplyPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border inputSupplyPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder,
        StringLocalizer.keyToString("SERVICE_UI_INPUT_SUPPLY_LABEL_KEY")), emptyBorderForPanelWithOneComponent);
    inputSupplyPanel.setBorder(inputSupplyPanelBorder);
    innerInputSupplyPanel.add(_inputSupplyLabel);
    inputSupplyPanel.add(innerInputSupplyPanel);

    JPanel powerOnTimePanel = new JPanel(new FlowLayout());
    JPanel innerPowerOnTimePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border powerOnPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_ON_TIME_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    powerOnTimePanel.setBorder(powerOnPanelBorder);
    innerPowerOnTimePanel.add(_powerOnTimeLabel);
    powerOnTimePanel.add(innerPowerOnTimePanel);

    JPanel xrayEmissionTimePanel = new JPanel(new FlowLayout());
    JPanel innerXrayEmissionTimePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border xrayEmissionTimePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_EMISSION_TIME_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    xrayEmissionTimePanel.setBorder(xrayEmissionTimePanelBorder);
    innerXrayEmissionTimePanel.add(_xrayEmissionTimeLabel);
    xrayEmissionTimePanel.add(innerXrayEmissionTimePanel);

    JPanel operationalStatusPanel = new JPanel(new FlowLayout());
    JPanel innerOperationalStatusPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border operationalStatusPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_OPERATIONAL_STATUS_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    operationalStatusPanel.setBorder(operationalStatusPanelBorder);
    innerOperationalStatusPanel.add(_operationalStatusLabel);
    operationalStatusPanel.add(innerOperationalStatusPanel);

    JPanel warmupModePanel = new JPanel(new FlowLayout());
    JPanel innerWarmupModePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border warmupModePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_WARMUP_MODE_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    warmupModePanel.setBorder(warmupModePanelBorder);
    innerWarmupModePanel.add(_warmupModeLabel);
    warmupModePanel.add(innerWarmupModePanel);

    JPanel preheatMonitorPanel = new JPanel(new FlowLayout());
    JPanel innerPreheatMonitorPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border preheatMonitorPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_XRAY_PREHEAT_MONITOR_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    preheatMonitorPanel.setBorder(preheatMonitorPanelBorder);
    innerPreheatMonitorPanel.add(_preheatMonitorLabel);
    preheatMonitorPanel.add(innerPreheatMonitorPanel);

    // add the panels to the main parameters panel
    Box topRowParameters = Box.createHorizontalBox();
    topRowParameters.add(anodePanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(cathodePanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(focalSpotPanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(selfTestPanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(innerBarrierPanel);

    Box middleRowParameters = Box.createHorizontalBox();
    middleRowParameters.add(gridPanel);
    middleRowParameters.add(Box.createHorizontalGlue());
    middleRowParameters.add(gunTempPanel);
    middleRowParameters.add(Box.createHorizontalGlue());
    middleRowParameters.add(inputSupplyPanel);
    middleRowParameters.add(Box.createHorizontalGlue());
    middleRowParameters.add(powerOnTimePanel);
    middleRowParameters.add(Box.createHorizontalGlue());
    middleRowParameters.add(xrayEmissionTimePanel);

    Box bottomRowParameters = Box.createHorizontalBox();
    bottomRowParameters.add(operationalStatusPanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(warmupModePanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(preheatMonitorPanel);

    Box parametersBox = Box.createVerticalBox();
    parametersBox.add(topRowParameters);
    parametersBox.add(Box.createVerticalStrut(15));
    parametersBox.add(middleRowParameters);
    parametersBox.add(Box.createVerticalStrut(15));
    parametersBox.add(bottomRowParameters);

    xraySourceParametersPanel.add(parametersBox, BorderLayout.CENTER);
    _mainBox.add(xraySourceParametersPanel);
  }

  /**
   * This method creates the panel that will handle the hardware reported errors
   * @author Erica Wheatcroft
   */
  private void createHardwareReportedErrorsPanel()
  {
    JPanel errorMessageAndFirmwarePanel = new JPanel(new BorderLayout(0, 10));
    _firmwareRevisionPanel = new JPanel();
    _firmwareRevisionPanel.setLayout(new FlowLayout());
    _firmwareRevisionLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _firmwareRevisionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    //_firmwareRevisionPanel.add(_firmwareRevisionLabel);
    errorMessageAndFirmwarePanel.add(_firmwareRevisionPanel, BorderLayout.SOUTH);
    JPanel errorMessagesPanel = new JPanel();
    Border border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_XRAYS_PANEL_HW_REPORTED_ERRORS_LABEL_KEY")), BorderFactory.createEmptyBorder(5, 10, 10, 10));
    errorMessagesPanel.setLayout(new BorderLayout(0, 10));
    errorMessagesPanel.setBorder(border4);
    _errorMessageTextArea.setEditable(false);
    _errorMessageTextArea.setRows(10);
    _clearErrorMessagesButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_PANEL_CLEAR_ERROR_MSG_KEY"));
    JPanel clearErrorMessagesButtonPanel = new JPanel();
    clearErrorMessagesButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    clearErrorMessagesButtonPanel.add(_clearErrorMessagesButton);
    errorMessageAndFirmwarePanel.add(errorMessagesPanel, BorderLayout.CENTER);
    errorMessagesPanel.add(_errorMessagesTextAreaScrollPane, BorderLayout.CENTER);
    errorMessagesPanel.add(clearErrorMessagesButtonPanel, BorderLayout.NORTH);
    _errorMessagesTextAreaScrollPane.getViewport().add(_errorMessageTextArea);
    _mainBox.add(errorMessageAndFirmwarePanel);
  }

  /**
   * THis method will create the xray control and status panel
   * @author Erica Wheatcroft
   */
  private void createXrayControlPanel()
  {
    JPanel xrayStatusAndControlPanel = new JPanel();
    xrayStatusAndControlPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    xrayStatusAndControlPanel.setLayout(new BorderLayout());

    JPanel xrayControlButtonPanel = new JPanel();
    xrayControlButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 5));
    xrayControlButtonPanel.add(_turnXraysOnButton);
    xrayControlButtonPanel.add(_turnXrayPowerOffButton);
    xrayControlButtonPanel.add(_quickWarmupXrayButton);
    xrayControlButtonPanel.add(_slowWarmupXrayButton);
    
//    xrayControlButtonPanel.add(_turnXraysOffForImagingButton);
//    _turnXraysOffForImagingButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_XRAYS_OFF_FOR_IMAGING_KEY"));
    _turnXrayPowerOffButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_FULL_XRAYS_OFF_KEY"));
    _turnXraysOnButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_CURRENT_AND_VOLTAGE_ON_KEY"));
    _quickWarmupXrayButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_QUICK_WARM_UP_KEY"));
    _slowWarmupXrayButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_SLOW_WARM_UP_KEY"));

    JPanel xrayTubeStatusPanel = new JPanel(new FlowLayout());
    _xrayStatusLabel.setFont(FontUtil.getBiggerFont(_xrayStatusLabel.getFont(),Localization.getLocale()));
    _xrayStatusLabel.setFont(FontUtil.getBoldFont(_xrayStatusLabel.getFont(),Localization.getLocale()));
    xrayTubeStatusPanel.add(_xrayStatusLabel);
    xrayStatusAndControlPanel.add(xrayTubeStatusPanel, BorderLayout.NORTH);
    xrayStatusAndControlPanel.add(xrayControlButtonPanel, BorderLayout.CENTER);

    JPanel initializePanel = new JPanel(new FlowLayout());
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    initializePanel.add(_initializeButton);
    xrayStatusAndControlPanel.add(initializePanel, BorderLayout.EAST);

    _mainBox.add(xrayStatusAndControlPanel);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new GridLayout());
    setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 30));

    createXrayControlPanel();
    _mainBox.add(Box.createVerticalStrut(15));
    createXrayParametersPanel();
    _mainBox.add(Box.createVerticalStrut(15));
    createHardwareReportedErrorsPanel();

    add(_mainBox, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private synchronized void startMonitoring()
  {
    //Siew Yeng 
    //This is to check if monitor thread is already running. Run it only when it is not running to avoid creating extra thread.
    if(!_monitorEnabled)
    {
      _monitorEnabled = true;
      _monitorThread.invokeLater(new Runnable()
      {
        public void run()
        {
          while (_monitorEnabled)
          {
            updateXrayParameters();
//          checkVoltage();
            try
            {
              Thread.sleep(500);
            }
            catch (InterruptedException ie)
            {
              // do nothing
            }
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void updateXrayStatusLabel()
  {
    Assert.expect(_xraySource != null);

    if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.NOT_READY))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_OFF_KEY"));
    }
    else if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.OVER))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_OVER_KEY"));
    }
    else if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.SELF_TEST))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_IS_RUNNING_SELFTEST_KEY"));
    }
    else if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.STANDBY))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_IS_STANDBY_KEY"));
    }
    else if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.WARMUP))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_IS_WARMING_UP_KEY"));
    }
    else if(_xraySource.getXraySourceStatusEnum().equals(HTubeXraySourceStatusEnum.XON))
    {
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_ON_KEY"));
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          if ((object instanceof ProgressReport))
          {
            ProgressReport progressReport = (ProgressReport)object;
            int percentDone = progressReport.getPercentDone();

            String reporterAction = progressReport.getAction();

            // don't indicate 100% until event actually completes
            if (percentDone == 100)
              percentDone = 99;

            if (_progressDialog != null)
              _progressDialog.updateProgressBarPrecent(percentDone, reporterAction);
          }
        }
        else if (observable instanceof HardwareObservable)
        {
          if (object instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)object;
            HardwareEventEnum eventEnum = event.getEventEnum();

            if (eventEnum instanceof XraySourceEventEnum)
            {
              if(event.isStart() && eventEnum.equals(XraySourceEventEnum.SHUTDOWN))
              {
                _shuttingDownSystem = true;
              }

              if(event.isStart() == false && eventEnum.equals(XraySourceEventEnum.SHUTDOWN))
              {
                _shuttingDownSystem = false;
                _startingComplete = true;
              }

              if (event.isStart() && eventEnum.equals(XraySourceEventEnum.STARTUP))
              {
                _startingSystem = true;
              }

              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.STARTUP))
              {
                _startingSystem = false;
                _startingComplete = true;

                if (_progressDialog != null)
                  _progressDialog.updateProgressBarPrecent(100);
              }

              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.ON))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.OFF))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.POWER_OFF))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.SELF_TEST))
              {
                if (_progressDialog != null) // && _runningSelfTest == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
            }
            else if (eventEnum instanceof InnerBarrierEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(InnerBarrierEventEnum.BARRIER_OPEN) ||
                                               eventEnum.equals(InnerBarrierEventEnum.BARRIER_CLOSE)))
              {
                populateInnerBarrierPanel();
              }
            }
          }             
          else if (object instanceof XrayTesterException)
          {
            handleXrayTesterException((XrayTesterException)object);
          }
        }
        else
          Assert.expect(false);
      }
    });
  }
}