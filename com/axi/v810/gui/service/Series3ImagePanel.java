package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.geom.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Andy Mechtenberg
 */
public class Series3ImagePanel extends JPanel
{
  // drawing placement issues
  private final int _x = 280;
  private final int _y = 30;
  private final int _width = 315;
  private final int _height = 200;

  private final int _loaderWidth = 150;
  private final int _loaderHeight = _height/2;  // 1/2 of 5dx height
  private final int _loaderGap = 4;  // gap between loader and 5dx

  private final int _barrierWidth = 15;
  private final int _barrierHeight = _loaderHeight*2/3;

  private final int _panelOpticalSize = 13;

  // These member variables are related to the drawable components (doors, sensors, etc)
  // in the drawing

  // barriers (4)
  private boolean _leftOuterBarrierOpen = false;
  private boolean _rightOuterBarrierOpen = false;
  private boolean _innerBarrierOpen = true;

  // panel optical (crunch) sensors (4)
  private boolean _leftFrontPanelOpticalBlocked   = false;
  private boolean _rightFrontPanelOpticalBlocked  = false;
  private boolean _leftRearPanelOpticalBlocked   = false;
  private boolean _rightRearPanelOpticalBlocked  = false;

  // belt direction
  private boolean _beltDirectionForward = false;

  // middle rail installed
  private boolean _middleRailInstalled = true;


  private BorderLayout _mainBorderLayout = new BorderLayout();

  /**
   * @author Andy Mechtenberg
   */
  Series3ImagePanel()
  {
    jbInit();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void jbInit()
  {
    this.setLayout(_mainBorderLayout);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void paint(Graphics g)
  {
    draw5dx(g);
//    drawLeftLoader(g);
//    drawRightLoader(g);
    drawLegend(g);

    drawSensorComponents(g);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void draw5dx(Graphics g)
  {
    g.setColor(Color.black);
    g.drawRect(_x, _y, _width, _height);
    // put in the 5DX text
    Font f = g.getFont();
    Font newFont = FontUtil.getRendererFont(18);
    g.setFont(newFont);
    g.drawString("5DX", _x+129, _y+60);  // was y+60

    g.setFont(f);  // restore font.

    // vertical and horizontal lines for debug reference
    //g.drawLine(_x+_width/2, _y, _x+_width/2, _y+_height);
    //g.drawLine(_x, _y+_height/2, _x+_width, _y+_height/2);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawLegend(Graphics g)
  {
    Graphics2D g2 = (Graphics2D)g;
    int legendx =  _x + _width + _loaderWidth + _loaderGap + 50;
    int legendsymx = legendx + 10;
    int legendy = 5;

    Font f = g.getFont();
    g.setFont(FontUtil.getServiceLegendFont());
    g.drawRect(legendx, legendy, 120, 140);

    // closed circle for block sensor
    Ellipse2D e = new Ellipse2D.Double(legendsymx, legendy + 10, 10, 10);
    g2.setPaint(Color.black);
    g2.fill(e);
    g2.setColor(Color.black);
    g2.setStroke(new BasicStroke(2.0f));
    g2.draw(e);
    g.drawString("Blocked Panel", legendsymx + 20, legendy + 12);
    g.drawString("Optical Sensor", legendsymx + 20, legendy + 22);

    // open circle for open sensor
    e = new Ellipse2D.Double(legendsymx, legendy + 33, 10, 10);
    g2.setPaint(Color.white);
    g2.fill(e);
    g2.setColor(Color.black);
    g2.setStroke(new BasicStroke(2.0f));
    g2.draw(e);

    g2.setColor(Color.black);
    g.drawString("Clear Panel", legendsymx + 20, legendy + 37);
    g.drawString("Optical Sensor", legendsymx + 20, legendy + 47);

    // closed barrier
    g.fillRect(legendsymx, legendy + 52, 10, 35);
    g.drawString("Closed Barrier", legendsymx + 20, legendy + 72);

    // open barrier
    g2.setStroke(new BasicStroke(2.0f));
    g.drawLine(legendsymx, legendy + 98, legendsymx+10, legendy+98);  // horizontal
    g.drawLine(legendsymx+5, legendy + 93, legendsymx+5, legendy+98); // vertical
    g.drawLine(legendsymx, legendy + 123, legendsymx+10, legendy+123);  // horizontal
    g.drawLine(legendsymx+5, legendy + 123, legendsymx+5, legendy+128);  // vertical
    g.drawString("Open Barrier", legendsymx + 20, legendy+112);

    g.setFont(f); // restore
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawSensorComponents(Graphics g)
  {
    drawLeftBarrier(g);
    drawRightBarrier(g);
    drawShutterBarrier(g);

    drawMiddleRail(g);

    drawFrontLeftOpticalSensor(g);
    drawFrontRightOpticalSensor(g);
    drawRearLeftOpticalSensor(g);
    drawRearRightOpticalSensor(g);


    drawBeltDirection(g);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawMiddleRail(Graphics g)
  {
    if (_middleRailInstalled)
    {
      int xinset = 30;  // offset from the loader door to the start of the middle rail

      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(4.0f));
      g.drawLine(_x+xinset, _y+_height/2, _x+_width-xinset, _y+_height/2);

      g2.setStroke(s); // restore
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawShutterBarrier(Graphics g)
  {
    // the shutter on the series3 is drawn lower than on the 2L because the middle rail is centered

    // since this barrier is drawn sideways, height and width are reversed
    int barrierx = _x + _width/2 - _barrierHeight/2;
    int barriery = _y + _height/2 - _barrierWidth/2 + 20;
    int insetLength = 12;  // the length of the horzontal little leg like in the legend.

    Graphics2D g2 = (Graphics2D) g;
    Stroke s = g2.getStroke();
    g2.setStroke(new BasicStroke(2.0f));

    if (_innerBarrierOpen)
    {
      g.setColor(SystemColor.control);
      g.fillRect(barrierx+insetLength, barriery, _barrierHeight+1-2*insetLength, _barrierWidth+1);
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx+insetLength, barriery+2, _barrierHeight-2*insetLength, _barrierWidth-4);
    }

    g.setColor(Color.black);
    g.drawLine(barrierx+1+insetLength, barriery+1, barrierx+1+insetLength, barriery+_barrierWidth-1);  // left wall
    g.drawLine(barrierx+_barrierHeight-insetLength, barriery+1, barrierx+_barrierHeight-insetLength, barriery+_barrierWidth-1);  // right wall
    // draw insets
    g.drawLine(barrierx+1, barriery+_barrierWidth/2, barrierx+1+insetLength, barriery+_barrierWidth/2);  // left inset
    g.drawLine(barrierx+_barrierHeight-insetLength, barriery+_barrierWidth/2, barrierx+_barrierHeight, barriery+_barrierWidth/2);  // right inset

    g2.setStroke(s); // restore
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawLeftBarrier(Graphics g)
  {
    int barrierx = _x - _barrierWidth/2;
    int barriery = _y + _height/4 + _loaderHeight/6;

    if (_leftOuterBarrierOpen)
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(2.0f));
      g.setColor(SystemColor.control);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
      g.setColor(Color.black);
      g.drawLine(barrierx+1, barriery+1, barrierx+_barrierWidth-1, barriery+1);
      g.drawLine(barrierx+1, barriery+_barrierHeight-1, barrierx+_barrierWidth-1, barriery+_barrierHeight-1);
      g2.setStroke(s); // restore
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawRightBarrier(Graphics g)
  {
    int barrierx = _x + _width - _barrierWidth/2;
    int barriery = _y + _height/4 + _loaderHeight/6;

    if (_rightOuterBarrierOpen)
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(2.0f));
      g.setColor(SystemColor.control);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
      g.setColor(Color.black);
      g.drawLine(barrierx+1, barriery+1, barrierx+_barrierWidth-1, barriery+1);
      g.drawLine(barrierx+1, barriery+_barrierHeight-1, barrierx+_barrierWidth-1, barriery+_barrierHeight-1);
      g2.setStroke(s); // restore
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawFrontLeftOpticalSensor(Graphics g)
  {
    int optx = _x + _barrierWidth/2;
    int opty = _y + _height*3/4 - _loaderHeight/6;

    if (_leftFrontPanelOpticalBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawRearLeftOpticalSensor(Graphics g)
  {
    int optx = _x + _barrierWidth/2;
    int opty = _y + _height/4;

    if (_leftRearPanelOpticalBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawFrontRightOpticalSensor(Graphics g)
  {
    int optx = _x + _width - _barrierWidth/2 - _panelOpticalSize;
    int opty = _y + _height*3/4 - _loaderHeight/6;

    if (_rightFrontPanelOpticalBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawRearRightOpticalSensor(Graphics g)
  {
    int optx = _x + _width - _barrierWidth/2 - _panelOpticalSize;
    int opty = _y + _height/4;

    if (_rightRearPanelOpticalBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D) g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawBeltDirection(Graphics g)
  {
    int beltTextX = _x + _width/2 - 30;
    int beltTextY = _y + _height - 30;

    int arrowX = beltTextX - 15;
    int arrowY = beltTextY + 10;
    int arrowLength = 100;
    int arrowHeadSize = 5;

    //clear out area to draw a new arrow, otherwise you might see two arrowheads
    g.setColor(SystemColor.control);
    g.fillRect(arrowX - arrowHeadSize*2 - 2, arrowY - arrowHeadSize -2, arrowLength + arrowHeadSize*4 + 6, arrowHeadSize*2+6);
    g.setColor(Color.black);
    g.drawString("Belt Direction", beltTextX, beltTextY);

    g.drawLine(arrowX, arrowY, arrowX+arrowLength, arrowY);
    Polygon pg = new Polygon();
    if (_beltDirectionForward)
    {
      pg.addPoint(arrowX+arrowLength, arrowY-arrowHeadSize);
      pg.addPoint(arrowX+arrowLength+arrowHeadSize*2, arrowY);
      pg.addPoint(arrowX+arrowLength, arrowY+arrowHeadSize);
    }
    else
    {
      pg.addPoint(arrowX, arrowY-arrowHeadSize);
      pg.addPoint(arrowX-arrowHeadSize*2, arrowY);
      pg.addPoint(arrowX, arrowY+arrowHeadSize);
    }
    g.drawPolygon(pg);
    g.fillPolygon(pg);
  }

  /**
   * @author Andy Mechtenberg
   */
  void updateFromServer()
  {
//    try
//    {
//      _leftOuterBarrierOpen = hardwareStatus.isLeftOuterBarrierOpen();
//      _rightOuterBarrierOpen = hardwareStatus.isRightOuterBarrierOpen();
////    _innerBarrierOpen = hwStatusAdapter.isInnerBarrierOpen();
////
////    _middleRailInstalled = hwStatusAdapter.isMiddleRailInstalled();
//
//      _beltDirectionForward = hardwareStatus.isStageBeltDirectionForward();
//
//      // front
//      _leftFrontPanelOpticalBlocked = hardwareStatus.isLeftMainPanelClearSensorBlocked();
//      _rightFrontPanelOpticalBlocked = hardwareStatus.isRightMainPanelClearSensorBlocked();
//
//      // rear
////    _leftRearPanelOpticalBlocked   =  hwStatusAdapter.isLeftRearPanelCrunchSensorBlocked();
////    _rightRearPanelOpticalBlocked  =  hwStatusAdapter.isRightRearPanelCrunchSensorBlocked();
//
//    }
//    catch (HardwareException ex)
//    {
////      mainFrame.handleHardwareError(ex);
//    }

    this.invalidate();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
          //UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
        Series3ImagePanel p = new Series3ImagePanel();
        JFrame f = new JFrame();
        f.getContentPane().add(p);

        f.setSize(950, 400);
        f.setVisible(true);
      }
    });
  }
}
