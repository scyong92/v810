package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
class MotionControlPanel extends JPanel implements TaskPanelInt, Observer
{
  private JScrollPane _statusPaneScrollPane = new JScrollPane();
  private JTextArea _statusPane = new JTextArea();
  private JButton _initializeButton = new JButton();
  private JButton _getConfigurationButton = new JButton();
  private JButton _updateFirmwareButton = new JButton();
  private JButton _cancelFirmwareUpdateButton = new JButton();

  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
  private WorkerThread _monitoringConfigurationThread = new WorkerThread("Update Configuration Monitor");
  private MotionControl _motionControl = null;
  private SubsystemStatusAndControlTaskPanel _parent = null;

  private boolean _initialized = false;
  private boolean _monitoring = false;
  private boolean _updateComplete = false;
  private boolean _configuringMotionControlSystem = false;

  private ProgressDialog _progressDialog = null;

  // listeners i create them here and add / remove them as needed
  private ActionListener _initializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      initializeSubsystem();
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  private ActionListener _getConfigurationButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      getStatus();
    }
  };

  private ActionListener _updateButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      String message = StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_CONTINUE_WITH_CONFIG_KEY");

      int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_CONTINUE_WITH_CONFIG_DIALOG_KEY"),
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.WARNING_MESSAGE);

      if(response != JOptionPane.YES_OPTION)
        return;

      _updateComplete = false;

      // clear the text field.
      clearStatusPane();

      // disable all buttons.
      disableButtons(true);

      // start the thread that is looking for updates
      startMonitoring();

      // start the configuration
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (RemoteMotionServerExecution.getInstance().isStartupRequired())
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  _statusPane.append("\n\n");
                  _statusPane.append("Starting Remote Motion Server");
                  _statusPane.append("\n\n");
                  _statusPane.append("**********************************************************************");
                }
              });
              RemoteMotionServerExecution.getInstance().startup();

              SwingUtils.invokeLater(new Runnable()
              {

                public void run()
                {
                  _statusPane.append("\n\n");
                  _statusPane.append("Remote Motion Server is initialized.");
                  _statusPane.append("\n\n");
                  _statusPane.append("**********************************************************************");
                }
              });
            }
            
            _configuringMotionControlSystem = true;
            _motionControl.setServiceModeConfiguration();
            _configuringMotionControlSystem = false;
            // now sleep so we can get the last message
            try
            {
              Thread.sleep(200000);
            }
            catch (Exception e)
            {
              // do nothing
            }            
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
                stopMonitoring();
                disableButtons(false);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _cancelUpdateActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // lets ask the user if they wish to stop this update.
      String message = StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_ABORT_CONFIG_KEY");
      int result = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringUtil.format(message, 50),
                                                 StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_ABORT_CONFIG_DIALOG_KEY"),
                                                 JOptionPane.WARNING_MESSAGE,
                                                 JOptionPane.YES_NO_OPTION);

      if (result != JOptionPane.YES_OPTION)
        return;

      _updateComplete = true;

      _statusPane.append("\n");
      _statusPane.append(StringLocalizer.keyToString("SERVICE_UI_MOTION_CONTROL_OPERATION_CANCELLED_KEY"));

      // CR1067 fix by LeeHerng - Added a BusyDialogBox to show we are actually cancelling the firmware upgrade
      final BusyCancelDialog busyDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                               StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_CANCEL_FIRMWARE_UPGRADE_KEY"),
                                                               StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                               true);
      busyDialog.pack();
      SwingUtils.centerOnComponent(busyDialog, MainMenuGui.getInstance());
      SwingWorkerThread.getInstance().invokeLater(new Runnable()
      {
          public void run()
          {
              // disable all buttons.
              disableButtons(true);

              // start the configuration
              _workerThread.invokeLater(new Runnable()
              {
                public void run()
                {
                  _motionControl.abortServiceModeConfiguration();
                  disableButtons(false);
                  busyDialog.setVisible(false);
                }
              });

              stopMonitoring();
              _initialized = false;
          }
      });
      busyDialog.setVisible(true);
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  public MotionControlPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    // create the ui components
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    ProgressObservable.getInstance().addObserver(this);
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    ProgressObservable.getInstance().deleteObserver(this);
    HardwareObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startMonitoring()
  {
    _monitoring = true;

    _monitoringConfigurationThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while(true)
        {
          try
          {
            if(RemoteMotionServerExecution.getInstance().isStartupRequired() == false)
              break;
            try
            {
              Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
              // do nothing.
            }
          }
          catch(XrayTesterException e)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _statusPane.append("\n\n");
                _statusPane.append("Remote Motion Server Failed to initialized.");
                _statusPane.append("\n\n");
                _statusPane.append("**********************************************************************");
              }
            });
            break;
          }
        }
        while (_monitoring && _updateComplete == false)
        {

          MotionControlServiceModeConfigurationStatus configStatus = _motionControl.getServiceModeConfigurationStatus();
          LocalizedString localizedString = new LocalizedString(configStatus.getKey(), configStatus.getParameters().toArray());
          String message = StringLocalizer.keyToString(localizedString);
          if(message.length() > 0)
            message = StringUtil.format(message, 50);
          final String formatedMessage = message;
          if (message.length() > 0)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _statusPane.append("\n\n");
                _statusPane.append(formatedMessage);
                _statusPane.append("\n\n");
                _statusPane.append("**********************************************************************");
              }
            });
          }
          try
          {
            Thread.sleep(100);
          }
          catch (InterruptedException e)
          {
            // do nothing.
          }
          if(configStatus.getKey().equals("HW_XMP_SYNQNET_PCI_MOTION_CONTROLLER_SERVICE_MODE_CONFIGURATION_SUCCESSFUL_END_KEY"))
            _updateComplete = true;

          if(_updateComplete)
          {
            _initialized = true;
            stopMonitoring();

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                disableButtons(false);
              }
            });
          }
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitoring = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void disableButtons(boolean configuringHardware)
  {
    if (configuringHardware)
    {
      // then disable everything but the cancel button should be enabled
      _initializeButton.setEnabled(false);
      _getConfigurationButton.setEnabled(false);
      _updateFirmwareButton.setEnabled(false);
      _cancelFirmwareUpdateButton.setEnabled(true);
    }
    else
    {
      if (_initialized)
      {
        _initializeButton.setEnabled(true);
        _getConfigurationButton.setEnabled(true);
        _updateFirmwareButton.setEnabled(true);
        _cancelFirmwareUpdateButton.setEnabled(false);
      }
      else
      {
        _initializeButton.setEnabled(true);
        _getConfigurationButton.setEnabled(false);
        _updateFirmwareButton.setEnabled(true);
        _cancelFirmwareUpdateButton.setEnabled(false);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void initializeSubsystem()
  {
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _monitoring = false;

          // shutdown the subsystem
          _motionControl.shutdown();

          //Siew Yeng - XCR1426 fix hardwareWorkerThread assert
          DigitalIo.getInstance().shutdown();
          DigitalIo.getInstance().startup();
          // initialize the hardware
          _motionControl.startup();

          // set the flag
          _initialized = true;

          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              // set the state of the buttons
              disableButtons(false);
            }
          });

        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              handleException(xte);
              _progressDialog.setVisible(false);
              _progressDialog = null;
            }
          });
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _parent.getSparkIcon().getIconWidth();
    int iconHeight = _parent.getSparkIcon().getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _parent.getSparkIcon().paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(100, 0));
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    JPanel innerInitializePanel = new JPanel(new GridLayout(2, 1, 5, 50));
    JPanel initializeAndGetConfigPanel = new JPanel(new GridLayout(2, 1, 5, 25));
    initializeAndGetConfigPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    _getConfigurationButton.setText(StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_GET_CONFIG_DESCR_KEY"));
    initializeAndGetConfigPanel.setOpaque(false);
    initializeAndGetConfigPanel.add(_initializeButton);
    initializeAndGetConfigPanel.add(_getConfigurationButton);
    innerInitializePanel.add(initializeAndGetConfigPanel);

    JPanel configureFirmawarePanel = new JPanel(new GridLayout(2, 1, 5, 18));
    configureFirmawarePanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                         StringLocalizer.keyToString("SERVICE_UI_MOTION_CONTROL_CONFIGURE_MOTION_CTRL_KEY")),
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));

    _cancelFirmwareUpdateButton.setText(StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY"));
    _cancelFirmwareUpdateButton.setEnabled(false);
    _updateFirmwareButton.setText(StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_CONFIGURE_BUTTON_KEY"));
    configureFirmawarePanel.add(_updateFirmwareButton);
    configureFirmawarePanel.add(_cancelFirmwareUpdateButton);
    innerInitializePanel.add(configureFirmawarePanel);


    JPanel buttonPanel = new JPanel(new BorderLayout());
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(30, 30, 0, 30));
    buttonPanel.setOpaque(false);
    buttonPanel.add(innerInitializePanel, BorderLayout.NORTH);

    JPanel statusPanel = new JPanel(new BorderLayout());
    statusPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                         StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_MESSAGES_KEY")),
        BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    statusPanel.add(_statusPaneScrollPane, BorderLayout.CENTER);
    _statusPaneScrollPane.setOpaque(false);
    _statusPaneScrollPane.getViewport().add(_statusPane);

    add(statusPanel, BorderLayout.CENTER);
    add(buttonPanel, BorderLayout.EAST);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get the instance of the motion control system
    /** @todo EEW this should be removed once greg lets me know*/
    java.util.List<MotionControllerIdEnum> ids = MotionControllerIdEnum.getAllEnums();
    Assert.expect(ids != null);
    Assert.expect(ids.size() != 0);
    _motionControl = MotionControl.getInstance(ids.get(0));

    try
    {
      if (_motionControl.isStartupRequired())
        _initialized = false;
      else
        _initialized = true;
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }

    // set the state of the buttons
    disableButtons(false);

    // add the listeners
    addListeners();

    startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void getStatus()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {

        // get the motion control state and update the text area and the status label
        java.util.List<HardwareConfigurationDescriptionInt> descriptions = _motionControl.getConfigurationDescription();
        Assert.expect(descriptions != null);

        // clear the status pane
        clearStatusPane();
        StringBuffer statusMessageBuffer = new StringBuffer();
        for (HardwareConfigurationDescriptionInt configuration : descriptions)
        {
          LocalizedString localizedString = new LocalizedString(configuration.getKey(),
              configuration.getParameters().toArray());
          String message = StringUtil.format(StringLocalizer.keyToString(localizedString), 50);
          statusMessageBuffer.append(message);
          statusMessageBuffer.append("\n");
        }
        _statusPane.setText(statusMessageBuffer.toString());
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void clearStatusPane()
  {
    try
    {
      _statusPane.getDocument().remove(0, _statusPane.getDocument().getLength());
    }
    catch (javax.swing.text.BadLocationException ex)
    {
      // this should not happen
      Assert.expect(false);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _getConfigurationButton.addActionListener(_getConfigurationButtonActionListener);
    _updateFirmwareButton.addActionListener(_updateButtonActionListener);
    _cancelFirmwareUpdateButton.addActionListener(_cancelUpdateActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _getConfigurationButton.removeActionListener(_getConfigurationButtonActionListener);
    _updateFirmwareButton.removeActionListener(_updateButtonActionListener);
    _cancelFirmwareUpdateButton.removeActionListener(_cancelUpdateActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    if (_configuringMotionControlSystem)
      return false;

    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // remove the listeners
    removeListeners();

    stopObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof MotionControlEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(MotionControlEventEnum.INITIALIZE))
                _progressDialog.updateProgressBarPrecent(100);
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+" - "+StringLocalizer.keyToString("SERVICE_UI_SSACTP_MOTION_CONTROL_TAB_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }

}
