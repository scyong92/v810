package com.axi.v810.gui.service;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class LoopControlTableModel extends DefaultSortTableModel
{
  private final String[] _tableColumns = {StringLocalizer.keyToString("LOOP_DLG_HARDWARE_OPERATION_TABLE_COLUMN_NAME_KEY"),
                                          StringLocalizer.keyToString("LOOP_DLG_STATUS_COLUMN_NAME_KEY")};

  private List<Pair<HardwareOperationsEnum, String>> _hardwareOperationsEnumAndStatusPairsList = null;

  /**
   * @author Erica Wheatcroft
   */
  public LoopControlTableModel()
  {
    _hardwareOperationsEnumAndStatusPairsList = new ArrayList<Pair<HardwareOperationsEnum,String>>();
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return the number of columns in the model.
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void insertRow()
  {
    // lets add a row to the bottom of the list
    Pair<HardwareOperationsEnum,String> newItem = new Pair<HardwareOperationsEnum,String>();
    newItem.setPair(HardwareOperationsEnum.OPEN_INNER_BARRIER, new String());
    _hardwareOperationsEnumAndStatusPairsList.add(newItem);
    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void deleteRow(int row)
  {
    Assert.expect(row >= 0);
    _hardwareOperationsEnumAndStatusPairsList.remove(row);
    fireTableDataChanged();
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   *
   * @param columnIndex the index of the column
   * @return the name of the column
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _tableColumns[columnIndex];
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return the number of rows in the model
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if(_hardwareOperationsEnumAndStatusPairsList == null)
      return 0;
    else
      return _hardwareOperationsEnumAndStatusPairsList.size();
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
   *
   * @param rowIndex the row whose value is to be queried
   * @param columnIndex the column whose value is to be queried
   * @return the value Object at the specified cell
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_hardwareOperationsEnumAndStatusPairsList != null, "image set data is null");
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    Pair<HardwareOperationsEnum,String> selectedHardwareOperationToStatusPair = _hardwareOperationsEnumAndStatusPairsList.get(rowIndex);
    Object returnValue = null;

    switch(columnIndex)
    {
      case 0:
        returnValue = selectedHardwareOperationToStatusPair.getFirst();
        break;
      case 1:
        returnValue = selectedHardwareOperationToStatusPair.getSecond();
        break;
      default:
        Assert.expect(false, "Only 2 columns exist");
    }

    Assert.expect(returnValue != null);
    return returnValue;
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and <code>columnIndex</code> is editable.
   *
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @todo Implement this javax.swing.table.TableModel method
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(columnIndex >= 0, "column index is negative");

    if (columnIndex == 0) // the user can only edit column 1 everything else is not editable.
      return true;

    return false;
  }

  /**
   * isSortable
   *
   * @param col int
   * @return boolean
   * @todo Implement this com.axi.guiUtil.SortTableModel method
   */
  public boolean isSortable(int col)
  {
    return false;
  }

  /**
   * Sets the value in the cell at <code>columnIndex</code> and <code>rowIndex</code> to <code>aValue</code>.
   *
   * @param aValue the new value
   * @param rowIndex the row whose value is to be changed
   * @param columnIndex the column whose value is to be changed
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(aValue != null, "value is null");
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");
    Assert.expect(_hardwareOperationsEnumAndStatusPairsList != null);

    if(columnIndex == 0)
    {
      // remove the row if the element exists
      _hardwareOperationsEnumAndStatusPairsList.remove(rowIndex);

      Pair<HardwareOperationsEnum, String> hardwareOperationsEnumToStatusPair =
          new Pair<HardwareOperationsEnum, String>((HardwareOperationsEnum)aValue, "");

      // add the new selection
      _hardwareOperationsEnumAndStatusPairsList.add(rowIndex, hardwareOperationsEnumToStatusPair);
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  public List<HardwareOperationsEnum> getSelectedHardwareOperations()
  {
    Assert.expect(_hardwareOperationsEnumAndStatusPairsList != null);

    List<HardwareOperationsEnum> hardwareOperations = new ArrayList<HardwareOperationsEnum>();
    for(Pair<HardwareOperationsEnum, String> hardwareOperationsEnumAndStatusPair : _hardwareOperationsEnumAndStatusPairsList)
      hardwareOperations.add(hardwareOperationsEnumAndStatusPair.getFirst());

    return hardwareOperations;
  }

  /**
   * @author Erica Wheatcroft
   */
  void updateStatusForHardwareOperation(String status, int rowIndex)
  {
    Pair<HardwareOperationsEnum,String> selectedHardwareOperationToStatusPair = _hardwareOperationsEnumAndStatusPairsList.get(rowIndex);
    selectedHardwareOperationToStatusPair.setSecond((String) status);
    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  void updateRemaningRowsWithWillNotRunStatus(String statusString, int rowIndexToStartAt)
  {
    Assert.expect(statusString != null);
    Assert.expect(_hardwareOperationsEnumAndStatusPairsList != null);

    int index = rowIndexToStartAt;
    int hardwareOperationsCount = _hardwareOperationsEnumAndStatusPairsList.size();
    // now update the rest to show that they will not be run
    while (index < hardwareOperationsCount)
    {
      Pair<HardwareOperationsEnum,String> selectedHardwareOperationToStatusPair = _hardwareOperationsEnumAndStatusPairsList.get(index);
      selectedHardwareOperationToStatusPair.setSecond(statusString);
      index++;
    }
    fireTableDataChanged();
  }

  /**
   * @author Erica Wheatcroft
   */
  void clearStatus()
  {
    Assert.expect(_hardwareOperationsEnumAndStatusPairsList != null);

    for(Pair<HardwareOperationsEnum,String> selectedHardwareOperationToStatusPair : _hardwareOperationsEnumAndStatusPairsList)
      selectedHardwareOperationToStatusPair.setSecond("");

    fireTableDataChanged();
  }
}
