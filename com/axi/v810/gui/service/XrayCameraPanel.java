package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.BusinessException;
import com.axi.v810.business.license.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class XrayCameraPanel extends JPanel implements TaskPanelInt, Observer
{
  private JTabbedPane _centerTabPane = new JTabbedPane();
  private JScrollPane _statusPaneScrollPane = new JScrollPane();
  private JTextArea _statusPane = new JTextArea();
  private JButton _initializeButton = new JButton();
  private JButton _getConfigurationButton = new JButton();
  private JButton _upgradeFirmwareButton = new JButton();
  private JButton _cameraSPCButton = new JButton();
  private JComboBox _camerasComboBox = new JComboBox();
  private JLabel _cameraLabel = new JLabel();
  
  //Kee Chin Seong - Forcing ugrade firmware.
  //Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
  private JCheckBox _forceUpdateFirmwareCheckBox = new JCheckBox();

  private JCheckBox _cbCPCheckBox = new JCheckBox("Show CP" );
  private JRadioButton _rMeanRadioButton = new JRadioButton("Gradient Mean" );
  private JRadioButton _rStdDevRadioButton = new JRadioButton("Gradient StdDev" );
  private JRadioButton _rAboveMeanGradientRadioButton = new JRadioButton("Gradient Above Mean" );
  private JRadioButton _rMaxGradientRadioButton = new JRadioButton("Gradient Max" );
  private JRadioButton _rBetaRadioButton = new JRadioButton("Gradient Beta" );
  //rivate X_barPanel  xBarSPC= new X_barPanel(  "dataFileName.xml" , String.valueOf(cameraID *10+radioSelection));
  private XrayCameraSPCPanel _xrayCameraSPC;
  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
  private SubsystemStatusAndControlTaskPanel _parent = null;
  private XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();
  private CameraTrigger _xrayCameraController = CameraTrigger.getInstance();
  private java.util.List<AbstractXrayCamera> _cameras = null;
  private boolean _initialized = false;
  private boolean _monitoring = false;

  private final static String _ALL = StringLocalizer.keyToString("ATGUI_ALL_KEY");
  private ProgressDialog _progressDialog = null;
  private boolean _useAllCameras = true;
  private AbstractXrayCamera _currentSelectedCamera = null;
  private final static int _MAX_NUMBER_CAMERAS = 14;

  private boolean _loadingSwingComponent = false;

  // a count of the cameras that have been initialized
  private int _initializedCameraCount = 0;
  // listeners i create them here and add / remove them as needed
  private ActionListener _initializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      initializeSubsystem();
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  private ActionListener _camerasComboBoxActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      if(_loadingSwingComponent == false)
      {
        if (_camerasComboBox.getSelectedItem() instanceof String)
        {
          _useAllCameras = true;
          _currentSelectedCamera = null;
        }
        else if (_camerasComboBox.getSelectedItem() instanceof AbstractXrayCamera)
        {
          _useAllCameras = false;
          _currentSelectedCamera = (AbstractXrayCamera)_camerasComboBox.getSelectedItem();
        }
        else
          Assert.expect(false);
      }
    }
  };

  private ActionListener _getConfigurationButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      if(_centerTabPane != null && _centerTabPane.isDisplayable())
        _centerTabPane.setSelectedIndex(0);
      getStatus();
    }
  };

  /**
   * Wei Chin, Chong
   * Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
   */
  private ActionListener _upgradeFirmwareButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      boolean continueUpgrade = continueUpgradeFirmwareDialog();
      if (continueUpgrade)
      {
        // clear the text field.
        clearStatusPane();

        // disable all buttons.
        disableButtons(true);

        // start the thread that is looking for updates
        _monitoring = true;
        
        //Swee Yee Wong - XCR-3577 System crash when initialize xray camera while camera is upgrading firmware
        final BusyCancelDialog busyDialog = new BusyCancelDialog(MainMenuGui.getInstance(),
                                                                 StringLocalizer.keyToString("SERVICE_UI_UPGRADE_FIRMWARE_BUSY_KEY"),
                                                                 StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
                                                                 true);
        busyDialog.pack();
        SwingUtils.centerOnComponent(busyDialog, MainMenuGui.getInstance());

        SwingWorkerThread.getInstance().invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              try
              {
                //_xrayCameraArray.updateCameraFirmware();
                //Khang Wah Changes
                if (_useAllCameras)
                {
                  _statusPane.append("\n\n");
                  _statusPane.append("Upgrade All Camera\n");
                  for (AbstractXrayCamera xrayCamera : _xrayCameraArray.getCameras())
                  {
                    _statusPane.append("Camera " + xrayCamera.getId() + " -- " + xrayCamera.getCameraBoardVersion() + "\n");
                  }
                  _statusPane.append("\n\n");
                  _statusPane.append("**********************************************************************\n");
                  //Kee Chin Seong - Forcing ugrade firmware.
				  //Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
                  if(Config.isDeveloperDebugModeOn())
                    _xrayCameraArray.updateCameraFirmware(_forceUpdateFirmwareCheckBox.isSelected());
                  else
                    _xrayCameraArray.updateCameraFirmware(false);
                }
                else
                {
                  // update only one camera
                  Assert.expect(_currentSelectedCamera != null);
                  _statusPane.append("\n\n");
                  _statusPane.append("Upgrade Camera: " + _currentSelectedCamera.getId() + "\n");
                  _statusPane.append("Camera " + _currentSelectedCamera.getId() + " -- " + _currentSelectedCamera.getCameraBoardVersion() + "\n");
                  _statusPane.append("\n\n");
                  _statusPane.append("**********************************************************************\n");
                  //Kee Chin Seong - Forcing ugrade firmware.
				  //Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
                  if(Config.isDeveloperDebugModeOn())
                    _currentSelectedCamera.updateCameraFirmware(_forceUpdateFirmwareCheckBox.isSelected());
                  else
                    _currentSelectedCamera.updateCameraFirmware(false);
                  _currentSelectedCamera.reboot();
                }
                //Swee Yee Wong - XCR-3577 System crash when initialize xray camera while camera is upgrading firmware
                _statusPane.append("\n");
                _statusPane.append("Upgrade completed\n");
                _statusPane.append("**********************************************************************");
              }
              catch(XrayTesterException xte)
              {
                //Swee Yee Wong - XCR-3577 System crash when initialize xray camera while camera is upgrading firmware
                _statusPane.append("\n");
                _statusPane.append("Upgrade failed\n");
                _statusPane.append("**********************************************************************");
                handleException(xte);
              }
              finally
              {
                _monitoring = false;
                disableButtons(false);
              }
            }
            //Swee Yee Wong - XCR-3577 System crash when initialize xray camera while camera is upgrading firmware
            finally
            {
              busyDialog.setVisible(false);
            }
          }
        });
        busyDialog.setVisible(true);
      }
    }
  };


  /**
   * @author Erica Wheatcroft
   */
  public XrayCameraPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    // create the ui components
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    ProgressObservable.getInstance().addObserver(this);
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    ProgressObservable.getInstance().deleteObserver(this);
    HardwareObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   * @author Ronald Lim
   * @edited By Kee Chin Seong
   * @edited Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
   */
  private void disableButtons(boolean configuringHardware)
  {
    try
    {
      if (configuringHardware)
      {
        // then disable everything but the cancel button should be enabled
        _initializeButton.setEnabled(false);
        _getConfigurationButton.setEnabled(false);
        _upgradeFirmwareButton.setEnabled(false);
        _forceUpdateFirmwareCheckBox.setEnabled(false);
      }
      else
      {
        if (_initialized && _xrayCameraArray.isStartupRequired() == false)
        {
          _initializeButton.setEnabled(true);
          _getConfigurationButton.setEnabled(true);
          _upgradeFirmwareButton.setEnabled(true);
          _forceUpdateFirmwareCheckBox.setEnabled(true);
        }
        else if (_initialized)
        {
          _initializeButton.setEnabled(true);
          _getConfigurationButton.setEnabled(true);
          _upgradeFirmwareButton.setEnabled(true);
          _forceUpdateFirmwareCheckBox.setEnabled(true);
        }
        else
        {
          _initializeButton.setEnabled(true);
          _getConfigurationButton.setEnabled(false);
          _upgradeFirmwareButton.setEnabled(true);
          _forceUpdateFirmwareCheckBox.setEnabled(true);
        }
      }
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void initializeSubsystem()
  {
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _monitoring = false;
          _xrayCameraController.startup();
          if(_useAllCameras)
          {
            _initializedCameraCount = 0;
            _xrayCameraArray.startup();
          }
          else
          {
            // initialize only one camera
            Assert.expect(_currentSelectedCamera != null);
            _currentSelectedCamera.startup();
          }

          // set the flag
          _initialized = true;

          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              // set the state of the buttons
              disableButtons(false);
              clearStatusPane();
            }
          });

        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              handleException(xte);
              _progressDialog.setVisible(false);
              _progressDialog = null;
            }
          });
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _parent.getSparkIcon().getIconWidth();
    int iconHeight = _parent.getSparkIcon().getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _parent.getSparkIcon().paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(50, 0));
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    JPanel innerInitializePanel = new JPanel(new GridLayout(3, 1, 0, 5));



    JPanel cameraSelectionPanel = new JPanel(new FlowLayout());
    _cameraLabel.setText(StringLocalizer.keyToString("SERVICE_AVAILABLE_CAMERAS_KEY"));
    cameraSelectionPanel.add(_cameraLabel);
    cameraSelectionPanel.add(_camerasComboBox);
    innerInitializePanel.add(cameraSelectionPanel);

    JPanel initializeAndGetConfigPanel = new JPanel(new GridLayout(2, 1, 5, 25));
    initializeAndGetConfigPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    _getConfigurationButton.setText(StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_GET_CONFIG_DESCR_KEY"));

    JPanel innerConfigureXrayPanel = new JPanel(new FlowLayout());
    innerConfigureXrayPanel.setBorder(
        BorderFactory.createTitledBorder(LineBorder.createBlackLineBorder(),
                                         StringLocalizer.keyToString("SERVICE_UI_CONFIGURE_XRAY_CAMERA_LABEL_KEY"),
                                         TitledBorder.CENTER,
                                         TitledBorder.DEFAULT_POSITION));
    
	//Kee Chin Seong - Forcing ugrade firmware.
    //Wong Swee Yee - XCR-3571 Allow to force upgrade camera firmware
    _forceUpdateFirmwareCheckBox.setText(StringLocalizer.keyToString("SERVICE_UI_FORCE_UPGRADE_FIRMWARE_CHECK_BOX_LABEL_KEY"));
    if(Config.isDeveloperDebugModeOn())
    {
      innerConfigureXrayPanel.add(_forceUpdateFirmwareCheckBox);
    }
    
    _upgradeFirmwareButton.setText(StringLocalizer.keyToString("SERVICE_UI_UPGRADE_FIRMWARE_BUTTON_KEY"));
    innerConfigureXrayPanel.add(_upgradeFirmwareButton);

    initializeAndGetConfigPanel.setOpaque(false);
    initializeAndGetConfigPanel.add(_initializeButton);
    initializeAndGetConfigPanel.add(_getConfigurationButton);

    innerInitializePanel.add(initializeAndGetConfigPanel);
    innerInitializePanel.add(innerConfigureXrayPanel);


    JPanel buttonPanel = new JPanel(new BorderLayout());
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(30, 30, 0, 30));
    buttonPanel.setOpaque(false);
    buttonPanel.add(innerInitializePanel, BorderLayout.NORTH);

    JPanel statusPanel = new JPanel(new BorderLayout());
    statusPanel.setBorder(BorderFactory.createCompoundBorder(
        new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                         StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_MESSAGES_KEY")),
        BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    statusPanel.add(_statusPaneScrollPane, BorderLayout.CENTER);
    _statusPaneScrollPane.setOpaque(false);
    //Swee Yee Wong - XCR-3577 System crash when initialize xray camera while camera is upgrading firmware
    DefaultCaret caret = (DefaultCaret)_statusPane.getCaret();
    caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    _statusPaneScrollPane.getViewport().add(_statusPane);
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_FEATURE)==false)
    {
        add(statusPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.EAST);
    }
    else
    {
        try           
        {
            if(Config.isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled()==true)
            {
                _xrayCameraSPC = new XrayCameraSPCPanel("","SPC");

                JPanel chartPanel = new JPanel(new BorderLayout());
                chartPanel.setBorder(BorderFactory.createCompoundBorder(
                    new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                     ""),
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
                 JScrollPane chartPaneScrollPane = new JScrollPane();
                chartPanel.add(chartPaneScrollPane, BorderLayout.CENTER);
                chartPaneScrollPane.setOpaque(false);
                chartPaneScrollPane.getViewport().add(_xrayCameraSPC);

                add(_centerTabPane, BorderLayout.CENTER);
                _centerTabPane.setFont(FontUtil.getBoldFont(_centerTabPane.getFont(),Localization.getLocale()));
                _centerTabPane.add(statusPanel, StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_CONFIG_INFO_TAB_KEY"));
                _centerTabPane.add(chartPanel, StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_SPC_CHART_TAB_KEY"));



                // Configure the tabs to scroll
                _centerTabPane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
                int _currentTabIndex = _centerTabPane.getSelectedIndex();

                add(buttonPanel, BorderLayout.EAST);

                JPanel innerSPCPanel = new JPanel(new FlowLayout());
                innerSPCPanel.setBorder(
                BorderFactory.createTitledBorder(LineBorder.createBlackLineBorder(),
                                                 StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_AREA_MODE_SPC_KEY"),
                                                 TitledBorder.CENTER,
                                                 TitledBorder.DEFAULT_POSITION));
                _cameraSPCButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GENERATE_SPC_CHART_KEY"));



                ButtonGroup rSPCButtonGroup = new ButtonGroup( );
                _rMeanRadioButton = new JRadioButton(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_MEAN_KEY"));
                _rStdDevRadioButton = new JRadioButton(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_STD_DEV_KEY"));
                _rBetaRadioButton = new JRadioButton(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_BETA_KEY"));
                _rAboveMeanGradientRadioButton = new JRadioButton(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_ABOVE_MEAN_KEY"));
                _rMaxGradientRadioButton = new JRadioButton(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_MAX_KEY"));


                _cbCPCheckBox = new JCheckBox(StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_SHOW_CP_KEY"));

                _rAboveMeanGradientRadioButton.setSelected(true);
                rSPCButtonGroup.add(_rMeanRadioButton);
                rSPCButtonGroup.add(_rStdDevRadioButton);
                rSPCButtonGroup.add(_rBetaRadioButton);
                rSPCButtonGroup.add(_rAboveMeanGradientRadioButton);
                rSPCButtonGroup.add(_rMaxGradientRadioButton);


                JPanel jplRadio = new JPanel(new BorderLayout());
                jplRadio.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                jplRadio.setPreferredSize(new Dimension(170, 80));
                jplRadio.setLayout(new GridLayout(0, 1));
                jplRadio.setOpaque(true);

                jplRadio.add(_rMeanRadioButton);
                jplRadio.add(_rStdDevRadioButton);
                jplRadio.add(_rBetaRadioButton);
                jplRadio.add(_rAboveMeanGradientRadioButton);
                jplRadio.add(_rMaxGradientRadioButton);

                JPanel jplAreaMode = new JPanel(new BorderLayout());
                jplAreaMode.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
                jplAreaMode.setLayout(new GridLayout(0, 1));
                jplAreaMode.setOpaque(false);

                jplAreaMode.add(jplRadio);
                jplAreaMode.add(_cbCPCheckBox);

                jplAreaMode.add(_cameraSPCButton);
                _cameraSPCButton.setPreferredSize(new Dimension(170, 20));
                innerSPCPanel.add(jplAreaMode);

                JPanel buttonSPCPanel = new JPanel(new BorderLayout());
                buttonSPCPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
                buttonSPCPanel.setOpaque(false);
                buttonSPCPanel.add(innerSPCPanel, BorderLayout.NORTH);

                buttonPanel.add(buttonSPCPanel, BorderLayout.CENTER);
            }
            else
            {
                add(statusPanel, BorderLayout.CENTER);
                add(buttonPanel, BorderLayout.EAST);
            }

        }
        catch (BusinessException ex)
        {
        }
      }
  }
  private ActionListener _cameraSPCButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
        if  (_currentSelectedCamera != null)
        {
            _centerTabPane.setSelectedIndex(1);

            int cameraID = _currentSelectedCamera.getId();

            int radioSelection = ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX;

            if(_rMeanRadioButton.isSelected())
            {
                radioSelection = ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN_INDEX;
            }
            else if (_rStdDevRadioButton.isSelected())
            {
                radioSelection =  ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STD_DEV_INDEX;
            }
            else if (_rAboveMeanGradientRadioButton.isSelected())
            {
                radioSelection =  ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX;
            }
            else if (_rMaxGradientRadioButton.isSelected())
            {
                radioSelection =  ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX_INDEX;
            }
            else if (_rBetaRadioButton.isSelected())
            {
                radioSelection =  ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA_INDEX;
            }
            XrayCameraQualityMonitoring xrayCameraQualityMonitoring= new XrayCameraQualityMonitoring();
            boolean alarm = xrayCameraQualityMonitoring.generateXBarChart(String.valueOf(cameraID *10+radioSelection),false,_cbCPCheckBox.isSelected());
            String xBarChartImageFilename=xrayCameraQualityMonitoring.getImageFilename();
           _xrayCameraSPC.panelXbarChart.setXBarChartImage( xBarChartImageFilename);
        }
        else
        {
            JOptionPane.showMessageDialog(null,StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_CAMERA_SELECTION_KEY"));
        }
    }
  };

  /**
   * @author Erica Wheatcroft
   * @edited By Kee Chin Seong
   */
  public void start()
  {
    try
    {
      if (_xrayCameraArray.isStartupRequired() || _xrayCameraController.isStartupRequired())
        _initialized = false;
      else
        _initialized = true;
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }
    
    //always restart the checkbox to false
    _forceUpdateFirmwareCheckBox.setSelected(false);

    // set the state of the buttons
    disableButtons(false);

    // add the listeners
    addListeners();

    populatePanel();

    startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    _loadingSwingComponent = true;
    _camerasComboBox.removeAllItems();
    // first lets add the key word ALL to the combo box. The use can use this when they want to manipulate all
    // cameras at one time
    _camerasComboBox.addItem(_ALL);

    // now lets populate the combo box with all the available cameras.
    _cameras = _xrayCameraArray.getCameras();

    // sort the cameras so that they are ordered correctly
    Collections.sort(_cameras);

    // place them into the combo box
    for (AbstractXrayCamera camera : _cameras)
      _camerasComboBox.addItem(camera);

    // we want the ALL to be selected
    _camerasComboBox.setSelectedIndex(0);
    _loadingSwingComponent = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void getStatus()
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        java.util.List<HardwareConfigurationDescriptionInt> descriptions = new ArrayList<HardwareConfigurationDescriptionInt>();
        if (_useAllCameras)
        {
          // get the config for all cameras
          clearStatusPane();

          for (AbstractXrayCamera camera : _cameras)
          {
            descriptions.addAll(camera.getConfigurationDescription());
            Assert.expect(descriptions != null);

            displayDescription(descriptions);
          }
        }
        else
        {
          // get the config for only one camera
          Assert.expect(_currentSelectedCamera != null);

          clearStatusPane();
          descriptions = _currentSelectedCamera.getConfigurationDescription();
          Assert.expect(descriptions != null);

          displayDescription(descriptions);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void displayDescription(java.util.List<HardwareConfigurationDescriptionInt> descriptions)
  {
    StringBuffer statusMessageBuffer = new StringBuffer();
    for (HardwareConfigurationDescriptionInt configuration : descriptions)
    {
      LocalizedString localizedString = new LocalizedString(configuration.getKey(),
                                                            configuration.getParameters().toArray());
      String message = StringUtil.format(StringLocalizer.keyToString(localizedString), 50);
      statusMessageBuffer.append(message);
      statusMessageBuffer.append("\n");
    }
    _statusPane.setText(statusMessageBuffer.toString());
  }

  /**
   * @author Erica Wheatcroft
   */
  private void clearStatusPane()
  {
    try
    {
      _statusPane.getDocument().remove(0, _statusPane.getDocument().getLength());
    }
    catch (javax.swing.text.BadLocationException ex)
    {
      // this should not happen
      Assert.expect(false);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _camerasComboBox.addActionListener(_camerasComboBoxActionListener);
    _getConfigurationButton.addActionListener(_getConfigurationButtonActionListener);
    _upgradeFirmwareButton.addActionListener(_upgradeFirmwareButtonActionListener);
    _cameraSPCButton.addActionListener(_cameraSPCButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _camerasComboBox.removeActionListener(_camerasComboBoxActionListener);
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _getConfigurationButton.removeActionListener(_getConfigurationButtonActionListener);
    _upgradeFirmwareButton.removeActionListener(_upgradeFirmwareButtonActionListener);
    _cameraSPCButton.removeActionListener(_cameraSPCButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // remove the listeners
    removeListeners();

    stopObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof XrayCameraEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(XrayCameraEventEnum.INITIALIZE))
              {
                if(_useAllCameras)
                {
                  // if the user is initializing all cameras then we need to wait for all cameras to be completed before
                  // we shutdown the progress bar.
                  if(_initializedCameraCount == _MAX_NUMBER_CAMERAS - 1)
                    _progressDialog.updateProgressBarPrecent(100);
                  else
                    _initializedCameraCount++;
                }
                else
                {
                  _progressDialog.updateProgressBarPrecent(100);
                }
              }
			  // Swee Yee Wong - XCR-3125
			  // Support new camera upgrade firmware function
              else if (eventEnum.equals(XrayCameraEventEnum.VERIFY_FILE_VERSION)
                || eventEnum.equals(XrayCameraEventEnum.UPLOAD_CAMERA_FIRMFARE_FILE)
                || eventEnum.equals(XrayCameraEventEnum.UPDATE_FIRMWARE))
              {
                if(_monitoring == true)
                {
                  final XrayCameraEventEnum cameraEnum = (XrayCameraEventEnum)eventEnum;
                  final AbstractXrayCamera xrayCamera = (AbstractXrayCamera)event.getSource();
                  final String message = xrayCamera.getUpgradingInfoMessage();
                  if (event.isStart())
                  {
                    SwingUtils.invokeLater(new Runnable()
                    {
                      public void run()
                      {
                        _statusPane.append(cameraEnum.toString());
                        _statusPane.append("\n");
                        _statusPane.append("Camera: " + xrayCamera.getId() + "\n");
                        if(message.length() > 0)
                          _statusPane.append(message);
                        _statusPane.append("\n");
                      }
                    });
                  }
                  else if(event.isStart()==false)
                  {
                    SwingUtils.invokeLater(new Runnable()
                    {
                      public void run()
                      {
                        if(message.length() > 0)
                          _statusPane.append(message);
                        _statusPane.append("Camera: " + xrayCamera.getId() + " - " + cameraEnum.toString() + "\n");
                        _statusPane.append("Done\n");
                        _statusPane.append("=====================================================\n");
                        _statusPane.append("\n");
                      }
                    });
                  }
                }
              }
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                         StringLocalizer.keyToString("SERVICE_CAMERAS_INITIALIZED_KEY"),
                                         StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  protected boolean continueUpgradeFirmwareDialog()
  {
    // confirm with the user if they want to export the dialog

    int response = ChoiceInputDialog.showConfirmDialog(MainMenuGui.getInstance(),
                                                       StringLocalizer.keyToString("SERVICE_UI_CONTINUE_UPGRADE_FIRMWARE_MESSAGE_KEY"),
                                                       StringLocalizer.keyToString("SERVICE_UI_CONTINUE_UPGRADE_FIRMWARE_TITLE_KEY"));

    if (response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION)
      return false;

    return true;
  }
}
