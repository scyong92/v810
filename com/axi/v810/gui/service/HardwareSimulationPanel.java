package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;


/**
 * @author Erica Wheatcroft
 */
public class HardwareSimulationPanel extends JPanel implements TaskPanelInt, Observer
{
  private JCheckBox _panelHandlerCheckbox = new JCheckBox();
  private JCheckBox _panelPositionerCheckbox = new JCheckBox();
  private JCheckBox _xraySourceCheckbox = new JCheckBox();
  private JCheckBox _interlocksCheckbox = new JCheckBox();
  private JCheckBox _digitialIoCheckbox = new JCheckBox();
  private JCheckBox _indicatorsCheckbox = new JCheckBox();
  private JCheckBox _yAxisMotionControlCheckbox = new JCheckBox();
  private JCheckBox _xAxisMoitionControlCheckbox = new JCheckBox();
  private JCheckBox _railWidthAxisMotionControlCheckbox = new JCheckBox();
  private JCheckBox _allAxisForMotionControlCheckbox = new JCheckBox();
  private JCheckBox _simulateAllHardwareSubsystemsCheckbox = new JCheckBox();
  private JCheckBox _opticalCameraCheckbox = new JCheckBox();
  private JCheckBox _ebleCheckbox = new JCheckBox();

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private ProgressDialog _progressDialog = null;
  private boolean _systemInitialized = false;
  private MotionControl _motionControl = null;

  private SubsystemStatusAndControlTaskPanel _parent = null;

  private ActionListener _panelHandlerCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_panelHandlerCheckbox.isSelected())
        {
          PanelHandler.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                // since we are cleaing the simuatlion mode we need to force the subsystem to reintialize itself
                XrayTester.getInstance().shutdownMotionRelatedHardware();

                // now clear the simulation mode
                PanelHandler.getInstance().clearSimulationMode();

                notifySimulationStateChanged();
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _panelPositionerCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_panelPositionerCheckbox.isSelected())
        {
          PanelPositioner.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {

          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                // since we are cleaing the simuatlion mode we need to force the subsystem to reintialize itself
                PanelPositioner.getInstance().shutdown();

                PanelPositioner.getInstance().clearSimulationMode();

                notifySimulationStateChanged();
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _xraySourceCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_xraySourceCheckbox.isSelected())
        {
          AbstractXraySource.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                // since we are cleaing the simuatlion mode we need to force the subsystem to reintialize itself
                AbstractXraySource.getInstance().shutdown();

                AbstractXraySource.getInstance().clearSimulationMode();

                notifySimulationStateChanged();
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _interlocksCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {

      try
      {
        if (_interlocksCheckbox.isSelected())
        {
          Interlock.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                Interlock.getInstance().shutdown();
                DigitalIo.getInstance().shutdown();

                Interlock.getInstance().clearSimulationMode();

                notifySimulationStateChanged();

              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _digitialIoCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_digitialIoCheckbox.isSelected())
        {
          DigitalIo.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                DigitalIo.getInstance().shutdown();

                DigitalIo.getInstance().clearSimulationMode();

                notifySimulationStateChanged();

              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }

    }
  };
  private ActionListener _indicatorsCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_indicatorsCheckbox.isSelected())
        {
          LightStack.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                DigitalIo.getInstance().shutdown();

                LightStack.getInstance().clearSimulationMode();

                notifySimulationStateChanged();

              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };  
  private ActionListener _opticalCameraCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_opticalCameraCheckbox.isSelected())
        {
          OpticalCameraArray.getInstance().setSimulationMode();          
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                OpticalCameraArray.getInstance().shutdown();
                OpticalCameraArray.getInstance().clearSimulationMode();

                notifySimulationStateChanged();

              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _ebleCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        if (_ebleCheckbox.isSelected())
        {
          EthernetBasedLightEngineProcessor.getInstance().setSimulationMode();
          notifySimulationStateChanged();
        }
        else
        {
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                EthernetBasedLightEngineProcessor.getInstance().shutdown();
                EthernetBasedLightEngineProcessor.getInstance().clearSimulationMode();

                notifySimulationStateChanged();

              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _yAxisMotionControlCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initializeSubsystemIfNeeded();
      if (_systemInitialized == false)
      {
        _yAxisMotionControlCheckbox.setSelected(!_yAxisMotionControlCheckbox.isSelected());
        return;
      }
      if (_yAxisMotionControlCheckbox.isSelected())
      {
        _motionControl.setSimulationMode(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
        notifySimulationStateChanged();
      }
      else
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _motionControl.clearSimulationMode(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS);
              _motionControl.shutdown();
              notifySimulationStateChanged();
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                }
              });
            }
          }
        });
      }
    }
  };
  private ActionListener _xAxisMoitionControlCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initializeSubsystemIfNeeded();
      if (_systemInitialized == false)
      {
        _xAxisMoitionControlCheckbox.setSelected(!_xAxisMoitionControlCheckbox.isSelected());
        return;
      }
      if (_xAxisMoitionControlCheckbox.isSelected())
      {
        _motionControl.setSimulationMode(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
        notifySimulationStateChanged();
      }
      else
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _motionControl.clearSimulationMode(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS);
              _motionControl.shutdown();
              notifySimulationStateChanged();
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                }
              });
            }
          }
        });
      }
    }
  };
  private ActionListener _railWidthAxisMotionControlCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initializeSubsystemIfNeeded();
      if (_systemInitialized == false)
      {
        _railWidthAxisMotionControlCheckbox.setSelected(!_railWidthAxisMotionControlCheckbox.isSelected());
        return;
      }

      if (_railWidthAxisMotionControlCheckbox.isSelected())
      {
        _motionControl.setSimulationMode(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
        notifySimulationStateChanged();
      }
      else
      {
        _hardwareWorkerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _motionControl.clearSimulationMode(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS);
              _motionControl.shutdown();
              notifySimulationStateChanged();
            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                }
              });
            }
          }
        });
      }
    }
  };
  private ActionListener _allAxisForMotionControlCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      initializeSubsystemIfNeeded();
      if (_systemInitialized == false)
      {
        _allAxisForMotionControlCheckbox.setSelected(!_allAxisForMotionControlCheckbox.isSelected());
        return;
      }

      try
      {
        if (_allAxisForMotionControlCheckbox.isSelected())
        {
          _motionControl.setSimulationMode();
          disableAndCheckAllMotionControlCheckBoxes(true);
          notifySimulationStateChanged();
        }
        else
        {
          disableAndCheckAllMotionControlCheckBoxes(false);
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                _motionControl.clearSimulationMode();
                _motionControl.shutdown();
                notifySimulationStateChanged();
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
            }
          });
        }
      }
      catch (XrayTesterException xte)
      {
        handleException(xte);
      }
    }
  };
  private ActionListener _simulateAllHardwareSubsystemsCheckboxAction = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {

        if (_simulateAllHardwareSubsystemsCheckbox.isSelected())
        {
          XrayTester.getInstance().setSimulationMode();
          disableAndCheckAllOtherCheckBoxes(true);
          notifySimulationStateChanged();
        }
        else
        {
          disableAndCheckAllOtherCheckBoxes(false);
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                if(XrayTester.getInstance().isStartupRequired() == false)
                  XrayTester.getInstance().shutdown();                
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                  }
                });
              }
              finally
              {
                try
                {
                  // Siew Yeng - XCR1426
                  XrayTester.getInstance().clearSimulationMode();                  
                }
                catch(final XrayTesterException xte)
                {
                  // do nothing
                }
                finally
                {
                  notifySimulationStateChanged();
                }
              }
            }
          });
        }
      }
      catch (DatastoreException de)
      {
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(de.getMessage(), 50),
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  HardwareSimulationPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void initializeSubsystemIfNeeded()
  {
    Assert.expect(_motionControl != null);

    try
    {
      if (_motionControl.isStartupRequired() == false)
        _systemInitialized = true;
      else
      {
        // the system is not initialized so lets prompt the user to see if they would like to initialize
        String message = StringLocalizer.keyToString("SERVICE_UI_INITIALIZE_MOTION_CONTROL_KEY");
        int response = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                     StringUtil.format(message, 50),
                                                     StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                                     JOptionPane.YES_NO_OPTION,
                                                     JOptionPane.QUESTION_MESSAGE);
        if (response != JOptionPane.YES_OPTION)
        {
          // they do not wish to initialize the system
          _systemInitialized = false;
          return;
        }
        else
        {
          // they do want to initialize the subsystem
          createProgressDialog();
          _hardwareWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                // initialize the hardware
                _motionControl.startup();
                _systemInitialized = true;
              }
              catch (final XrayTesterException xte)
              {
                SwingUtils.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    handleException(xte);
                    _progressDialog.setVisible(false);
                    _progressDialog = null;
                    _systemInitialized = false;
                  }
                });
              }
            }
          });
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
      }
    }
    catch (XrayTesterException xte)
    {
      // an error occurred so lets display the error and return out
      handleException(xte);
      _systemInitialized = false;
    }

    return ;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof MotionControlEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(MotionControlEventEnum.INITIALIZE))
                _progressDialog.updateProgressBarPrecent(100);
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }


  /**
   * @author Erica Wheatcroft
   */
  private void disableAndCheckAllMotionControlCheckBoxes(boolean allHardwareSimOn)
  {
    _yAxisMotionControlCheckbox.setEnabled(!allHardwareSimOn);
    _yAxisMotionControlCheckbox.setSelected(allHardwareSimOn);

    _xAxisMoitionControlCheckbox.setEnabled(!allHardwareSimOn);
    _xAxisMoitionControlCheckbox.setSelected(allHardwareSimOn);

    _railWidthAxisMotionControlCheckbox.setSelected(allHardwareSimOn);
    _railWidthAxisMotionControlCheckbox.setEnabled(!allHardwareSimOn);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void disableAndCheckAllOtherCheckBoxes(boolean allHardwareSimOn)
  {
    _panelHandlerCheckbox.setSelected(allHardwareSimOn);
    _panelHandlerCheckbox.setEnabled(!allHardwareSimOn);
    _panelPositionerCheckbox.setSelected(allHardwareSimOn);
    _panelPositionerCheckbox.setEnabled(!allHardwareSimOn);
    _xraySourceCheckbox.setEnabled(!allHardwareSimOn);
    _xraySourceCheckbox.setSelected(allHardwareSimOn);
    _interlocksCheckbox.setEnabled(!allHardwareSimOn);
    _interlocksCheckbox.setSelected(allHardwareSimOn);
    _digitialIoCheckbox.setEnabled(!allHardwareSimOn);
    _digitialIoCheckbox.setSelected(allHardwareSimOn);
    _indicatorsCheckbox.setEnabled(!allHardwareSimOn);
    _indicatorsCheckbox.setSelected(allHardwareSimOn);
    _opticalCameraCheckbox.setEnabled(!allHardwareSimOn);
    _opticalCameraCheckbox.setSelected(allHardwareSimOn); 
    _ebleCheckbox.setEnabled(!allHardwareSimOn);
    _ebleCheckbox.setSelected(allHardwareSimOn);
    _yAxisMotionControlCheckbox.setEnabled(!allHardwareSimOn);
    _yAxisMotionControlCheckbox.setSelected(allHardwareSimOn);
    _xAxisMoitionControlCheckbox.setEnabled(!allHardwareSimOn);
    _xAxisMoitionControlCheckbox.setSelected(allHardwareSimOn);
    _railWidthAxisMotionControlCheckbox.setSelected(allHardwareSimOn);
    _railWidthAxisMotionControlCheckbox.setEnabled(!allHardwareSimOn);
    _allAxisForMotionControlCheckbox.setSelected(allHardwareSimOn);
    _allAxisForMotionControlCheckbox.setEnabled(!allHardwareSimOn);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    if (XrayTester.getInstance().isSimulationModeOn())
    {
      _simulateAllHardwareSubsystemsCheckbox.setSelected(true);
      disableAndCheckAllOtherCheckBoxes(true);
    }
    else
    {
      disableAndCheckAllOtherCheckBoxes(false);
      // lets check each subsystem
      if (PanelHandler.getInstance().isSimulationModeOn())
      {
        _panelHandlerCheckbox.setSelected(true);
      }

      if (PanelPositioner.getInstance().isSimulationModeOn())
      {
        _panelPositionerCheckbox.setSelected(true);
      }

      if (AbstractXraySource.getInstance().isSimulationModeOn())
      {
        _xraySourceCheckbox.setSelected(true);
      }

      if (LightStack.getInstance().isSimulationModeOn())
      {
        _indicatorsCheckbox.setSelected(true);
      }

      if (Interlock.getInstance().isSimulationModeOn())
      {
        _interlocksCheckbox.setSelected(true);
      }
      
      if (OpticalCameraArray.getInstance().isSimulationModeOn())
      {
        _opticalCameraCheckbox.setSelected(true);
      }
      
      if (EthernetBasedLightEngineProcessor.getInstance().isSimulationModeOn())
      {
        _ebleCheckbox.setSelected(true);
      }

      if (_motionControl.isSimulationModeOn())
      {
        disableAndCheckAllMotionControlCheckBoxes(true);
        _allAxisForMotionControlCheckbox.setSelected(true);
      }
      else
      {
        disableAndCheckAllMotionControlCheckBoxes(false);
        _allAxisForMotionControlCheckbox.setSelected(false);
        try
        {
          if (_motionControl.isStartupRequired() == false)
          {
            if (_motionControl.isSimulationModeOn(MotionAxisToMoveEnum.RAILWIDTH_INDEPENDENT_AXIS))
              _railWidthAxisMotionControlCheckbox.setSelected(true);
            else
              _railWidthAxisMotionControlCheckbox.setSelected(false);

            if (_motionControl.isSimulationModeOn(MotionAxisToMoveEnum.X_INDEPENDENT_AXIS))
              _xAxisMoitionControlCheckbox.setSelected(true);
            else
              _xAxisMoitionControlCheckbox.setSelected(false);

            if (_motionControl.isSimulationModeOn(MotionAxisToMoveEnum.Y_INDEPENDENT_AXIS))
              _yAxisMotionControlCheckbox.setSelected(true);
            else
              _yAxisMotionControlCheckbox.setSelected(false);
          }
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }
      }

      if (DigitalIo.getInstance().isSimulationModeOn())
      {
        _digitialIoCheckbox.setSelected(true);
      }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // lets setup motion control
    java.util.List<MotionControllerIdEnum> ids = MotionControllerIdEnum.getAllEnums();
    _motionControl = MotionControl.getInstance(ids.get(0));

    // add observers
    HardwareObservable.getInstance().addObserver(this);
    ProgressObservable.getInstance().addObserver(this);

    // populate the panel
    populatePanel();

    // add listeners
    addListners();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // delete observers
    HardwareObservable.getInstance().deleteObserver(this);
    ProgressObservable.getInstance().deleteObserver(this);

    // remove listeners
    removeListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListners()
  {
    _panelHandlerCheckbox.addActionListener(_panelHandlerCheckboxAction);
    _panelPositionerCheckbox.addActionListener(_panelPositionerCheckboxAction);
    _xraySourceCheckbox.addActionListener(_xraySourceCheckboxAction);
    _interlocksCheckbox.addActionListener(_interlocksCheckboxAction);
    _digitialIoCheckbox.addActionListener(_digitialIoCheckboxAction);
    _indicatorsCheckbox.addActionListener(_indicatorsCheckboxAction);
    _opticalCameraCheckbox.addActionListener(_opticalCameraCheckboxAction);
    _ebleCheckbox.addActionListener(_ebleCheckboxAction);
    _yAxisMotionControlCheckbox.addActionListener(_yAxisMotionControlCheckboxAction);
    _xAxisMoitionControlCheckbox.addActionListener(_xAxisMoitionControlCheckboxAction);
    _railWidthAxisMotionControlCheckbox.addActionListener(_railWidthAxisMotionControlCheckboxAction);
    _allAxisForMotionControlCheckbox.addActionListener(_allAxisForMotionControlCheckboxAction);
    _simulateAllHardwareSubsystemsCheckbox.addActionListener(_simulateAllHardwareSubsystemsCheckboxAction);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _panelHandlerCheckbox.removeActionListener(_panelHandlerCheckboxAction);
    _panelPositionerCheckbox.removeActionListener(_panelPositionerCheckboxAction);
    _xraySourceCheckbox.removeActionListener(_xraySourceCheckboxAction);
    _interlocksCheckbox.removeActionListener(_interlocksCheckboxAction);
    _digitialIoCheckbox.removeActionListener(_digitialIoCheckboxAction);
    _indicatorsCheckbox.removeActionListener(_indicatorsCheckboxAction);
    _opticalCameraCheckbox.removeActionListener(_opticalCameraCheckboxAction);
    _ebleCheckbox.removeActionListener(_ebleCheckboxAction);
    _yAxisMotionControlCheckbox.removeActionListener(_yAxisMotionControlCheckboxAction);
    _xAxisMoitionControlCheckbox.removeActionListener(_xAxisMoitionControlCheckboxAction);
    _railWidthAxisMotionControlCheckbox.removeActionListener(_railWidthAxisMotionControlCheckboxAction);
    _allAxisForMotionControlCheckbox.removeActionListener(_allAxisForMotionControlCheckboxAction);
    _simulateAllHardwareSubsystemsCheckbox.removeActionListener(_simulateAllHardwareSubsystemsCheckboxAction);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout(5, 15));

    JPanel northPanel = new JPanel(new FlowLayout());
    _simulateAllHardwareSubsystemsCheckbox.setText(StringLocalizer.keyToString("SERVICE_SIMULATE_HW_SYSTEMS_KEY"));
    northPanel.add(_simulateAllHardwareSubsystemsCheckbox);
    mainPanel.add(northPanel, BorderLayout.NORTH);

    JPanel individualSubsystemsPanel = new JPanel(new GridLayout(0, 3));

    JPanel westPanel = new JPanel(new GridLayout(4, 0, 0, 10));
    westPanel.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
    _panelHandlerCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_TAB_NAME_KEY"));
    _panelPositionerCheckbox.setText( StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_TAB_NAME_KEY"));
    _xraySourceCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"));
    _opticalCameraCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_OPTICAL_CAMERA_TAB_NAME_KEY"));
    westPanel.add(_panelPositionerCheckbox);
    westPanel.add(_panelHandlerCheckbox);
    westPanel.add(_xraySourceCheckbox);
    westPanel.add(_opticalCameraCheckbox);
    individualSubsystemsPanel.add(westPanel);

    JPanel centerPanel = new JPanel(new GridLayout(4, 0, 0, 10));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    _interlocksCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INTERLOCKS_TAB_NAME_KEY"));
    _digitialIoCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_DIGITAL_IO_TAB_NAME_KEY"));
    _indicatorsCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INDICATORS_TAB_NAME_KEY"));
    _ebleCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_EBLE_TAB_NAME_KEY"));
    centerPanel.add(_interlocksCheckbox);
    centerPanel.add(_indicatorsCheckbox);
    centerPanel.add(_digitialIoCheckbox);
    centerPanel.add(_ebleCheckbox);
    individualSubsystemsPanel.add(centerPanel);

    JPanel motionControlPanel = new JPanel(new GridLayout(2, 2, 0, 10));
    motionControlPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        StringLocalizer.keyToString("SERVICE_UI_SSACTP_MOTION_CONTROL_TAB_NAME_KEY")), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    _yAxisMotionControlCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_Y_AXIS_KEY"));
    _xAxisMoitionControlCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_X_AXIS_KEY"));
    _railWidthAxisMotionControlCheckbox.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_RAIL_WIDTH_ADJUSTMENT_KEY"));
    _allAxisForMotionControlCheckbox.setText(StringLocalizer.keyToString("SERVICE_ALL_KEY"));
    motionControlPanel.add(_allAxisForMotionControlCheckbox);
    motionControlPanel.add(_railWidthAxisMotionControlCheckbox);
    motionControlPanel.add(_xAxisMoitionControlCheckbox);
    motionControlPanel.add(_yAxisMotionControlCheckbox);
    individualSubsystemsPanel.add(motionControlPanel);

    mainPanel.add(individualSubsystemsPanel, BorderLayout.CENTER);
    add(mainPanel, BorderLayout.NORTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void notifySimulationStateChanged()
  {
    if(SwingUtilities.isEventDispatchThread())
      _parent.updatePanelWithSimulationStatus();
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
           _parent.updatePanelWithSimulationStatus();
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }
}
