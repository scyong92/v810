package com.axi.v810.gui.service;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.images.Image5DX;

/**
* This class controls the color for the results table.  Green for passing tests, red for failing.
*
* @author Bill Darbie
*/
class ResultTableCellRenderer extends JLabel implements TableCellRenderer
{
  private ImageIcon _testPassedIcon;
  private ImageIcon _testFailedIcon;

  private boolean _renderIcons = false;
  private boolean _renderErrorMessagesOnly = false;
  private boolean _renderToolTips = false;

  /**
   * @author Erica Wheatcroft
   */
  ResultTableCellRenderer()
  {
    _testPassedIcon = Image5DX.getImageIcon(Image5DX.CD_PASSED_LEAF_TEST);
    _testFailedIcon = Image5DX.getImageIcon(Image5DX.CD_FAILED_LEAF_TEST);
  }

  /**
   * @author Erica WHeatcroft
   */
  void renderIconsOnly()
  {
    _renderIcons = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  void renderErrorMessage()
  {
    _renderErrorMessagesOnly = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  void renderToolTips()
  {
    _renderToolTips = true;;
  }

  /**
   * @author Erica Wheatcroft
   */
  public Component getTableCellRendererComponent(JTable  table,
                                                 Object  value,
                                                 boolean isSelected,
                                                 boolean hasFocus,
                                                 int     row,
                                                 int     col)
  {
    Assert.expect(row >= 0 && col >= 0);

    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor = Color.WHITE;
    
    setOpaque(true);

    TableModel tableModel = table.getModel();
    Assert.expect(tableModel instanceof TestResultModel);
    TestResultModel testResultModel = (TestResultModel)tableModel;
    Result result = testResultModel.getResultAtRow(row);
    HardwareTaskStatusEnum status = result.getStatus();

    // set background
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
//      setBackground(table.getBackground());
      if(row%2 == 0)
        setBackground(backgroundColor);
      else
        setBackground(alternativeColor);
    }

    if (table.isRowSelected(row))
      setBackground(table.getSelectionBackground());
    else
    {
      if (hasFocus)
        setBackground(table.getSelectionBackground());
      else
      {
//        setBackground(table.getBackground());
        if (row % 2 == 0)
          setBackground(backgroundColor);
        else
          setBackground(alternativeColor);
      }
    }

    if (value != null)
    {
      String text = value.toString();
      setText(text);
    }


    if(_renderErrorMessagesOnly)
    {
      // we want to change the color of the text messages to red if the status is fail or partial fail
      if (value != null)
      {
        String text = value.toString();
        setText(text);
      }

      setFont(table.getFont());

      // set foreground
      setForeground(Color.black);

      if (status.equals(HardwareTaskStatusEnum.FAILED) || status.equals(HardwareTaskStatusEnum.PARTIAL_PASS) ||
          status.equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED)              )
        setForeground(Color.red);

      if(_renderToolTips)
        setToolTipText(result.getTaskStatusMessage());

      return this;
    }

    if(_renderIcons)
    {
      // we want to only show icons to indicate status
      if (status.equals(HardwareTaskStatusEnum.PASSED) ||
          status.equals(HardwareTaskStatusEnum.PARTIAL_PASS))
        setIcon(_testPassedIcon);
      else if (status.equals(HardwareTaskStatusEnum.FAILED) ||
               status.equals(HardwareTaskStatusEnum.PARTIAL_PASS) ||
               status.equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED))
        setIcon(_testFailedIcon);
      else if (status.equals(HardwareTaskStatusEnum.NOT_RUN))
        setIcon(null);

      setVerticalAlignment(SwingConstants.CENTER);
      setHorizontalAlignment(SwingConstants.CENTER);

      return this;
    }

    if(_renderToolTips)
    {
      if(col == 3)
      {
        setToolTipText(result.getTaskStatusMessage());
        return this;
      }
      else if(col == 4)
      {
        setToolTipText(result.getSuggestedUserActionMessage());
        return this;
      }
      else
        Assert.expect(false); // we only want tool tips on the message and actions columns.
    }

    return this;
  }
}
