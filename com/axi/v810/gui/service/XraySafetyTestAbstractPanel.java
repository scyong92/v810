package com.axi.v810.gui.service;

import java.io.*;
import java.net.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.util.*;

/**
 * This class is an abstract panel for Xray Safety Test panels.  It has common routines for
 * displaying HTML files in EditorPanes and default classes for setting and getting the selected test.
 *
 * @author George Booth
 */
public abstract class XraySafetyTestAbstractPanel extends AbstractTaskPanel
{
  public XraySafetyTestAbstractPanel(AbstractEnvironmentPanel envPanel)
  {
    super(envPanel);
    Assert.expect(envPanel != null);
    // do nothing
  }

  /**
   * instantiating class should override if needed
   * @author George Booth
   */
  protected void selectTest(String testName)
  {
    // do nothing
  }

  /**
   * instantiating class should override if needed
   * @author Geprge Booth
   */
  protected String getSelectedTest()
  {
    // do nothing
    return "";
  }

  // HTML display

  /**
   * @author Geprge Booth
   */
  protected URL convertKeyToURL(String key)
  {
    URL docsURL = null;

    if (key.length() > 0) // empty string
    {
      String valueOfKey = StringLocalizer.keyToString(key);

      if (valueOfKey.length() > 0)
      {
        try
        {
          docsURL = new URL(Directory.getXraySafetyTestFileDir() + File.separator + valueOfKey);
        }
        catch (MalformedURLException ex)
        {
          docsURL = null;
        }
      }
    }
    return docsURL;
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void setInstructionDescriptionText(JEditorPane editorPane, String text)
  {
    javax.swing.text.Document document = editorPane.getEditorKit().createDefaultDocument();
    editorPane.setDocument(document);
    editorPane.setText(text);
  }

  /**
   * @author Andy Mechtenberg
   */
  protected void setInstructionDescriptionPage(JEditorPane editorPane, URL page) throws IOException
  {
    javax.swing.text.Document document = editorPane.getEditorKit().createDefaultDocument();
    editorPane.setDocument(document);
    editorPane.setPage(page);
  }

  /**
   * @author Geprge Booth
   */
  protected void displayURL(JEditorPane editorPane, URL page)
  {
    try
    {
      setInstructionDescriptionPage(editorPane, page);
    }
    catch (IOException ex1)
    {
      editorPane.setContentType("text/html");
      setInstructionDescriptionText(
        editorPane, StringLocalizer.keyToString("SERVICE_UI_XST_HTML_FILE_NOT_FOUND_MESSAGES_KEY") + page.getFile());
    }
  }
}
