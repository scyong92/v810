package com.axi.v810.gui.service;

import java.util.*;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.hardware.*;
import com.axi.v810.gui.mainMenu.MainMenuGui;
import com.axi.v810.util.*;

/**
 * This table model is used to define all point - to - point moves.
 *
 * @author Erica Wheatcroft
 */
public class PointToPointMoveDefinitionTableModel extends DefaultTableModel implements Observer
{
  private java.util.List<StagePositionMutable> _tableEntries = null;

  private StagePositionMutable _currentSelectedRowEntry = null;

  private final String[] _columnNames =
      {StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_NUMBER_OF_ENTRIES_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_X_AXIS_COLUMN_HEADER_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_Y_AXIS_COLUMN_HEADER_KEY"),
      StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_UNITS_KEY")};

  private PanelPositioner _panelPositioner = null;
  private MathUtilEnum _units = null;

  /**
   * @author Erica Wheatcroft
   */
  public PointToPointMoveDefinitionTableModel()
  {
    _tableEntries = new ArrayList<StagePositionMutable>();
    _panelPositioner = PanelPositioner.getInstance();
    GuiObservable.getInstance().addObserver(this);
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   *
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _columnNames[columnIndex];
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and <code>columnIndex</code> is editable.
   *
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    if(_currentSelectedRowEntry == null || _currentSelectedRowEntry.equals(_tableEntries.get(rowIndex)) == false)
      return false;

    switch (columnIndex)
    {
      case 0: // point # in table 1 based
        return false;
      case 1: // x point
        return true;
      case 2: // y point
        return true;
      case 3: // units
        return false;
      default:
        Assert.expect(false);
    }

    Assert.expect(false);
    return false;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setSelectedRowEntry(StagePositionMutable entry)
  {
    Assert.expect(entry != null);
    _currentSelectedRowEntry = entry;
  }


  /**
   * Sets the value in the cell at <code>columnIndex</code> and <code>rowIndex</code> to <code>aValue</code>.
   *
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(_tableEntries != null);
    Assert.expect(aValue != null);
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    StagePositionMutable tableEntry = _tableEntries.get(rowIndex);
    switch (columnIndex)
    {
      case 0:
        // id
        Assert.expect(false); // not editable
        break;
      case 1:
        // x position
        validateXStartPosition(aValue, tableEntry);
        break;
      case 2:
        // y position
        validateYStartValue(aValue, tableEntry);
        break;
      case 3:
        // units
        Assert.expect(false); // not editable
        break;
      default:
        Assert.expect(false);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void validateXStartPosition(Object aValue, StagePositionMutable tableEntry)
  {
    Assert.expect(aValue instanceof Number);
    int originalValue = tableEntry.getXInNanometers();

    int convertedValue = 0;
    if (((Number)aValue) instanceof Long && _units.equals(MathUtilEnum.NANOMETERS))
      convertedValue = (int)((Number)aValue).longValue();
    else
      convertedValue = (int)MathUtil.convertUnits(((Number)aValue).doubleValue(), _units, MathUtilEnum.NANOMETERS);

    try
    {
      int xStartMinValueInNanometers = _panelPositioner.getXaxisMinimumPositionLimitInNanometers();
      int xStartMaxValueInNanometers = _panelPositioner.getXaxisMaximumPositionLimitInNanometers();
      if ((convertedValue >= xStartMinValueInNanometers && convertedValue <= xStartMaxValueInNanometers) == false)
      {
        double xStartMinValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(xStartMinValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        double xStartMaxValue = MathUtil.roundToPlaces((double)MathUtil.convertUnits(xStartMaxValueInNanometers,
            MathUtilEnum.NANOMETERS, _units),
            MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));

        // the value entered is not between the min and max defined by the panel positioner
        String message = null;
        if(((Number)aValue) instanceof Long)
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_X_START_INVALID_KEY",
                                                         new Object[]{((Number)aValue).longValue(), _units, xStartMinValue, _units, xStartMaxValue, _units}));
        else
          message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_X_START_INVALID_KEY",
                                                 new Object[]{((Number)aValue).doubleValue(), _units, xStartMinValue, _units, xStartMaxValue, _units}));

        message = StringUtil.format(message, 50);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        // set the original value back
        tableEntry.setXInNanometers(originalValue);
      }
      else
        tableEntry.setXInNanometers(convertedValue);

//      fireTableDataChanged();
    }
    catch (XrayTesterException xte)
    {
      // set the original value back
      tableEntry.setXInNanometers(originalValue);

      handleException(xte);
    }
  }

  /**
    * @author Erica Wheatcroft
    */
   private void handleException(XrayTesterException xte)
   {
     String message = xte.getMessage();
     JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                   StringUtil.format(message, 50),
                                   StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                   JOptionPane.ERROR_MESSAGE);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void validateYStartValue(Object aValue, StagePositionMutable tableEntry)
  {
    Assert.expect(aValue instanceof Number);
    int originalValue = tableEntry.getYInNanometers();

    int convertedValue = 0;
    if (((Number)aValue) instanceof Long && _units.equals(MathUtilEnum.NANOMETERS))
      convertedValue = (int)((Number)aValue).longValue();
    else
      convertedValue = (int)MathUtil.convertUnits(((Number)aValue).doubleValue(), _units, MathUtilEnum.NANOMETERS);

    // now lets verify that the value entered is correct
    try
    {
      int yStartMinValueInNanometers = _panelPositioner.getYaxisMinimumPositionLimitInNanometers();
      int yStartMaxValueInNanometers = _panelPositioner.getYaxisMaximumPositionLimitInNanometers();
      if ((convertedValue >= yStartMinValueInNanometers && convertedValue <= yStartMaxValueInNanometers) == false)
      {
        double valueToDisplay =  MathUtil.roundToPlaces((double)MathUtil.convertUnits(yStartMaxValueInNanometers, MathUtilEnum.NANOMETERS, _units),
                                         MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        String message = null;
        if(((Number)aValue) instanceof Long)
          message =  StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_REVERSE_KEY",
                                                         new Object[]{((Number)aValue).longValue(), _units, valueToDisplay, _units}));
        else
          message =  StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_PANEL_POSITIONER_Y_START_INVALID_SCAN_PASS_REVERSE_KEY",
                                                         new Object[]{((Number)aValue).doubleValue(), _units, valueToDisplay, _units}));
        message = StringUtil.format(message, 50);

        // set the original value back
        tableEntry.setYInNanometers(originalValue);

        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      message,
                                      StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                      JOptionPane.ERROR_MESSAGE);

        return;
      }
      else
      {
        tableEntry.setYInNanometers(convertedValue);
      }

//      fireTableDataChanged();

    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      // set the original value back
      tableEntry.setYInNanometers(originalValue);
    }
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return the number of columns in the model
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_columnNames != null);
    return _columnNames.length;
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return the number of rows in the model
   * @author Erica Wheatcroft
   */
  public int getRowCount()
  {
    if (_tableEntries == null)
      return 0;

    return _tableEntries.size();
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
   *
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_tableEntries != null);
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    StagePositionMutable tableEntry = _tableEntries.get(rowIndex);
    Object returnValue = null;
    switch (columnIndex)
    {
      case 0:
        returnValue = rowIndex + 1;
        break;
      case 1:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getXInNanometers();
        else
          returnValue = MathUtil.roundToPlaces(MathUtil.convertUnits(tableEntry.getXInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 2:
        if (_units.equals(MathUtilEnum.NANOMETERS))
          returnValue = (int)tableEntry.getYInNanometers();
        else
          returnValue = MathUtil.roundToPlaces(MathUtil.convertUnits(tableEntry.getYInNanometers(), MathUtilEnum.NANOMETERS, _units),
                                               MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
        break;
      case 3:
        returnValue = _units;
        break;
      default:
        Assert.expect(false);
    }

    return returnValue;
  }

  /**
   * @author Erica Wheatcroft
   */
  public StagePositionMutable insertRow()
  {
    StagePositionMutable tableEntry = new StagePositionMutable();
    _tableEntries.add(tableEntry);

    try
    {
      int xStartMinValueInNanometers = _panelPositioner.getXaxisMinimumPositionLimitInNanometers();
      tableEntry.setXInNanometers(xStartMinValueInNanometers);

      int yScanStartPositionLimitInNanometers = _panelPositioner.getYaxisMinimumPositionLimitInNanometers();
      tableEntry.setYInNanometers(yScanStartPositionLimitInNanometers);

      fireTableDataChanged();
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
    }

    fireTableRowsInserted(_tableEntries.size(), _tableEntries.size());

    return tableEntry;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clearSelectedRows(int row)
  {
    Assert.expect(row >= 0);
    _tableEntries.remove(row);

    fireTableRowsDeleted(row, row);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clearAllRows()
  {
    int numberOfRows = _tableEntries.size();
    _tableEntries.clear();

    fireTableRowsDeleted(0, numberOfRows);
  }

  /**
   * @author Erica WHeatcroft
   */
  public synchronized void update(Observable observable, Object arg)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if(arg instanceof GuiEvent)
      {
        GuiEvent event = (GuiEvent)arg;
        GuiEventEnum eventEnum = event.getGuiEventEnum();
        if (eventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)eventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
          {
            Assert.expect(event.getSource() instanceof MathUtilEnum);
            _units = (MathUtilEnum)event.getSource();
            fireTableDataChanged();
          }
        }
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * This method will change the units to whatever is sent in. It WILL NOT fire tableDataChangedEvent. This method
   * is intended to change all values to nanometers so it can be written out.
   *
   * @author Erica Wheatcroft
   */
  void changeUnits(MathUtilEnum units)
  {
    Assert.expect(units != null);
    _units = units;
  }

  /**
   * @author Erica Wheatcroft
   */
  MathUtilEnum getCurrentUnits()
  {
    Assert.expect(_units != null);
    return _units;
  }

  /**
   * @author Erica Wheatcroft
   */
  java.util.List<StagePositionMutable> getPoints()
  {
    Assert.expect(_tableEntries != null);
    return _tableEntries;
  }

  /**
   * @author Erica Wheatcroft
   */
  StagePositionMutable getTableEntryAt(int rowIndex)
  {
    Assert.expect(rowIndex >= 0 && rowIndex < _tableEntries.size());
    Assert.expect(_tableEntries != null);

    return _tableEntries.get(rowIndex);
  }

}
