package com.axi.v810.gui.service;

import com.axi.util.Enum;
import java.util.*;
import com.axi.v810.util.*;

public class HardwareOperationsEnum extends Enum
{
  private static Map<String, HardwareOperationsEnum> _stringToEnumMap = new HashMap<String,HardwareOperationsEnum>();
  private String _name;
  private static int _index = -1;

  public final static HardwareOperationsEnum OPEN_LEFT_OUTER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_OPEN_LEFT_OUTER_BARRIER_KEY"));
  public final static HardwareOperationsEnum CLOSE_LEFT_OUTER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_CLOSE_LEFT_OUTER_BARRIER_KEY"));
  public final static HardwareOperationsEnum OPEN_RIGHT_OUTER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_OPEN_RIGHT_OUTER_BARRIER_KEY"));
  public final static HardwareOperationsEnum CLOSE_RIGHT_OUTER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_CLOSE_RIGHT_OUTER_BARRIER_KEY"));
  public final static HardwareOperationsEnum OPEN_INNER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_OPEN_INNER_BARRIER_KEY"));
  public final static HardwareOperationsEnum CLOSE_INNER_BARRIER = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_CLOSE_INNER_BARRIER_KEY"));
  public final static HardwareOperationsEnum BELT_MOTION_ON = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_BELT_MOTION_ON_KEY"));
  public final static HardwareOperationsEnum BELT_MOTION_OFF = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_BELT_MOTION_OFF_KEY"));
  public final static HardwareOperationsEnum LEFT_TO_RIGHT_BELT_DIRECTION = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_LEFT_TO_RIGHT_BELT_MOTION_KEY"));
  public final static HardwareOperationsEnum RIGHT_TO_LEFT_BELT_DIRECTION = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RIGHT_TO_LEFT_BELT_MOTION_KEY"));
  public final static HardwareOperationsEnum OPEN_CLAMPS = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_OPEN_CLAMPS_KEY"));
  public final static HardwareOperationsEnum CLOSE_CLAMPS = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_CLOSE_CLAMPS_KEY"));
  public final static HardwareOperationsEnum EXTEND_LEFT_PIP = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_EXTEND_LEFT_PIP_KEY"));
  public final static HardwareOperationsEnum RETRACT_LEFT_PIP = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RETRACT_LEFT_PIP_KEY"));
  public final static HardwareOperationsEnum EXTEND_RIGHT_PIP = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_EXTEND_RIGHT_PIP_KEY"));
  public final static HardwareOperationsEnum RETRACT_RIGHT_PIP = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RETRACT_RIGHT_PIP_KEY"));
  public final static HardwareOperationsEnum RAIL_WIDTH_MAX = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RAIL_WIDTH_MAX_KEY"));
  public final static HardwareOperationsEnum RAIL_WIDTH_MIN = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RAIL_WIDTH_MIN_KEY"));
  public final static HardwareOperationsEnum RAIL_WIDTH_HOME = new HardwareOperationsEnum(++_index,
                                                 StringLocalizer.keyToString("HW_OPERATIONS_ENUM_RAIL_WIDTH_HOME_KEY"));

  /**
   * @author Erica Wheatcroft
   */
  private HardwareOperationsEnum(int id, String name)
  {
    super(id);
    _name = name;

    _stringToEnumMap.put(name, this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    return _name;
  }

  static Collection<HardwareOperationsEnum> getAllTypes()
  {
    return _stringToEnumMap.values();
  }
}
