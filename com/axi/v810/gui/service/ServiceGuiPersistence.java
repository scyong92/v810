package com.axi.v810.gui.service;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for persitance of certain Serive UI components.
 *
 * @author Erica Wheatcroft
 */
class ServiceGuiPersistence implements Serializable
{
  // confirmation, diagnostics, and adjustments settings
  private boolean _runToFirstFailure = true;
  private boolean _loopContinuoslyInCandDPanel = false;
  private int _loopCountInCandDPanel = 1;
  private int _cAndDSplitLocation = 300;

  // subsystem and status control settings
  private MathUtilEnum _selectedUnits = null;

  // panel positioner - scan moves panel
  private boolean _loopContinuoslyInScanMovesPanel = false;
  private int _loopCountInScanMovesPanel = 1;
  private int _delayBetweenScanPasses = 0;

  private static ServiceGuiPersistence _instance = null;

  /**
   * @author Erica Wheatcroft
   */
  public static ServiceGuiPersistence getInstance()
  {
    if(_instance == null)
      _instance = new ServiceGuiPersistence();

    return _instance;
  }

  /**
   * @author Erica Wheatcroft
   */
  private ServiceGuiPersistence()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  MathUtilEnum getSelectedUnits()
  {
    return _selectedUnits;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setSelectedUnits(MathUtilEnum selectedUnits)
  {
    Assert.expect(selectedUnits != null);
    _selectedUnits = selectedUnits;
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean runToFirstFailure()
  {
    return _runToFirstFailure;
  }

  /**
   * @author Erica Wheatcroft
   */
  void runToFirstFailure(boolean runToFirstFailure)
  {
    _runToFirstFailure = runToFirstFailure;
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean loopContinuosly()
  {
    return _loopContinuoslyInCandDPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  void loopContinuosly(boolean loopContinuosly)
  {
    _loopContinuoslyInCandDPanel = loopContinuosly;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getLoopCount()
  {
    return _loopCountInCandDPanel;
  }

  /**
   * @author Andy Mechtenberg
   */
  int getCandDSplitLocation()
  {
    return _cAndDSplitLocation;
  }

  /**
   * @author Erica Wheatcroft
   */
  boolean loopContinuoslyForScanPassPanel()
  {
    return _loopContinuoslyInScanMovesPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  void loopContinuoslyForScanPassPanel(boolean loopContinuosly)
  {
    _loopContinuoslyInScanMovesPanel = loopContinuosly;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getLoopCountForScanPassPanel()
  {
    return _loopCountInScanMovesPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setLoopCountForScanPassPanel(int loopCount)
  {
    Assert.expect(loopCount >= 1);
    _loopCountInScanMovesPanel = loopCount;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getScanPassDelay()
  {
    return _delayBetweenScanPasses;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setScanPassDelay(int delayInMiliSeconds)
  {
    Assert.expect(delayInMiliSeconds >= 0);
    _delayBetweenScanPasses = delayInMiliSeconds;
  }


  /**
   * @author Erica Wheatcroft
   */
  void setLoopCount(int loopCount)
  {
    Assert.expect(loopCount >= 0);
    _loopCountInCandDPanel = loopCount;
  }

  /**
   * @author Andy Mechtenberg
   */
  void setCandDSplitLocation(int dividerLocation)
  {
    _cAndDSplitLocation = dividerLocation;
  }


  /**
   * @author Erica Wheatcroft
   */
  public void readSettings()
  {
    try
    {
      if (FileUtilAxi.exists(FileName.getServiceUIStatePersistFullPath()))
        _instance = (ServiceGuiPersistence)FileUtilAxi.loadObjectFromSerializedFile(FileName.getServiceUIStatePersistFullPath());
      else
        _instance = new ServiceGuiPersistence();
    }
    catch (DatastoreException de)
    {
      _instance = new ServiceGuiPersistence();
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void writeSettings()
  {
    try
    {
      FileUtilAxi.saveObjectToSerializedFile(this, FileName.getServiceUIStatePersistFullPath());
    }
    catch (DatastoreException de)
    {
      Assert.logException(de);
    }
  }

}
