package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.HardwareConfigEnum;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.*;

/**
 * This class supports the X-ray Safety Test main task panel. The panel provides the infrastructure for
 * a Wizard-like sequencer for the safety test process. It displays the hardware status indicators, supports
 * panels for EditorPanes for displaying HTML files and provides cancel/back/next/finish buttons for sequencing.
 *
 * This panel will receive a list of process steps from the Test Selection panel and will sequence these steps
 * under user control. The x6000 hardware is configured as needed for each step.
 *
 * @author George Booth
 */

public class XraySafetyTestTaskPanel extends XraySafetyTestAbstractPanel implements Observer
{
  private static ImageIcon _spark = Image5DX.getImageIcon(Image5DX.AXI_SPARK);

  // hardware status panel
  private JPanel _hardwareStatusPanel;
  private Border _statusPanelBorder;
  private JPanel _xraySourcePanel;
  private JLabel _xraySourceVoltageLabel;
  private NumericTextField _xraySourceVoltageTextField;
  private JLabel _xraySourceCurrentLabel;
  private NumericTextField _xraySourceCurrentTextField;
  private JPanel _panelPositionerPanel;
  private JLabel _panelPositionerXAxisLabel;
  private NumericTextField _panelPositionerXAxisTextField;
  private JLabel _panelPositionerYAxisLabel;
  private NumericTextField _panelPositionerYAxisTextField;
  private JPanel _panelHandlerPanel;
  private JPanel _panelHandlerRailWidthPanel;
  private JLabel _panelHandlerRailWidthLabel;
  private NumericTextField _panelHandlerRailWidthTextField;
  private JPanel _outerBarriersPanel;
  private JLabel _outerBarriersLeftLabel;
  private JTextField _outerBarriersLeftTextField;
  private JLabel _outerBarriersRightLabel;
  private JTextField _outerBarriersRightTextField;
  private JPanel _innerBarrierPanel;
  private JTextField _innerBarrierTextField;
  private JPanel _magnificationPanel;
  private JTextField _magnificationTextField;

  // instruction panel
  private XraySafetyTestAbstractPanel _instructionPanel;

  // navigation panel
  private JPanel _navigationPanel;
  private JEditorPane _promptPane;
  private JPanel _buttonPanel;
  private FlowLayout _buttonPanelFlowLayout;
  private JPanel _cancelButtonPanel;
  private JButton _cancelButton;
  private JPanel _backPanel;
  private JButton _backButton;
  private JPanel _nextPanel;
  private JButton _nextButton;
  private JPanel _finishPanel;
  private JButton _finishButton;

  // button names..
  private char _cancelButtonMnemonicChar = 'C';
  private char _backButtonMnemonicChar = 'B';
  private char _nextButtonMnemonicChar = 'N';
  private char _finishButtonMnemonicChar = 'I';

  private final int _MINIMUM_WIDTH = 100;
  private final int _MINIMUM_HEIGHT = 50;
  private final int _PREFERRED_WIDTH = 820;
  private final int _PREFERRED_PROMPT_HEIGHT = 40;

  // parent panel
  private ServiceEnvironmentPanel _parent = null;

  // list of process steps
  private java.util.List<XraySafetyTestProcessStep> _processSteps;
  private String _currentTestName = "";
  private int _currentStep = -1;
  private int _lastStageXPosition = 0;
  private int _lastStageYPosition = 0;
  private int _lastRailWidth = 0;
  private XraySafetyTestPowerEnum _lastXrayPowerEnum = null;
  private boolean _innerBarrierClosed = false;  // false is pessimistic until known for sure
  private boolean _outerBarriersClosed = false;
  
  //Swee-Yee.Wong
  private boolean _highMagnification = false;
  
  private boolean _setAllHardware = false;

  private BusyCancelDialog _busyDialog;
  private boolean _stageChanging = false;
  private boolean _railChanging = false;
  private boolean _xrayTubeChanging = false;
  private boolean _barriersChanging = false;
  
  //Swee-Yee.Wong
  private boolean _magnificationChanging = false;

  private boolean _hardwareIsInitialized = false;
  private boolean _systemWasShutdown = false;
  private boolean _monitorEnabled = false;

  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private WorkerThread _monitorHardwareStatusThread = new WorkerThread("Hardware Status Monitor");
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private HTubeXraySource _xraySource = null;
  private InnerBarrier _innerBarrier = InnerBarrier.getInstance();
  private RightOuterBarrier _rightOuterBarrier = RightOuterBarrier.getInstance();
  private LeftOuterBarrier _leftOuterBarrier = LeftOuterBarrier.getInstance();
  //private XrayCylinder _xrayCylinder = XrayCylinder.getInstance();
  private XrayActuatorInt _xrayActuator = XrayActuator.getInstance();
  private Config _config = Config.getInstance();
  private MotionControl _motionControl = null;

  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");
  private StringBuffer _decimalFormat = new StringBuffer("########0.0");
  private MathUtilEnum _units = MathUtilEnum.MILS;
  private static final int _NUMBER_OF_DECIMAL_PLACES = 2;

  /**
   * @author George Booth
   */
  XraySafetyTestTaskPanel(ServiceEnvironmentPanel parent)
  {
    super(parent);
    Assert.expect(parent != null);
    _parent = parent;
    createPanel();
  }

  /**
   * @author George Booth
   */
  private void createPanel()
  {
    this.setLayout(new BorderLayout());

    // upper hardware status panel
    _hardwareStatusPanel = new JPanel();
    _hardwareStatusPanel.setLayout(new GridLayout(1, 5));
    this.add(_hardwareStatusPanel, BorderLayout.NORTH);

    _statusPanelBorder = BorderFactory.createCompoundBorder(
      BorderFactory.createEtchedBorder(Color.white,new Color(165, 163, 151)),
      BorderFactory.createEmptyBorder(0, 5, 5, 5));

    // show x-ray values with 1 decimal place
    //    StringBuffer decimalFormat = new StringBuffer("########0.");

//    int decimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_enumSelectedUnits);
//    for(int i = 0; i < decimalPlaces; i++)
//      decimalFormat.append("0");  // use a "0" if you want to show trailing zeros, use a "#" if you don't

    // x-ray source status
    _xraySourcePanel = new JPanel();
    _xraySourcePanel.setLayout(new GridLayout(2, 2, 5, 0));
    _xraySourcePanel.setBorder(new TitledBorder(_statusPanelBorder,
       StringLocalizer.keyToString("SERVICE_UI_XST_XRAY_SOURCE_PANEL_LABEL_KEY")));
    _hardwareStatusPanel.add(_xraySourcePanel);

    _xraySourceVoltageLabel = new JLabel();
    _xraySourceVoltageLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_XRAY_SOURCE_VOLTAGE_LABEL_KEY"));
    _xraySourceVoltageLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _xraySourcePanel.add(_xraySourceVoltageLabel);

    _xraySourceVoltageTextField = new NumericTextField();
    _xraySourceVoltageTextField.setText("0.0");
    _xraySourceVoltageTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _xraySourceVoltageTextField.setFormat(new DecimalFormat(_decimalFormat.toString()));
    _xraySourceVoltageTextField.setEditable(false);
    _xraySourcePanel.add(_xraySourceVoltageTextField);

    _xraySourceCurrentLabel = new JLabel();
    _xraySourceCurrentLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_XRAY_SOURCE_CURRENT_LABEL_KEY"));
    _xraySourceCurrentLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _xraySourcePanel.add(_xraySourceCurrentLabel);

    _xraySourceCurrentTextField = new NumericTextField();
    _xraySourceCurrentTextField.setText("0.0");
    _xraySourceCurrentTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _xraySourceCurrentTextField.setFormat(new DecimalFormat(_decimalFormat.toString()));
    _xraySourceCurrentTextField.setEditable(false);
    _xraySourcePanel.add(_xraySourceCurrentTextField);

    // panel positioner status
    _panelPositionerPanel = new JPanel();
    _panelPositionerPanel.setLayout(new GridLayout(2, 2, 5, 0));
    _panelPositionerPanel.setBorder(new TitledBorder(_statusPanelBorder,
    StringLocalizer.keyToString("SERVICE_UI_XST_PANEL_POSITIONER_PANEL_LABEL_KEY")));

    _hardwareStatusPanel.add(_panelPositionerPanel);

    _panelPositionerXAxisLabel = new JLabel();
    _panelPositionerXAxisLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_PANEL_POSITIONER_X_AXIS_LABEL_KEY"));
    _panelPositionerXAxisLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _panelPositionerPanel.add(_panelPositionerXAxisLabel);

    _panelPositionerXAxisTextField = new NumericTextField();
    _panelPositionerXAxisTextField.setText("0.0");
    _panelPositionerXAxisTextField.setHorizontalAlignment(SwingConstants.LEFT);
//    _panelPositionerXAxisTextField.setFormat(new DecimalFormat(_decimalFormat.toString()));
    _panelPositionerXAxisTextField.setEditable(false);
    _panelPositionerPanel.add(_panelPositionerXAxisTextField);

    _panelPositionerYAxisLabel = new JLabel();
    _panelPositionerYAxisLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_PANEL_POSITIONER_Y_AXIS_LABEL_KEY"));
    _panelPositionerYAxisLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _panelPositionerPanel.add(_panelPositionerYAxisLabel);

    _panelPositionerYAxisTextField = new NumericTextField();
    _panelPositionerYAxisTextField.setText("0.0");
    _panelPositionerYAxisTextField.setHorizontalAlignment(SwingConstants.LEFT);
//    _panelPositionerYAxisTextField.setFormat(new DecimalFormat(_decimalFormat.toString()));
    _panelPositionerYAxisTextField.setEditable(false);
    _panelPositionerPanel.add(_panelPositionerYAxisTextField);

    // panel handler status
    _panelHandlerPanel = new JPanel();
    _panelHandlerPanel.setLayout(new BorderLayout(5, 0));
    _panelHandlerPanel.setBorder(new TitledBorder(_statusPanelBorder,
      StringLocalizer.keyToString("SERVICE_UI_XST_PANEL_HANDLER_PANEL_LABEL_KEY")));
    _hardwareStatusPanel.add(_panelHandlerPanel);

    _panelHandlerRailWidthPanel = new JPanel();
    _panelHandlerRailWidthPanel.setLayout(new GridLayout(1, 2, 5, 0));
    _panelHandlerPanel.add(_panelHandlerRailWidthPanel, BorderLayout.NORTH);

    _panelHandlerRailWidthLabel = new JLabel();
    _panelHandlerRailWidthLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_PANEL_HANDLER_RAIL_WIDTH_LABEL_KEY"));
    _panelHandlerRailWidthLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _panelHandlerRailWidthPanel.add(_panelHandlerRailWidthLabel, BorderLayout.WEST);

    _panelHandlerRailWidthTextField = new NumericTextField();
    _panelHandlerRailWidthTextField.setText("4000.0");
    _panelHandlerRailWidthTextField.setHorizontalAlignment(SwingConstants.LEFT);
//    _panelHandlerRailWidthTextField.setFormat(new DecimalFormat(_decimalFormat.toString()));
    _panelHandlerRailWidthTextField.setEditable(false);
    _panelHandlerRailWidthPanel.add(_panelHandlerRailWidthTextField, BorderLayout.EAST);
    
    //Swee-Yee.Wong
    // inner barrier status
    _magnificationPanel = new JPanel();
    _magnificationPanel.setLayout(new BorderLayout());
    _magnificationPanel.setBorder(new TitledBorder(_statusPanelBorder,
      StringLocalizer.keyToString("SERVICE_UI_XST_MAGNIFICATION_PANEL_LABEL_KEY")));
    _hardwareStatusPanel.add(_magnificationPanel);

    _magnificationTextField = new JTextField();
    _magnificationTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_LOW_MAG_VALUE_KEY"));
    _magnificationTextField.setBackground(Color.GREEN);
    _magnificationTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _magnificationTextField.setEditable(false);
    _magnificationPanel.add(_magnificationTextField, BorderLayout.NORTH);

    // inner barrier status
    _innerBarrierPanel = new JPanel();
    _innerBarrierPanel.setLayout(new BorderLayout());
    _innerBarrierPanel.setBorder(new TitledBorder(_statusPanelBorder,
      StringLocalizer.keyToString("SERVICE_UI_XST_INNER_BARRIER_PANEL_LABEL_KEY")));
    _hardwareStatusPanel.add(_innerBarrierPanel);

    _innerBarrierTextField = new JTextField();
    _innerBarrierTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
    _innerBarrierTextField.setBackground(Color.GREEN);
    _innerBarrierTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _innerBarrierTextField.setEditable(false);
    _innerBarrierPanel.add(_innerBarrierTextField, BorderLayout.NORTH);

    // outer barriers status
    _outerBarriersPanel = new JPanel();
    _outerBarriersPanel.setLayout(new GridLayout(2, 2, 5, 0));
    _outerBarriersPanel.setBorder(new TitledBorder(_statusPanelBorder,
      StringLocalizer.keyToString("SERVICE_UI_XST_OUTER_BARRIERS_PANEL_LABEL_KEY")));
    _hardwareStatusPanel.add(_outerBarriersPanel);

    _outerBarriersLeftLabel = new JLabel();
    _outerBarriersLeftLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_LEFT_BARRIER_LABEL_KEY"));
    _outerBarriersLeftLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _outerBarriersPanel.add(_outerBarriersLeftLabel);

    _outerBarriersLeftTextField = new JTextField();
    _outerBarriersLeftTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
    _outerBarriersLeftTextField.setBackground(Color.GREEN);
    _outerBarriersLeftTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _outerBarriersLeftTextField.setEditable(false);
    _outerBarriersPanel.add(_outerBarriersLeftTextField);

    _outerBarriersRightLabel = new JLabel();
    _outerBarriersRightLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XST_RIGHT_BARRIER_LABEL_KEY"));
    _outerBarriersRightLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    _outerBarriersPanel.add(_outerBarriersRightLabel);

    _outerBarriersRightTextField = new JTextField();
    _outerBarriersRightTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
    _outerBarriersRightTextField.setBackground(Color.GREEN);
    _outerBarriersRightTextField.setHorizontalAlignment(SwingConstants.LEFT);
    _outerBarriersRightTextField.setEditable(false);
    _outerBarriersPanel.add(_outerBarriersRightTextField);

    // area for user instructions - initialize with intro screen
    // sub panels are swapped in and out of this panel when navigating
    _instructionPanel = new XraySafetyTestInstructionsPanel(_parent, "SERVICE_XST_SAFETY_AND_REGULATORY_KEY");
    this.add(_instructionPanel, BorderLayout.CENTER);

    // navigation prompt and buttons
    _navigationPanel = new JPanel();
    _navigationPanel.setLayout(new BorderLayout());
    _navigationPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
    this.add(_navigationPanel, BorderLayout.SOUTH);

    // create prompt area
    _promptPane = new JEditorPane();
    _promptPane.setEditable(false);
    _promptPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_PROMPT_HEIGHT));
    _promptPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _promptPane.setBackground(this.getBackground());
    _navigationPanel.add(_promptPane, BorderLayout.NORTH);

    _buttonPanelFlowLayout = new FlowLayout();
    _buttonPanelFlowLayout.setAlignment(FlowLayout.CENTER);

    _buttonPanel = new JPanel();
    _buttonPanel.setLayout(_buttonPanelFlowLayout);
    _buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
    _navigationPanel.add(_buttonPanel, BorderLayout.CENTER);

    _cancelButtonPanel = new JPanel();
    _cancelButtonPanel.setLayout(new GridLayout());
    _cancelButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 80));
    _buttonPanel.add(_cancelButtonPanel);

    _cancelButton = new JButton();
    _cancelButton.setText(StringLocalizer.keyToString("WIZARD_CANCEL_KEY"));
    _cancelButton.setMnemonic(_cancelButtonMnemonicChar);
    _cancelButton.setEnabled(true);
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleCancelButtonEvent();
      }
    });
    _cancelButtonPanel.add(_cancelButton);

    _backPanel = new JPanel();
    _backPanel.setLayout(new GridLayout());
    _backPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 20));
    _buttonPanel.add(_backPanel);

    _backButton = new JButton();
    _backButton.setText(StringLocalizer.keyToString("WIZARD_BACK_KEY"));
    _backButton.setMnemonic(_backButtonMnemonicChar);
    _backButton.setEnabled(false);
    _backButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handlePreviousScreenButtonEvent();
      }
    });
    _backPanel.add(_backButton);

    _nextPanel = new JPanel();
    _nextPanel.setLayout(new GridLayout());
    _nextPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
    _buttonPanel.add(_nextPanel);

    _nextButton = new JButton();
    _nextButton.setText(StringLocalizer.keyToString("WIZARD_NEXT_KEY"));
    _nextButton.setMnemonic(_nextButtonMnemonicChar);
    _nextButton.setEnabled(false);
    _nextButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleNextScreenButtonEvent();
      }
    });
    _nextPanel.add(_nextButton);

    _finishPanel = new JPanel();
    _finishPanel.setLayout(new GridLayout());
    _finishPanel.setBorder(BorderFactory.createEmptyBorder(0, 80, 0, 0));
    _buttonPanel.add(_finishPanel);

    _finishButton = new JButton();
    _finishButton.setText(StringLocalizer.keyToString("WIZARD_FINISH_KEY"));
    _finishButton.setMnemonic(_finishButtonMnemonicChar);
    _finishButton.setEnabled(false);
    _finishButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleProcessCompleteButtonEvent();
      }
    });
    _finishPanel.add(_finishButton);

  }

  /**
   * @author George Booth
   */
  void createInitialTestSteps()
  {
    _processSteps = new ArrayList<XraySafetyTestProcessStep>();

    // Intro screen - single instruction
    XraySafetyTestProcessStep step1 = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP);
    step1.setDocument1Key("SERVICE_XST_SAFETY_AND_REGULATORY_KEY");
    step1.setPromptKey("SERVICE_XST_SELECT_TEST_PROMPT_KEY");
    _processSteps.add(step1);
    // Test Selection screen - hard coded instruction
    XraySafetyTestProcessStep step2 = new XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum.TEST_SELECTION_STEP);
    step2.setPromptKey("SERVICE_XST_BEGIN_SELECTED_TEST_PROMPT_KEY");
    _processSteps.add(step2);
  }

  /**
   * @author George Booth
   */
  void displayProcessStep(XraySafetyTestProcessStep step, boolean nextStep)
  {
    _instructionPanel.finish();
    this.remove(_instructionPanel);

//    setupAllHardware(step);

    // show the step
    XraySafetyTestProcessStepEnum stepEnum = step.getStepEnum();
    String document1Key = step.getDocument1Key();
    String document2Key = step.getDocument2Key();
    String promptKey = step.getpromptKey();

    if (stepEnum.equals(XraySafetyTestProcessStepEnum.INSTRUCTION_STEP))
    {
      Assert.expect(document1Key != null);
      _instructionPanel = new XraySafetyTestInstructionsPanel(_parent, document1Key);
    }
    else if (stepEnum.equals(XraySafetyTestProcessStepEnum.TEST_SELECTION_STEP))
    {
      _instructionPanel = new XraySafetyTestSelectionPanel(_parent);
    }
    else if (stepEnum.equals(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_STEP))
    {
      Assert.expect(document1Key != null);
      _instructionPanel = new XraySafetyTestInstructionsPanel(_parent, document1Key);
    }
    else if (stepEnum.equals(XraySafetyTestProcessStepEnum.INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP))
    {
      Assert.expect(document1Key != null);
      Assert.expect(document2Key != null);
      _instructionPanel = new XraySafetyTestInstructionShutdownInstructionPanel(_parent, document1Key, document2Key);
    }

    else if (stepEnum.equals(XraySafetyTestProcessStepEnum.PROCESS_COMPLETE_STEP))
    {
      Assert.expect(document1Key != null);
      _instructionPanel = new XraySafetyTestInstructionsPanel(_parent, document1Key);
    }

    if (promptKey != null)
    {
      URL prompt = convertKeyToURL(promptKey);
      displayURL(_promptPane, prompt);
    }

    this.add(_instructionPanel, BorderLayout.CENTER);

    _instructionPanel.start();

    // make sure test selection panel knows what to select - others panels will ignore
    if (nextStep)
    {
      // came from initial panel - no test should be selected
      _instructionPanel.selectTest("");
    }
    else
    {
      // came from upstream panel - select current test
      _instructionPanel.selectTest(_currentTestName);
    }

    // update UI title with Step m of n if in test process steps
    if (_currentStep > 1)
    {
      int currentTestStep = _currentStep - 1;
      int lastTestStep = _processSteps.size() - 2;
      LocalizedString message = new LocalizedString("SERVICE_XST_STEP_M_OF_N_KEY",
                                                       new Object[] {currentTestStep, lastTestStep});
      String titleMessage = " - " + StringLocalizer.keyToString(message);
      debug("- - -" + titleMessage);
      _mainUI.AppendToTitle(titleMessage);
    }
    else
    {
      _mainUI.AppendToTitle("");
    }

    // allow UI to be refreshed before setting up hardware so user can see step info
    final XraySafetyTestProcessStep finalStep = step;
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // setup hardware as needed for this step
        setupAllHardware(finalStep);
      }
    });

    repaint();
  }

  /**
   * @author George Booth
   */
  void setupAllHardware(final XraySafetyTestProcessStep step)
  {
//    debug("setupAllHardware()");
    try
    {
      _hardwareIsInitialized = XrayTester.getInstance().isStartupRequired() == false;
      debug("  refresh hardware status");
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      return;
    }
    // don't talk to the hardware if a shutdown was done.
    // user will need to select a new test to restart
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      return;
    }
    if (_hardwareIsInitialized == false)
    {
      try
      {
        debug("  _mainUI.performStartupIfNeeded()");
        _mainUI.performStartupIfNeeded();
        _hardwareIsInitialized = XrayTester.getInstance().isStartupRequired() == false;
        debug("  _hardwareIsInitialized = " + _hardwareIsInitialized);
        // make sure proper safety test state is set after a normal startup
        _setAllHardware = true;
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);

        // XCR1144, Chnee Khang Wah, 15-Feb-2011
        if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);

        return;
      }
    }

    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    // if a panel is loaded, unload it
      try
      {
        if (PanelHandler.getInstance().isPanelLoaded())
        {
          _mainUI.unloadPanel();
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);

        // XCR1144, Chnee Khang Wah, 15-Feb-2011
        if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);

        return;
      }


    if (_hardwareIsInitialized)
    {
      startMonitoring();
    }

    setupStageXPosition(step.getStageXPositionNanometers());
    setupStageYPosition(step.getStageYPositionNanometers());

    setupRailWidth(step.getRailWidthNanometers());

    setupXrayPower(step.getXrayPowerEnum());

    // check if only barriers are changing
    setupBarriers(step);
    
    setupXrayActuator(step);

    // XCR-3822 System crash when switch to xray safety test tab
    if (ableToProceedXraySafetyTest() && (_stageChanging || _xrayTubeChanging || _barriersChanging || _magnificationChanging))
    {
      // open dialog
      createBusyDialog();

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          
          
          // make sure barriers are closed when changing setup
          setOuterBarriersClosed();
          //Swee-Yee.Wong - make sure low magnification is set when changing setup
          //Kee Chin Seong - Only have High Mag to Set to Low Mag
          if (Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == true ||
              Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == true )
            setMagnificationLow();
          
          setInnerBarrierClosed();

          // re-check if barriers are still changing (both are closed now)
          setupBarriers(step);
          
          //Swee-Yee.Wong - re-check if magnification is still changing (low mag now)
          setupXrayActuator(step);

          // do stuff
          if (_stageChanging)
          {
            moveStage(_lastStageXPosition, _lastStageYPosition);
          }

          if (_railChanging)
          {
            moveRail(_lastRailWidth);
          }

          if (_xrayTubeChanging)
          {
            setXrayPower(_lastXrayPowerEnum);
          }

          if (_barriersChanging)
          {
            if (step.isInnerBarrierClosed() == false)
            {
              setInnerBarrierOpen();
            }
            if (step.areOuterBarriersClosed() == false)
            {
              setOuterBarriersOpen();
            }
            _barriersChanging = false;
          }
          
          //Swee-Yee.Wong
          if (_magnificationChanging)
          {
            if (step.isXrayActuatorDown() == true)
            {
              setMagnificationHigh();
            }
            _magnificationChanging = false;
          }

          // close dialog after changing is done
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              do
              {
                try
                {
                  Thread.sleep(1000);
                }
                catch (InterruptedException ie)
                {
                  Assert.expect(false);
                }
              }
              while (_stageChanging || _xrayTubeChanging || _barriersChanging || _magnificationChanging );
              closeBusyDialog();
              _setAllHardware = false;
            }
          });
        }
      });

      // show busy dialog
      _busyDialog.setVisible(true);
    }
  }

  /**
   * @author George Booth
   */
  private void createBusyDialog()
  {
    _busyDialog = new BusyCancelDialog(
        MainMenuGui.getInstance(),
        StringLocalizer.keyToString("SERVICE_XST_BUSY_MESSAGE_KEY"),
        StringLocalizer.keyToString("SERVICE_XST_BUSY_TITLE_KEY"),
        true);

    _busyDialog.pack();
    SwingUtils.centerOnComponent(_busyDialog, MainMenuGui.getInstance());
  }

  /**
   * @author George Booth
   */
  private void closeBusyDialog()
  {
    if (_busyDialog != null)
    {
      // wait until visible if not forced
      while (_busyDialog.isVisible() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
      }
      _busyDialog.dispose();
      _busyDialog = null;
    }
  }

  /**
   * @author George Booth
   */
  void setupStageXPosition(int stageXPosition)
  {
    if (_setAllHardware || _lastStageXPosition != stageXPosition)
    {
      _lastStageXPosition = stageXPosition;
      _stageChanging = true;
    }
  }

  /**
   * @author George Booth
   */
  void setupStageYPosition(int stageYPosition)
  {
    if (_setAllHardware || _lastStageYPosition != stageYPosition)
    {
      _lastStageYPosition = stageYPosition;
      _stageChanging = true;
    }
  }

  /**
   * @author George Booth
   */
  void moveStage(int x, int y)
  {
    debug("moveStage(" + x + ", " + y + ")");
    Assert.expect(_panelPositioner != null);

    StagePositionMutable point = new StagePositionMutable();
    point.setXInNanometers(x);
    point.setYInNanometers(y);

    try
    {
      // Save off the previously loaded motion profile
      debug("  _panelPositioner.getActiveMotionProfile()");
      MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
      // Set to the fastest motion profile.
      debug("  _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1)");
      _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
      debug("  _panelPositioner.pointToPointMoveAllAxes(point)");
      _panelPositioner.pointToPointMoveAllAxes(point);
      // Now that we are done with the move, restore the motion profile.
      debug("  _panelPositioner.setMotionProfile(<originalMotionProfile>)");
      _panelPositioner.setMotionProfile(originalMotionProfile);
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);

      // XCR1144, Chnee Khang Wah, 15-Feb-2011
      if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);
    }
    finally
    {
      _stageChanging = false;
    }
  }

  /**
   * @author George Booth
   */
  void setupRailWidth(int railWidth)
  {
    if (_setAllHardware || _lastRailWidth != railWidth)
    {
      _lastRailWidth = railWidth;
      _railChanging = true;
    }
  }

  /**
   * @author George Booth
   */
  void moveRail(int width)
  {
    debug("moveRail(" + width + ")");
    Assert.expect(_panelHandler != null);

    try
    {
      debug("  _panelHandler.setRailWidthInNanoMeters()");
      _panelHandler.setRailWidthInNanoMeters(width);
    }
    catch (final XrayTesterException xte)
    {
      handleXrayTesterException(xte);

      // XCR1144, Chnee Khang Wah, 15-Feb-2011
      if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);
    }
    finally
    {
      _railChanging = false;
    }
  }

  /**
   * @author George Booth
   */
  void setupXrayPower(XraySafetyTestPowerEnum xrayPowerEnum)
  {
    if (_setAllHardware || _lastXrayPowerEnum != xrayPowerEnum)
    {
      _lastXrayPowerEnum = xrayPowerEnum;
      _xrayTubeChanging = true;
    }
  }

  /**
   * @author George Booth
   */
  void setXrayPower(XraySafetyTestPowerEnum xrayPowerEnum)
  {
    debug("setXrayVoltageAndCurrent(" + xrayPowerEnum.toString() + ")");
    try
    {
      if (xrayPowerEnum.equals(XraySafetyTestPowerEnum.POWER_OFF))
      {
        debug("  _xraySource.powerOff()");
        _xraySource.powerOff();
      }
      else if (xrayPowerEnum.equals(XraySafetyTestPowerEnum.LOW_POWER))
      {
        debug("  _xraySource.lowPowerOn()");
        _xraySource.lowPowerOn();
      }
//      else if (xrayPowerEnum.equals(XraySafetyTestPowerEnum.NORMAL_POWER))
//      {
//        debug("  _xraySource.normalPowerOn()");
//        _xraySource.normalPowerOn();
//      }
      else if (xrayPowerEnum.equals(XraySafetyTestPowerEnum.HIGH_POWER))
      {
        debug("  _xraySource.highPowerOn()");
        _xraySource.highPowerOn();
      }
      else
      {
        Assert.expect(false, "Unsupported X-ray power; power = " + xrayPowerEnum.toString());
      }
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }

    _xrayTubeChanging = false;
  }

  /**
   * @author George Booth
   */
  void setupBarriers(XraySafetyTestProcessStep step)
  {
    _barriersChanging = step.isInnerBarrierClosed() != _innerBarrierClosed ||
                       step.areOuterBarriersClosed() != _outerBarriersClosed;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  void setupXrayActuator(XraySafetyTestProcessStep step)
  {
    _magnificationChanging = step.isXrayActuatorDown() != _highMagnification;
  }

  /**
   * @author George Booth
   */
  void setInnerBarrierClosed()
  {
    debug("setInnerBarrierClosed()");
    try
    {
      debug("  _innerBarrier.close()");
      _innerBarrier.close();
      _innerBarrierClosed = true;
      updateInnerBarrierState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author George Booth
   */
  void setInnerBarrierOpen()
  {
    debug("setInnerBarrierOpen()");
    try
    {
      debug("  _innerBarrier.open()");
      _innerBarrier.open();
      _innerBarrierClosed = false;
      updateInnerBarrierState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  void setMagnificationHigh()
  {
    debug("setMagnificationHigh()");
    try
    {
      debug("  _xrayActuator.down(false)");
      _xrayActuator.down(false);
      _highMagnification = true;
      updateMagnificationState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  void setMagnificationLow()
  {
    debug("setMagnificationLow()");
    try
    {
      debug("  _xrayActuator.up(false)");
      _xrayActuator.up(false);
      _highMagnification = false;
      updateMagnificationState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author George Booth
   */
  void setOuterBarriersClosed()
  {
    debug("setOuterBarriersClosed()");
    try
    {
      debug("  _leftOuterBarrier.close()");
      _leftOuterBarrier.close();
      debug("  _rightOuterBarrier.close()");
      _rightOuterBarrier.close();
      _outerBarriersClosed = true;
      updateOuterBarriersState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author George Booth
   */
  void setOuterBarriersOpen()
  {
    debug("setOuterBarriersOpen()");
    try
    {
      debug("  _leftOuterBarrier.open()");
      _leftOuterBarrier.open();
      debug("  _rightOuterBarrier.open()");
      _rightOuterBarrier.open();
      _outerBarriersClosed = false;
      updateOuterBarriersState();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
    }
  }

  // handlers

  /**
   * return to inital step
   * @author George Booth
   */
  void handleCancelButtonEvent()
  {
//    debug("handleCancelButtonEvent()");

    // make sure they want to cancel
    if (JOptionPane.showConfirmDialog(
      this,
      StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_XST_CONFIRM_CANCEL_TASK_KEY", null)),
      StringLocalizer.keyToString("SERVICE_UI_XST_CONFIRM_CANCEL_TASK_TITLE_KEY"),
      JOptionPane.YES_NO_OPTION,
      JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
    {

      createInitialTestSteps();
      _currentStep = 0;

      XraySafetyTestProcessStep step = _processSteps.get(_currentStep);
      displayProcessStep(step, false);

      enableButtons();
      repaint();
    }
  }

  /**
   * @author George Booth
   */
  void handlePreviousScreenButtonEvent()
  {
//    debug("handlePreviousScreenEvent()");
    Assert.expect(_currentStep > 0);
    _currentStep--;

    XraySafetyTestProcessStep step = _processSteps.get(_currentStep);
    displayProcessStep(step, false);

    enableButtons();
    repaint();
  }

  /**
   * @author George Booth
   */
  void handleNextScreenButtonEvent()
  {
//    debug("handleNextScreenEvent()");
    Assert.expect(_currentStep < _processSteps.size());
    _currentStep++;

    XraySafetyTestProcessStep step = _processSteps.get(_currentStep);
    displayProcessStep(step, true);

    enableButtons();
    repaint();
  }

  /**
   * @author George Booth
   */
  void handleProcessCompleteButtonEvent()
  {
//    debug("handleProcessCompleteButtonEvent()");

    createInitialTestSteps();
    _currentStep = 0;

    XraySafetyTestProcessStep step = _processSteps.get(_currentStep);
    displayProcessStep(step, false);
    
    enableButtons();
    repaint();
  }

  /**
   * @author George Booth
   */
  void enableButtons()
  {
    // can't cancel from initial step - navigate away
    _cancelButton.setEnabled(_currentStep != 0);
    // only go back after initial step
    _backButton.setEnabled(_currentStep > 0);
    // next only if more steps
    _nextButton.setEnabled(_currentStep < _processSteps.size() - 1);
    // only finish on last step but not initial or select tests
    _finishButton.setEnabled(_currentStep > 1 && _currentStep == _processSteps.size() - 1);
  }

  /**
   * XCR-3822 System crash when switch to xray safety test tab
   * @author Cheah Lee Herng
   */
  private void disableButtons()
  {
    _cancelButton.setEnabled(false);
    _backButton.setEnabled(false);
    _nextButton.setEnabled(false);
    _finishButton.setEnabled(false);
  }

  void handleTestSelection(ArrayList<XraySafetyTestProcessStep> newSteps)
  {
    // new test was selected, reset shutdown flag if needed
    _systemWasShutdown = false;

    // reset to initial and test selection steps
    createInitialTestSteps();

    // add new steps
    _processSteps.addAll(newSteps);

    // save current test name
    _currentTestName = _instructionPanel.getSelectedTest();
//    debug("  currentTestName = " + _currentTestName);

    // update the navigation buttons
    enableButtons();
  }

  /**
   * @author George Booth
   */
  void handleXrayTesterShutdown()
  {
    debug("handleXrayTesterShutdown()");

    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        //Swee-Yee.Wong - make sure low magnification is set
        setMagnificationLow();
        // make sure barriers are closed
        setInnerBarrierClosed();
        setOuterBarriersClosed();
        // closed state will be unknown until system starts up; indicate worst case
        _innerBarrierClosed = false;
        _outerBarriersClosed = false;
      }
    });

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // now shut it down
        debug("stopping hardware");
        try
        {
          _mainUI.stopXrayTester();
        }
        catch (XrayTesterException xte)
        {
          handleXrayTesterException(xte);
        }
        _hardwareIsInitialized = false;
        _systemWasShutdown = true;
        // don't talk to hardware if shutdown
        stopMonitoring();
        // set hardware status fields to "unknown" (forced if _systemWasShutdown = true)
        updateXrayParameters();
        updateStagePosition();
        updateRailWidth();
        //Swee-Yee.Wong
        updateMagnificationState();
        updateInnerBarrierState();
        updateOuterBarriersState();

        // allow system to be programmed to off state
        createInitialTestSteps();
        _currentStep = 0;

        XraySafetyTestProcessStep step = _processSteps.get(_currentStep);
        displayProcessStep(step, false);

        enableButtons();
        repaint();
      }
    });
  }

  /**
   * @author George Booth
   */
  void handleXrayTesterException(final Throwable exception)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // stop the monitor loop so that exceptions aren't shown coninuously
        stopMonitoring();
        // initialize all hardware on the next set up
        _hardwareIsInitialized = false;
        _setAllHardware = true;

        String messageToDisplay = StringUtil.format(exception.getLocalizedMessage(), 50);
        JOptionPane.showMessageDialog(
            MainMenuGui.getInstance(),
            messageToDisplay,
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.ERROR_MESSAGE);
      }
    });
  }

  /**
   * @author George Booth
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable instanceof GuiObservable);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        GuiEvent guiEvent = (GuiEvent)object;
        GuiEventEnum guiEventEnum = guiEvent.getGuiEventEnum();
        if (guiEventEnum instanceof XraySafetyTestEventEnum)
        {
          XraySafetyTestEventEnum xraySafetyTestEventEnum = (XraySafetyTestEventEnum)guiEventEnum;
          if (xraySafetyTestEventEnum.equals(XraySafetyTestEventEnum.TEST_SELECTED))
          {
//            debug("Event = XraySafetyTestEventEnum.TEST_SELECTED");
            // append the steps sent by TestSelection to the list of process steps
            Assert.expect(object != null);
            // Warning "unchecked cast" approved for cast from Object type
            ArrayList<XraySafetyTestProcessStep> newSteps = (ArrayList<XraySafetyTestProcessStep>)guiEvent.getSource();
            handleTestSelection(newSteps);
          }
          else if (xraySafetyTestEventEnum.equals(XraySafetyTestEventEnum.SHUTDOWN))
          {
//            debug("Event = XraySafetyTestEventEnum.SHUTDOWN");
            // shutdown the tester
            handleXrayTesterShutdown();
          }
          else if (xraySafetyTestEventEnum.equals(XraySafetyTestEventEnum.NORMAL_POWER))
          {
//            debug("Event = XraySafetyTestEventEnum.NORMAL_POWE");
            // set normal power mode
          }
          else if (xraySafetyTestEventEnum.equals(XraySafetyTestEventEnum.HIGH_POWER))
          {
//            debug("Event = XraySafetyTestEventEnum.HIGH_POWER");
            // set high power mode
          }
        }
      }
    });
  }

  /**
   * @author George Booth
   */
  void updateCurrentStatus()
  {
    updateXrayParameters();
    updateStagePosition();
    updateRailWidth();
    // don't update barrier states in monitor loop - only do them after they are changed
    // they might be caught neither open nor closed and will generate an exception
//    updateInnerBarrierState();
//    updateOuterBarriersState();
  }

  /**
   * @author George Booth
   */
  private void updateXrayParameters()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _xraySourceVoltageTextField.setText(_UNKNOWN);
      _xraySourceCurrentTextField.setText(_UNKNOWN);
      return;
    }

    if (_hardwareIsInitialized)
    {
    //      debug("  hardware initialized");
      try
      {
        final double currentAnodeValue = MathUtil.roundToPlaces(_xraySource.getAnodeVoltageInKiloVolts(),
                                                                _NUMBER_OF_DECIMAL_PLACES);
        final double cathodeValue = MathUtil.roundToPlaces(_xraySource.getCathodeCurrentInMicroAmps(),
                                                           _NUMBER_OF_DECIMAL_PLACES);

        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            _xraySourceVoltageTextField.setText(Double.toString(currentAnodeValue));
            _xraySourceCurrentTextField.setText(Double.toString(cathodeValue));
          }
        });
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
      }
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _xraySourceVoltageTextField.setText(_UNKNOWN);
      _xraySourceCurrentTextField.setText(_UNKNOWN);
    }
  }

  /**
   * @author George Booth
   */
  private void updateStagePosition()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _panelPositionerXAxisTextField.setText(_UNKNOWN);
      _panelPositionerYAxisTextField.setText(_UNKNOWN);
      return;
    }

//    debug("updateStageLocation()");
    if (_hardwareIsInitialized)
    {
//      debug("  hardware initialized");
      int xPositionInNanometers = 0;
      int yPositionInNanometers = 0;

      try
      {
        if (_panelPositioner.isStartupRequired() == false)
        {
          xPositionInNanometers = _panelPositioner.getXaxisActualPositionInNanometers();
          yPositionInNanometers = _panelPositioner.getYaxisActualPositionInNanometers();
        }
        else
        {
          _panelPositionerXAxisTextField.setText(_UNKNOWN);
          _panelPositionerYAxisTextField.setText(_UNKNOWN);
          return;
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);

        // XCR1144, Chnee Khang Wah, 15-Feb-2011
        if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);

        _panelPositionerXAxisTextField.setText(_UNKNOWN);
        _panelPositionerYAxisTextField.setText(_UNKNOWN);
        return;
      }

      final int xPositionInNanometersConst = xPositionInNanometers;
      final int yPositionInNanometersConst = yPositionInNanometers;

      double xPosition = MathUtil.roundToPlaces((double)MathUtil.convertUnits(xPositionInNanometersConst, MathUtilEnum.NANOMETERS, _units),
                                                MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      double yPosition = MathUtil.roundToPlaces((double)MathUtil.convertUnits(yPositionInNanometersConst, MathUtilEnum.NANOMETERS, _units),
                                                MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      _panelPositionerXAxisTextField.setText(new Double(xPosition).toString());
      _panelPositionerYAxisTextField.setText(new Double(yPosition).toString());
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _panelPositionerXAxisTextField.setText(_UNKNOWN);
      _panelPositionerYAxisTextField.setText(_UNKNOWN);
    }
  }

  /**
   * @author George Booth
   */
  private void updateRailWidth()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _panelHandlerRailWidthTextField.setText(_UNKNOWN);
      return;
    }

//    debug("updateStageLocation()");
    if (_hardwareIsInitialized)
    {
//      debug("  hardware initialized");
      int widthInNanometers = 0;

      try
      {
        if (_panelHandler.isStartupRequired() == false &&
            _motionControl.isStartupRequired() == false)
        {
          widthInNanometers = _panelHandler.getRailWidthInNanoMeters();
        }
        else
        {
          _panelHandlerRailWidthTextField.setText(_UNKNOWN);
          return;
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);

        // XCR1144, Chnee Khang Wah, 15-Feb-2011
        if(xte instanceof PanelPositionerException)
            _mainUI.handleXrayTesterException(xte);

        _panelHandlerRailWidthTextField.setText(_UNKNOWN);
        return;
      }

      int widthInNanometersConst = widthInNanometers;

      double width = MathUtil.roundToPlaces((double)MathUtil.convertUnits(widthInNanometersConst, MathUtilEnum.NANOMETERS, _units),
                                                MeasurementUnits.getMeasurementUnitDecimalPlaces(_units));
      _panelHandlerRailWidthTextField.setText(new Double(width).toString());
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _panelHandlerRailWidthTextField.setText(_UNKNOWN);
    }
  }

  /**
   * @author George Booth
   */
  void updateInnerBarrierState()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _innerBarrierTextField.setText(_UNKNOWN);
      _innerBarrierTextField.setBackground(Color.YELLOW);
      return;
    }

    if (_hardwareIsInitialized)
    {
      try
      {
        if (_innerBarrier.isOpen())
        {
          _innerBarrierTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_OPEN_VALUE_KEY"));
          _innerBarrierTextField.setBackground(Color.RED);
        }
        else
        {
          _innerBarrierTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
          _innerBarrierTextField.setBackground(Color.GREEN);
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
      }
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _innerBarrierTextField.setText(_UNKNOWN);
      _innerBarrierTextField.setBackground(Color.YELLOW);
    }
  }
  
    /**
   * @author Swee-Yee.Wong
   */
  void updateMagnificationState()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _magnificationTextField.setText(_UNKNOWN);
      _magnificationTextField.setBackground(Color.YELLOW);
      return;
    }

    if (_hardwareIsInitialized)
    {
      try
      {
        if (_xrayActuator.isDown())
        {
          _magnificationTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_HIGH_MAG_VALUE_KEY"));
          _magnificationTextField.setBackground(Color.RED);
        }
        else
        {
          _magnificationTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_LOW_MAG_VALUE_KEY"));
          _magnificationTextField.setBackground(Color.GREEN);
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
      }
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _magnificationTextField.setText(_UNKNOWN);
      _magnificationTextField.setBackground(Color.YELLOW);
    }
  }

  /**
   * @author George Booth
   */
  void updateOuterBarriersState()
  {
    if (_systemWasShutdown)
    {
      debug("  systemWasShutdown");
      _outerBarriersLeftTextField.setText(_UNKNOWN);
      _outerBarriersLeftTextField.setBackground(Color.YELLOW);
      _outerBarriersRightTextField.setText(_UNKNOWN);
      _outerBarriersRightTextField.setBackground(Color.YELLOW);
      return;
    }

    if (_hardwareIsInitialized)
    {
      try
      {
        if (_leftOuterBarrier.isOpen())
        {
          _outerBarriersLeftTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_OPEN_VALUE_KEY"));
          _outerBarriersLeftTextField.setBackground(Color.RED);
        }
        else
        {
          _outerBarriersLeftTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
          _outerBarriersLeftTextField.setBackground(Color.GREEN);
        }
        if (_rightOuterBarrier.isOpen())
        {
          _outerBarriersRightTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_OPEN_VALUE_KEY"));
          _outerBarriersRightTextField.setBackground(Color.RED);
        }
        else
        {
          _outerBarriersRightTextField.setText(StringLocalizer.keyToString("SERVICE_UI_XST_BARRIER_CLOSED_VALUE_KEY"));
          _outerBarriersRightTextField.setBackground(Color.GREEN);
        }
      }
      catch (XrayTesterException xte)
      {
        handleXrayTesterException(xte);
      }
    }
    else
    {
      // the subsystem is not initialized
      debug("  hardware NOT initialized");
      _outerBarriersLeftTextField.setText(_UNKNOWN);
      _outerBarriersLeftTextField.setBackground(Color.YELLOW);
      _outerBarriersRightTextField.setText(_UNKNOWN);
      _outerBarriersRightTextField.setBackground(Color.YELLOW);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startMonitoring()
  {
    if (_monitorEnabled == false)
    {
      debug("*** startMonitoring()");
      _monitorEnabled = true;
      _monitorHardwareStatusThread.invokeLater(new Runnable()
      {
        public void run()
        {
          while (_monitorEnabled)
          {
            updateCurrentStatus();
            try
            {
              Thread.sleep(1000);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    debug("*** stopMonitoring()");
    _monitorEnabled = false;
  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   */
  public void start()
  {
    debug("\nXraySafetyTestTaskPanel.start()");

    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    //Assert.expect(xraySource instanceof LegacyXraySource);
    _xraySource = (HTubeXraySource)xraySource;

    Integer motionControllerId = (Integer)_config.getValue(HardwareConfigEnum.PANEL_HANDLER_MOTION_CONTROLLER_ID);
    MotionControllerIdEnum motionControllerIdEnum = MotionControllerIdEnum.getEnum(motionControllerId);
    _motionControl = MotionControl.getInstance(motionControllerIdEnum);

    // if we are here, there is no currently selected test
    _currentTestName = "";

    // update current status ("unknown" if not initialized)
    updateCurrentStatus();

    // start observing
    _guiObservable.addObserver(this);

    // make sure hardware is set up initially
    _setAllHardware = true;

    // XCR-3822 System crash when switch to xray safety test tab
    if (ableToProceedXraySafetyTest())
    {
      // default first screen
      createInitialTestSteps();
      _currentStep = -1;
      handleNextScreenButtonEvent();
    }
    else
      disableButtons();
  }

  /**
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    boolean readyToFinish = true;
    if (_instructionPanel instanceof XraySafetyTestInstructionShutdownInstructionPanel)
    {
      // x-rays are on, does the user want to shut them off?
      readyToFinish = _instructionPanel.isReadyToFinish();
      if (readyToFinish)
      {
        handleXrayTesterShutdown();
      }
    }
    return readyToFinish;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   */
  public void finish()
  {
    debug("XraySafetyTestTaskPanel.finish()");

    // reset back to original x-ray power setting...
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _xraySource.setupImagingXrayParameters();
        }
        catch (XrayTesterException ex)
        {
          handleXrayTesterException(ex);
        }
      }
    });

    _instructionPanel.finish();

    // stop monitoring status
    stopMonitoring();

    // stop observing
    _guiObservable.deleteObserver(this);
  }

  /**
   * @author George Booth
   */
  void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.XRAY_SAFETY_TEST_DEBUG))
      System.out.println(message);
  }

  /**
   * XCR-3822 System crash when switch to xray safety test tab
   * @author Cheah Lee Herng
   */
  private boolean ableToProceedXraySafetyTest()
  {
    boolean isXrayCylinderInstalled   = Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED);
    boolean isXrayZAxisMoterInstalled = Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED);
    
    if (XrayTester.isS2EXEnabled())
    {
      if ((isXrayCylinderInstalled == true && isXrayZAxisMoterInstalled == true) ||
          (isXrayCylinderInstalled == false && isXrayZAxisMoterInstalled == false))
      {
        return false;
      }
      else
        return true;
    }
    else
    {
      // Standard system
      if (isXrayZAxisMoterInstalled == true)
        return false;
      else
        return true;
    }
  }
}
