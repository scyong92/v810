package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;


/**
 * This panel allows the user to define and run a point - to - point configuration.
 *
 * @author Erica Wheatcroft
 */
class PointToPointMovesPanel extends JPanel implements TaskPanelInt, Observer
{
  private static ImageIcon _stopIcon = Image5DX.getImageIcon(Image5DX.AT_STOP);
  private static ImageIcon _runAllIcon = Image5DX.getImageIcon(Image5DX.AT_START);
  private static ImageIcon _runOneIcon = Image5DX.getImageIcon(Image5DX.AT_RUN_TO_JOINT);

  private static final int _X_ONLY_MOVE = 0;
  private static final int _Y_ONLY_MOVE = 1;
  private static final int _XY_MOVE = 2;

  private JRadioButton _xOnlyRadioButton = new JRadioButton();
  private JRadioButton _yOnlyRadioButton = new JRadioButton();
  private JRadioButton _xyRadioButton = new JRadioButton();
  private JButton _abortButton = new JButton();
  private JButton _runOnePointButton = new JButton();
  private JButton _runAllButton = new JButton();
  private JButton _removeAllButton = new JButton();
  private JButton _removeButton = new JButton();
  private JButton _addPointButton = new JButton();
  private JLabel _numberOfCyclesLabel = new JLabel();
  private JLabel _miliSecondsLabel = new JLabel();
  private JLabel _delayBetweenPointsLabel = new JLabel();
  private NumericTextField _numberOfCyclesTextField = new NumericTextField();
  private NumericRangePlainDocument _cylcesNumericRangePlainDocument = new NumericRangePlainDocument();
  private NumericTextField _delayBetweenPointsTextField = new NumericTextField();
  private NumericRangePlainDocument _delayPlainDocument = new NumericRangePlainDocument();
  private JCheckBox _loopContinuoslyCheckBox = new JCheckBox();
  private JSlider _motionProfileSelectionSlider = null;
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTable _pointToPointMovesTable = new JTable();
  private PointToPointMoveDefinitionTableModel _tableModel = new PointToPointMoveDefinitionTableModel();
  private ListSelectionModel _listSelectionModel = _pointToPointMovesTable.getSelectionModel();

  private PanelPositioner _panelPositioner = null;
  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();

  private PanelPositionerPanel _parent = null;

  private NumericEditor _xNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private NumericEditor _yNumericEditor = new NumericEditor(getBackground(), getForeground(), JTextField.CENTER, 2);
  private MathUtilEnum _currentUnits = null;

  private boolean _loopContinuosly = false;
  private boolean _motionStarted = false;
  private int _numberOfLoops = 1;
  private int _delayBetweenPoints = 0;
  private boolean _runOnePointAtATime = false;
  private boolean _pointMoveCancelled = false;
  private int _moveType = _XY_MOVE;

  private int _indexOfRunningPoint = 0;

  // i create all listeners here and then add them and remove them as needed.
  public ActionListener _xOnlyMoveRadioButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _moveType = _X_ONLY_MOVE;
    }
  };
  public ActionListener _yOnlyMoveRadioButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _moveType = _Y_ONLY_MOVE;
    }
  };
  public ActionListener _xyMoveRadioButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _moveType = _XY_MOVE;
    }
  };

  public ActionListener _removePointButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      int selectedRow = _pointToPointMovesTable.getSelectedRow();
      Assert.expect(selectedRow >= 0);
      _tableModel.clearSelectedRows(selectedRow);
      if(_pointToPointMovesTable.getRowCount() > 0)
      {
        _pointToPointMovesTable.setRowSelectionInterval(_pointToPointMovesTable.getRowCount() -1,_pointToPointMovesTable.getRowCount() -1);
      }
      else
      {
        _removeAllButton.setEnabled(false);
        _removeButton.setEnabled(false);
        _runAllButton.setEnabled(false);
        _runOnePointButton.setEnabled(false);
        _parent.enableLoadConfigurationButton();
        _parent.enableSaveConfigurationButton(false);
      }      
    }
  };

  public ActionListener _removeAllButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _tableModel.clearAllRows();
      _removeAllButton.setEnabled(false);
      _removeButton.setEnabled(false);
      _runAllButton.setEnabled(false);
      _runOnePointButton.setEnabled(false);
      _parent.enableLoadConfigurationButton();
      _parent.enableSaveConfigurationButton(false);
    }
  };
  public ActionListener _runAllPointsActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // first check to make sure the user has not disabled the x or y axis.
      if (_parent.areAnyAxesDisabled() == false)
      {
        _runOnePointAtATime = false;
        runAllPoints();
      }
      else
      {
        // they have an axis disabled, notify the user to enable it and return out of the function
        String message = "Please enable all axes and try again.";
        JOptionPane.showMessageDialog(_parent, StringUtil.format(message, 50), "Cannot run configuration", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
  };

  public ActionListener _addPointActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _tableModel.insertRow();
      _pointToPointMovesTable.setEnabled(true);
      _pointToPointMovesTable.setRowSelectionInterval(_tableModel.getRowCount() -1, _tableModel.getRowCount() -1);
      if (_tableModel.getRowCount() == 1)
      {
        _parent.enableSaveConfigurationButton(true);
        _removeAllButton.setEnabled(true);
        _removeButton.setEnabled(true);
        _runAllButton.setEnabled(true);
        _runOnePointButton.setEnabled(true);
      }
    }
  };

  private FocusListener _numberOfCyclesTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleNumberOfCylcesEntry();
    }
  };

  private ActionListener _numberOfCyclesTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleNumberOfCylcesEntry();
    }
  };

  private FocusListener _delayBetweenPointsTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      // do nothing
    }

    public void focusLost(FocusEvent evt)
    {
      handleDelayEntry();
    }
  };

  private ActionListener _delayBetweenPointsTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleDelayEntry();
    }
  };


  private ActionListener _loopContinuoslyCheckBoxActionlistener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
      if (_loopContinuosly)
        _numberOfCyclesTextField.setEnabled(false);
      else
        _numberOfCyclesTextField.setEnabled(true);
    }
  };
  private ActionListener _abortButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _pointMoveCancelled = true;

      try
      {
        _panelPositioner.abort();
        disableControls(false, false);
      }
      catch (XrayTesterException xte)
      {
        _parent.handleXrayTesterException(xte);
      }
    }
  };

  private MouseAdapter _pointToPointDefinitionTableMouseListener = new MouseAdapter()
  {
    public void mouseClicked(MouseEvent evt)
    {}

    public void mouseReleased(MouseEvent e)
    {
      int selectedRowFromClick = _pointToPointMovesTable.rowAtPoint(e.getPoint());
      if (selectedRowFromClick > -1)
      {
        int selectedRow = _pointToPointMovesTable.getSelectedRow();

        if(selectedRow >= 0)
        {
          _pointToPointMovesTable.setRowSelectionInterval(selectedRow,selectedRow);
          _removeButton.setEnabled(true);
        }

        if (selectedRow != selectedRowFromClick)
        {
          // first we need to stop the cell editing
          TableCellEditor cellEditor = _pointToPointMovesTable.getColumnModel().getColumn(0).getCellEditor();
          if (cellEditor != null)
            cellEditor.cancelCellEditing();
          // clear the selection
          _pointToPointMovesTable.clearSelection();
          // then select the row
          _pointToPointMovesTable.setRowSelectionInterval(selectedRowFromClick, selectedRowFromClick);
        }
      }
    }
  };

  private TableModelListener _tableModelListener = new TableModelListener()
  {
    public void tableChanged(TableModelEvent e)
    {
      if(e.getType() == TableModelEvent.UPDATE)
      {
        if(_tableModel.getRowCount() > 0)
          _pointToPointMovesTable.setRowSelectionInterval(0,0);
      }
    }
  };

  public ActionListener _runOnePointActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      if(_motionStarted == false)
      {
        // first check to make sure the user has not disabled the x or y axis.
        if (_parent.areAnyAxesDisabled())
        {
          // they have an axis disabled, notify the user to enable it and return out of the function
          String message = "Please enable all axes and try again.";
          JOptionPane.showMessageDialog(_parent, StringUtil.format(message, 50), "Cannot run configuration", JOptionPane.ERROR_MESSAGE);
          return;
        }
        _motionStarted = false;
      }
      _runOnePointAtATime = true;
      runOnePoint();
    }
  };

  public ChangeListener _sliderChangeListener = new ChangeListener()
  {
    public void stateChanged(ChangeEvent evt)
    {
      if (_motionProfileSelectionSlider.getValueIsAdjusting() == false)
      {
        int value = _motionProfileSelectionSlider.getValue();
        setMotionProfile(value);
      }
    }
  };

  private ListSelectionListener _tableListSelectionListener = new ListSelectionListener()
  {
    public void valueChanged(ListSelectionEvent e)
    {
      ListSelectionModel lsm = (ListSelectionModel)e.getSource();

      if (lsm.isSelectionEmpty() || lsm.getValueIsAdjusting())
      {
        // DO NOTHING
      }
      else
      {
        // now lets set which entry the user is selecting.
        int selectedRowIndex = _pointToPointMovesTable.getSelectedRow();
        StagePositionMutable tableEntry = _tableModel.getTableEntryAt(selectedRowIndex);
        _tableModel.setSelectedRowEntry(tableEntry);
      }

    }
  };

  /**
   * @author Erica Wheatcroft
   */
  PointToPointMovesPanel(PanelPositionerPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    // we need to listen to when the user changes display units
    GuiObservable.getInstance().addObserver(this);

    createPanel();

  }

  /**
   * The task panel is now on top, ready to run and should perform sync
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get the instance of the panel positioner
    _panelPositioner = PanelPositioner.getInstance();

    addListners();

    restoreLastUIState();

  }


  /**
   * @author Erica Wheatcroft
   */
  private void handleNumberOfCylcesEntry()
  {
    try
    {
      int intValue = _numberOfCyclesTextField.getNumberValue().intValue();
      _numberOfLoops = intValue;
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _numberOfCyclesTextField.setValue(_numberOfLoops);
    }

    _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleDelayEntry()
  {
    try
    {
      int intValue = _delayBetweenPointsTextField.getNumberValue().intValue();
      _delayBetweenPoints = intValue;
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _delayBetweenPointsTextField.setValue(_delayBetweenPoints);
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private void setMotionProfile(final int value)
  {
    if(_panelPositioner == null)
    {
      // get the instance of the panel positioner
      _panelPositioner = PanelPositioner.getInstance();
    }

    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if(_panelPositioner.isStartupRequired() == false)
          {
            switch (value)
            {
              case 1:
                _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE0));
                break;
              case 2:
                _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
                break;
              case 3:
                _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE2));
                break;
              case 4:
                _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE3));
                break;
              default:
                Assert.expect(false);
            }
          }
        }
        catch (XrayTesterException xte)
        {
          _parent.handleXrayTesterException(xte);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListners()
  {
    _addPointButton.addActionListener(_addPointActionListener);
    _loopContinuoslyCheckBox.addActionListener(_loopContinuoslyCheckBoxActionlistener);
    _delayBetweenPointsTextField.addFocusListener(_delayBetweenPointsTextFieldListener);
    _delayBetweenPointsTextField.addActionListener(_delayBetweenPointsTextFieldActionListener);
    _numberOfCyclesTextField.addFocusListener(_numberOfCyclesTextFieldListener);
    _numberOfCyclesTextField.addActionListener(_numberOfCyclesTextFieldActionListener);
    _runAllButton.addActionListener(_runAllPointsActionListener);
    _runOnePointButton.addActionListener(_runOnePointActionListener);
    _removeAllButton.addActionListener(_removeAllButtonActionListener);
    _pointToPointMovesTable.addMouseListener(_pointToPointDefinitionTableMouseListener);
    _tableModel.addTableModelListener(_tableModelListener);
    _abortButton.addActionListener(_abortButtonActionListener);
    _motionProfileSelectionSlider.addChangeListener(_sliderChangeListener);
    _xOnlyRadioButton.addActionListener(_xOnlyMoveRadioButtonActionListener);
    _yOnlyRadioButton.addActionListener(_yOnlyMoveRadioButtonActionListener);
    _xyRadioButton.addActionListener(_xyMoveRadioButtonActionListener);
    _removeButton.addActionListener(_removePointButtonActionListener);
    _listSelectionModel.addListSelectionListener(_tableListSelectionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _tableModel.removeTableModelListener(_tableModelListener);
    _addPointButton.removeActionListener(_addPointActionListener);
    _loopContinuoslyCheckBox.removeActionListener(_loopContinuoslyCheckBoxActionlistener);
    _delayBetweenPointsTextField.removeFocusListener(_delayBetweenPointsTextFieldListener);
    _delayBetweenPointsTextField.removeActionListener(_delayBetweenPointsTextFieldActionListener);
    _numberOfCyclesTextField.removeFocusListener(_numberOfCyclesTextFieldListener);
    _numberOfCyclesTextField.removeActionListener(_numberOfCyclesTextFieldActionListener);
    _runAllButton.removeActionListener(_runAllPointsActionListener);
    _runOnePointButton.removeActionListener(_runOnePointActionListener);
    _removeAllButton.removeActionListener(_removeAllButtonActionListener);
    _pointToPointMovesTable.removeMouseListener(_pointToPointDefinitionTableMouseListener);
    _abortButton.removeActionListener(_abortButtonActionListener);
    _motionProfileSelectionSlider.removeChangeListener(_sliderChangeListener);
    _xOnlyRadioButton.removeActionListener(_xOnlyMoveRadioButtonActionListener);
    _yOnlyRadioButton.removeActionListener(_yOnlyMoveRadioButtonActionListener);
    _xyRadioButton.removeActionListener(_xyMoveRadioButtonActionListener);
    _removeButton.removeActionListener(_removePointButtonActionListener);
    _listSelectionModel.removeListSelectionListener(_tableListSelectionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void restoreLastUIState()
  {
    // do nothing
  }


  /**
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * The task panel is about to be exited and should perform any cleanup needed
   * @author George Booth
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    removeListeners();

    saveUIState();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createControlPanel()
  {
    // lets create the button panel
    JPanel controlPanel = new JPanel(new FlowLayout());
    JPanel innerControlPanel = new JPanel(new GridLayout(1, 3));
    JPanel controlButtonPanel = new JPanel(new FlowLayout());
    JPanel innerControlButtonPanel = new JPanel(new GridLayout(1, 1, 25, 0));
    _abortButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _abortButton.setIcon(_stopIcon);
    _abortButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_ABORT_BUTTON_KEY"));
    _abortButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _abortButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _runOnePointButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _runOnePointButton.setIcon(_runOneIcon);
    _runOnePointButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_RUN_ONE_BUTTON_KEY"));
    _runOnePointButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _runOnePointButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    _runAllButton.setHorizontalTextPosition(SwingConstants.CENTER);
    _runAllButton.setIcon(_runAllIcon);
    _runAllButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_RUN_ALL_BUTTON_KEY"));
    _runAllButton.setVerticalAlignment(SwingConstants.BOTTOM);
    _runAllButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    innerControlButtonPanel.add(_abortButton);
    innerControlButtonPanel.add(_runAllButton);
    innerControlButtonPanel.add(_runOnePointButton);
    controlButtonPanel.add(innerControlButtonPanel);

    // now lets create the panel that will describe the iteration and delay between points
    JPanel numberOfCyclesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    _numberOfCyclesLabel.setText(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY"));
    _numberOfCyclesTextField.setText("1");
    _numberOfCyclesTextField.setColumns(5);
    DecimalFormat numberOfCyclesDecimalformat = new DecimalFormat();
    numberOfCyclesDecimalformat.setParseIntegerOnly(true);
    _numberOfCyclesTextField.setDocument(_cylcesNumericRangePlainDocument);
    _numberOfCyclesTextField.setFormat(numberOfCyclesDecimalformat);
    _cylcesNumericRangePlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    _numberOfCyclesTextField.setText(String.valueOf(_numberOfLoops));
    numberOfCyclesPanel.add(_numberOfCyclesLabel);
    numberOfCyclesPanel.add(_numberOfCyclesTextField);
    innerControlPanel.add(numberOfCyclesPanel);

    JPanel loopCheckBoxPanel = new JPanel(new FlowLayout());
    _loopContinuoslyCheckBox.setText(StringLocalizer.keyToString("GUI_LOOP_CONTINUOUSLY_LABEL_KEY"));
    loopCheckBoxPanel.add(_loopContinuoslyCheckBox);
    innerControlPanel.add(loopCheckBoxPanel);

    JPanel delayBetweenPointsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _miliSecondsLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_MILISECONDS_KEY"));
    _delayBetweenPointsTextField.setColumns(5);
    _delayBetweenPointsLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_DELAY_KEY"));
    DecimalFormat format = new DecimalFormat();
    format.setParseIntegerOnly(true);
    _delayBetweenPointsTextField.setDocument(_delayPlainDocument);
    _delayBetweenPointsTextField.setFormat(format);
    _delayPlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    delayBetweenPointsPanel.add(_delayBetweenPointsLabel);
    delayBetweenPointsPanel.add(_delayBetweenPointsTextField);
    delayBetweenPointsPanel.add(_miliSecondsLabel);
    innerControlPanel.add(delayBetweenPointsPanel);
    controlPanel.add(innerControlPanel);

    JPanel executePointToPointMoveDefinitionsPanel = new JPanel(new BorderLayout(0, 20));
    executePointToPointMoveDefinitionsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    executePointToPointMoveDefinitionsPanel.add(controlPanel, BorderLayout.NORTH);
    executePointToPointMoveDefinitionsPanel.add(controlButtonPanel, BorderLayout.CENTER);

    add(executePointToPointMoveDefinitionsPanel, BorderLayout.SOUTH);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPointToPointMoveDefinitionPanel()
  {
    JPanel pointToPointMoveDefinitionPanel = new JPanel(new BorderLayout(0, 5));
    pointToPointMoveDefinitionPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    // lets create the north panel - the slider panel
    JPanel sliderAndRadioButtonPanel = new JPanel(new BorderLayout(5, 10));
    JPanel sliderPanel = new JPanel(new GridLayout());
    int numberOfProfiles = PointToPointMotionProfileEnum.getNumberOfMotionProfiles();
    int defaultMotionProfile = (int)numberOfProfiles / 2;
    _motionProfileSelectionSlider = new JSlider(JSlider.HORIZONTAL, 1, numberOfProfiles, defaultMotionProfile);
    _motionProfileSelectionSlider.setMajorTickSpacing(1);
    _motionProfileSelectionSlider.setPaintTicks(true);
    java.util.Hashtable table = new java.util.Hashtable();
    JLabel slowestLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_SLOWEST_KEY"));
    JLabel fastestLabel = new JLabel(StringLocalizer.keyToString("SERVICE_UI_FASTEST_KEY"));
    table.put(new Integer(_motionProfileSelectionSlider.getMinimum()), slowestLabel);
    table.put(new Integer(_motionProfileSelectionSlider.getMaximum()), fastestLabel);
    // Force the slider to use the new labels
    _motionProfileSelectionSlider.setLabelTable(table);
    _motionProfileSelectionSlider.setPaintLabels(true);
    sliderPanel.add(_motionProfileSelectionSlider);
    sliderPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.
        white, new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_MOTION_PROFILE_SELECTION_PANEL_LABEL_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    sliderAndRadioButtonPanel.add(sliderPanel, BorderLayout.NORTH);

    // now lets create the radio button panel
    JPanel innerRadioButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 40, 0));
    _xOnlyRadioButton.setText(StringLocalizer.keyToString("SERVICE_UI_X_ONLY_MOVE_KEY"));
    _xOnlyRadioButton.setFont(FontUtil.getBoldFont(_xOnlyRadioButton.getFont(),Localization.getLocale()));
    _yOnlyRadioButton.setText(StringLocalizer.keyToString("SERVICE_UI_Y_ONLY_MOVE_KEY"));
    _yOnlyRadioButton.setFont(FontUtil.getBoldFont(_yOnlyRadioButton.getFont(),Localization.getLocale()));
    _xyRadioButton.setText(StringLocalizer.keyToString("SERVICE_UI_X_AND_Y_ONLY_MOVE_KEY"));
    _xyRadioButton.setFont(FontUtil.getBoldFont(_xyRadioButton.getFont(),Localization.getLocale()));
    _xyRadioButton.setSelected(true);
    innerRadioButtonPanel.add(_xOnlyRadioButton);
    innerRadioButtonPanel.add(_yOnlyRadioButton);
    innerRadioButtonPanel.add(_xyRadioButton);
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_xOnlyRadioButton);
    buttonGroup.add(_yOnlyRadioButton);
    buttonGroup.add(_xyRadioButton);
    sliderAndRadioButtonPanel.add(innerRadioButtonPanel, BorderLayout.CENTER);

    // lets create the center panel

    // first part - create the NORTH portion of the center panel - adding and clearing points
    JPanel addAndClearPointsInnerButtonPanel = new JPanel(new GridLayout(1, 3, 15, 0));
    _removeAllButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_REMOVE_ALL_BUTTON_KEY"));
    _addPointButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_ADD_POINT_KEY"));
    _removeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_REMOVE_POINT_KEY"));
    addAndClearPointsInnerButtonPanel.add(_addPointButton);
    addAndClearPointsInnerButtonPanel.add(_removeAllButton);
    addAndClearPointsInnerButtonPanel.add(_removeButton);
    _removeButton.setEnabled(false);
    JPanel addAndClearPointsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    addAndClearPointsPanel.add(addAndClearPointsInnerButtonPanel);

    // now lets create the second part of the center panel - table
    JPanel centerPanel = new JPanel(new BorderLayout(0, 10));
    _tableModel = new PointToPointMoveDefinitionTableModel();
    _pointToPointMovesTable.setModel(_tableModel);
    _pointToPointMovesTable.getTableHeader().setReorderingAllowed(false);
    _pointToPointMovesTable.setDragEnabled(false);
    _pointToPointMovesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _pointToPointMovesTable.setRowSelectionAllowed(true);
    _pointToPointMovesTable.setColumnSelectionAllowed(false);
    _pointToPointMovesTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    jScrollPane1.getViewport().add(_pointToPointMovesTable);
    centerPanel.add(jScrollPane1, BorderLayout.CENTER);
    centerPanel.add(addAndClearPointsPanel, BorderLayout.NORTH);

    // add the north and center panels
    pointToPointMoveDefinitionPanel.add(sliderAndRadioButtonPanel, BorderLayout.NORTH);
    pointToPointMoveDefinitionPanel.add(centerPanel, BorderLayout.CENTER);

    setUpTable();

    // add this panel to the main panel
    add(pointToPointMoveDefinitionPanel, BorderLayout.CENTER);

  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    createPointToPointMoveDefinitionPanel();
    createControlPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  void enablePanel(boolean subsystemInitialized)
  {
    _addPointButton.setEnabled(subsystemInitialized);
    _removeAllButton.setEnabled(subsystemInitialized);
    _numberOfCyclesTextField.setEnabled(subsystemInitialized);
    _loopContinuoslyCheckBox.setEnabled(subsystemInitialized);
    _delayBetweenPointsTextField.setEnabled(subsystemInitialized);
    _xOnlyRadioButton.setEnabled(subsystemInitialized);
    _yOnlyRadioButton.setEnabled(subsystemInitialized);
    _xyRadioButton.setEnabled(subsystemInitialized);
    _motionProfileSelectionSlider.setEnabled(subsystemInitialized);
    _removeButton.setEnabled(subsystemInitialized);

    if (subsystemInitialized == false)
    {
      _abortButton.setEnabled(subsystemInitialized);
      _runAllButton.setEnabled(subsystemInitialized);
      _runOnePointButton.setEnabled(subsystemInitialized);
      _pointToPointMovesTable.setEnabled(false);
    }
    else
    {
      _abortButton.setEnabled(false);
      _pointToPointMovesTable.setEnabled(true);

      if (_tableModel.getRowCount() == 0)
      {
        _runAllButton.setEnabled(false);
        _runOnePointButton.setEnabled(false);
        _removeButton.setEnabled(false);
        _removeAllButton.setEnabled(false);
        _pointToPointMovesTable.setEnabled(false);
      }
      else
      {
        _removeAllButton.setEnabled(true);
        _runOnePointButton.setEnabled(true);
        _runAllButton.setEnabled(true);
        _pointToPointMovesTable.setRowSelectionInterval(0,0);
        _pointToPointMovesTable.setEnabled(true);
        _removeButton.setEnabled(true);
      }

      if (_loopContinuosly)
        _numberOfCyclesTextField.setEnabled(false);
      else
        _numberOfCyclesTextField.setEnabled(true);

      restoreLastUIState();

    }
  }

  /**
   * @author Erica Wheatcroft
   */
  void disableControls(final boolean testRunning, final boolean userIsControllingScanPass)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        // the panel positioner needs to be notified as well
        _parent.disableControls(testRunning);

        _runAllButton.setEnabled(!testRunning);
        _addPointButton.setEnabled(!testRunning);
        _removeAllButton.setEnabled(!testRunning);
        _removeButton.setEnabled(!testRunning);
        _motionProfileSelectionSlider.setEnabled(!testRunning);
        _numberOfCyclesTextField.setEnabled(!testRunning);
        _loopContinuoslyCheckBox.setEnabled(!testRunning);
        _delayBetweenPointsTextField.setEnabled(!testRunning);
        _pointToPointMovesTable.setEnabled(!testRunning);
        _xOnlyRadioButton.setEnabled(!testRunning);
        _yOnlyRadioButton.setEnabled(!testRunning);
        _xyRadioButton.setEnabled(!testRunning);

        if (testRunning)
        {
          // we need to disable everything and only turn on the appropriate buttons.
          _abortButton.setEnabled(true); // abort is set to true

          if (userIsControllingScanPass)
          {
            // the user is controlling when the next scan pass is starting so this needs to be enabled
            _runOnePointButton.setEnabled(true);
          }
          else
            _runOnePointButton.setEnabled(false);
        }
        else
        {
          _abortButton.setEnabled(false);
          _runOnePointButton.setEnabled(true);
          if (_loopContinuosly)
            _numberOfCyclesTextField.setEnabled(false);
          else
            _numberOfCyclesTextField.setEnabled(true);
        }
      }
    });
  }

  /**
   * Write out a configuration file that will represent the point to point moves the user has defined in the table
   * @author Erica Wheatcroft
   */
  void saveConfigurationFile(String fileName)
  {
    Assert.expect(fileName != null);

    MathUtilEnum currentUnits = _tableModel.getCurrentUnits();
    // now tell that table to change the units to nanometers since we will write out all values in nanometers
    _tableModel.changeUnits(MathUtilEnum.NANOMETERS);

    try
    {
      // now write out the visible file.
      JTableToTextFileExporter exporter = new JTableToTextFileExporter();
      exporter.writeFile(_pointToPointMovesTable, fileName, "\t", true);

      String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_SSACTP_PANEL_POSITIONER_P2P_CONFIG_LOCATION_KEY",
                                                                                     new Object[]{fileName}));

      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message,50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

      _tableModel.changeUnits(currentUnits);
      _tableModel.fireTableDataChanged();
      _parent.enableLoadConfigurationButton();
    }
    catch (FileNotFoundException fnfe)
    {
      _tableModel.changeUnits(currentUnits);
      String message = fnfe.getMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message, 50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }

  }

  /**
   * @author Erica Wheatcroft
   */
  boolean isTableCurrentlyPopulated()
  {
    if (_pointToPointMovesTable.getRowCount() >= 1)
      return true;

    return false;
  }

  /**
   * @author Erica Wheatcroft
   */
  void loadConfigurationFile(String fileName)
  {
    Assert.expect(fileName != null);

    // clear the table model
    _tableModel.clearAllRows();

    // read the file and populated the table
    PointToPointDefinitionTableReader reader = new PointToPointDefinitionTableReader();
    try
    {
      reader.read(_pointToPointMovesTable, fileName);

      _tableModel.fireTableDataChanged();

      // // notify the user load was successfull
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_P2P_CONFIG_FILE_LOADED_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message,50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

      // now enable the save config button
      _parent.enableSaveConfigurationButton(true);

      disableControls(false, false);

    }
    catch (FileCorruptDatastoreException fcde)
    {
      // the file was corrupt so give the user an error message and then clear out the table model.
      _tableModel.clearAllRows();
      _tableModel.fireTableDataChanged();
      String message = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_POSITIONER_SCAN_PATH_CONFIG_FILE_CORRUPT_KEY");
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                    StringUtil.format(message,50),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.ERROR_MESSAGE);
    }
    catch (DatastoreException de)
    {
      _tableModel.clearAllRows();
      _tableModel.fireTableDataChanged();
      _parent.handleXrayTesterException(de);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setUpTable()
  {
    // we want to center all the data in the table
    int numberOfColumns = _pointToPointMovesTable.getColumnModel().getColumnCount();
    for (int i = 0; i < numberOfColumns; i++)
    {
      TableColumn col = _pointToPointMovesTable.getColumnModel().getColumn(i);
      DefaultTableCellRenderer columnCenterRenderer = new DefaultTableCellRenderer();
      columnCenterRenderer.setHorizontalAlignment(JLabel.CENTER);
      col.setCellRenderer(columnCenterRenderer);
    }

    // bold the table headers
    _pointToPointMovesTable.getTableHeader().setFont(FontUtil.getBoldFont(_pointToPointMovesTable.getTableHeader().
        getFont(),Localization.getLocale()));

    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_pointToPointMovesTable, true);

    _pointToPointMovesTable.getTableHeader().setReorderingAllowed(false);

    NumericRenderer numericRenderer = new NumericRenderer(2, JLabel.CENTER);

    TableColumn xStartColumn = _pointToPointMovesTable.getColumnModel().getColumn(1);
    xStartColumn.setCellEditor(_xNumericEditor);
    xStartColumn.setCellRenderer(numericRenderer);

    TableColumn yStartColumn = _pointToPointMovesTable.getColumnModel().getColumn(2);
    yStartColumn.setCellEditor(_yNumericEditor);
    yStartColumn.setCellRenderer(numericRenderer);

    updateDecimalPlaces();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void runAllPoints()
  {
    Assert.expect(_panelPositioner != null);
    Assert.expect(_runOnePointAtATime == false);

    _pointMoveCancelled = false;

    disableControls(true, false);

    // now set the motion profile
    int value = _motionProfileSelectionSlider.getValue();
    if (value > 0)
      setMotionProfile(value);

    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        // get the list of scan passes from the table model.
        java.util.List<StagePositionMutable> points = _tableModel.getPoints();
        try
        {

        // if we are looping for a finate amount of cycles....
        if (_loopContinuosly == false && _numberOfLoops > 1)
        {
          for (int loopCount = 0; loopCount < _numberOfLoops && _pointMoveCancelled == false; loopCount++)
          {
            for (StagePositionMutable point : points)
            {
              move(point);
              try
              {
                Thread.sleep(_delayBetweenPoints);
              }
              catch (InterruptedException ie)
              {
                Assert.expect(false);
              }
            }
          }
        }
        else if (_loopContinuosly)
        {
          // if we are looping until the user tells us to stop..
          while (_pointMoveCancelled == false)
          {
            for (StagePositionMutable point : points)
            {
              move(point);
              try
              {
                Thread.sleep(_delayBetweenPoints);
              }
              catch (InterruptedException ie)
              {
                Assert.expect(false);
              }
            }
          }
        }
        else // just run the scan path once.
        {
          for (StagePositionMutable point : points)
          {
            move(point);
            try
            {
              Thread.sleep(_delayBetweenPoints);
            }
            catch (InterruptedException ie)
            {
              Assert.expect(false);
            }
          }
        }

        //make the dialog go away
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            disableControls(false, false);
          }
        });
        }
        catch (XrayTesterException xte)
        {
          _parent.handleXrayTesterException(xte);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void move(StagePositionMutable point) throws XrayTesterException
  {
    Assert.expect(point != null);

    if(_moveType == _X_ONLY_MOVE)
    {
      _panelPositioner.pointToPointMoveXaxis(point);
    }
    else if(_moveType == _Y_ONLY_MOVE)
    {
      _panelPositioner.pointToPointMoveYaxis(point);
    }
    else if(_moveType == _XY_MOVE)
    {
      _panelPositioner.pointToPointMoveAllAxes(point);
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void runOnePoint()
  {
    Assert.expect(_panelPositioner != null);

    _pointMoveCancelled = false;
    disableControls(true, true);

    // lets select the row of the scan pass we are running
    _pointToPointMovesTable.setRowSelectionInterval(_indexOfRunningPoint, _indexOfRunningPoint);

    // now set the motion profile
    int value = _motionProfileSelectionSlider.getValue();
    if (value > 0)
      setMotionProfile(value);

    if (_pointMoveCancelled != true)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          // get the list of scan passes from the table model.
          java.util.List<StagePositionMutable> points = _tableModel.getPoints();

          try
          {
            move(points.get(_indexOfRunningPoint));

            if (_indexOfRunningPoint == points.size() - 1)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  // turn on the controls
                  disableControls(false, false);
                }
              });
              _motionStarted = false;
              _indexOfRunningPoint = 0;
            }
            else
            {
              // increment our index.
              _indexOfRunningPoint++;
            }
          }
          catch (XrayTesterException xte)
          {
            _motionStarted = false;
            _parent.handleXrayTesterException(xte);
          }
        }
        });
    }
  }

  /**
   * @author Erica WHeatcroft
   */
  public synchronized void update(Observable observable, Object arg)
  {
    Assert.expect(observable != null, " observable is null");

    // this should be an update from something already on the swing thread. if not then assert.
    Assert.expect(SwingUtilities.isEventDispatchThread(), "update not from a swing thread");

    if (observable instanceof GuiObservable)
    {
      if (arg instanceof GuiEvent)
      {
        GuiEvent event = (GuiEvent)arg;
        GuiEventEnum eventEnum = event.getGuiEventEnum();
        if (eventEnum instanceof SelectionEventEnum)
        {
          SelectionEventEnum selectionEventEnum = (SelectionEventEnum)eventEnum;
          if (selectionEventEnum.equals(SelectionEventEnum.DISPLAY_UNITS))
          {
            Assert.expect(event.getSource() instanceof MathUtilEnum);
            _currentUnits = (MathUtilEnum)event.getSource();
            updateDecimalPlaces();
            if(_tableModel.getRowCount() > 0 )
              _pointToPointMovesTable.setRowSelectionInterval(0,0);
          }
        }
      }
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateDecimalPlaces()
  {
    if (_currentUnits != null)
    {
      int numberOfDecimalPlaces = MeasurementUnits.getMeasurementUnitDecimalPlaces(_currentUnits);
      _xNumericEditor.setDecimalPlaces(numberOfDecimalPlaces);
      _yNumericEditor.setDecimalPlaces(numberOfDecimalPlaces);
    }
}

}
