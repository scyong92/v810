package com.axi.v810.gui.service;

import javax.swing.*;

import com.axi.util.*;

/**
 *
 * This is a simple abstract class to allow different X-ray Source Panels to be used in the Service UI.
 *
 * @author George Booth
 */
public class AbstractXraySourcePanel extends JPanel
{
  /**
   * @author George Booth
   */
  AbstractXraySourcePanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
  }
}
