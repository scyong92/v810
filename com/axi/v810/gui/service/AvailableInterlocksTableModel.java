package com.axi.v810.gui.service;

import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.guiUtil.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
class AvailableInterlocksTableModel extends DefaultSortTableModel
{
  private final String[] _interLocks = {StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_TABLE_STATUS_COLUMN_KEY"),
                                        StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_TABLE_NAME_COLUMN_KEY")};

  private List<InterlockTableEntry> _entries = null;

  private Interlock _interlock = Interlock.getInstance();

  private boolean _serviceModeEnabled = false;

  private final static ImageIcon _closedIcon = Image5DX.getImageIcon(Image5DX.CD_PASSED_LEAF_TEST);
  private final static ImageIcon _openIcon = Image5DX.getImageIcon(Image5DX.CD_FAILED_LEAF_TEST);
  private final static ImageIcon _unknownStateIcon = Image5DX.getImageIcon(Image5DX.UNKNOWN_STATE);

 /**
  * @author Erica Wheatcroft
  */
  AvailableInterlocksTableModel()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  void populateTable()
  {
    _entries = new ArrayList<InterlockTableEntry>();

    // first lets get the service mode
    boolean areChainsOpen = false;
    try
    {
      _serviceModeEnabled = _interlock.isServiceModeOn();
      areChainsOpen = _interlock.isInterlockOpen();
    }
    catch (XrayTesterException xte)
    {
      handleXrayTesterException(xte);
      return;
    }

    List<String> names = _interlock.getDiagnosticsDescriptionNames();
    if (_serviceModeEnabled == false)
    {
      // they don't have it enabled so there is no way we can know what the state is
      for (String name : names)
      {
        InterlockTableEntry entry = new InterlockTableEntry(name, "TBD");
        entry.setInterlockStateIcon(_unknownStateIcon);
        _entries.add(entry);
      }
    }
    else if (_serviceModeEnabled && areChainsOpen == false)
    {
      // they have service mode enabled and no chains are open..
      for (String name : names)
      {
        InterlockTableEntry entry = new InterlockTableEntry(name, "TBD");
        entry.setInterlockStateIcon(_closedIcon);
        _entries.add(entry);
      }
    }
    else if (_serviceModeEnabled && areChainsOpen)
    {
      // they have service mode enabled and chains are open.. we need to figure out which one
      // is open and mark that as open and then the rest as unknown.
      boolean openInterlockFound = false;
      for (String name : names)
      {
        try
        {
          String nameOfOpenInterlock = _interlock.getDiagnosticsDescription();
          InterlockTableEntry entry = new InterlockTableEntry(name, "TBD");
          if(openInterlockFound == false && (name.equalsIgnoreCase(nameOfOpenInterlock)))
          {
            openInterlockFound = true;
            entry.setInterlockStateIcon(_openIcon);
          }
          else
          {
            if(openInterlockFound)
              entry.setInterlockStateIcon(_unknownStateIcon);
            else
              entry.setInterlockStateIcon(_closedIcon);
          }
          _entries.add(entry);
        }
        catch(XrayTesterException xte)
        {
          handleXrayTesterException(xte);
          return;
        }
      }
    }
    else
    {
      Assert.expect(false);
    }

    fireTableDataChanged();
  }

  /**
   * Returns the number of columns in the model.
   * @author Erica Wheatcroft
   */
  public int getColumnCount()
  {
    Assert.expect(_interLocks != null);
    return _interLocks.length;
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>.
   * @author Erica Wheatcroft
   */
  public String getColumnName(int columnIndex)
  {
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return _interLocks[columnIndex];
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return the number of rows in the model
   * @todo Implement this javax.swing.table.TableModel method
   */
  public int getRowCount()
  {
    if(_entries == null)
      return 0;

    return _entries.size();
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and <code>rowIndex</code>.
   * @author Erica Wheatcroft
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_entries != null);
    Assert.expect(rowIndex >= 0, "Row Index is negative!");
    Assert.expect(columnIndex >= 0, "Column Index is negative!");

    InterlockTableEntry tableEntry = _entries.get(rowIndex);
    Object returnValue = null;
    switch (columnIndex)
    {
      case 0:
        returnValue = tableEntry.getInterlockStateIcon();
        break;
      case 1:
        returnValue = tableEntry.getName();
        break;
      case 2:
        returnValue = tableEntry.getAbbreviation();
        break;
      default:
        Assert.expect(false);
    }

    return returnValue;
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and <code>columnIndex</code> is editable.
   *
   * @author Erica Wheatcroft
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isSortable()
  {
    return false;
  }


  /**
   * Sets the value in the cell at <code>columnIndex</code> and <code>rowIndex</code> to <code>aValue</code>.
   *
   * @author Erica Wheatcroft
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    Assert.expect(false, "nothing is editable");
  }

  /**
   * @author Erica Wheatcroft
   */
  public void sortColumn(int columnIndex, boolean ascending)
  {
    // do nothing
  }

  /**
   *
   * @author Erica Wheatcroft
   */
  synchronized void handleXrayTesterException(XrayTesterException xte)
  {
    String message = xte.getMessage();
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringUtil.format(message, 50),
                                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);
  }

  /**
   * @author Erica Wheatcroft
   */
  InterlockTableEntry getInterlockEntryAtRow(int rowIndex)
  {
    Assert.expect(rowIndex >= 0, "Row Index is negative!");

    InterlockTableEntry currentEntry = _entries.get(rowIndex);
    Assert.expect(currentEntry != null);

    return currentEntry;
  }
}
