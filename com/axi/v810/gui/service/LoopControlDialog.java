package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.*;

/**
 * @author Erica Wheatcroft
 */
public class LoopControlDialog extends JDialog
{
  // ui components for the dialog
  private JScrollPane _tableScrollPane = new JScrollPane();
  private JSortTable _availableHardwareToLoopTable = new JSortTable();
  private LoopControlTableModel _tableModel = new LoopControlTableModel();
  private NumericTextField _numberOfLoopsTextField  = new NumericTextField();
  private JCheckBox _loopContinuoslyCheckBox = new JCheckBox();
  private NumericRangePlainDocument _numericRangePlainDocument = new NumericRangePlainDocument();
  private JButton _runButton = new JButton();
  private JButton _abortButton = new JButton();
  private JButton _doneButton = new JButton();
  private JButton _addRowButton = new JButton();
  private JButton _deleteRowButton = new JButton();

  private int _numberOfLoops = 1;
  private boolean _loopContinuosly = false;
  private boolean _operationCancelled = false;

  private Map<HardwareOperationsEnum, RunnableWithExceptions> _hardwareOperationsEnumToRunnableMap = null;

  private PanelHandler _panelHandler = null;
  private StageBelts _belts = null;
  private RightOuterBarrier _rightOuterBarrier = null;
  private LeftOuterBarrier _leftOuterBarrier = null;
  private InnerBarrier _innerBarrier = null;
  private PanelClamps _panelClamps = null;
  private LeftPanelInPlaceSensor _leftPanelInPlaceSensor = null;
  private RightPanelInPlaceSensor _rightPanelInPlaceSensor = null;

  private final String _NOT_RUN_STATUS_STRING = StringLocalizer.keyToString("LOOP_DLG_NOT_RUN_STATUS_STRING_KEY");
  private final String _NOT_RUN_DUE_TO_ABORT_STATUS_STRING = StringLocalizer.keyToString("LOOP_DLG_NOT_RUN_DUE_TO_ABORT_KEY");
  private boolean _exceptionOccurred = false;


  /**
   * @author Erica Wheatcroft
   */
  public LoopControlDialog()
  {
    super(MainMenuGui.getInstance(), StringLocalizer.keyToString("LOOP_DLG_TITLE_KEY"), true );

    createDialog();

    disablePanel(false);

    initialize();

    setupRunnables();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void initialize()
  {
    _panelHandler = PanelHandler.getInstance();
    _belts = StageBelts.getInstance();
    _rightOuterBarrier = RightOuterBarrier.getInstance();
    _leftOuterBarrier = LeftOuterBarrier.getInstance();
    _innerBarrier = InnerBarrier.getInstance();
    _panelClamps = PanelClamps.getInstance();
    _leftPanelInPlaceSensor = LeftPanelInPlaceSensor.getInstance();
    _rightPanelInPlaceSensor = RightPanelInPlaceSensor.getInstance();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createDialog()
  {
    // the north panel - which i will call the instruction panel
    JPanel northPanel = new JPanel(new FlowLayout());
    JLabel instructionLabel = new JLabel();
    instructionLabel.setFont(FontUtil.getBoldFont(instructionLabel.getFont(),Localization.getLocale()));
    instructionLabel.setText(StringLocalizer.keyToString("LOOP_DLG_INSTRUCTION_KEY"));
    northPanel.add(instructionLabel);

    // center panel - which will hold the scroll pane and the table displaying all hardware that can be looped
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    JPanel tablePanel = new JPanel(new BorderLayout());
    tablePanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    tablePanel.add(_tableScrollPane);
    centerPanel.add(tablePanel, BorderLayout.CENTER);
    _tableScrollPane.getViewport().add(_availableHardwareToLoopTable);
    _tableModel = new LoopControlTableModel();
    _availableHardwareToLoopTable.setModel(_tableModel);
    _availableHardwareToLoopTable.setRowSelectionAllowed(true);
    _availableHardwareToLoopTable.setColumnSelectionAllowed(false);

    JPanel southCenterPanel = new JPanel(new BorderLayout());
    JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,0,0));

    JPanel numberOfLoopsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    numberOfLoopsPanel.add(new JLabel(StringLocalizer.keyToString("CDGUI_LOOP_COUNT_LABEL_KEY")));
    _numberOfLoopsTextField.setColumns(5);
    DecimalFormat format = new DecimalFormat();
    format.setParseIntegerOnly(true);
    _numberOfLoopsTextField.setDocument(_numericRangePlainDocument);
    _numberOfLoopsTextField.setFormat(format);
    _numericRangePlainDocument.setRange(new IntegerRange(1, Integer.MAX_VALUE));
    _numberOfLoopsTextField.setValue(_numberOfLoops);
    numberOfLoopsPanel.add(_numberOfLoopsTextField);
    controlPanel.add(numberOfLoopsPanel);

    JPanel loopContinuoslyPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    loopContinuoslyPanel.add(_loopContinuoslyCheckBox);
    _loopContinuoslyCheckBox.setText(StringLocalizer.keyToString("CDGUI_LOOP_CONTINUOSLY_KEY"));
    _loopContinuoslyCheckBox.setSelected(false);
    controlPanel.add(loopContinuoslyPanel);

    JPanel innerAddDeleteRowButtonPanel = new JPanel(new GridLayout(1,2,5,0));
    _addRowButton.setText(StringLocalizer.keyToString("LOOP_DLG_ADD_ROW_KEY"));
    _deleteRowButton.setText(StringLocalizer.keyToString("LOOP_DLG_DELETE_ROW_KEY"));
    _deleteRowButton.setEnabled(false);
    innerAddDeleteRowButtonPanel.add(_addRowButton);
    innerAddDeleteRowButtonPanel.add(_deleteRowButton);

    JPanel AddDeleteRowButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT,0,5));
    AddDeleteRowButtonPanel.add(innerAddDeleteRowButtonPanel);
    southCenterPanel.add(controlPanel, BorderLayout.WEST);
    southCenterPanel.add(AddDeleteRowButtonPanel, BorderLayout.EAST);
    centerPanel.add(southCenterPanel, BorderLayout.SOUTH);

    // the south panel - which will hold all the buttons ABORT, RUN, and OK
    JPanel southPanel = new JPanel(new BorderLayout());

    JPanel innerAbortRunButtonPanel = new JPanel(new GridLayout(1,2,5,0));
    _runButton.setText(StringLocalizer.keyToString("LOOP_DLG_RUN_BUTTON_KEY"));
    _abortButton.setText(StringLocalizer.keyToString("LOOP_DLG_ABORT_BUTTON_KEY"));
    innerAbortRunButtonPanel.add(_runButton);
    innerAbortRunButtonPanel.add(_abortButton);

    JPanel abortRunButtonsPanel = new JPanel(new FlowLayout());
    abortRunButtonsPanel.add(innerAbortRunButtonPanel);
    southPanel.add(abortRunButtonsPanel, BorderLayout. WEST);

    JPanel okButtonPanel = new JPanel(new FlowLayout());
    _doneButton.setText(StringLocalizer.keyToString("LOOP_DLG_DONE_BUTTON_KEY"));
    okButtonPanel.add(_doneButton);
    southPanel.add(okButtonPanel, BorderLayout.EAST);

    // now lets create the main panel that the above panels all belong in
    JPanel mainPanel = new JPanel(new BorderLayout(10,10));
    Border mainPanelBorder = BorderFactory.createEmptyBorder(15, 15, 15, 15);
    mainPanel.setBorder(mainPanelBorder);
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    mainPanel.add(southPanel, BorderLayout.SOUTH);
    mainPanel.add(northPanel, BorderLayout.NORTH);

    // now add the main panel to the content pane
    add(mainPanel, BorderLayout.NORTH);

    // set the title
    setResizable(true);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    addListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _addRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _tableModel.insertRow();
        int rowCount = _tableModel.getRowCount();
        _availableHardwareToLoopTable.setRowSelectionInterval(rowCount - 1, rowCount - 1);
        if(rowCount >= 1)
          _deleteRowButton.setEnabled(true);

        setupTable();
      }
    });

    _deleteRowButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
         int selectedRowIndex = _availableHardwareToLoopTable.getSelectedRow();
         Assert.expect(selectedRowIndex >= 0);
         _tableModel.deleteRow(selectedRowIndex);

         int rowCount = _tableModel.getRowCount();
         if(rowCount == 0)
           _deleteRowButton.setEnabled(false);
         else
           _availableHardwareToLoopTable.setRowSelectionInterval(0, 0);

         setupTable();
      }
    });
    _numberOfLoopsTextField.addActionListener( new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        handleLoopCountEntry();
      }
    });
    _numberOfLoopsTextField.addFocusListener( new FocusListener()
    {
      public void focusGained(FocusEvent evt)
      {
        // do nothing
      }

      public void focusLost(FocusEvent evt)
      {
        handleLoopCountEntry();
      }
    });
    _loopContinuoslyCheckBox.addActionListener( new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
        if (_loopContinuosly)
          _numberOfLoopsTextField.setEnabled(false);
        else
          _numberOfLoopsTextField.setEnabled(true);
      }
    });
    _doneButton.addActionListener( new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    _abortButton.addActionListener( new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        _operationCancelled = true;
      }
    });
    _runButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        run();
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void disablePanel(boolean hardwareMoving)
  {
     _numberOfLoopsTextField.setEnabled(! hardwareMoving);
     _loopContinuoslyCheckBox.setEnabled(! hardwareMoving);
     _runButton.setEnabled(! hardwareMoving);
     _abortButton.setEnabled(hardwareMoving);
     _doneButton.setEnabled(!hardwareMoving);
     _addRowButton.setEnabled(!hardwareMoving);
     _deleteRowButton.setEnabled(!hardwareMoving);
     _availableHardwareToLoopTable.setEnabled(!hardwareMoving);
     if (hardwareMoving == false)
     {
       if (_loopContinuosly)
         _numberOfLoopsTextField.setEnabled(false);
       else
         _numberOfLoopsTextField.setEnabled(true);

       if(_tableModel.getRowCount() > 0)
         _availableHardwareToLoopTable.setRowSelectionInterval(0, 0);
       _abortButton.setEnabled(false);
     }
     else
     {
       _abortButton.setEnabled(true);
     }

     if(_exceptionOccurred)
     {
       _runButton.setEnabled(false);
       _abortButton.setEnabled(false);
     }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void run()
  {
    _operationCancelled = false;

    disablePanel(true);

    _tableModel.clearStatus();

    final java.util.List<HardwareOperationsEnum> selectedOperationsEnums = _tableModel.getSelectedHardwareOperations();

    SwingWorkerThread.getInstance().invokeLater(new Runnable()
    {
      public void run()
      {
        // lets loop for a finite #
        if (_loopContinuosly == false && _numberOfLoops >= 1)
        {
          for (int loopCount = 0; loopCount < _numberOfLoops && _operationCancelled == false; loopCount++)
            loop(selectedOperationsEnums, loopCount);

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                disablePanel(false);
              }
            });
            return;
        }
        else if (_loopContinuosly)
        {
          int loopCount = 0;

          // loop until the user tells us to stop
          while(_operationCancelled == false)
          {
            loop(selectedOperationsEnums, loopCount);
            loopCount++;
          }

          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              disablePanel(false);
            }
          });
          return;
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void loop(java.util.List<HardwareOperationsEnum> selectedOperationsEnums, final int loopCount) throws HeadlessException
  {
      int hardwareOperationsCount = 0;
      for (HardwareOperationsEnum hardwareOpertation : selectedOperationsEnums)
      {
        final int finalHardwareOperationsCount = hardwareOperationsCount;
        if(_operationCancelled)
        {
          // lets check to see if the operation is aborted if so update the status and then return out.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
               _tableModel.updateRemaningRowsWithWillNotRunStatus(_NOT_RUN_DUE_TO_ABORT_STATUS_STRING, finalHardwareOperationsCount);
            }
          });
          break;
        }
        final RunnableWithExceptions runnable = _hardwareOperationsEnumToRunnableMap.get(hardwareOpertation);
        try
        {
          // run the runnable
          HardwareWorkerThread.getInstance().invokeAndWait(runnable);

          // if we get to this point then we know we have succeeded update the status
          final int hardwareOperationsCountFinal = hardwareOperationsCount;

          if(_loopContinuosly == false)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                String status = StringLocalizer.keyToString( new LocalizedString("LOOP_DLG_LOOP_FINITE_STATUS_STRING_KEY", new Object[]{(loopCount + 1), _numberOfLoops}));
                _tableModel.updateStatusForHardwareOperation(status, hardwareOperationsCountFinal);
              }
            });
          }
          else
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                String status = StringLocalizer.keyToString( new LocalizedString("LOOP_DLG_LOOP_CONTINUOSLY_STATUS_STRING_KEY", new Object[]{(loopCount + 1)}));
                _tableModel.updateStatusForHardwareOperation(status , hardwareOperationsCountFinal);
              }
            });
          }
          hardwareOperationsCount++;
        }
        catch (final XrayTesterException xte)
        {
          final int finalLoopCount = loopCount;

          // cancel the process
          _operationCancelled = true;

          _exceptionOccurred = true;

          // display the exception back on the swing thread.
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              disablePanel(false);

              String status = StringLocalizer.keyToString( new LocalizedString("LOOP_DLG_EXCEPTION_OCCURED_STATUS_STRING_KEY", new Object[]{(finalLoopCount + 1)}));
              // an exception occured so update the status
              _tableModel.updateStatusForHardwareOperation(status + (finalLoopCount + 1), finalHardwareOperationsCount);

              // now update the rest to show that they will not be run
              _tableModel.updateRemaningRowsWithWillNotRunStatus(_NOT_RUN_STATUS_STRING, finalHardwareOperationsCount + 1);

              String messageToDisplay = StringUtil.format(xte.getLocalizedMessage(), 50);
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                            messageToDisplay,
                                            StringLocalizer.keyToString("LOOP_DLG_TITLE_KEY"),
                                            JOptionPane.ERROR_MESSAGE);

            }
          });
          return;
        }
        catch (Throwable throwableException)
        {
          Assert.logException(throwableException);
          Assert.expect(false);
        }
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleLoopCountEntry()
  {
    try
    {
      int intValue = _numberOfLoopsTextField.getNumberValue().intValue();
      _numberOfLoops = intValue;
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _numberOfLoopsTextField.setValue(_numberOfLoops);
    }

    _loopContinuosly = _loopContinuoslyCheckBox.isSelected();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setupTable()
  {
    _availableHardwareToLoopTable.getTableHeader().setReorderingAllowed(false);

    TableColumn hardwareOperationsColumn = _availableHardwareToLoopTable.getColumnModel().getColumn(0);
    JComboBox hardwareOperationsComboBox = new JComboBox();
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.BELT_MOTION_ON);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.BELT_MOTION_OFF);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.OPEN_CLAMPS);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.CLOSE_CLAMPS);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.OPEN_INNER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.CLOSE_INNER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.OPEN_LEFT_OUTER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.CLOSE_LEFT_OUTER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.OPEN_RIGHT_OUTER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.CLOSE_RIGHT_OUTER_BARRIER);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.EXTEND_LEFT_PIP);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RETRACT_LEFT_PIP);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.EXTEND_RIGHT_PIP);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RETRACT_RIGHT_PIP);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.LEFT_TO_RIGHT_BELT_DIRECTION);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RIGHT_TO_LEFT_BELT_DIRECTION);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RAIL_WIDTH_HOME);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RAIL_WIDTH_MAX);
    hardwareOperationsComboBox.addItem(HardwareOperationsEnum.RAIL_WIDTH_MIN);
    hardwareOperationsColumn.setCellEditor(new DefaultCellEditor(hardwareOperationsComboBox));

    // set up column widths
    ColumnResizer.adjustColumnPreferredWidths(_availableHardwareToLoopTable, true);

    int padding = 175;
    int origTableWidth = _availableHardwareToLoopTable.getMinimumSize().width;
    int scrollHeight = _availableHardwareToLoopTable.getPreferredSize().height;
    int tableHeight = _availableHardwareToLoopTable.getPreferredSize().height;
    int prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
      _availableHardwareToLoopTable.setPreferredScrollableViewportSize(prefScrollSize);
    }
  }

  /**
   * set up the map that will map HardwareOperationsEnum to the appropriate runnable
   * @author Erica Wheatcroft
   */
  private void setupRunnables()
  {
    _hardwareOperationsEnumToRunnableMap = new HashMap<HardwareOperationsEnum,RunnableWithExceptions>();

    RunnableWithExceptions openLeftOuterBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _leftOuterBarrier.open();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.OPEN_LEFT_OUTER_BARRIER, openLeftOuterBarrierRunnable);

    RunnableWithExceptions closeLeftOuterBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _leftOuterBarrier.close();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.CLOSE_LEFT_OUTER_BARRIER, closeLeftOuterBarrierRunnable);

    RunnableWithExceptions openRightOuterBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _rightOuterBarrier.open();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.OPEN_RIGHT_OUTER_BARRIER, openRightOuterBarrierRunnable);

    RunnableWithExceptions closeRightOuterBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _rightOuterBarrier.close();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.CLOSE_RIGHT_OUTER_BARRIER, closeRightOuterBarrierRunnable);

    RunnableWithExceptions openInnerBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _innerBarrier.open();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.OPEN_INNER_BARRIER, openInnerBarrierRunnable);

    RunnableWithExceptions closeInnerBarrierRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _innerBarrier.close();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.CLOSE_INNER_BARRIER, closeInnerBarrierRunnable);

    RunnableWithExceptions beltMotionOffRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _belts.off();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.BELT_MOTION_OFF, beltMotionOffRunnable);

    RunnableWithExceptions beltMotionOnRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _belts.on();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.BELT_MOTION_ON, beltMotionOnRunnable);

    RunnableWithExceptions closeClampsRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelClamps.close();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.CLOSE_CLAMPS, closeClampsRunnable);

    RunnableWithExceptions openClampsRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelClamps.open();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.OPEN_CLAMPS, openClampsRunnable);

    RunnableWithExceptions extendLeftPipRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _leftPanelInPlaceSensor.extend();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.EXTEND_LEFT_PIP, extendLeftPipRunnable);

    RunnableWithExceptions retractLeftPipRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _leftPanelInPlaceSensor.retract();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RETRACT_LEFT_PIP, retractLeftPipRunnable);

    RunnableWithExceptions extendRightPipRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _rightPanelInPlaceSensor.extend();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.EXTEND_RIGHT_PIP, extendRightPipRunnable);

    RunnableWithExceptions retractRightPipRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _rightPanelInPlaceSensor.retract();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RETRACT_RIGHT_PIP, retractRightPipRunnable);

    RunnableWithExceptions leftToRightBeltDirectionRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _belts.setDirectionLeftToRight();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.LEFT_TO_RIGHT_BELT_DIRECTION, leftToRightBeltDirectionRunnable);

    RunnableWithExceptions rightToLeftBeltDirectionRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _belts.setDirectionRightToLeft();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RIGHT_TO_LEFT_BELT_DIRECTION, rightToLeftBeltDirectionRunnable);

    RunnableWithExceptions railWidthHomeRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.homeStageRails();
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RAIL_WIDTH_HOME, railWidthHomeRunnable);

    RunnableWithExceptions railWidthMaxRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setRailWidthInNanoMeters(_panelHandler.getMaximumPanelWidthInNanoMeters());
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RAIL_WIDTH_MAX, railWidthMaxRunnable);

    RunnableWithExceptions railWidthMinRunnable = new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _panelHandler.setRailWidthInNanoMeters(_panelHandler.getMinimumRailWidthInNanoMeters());
      }
    };
    _hardwareOperationsEnumToRunnableMap.put(HardwareOperationsEnum.RAIL_WIDTH_MIN, railWidthMinRunnable);
  }
}
