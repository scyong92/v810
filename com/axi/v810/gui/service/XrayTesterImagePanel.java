package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.BusinessException;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.hardware.*;

/**
 * @author George A. David
 */
public class XrayTesterImagePanel extends JPanel implements Observer
{
  // drawing placement issues
  private int _x = 250;
  private int _y = 190;
  private final int _xrayTesterWidth = 315;
  private final int _xrayTesterHeight = 200;

  private int _legendWidth = 120;
  private int _legendHeight = 140;

  private final int _loaderWidth = 75;
  private final int _loaderHeight = _xrayTesterHeight / 2; // 1/2 of x-ray tester height
  private final int _barrierWidth = 15;
  private final int _barrierHeight = _loaderHeight * 2 / 3;
  private final int _panelOpticalSize = 13;

  private final int _safetyLevelWidth = 5;
  private final int _safetyLevelHeight = _xrayTesterHeight * 3 / 4;


  // These member variables are related to the drawable components (doors, sensors, etc)
  // in the drawing

  private boolean _leftOuterBarrierOpen = false;
  private boolean _rightOuterBarrierOpen = false;
  private boolean _innerBarrierOpen = false;

  // panel optical (crunch) sensors (4)
  private boolean _isLeftPanelClearSensorBlocked = false;
  private boolean _isRightPanelClearSensorBlocked = false;

  // panel- Xray Height Safety Level sensors
  private boolean _isXrayLeftSafetyLevelOK = false;
  private boolean _isXrayRightSafetyLevelOK = false;

  // panel- Xray Z axis position
  private boolean _isUpToPosition1 = false;
  private boolean _isUpToPosition2 = false;
  private boolean _isHighMag = false;
  private boolean _isHome = true; //Ngie Xing this is for s2ex based machines
  
  // Swee Yee - panel- Xray Height Safety Level sensors
  private boolean _isSafetySensorUp = true;

  // belt direction
  private boolean _beltDirectionForward = true;

  private BorderLayout _mainBorderLayout = new BorderLayout();

  /**
   * @author George A. David
   */
  XrayTesterImagePanel()
  {
    try
    {
         if(LicenseManager.isVariableMagnificationEnabled()==true)
         {
            _legendWidth = 140;
            _legendHeight = 270;    
         }
         else
         {
            _legendWidth = 120;
            _legendHeight = 140;    
         }
    }catch(BusinessException bex)
    {
      //do nothing
    }
    createTesterImage();
  }

  /**
   * @author George A. David
   */
  private void createTesterImage()
  {
    setLayout(_mainBorderLayout);
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    drawXrayTester(g);
    drawLegend(g);

    drawSensorComponents(g);
  }

  /**
   * @author George A. David
   */
  private void drawXrayTester(Graphics g)
  {
    // find our new x,y based on JPanel size
    int newx;
    int newy;
    int panelHeight = this.getHeight();
    int panelWidth = this.getWidth();
    // for now, just mess with Y - center it
    newy = (panelHeight/2) - (_xrayTesterHeight/2);
    // don't let it go negative
    if (newy < 0)
      newy = 0;

    // ok, now X -- must take into account the legend too.
    // total width of tester + legeng = 510 pixels
    int totalSizeInX = _xrayTesterWidth + _legendWidth + _loaderWidth;
    newx = (panelWidth/2) - (totalSizeInX/2);
    // don't let it go negative
    if (newx < 0)
      newx = 0;

    _y = newy;
    _x = newx;

    g.setColor(Color.black);
    g.drawRect(_x, _y, _xrayTesterWidth, _xrayTesterHeight);
    // put in the X-ray Tester text
    Font f = g.getFont();
    Font newFont = FontUtil.getRendererFont(18);
    FontMetrics fm = getFontMetrics(newFont);
    String message = "AXI System v810";
    int stringWidth = fm.stringWidth(message);
    int xCenter = _x + (_xrayTesterWidth/2) - (stringWidth/2);
    g.setFont(newFont);
    g.drawString(message, xCenter, _y + 45); // was y+60

    g.setFont(f); // restore font.

    // vertical and horizontal lines for debug reference
    //g.drawLine(_x+_width/2, _y, _x+_width/2, _y+_height);
    //g.drawLine(_x, _y+_height/2, _x+_width, _y+_height/2);
  }

  /**
   * @author George A. David
   */
  private void drawLegend(Graphics g)
  {
    // this must be called after drawXrayTester, as that method sets the new _x _y
    Graphics2D g2 = (Graphics2D)g;
    int legendx = _x + _xrayTesterWidth + _loaderWidth;
    int legendsymx = legendx + 10;
//    int legendy = 210;
    int legendy = (getHeight()/2) - _legendHeight/2;
    if (legendy < 0)
      legendy = 0;

    Font f = g.getFont();
    g.setFont(FontUtil.getServiceLegendFont());

//    g.drawRect(legendx, legendy, 120, 140);
    g.drawRect(legendx, legendy, _legendWidth, _legendHeight);
    // closed circle for block sensor
    Ellipse2D e = new Ellipse2D.Double(legendsymx, legendy + 10, 10, 10);
    g2.setPaint(Color.black);
    g2.fill(e);
    g2.setColor(Color.black);
    g2.setStroke(new BasicStroke(2.0f));
    g2.draw(e);
    g.drawString("Blocked Panel", legendsymx + 20, legendy + 12);
    g.drawString("Optical Sensor", legendsymx + 20, legendy + 22);

    // open circle for open sensor
    e = new Ellipse2D.Double(legendsymx, legendy + 33, 10, 10);
    g2.setPaint(Color.white);
    g2.fill(e);
    g2.setColor(Color.black);
    g2.setStroke(new BasicStroke(2.0f));
    g2.draw(e);

    g2.setColor(Color.black);
    g.drawString("Clear Panel", legendsymx + 20, legendy + 37);
    g.drawString("Optical Sensor", legendsymx + 20, legendy + 47);

    // closed barrier
    g.fillRect(legendsymx, legendy + 52, 10, 35);
    g.drawString("Closed Barrier", legendsymx + 20, legendy + 72);

    // open barrier
    g2.setStroke(new BasicStroke(2.0f));
    g.drawLine(legendsymx, legendy + 98, legendsymx + 10, legendy + 98); // horizontal
    g.drawLine(legendsymx + 5, legendy + 93, legendsymx + 5, legendy + 98); // vertical
    g.drawLine(legendsymx, legendy + 123, legendsymx + 10, legendy + 123); // horizontal
    g.drawLine(legendsymx + 5, legendy + 123, legendsymx + 5, legendy + 128); // vertical
    g.drawString("Open Barrier", legendsymx + 20, legendy + 112);

    
    try
    {
        if(LicenseManager.isVariableMagnificationEnabled()==true)
        { 
            //safety level OK
            int safetyLevelHeight = 25;
            int safetyLevelx = legendsymx + 3;
            int safetyLevely = legendy +140;
            float dash[] = { 2.0f };
            g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT,
                             BasicStroke.JOIN_MITER, 2.0f, dash, 0.0f));

            g.setColor(SystemColor.WHITE);

            g.drawLine(safetyLevelx + 1, safetyLevely, safetyLevelx + 1, safetyLevely +safetyLevelHeight );
            g2.setStroke(new BasicStroke(3.0f));

            g.setColor(Color.black);
            g.drawLine(safetyLevelx - 2, safetyLevely + 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + 1);
            g.drawLine(safetyLevelx - 2, safetyLevely + safetyLevelHeight  - 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + safetyLevelHeight  - 1);
            g.drawString("Safety Level OK", legendsymx + 20, legendy + 155);


            //safety level Not OK
            safetyLevelx = legendsymx + 3;
            safetyLevely = legendy + 170;
            g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT,
                             BasicStroke.JOIN_MITER, 2.0f, dash, 0.0f));

            g.setColor(SystemColor.BLACK);

            g.drawLine(safetyLevelx + 1, safetyLevely, safetyLevelx + 1, safetyLevely +safetyLevelHeight );
            g2.setStroke(new BasicStroke(3.0f));

            g.setColor(Color.black);
            g.drawLine(safetyLevelx - 2, safetyLevely + 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + 1);
            g.drawLine(safetyLevelx - 2, safetyLevely + safetyLevelHeight  - 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + safetyLevelHeight  - 1);
            g.drawString("Safety Level Not OK", legendsymx + 20, legendy + 185);

            //g2.setStroke(s); // restore


            //Draw High Magnification Legend
            int arrowLength = 10;
            int arrowHeadSize = 3;
            int xrayHeadSize = 5;
            int magTextX = legendsymx + arrowHeadSize + 3;
            int magTextY = legendy + 230 + arrowLength + arrowHeadSize*2;

            int arrowX = magTextX ;
            int arrowY = magTextY - arrowLength - 28;

            //clear out area to draw a new arrow, otherwise you might see two arrowheads
            g.setColor(getBackground());
            g.setColor(Color.black);
            g.fillRect( arrowX - xrayHeadSize - 2,arrowY - xrayHeadSize,
                    xrayHeadSize * 2 + 4, arrowLength + xrayHeadSize * 2 + 6);


            Polygon pg = new Polygon();

            arrowY = arrowY + arrowHeadSize*2;
            //Ngie Xing, for S2EX based machines, we'll want to add for Home, Low Mag and High Mag
            //however, the current feature only includes up(home) and down
            //so for now we'll set it to Home
            if(XrayTester.isS2EXEnabled())
              g.drawString("Home", legendsymx + 20, magTextY - 25);
            else
              g.drawString("Low Mag", legendsymx + 20, magTextY - 25);

            g.setColor(Color.WHITE);
            g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
            pg.addPoint(arrowX - arrowHeadSize, arrowY);
            pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
            pg.addPoint(arrowX + arrowHeadSize, arrowY);

            g.drawPolygon(pg);
            g.fillPolygon(pg);

            //Draw High Magnification Legend
            magTextX = legendsymx + arrowHeadSize + 3;
            magTextY = legendy + 260 + arrowLength + arrowHeadSize*2;

            arrowX = magTextX ;
            arrowY = magTextY - arrowLength - 28;

            //clear out area to draw a new arrow, otherwise you might see two arrowheads
            g.setColor(getBackground());
            g.setColor(Color.black);
            g.fillRect( arrowX - xrayHeadSize - 2,arrowY - xrayHeadSize,
                    xrayHeadSize * 2 + 4, arrowLength + xrayHeadSize * 2 + 6);


            pg = new Polygon();

            g.drawString("High Mag", legendsymx + 20, magTextY - 25);
            g.setColor(Color.WHITE);
            g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
            pg.addPoint(arrowX - arrowHeadSize, arrowY + arrowLength);
            pg.addPoint(arrowX , arrowY+ arrowLength + arrowHeadSize * 2);
            pg.addPoint(arrowX+ arrowHeadSize, arrowY + arrowLength );

            g.drawPolygon(pg);
            g.fillPolygon(pg);
        }
    }catch(BusinessException bex)
    {
      //do nothing
    }

    g.setFont(f); // restore
  }

  /**
   * @author George A. David
   */
  private void drawSensorComponents(Graphics g)
  {
    drawLeftBarrier(g);
    drawRightBarrier(g);
    drawFrontLeftOpticalSensor(g);
    drawFrontRightOpticalSensor(g);
//    drawRearLeftOpticalSensor(g);
//    drawRearRightOpticalSensor(g);

    drawShutterBarrier(g);
    drawBeltDirection(g);

    
    try
    {
         if(LicenseManager.isVariableMagnificationEnabled()==true)
         {
            drawXrayLeftSafetyLevelOpticalSensor(g);
            drawXrayRightSafetyLevelOpticalSensor(g);
            drawMagnificationLevelStatus(g);
            
            if(DigitalIo.isXrayMotorizedHeightLevelSafetySensorInstalled())
            {
              drawLeftMotorizedSafetyLevelStatus(g);
              drawRightMotorizedSafetyLevelStatus(g);
            }
         }
    }catch(BusinessException bex)
    {
      //do nothing
    }
  }


  /**
   * @author George A. David
   */
  private void drawLeftBarrier(Graphics g)
  {
    int barrierx = _x - _barrierWidth / 2;
    int barriery = _y + _xrayTesterHeight / 4 + _loaderHeight / 6;

    if (_leftOuterBarrierOpen)
    {
      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(2.0f));
      g.setColor(SystemColor.control);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
      g.setColor(Color.black);
      g.drawLine(barrierx + 1, barriery + 1, barrierx + _barrierWidth - 1, barriery + 1);
      g.drawLine(barrierx + 1, barriery + _barrierHeight - 1, barrierx + _barrierWidth - 1, barriery + _barrierHeight - 1);
      g2.setStroke(s); // restore
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
    }
  }


  /**
   * @author George A. David
   */
  private void drawRightBarrier(Graphics g)
  {
    int barrierx = _x + _xrayTesterWidth - _barrierWidth / 2;
    int barriery = _y + _xrayTesterHeight / 4 + _loaderHeight / 6;

    if (_rightOuterBarrierOpen)
    {
      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(2.0f));
      g.setColor(SystemColor.control);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
      g.setColor(Color.black);
      g.drawLine(barrierx + 1, barriery + 1, barrierx + _barrierWidth - 1, barriery + 1);
      g.drawLine(barrierx + 1, barriery + _barrierHeight - 1, barrierx + _barrierWidth - 1, barriery + _barrierHeight - 1);
      g2.setStroke(s); // restore
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx, barriery, _barrierWidth, _barrierHeight);
    }
  }

  /**
   * @author George A. David
   */
  private void drawFrontLeftOpticalSensor(Graphics g)
  {
    int optx = _x + _barrierWidth / 2;
    int opty = _y + _xrayTesterHeight * 3 / 4 - _loaderHeight / 6;

    if (_isLeftPanelClearSensorBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author George A. David
   */
  private void drawFrontRightOpticalSensor(Graphics g)
  {
    int optx = _x + _xrayTesterWidth - _barrierWidth / 2 - _panelOpticalSize;
    int opty = _y + _xrayTesterHeight * 3 / 4 - _loaderHeight / 6;

    if (_isRightPanelClearSensorBlocked)
    {
      g.setColor(Color.black);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
    }
    else
    {
      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();
      g2.setStroke(new BasicStroke(1.5f));
      g.setColor(Color.white);
      g.fillOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g.setColor(Color.black);
      g.drawOval(optx, opty, _panelOpticalSize, _panelOpticalSize);
      g2.setStroke(s);
    }
  }

  /**
   * @author George A. David
   */
  private void drawBeltDirection(Graphics g)
  {
    int beltTextX = _x + _xrayTesterWidth / 2 - 30;
    int beltTextY = _y + _xrayTesterHeight - 30;

    int arrowX = beltTextX - 15;
    int arrowY = beltTextY + 10;
    int arrowLength = 100;
    int arrowHeadSize = 5;

    //clear out area to draw a new arrow, otherwise you might see two arrowheads
    g.setColor(getBackground());
//    g.setColor(CommonUtil._SECONDARY_COLOR_BACKGROUND);
    g.fillRect(arrowX - arrowHeadSize * 2 - 2, arrowY - arrowHeadSize - 2, arrowLength + arrowHeadSize * 4 + 6, arrowHeadSize * 2 + 6);
    g.setColor(Color.black);
    g.drawString("Belt Direction", beltTextX, beltTextY);

    g.drawLine(arrowX, arrowY, arrowX + arrowLength, arrowY);
    Polygon pg = new Polygon();
    if (_beltDirectionForward)
    {
      pg.addPoint(arrowX + arrowLength, arrowY - arrowHeadSize);
      pg.addPoint(arrowX + arrowLength + arrowHeadSize * 2, arrowY);
      pg.addPoint(arrowX + arrowLength, arrowY + arrowHeadSize);
    }
    else
    {
      pg.addPoint(arrowX, arrowY - arrowHeadSize);
      pg.addPoint(arrowX - arrowHeadSize * 2, arrowY);
      pg.addPoint(arrowX, arrowY + arrowHeadSize);
    }
    g.drawPolygon(pg);
    g.fillPolygon(pg);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void drawShutterBarrier(Graphics g)
  {
    // the shutter on the series3 is drawn lower than on the 2L because the middle rail is centered

    // since this barrier is drawn sideways, height and width are reversed
    int barrierx = _x + _xrayTesterWidth / 2 - _barrierHeight / 2;
    int barriery = _y + _xrayTesterHeight / 2 - _barrierWidth / 2 + 20;
    int insetLength = 12; // the length of the horzontal little leg like in the legend.

    Graphics2D g2 = (Graphics2D)g;
    Stroke s = g2.getStroke();
    g2.setStroke(new BasicStroke(2.0f));

    if (_innerBarrierOpen)
    {
      g.setColor(SystemColor.control);
      g.fillRect(barrierx + insetLength, barriery, _barrierHeight + 1 - 2 * insetLength, _barrierWidth + 1);
    }
    else
    {
      g.setColor(Color.black);
      g.fillRect(barrierx + insetLength, barriery + 2, _barrierHeight - 2 * insetLength, _barrierWidth - 4);
    }

    g.setColor(Color.black);
    g.drawLine(barrierx + 1 + insetLength, barriery + 1, barrierx + 1 + insetLength, barriery + _barrierWidth - 1); // left wall
    g.drawLine(barrierx + _barrierHeight - insetLength, barriery + 1, barrierx + _barrierHeight - insetLength,
               barriery + _barrierWidth - 1); // right wall
    // draw insets
    g.drawLine(barrierx + 1, barriery + _barrierWidth / 2, barrierx + 1 + insetLength, barriery + _barrierWidth / 2); // left inset
    g.drawLine(barrierx + _barrierHeight - insetLength, barriery + _barrierWidth / 2, barrierx + _barrierHeight,
               barriery + _barrierWidth / 2); // right inset

    g2.setStroke(s); // restore
  }

  /**
   * @author Anthony Fong
   */
  private void drawXrayLeftSafetyLevelOpticalSensor(Graphics g)
  {
    int safetyLevelx = _x - _safetyLevelWidth / 2 +40;
    int safetyLevely = _y + _xrayTesterHeight / 2 - _safetyLevelHeight / 2;


      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();

      float dash[] = { 10.0f };
      g2.setStroke(new BasicStroke(5.0f, BasicStroke.CAP_BUTT,
                       BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));

      if (_isXrayLeftSafetyLevelOK )
      {
         g.setColor(SystemColor.WHITE);
      }
      else
      {
         g.setColor(SystemColor.BLACK);
      }

      g.drawLine(safetyLevelx + 1, safetyLevely, safetyLevelx + 1, safetyLevely +_safetyLevelHeight );
      g2.setStroke(new BasicStroke(5.0f));

      g.setColor(Color.black);
      g.drawLine(safetyLevelx - 2, safetyLevely + 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + 1);
      g.drawLine(safetyLevelx - 2, safetyLevely + _safetyLevelHeight  - 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + _safetyLevelHeight  - 1);
      g2.setStroke(s); // restore

  }
  /**
   * @author Anthony Fong
   */
  private void drawXrayRightSafetyLevelOpticalSensor(Graphics g)
  {
    int safetyLevelx = _x - _safetyLevelWidth / 2 +_xrayTesterWidth  -40;
    int safetyLevely = _y + _xrayTesterHeight / 2 - _safetyLevelHeight / 2;

      Graphics2D g2 = (Graphics2D)g;
      Stroke s = g2.getStroke();

      float dash[] = { 10.0f };
      g2.setStroke(new BasicStroke(5.0f, BasicStroke.CAP_BUTT,
                       BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));

      if (_isXrayRightSafetyLevelOK )
      {
         g.setColor(SystemColor.WHITE);
      }
      else
      {
         g.setColor(SystemColor.BLACK);
      }

      g.drawLine(safetyLevelx + 1, safetyLevely, safetyLevelx + 1, safetyLevely +_safetyLevelHeight );
      g2.setStroke(new BasicStroke(5.0f));

      g.setColor(Color.black);
      g.drawLine(safetyLevelx - 2, safetyLevely + 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + 1);
      g.drawLine(safetyLevelx - 2, safetyLevely + _safetyLevelHeight  - 1, safetyLevelx + _safetyLevelWidth - 1, safetyLevely + _safetyLevelHeight  - 1);
      g2.setStroke(s); // restore
  }
  
    /**
   * @author Anthony Fong
   */
  private void drawMagnificationLevelStatus(Graphics g)
  {
    int magTextX = _x + _xrayTesterWidth / 2 ;
    int magTextY = _y + _xrayTesterHeight / 2+5;

    int arrowLength = 20;
    int arrowHeadSize = 5;
    int xrayHeadSize = 8;
    int arrowX = magTextX ;
    int arrowY = magTextY - arrowLength - 28;

    //clear out area to draw a new arrow, otherwise you might see two arrowheads
    g.setColor(getBackground());
    g.setColor(Color.black);
    g.fillRect( arrowX - xrayHeadSize - 4,arrowY - xrayHeadSize,
            xrayHeadSize * 2 + 8, arrowLength + xrayHeadSize * 2 + 8);
    

    Polygon pg = new Polygon();
        
    if (_isHome)
    {
      arrowY = arrowY + arrowHeadSize*2;
      g.drawString("Home", magTextX-16, magTextY);

      g.setColor(Color.WHITE);
      g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
      pg.addPoint(arrowX - arrowHeadSize, arrowY);
      pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
      pg.addPoint(arrowX + arrowHeadSize, arrowY);
    }
    else
    {
      //_isHighMag=true;
      if (_isHighMag)
      {
        g.drawString("High Mag", magTextX-25, magTextY);

        g.setColor(Color.WHITE);
        g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
        pg.addPoint(arrowX - arrowHeadSize, arrowY + arrowLength);
        pg.addPoint(arrowX , arrowY+ arrowLength + arrowHeadSize * 2);
        pg.addPoint(arrowX+ arrowHeadSize, arrowY + arrowLength );
      }
      else if (_isUpToPosition1)
      {
        if(XrayCylinderActuator.isInstalled())
        {
          arrowY = arrowY + arrowHeadSize * 2;
          g.drawString("Low Mag", magTextX - 25, magTextY);

          g.setColor(Color.WHITE);
          g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
          pg.addPoint(arrowX - arrowHeadSize, arrowY);
          pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
          pg.addPoint(arrowX + arrowHeadSize, arrowY);
        }
        else if(XrayCPMotorActuator.isInstalled())
        {
          arrowY = arrowY + arrowHeadSize * 2;
          g.drawString("Up To Pos 1", magTextX - 25, magTextY);

          g.setColor(Color.WHITE);
          g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
          pg.addPoint(arrowX - arrowHeadSize, arrowY);
          pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
          pg.addPoint(arrowX + arrowHeadSize, arrowY);
        }
      }
      else
      {
        arrowY = arrowY + arrowHeadSize*2;
        g.drawString("Up To Pos 2", magTextX-25, magTextY);

        g.setColor(Color.WHITE);
        g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
        pg.addPoint(arrowX - arrowHeadSize, arrowY);
        pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
        pg.addPoint(arrowX + arrowHeadSize, arrowY);
      }
    }
    g.drawPolygon(pg);
    g.fillPolygon(pg);

  }
  
  /**
   * @author Swee Yee Wong
   */
  private void drawRightMotorizedSafetyLevelStatus(Graphics g)
  {
    
    int magTextX = _x - _safetyLevelWidth / 2 +_xrayTesterWidth  -39;
    int magTextY = _y + _xrayTesterHeight / 2 - _safetyLevelHeight / 2 + 200;

    int arrowLength = 10;
    int arrowHeadSize = 2;
    int xrayHeadSize = 4;
    int arrowX = magTextX ;
    int arrowY = magTextY - arrowLength - 28;

    //clear out area to draw a new arrow, otherwise you might see two arrowheads
    g.setColor(getBackground());
    g.setColor(Color.black);
    g.fillRect( arrowX - xrayHeadSize - 4,arrowY - xrayHeadSize,
            xrayHeadSize * 2 + 8, arrowLength + xrayHeadSize * 2 + 8);
    

    Polygon pg = new Polygon();
      
    if (_isSafetySensorUp)
    {
      arrowY = arrowY + arrowHeadSize * 2;
      g.drawString("Up", magTextX - 25, magTextY);

      g.setColor(Color.WHITE);
      g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
      pg.addPoint(arrowX - arrowHeadSize, arrowY);
      pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
      pg.addPoint(arrowX + arrowHeadSize, arrowY);
    }
    else
    {
      g.drawString("Down", magTextX - 25, magTextY);

      g.setColor(Color.WHITE);
      g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
      pg.addPoint(arrowX - arrowHeadSize, arrowY + arrowLength);
      pg.addPoint(arrowX, arrowY + arrowLength + arrowHeadSize * 2);
      pg.addPoint(arrowX + arrowHeadSize, arrowY + arrowLength);
    }
      
    g.drawPolygon(pg);
    g.fillPolygon(pg);
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void drawLeftMotorizedSafetyLevelStatus(Graphics g)
  {
    int magTextX = _x - _safetyLevelWidth / 2 +41;
    int magTextY = _y + _xrayTesterHeight / 2 - _safetyLevelHeight / 2 + 200;

    int arrowLength = 10;
    int arrowHeadSize = 2;
    int xrayHeadSize = 4;
    int arrowX = magTextX ;
    int arrowY = magTextY - arrowLength - 28;

    //clear out area to draw a new arrow, otherwise you might see two arrowheads
    g.setColor(getBackground());
    g.setColor(Color.black);
    g.fillRect( arrowX - xrayHeadSize - 4,arrowY - xrayHeadSize,
            xrayHeadSize * 2 + 8, arrowLength + xrayHeadSize * 2 + 8);
    

    Polygon pg = new Polygon();
      
    if (_isSafetySensorUp)
    {
      arrowY = arrowY + arrowHeadSize * 2;
      g.drawString("Up", magTextX - 25, magTextY);

      g.setColor(Color.WHITE);
      g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
      pg.addPoint(arrowX - arrowHeadSize, arrowY);
      pg.addPoint(arrowX, arrowY - arrowHeadSize * 2);
      pg.addPoint(arrowX + arrowHeadSize, arrowY);
    }
    else
    {
      g.drawString("Down", magTextX - 25, magTextY);

      g.setColor(Color.WHITE);
      g.drawLine(arrowX, arrowY, arrowX, arrowY + arrowLength);
      pg.addPoint(arrowX - arrowHeadSize, arrowY + arrowLength);
      pg.addPoint(arrowX, arrowY + arrowLength + arrowHeadSize * 2);
      pg.addPoint(arrowX + arrowHeadSize, arrowY + arrowLength);
    }
      
    g.drawPolygon(pg);
    g.fillPolygon(pg);
  }
  
  /**
   * @author Erica Wheatcroft
   */
  void startObserving()
  {
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  void stopObserving()
  {
    HardwareObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  void rightPanelClearSensorStateBlocked()
  {
    _isRightPanelClearSensorBlocked = true;
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  void rightPanelClearSensorStateClear()
  {
    _isRightPanelClearSensorBlocked = false;
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  void leftPanelClearSensorStateBlocked()
  {
    _isLeftPanelClearSensorBlocked = true;
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  void leftPanelClearSensorStateClear()
  {
    _isLeftPanelClearSensorBlocked = false;
    repaint();
  }

   /**
   * @author Anthony Fong
   */
  void leftSafetyLevelOK()
  {
    _isXrayLeftSafetyLevelOK = true;
    repaint();
  }

  /**
   * @author Anthony Fong
   */
  void leftSafetyLevelNotOK()
  {
    _isXrayLeftSafetyLevelOK = false;
    repaint();
  }

   /**
   * @author Anthony Fong
   */
  void rightSafetyLevelOK()
  {
    _isXrayRightSafetyLevelOK = true;
    repaint();
  }

  /**
   * @author Anthony Fong
   */
  void rightSafetyLevelNotOK()
  {
    _isXrayRightSafetyLevelOK = false;
    repaint();
  }

  /**
   * @author Anthony Fong
   */
  void xrayCylinderHighMag()
  {
    _isUpToPosition1 = false;
    _isUpToPosition2 = false;
    _isHome = false;
    _isHighMag = true;
    repaint();
  }

  /**
   * @author Anthony Fong
   */
  void xrayCylinderUpToPosition1()
  {
    _isUpToPosition1 = true;
    _isUpToPosition2 = false;
    _isHome = false;
    _isHighMag = false;
    repaint();
  }
  
  /**
   * @author Anthony Fong
   */
  void xrayCylinderUpToPosition2()
  {
    _isUpToPosition1 = false;
    _isUpToPosition2 = true;
    _isHome = false;
    _isHighMag = false;
    repaint();
  }
  
  /**
   * @author Ngie Xing
   */
  void xrayCylinderHome()
  {
    _isUpToPosition1 = false;
    _isUpToPosition2 = false;
    _isHome = true;
    _isHighMag = false;
    repaint();
  }
  
  /**
   * @author Swee Yee Wong
   */
  void motorizedHeightLevelSensorUp()
  {
    _isSafetySensorUp = true;
    repaint();
  }

  /**
   * @author Swee Yee Wong
   */
  void motorizedHeightLevelSensorDown()
  {
    _isSafetySensorUp = false;
    repaint();
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();

            if (eventEnum instanceof InnerBarrierEventEnum)
            {
              if ((event.isStart() == false) && (eventEnum.equals(InnerBarrierEventEnum.BARRIER_OPEN)))
              {
                _innerBarrierOpen = true;
              }
              else if ((event.isStart() == false) && eventEnum.equals(InnerBarrierEventEnum.BARRIER_CLOSE))
              {
                _innerBarrierOpen = false;
              }
              XrayTesterImagePanel.this.repaint();
            }
            else if (eventEnum instanceof RightOuterBarrierEventEnum)
            {
              if ((event.isStart() == false) && (eventEnum.equals(RightOuterBarrierEventEnum.BARRIER_CLOSE)))
              {
                _rightOuterBarrierOpen = false;
              }
              else if ((event.isStart() == false) && eventEnum.equals(RightOuterBarrierEventEnum.BARRIER_OPEN))
              {
                _rightOuterBarrierOpen = true;
              }
              XrayTesterImagePanel.this.repaint();
            }
            else if (eventEnum instanceof LeftOuterBarrierEventEnum)
            {
              if ((event.isStart() == false) && (eventEnum.equals(LeftOuterBarrierEventEnum.BARRIER_CLOSE)))
              {
                _leftOuterBarrierOpen = false;
              }
              else if ((event.isStart() == false) && eventEnum.equals(LeftOuterBarrierEventEnum.BARRIER_OPEN))
              {
                _leftOuterBarrierOpen = true;
              }
              XrayTesterImagePanel.this.repaint();
            }
            else if (eventEnum instanceof StageBeltsEventEnum)
            {
              if ((event.isStart() == false) && (eventEnum.equals(StageBeltsEventEnum.BELTS_DIRECTION_LEFT_TO_RIGHT)))
              {
                _beltDirectionForward = true;
              }
              else if ((event.isStart() == false) && eventEnum.equals(StageBeltsEventEnum.BELTS_DIRECTION_RIGHT_TO_LEFT))
              {
                _beltDirectionForward = false;
              }
              XrayTesterImagePanel.this.repaint();
            }
            else if (eventEnum instanceof XrayCylinderEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(XrayCylinderEventEnum.XRAY_CYLINDER_UP)))
              {
                _isHighMag = false;
              }
              else if(event.isStart() == false && (eventEnum.equals(XrayCylinderEventEnum.XRAY_CYLINDER_DOWN)))
              {
                _isHighMag = true;
              }
              XrayTesterImagePanel.this.repaint();
            }
          }
        }
      }
    });
  }
 
  /**
   * @author Ngie Xing
   */
  public void forceRepaint()
  {
    XrayTesterImagePanel.this.repaint();
  }

  /**
   * @author Ngie Xing
   */
  public void setInnerBarrierState(boolean isInnerBarrierOpen)
  {
    _innerBarrierOpen = isInnerBarrierOpen;
  }

  /**
   * @author Ngie Xing
   */
  public void setRightOuterBarrierState(boolean rightOuterBarrierOpen)
  {
    _rightOuterBarrierOpen = rightOuterBarrierOpen;
  }

  /**
   * @author Ngie Xing
   */
  public void setLeftOuterBarrierState(boolean leftOuterBarrierOpen)
  {
    _leftOuterBarrierOpen = leftOuterBarrierOpen;
  }

  /**
   * @author Ngie Xing
   */
  public void setBeltDirectionState(boolean beltDirectionForward)
  {
    _beltDirectionForward = beltDirectionForward;
  }
}

