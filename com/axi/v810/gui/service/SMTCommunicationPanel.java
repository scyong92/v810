package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;


/**
 * This class will be responsible for display all SMEMA information to the user.
 *
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
class SMTCommunicationPanel extends JPanel implements TaskPanelInt, Observer
{
  private boolean _firstTimeVisible = false;

  private static final String _TRUE = StringLocalizer.keyToString("SERVICE_TRUE_KEY");
  private static final String _FALSE = StringLocalizer.keyToString("SERVICE_FALSE_KEY");

  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");

  // image of the SMT communication signals
  private ImageIcon _SMTCommImage;

  private JPanel _smemaPanel = new JPanel();
  private JPanel _pcaFlowImagePanel = new JPanel();
  private JPanel _testResultsPanel = new JPanel();

  private FlowLayout flowLayout3 = new FlowLayout();

  private JLabel _troDescriptionLabel = new JLabel();
  private JLabel _troStateLabel = new JLabel();
  private JLabel _triDescriptionLabel = new JLabel();
  private JLabel _triStateLabel = new JLabel();
  private JLabel _sroDescriptionLabel = new JLabel();
  private JLabel _sroStateLabel = new JLabel();
  private JLabel _sriDescriptionLabel = new JLabel();
  private JLabel _sriStateLabel = new JLabel();
  private JLabel _untestedPanelOutDescriptionLabel = new JLabel();
  private JLabel _untestedPanelOutStateLabel = new JLabel();
  private JLabel _goodPanelOutDescriptionLabel = new JLabel();
  private JLabel _goodPanelOutStateLabel = new JLabel();
  private JLabel _troLabel = new JLabel();
  private JLabel _triLabel = new JLabel();
  private JLabel _sriLabel = new JLabel();
  private JLabel _sroLabel = new JLabel();
  private JLabel _goodPanelOutBitStateLabel = new JLabel();
  private JLabel _untestedPanelOutBitStateLabel = new JLabel();
  private JLabel _pcaImageLabel = new JLabel();
  private JButton _troButton = new JButton();
  private JButton _sroButton = new JButton();
  private JButton _initializeButton = new JButton();
  private JRadioButton _untestedRadioButton = new JRadioButton();
  private JRadioButton _passedRadioButton = new JRadioButton();
  private JRadioButton _failedRadioButton = new JRadioButton();
  private Border _smemaBorder;
  private Border _resultBorder;
  private TitledBorder _smemaTitledBorder;
  private TitledBorder _resultTitledBorder;

  // static truth table data
  private Object columnNames[] =
      {StringLocalizer.keyToString("SERVICE_TEST_RESULT_TABLE_COLUMN_KEY"),
       StringLocalizer.keyToString("SERVICE_SMEMA_GPO_ABBR_KEY"),
       StringLocalizer.keyToString("SERVICE_SMEMA_UPO_ABBR_KEY")};

  private Object rowData[][] =
      {
      {StringLocalizer.keyToString("SERVICE_UNTESTED_KEY"), _FALSE, _TRUE},
      {StringLocalizer.keyToString("SERVICE_PASSED_KEY"), _TRUE, _FALSE},
      {StringLocalizer.keyToString("SERVICE_FAILED_KEY"), _FALSE, _FALSE}
  };

  private TruthTableModel tableModel = new TruthTableModel(rowData, columnNames);
  private JTable _truthTable = new JTable(tableModel);

  private SubsystemStatusAndControlTaskPanel _parent = null;

  private ManufacturingEquipmentInterface _manufacturingEquipmentInt = null;

  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
  private WorkerThread _monitoringThread = new WorkerThread("SMT Communication Monitor");

  private static final String _FALSE_STATE = StringLocalizer.keyToString("SERVICE_SMEMA_FALSE_STATE_KEY");
  private static final String _TRUE_STATE = StringLocalizer.keyToString("SERVICE_SMEMA_TRUE_STATE_KEY");

  private boolean _initialized = false;
  private boolean _monitoring = false;

  private ProgressDialog _progressDialog = null;


  // listeners
  private ActionListener _untestedButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _manufacturingEquipmentInt.setPanelOutUntested();
            updateTestResultsPanel();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _failedButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _manufacturingEquipmentInt.setPanelOutFailed();
            updateTestResultsPanel();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _passedButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _manufacturingEquipmentInt.setPanelOutPassed();
            updateTestResultsPanel();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _troButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _manufacturingEquipmentInt.setXrayTesterIsReadyToAcceptPanel(!_manufacturingEquipmentInt.
                isXrayTesterReadyToAcceptPanel());
            updateXrayTesterReadyToAcceptPanel();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
              }
            });
          }
        }
      });
    }
  };

  private ActionListener _sroButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _manufacturingEquipmentInt.setPanelFromXrayTesterAvailable(!_manufacturingEquipmentInt.
                isPanelFromXrayTesterAvailable());
            updatePanelFromXrayTesterAvailablePanel();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
              }
            });
          }
        }
      });
    }
  };

  private ComponentListener _componentListener = new ComponentListener()
  {
    public void componentHidden(ComponentEvent e)
    {}

    public void componentMoved(ComponentEvent e)
    {}

    public void componentResized(ComponentEvent e)
    {}

    public void componentShown(ComponentEvent e)
    {
      if (_firstTimeVisible == false)
      {
        _firstTimeVisible = true;
        createSmemaPanel();
        repaint();
        revalidate();
      }
    }
  };

  private ActionListener _intializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {

            // shutdown first
            DigitalIo.getInstance().shutdown();

            // startup first
            DigitalIo.getInstance().startup();

            _initialized = true;

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                // turn on the buttons
                enableButtons();

                // populate the UI
                populatePanel();

                startMonitoring();
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _initialized = false;
                handleException(xte);
                _progressDialog.setVisible(false);
               _progressDialog = null;
              }
            });
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  SMTCommunicationPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    // create the ui components
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+" - "+
                                         StringLocalizer.keyToString("SERVICE_UI_SSACTP_SMEMA_TAB_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZING_MSG_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }


  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    stopMonitoring();
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _parent.getSparkIcon().getIconWidth();
    int iconHeight = _parent.getSparkIcon().getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _parent.getSparkIcon().paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author Erica Wheatcroft
   */
  private synchronized void startMonitoring()
  {
    //Siew Yeng 
    //This is to check if monitor thread is already running. Run it only when it is not running to avoid creating extra thread.
    if(!_monitoring)
    {
      _monitoring = true;

      _monitoringThread.invokeLater(new Runnable()
      {
        public void run()
        {
          while(_monitoring)
          {
            updateXrayTesterReadyToAcceptPanel();
            updateDownStreamSystemReadyToAcceptPanel();
            updatePanelFromXrayTesterAvailablePanel();
            updatePanelFromUpstreamSystemAvailablePanel();
            try
            {
              Thread.sleep(500);
            }
            catch (InterruptedException ie)
            {
              // do nothing
            }
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitoring = false;
  }


  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    ProgressObservable.getInstance().addObserver(this);
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    ProgressObservable.getInstance().deleteObserver(this);
    HardwareObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setBorder(BorderFactory.createEmptyBorder(30,30,30,30));
    setLayout(new BorderLayout());

    _SMTCommImage = Image5DX.getImageIcon(Image5DX.HWSTATUS_SMT_COMM);
    _smemaBorder = new EtchedBorder(EtchedBorder.RAISED, Color.white, new java.awt.Color(134, 134, 134));
    _smemaTitledBorder = new TitledBorder(_smemaBorder, StringLocalizer.keyToString("SERVICE_SMEMA_KEY"));
    _resultBorder = new EtchedBorder(EtchedBorder.RAISED, Color.white, new java.awt.Color(134, 134, 134));
    _resultTitledBorder = new TitledBorder(_resultBorder, StringLocalizer.keyToString("SERVICE_XRAY_MACHINE_TEST_RESULTS_KEY"));

    // smema panel controls
    _troDescriptionLabel.setFont(FontUtil.getBiggerFont(_troDescriptionLabel.getFont(),Localization.getLocale()));
    _troDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _troDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_SMEMA_XRAY_MACHINE_READY_ACCEPT_KEY"));
    _troStateLabel.setFont(FontUtil.getBiggerFont(_troStateLabel.getFont(),Localization.getLocale()));
    _troStateLabel.setText(_FALSE_STATE);
    _triDescriptionLabel.setFont(FontUtil.getBiggerFont(_triDescriptionLabel.getFont(),Localization.getLocale()));
    _triDescriptionLabel.setPreferredSize(new Dimension(275, 22));
    _triDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _triDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_DOWNSTREAM_READY_TO_ACCEPT_KEY"));
    _triStateLabel.setFont(FontUtil.getBiggerFont(_triStateLabel.getFont(),Localization.getLocale()));
    _triStateLabel.setText(_FALSE_STATE);
    _troButton.setText(_TRUE);
    _sroDescriptionLabel.setFont(FontUtil.getBiggerFont(_sroDescriptionLabel.getFont(),Localization.getLocale()));
    _sroDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _sroDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_PANEL_FROM_XRAY_MACHINE_READY_KEY"));
    _sroStateLabel.setFont(FontUtil.getBiggerFont(_sroStateLabel.getFont(),Localization.getLocale()));
    _sroStateLabel.setText(_FALSE_STATE);
    _sriDescriptionLabel.setFont(FontUtil.getBiggerFont(_sriDescriptionLabel.getFont(),Localization.getLocale()));
    _sriDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_PANEL_FROM_UPSTREAM_KEY"));
    _sriStateLabel.setFont(FontUtil.getBiggerFont(_sriStateLabel.getFont(),Localization.getLocale()));
    _sriStateLabel.setText(_FALSE_STATE);
    _sroButton.setText(_TRUE);
    _troLabel.setFont(FontUtil.getBiggerFont(_troLabel.getFont(),Localization.getLocale()));
    _troLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _troLabel.setText(StringLocalizer.keyToString("SERVICE_SMEMA_TRO_KEY"));
    _triLabel.setFont(FontUtil.getBiggerFont(_triLabel.getFont(),Localization.getLocale()));
    _triLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _triLabel.setText(StringLocalizer.keyToString("SERVICE_SMEMA_TRI_KEY"));
    _sriLabel.setFont(FontUtil.getBiggerFont(_sriLabel.getFont(),Localization.getLocale()));
    _sriLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _sriLabel.setText(StringLocalizer.keyToString("SERVICE_SMEMA_SRI_KEY"));
    _sroLabel.setFont(FontUtil.getBiggerFont(_sroLabel.getFont(),Localization.getLocale()));
    _sroLabel.setText(StringLocalizer.keyToString("SERVICE_SMEMA_SRO_KEY"));
    _pcaFlowImagePanel.setLayout(flowLayout3);
    _pcaImageLabel.setIcon(_SMTCommImage);
    _pcaFlowImagePanel.setOpaque(false);
    _pcaFlowImagePanel.add(_pcaImageLabel);

    // into panel
    _untestedPanelOutDescriptionLabel.setFont(FontUtil.getBiggerFont(_untestedPanelOutDescriptionLabel.getFont(),Localization.getLocale()));
    _untestedPanelOutDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_UPO_KEY"));
    _untestedPanelOutStateLabel.setFont(FontUtil.getBiggerFont(_untestedPanelOutStateLabel.getFont(),Localization.getLocale()));
    _untestedPanelOutStateLabel.setText(_FALSE_STATE);
    _goodPanelOutDescriptionLabel.setFont(FontUtil.getBiggerFont(_goodPanelOutDescriptionLabel.getFont(),Localization.getLocale()));
    _goodPanelOutDescriptionLabel.setText(StringLocalizer.keyToString("SERVICE_GPO_KEY"));
    _goodPanelOutStateLabel.setFont(FontUtil.getBiggerFont(_goodPanelOutStateLabel.getFont(),Localization.getLocale()));
    _goodPanelOutStateLabel.setText(_FALSE_STATE);

    //XCR-3242, Smema tab does not populate all GUI properly for the first visit
    addComponentListener(_componentListener);
    createTestResultsPanel();

    // now create the initialize button panel
    JPanel initializedPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    initializedPanel.add(_initializeButton);

    add(initializedPanel, BorderLayout.NORTH);

    Box box = Box.createVerticalBox();
    box.add(_smemaPanel);
    box.add(Box.createVerticalStrut(15));
    box.add(Box.createVerticalGlue());
    box.add(_testResultsPanel);
    box.add(Box.createVerticalGlue());

    add(box, BorderLayout.WEST);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createTestResultsPanel()
  {
    JPanel testResultsPanel = new JPanel(new VerticalFlowLayout(1,5));
    Box testResultsPanelBox = Box.createVerticalBox();
    testResultsPanelBox.add(Box.createVerticalStrut(25));

    JPanel radioButtonPanel = new JPanel(new GridLayout(1,3));
    JPanel passedRadioButtonPanel = new JPanel(new FlowLayout());
    passedRadioButtonPanel.add(_passedRadioButton);
    _passedRadioButton.setText(StringLocalizer.keyToString("SERVICE_SET_GPO_TO_KEY")+" " + StringLocalizer.keyToString("SERVICE_PASSED_KEY"));
    radioButtonPanel.add(passedRadioButtonPanel);

    JPanel failedRadioButtonPanel = new JPanel(new FlowLayout());
    failedRadioButtonPanel.add(_failedRadioButton);
    _failedRadioButton.setText(StringLocalizer.keyToString("SERVICE_SET_GPO_TO_KEY")+" " + StringLocalizer.keyToString("SERVICE_FAILED_KEY"));
    radioButtonPanel.add(failedRadioButtonPanel);

    JPanel untestedRadioButtonPanel = new JPanel(new FlowLayout());
    untestedRadioButtonPanel.add(_untestedRadioButton);
    _untestedRadioButton.setText(StringLocalizer.keyToString("SERVICE_SET_UPO_TO_KEY")+" " + StringLocalizer.keyToString("SERVICE_UNTESTED_KEY"));
    radioButtonPanel.add(untestedRadioButtonPanel);

    ButtonGroup group = new ButtonGroup();
    group.add(_passedRadioButton);
    group.add(_failedRadioButton);
    group.add(_untestedRadioButton);

    testResultsPanelBox.add(radioButtonPanel);

    // create the truth table panel
    JPanel truthTablePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JScrollPane truthTableScrollPane = new JScrollPane();
    truthTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
    truthTableScrollPane.setOpaque(false);
    truthTablePanel.setOpaque(false);
    truthTablePanel.add(truthTableScrollPane, null);
    truthTableScrollPane.getViewport().add(_truthTable, null);
    _truthTable.getTableHeader().setResizingAllowed(false);
    _truthTable.getTableHeader().setReorderingAllowed(false);

    // set up the scroll pane and the table size so we don't have a bunch of blank space if not needed.
    int origTableWidth = _truthTable.getMinimumSize().width;
    int padding = 175;
    int scrollHeight = truthTableScrollPane.getPreferredSize().height;
    int tableHeight = _truthTable.getPreferredSize().height;
    int prefHeight = scrollHeight;
    if (tableHeight < scrollHeight)
    {
      prefHeight = tableHeight;
      Dimension prefScrollSize = new Dimension(origTableWidth + padding, prefHeight);
      _truthTable.setPreferredScrollableViewportSize(prefScrollSize);
    }
    ColumnResizer.adjustColumnPreferredWidths(_truthTable, true);

    testResultsPanelBox.add(truthTablePanel);
    testResultsPanel.add(testResultsPanelBox);

    _testResultsPanel = new JPanel();
    _testResultsPanel.add(testResultsPanel);
    _testResultsPanel.setBorder(_resultTitledBorder);
  }

  /**
   * this method will create the smema panel.
   */
  private void createSmemaPanel()
  {
    _smemaPanel.setLayout(new GridLayout());
    JPanel smemaHeaderPanel = new JPanel(new VerticalFlowLayout());
    // this panel has 4 innner boxes. All components within this panel will need to be display as a grid. we want to take
    // into account the text displayed (no matter the language) and add the appropriate strut space between components
    // within each box.The GRID will consist of 4 columns. The last column will have a button on some panels and blank
    // on other panels.

    int minimumStrutSizeInPixels = 40;

    // okay lets create our box components.there will be a box for each panel.
    Box troBox = Box.createHorizontalBox();
    Box triBox = Box.createHorizontalBox();
    Box sroBox = Box.createHorizontalBox();
    Box sriBox = Box.createHorizontalBox();

    // okay lets create the first column.
    // first we need to figure out the length in pixels of the labels
    int troPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_troLabel.getFont(), _troLabel.getText());
    int triPanelTextSizeInPixels = calculatePixelSizeFromTextSize( _triLabel.getFont(), _triLabel.getText());
    int sroPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sroLabel.getFont(), _sroLabel.getText());
    int sriPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sriLabel.getFont(), _sriLabel.getText());

    // now lets figure out the max of the text size.
    int columnOneMaxSizeInPixels = ArrayUtil.max(new int[] {troPanelTextSizeInPixels,
                                                            triPanelTextSizeInPixels,
                                                            sroPanelTextSizeInPixels,
                                                            sriPanelTextSizeInPixels}) + minimumStrutSizeInPixels;

    // now lets figure out what the difference is between the text size and our column size.
    int troPanelDifference = columnOneMaxSizeInPixels - troPanelTextSizeInPixels;
    int triPanelDifference = columnOneMaxSizeInPixels - triPanelTextSizeInPixels;
    int sroPanelDifference = columnOneMaxSizeInPixels - sroPanelTextSizeInPixels;
    int sriPanelDifference = columnOneMaxSizeInPixels - sriPanelTextSizeInPixels;

    // now lets take the max of our difference and the minimum strut size this will determine what our strut size should be
    int troPanelStrutSize = java.lang.Math.max(troPanelDifference, minimumStrutSizeInPixels);
    int triPanelStrutSize = java.lang.Math.max(triPanelDifference, minimumStrutSizeInPixels);
    int sroPanelStrutSize = java.lang.Math.max(sroPanelDifference, minimumStrutSizeInPixels);
    int sriPanelStrutSize = java.lang.Math.max(sriPanelDifference, minimumStrutSizeInPixels);

    // now lets add our components to their box and the calculated strut size.
    troBox.add(_troLabel);
    troBox.add(Box.createHorizontalStrut(troPanelStrutSize));
    triBox.add(_triLabel);
    triBox.add(Box.createHorizontalStrut(triPanelStrutSize));
    sroBox.add(_sroLabel);
    sroBox.add(Box.createHorizontalStrut(sroPanelStrutSize));
    sriBox.add(_sriLabel);
    sriBox.add(Box.createHorizontalStrut(sriPanelStrutSize));

    // now lets create the second column

    troPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_troDescriptionLabel.getFont(), _troDescriptionLabel.getText());
    triPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_triDescriptionLabel.getFont(), _triDescriptionLabel.getText());
    sroPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sroDescriptionLabel.getFont(), _sroDescriptionLabel.getText());
    sriPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sriDescriptionLabel.getFont(), _sriDescriptionLabel.getText());

    // now lets figure out the max of the text size.
    int columnTwoMaxSizeInPixels = ArrayUtil.max(new int[] {troPanelTextSizeInPixels,
                                                            triPanelTextSizeInPixels,
                                                            sroPanelTextSizeInPixels,
                                                            sriPanelTextSizeInPixels}) + minimumStrutSizeInPixels;

    // now lets figure out what the difference is between the text size and our column size.
    troPanelDifference = columnTwoMaxSizeInPixels - troPanelTextSizeInPixels;
    triPanelDifference = columnTwoMaxSizeInPixels - triPanelTextSizeInPixels;
    sroPanelDifference = columnTwoMaxSizeInPixels - sroPanelTextSizeInPixels;
    sriPanelDifference = columnTwoMaxSizeInPixels - sriPanelTextSizeInPixels;

    // now lets take the max of our difference and the minimum strut size this will determine what our strut size should be
    troPanelStrutSize = java.lang.Math.max(troPanelDifference, minimumStrutSizeInPixels);
    triPanelStrutSize = java.lang.Math.max(triPanelDifference, minimumStrutSizeInPixels);
    sroPanelStrutSize = java.lang.Math.max(sroPanelDifference, minimumStrutSizeInPixels);
    sriPanelStrutSize = java.lang.Math.max(sriPanelDifference, minimumStrutSizeInPixels);

    // now lets add our components to their box and the calculated strut size.
    troBox.add(_troDescriptionLabel);
    troBox.add(Box.createHorizontalStrut(troPanelStrutSize));
    triBox.add(_triDescriptionLabel);
    triBox.add(Box.createHorizontalStrut(triPanelStrutSize));
    sroBox.add(_sroDescriptionLabel);
    sroBox.add(Box.createHorizontalStrut(sroPanelStrutSize));
    sriBox.add(_sriDescriptionLabel);
    sriBox.add(Box.createHorizontalStrut(sriPanelStrutSize));

    // now lets create the third column
    troPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_troStateLabel.getFont(), _troStateLabel.getText());
    triPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_triStateLabel.getFont(), _triStateLabel.getText());
    sroPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sroStateLabel.getFont(), _sroStateLabel.getText());
    sriPanelTextSizeInPixels = calculatePixelSizeFromTextSize(_sriStateLabel.getFont(), _sriStateLabel.getText());

    // now lets figure out the max of the text size.
    int columnThreeMaxSizeInPixels = ArrayUtil.max(new int[]
                                                 {troPanelTextSizeInPixels,
                                                 triPanelTextSizeInPixels,
                                                 sroPanelTextSizeInPixels,
                                                 sriPanelTextSizeInPixels}) + minimumStrutSizeInPixels;

    // now lets figure out what the difference is between the text size and our column size.
    troPanelDifference = columnThreeMaxSizeInPixels - troPanelTextSizeInPixels;
    triPanelDifference = columnThreeMaxSizeInPixels - triPanelTextSizeInPixels;
    sroPanelDifference = columnThreeMaxSizeInPixels - sroPanelTextSizeInPixels;
    sriPanelDifference = columnThreeMaxSizeInPixels - sriPanelTextSizeInPixels;

    // now lets take the max of our difference and the minimum strut size this will determine what our strut size should be
    troPanelStrutSize = java.lang.Math.max(troPanelDifference, minimumStrutSizeInPixels);
    triPanelStrutSize = java.lang.Math.max(triPanelDifference, minimumStrutSizeInPixels);
    sroPanelStrutSize = java.lang.Math.max(sroPanelDifference, minimumStrutSizeInPixels);
    sriPanelStrutSize = java.lang.Math.max(sriPanelDifference, minimumStrutSizeInPixels);

    // now lets add our components to their box and the calculated strut size.
    troBox.add(_troStateLabel);
    troBox.add(Box.createHorizontalStrut(troPanelStrutSize));
    triBox.add(_triStateLabel);
    triBox.add(Box.createHorizontalStrut(triPanelStrutSize));
    sroBox.add(_sroStateLabel);
    sroBox.add(Box.createHorizontalStrut(sroPanelStrutSize));
    sriBox.add(_sriStateLabel);
    sriBox.add(Box.createHorizontalStrut(sriPanelStrutSize));

    // now lets add our components to their box
    troBox.add(_troButton);
    sroBox.add(_sroButton);

    // now add the boxes to the smema panel header
    smemaHeaderPanel.add(troBox);
    smemaHeaderPanel.add(triBox);
    smemaHeaderPanel.add(sroBox);
    smemaHeaderPanel.add(sriBox);

    // now lets create the smema panel

    Box smemaPanelBox = Box.createVerticalBox();
    smemaPanelBox.add(smemaHeaderPanel);
    smemaPanelBox.add(Box.createVerticalGlue());
    smemaPanelBox.add(_pcaFlowImagePanel);

    _smemaPanel.add(smemaPanelBox, BorderLayout.CENTER);
    _smemaPanel.setBorder(_smemaTitledBorder);
    _smemaPanel.setOpaque(false);

  }

  /**
   * @author Erica Wheatcroft
   */
  private int calculatePixelSizeFromTextSize(Font font, String text)
  {
    Assert.expect(font != null, "font is null");
    Assert.expect(text != null, "text is null");

    Graphics2D graphics = (Graphics2D) this.getGraphics();
    Assert.expect(graphics != null, "graphics is null");

    java.awt.font.FontRenderContext frc = graphics.getFontRenderContext();
    Assert.expect(frc != null, "frc is null");

    java.awt.geom.Rectangle2D rectable2d = font.getStringBounds(text, frc);
    Assert.expect(rectable2d != null, "rectable2d is null");

    return (int)rectable2d.getWidth();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get an SMEMA instance
    _manufacturingEquipmentInt = ManufacturingEquipmentInterface.getInstance();

    // now see if digitial io needs to be started up since its required for SMEMA
    try
    {
      _initialized = ! DigitalIo.getInstance().isStartupRequired();
    }
    catch(XrayTesterException xte)
    {
      handleException(xte);
    }

    // enable the buttons if the system is initialized.
    enableButtons();

    // populate the UI
    populatePanel();

    // add the listeners
    addListeners();

    startObserving();

    if(_initialized)
      startMonitoring();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void enableButtons()
  {
    _initializeButton.setEnabled(true);
    if(_initialized)
    {
      _troButton.setEnabled(true);
      _sroButton.setEnabled(true);
      _untestedRadioButton.setEnabled(true);
      _passedRadioButton.setEnabled(true);
      _failedRadioButton.setEnabled(true);
    }
    else
    {
      _troButton.setEnabled(false);
      _sroButton.setEnabled(false);
      _untestedRadioButton.setEnabled(false);
      _passedRadioButton.setEnabled(false);
      _failedRadioButton.setEnabled(false);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    stopMonitoring();

    removeListeners();

    stopObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateXrayTesterReadyToAcceptPanel()
  {
    if(_initialized == false)
    {
      _troStateLabel.setText(_UNKNOWN);
      _troButton.setText(_UNKNOWN);
      return;
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try

        {
          if (_manufacturingEquipmentInt.isXrayTesterReadyToAcceptPanel())
          {
            _troStateLabel.setText(_TRUE_STATE);
            _troButton.setText(_FALSE);
          }
          else
          {
            _troStateLabel.setText(_FALSE_STATE);
            _troButton.setText(_TRUE);
          }
        }
        catch(XrayTesterException xte)
        {
          handleException(xte);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateDownStreamSystemReadyToAcceptPanel()
  {
    if(_initialized == false)
    {
      _triStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_manufacturingEquipmentInt.isDownstreamSystemReadyToAcceptPanel())
        _triStateLabel.setText(_TRUE_STATE);
      else
        _triStateLabel.setText(_FALSE_STATE);
    }
    catch(XrayTesterException xte)
    {
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updatePanelFromXrayTesterAvailablePanel()
  {
    if(_initialized == false)
    {
      _sroStateLabel.setText(_UNKNOWN);
      _sroButton.setText(_UNKNOWN);
      return;
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (_manufacturingEquipmentInt.isPanelFromXrayTesterAvailable())
          {
            _sroStateLabel.setText(_TRUE_STATE);
            _sroButton.setText(_FALSE);
          }
          else
          {
            _sroStateLabel.setText(_FALSE_STATE);
            _sroButton.setText(_TRUE);
          }
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updatePanelFromUpstreamSystemAvailablePanel()
  {
    if(_initialized == false)
    {
      _sriStateLabel.setText(_UNKNOWN);
      return;
    }

    try
    {
      if (_manufacturingEquipmentInt.isPanelFromUpstreamSystemAvailable())
        _sriStateLabel.setText(_TRUE_STATE);
      else
        _sriStateLabel.setText(_FALSE_STATE);
    }
    catch(XrayTesterException xte)
    {
      handleException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    updateXrayTesterReadyToAcceptPanel();
    updateDownStreamSystemReadyToAcceptPanel();
    updatePanelFromXrayTesterAvailablePanel();
    updatePanelFromUpstreamSystemAvailablePanel();
    updateTestResultsPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateTestResultsPanel()
  {
    if(_initialized == false)
    {
      _goodPanelOutBitStateLabel.setText(_UNKNOWN);
      _untestedPanelOutBitStateLabel.setText(_UNKNOWN);
      return;
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (_manufacturingEquipmentInt.isPanelOutPass())
            _goodPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_GPO_KEY") + " " + _TRUE_STATE);

          if (_manufacturingEquipmentInt.isPanelOutFail())
            _goodPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_GPO_KEY") + " " + _FALSE_STATE);

          if (_manufacturingEquipmentInt.isPanelOutUntested())
            _untestedPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_UPO_KEY") + " " + _TRUE_STATE);
          else
            _untestedPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_UPO_KEY") + " " + _FALSE_STATE);
        }
        catch (XrayTesterException xte)
        {
          _goodPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_GPO_KEY") + " " + "is unknown");
          _untestedPanelOutBitStateLabel.setText(StringLocalizer.keyToString("SERVICE_UPO_KEY") + " " + "is unknown");
          handleException(xte);
        }
      }
    });
  }


  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_intializeButtonActionListener);
    _passedRadioButton.addActionListener(_passedButtonActionListener);
    _failedRadioButton.addActionListener(_failedButtonActionListener);
    _untestedRadioButton.addActionListener(_untestedButtonActionListener);
    _troButton.addActionListener(_troButtonActionListener);
    _sroButton.addActionListener(_sroButtonActionListener);
    //addComponentListener(_componentListener);//Mini: Solve SMEMA no image on 1st time issue
  }


  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_intializeButtonActionListener);
    _passedRadioButton.removeActionListener(_passedButtonActionListener);
    _failedRadioButton.removeActionListener(_failedButtonActionListener);
    _untestedRadioButton.removeActionListener(_untestedButtonActionListener);
    _troButton.removeActionListener(_troButtonActionListener);
    _sroButton.removeActionListener(_sroButtonActionListener);
  }

  /**
   * @author Andy Mechtenberg
   */
  class TruthTableModel extends DefaultTableModel
  {
    TruthTableModel(Object[][] data, Object[] colNames)
    {
      super(data, colNames);
    }

    public boolean isCellEditable(int row, int col)
    {
      return false;
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof DigitalIoEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(DigitalIoEventEnum.INITIALIZE))
                _progressDialog.updateProgressBarPrecent(100);
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }

}
