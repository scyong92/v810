package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.Config;

/**
 * @author Erica Wheatcroft
 */
public class ImagingChainPanel extends JPanel implements TaskPanelInt
{
  private SubsystemStatusAndControlTaskPanel _parent = null;
  private MainMenuGui _mainUI = null;
  private JButton _rebootAllIPRHardwareButton = new JButton();
  private JButton _resetAllIRPsButton = new JButton();
  private JButton _stopAllIRPsButton = new JButton();
  private JButton _restartIRPsInDiagnosticModeButton = new JButton();

  private JButton _rebootRMSHardwareButton = new JButton();
  private JButton _resetRMSButton = new JButton();
  private JButton _stopRMSButton = new JButton();
  private JButton _upgradeRMSButton = new JButton();

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private ActionListener _rebootAllIRPsButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_REBOOTING_IRPS_KEY"),
         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            ImageReconstructionProcessorManager.getInstance().rebootImageReconstructionProcessors();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };

  private ActionListener _resetAllIRPsButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_RESTARTING_IRPS_KEY"),
          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            ImageReconstructionProcessorManager.getInstance().restartImageReconstructionProcessors();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };

  private ActionListener _restartIRPsInDiagnosticModeButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      String message = "This is intended for ViTox Use Only. This feature will cause system performance to be very slow."+
                       "Would you like to continue?";
      int result = JOptionPane.showConfirmDialog(MainMenuGui.getInstance(),
                                                 StringUtil.format(message, 50),
                                                 StringLocalizer.keyToString("Are you sure?"),
                                                 JOptionPane.WARNING_MESSAGE,
                                                 JOptionPane.YES_NO_OPTION);

      if (result != JOptionPane.YES_OPTION)
        return;


      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
                                                                             StringLocalizer.keyToString("SERVICE_RESTARTING_IRPS_KEY"),
                                                                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                                                             true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            ImageReconstructionProcessorManager.getInstance().restartImageReconstructionProcessorsWithSaveProjections();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };

  private ActionListener _stopAllIRPsButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_STOPPING_IRPS_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            ImageReconstructionProcessorManager.getInstance().stopImageReconstructionProcessors();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("SERVICE_IRPS_HAVE_BEEN_STOPPED_KEY"),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

    }
  };

  private ActionListener _rebootRMSButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_REBOOTING_RMS_KEY"),
         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            RemoteMotionServerExecution.getInstance().rebootRemoteMotionServer();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };

  private ActionListener _resetRMSButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_RESTARTING_RMS_KEY"),
          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            RemoteMotionServerExecution.getInstance().startRemoteMotionServer();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };

  private ActionListener _stopRMSButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_STOPPING_RMS_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            RemoteMotionServerExecution.getInstance().stopRemoteMotionServer();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("SERVICE_RMS_HAVE_BEEN_STOPPED_KEY"),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

    }
  };

  private ActionListener _upgradeRMSButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_UPGRADING_RMS_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            RemoteMotionServerExecution.getInstance().updateApplicationFiles();
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("SERVICE_RMS_HAVE_BEEN_UPGRADED_KEY"),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

    }
  };

  /**
   * @author Erica Wheatcroft
   */
  public ImagingChainPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent!=null);
    _parent = parent;

    _mainUI = MainMenuGui.getInstance();
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    addListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    removeListeners();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _rebootAllIPRHardwareButton.addActionListener(_rebootAllIRPsButtonActionPerformed);
    _restartIRPsInDiagnosticModeButton.addActionListener(_restartIRPsInDiagnosticModeButtonActionPerformed);
    _resetAllIRPsButton.addActionListener(_resetAllIRPsButtonActionPerformed);
    _stopAllIRPsButton.addActionListener(_stopAllIRPsButtonActionPerformed);

    _rebootRMSHardwareButton.addActionListener(_rebootRMSButtonActionPerformed);
    _resetRMSButton.addActionListener(_resetRMSButtonActionPerformed);
    _stopRMSButton.addActionListener(_stopRMSButtonActionPerformed);
    _upgradeRMSButton.addActionListener(_upgradeRMSButtonActionPerformed);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _rebootAllIPRHardwareButton.removeActionListener(_rebootAllIRPsButtonActionPerformed);
    _restartIRPsInDiagnosticModeButton.removeActionListener(_restartIRPsInDiagnosticModeButtonActionPerformed);
    _resetAllIRPsButton.removeActionListener(_resetAllIRPsButtonActionPerformed);
    _stopAllIRPsButton.removeActionListener(_stopAllIRPsButtonActionPerformed);

    _rebootRMSHardwareButton.removeActionListener(_rebootRMSButtonActionPerformed);
    _resetRMSButton.removeActionListener(_resetRMSButtonActionPerformed);
    _stopRMSButton.removeActionListener(_stopRMSButtonActionPerformed);
    _upgradeRMSButton.removeActionListener(_upgradeRMSButtonActionPerformed);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
    setLayout(new BorderLayout());

    JPanel northPanel = new JPanel(new FlowLayout());

    JPanel innerIRPPanel = new JPanel();
    innerIRPPanel.setBorder( BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(148, 145, 140)), StringLocalizer.keyToString("ACD_IMAGE_RECONSTRUCTION_PROCESSOR_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    innerIRPPanel.setLayout(new FlowLayout());
    _rebootAllIPRHardwareButton.setText(StringLocalizer.keyToString("SERVICE_REBOOT_ALL_IRPS_KEY"));
    _resetAllIRPsButton.setText(StringLocalizer.keyToString("SERVICE_RESTART_ALL_IRPS_KEY"));
    _stopAllIRPsButton.setText(StringLocalizer.keyToString("SERVICE_STOP_ALL_IRPS_KEY"));
    _restartIRPsInDiagnosticModeButton.setText("Restart All IRPs in Slow Mode");
    innerIRPPanel.add(_stopAllIRPsButton);
    innerIRPPanel.add(_resetAllIRPsButton);
    if (Config.isDeveloperDebugModeOn())
      innerIRPPanel.add(_restartIRPsInDiagnosticModeButton);
    
    // XCR 3640 - Remove "Reboot All IRPs " button from Service tab
    // to check whether the system is series 1 system or series 2 system
    // if is series 1 system, the reboot buttom will be shown in IRP panel
    if (Config.is64bitIrp() == false)
    {
      innerIRPPanel.add(_rebootAllIPRHardwareButton);
    }
     
    JPanel innerRMSPanel = new JPanel();
    innerRMSPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(148, 145, 140)), StringLocalizer.keyToString("ACD_REMOTE_MOTION_SERVER_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    innerRMSPanel.setLayout(new FlowLayout());
    _rebootRMSHardwareButton.setText(StringLocalizer.keyToString("SERVICE_REBOOT_RMS_KEY"));
    _resetRMSButton.setText(StringLocalizer.keyToString("SERVICE_RESTART_RMS_KEY"));
    _stopRMSButton.setText(StringLocalizer.keyToString("SERVICE_STOP_RMS_KEY"));
    _upgradeRMSButton.setText(StringLocalizer.keyToString("SERVICE_UPGRADE_RMS_KEY"));
    innerRMSPanel.add(_rebootRMSHardwareButton);
    innerRMSPanel.add(_resetRMSButton);
    innerRMSPanel.add(_stopRMSButton);
    innerRMSPanel.add(_upgradeRMSButton);

    JPanel comminicationPanel = new JPanel(new VerticalFlowLayout(1, 5));
    comminicationPanel.add(innerIRPPanel);
    comminicationPanel.add(innerRMSPanel);
    northPanel.add(comminicationPanel);
    add(northPanel, BorderLayout.NORTH);
  }
}
