package com.axi.v810.gui.service;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for drawing a digital io input bit as a label.
 *
 * @author Erica Wheatcroft
 */
class DigitalIoBitLabel extends JLabel
{
  private ImageIcon _onStateIcon = null;
  private ImageIcon _offStateIcon = null;
  private ImageIcon _errorOccurredStateIcon = null;
  private String _bitName = null;
  private DigitalIo _digitalIo = null;
  private boolean _on = false;

  /**
   * @author Erica Wheatcroft
   */
  private DigitalIoBitLabel()
  {
    // do nothing.. not supported
  }

  /**
   * @author Erica WHeatcroft
   */
  DigitalIoBitLabel(String name, DigitalIo digitalIo)
  {
    Assert.expect(name != null);
    Assert.expect(digitalIo != null);

    _onStateIcon = Image5DX.getImageIcon(Image5DX.ON_STATE);
    _offStateIcon = Image5DX.getImageIcon(Image5DX.OFF_STATE);
    _errorOccurredStateIcon = Image5DX.getImageIcon(Image5DX.ERROR_STATE);

    _bitName = name;
    _digitalIo = digitalIo;

    createLabel();
  }

  /**
   * @author Erica WHeatcroft
   */
  private void createLabel()
  {
    setText(_bitName);
    setFont(FontUtil.getBoldFont(getFont(),Localization.getLocale()));

//    try
//    {
//      _on = _digitalIo.getInputBitState(_bitName);
//    }
//    catch(XrayTesterException xte)
//    {
//      setIcon(_errorOccurredStateIcon);
//      return;
//    }
    
    //XCR1400 - Siew Yeng
    try
    {
      HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
      {
        public void run()
        {
          try
          {
            _on = _digitalIo.getInputBitState(_bitName);
          }
          catch(XrayTesterException xte)
          {
            setIcon(_errorOccurredStateIcon);
            return;
          }
        }
      });
    }
    catch(XrayTesterException e)
    {
      setIcon(_errorOccurredStateIcon);
      return;
    }
    //XCR1400 - end
    
    if (_on)
      setIcon(_onStateIcon);
    else
      setIcon(_offStateIcon);

    setHorizontalAlignment(SwingConstants.LEADING);
  }

  /**
   * @author Erica Wheatcroft
   */
  void refresh() throws XrayTesterException
  {
//    try
//    {
//      _on = _digitalIo.getInputBitState(_bitName);
//    }
//    catch (XrayTesterException xte)
//    {
//      setIcon(_errorOccurredStateIcon);
//      return;
//    }

    //XCR1400 - Siew Yeng
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        try
        {
          _on = _digitalIo.getInputBitState(_bitName);
        }
        catch(XrayTesterException xte)
        {
          setIcon(_errorOccurredStateIcon);
          throw xte;
        }
      }
    });
    //XCR1400 - end
    
    if (_on)
      setIcon(_onStateIcon);
    else
      setIcon(_offStateIcon);
  }
}
