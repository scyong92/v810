package com.axi.v810.gui.service;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for drawing a digital io output bit as a button
 *
 * @author Erica Wheatcroft
 */
class DigitalIoOutputBitButton extends JButton
{

  private String _bitName = null;
  private ImageIcon _onStateIcon = null;
  private ImageIcon _offStateIcon = null;
  private ImageIcon _errorOccurredStateIcon = null;
  private DigitalIo _digitalIo = null;
  private boolean _on = false;

  /**
   * @author Erica Wheatcroft
   */
  private DigitalIoOutputBitButton()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  DigitalIoOutputBitButton(String bitName, DigitalIo digitalIo)
  {
    super();

    Assert.expect(bitName != null);
    Assert.expect(digitalIo != null);

    _onStateIcon = Image5DX.getImageIcon(Image5DX.ON_STATE);
    _offStateIcon = Image5DX.getImageIcon(Image5DX.OFF_STATE);
    _errorOccurredStateIcon = Image5DX.getImageIcon(Image5DX.ERROR_STATE);

    _bitName = bitName;
    _digitalIo = digitalIo;

    createButton();
  }

  /**
   * @author Erica Wheatcroft
   */
  void createButton()
  {
    setText(_bitName);

    try
    {
      _on = _digitalIo.getOutputBitState(_bitName);
    }
    catch (XrayTesterException xte)
    {
      setIcon(_errorOccurredStateIcon);
      return;
    }

    if (_on)
    {
      setIcon(_onStateIcon);
    }
    else
    {
      setIcon(_offStateIcon);
    }

    setHorizontalAlignment(SwingConstants.LEADING);
    setToolTipText(_bitName);
  }

  /**
   * @author Erica Wheatcroft
   */
  void toggleState()
  {
    try
    {
//      if (XrayTester.isS2EXEnabled())
//      {
//        if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitAOutputBitNumber()
//          || _digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitBOutputBitNumber()
//          || _digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisEnableOutputBitNumber())
//        {
//          try
//          {
//            //if need to enable Z Axis for triggering move up and down
//            if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitAOutputBitNumber()
//              || _digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitBOutputBitNumber())
//            {
//              _digitalIo.enableZAxis();
//            }
//          }
//          catch (XrayTesterException xte)
//          {
//            String message = xte.getMessage();
//            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
//              StringUtil.format(message, 50),
//              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
//              JOptionPane.ERROR_MESSAGE);
//          }
//        }
//      }
      
      //This is required for Standard and XXL machine
      if (XrayCylinderActuator.getInstance().isInstalled())
      {
        if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getXrayCylinderDownOutputBitNumber())
        {
          try
          {

            _digitalIo.turnXrayCylinderUnClamp();
          }
          catch (XrayTesterException xte)
          {
            String message = xte.getMessage();
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringUtil.format(message, 50),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.ERROR_MESSAGE);
            return;
          }
        }
      }

      //XXL Based Anthony Jan 2013
      if (!_digitalIo.setOutputBitState(_bitName, !_on))
      {
        //Swee Yee
        //This is required for Standard, XXL and S2EX machine
        if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getStageBeltsOutputBitNumber())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("HW_STAGE_BELTS_NOT_ALLOWED_TO_TRIGGER_EXCEPTION_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.ERROR_MESSAGE);
          return;
        }
        else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getPanelClampOutputBitNumber() && _digitalIo.areStageBeltsOn())
        {
          JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
            StringLocalizer.keyToString("HW_PANEL_CLAMP_CYLINDER_NOT_ALLOWED_TO_TRIGGER_EXCEPTION_KEY"),
            StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
            JOptionPane.ERROR_MESSAGE);
          return;
        }
        //This is required for Standard, XXL and S2EX machine
        if (XrayCylinderActuator.getInstance().isInstalled())
        {

          if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getXrayCylinderDownOutputBitNumber())
          {
            //String message = "Not Safe to Trigger Xray Cylinder while Inner Barrier is Closed";
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("HW_XRAY_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
              //StringUtil.format(message, 50),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.ERROR_MESSAGE);
            return;
          }
          else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getInnerBarrierOutputBitNumber())
          {
            //String message = "Not Safe to Trigger Inner Barrier while Xray Cylinder is Down";
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
              StringLocalizer.keyToString("HW_INNER_BARRIER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
              //StringUtil.format(message, 50),
              StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
              JOptionPane.ERROR_MESSAGE);
            return;
          }
        }

        if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
        {
          if (XrayTester.isS2EXEnabled())
          {

            if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisEnableOutputBitNumber())
            {
              String message = "Not Safe to Enable Xray Z Axis Motor while Inner Barrier is Closed";
              System.out.print(message);
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_XRAY_Z_AXIS_NOT_SAFE_TO_ENABLE_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
            else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitAOutputBitNumber())
            {
              if (_digitalIo.isInnerBarrierClosed())
              {
                String message = "Not Safe to Trigger Xray Z Axis Motor while Inner Barrier is Closed";
                System.out.print(message);
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_XRAY_Z_AXIS_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
              else if (_digitalIo.isXrayZAxisEnabled() == false)
              {
                String message = "Not Allowed to Trigger Xray Z Axis Motor while Xray Z-Axis is not enabled";
                System.out.print(message);
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_XRAY_Z_AXIS_NOT_ALLOWED_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
            }
            else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getZAxisPositionBitBOutputBitNumber())
            {
              if (_digitalIo.isInnerBarrierClosed())
              {
                String message = "Not Safe to Trigger Xray Z Axis Motor while Inner Barrier is Closed";
                System.out.print(message);
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_XRAY_Z_AXIS_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
              else if (_digitalIo.isXrayZAxisEnabled() == false)
              {
                String message = "Not Allowed to Trigger Xray Z Axis Motor while Xray Z-Axis is not enabled";
                System.out.print(message);
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_XRAY_Z_AXIS_NOT_ALLOWED_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
            }
            else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getInnerBarrierOutputBitNumber())
            {
              String message = "Not Safe to Trigger Inner Barrier while Xray Z Axis Motor is not Home";
              System.out.print(message);
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_INNER_BARRIER_NOT_SAFE_TO_TRIGGER_FOR_Z_AXIS_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
          }

          if (DigitalIo.isOpticalStopControllerInstalled())
          {
            if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getPanelClampOutputBitNumber())
            {
              //String message =  "It is not  safe to trigger panel clamp while PIP is not fully retracted.
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_PANEL_CLAMP_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }

            if (_digitalIo.isOpticalStopControllerEndStopperInstalled())
            {
              if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getLeftPanelInPlaceSensorOutputBitNumber())
              {
                //String message =  "It is not safe to trigger left PIP cylinder while panel is clamped."
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_PANEL_IN_PLACE_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
              else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getRightPanelInPlaceSensorOutputBitNumber())
              {
                //String message =  "It is not safe to trigger right PIP cylinder while panel is clamped."
                JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                  StringLocalizer.keyToString("HW_PANEL_IN_PLACE_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                  //StringUtil.format(message, 50),
                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                  JOptionPane.ERROR_MESSAGE);
                return;
              }
            }
          }
          else
          {
            //For XXL or S2Ex to work without optical PIP and panel is clamped from bottom
            if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getPanelClampOutputBitNumber())
            {
              //String message =  "It is not  safe to trigger panel clamp while PIP is not fully retracted.
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_PANEL_CLAMP_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
            else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getLeftPanelInPlaceSensorOutputBitNumber())
            {
              //String message =  "It is not safe to trigger left PIP cylinder while panel is clamped."
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_PANEL_IN_PLACE_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
            else if (_digitalIo.getOutputBitFromKeyString(_bitName) == _digitalIo.getRightPanelInPlaceSensorOutputBitNumber())
            {
              //String message =  "It is not safe to trigger right PIP cylinder while panel is clamped."
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                StringLocalizer.keyToString("HW_PANEL_IN_PLACE_CYLINDER_NOT_SAFE_TO_TRIGGER_EXCEPTION_KEY"),
                //StringUtil.format(message, 50),
                StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                JOptionPane.ERROR_MESSAGE);
              return;
            }
          }
        }
        return; //Always return if IO trigger failed to prevent GUI update wrongly
      }

//      if (XrayCylinderActuator.getInstance().isInstalled() || XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
//      {
//        //OK proceeed
//      }
//      else
//      {
//        //Standard operation
//        _digitalIo.setOutputBitState(_bitName, !_on);
//      }
      _on = !_on;
    }
    catch (XrayTesterException xte)
    {
      String message = xte.getMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        StringUtil.format(message, 50),
        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
        JOptionPane.ERROR_MESSAGE);
      setIcon(_errorOccurredStateIcon);
      return;
    }

    if (_on)
    {
      setIcon(_onStateIcon);
    }
    else
    {
      setIcon(_offStateIcon);
    }
  }

  /**
   *
   */
  public void setONIcon()
  {
    setIcon(_onStateIcon);
  }

  /**
   *
   */
  public void setOFFIcon()
  {
    setIcon(_offStateIcon);
  }

  /**
   *
   */
  public void setERRORIcon()
  {
    setIcon(_errorOccurredStateIcon);
  }

  /**
   *
   */
  void monitorStateBasedOnInputBit()
  {
    try
    {
      if (_on != _digitalIo.getOutputBitState(_bitName))
      {
        if (_digitalIo.getOutputBitState(_bitName))
        {
          setIcon(_onStateIcon);
          _on = true;
          //return 1;
        }
        else
        {
          setIcon(_offStateIcon);
          _on = false;
          //return 0;
        }
      }
    }
    catch (XrayTesterException xte)
    {
      String message = xte.getMessage();
      JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
        StringUtil.format(message, 50),
        StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
        JOptionPane.ERROR_MESSAGE);
      setIcon(_errorOccurredStateIcon);
      //return -1;
    }

  }

  /**
   *
   * @return
   */
  public DigitalIo getDigitalIo()
  {
    return _digitalIo;
  }

  /**
   *
   * @return
   */
  public String getBitName()
  {
    return _bitName;
  }
}
