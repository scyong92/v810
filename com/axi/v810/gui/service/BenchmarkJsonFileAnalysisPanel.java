package com.axi.v810.gui.service;

import com.axi.guiUtil.DefaultSortTableModel;
import com.axi.guiUtil.FileFilterUtil;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;
import java.io.File;

/**
 * Benchmark Analysis will compare the system performance based on the timing
 * data collected during executing the CDA and Benchmark Task manually. User can
 * compare the system performance based on - system to system comparison or -
 * form time to time comparison
 *
 * @author Anthony Fong
 */
public class BenchmarkJsonFileAnalysisPanel
{

  JLabel _fileFromLabel = new JLabel(StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_FILE_FROM_KEY") + ":");
  JLabel _fileToLabel = new JLabel(StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_FILE_TO_KEY") + ":");
  JLabel _dummy1Label = new JLabel(StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_FILE_SAVE_KEY") + ":");
  JLabel _dummy2Label = new JLabel("");
  JTextField _fileFromPathSelectionTextField = new JTextField(20);
  JTextField _fileToPathSelectionTextField = new JTextField(20);
  JButton _fileFromPathSelectionButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_FILE_FROM_KEY") + "...");
  JButton _fileToPathSelectionButton = new JButton(StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_FILE_TO_KEY") + "...");
  JButton _analyzeButton = new JButton(StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TEST_RESULT_KEY"));
  JButton _saveButton = new JButton(StringLocalizer.keyToString("GUI_SAVE_CDNA_BENCHMARK_RESULT_KEY"));
  BenchmarkTableModel _benchmarkTableModel = null;
  JTable _benchmarkTable = null;
  JDialog _benchmarkFrame = null;


  /*
   * Note: Typically the main method will be in a separate class.
   * As this simple one class can be tested using Run File (Shift F6).
   * @author Anthony Fong
   */
  public static void main(String[] args)
  {
    //Use the event dispatch thread for Swing components 
    EventQueue.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        //Test under V810 Theme
        new BenchmarkJsonFileAnalysisPanel(MainMenuGui.getInstance(), BenchmarkTableModel.getColumnNames(), BenchmarkTableModel.getEmptyData());

        //Test under Standard Theme
        //new BenchmarkJsonFileAnalysisPanel(null, BenchmarkTableModel.getColumnNames(), BenchmarkTableModel.getEmptyData());
      }
    });
  }

  /*
   *
   * @author Anthony Fong
   */
  public BenchmarkJsonFileAnalysisPanel(Component mainFrame, String[] columnNames, Object[][] data)
  {
    //Create the JTable using the BenchmarkTableModel    
    _benchmarkTableModel = new BenchmarkTableModel(columnNames, data);
    _benchmarkTable = new JTable(_benchmarkTableModel)
    {
      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
      {
        //System.out.println("row" + row + " column=" + column);
        Component c = super.prepareRenderer(renderer, row, column);
        if (isCellSelected(row, column) == false)
        {
          c.setBackground(colorForRow(row));
          c.setForeground(UIManager.getColor("Table.foreground"));
        }
        else
        {
          c.setBackground(new Color(181, 228, 255));
          c.setForeground(UIManager.getColor("Table.selectionForeground"));
        }
        return c;
      }

      /**
       * Returns the alternate background color for the given row.
       */
      protected Color colorForRow(int row)
      {

        if (_benchmarkTableModel.getValueAt(row, 4).equals("N/A"))
        {
          return Color.GRAY;
        }
        else
        {
          String strData = _benchmarkTableModel.getValueAt(row, 4).toString();
          if ((strData != "") && (Float.parseFloat(strData) > 2.0f))
          {
            return Color.RED;
          }
          else
          {
            return (row % 2 == 0) ? new Color(204, 255, 255) : Color.WHITE;
          }
        }
      }
    };

    //Set the column sorting functionality on 
    _benchmarkTable.setAutoCreateRowSorter(true);
    _benchmarkTable.setShowGrid(true);

    //Place the JTable object in a JScrollPane for a scrolling table 
    JScrollPane tableScrollPane = new JScrollPane(_benchmarkTable);
    tableScrollPane.setPreferredSize(new Dimension(1000, 1000));

    _benchmarkFrame = new JDialog(); // new JFrame();

    JPanel selectionPanel = new JPanel();
    selectionPanel.setBorder(BorderFactory.createTitledBorder("File Selection"));

    GroupLayout layout = new GroupLayout(selectionPanel);
    selectionPanel.setLayout(layout);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createSequentialGroup()
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
      .addComponent(_fileFromLabel)
      .addComponent(_fileToLabel)
      .addComponent(_dummy1Label))
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addComponent(_fileFromPathSelectionTextField)
      .addComponent(_fileToPathSelectionTextField)
      .addComponent(_saveButton))
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addComponent(_fileFromPathSelectionButton)
      .addComponent(_fileToPathSelectionButton)
      .addComponent(_analyzeButton)));

    layout.setVerticalGroup(layout.createSequentialGroup()
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      .addComponent(_fileFromLabel)
      .addComponent(_fileFromPathSelectionTextField)
      .addComponent(_fileFromPathSelectionButton))
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      .addComponent(_fileToLabel)
      .addComponent(_fileToPathSelectionTextField)
      .addComponent(_fileToPathSelectionButton))
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
      .addComponent(_dummy1Label)
      .addComponent(_saveButton)
      .addComponent(_analyzeButton)));

    _fileFromPathSelectionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JFileChooser fileChooser = new JFileChooser(Directory.getBenchmarkConfirmationLogDir());
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilterUtil jsonFileFilter = new FileFilterUtil(FileName.getJsonFileExtension(), StringLocalizer.keyToString("RDGUI_JSON_FILE_DESCRIPTION_KEY"));
        fileChooser.setFileFilter(jsonFileFilter);

        int returnVal = fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_KEY"));
        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
          return;
        }
        File selectedFile = fileChooser.getSelectedFile();
        String selectedFileName = selectedFile.getPath();
        String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
        if (seelctedFileExtension.equalsIgnoreCase(FileName.getJsonFileExtension()) == false)
        {
          selectedFileName += FileName.getCommaSeparatedValueFileExtension();
        }

        try
        {
          _fileFromPathSelectionTextField.setText(selectedFileName);
        }
        catch (Exception ex)
        {
          // if I/O error occurs
          ex.printStackTrace();
        }
        finally
        {
          // releases any system resources associated with the stream
        }

      }
    });

    _fileToPathSelectionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JFileChooser fileChooser = new JFileChooser(Directory.getBenchmarkConfirmationLogDir());
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilterUtil jsonFileFilter = new FileFilterUtil(FileName.getJsonFileExtension(), StringLocalizer.keyToString("RDGUI_JSON_FILE_DESCRIPTION_KEY"));
        fileChooser.setFileFilter(jsonFileFilter);

        int returnVal = fileChooser.showDialog(MainMenuGui.getInstance(), StringLocalizer.keyToString("GUI_SELECT_CDNA_TEST_RESULT_KEY"));
        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
          return;
        }
        File selectedFile = fileChooser.getSelectedFile();
        String selectedFileName = selectedFile.getPath();
        String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
        if (seelctedFileExtension.equalsIgnoreCase(FileName.getJsonFileExtension()) == false)
        {
          selectedFileName += FileName.getCommaSeparatedValueFileExtension();
        }

        try
        {
          _fileToPathSelectionTextField.setText(selectedFileName);
        }
        catch (Exception ex)
        {
          // if I/O error occurs
          ex.printStackTrace();
        }
        finally
        {
          // releases any system resources associated with the stream
        }

      }
    });
    _saveButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        JFileChooser fileChooser = new JFileChooser(Directory.getBenchmarkConfirmationLogDir());
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileFilterUtil csvFileFilter = new FileFilterUtil(FileName.getCommaSeparatedValueFileExtension(), StringLocalizer.keyToString("RDGUI_GENERATE_REPORT_FILE_DESCRIPTION_KEY"));
        fileChooser.setFileFilter(csvFileFilter);

        int returnVal = fileChooser.showDialog(MainMenuGui.getInstance(),
          StringLocalizer.keyToString("GUI_SAVE_CDNA_BENCHMARK_RESULT_KEY"));
        if (returnVal != JFileChooser.APPROVE_OPTION)
        {
          return;
        }
        File selectedFile = fileChooser.getSelectedFile();
        String selectedFileName = selectedFile.getPath();
        String seelctedFileExtension = FileUtilAxi.getExtension(selectedFileName);
        if (seelctedFileExtension.equalsIgnoreCase(FileName.getCommaSeparatedValueFileExtension()) == false)
        {
          selectedFileName += FileName.getCommaSeparatedValueFileExtension();
        }

        try
        {
          // write report
          String reportTitle = StringLocalizer.keyToString("GUI_SAVE_CDNA_BENCHMARK_RESULT_KEY");
          TableModelReportWriter reportWriter = new TableModelReportWriter(StringLocalizer.keyToString("CD_CONFIRM_TABLE_MODEL_CSV_REPORT_DELIMITER_KEY"));
          String reportFileName = fileChooser.getSelectedFile().getPath();
          reportWriter.writeCSVFile(reportFileName, reportTitle, _benchmarkTableModel);
        }
        //catch (IOException e)
        catch (Exception e)
        {
          // if I/O error occurs
          e.printStackTrace();
        }
        finally
        {
          // releases any system resources associated with the stream
        }

      }
    });
    _analyzeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        //Update the Table Format
        _benchmarkTableModel = new BenchmarkTableModel(BenchmarkTableModel.getColumnNames(), BenchmarkTableModel.getEmptyData());
        _benchmarkTable.setModel(_benchmarkTableModel);

        //Make sure the files exist before proceeding
        String filepathFrom = _fileFromPathSelectionTextField.getText();
        String filepathTo = _fileToPathSelectionTextField.getText();
        java.io.File fileFrom = new java.io.File(filepathFrom);
        java.io.File fileTo = new java.io.File(filepathTo);
        if (fileFrom.exists() == false || fileTo.exists() == false)
        {
          return;
        }

        BenchmarkJsonFolder rootFrom = BenchmarkJsonFileTimingLogger.LoadBenchmarkJsonFolder(filepathFrom);
        BenchmarkJsonFolder rootTo = BenchmarkJsonFileTimingLogger.LoadBenchmarkJsonFolder(filepathTo);
        String[] columnNames =
        {
          //"Task", "Benchmark Task Description", "Time ms (From File)", "Time ms (To File)", "Change (%)"
          StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_KEY"),
          StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_DESCRIPTION_KEY"),
          StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_FROM_TIME_KEY"),
          StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_TO_TIME_KEY"),
          StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_CHANGE_KEY")
        };

        //Build the Table Data for  Benchmarking
        Object[][] data = new Object[BenchmarkJsonFileEnum.values().length + 1][columnNames.length];
        String totalSumTask = "";
        int taskID = 0, count1 = 0, count2 = 0, totalCount = 0;
        float sum1 = 0, sum2 = 0;
        float totalSum1 = 0, totalSum2 = 0;
        for (BenchmarkJsonFileEnum bEnum : BenchmarkJsonFileEnum.values())
        {
          taskID = bEnum.ordinal();
          if ((rootFrom.getFiles() != null) && (rootTo.getFiles() != null))
          {
            if ((rootFrom.getFiles().length > taskID) && (rootTo.getFiles().length > taskID))
            {
              if ((rootFrom.getFiles()[taskID] != null) && (rootTo.getFiles()[taskID] != null))
              {
                List<Long> timingDatasFrom = rootFrom.getFiles()[taskID].getTimingDatas();
                List<Long> timingDatasTo = rootTo.getFiles()[taskID].getTimingDatas();

                sum1 = 0;
                count1 = 0;
                for (Long timing1 : timingDatasFrom)
                {
                  sum1 += timing1.intValue();
                  count1++;
                }
                sum2 = 0;
                count2 = 0;
                for (Long timing2 : timingDatasTo)
                {
                  sum2 += timing2.intValue();
                  count2++;
                }
                if (count1 > 0)
                {
                  sum1 = sum1 / count1;
                }

                if (count2 > 0)
                {
                  sum2 = sum2 / count2;
                }

                data[taskID][0] = taskID + 1;
                data[taskID][1] = bEnum.toString();

                if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_CDNA_BENCHMARK_DEBUG))
                {
                  data[taskID][2] = sum1 + " (" + timingDatasFrom + ")";
                  data[taskID][3] = sum2 + " (" + timingDatasTo + ")";
                }
                else
                {
                  data[taskID][2] = sum1;
                  data[taskID][3] = sum2;
                }

                if ((sum1 > 0) && (sum2 > 0))
                {
                  data[taskID][4] = "" + 100 * (sum2 - sum1) / sum1 + "";
                  totalSum1 += sum1;
                  totalSum2 += sum2;
                  totalCount++;

                  if ((taskID + 1) < BenchmarkJsonFileEnum.values().length)
                  {
                    totalSumTask += (taskID + 1) + ",";
                  }
                  else
                  {
                    totalSumTask += (taskID + 1);
                  }
                }
                else
                {
                  data[taskID][4] = "N/A";
                }
              }
            }
            else
            {
              //For new Added Task, the old json file will show Invalid Data
              data[taskID][0] = taskID + 1;
              data[taskID][1] = bEnum.toString();
              data[taskID][2] = "Invalid Data";
              data[taskID][3] = "Invalid Data";
              data[taskID][4] = "N/A";
            }
          }
          if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_CDNA_BENCHMARK_DEBUG))
          {
            System.out.println("data[" + taskID + "][0]=" + data[taskID][0]);
            System.out.println("data[" + taskID + "]][1]=" + data[taskID][1]);
            System.out.println("data[" + taskID + "]][2]=" + data[taskID][2]);
            System.out.println("data[" + taskID + "]][3]=" + data[taskID][3]);
            System.out.println("data[" + taskID + "]][4]=" + data[taskID][4]);
          }

        }

        //Total
        totalSumTask += ")";
        data[taskID + 1][0] = taskID + 2;
        if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_CDNA_BENCHMARK_DEBUG))
        {
          data[taskID + 1][1] = "Total of " + totalCount + " tasks (" + totalSumTask;
        }
        else
        {
          data[taskID + 1][1] = "Total of " + totalCount + "/" + BenchmarkJsonFileEnum.values().length + " tasks";
        }
        data[taskID + 1][2] = totalSum1;
        data[taskID + 1][3] = totalSum2;
        data[taskID + 1][4] = 100 * (totalSum2 - totalSum1) / totalSum1;

        if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_CDNA_BENCHMARK_DEBUG))
        {
          System.out.println("data[" + taskID + 1 + "][0]=" + data[taskID + 1][0]);
          System.out.println("data[" + taskID + 1 + "]][1]=" + data[taskID + 1][1]);
          System.out.println("data[" + taskID + 1 + "]][2]=" + data[taskID + 1][2]);
          System.out.println("data[" + taskID + 1 + "]][3]=" + data[taskID + 1][3]);
          System.out.println("data[" + taskID + 1 + "]][4]=" + data[taskID + 1][4]);
        }

        _benchmarkTableModel = new BenchmarkTableModel(columnNames, data);
        _benchmarkTable.setModel(_benchmarkTableModel);

        formatTable();
        _benchmarkTable.repaint();
        _benchmarkFrame.repaint();
      }
    });

    formatTable();

    //Uncomment For Simulation Test
    //_file1PathSelectionTextField.setText("C:\\Program Files\\AXI System\\v810\\5.0\\log\\confirm\\Benchmark\\2015-05-20.json");
    //_file2PathSelectionTextField.setText("C:\\Program Files\\AXI System\\v810\\5.0\\log\\confirm\\Benchmark\\2015-05-21.json");

    _benchmarkFrame.setLayout(new BoxLayout(_benchmarkFrame.getContentPane(), BoxLayout.Y_AXIS));
    _benchmarkFrame.setTitle("CDNA Benchmark Analysis");
    _benchmarkFrame.setSize(1000, 1000);
    java.awt.Image image = Image5DX.getImage(Image5DX.FRAME_X5000);

    _benchmarkFrame.setIconImage(image);
    //This will center the JFrame in the middle of the screen 
    _benchmarkFrame.setLocationRelativeTo(mainFrame);
    _benchmarkFrame.add(selectionPanel);
    _benchmarkFrame.add(tableScrollPane);

    _benchmarkFrame.setModal(true);
    _benchmarkFrame.setResizable(true);
    _benchmarkFrame.setVisible(true);
  }

  /*
   *
   * @author Anthony Fong
   */
  public void formatTable()
  {
    //Set the default editor for the Country column to be the combobox 
    TableColumn tableColumn1 = _benchmarkTable.getColumnModel().getColumn(0);
    tableColumn1.setPreferredWidth(50);
    TableColumn tableColumn2 = _benchmarkTable.getColumnModel().getColumn(1);
    tableColumn2.setPreferredWidth(350);
    TableColumn tableColumn3 = _benchmarkTable.getColumnModel().getColumn(2);
    tableColumn3.setPreferredWidth(200);
    TableColumn tableColumn4 = _benchmarkTable.getColumnModel().getColumn(3);
    tableColumn4.setPreferredWidth(200);
  }

  /*
   *
   * @author Anthony Fong
   */
  public static class BenchmarkTableModel extends DefaultSortTableModel //AbstractTableModel
  {

    public static String[] _columnNames = getColumnNames();
    public static Object[][] _data = getEmptyData();

    /*
     * @author Anthony Fong
     */
    public static String[] getColumnNames()
    {
      String[] columnNames =
      {
        //"Task", "Benchmark Task Description", "Time ms (From File)", "Time ms (To File)", "Change (%)"
        StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_KEY"),
        StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_DESCRIPTION_KEY"),
        StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_FROM_TIME_KEY"),
        StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_TO_TIME_KEY"),
        StringLocalizer.keyToString("GUI_ANALYZE_CDNA_BENCHMARK_TABLE_TASK_CHANGE_KEY")
      };
      return columnNames;
    }

    /*
     * @author Anthony Fong
     */
    public static Object[][] getEmptyData()
    {
      Object[][] data =
      {
        {
          "", "", "", "", "",
        }
      };
      return data;
    }

    /*
     * @author Anthony Fong
     */
    public BenchmarkTableModel(String[] columnNames, Object[][] data)
    {
      _columnNames = columnNames;
      _data = data;
    }

    /*
     * @author Anthony Fong
     */
    @Override
    public int getRowCount()
    {
      return _data.length;
    }

    /*
     * @author Anthony Fong
     */
    @Override
    public int getColumnCount()
    {
      return _columnNames.length;
    }

    /*
     * @author Anthony Fong
     */
    @Override
    public Object getValueAt(int row, int column)
    {
      return _data[row][column];
    }

    /*
     * Used by the JTable object to set the column names 
     * @author Anthony Fong
     */
    @Override
    public String getColumnName(int column)
    {
      return _columnNames[column];
    }

    /*
     * Used by the JTable object to render different 
     * functionality based on the data type 
     * @author Anthony Fong
     */
    @Override
    public Class getColumnClass(int c)
    {
      return getValueAt(0, c).getClass();
    }

    /*
     * @author Anthony Fong
     */
    @Override
    public boolean isCellEditable(int row, int column)
    {
      return false;
      //if (column == 0 || column == 1)
      //{
      //  return false;
      //}
      //else
      //{
      //  return true;
      //}
    }
  }
}
