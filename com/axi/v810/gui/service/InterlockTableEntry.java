package com.axi.v810.gui.service;

import javax.swing.*;

import com.axi.util.*;


/**
 * @author Erica Wheatcroft
 */
class InterlockTableEntry
{
  // full name of the interlock
  private String _name = "";

  // abbreviation of the interlock
  private String _abbreviation = "";

  // the current state of the interlock
  private ImageIcon _currentStateIcon = null;

  /**
   * @author Erica Wheatcroft
   */
  private InterlockTableEntry()
  {
    // do nothing.
  }

  /**
   * @author Erica Wheatcroft
   */
  InterlockTableEntry(String name, String abbreviation, ImageIcon currentStateIcon)
  {
    Assert.expect(name != null);
    Assert.expect(abbreviation != null);
    Assert.expect(currentStateIcon != null);

    _name = name;
    _abbreviation = abbreviation;
    _currentStateIcon = currentStateIcon;
  }

  /**
   * @author Erica Wheatcroft
   */
  InterlockTableEntry(String name, String abbreviation)
  {
    Assert.expect(name != null);
    Assert.expect(abbreviation != null);

    _name = name;
    _abbreviation = abbreviation;
  }

  /**
   * @author Erica Wheatcroft
   */
  void setInterlockStateIcon(ImageIcon currentStateIcon)
  {
    Assert.expect(currentStateIcon != null);
    _currentStateIcon = currentStateIcon;
  }

  /**
   * @author Erica Wheatcroft
   */
  ImageIcon getInterlockStateIcon()
  {
    Assert.expect(_currentStateIcon != null);
    return _currentStateIcon;
  }


  /**
   * @author Erica Wheatcroft
   */
  String getAbbreviation()
  {
    Assert.expect(_abbreviation != null);
    return _abbreviation;
  }

  /**
   * @author Erica Wheatcroft
   */
  String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
}
