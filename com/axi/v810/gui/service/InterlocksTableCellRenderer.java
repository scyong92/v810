package com.axi.v810.gui.service;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;

import com.axi.util.*;

/**
 * @author Erica Wheatcroft
 */
public class InterlocksTableCellRenderer extends JLabel implements TableCellRenderer
{
  /**
   * @author Erica Wheatcroft
   */
  InterlocksTableCellRenderer()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public Component getTableCellRendererComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 boolean hasFocus,
                                                 int row,
                                                 int col)
  {
    Assert.expect(row >= 0 && col >= 0);

    setOpaque(true);
    
    Color alternativeColor = (Color) UIManager.get ("Table.alternateRowColor");
    Color backgroundColor = Color.WHITE;

    TableModel tableModel = table.getModel();
    Assert.expect(tableModel instanceof AvailableInterlocksTableModel);
    AvailableInterlocksTableModel interlocksTableModel = (AvailableInterlocksTableModel)tableModel;
    InterlockTableEntry entry = interlocksTableModel.getInterlockEntryAtRow(row);
    ImageIcon statusIcon = entry.getInterlockStateIcon();

    // set background
    if (isSelected)
    {
      setForeground(table.getSelectionForeground());
      setBackground(table.getSelectionBackground());
    }
    else
    {
      setForeground(table.getForeground());
      //setBackground(table.getBackground());
      if(row%2 == 0)
        setBackground(backgroundColor);
      else
        setBackground(alternativeColor);
    }

    if (table.isRowSelected(row))
      setBackground(table.getSelectionBackground());
    else
    {
      if (hasFocus)
        setBackground(table.getSelectionBackground());
      else
      {
        //setBackground(table.getBackground());
        if (row % 2 == 0)
          setBackground(backgroundColor);
        else
          setBackground(alternativeColor);
      }
    }

    // we want to only show icons to indicate status
    setIcon(statusIcon);

    setVerticalAlignment(SwingConstants.CENTER);
    setHorizontalAlignment(SwingConstants.CENTER);

    return this;

  }
}
