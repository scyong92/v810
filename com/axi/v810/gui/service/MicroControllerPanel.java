package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MicroControllerPanel extends JPanel implements TaskPanelInt, Observer
{
    private JTextArea _statusPane = new JTextArea();
    private JButton _initializeButton = new JButton();        

    private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
    private SubsystemStatusAndControlTaskPanel _parent = null;
    
    private PspMicroController _pspMicroController = null;

    private boolean _initialized = false;
    private boolean _monitoring = false;
    
    private ProgressDialog _progressDialog = null;

    private boolean _loadingSwingComponent = false;

    // a count of the cameras that have been initialized
    private int _initializedCameraCount = 0;

    private ActionListener _initializeButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createProgressDialog();
          initializeSubsystem();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };    

    /**
     * @author Cheah Lee Herng
     */
    private void disableButtons(boolean configuringHardware)
    {
        try
        {
          if (configuringHardware)
          {
            // then disable everything but the cancel button should be enabled
            _initializeButton.setEnabled(false);
          }
          else
          {
            if (_initialized && _pspMicroController.isStartupRequired() == false)
            {
              _initializeButton.setEnabled(true);
            }
            else if (_initialized)
            {
              _initializeButton.setEnabled(true);
            }
            else
            {
              _initializeButton.setEnabled(true);
            }
          }
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private void initializeSubsystem()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _monitoring = false;

              _pspMicroController.startup();

              // set the flag
              _initialized = true;

              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  // set the state of the buttons
                  disableButtons(false);
                  clearStatusPane();
                }
              });

            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createProgressDialog()
    {
        _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                             StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                             StringLocalizer.keyToString("SERVICE_MICROCONTROLLER_INITIALIZED_KEY"),
                                             StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"),
                                             StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                             0,
                                             100,
                                             true);

        _progressDialog.pack();
        _progressDialog.setSize(new Dimension(500, 175));
        SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
    }

    /**
     * @author Cheah Lee Herng
     */
    public MicroControllerPanel(SubsystemStatusAndControlTaskPanel parent)
    {
        Assert.expect(parent != null);
        _parent = parent;

        // create the ui components
        createPanel();
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createPanel()
    {
        setLayout(new BorderLayout(50, 0));
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

        JPanel innerInitializePanel = new JPanel(new GridLayout(3, 1, 0, 5));

        JPanel initializeAndGetConfigPanel = new JPanel(new GridLayout(2, 1, 5, 25));
        initializeAndGetConfigPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));

        initializeAndGetConfigPanel.setOpaque(false);
        initializeAndGetConfigPanel.add(_initializeButton);        

        innerInitializePanel.add(initializeAndGetConfigPanel);

        JPanel buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(30, 30, 0, 30));
        buttonPanel.setOpaque(false);
        buttonPanel.add(innerInitializePanel, BorderLayout.NORTH);
        
        add(buttonPanel, BorderLayout.EAST);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void handleException(XrayTesterException xte)
    {
        _parent.handleXrayTesterException(xte);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void clearStatusPane()
    {
        try
        {
          _statusPane.getDocument().remove(0, _statusPane.getDocument().getLength());
        }
        catch (javax.swing.text.BadLocationException ex)
        {
          // this should not happen
          Assert.expect(false);
        }
    }

    private void addListeners()
    {
        _initializeButton.addActionListener(_initializeButtonActionListener);
    }

    /**
    * @author Erica Wheatcroft
    */
    private void removeListeners()
    {
        _initializeButton.removeActionListener(_initializeButtonActionListener);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void startObserving()
    {
        ProgressObservable.getInstance().addObserver(this);
        HardwareObservable.getInstance().addObserver(this);
    }

    /**
    * @author Cheah Lee Herng
    */
    private void stopObserving()
    {
        ProgressObservable.getInstance().deleteObserver(this);
        HardwareObservable.getInstance().deleteObserver(this);
    }

    /**
    * @author Erica Wheatcroft
    */
    public boolean isReadyToFinish()
    {
        return true;
    }

    /**
    * @author Erica Wheatcroft
    */
    public void finish()
    {
        // remove the listeners
        removeListeners();

        stopObserving();
    }

    /**
     * @author Cheah Lee Herng
     */
    public void start()
    {
        AbstractMicroController abstractMicroController = AbstractMicroController.getInstance();
        Assert.expect(abstractMicroController instanceof PspMicroController);
        _pspMicroController = (PspMicroController)abstractMicroController;

        try
        {
          if (_pspMicroController.isStartupRequired())
            _initialized = false;
          else
            _initialized = true;
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }

        // set the state of the buttons
        disableButtons(false);

        // add the listeners
        addListeners();

        populatePanel();

        startObserving();
    }

    /**
     * @author Cheah Lee Herng
     */
    private void populatePanel()
    {
        _loadingSwingComponent = true;        
        _loadingSwingComponent = false;
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized void update(final Observable observable, final Object arg)
    {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            if (observable instanceof ProgressObservable)
            {
              Assert.expect(arg instanceof ProgressReport);
              ProgressReport progressReport = (ProgressReport)arg;
              int percentDone = progressReport.getPercentDone();

              // don't indicate 100% until event actually completes
              if (percentDone == 100)
                percentDone = 99;

              if (_progressDialog != null)
                _progressDialog.updateProgressBarPrecent(percentDone);
            }
            else if (observable instanceof HardwareObservable)
            {
              if (arg instanceof HardwareEvent)
              {
                HardwareEvent event = (HardwareEvent)arg;
                HardwareEventEnum eventEnum = event.getEventEnum();
                if (eventEnum instanceof MicroControllerEventEnum)
                {
                  if (event.isStart() == false && eventEnum.equals(MicroControllerEventEnum.ON))
                  {
                     _progressDialog.updateProgressBarPrecent(100);
                  }
                }
              }
            }
            else
              Assert.expect(false);
          }
        });
    }

}
