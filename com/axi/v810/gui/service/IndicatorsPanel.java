package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * This class is in charge of the indicators (light stack). The user will be able to command any of the lights as well
 * as the buzzer to turn on or off.
 *
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
class IndicatorsPanel extends JPanel implements TaskPanelInt, Observer
{
  // colors used when the indicator is in the off state
  private final Color OFF_RED = new Color(160,0,0);
  private final Color OFF_YELLOW = new Color(160,160,0);
  private final Color OFF_GREEN = new Color(0,160,0);

  // ui components
  private JPanel _redLightInfoPanel = new JPanel();
  private JPanel _redLightPanel = new JPanel();
  private JPanel _redLightButtonPanel = new JPanel();
  private JPanel _redLightStatePanel = new JPanel();
  private JPanel _buzzerInfoPanel = new JPanel();
  private JPanel _buzzerPanel = new JPanel();
  private JPanel _buzzerButtonPanel = new JPanel();
  private JPanel _buzzerStatePanel = new JPanel();
  private JPanel _yellowLightInfoPanel = new JPanel();
  private JPanel _yellowLightPanel = new JPanel();
  private JPanel _yellowLightButtonPanel = new JPanel();
  private JPanel _yellowLightStatePanel = new JPanel();
  private JPanel _greenLightInfoPanel = new JPanel();
  private JPanel _greenLightPanel = new JPanel();
  private JPanel _greenLightButtonPanel = new JPanel();
  private JPanel _greenLightStatePanel = new JPanel();
  private JPanel _lightStackPanel = new JPanel();
  private BorderLayout _redLightBorderLayout = new BorderLayout();
  private BorderLayout _yellowLightBorderLayout = new BorderLayout();
  private BorderLayout _greenLightBorderLayout = new BorderLayout();
  private BorderLayout _buzzerBorderLayout = new BorderLayout();
  private FlowLayout _greenLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _greenLightStateFlowLayout = new FlowLayout();
  private FlowLayout _buzzerButtonFlowLayout = new FlowLayout();
  private FlowLayout _buzzerStateFlowLayout = new FlowLayout();
  private FlowLayout _redLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _redLightStateFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightButtonFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightStateFlowLayout = new FlowLayout();
  private FlowLayout _redLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _greenLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _yellowLightInfoFlowLayout = new FlowLayout();
  private FlowLayout _buzzerInfoFlowLayout = new FlowLayout();
  private JLabel _buzzerLabel = new JLabel();
  private JLabel _buzzerStateLabel = new JLabel();
  private JLabel _yellowLightLabel = new JLabel();
  private JLabel _yellowLightStateLabel = new JLabel();
  private JLabel _redLightLabel = new JLabel();
  private JLabel _redLightStateLabel = new JLabel();
  private JLabel _greenLightLabel = new JLabel();
  private JLabel _greenLightStateLabel = new JLabel();
  private JButton _redLightButton = new JButton();
  private JButton _buzzerButton = new JButton();
  private JButton _yellowLightButton = new JButton();
  private JButton _greenLightButton = new JButton();
  private JButton _initializeButton = new JButton();
  private Box _mainBox = null;

  // hardware subsystem
  private LightStack _lightStack = null;

  // all hardware calls need to be on the hardware worker thread.
  private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();

  private SubsystemStatusAndControlTaskPanel _parent = null;

  private final String _offButtonText = StringLocalizer.keyToString("SERVICE_OFF_BUTTON_TEXT_KEY");
  private final String _onButtonText = StringLocalizer.keyToString("SERVICE_ON_BUTTON_TEXT_KEY");
  private final String _offStateText = StringLocalizer.keyToString("SERVICE_OFF_STATE_TEXT_KEY");
  private final String _onStateText = StringLocalizer.keyToString("SERVICE_ON_STATE_TEXT_KEY");

  private ProgressDialog _progressDialog = null;

  private boolean _initialized = false;

  // listeners created here so i can add and remove them when needed
  private ActionListener _redLightButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      commandLightsOrBuzzer(true, false, false, false);
    }
  };
  private ActionListener _yellowLightButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      commandLightsOrBuzzer(false, false, true, false);
    }
  };
  private ActionListener _greenLightButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      commandLightsOrBuzzer(false, true, false, false);
    }
  };
  private ActionListener _buzzerButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      commandLightsOrBuzzer(false, false, false, true);
    }
  };
  private ActionListener _initializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      _workerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // shutdown digitial io
            DigitalIo.getInstance().shutdown();

            // startup digitial io
            DigitalIo.getInstance().startup();

            _initialized = true;

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                enableButtons();
                populatePanel();
              }
            });
          }
          catch(final XrayTesterException xte)
          {
            _initialized = false;
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                handleException(xte);
                _progressDialog.setVisible(false);
                _progressDialog = null;
              }
            });
          }
        }
        });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };


  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  IndicatorsPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;

    // create ui components
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+" - "+ StringLocalizer.keyToString("SERVICE_UI_SSACTP_INDICATORS_TAB_NAME_KEY"),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }


  /**
   * This method will turn on the either the red, green, yellow light or the buzzer
   *
   * @author Erica Wheatcroft
   */
  private void commandLightsOrBuzzer(final boolean commandRedLight,
                                     final boolean commandGreenLight,
                                     final boolean commandYellowLight,
                                     final boolean commandBuzzer)
  {
    _workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (commandRedLight)
          {
            if (_lightStack.isRedLightOn())
              _lightStack.turnOffRedLight();
            else
              _lightStack.turnOnRedLight();

            updateRedLightStatus();

            return;
          }
          else if (commandGreenLight)
          {
            if (_lightStack.isGreenLightOn())
              _lightStack.turnOffGreenLight();
            else
              _lightStack.turnOnGreenLight();

            updateGreenLightStatus();
            return;
          }
          else if (commandYellowLight)
          {
            if (_lightStack.isYellowLightOn())
              _lightStack.turnOffYellowLight();
            else
              _lightStack.turnOnYellowLight();

            updateYellowLightStatus();
            return;
          }
          else if (commandBuzzer)
          {
            if (_lightStack.isBuzzerOn())
              _lightStack.turnBuzzerOff();
            else
              _lightStack.trunBuzzerOnForServiceMode();

            updateBuzzerStatus();
            return;
          }
        }
        catch (final XrayTesterException xte)
        {
          SwingUtils.invokeLater(new Runnable()
          {
            public void run()
            {
              handleException(xte);
            }
          });
        }
      }
    });
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    // lets create the red light panel
    createRedLightPanel();

    // now lets create the yellow light panel
    createYellowLightPanel();

    // now lets create the green light panel
    createGreenLightPanel();

    // now lets create the buzzer panel
    createBuzzerPanel();

    // lets set all the panels to opaque = false so that our logo will show up
    udpateOpaqueSetting();

    // creat the main box to display all panels.
    _mainBox = Box.createVerticalBox();
    _mainBox.add(_redLightInfoPanel);
    _mainBox.add(Box.createVerticalStrut(50));
    _mainBox.add(_yellowLightInfoPanel);
    _mainBox.add(Box.createVerticalStrut(50));
    _mainBox.add(_greenLightInfoPanel);
    _mainBox.add(Box.createVerticalStrut(50));
    _mainBox.add(_buzzerInfoPanel);

    add(_mainBox, BorderLayout.WEST);

    JPanel initializePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    initializePanel.add(_initializeButton);

    add(initializePanel, BorderLayout.NORTH);
  }

  /**
   * this method sets all the panels to opaque = false so that our logo will show up
   *
   * @author Erica Wheatcroft
   */
  private void udpateOpaqueSetting()
  {
    _lightStackPanel.setOpaque(false);
    _greenLightButtonPanel.setOpaque(false);
    _greenLightStatePanel.setOpaque(false);
    _greenLightInfoPanel.setOpaque(false);
    _buzzerButtonPanel.setOpaque(false);
    _buzzerStatePanel.setOpaque(false);
    _buzzerInfoPanel.setOpaque(false);
    _yellowLightInfoPanel.setOpaque(false);
    _yellowLightButtonPanel.setOpaque(false);
    _yellowLightStatePanel.setOpaque(false);
    _redLightInfoPanel.setOpaque(false);
    _redLightButtonPanel.setOpaque(false);
    _redLightStatePanel.setOpaque(false);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createBuzzerPanel()
  {
    _buzzerPanel = new JPanel();
    _buzzerLabel.setText(StringLocalizer.keyToString("SERVICE_INDICATORS_BUZZER_KEY"));
    _buzzerLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _buzzerLabel.setFont(FontUtil.getBiggerFont(_buzzerLabel.getFont(),Localization.getLocale()));
    _buzzerLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _buzzerStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _buzzerStateLabel.setFont(FontUtil.getBiggerFont(_buzzerStateLabel.getFont(),Localization.getLocale()));
    _buzzerStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _buzzerButtonFlowLayout.setVgap(9);
    _buzzerStateFlowLayout.setVgap(15);
    _buzzerButton.setPreferredSize(new Dimension(75, 25));
    _buzzerPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _buzzerPanel.setPreferredSize(new Dimension(70, 60));
    _buzzerPanel.setLayout(_buzzerBorderLayout);
    _buzzerButtonPanel.setLayout(_buzzerButtonFlowLayout);
    _buzzerStatePanel.setLayout(_buzzerStateFlowLayout);
    _buzzerInfoPanel.setLayout(_buzzerInfoFlowLayout);
    _buzzerInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _buzzerInfoFlowLayout.setHgap(15);
    _buzzerInfoFlowLayout.setVgap(0);
    _buzzerInfoPanel.add(_buzzerPanel, null);
    _buzzerPanel.add(_buzzerLabel, BorderLayout.CENTER);
    _buzzerInfoPanel.add(_buzzerStatePanel, null);
    _buzzerStatePanel.add(_buzzerStateLabel, null);
    _buzzerInfoPanel.add(_buzzerButtonPanel, null);
    _buzzerButtonPanel.add(_buzzerButton, null);
    _buzzerStateLabel.setText(_offStateText);
    _buzzerButton.setText(_onButtonText);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createGreenLightPanel()
  {
    _greenLightLabel.setText(StringLocalizer.keyToString("SERVICE_INDICATORS_GREEN_KEY"));
    _greenLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _greenLightLabel.setFont(FontUtil.getBiggerFont(_greenLightLabel.getFont(),Localization.getLocale()));
    _greenLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _greenLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _greenLightStateLabel.setFont(FontUtil.getBiggerFont(_greenLightStateLabel.getFont(),Localization.getLocale()));
    _greenLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _greenLightButtonFlowLayout.setVgap(9);
    _greenLightStateFlowLayout.setVgap(15);
    _greenLightButton.setPreferredSize(new Dimension(75, 25));
    _greenLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _greenLightPanel.setPreferredSize(new Dimension(70, 60));
    _greenLightPanel.setLayout(_greenLightBorderLayout);
    _greenLightButtonPanel.setLayout(_greenLightButtonFlowLayout);
    _greenLightStatePanel.setLayout(_greenLightStateFlowLayout);
    _greenLightInfoPanel.setLayout(_greenLightInfoFlowLayout);
    _greenLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _greenLightInfoFlowLayout.setHgap(15);
    _greenLightInfoFlowLayout.setVgap(0);
    _greenLightInfoPanel.add(_greenLightPanel, null);
    _greenLightPanel.add(_greenLightLabel, BorderLayout.CENTER);
    _greenLightInfoPanel.add(_greenLightStatePanel, null);
    _greenLightStatePanel.add(_greenLightStateLabel, null);
    _greenLightInfoPanel.add(_greenLightButtonPanel, null);
    _greenLightButtonPanel.add(_greenLightButton, null);
    _greenLightPanel.setBackground(OFF_GREEN);
    _greenLightStateLabel.setText(_offStateText);
    _greenLightButton.setText(_onButtonText);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createYellowLightPanel()
  {
    _yellowLightInfoPanel.setLayout(_yellowLightInfoFlowLayout);
    _yellowLightLabel.setText(StringLocalizer.keyToString("SERVICE_INDICATORS_YELLOW_KEY"));
    _yellowLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _yellowLightLabel.setFont(FontUtil.getBiggerFont(_yellowLightLabel.getFont(),Localization.getLocale()));
    _yellowLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _yellowLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _yellowLightStateLabel.setFont(FontUtil.getBiggerFont(_yellowLightStateLabel.getFont(),Localization.getLocale()));
    _yellowLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _yellowLightButtonFlowLayout.setVgap(9);
    _yellowLightStateFlowLayout.setVgap(15);
    _yellowLightButton.setPreferredSize(new Dimension(75, 25));
    _yellowLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _yellowLightPanel.setPreferredSize(new Dimension(70, 60));
    _yellowLightPanel.setLayout(_yellowLightBorderLayout);
    _yellowLightButtonPanel.setLayout(_yellowLightButtonFlowLayout);
    _yellowLightStatePanel.setLayout(_yellowLightStateFlowLayout);
    _yellowLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _yellowLightInfoFlowLayout.setHgap(15);
    _yellowLightInfoFlowLayout.setVgap(0);
    _yellowLightInfoPanel.add(_yellowLightPanel, null);
    _yellowLightPanel.add(_yellowLightLabel, BorderLayout.CENTER);
    _yellowLightInfoPanel.add(_yellowLightStatePanel, null);
    _yellowLightStatePanel.add(_yellowLightStateLabel, null);
    _yellowLightInfoPanel.add(_yellowLightButtonPanel, null);
    _yellowLightButtonPanel.add(_yellowLightButton, null);
    _yellowLightPanel.setBackground(OFF_YELLOW);
    _yellowLightStateLabel.setText(_offStateText);
    _yellowLightButton.setText(_onButtonText);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createRedLightPanel()
  {
    _redLightInfoPanel.setLayout(_redLightInfoFlowLayout);
    _redLightLabel.setFont(FontUtil.getBiggerFont(_redLightLabel.getFont(),Localization.getLocale()));
    _redLightLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _redLightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _redLightLabel.setText(StringLocalizer.keyToString("SERVICE_INDICATORS_RED_KEY"));
    _redLightStateLabel.setFont(FontUtil.getBiggerFont(_redLightStateLabel.getFont(),Localization.getLocale()));
    _redLightStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _redLightStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _redLightButton.setPreferredSize(new Dimension(75, 25));
    _redLightPanel.setLayout(_redLightBorderLayout);
    _redLightPanel.setBorder(BorderFactory.createLineBorder(Color.black));
    _redLightPanel.setPreferredSize(new Dimension(70, 60));
    _redLightButtonFlowLayout.setVgap(9);
    _redLightButtonPanel.setLayout(_redLightButtonFlowLayout);
    _redLightStatePanel.setLayout(_redLightStateFlowLayout);
    _redLightStateFlowLayout.setVgap(15);
    _redLightInfoFlowLayout.setAlignment(FlowLayout.LEFT);
    _redLightInfoFlowLayout.setHgap(15);
    _redLightInfoFlowLayout.setVgap(0);
    _redLightInfoPanel.add(_redLightPanel, null);
    _redLightPanel.add(_redLightLabel, BorderLayout.CENTER);
    _redLightInfoPanel.add(_redLightStatePanel, null);
    _redLightStatePanel.add(_redLightStateLabel, null);
    _redLightInfoPanel.add(_redLightButtonPanel, null);
    _redLightButtonPanel.add(_redLightButton, null);
    _redLightPanel.setBackground(OFF_RED);
    _redLightStateLabel.setText(_offStateText);
    _redLightButton.setText(_onButtonText);
  }

  /**
   * This method is called every time the indicators panel is brought to front.
   *
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // get an instance of the light stack
    _lightStack = _lightStack.getInstance();

    // now lets check to see if we need to start up digitial io since that is a required subsystem.
    try
    {
      _initialized = ! DigitalIo.getInstance().isStartupRequired();
    }
    catch (XrayTesterException xte)
    {
      handleException(xte);
      _initialized = false;
    }

    // enable the button state
    enableButtons();

    // populate the UI
    populatePanel();

    // add the listeners
    addListeners();

    // start observing
    startObserving();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enableButtons()
  {
    _initializeButton.setEnabled(true);
    _redLightButton.setEnabled(_initialized);
    _buzzerButton.setEnabled(_initialized);
    _yellowLightButton.setEnabled(_initialized);
    _greenLightButton.setEnabled(_initialized);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_initializeButtonActionListener);
    _redLightButton.addActionListener(_redLightButtonActionListener);
    _yellowLightButton.addActionListener(_yellowLightButtonActionListener);
    _greenLightButton.addActionListener(_greenLightButtonActionListener);
    _buzzerButton.addActionListener(_buzzerButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_initializeButtonActionListener);
    _redLightButton.removeActionListener(_redLightButtonActionListener);
    _yellowLightButton.removeActionListener(_yellowLightButtonActionListener);
    _greenLightButton.removeActionListener(_greenLightButtonActionListener);
    _buzzerButton.removeActionListener(_buzzerButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // remove the listeners
    removeListeners();

    // start observing
    stopObserving();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    updateRedLightStatus();
    updateYellowLightStatus();
    updateGreenLightStatus();
    updateBuzzerStatus();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateBuzzerStatus()
  {
    if (_initialized == false)
    {
      _buzzerStateLabel.setText(_offStateText);
      _buzzerButton.setText(_onButtonText);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          if (_buzzerPanel == null)
            createBuzzerPanel();
          try
          {
            if (_lightStack.isBuzzerOn())
            {
              _buzzerStateLabel.setText(_onStateText);
              _buzzerButton.setText(_offButtonText);
            }
            else
            {
              _buzzerStateLabel.setText(_offStateText);
              _buzzerButton.setText(_onButtonText);
            }
          }
          catch (XrayTesterException xte)
          {
            handleException(xte);
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateGreenLightStatus()
  {
    if (_initialized == false)
    {
      _greenLightPanel.setBackground(OFF_GREEN);
      _greenLightStateLabel.setText(_offStateText);
      _greenLightButton.setText(_onButtonText);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (_lightStack.isGreenLightOn())
            {
              _greenLightPanel.setBackground(Color.green);
              _greenLightStateLabel.setText(_onStateText);
              _greenLightButton.setText(_offButtonText);
            }
            else
            {
              _greenLightPanel.setBackground(OFF_GREEN);
              _greenLightStateLabel.setText(_offStateText);
              _greenLightButton.setText(_onButtonText);
            }
          }
          catch (XrayTesterException xte)
          {
            handleException(xte);
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateYellowLightStatus()
  {
    if (_initialized == false)
    {
      _yellowLightPanel.setBackground(OFF_YELLOW);
      _yellowLightStateLabel.setText(_offStateText);
      _yellowLightButton.setText(_onButtonText);

    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // yellow light info
            if (_lightStack.isYellowLightOn())
            {
              _yellowLightPanel.setBackground(Color.yellow);
              _yellowLightStateLabel.setText(_onStateText);
              _yellowLightButton.setText(_offButtonText);
            }
            else
            {
              _yellowLightPanel.setBackground(OFF_YELLOW);
              _yellowLightStateLabel.setText(_offStateText);
              _yellowLightButton.setText(_onButtonText);
            }
          }
          catch (XrayTesterException xte)
          {
            handleException(xte);
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateRedLightStatus()
  {
    if(_initialized == false)
    {
      _redLightPanel.setBackground(OFF_RED);
      _redLightStateLabel.setText(_offStateText);
      _redLightButton.setText(_onButtonText);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          // red light info
          try
          {
            if (_lightStack.isRedLightOn())
            {
              _redLightPanel.setBackground(Color.red);
              _redLightStateLabel.setText(_onStateText);
              _redLightButton.setText(_offButtonText);
            }
            else
            {
              _redLightPanel.setBackground(OFF_RED);
              _redLightStateLabel.setText(_offStateText);
              _redLightButton.setText(_onButtonText);
            }
          }
          catch (XrayTesterException xte)
          {
           handleException(xte);
          }
        }
      });
    }
  }

  /**
   * @author George A. David
   */
  public void paintComponent(Graphics graphics)
  {
    super.paintComponent(graphics);

    int panelWidth = getWidth();
    int panelHeight = getHeight();
    int iconWidth = _parent.getSparkIcon().getIconWidth();
    int iconHeight = _parent.getSparkIcon().getIconHeight();
    int xCoordinate = panelWidth - iconWidth;
    int yCoordinate = panelHeight - iconHeight;

    _parent.getSparkIcon().paintIcon(this, graphics, xCoordinate, yCoordinate);
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    ProgressObservable.getInstance().addObserver(this);
    HardwareObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    ProgressObservable.getInstance().deleteObserver(this);
    HardwareObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if(observable instanceof HardwareObservable)
        {
          if(arg instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)arg;
            HardwareEventEnum eventEnum = event.getEventEnum();
            if (eventEnum instanceof DigitalIoEventEnum)
            {
              if (event.isStart() == false && eventEnum.equals(DigitalIoEventEnum.INITIALIZE))
                if(_progressDialog != null)
                  _progressDialog.updateProgressBarPrecent(100);
            }
          }
        }
        else
          Assert.expect(false);
      }
    });
  }
}
