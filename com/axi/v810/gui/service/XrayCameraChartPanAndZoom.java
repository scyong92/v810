package com.axi.v810.gui.service;

/**
 * Statistical Process Control Math Library V 1.00.
 * Copyright (C) 2002 Thamaraiselvan.P
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; rsion.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * For any questions or suggestions, you can write to me at :
 * Email :p_thamarai@hotmail.com
 */
// XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.*;
/**
 * A simple demonstration application showing how to create a line chart using data from an
 * XYDataset.
 */

public class XrayCameraChartPanAndZoom implements ChangeListener
{
    BufferedImage image;
    JLabel label;
    int value =100;
    public BufferedImage imageXBarChart = null;
    public void setXBarChartImage(String imagePath)
    {
        try
        {
            imageXBarChart = ImageIO.read(new File(imagePath));
            createAnImage(imageXBarChart.getWidth(),imageXBarChart.getHeight());
            stateUpdate();
        }
        catch(Exception ie)
        {
        }
    }

    public void stateUpdate()
    {
        double scale = value/100.0;
        BufferedImage scaled = getScaledImage(scale);
        label.setIcon(new ImageIcon(scaled));
        //label.setSize(800, 600);
        label.getParent().getParent().getParent().doLayout();
        label.revalidate();  // signal scrollpane
    }
    public void stateChanged(ChangeEvent e)
    {
        value = ((JSlider)e.getSource()).getValue();
        stateUpdate();
    }

    public BufferedImage getScaledImage(double scale) 
    {

        int w = (int)(scale*image.getWidth());
        int h = (int)(scale*image.getHeight());
        BufferedImage bi = new BufferedImage(w, h, image.getType());
        Graphics2D g2 = bi.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                            RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        //AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
        //g2.drawRenderedImage(image, at);
        
        if(imageXBarChart != null)
        {

            double scale2 =  image.getWidth()/(double)imageXBarChart.getWidth();
            AffineTransform at2 = AffineTransform.getScaleInstance(scale*scale2, scale*scale2);
            g2.drawRenderedImage(imageXBarChart, at2);
        }

        g2.dispose();
        return bi;
    }

    public JLabel getContent()
    {
        Config _config = Config.getInstance();
        int   chartWidth=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_WIDTH);
        int   chartHeight=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_HEIGHT);

        createAnImage(chartWidth, chartHeight);
        label = new JLabel(new ImageIcon(image));
        //label.setHorizontalAlignment(JLabel.CENTER);
        return label;
    }

    public void createAnImage(int w, int h )
    {

        int type = BufferedImage.TYPE_INT_RGB; // many options
        image = new BufferedImage(w, h, type);
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                            RenderingHints.VALUE_STROKE_PURE);
        //g2.setPaint(new Color(240,200,200));
        g2.setPaint(Color.WHITE);
        g2.fillRect(0,0,w,h);
//        g2.setPaint(Color.blue);
//        g2.draw(new Rectangle2D.Double(w/16, h/16, w*7/8, h*7/8));
//        g2.setPaint(Color.green.darker());
//        g2.draw(new Line2D.Double(w/16, h*15/16, w*15/16, h/16));
//        Ellipse2D e = new Ellipse2D.Double(w/4, h/4, w/2, h/2);
//        g2.setPaint(new Color(240,240,200));
//        g2.fill(e);
//        g2.setPaint(Color.red);
//        g2.draw(e);
//        g2.dispose();
    }

    public JSlider getControl()
    {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 100, 200, 100);
        slider.setMajorTickSpacing(50);
        slider.setMinorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.addChangeListener(this);

        //slider.setPreferredSize(new Dimension(40, 240));
        return slider;
    }


}


