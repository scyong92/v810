package com.axi.v810.gui.service;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * This class supports an X-ray Safety Test process step definition. The process step has:
 * 1. An enumeration of the step type,
 * 2. A primary document key that references an HTML file that is presented to the user during the proces step,
 * 3. An optional second document key for two-part instructions with a UI control in between,
 * 4. A prompt key that references an HTML file that describes what to do next,
 * 5. Values of x-ray power, stage position, rail width and barrier states for the process step
 *
 * @author George Booth
 */
public class XraySafetyTestProcessStep
{
  private Config _config = Config.getInstance();

  private XraySafetyTestProcessStepEnum _stepEnum;
  private String _document1Key;
  private String _document2Key;
  private String _promptKey;
  private XraySafetyTestPowerEnum _xrayPowerEnum = XraySafetyTestPowerEnum.POWER_OFF;
  private int _stageXPositionNanometers = _config.getIntValue(HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS);
  private int _stageYPositionNanometers = _config.getIntValue(HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS);
//  CR31292: Use maximum width limit rather than hardcoded 18.0" width. System should allow 18.0" (spec) but improper
//           calibration may have maximum slightly less than 18.0", causing an assert.
//  private int _railWidthNanometers = _config.getIntValue(HardwareCalibEnum.XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS);
  private int _railWidthNanometers = _config.getIntValue(HardwareConfigEnum.RAIL_WIDTH_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS);

  private boolean _innerBarrierClosed = true;
  private boolean _outerBarriersClosed = true;
  private boolean _xrayActuatorDown = false;

  /**
   * @author George Booth
   */
  public XraySafetyTestProcessStep(XraySafetyTestProcessStepEnum stepEnum)
  {
    Assert.expect(stepEnum != null);
    _stepEnum = stepEnum;
  }

  /**
   * @author George Booth
   */
  public XraySafetyTestProcessStepEnum getStepEnum()
  {
    Assert.expect(_stepEnum != null);
    return _stepEnum;
  }

  /**
   * @author George Booth
   */
  public void setDocument1Key(String documentKey)
  {
    Assert.expect(documentKey != null);
    _document1Key = documentKey;
  }

  /**
   * @author George Booth
   */
  public String getDocument1Key()
  {
    return _document1Key;
  }

  /**
   * @author George Booth
   */
  public void setDocument2Key(String documentKey)
  {
    Assert.expect(documentKey != null);
    _document2Key = documentKey;
  }

  /**
   * @author George Booth
   */
  public String getDocument2Key()
  {
    return _document2Key;
  }

  /**
   * @author George Booth
   */
  public void setPromptKey(String promptKey)
  {
    Assert.expect(promptKey != null);
    _promptKey = promptKey;
  }

  /**
   * @author George Booth
   */
  public String getpromptKey()
  {
    return _promptKey;
  }

  /**
   * @author George Booth
   */
  public void setXrayPower(XraySafetyTestPowerEnum xrayPowerEnum)
  {
    Assert.expect(xrayPowerEnum != null);
    _xrayPowerEnum = xrayPowerEnum;
  }

  /**
   * @author George Booth
   */
  public XraySafetyTestPowerEnum getXrayPowerEnum()
  {
    return _xrayPowerEnum;
  }

  /**
   * @author George Booth
   */
  public void setStageXPositionNanometers(int stageXPositionNanometers)
  {
    Assert.expect(stageXPositionNanometers >= 0);
    _stageXPositionNanometers = stageXPositionNanometers;
  }

  /**
   * @author George Booth
   */
  public int getStageXPositionNanometers()
  {
    return _stageXPositionNanometers;
  }

  /**
   * @author George Booth
   */
  public void setStageYPositionNanometers(int stageYPositionNanometers)
  {
    Assert.expect(stageYPositionNanometers >= 0);
    _stageYPositionNanometers = stageYPositionNanometers;
  }

  /**
   * @author George Booth
   */
  public int getStageYPositionNanometers()
  {
    return _stageYPositionNanometers;
  }

  /**
   * @author George Booth
   */
  public void setRailWidthNanometers(int railWidthNanometers)
  {
    Assert.expect(railWidthNanometers >= 0);
    _railWidthNanometers = railWidthNanometers;
  }

  /**
   * @author George Booth
   */
  public int getRailWidthNanometers()
  {
    return _railWidthNanometers;
  }

  /**
   * @author George Booth
   */
  public void closeInnerBarrier()
  {
    _innerBarrierClosed = true;
  }

  /**
   * @author George Booth
   */
  public void openInnerBarrier()
  {
    _innerBarrierClosed = false;
  }

  /**
   * @author George Booth
   */
  public boolean isInnerBarrierClosed()
  {
    return _innerBarrierClosed;
  }

  /**
   * @author George Booth
   */
  public void closeOuterBarriers()
  {
    _outerBarriersClosed = true;
  }

  /**
   * @author George Booth
   */
  public void openOuterBarriers()
  {
    _outerBarriersClosed = false;
  }

  /**
   * @author George Booth
   */
  public boolean areOuterBarriersClosed()
  {
    return _outerBarriersClosed;
  }
  
    /**
   * @author George Booth
   */
  public void upXrayActuator()
  {
    _xrayActuatorDown = false;
  }

  /**
   * @author George Booth
   */
  public void downXrayActuator()
  {
    _xrayActuatorDown = true;
  }

  /**
   * @author George Booth
   */
  public boolean isXrayActuatorDown()
  {
    return _xrayActuatorDown;
  }

}
