package com.axi.v810.gui.service;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;

import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.images.*;

/**
 * This class controls display of the test tree entries.  We need to display enabled tests normally and
 * disabled test in grey text
 *
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
class TestTreeCellRenderer extends DefaultTreeCellRenderer
{
  // parent node icons
  private ImageIcon _testParentDisabledIcon;
  private ImageIcon _testParentEnabledIcon;

  // leaf node icons
  private ImageIcon _testLeafPassedIcon;
  private ImageIcon _testLeafFailedIcon;
  private ImageIcon _testLeafCancelledIcon;
  private ImageIcon _testLeafEnabledIcon;
  private ImageIcon _testLeafDisabledIcon;

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public TestTreeCellRenderer()
  {
    _testParentDisabledIcon = Image5DX.getImageIcon(Image5DX.CD_DISABLED_TEST);
    _testParentEnabledIcon = Image5DX.getImageIcon(Image5DX.CD_ENABLED_TEST);

    _testLeafPassedIcon = Image5DX.getImageIcon(Image5DX.CD_PASSED_LEAF_TEST);
    _testLeafFailedIcon = Image5DX.getImageIcon(Image5DX.CD_FAILED_LEAF_TEST);
    _testLeafCancelledIcon = Image5DX.getImageIcon(Image5DX.CD_CANCELLED_LEAF_TEST);
    _testLeafEnabledIcon = Image5DX.getImageIcon(Image5DX.CD_ENABLED_LEAF_TEST);
    _testLeafDisabledIcon = Image5DX.getImageIcon(Image5DX.CD_DISABLED_LEAF_TEST);
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public Component getTreeCellRendererComponent(JTree tree,
                                                Object value,
                                                boolean sel,
                                                boolean expanded,
                                                boolean leaf,
                                                int row,
                                                boolean hasFocus)
  {
    super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

    if(tree.isEnabled() == false)
      return this;

    DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

    if(node.getUserObject() instanceof TestNode)
    {
      TestNode task = (TestNode) ((DefaultMutableTreeNode) value).getUserObject();

      if(task.getTest().canRunOnThisHardware() == false)
      {
        // test can not run on this available hardware
        setForeground(Color.gray);
        setIcon(_testLeafDisabledIcon);
      }
      else
      {
        // test can run on the available hardware
        setIcon(_testLeafEnabledIcon);
      }
      if(task.getTest().isEnabled())
      {
        setIcon(_testLeafEnabledIcon);
      }
      else
      {
        setForeground(Color.gray);
        setIcon(_testLeafDisabledIcon);
      }

      HardwareTaskStatusEnum status = task.getTest().getTaskStatus();
      if(status != null)
      {
        if (status.equals(HardwareTaskStatusEnum.PASSED))
          setIcon(_testLeafPassedIcon);
        else if (status.equals(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED))
          setIcon(_testLeafFailedIcon);
        else if (status.equals(HardwareTaskStatusEnum.FAILED))
          setIcon(_testLeafFailedIcon);
        else if (status.equals(HardwareTaskStatusEnum.CANCELED))
          setIcon(_testLeafCancelledIcon);
        else if (status.equals(HardwareTaskStatusEnum.PARTIAL_PASS))
          setIcon(_testLeafPassedIcon);
        else if (status.equals(HardwareTaskStatusEnum.PARTIAL_FAIL))
          setIcon(_testLeafFailedIcon);
      }
    }

    return this;
  }
}
