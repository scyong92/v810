package com.axi.v810.gui.service;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.gui.*;

/**
 * This class is a wrapper class for each node in the test list tree.  It contains a reference to a TestableAdapter and
 * provides a toString method to represent the string on the tree control.  It also provides the high level test execution
 * methods:  execute, loop and cancel.  These are used when the user clicks on 'run' from the GUI
 *
 * @author Andy Mechtenberg
 * @author Erica Wheatcroft
 */
class TestNode
{
  private HardwareTaskExecutionInterface _test = null;
  private ConfirmDiagnosticAdjustmentTaskPanel _parent = null;

  /**
   * @author Erica Wheatcroft
   */
  TestNode(HardwareTaskExecutionInterface test, ConfirmDiagnosticAdjustmentTaskPanel parent)
  {
    Assert.expect(test != null);
    Assert.expect(parent != null);
    _test = test;
    _parent = parent;
  }

  /**
   * This function will cause the test to execute the task
   *
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  boolean execute() throws XrayTesterException
  {
    Assert.expect(_test != null);
    boolean testPassed = false;
    testPassed = _test.executeUnconditionally();
    return testPassed;
  }

  /**
   * @author Erica Wheatcroft
   */
  void cancel()
  {
    Assert.expect(_test != null);
    _test.cancelNoWait();
  }

  /**
   * @author Erica Wheatcroft
   */
  HardwareTaskExecutionInterface getTest()
  {
    Assert.expect(_test != null);
    return _test;
  }

  /**
   * @author Andy Mechtenberg
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    if (_test == null)
      return "";

    return _test.getName();
  }

}
