package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.text.*;
import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EthernetBasedLightEnginePanel extends JPanel implements TaskPanelInt
{
  private static final int TOTAL_PROCESSING_LOOP = 25;
  
  private SubsystemStatusAndControlTaskPanel _parent = null;
  private MainMenuGui _mainUI = null;
  private JButton _initializeEbleHardwareButton = new JButton();
  private JButton _stopEbleHardwareButton = new JButton();
  
  private JButton _displayImageOneForModuleOneButton = new JButton();
  private JButton _displayImageTwoForModuleOneButton = new JButton();
  private JButton _displayImageThreeForModuleOneButton = new JButton();
  private JButton _displayImageFourForModuleOneButton = new JButton();
  private JButton _displayImageFiveForModuleOneButton = new JButton();
  private JButton _displayImageSixForModuleOneButton = new JButton();
  private JButton _displayImageSevenForModuleOneButton = new JButton();
  private JButton _displayImageEightForModuleOneButton = new JButton();
  private JButton _displayWhiteLightForModuleOneButton = new JButton();
  private JButton _turnOffLEDModuleOneButton = new JButton();
  private JButton _singleCaptureImageForModuleOneButton = new JButton();
  private JButton _loopCaptureImageForModuleOneButton = new JButton();
  
  private JButton _displayImageOneForModuleTwoButton = new JButton();
  private JButton _displayImageTwoForModuleTwoButton = new JButton();
  private JButton _displayImageThreeForModuleTwoButton = new JButton();
  private JButton _displayImageFourForModuleTwoButton = new JButton();
  private JButton _displayImageFiveForModuleTwoButton = new JButton();
  private JButton _displayImageSixForModuleTwoButton = new JButton();
  private JButton _displayImageSevenForModuleTwoButton = new JButton();
  private JButton _displayImageEightForModuleTwoButton = new JButton();
  private JButton _displayWhiteLightForModuleTwoButton = new JButton();
  private JButton _turnOffLEDModuleTwoButton = new JButton();
  private JButton _singleCaptureImageForModuleTwoButton = new JButton();
  private JButton _loopCaptureImageForModuleTwoButton = new JButton();
  
  private JScrollPane _statusPaneScrollPane = new JScrollPane();
  private JTextArea _statusPane = new JTextArea();
  
  private EthernetBasedLightEngineProcessor _ebleProcessor = null;
  private boolean _initialized = false;
  
  private static PspSettingEnum _settingEnum;
  
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  
  private ActionListener _initializeElbeButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_INITIALIZING_EBLE_KEY"),
          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.restart();
            _initialized = true;
            
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                enableButtons();
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _stopElbeButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_STOPPING_EBLE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.stop();
            _initialized = false;
            
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                enableButtons();
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
      JOptionPane.showMessageDialog(_mainUI,
                                    StringLocalizer.keyToString("SERVICE_EBLE_HAVE_BEEN_STOPPED_KEY"),
                                    StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                    JOptionPane.INFORMATION_MESSAGE);

    }
  };
  
  private ActionListener _turnOffLEDModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_TURNING_OFF_PROJECTOR_LED_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.turnOffLED(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _turnOffLEDModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_TURNING_OFF_PROJECTOR_LED_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.turnOffLED(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageOneForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_ONE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern1(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageTwoForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_TWO_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern2(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageThreeForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_THREE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern3(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageFourForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_FOUR_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern4(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageFiveForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_FIVE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern5(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageSixForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_SIX_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern6(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageSevenForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_SEVEN_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern7(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageEightForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_EIGHT_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern8(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayWhiteLightForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_WHITE_LIGHT_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayWhiteLight(EthernetBasedLightEngineEnum.ENGINE_0);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _singleCaptureImageForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_CAPTURING_IMAGE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
            opticalPointToPointScan.setId(1);
            opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            opticalPointToPointScan.setRegionPositionName("captureImageModuleOne");
            opticalPointToPointScan.setOpticalCameraRectangleName("captureImageModuleOne");
            opticalPointToPointScan.setPointPositionName("captureImageModuleOne");
            opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(new StagePositionMutable());
            opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
            opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
            
            if (_ebleProcessor.isSimulationModeOn() == false)
            {
              PspEngine.getInstance().performHeightMapOperation(opticalPointToPointScan, "CaptureImageModuleOne", "CaptureImageModuleOne", false);
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                displayCaptureImageMessage(PspModuleEnum.FRONT, true);
                _singleCaptureImageForModuleOneButton.setEnabled(true);
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _singleCaptureImageForModuleOneButton.setEnabled(true);
                
                displayCaptureImageMessage(PspModuleEnum.FRONT, false);
                
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
          finally
          {
            _singleCaptureImageForModuleOneButton.setEnabled(true);
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _loopCaptureImageForModuleOneButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_CAPTURING_IMAGE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
            opticalPointToPointScan.setId(1);
            opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            opticalPointToPointScan.setRegionPositionName("captureImageModuleOne");
            opticalPointToPointScan.setOpticalCameraRectangleName("captureImageModuleOne");
            opticalPointToPointScan.setPointPositionName("captureImageModuleOne");
            opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(new StagePositionMutable());
            opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
            opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
            
            for(int i=1; i <= TOTAL_PROCESSING_LOOP; ++i)
            {
              if (_ebleProcessor.isSimulationModeOn() == false)
              {
                PspEngine.getInstance().performHeightMapOperation(opticalPointToPointScan, "CaptureImageModuleOne", "CaptureImageModuleOne", false);
              }
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  displayCaptureImageMessage(PspModuleEnum.FRONT, true);
                }
              });
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _loopCaptureImageForModuleOneButton.setEnabled(true);
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _loopCaptureImageForModuleOneButton.setEnabled(true);
                
                displayCaptureImageMessage(PspModuleEnum.FRONT, false);
                
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
          finally
          {
            _loopCaptureImageForModuleOneButton.setEnabled(true);
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageOneForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_ONE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern1(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageTwoForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_TWO_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern2(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageThreeForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_THREE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern3(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageFourForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_FOUR_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern4(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageFiveForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_FIVE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern5(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageSixForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_SIX_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern6(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageSevenForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_SEVEN_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern7(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayImageEightForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_IMAGE_EIGHT_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayFringePattern8(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _displayWhiteLightForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_DISPLAYING_WHITE_LIGHT_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _ebleProcessor.displayWhiteLight(EthernetBasedLightEngineEnum.ENGINE_1);
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _singleCaptureImageForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_CAPTURING_IMAGE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
            opticalPointToPointScan.setId(1);
            opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
            opticalPointToPointScan.setRegionPositionName("captureImageModuleTwo");
            opticalPointToPointScan.setOpticalCameraRectangleName("captureImageModuleTwo");
            opticalPointToPointScan.setPointPositionName("captureImageModuleTwo");
            opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(new StagePositionMutable());
            opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
            opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
            
            if (_ebleProcessor.isSimulationModeOn() == false)
            {
              PspEngine.getInstance().performHeightMapOperation(opticalPointToPointScan, "captureImageModuleTwo", "captureImageModuleTwo", false);
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                displayCaptureImageMessage(PspModuleEnum.REAR, true);
                _singleCaptureImageForModuleTwoButton.setEnabled(true);
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _singleCaptureImageForModuleTwoButton.setEnabled(true);
                
                displayCaptureImageMessage(PspModuleEnum.REAR, false);
                
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
          finally
          {
            _singleCaptureImageForModuleTwoButton.setEnabled(true);
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  private ActionListener _loopCaptureImageForModuleTwoButtonActionPerformed = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      final BusyCancelDialog hardwareBusyCancelDialog = new BusyCancelDialog(_mainUI,
          StringLocalizer.keyToString("SERVICE_EBLE_CAPTURING_IMAGE_KEY"),
           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
          true);

      hardwareBusyCancelDialog.pack();
      SwingUtils.centerOnComponent(hardwareBusyCancelDialog, _mainUI);

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
            opticalPointToPointScan.setId(1);
            opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
            opticalPointToPointScan.setRegionPositionName("captureImageModuleTwo");
            opticalPointToPointScan.setOpticalCameraRectangleName("captureImageModuleTwo");
            opticalPointToPointScan.setPointPositionName("captureImageModuleTwo");
            opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(new StagePositionMutable());
            opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
            opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
            
            for(int i=1; i <= TOTAL_PROCESSING_LOOP; ++i)
            {
              if (_ebleProcessor.isSimulationModeOn() == false)
              {
                PspEngine.getInstance().performHeightMapOperation(opticalPointToPointScan, "CaptureImageModuleTwo", "CaptureImageModuleTwo", false);
              }
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  displayCaptureImageMessage(PspModuleEnum.REAR, true);
                }
              });
            }
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _loopCaptureImageForModuleTwoButton.setEnabled(true);
                hardwareBusyCancelDialog.setVisible(false);
              }
            });
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _loopCaptureImageForModuleTwoButton.setEnabled(true);
                
                displayCaptureImageMessage(PspModuleEnum.REAR, false);
                
                hardwareBusyCancelDialog.setVisible(false);
                _parent.handleXrayTesterException(xte);
                return;
              }
            });
          }
          finally
          {
            _loopCaptureImageForModuleTwoButton.setEnabled(true);
          }
        }
      });
      hardwareBusyCancelDialog.setVisible(true);
    }
  };
  
  /**
   * @author Erica Wheatcroft
   */
  public EthernetBasedLightEnginePanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent!=null);
    _parent = parent;

    _mainUI = MainMenuGui.getInstance();
    createPanel();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void start() 
  {
    // get an instance to the digital io interface
    _ebleProcessor = EthernetBasedLightEngineProcessor.getInstance();
    
    // lets check to see if we need to start it up.
    try
    {
      _initialized = ! _ebleProcessor.isStartupRequired();
    }
    catch (XrayTesterException xte)
    {
      _parent.handleXrayTesterException(xte);
    }

    enableButtons();
    addListeners();
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isReadyToFinish() 
  {
    return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void finish() 
  {
    removeListeners();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void addListeners()
  {    
    _initializeEbleHardwareButton.addActionListener(_initializeElbeButtonActionPerformed);
    _stopEbleHardwareButton.addActionListener(_stopElbeButtonActionPerformed);
    
    _displayImageOneForModuleOneButton.addActionListener(_displayImageOneForModuleOneButtonActionPerformed);
    _displayImageTwoForModuleOneButton.addActionListener(_displayImageTwoForModuleOneButtonActionPerformed);
    _displayImageThreeForModuleOneButton.addActionListener(_displayImageThreeForModuleOneButtonActionPerformed);
    _displayImageFourForModuleOneButton.addActionListener(_displayImageFourForModuleOneButtonActionPerformed);
    _displayImageFiveForModuleOneButton.addActionListener(_displayImageFiveForModuleOneButtonActionPerformed);
    _displayImageSixForModuleOneButton.addActionListener(_displayImageSixForModuleOneButtonActionPerformed);
    _displayImageSevenForModuleOneButton.addActionListener(_displayImageSevenForModuleOneButtonActionPerformed);
    _displayImageEightForModuleOneButton.addActionListener(_displayImageEightForModuleOneButtonActionPerformed);
    _displayWhiteLightForModuleOneButton.addActionListener(_displayWhiteLightForModuleOneButtonActionPerformed);
    _turnOffLEDModuleOneButton.addActionListener(_turnOffLEDModuleOneButtonActionPerformed);    
    _singleCaptureImageForModuleOneButton.addActionListener(_singleCaptureImageForModuleOneButtonActionPerformed);
    _loopCaptureImageForModuleOneButton.addActionListener(_loopCaptureImageForModuleOneButtonActionPerformed);
    
    _displayImageOneForModuleTwoButton.addActionListener(_displayImageOneForModuleTwoButtonActionPerformed);
    _displayImageTwoForModuleTwoButton.addActionListener(_displayImageTwoForModuleTwoButtonActionPerformed);
    _displayImageThreeForModuleTwoButton.addActionListener(_displayImageThreeForModuleTwoButtonActionPerformed);
    _displayImageFourForModuleTwoButton.addActionListener(_displayImageFourForModuleTwoButtonActionPerformed);
    _displayImageFiveForModuleTwoButton.addActionListener(_displayImageFiveForModuleTwoButtonActionPerformed);
    _displayImageSixForModuleTwoButton.addActionListener(_displayImageSixForModuleTwoButtonActionPerformed);
    _displayImageSevenForModuleTwoButton.addActionListener(_displayImageSevenForModuleTwoButtonActionPerformed);
    _displayImageEightForModuleTwoButton.addActionListener(_displayImageEightForModuleTwoButtonActionPerformed);
    _displayWhiteLightForModuleTwoButton.addActionListener(_displayWhiteLightForModuleTwoButtonActionPerformed);
    _turnOffLEDModuleTwoButton.addActionListener(_turnOffLEDModuleTwoButtonActionPerformed);    
    _singleCaptureImageForModuleTwoButton.addActionListener(_singleCaptureImageForModuleTwoButtonActionPerformed);
    _loopCaptureImageForModuleTwoButton.addActionListener(_loopCaptureImageForModuleTwoButtonActionPerformed);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void removeListeners()
  {    
    _initializeEbleHardwareButton.removeActionListener(_initializeElbeButtonActionPerformed);
    _stopEbleHardwareButton.removeActionListener(_stopElbeButtonActionPerformed);
    _turnOffLEDModuleOneButton.removeActionListener(_turnOffLEDModuleOneButtonActionPerformed);
    _displayImageOneForModuleOneButton.removeActionListener(_displayImageOneForModuleOneButtonActionPerformed);
    _displayImageTwoForModuleOneButton.removeActionListener(_displayImageTwoForModuleOneButtonActionPerformed);
    _displayImageThreeForModuleOneButton.removeActionListener(_displayImageThreeForModuleOneButtonActionPerformed);
    _displayImageFourForModuleOneButton.removeActionListener(_displayImageFourForModuleOneButtonActionPerformed);
    _displayImageFiveForModuleOneButton.removeActionListener(_displayImageFiveForModuleOneButtonActionPerformed);
    _displayImageSixForModuleOneButton.removeActionListener(_displayImageSixForModuleOneButtonActionPerformed);
    _displayImageSevenForModuleOneButton.removeActionListener(_displayImageSevenForModuleOneButtonActionPerformed);
    _displayImageEightForModuleOneButton.removeActionListener(_displayImageEightForModuleOneButtonActionPerformed);
    _displayWhiteLightForModuleOneButton.removeActionListener(_displayWhiteLightForModuleOneButtonActionPerformed);    
    _singleCaptureImageForModuleOneButton.removeActionListener(_singleCaptureImageForModuleOneButtonActionPerformed);
    _loopCaptureImageForModuleOneButton.removeActionListener(_loopCaptureImageForModuleOneButtonActionPerformed);
    
    _displayImageOneForModuleTwoButton.removeActionListener(_displayImageOneForModuleTwoButtonActionPerformed);
    _displayImageTwoForModuleTwoButton.removeActionListener(_displayImageTwoForModuleTwoButtonActionPerformed);
    _displayImageThreeForModuleTwoButton.removeActionListener(_displayImageThreeForModuleTwoButtonActionPerformed);
    _displayImageFourForModuleTwoButton.removeActionListener(_displayImageFourForModuleTwoButtonActionPerformed);
    _displayImageFiveForModuleTwoButton.removeActionListener(_displayImageFiveForModuleTwoButtonActionPerformed);
    _displayImageSixForModuleTwoButton.removeActionListener(_displayImageSixForModuleTwoButtonActionPerformed);
    _displayImageSevenForModuleTwoButton.removeActionListener(_displayImageSevenForModuleTwoButtonActionPerformed);
    _displayImageEightForModuleTwoButton.removeActionListener(_displayImageEightForModuleTwoButtonActionPerformed);
    _displayWhiteLightForModuleTwoButton.removeActionListener(_displayWhiteLightForModuleTwoButtonActionPerformed);
    _turnOffLEDModuleTwoButton.removeActionListener(_turnOffLEDModuleTwoButtonActionPerformed);    
    _singleCaptureImageForModuleTwoButton.removeActionListener(_singleCaptureImageForModuleTwoButtonActionPerformed);
    _loopCaptureImageForModuleTwoButton.removeActionListener(_loopCaptureImageForModuleTwoButtonActionPerformed);
  }
  
  /**
   * @author Cheah Lee Herngt
   */
  private void createPanel()
  {
    setLayout(new BorderLayout(10,5));
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 15));
    
    add(createButtonPanel(),BorderLayout.NORTH);
    
    JPanel mainModulePanel = new JPanel();
    mainModulePanel.setLayout(new BorderLayout());
    mainModulePanel.add(createModulePanel(), BorderLayout.NORTH);
    mainModulePanel.add(createStatusPanel(), BorderLayout.CENTER);
    
    add(mainModulePanel, BorderLayout.CENTER);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private JPanel createButtonPanel()
  {
    JPanel buttonPanel = new JPanel(new GridLayout(1,2,5,5));
    buttonPanel.setLayout(new FlowLayout());
    _initializeEbleHardwareButton.setText(StringLocalizer.keyToString("SERVICE_INITIALIZE_EBLE_KEY"));
    _stopEbleHardwareButton.setText(StringLocalizer.keyToString("SERVICE_STOP_EBLE_KEY"));
    buttonPanel.add(_initializeEbleHardwareButton);
    buttonPanel.add(_stopEbleHardwareButton);
    
    return buttonPanel;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private JPanel createModulePanel()
  {
    JPanel centerPanel = new JPanel(new GridLayout(1,2,5,5));
    
    JPanel leftModulePanel = new JPanel();
    leftModulePanel.setBorder( BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(148, 145, 140)), StringLocalizer.keyToString("ACD_PSP_FRONT_MODULE_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    leftModulePanel.setLayout(new GridLayout(3,4,30,5));
    _displayImageOneForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_ONE_KEY"));
    _displayImageTwoForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_TWO_KEY"));
    _displayImageThreeForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_THREE_KEY"));
    _displayImageFourForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_FOUR_KEY"));
    _displayImageFiveForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_FIVE_KEY"));
    _displayImageSixForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_SIX_KEY"));
    _displayImageSevenForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_SEVEN_KEY"));
    _displayImageEightForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_EIGHT_KEY"));
    _displayWhiteLightForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_WHITE_LIGHT_KEY"));
    _turnOffLEDModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_TURN_OFF_LED_KEY"));
    _singleCaptureImageForModuleOneButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_SINGLE_CAPTURE_IMAGE_KEY"));    
    _loopCaptureImageForModuleOneButton.setText(StringLocalizer.keyToString(new LocalizedString("SERVICE_EBLE_LOOP_CAPTURE_IMAGE_KEY", new Object[]{TOTAL_PROCESSING_LOOP})));
    leftModulePanel.add(_displayImageOneForModuleOneButton);
    leftModulePanel.add(_displayImageTwoForModuleOneButton);
    leftModulePanel.add(_displayImageThreeForModuleOneButton);
    leftModulePanel.add(_displayImageFourForModuleOneButton);
    leftModulePanel.add(_displayImageFiveForModuleOneButton);
    leftModulePanel.add(_displayImageSixForModuleOneButton);
    leftModulePanel.add(_displayImageSevenForModuleOneButton);
    leftModulePanel.add(_displayImageEightForModuleOneButton);
    leftModulePanel.add(_displayWhiteLightForModuleOneButton);
    leftModulePanel.add(_turnOffLEDModuleOneButton);
    leftModulePanel.add(_singleCaptureImageForModuleOneButton);
    leftModulePanel.add(_loopCaptureImageForModuleOneButton);
    
    JPanel rightModulePanel = new JPanel();
    rightModulePanel.setBorder( BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
      new Color(148, 145, 140)), StringLocalizer.keyToString("ACD_PSP_REAR_MODULE_KEY")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    rightModulePanel.setLayout(new GridLayout(3,4,30,20));
    _displayImageOneForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_ONE_KEY"));
    _displayImageTwoForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_TWO_KEY"));
    _displayImageThreeForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_THREE_KEY"));
    _displayImageFourForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_FOUR_KEY"));
    _displayImageFiveForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_FIVE_KEY"));
    _displayImageSixForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_SIX_KEY"));
    _displayImageSevenForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_SEVEN_KEY"));
    _displayImageEightForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_IMAGE_EIGHT_KEY"));
    _displayWhiteLightForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_DISPLAY_WHITE_LIGHT_KEY"));
    _turnOffLEDModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_TURN_OFF_LED_KEY"));
    _singleCaptureImageForModuleTwoButton.setText(StringLocalizer.keyToString("SERVICE_EBLE_SINGLE_CAPTURE_IMAGE_KEY"));
    _loopCaptureImageForModuleTwoButton.setText(StringLocalizer.keyToString(new LocalizedString("SERVICE_EBLE_LOOP_CAPTURE_IMAGE_KEY", new Object[]{TOTAL_PROCESSING_LOOP})));
    rightModulePanel.add(_displayImageOneForModuleTwoButton);
    rightModulePanel.add(_displayImageTwoForModuleTwoButton);
    rightModulePanel.add(_displayImageThreeForModuleTwoButton);
    rightModulePanel.add(_displayImageFourForModuleTwoButton);
    rightModulePanel.add(_displayImageFiveForModuleTwoButton);
    rightModulePanel.add(_displayImageSixForModuleTwoButton);
    rightModulePanel.add(_displayImageSevenForModuleTwoButton);
    rightModulePanel.add(_displayImageEightForModuleTwoButton);
    rightModulePanel.add(_displayWhiteLightForModuleTwoButton);
    rightModulePanel.add(_turnOffLEDModuleTwoButton);
    rightModulePanel.add(_singleCaptureImageForModuleTwoButton);
    rightModulePanel.add(_loopCaptureImageForModuleTwoButton);
    
    // get PspSetting (whether to connect to Both PSP Module, or front module, or rear module)
    _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));    
    
    centerPanel.add(leftModulePanel);
    centerPanel.add(rightModulePanel);
    
    return centerPanel;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private JPanel createStatusPanel()
  {
    JPanel statusPanel = new JPanel(new BorderLayout());
    statusPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                             StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_MESSAGES_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    statusPanel.add(_statusPaneScrollPane, BorderLayout.CENTER);
    _statusPaneScrollPane.setOpaque(false);
    _statusPaneScrollPane.getViewport().add(_statusPane);
    
    return statusPanel;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void displayHardwareTriggerMessage(PspModuleEnum moduleEnum, boolean enableHardwareTriggerSuccessful)
  {
    Assert.expect(moduleEnum != null);
    
    if (enableHardwareTriggerSuccessful)
    {
      _statusPane.append("\n" + getFormattedOperationDateTime() + "Module " + moduleEnum.toString() + " : Optical Camera Trigger SUCCESS");
    }
    else
    {
      _statusPane.append("\n" + getFormattedOperationDateTime() + "Module " + moduleEnum.toString() + " : Optical Camera Trigger FAILED");
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void displayCaptureImageMessage(PspModuleEnum moduleEnum, boolean captureImageSuccessful)
  {
    Assert.expect(moduleEnum != null);
    
    if (captureImageSuccessful)
    {
      _statusPane.append("\n" + getFormattedOperationDateTime() + "Module " + moduleEnum.toString() + " : Capture Image SUCCESS");
    }
    else
    {
      _statusPane.append("\n" + getFormattedOperationDateTime() + "Module " + moduleEnum.toString() + " : Capture Image FAILED");
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private String getFormattedOperationDateTime()
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd   HH:mm:ss");
    return "[" + dateFormat.format(new Date()) + "] ";
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableButtons()
  {
    if(_initialized)
    {
      _initializeEbleHardwareButton.setEnabled(false);
      _stopEbleHardwareButton.setEnabled(true);
      
      if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH) ||
          _settingEnum.equals(PspSettingEnum.SETTING_FRONT))
      {
        _displayImageOneForModuleOneButton.setEnabled(true);
        _displayImageTwoForModuleOneButton.setEnabled(true);
        _displayImageThreeForModuleOneButton.setEnabled(true);
        _displayImageFourForModuleOneButton.setEnabled(true);
        _displayImageFiveForModuleOneButton.setEnabled(true);
        _displayImageSixForModuleOneButton.setEnabled(true);
        _displayImageSevenForModuleOneButton.setEnabled(true);
        _displayImageEightForModuleOneButton.setEnabled(true);
        _displayWhiteLightForModuleOneButton.setEnabled(true);
        _turnOffLEDModuleOneButton.setEnabled(true);
        _singleCaptureImageForModuleOneButton.setEnabled(true);
        _loopCaptureImageForModuleOneButton.setEnabled(true);
      }
      else
      {
        _displayImageOneForModuleOneButton.setEnabled(false);
        _displayImageTwoForModuleOneButton.setEnabled(false);
        _displayImageThreeForModuleOneButton.setEnabled(false);
        _displayImageFourForModuleOneButton.setEnabled(false);
        _displayImageFiveForModuleOneButton.setEnabled(false);
        _displayImageSixForModuleOneButton.setEnabled(false);
        _displayImageSevenForModuleOneButton.setEnabled(false);
        _displayImageEightForModuleOneButton.setEnabled(false);
        _displayWhiteLightForModuleOneButton.setEnabled(false);
        _turnOffLEDModuleOneButton.setEnabled(false);
        _singleCaptureImageForModuleOneButton.setEnabled(false);
        _loopCaptureImageForModuleOneButton.setEnabled(false);
      }
      
      if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH) ||
          _settingEnum.equals(PspSettingEnum.SETTING_REAR))
      {
        _displayImageOneForModuleTwoButton.setEnabled(true);
        _displayImageTwoForModuleTwoButton.setEnabled(true);
        _displayImageThreeForModuleTwoButton.setEnabled(true);
        _displayImageFourForModuleTwoButton.setEnabled(true);
        _displayImageFiveForModuleTwoButton.setEnabled(true);
        _displayImageSixForModuleTwoButton.setEnabled(true);
        _displayImageSevenForModuleTwoButton.setEnabled(true);
        _displayImageEightForModuleTwoButton.setEnabled(true);
        _displayWhiteLightForModuleTwoButton.setEnabled(true);
        _turnOffLEDModuleTwoButton.setEnabled(true);
        _singleCaptureImageForModuleTwoButton.setEnabled(true);
        _loopCaptureImageForModuleTwoButton.setEnabled(true);
      }
      else
      {
        _displayImageOneForModuleTwoButton.setEnabled(false);
        _displayImageTwoForModuleTwoButton.setEnabled(false);
        _displayImageThreeForModuleTwoButton.setEnabled(false);
        _displayImageFourForModuleTwoButton.setEnabled(false);
        _displayImageFiveForModuleTwoButton.setEnabled(false);
        _displayImageSixForModuleTwoButton.setEnabled(false);
        _displayImageSevenForModuleTwoButton.setEnabled(false);
        _displayImageEightForModuleTwoButton.setEnabled(false);
        _displayWhiteLightForModuleTwoButton.setEnabled(false);
        _turnOffLEDModuleTwoButton.setEnabled(false);
        _singleCaptureImageForModuleTwoButton.setEnabled(false);
        _loopCaptureImageForModuleTwoButton.setEnabled(false);
      }      
    }
    else
    {
      _initializeEbleHardwareButton.setEnabled(true);
      _stopEbleHardwareButton.setEnabled(false);
      
      _displayImageOneForModuleOneButton.setEnabled(false);
      _displayImageTwoForModuleOneButton.setEnabled(false);
      _displayImageThreeForModuleOneButton.setEnabled(false);
      _displayImageFourForModuleOneButton.setEnabled(false);
      _displayImageFiveForModuleOneButton.setEnabled(false);
      _displayImageSixForModuleOneButton.setEnabled(false);
      _displayImageSevenForModuleOneButton.setEnabled(false);
      _displayImageEightForModuleOneButton.setEnabled(false);
      _displayWhiteLightForModuleOneButton.setEnabled(false);
      _turnOffLEDModuleOneButton.setEnabled(false);
      _singleCaptureImageForModuleOneButton.setEnabled(false);
      _loopCaptureImageForModuleOneButton.setEnabled(false);
      
      _displayImageOneForModuleTwoButton.setEnabled(false);
      _displayImageTwoForModuleTwoButton.setEnabled(false);
      _displayImageThreeForModuleTwoButton.setEnabled(false);
      _displayImageFourForModuleTwoButton.setEnabled(false);
      _displayImageFiveForModuleTwoButton.setEnabled(false);
      _displayImageSixForModuleTwoButton.setEnabled(false);
      _displayImageSevenForModuleTwoButton.setEnabled(false);
      _displayImageEightForModuleTwoButton.setEnabled(false);
      _displayWhiteLightForModuleTwoButton.setEnabled(false);
      _turnOffLEDModuleTwoButton.setEnabled(false);
      _singleCaptureImageForModuleTwoButton.setEnabled(false);
      _loopCaptureImageForModuleTwoButton.setEnabled(false);
    }
  }
}
