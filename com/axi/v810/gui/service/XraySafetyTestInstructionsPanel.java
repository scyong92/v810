package com.axi.v810.gui.service;

import java.awt.*;
import java.net.*;

import javax.swing.*;

import com.axi.util.*;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;
import java.io.IOException;
import com.axi.v810.gui.MessageDialog;
import com.axi.v810.gui.mainMenu.MainMenuGui;

/**
 * This class supports an Xray Safety Test instruction panel that has single set of user instructions
 * with no other UI controls.
 *
 * @author George Booth
 */
public class XraySafetyTestInstructionsPanel extends XraySafetyTestAbstractPanel
{
  private JEditorPane _warningPane;
  private JScrollPane _warningScrollPane;

  private final int _MINIMUM_WIDTH = 100;
  private final int _MINIMUM_HEIGHT = 50;
  private final int _PREFERRED_WIDTH = 820;
  // the followimg heights should add up to about 520 for a default 1024x768 screen size
  // (takes into account the single BoarderLayout vertical gap)
  private final int _PREFERRED_WARNING_HEIGHT = 520;

  private String _documentKey;

  /**
   * @author George Booth
   */
  public XraySafetyTestInstructionsPanel(ServiceEnvironmentPanel parent, String documentKey)
  {
    super(parent);
    Assert.expect(parent != null);
    Assert.expect(documentKey != null);
    _documentKey = documentKey;

    this.setLayout(new BorderLayout(10, 10));

    // set up warning
    _warningPane = new JEditorPane();
    _warningPane.setEditable(false);
    _warningPane.setBackground(this.getBackground());
    _warningPane.addHyperlinkListener(new HyperlinkListener()
    {
      public void hyperlinkUpdate(HyperlinkEvent e)
      {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
        {
          URL url = e.getURL();
          try
          {
            BrowserLauncher.openURLInNewWindow(url.toString());
          }
          catch (IOException ioe)
          {
            MessageDialog.reportIOError(MainMenuGui.getInstance(), ioe.getLocalizedMessage());
          }
        }
      }
    });

    _warningScrollPane = new JScrollPane();
    _warningScrollPane.getViewport().add(_warningPane);
    _warningScrollPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_WARNING_HEIGHT));
    _warningScrollPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    this.add(_warningScrollPane, BorderLayout.CENTER);

    URL warning = convertKeyToURL(_documentKey);
    displayURL(_warningPane, warning);
  }

  public void handleHyperlinkEvent(HyperlinkEvent e)
  {
    URL url = e.getURL();
  }

  /**
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("XraySafetyTestInstructionsPanel.start()");
  }

  /**
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("XraySafetyTestInstructionsPanel.finish()");
  }

}
