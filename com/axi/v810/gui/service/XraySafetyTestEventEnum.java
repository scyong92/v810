package com.axi.v810.gui.service;

import com.axi.v810.gui.*;

/**
 * This class defines XraySafetyTestEventEnums for observer/observable communications
 * within the Xray Safety Test classes.  These events are used with the GuiObservable.
 *
 * @author George Booth
 */
public class XraySafetyTestEventEnum extends GuiEventEnum
{
  private static int _index = -1;

  public static XraySafetyTestEventEnum TEST_SELECTED = new XraySafetyTestEventEnum(++_index);
  public static XraySafetyTestEventEnum SHUTDOWN = new XraySafetyTestEventEnum(++_index);
  public static XraySafetyTestEventEnum NORMAL_POWER = new XraySafetyTestEventEnum(++_index);
  public static XraySafetyTestEventEnum HIGH_POWER = new XraySafetyTestEventEnum(++_index);

  /**
   * @author George Booth
   */
  private XraySafetyTestEventEnum(int id)
  {
    super(id);
  }
}
