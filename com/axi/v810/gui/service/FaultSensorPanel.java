package com.axi.v810.gui.service;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

class FaultSensorPanel extends JPanel
{

  // Variables that control the 5DX here
  private boolean _stageDisabled           = false;
  private boolean _motionControllerAborted = false;
  private boolean _motionAmpPowerOff       = false;
  private boolean _motionAmpInFaultState   = false;
  private boolean _xMotionAmpNotReady      = false;
  private boolean _y1MotionAmpNotReady     = false;
  private boolean _y2MotionAmpNotReady     = false;

  private boolean _xrayInterlock1Open      = false;
  private boolean _xrayInterlock2Open      = false;
  private boolean _motionInterlockOpen     = false;

  // all components go here
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private JPanel _centerPanel = new JPanel();
  private JPanel _westEmptyBufferPanel = new JPanel();
  private JPanel _stagePanel = new JPanel();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private BorderLayout _smemaPanelBorderLayout = new BorderLayout();
  private JPanel _ampStatusPanel = new JPanel();
  private JPanel _overallStagePanel = new JPanel();
  private FlowLayout _stageRelayFlowLayout = new FlowLayout();
  private FlowLayout _mainAmpStatusFlowLayout = new FlowLayout();
  private JPanel _stageRelayPanel = new JPanel();
  private JPanel _mainAmpStatusPanel = new JPanel();
  private JLabel _stageRelayStateLabel = new JLabel();
  private JLabel _mainAmpStatusFaultStateLabel = new JLabel();
  private GridLayout _overallStageGridLayout = new GridLayout();
  private FlowLayout _xAmpStatusFlowLayout = new FlowLayout();
  private FlowLayout _motionControllerFlowLayout = new FlowLayout();
  private JPanel _xAmpStatusPanel = new JPanel();
  private JPanel _motionControllerPanel = new JPanel();
  private JLabel _xAmpStatusDescriptionLabel = new JLabel();
  private JLabel _xAmpStatusStateLabel = new JLabel();
  private JLabel _motionControllerDescriptionLabel = new JLabel();
  private JLabel _motionControllerStateLabel = new JLabel();
  private FlowLayout _motionInterlockFlowLayout = new FlowLayout();
  private FlowLayout _xrayInterlock1FlowLayout = new FlowLayout();
  private JPanel _motionInterlockPanel = new JPanel();
  private JPanel _xrayInterlock1Panel = new JPanel();
  private JLabel _motionInterlockDescriptionLabel = new JLabel();
  private JLabel _motionInterlockStateLabel = new JLabel();
  private JLabel _xrayInterlock1DescriptionLabel = new JLabel();
  private JLabel _xrayInterlock1StateLabel = new JLabel();
  private JPanel _interlocksGridPanel = new JPanel();
  private GridLayout _interlocksGridLayout = new GridLayout();
  private JPanel _interlocksPanel = new JPanel();
  private JLabel _stageRelayDescriptionLabel = new JLabel();
  private JLabel _mainAmpStatusDescriptionLabel = new JLabel();
  private JPanel _interlocksMainPanel = new JPanel();
  private BorderLayout _interlocksBorderLayout = new BorderLayout();


  private Border _stageBorder;
  private TitledBorder _stageTitledBorder;
  private Border _interlockBorder;
  private TitledBorder _interlocksTitledBorder;
  private JPanel _northEmptyBufferPanel = new JPanel();
  private FlowLayout _y2AmpStatusFlowLayout = new FlowLayout();
  private JLabel _y2AmpStatusStateLabel = new JLabel();
  private JPanel _y2AmpStatusPanel = new JPanel();
  private JLabel _y2AmpStatusDescriptionLabel = new JLabel();
  private FlowLayout _y1AmpStatusFlowLayout = new FlowLayout();
  private JLabel _y1AmpStatusStateLabel = new JLabel();
  private JPanel _y1AmpStatusPanel = new JPanel();
  private JLabel _y1AmpStatusDescriptionLabel = new JLabel();
  private JLabel _xrayInterlock2DescriptionLabel = new JLabel();
  private JPanel _xrayInterlock2Panel = new JPanel();
  private FlowLayout _xrayInterlock2FlowLayout1 = new FlowLayout();
  private JLabel _xrayInterlock2StateLabel = new JLabel();
  private JPanel _stageMainPanel = new JPanel();
  private BorderLayout _stageMainBorderLayout = new BorderLayout();
  private JLabel _mainAmpStatusPowerStateLabel = new JLabel();
  private JPanel _lineSeparaterPanel = new JPanel();
  private JLabel _lineLabel = new JLabel();
  private GridLayout _ampStatusGridLayout = new GridLayout();
  private FlowLayout flowLayout1 = new FlowLayout();

  FaultSensorPanel()
  {
    jbInit();
  }

  private void jbInit()
  {
    _stageBorder = new EtchedBorder(EtchedBorder.RAISED,Color.white,new java.awt.Color(134, 134, 134));
    _stageTitledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new java.awt.Color(134, 134, 134)),"Stage");
    _interlockBorder = new EtchedBorder(EtchedBorder.RAISED,Color.white,new java.awt.Color(134, 134, 134));
    _interlocksTitledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.RAISED,Color.white,new java.awt.Color(134, 134, 134)),"Interlocks");
    this.setLayout(_mainPanelBorderLayout);
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _stagePanel.setLayout(_smemaPanelBorderLayout);
    _stageRelayFlowLayout.setAlignment(FlowLayout.LEFT);
    _stageRelayFlowLayout.setHgap(10);
    _mainAmpStatusFlowLayout.setAlignment(FlowLayout.LEFT);
    _mainAmpStatusFlowLayout.setHgap(10);
    _stageRelayPanel.setLayout(_stageRelayFlowLayout);
    _mainAmpStatusPanel.setLayout(_mainAmpStatusFlowLayout);
    _stageRelayStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));

    _mainAmpStatusFaultStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));


    _overallStagePanel.setLayout(_overallStageGridLayout);
    _overallStageGridLayout.setRows(3);
    _ampStatusPanel.setLayout(_ampStatusGridLayout);
    _xAmpStatusFlowLayout.setHgap(10);
    _xAmpStatusFlowLayout.setAlignment(FlowLayout.LEFT);
    _motionControllerFlowLayout.setHgap(10);
    _motionControllerFlowLayout.setAlignment(FlowLayout.LEFT);
    _xAmpStatusPanel.setLayout(_xAmpStatusFlowLayout);
    _motionControllerPanel.setLayout(_motionControllerFlowLayout);
    _xAmpStatusDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _xAmpStatusDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _xAmpStatusDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _xAmpStatusDescriptionLabel.setText("X Servo Module:");
    _xAmpStatusStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));

    _motionControllerDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _motionControllerDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _motionControllerDescriptionLabel.setText("Motion Controller PCI 1:");
    _motionControllerStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));


    _motionInterlockFlowLayout.setAlignment(FlowLayout.LEFT);
    _motionInterlockFlowLayout.setHgap(10);
    _xrayInterlock1FlowLayout.setAlignment(FlowLayout.LEFT);
    _xrayInterlock1FlowLayout.setHgap(10);
    _motionInterlockPanel.setLayout(_motionInterlockFlowLayout);
    _xrayInterlock1Panel.setLayout(_xrayInterlock1FlowLayout);
    _motionInterlockDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _motionInterlockDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _motionInterlockDescriptionLabel.setText("Motion Enable Circuit:");
    _motionInterlockStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));

    _xrayInterlock1DescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _xrayInterlock1DescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _xrayInterlock1DescriptionLabel.setText("Safety Interlock Chain 1:");
    _xrayInterlock1StateLabel.setFont(new java.awt.Font("Dialog", 1, 16));

    _interlocksGridPanel.setLayout(_interlocksGridLayout);
    _interlocksGridLayout.setRows(3);
    _smemaPanelBorderLayout.setVgap(20);
    _stageRelayDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _stageRelayDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _stageRelayDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _stageRelayDescriptionLabel.setText("Stage Main Motion:");
    _mainAmpStatusDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _mainAmpStatusDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _mainAmpStatusDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _mainAmpStatusDescriptionLabel.setText("Servo Modules Status:");

    _interlocksPanel.setLayout(_interlocksBorderLayout);
    _centerPanelBorderLayout.setHgap(60);

    _y2AmpStatusFlowLayout.setAlignment(FlowLayout.LEFT);
    _y2AmpStatusFlowLayout.setHgap(10);

    _y2AmpStatusStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _y2AmpStatusPanel.setLayout(_y2AmpStatusFlowLayout);
    _y2AmpStatusDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _y2AmpStatusDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _y2AmpStatusDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _y2AmpStatusDescriptionLabel.setText("Y2 Servo Module:");
    _y1AmpStatusFlowLayout.setAlignment(FlowLayout.LEFT);
    _y1AmpStatusFlowLayout.setHgap(10);


    _y1AmpStatusStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _y1AmpStatusPanel.setLayout(_y1AmpStatusFlowLayout);
    _y1AmpStatusDescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _y1AmpStatusDescriptionLabel.setPreferredSize(new Dimension(200, 22));

    _y1AmpStatusDescriptionLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _y1AmpStatusDescriptionLabel.setText("Y1 Servo Module:");
    _xrayInterlock2DescriptionLabel.setText("Safety Interlock Chain 2:");

    _xrayInterlock2DescriptionLabel.setFont(new java.awt.Font("Dialog", 0, 16));
    _xrayInterlock2DescriptionLabel.setPreferredSize(new Dimension(200, 22));
    _xrayInterlock2Panel.setLayout(_xrayInterlock2FlowLayout1);

    _xrayInterlock2FlowLayout1.setAlignment(FlowLayout.LEFT);
    _xrayInterlock2FlowLayout1.setHgap(10);
    _xrayInterlock2StateLabel.setFont(new java.awt.Font("Dialog", 1, 16));

    _stageMainPanel.setLayout(_stageMainBorderLayout);
    _stageMainPanel.setBorder(_stageTitledBorder);
    _interlocksMainPanel.setLayout(flowLayout1);
    _mainAmpStatusPowerStateLabel.setFont(new java.awt.Font("Dialog", 1, 16));
    _lineLabel.setFont(new java.awt.Font("Dialog", 1, 12));
    _lineLabel.setText("___________________________________________");
    _ampStatusGridLayout.setRows(3);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    _interlocksGridPanel.setBorder(_interlocksTitledBorder);
    this.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_stagePanel, BorderLayout.WEST);
    _stagePanel.add(_stageMainPanel, BorderLayout.NORTH);
    _stageMainPanel.add(_overallStagePanel, BorderLayout.NORTH);
    _overallStagePanel.add(_stageRelayPanel, null);
    _stageRelayPanel.add(_stageRelayDescriptionLabel, null);
    _stageRelayPanel.add(_stageRelayStateLabel, null);
    _overallStagePanel.add(_motionControllerPanel, null);
    _motionControllerPanel.add(_motionControllerDescriptionLabel, null);
    _motionControllerPanel.add(_motionControllerStateLabel, null);
    _overallStagePanel.add(_mainAmpStatusPanel, null);
    _mainAmpStatusPanel.add(_mainAmpStatusDescriptionLabel, null);
    _mainAmpStatusPanel.add(_mainAmpStatusPowerStateLabel, null);
    _mainAmpStatusPanel.add(_mainAmpStatusFaultStateLabel, null);
    _stageMainPanel.add(_ampStatusPanel, BorderLayout.SOUTH);
    _ampStatusPanel.add(_xAmpStatusPanel, null);
    _xAmpStatusPanel.add(_xAmpStatusDescriptionLabel, null);
    _xAmpStatusPanel.add(_xAmpStatusStateLabel, null);
    _ampStatusPanel.add(_y1AmpStatusPanel, null);
    _y1AmpStatusPanel.add(_y1AmpStatusDescriptionLabel, null);
    _y1AmpStatusPanel.add(_y1AmpStatusStateLabel, null);
    _ampStatusPanel.add(_y2AmpStatusPanel, null);
    _y2AmpStatusPanel.add(_y2AmpStatusDescriptionLabel, null);
    _y2AmpStatusPanel.add(_y2AmpStatusStateLabel, null);
    _stageMainPanel.add(_lineSeparaterPanel, BorderLayout.CENTER);
    _lineSeparaterPanel.add(_lineLabel, null);
    _centerPanel.add(_interlocksPanel, BorderLayout.CENTER);

    _interlocksPanel.add(_interlocksMainPanel, BorderLayout.NORTH);
    _interlocksMainPanel.add(_interlocksGridPanel, null);
    _interlocksGridPanel.add(_xrayInterlock1Panel, null);
    _xrayInterlock1Panel.add(_xrayInterlock1DescriptionLabel, null);
    _xrayInterlock1Panel.add(_xrayInterlock1StateLabel, null);
    _interlocksGridPanel.add(_xrayInterlock2Panel, null);
    _xrayInterlock2Panel.add(_xrayInterlock2DescriptionLabel, null);
    _xrayInterlock2Panel.add(_xrayInterlock2StateLabel, null);
    _interlocksGridPanel.add(_motionInterlockPanel, null);
    _motionInterlockPanel.add(_motionInterlockDescriptionLabel, null);
    _motionInterlockPanel.add(_motionInterlockStateLabel, null);

    this.add(_westEmptyBufferPanel, BorderLayout.WEST);
    this.add(_northEmptyBufferPanel, BorderLayout.NORTH);

    updateFromServer();
  }

  void updateFromServer()
  {
//    if (_hwStatusGui.isHardwareReady())
//    {
//      try
//      {
//        _stageDisabled = _hardwareStatus.isStageDisabled();
////    _motionControllerAborted = _hwStatusAdapter.isStageControllerAborted();
//        _motionAmpPowerOff = _hardwareStatus.isMotionAmpPowerOff();
//        _motionAmpInFaultState = _hardwareStatus.isMotionAmpInFaultState();
//        _xMotionAmpNotReady = _hardwareStatus.isXMotionAmpNotReady();
//        _y1MotionAmpNotReady = _hardwareStatus.isYMotionAmpNotReady();
////    _y2MotionAmpNotReady     = _hwStatusAdapter.isY2MotionAmpNotReady();
//
//        _xrayInterlock1Open = _hardwareStatus.isXrayInterlockChain1Open();
//        _xrayInterlock2Open = _hardwareStatus.isXrayInterlockChain2Open();
//        _motionInterlockOpen = _hardwareStatus.isMotionInterlockChainOpen();
//      }
//      catch (HardwareException ex)
//      {
//        //_hwStatusGui.handleHardwareError(ex);
//      }
//
//      updateStateLabels();
//    }
  }

  private void updateStateLabels()
  {
    if (_stageDisabled)
      _stageRelayStateLabel.setText("Disabled");
    else
      _stageRelayStateLabel.setText("Enabled");

    if (_motionControllerAborted)
      _motionControllerStateLabel.setText("Disabled");
    else
      _motionControllerStateLabel.setText("Enabled");

    if (_motionAmpPowerOff)
      _mainAmpStatusPowerStateLabel.setText("Power Off");
    else
      _mainAmpStatusPowerStateLabel.setText("Power On");

    if (_motionAmpInFaultState)
      _mainAmpStatusFaultStateLabel.setText("Fault");
    else
      _mainAmpStatusFaultStateLabel.setText("No Fault");


    if (_xMotionAmpNotReady)
     _xAmpStatusStateLabel.setText("Disabled");
    else
     _xAmpStatusStateLabel.setText("Enabled");

    if (_y1MotionAmpNotReady)
     _y1AmpStatusStateLabel.setText("Disabled");
    else
     _y1AmpStatusStateLabel.setText("Enabled");

    if (_y2MotionAmpNotReady)
     _y2AmpStatusStateLabel.setText("Disabled");
    else
     _y2AmpStatusStateLabel.setText("Enabled");

    if (_xrayInterlock1Open)
      _xrayInterlock1StateLabel.setText("Open");
    else
      _xrayInterlock1StateLabel.setText("Closed");

    if (_xrayInterlock2Open)
      _xrayInterlock2StateLabel.setText("Open");
    else
      _xrayInterlock2StateLabel.setText("Closed");

    if (_motionInterlockOpen)
      _motionInterlockStateLabel.setText("Open");
    else
      _motionInterlockStateLabel.setText("Closed");
  }

}

