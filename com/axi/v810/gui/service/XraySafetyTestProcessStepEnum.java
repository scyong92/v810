package com.axi.v810.gui.service;

/**
 * This class defines the types of X-ray Safety Test process steps.
 *
 * @author George Booth
 */
public class XraySafetyTestProcessStepEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static XraySafetyTestProcessStepEnum INSTRUCTION_STEP = new XraySafetyTestProcessStepEnum(++_index);
  public static XraySafetyTestProcessStepEnum INSTRUCTION_SHUTDOWN_STEP = new XraySafetyTestProcessStepEnum(++_index);
  public static XraySafetyTestProcessStepEnum INSTRUCTION_SHUTDOWN_INSTRUCTION_STEP = new XraySafetyTestProcessStepEnum(++_index);
  public static XraySafetyTestProcessStepEnum TEST_SELECTION_STEP = new XraySafetyTestProcessStepEnum(++_index);
  public static XraySafetyTestProcessStepEnum PROCESS_COMPLETE_STEP = new XraySafetyTestProcessStepEnum(++_index);

  /**
   * @author George Booth
   */
  private XraySafetyTestProcessStepEnum(int id)
  {
    super(id);
  }
}
