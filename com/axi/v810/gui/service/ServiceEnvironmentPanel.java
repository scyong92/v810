package com.axi.v810.gui.service;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.gui.undo.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * The service environment panel will allow the user to perform service operations on the Genesis system.
 *
 * @author Erica Wheatcroft
 */
public class ServiceEnvironmentPanel extends AbstractEnvironmentPanel
{
  // needed by abstract panel however will not be used
  private com.axi.v810.gui.undo.CommandManager _commandManager = com.axi.v810.gui.undo.CommandManager.getServiceInstance();

  private GuiObservable _guiObservable = GuiObservable.getInstance();
  private TaskPanelScreenEnum _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
  private TaskPanelScreenEnum _nextScreen = null;

  // the navigation button names.. set this up for the navigation panel. <html><center>User<br>Options
  private static String _confirmDiagnosticAdjustmentButtonName = StringLocalizer.keyToString("SERVICE_UI_CDA_BUTTON_NAME_KEY");
  private static String _subsystemStatusAndControlButtonName = StringLocalizer.keyToString("SERVICE_UI_SUBSYSTEM_STATAUS_CONTROL_BUTTON_NAME_KEY");
  private static String _xraySafetyTestButtonName = StringLocalizer.keyToString("SERVICE_UI_XRAY_SAFETY_TEST_BUTTON_NAME_KEY");

  // card layout strings. These strings are not seen by the user and will not be localized!
  private final static String _SUBSYSTEM_STATUS_AND_CONTROL_PANEL_NAME = "SUBSYSTEM_STATUS_AND_CONTROL";
  private final static String _CONFIRM_DIAGNOSTIC_ADJUSTMENT_PANEL_NAME = "CONFIRM_DIAGNOSTIC_ADJUST";
  private final static String _XRAY_SAFETY_TEST_PANEL_NAME = "XRAY_SAFETY_TEST";

  private String[] _buttonNames = {_confirmDiagnosticAdjustmentButtonName, _subsystemStatusAndControlButtonName,
                                   _xraySafetyTestButtonName};

  private ServiceEnvironmentNavigationPanel _navigationPanel = null;

  // now lets setup our task panels
  private ConfirmDiagnosticAdjustmentTaskPanel _confirmDiagnosticAdjustmentPanel = null;
  private SubsystemStatusAndControlTaskPanel _subsystemStatusAndControlPanel = null;
  private XraySafetyTestTaskPanel _XraySafetyTestPanel = null;

  // now lets create our service menu items
  private JMenu _serviceMenu = new JMenu(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MENU_KEY"));
  private JMenuItem _dataMiningWizardMenuItem = new JMenuItem(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MENU_COLLECT_LOG_FILES_MENU_ITEM_KEY"));

  private java.util.List<Boolean> _actionsMenuMenuItemsStatus = null;

  // icons used by all tasks
  private static ImageIcon _startImage;
  private static ImageIcon _stopImage;

  private static ServiceEnvironmentPanel _instance = null;
  /**
   * @author Erica Wheatcroft
   */
  public com.axi.v810.gui.undo.CommandManager getCommandManager()
  {
    return _commandManager;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ServiceEnvironmentPanel(MainMenuGui mainMenu)
  {
    super(mainMenu);

    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    // now lets create the navigation panel
    _navigationPanel = new ServiceEnvironmentNavigationPanel(_buttonNames, this);

    // create the icons used by this environment
    _startImage = Image5DX.getImageIcon(Image5DX.AT_START);
    _stopImage = Image5DX.getImageIcon(Image5DX.AT_STOP);

    // set the navigation panel
    setNavigationPanel(_navigationPanel);

    if (_confirmDiagnosticAdjustmentPanel == null)
    {
      _confirmDiagnosticAdjustmentPanel = new ConfirmDiagnosticAdjustmentTaskPanel(this);
      _centerPanel.add(_confirmDiagnosticAdjustmentPanel, _CONFIRM_DIAGNOSTIC_ADJUSTMENT_PANEL_NAME);
    }

    if (_subsystemStatusAndControlPanel == null)
    {
      _subsystemStatusAndControlPanel = new SubsystemStatusAndControlTaskPanel(this);
      _centerPanel.add(_subsystemStatusAndControlPanel, _SUBSYSTEM_STATUS_AND_CONTROL_PANEL_NAME);
    }

    if (_XraySafetyTestPanel == null)
    {
      _XraySafetyTestPanel = new XraySafetyTestTaskPanel(this);
      _centerPanel.add(_XraySafetyTestPanel, _XRAY_SAFETY_TEST_PANEL_NAME);
    }


  }

  /**
   * This is called when a task navigation button is clicked.
   * @author Erica Wheatcroft
   */
  public void navButtonClicked(String buttonName)
  {
    if (buttonName.equalsIgnoreCase(_confirmDiagnosticAdjustmentButtonName))
    {
      _nextScreen = TaskPanelScreenEnum.SERVICE_CDA;
    }
    else if (buttonName.equalsIgnoreCase(_subsystemStatusAndControlButtonName))
    {
      _nextScreen = TaskPanelScreenEnum.SERVICE_SSC;
    }
    else if (buttonName.equalsIgnoreCase(_xraySafetyTestButtonName))
    {
      _nextScreen = TaskPanelScreenEnum.SERVICE_XST;
    }
    TaskPanelChangeScreenCommand command = new
      TaskPanelChangeScreenCommand(this, _currentScreen, _nextScreen);
    _commandManager.execute(command);
    _currentScreen = _nextScreen;
    _guiObservable.stateChanged(null, _currentScreen);

  }

  /**
   * change to a different screen.
   * @author Erica Wheatcroft
   */
  public void switchToScreen(TaskPanelScreenEnum screen)
  {
    Assert.expect(screen != null);

    if (_currentScreen.equals(screen))
      return;

    if(screen.equals(screen.SERVICE_CDA))
    {
      switchToTask(_confirmDiagnosticAdjustmentPanel, _CONFIRM_DIAGNOSTIC_ADJUSTMENT_PANEL_NAME);
      _navigationPanel.highlightButton(_confirmDiagnosticAdjustmentButtonName);
    }
    else if(screen.equals(screen.SERVICE_SSC))
    {
      switchToTask(_subsystemStatusAndControlPanel, _SUBSYSTEM_STATUS_AND_CONTROL_PANEL_NAME);
      _navigationPanel.highlightButton(_subsystemStatusAndControlButtonName);
    }
    else if(screen.equals(screen.SERVICE_XST))
    {
      switchToTask(_XraySafetyTestPanel, _XRAY_SAFETY_TEST_PANEL_NAME);
      _navigationPanel.highlightButton(_xraySafetyTestButtonName);
    }
  }

  /**
   * The environment is now on top, ready to run and should perform startup operations such as registering listeners,
   * adding context menus and tool bars and starting its default task panel.
   *
   * @author Erica Wheatcroft
   */
  public void start()
  {
    // tell the interlock panel to always poll
    com.axi.v810.hardware.Interlock.getInstance().setTroubleShootModeOn();

    // add custom menus and tool bars for this environment
    createMenusAndToolBar();

    // start default task
    _navigationPanel.selectDefaultTask();

    //turn off the auto confirm and ajust system while we are in the service environment
    HardwareTaskEngine.getInstance().disableEvents();

    restoreLastUIState();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createMenusAndToolBar()
  {
//    startMenusAndEmptyToolBar();
    startMenusAndDisableToolBar();

    MainUIMenuBar menuBar = _mainUI.getMainMenuBar();
    menuBar.addMenu(_menuRequesterID, 2, _serviceMenu);
    menuBar.addMenuItem(_menuRequesterID, _serviceMenu, 0, _dataMiningWizardMenuItem);

    addToolBar();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void saveUIState()
  {
    ServiceGuiPersistence.getInstance().writeSettings();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void restoreLastUIState()
  {
    ServiceGuiPersistence.getInstance().readSettings();
  }

  /**
   * The user has requested an environment change.
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    if (_currentScreen.equals(TaskPanelScreenEnum.SERVICE_CDA))
    {
      return _confirmDiagnosticAdjustmentPanel.isReadyToFinish();
    }
    else if (_currentScreen.equals(TaskPanelScreenEnum.SERVICE_SSC))
    {
      return _subsystemStatusAndControlPanel.isReadyToFinish();
    }
    else if (_currentScreen.equals(TaskPanelScreenEnum.SERVICE_XST))
    {
      return _XraySafetyTestPanel.isReadyToFinish();
    }
    else
      Assert.expect(false);

    return false;
  }

  /**
   * The environment is about to be terminated and should perform any cleanup needed such as terminating the current
   * task panel, removing context menus and tool bars and unregistering listeners.
   *
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    // tell the interlock panel to stop polling
    com.axi.v810.hardware.Interlock.getInstance().setTroubleShootModeOff();

    // finish current task
    finishCurrentTaskPanel();

    // allow restart with default task even if it was the current task
    _currentScreen = TaskPanelScreenEnum.NULL_SCREEN;
    _navigationPanel.clearCurrentNavigation();

    // remove custom menus and toolbar components
    super.finishMenusAndToolBar();
    
    // remove status bar
    _mainUI.removeStatusBar();

    // switch the menus back
    _mainUI.getMainMenuBar().enableMenuItems(MainMenuGui.getInstance().getCurrentUserType(), true);

    //turn off the auto confirm and ajust system
    HardwareTaskEngine.getInstance().enableEvents();

    saveUIState();

    try
    {
      if (XrayTester.getInstance().isHardwareAvailable() == false)
        XrayTester.getInstance().clearSimulationMode();
    }
    catch (XrayTesterException xte)
    {
      _mainUI.handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  JMenu getMenu()
  {
    Assert.expect(_serviceMenu != null);
    return _serviceMenu;
  }

  /**
   * @author Erica Wheatcroft
   */
  Icon getStartImage()
  {
    Assert.expect(_startImage != null);
    return _startImage;
  }

  /**
   * @author Erica Wheatcroft
   */
  Icon getStopImage()
  {
    Assert.expect(_stopImage != null);
    return _stopImage;
  }
}
