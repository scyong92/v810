package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraPanel extends JPanel implements TaskPanelInt, Observer
{
    private JScrollPane _statusPaneScrollPane = new JScrollPane();
    private JTextArea _statusPane = new JTextArea();
    private JButton _initializeButton = new JButton();
    private JButton _getConfigurationButton = new JButton();    
    private JComboBox _camerasComboBox = new JComboBox();
    private JLabel _cameraLabel = new JLabel();

    private HardwareWorkerThread _workerThread = HardwareWorkerThread.getInstance();
    private SubsystemStatusAndControlTaskPanel _parent = null;
    private OpticalCameraArray _opticalCameraArray = OpticalCameraArray.getInstance();    
    private java.util.List<AbstractOpticalCamera> _cameras = null;
    private boolean _initialized = false;
    private boolean _monitoring = false;

    private final static String _ALL = StringLocalizer.keyToString("ATGUI_ALL_KEY");
    private ProgressDialog _progressDialog = null;
    private boolean _useAllCameras = true;
    private AbstractOpticalCamera _currentSelectedCamera = null;
    private final static int _MAX_NUMBER_CAMERAS = 2;

    private boolean _loadingSwingComponent = false;

    // a count of the cameras that have been initialized
    private int _initializedCameraCount = 0;

    private ActionListener _initializeButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          createProgressDialog();
          initializeSubsystem();
          _progressDialog.setVisible(true);
          _progressDialog = null;
        }
    };

    private ActionListener _camerasComboBoxActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          if(_loadingSwingComponent == false)
          {
            if (_camerasComboBox.getSelectedItem() instanceof String)
            {
              _useAllCameras = true;
              _currentSelectedCamera = null;
            }
            else if (_camerasComboBox.getSelectedItem() instanceof AbstractOpticalCamera)
            {
              _useAllCameras = false;
              _currentSelectedCamera = (AbstractOpticalCamera)_camerasComboBox.getSelectedItem();
            }
            else
              Assert.expect(false);
          }
        }
    };
    
    private ActionListener _getConfigurationButtonActionListener = new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {
          getStatus();
        }
    };

    /**
     * @author Cheah Lee Herng
     */
    private void disableButtons(boolean configuringHardware)
    {
        try
        {
          if (configuringHardware)
          {
            // then disable everything but the cancel button should be enabled
            _initializeButton.setEnabled(false);
            _getConfigurationButton.setEnabled(false);
          }
          else
          {
            if (_initialized && _opticalCameraArray.isStartupRequired() == false)
            {
              _initializeButton.setEnabled(true);
              _getConfigurationButton.setEnabled(true);
            }
            else if (_initialized)
            {
              _initializeButton.setEnabled(true);
              _getConfigurationButton.setEnabled(true);
            }
            else
            {
              _initializeButton.setEnabled(true);
              _getConfigurationButton.setEnabled(false);
            }
          }
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    private void initializeSubsystem()
    {
        _workerThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _monitoring = false;              
              if(_useAllCameras)
              {
                _initializedCameraCount = 0;
                _opticalCameraArray.startup();
              }
              else
              {
                // initialize only one camera
                Assert.expect(_currentSelectedCamera != null);
                _currentSelectedCamera.startup();
              }

              // set the flag
              _initialized = true;

              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  // set the state of the buttons
                  disableButtons(false);
                  clearStatusPane();
                }
              });

            }
            catch (final XrayTesterException xte)
            {
              SwingUtils.invokeLater(new Runnable()
              {
                public void run()
                {
                  handleException(xte);
                  _progressDialog.setVisible(false);
                  _progressDialog = null;
                }
              });
            }
          }
        });
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createProgressDialog()
    {
        _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                             StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_INITIALIZING_KEY"),
                                             StringLocalizer.keyToString("SERVICE_OPTICAL_CAMERAS_INITIALIZED_KEY"),
                                             StringLocalizer.keyToString("GUI_EXIT_BUTTON_KEY"),
                                             StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                             0,
                                             100,
                                             true);

        _progressDialog.pack();
        _progressDialog.setSize(new Dimension(500, 175));
        SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
    }

    /**
     * @author Cheah Lee Herng
     */
    public OpticalCameraPanel(SubsystemStatusAndControlTaskPanel parent)
    {
        Assert.expect(parent != null);
        _parent = parent;

        // create the ui components
        createPanel();
    }

    /**
     * @author Cheah Lee Herng
     */
    private void createPanel()
    {
        setLayout(new BorderLayout(50, 0));
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

        JPanel innerInitializePanel = new JPanel(new GridLayout(3, 1, 0, 5));

        JPanel cameraSelectionPanel = new JPanel(new FlowLayout());
        _cameraLabel.setText(StringLocalizer.keyToString("SERVICE_AVAILABLE_CAMERAS_KEY"));
        cameraSelectionPanel.add(_cameraLabel);
        cameraSelectionPanel.add(_camerasComboBox);
        innerInitializePanel.add(cameraSelectionPanel);

        JPanel initializeAndGetConfigPanel = new JPanel(new GridLayout(2, 1, 5, 25));
        initializeAndGetConfigPanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
        _getConfigurationButton.setText(StringLocalizer.keyToString("SERVICE_MOTION_CONTROL_GET_CONFIG_DESCR_KEY"));

        initializeAndGetConfigPanel.setOpaque(false);
        initializeAndGetConfigPanel.add(_initializeButton);
        initializeAndGetConfigPanel.add(_getConfigurationButton);

        innerInitializePanel.add(initializeAndGetConfigPanel);


        JPanel buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(30, 30, 0, 30));
        buttonPanel.setOpaque(false);
        buttonPanel.add(innerInitializePanel, BorderLayout.NORTH);

        JPanel statusPanel = new JPanel(new BorderLayout());
        statusPanel.setBorder(BorderFactory.createCompoundBorder(
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                             StringLocalizer.keyToString("SERIVE_UI_MOTION_CONTROL_MESSAGES_KEY")),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        statusPanel.add(_statusPaneScrollPane, BorderLayout.CENTER);
        _statusPaneScrollPane.setOpaque(false);
        _statusPaneScrollPane.getViewport().add(_statusPane);

        add(statusPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.EAST);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void handleException(XrayTesterException xte)
    {
        _parent.handleXrayTesterException(xte);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void clearStatusPane()
    {
        try
        {
          _statusPane.getDocument().remove(0, _statusPane.getDocument().getLength());
        }
        catch (javax.swing.text.BadLocationException ex)
        {
          // this should not happen
          Assert.expect(false);
        }
    }

    private void addListeners()
    {
        _initializeButton.addActionListener(_initializeButtonActionListener);
        _camerasComboBox.addActionListener(_camerasComboBoxActionListener);
        _getConfigurationButton.addActionListener(_getConfigurationButtonActionListener);
    }

    /**
    * @author Erica Wheatcroft
    */
    private void removeListeners()
    {
        _camerasComboBox.removeActionListener(_camerasComboBoxActionListener);
        _initializeButton.removeActionListener(_initializeButtonActionListener);
        _getConfigurationButton.removeActionListener(_getConfigurationButtonActionListener);
    }

    /**
     * @author Cheah Lee Herng
     */
    private void startObserving()
    {
        ProgressObservable.getInstance().addObserver(this);
        HardwareObservable.getInstance().addObserver(this);
    }

    /**
    * @author Cheah Lee Herng
    */
    private void stopObserving()
    {
        ProgressObservable.getInstance().deleteObserver(this);
        HardwareObservable.getInstance().deleteObserver(this);
    }

    /**
    * @author Erica Wheatcroft
    */
    public boolean isReadyToFinish()
    {
        return true;
    }

    /**
    * @author Erica Wheatcroft
    */
    public void finish()
    {
        // remove the listeners
        removeListeners();

        stopObserving();
    }

    /**
     * @author Cheah Lee Herng
     */
    public void start()
    {
        try
        {
          if (_opticalCameraArray.isStartupRequired())
            _initialized = false;
          else
            _initialized = true;
        }
        catch (XrayTesterException xte)
        {
          handleException(xte);
        }

        // set the state of the buttons
        disableButtons(false);

        // add the listeners
        addListeners();

        populatePanel();

        startObserving();
    }

    /**
     * @author Cheah Lee Herng
     */
    private void populatePanel()
    {
        _loadingSwingComponent = true;
        _camerasComboBox.removeAllItems();
        // first lets add the key word ALL to the combo box. The use can use this when they want to manipulate all
        // cameras at one time
        _camerasComboBox.addItem(_ALL);

        // now lets populate the combo box will all the available cameras.
        _cameras = _opticalCameraArray.getCameras();

        // sort the cameras so that they are ordered correctly
        Collections.sort(_cameras);

        // place them into the combo box
        for (AbstractOpticalCamera camera : _cameras)
          _camerasComboBox.addItem(camera);

        // we want the ALL to be selected
        _camerasComboBox.setSelectedIndex(0);
        _loadingSwingComponent = false;
    }
    
    /**
     * @author Erica Wheatcroft
    */
    private void getStatus()
    {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            java.util.List<HardwareConfigurationDescriptionInt> descriptions = new ArrayList<HardwareConfigurationDescriptionInt>();
            if (_useAllCameras)
            {
              // get the config for all cameras
              clearStatusPane();

              for (AbstractOpticalCamera camera : _cameras)
              {
                descriptions.addAll(camera.getConfigurationDescription());
                Assert.expect(descriptions != null);

                displayDescription(descriptions);
              }
            }
            else
            {
              // get the config for only one camera
              Assert.expect(_currentSelectedCamera != null);

              clearStatusPane();
              descriptions = _currentSelectedCamera.getConfigurationDescription();
              Assert.expect(descriptions != null);

              displayDescription(descriptions);
            }
          }
        });
    }
    
    /**
     * @author Erica Wheatcroft
    */
    private void displayDescription(java.util.List<HardwareConfigurationDescriptionInt> descriptions)
    {
        StringBuffer statusMessageBuffer = new StringBuffer();
        for (HardwareConfigurationDescriptionInt configuration : descriptions)
        {
          LocalizedString localizedString = new LocalizedString(configuration.getKey(),
                                                                configuration.getParameters().toArray());
          String message = StringUtil.format(StringLocalizer.keyToString(localizedString), 50);
          statusMessageBuffer.append(message);
          statusMessageBuffer.append("\n");
        }
        _statusPane.setText(statusMessageBuffer.toString());
    }

    /**
     * @author Cheah Lee Herng
     */
    public synchronized void update(final Observable observable, final Object arg)
    {
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            if (observable instanceof ProgressObservable)
            {
              Assert.expect(arg instanceof ProgressReport);
              ProgressReport progressReport = (ProgressReport)arg;
              int percentDone = progressReport.getPercentDone();

              // don't indicate 100% until event actually completes
              if (percentDone == 100)
                percentDone = 99;

              if (_progressDialog != null)
                _progressDialog.updateProgressBarPrecent(percentDone);
            }
            else if (observable instanceof HardwareObservable)
            {
              if (arg instanceof HardwareEvent)
              {
                HardwareEvent event = (HardwareEvent)arg;
                HardwareEventEnum eventEnum = event.getEventEnum();
                if (eventEnum instanceof OpticalCameraEventEnum)
                {
                  if (event.isStart() == false && eventEnum.equals(OpticalCameraEventEnum.INITIALIZE))
                  {
                    //XCR-2735: java.lang.NullPointerException for optical camera initialization on V810 5.7 Series 1
                    if(_useAllCameras)
                    {
                      // if the user is initializing all cameras then we need to wait for all cameras to be completed before
                      // we shutdown the progress bar.
                      if(_initializedCameraCount == _MAX_NUMBER_CAMERAS - 1)
                        _progressDialog.updateProgressBarPrecent(100);
                      else
                        _initializedCameraCount++;
                    }
                    else
                    {
                      _progressDialog.updateProgressBarPrecent(100);
                    }
                  }
                }
              }
            }
            else
              Assert.expect(false);
          }
        });
    }

}
