package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 */
public class InterlocksPanel extends JPanel implements TaskPanelInt, Observer
{
  private JButton _initializeButton = new JButton();
  private JLabel _goodStateLabel = new JLabel();
  private JLabel _unknownStateLabel = new JLabel();
  private JLabel _failedStateLabel = new JLabel();
  private JLabel _interlockJunctionBoardStatusLabel = new JLabel();
  private JLabel _chainStatusLabel = new JLabel();
  private JLabel _chain1StatusLabel = new JLabel();
  private JLabel _chain2StatusLabel = new JLabel();
  private JLabel _hwRevisionLabel = new JLabel();
  private JLabel _serviceModeStateLabel = new JLabel();

  private JScrollPane _interlocksTableScroller = new JScrollPane();
  private JSortTable _interlocksTable = new JSortTable();
  private AvailableInterlocksTableModel _interlocksTableModel = new AvailableInterlocksTableModel();

  private JPanel _interlockChainStatusPanel = new JPanel(new GridLayout());
  private JPanel _interlockChain1StatusPanel = new JPanel(new GridLayout());
  private JPanel _interlockChain2StatusPanel = new JPanel(new GridLayout());

  private ProgressDialog _progressDialog = null;

  private final static ImageIcon _closedIcon = Image5DX.getImageIcon(Image5DX.CD_PASSED_LEAF_TEST);
  private final static ImageIcon _openIcon = Image5DX.getImageIcon(Image5DX.CD_FAILED_LEAF_TEST);
  private final static ImageIcon _unknownStateIcon = Image5DX.getImageIcon(Image5DX.UNKNOWN_STATE);
  private final static ImageIcon _interlockDrawingIcon = Image5DX.getImageIcon(Image5DX.INTERLOCK_SYSTEM_DRAWING);
  private final static ImageIcon _keylockInterlockDrawingIcon = Image5DX.getImageIcon(Image5DX.KEYLOCK_INTERLOCK_SYSTEM_DRAWING);

  private Interlock _interlocks = null;
  private SubsystemStatusAndControlTaskPanel _parent = null;
  private boolean _serviceModeEnabled = false;
  private int _id = -1;

  private boolean _monitorEnabled = false;
  private WorkerThread _monitorThread = new WorkerThread("Service Mode Monitor");
  private boolean _initialized = false;
  private boolean _firstMessageDisplayed = false;

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private ActionListener _intializeButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog();
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _monitorEnabled = false;

            // shutdown interlocks
            _interlocks.shutdown();

            // shutdown digitial io
            DigitalIo.getInstance().shutdown();

            // startup digitial io
            DigitalIo.getInstance().startup();

            // startup interlocks
            _interlocks.startup();

            _initialized = true;

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                // populate the UI
                populatePanel();
                //start observing
                startObserving();
                //start monitoring
                startMonitoring();
              }
            });
          }
          catch (final XrayTesterException xte)
          {
//            SwingUtils.invokeLater(new Runnable()
//            {
//              public void run()
//              {
//                if (xte instanceof InterlockHardwareException)
//                {
//                  InterlockHardwareException exception = (InterlockHardwareException)xte;
//                  if (exception.getExceptionType().equals(InterlockHardwareExceptionEnum.INTERLOCK_IS_IN_SERVICE_MODE))
//                  {
//                    _initialized = true;
//                    // populate the UI
//                    populatePanel();
//                    //start observing
//                    startObserving();
//                    //start monitoring
//                    startMonitoring();
//                  }
//                  else
//                  {
//                    // since its another type of exception show the user
//                    handleXrayTesterException(xte);
//                  }
//                }
//                else
//                {
//                  handleXrayTesterException(xte);
//                }
//                _progressDialog.setVisible(false);
//                _progressDialog = null;
//              }
//            });
            //Siew Yeng
            //Fix initialize interlocks in service mode prompt infinite message box.
            //Allow to initialize only in normal mode. _initialized flag cannot be true since initialization fail.
            handleXrayTesterException(xte);
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  InterlocksPanel(SubsystemStatusAndControlTaskPanel parent)
  {
    Assert.expect(parent != null);
    _parent = parent;
    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startObserving()
  {
    HardwareObservable.getInstance().addObserver(this);
    ProgressObservable.getInstance().addObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopObserving()
  {
    HardwareObservable.getInstance().deleteObserver(this);
    ProgressObservable.getInstance().deleteObserver(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog()
  {
    _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                         StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+" - "+StringLocalizer.keyToString("SERVICE_UI_SSACTP_INTERLOCKS_TAB_NAME_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZING_MSG_KEY"),
                                         StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"),
                                         StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                         StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                         0,
                                         100,
                                         true);

    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());
  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _initializeButton.addActionListener(_intializeButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _initializeButton.removeActionListener(_intializeButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {

   // get an instance to the interlocks interface
   _interlocks = Interlock.getInstance();

   createToolbarItems();

   _firstMessageDisplayed =false;

   try
   {
     _initialized = ! _interlocks.isStartupRequired();

     // populate the UI
     populatePanel();

     //start observing
     startObserving();

     //start monitoring
     startMonitoring();

     addListeners();

   }
   catch (final XrayTesterException xte)
   {
//      if (xte instanceof InterlockHardwareException)
//      {
//        InterlockHardwareException exception = (InterlockHardwareException)xte;
//        if (exception.getExceptionType().equals(InterlockHardwareExceptionEnum.INTERLOCK_IS_IN_SERVICE_MODE))
//        {
//          _initialized = true;
//
//          // populate the UI
//          populatePanel();
//          //start observing
//          startObserving();
//          //start monitoring
//          startMonitoring();
//        }
//        else
//        {
//         // since its another type of exception show the user
//          handleXrayTesterException(xte);
//        }
//      }
//      else
//      {
//        handleXrayTesterException(xte);
//      }
      //Siew Yeng
      //Fix initialize interlocks in service mode prompt infinite message box.
      //Allow to initialize only in normal mode. _initialized flag cannot be true since initialization fail.
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createToolbarItems()
  {
    _id = _parent.getEnvironment().getEnvToolBar().startToolBarAdd();
    JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    panel.add(_serviceModeStateLabel);
    _parent.getEnvironment().getEnvToolBar().removeDefectPackagerButtons();
    _parent.getEnvironment().getEnvToolBar().addToolBarComponent(_id, panel);
    _parent.getEnvironment().getEnvToolBar().addDefectPackagerButtons();
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    if(_initialized == false)
     return true;

    if(_serviceModeEnabled == false)
      return true;

    String message = StringLocalizer.keyToString(new LocalizedString("HW_INTERLOCK_IS_IN_SERVICE_MODE_EXCEPTION_KEY", null));
    JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                  StringUtil.format(message, 50),
                                  StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                  JOptionPane.ERROR_MESSAGE);

    return false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    removeListeners();

    stopObserving();

    stopMonitoring();

    if(_id != -1)
      _parent.getEnvironment().getEnvToolBar().undoToolBarChanges(_id);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    if(_initialized == true)
      _initializeButton.setEnabled(false);
    else
      _initializeButton.setEnabled(true);
    
    // populate the service mode
    updateServiceModeStateLabel();

    // populate if the interlock juntion board is in a good state
    updateInterlockJuntionBoardStatePanel();

    // now populate if the chains are open or closed
    updateChainStatePanel();

    // now populate if the chains 1 and 2 are open or closed in normal mode
    updateChain12StatePanel();

    // populate the interlocks table
    populateInterlocksTable();

    // now populate the hw revision label
    populateHardwareRevisionLabel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateHardwareRevisionLabel()
  {
    if(_initialized == false)
    {
      _hwRevisionLabel.setForeground(Color.BLACK);
      _hwRevisionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      return;
    }

    try
    {
      _hwRevisionLabel.setForeground(Color.BLACK);
      _hwRevisionLabel.setText(Integer.toString(_interlocks.getHardwareRevisionId()));
    }
    catch(XrayTesterException xte)
    {
      _hwRevisionLabel.setForeground(Color.RED);
      _hwRevisionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void populateInterlocksTable()
  {
    if(_initialized == false)
    {
      setUpTable();
      return;
    }

    _interlocksTableModel.populateTable();
    setUpTable();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateInterlockJuntionBoardStatePanel()
  {
    if(_initialized == false)
    {
      _interlockJunctionBoardStatusLabel.setForeground(Color.BLACK);
      _interlockJunctionBoardStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      return;
    }

    try
    {
      if(_interlocks.isInterlockPoweredOn())
      {
        _interlockJunctionBoardStatusLabel.setForeground(Color.BLACK);
        _interlockJunctionBoardStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_IS_ON_KEY"));
      }
      else
      {
        _interlockJunctionBoardStatusLabel.setForeground(Color.RED);
        _interlockJunctionBoardStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_IS_OFF_KEY"));
      }
    }
    catch(XrayTesterException xte)
    {
      _interlockJunctionBoardStatusLabel.setForeground(Color.RED);
      _interlockJunctionBoardStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateChainStatePanel()
  {
    if(_initialized == false)
    {
      _chainStatusLabel.setForeground(Color.BLACK);
      _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      return;
    }

    if(_serviceModeEnabled == false) {
      _interlockChain1StatusPanel.setVisible(true);
      _interlockChain2StatusPanel.setVisible(true);
      _interlockChainStatusPanel.setVisible(false);
    } else {
      _interlockChain1StatusPanel.setVisible(false);
      _interlockChain2StatusPanel.setVisible(false);
      _interlockChainStatusPanel.setVisible(true);
    }

    try
    {
      if (_interlocks.isInterlockOpen() == false)
      {
        _chainStatusLabel.setForeground(Color.BLACK);
        _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CLOSED_STATE_KEY"));
      }
      else
      {
        _chainStatusLabel.setForeground(Color.RED);
        _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_OPEN_STATE_KEY"));
      }
    }
    catch (XrayTesterException xte)
    {
      _chainStatusLabel.setForeground(Color.RED);
      _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author Poh Kheng
   */
  private void updateChain12StatePanel()
  {
    if(_initialized == false)
    {
      _chain1StatusLabel.setForeground(Color.BLACK);
      _chain1StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      _chain2StatusLabel.setForeground(Color.BLACK);
      _chain2StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      return;
    }

    try
    {
      if (_interlocks.isInterlockChain1Open() == false)
      {
        _chain1StatusLabel.setForeground(Color.BLACK);
        _chain1StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CLOSED_STATE_KEY"));
      }
      else
      {
        _chain1StatusLabel.setForeground(Color.RED);
        _chain1StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_OPEN_STATE_KEY"));
      }

      if (_interlocks.isInterlockChain2Open() == false)
      {
        _chain2StatusLabel.setForeground(Color.BLACK);
        _chain2StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CLOSED_STATE_KEY"));
      }
      else
      {
        _chain2StatusLabel.setForeground(Color.RED);
        _chain2StatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_OPEN_STATE_KEY"));
      }

    }
    catch (XrayTesterException xte)
    {
      _chainStatusLabel.setForeground(Color.RED);
      _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
      handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateServiceModeStateLabel()
  {
    if(_initialized == false)
    {
      _serviceModeStateLabel.setForeground(Color.BLACK);
      _serviceModeStateLabel.setFont(FontUtil.getDefaultFont());
      _serviceModeStateLabel.setFont(FontUtil.getBoldFont(_serviceModeStateLabel.getFont(),Localization.getLocale()));
      _serviceModeStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MODE_IS_STATE_UNKNOWN_KEY"));
      return;
    }
    try
    {
      _serviceModeEnabled = _interlocks.isServiceModeOn();
      if(_serviceModeEnabled)
      {
        _serviceModeStateLabel.setForeground(Color.RED);
        _serviceModeStateLabel.setFont(FontUtil.getExtraLargeFont());
        _serviceModeStateLabel.setFont(FontUtil.getBoldFont(_serviceModeStateLabel.getFont(),Localization.getLocale()));
        _serviceModeStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MODE_IS_ENABLED_KEY"));
      }
      else
      {
        _serviceModeStateLabel.setForeground(Color.BLACK);
        _serviceModeStateLabel.setFont(FontUtil.getDefaultFont());
        _serviceModeStateLabel.setFont(FontUtil.getBoldFont(_serviceModeStateLabel.getFont(),Localization.getLocale()));
        _serviceModeStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MODE_IS_NOT_ENABLED_KEY"));
      }
    }
    catch(XrayTesterException xte)
    {
      _serviceModeStateLabel.setForeground(Color.RED);
      _serviceModeStateLabel.setFont(FontUtil.getExtraLargeFont());
      _serviceModeStateLabel.setFont(FontUtil.getBoldFont(_serviceModeStateLabel.getFont(),Localization.getLocale()));
      _serviceModeStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SERVICE_MODE_IS_STATE_UNKNOWN_KEY"));
      handleXrayTesterException(xte);
    }
  }

  /**
   *
   * @author Erica Wheatcroft
   */
  synchronized void handleXrayTesterException(XrayTesterException xte)
  {
    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private synchronized void startMonitoring()
  {
    //Siew Yeng 
    //This is to check if monitor thread is already running. Run it only when it is not running to avoid creating extra thread.
    if(!_monitorEnabled)
    {
      _monitorEnabled = true;
      _monitorThread.invokeLater(new Runnable()
      {
        public void run()
        {
          while (_monitorEnabled)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                // populate the service mode
                populatePanel();
              }
            });
            try
            {
              Thread.sleep(500);
            }
            catch (InterruptedException ie)
            {
              // do nothing
            }
          }
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object arg)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          Assert.expect(arg instanceof ProgressReport);
          ProgressReport progressReport = (ProgressReport)arg;
          int percentDone = progressReport.getPercentDone();

          // don't indicate 100% until event actually completes
          if (percentDone == 100)
            percentDone = 99;

          if (_progressDialog != null)
            _progressDialog.updateProgressBarPrecent(percentDone);
        }
        else if (observable instanceof HardwareObservable)
        {
          if (arg instanceof InterlockHardwareException)
          {
            // if the exception is a interlock exception then we want to refresh the panel however if the user is not
            // in service mode then lets notify them that that might be a good idea.
            populatePanel();
            if(_serviceModeEnabled == false && _firstMessageDisplayed == false)
            {
              if(_firstMessageDisplayed == false)
                _firstMessageDisplayed = true;
              String message = StringLocalizer.keyToString("SERVICE_SWITCH_TO_SERVICE_MODE_KEY");
              JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                            StringUtil.format(message, 50),
                                            StringLocalizer.keyToString("SERVICE_NEED_MORE_INFO_KEY"),
                                            JOptionPane.INFORMATION_MESSAGE);
            }
          }
          else if (arg instanceof XrayTesterException)
          {
            // display any exception
            handleXrayTesterException((XrayTesterException)arg);
          }
          else if (arg instanceof HardwareEvent)
           {
             HardwareEvent event = (HardwareEvent)arg;
             HardwareEventEnum eventEnum = event.getEventEnum();
             if (eventEnum instanceof DigitalIoEventEnum)
             {
               if (event.isStart() == false && eventEnum.equals(DigitalIoEventEnum.INITIALIZE))
               {
                 if(_progressDialog != null)
                   _progressDialog.updateProgressBarPrecent(100);
               }
             }
           }
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void setUpTable()
  {
    // we want to center all the data in the table

    TableColumn col = _interlocksTable.getColumnModel().getColumn(1);
    DefaultTableCellRenderer columnCenterRenderer = new DefaultTableCellRenderer();
    columnCenterRenderer.setHorizontalAlignment(JLabel.LEFT);
    col.setCellRenderer(columnCenterRenderer);

    // set the cell renderer for the first column.
    TableColumn statusIconColumn = _interlocksTable.getColumnModel().getColumn(0);
    InterlocksTableCellRenderer statusIconRenderer = new InterlocksTableCellRenderer();
    statusIconColumn.setCellRenderer(statusIconRenderer);

    // bold the table headers
    _interlocksTable.getTableHeader().setFont(FontUtil.getBoldFont(_interlocksTable.getTableHeader().getFont(),Localization.getLocale()));

    _interlocksTable.getTableHeader().setReorderingAllowed(false);

    // now lets set up the % of relestate for each column.
    // set up column widths
    int iTotalColumnWidth = _interlocksTable.getColumnModel().getTotalColumnWidth();
    _interlocksTable.getColumnModel().getColumn(0).setPreferredWidth((int)(iTotalColumnWidth * .10));
    _interlocksTable.getColumnModel().getColumn(1).setPreferredWidth((int)(iTotalColumnWidth * .80));
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createInterlockTableAndDrawingPanel()
  {
    JPanel centerPanel = new JPanel(new GridLayout(0, 2, 50,0));

    JPanel availableInterlocksTableAndBobStatusPanel = new JPanel();
    availableInterlocksTableAndBobStatusPanel.setLayout(new BorderLayout(0, 25));

    JPanel availableInterLocksTablePanel = new JPanel(new GridLayout());
    _interlocksTableScroller.getViewport().add(_interlocksTable);
    _interlocksTable.setModel(_interlocksTableModel);
    _interlocksTable.getTableHeader().setReorderingAllowed(false);
    _interlocksTable.setFont(FontUtil.getSizedFont(_interlocksTable.getFont(), 14,Localization.getLocale()));
    _interlocksTable.setFont(FontUtil.getBoldFont(_interlocksTable.getFont(),Localization.getLocale()));
    _interlocksTable.setRowHeight(25);
    availableInterLocksTablePanel.add(_interlocksTableScroller);
    availableInterlocksTableAndBobStatusPanel.add(availableInterLocksTablePanel, BorderLayout.CENTER);

    JPanel outerInterLockJunctionBoardStatusPanel = new JPanel( new FlowLayout(FlowLayout.CENTER, 1, 5));

    JPanel innerInterlockJunctionBoardStatusPanel = new JPanel(new GridLayout());
    innerInterlockJunctionBoardStatusPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                                StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_INTERLOCK_JUNCTION_BOARD_KEY")), BorderFactory.createEmptyBorder(25, 50, 25, 50)));

    _interlockJunctionBoardStatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _interlockJunctionBoardStatusLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _interlockJunctionBoardStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
    innerInterlockJunctionBoardStatusPanel.add(_interlockJunctionBoardStatusLabel);
    outerInterLockJunctionBoardStatusPanel.add(innerInterlockJunctionBoardStatusPanel);

    _interlockChainStatusPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
       StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CHAIN_KEY")), BorderFactory.createEmptyBorder(25, 50, 25, 50)));

    _chainStatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _chainStatusLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _chainStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
    _interlockChainStatusPanel.add(_chainStatusLabel);
    outerInterLockJunctionBoardStatusPanel.add(_interlockChainStatusPanel);
    _interlockChainStatusPanel.setVisible(false);

    _interlockChain1StatusPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
       StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CHAIN_KEY")+" 1"), BorderFactory.createEmptyBorder(25, 30, 25, 30)));

    _chain1StatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _chain1StatusLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _interlockChain1StatusPanel.add(_chain1StatusLabel);
    outerInterLockJunctionBoardStatusPanel.add(_interlockChain1StatusPanel);

    _interlockChain2StatusPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
       StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CHAIN_KEY")+" 2"), BorderFactory.createEmptyBorder(25, 30, 25, 30)));

    _chain2StatusLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _chain2StatusLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _interlockChain2StatusPanel.add(_chain2StatusLabel);
    outerInterLockJunctionBoardStatusPanel.add(_interlockChain2StatusPanel);

    JPanel hardwareRevisionPanel = new JPanel(new GridLayout());
    hardwareRevisionPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
       StringLocalizer.keyToString("SERVICE_UI_HW_REVISION_KEY")), BorderFactory.createEmptyBorder(25, 50, 25, 50)));

    _hwRevisionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _hwRevisionLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _hwRevisionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
    hardwareRevisionPanel.add(_hwRevisionLabel);
    outerInterLockJunctionBoardStatusPanel.add(hardwareRevisionPanel);

    JPanel drawingAndLegendPanel = new JPanel(new BorderLayout(0, 5));
    JPanel legendPanel = new JPanel(new FlowLayout());
    JPanel innerLegendPanel = new JPanel(new GridLayout(1,3,10,0));
    innerLegendPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
                               StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_LEGEND_KEY")), BorderFactory.createEmptyBorder(25, 50, 25, 50)));

    _goodStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _goodStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _goodStateLabel.setIcon(_closedIcon);
    _goodStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_CLOSED_STATE_KEY"));
    innerLegendPanel.add(_goodStateLabel);

    _failedStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _failedStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _failedStateLabel.setIcon(_openIcon);
    _failedStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_OPEN_STATE_KEY"));
    innerLegendPanel.add(_failedStateLabel);

    _unknownStateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    _unknownStateLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
    _unknownStateLabel.setIcon(_unknownStateIcon);
    _unknownStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_INTERLOCKS_PANEL_POWER_STATE_UNKNOWN_KEY"));
    innerLegendPanel.add(_unknownStateLabel);

    legendPanel.add(innerLegendPanel);
    ImageDisplayPanel interLockSystemDrawingPanel = new ImageDisplayPanel(_interlockDrawingIcon.getImage());
    if(Interlock.getInterLockLogicBoardWithKeylock()==true)
        interLockSystemDrawingPanel = new ImageDisplayPanel(_keylockInterlockDrawingIcon.getImage());
    drawingAndLegendPanel.add(interLockSystemDrawingPanel, BorderLayout.CENTER);
    drawingAndLegendPanel.add(outerInterLockJunctionBoardStatusPanel, BorderLayout.SOUTH);
    availableInterlocksTableAndBobStatusPanel.add(legendPanel, BorderLayout.SOUTH);
    centerPanel.add(availableInterlocksTableAndBobStatusPanel, BorderLayout.CENTER);
    centerPanel.add(drawingAndLegendPanel,BorderLayout.EAST);
    add(centerPanel, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    createInterlockTableAndDrawingPanel();

    // not lets create the initialize panel
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    northPanel.add(_initializeButton);

    add(northPanel, BorderLayout.NORTH);
  }
}
