package com.axi.v810.gui.service;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * THis class represents on entry into the Scan Path Definition Table.
 *
 * @author Erica Wheatcroft
 */
public class ScanPassDefinitionTableEntry
{
  private ScanPass _scanPass = null;
  private ScanPassDirectionEnum _scanPassDirectionEnum = null;
  private ScanStepDirectionEnum _scanStepDirectionEnum = null;
  private StagePositionMutable _stagePositionMutable = null;
  private int _scanPassLengthInNanometers = 0;
  private int _estimatedPassLengthInNanometers = 0;
  private int _estimatedStepWidthInNanometers = 0;

  /**
   * @author Erica Wheatcroft
   */
  ScanPassDefinitionTableEntry()
  {
    // create a default scan pass object
    _scanPass = new ScanPass();
    _scanStepDirectionEnum = ScanStepDirectionEnum.FORWARD;
    _scanPassDirectionEnum = ScanPassDirectionEnum.FORWARD;
    _stagePositionMutable = new StagePositionMutable();
  }

  /**
   * @author Erica Wheatcroft
   */
  void setId(int id)
  {
    Assert.expect(id >= 0);
    _scanPass.setId(id);
  }

  /**
   * @author Erica Wheatcroft
   */
  int getId()
  {
    return _scanPass.getId();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setScanPassDirection(ScanPassDirectionEnum scanPassDirectionEnum)
  {
    Assert.expect(scanPassDirectionEnum != null);
    _scanPassDirectionEnum = scanPassDirectionEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  ScanPassDirectionEnum getScanPassDirection()
  {
    return _scanPassDirectionEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setXInNanometers(int xInNanometers)
  {
    Assert.expect(_stagePositionMutable != null);
    _stagePositionMutable.setXInNanometers(xInNanometers);
  }

  /**
   * @author Erica Wheatcroft
   */
  int getXInNanometers()
  {
    Assert.expect(_stagePositionMutable != null);
    return _stagePositionMutable.getXInNanometers();
  }


  /**
   * @author Erica Wheatcroft
   */
  public void setYInNanometers(int yInNanometers)
  {
    Assert.expect(_stagePositionMutable != null);
    _stagePositionMutable.setYInNanometers(yInNanometers);
  }

  /**
   * @author Erica Wheatcroft
   */
  int getYInNanometers()
  {
    Assert.expect(_stagePositionMutable != null);
    return _stagePositionMutable.getYInNanometers();
  }


  /**
   * @author Erica Wheatcroft
   */
  public void setScanPassLengthInNanometers(int lengthInNanometers)
  {
    _scanPassLengthInNanometers = lengthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getScanPassLengthInNanometers()
  {
    return _scanPassLengthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setEstimatedPassLengthInNanometers(int estimatedPassLengthInNanometers)
  {
    _estimatedPassLengthInNanometers = estimatedPassLengthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getEstimatedPassLengthInNanometers()
  {
    return _estimatedPassLengthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setScanStepDirection(ScanStepDirectionEnum scanStepDirectionEnum)
  {
    Assert.expect(scanStepDirectionEnum != null);
    _scanStepDirectionEnum = scanStepDirectionEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  ScanStepDirectionEnum getScanStepDirection()
  {
    Assert.expect(_scanStepDirectionEnum != null);
    return _scanStepDirectionEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setEstimatedStepWidthInNanometers(int estimatedStepSizeInNanometers)
  {
    _estimatedStepWidthInNanometers = estimatedStepSizeInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  int getEstimatedStepWidthInNanometers()
  {
    return _estimatedStepWidthInNanometers;
  }

  /**
   * @author Erica Wheatcroft
   */
  ScanPass createScanPassFromEntry()
  {
    // set the starting point
    _scanPass.setStartPointInNanoMeters(_stagePositionMutable);

    // now calculate the end position
    StagePositionMutable endPosition = new StagePositionMutable();
    endPosition.setXInNanometers(_stagePositionMutable.getXInNanometers());
    if(_scanPassDirectionEnum.equals(ScanPassDirectionEnum.FORWARD))
      endPosition.setYInNanometers(_stagePositionMutable.getYInNanometers() + _scanPassLengthInNanometers);
    else
      endPosition.setYInNanometers(_stagePositionMutable.getYInNanometers() - _scanPassLengthInNanometers);

    // set the end point
    _scanPass.setEndPointInNanoMeters(endPosition);

    return _scanPass;
  }

}
