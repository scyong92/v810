package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;

/**
 * This class supports an Xray Safety Test instruction panel that has an upper description and warning,
 * a middle Shutdown button and a lower set of user instructions.
 *
 * @author George Booth
 */
public class XraySafetyTestInstructionShutdownInstructionPanel extends XraySafetyTestAbstractPanel
{
  private JPanel _northPanel;
  private JPanel _centerPanel;
  private JScrollPane _panelScrollPane;

  private JEditorPane _warningPane;
  private JPanel _buttonPanel;
  private JPanel _leftButtonPanel;
  private JPanel _rightButtonPanel;
  private JButton _shutdownButton;
  private JEditorPane _instructionPane;

  private final int _MINIMUM_WIDTH = 100;
  private final int _MINIMUM_HEIGHT = 50;
  private final int _PREFERRED_WIDTH = 820;
  private final int _PREFERRED_BUTTON_WIDTH = _PREFERRED_WIDTH / 2;
  // the followimg heights should add up to about 500 for a default 1024x768 screen size
  // (takes into account the single BoarderLayout vertical gap)
  private final int _PREFERRED_WARNING_HEIGHT = 175;
  private final int _PREFERRED_BUTTON_HEIGHT = 35;
  private final int _PREFERRED_INSTRUCTION_HEIGHT = 290;

  private String _documentKey1;
  private String _documentKey2;
  private boolean _readyToFinish = true;

  private GuiObservable _guiObservable = GuiObservable.getInstance();

  /**
   * @author George Booth
   */
  public XraySafetyTestInstructionShutdownInstructionPanel(ServiceEnvironmentPanel parent,
                                                String documentKey1, String documentKey2)
  {
    super(parent);
    Assert.expect(parent != null);
    Assert.expect(documentKey1 != null);
    Assert.expect(documentKey2 != null);
    _documentKey1 = documentKey1;
    _documentKey2 = documentKey2;

    this.setLayout(new BorderLayout(10, 10));

    _northPanel = new JPanel();
    _northPanel.setLayout(new BorderLayout());
    this.add(_northPanel, BorderLayout.NORTH);

    // set up warning
    _warningPane = new JEditorPane();
    _warningPane.setEditable(false);
    _warningPane.setBackground(this.getBackground());
    _warningPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_WARNING_HEIGHT));
    _warningPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _northPanel.add(_warningPane, BorderLayout.CENTER);

    URL warning = convertKeyToURL(_documentKey1);
    displayURL(_warningPane, warning);

    // set up shut down button
    _buttonPanel = new JPanel();
    _buttonPanel.setLayout(new GridLayout(1, 3));
    _northPanel.add(_buttonPanel, BorderLayout.SOUTH);

    _leftButtonPanel = new JPanel();
    _buttonPanel.add(_leftButtonPanel);

    _shutdownButton = new JButton();
    _shutdownButton.setText(StringLocalizer.keyToString("SERVICE_UI_XST_SHUTDOWN_BUTTON_LABEL_KEY"));
    _shutdownButton.setPreferredSize(new Dimension(_PREFERRED_BUTTON_WIDTH, _PREFERRED_BUTTON_HEIGHT));
    _shutdownButton.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    _shutdownButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        handleShutdownButtonEvent();
      }
    });
    _buttonPanel.add(_shutdownButton);

    _rightButtonPanel = new JPanel();
    _buttonPanel.add(_rightButtonPanel);

    // set up instruction
    _centerPanel = new JPanel();
    _centerPanel.setLayout(new BorderLayout());

    _instructionPane = new JEditorPane();
    _instructionPane.setEditable(false);
    _instructionPane.setBackground(this.getBackground());
    _centerPanel.add(_instructionPane, BorderLayout.CENTER);

    URL instruction = convertKeyToURL(_documentKey2);
    displayURL(_instructionPane, instruction);

    _panelScrollPane = new JScrollPane();
    _panelScrollPane.getViewport().add(_centerPanel);
    _panelScrollPane.setPreferredSize(new Dimension(_PREFERRED_WIDTH, _PREFERRED_INSTRUCTION_HEIGHT));
    _panelScrollPane.setMinimumSize(new Dimension(_MINIMUM_WIDTH, _MINIMUM_HEIGHT));
    this.add(_panelScrollPane, BorderLayout.CENTER);
  }

  /**
   * @author George Booth
   */
  void handleShutdownButtonEvent()
  {
//    System.out.println("handleShutdownButtonEvent()");

    _guiObservable.stateChanged(null, XraySafetyTestEventEnum.SHUTDOWN);
  }


  /**
   * @author George Booth
   */
  public void start()
  {
//    System.out.println("XraySafetyTestInstructionShutdownInstructionPanel.start()");
    _readyToFinish = false;
  }

  /**
   * @author George Booth
   */
  public boolean isReadyToFinish()
  {
    if (_readyToFinish)
    {
      return true;
    }
    // trying to navigate away from the Xray SafetyTest requires the hardware will be shut down.
    // make sure they want to do this
    _readyToFinish = JOptionPane.showConfirmDialog(
        this,
        StringLocalizer.keyToString(new LocalizedString("SERVICE_UI_XST_CONFIRM_SWITCH_TASK_KEY", null)),
        StringLocalizer.keyToString("SERVICE_UI_XST_CONFIRM_SWITCH_TASK_TITLE_KEY"),
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION;

    return _readyToFinish;
  }

  /**
   * @author George Booth
   */
  public void finish()
  {
//    System.out.println("XraySafetyTestInstructionShutdownInstructionPanel.finish()");
    _readyToFinish = true;
  }

}
