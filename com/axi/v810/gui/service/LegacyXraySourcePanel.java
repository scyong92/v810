package com.axi.v810.gui.service;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.business.testExec.TestExecution;
import com.axi.v810.gui.mainMenu.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Erica Wheatcroft
 * @author George Booth (created AbstractXraySourcePanel class to allow different ray sources to be used)
 */
class LegacyXraySourcePanel extends AbstractXraySourcePanel implements TaskPanelInt, Observer
{
  private JScrollPane _errorMessagesTextAreaScrollPane = new JScrollPane();
  private JTextArea _errorMessageTextArea = new JTextArea();
  private JButton _turnXraysOffForImagingButton = new JButton();
  private JButton _turnXrayPowerOffButton = new JButton();
  private JButton _clearErrorMessagesButton = new JButton();
  private JButton _turnXraysOnButton = new JButton();
  private JButton _initializeButton = new JButton();
  private JButton _innerBarrierCommandButton = new JButton();
  private JLabel _xrayStatusLabel = new JLabel();
  private JLabel _firmwareRevisionLabel = new JLabel();
  private JLabel _gridValueLabel = new JLabel();
  private JLabel _totalLabel = new JLabel();
  private JLabel _bleederValueLabel = new JLabel();
  private JLabel _isolatedSupplyLabel = new JLabel();
  private JLabel _gunTempLabel = new JLabel();
  private JLabel _currentAnodeValueLabel = new JLabel();
  private JLabel _currentFilamentValueLabel = new JLabel();
  private JLabel _currentCathodeLabel = new JLabel();
  private JLabel _currentFocusGridLabel = new JLabel();
  private JLabel _innerBarrierStateLabel = new JLabel();
  private NumericTextField _anodeTextField = new NumericTextField();
  private NumericTextField _filamentTextField = new NumericTextField();
  private NumericTextField _cathodeTextField = new NumericTextField();
  private NumericTextField _focusGridTextField = new NumericTextField();
  private Box _mainBox = Box.createVerticalBox();
  private ProgressDialog _progressDialog = null;

  private static final int _NUMBER_OF_DECIMAL_PLACES = 2;
  private static final String _UNKNOWN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_UNKNOWN_KEY");
  private static final String _OPEN = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_OPEN_STATE_KEY");
  private static final String _CLOSED = StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_CLOSED_STATE_KEY");

  private LegacyXraySource _xraySource = null;
  private DigitalIo _digitialIo = null;
  private InnerBarrier _innerBarrier = null;

  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private WorkerThread _monitorThread = new WorkerThread("Xray Source Monitor");
  private boolean _subsystemInitialized = false;

  private boolean _monitorEnabled = false;
  private boolean _initializingSubsystem = false;
  private boolean _shuttingDownSystem = false;
  private boolean _shuttingDownComplete = false;
  private boolean _startingSystem = false;
  private boolean _startingComplete = false;
  private boolean _populatingUiControls = false;

  private double _maximumAllowableHighRangeFocusVoltageInVolts;
  private double _minimumAllowableHighRangeFocusVoltageInVolts;
  private double _maximumAllowableLowRangeFocusVoltageInVolts;
  private double _minimumAllowableLowRangeFocusVoltageInVolts;


  private SubsystemStatusAndControlTaskPanel _parent = null;

  // action listeners - i create them here so i can add and remove them as needed.
  private ActionListener _innerBarrierCommandButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      Assert.expect(_innerBarrier != null);
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (_innerBarrier.isOpen())
              _innerBarrier.close();
            else
              _innerBarrier.open();
          }
          catch (final XrayTesterException xte)
          {
            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                _parent.handleXrayTesterException(xte);
                _innerBarrierStateLabel.setForeground(Color.RED);
                _innerBarrierStateLabel.setText(_UNKNOWN);
                _innerBarrierCommandButton.setText(_UNKNOWN);
              }
            });
          }
        }
      });
    }
  };
  private ActionListener _initializeActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      // turn off xrays for imaging only
      createProgressDialog(true,
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZING_MSG_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY")+"-"+StringLocalizer.keyToString("SERVICE_UI_SSACTP_XRAY_SOURCE_TAB_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_INITIALIZE_COMPLETED_KEY"));
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _initializingSubsystem = true;
            _monitorEnabled = false;

            // shutdown digitial io
            _digitialIo.shutdown();

            // shutdown xray source
            _xraySource.shutdown();

            //startup digitial io
            _digitialIo.startup();

            // startup xray source
            _xraySource.startup();

            _subsystemInitialized = true;

            SwingUtils.invokeLater(new Runnable()
            {
              public void run()
              {
                enablePanel();

                // populate the panel
                populatePanel();

                // start monitoring.
                startMonitoring();
              }
            });
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;

      _initializingSubsystem = false;
    }
  };
  private ActionListener _turnXraysOffForImagingButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog(false,
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURNING_OFF_CURRENT_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_CURRENT_OFF_KEY"));

      // turn off xrays for imaging only
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.offForServiceMode();
            updateXrayStatus();
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;

          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  private ActionListener _turnXrayPowerOffButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      createProgressDialog(false,
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURNING_OFF_CURRENT_AND_VOLTAGE_KEY"),
                           StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                           StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_CURRENT_AND_VOLTAGE_OFF_KEY"));

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.powerOff();
            updateXrayStatus();
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });
      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  private ActionListener _turnXraysOnButtonActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      if (_xraySource.isWarmStartAllowed())
        createProgressDialog(false,
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_WARM_START_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));
      else
        createProgressDialog(false,
                             StringLocalizer.keyToString("SERVICE_UI_TURNING_XRAYS_ON_COLD_START_MSG_KEY"),
                             StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                             StringLocalizer.keyToString("SERVICE_UI_XRAYS_ARE_ON_KEY"));
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.on();
            updateXrayStatus();
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
            _progressDialog.setVisible(false);
            _progressDialog = null;
          }
        }
      });

      _progressDialog.setVisible(true);
      _progressDialog = null;
    }
  };
  private ActionListener _clearErrorMessagesButtonActionListner = new ActionListener()
  {
    public void actionPerformed(ActionEvent e)
    {
      try
      {
        _xraySource.resetHardwareReportedErrors();
      }
      catch (XrayTesterException he)
      {
        handleXrayTesterException(he);
      }
      try
      {
        _errorMessageTextArea.getDocument().remove(0, _errorMessageTextArea.getDocument().getLength());
      }
      catch (javax.swing.text.BadLocationException ex)
      {
        // this should not happen
        Assert.expect(false);
      }

    }
  };
  private ActionListener _anodeTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleAnodeTextFieldEntry();
    }
  };
  private FocusListener _anodeTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
      String currentAnodeValueString =_currentAnodeValueLabel.getText();
      _anodeTextField.setText(currentAnodeValueString);
      _anodeTextField.selectAll();
      _populatingUiControls = false;
    }

    public void focusLost(FocusEvent evt)
    {
      handleAnodeTextFieldEntry();
    }
  };

  private ActionListener _filamentTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleFilamentTextFieldEntry();
    }
  };

  private FocusListener _filamentTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
      String currentValueString =_currentFilamentValueLabel.getText();
      _filamentTextField.setText(currentValueString);
      _filamentTextField.selectAll();
      _populatingUiControls = false;
    }

    public void focusLost(FocusEvent evt)
    {
      handleFilamentTextFieldEntry();
    }
  };

  private ActionListener _cathodeTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleCathodeTextFieldEntry();
    }
  };

  private FocusListener _cathodeTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
     String currentValueString =_currentCathodeLabel.getText();
     _cathodeTextField.setText(currentValueString);
      _cathodeTextField.selectAll();
     _populatingUiControls = false;

    }

    public void focusLost(FocusEvent evt)
    {
      handleCathodeTextFieldEntry();
    }
  };

  private ActionListener _focusGridTextFieldActionListener = new ActionListener()
  {
    public void actionPerformed(ActionEvent evt)
    {
      handleFocusGridTextFieldEntry();
    }
  };

  private FocusListener _focusGridTextFieldListener = new FocusListener()
  {
    public void focusGained(FocusEvent evt)
    {
      _populatingUiControls = true;
      String currentValueString = _currentFocusGridLabel.getText();
      _focusGridTextField.setText(currentValueString);
      _focusGridTextField.selectAll();
      _populatingUiControls = false;
    }

    public void focusLost(FocusEvent evt)
    {
      handleFocusGridTextFieldEntry();
    }
  };

  /**
   * @author Erica Wheatcroft
   */
  LegacyXraySourcePanel(SubsystemStatusAndControlTaskPanel parent)
  {
    super(parent);
    Assert.expect(parent != null);

    _parent = parent;

    createPanel();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void enablePanel()
  {
    _errorMessagesTextAreaScrollPane.setEnabled(_subsystemInitialized);
    _errorMessageTextArea.setEnabled(_subsystemInitialized);
    _clearErrorMessagesButton.setEnabled(_subsystemInitialized);
    _anodeTextField.setEnabled(_subsystemInitialized);
    _filamentTextField.setEnabled(_subsystemInitialized);
    _cathodeTextField.setEnabled(_subsystemInitialized);
    _focusGridTextField.setEnabled(_subsystemInitialized);
    _innerBarrierCommandButton.setEnabled(_subsystemInitialized);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void handleFocusGridTextFieldEntry() throws HeadlessException
  {
    if(_populatingUiControls)
      return;

    try
    {
      final double value = _focusGridTextField.getNumberValue().doubleValue();
      // for the focus grid the only allowed values are 600 ?1100 & 2000 - 2980
      // lets check to make sure that the value entered is correct.

      // since i have range checking enabled, the user can not enter 0 for the value so if the value is 0 then
      // the user must have not tabbed through the textfield.
      if (value == 0.0)
        return;

      if (((value >= _minimumAllowableLowRangeFocusVoltageInVolts && value <= _maximumAllowableLowRangeFocusVoltageInVolts) ||
           (value >= _maximumAllowableHighRangeFocusVoltageInVolts && value <= _maximumAllowableHighRangeFocusVoltageInVolts)) == false)
      {
         _focusGridTextField.setText(_currentFocusGridLabel.getText());
        // the value entered is not correct. Notify the user
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_FOCUS_GRID_VALUE_KEY",
                                                                   new Object[]{value}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("SERVICE_INVALID_FOCUS_GRID_VALUE_TITLE_KEY"),
                                      JOptionPane.ERROR_MESSAGE);
        return;
      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setFocusVoltageInVolts(value);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }

        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _focusGridTextField.setText(_currentFocusGridLabel.getText());
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private void handleCathodeTextFieldEntry()
  {

    if(_populatingUiControls)
      return;

    try
    {
      final double value = _cathodeTextField.getNumberValue().doubleValue();
      // the cathode value must be between 0 and 125
      if (value < 0 || value > 125)
      {
        _cathodeTextField.setText(_currentCathodeLabel.getText());

        // the user has entered a invalid value, display the message and return out
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_CATHODE_VALUE_KEY",
                                                                         new Object[]
                                                                         {value}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("Invalid Cathode Value"),
                                      JOptionPane.ERROR_MESSAGE);
        return;

      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setCathodeCurrentInMicroAmps(value);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
     _cathodeTextField.setText(_currentCathodeLabel.getText());
    }
  }



  /**
   * @author Erica Wheatcroft
   */
  private void handleFilamentTextFieldEntry()
  {
    if(_populatingUiControls)
      return;

    try
    {
      final double value = _filamentTextField.getNumberValue().doubleValue();
      // the filament value must be between 0 and 6
      if (value < 0 || value > 6)
      {
        _filamentTextField.setText(_currentFilamentValueLabel.getText());
        // the user has entered a invalid value, display the message and return out
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_FILAMENT_VALUE_KEY",
                                                                         new Object[]
                                                                         {value}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("Invalid Filament Value"),
                                      JOptionPane.ERROR_MESSAGE);
        return;

      }
      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setFilamentVoltageInVolts(value);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original
      _filamentTextField.setText(_currentFilamentValueLabel.getText());
    }
  }


  /**
   * @author Erica Wheatcroft
   */
  private void handleAnodeTextFieldEntry()
  {
    if(_populatingUiControls)
      return;

    try
    {
      final double value = _anodeTextField.getNumberValue().doubleValue();
      // the anode value must be between 0 and 165
      if(value < 0 || value > 165)
      {
        _anodeTextField.setText(_currentAnodeValueLabel.getText());
        // the user has entered a invalid value, display the message and return out
        String message = StringLocalizer.keyToString(new LocalizedString("SERVICE_INVALID_ANODE_VALUE_KEY",
                                                                         new Object[]
                                                                         {value}));
        JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                      StringUtil.format(message, 50),
                                      StringLocalizer.keyToString("Invalid Anode Value"),
                                      JOptionPane.ERROR_MESSAGE);
        return;

      }

      _hardwareWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _xraySource.setAnodeVoltageInKiloVolts(value);
          }
          catch (XrayTesterException xte)
          {
            handleXrayTesterException(xte);
          }
        }
      });
    }
    catch (ParseException pe)
    {
      // the value entered is invalid.. set it back to the original value displayed in the label
      _anodeTextField.setText(_currentAnodeValueLabel.getText());
    }
  }


  /**
    * @author Erica WHeatcroft
    */
   private void startObserving(boolean start)
   {
     if (start)
     {
       ProgressObservable.getInstance().addObserver(this);
       HardwareObservable.getInstance().addObserver(this);
     }
     else
     {
       ProgressObservable.getInstance().deleteObserver(this);
       HardwareObservable.getInstance().deleteObserver(this);
     }
  }

  /**
   * @author Erica Wheatcroft
   */
  public void start()
  {
    startObserving(true);

    createHardwareHandles();

    enablePanel();

    // populate the UI
    populatePanel();

    // add the listeners
    addListeners();

    if(_subsystemInitialized)
      startMonitoring();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createHardwareHandles()
  {
    _digitialIo = DigitalIo.getInstance();
    _innerBarrier = InnerBarrier.getInstance();
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof LegacyXraySource);
    _xraySource = (LegacyXraySource)xraySource;

    try
    {
      _subsystemInitialized = ! _digitialIo.isStartupRequired();
    }
    catch(XrayTesterException xte)
    {
      _subsystemInitialized = false;
      _parent.handleXrayTesterException(xte);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createProgressDialog(boolean initializingSubsystem,
                                    String message,
                                    String title,
                                    String operationCompletedMessage)
  {
    if (initializingSubsystem == false)
    {
      // now lets create the progress bar.
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           title,
                                           message,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("GUI_CANCEL_BUTTON_KEY") + " ",
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);

      // now create the listner tied to the cancel button for the progress bar dialog.
      _progressDialog.addCancelActionListener(new ActionListener()
      {
        public void actionPerformed(java.awt.event.ActionEvent e)
        {
          try
          {
            _xraySource.abort();
          }
          catch (XrayTesterException xrayTesterException)
          {
            JOptionPane.showMessageDialog(MainMenuGui.getInstance(),
                                          StringUtil.format(xrayTesterException.getMessage(), 50),
                                          StringLocalizer.keyToString("CDGUI_ENV_NAME_KEY"),
                                          JOptionPane.ERROR_MESSAGE);
          }
          finally
          {
            _progressDialog.dispose();
          }
        }
      });
    }
    else
    {
      // now lets create the progress bar.
      _progressDialog = new ProgressDialog(MainMenuGui.getInstance(),
                                           title,
                                           message,
                                           operationCompletedMessage,
                                           StringLocalizer.keyToString("GUI_OK_BUTTON_KEY"),
                                           StringLocalizer.keyToString("MMGUI_START_UP_PERCENT_COMPLETE_MESSAGE_KEY"),
                                           0,
                                           100,
                                           true);
    }
    _progressDialog.pack();
    _progressDialog.setSize(new Dimension(500, 175));
    SwingUtils.centerOnComponent(_progressDialog, MainMenuGui.getInstance());

  }


  /**
   * @author Erica Wheatcroft
   */
  private void populatePanel()
  {
    _maximumAllowableHighRangeFocusVoltageInVolts = _xraySource.getMaximumAllowableHighRangeFocusVoltageInVolts();
    _minimumAllowableHighRangeFocusVoltageInVolts = _xraySource.getMinimumAllowableHighRangeFocusVoltageInVolts();
    _maximumAllowableLowRangeFocusVoltageInVolts = _xraySource.getMaximumAllowableLowRangeFocusVoltageInVolts();
    _minimumAllowableLowRangeFocusVoltageInVolts = _xraySource.getMinimumAllowableLowRangeFocusVoltageInVolts();

    // lets check for xrays
    updateXrayStatus();

    populateInnerBarrierPanel();

    // update the error message text area if messages exist.
    updateMessagesAndFirmwareVersion();
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateMessagesAndFirmwareVersion()
  {
    if(_subsystemInitialized == false)
      return;

    try
    {
      XraySourceHardwareReportedErrorsException error = _xraySource.getHardwareReportedErrors();
      if (error != null)
        _errorMessageTextArea.append(error.getLocalizedMessage());
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
    }

    try
    {
      _firmwareRevisionLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_FIRMWARE_REV_KEY") +
                                     " " +
                                     _xraySource.getFirmwareRevisionCode());
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
      _firmwareRevisionLabel.setText("");
    }

  }

  /**
   * @author Erica WHeatcroft
   */
  private void handleXrayTesterException(final XrayTesterException xte)
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      showXrayTesterException(xte);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          showXrayTesterException(xte);
        }
      });
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void showXrayTesterException(XrayTesterException xte)
  {
    _subsystemInitialized = false;

    // repopulate to show that the subsystem needs to reinitialized
    populatePanel();

    // stop thread that are polling
    stopMonitoring();

    _parent.handleXrayTesterException(xte);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void updateXrayParameters()
  {
    try
    {
      final double bleederMicroAmps = MathUtil.roundToPlaces(_xraySource.getExpectedAnodeOnBleederMicroAmps(),
                                                             _NUMBER_OF_DECIMAL_PLACES);
      final double gridVoltageValue = MathUtil.roundToPlaces(_xraySource.getGridVoltageInVolts(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double currentValue = MathUtil.roundToPlaces(_xraySource.getTotalCurrentInMicroAmps(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double isolatedSupplyValue = MathUtil.roundToPlaces(_xraySource.getIsolatedSupplyVoltageInVolts(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double gunTempValue = MathUtil.roundToPlaces(_xraySource.getGunTemperatureInDegreesCelcius(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double currentAnodeValue = MathUtil.roundToPlaces(_xraySource.getAnodeVoltageInKiloVolts(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double currentFilamentValue = MathUtil.roundToPlaces(_xraySource.getFilamentVoltageInVolts(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double cathodeValue = MathUtil.roundToPlaces(_xraySource.getCathodeCurrentInMicroAmps(),
          _NUMBER_OF_DECIMAL_PLACES);
      final double focusGridValue = MathUtil.roundToPlaces(_xraySource.getFocusVoltageInVolts(),
          _NUMBER_OF_DECIMAL_PLACES);

      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          _bleederValueLabel.setText(Double.toString(bleederMicroAmps));
          _totalLabel.setText(Double.toString(currentValue));
          _isolatedSupplyLabel.setText(Double.toString(isolatedSupplyValue));
          _gunTempLabel.setText(Double.toString(gunTempValue));
          _currentAnodeValueLabel.setText(Double.toString(currentAnodeValue));
          _currentFilamentValueLabel.setText(Double.toString(currentFilamentValue));
          _currentCathodeLabel.setText(Double.toString(cathodeValue));
          _currentFocusGridLabel.setText(Double.toString(focusGridValue));
          _gridValueLabel.setText(Double.toString(gridVoltageValue));
        }
      });
    }
    catch (XrayTesterException he)
    {
      handleXrayTesterException(he);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void populateInnerBarrierPanel()
  {
    if (_subsystemInitialized == false)
    {
      _innerBarrierStateLabel.setForeground(Color.BLACK);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
      return;
    }
    try
    {
      if (_innerBarrier.isInstalled())
      {
        if (_innerBarrier.isOpen())
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_OPEN);
          _innerBarrierCommandButton.setText(_CLOSED);
        }
        else
        {
          _innerBarrierStateLabel.setForeground(Color.BLACK);
          _innerBarrierStateLabel.setText(_CLOSED);
          _innerBarrierCommandButton.setText(_OPEN);
        }
      }
      else
      {
        _innerBarrierStateLabel.setForeground(Color.RED);
        _innerBarrierStateLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_PANEL_HANDLING_NOT_INSTALLED_KEY"));
        _innerBarrierCommandButton.setEnabled(false);
      }
    }
    catch (XrayTesterException xte)
    {
      _parent.handleXrayTesterException(xte);
      _innerBarrierStateLabel.setForeground(Color.RED);
      _innerBarrierStateLabel.setText(_UNKNOWN);
      _innerBarrierCommandButton.setText(_UNKNOWN);
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  private void checkVoltage()
  {
    if(_subsystemInitialized)
    {
      try
      {
        if (_xraySource.isAnodeVoltageOff())
        {
          // its off so the user can't tweak the current
          _cathodeTextField.setEnabled(false);
        }
        else
        {
          // its on so the user can continue
          _cathodeTextField.setEnabled(true);
        }
      }
      catch(XrayTesterException xte)
      {
        _parent.handleXrayTesterException(xte);
      }
    }
  }

  /**
   * @uthor Erica Wheatcroft
   */
  private void updateXrayStatus()
  {
    if (_subsystemInitialized == false)
    {
      _xrayStatusLabel.setForeground(Color.BLACK);
      _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_SUBSYSTEM_NOT_INITIALIZED_KEY"));
      _turnXraysOnButton.setEnabled(false);
      _turnXraysOffForImagingButton.setEnabled(false);
      _turnXrayPowerOffButton.setEnabled(false);
      return;
    }

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          boolean areXraysOn = _xraySource.areXraysOnForServiceMode();
          boolean couldUnsafeXraysBeEmitted = _xraySource.couldUnsafeXraysBeEmitted();

          if (areXraysOn == false && couldUnsafeXraysBeEmitted)
          {
            _xrayStatusLabel.setForeground(Color.BLACK);
            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_ON_KEY"));
            _turnXraysOnButton.setEnabled(true);
            _turnXraysOffForImagingButton.setEnabled(false);
            _turnXrayPowerOffButton.setEnabled(true);
            return;
          }
          else if (areXraysOn == false && couldUnsafeXraysBeEmitted == false)
          {
            _xrayStatusLabel.setForeground(Color.RED);
            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_POWER_IS_OFF_KEY"));
            _turnXraysOnButton.setEnabled(true);
            _turnXraysOffForImagingButton.setEnabled(false);
            _turnXrayPowerOffButton.setEnabled(false);
            return;
          }
          else if (areXraysOn && couldUnsafeXraysBeEmitted)
          {
            _xrayStatusLabel.setForeground(Color.BLACK);
            _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_ON_KEY"));
            _turnXraysOnButton.setEnabled(false);
            _turnXraysOffForImagingButton.setEnabled(true);
            _turnXrayPowerOffButton.setEnabled(true);
            return;
          }

          populateInnerBarrierPanel();

        }
        catch (XrayTesterException xte)
        {
          handleXrayTesterException(xte);
          _xrayStatusLabel.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_ERROR_KEY"));
          _turnXraysOnButton.setEnabled(false);
          _turnXraysOffForImagingButton.setEnabled(false);
          _turnXrayPowerOffButton.setEnabled(false);
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean isReadyToFinish()
  {
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void finish()
  {
    stopMonitoring();

    startObserving(false);

    // remove the listeners
    removeListeners();

  }

  /**
   * @author Erica Wheatcroft
   */
  private void addListeners()
  {
    _turnXrayPowerOffButton.addActionListener(_turnXrayPowerOffButtonActionListener);
    _turnXraysOffForImagingButton.addActionListener(_turnXraysOffForImagingButtonActionListener);
    _turnXraysOnButton.addActionListener(_turnXraysOnButtonActionListener);
    _clearErrorMessagesButton.addActionListener(_clearErrorMessagesButtonActionListner);
    _anodeTextField.addFocusListener(_anodeTextFieldListener);
    _cathodeTextField.addFocusListener(_cathodeTextFieldListener);
    _filamentTextField.addFocusListener(_filamentTextFieldListener);
    _focusGridTextField.addFocusListener(_focusGridTextFieldListener);
    _anodeTextField.addActionListener(_anodeTextFieldActionListener);
    _cathodeTextField.addActionListener(_cathodeTextFieldActionListener);
    _filamentTextField.addActionListener(_filamentTextFieldActionListener);
    _focusGridTextField.addActionListener(_focusGridTextFieldActionListener);
    _initializeButton.addActionListener(_initializeActionListener);
    _innerBarrierCommandButton.addActionListener(_innerBarrierCommandButtonActionListener);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void removeListeners()
  {
    _turnXrayPowerOffButton.removeActionListener(_turnXrayPowerOffButtonActionListener);
    _turnXraysOffForImagingButton.removeActionListener(_turnXraysOffForImagingButtonActionListener);
    _turnXraysOnButton.removeActionListener(_turnXraysOnButtonActionListener);
    _clearErrorMessagesButton.removeActionListener(_clearErrorMessagesButtonActionListner);
    _anodeTextField.removeFocusListener(_anodeTextFieldListener);
    _cathodeTextField.removeFocusListener(_cathodeTextFieldListener);
    _filamentTextField.removeFocusListener(_filamentTextFieldListener);
    _focusGridTextField.removeFocusListener(_focusGridTextFieldListener);
    _anodeTextField.removeActionListener(_anodeTextFieldActionListener);
    _cathodeTextField.removeActionListener(_cathodeTextFieldActionListener);
    _filamentTextField.removeActionListener(_filamentTextFieldActionListener);
    _focusGridTextField.removeActionListener(_focusGridTextFieldActionListener);
    _initializeButton.removeActionListener(_initializeActionListener);
    _innerBarrierCommandButton.removeActionListener(_innerBarrierCommandButtonActionListener);
  }


  /**
   * This method will create panel displaying all parameters that we expose about the tube to the user
   * @author Erica Wheatcroft
   */
  private void createXrayParametersPanel()
  {
    int textFieldColumnLength = 5;

    JPanel xraySourceParametersPanel = new JPanel(new BorderLayout());
    Border etchedBorder = BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140));
    Border emptyBorderForPanelWithOneComponent = BorderFactory.createEmptyBorder(17, 5, 5, 5);
    Border emptyBorderForPanelWithTwoComponents = BorderFactory.createEmptyBorder(17, 5, 5, 5);

    JPanel gunTempPanel = new JPanel(new FlowLayout());
    JPanel innerGunTempPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.CENTER, VerticalFlowLayout.MIDDLE));
    Border gunTempPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_GUN_TEMP_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    gunTempPanel.setBorder(gunTempPanelBorder);
    innerGunTempPanel.add(_gunTempLabel);
    gunTempPanel.add(innerGunTempPanel);

    JPanel isolatedSupplyPanel = new JPanel(new FlowLayout());
    JPanel innerIsolatedSupplyPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border isolatedSupplyPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder,
        StringLocalizer.keyToString("SERVICE_UI_ISOLATED_SUPPLY_LABEL_KEY")), emptyBorderForPanelWithOneComponent);
    isolatedSupplyPanel.setBorder(isolatedSupplyPanelBorder);
    innerIsolatedSupplyPanel.add(_isolatedSupplyLabel);
    isolatedSupplyPanel.add(innerIsolatedSupplyPanel);

    JPanel bleederPanel = new JPanel(new FlowLayout());
    JPanel innerBleederPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border bleederPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_BLEEDER_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    bleederPanel.setBorder(bleederPanelBorder);
    innerBleederPanel.add(_bleederValueLabel);
    bleederPanel.add(innerBleederPanel);

    JPanel totalPanel = new JPanel(new FlowLayout());
    JPanel innerTotalPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border totalPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_TOTAL_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    totalPanel.setBorder(totalPanelBorder);
    innerTotalPanel.add(_totalLabel);
    totalPanel.add(innerTotalPanel);

    JPanel gridPanel = new JPanel(new FlowLayout());
    JPanel innerGridPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border gridPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_GRID_LABEL_KEY")),
        emptyBorderForPanelWithOneComponent);
    gridPanel.setBorder(gridPanelBorder);
    innerGridPanel.add(_gridValueLabel);
    gridPanel.add(innerGridPanel);

    JPanel focusGridPanel = new JPanel(new FlowLayout());
    JPanel innerFocusGridPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border focusGridPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_FOCUS_GRID_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    focusGridPanel.setBorder(focusGridPanelBorder);
    innerFocusGridPanel.add(_currentFocusGridLabel);
    innerFocusGridPanel.add(_focusGridTextField);
    DecimalFormat formatForFocusGridTextField = new DecimalFormat();
    NumericRangePlainDocument focusGridTextFieldDocument = new NumericRangePlainDocument();
    _focusGridTextField.setDocument(focusGridTextFieldDocument);
    _focusGridTextField.setFormat(formatForFocusGridTextField);
    focusGridTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _focusGridTextField.setColumns(textFieldColumnLength);
    focusGridPanel.add(innerFocusGridPanel);

    JPanel cathodePanel = new JPanel(new FlowLayout());
    JPanel innerCathodePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border cathodePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_CATHODE_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    cathodePanel.setBorder(cathodePanelBorder);
    innerCathodePanel.add(_currentCathodeLabel);
    innerCathodePanel.add(_cathodeTextField);
    DecimalFormat formatForcathodeTextField = new DecimalFormat();
    NumericRangePlainDocument cathodeTextFieldDocument = new NumericRangePlainDocument();
    _cathodeTextField.setDocument(cathodeTextFieldDocument);
    _cathodeTextField.setFormat(formatForcathodeTextField);
    cathodeTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _cathodeTextField.setColumns(textFieldColumnLength);
    cathodePanel.add(innerCathodePanel);

    JPanel filamentPanel = new JPanel(new FlowLayout());
    JPanel innerFilamentPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border filamentPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_FILAMENT_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    filamentPanel.setBorder(filamentPanelBorder);
    innerFilamentPanel.add(_currentFilamentValueLabel);
    innerFilamentPanel.add(_filamentTextField);
    DecimalFormat formatForfilamentTextField = new DecimalFormat();
    NumericRangePlainDocument filamentTextFieldDocument = new NumericRangePlainDocument();
    _filamentTextField.setDocument(filamentTextFieldDocument);
    _filamentTextField.setFormat(formatForfilamentTextField);
    filamentTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _filamentTextField.setColumns(textFieldColumnLength);
    filamentPanel.add(innerFilamentPanel);

    JPanel anodePanel = new JPanel(new FlowLayout());
    JPanel innerAnodePanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border anodePanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("SERVICE_UI_ANODE_LABEL_KEY")),
        emptyBorderForPanelWithTwoComponents);
    anodePanel.setBorder(anodePanelBorder);
    innerAnodePanel.add(_currentAnodeValueLabel);
    innerAnodePanel.add(_anodeTextField);
    DecimalFormat formatForAnodeTextField = new DecimalFormat();
    NumericRangePlainDocument anodeTextFieldDocument = new NumericRangePlainDocument();
    _anodeTextField.setDocument(anodeTextFieldDocument);
    _anodeTextField.setFormat(formatForAnodeTextField);
    anodeTextFieldDocument.setRange(new IntegerRange(0, Integer.MAX_VALUE));
    _anodeTextField.setColumns(textFieldColumnLength);
    anodePanel.add(innerAnodePanel);

    JPanel innerBarrierPanel = new JPanel();
    JPanel innerInnerBarrierPanel = new JPanel(new VerticalFlowLayout(1, 5));
    Border innerBarrierPanelBorder = BorderFactory.createCompoundBorder(new TitledBorder(etchedBorder, StringLocalizer.keyToString("Inner Barrier")),
        emptyBorderForPanelWithTwoComponents);
    innerBarrierPanel.setBorder(innerBarrierPanelBorder);
    innerInnerBarrierPanel.add(_innerBarrierStateLabel);
    _innerBarrierStateLabel.setText("Open");
    innerInnerBarrierPanel.add(_innerBarrierCommandButton);
    _innerBarrierCommandButton.setText("Close");
    innerBarrierPanel.add(innerInnerBarrierPanel);

    // add the panels to the main parameters panel
    Box topRowParameters = Box.createHorizontalBox();
    topRowParameters.add(anodePanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(filamentPanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(cathodePanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(focusGridPanel);
    topRowParameters.add(Box.createHorizontalGlue());
    topRowParameters.add(innerBarrierPanel);

    Box bottomRowParameters = Box.createHorizontalBox();
    bottomRowParameters.add(gridPanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(totalPanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(bleederPanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(isolatedSupplyPanel);
    bottomRowParameters.add(Box.createHorizontalGlue());
    bottomRowParameters.add(gunTempPanel);

    Box parametersBox = Box.createVerticalBox();
    parametersBox.add(topRowParameters);
    parametersBox.add(Box.createVerticalStrut(25));
    parametersBox.add(bottomRowParameters);

    xraySourceParametersPanel.add(parametersBox, BorderLayout.CENTER);
    _mainBox.add(xraySourceParametersPanel);
  }

  /**
   * THis method create the panel that will handle the hardware reported errors
   * @author Erica Wheatcroft
   */
  private void createHardwareReportedErrorsPanel()
  {
    JPanel errorMessageAndFirmwarePanel = new JPanel(new BorderLayout(0, 10));
    JPanel firmwareRevisionPanel = new JPanel();
    firmwareRevisionPanel.setLayout(new FlowLayout());
    _firmwareRevisionLabel.setHorizontalAlignment(SwingConstants.CENTER);
    _firmwareRevisionLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    firmwareRevisionPanel.add(_firmwareRevisionLabel);
    errorMessageAndFirmwarePanel.add(firmwareRevisionPanel, BorderLayout.SOUTH);
    JPanel errorMessagesPanel = new JPanel();
    Border border4 = BorderFactory.createCompoundBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,
        new Color(148, 145, 140)), StringLocalizer.keyToString("SERVICE_UI_XRAYS_PANEL_HW_REPORTED_ERRORS_LABEL_KEY")), BorderFactory.createEmptyBorder(5, 10, 10, 10));
    errorMessagesPanel.setLayout(new BorderLayout(0, 10));
    errorMessagesPanel.setBorder(border4);
    _errorMessageTextArea.setEditable(false);
    _errorMessageTextArea.setRows(10);
    _clearErrorMessagesButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAYS_PANEL_CLEAR_ERROR_MSG_KEY"));
    JPanel clearErrorMessagesButtonPanel = new JPanel();
    clearErrorMessagesButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    clearErrorMessagesButtonPanel.add(_clearErrorMessagesButton);
    errorMessageAndFirmwarePanel.add(errorMessagesPanel, BorderLayout.CENTER);
    errorMessagesPanel.add(_errorMessagesTextAreaScrollPane, BorderLayout.CENTER);
    errorMessagesPanel.add(clearErrorMessagesButtonPanel, BorderLayout.NORTH);
    _errorMessagesTextAreaScrollPane.getViewport().add(_errorMessageTextArea);
    _mainBox.add(errorMessageAndFirmwarePanel);
  }

  /**
   * THis method will create the xray control and status panel
   * @author Erica Wheatcroft
   */
  private void createXrayControlPanel()
  {
    JPanel xrayStatusAndControlPanel = new JPanel();
    xrayStatusAndControlPanel.setBorder(BorderFactory.createEmptyBorder(25, 0, 0, 0));
    xrayStatusAndControlPanel.setLayout(new BorderLayout());

    JPanel xrayControlButtonPanel = new JPanel();
    xrayControlButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 5));
    xrayControlButtonPanel.add(_turnXraysOnButton);
    xrayControlButtonPanel.add(_turnXrayPowerOffButton);
    xrayControlButtonPanel.add(_turnXraysOffForImagingButton);
    _turnXraysOffForImagingButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_XRAYS_OFF_FOR_IMAGING_KEY"));
    _turnXrayPowerOffButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_FULL_XRAYS_OFF_KEY"));
    _turnXraysOnButton.setText(StringLocalizer.keyToString("SERVICE_UI_XRAY_PANEL_TURN_CURRENT_AND_VOLTAGE_ON_KEY"));

    JPanel xrayTubeStatusPanel = new JPanel(new FlowLayout());
    _xrayStatusLabel.setFont(FontUtil.getBiggerFont(_xrayStatusLabel.getFont(),Localization.getLocale()));
    _xrayStatusLabel.setFont(FontUtil.getBoldFont(_xrayStatusLabel.getFont(),Localization.getLocale()));
    xrayTubeStatusPanel.add(_xrayStatusLabel);
    xrayStatusAndControlPanel.add(xrayTubeStatusPanel, BorderLayout.NORTH);
    xrayStatusAndControlPanel.add(xrayControlButtonPanel, BorderLayout.CENTER);

    JPanel initializePanel = new JPanel(new FlowLayout());
    _initializeButton.setText(StringLocalizer.keyToString("SERVICE_UI_SSACTP_INITIALIZE_BUTTON_TEXT_KEY"));
    initializePanel.add(_initializeButton);
    xrayStatusAndControlPanel.add(initializePanel, BorderLayout.EAST);

    _mainBox.add(xrayStatusAndControlPanel);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void createPanel()
  {
    setLayout(new GridLayout());
    setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));

    createXrayControlPanel();
    _mainBox.add(Box.createVerticalStrut(25));
    createXrayParametersPanel();
    _mainBox.add(Box.createVerticalStrut(25));
    createHardwareReportedErrorsPanel();

    add(_mainBox, BorderLayout.CENTER);
  }

  /**
   * @author Erica Wheatcroft
   */
  private void startMonitoring()
  {
    _monitorEnabled = true;
    _monitorThread.invokeLater(new Runnable()
    {
      public void run()
      {
        while (_monitorEnabled)
        {
          updateXrayParameters();
          checkVoltage();
          try
          {
            Thread.sleep(500);
          }
          catch (InterruptedException ie)
          {
            // do nothing
          }
        }
      }
    });
  }

  /**
   * @author Erica Wheatcroft
   */
  private void stopMonitoring()
  {
    _monitorEnabled = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized void update(final Observable observable, final Object object)
  {
    Assert.expect(observable != null);

    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof ProgressObservable)
        {
          if ((object instanceof ProgressReport))
          {
            ProgressReport progressReport = (ProgressReport)object;
            int percentDone = progressReport.getPercentDone();

            // since the UI now calls shutdown then startup, i found that the shutdown was
            // using the progress observable to dislpay status on the shutdown procedure.
            // so instead of showing two different progress bars for the initialization procedure
            // i used on and tweek the percent done value accordingly.
            if(_initializingSubsystem)
            {
              if(_shuttingDownSystem && _shuttingDownComplete == false)
                percentDone = percentDone / 2;

              if(_startingSystem && _startingComplete == false)
                percentDone = percentDone / 2 + 50;
            }

            // don't indicate 100% until event actually completes
            if (percentDone == 100)
              percentDone = 99;

            if (_progressDialog != null)
              _progressDialog.updateProgressBarPrecent(percentDone);
          }
        }
        else if (observable instanceof HardwareObservable)
        {
          if (object instanceof HardwareEvent)
          {
            HardwareEvent event = (HardwareEvent)object;
            HardwareEventEnum eventEnum = event.getEventEnum();

            if (eventEnum instanceof XraySourceEventEnum)
            {
              if(event.isStart() && eventEnum.equals(XraySourceEventEnum.SHUTDOWN))
              {
                _shuttingDownSystem = true;
              }

              if(event.isStart() == false && eventEnum.equals(XraySourceEventEnum.SHUTDOWN))
              {
                _shuttingDownSystem = false;
                _startingComplete = true;
              }

              if (event.isStart() && eventEnum.equals(XraySourceEventEnum.STARTUP))
              {
                _startingSystem = true;
              }

              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.STARTUP))
              {
                _startingSystem = false;
                _startingComplete = true;

                if (_progressDialog != null)
                  _progressDialog.updateProgressBarPrecent(100);
              }

              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.ON))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.OFF))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
              if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.POWER_OFF))
              {
                if (_progressDialog != null && _initializingSubsystem == false)
                  _progressDialog.updateProgressBarPrecent(100);
              }
            }
            else if (eventEnum instanceof InnerBarrierEventEnum)
            {
              if (event.isStart() == false && (eventEnum.equals(InnerBarrierEventEnum.BARRIER_OPEN) ||
                                               eventEnum.equals(InnerBarrierEventEnum.BARRIER_CLOSE)))
              {
                populateInnerBarrierPanel();
              }
            }
          }
          else if (object instanceof XrayTesterException)
          {
            handleXrayTesterException((XrayTesterException)object);
          }
        }
        else
          Assert.expect(false);
      }
    });
  }
}
